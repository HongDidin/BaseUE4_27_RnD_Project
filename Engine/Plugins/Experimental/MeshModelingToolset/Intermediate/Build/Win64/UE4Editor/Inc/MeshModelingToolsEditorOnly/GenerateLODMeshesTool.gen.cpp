// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingToolsEditorOnly/Public/GenerateLODMeshesTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGenerateLODMeshesTool() {}
// Cross Module References
	MESHMODELINGTOOLSEDITORONLY_API UScriptStruct* Z_Construct_UScriptStruct_FLODLevelGenerateSettings();
	UPackage* Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly();
	MODELINGOPERATORSEDITORONLY_API UEnum* Z_Construct_UEnum_ModelingOperatorsEditorOnly_ESimplifyType();
	MODELINGOPERATORSEDITORONLY_API UEnum* Z_Construct_UEnum_ModelingOperatorsEditorOnly_ESimplifyTargetType();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UGenerateLODMeshesToolBuilder_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UGenerateLODMeshesToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UGenerateLODMeshesToolProperties_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UGenerateLODMeshesToolProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshConstraintProperties();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UGenerateLODMeshesTool_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UGenerateLODMeshesTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_USingleSelectionTool();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_NoRegister();
// End Cross Module References
class UScriptStruct* FLODLevelGenerateSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MESHMODELINGTOOLSEDITORONLY_API uint32 Get_Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FLODLevelGenerateSettings, Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly(), TEXT("LODLevelGenerateSettings"), sizeof(FLODLevelGenerateSettings), Get_Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Hash());
	}
	return Singleton;
}
template<> MESHMODELINGTOOLSEDITORONLY_API UScriptStruct* StaticStruct<FLODLevelGenerateSettings>()
{
	return FLODLevelGenerateSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FLODLevelGenerateSettings(FLODLevelGenerateSettings::StaticStruct, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("LODLevelGenerateSettings"), false, nullptr, nullptr);
static struct FScriptStruct_MeshModelingToolsEditorOnly_StaticRegisterNativesFLODLevelGenerateSettings
{
	FScriptStruct_MeshModelingToolsEditorOnly_StaticRegisterNativesFLODLevelGenerateSettings()
	{
		UScriptStruct::DeferCppStructOps<FLODLevelGenerateSettings>(FName(TEXT("LODLevelGenerateSettings")));
	}
} ScriptStruct_MeshModelingToolsEditorOnly_StaticRegisterNativesFLODLevelGenerateSettings;
	struct Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_SimplifierType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SimplifierType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_SimplifierType;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TargetMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TargetMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetPercentage_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_TargetPercentage;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_TargetCount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bReproject_MetaData[];
#endif
		static void NewProp_bReproject_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bReproject;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/GenerateLODMeshesTool.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FLODLevelGenerateSettings>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::NewProp_SimplifierType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::NewProp_SimplifierType_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Simplification Scheme  */" },
		{ "ModuleRelativePath", "Public/GenerateLODMeshesTool.h" },
		{ "ToolTip", "Simplification Scheme" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::NewProp_SimplifierType = { "SimplifierType", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLODLevelGenerateSettings, SimplifierType), Z_Construct_UEnum_ModelingOperatorsEditorOnly_ESimplifyType, METADATA_PARAMS(Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::NewProp_SimplifierType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::NewProp_SimplifierType_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::NewProp_TargetMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::NewProp_TargetMode_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Simplification Target Type  */" },
		{ "ModuleRelativePath", "Public/GenerateLODMeshesTool.h" },
		{ "ToolTip", "Simplification Target Type" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::NewProp_TargetMode = { "TargetMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLODLevelGenerateSettings, TargetMode), Z_Construct_UEnum_ModelingOperatorsEditorOnly_ESimplifyTargetType, METADATA_PARAMS(Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::NewProp_TargetMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::NewProp_TargetMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::NewProp_TargetPercentage_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Target percentage */" },
		{ "EditCondition", "TargetMode == ESimplifyTargetType::Percentage" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/GenerateLODMeshesTool.h" },
		{ "ToolTip", "Target percentage" },
		{ "UIMax", "100" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::NewProp_TargetPercentage = { "TargetPercentage", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLODLevelGenerateSettings, TargetPercentage), METADATA_PARAMS(Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::NewProp_TargetPercentage_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::NewProp_TargetPercentage_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::NewProp_TargetCount_MetaData[] = {
		{ "Category", "Options" },
		{ "ClampMax", "9999999999" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Target vertex/triangle count */" },
		{ "EditCondition", "TargetMode == ESimplifyTargetType::TriangleCount || TargetMode == ESimplifyTargetType::VertexCount" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/GenerateLODMeshesTool.h" },
		{ "ToolTip", "Target vertex/triangle count" },
		{ "UIMax", "10000" },
		{ "UIMin", "4" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::NewProp_TargetCount = { "TargetCount", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLODLevelGenerateSettings, TargetCount), METADATA_PARAMS(Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::NewProp_TargetCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::NewProp_TargetCount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::NewProp_bReproject_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Target vertex/triangle count */" },
		{ "ModuleRelativePath", "Public/GenerateLODMeshesTool.h" },
		{ "ToolTip", "Target vertex/triangle count" },
	};
#endif
	void Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::NewProp_bReproject_SetBit(void* Obj)
	{
		((FLODLevelGenerateSettings*)Obj)->bReproject = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::NewProp_bReproject = { "bReproject", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FLODLevelGenerateSettings), &Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::NewProp_bReproject_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::NewProp_bReproject_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::NewProp_bReproject_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::NewProp_Result_MetaData[] = {
		{ "Category", "Options" },
		{ "ModuleRelativePath", "Public/GenerateLODMeshesTool.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLODLevelGenerateSettings, Result), METADATA_PARAMS(Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::NewProp_SimplifierType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::NewProp_SimplifierType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::NewProp_TargetMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::NewProp_TargetMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::NewProp_TargetPercentage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::NewProp_TargetCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::NewProp_bReproject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
		nullptr,
		&NewStructOps,
		"LODLevelGenerateSettings",
		sizeof(FLODLevelGenerateSettings),
		alignof(FLODLevelGenerateSettings),
		Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FLODLevelGenerateSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("LODLevelGenerateSettings"), sizeof(FLODLevelGenerateSettings), Get_Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FLODLevelGenerateSettings_Hash() { return 1139052838U; }
	void UGenerateLODMeshesToolBuilder::StaticRegisterNativesUGenerateLODMeshesToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UGenerateLODMeshesToolBuilder_NoRegister()
	{
		return UGenerateLODMeshesToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UGenerateLODMeshesToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGenerateLODMeshesToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGenerateLODMeshesToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "GenerateLODMeshesTool.h" },
		{ "ModuleRelativePath", "Public/GenerateLODMeshesTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGenerateLODMeshesToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGenerateLODMeshesToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGenerateLODMeshesToolBuilder_Statics::ClassParams = {
		&UGenerateLODMeshesToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UGenerateLODMeshesToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGenerateLODMeshesToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGenerateLODMeshesToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGenerateLODMeshesToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGenerateLODMeshesToolBuilder, 659550154);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UGenerateLODMeshesToolBuilder>()
	{
		return UGenerateLODMeshesToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGenerateLODMeshesToolBuilder(Z_Construct_UClass_UGenerateLODMeshesToolBuilder, &UGenerateLODMeshesToolBuilder::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UGenerateLODMeshesToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGenerateLODMeshesToolBuilder);
	void UGenerateLODMeshesToolProperties::StaticRegisterNativesUGenerateLODMeshesToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UGenerateLODMeshesToolProperties_NoRegister()
	{
		return UGenerateLODMeshesToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TargetMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TargetMode;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_SimplifierType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SimplifierType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_SimplifierType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NameIndexBase_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_NameIndexBase;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetPercentage_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_TargetPercentage;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetEdgeLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TargetEdgeLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_TargetCount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDiscardAttributes_MetaData[];
#endif
		static void NewProp_bDiscardAttributes_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDiscardAttributes;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowWireframe_MetaData[];
#endif
		static void NewProp_bShowWireframe_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowWireframe;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowGroupColors_MetaData[];
#endif
		static void NewProp_bShowGroupColors_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowGroupColors;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bReproject_MetaData[];
#endif
		static void NewProp_bReproject_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bReproject;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LODLevels_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LODLevels_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_LODLevels;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshConstraintProperties,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Standard properties of the Simplify operation\n */" },
		{ "IncludePath", "GenerateLODMeshesTool.h" },
		{ "ModuleRelativePath", "Public/GenerateLODMeshesTool.h" },
		{ "ToolTip", "Standard properties of the Simplify operation" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_TargetMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_TargetMode_MetaData[] = {
		{ "Comment", "/** Simplification Target Type  *///UPROPERTY(EditAnywhere, Category = Options)\n" },
		{ "ModuleRelativePath", "Public/GenerateLODMeshesTool.h" },
		{ "ToolTip", "Simplification Target Type  //UPROPERTY(EditAnywhere, Category = Options)" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_TargetMode = { "TargetMode", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGenerateLODMeshesToolProperties, TargetMode), Z_Construct_UEnum_ModelingOperatorsEditorOnly_ESimplifyTargetType, METADATA_PARAMS(Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_TargetMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_TargetMode_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_SimplifierType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_SimplifierType_MetaData[] = {
		{ "Comment", "/** Simplification Scheme  *///UPROPERTY(EditAnywhere, Category = Options)\n" },
		{ "ModuleRelativePath", "Public/GenerateLODMeshesTool.h" },
		{ "ToolTip", "Simplification Scheme  //UPROPERTY(EditAnywhere, Category = Options)" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_SimplifierType = { "SimplifierType", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGenerateLODMeshesToolProperties, SimplifierType), Z_Construct_UEnum_ModelingOperatorsEditorOnly_ESimplifyType, METADATA_PARAMS(Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_SimplifierType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_SimplifierType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_NameIndexBase_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Output LOD Assets will be numbered starting at this number */" },
		{ "ModuleRelativePath", "Public/GenerateLODMeshesTool.h" },
		{ "ToolTip", "Output LOD Assets will be numbered starting at this number" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_NameIndexBase = { "NameIndexBase", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGenerateLODMeshesToolProperties, NameIndexBase), METADATA_PARAMS(Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_NameIndexBase_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_NameIndexBase_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_TargetPercentage_MetaData[] = {
		{ "Comment", "/** Target percentage of original triangle count *///UPROPERTY(EditAnywhere, Category = Options, meta = (UIMin = \"0\", UIMax = \"100\", EditCondition = \"TargetMode == ESimplifyTargetType::Percentage\"))\n" },
		{ "ModuleRelativePath", "Public/GenerateLODMeshesTool.h" },
		{ "ToolTip", "Target percentage of original triangle count //UPROPERTY(EditAnywhere, Category = Options, meta = (UIMin = \"0\", UIMax = \"100\", EditCondition = \"TargetMode == ESimplifyTargetType::Percentage\"))" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_TargetPercentage = { "TargetPercentage", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGenerateLODMeshesToolProperties, TargetPercentage), METADATA_PARAMS(Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_TargetPercentage_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_TargetPercentage_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_TargetEdgeLength_MetaData[] = {
		{ "Comment", "/** Target edge length *///UPROPERTY(EditAnywhere, Category = Options, meta = (UIMin = \"3.0\", UIMax = \"10.0\", ClampMin = \"0.001\", ClampMax = \"1000.0\", EditCondition = \"TargetMode == ESimplifyTargetType::EdgeLength && SimplifierType != ESimplifyType::UE4Standard\"))\n" },
		{ "ModuleRelativePath", "Public/GenerateLODMeshesTool.h" },
		{ "ToolTip", "Target edge length //UPROPERTY(EditAnywhere, Category = Options, meta = (UIMin = \"3.0\", UIMax = \"10.0\", ClampMin = \"0.001\", ClampMax = \"1000.0\", EditCondition = \"TargetMode == ESimplifyTargetType::EdgeLength && SimplifierType != ESimplifyType::UE4Standard\"))" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_TargetEdgeLength = { "TargetEdgeLength", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGenerateLODMeshesToolProperties, TargetEdgeLength), METADATA_PARAMS(Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_TargetEdgeLength_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_TargetEdgeLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_TargetCount_MetaData[] = {
		{ "Comment", "/** Target triangle/vertex count *///UPROPERTY(EditAnywhere, Category = Options, meta = (UIMin = \"4\", UIMax = \"10000\", ClampMin = \"1\", ClampMax = \"9999999999\", EditCondition = \"TargetMode == ESimplifyTargetType::TriangleCount\"))\n" },
		{ "ModuleRelativePath", "Public/GenerateLODMeshesTool.h" },
		{ "ToolTip", "Target triangle/vertex count //UPROPERTY(EditAnywhere, Category = Options, meta = (UIMin = \"4\", UIMax = \"10000\", ClampMin = \"1\", ClampMax = \"9999999999\", EditCondition = \"TargetMode == ESimplifyTargetType::TriangleCount\"))" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_TargetCount = { "TargetCount", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGenerateLODMeshesToolProperties, TargetCount), METADATA_PARAMS(Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_TargetCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_TargetCount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_bDiscardAttributes_MetaData[] = {
		{ "Comment", "/** If true, UVs and Normals are discarded  *///UPROPERTY(EditAnywhere, Category = Options)\n" },
		{ "ModuleRelativePath", "Public/GenerateLODMeshesTool.h" },
		{ "ToolTip", "If true, UVs and Normals are discarded  //UPROPERTY(EditAnywhere, Category = Options)" },
	};
#endif
	void Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_bDiscardAttributes_SetBit(void* Obj)
	{
		((UGenerateLODMeshesToolProperties*)Obj)->bDiscardAttributes = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_bDiscardAttributes = { "bDiscardAttributes", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UGenerateLODMeshesToolProperties), &Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_bDiscardAttributes_SetBit, METADATA_PARAMS(Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_bDiscardAttributes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_bDiscardAttributes_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_bShowWireframe_MetaData[] = {
		{ "Category", "Display" },
		{ "Comment", "/** If true, display wireframe */" },
		{ "ModuleRelativePath", "Public/GenerateLODMeshesTool.h" },
		{ "ToolTip", "If true, display wireframe" },
	};
#endif
	void Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_bShowWireframe_SetBit(void* Obj)
	{
		((UGenerateLODMeshesToolProperties*)Obj)->bShowWireframe = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_bShowWireframe = { "bShowWireframe", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UGenerateLODMeshesToolProperties), &Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_bShowWireframe_SetBit, METADATA_PARAMS(Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_bShowWireframe_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_bShowWireframe_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_bShowGroupColors_MetaData[] = {
		{ "Comment", "/** Display colors corresponding to the mesh's polygon groups *///UPROPERTY(EditAnywhere, Category = Display)\n" },
		{ "ModuleRelativePath", "Public/GenerateLODMeshesTool.h" },
		{ "ToolTip", "Display colors corresponding to the mesh's polygon groups //UPROPERTY(EditAnywhere, Category = Display)" },
	};
#endif
	void Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_bShowGroupColors_SetBit(void* Obj)
	{
		((UGenerateLODMeshesToolProperties*)Obj)->bShowGroupColors = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_bShowGroupColors = { "bShowGroupColors", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UGenerateLODMeshesToolProperties), &Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_bShowGroupColors_SetBit, METADATA_PARAMS(Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_bShowGroupColors_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_bShowGroupColors_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_bReproject_MetaData[] = {
		{ "Comment", "/** Enable projection back to input mesh *///UPROPERTY(EditAnywhere, Category = Options, AdvancedDisplay)\n" },
		{ "ModuleRelativePath", "Public/GenerateLODMeshesTool.h" },
		{ "ToolTip", "Enable projection back to input mesh //UPROPERTY(EditAnywhere, Category = Options, AdvancedDisplay)" },
	};
#endif
	void Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_bReproject_SetBit(void* Obj)
	{
		((UGenerateLODMeshesToolProperties*)Obj)->bReproject = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_bReproject = { "bReproject", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UGenerateLODMeshesToolProperties), &Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_bReproject_SetBit, METADATA_PARAMS(Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_bReproject_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_bReproject_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_LODLevels_Inner = { "LODLevels", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FLODLevelGenerateSettings, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_LODLevels_MetaData[] = {
		{ "Category", "Options" },
		{ "ModuleRelativePath", "Public/GenerateLODMeshesTool.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_LODLevels = { "LODLevels", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGenerateLODMeshesToolProperties, LODLevels), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_LODLevels_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_LODLevels_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_TargetMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_TargetMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_SimplifierType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_SimplifierType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_NameIndexBase,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_TargetPercentage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_TargetEdgeLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_TargetCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_bDiscardAttributes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_bShowWireframe,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_bShowGroupColors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_bReproject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_LODLevels_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::NewProp_LODLevels,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGenerateLODMeshesToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::ClassParams = {
		&UGenerateLODMeshesToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGenerateLODMeshesToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGenerateLODMeshesToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGenerateLODMeshesToolProperties, 431936766);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UGenerateLODMeshesToolProperties>()
	{
		return UGenerateLODMeshesToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGenerateLODMeshesToolProperties(Z_Construct_UClass_UGenerateLODMeshesToolProperties, &UGenerateLODMeshesToolProperties::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UGenerateLODMeshesToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGenerateLODMeshesToolProperties);
	void UGenerateLODMeshesTool::StaticRegisterNativesUGenerateLODMeshesTool()
	{
	}
	UClass* Z_Construct_UClass_UGenerateLODMeshesTool_NoRegister()
	{
		return UGenerateLODMeshesTool::StaticClass();
	}
	struct Z_Construct_UClass_UGenerateLODMeshesTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SimplifyProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SimplifyProperties;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Previews_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Previews_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Previews;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGenerateLODMeshesTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USingleSelectionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGenerateLODMeshesTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Simple Mesh Simplifying Tool\n */" },
		{ "IncludePath", "GenerateLODMeshesTool.h" },
		{ "ModuleRelativePath", "Public/GenerateLODMeshesTool.h" },
		{ "ToolTip", "Simple Mesh Simplifying Tool" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGenerateLODMeshesTool_Statics::NewProp_SimplifyProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/GenerateLODMeshesTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGenerateLODMeshesTool_Statics::NewProp_SimplifyProperties = { "SimplifyProperties", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGenerateLODMeshesTool, SimplifyProperties), Z_Construct_UClass_UGenerateLODMeshesToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGenerateLODMeshesTool_Statics::NewProp_SimplifyProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGenerateLODMeshesTool_Statics::NewProp_SimplifyProperties_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGenerateLODMeshesTool_Statics::NewProp_Previews_Inner = { "Previews", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGenerateLODMeshesTool_Statics::NewProp_Previews_MetaData[] = {
		{ "ModuleRelativePath", "Public/GenerateLODMeshesTool.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UGenerateLODMeshesTool_Statics::NewProp_Previews = { "Previews", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGenerateLODMeshesTool, Previews), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UGenerateLODMeshesTool_Statics::NewProp_Previews_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGenerateLODMeshesTool_Statics::NewProp_Previews_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGenerateLODMeshesTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGenerateLODMeshesTool_Statics::NewProp_SimplifyProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGenerateLODMeshesTool_Statics::NewProp_Previews_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGenerateLODMeshesTool_Statics::NewProp_Previews,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGenerateLODMeshesTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGenerateLODMeshesTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGenerateLODMeshesTool_Statics::ClassParams = {
		&UGenerateLODMeshesTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UGenerateLODMeshesTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UGenerateLODMeshesTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UGenerateLODMeshesTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGenerateLODMeshesTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGenerateLODMeshesTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGenerateLODMeshesTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGenerateLODMeshesTool, 1842821639);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UGenerateLODMeshesTool>()
	{
		return UGenerateLODMeshesTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGenerateLODMeshesTool(Z_Construct_UClass_UGenerateLODMeshesTool, &UGenerateLODMeshesTool::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UGenerateLODMeshesTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGenerateLODMeshesTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
