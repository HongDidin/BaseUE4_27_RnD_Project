// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLS_PositionPlaneGizmo_generated_h
#error "PositionPlaneGizmo.generated.h already included, missing '#pragma once' in PositionPlaneGizmo.h"
#endif
#define MESHMODELINGTOOLS_PositionPlaneGizmo_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_20_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_20_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_20_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPositionPlaneGizmoBuilder(); \
	friend struct Z_Construct_UClass_UPositionPlaneGizmoBuilder_Statics; \
public: \
	DECLARE_CLASS(UPositionPlaneGizmoBuilder, UInteractiveGizmoBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UPositionPlaneGizmoBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_20_INCLASS \
private: \
	static void StaticRegisterNativesUPositionPlaneGizmoBuilder(); \
	friend struct Z_Construct_UClass_UPositionPlaneGizmoBuilder_Statics; \
public: \
	DECLARE_CLASS(UPositionPlaneGizmoBuilder, UInteractiveGizmoBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UPositionPlaneGizmoBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPositionPlaneGizmoBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPositionPlaneGizmoBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPositionPlaneGizmoBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPositionPlaneGizmoBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPositionPlaneGizmoBuilder(UPositionPlaneGizmoBuilder&&); \
	NO_API UPositionPlaneGizmoBuilder(const UPositionPlaneGizmoBuilder&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_20_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPositionPlaneGizmoBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPositionPlaneGizmoBuilder(UPositionPlaneGizmoBuilder&&); \
	NO_API UPositionPlaneGizmoBuilder(const UPositionPlaneGizmoBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPositionPlaneGizmoBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPositionPlaneGizmoBuilder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPositionPlaneGizmoBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_20_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_17_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_20_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_20_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_20_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_20_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_20_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UPositionPlaneGizmoBuilder>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_35_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_35_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_35_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_35_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPositionPlaneGizmo(); \
	friend struct Z_Construct_UClass_UPositionPlaneGizmo_Statics; \
public: \
	DECLARE_CLASS(UPositionPlaneGizmo, UInteractiveGizmo, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UPositionPlaneGizmo)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_35_INCLASS \
private: \
	static void StaticRegisterNativesUPositionPlaneGizmo(); \
	friend struct Z_Construct_UClass_UPositionPlaneGizmo_Statics; \
public: \
	DECLARE_CLASS(UPositionPlaneGizmo, UInteractiveGizmo, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UPositionPlaneGizmo)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_35_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPositionPlaneGizmo(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPositionPlaneGizmo) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPositionPlaneGizmo); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPositionPlaneGizmo); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPositionPlaneGizmo(UPositionPlaneGizmo&&); \
	NO_API UPositionPlaneGizmo(const UPositionPlaneGizmo&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_35_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPositionPlaneGizmo() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPositionPlaneGizmo(UPositionPlaneGizmo&&); \
	NO_API UPositionPlaneGizmo(const UPositionPlaneGizmo&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPositionPlaneGizmo); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPositionPlaneGizmo); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UPositionPlaneGizmo)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_35_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_32_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_35_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_35_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_35_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_35_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_35_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_35_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_35_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_35_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_35_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_35_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_35_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_35_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UPositionPlaneGizmo>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_104_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_104_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_104_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_104_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPositionPlaneOnSceneInputBehavior(); \
	friend struct Z_Construct_UClass_UPositionPlaneOnSceneInputBehavior_Statics; \
public: \
	DECLARE_CLASS(UPositionPlaneOnSceneInputBehavior, UAnyButtonInputBehavior, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UPositionPlaneOnSceneInputBehavior)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_104_INCLASS \
private: \
	static void StaticRegisterNativesUPositionPlaneOnSceneInputBehavior(); \
	friend struct Z_Construct_UClass_UPositionPlaneOnSceneInputBehavior_Statics; \
public: \
	DECLARE_CLASS(UPositionPlaneOnSceneInputBehavior, UAnyButtonInputBehavior, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UPositionPlaneOnSceneInputBehavior)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_104_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPositionPlaneOnSceneInputBehavior(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPositionPlaneOnSceneInputBehavior) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPositionPlaneOnSceneInputBehavior); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPositionPlaneOnSceneInputBehavior); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPositionPlaneOnSceneInputBehavior(UPositionPlaneOnSceneInputBehavior&&); \
	NO_API UPositionPlaneOnSceneInputBehavior(const UPositionPlaneOnSceneInputBehavior&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_104_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPositionPlaneOnSceneInputBehavior() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPositionPlaneOnSceneInputBehavior(UPositionPlaneOnSceneInputBehavior&&); \
	NO_API UPositionPlaneOnSceneInputBehavior(const UPositionPlaneOnSceneInputBehavior&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPositionPlaneOnSceneInputBehavior); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPositionPlaneOnSceneInputBehavior); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UPositionPlaneOnSceneInputBehavior)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_104_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_101_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_104_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_104_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_104_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_104_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_104_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_104_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_104_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_104_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_104_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_104_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_104_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h_104_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UPositionPlaneOnSceneInputBehavior>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PositionPlaneGizmo_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
