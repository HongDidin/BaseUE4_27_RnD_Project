// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/Sculpting/MeshSculptToolBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMeshSculptToolBase() {}
// Cross Module References
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EMeshSculptFalloffType();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_USculptBrushProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_USculptBrushProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UBrushBaseProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UKelvinBrushProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UKelvinBrushProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UWorkPlaneProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UWorkPlaneProperties();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FQuat();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_USculptMaxBrushProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_USculptMaxBrushProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshSculptToolBase_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshSculptToolBase();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UMeshSurfacePointTool();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshSculptBrushOpProps_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshEditingViewProperties_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UBrushStampIndicator_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPreviewMesh_NoRegister();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UTransformGizmo_NoRegister();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UTransformProxy_NoRegister();
// End Cross Module References
	static UEnum* EMeshSculptFalloffType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EMeshSculptFalloffType, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EMeshSculptFalloffType"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EMeshSculptFalloffType>()
	{
		return EMeshSculptFalloffType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMeshSculptFalloffType(EMeshSculptFalloffType_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EMeshSculptFalloffType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EMeshSculptFalloffType_Hash() { return 1075883979U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EMeshSculptFalloffType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMeshSculptFalloffType"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EMeshSculptFalloffType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMeshSculptFalloffType::Smooth", (int64)EMeshSculptFalloffType::Smooth },
				{ "EMeshSculptFalloffType::Linear", (int64)EMeshSculptFalloffType::Linear },
				{ "EMeshSculptFalloffType::Inverse", (int64)EMeshSculptFalloffType::Inverse },
				{ "EMeshSculptFalloffType::Round", (int64)EMeshSculptFalloffType::Round },
				{ "EMeshSculptFalloffType::BoxSmooth", (int64)EMeshSculptFalloffType::BoxSmooth },
				{ "EMeshSculptFalloffType::BoxLinear", (int64)EMeshSculptFalloffType::BoxLinear },
				{ "EMeshSculptFalloffType::BoxInverse", (int64)EMeshSculptFalloffType::BoxInverse },
				{ "EMeshSculptFalloffType::BoxRound", (int64)EMeshSculptFalloffType::BoxRound },
				{ "EMeshSculptFalloffType::LastValue", (int64)EMeshSculptFalloffType::LastValue },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BoxInverse.Name", "EMeshSculptFalloffType::BoxInverse" },
				{ "BoxLinear.Name", "EMeshSculptFalloffType::BoxLinear" },
				{ "BoxRound.Name", "EMeshSculptFalloffType::BoxRound" },
				{ "BoxSmooth.Name", "EMeshSculptFalloffType::BoxSmooth" },
				{ "Comment", "/** Mesh Sculpting Brush Types */" },
				{ "Inverse.Name", "EMeshSculptFalloffType::Inverse" },
				{ "LastValue.Hidden", "" },
				{ "LastValue.Name", "EMeshSculptFalloffType::LastValue" },
				{ "Linear.Name", "EMeshSculptFalloffType::Linear" },
				{ "ModuleRelativePath", "Public/Sculpting/MeshSculptToolBase.h" },
				{ "Round.Name", "EMeshSculptFalloffType::Round" },
				{ "Smooth.Name", "EMeshSculptFalloffType::Smooth" },
				{ "ToolTip", "Mesh Sculpting Brush Types" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EMeshSculptFalloffType",
				"EMeshSculptFalloffType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void USculptBrushProperties::StaticRegisterNativesUSculptBrushProperties()
	{
	}
	UClass* Z_Construct_UClass_USculptBrushProperties_NoRegister()
	{
		return USculptBrushProperties::StaticClass();
	}
	struct Z_Construct_UClass_USculptBrushProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Depth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Depth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bHitBackFaces_MetaData[];
#endif
		static void NewProp_bHitBackFaces_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bHitBackFaces;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Lazyness_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Lazyness;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowPerBrushProps_MetaData[];
#endif
		static void NewProp_bShowPerBrushProps_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowPerBrushProps;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USculptBrushProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBrushBaseProperties,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USculptBrushProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Sculpting/MeshSculptToolBase.h" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshSculptToolBase.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USculptBrushProperties_Statics::NewProp_Depth_MetaData[] = {
		{ "Category", "Brush" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "-1.0" },
		{ "Comment", "/** Depth of Brush into surface along view ray or surface normal, depending on the Active Brush Type */" },
		{ "DisplayPriority", "5" },
		{ "EditCondition", "bShowPerBrushProps" },
		{ "EditConditionHides", "" },
		{ "HideEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshSculptToolBase.h" },
		{ "ToolTip", "Depth of Brush into surface along view ray or surface normal, depending on the Active Brush Type" },
		{ "UIMax", "0.5" },
		{ "UIMin", "-0.5" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_USculptBrushProperties_Statics::NewProp_Depth = { "Depth", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USculptBrushProperties, Depth), METADATA_PARAMS(Z_Construct_UClass_USculptBrushProperties_Statics::NewProp_Depth_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USculptBrushProperties_Statics::NewProp_Depth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USculptBrushProperties_Statics::NewProp_bHitBackFaces_MetaData[] = {
		{ "Category", "Brush" },
		{ "Comment", "/** Allow the Brush to hit the back-side of the mesh */" },
		{ "DisplayPriority", "6" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshSculptToolBase.h" },
		{ "ToolTip", "Allow the Brush to hit the back-side of the mesh" },
	};
#endif
	void Z_Construct_UClass_USculptBrushProperties_Statics::NewProp_bHitBackFaces_SetBit(void* Obj)
	{
		((USculptBrushProperties*)Obj)->bHitBackFaces = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USculptBrushProperties_Statics::NewProp_bHitBackFaces = { "bHitBackFaces", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(USculptBrushProperties), &Z_Construct_UClass_USculptBrushProperties_Statics::NewProp_bHitBackFaces_SetBit, METADATA_PARAMS(Z_Construct_UClass_USculptBrushProperties_Statics::NewProp_bHitBackFaces_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USculptBrushProperties_Statics::NewProp_bHitBackFaces_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USculptBrushProperties_Statics::NewProp_Lazyness_MetaData[] = {
		{ "Category", "Brush" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0.0" },
		{ "DisplayPriority", "7" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshSculptToolBase.h" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_USculptBrushProperties_Statics::NewProp_Lazyness = { "Lazyness", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USculptBrushProperties, Lazyness), METADATA_PARAMS(Z_Construct_UClass_USculptBrushProperties_Statics::NewProp_Lazyness_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USculptBrushProperties_Statics::NewProp_Lazyness_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USculptBrushProperties_Statics::NewProp_bShowPerBrushProps_MetaData[] = {
		{ "Comment", "/**  */" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshSculptToolBase.h" },
		{ "TransientToolProperty", "" },
	};
#endif
	void Z_Construct_UClass_USculptBrushProperties_Statics::NewProp_bShowPerBrushProps_SetBit(void* Obj)
	{
		((USculptBrushProperties*)Obj)->bShowPerBrushProps = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USculptBrushProperties_Statics::NewProp_bShowPerBrushProps = { "bShowPerBrushProps", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(USculptBrushProperties), &Z_Construct_UClass_USculptBrushProperties_Statics::NewProp_bShowPerBrushProps_SetBit, METADATA_PARAMS(Z_Construct_UClass_USculptBrushProperties_Statics::NewProp_bShowPerBrushProps_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USculptBrushProperties_Statics::NewProp_bShowPerBrushProps_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USculptBrushProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USculptBrushProperties_Statics::NewProp_Depth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USculptBrushProperties_Statics::NewProp_bHitBackFaces,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USculptBrushProperties_Statics::NewProp_Lazyness,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USculptBrushProperties_Statics::NewProp_bShowPerBrushProps,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USculptBrushProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USculptBrushProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USculptBrushProperties_Statics::ClassParams = {
		&USculptBrushProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USculptBrushProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USculptBrushProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_USculptBrushProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USculptBrushProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USculptBrushProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USculptBrushProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USculptBrushProperties, 2691128369);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<USculptBrushProperties>()
	{
		return USculptBrushProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USculptBrushProperties(Z_Construct_UClass_USculptBrushProperties, &USculptBrushProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("USculptBrushProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USculptBrushProperties);
	void UKelvinBrushProperties::StaticRegisterNativesUKelvinBrushProperties()
	{
	}
	UClass* Z_Construct_UClass_UKelvinBrushProperties_NoRegister()
	{
		return UKelvinBrushProperties::StaticClass();
	}
	struct Z_Construct_UClass_UKelvinBrushProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FallOffDistance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FallOffDistance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Stiffness_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Stiffness;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Incompressiblity_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Incompressiblity;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BrushSteps_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_BrushSteps;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UKelvinBrushProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UKelvinBrushProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Sculpting/MeshSculptToolBase.h" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshSculptToolBase.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UKelvinBrushProperties_Statics::NewProp_FallOffDistance_MetaData[] = {
		{ "Category", "Kelvin" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Brush Fall off as fraction of brush size*/" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshSculptToolBase.h" },
		{ "ToolTip", "Brush Fall off as fraction of brush size" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UKelvinBrushProperties_Statics::NewProp_FallOffDistance = { "FallOffDistance", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UKelvinBrushProperties, FallOffDistance), METADATA_PARAMS(Z_Construct_UClass_UKelvinBrushProperties_Statics::NewProp_FallOffDistance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UKelvinBrushProperties_Statics::NewProp_FallOffDistance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UKelvinBrushProperties_Statics::NewProp_Stiffness_MetaData[] = {
		{ "Category", "Kelvin" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** How much the mesh resists shear */" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshSculptToolBase.h" },
		{ "ToolTip", "How much the mesh resists shear" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UKelvinBrushProperties_Statics::NewProp_Stiffness = { "Stiffness", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UKelvinBrushProperties, Stiffness), METADATA_PARAMS(Z_Construct_UClass_UKelvinBrushProperties_Statics::NewProp_Stiffness_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UKelvinBrushProperties_Statics::NewProp_Stiffness_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UKelvinBrushProperties_Statics::NewProp_Incompressiblity_MetaData[] = {
		{ "Category", "Kelvin" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** How compressible the spatial region is: 1 - 2 x Poisson ratio */" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshSculptToolBase.h" },
		{ "ToolTip", "How compressible the spatial region is: 1 - 2 x Poisson ratio" },
		{ "UIMax", "0.1" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UKelvinBrushProperties_Statics::NewProp_Incompressiblity = { "Incompressiblity", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UKelvinBrushProperties, Incompressiblity), METADATA_PARAMS(Z_Construct_UClass_UKelvinBrushProperties_Statics::NewProp_Incompressiblity_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UKelvinBrushProperties_Statics::NewProp_Incompressiblity_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UKelvinBrushProperties_Statics::NewProp_BrushSteps_MetaData[] = {
		{ "Category", "Kelvin" },
		{ "ClampMax", "100" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Integration steps*/" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshSculptToolBase.h" },
		{ "ToolTip", "Integration steps" },
		{ "UIMax", "100" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UKelvinBrushProperties_Statics::NewProp_BrushSteps = { "BrushSteps", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UKelvinBrushProperties, BrushSteps), METADATA_PARAMS(Z_Construct_UClass_UKelvinBrushProperties_Statics::NewProp_BrushSteps_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UKelvinBrushProperties_Statics::NewProp_BrushSteps_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UKelvinBrushProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UKelvinBrushProperties_Statics::NewProp_FallOffDistance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UKelvinBrushProperties_Statics::NewProp_Stiffness,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UKelvinBrushProperties_Statics::NewProp_Incompressiblity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UKelvinBrushProperties_Statics::NewProp_BrushSteps,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UKelvinBrushProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UKelvinBrushProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UKelvinBrushProperties_Statics::ClassParams = {
		&UKelvinBrushProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UKelvinBrushProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UKelvinBrushProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UKelvinBrushProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UKelvinBrushProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UKelvinBrushProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UKelvinBrushProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UKelvinBrushProperties, 3619602918);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UKelvinBrushProperties>()
	{
		return UKelvinBrushProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UKelvinBrushProperties(Z_Construct_UClass_UKelvinBrushProperties, &UKelvinBrushProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UKelvinBrushProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UKelvinBrushProperties);
	void UWorkPlaneProperties::StaticRegisterNativesUWorkPlaneProperties()
	{
	}
	UClass* Z_Construct_UClass_UWorkPlaneProperties_NoRegister()
	{
		return UWorkPlaneProperties::StaticClass();
	}
	struct Z_Construct_UClass_UWorkPlaneProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPropertySetEnabled_MetaData[];
#endif
		static void NewProp_bPropertySetEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPropertySetEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowGizmo_MetaData[];
#endif
		static void NewProp_bShowGizmo_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowGizmo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSnapToGrid_MetaData[];
#endif
		static void NewProp_bSnapToGrid_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSnapToGrid;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Position_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Position;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Rotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Rotation;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UWorkPlaneProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWorkPlaneProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Sculpting/MeshSculptToolBase.h" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshSculptToolBase.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWorkPlaneProperties_Statics::NewProp_bPropertySetEnabled_MetaData[] = {
		{ "ModuleRelativePath", "Public/Sculpting/MeshSculptToolBase.h" },
		{ "TransientToolProperty", "" },
	};
#endif
	void Z_Construct_UClass_UWorkPlaneProperties_Statics::NewProp_bPropertySetEnabled_SetBit(void* Obj)
	{
		((UWorkPlaneProperties*)Obj)->bPropertySetEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UWorkPlaneProperties_Statics::NewProp_bPropertySetEnabled = { "bPropertySetEnabled", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UWorkPlaneProperties), &Z_Construct_UClass_UWorkPlaneProperties_Statics::NewProp_bPropertySetEnabled_SetBit, METADATA_PARAMS(Z_Construct_UClass_UWorkPlaneProperties_Statics::NewProp_bPropertySetEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWorkPlaneProperties_Statics::NewProp_bPropertySetEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWorkPlaneProperties_Statics::NewProp_bShowGizmo_MetaData[] = {
		{ "Category", "TargetPlane" },
		{ "Comment", "/** Toggle whether Work Plane Positioing Gizmo is visible */" },
		{ "EditCondition", "bPropertySetEnabled == true" },
		{ "HideEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshSculptToolBase.h" },
		{ "ToolTip", "Toggle whether Work Plane Positioing Gizmo is visible" },
	};
#endif
	void Z_Construct_UClass_UWorkPlaneProperties_Statics::NewProp_bShowGizmo_SetBit(void* Obj)
	{
		((UWorkPlaneProperties*)Obj)->bShowGizmo = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UWorkPlaneProperties_Statics::NewProp_bShowGizmo = { "bShowGizmo", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UWorkPlaneProperties), &Z_Construct_UClass_UWorkPlaneProperties_Statics::NewProp_bShowGizmo_SetBit, METADATA_PARAMS(Z_Construct_UClass_UWorkPlaneProperties_Statics::NewProp_bShowGizmo_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWorkPlaneProperties_Statics::NewProp_bShowGizmo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWorkPlaneProperties_Statics::NewProp_bSnapToGrid_MetaData[] = {
		{ "Category", "TargetPlane" },
		{ "Comment", "/** Toggle whether Work Plane snaps to grid when using Gizmo */" },
		{ "EditCondition", "bPropertySetEnabled == true" },
		{ "HideEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshSculptToolBase.h" },
		{ "ToolTip", "Toggle whether Work Plane snaps to grid when using Gizmo" },
	};
#endif
	void Z_Construct_UClass_UWorkPlaneProperties_Statics::NewProp_bSnapToGrid_SetBit(void* Obj)
	{
		((UWorkPlaneProperties*)Obj)->bSnapToGrid = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UWorkPlaneProperties_Statics::NewProp_bSnapToGrid = { "bSnapToGrid", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UWorkPlaneProperties), &Z_Construct_UClass_UWorkPlaneProperties_Statics::NewProp_bSnapToGrid_SetBit, METADATA_PARAMS(Z_Construct_UClass_UWorkPlaneProperties_Statics::NewProp_bSnapToGrid_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWorkPlaneProperties_Statics::NewProp_bSnapToGrid_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWorkPlaneProperties_Statics::NewProp_Position_MetaData[] = {
		{ "Category", "TargetPlane" },
		{ "EditCondition", "bPropertySetEnabled == true" },
		{ "HideEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshSculptToolBase.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UWorkPlaneProperties_Statics::NewProp_Position = { "Position", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWorkPlaneProperties, Position), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UWorkPlaneProperties_Statics::NewProp_Position_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWorkPlaneProperties_Statics::NewProp_Position_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWorkPlaneProperties_Statics::NewProp_Rotation_MetaData[] = {
		{ "Category", "TargetPlane" },
		{ "EditCondition", "bPropertySetEnabled == true" },
		{ "HideEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshSculptToolBase.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UWorkPlaneProperties_Statics::NewProp_Rotation = { "Rotation", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWorkPlaneProperties, Rotation), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UClass_UWorkPlaneProperties_Statics::NewProp_Rotation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWorkPlaneProperties_Statics::NewProp_Rotation_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UWorkPlaneProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWorkPlaneProperties_Statics::NewProp_bPropertySetEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWorkPlaneProperties_Statics::NewProp_bShowGizmo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWorkPlaneProperties_Statics::NewProp_bSnapToGrid,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWorkPlaneProperties_Statics::NewProp_Position,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWorkPlaneProperties_Statics::NewProp_Rotation,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UWorkPlaneProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UWorkPlaneProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UWorkPlaneProperties_Statics::ClassParams = {
		&UWorkPlaneProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UWorkPlaneProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UWorkPlaneProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UWorkPlaneProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UWorkPlaneProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UWorkPlaneProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UWorkPlaneProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UWorkPlaneProperties, 2203874134);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UWorkPlaneProperties>()
	{
		return UWorkPlaneProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UWorkPlaneProperties(Z_Construct_UClass_UWorkPlaneProperties, &UWorkPlaneProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UWorkPlaneProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UWorkPlaneProperties);
	void USculptMaxBrushProperties::StaticRegisterNativesUSculptMaxBrushProperties()
	{
	}
	UClass* Z_Construct_UClass_USculptMaxBrushProperties_NoRegister()
	{
		return USculptMaxBrushProperties::StaticClass();
	}
	struct Z_Construct_UClass_USculptMaxBrushProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxHeight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaxHeight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bFreezeCurrentHeight_MetaData[];
#endif
		static void NewProp_bFreezeCurrentHeight_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFreezeCurrentHeight;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USculptMaxBrushProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USculptMaxBrushProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Sculpting/MeshSculptToolBase.h" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshSculptToolBase.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USculptMaxBrushProperties_Statics::NewProp_MaxHeight_MetaData[] = {
		{ "Category", "SculptMaxBrush" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Specify maximum displacement height (relative to brush size) */" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshSculptToolBase.h" },
		{ "ToolTip", "Specify maximum displacement height (relative to brush size)" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_USculptMaxBrushProperties_Statics::NewProp_MaxHeight = { "MaxHeight", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USculptMaxBrushProperties, MaxHeight), METADATA_PARAMS(Z_Construct_UClass_USculptMaxBrushProperties_Statics::NewProp_MaxHeight_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USculptMaxBrushProperties_Statics::NewProp_MaxHeight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USculptMaxBrushProperties_Statics::NewProp_bFreezeCurrentHeight_MetaData[] = {
		{ "Category", "SculptMaxBrush" },
		{ "Comment", "/** Use maximum height from last brush stroke, regardless of brush size. Note that spatial brush falloff still applies.  */" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshSculptToolBase.h" },
		{ "ToolTip", "Use maximum height from last brush stroke, regardless of brush size. Note that spatial brush falloff still applies." },
	};
#endif
	void Z_Construct_UClass_USculptMaxBrushProperties_Statics::NewProp_bFreezeCurrentHeight_SetBit(void* Obj)
	{
		((USculptMaxBrushProperties*)Obj)->bFreezeCurrentHeight = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USculptMaxBrushProperties_Statics::NewProp_bFreezeCurrentHeight = { "bFreezeCurrentHeight", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(USculptMaxBrushProperties), &Z_Construct_UClass_USculptMaxBrushProperties_Statics::NewProp_bFreezeCurrentHeight_SetBit, METADATA_PARAMS(Z_Construct_UClass_USculptMaxBrushProperties_Statics::NewProp_bFreezeCurrentHeight_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USculptMaxBrushProperties_Statics::NewProp_bFreezeCurrentHeight_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USculptMaxBrushProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USculptMaxBrushProperties_Statics::NewProp_MaxHeight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USculptMaxBrushProperties_Statics::NewProp_bFreezeCurrentHeight,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USculptMaxBrushProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USculptMaxBrushProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USculptMaxBrushProperties_Statics::ClassParams = {
		&USculptMaxBrushProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USculptMaxBrushProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USculptMaxBrushProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_USculptMaxBrushProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USculptMaxBrushProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USculptMaxBrushProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USculptMaxBrushProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USculptMaxBrushProperties, 4179390449);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<USculptMaxBrushProperties>()
	{
		return USculptMaxBrushProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USculptMaxBrushProperties(Z_Construct_UClass_USculptMaxBrushProperties, &USculptMaxBrushProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("USculptMaxBrushProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USculptMaxBrushProperties);
	void UMeshSculptToolBase::StaticRegisterNativesUMeshSculptToolBase()
	{
	}
	UClass* Z_Construct_UClass_UMeshSculptToolBase_NoRegister()
	{
		return UMeshSculptToolBase::StaticClass();
	}
	struct Z_Construct_UClass_UMeshSculptToolBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BrushProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BrushProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GizmoProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_GizmoProperties;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BrushOpPropSets_ValueProp;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_BrushOpPropSets_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BrushOpPropSets_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_BrushOpPropSets;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SecondaryBrushOpPropSets_ValueProp;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SecondaryBrushOpPropSets_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SecondaryBrushOpPropSets_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_SecondaryBrushOpPropSets;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ViewProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActiveOverrideMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ActiveOverrideMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BrushIndicator_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BrushIndicator;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BrushIndicatorMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BrushIndicatorMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BrushIndicatorMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BrushIndicatorMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlaneTransformGizmo_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PlaneTransformGizmo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlaneTransformProxy_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PlaneTransformProxy;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshSculptToolBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshSurfacePointTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSculptToolBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Base Tool for mesh sculpting tools, provides some shared functionality\n */" },
		{ "IncludePath", "Sculpting/MeshSculptToolBase.h" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshSculptToolBase.h" },
		{ "ToolTip", "Base Tool for mesh sculpting tools, provides some shared functionality" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_BrushProperties_MetaData[] = {
		{ "Comment", "/** Properties that control brush size/etc */" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshSculptToolBase.h" },
		{ "ToolTip", "Properties that control brush size/etc" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_BrushProperties = { "BrushProperties", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshSculptToolBase, BrushProperties), Z_Construct_UClass_USculptBrushProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_BrushProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_BrushProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_GizmoProperties_MetaData[] = {
		{ "Comment", "/** Properties for 3D workplane / gizmo */" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshSculptToolBase.h" },
		{ "ToolTip", "Properties for 3D workplane / gizmo" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_GizmoProperties = { "GizmoProperties", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshSculptToolBase, GizmoProperties), Z_Construct_UClass_UWorkPlaneProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_GizmoProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_GizmoProperties_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_BrushOpPropSets_ValueProp = { "BrushOpPropSets", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_UMeshSculptBrushOpProps_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_BrushOpPropSets_Key_KeyProp = { "BrushOpPropSets_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_BrushOpPropSets_MetaData[] = {
		{ "ModuleRelativePath", "Public/Sculpting/MeshSculptToolBase.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_BrushOpPropSets = { "BrushOpPropSets", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshSculptToolBase, BrushOpPropSets), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_BrushOpPropSets_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_BrushOpPropSets_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_SecondaryBrushOpPropSets_ValueProp = { "SecondaryBrushOpPropSets", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_UMeshSculptBrushOpProps_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_SecondaryBrushOpPropSets_Key_KeyProp = { "SecondaryBrushOpPropSets_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_SecondaryBrushOpPropSets_MetaData[] = {
		{ "ModuleRelativePath", "Public/Sculpting/MeshSculptToolBase.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_SecondaryBrushOpPropSets = { "SecondaryBrushOpPropSets", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshSculptToolBase, SecondaryBrushOpPropSets), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_SecondaryBrushOpPropSets_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_SecondaryBrushOpPropSets_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_ViewProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/Sculpting/MeshSculptToolBase.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_ViewProperties = { "ViewProperties", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshSculptToolBase, ViewProperties), Z_Construct_UClass_UMeshEditingViewProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_ViewProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_ViewProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_ActiveOverrideMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Public/Sculpting/MeshSculptToolBase.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_ActiveOverrideMaterial = { "ActiveOverrideMaterial", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshSculptToolBase, ActiveOverrideMaterial), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_ActiveOverrideMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_ActiveOverrideMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_BrushIndicator_MetaData[] = {
		{ "ModuleRelativePath", "Public/Sculpting/MeshSculptToolBase.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_BrushIndicator = { "BrushIndicator", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshSculptToolBase, BrushIndicator), Z_Construct_UClass_UBrushStampIndicator_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_BrushIndicator_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_BrushIndicator_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_BrushIndicatorMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Public/Sculpting/MeshSculptToolBase.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_BrushIndicatorMaterial = { "BrushIndicatorMaterial", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshSculptToolBase, BrushIndicatorMaterial), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_BrushIndicatorMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_BrushIndicatorMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_BrushIndicatorMesh_MetaData[] = {
		{ "ModuleRelativePath", "Public/Sculpting/MeshSculptToolBase.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_BrushIndicatorMesh = { "BrushIndicatorMesh", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshSculptToolBase, BrushIndicatorMesh), Z_Construct_UClass_UPreviewMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_BrushIndicatorMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_BrushIndicatorMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_PlaneTransformGizmo_MetaData[] = {
		{ "Comment", "// plane gizmo\n" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshSculptToolBase.h" },
		{ "ToolTip", "plane gizmo" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_PlaneTransformGizmo = { "PlaneTransformGizmo", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshSculptToolBase, PlaneTransformGizmo), Z_Construct_UClass_UTransformGizmo_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_PlaneTransformGizmo_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_PlaneTransformGizmo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_PlaneTransformProxy_MetaData[] = {
		{ "ModuleRelativePath", "Public/Sculpting/MeshSculptToolBase.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_PlaneTransformProxy = { "PlaneTransformProxy", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshSculptToolBase, PlaneTransformProxy), Z_Construct_UClass_UTransformProxy_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_PlaneTransformProxy_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_PlaneTransformProxy_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshSculptToolBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_BrushProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_GizmoProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_BrushOpPropSets_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_BrushOpPropSets_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_BrushOpPropSets,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_SecondaryBrushOpPropSets_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_SecondaryBrushOpPropSets_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_SecondaryBrushOpPropSets,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_ViewProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_ActiveOverrideMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_BrushIndicator,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_BrushIndicatorMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_BrushIndicatorMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_PlaneTransformGizmo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSculptToolBase_Statics::NewProp_PlaneTransformProxy,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshSculptToolBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshSculptToolBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshSculptToolBase_Statics::ClassParams = {
		&UMeshSculptToolBase::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshSculptToolBase_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSculptToolBase_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshSculptToolBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSculptToolBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshSculptToolBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshSculptToolBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshSculptToolBase, 3137791790);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UMeshSculptToolBase>()
	{
		return UMeshSculptToolBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshSculptToolBase(Z_Construct_UClass_UMeshSculptToolBase, &UMeshSculptToolBase::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UMeshSculptToolBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshSculptToolBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
