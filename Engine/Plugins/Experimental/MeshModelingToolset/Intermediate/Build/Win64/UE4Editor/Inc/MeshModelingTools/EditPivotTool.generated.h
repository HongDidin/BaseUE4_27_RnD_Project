// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLS_EditPivotTool_generated_h
#error "EditPivotTool.generated.h already included, missing '#pragma once' in EditPivotTool.h"
#endif
#define MESHMODELINGTOOLS_EditPivotTool_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_75_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FEditPivotTarget_Statics; \
	MESHMODELINGTOOLS_API static class UScriptStruct* StaticStruct();


template<> MESHMODELINGTOOLS_API UScriptStruct* StaticStruct<struct FEditPivotTarget>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_25_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_25_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_25_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_25_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUEditPivotToolBuilder(); \
	friend struct Z_Construct_UClass_UEditPivotToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UEditPivotToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UEditPivotToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_25_INCLASS \
private: \
	static void StaticRegisterNativesUEditPivotToolBuilder(); \
	friend struct Z_Construct_UClass_UEditPivotToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UEditPivotToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UEditPivotToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_25_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEditPivotToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEditPivotToolBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEditPivotToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEditPivotToolBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEditPivotToolBuilder(UEditPivotToolBuilder&&); \
	NO_API UEditPivotToolBuilder(const UEditPivotToolBuilder&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_25_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEditPivotToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEditPivotToolBuilder(UEditPivotToolBuilder&&); \
	NO_API UEditPivotToolBuilder(const UEditPivotToolBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEditPivotToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEditPivotToolBuilder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEditPivotToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_25_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_22_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_25_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_25_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_25_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_25_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_25_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_25_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_25_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_25_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_25_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_25_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UEditPivotToolBuilder>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_59_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_59_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_59_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_59_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUEditPivotToolProperties(); \
	friend struct Z_Construct_UClass_UEditPivotToolProperties_Statics; \
public: \
	DECLARE_CLASS(UEditPivotToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UEditPivotToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_59_INCLASS \
private: \
	static void StaticRegisterNativesUEditPivotToolProperties(); \
	friend struct Z_Construct_UClass_UEditPivotToolProperties_Statics; \
public: \
	DECLARE_CLASS(UEditPivotToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UEditPivotToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_59_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEditPivotToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEditPivotToolProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEditPivotToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEditPivotToolProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEditPivotToolProperties(UEditPivotToolProperties&&); \
	NO_API UEditPivotToolProperties(const UEditPivotToolProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_59_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEditPivotToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEditPivotToolProperties(UEditPivotToolProperties&&); \
	NO_API UEditPivotToolProperties(const UEditPivotToolProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEditPivotToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEditPivotToolProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEditPivotToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_59_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_56_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_59_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_59_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_59_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_59_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_59_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_59_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_59_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_59_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_59_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_59_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_59_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_59_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UEditPivotToolProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_106_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_106_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execBack); \
	DECLARE_FUNCTION(execFront); \
	DECLARE_FUNCTION(execRight); \
	DECLARE_FUNCTION(execLeft); \
	DECLARE_FUNCTION(execTop); \
	DECLARE_FUNCTION(execBottom); \
	DECLARE_FUNCTION(execCenter);


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_106_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execBack); \
	DECLARE_FUNCTION(execFront); \
	DECLARE_FUNCTION(execRight); \
	DECLARE_FUNCTION(execLeft); \
	DECLARE_FUNCTION(execTop); \
	DECLARE_FUNCTION(execBottom); \
	DECLARE_FUNCTION(execCenter);


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_106_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUEditPivotToolActionPropertySet(); \
	friend struct Z_Construct_UClass_UEditPivotToolActionPropertySet_Statics; \
public: \
	DECLARE_CLASS(UEditPivotToolActionPropertySet, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UEditPivotToolActionPropertySet)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_106_INCLASS \
private: \
	static void StaticRegisterNativesUEditPivotToolActionPropertySet(); \
	friend struct Z_Construct_UClass_UEditPivotToolActionPropertySet_Statics; \
public: \
	DECLARE_CLASS(UEditPivotToolActionPropertySet, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UEditPivotToolActionPropertySet)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_106_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEditPivotToolActionPropertySet(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEditPivotToolActionPropertySet) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEditPivotToolActionPropertySet); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEditPivotToolActionPropertySet); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEditPivotToolActionPropertySet(UEditPivotToolActionPropertySet&&); \
	NO_API UEditPivotToolActionPropertySet(const UEditPivotToolActionPropertySet&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_106_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEditPivotToolActionPropertySet(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEditPivotToolActionPropertySet(UEditPivotToolActionPropertySet&&); \
	NO_API UEditPivotToolActionPropertySet(const UEditPivotToolActionPropertySet&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEditPivotToolActionPropertySet); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEditPivotToolActionPropertySet); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEditPivotToolActionPropertySet)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_106_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_103_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_106_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_106_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_106_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_106_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_106_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_106_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_106_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_106_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_106_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_106_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_106_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_106_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UEditPivotToolActionPropertySet>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_149_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_149_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_149_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_149_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUEditPivotTool(); \
	friend struct Z_Construct_UClass_UEditPivotTool_Statics; \
public: \
	DECLARE_CLASS(UEditPivotTool, UMultiSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UEditPivotTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_149_INCLASS \
private: \
	static void StaticRegisterNativesUEditPivotTool(); \
	friend struct Z_Construct_UClass_UEditPivotTool_Statics; \
public: \
	DECLARE_CLASS(UEditPivotTool, UMultiSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UEditPivotTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_149_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEditPivotTool(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEditPivotTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEditPivotTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEditPivotTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEditPivotTool(UEditPivotTool&&); \
	NO_API UEditPivotTool(const UEditPivotTool&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_149_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEditPivotTool(UEditPivotTool&&); \
	NO_API UEditPivotTool(const UEditPivotTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEditPivotTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEditPivotTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UEditPivotTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_149_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ActiveGizmos() { return STRUCT_OFFSET(UEditPivotTool, ActiveGizmos); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_146_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_149_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_149_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_149_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_149_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_149_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_149_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_149_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_149_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_149_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_149_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_149_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h_149_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UEditPivotTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditPivotTool_h


#define FOREACH_ENUM_EEDITPIVOTTOOLACTIONS(op) \
	op(EEditPivotToolActions::NoAction) \
	op(EEditPivotToolActions::Center) \
	op(EEditPivotToolActions::Bottom) \
	op(EEditPivotToolActions::Top) \
	op(EEditPivotToolActions::Left) \
	op(EEditPivotToolActions::Right) \
	op(EEditPivotToolActions::Front) \
	op(EEditPivotToolActions::Back) 

enum class EEditPivotToolActions;
template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EEditPivotToolActions>();

#define FOREACH_ENUM_EEDITPIVOTSNAPDRAGROTATIONMODE(op) \
	op(EEditPivotSnapDragRotationMode::Ignore) \
	op(EEditPivotSnapDragRotationMode::Align) \
	op(EEditPivotSnapDragRotationMode::AlignFlipped) \
	op(EEditPivotSnapDragRotationMode::LastValue) 

enum class EEditPivotSnapDragRotationMode : uint8;
template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EEditPivotSnapDragRotationMode>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
