// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingToolsEditorOnly/Public/EditMeshMaterialsTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEditMeshMaterialsTool() {}
// Cross Module References
	MESHMODELINGTOOLSEDITORONLY_API UEnum* Z_Construct_UEnum_MeshModelingToolsEditorOnly_EEditMeshMaterialsToolActions();
	UPackage* Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UEditMeshMaterialsToolBuilder_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UEditMeshMaterialsToolBuilder();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshSelectionToolBuilder();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UEditMeshMaterialsToolProperties_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UEditMeshMaterialsToolProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UEditMeshMaterialsEditActions_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UEditMeshMaterialsEditActions();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshSelectionToolActionPropertySet();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UEditMeshMaterialsTool_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UEditMeshMaterialsTool();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshSelectionTool();
// End Cross Module References
	static UEnum* EEditMeshMaterialsToolActions_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingToolsEditorOnly_EEditMeshMaterialsToolActions, Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly(), TEXT("EEditMeshMaterialsToolActions"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLSEDITORONLY_API UEnum* StaticEnum<EEditMeshMaterialsToolActions>()
	{
		return EEditMeshMaterialsToolActions_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EEditMeshMaterialsToolActions(EEditMeshMaterialsToolActions_StaticEnum, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("EEditMeshMaterialsToolActions"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingToolsEditorOnly_EEditMeshMaterialsToolActions_Hash() { return 3918468399U; }
	UEnum* Z_Construct_UEnum_MeshModelingToolsEditorOnly_EEditMeshMaterialsToolActions()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EEditMeshMaterialsToolActions"), 0, Get_Z_Construct_UEnum_MeshModelingToolsEditorOnly_EEditMeshMaterialsToolActions_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EEditMeshMaterialsToolActions::NoAction", (int64)EEditMeshMaterialsToolActions::NoAction },
				{ "EEditMeshMaterialsToolActions::AssignMaterial", (int64)EEditMeshMaterialsToolActions::AssignMaterial },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "AssignMaterial.Name", "EEditMeshMaterialsToolActions::AssignMaterial" },
				{ "ModuleRelativePath", "Public/EditMeshMaterialsTool.h" },
				{ "NoAction.Name", "EEditMeshMaterialsToolActions::NoAction" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
				nullptr,
				"EEditMeshMaterialsToolActions",
				"EEditMeshMaterialsToolActions",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UEditMeshMaterialsToolBuilder::StaticRegisterNativesUEditMeshMaterialsToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UEditMeshMaterialsToolBuilder_NoRegister()
	{
		return UEditMeshMaterialsToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UEditMeshMaterialsToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEditMeshMaterialsToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshSelectionToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditMeshMaterialsToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "EditMeshMaterialsTool.h" },
		{ "ModuleRelativePath", "Public/EditMeshMaterialsTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEditMeshMaterialsToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEditMeshMaterialsToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEditMeshMaterialsToolBuilder_Statics::ClassParams = {
		&UEditMeshMaterialsToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UEditMeshMaterialsToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEditMeshMaterialsToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEditMeshMaterialsToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEditMeshMaterialsToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEditMeshMaterialsToolBuilder, 1065928509);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UEditMeshMaterialsToolBuilder>()
	{
		return UEditMeshMaterialsToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEditMeshMaterialsToolBuilder(Z_Construct_UClass_UEditMeshMaterialsToolBuilder, &UEditMeshMaterialsToolBuilder::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UEditMeshMaterialsToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEditMeshMaterialsToolBuilder);
	void UEditMeshMaterialsToolProperties::StaticRegisterNativesUEditMeshMaterialsToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UEditMeshMaterialsToolProperties_NoRegister()
	{
		return UEditMeshMaterialsToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UEditMeshMaterialsToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SelectedMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_SelectedMaterial;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Materials_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Materials_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Materials;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEditMeshMaterialsToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditMeshMaterialsToolProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "EditMeshMaterialsTool.h" },
		{ "ModuleRelativePath", "Public/EditMeshMaterialsTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditMeshMaterialsToolProperties_Statics::NewProp_SelectedMaterial_MetaData[] = {
		{ "ArrayClamp", "Materials" },
		{ "Category", "Materials" },
		{ "ModuleRelativePath", "Public/EditMeshMaterialsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UEditMeshMaterialsToolProperties_Statics::NewProp_SelectedMaterial = { "SelectedMaterial", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEditMeshMaterialsToolProperties, SelectedMaterial), METADATA_PARAMS(Z_Construct_UClass_UEditMeshMaterialsToolProperties_Statics::NewProp_SelectedMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditMeshMaterialsToolProperties_Statics::NewProp_SelectedMaterial_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UEditMeshMaterialsToolProperties_Statics::NewProp_Materials_Inner = { "Materials", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditMeshMaterialsToolProperties_Statics::NewProp_Materials_MetaData[] = {
		{ "Category", "Materials" },
		{ "ModuleRelativePath", "Public/EditMeshMaterialsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UEditMeshMaterialsToolProperties_Statics::NewProp_Materials = { "Materials", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEditMeshMaterialsToolProperties, Materials), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UEditMeshMaterialsToolProperties_Statics::NewProp_Materials_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditMeshMaterialsToolProperties_Statics::NewProp_Materials_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UEditMeshMaterialsToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditMeshMaterialsToolProperties_Statics::NewProp_SelectedMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditMeshMaterialsToolProperties_Statics::NewProp_Materials_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditMeshMaterialsToolProperties_Statics::NewProp_Materials,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEditMeshMaterialsToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEditMeshMaterialsToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEditMeshMaterialsToolProperties_Statics::ClassParams = {
		&UEditMeshMaterialsToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UEditMeshMaterialsToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UEditMeshMaterialsToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UEditMeshMaterialsToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEditMeshMaterialsToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEditMeshMaterialsToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEditMeshMaterialsToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEditMeshMaterialsToolProperties, 2823896062);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UEditMeshMaterialsToolProperties>()
	{
		return UEditMeshMaterialsToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEditMeshMaterialsToolProperties(Z_Construct_UClass_UEditMeshMaterialsToolProperties, &UEditMeshMaterialsToolProperties::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UEditMeshMaterialsToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEditMeshMaterialsToolProperties);
	DEFINE_FUNCTION(UEditMeshMaterialsEditActions::execAssignSelectedMaterial)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AssignSelectedMaterial();
		P_NATIVE_END;
	}
	void UEditMeshMaterialsEditActions::StaticRegisterNativesUEditMeshMaterialsEditActions()
	{
		UClass* Class = UEditMeshMaterialsEditActions::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AssignSelectedMaterial", &UEditMeshMaterialsEditActions::execAssignSelectedMaterial },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UEditMeshMaterialsEditActions_AssignSelectedMaterial_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditMeshMaterialsEditActions_AssignSelectedMaterial_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "MaterialEdits" },
		{ "DisplayName", "Assign Selected Material" },
		{ "DisplayPriority", "1" },
		{ "ModuleRelativePath", "Public/EditMeshMaterialsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditMeshMaterialsEditActions_AssignSelectedMaterial_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditMeshMaterialsEditActions, nullptr, "AssignSelectedMaterial", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditMeshMaterialsEditActions_AssignSelectedMaterial_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditMeshMaterialsEditActions_AssignSelectedMaterial_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditMeshMaterialsEditActions_AssignSelectedMaterial()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditMeshMaterialsEditActions_AssignSelectedMaterial_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UEditMeshMaterialsEditActions_NoRegister()
	{
		return UEditMeshMaterialsEditActions::StaticClass();
	}
	struct Z_Construct_UClass_UEditMeshMaterialsEditActions_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEditMeshMaterialsEditActions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshSelectionToolActionPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UEditMeshMaterialsEditActions_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UEditMeshMaterialsEditActions_AssignSelectedMaterial, "AssignSelectedMaterial" }, // 2260033452
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditMeshMaterialsEditActions_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "EditMeshMaterialsTool.h" },
		{ "ModuleRelativePath", "Public/EditMeshMaterialsTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEditMeshMaterialsEditActions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEditMeshMaterialsEditActions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEditMeshMaterialsEditActions_Statics::ClassParams = {
		&UEditMeshMaterialsEditActions::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UEditMeshMaterialsEditActions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEditMeshMaterialsEditActions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEditMeshMaterialsEditActions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEditMeshMaterialsEditActions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEditMeshMaterialsEditActions, 318944929);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UEditMeshMaterialsEditActions>()
	{
		return UEditMeshMaterialsEditActions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEditMeshMaterialsEditActions(Z_Construct_UClass_UEditMeshMaterialsEditActions, &UEditMeshMaterialsEditActions::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UEditMeshMaterialsEditActions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEditMeshMaterialsEditActions);
	void UEditMeshMaterialsTool::StaticRegisterNativesUEditMeshMaterialsTool()
	{
	}
	UClass* Z_Construct_UClass_UEditMeshMaterialsTool_NoRegister()
	{
		return UEditMeshMaterialsTool::StaticClass();
	}
	struct Z_Construct_UClass_UEditMeshMaterialsTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaterialProps_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MaterialProps;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEditMeshMaterialsTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshSelectionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditMeshMaterialsTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "EditMeshMaterialsTool.h" },
		{ "ModuleRelativePath", "Public/EditMeshMaterialsTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditMeshMaterialsTool_Statics::NewProp_MaterialProps_MetaData[] = {
		{ "ModuleRelativePath", "Public/EditMeshMaterialsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UEditMeshMaterialsTool_Statics::NewProp_MaterialProps = { "MaterialProps", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEditMeshMaterialsTool, MaterialProps), Z_Construct_UClass_UEditMeshMaterialsToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UEditMeshMaterialsTool_Statics::NewProp_MaterialProps_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditMeshMaterialsTool_Statics::NewProp_MaterialProps_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UEditMeshMaterialsTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditMeshMaterialsTool_Statics::NewProp_MaterialProps,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEditMeshMaterialsTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEditMeshMaterialsTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEditMeshMaterialsTool_Statics::ClassParams = {
		&UEditMeshMaterialsTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UEditMeshMaterialsTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UEditMeshMaterialsTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UEditMeshMaterialsTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEditMeshMaterialsTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEditMeshMaterialsTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEditMeshMaterialsTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEditMeshMaterialsTool, 2908471902);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UEditMeshMaterialsTool>()
	{
		return UEditMeshMaterialsTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEditMeshMaterialsTool(Z_Construct_UClass_UEditMeshMaterialsTool, &UEditMeshMaterialsTool::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UEditMeshMaterialsTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEditMeshMaterialsTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
