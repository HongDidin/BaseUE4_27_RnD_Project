// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/DynamicMeshSculptTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDynamicMeshSculptTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EDynamicMeshSculptBrushType();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDynamicMeshSculptToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDynamicMeshSculptToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UMeshSurfacePointToolBuilder();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UBrushSculptProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UBrushSculptProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDynamicSculptToolActions_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDynamicSculptToolActions();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UBrushRemeshProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UBrushRemeshProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_URemeshProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UFixedPlaneBrushProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UFixedPlaneBrushProperties();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FQuat();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDynamicMeshSculptTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDynamicMeshSculptTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UMeshSurfacePointTool();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_USculptBrushProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_USculptMaxBrushProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UKelvinBrushProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshEditingViewProperties_NoRegister();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UBrushStampIndicator_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPreviewMesh_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UOctreeDynamicMeshComponent_NoRegister();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UTransformGizmo_NoRegister();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UTransformProxy_NoRegister();
// End Cross Module References
	static UEnum* EDynamicMeshSculptBrushType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EDynamicMeshSculptBrushType, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EDynamicMeshSculptBrushType"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EDynamicMeshSculptBrushType>()
	{
		return EDynamicMeshSculptBrushType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDynamicMeshSculptBrushType(EDynamicMeshSculptBrushType_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EDynamicMeshSculptBrushType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EDynamicMeshSculptBrushType_Hash() { return 1173187111U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EDynamicMeshSculptBrushType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDynamicMeshSculptBrushType"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EDynamicMeshSculptBrushType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDynamicMeshSculptBrushType::Move", (int64)EDynamicMeshSculptBrushType::Move },
				{ "EDynamicMeshSculptBrushType::PullKelvin", (int64)EDynamicMeshSculptBrushType::PullKelvin },
				{ "EDynamicMeshSculptBrushType::PullSharpKelvin", (int64)EDynamicMeshSculptBrushType::PullSharpKelvin },
				{ "EDynamicMeshSculptBrushType::Smooth", (int64)EDynamicMeshSculptBrushType::Smooth },
				{ "EDynamicMeshSculptBrushType::Offset", (int64)EDynamicMeshSculptBrushType::Offset },
				{ "EDynamicMeshSculptBrushType::SculptView", (int64)EDynamicMeshSculptBrushType::SculptView },
				{ "EDynamicMeshSculptBrushType::SculptMax", (int64)EDynamicMeshSculptBrushType::SculptMax },
				{ "EDynamicMeshSculptBrushType::Inflate", (int64)EDynamicMeshSculptBrushType::Inflate },
				{ "EDynamicMeshSculptBrushType::ScaleKelvin", (int64)EDynamicMeshSculptBrushType::ScaleKelvin },
				{ "EDynamicMeshSculptBrushType::Pinch", (int64)EDynamicMeshSculptBrushType::Pinch },
				{ "EDynamicMeshSculptBrushType::TwistKelvin", (int64)EDynamicMeshSculptBrushType::TwistKelvin },
				{ "EDynamicMeshSculptBrushType::Flatten", (int64)EDynamicMeshSculptBrushType::Flatten },
				{ "EDynamicMeshSculptBrushType::Plane", (int64)EDynamicMeshSculptBrushType::Plane },
				{ "EDynamicMeshSculptBrushType::PlaneViewAligned", (int64)EDynamicMeshSculptBrushType::PlaneViewAligned },
				{ "EDynamicMeshSculptBrushType::FixedPlane", (int64)EDynamicMeshSculptBrushType::FixedPlane },
				{ "EDynamicMeshSculptBrushType::Resample", (int64)EDynamicMeshSculptBrushType::Resample },
				{ "EDynamicMeshSculptBrushType::LastValue", (int64)EDynamicMeshSculptBrushType::LastValue },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** Mesh Sculpting Brush Types */" },
				{ "FixedPlane.Comment", "/** Move vertices towards a fixed plane in world space, positioned with a 3D gizmo */" },
				{ "FixedPlane.DisplayName", "FixedPlane" },
				{ "FixedPlane.Name", "EDynamicMeshSculptBrushType::FixedPlane" },
				{ "FixedPlane.ToolTip", "Move vertices towards a fixed plane in world space, positioned with a 3D gizmo" },
				{ "Flatten.Comment", "/** Move vertices towards the average plane of the brush stamp region */" },
				{ "Flatten.DisplayName", "Flatten" },
				{ "Flatten.Name", "EDynamicMeshSculptBrushType::Flatten" },
				{ "Flatten.ToolTip", "Move vertices towards the average plane of the brush stamp region" },
				{ "Inflate.Comment", "/** Displace vertices along their vertex normals */" },
				{ "Inflate.DisplayName", "Inflate" },
				{ "Inflate.Name", "EDynamicMeshSculptBrushType::Inflate" },
				{ "Inflate.ToolTip", "Displace vertices along their vertex normals" },
				{ "LastValue.Hidden", "" },
				{ "LastValue.Name", "EDynamicMeshSculptBrushType::LastValue" },
				{ "ModuleRelativePath", "Public/DynamicMeshSculptTool.h" },
				{ "Move.Comment", "/** Move vertices parallel to the view plane  */" },
				{ "Move.DisplayName", "Move" },
				{ "Move.Name", "EDynamicMeshSculptBrushType::Move" },
				{ "Move.ToolTip", "Move vertices parallel to the view plane" },
				{ "Offset.Comment", "/** Displace vertices along the average surface normal (Ctrl to invert) */" },
				{ "Offset.DisplayName", "Sculpt (Normal)" },
				{ "Offset.Name", "EDynamicMeshSculptBrushType::Offset" },
				{ "Offset.ToolTip", "Displace vertices along the average surface normal (Ctrl to invert)" },
				{ "Pinch.Comment", "/** Move vertices towards the center of the brush (Ctrl to push away)*/" },
				{ "Pinch.DisplayName", "Pinch" },
				{ "Pinch.Name", "EDynamicMeshSculptBrushType::Pinch" },
				{ "Pinch.ToolTip", "Move vertices towards the center of the brush (Ctrl to push away)" },
				{ "Plane.Comment", "/** Move vertices towards a plane defined by the initial brush position  */" },
				{ "Plane.DisplayName", "Plane (Normal)" },
				{ "Plane.Name", "EDynamicMeshSculptBrushType::Plane" },
				{ "Plane.ToolTip", "Move vertices towards a plane defined by the initial brush position" },
				{ "PlaneViewAligned.Comment", "/** Move vertices towards a view-facing plane defined at the initial brush position */" },
				{ "PlaneViewAligned.DisplayName", "Plane (Viewpoint)" },
				{ "PlaneViewAligned.Name", "EDynamicMeshSculptBrushType::PlaneViewAligned" },
				{ "PlaneViewAligned.ToolTip", "Move vertices towards a view-facing plane defined at the initial brush position" },
				{ "PullKelvin.Comment", "/** Grab Brush, fall-off alters the influence of the grab */" },
				{ "PullKelvin.DisplayName", "Kelvin Grab" },
				{ "PullKelvin.Name", "EDynamicMeshSculptBrushType::PullKelvin" },
				{ "PullKelvin.ToolTip", "Grab Brush, fall-off alters the influence of the grab" },
				{ "PullSharpKelvin.Comment", "/** Grab Brush that may generate cusps, fall-off alters the influence of the grab */" },
				{ "PullSharpKelvin.DisplayName", "Sharp Kelvin Grab" },
				{ "PullSharpKelvin.Name", "EDynamicMeshSculptBrushType::PullSharpKelvin" },
				{ "PullSharpKelvin.ToolTip", "Grab Brush that may generate cusps, fall-off alters the influence of the grab" },
				{ "Resample.Comment", "/** Remesh the brushed region but do not otherwise deform it */" },
				{ "Resample.DisplayName", "Resample" },
				{ "Resample.Name", "EDynamicMeshSculptBrushType::Resample" },
				{ "Resample.ToolTip", "Remesh the brushed region but do not otherwise deform it" },
				{ "ScaleKelvin.Comment", "/** Scale Brush will inflate or pinch radially from the center of the brush */" },
				{ "ScaleKelvin.DisplayName", "Kelvin Scale" },
				{ "ScaleKelvin.Name", "EDynamicMeshSculptBrushType::ScaleKelvin" },
				{ "ScaleKelvin.ToolTip", "Scale Brush will inflate or pinch radially from the center of the brush" },
				{ "SculptMax.Comment", "/** Displaces vertices along the average surface normal to a maximum height based on the brush size (Ctrl to invert) */" },
				{ "SculptMax.DisplayName", "Sculpt Max" },
				{ "SculptMax.Name", "EDynamicMeshSculptBrushType::SculptMax" },
				{ "SculptMax.ToolTip", "Displaces vertices along the average surface normal to a maximum height based on the brush size (Ctrl to invert)" },
				{ "SculptView.Comment", "/** Displace vertices towards the camera viewpoint (Ctrl to invert) */" },
				{ "SculptView.DisplayName", "Sculpt (Viewpoint)" },
				{ "SculptView.Name", "EDynamicMeshSculptBrushType::SculptView" },
				{ "SculptView.ToolTip", "Displace vertices towards the camera viewpoint (Ctrl to invert)" },
				{ "Smooth.Comment", "/** Smooth mesh vertices  */" },
				{ "Smooth.DisplayName", "Smooth" },
				{ "Smooth.Name", "EDynamicMeshSculptBrushType::Smooth" },
				{ "Smooth.ToolTip", "Smooth mesh vertices" },
				{ "ToolTip", "Mesh Sculpting Brush Types" },
				{ "TwistKelvin.Comment", "/** Twist Brush moves vertices in the plane perpendicular to the local mesh normal */" },
				{ "TwistKelvin.DisplayName", "Kelvin Twist" },
				{ "TwistKelvin.Name", "EDynamicMeshSculptBrushType::TwistKelvin" },
				{ "TwistKelvin.ToolTip", "Twist Brush moves vertices in the plane perpendicular to the local mesh normal" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EDynamicMeshSculptBrushType",
				"EDynamicMeshSculptBrushType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UDynamicMeshSculptToolBuilder::StaticRegisterNativesUDynamicMeshSculptToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UDynamicMeshSculptToolBuilder_NoRegister()
	{
		return UDynamicMeshSculptToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UDynamicMeshSculptToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDynamicMeshSculptToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshSurfacePointToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDynamicMeshSculptToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Tool Builder\n */" },
		{ "IncludePath", "DynamicMeshSculptTool.h" },
		{ "ModuleRelativePath", "Public/DynamicMeshSculptTool.h" },
		{ "ToolTip", "Tool Builder" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDynamicMeshSculptToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDynamicMeshSculptToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDynamicMeshSculptToolBuilder_Statics::ClassParams = {
		&UDynamicMeshSculptToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UDynamicMeshSculptToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDynamicMeshSculptToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDynamicMeshSculptToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDynamicMeshSculptToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDynamicMeshSculptToolBuilder, 2995661580);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UDynamicMeshSculptToolBuilder>()
	{
		return UDynamicMeshSculptToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDynamicMeshSculptToolBuilder(Z_Construct_UClass_UDynamicMeshSculptToolBuilder, &UDynamicMeshSculptToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UDynamicMeshSculptToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDynamicMeshSculptToolBuilder);
	void UBrushSculptProperties::StaticRegisterNativesUBrushSculptProperties()
	{
	}
	UClass* Z_Construct_UClass_UBrushSculptProperties_NoRegister()
	{
		return UBrushSculptProperties::StaticClass();
	}
	struct Z_Construct_UClass_UBrushSculptProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsRemeshingEnabled_MetaData[];
#endif
		static void NewProp_bIsRemeshingEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsRemeshingEnabled;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_PrimaryBrushType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PrimaryBrushType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_PrimaryBrushType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PrimaryBrushSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PrimaryBrushSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPreserveUVFlow_MetaData[];
#endif
		static void NewProp_bPreserveUVFlow_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPreserveUVFlow;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bFreezeTarget_MetaData[];
#endif
		static void NewProp_bFreezeTarget_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFreezeTarget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SmoothBrushSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SmoothBrushSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDetailPreservingSmooth_MetaData[];
#endif
		static void NewProp_bDetailPreservingSmooth_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDetailPreservingSmooth;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBrushSculptProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBrushSculptProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "DynamicMeshSculptTool.h" },
		{ "ModuleRelativePath", "Public/DynamicMeshSculptTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_bIsRemeshingEnabled_MetaData[] = {
		{ "Comment", "/* This is a dupe of the bool in the tool class.  I needed it here so it could be checked as an EditCondition */" },
		{ "ModuleRelativePath", "Public/DynamicMeshSculptTool.h" },
		{ "ToolTip", "This is a dupe of the bool in the tool class.  I needed it here so it could be checked as an EditCondition" },
		{ "TransientToolProperty", "" },
	};
#endif
	void Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_bIsRemeshingEnabled_SetBit(void* Obj)
	{
		((UBrushSculptProperties*)Obj)->bIsRemeshingEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_bIsRemeshingEnabled = { "bIsRemeshingEnabled", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UBrushSculptProperties), &Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_bIsRemeshingEnabled_SetBit, METADATA_PARAMS(Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_bIsRemeshingEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_bIsRemeshingEnabled_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_PrimaryBrushType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_PrimaryBrushType_MetaData[] = {
		{ "Category", "Sculpting" },
		{ "Comment", "/** Primary Brush Mode */" },
		{ "DisplayName", "Brush Type" },
		{ "ModuleRelativePath", "Public/DynamicMeshSculptTool.h" },
		{ "ToolTip", "Primary Brush Mode" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_PrimaryBrushType = { "PrimaryBrushType", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBrushSculptProperties, PrimaryBrushType), Z_Construct_UEnum_MeshModelingTools_EDynamicMeshSculptBrushType, METADATA_PARAMS(Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_PrimaryBrushType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_PrimaryBrushType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_PrimaryBrushSpeed_MetaData[] = {
		{ "Category", "Sculpting" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Strength of the Primary Brush */" },
		{ "DisplayName", "Strength" },
		{ "EditCondition", "PrimaryBrushType != EDynamicMeshSculptBrushType::Pull" },
		{ "ModuleRelativePath", "Public/DynamicMeshSculptTool.h" },
		{ "ToolTip", "Strength of the Primary Brush" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_PrimaryBrushSpeed = { "PrimaryBrushSpeed", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBrushSculptProperties, PrimaryBrushSpeed), METADATA_PARAMS(Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_PrimaryBrushSpeed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_PrimaryBrushSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_bPreserveUVFlow_MetaData[] = {
		{ "Category", "Sculpting" },
		{ "Comment", "/** If true, try to preserve the shape of the UV/3D mapping. This will limit Smoothing and Remeshing in some cases. */" },
		{ "ModuleRelativePath", "Public/DynamicMeshSculptTool.h" },
		{ "ToolTip", "If true, try to preserve the shape of the UV/3D mapping. This will limit Smoothing and Remeshing in some cases." },
	};
#endif
	void Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_bPreserveUVFlow_SetBit(void* Obj)
	{
		((UBrushSculptProperties*)Obj)->bPreserveUVFlow = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_bPreserveUVFlow = { "bPreserveUVFlow", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UBrushSculptProperties), &Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_bPreserveUVFlow_SetBit, METADATA_PARAMS(Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_bPreserveUVFlow_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_bPreserveUVFlow_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_bFreezeTarget_MetaData[] = {
		{ "Category", "Sculpting" },
		{ "Comment", "/** When Freeze Target is toggled on, the Brush Target Surface will be Frozen in its current state, until toggled off. Brush strokes will be applied relative to the Target Surface, for applicable Brushes */" },
		{ "EditCondition", "PrimaryBrushType == EDynamicMeshSculptBrushType::Sculpt || PrimaryBrushType == EDynamicMeshSculptBrushType::SculptMax || PrimaryBrushType == EDynamicMeshSculptBrushType::SculptView || PrimaryBrushType == EDynamicMeshSculptBrushType::Pinch || PrimaryBrushType == EDynamicMeshSculptBrushType::Resample" },
		{ "ModuleRelativePath", "Public/DynamicMeshSculptTool.h" },
		{ "ToolTip", "When Freeze Target is toggled on, the Brush Target Surface will be Frozen in its current state, until toggled off. Brush strokes will be applied relative to the Target Surface, for applicable Brushes" },
	};
#endif
	void Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_bFreezeTarget_SetBit(void* Obj)
	{
		((UBrushSculptProperties*)Obj)->bFreezeTarget = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_bFreezeTarget = { "bFreezeTarget", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UBrushSculptProperties), &Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_bFreezeTarget_SetBit, METADATA_PARAMS(Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_bFreezeTarget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_bFreezeTarget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_SmoothBrushSpeed_MetaData[] = {
		{ "Category", "Smoothing" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Strength of Shift-to-Smooth Brushing and Smoothing Brush */" },
		{ "DisplayName", "Smoothing Strength" },
		{ "ModuleRelativePath", "Public/DynamicMeshSculptTool.h" },
		{ "ToolTip", "Strength of Shift-to-Smooth Brushing and Smoothing Brush" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_SmoothBrushSpeed = { "SmoothBrushSpeed", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBrushSculptProperties, SmoothBrushSpeed), METADATA_PARAMS(Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_SmoothBrushSpeed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_SmoothBrushSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_bDetailPreservingSmooth_MetaData[] = {
		{ "Category", "Smoothing" },
		{ "Comment", "/** If enabled, Remeshing is limited during Smoothing to avoid wiping out higher-density triangle areas */" },
		{ "DisplayName", "Preserve Tri Density" },
		{ "EditCondition", "bIsRemeshingEnabled" },
		{ "EditConditionHides", "" },
		{ "HideEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/DynamicMeshSculptTool.h" },
		{ "ToolTip", "If enabled, Remeshing is limited during Smoothing to avoid wiping out higher-density triangle areas" },
	};
#endif
	void Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_bDetailPreservingSmooth_SetBit(void* Obj)
	{
		((UBrushSculptProperties*)Obj)->bDetailPreservingSmooth = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_bDetailPreservingSmooth = { "bDetailPreservingSmooth", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UBrushSculptProperties), &Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_bDetailPreservingSmooth_SetBit, METADATA_PARAMS(Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_bDetailPreservingSmooth_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_bDetailPreservingSmooth_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UBrushSculptProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_bIsRemeshingEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_PrimaryBrushType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_PrimaryBrushType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_PrimaryBrushSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_bPreserveUVFlow,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_bFreezeTarget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_SmoothBrushSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBrushSculptProperties_Statics::NewProp_bDetailPreservingSmooth,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBrushSculptProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBrushSculptProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBrushSculptProperties_Statics::ClassParams = {
		&UBrushSculptProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UBrushSculptProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UBrushSculptProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UBrushSculptProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBrushSculptProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBrushSculptProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBrushSculptProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBrushSculptProperties, 3790431805);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UBrushSculptProperties>()
	{
		return UBrushSculptProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBrushSculptProperties(Z_Construct_UClass_UBrushSculptProperties, &UBrushSculptProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UBrushSculptProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBrushSculptProperties);
	DEFINE_FUNCTION(UDynamicSculptToolActions::execDiscardAttributes)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->DiscardAttributes();
		P_NATIVE_END;
	}
	void UDynamicSculptToolActions::StaticRegisterNativesUDynamicSculptToolActions()
	{
		UClass* Class = UDynamicSculptToolActions::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "DiscardAttributes", &UDynamicSculptToolActions::execDiscardAttributes },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UDynamicSculptToolActions_DiscardAttributes_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDynamicSculptToolActions_DiscardAttributes_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "MeshEdits" },
		{ "ModuleRelativePath", "Public/DynamicMeshSculptTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDynamicSculptToolActions_DiscardAttributes_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDynamicSculptToolActions, nullptr, "DiscardAttributes", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDynamicSculptToolActions_DiscardAttributes_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDynamicSculptToolActions_DiscardAttributes_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDynamicSculptToolActions_DiscardAttributes()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDynamicSculptToolActions_DiscardAttributes_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UDynamicSculptToolActions_NoRegister()
	{
		return UDynamicSculptToolActions::StaticClass();
	}
	struct Z_Construct_UClass_UDynamicSculptToolActions_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDynamicSculptToolActions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UDynamicSculptToolActions_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UDynamicSculptToolActions_DiscardAttributes, "DiscardAttributes" }, // 4050533485
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDynamicSculptToolActions_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "DynamicMeshSculptTool.h" },
		{ "ModuleRelativePath", "Public/DynamicMeshSculptTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDynamicSculptToolActions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDynamicSculptToolActions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDynamicSculptToolActions_Statics::ClassParams = {
		&UDynamicSculptToolActions::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UDynamicSculptToolActions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDynamicSculptToolActions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDynamicSculptToolActions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDynamicSculptToolActions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDynamicSculptToolActions, 1978648187);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UDynamicSculptToolActions>()
	{
		return UDynamicSculptToolActions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDynamicSculptToolActions(Z_Construct_UClass_UDynamicSculptToolActions, &UDynamicSculptToolActions::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UDynamicSculptToolActions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDynamicSculptToolActions);
	void UBrushRemeshProperties::StaticRegisterNativesUBrushRemeshProperties()
	{
	}
	UClass* Z_Construct_UClass_UBrushRemeshProperties_NoRegister()
	{
		return UBrushRemeshProperties::StaticClass();
	}
	struct Z_Construct_UClass_UBrushRemeshProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableRemeshing_MetaData[];
#endif
		static void NewProp_bEnableRemeshing_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableRemeshing;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TriangleSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_TriangleSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreserveDetail_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_PreserveDetail;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Iterations_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Iterations;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBrushRemeshProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_URemeshProperties,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBrushRemeshProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "DynamicMeshSculptTool.h" },
		{ "ModuleRelativePath", "Public/DynamicMeshSculptTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBrushRemeshProperties_Statics::NewProp_bEnableRemeshing_MetaData[] = {
		{ "Category", "Remeshing" },
		{ "Comment", "/** Toggle remeshing on/off */" },
		{ "DisplayPriority", "1" },
		{ "ModuleRelativePath", "Public/DynamicMeshSculptTool.h" },
		{ "ToolTip", "Toggle remeshing on/off" },
	};
#endif
	void Z_Construct_UClass_UBrushRemeshProperties_Statics::NewProp_bEnableRemeshing_SetBit(void* Obj)
	{
		((UBrushRemeshProperties*)Obj)->bEnableRemeshing = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UBrushRemeshProperties_Statics::NewProp_bEnableRemeshing = { "bEnableRemeshing", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UBrushRemeshProperties), &Z_Construct_UClass_UBrushRemeshProperties_Statics::NewProp_bEnableRemeshing_SetBit, METADATA_PARAMS(Z_Construct_UClass_UBrushRemeshProperties_Statics::NewProp_bEnableRemeshing_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBrushRemeshProperties_Statics::NewProp_bEnableRemeshing_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBrushRemeshProperties_Statics::NewProp_TriangleSize_MetaData[] = {
		{ "Category", "Remeshing" },
		{ "ClampMax", "5" },
		{ "ClampMin", "-5" },
		{ "Comment", "/** Desired size of triangles after Remeshing, relative to average initial triangle size. Larger value results in larger triangles. */" },
		{ "DisplayName", "Relative Tri Size" },
		{ "DisplayPriority", "2" },
		{ "ModuleRelativePath", "Public/DynamicMeshSculptTool.h" },
		{ "ToolTip", "Desired size of triangles after Remeshing, relative to average initial triangle size. Larger value results in larger triangles." },
		{ "UIMax", "5" },
		{ "UIMin", "-5" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UBrushRemeshProperties_Statics::NewProp_TriangleSize = { "TriangleSize", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBrushRemeshProperties, TriangleSize), METADATA_PARAMS(Z_Construct_UClass_UBrushRemeshProperties_Statics::NewProp_TriangleSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBrushRemeshProperties_Statics::NewProp_TriangleSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBrushRemeshProperties_Statics::NewProp_PreserveDetail_MetaData[] = {
		{ "Category", "Remeshing" },
		{ "ClampMax", "5" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Control the amount of simplification during sculpting. Higher values will avoid wiping out fine details on the mesh. */" },
		{ "DisplayPriority", "3" },
		{ "ModuleRelativePath", "Public/DynamicMeshSculptTool.h" },
		{ "ToolTip", "Control the amount of simplification during sculpting. Higher values will avoid wiping out fine details on the mesh." },
		{ "UIMax", "5" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UBrushRemeshProperties_Statics::NewProp_PreserveDetail = { "PreserveDetail", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBrushRemeshProperties, PreserveDetail), METADATA_PARAMS(Z_Construct_UClass_UBrushRemeshProperties_Statics::NewProp_PreserveDetail_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBrushRemeshProperties_Statics::NewProp_PreserveDetail_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBrushRemeshProperties_Statics::NewProp_Iterations_MetaData[] = {
		{ "Category", "Remeshing" },
		{ "ModuleRelativePath", "Public/DynamicMeshSculptTool.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UBrushRemeshProperties_Statics::NewProp_Iterations = { "Iterations", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBrushRemeshProperties, Iterations), METADATA_PARAMS(Z_Construct_UClass_UBrushRemeshProperties_Statics::NewProp_Iterations_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBrushRemeshProperties_Statics::NewProp_Iterations_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UBrushRemeshProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBrushRemeshProperties_Statics::NewProp_bEnableRemeshing,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBrushRemeshProperties_Statics::NewProp_TriangleSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBrushRemeshProperties_Statics::NewProp_PreserveDetail,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBrushRemeshProperties_Statics::NewProp_Iterations,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBrushRemeshProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBrushRemeshProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBrushRemeshProperties_Statics::ClassParams = {
		&UBrushRemeshProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UBrushRemeshProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UBrushRemeshProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UBrushRemeshProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBrushRemeshProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBrushRemeshProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBrushRemeshProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBrushRemeshProperties, 309726258);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UBrushRemeshProperties>()
	{
		return UBrushRemeshProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBrushRemeshProperties(Z_Construct_UClass_UBrushRemeshProperties, &UBrushRemeshProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UBrushRemeshProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBrushRemeshProperties);
	void UFixedPlaneBrushProperties::StaticRegisterNativesUFixedPlaneBrushProperties()
	{
	}
	UClass* Z_Construct_UClass_UFixedPlaneBrushProperties_NoRegister()
	{
		return UFixedPlaneBrushProperties::StaticClass();
	}
	struct Z_Construct_UClass_UFixedPlaneBrushProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPropertySetEnabled_MetaData[];
#endif
		static void NewProp_bPropertySetEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPropertySetEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowGizmo_MetaData[];
#endif
		static void NewProp_bShowGizmo_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowGizmo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSnapToGrid_MetaData[];
#endif
		static void NewProp_bSnapToGrid_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSnapToGrid;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Position_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Position;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Rotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Rotation;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFixedPlaneBrushProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFixedPlaneBrushProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "DynamicMeshSculptTool.h" },
		{ "ModuleRelativePath", "Public/DynamicMeshSculptTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFixedPlaneBrushProperties_Statics::NewProp_bPropertySetEnabled_MetaData[] = {
		{ "ModuleRelativePath", "Public/DynamicMeshSculptTool.h" },
		{ "TransientToolProperty", "" },
	};
#endif
	void Z_Construct_UClass_UFixedPlaneBrushProperties_Statics::NewProp_bPropertySetEnabled_SetBit(void* Obj)
	{
		((UFixedPlaneBrushProperties*)Obj)->bPropertySetEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UFixedPlaneBrushProperties_Statics::NewProp_bPropertySetEnabled = { "bPropertySetEnabled", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UFixedPlaneBrushProperties), &Z_Construct_UClass_UFixedPlaneBrushProperties_Statics::NewProp_bPropertySetEnabled_SetBit, METADATA_PARAMS(Z_Construct_UClass_UFixedPlaneBrushProperties_Statics::NewProp_bPropertySetEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFixedPlaneBrushProperties_Statics::NewProp_bPropertySetEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFixedPlaneBrushProperties_Statics::NewProp_bShowGizmo_MetaData[] = {
		{ "Category", "TargetPlane" },
		{ "Comment", "/** Toggle whether Work Plane Positioing Gizmo is visible */" },
		{ "EditCondition", "bPropertySetEnabled == true" },
		{ "HideEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/DynamicMeshSculptTool.h" },
		{ "ToolTip", "Toggle whether Work Plane Positioing Gizmo is visible" },
	};
#endif
	void Z_Construct_UClass_UFixedPlaneBrushProperties_Statics::NewProp_bShowGizmo_SetBit(void* Obj)
	{
		((UFixedPlaneBrushProperties*)Obj)->bShowGizmo = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UFixedPlaneBrushProperties_Statics::NewProp_bShowGizmo = { "bShowGizmo", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UFixedPlaneBrushProperties), &Z_Construct_UClass_UFixedPlaneBrushProperties_Statics::NewProp_bShowGizmo_SetBit, METADATA_PARAMS(Z_Construct_UClass_UFixedPlaneBrushProperties_Statics::NewProp_bShowGizmo_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFixedPlaneBrushProperties_Statics::NewProp_bShowGizmo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFixedPlaneBrushProperties_Statics::NewProp_bSnapToGrid_MetaData[] = {
		{ "Category", "TargetPlane" },
		{ "Comment", "/** Toggle whether Work Plane snaps to grid when using Gizmo */" },
		{ "EditCondition", "bPropertySetEnabled == true" },
		{ "HideEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/DynamicMeshSculptTool.h" },
		{ "ToolTip", "Toggle whether Work Plane snaps to grid when using Gizmo" },
	};
#endif
	void Z_Construct_UClass_UFixedPlaneBrushProperties_Statics::NewProp_bSnapToGrid_SetBit(void* Obj)
	{
		((UFixedPlaneBrushProperties*)Obj)->bSnapToGrid = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UFixedPlaneBrushProperties_Statics::NewProp_bSnapToGrid = { "bSnapToGrid", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UFixedPlaneBrushProperties), &Z_Construct_UClass_UFixedPlaneBrushProperties_Statics::NewProp_bSnapToGrid_SetBit, METADATA_PARAMS(Z_Construct_UClass_UFixedPlaneBrushProperties_Statics::NewProp_bSnapToGrid_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFixedPlaneBrushProperties_Statics::NewProp_bSnapToGrid_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFixedPlaneBrushProperties_Statics::NewProp_Position_MetaData[] = {
		{ "Category", "TargetPlane" },
		{ "EditCondition", "bPropertySetEnabled == true" },
		{ "HideEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/DynamicMeshSculptTool.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UFixedPlaneBrushProperties_Statics::NewProp_Position = { "Position", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFixedPlaneBrushProperties, Position), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UFixedPlaneBrushProperties_Statics::NewProp_Position_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFixedPlaneBrushProperties_Statics::NewProp_Position_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFixedPlaneBrushProperties_Statics::NewProp_Rotation_MetaData[] = {
		{ "Category", "TargetPlane" },
		{ "EditCondition", "bPropertySetEnabled == true" },
		{ "HideEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/DynamicMeshSculptTool.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UFixedPlaneBrushProperties_Statics::NewProp_Rotation = { "Rotation", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFixedPlaneBrushProperties, Rotation), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UClass_UFixedPlaneBrushProperties_Statics::NewProp_Rotation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFixedPlaneBrushProperties_Statics::NewProp_Rotation_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UFixedPlaneBrushProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFixedPlaneBrushProperties_Statics::NewProp_bPropertySetEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFixedPlaneBrushProperties_Statics::NewProp_bShowGizmo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFixedPlaneBrushProperties_Statics::NewProp_bSnapToGrid,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFixedPlaneBrushProperties_Statics::NewProp_Position,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFixedPlaneBrushProperties_Statics::NewProp_Rotation,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFixedPlaneBrushProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFixedPlaneBrushProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFixedPlaneBrushProperties_Statics::ClassParams = {
		&UFixedPlaneBrushProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UFixedPlaneBrushProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UFixedPlaneBrushProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UFixedPlaneBrushProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFixedPlaneBrushProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFixedPlaneBrushProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFixedPlaneBrushProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFixedPlaneBrushProperties, 1285770471);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UFixedPlaneBrushProperties>()
	{
		return UFixedPlaneBrushProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFixedPlaneBrushProperties(Z_Construct_UClass_UFixedPlaneBrushProperties, &UFixedPlaneBrushProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UFixedPlaneBrushProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFixedPlaneBrushProperties);
	void UDynamicMeshSculptTool::StaticRegisterNativesUDynamicMeshSculptTool()
	{
	}
	UClass* Z_Construct_UClass_UDynamicMeshSculptTool_NoRegister()
	{
		return UDynamicMeshSculptTool::StaticClass();
	}
	struct Z_Construct_UClass_UDynamicMeshSculptTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BrushProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BrushProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SculptProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SculptProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SculptMaxBrushProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SculptMaxBrushProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_KelvinBrushProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_KelvinBrushProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RemeshProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RemeshProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GizmoProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_GizmoProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ViewProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SculptToolActions_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SculptToolActions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BrushIndicator_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BrushIndicator;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BrushIndicatorMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BrushIndicatorMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BrushIndicatorMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BrushIndicatorMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DynamicMeshComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DynamicMeshComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActiveOverrideMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ActiveOverrideMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlaneTransformGizmo_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PlaneTransformGizmo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlaneTransformProxy_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PlaneTransformProxy;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDynamicMeshSculptTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshSurfacePointTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDynamicMeshSculptTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Dynamic Mesh Sculpt Tool Class\n */" },
		{ "IncludePath", "DynamicMeshSculptTool.h" },
		{ "ModuleRelativePath", "Public/DynamicMeshSculptTool.h" },
		{ "ToolTip", "Dynamic Mesh Sculpt Tool Class" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_BrushProperties_MetaData[] = {
		{ "Comment", "/** Properties that control brush size/etc*/" },
		{ "ModuleRelativePath", "Public/DynamicMeshSculptTool.h" },
		{ "ToolTip", "Properties that control brush size/etc" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_BrushProperties = { "BrushProperties", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDynamicMeshSculptTool, BrushProperties), Z_Construct_UClass_USculptBrushProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_BrushProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_BrushProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_SculptProperties_MetaData[] = {
		{ "Comment", "/** Properties that control sculpting*/" },
		{ "ModuleRelativePath", "Public/DynamicMeshSculptTool.h" },
		{ "ToolTip", "Properties that control sculpting" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_SculptProperties = { "SculptProperties", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDynamicMeshSculptTool, SculptProperties), Z_Construct_UClass_UBrushSculptProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_SculptProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_SculptProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_SculptMaxBrushProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/DynamicMeshSculptTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_SculptMaxBrushProperties = { "SculptMaxBrushProperties", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDynamicMeshSculptTool, SculptMaxBrushProperties), Z_Construct_UClass_USculptMaxBrushProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_SculptMaxBrushProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_SculptMaxBrushProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_KelvinBrushProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/DynamicMeshSculptTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_KelvinBrushProperties = { "KelvinBrushProperties", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDynamicMeshSculptTool, KelvinBrushProperties), Z_Construct_UClass_UKelvinBrushProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_KelvinBrushProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_KelvinBrushProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_RemeshProperties_MetaData[] = {
		{ "Comment", "/** Properties that control dynamic remeshing */" },
		{ "ModuleRelativePath", "Public/DynamicMeshSculptTool.h" },
		{ "ToolTip", "Properties that control dynamic remeshing" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_RemeshProperties = { "RemeshProperties", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDynamicMeshSculptTool, RemeshProperties), Z_Construct_UClass_UBrushRemeshProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_RemeshProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_RemeshProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_GizmoProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/DynamicMeshSculptTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_GizmoProperties = { "GizmoProperties", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDynamicMeshSculptTool, GizmoProperties), Z_Construct_UClass_UFixedPlaneBrushProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_GizmoProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_GizmoProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_ViewProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/DynamicMeshSculptTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_ViewProperties = { "ViewProperties", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDynamicMeshSculptTool, ViewProperties), Z_Construct_UClass_UMeshEditingViewProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_ViewProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_ViewProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_SculptToolActions_MetaData[] = {
		{ "ModuleRelativePath", "Public/DynamicMeshSculptTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_SculptToolActions = { "SculptToolActions", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDynamicMeshSculptTool, SculptToolActions), Z_Construct_UClass_UDynamicSculptToolActions_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_SculptToolActions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_SculptToolActions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_BrushIndicator_MetaData[] = {
		{ "ModuleRelativePath", "Public/DynamicMeshSculptTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_BrushIndicator = { "BrushIndicator", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDynamicMeshSculptTool, BrushIndicator), Z_Construct_UClass_UBrushStampIndicator_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_BrushIndicator_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_BrushIndicator_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_BrushIndicatorMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Public/DynamicMeshSculptTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_BrushIndicatorMaterial = { "BrushIndicatorMaterial", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDynamicMeshSculptTool, BrushIndicatorMaterial), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_BrushIndicatorMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_BrushIndicatorMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_BrushIndicatorMesh_MetaData[] = {
		{ "ModuleRelativePath", "Public/DynamicMeshSculptTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_BrushIndicatorMesh = { "BrushIndicatorMesh", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDynamicMeshSculptTool, BrushIndicatorMesh), Z_Construct_UClass_UPreviewMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_BrushIndicatorMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_BrushIndicatorMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_DynamicMeshComponent_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DynamicMeshSculptTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_DynamicMeshComponent = { "DynamicMeshComponent", nullptr, (EPropertyFlags)0x0040000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDynamicMeshSculptTool, DynamicMeshComponent), Z_Construct_UClass_UOctreeDynamicMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_DynamicMeshComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_DynamicMeshComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_ActiveOverrideMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Public/DynamicMeshSculptTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_ActiveOverrideMaterial = { "ActiveOverrideMaterial", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDynamicMeshSculptTool, ActiveOverrideMaterial), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_ActiveOverrideMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_ActiveOverrideMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_PlaneTransformGizmo_MetaData[] = {
		{ "Comment", "// plane gizmo\n" },
		{ "ModuleRelativePath", "Public/DynamicMeshSculptTool.h" },
		{ "ToolTip", "plane gizmo" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_PlaneTransformGizmo = { "PlaneTransformGizmo", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDynamicMeshSculptTool, PlaneTransformGizmo), Z_Construct_UClass_UTransformGizmo_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_PlaneTransformGizmo_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_PlaneTransformGizmo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_PlaneTransformProxy_MetaData[] = {
		{ "ModuleRelativePath", "Public/DynamicMeshSculptTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_PlaneTransformProxy = { "PlaneTransformProxy", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDynamicMeshSculptTool, PlaneTransformProxy), Z_Construct_UClass_UTransformProxy_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_PlaneTransformProxy_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_PlaneTransformProxy_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDynamicMeshSculptTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_BrushProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_SculptProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_SculptMaxBrushProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_KelvinBrushProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_RemeshProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_GizmoProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_ViewProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_SculptToolActions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_BrushIndicator,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_BrushIndicatorMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_BrushIndicatorMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_DynamicMeshComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_ActiveOverrideMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_PlaneTransformGizmo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDynamicMeshSculptTool_Statics::NewProp_PlaneTransformProxy,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDynamicMeshSculptTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDynamicMeshSculptTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDynamicMeshSculptTool_Statics::ClassParams = {
		&UDynamicMeshSculptTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDynamicMeshSculptTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDynamicMeshSculptTool_Statics::PropPointers),
		0,
		0x009000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UDynamicMeshSculptTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDynamicMeshSculptTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDynamicMeshSculptTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDynamicMeshSculptTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDynamicMeshSculptTool, 477381127);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UDynamicMeshSculptTool>()
	{
		return UDynamicMeshSculptTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDynamicMeshSculptTool(Z_Construct_UClass_UDynamicMeshSculptTool, &UDynamicMeshSculptTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UDynamicMeshSculptTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDynamicMeshSculptTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
