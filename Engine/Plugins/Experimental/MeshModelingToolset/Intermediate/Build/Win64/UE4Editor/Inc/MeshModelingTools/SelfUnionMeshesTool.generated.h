// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLS_SelfUnionMeshesTool_generated_h
#error "SelfUnionMeshesTool.generated.h already included, missing '#pragma once' in SelfUnionMeshesTool.h"
#endif
#define MESHMODELINGTOOLS_SelfUnionMeshesTool_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_25_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_25_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_25_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_25_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSelfUnionMeshesToolProperties(); \
	friend struct Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics; \
public: \
	DECLARE_CLASS(USelfUnionMeshesToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(USelfUnionMeshesToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_25_INCLASS \
private: \
	static void StaticRegisterNativesUSelfUnionMeshesToolProperties(); \
	friend struct Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics; \
public: \
	DECLARE_CLASS(USelfUnionMeshesToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(USelfUnionMeshesToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_25_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USelfUnionMeshesToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USelfUnionMeshesToolProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USelfUnionMeshesToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USelfUnionMeshesToolProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USelfUnionMeshesToolProperties(USelfUnionMeshesToolProperties&&); \
	NO_API USelfUnionMeshesToolProperties(const USelfUnionMeshesToolProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_25_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USelfUnionMeshesToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USelfUnionMeshesToolProperties(USelfUnionMeshesToolProperties&&); \
	NO_API USelfUnionMeshesToolProperties(const USelfUnionMeshesToolProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USelfUnionMeshesToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USelfUnionMeshesToolProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USelfUnionMeshesToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_25_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_22_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_25_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_25_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_25_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_25_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_25_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_25_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_25_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_25_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_25_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_25_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class USelfUnionMeshesToolProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_56_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_56_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_56_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_56_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSelfUnionMeshesTool(); \
	friend struct Z_Construct_UClass_USelfUnionMeshesTool_Statics; \
public: \
	DECLARE_CLASS(USelfUnionMeshesTool, UBaseCreateFromSelectedTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(USelfUnionMeshesTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_56_INCLASS \
private: \
	static void StaticRegisterNativesUSelfUnionMeshesTool(); \
	friend struct Z_Construct_UClass_USelfUnionMeshesTool_Statics; \
public: \
	DECLARE_CLASS(USelfUnionMeshesTool, UBaseCreateFromSelectedTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(USelfUnionMeshesTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_56_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USelfUnionMeshesTool(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USelfUnionMeshesTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USelfUnionMeshesTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USelfUnionMeshesTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USelfUnionMeshesTool(USelfUnionMeshesTool&&); \
	NO_API USelfUnionMeshesTool(const USelfUnionMeshesTool&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_56_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USelfUnionMeshesTool(USelfUnionMeshesTool&&); \
	NO_API USelfUnionMeshesTool(const USelfUnionMeshesTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USelfUnionMeshesTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USelfUnionMeshesTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(USelfUnionMeshesTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_56_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Properties() { return STRUCT_OFFSET(USelfUnionMeshesTool, Properties); } \
	FORCEINLINE static uint32 __PPO__DrawnLineSet() { return STRUCT_OFFSET(USelfUnionMeshesTool, DrawnLineSet); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_53_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_56_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_56_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_56_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_56_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_56_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_56_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_56_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_56_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_56_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_56_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_56_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_56_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class USelfUnionMeshesTool>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_102_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_102_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_102_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_102_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSelfUnionMeshesToolBuilder(); \
	friend struct Z_Construct_UClass_USelfUnionMeshesToolBuilder_Statics; \
public: \
	DECLARE_CLASS(USelfUnionMeshesToolBuilder, UBaseCreateFromSelectedToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(USelfUnionMeshesToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_102_INCLASS \
private: \
	static void StaticRegisterNativesUSelfUnionMeshesToolBuilder(); \
	friend struct Z_Construct_UClass_USelfUnionMeshesToolBuilder_Statics; \
public: \
	DECLARE_CLASS(USelfUnionMeshesToolBuilder, UBaseCreateFromSelectedToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(USelfUnionMeshesToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_102_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USelfUnionMeshesToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USelfUnionMeshesToolBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USelfUnionMeshesToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USelfUnionMeshesToolBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USelfUnionMeshesToolBuilder(USelfUnionMeshesToolBuilder&&); \
	NO_API USelfUnionMeshesToolBuilder(const USelfUnionMeshesToolBuilder&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_102_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USelfUnionMeshesToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USelfUnionMeshesToolBuilder(USelfUnionMeshesToolBuilder&&); \
	NO_API USelfUnionMeshesToolBuilder(const USelfUnionMeshesToolBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USelfUnionMeshesToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USelfUnionMeshesToolBuilder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USelfUnionMeshesToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_102_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_99_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_102_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_102_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_102_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_102_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_102_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_102_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_102_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_102_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_102_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_102_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_102_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h_102_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class USelfUnionMeshesToolBuilder>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SelfUnionMeshesTool_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
