// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MODELINGCOMPONENTS_PlaneDistanceFromHitMechanic_generated_h
#error "PlaneDistanceFromHitMechanic.generated.h already included, missing '#pragma once' in PlaneDistanceFromHitMechanic.h"
#endif
#define MODELINGCOMPONENTS_PlaneDistanceFromHitMechanic_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_PlaneDistanceFromHitMechanic_h_20_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_PlaneDistanceFromHitMechanic_h_20_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_PlaneDistanceFromHitMechanic_h_20_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_PlaneDistanceFromHitMechanic_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPlaneDistanceFromHitMechanic(); \
	friend struct Z_Construct_UClass_UPlaneDistanceFromHitMechanic_Statics; \
public: \
	DECLARE_CLASS(UPlaneDistanceFromHitMechanic, UInteractionMechanic, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UPlaneDistanceFromHitMechanic)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_PlaneDistanceFromHitMechanic_h_20_INCLASS \
private: \
	static void StaticRegisterNativesUPlaneDistanceFromHitMechanic(); \
	friend struct Z_Construct_UClass_UPlaneDistanceFromHitMechanic_Statics; \
public: \
	DECLARE_CLASS(UPlaneDistanceFromHitMechanic, UInteractionMechanic, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UPlaneDistanceFromHitMechanic)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_PlaneDistanceFromHitMechanic_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPlaneDistanceFromHitMechanic(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPlaneDistanceFromHitMechanic) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPlaneDistanceFromHitMechanic); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPlaneDistanceFromHitMechanic); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPlaneDistanceFromHitMechanic(UPlaneDistanceFromHitMechanic&&); \
	NO_API UPlaneDistanceFromHitMechanic(const UPlaneDistanceFromHitMechanic&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_PlaneDistanceFromHitMechanic_h_20_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPlaneDistanceFromHitMechanic() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPlaneDistanceFromHitMechanic(UPlaneDistanceFromHitMechanic&&); \
	NO_API UPlaneDistanceFromHitMechanic(const UPlaneDistanceFromHitMechanic&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPlaneDistanceFromHitMechanic); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPlaneDistanceFromHitMechanic); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UPlaneDistanceFromHitMechanic)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_PlaneDistanceFromHitMechanic_h_20_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_PlaneDistanceFromHitMechanic_h_17_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_PlaneDistanceFromHitMechanic_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_PlaneDistanceFromHitMechanic_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_PlaneDistanceFromHitMechanic_h_20_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_PlaneDistanceFromHitMechanic_h_20_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_PlaneDistanceFromHitMechanic_h_20_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_PlaneDistanceFromHitMechanic_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_PlaneDistanceFromHitMechanic_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_PlaneDistanceFromHitMechanic_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_PlaneDistanceFromHitMechanic_h_20_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_PlaneDistanceFromHitMechanic_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_PlaneDistanceFromHitMechanic_h_20_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_PlaneDistanceFromHitMechanic_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MODELINGCOMPONENTS_API UClass* StaticClass<class UPlaneDistanceFromHitMechanic>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_PlaneDistanceFromHitMechanic_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
