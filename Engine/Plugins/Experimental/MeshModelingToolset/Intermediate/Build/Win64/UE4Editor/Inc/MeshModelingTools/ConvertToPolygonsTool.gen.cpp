// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/ConvertToPolygonsTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConvertToPolygonsTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EConvertToPolygonsMode();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UConvertToPolygonsToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UConvertToPolygonsToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UConvertToPolygonsToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UConvertToPolygonsToolProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UConvertToPolygonsTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UConvertToPolygonsTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_USingleSelectionTool();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPreviewMesh_NoRegister();
// End Cross Module References
	static UEnum* EConvertToPolygonsMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EConvertToPolygonsMode, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EConvertToPolygonsMode"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EConvertToPolygonsMode>()
	{
		return EConvertToPolygonsMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EConvertToPolygonsMode(EConvertToPolygonsMode_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EConvertToPolygonsMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EConvertToPolygonsMode_Hash() { return 930372374U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EConvertToPolygonsMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EConvertToPolygonsMode"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EConvertToPolygonsMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EConvertToPolygonsMode::FaceNormalDeviation", (int64)EConvertToPolygonsMode::FaceNormalDeviation },
				{ "EConvertToPolygonsMode::FromUVISlands", (int64)EConvertToPolygonsMode::FromUVISlands },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "FaceNormalDeviation.Comment", "/** Convert based on Angle Tolerance between Face Normals */" },
				{ "FaceNormalDeviation.Name", "EConvertToPolygonsMode::FaceNormalDeviation" },
				{ "FaceNormalDeviation.ToolTip", "Convert based on Angle Tolerance between Face Normals" },
				{ "FromUVISlands.Comment", "/** Create PolyGroups based on UV Islands */" },
				{ "FromUVISlands.Name", "EConvertToPolygonsMode::FromUVISlands" },
				{ "FromUVISlands.ToolTip", "Create PolyGroups based on UV Islands" },
				{ "ModuleRelativePath", "Public/ConvertToPolygonsTool.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EConvertToPolygonsMode",
				"EConvertToPolygonsMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UConvertToPolygonsToolBuilder::StaticRegisterNativesUConvertToPolygonsToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UConvertToPolygonsToolBuilder_NoRegister()
	{
		return UConvertToPolygonsToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UConvertToPolygonsToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UConvertToPolygonsToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConvertToPolygonsToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "ConvertToPolygonsTool.h" },
		{ "ModuleRelativePath", "Public/ConvertToPolygonsTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UConvertToPolygonsToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UConvertToPolygonsToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UConvertToPolygonsToolBuilder_Statics::ClassParams = {
		&UConvertToPolygonsToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UConvertToPolygonsToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UConvertToPolygonsToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UConvertToPolygonsToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UConvertToPolygonsToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UConvertToPolygonsToolBuilder, 1628867349);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UConvertToPolygonsToolBuilder>()
	{
		return UConvertToPolygonsToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UConvertToPolygonsToolBuilder(Z_Construct_UClass_UConvertToPolygonsToolBuilder, &UConvertToPolygonsToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UConvertToPolygonsToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UConvertToPolygonsToolBuilder);
	void UConvertToPolygonsToolProperties::StaticRegisterNativesUConvertToPolygonsToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UConvertToPolygonsToolProperties_NoRegister()
	{
		return UConvertToPolygonsToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UConvertToPolygonsToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_ConversionMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConversionMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ConversionMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AngleTolerance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AngleTolerance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCalculateNormals_MetaData[];
#endif
		static void NewProp_bCalculateNormals_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCalculateNormals;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowGroupColors_MetaData[];
#endif
		static void NewProp_bShowGroupColors_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowGroupColors;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UConvertToPolygonsToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConvertToPolygonsToolProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ConvertToPolygonsTool.h" },
		{ "ModuleRelativePath", "Public/ConvertToPolygonsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UConvertToPolygonsToolProperties_Statics::NewProp_ConversionMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConvertToPolygonsToolProperties_Statics::NewProp_ConversionMode_MetaData[] = {
		{ "Category", "PolyGroups" },
		{ "Comment", "/** Strategy to use to group triangles */" },
		{ "ModuleRelativePath", "Public/ConvertToPolygonsTool.h" },
		{ "ToolTip", "Strategy to use to group triangles" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UConvertToPolygonsToolProperties_Statics::NewProp_ConversionMode = { "ConversionMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConvertToPolygonsToolProperties, ConversionMode), Z_Construct_UEnum_MeshModelingTools_EConvertToPolygonsMode, METADATA_PARAMS(Z_Construct_UClass_UConvertToPolygonsToolProperties_Statics::NewProp_ConversionMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConvertToPolygonsToolProperties_Statics::NewProp_ConversionMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConvertToPolygonsToolProperties_Statics::NewProp_AngleTolerance_MetaData[] = {
		{ "Category", "PolyGroups" },
		{ "ClampMax", "90.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Tolerance for planarity */" },
		{ "EditCondition", "ConversionMode == EConvertToPolygonsMode::FaceNormalDeviation" },
		{ "ModuleRelativePath", "Public/ConvertToPolygonsTool.h" },
		{ "ToolTip", "Tolerance for planarity" },
		{ "UIMax", "20.0" },
		{ "UIMin", "0.001" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UConvertToPolygonsToolProperties_Statics::NewProp_AngleTolerance = { "AngleTolerance", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConvertToPolygonsToolProperties, AngleTolerance), METADATA_PARAMS(Z_Construct_UClass_UConvertToPolygonsToolProperties_Statics::NewProp_AngleTolerance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConvertToPolygonsToolProperties_Statics::NewProp_AngleTolerance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConvertToPolygonsToolProperties_Statics::NewProp_bCalculateNormals_MetaData[] = {
		{ "Category", "PolyGroups" },
		{ "Comment", "/** If true, normals are recomputed per-group, with hard edges at group boundaries */" },
		{ "ModuleRelativePath", "Public/ConvertToPolygonsTool.h" },
		{ "ToolTip", "If true, normals are recomputed per-group, with hard edges at group boundaries" },
	};
#endif
	void Z_Construct_UClass_UConvertToPolygonsToolProperties_Statics::NewProp_bCalculateNormals_SetBit(void* Obj)
	{
		((UConvertToPolygonsToolProperties*)Obj)->bCalculateNormals = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UConvertToPolygonsToolProperties_Statics::NewProp_bCalculateNormals = { "bCalculateNormals", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UConvertToPolygonsToolProperties), &Z_Construct_UClass_UConvertToPolygonsToolProperties_Statics::NewProp_bCalculateNormals_SetBit, METADATA_PARAMS(Z_Construct_UClass_UConvertToPolygonsToolProperties_Statics::NewProp_bCalculateNormals_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConvertToPolygonsToolProperties_Statics::NewProp_bCalculateNormals_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConvertToPolygonsToolProperties_Statics::NewProp_bShowGroupColors_MetaData[] = {
		{ "Category", "Display" },
		{ "Comment", "/** Display each group with a different auto-generated color */" },
		{ "ModuleRelativePath", "Public/ConvertToPolygonsTool.h" },
		{ "ToolTip", "Display each group with a different auto-generated color" },
	};
#endif
	void Z_Construct_UClass_UConvertToPolygonsToolProperties_Statics::NewProp_bShowGroupColors_SetBit(void* Obj)
	{
		((UConvertToPolygonsToolProperties*)Obj)->bShowGroupColors = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UConvertToPolygonsToolProperties_Statics::NewProp_bShowGroupColors = { "bShowGroupColors", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UConvertToPolygonsToolProperties), &Z_Construct_UClass_UConvertToPolygonsToolProperties_Statics::NewProp_bShowGroupColors_SetBit, METADATA_PARAMS(Z_Construct_UClass_UConvertToPolygonsToolProperties_Statics::NewProp_bShowGroupColors_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConvertToPolygonsToolProperties_Statics::NewProp_bShowGroupColors_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UConvertToPolygonsToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConvertToPolygonsToolProperties_Statics::NewProp_ConversionMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConvertToPolygonsToolProperties_Statics::NewProp_ConversionMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConvertToPolygonsToolProperties_Statics::NewProp_AngleTolerance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConvertToPolygonsToolProperties_Statics::NewProp_bCalculateNormals,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConvertToPolygonsToolProperties_Statics::NewProp_bShowGroupColors,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UConvertToPolygonsToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UConvertToPolygonsToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UConvertToPolygonsToolProperties_Statics::ClassParams = {
		&UConvertToPolygonsToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UConvertToPolygonsToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UConvertToPolygonsToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UConvertToPolygonsToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UConvertToPolygonsToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UConvertToPolygonsToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UConvertToPolygonsToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UConvertToPolygonsToolProperties, 1736246738);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UConvertToPolygonsToolProperties>()
	{
		return UConvertToPolygonsToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UConvertToPolygonsToolProperties(Z_Construct_UClass_UConvertToPolygonsToolProperties, &UConvertToPolygonsToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UConvertToPolygonsToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UConvertToPolygonsToolProperties);
	void UConvertToPolygonsTool::StaticRegisterNativesUConvertToPolygonsTool()
	{
	}
	UClass* Z_Construct_UClass_UConvertToPolygonsTool_NoRegister()
	{
		return UConvertToPolygonsTool::StaticClass();
	}
	struct Z_Construct_UClass_UConvertToPolygonsTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Settings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Settings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviewMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviewMesh;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UConvertToPolygonsTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USingleSelectionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConvertToPolygonsTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "ConvertToPolygonsTool.h" },
		{ "ModuleRelativePath", "Public/ConvertToPolygonsTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConvertToPolygonsTool_Statics::NewProp_Settings_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConvertToPolygonsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UConvertToPolygonsTool_Statics::NewProp_Settings = { "Settings", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConvertToPolygonsTool, Settings), Z_Construct_UClass_UConvertToPolygonsToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UConvertToPolygonsTool_Statics::NewProp_Settings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConvertToPolygonsTool_Statics::NewProp_Settings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConvertToPolygonsTool_Statics::NewProp_PreviewMesh_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConvertToPolygonsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UConvertToPolygonsTool_Statics::NewProp_PreviewMesh = { "PreviewMesh", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConvertToPolygonsTool, PreviewMesh), Z_Construct_UClass_UPreviewMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UConvertToPolygonsTool_Statics::NewProp_PreviewMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConvertToPolygonsTool_Statics::NewProp_PreviewMesh_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UConvertToPolygonsTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConvertToPolygonsTool_Statics::NewProp_Settings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConvertToPolygonsTool_Statics::NewProp_PreviewMesh,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UConvertToPolygonsTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UConvertToPolygonsTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UConvertToPolygonsTool_Statics::ClassParams = {
		&UConvertToPolygonsTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UConvertToPolygonsTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UConvertToPolygonsTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UConvertToPolygonsTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UConvertToPolygonsTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UConvertToPolygonsTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UConvertToPolygonsTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UConvertToPolygonsTool, 1255495202);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UConvertToPolygonsTool>()
	{
		return UConvertToPolygonsTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UConvertToPolygonsTool(Z_Construct_UClass_UConvertToPolygonsTool, &UConvertToPolygonsTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UConvertToPolygonsTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UConvertToPolygonsTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
