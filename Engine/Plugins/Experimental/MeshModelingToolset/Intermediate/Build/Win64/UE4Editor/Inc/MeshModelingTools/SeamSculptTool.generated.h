// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLS_SeamSculptTool_generated_h
#error "SeamSculptTool.generated.h already included, missing '#pragma once' in SeamSculptTool.h"
#endif
#define MESHMODELINGTOOLS_SeamSculptTool_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_19_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_19_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSeamSculptToolBuilder(); \
	friend struct Z_Construct_UClass_USeamSculptToolBuilder_Statics; \
public: \
	DECLARE_CLASS(USeamSculptToolBuilder, UMeshSurfacePointToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(USeamSculptToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUSeamSculptToolBuilder(); \
	friend struct Z_Construct_UClass_USeamSculptToolBuilder_Statics; \
public: \
	DECLARE_CLASS(USeamSculptToolBuilder, UMeshSurfacePointToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(USeamSculptToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USeamSculptToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USeamSculptToolBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USeamSculptToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USeamSculptToolBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USeamSculptToolBuilder(USeamSculptToolBuilder&&); \
	NO_API USeamSculptToolBuilder(const USeamSculptToolBuilder&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_19_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USeamSculptToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USeamSculptToolBuilder(USeamSculptToolBuilder&&); \
	NO_API USeamSculptToolBuilder(const USeamSculptToolBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USeamSculptToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USeamSculptToolBuilder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USeamSculptToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_19_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_16_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_19_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_19_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_19_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_19_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_19_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class USeamSculptToolBuilder>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_32_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_32_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_32_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_32_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSeamSculptToolProperties(); \
	friend struct Z_Construct_UClass_USeamSculptToolProperties_Statics; \
public: \
	DECLARE_CLASS(USeamSculptToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(USeamSculptToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_32_INCLASS \
private: \
	static void StaticRegisterNativesUSeamSculptToolProperties(); \
	friend struct Z_Construct_UClass_USeamSculptToolProperties_Statics; \
public: \
	DECLARE_CLASS(USeamSculptToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(USeamSculptToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_32_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USeamSculptToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USeamSculptToolProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USeamSculptToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USeamSculptToolProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USeamSculptToolProperties(USeamSculptToolProperties&&); \
	NO_API USeamSculptToolProperties(const USeamSculptToolProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_32_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USeamSculptToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USeamSculptToolProperties(USeamSculptToolProperties&&); \
	NO_API USeamSculptToolProperties(const USeamSculptToolProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USeamSculptToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USeamSculptToolProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USeamSculptToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_32_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_29_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_32_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_32_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_32_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_32_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_32_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_32_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_32_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_32_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_32_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_32_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_32_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_32_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class USeamSculptToolProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_52_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_52_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_52_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_52_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSeamSculptTool(); \
	friend struct Z_Construct_UClass_USeamSculptTool_Statics; \
public: \
	DECLARE_CLASS(USeamSculptTool, UDynamicMeshBrushTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(USeamSculptTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_52_INCLASS \
private: \
	static void StaticRegisterNativesUSeamSculptTool(); \
	friend struct Z_Construct_UClass_USeamSculptTool_Statics; \
public: \
	DECLARE_CLASS(USeamSculptTool, UDynamicMeshBrushTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(USeamSculptTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_52_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USeamSculptTool(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USeamSculptTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USeamSculptTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USeamSculptTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USeamSculptTool(USeamSculptTool&&); \
	NO_API USeamSculptTool(const USeamSculptTool&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_52_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USeamSculptTool(USeamSculptTool&&); \
	NO_API USeamSculptTool(const USeamSculptTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USeamSculptTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USeamSculptTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(USeamSculptTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_52_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__PreviewGeom() { return STRUCT_OFFSET(USeamSculptTool, PreviewGeom); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_49_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_52_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_52_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_52_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_52_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_52_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_52_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_52_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_52_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_52_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_52_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_52_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h_52_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class USeamSculptTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_SeamSculptTool_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
