// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/VoxelSolidifyMeshesTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVoxelSolidifyMeshesTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UVoxelSolidifyMeshesTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UVoxelSolidifyMeshesTool();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UBaseVoxelTool();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UVoxelSolidifyMeshesToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UVoxelSolidifyMeshesToolBuilder();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UBaseCreateFromSelectedToolBuilder();
// End Cross Module References
	void UVoxelSolidifyMeshesToolProperties::StaticRegisterNativesUVoxelSolidifyMeshesToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_NoRegister()
	{
		return UVoxelSolidifyMeshesToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WindingThreshold_MetaData[];
#endif
		static const UE4CodeGen_Private::FDoublePropertyParams NewProp_WindingThreshold;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExtendBounds_MetaData[];
#endif
		static const UE4CodeGen_Private::FDoublePropertyParams NewProp_ExtendBounds;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SurfaceSearchSteps_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_SurfaceSearchSteps;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSolidAtBoundaries_MetaData[];
#endif
		static void NewProp_bSolidAtBoundaries_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSolidAtBoundaries;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bMakeOffsetSurfaces_MetaData[];
#endif
		static void NewProp_bMakeOffsetSurfaces_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bMakeOffsetSurfaces;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OffsetThickness_MetaData[];
#endif
		static const UE4CodeGen_Private::FDoublePropertyParams NewProp_OffsetThickness;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Properties of the solidify operation\n */" },
		{ "IncludePath", "VoxelSolidifyMeshesTool.h" },
		{ "ModuleRelativePath", "Public/VoxelSolidifyMeshesTool.h" },
		{ "ToolTip", "Properties of the solidify operation" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::NewProp_WindingThreshold_MetaData[] = {
		{ "Category", "Options" },
		{ "ClampMax", "10" },
		{ "ClampMin", "-10" },
		{ "Comment", "/** Winding number threshold to determine what is consider inside the mesh */" },
		{ "ModuleRelativePath", "Public/VoxelSolidifyMeshesTool.h" },
		{ "ToolTip", "Winding number threshold to determine what is consider inside the mesh" },
		{ "UIMax", ".9" },
		{ "UIMin", "0.1" },
	};
#endif
	const UE4CodeGen_Private::FDoublePropertyParams Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::NewProp_WindingThreshold = { "WindingThreshold", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVoxelSolidifyMeshesToolProperties, WindingThreshold), METADATA_PARAMS(Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::NewProp_WindingThreshold_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::NewProp_WindingThreshold_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::NewProp_ExtendBounds_MetaData[] = {
		{ "Category", "Options" },
		{ "ClampMax", "1000" },
		{ "ClampMin", "0" },
		{ "Comment", "/** How far we allow bounds of solid surface to go beyond the bounds of the original input surface before clamping / cutting the surface off */" },
		{ "ModuleRelativePath", "Public/VoxelSolidifyMeshesTool.h" },
		{ "ToolTip", "How far we allow bounds of solid surface to go beyond the bounds of the original input surface before clamping / cutting the surface off" },
		{ "UIMax", "10" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FDoublePropertyParams Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::NewProp_ExtendBounds = { "ExtendBounds", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVoxelSolidifyMeshesToolProperties, ExtendBounds), METADATA_PARAMS(Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::NewProp_ExtendBounds_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::NewProp_ExtendBounds_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::NewProp_SurfaceSearchSteps_MetaData[] = {
		{ "Category", "Options" },
		{ "ClampMax", "10" },
		{ "ClampMin", "0" },
		{ "Comment", "/** How many binary search steps to take when placing vertices on the surface */" },
		{ "ModuleRelativePath", "Public/VoxelSolidifyMeshesTool.h" },
		{ "ToolTip", "How many binary search steps to take when placing vertices on the surface" },
		{ "UIMax", "6" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::NewProp_SurfaceSearchSteps = { "SurfaceSearchSteps", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVoxelSolidifyMeshesToolProperties, SurfaceSearchSteps), METADATA_PARAMS(Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::NewProp_SurfaceSearchSteps_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::NewProp_SurfaceSearchSteps_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::NewProp_bSolidAtBoundaries_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Whether to fill at the border of the bounding box, if the surface extends beyond the voxel boundaries */" },
		{ "ModuleRelativePath", "Public/VoxelSolidifyMeshesTool.h" },
		{ "ToolTip", "Whether to fill at the border of the bounding box, if the surface extends beyond the voxel boundaries" },
	};
#endif
	void Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::NewProp_bSolidAtBoundaries_SetBit(void* Obj)
	{
		((UVoxelSolidifyMeshesToolProperties*)Obj)->bSolidAtBoundaries = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::NewProp_bSolidAtBoundaries = { "bSolidAtBoundaries", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVoxelSolidifyMeshesToolProperties), &Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::NewProp_bSolidAtBoundaries_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::NewProp_bSolidAtBoundaries_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::NewProp_bSolidAtBoundaries_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::NewProp_bMakeOffsetSurfaces_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** If true, treats mesh surfaces with open boundaries as having a fixed, user-defined thickness */" },
		{ "ModuleRelativePath", "Public/VoxelSolidifyMeshesTool.h" },
		{ "ToolTip", "If true, treats mesh surfaces with open boundaries as having a fixed, user-defined thickness" },
	};
#endif
	void Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::NewProp_bMakeOffsetSurfaces_SetBit(void* Obj)
	{
		((UVoxelSolidifyMeshesToolProperties*)Obj)->bMakeOffsetSurfaces = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::NewProp_bMakeOffsetSurfaces = { "bMakeOffsetSurfaces", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVoxelSolidifyMeshesToolProperties), &Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::NewProp_bMakeOffsetSurfaces_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::NewProp_bMakeOffsetSurfaces_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::NewProp_bMakeOffsetSurfaces_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::NewProp_OffsetThickness_MetaData[] = {
		{ "Category", "Options" },
		{ "ClampMax", "1000" },
		{ "ClampMin", ".001" },
		{ "Comment", "/** Thickness of offset surfaces */" },
		{ "EditCondition", "bMakeOffsetSurfaces == true" },
		{ "ModuleRelativePath", "Public/VoxelSolidifyMeshesTool.h" },
		{ "ToolTip", "Thickness of offset surfaces" },
		{ "UIMax", "100" },
		{ "UIMin", ".1" },
	};
#endif
	const UE4CodeGen_Private::FDoublePropertyParams Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::NewProp_OffsetThickness = { "OffsetThickness", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVoxelSolidifyMeshesToolProperties, OffsetThickness), METADATA_PARAMS(Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::NewProp_OffsetThickness_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::NewProp_OffsetThickness_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::NewProp_WindingThreshold,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::NewProp_ExtendBounds,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::NewProp_SurfaceSearchSteps,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::NewProp_bSolidAtBoundaries,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::NewProp_bMakeOffsetSurfaces,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::NewProp_OffsetThickness,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVoxelSolidifyMeshesToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::ClassParams = {
		&UVoxelSolidifyMeshesToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVoxelSolidifyMeshesToolProperties, 1490311570);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UVoxelSolidifyMeshesToolProperties>()
	{
		return UVoxelSolidifyMeshesToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVoxelSolidifyMeshesToolProperties(Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties, &UVoxelSolidifyMeshesToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UVoxelSolidifyMeshesToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVoxelSolidifyMeshesToolProperties);
	void UVoxelSolidifyMeshesTool::StaticRegisterNativesUVoxelSolidifyMeshesTool()
	{
	}
	UClass* Z_Construct_UClass_UVoxelSolidifyMeshesTool_NoRegister()
	{
		return UVoxelSolidifyMeshesTool::StaticClass();
	}
	struct Z_Construct_UClass_UVoxelSolidifyMeshesTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SolidifyProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SolidifyProperties;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVoxelSolidifyMeshesTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBaseVoxelTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVoxelSolidifyMeshesTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Tool to take one or more meshes, possibly intersecting and possibly with holes, and create a single solid mesh with consistent inside/outside\n */" },
		{ "IncludePath", "VoxelSolidifyMeshesTool.h" },
		{ "ModuleRelativePath", "Public/VoxelSolidifyMeshesTool.h" },
		{ "ToolTip", "Tool to take one or more meshes, possibly intersecting and possibly with holes, and create a single solid mesh with consistent inside/outside" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVoxelSolidifyMeshesTool_Statics::NewProp_SolidifyProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/VoxelSolidifyMeshesTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UVoxelSolidifyMeshesTool_Statics::NewProp_SolidifyProperties = { "SolidifyProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVoxelSolidifyMeshesTool, SolidifyProperties), Z_Construct_UClass_UVoxelSolidifyMeshesToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVoxelSolidifyMeshesTool_Statics::NewProp_SolidifyProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelSolidifyMeshesTool_Statics::NewProp_SolidifyProperties_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UVoxelSolidifyMeshesTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVoxelSolidifyMeshesTool_Statics::NewProp_SolidifyProperties,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVoxelSolidifyMeshesTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVoxelSolidifyMeshesTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVoxelSolidifyMeshesTool_Statics::ClassParams = {
		&UVoxelSolidifyMeshesTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UVoxelSolidifyMeshesTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelSolidifyMeshesTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UVoxelSolidifyMeshesTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelSolidifyMeshesTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVoxelSolidifyMeshesTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVoxelSolidifyMeshesTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVoxelSolidifyMeshesTool, 2141165420);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UVoxelSolidifyMeshesTool>()
	{
		return UVoxelSolidifyMeshesTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVoxelSolidifyMeshesTool(Z_Construct_UClass_UVoxelSolidifyMeshesTool, &UVoxelSolidifyMeshesTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UVoxelSolidifyMeshesTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVoxelSolidifyMeshesTool);
	void UVoxelSolidifyMeshesToolBuilder::StaticRegisterNativesUVoxelSolidifyMeshesToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UVoxelSolidifyMeshesToolBuilder_NoRegister()
	{
		return UVoxelSolidifyMeshesToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UVoxelSolidifyMeshesToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVoxelSolidifyMeshesToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBaseCreateFromSelectedToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVoxelSolidifyMeshesToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "VoxelSolidifyMeshesTool.h" },
		{ "ModuleRelativePath", "Public/VoxelSolidifyMeshesTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVoxelSolidifyMeshesToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVoxelSolidifyMeshesToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVoxelSolidifyMeshesToolBuilder_Statics::ClassParams = {
		&UVoxelSolidifyMeshesToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UVoxelSolidifyMeshesToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelSolidifyMeshesToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVoxelSolidifyMeshesToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVoxelSolidifyMeshesToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVoxelSolidifyMeshesToolBuilder, 2522498801);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UVoxelSolidifyMeshesToolBuilder>()
	{
		return UVoxelSolidifyMeshesToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVoxelSolidifyMeshesToolBuilder(Z_Construct_UClass_UVoxelSolidifyMeshesToolBuilder, &UVoxelSolidifyMeshesToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UVoxelSolidifyMeshesToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVoxelSolidifyMeshesToolBuilder);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
