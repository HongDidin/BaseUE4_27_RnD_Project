// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MODELINGCOMPONENTS_BaseMeshProcessingTool_generated_h
#error "BaseMeshProcessingTool.generated.h already included, missing '#pragma once' in BaseMeshProcessingTool.h"
#endif
#define MODELINGCOMPONENTS_BaseMeshProcessingTool_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_28_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_28_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_28_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_28_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUBaseMeshProcessingToolBuilder(); \
	friend struct Z_Construct_UClass_UBaseMeshProcessingToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UBaseMeshProcessingToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UBaseMeshProcessingToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_28_INCLASS \
private: \
	static void StaticRegisterNativesUBaseMeshProcessingToolBuilder(); \
	friend struct Z_Construct_UClass_UBaseMeshProcessingToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UBaseMeshProcessingToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UBaseMeshProcessingToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_28_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBaseMeshProcessingToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBaseMeshProcessingToolBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBaseMeshProcessingToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBaseMeshProcessingToolBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBaseMeshProcessingToolBuilder(UBaseMeshProcessingToolBuilder&&); \
	NO_API UBaseMeshProcessingToolBuilder(const UBaseMeshProcessingToolBuilder&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_28_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBaseMeshProcessingToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBaseMeshProcessingToolBuilder(UBaseMeshProcessingToolBuilder&&); \
	NO_API UBaseMeshProcessingToolBuilder(const UBaseMeshProcessingToolBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBaseMeshProcessingToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBaseMeshProcessingToolBuilder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBaseMeshProcessingToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_28_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_25_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_28_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_28_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_28_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_28_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_28_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_28_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_28_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_28_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_28_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_28_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_28_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_28_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MODELINGCOMPONENTS_API UClass* StaticClass<class UBaseMeshProcessingToolBuilder>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_79_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_79_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_79_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_79_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUBaseMeshProcessingTool(); \
	friend struct Z_Construct_UClass_UBaseMeshProcessingTool_Statics; \
public: \
	DECLARE_CLASS(UBaseMeshProcessingTool, USingleSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UBaseMeshProcessingTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_79_INCLASS \
private: \
	static void StaticRegisterNativesUBaseMeshProcessingTool(); \
	friend struct Z_Construct_UClass_UBaseMeshProcessingTool_Statics; \
public: \
	DECLARE_CLASS(UBaseMeshProcessingTool, USingleSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UBaseMeshProcessingTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_79_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBaseMeshProcessingTool(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBaseMeshProcessingTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBaseMeshProcessingTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBaseMeshProcessingTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBaseMeshProcessingTool(UBaseMeshProcessingTool&&); \
	NO_API UBaseMeshProcessingTool(const UBaseMeshProcessingTool&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_79_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBaseMeshProcessingTool(UBaseMeshProcessingTool&&); \
	NO_API UBaseMeshProcessingTool(const UBaseMeshProcessingTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBaseMeshProcessingTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBaseMeshProcessingTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UBaseMeshProcessingTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_79_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Preview() { return STRUCT_OFFSET(UBaseMeshProcessingTool, Preview); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_76_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_79_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_79_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_79_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_79_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_79_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_79_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_79_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_79_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_79_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_79_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_79_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h_79_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MODELINGCOMPONENTS_API UClass* StaticClass<class UBaseMeshProcessingTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseMeshProcessingTool_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
