// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLS_MeshAnalysisProperties_generated_h
#error "MeshAnalysisProperties.generated.h already included, missing '#pragma once' in MeshAnalysisProperties.h"
#endif
#define MESHMODELINGTOOLS_MeshAnalysisProperties_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshAnalysisProperties_h_20_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshAnalysisProperties_h_20_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshAnalysisProperties_h_20_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshAnalysisProperties_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshAnalysisProperties(); \
	friend struct Z_Construct_UClass_UMeshAnalysisProperties_Statics; \
public: \
	DECLARE_CLASS(UMeshAnalysisProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMeshAnalysisProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshAnalysisProperties_h_20_INCLASS \
private: \
	static void StaticRegisterNativesUMeshAnalysisProperties(); \
	friend struct Z_Construct_UClass_UMeshAnalysisProperties_Statics; \
public: \
	DECLARE_CLASS(UMeshAnalysisProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMeshAnalysisProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshAnalysisProperties_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshAnalysisProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshAnalysisProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshAnalysisProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshAnalysisProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshAnalysisProperties(UMeshAnalysisProperties&&); \
	NO_API UMeshAnalysisProperties(const UMeshAnalysisProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshAnalysisProperties_h_20_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshAnalysisProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshAnalysisProperties(UMeshAnalysisProperties&&); \
	NO_API UMeshAnalysisProperties(const UMeshAnalysisProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshAnalysisProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshAnalysisProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshAnalysisProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshAnalysisProperties_h_20_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshAnalysisProperties_h_17_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshAnalysisProperties_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshAnalysisProperties_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshAnalysisProperties_h_20_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshAnalysisProperties_h_20_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshAnalysisProperties_h_20_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshAnalysisProperties_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshAnalysisProperties_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshAnalysisProperties_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshAnalysisProperties_h_20_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshAnalysisProperties_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshAnalysisProperties_h_20_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshAnalysisProperties_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UMeshAnalysisProperties>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshAnalysisProperties_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
