// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/MeshVertexSculptTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMeshVertexSculptTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EMeshVertexSculptBrushType();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshVertexSculptToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshVertexSculptToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UMeshSurfacePointToolBuilder();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UVertexBrushSculptProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UVertexBrushSculptProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EMeshSculptFalloffType();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshVertexSculptTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshVertexSculptTool();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshSculptToolBase();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_USimpleDynamicMeshComponent_NoRegister();
// End Cross Module References
	static UEnum* EMeshVertexSculptBrushType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EMeshVertexSculptBrushType, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EMeshVertexSculptBrushType"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EMeshVertexSculptBrushType>()
	{
		return EMeshVertexSculptBrushType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMeshVertexSculptBrushType(EMeshVertexSculptBrushType_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EMeshVertexSculptBrushType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EMeshVertexSculptBrushType_Hash() { return 2998402062U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EMeshVertexSculptBrushType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMeshVertexSculptBrushType"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EMeshVertexSculptBrushType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMeshVertexSculptBrushType::Move", (int64)EMeshVertexSculptBrushType::Move },
				{ "EMeshVertexSculptBrushType::PullKelvin", (int64)EMeshVertexSculptBrushType::PullKelvin },
				{ "EMeshVertexSculptBrushType::PullSharpKelvin", (int64)EMeshVertexSculptBrushType::PullSharpKelvin },
				{ "EMeshVertexSculptBrushType::Smooth", (int64)EMeshVertexSculptBrushType::Smooth },
				{ "EMeshVertexSculptBrushType::SmoothFill", (int64)EMeshVertexSculptBrushType::SmoothFill },
				{ "EMeshVertexSculptBrushType::Offset", (int64)EMeshVertexSculptBrushType::Offset },
				{ "EMeshVertexSculptBrushType::SculptView", (int64)EMeshVertexSculptBrushType::SculptView },
				{ "EMeshVertexSculptBrushType::SculptMax", (int64)EMeshVertexSculptBrushType::SculptMax },
				{ "EMeshVertexSculptBrushType::Inflate", (int64)EMeshVertexSculptBrushType::Inflate },
				{ "EMeshVertexSculptBrushType::ScaleKelvin", (int64)EMeshVertexSculptBrushType::ScaleKelvin },
				{ "EMeshVertexSculptBrushType::Pinch", (int64)EMeshVertexSculptBrushType::Pinch },
				{ "EMeshVertexSculptBrushType::TwistKelvin", (int64)EMeshVertexSculptBrushType::TwistKelvin },
				{ "EMeshVertexSculptBrushType::Flatten", (int64)EMeshVertexSculptBrushType::Flatten },
				{ "EMeshVertexSculptBrushType::Plane", (int64)EMeshVertexSculptBrushType::Plane },
				{ "EMeshVertexSculptBrushType::PlaneViewAligned", (int64)EMeshVertexSculptBrushType::PlaneViewAligned },
				{ "EMeshVertexSculptBrushType::FixedPlane", (int64)EMeshVertexSculptBrushType::FixedPlane },
				{ "EMeshVertexSculptBrushType::LastValue", (int64)EMeshVertexSculptBrushType::LastValue },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** Mesh Sculpting Brush Types */" },
				{ "FixedPlane.Comment", "/** Move vertices towards a fixed plane in world space, positioned with a 3D gizmo */" },
				{ "FixedPlane.DisplayName", "FixedPlane" },
				{ "FixedPlane.Name", "EMeshVertexSculptBrushType::FixedPlane" },
				{ "FixedPlane.ToolTip", "Move vertices towards a fixed plane in world space, positioned with a 3D gizmo" },
				{ "Flatten.Comment", "/** Move vertices towards the average plane of the brush stamp region */" },
				{ "Flatten.DisplayName", "Flatten" },
				{ "Flatten.Name", "EMeshVertexSculptBrushType::Flatten" },
				{ "Flatten.ToolTip", "Move vertices towards the average plane of the brush stamp region" },
				{ "Inflate.Comment", "/** Displace vertices along their vertex normals */" },
				{ "Inflate.DisplayName", "Inflate" },
				{ "Inflate.Name", "EMeshVertexSculptBrushType::Inflate" },
				{ "Inflate.ToolTip", "Displace vertices along their vertex normals" },
				{ "LastValue.Hidden", "" },
				{ "LastValue.Name", "EMeshVertexSculptBrushType::LastValue" },
				{ "ModuleRelativePath", "Public/MeshVertexSculptTool.h" },
				{ "Move.Comment", "/** Move vertices parallel to the view plane  */" },
				{ "Move.DisplayName", "Move" },
				{ "Move.Name", "EMeshVertexSculptBrushType::Move" },
				{ "Move.ToolTip", "Move vertices parallel to the view plane" },
				{ "Offset.Comment", "/** Displace vertices along the average surface normal (Ctrl to invert) */" },
				{ "Offset.DisplayName", "Sculpt (Normal)" },
				{ "Offset.Name", "EMeshVertexSculptBrushType::Offset" },
				{ "Offset.ToolTip", "Displace vertices along the average surface normal (Ctrl to invert)" },
				{ "Pinch.Comment", "/** Move vertices towards the center of the brush (Ctrl to push away)*/" },
				{ "Pinch.DisplayName", "Pinch" },
				{ "Pinch.Name", "EMeshVertexSculptBrushType::Pinch" },
				{ "Pinch.ToolTip", "Move vertices towards the center of the brush (Ctrl to push away)" },
				{ "Plane.Comment", "/** Move vertices towards a plane defined by the initial brush position  */" },
				{ "Plane.DisplayName", "Plane (Normal)" },
				{ "Plane.Name", "EMeshVertexSculptBrushType::Plane" },
				{ "Plane.ToolTip", "Move vertices towards a plane defined by the initial brush position" },
				{ "PlaneViewAligned.Comment", "/** Move vertices towards a view-facing plane defined at the initial brush position */" },
				{ "PlaneViewAligned.DisplayName", "Plane (Viewpoint)" },
				{ "PlaneViewAligned.Name", "EMeshVertexSculptBrushType::PlaneViewAligned" },
				{ "PlaneViewAligned.ToolTip", "Move vertices towards a view-facing plane defined at the initial brush position" },
				{ "PullKelvin.Comment", "/** Grab Brush, fall-off alters the influence of the grab */" },
				{ "PullKelvin.DisplayName", "Kelvin Grab" },
				{ "PullKelvin.Name", "EMeshVertexSculptBrushType::PullKelvin" },
				{ "PullKelvin.ToolTip", "Grab Brush, fall-off alters the influence of the grab" },
				{ "PullSharpKelvin.Comment", "/** Grab Brush that may generate cusps, fall-off alters the influence of the grab */" },
				{ "PullSharpKelvin.DisplayName", "Sharp Kelvin Grab" },
				{ "PullSharpKelvin.Name", "EMeshVertexSculptBrushType::PullSharpKelvin" },
				{ "PullSharpKelvin.ToolTip", "Grab Brush that may generate cusps, fall-off alters the influence of the grab" },
				{ "ScaleKelvin.Comment", "/** Scale Brush will inflate or pinch radially from the center of the brush */" },
				{ "ScaleKelvin.DisplayName", "Kelvin Scale" },
				{ "ScaleKelvin.Name", "EMeshVertexSculptBrushType::ScaleKelvin" },
				{ "ScaleKelvin.ToolTip", "Scale Brush will inflate or pinch radially from the center of the brush" },
				{ "SculptMax.Comment", "/** Displaces vertices along the average surface normal to a maximum height based on the brush size (Ctrl to invert) */" },
				{ "SculptMax.DisplayName", "Sculpt Max" },
				{ "SculptMax.Name", "EMeshVertexSculptBrushType::SculptMax" },
				{ "SculptMax.ToolTip", "Displaces vertices along the average surface normal to a maximum height based on the brush size (Ctrl to invert)" },
				{ "SculptView.Comment", "/** Displace vertices towards the camera viewpoint (Ctrl to invert) */" },
				{ "SculptView.DisplayName", "Sculpt (Viewpoint)" },
				{ "SculptView.Name", "EMeshVertexSculptBrushType::SculptView" },
				{ "SculptView.ToolTip", "Displace vertices towards the camera viewpoint (Ctrl to invert)" },
				{ "Smooth.Comment", "/** Smooth mesh vertices  */" },
				{ "Smooth.DisplayName", "Smooth" },
				{ "Smooth.Name", "EMeshVertexSculptBrushType::Smooth" },
				{ "Smooth.ToolTip", "Smooth mesh vertices" },
				{ "SmoothFill.Comment", "/** Smooth mesh vertices but only in direction of normal (Ctrl to invert) */" },
				{ "SmoothFill.DisplayName", "SmoothFill" },
				{ "SmoothFill.Name", "EMeshVertexSculptBrushType::SmoothFill" },
				{ "SmoothFill.ToolTip", "Smooth mesh vertices but only in direction of normal (Ctrl to invert)" },
				{ "ToolTip", "Mesh Sculpting Brush Types" },
				{ "TwistKelvin.Comment", "/** Twist Brush moves vertices in the plane perpendicular to the local mesh normal */" },
				{ "TwistKelvin.DisplayName", "Kelvin Twist" },
				{ "TwistKelvin.Name", "EMeshVertexSculptBrushType::TwistKelvin" },
				{ "TwistKelvin.ToolTip", "Twist Brush moves vertices in the plane perpendicular to the local mesh normal" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EMeshVertexSculptBrushType",
				"EMeshVertexSculptBrushType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UMeshVertexSculptToolBuilder::StaticRegisterNativesUMeshVertexSculptToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UMeshVertexSculptToolBuilder_NoRegister()
	{
		return UMeshVertexSculptToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UMeshVertexSculptToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshVertexSculptToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshSurfacePointToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshVertexSculptToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Tool Builder\n */" },
		{ "IncludePath", "MeshVertexSculptTool.h" },
		{ "ModuleRelativePath", "Public/MeshVertexSculptTool.h" },
		{ "ToolTip", "Tool Builder" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshVertexSculptToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshVertexSculptToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshVertexSculptToolBuilder_Statics::ClassParams = {
		&UMeshVertexSculptToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshVertexSculptToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshVertexSculptToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshVertexSculptToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshVertexSculptToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshVertexSculptToolBuilder, 1975229239);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UMeshVertexSculptToolBuilder>()
	{
		return UMeshVertexSculptToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshVertexSculptToolBuilder(Z_Construct_UClass_UMeshVertexSculptToolBuilder, &UMeshVertexSculptToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UMeshVertexSculptToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshVertexSculptToolBuilder);
	void UVertexBrushSculptProperties::StaticRegisterNativesUVertexBrushSculptProperties()
	{
	}
	UClass* Z_Construct_UClass_UVertexBrushSculptProperties_NoRegister()
	{
		return UVertexBrushSculptProperties::StaticClass();
	}
	struct Z_Construct_UClass_UVertexBrushSculptProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_PrimaryBrushType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PrimaryBrushType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_PrimaryBrushType;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_PrimaryFalloffType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PrimaryFalloffType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_PrimaryFalloffType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bFreezeTarget_MetaData[];
#endif
		static void NewProp_bFreezeTarget_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFreezeTarget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSmoothErases_MetaData[];
#endif
		static void NewProp_bSmoothErases_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSmoothErases;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVertexBrushSculptProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVertexBrushSculptProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshVertexSculptTool.h" },
		{ "ModuleRelativePath", "Public/MeshVertexSculptTool.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UVertexBrushSculptProperties_Statics::NewProp_PrimaryBrushType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVertexBrushSculptProperties_Statics::NewProp_PrimaryBrushType_MetaData[] = {
		{ "Category", "Sculpting" },
		{ "Comment", "/** Primary Brush Mode */" },
		{ "DisplayName", "Brush Type" },
		{ "ModuleRelativePath", "Public/MeshVertexSculptTool.h" },
		{ "ToolTip", "Primary Brush Mode" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UVertexBrushSculptProperties_Statics::NewProp_PrimaryBrushType = { "PrimaryBrushType", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVertexBrushSculptProperties, PrimaryBrushType), Z_Construct_UEnum_MeshModelingTools_EMeshVertexSculptBrushType, METADATA_PARAMS(Z_Construct_UClass_UVertexBrushSculptProperties_Statics::NewProp_PrimaryBrushType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVertexBrushSculptProperties_Statics::NewProp_PrimaryBrushType_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UVertexBrushSculptProperties_Statics::NewProp_PrimaryFalloffType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVertexBrushSculptProperties_Statics::NewProp_PrimaryFalloffType_MetaData[] = {
		{ "Category", "Sculpting" },
		{ "Comment", "/** Primary Brush Falloff */" },
		{ "DisplayName", "Falloff Shape" },
		{ "ModuleRelativePath", "Public/MeshVertexSculptTool.h" },
		{ "ToolTip", "Primary Brush Falloff" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UVertexBrushSculptProperties_Statics::NewProp_PrimaryFalloffType = { "PrimaryFalloffType", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVertexBrushSculptProperties, PrimaryFalloffType), Z_Construct_UEnum_MeshModelingTools_EMeshSculptFalloffType, METADATA_PARAMS(Z_Construct_UClass_UVertexBrushSculptProperties_Statics::NewProp_PrimaryFalloffType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVertexBrushSculptProperties_Statics::NewProp_PrimaryFalloffType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVertexBrushSculptProperties_Statics::NewProp_bFreezeTarget_MetaData[] = {
		{ "Category", "Sculpting" },
		{ "Comment", "/** When Freeze Target is toggled on, the Brush Target Surface will be Frozen in its current state, until toggled off. Brush strokes will be applied relative to the Target Surface, for applicable Brushes */" },
		{ "EditCondition", "PrimaryBrushType == EMeshVertexSculptBrushType::Offset || PrimaryBrushType == EMeshVertexSculptBrushType::SculptMax || PrimaryBrushType == EMeshVertexSculptBrushType::SculptView || PrimaryBrushType == EMeshVertexSculptBrushType::Pinch || PrimaryBrushType == EMeshVertexSculptBrushType::Resample" },
		{ "ModuleRelativePath", "Public/MeshVertexSculptTool.h" },
		{ "ToolTip", "When Freeze Target is toggled on, the Brush Target Surface will be Frozen in its current state, until toggled off. Brush strokes will be applied relative to the Target Surface, for applicable Brushes" },
	};
#endif
	void Z_Construct_UClass_UVertexBrushSculptProperties_Statics::NewProp_bFreezeTarget_SetBit(void* Obj)
	{
		((UVertexBrushSculptProperties*)Obj)->bFreezeTarget = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVertexBrushSculptProperties_Statics::NewProp_bFreezeTarget = { "bFreezeTarget", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVertexBrushSculptProperties), &Z_Construct_UClass_UVertexBrushSculptProperties_Statics::NewProp_bFreezeTarget_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVertexBrushSculptProperties_Statics::NewProp_bFreezeTarget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVertexBrushSculptProperties_Statics::NewProp_bFreezeTarget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVertexBrushSculptProperties_Statics::NewProp_bSmoothErases_MetaData[] = {
		{ "Comment", "/** When enabled, instead of Mesh Smoothing, the Shift-Smooth modifier will \"erase\" the displacement relative to the Brush Target Surface *///UPROPERTY(EditAnywhere, Category = Sculpting, meta = (DisplayName = \"Shift-Smooth Erases\", EditCondition = \"bFreezeTarget == true\"))\n" },
		{ "ModuleRelativePath", "Public/MeshVertexSculptTool.h" },
		{ "ToolTip", "When enabled, instead of Mesh Smoothing, the Shift-Smooth modifier will \"erase\" the displacement relative to the Brush Target Surface //UPROPERTY(EditAnywhere, Category = Sculpting, meta = (DisplayName = \"Shift-Smooth Erases\", EditCondition = \"bFreezeTarget == true\"))" },
	};
#endif
	void Z_Construct_UClass_UVertexBrushSculptProperties_Statics::NewProp_bSmoothErases_SetBit(void* Obj)
	{
		((UVertexBrushSculptProperties*)Obj)->bSmoothErases = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVertexBrushSculptProperties_Statics::NewProp_bSmoothErases = { "bSmoothErases", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVertexBrushSculptProperties), &Z_Construct_UClass_UVertexBrushSculptProperties_Statics::NewProp_bSmoothErases_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVertexBrushSculptProperties_Statics::NewProp_bSmoothErases_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVertexBrushSculptProperties_Statics::NewProp_bSmoothErases_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UVertexBrushSculptProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVertexBrushSculptProperties_Statics::NewProp_PrimaryBrushType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVertexBrushSculptProperties_Statics::NewProp_PrimaryBrushType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVertexBrushSculptProperties_Statics::NewProp_PrimaryFalloffType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVertexBrushSculptProperties_Statics::NewProp_PrimaryFalloffType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVertexBrushSculptProperties_Statics::NewProp_bFreezeTarget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVertexBrushSculptProperties_Statics::NewProp_bSmoothErases,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVertexBrushSculptProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVertexBrushSculptProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVertexBrushSculptProperties_Statics::ClassParams = {
		&UVertexBrushSculptProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UVertexBrushSculptProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UVertexBrushSculptProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UVertexBrushSculptProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVertexBrushSculptProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVertexBrushSculptProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVertexBrushSculptProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVertexBrushSculptProperties, 717024664);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UVertexBrushSculptProperties>()
	{
		return UVertexBrushSculptProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVertexBrushSculptProperties(Z_Construct_UClass_UVertexBrushSculptProperties, &UVertexBrushSculptProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UVertexBrushSculptProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVertexBrushSculptProperties);
	void UMeshVertexSculptTool::StaticRegisterNativesUMeshVertexSculptTool()
	{
	}
	UClass* Z_Construct_UClass_UMeshVertexSculptTool_NoRegister()
	{
		return UMeshVertexSculptTool::StaticClass();
	}
	struct Z_Construct_UClass_UMeshVertexSculptTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SculptProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SculptProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DynamicMeshComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DynamicMeshComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshVertexSculptTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshSculptToolBase,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshVertexSculptTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Mesh Vertex Sculpt Tool Class\n */" },
		{ "IncludePath", "MeshVertexSculptTool.h" },
		{ "ModuleRelativePath", "Public/MeshVertexSculptTool.h" },
		{ "ToolTip", "Mesh Vertex Sculpt Tool Class" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshVertexSculptTool_Statics::NewProp_SculptProperties_MetaData[] = {
		{ "Comment", "/** Properties that control sculpting*/" },
		{ "ModuleRelativePath", "Public/MeshVertexSculptTool.h" },
		{ "ToolTip", "Properties that control sculpting" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshVertexSculptTool_Statics::NewProp_SculptProperties = { "SculptProperties", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshVertexSculptTool, SculptProperties), Z_Construct_UClass_UVertexBrushSculptProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshVertexSculptTool_Statics::NewProp_SculptProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshVertexSculptTool_Statics::NewProp_SculptProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshVertexSculptTool_Statics::NewProp_DynamicMeshComponent_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/MeshVertexSculptTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshVertexSculptTool_Statics::NewProp_DynamicMeshComponent = { "DynamicMeshComponent", nullptr, (EPropertyFlags)0x0020080000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshVertexSculptTool, DynamicMeshComponent), Z_Construct_UClass_USimpleDynamicMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshVertexSculptTool_Statics::NewProp_DynamicMeshComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshVertexSculptTool_Statics::NewProp_DynamicMeshComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshVertexSculptTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshVertexSculptTool_Statics::NewProp_SculptProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshVertexSculptTool_Statics::NewProp_DynamicMeshComponent,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshVertexSculptTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshVertexSculptTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshVertexSculptTool_Statics::ClassParams = {
		&UMeshVertexSculptTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshVertexSculptTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshVertexSculptTool_Statics::PropPointers),
		0,
		0x009000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshVertexSculptTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshVertexSculptTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshVertexSculptTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshVertexSculptTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshVertexSculptTool, 899272766);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UMeshVertexSculptTool>()
	{
		return UMeshVertexSculptTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshVertexSculptTool(Z_Construct_UClass_UMeshVertexSculptTool, &UMeshVertexSculptTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UMeshVertexSculptTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshVertexSculptTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
