// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/PositionPlaneGizmo.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePositionPlaneGizmo() {}
// Cross Module References
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPositionPlaneGizmoBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPositionPlaneGizmoBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveGizmoBuilder();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPositionPlaneGizmo_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPositionPlaneGizmo();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveGizmo();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPreviewMesh_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstance_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPositionPlaneOnSceneInputBehavior_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPositionPlaneOnSceneInputBehavior();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UAnyButtonInputBehavior();
// End Cross Module References
	void UPositionPlaneGizmoBuilder::StaticRegisterNativesUPositionPlaneGizmoBuilder()
	{
	}
	UClass* Z_Construct_UClass_UPositionPlaneGizmoBuilder_NoRegister()
	{
		return UPositionPlaneGizmoBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UPositionPlaneGizmoBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPositionPlaneGizmoBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveGizmoBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPositionPlaneGizmoBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "PositionPlaneGizmo.h" },
		{ "ModuleRelativePath", "Public/PositionPlaneGizmo.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPositionPlaneGizmoBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPositionPlaneGizmoBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPositionPlaneGizmoBuilder_Statics::ClassParams = {
		&UPositionPlaneGizmoBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UPositionPlaneGizmoBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPositionPlaneGizmoBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPositionPlaneGizmoBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPositionPlaneGizmoBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPositionPlaneGizmoBuilder, 2498396369);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UPositionPlaneGizmoBuilder>()
	{
		return UPositionPlaneGizmoBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPositionPlaneGizmoBuilder(Z_Construct_UClass_UPositionPlaneGizmoBuilder, &UPositionPlaneGizmoBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UPositionPlaneGizmoBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPositionPlaneGizmoBuilder);
	void UPositionPlaneGizmo::StaticRegisterNativesUPositionPlaneGizmo()
	{
	}
	UClass* Z_Construct_UClass_UPositionPlaneGizmo_NoRegister()
	{
		return UPositionPlaneGizmo::StaticClass();
	}
	struct Z_Construct_UClass_UPositionPlaneGizmo_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CenterBallShape_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CenterBallShape;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CenterBallMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CenterBallMaterial;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPositionPlaneGizmo_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveGizmo,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPositionPlaneGizmo_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * This is a simple gizmo you can use to position a 3D plane in the world, based on QuickAxisTransformer\n */" },
		{ "IncludePath", "PositionPlaneGizmo.h" },
		{ "ModuleRelativePath", "Public/PositionPlaneGizmo.h" },
		{ "ToolTip", "This is a simple gizmo you can use to position a 3D plane in the world, based on QuickAxisTransformer" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPositionPlaneGizmo_Statics::NewProp_CenterBallShape_MetaData[] = {
		{ "ModuleRelativePath", "Public/PositionPlaneGizmo.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPositionPlaneGizmo_Statics::NewProp_CenterBallShape = { "CenterBallShape", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPositionPlaneGizmo, CenterBallShape), Z_Construct_UClass_UPreviewMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPositionPlaneGizmo_Statics::NewProp_CenterBallShape_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPositionPlaneGizmo_Statics::NewProp_CenterBallShape_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPositionPlaneGizmo_Statics::NewProp_CenterBallMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Public/PositionPlaneGizmo.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPositionPlaneGizmo_Statics::NewProp_CenterBallMaterial = { "CenterBallMaterial", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPositionPlaneGizmo, CenterBallMaterial), Z_Construct_UClass_UMaterialInstance_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPositionPlaneGizmo_Statics::NewProp_CenterBallMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPositionPlaneGizmo_Statics::NewProp_CenterBallMaterial_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPositionPlaneGizmo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPositionPlaneGizmo_Statics::NewProp_CenterBallShape,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPositionPlaneGizmo_Statics::NewProp_CenterBallMaterial,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPositionPlaneGizmo_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPositionPlaneGizmo>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPositionPlaneGizmo_Statics::ClassParams = {
		&UPositionPlaneGizmo::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UPositionPlaneGizmo_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UPositionPlaneGizmo_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UPositionPlaneGizmo_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPositionPlaneGizmo_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPositionPlaneGizmo()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPositionPlaneGizmo_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPositionPlaneGizmo, 2815242811);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UPositionPlaneGizmo>()
	{
		return UPositionPlaneGizmo::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPositionPlaneGizmo(Z_Construct_UClass_UPositionPlaneGizmo, &UPositionPlaneGizmo::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UPositionPlaneGizmo"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPositionPlaneGizmo);
	void UPositionPlaneOnSceneInputBehavior::StaticRegisterNativesUPositionPlaneOnSceneInputBehavior()
	{
	}
	UClass* Z_Construct_UClass_UPositionPlaneOnSceneInputBehavior_NoRegister()
	{
		return UPositionPlaneOnSceneInputBehavior::StaticClass();
	}
	struct Z_Construct_UClass_UPositionPlaneOnSceneInputBehavior_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPositionPlaneOnSceneInputBehavior_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAnyButtonInputBehavior,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPositionPlaneOnSceneInputBehavior_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * UMeshSurfacePointToolMouseBehavior implements mouse press-drag-release interaction behavior for Mouse devices.\n * You can configure the base UAnyButtonInputBehavior to change the mouse button in use (default = left mouse)\n */" },
		{ "IncludePath", "PositionPlaneGizmo.h" },
		{ "ModuleRelativePath", "Public/PositionPlaneGizmo.h" },
		{ "ToolTip", "UMeshSurfacePointToolMouseBehavior implements mouse press-drag-release interaction behavior for Mouse devices.\nYou can configure the base UAnyButtonInputBehavior to change the mouse button in use (default = left mouse)" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPositionPlaneOnSceneInputBehavior_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPositionPlaneOnSceneInputBehavior>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPositionPlaneOnSceneInputBehavior_Statics::ClassParams = {
		&UPositionPlaneOnSceneInputBehavior::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UPositionPlaneOnSceneInputBehavior_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPositionPlaneOnSceneInputBehavior_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPositionPlaneOnSceneInputBehavior()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPositionPlaneOnSceneInputBehavior_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPositionPlaneOnSceneInputBehavior, 2796165585);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UPositionPlaneOnSceneInputBehavior>()
	{
		return UPositionPlaneOnSceneInputBehavior::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPositionPlaneOnSceneInputBehavior(Z_Construct_UClass_UPositionPlaneOnSceneInputBehavior, &UPositionPlaneOnSceneInputBehavior::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UPositionPlaneOnSceneInputBehavior"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPositionPlaneOnSceneInputBehavior);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
