// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLS_AlignObjectsTool_generated_h
#error "AlignObjectsTool.generated.h already included, missing '#pragma once' in AlignObjectsTool.h"
#endif
#define MESHMODELINGTOOLS_AlignObjectsTool_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_22_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_22_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_22_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAlignObjectsToolBuilder(); \
	friend struct Z_Construct_UClass_UAlignObjectsToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UAlignObjectsToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UAlignObjectsToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_22_INCLASS \
private: \
	static void StaticRegisterNativesUAlignObjectsToolBuilder(); \
	friend struct Z_Construct_UClass_UAlignObjectsToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UAlignObjectsToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UAlignObjectsToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAlignObjectsToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAlignObjectsToolBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAlignObjectsToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAlignObjectsToolBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAlignObjectsToolBuilder(UAlignObjectsToolBuilder&&); \
	NO_API UAlignObjectsToolBuilder(const UAlignObjectsToolBuilder&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_22_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAlignObjectsToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAlignObjectsToolBuilder(UAlignObjectsToolBuilder&&); \
	NO_API UAlignObjectsToolBuilder(const UAlignObjectsToolBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAlignObjectsToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAlignObjectsToolBuilder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAlignObjectsToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_22_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_19_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_22_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_22_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_22_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_22_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_22_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_22_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_22_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_22_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UAlignObjectsToolBuilder>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_67_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_67_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_67_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_67_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAlignObjectsToolProperties(); \
	friend struct Z_Construct_UClass_UAlignObjectsToolProperties_Statics; \
public: \
	DECLARE_CLASS(UAlignObjectsToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UAlignObjectsToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_67_INCLASS \
private: \
	static void StaticRegisterNativesUAlignObjectsToolProperties(); \
	friend struct Z_Construct_UClass_UAlignObjectsToolProperties_Statics; \
public: \
	DECLARE_CLASS(UAlignObjectsToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UAlignObjectsToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_67_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAlignObjectsToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAlignObjectsToolProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAlignObjectsToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAlignObjectsToolProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAlignObjectsToolProperties(UAlignObjectsToolProperties&&); \
	NO_API UAlignObjectsToolProperties(const UAlignObjectsToolProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_67_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAlignObjectsToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAlignObjectsToolProperties(UAlignObjectsToolProperties&&); \
	NO_API UAlignObjectsToolProperties(const UAlignObjectsToolProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAlignObjectsToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAlignObjectsToolProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAlignObjectsToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_67_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_64_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_67_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_67_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_67_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_67_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_67_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_67_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_67_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_67_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_67_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_67_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_67_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_67_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UAlignObjectsToolProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_101_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_101_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_101_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_101_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAlignObjectsTool(); \
	friend struct Z_Construct_UClass_UAlignObjectsTool_Statics; \
public: \
	DECLARE_CLASS(UAlignObjectsTool, UMultiSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UAlignObjectsTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_101_INCLASS \
private: \
	static void StaticRegisterNativesUAlignObjectsTool(); \
	friend struct Z_Construct_UClass_UAlignObjectsTool_Statics; \
public: \
	DECLARE_CLASS(UAlignObjectsTool, UMultiSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UAlignObjectsTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_101_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAlignObjectsTool(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAlignObjectsTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAlignObjectsTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAlignObjectsTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAlignObjectsTool(UAlignObjectsTool&&); \
	NO_API UAlignObjectsTool(const UAlignObjectsTool&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_101_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAlignObjectsTool(UAlignObjectsTool&&); \
	NO_API UAlignObjectsTool(const UAlignObjectsTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAlignObjectsTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAlignObjectsTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UAlignObjectsTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_101_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_98_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_101_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_101_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_101_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_101_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_101_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_101_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_101_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_101_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_101_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_101_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_101_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h_101_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UAlignObjectsTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AlignObjectsTool_h


#define FOREACH_ENUM_EALIGNOBJECTSBOXPOINT(op) \
	op(EAlignObjectsBoxPoint::Center) \
	op(EAlignObjectsBoxPoint::Bottom) \
	op(EAlignObjectsBoxPoint::Top) \
	op(EAlignObjectsBoxPoint::Left) \
	op(EAlignObjectsBoxPoint::Right) \
	op(EAlignObjectsBoxPoint::Front) \
	op(EAlignObjectsBoxPoint::Back) \
	op(EAlignObjectsBoxPoint::Min) \
	op(EAlignObjectsBoxPoint::Max) 

enum class EAlignObjectsBoxPoint;
template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EAlignObjectsBoxPoint>();

#define FOREACH_ENUM_EALIGNOBJECTSALIGNTOOPTIONS(op) \
	op(EAlignObjectsAlignToOptions::FirstSelected) \
	op(EAlignObjectsAlignToOptions::LastSelected) \
	op(EAlignObjectsAlignToOptions::Combined) 

enum class EAlignObjectsAlignToOptions;
template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EAlignObjectsAlignToOptions>();

#define FOREACH_ENUM_EALIGNOBJECTSALIGNTYPES(op) \
	op(EAlignObjectsAlignTypes::Pivots) \
	op(EAlignObjectsAlignTypes::BoundingBoxes) 

enum class EAlignObjectsAlignTypes;
template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EAlignObjectsAlignTypes>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
