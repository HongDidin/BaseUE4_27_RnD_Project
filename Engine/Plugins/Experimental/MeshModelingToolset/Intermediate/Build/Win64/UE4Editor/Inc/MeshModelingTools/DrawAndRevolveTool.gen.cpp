// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/DrawAndRevolveTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDrawAndRevolveTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDrawAndRevolveToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDrawAndRevolveToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_URevolveToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_URevolveToolProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_URevolveProperties();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_URevolveOperatorFactory_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_URevolveOperatorFactory();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDrawAndRevolveTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDrawAndRevolveTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveTool();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UCurveControlPointsMechanic_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UConstructionPlaneMechanic_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UNewMeshMaterialProperties_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_NoRegister();
// End Cross Module References
	void UDrawAndRevolveToolBuilder::StaticRegisterNativesUDrawAndRevolveToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UDrawAndRevolveToolBuilder_NoRegister()
	{
		return UDrawAndRevolveToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UDrawAndRevolveToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDrawAndRevolveToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawAndRevolveToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "DrawAndRevolveTool.h" },
		{ "ModuleRelativePath", "Public/DrawAndRevolveTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDrawAndRevolveToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDrawAndRevolveToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDrawAndRevolveToolBuilder_Statics::ClassParams = {
		&UDrawAndRevolveToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UDrawAndRevolveToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawAndRevolveToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDrawAndRevolveToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDrawAndRevolveToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDrawAndRevolveToolBuilder, 3726949573);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UDrawAndRevolveToolBuilder>()
	{
		return UDrawAndRevolveToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDrawAndRevolveToolBuilder(Z_Construct_UClass_UDrawAndRevolveToolBuilder, &UDrawAndRevolveToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UDrawAndRevolveToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDrawAndRevolveToolBuilder);
	void URevolveToolProperties::StaticRegisterNativesURevolveToolProperties()
	{
	}
	UClass* Z_Construct_UClass_URevolveToolProperties_NoRegister()
	{
		return URevolveToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_URevolveToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bConnectOpenProfileToAxis_MetaData[];
#endif
		static void NewProp_bConnectOpenProfileToAxis_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bConnectOpenProfileToAxis;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSnapToWorldGrid_MetaData[];
#endif
		static void NewProp_bSnapToWorldGrid_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSnapToWorldGrid;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DrawPlaneOrigin_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DrawPlaneOrigin;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DrawPlaneOrientation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DrawPlaneOrientation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableSnapping_MetaData[];
#endif
		static void NewProp_bEnableSnapping_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableSnapping;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAllowedToEditDrawPlane_MetaData[];
#endif
		static void NewProp_bAllowedToEditDrawPlane_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAllowedToEditDrawPlane;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URevolveToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_URevolveProperties,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URevolveToolProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "DrawAndRevolveTool.h" },
		{ "ModuleRelativePath", "Public/DrawAndRevolveTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URevolveToolProperties_Statics::NewProp_bConnectOpenProfileToAxis_MetaData[] = {
		{ "Category", "RevolveSettings" },
		{ "Comment", "/** Connect the ends of an open profile to the axis to close the top and bottom of the revolved result. Not relevant if profile curve is closed. */" },
		{ "ModuleRelativePath", "Public/DrawAndRevolveTool.h" },
		{ "ToolTip", "Connect the ends of an open profile to the axis to close the top and bottom of the revolved result. Not relevant if profile curve is closed." },
	};
#endif
	void Z_Construct_UClass_URevolveToolProperties_Statics::NewProp_bConnectOpenProfileToAxis_SetBit(void* Obj)
	{
		((URevolveToolProperties*)Obj)->bConnectOpenProfileToAxis = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URevolveToolProperties_Statics::NewProp_bConnectOpenProfileToAxis = { "bConnectOpenProfileToAxis", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URevolveToolProperties), &Z_Construct_UClass_URevolveToolProperties_Statics::NewProp_bConnectOpenProfileToAxis_SetBit, METADATA_PARAMS(Z_Construct_UClass_URevolveToolProperties_Statics::NewProp_bConnectOpenProfileToAxis_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URevolveToolProperties_Statics::NewProp_bConnectOpenProfileToAxis_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URevolveToolProperties_Statics::NewProp_bSnapToWorldGrid_MetaData[] = {
		{ "Category", "DrawPlane" },
		{ "Comment", "/** Determines whether plane control widget snaps to world grid (only relevant if world coordinate mode is active in viewport) .*/" },
		{ "ModuleRelativePath", "Public/DrawAndRevolveTool.h" },
		{ "ToolTip", "Determines whether plane control widget snaps to world grid (only relevant if world coordinate mode is active in viewport) ." },
	};
#endif
	void Z_Construct_UClass_URevolveToolProperties_Statics::NewProp_bSnapToWorldGrid_SetBit(void* Obj)
	{
		((URevolveToolProperties*)Obj)->bSnapToWorldGrid = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URevolveToolProperties_Statics::NewProp_bSnapToWorldGrid = { "bSnapToWorldGrid", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URevolveToolProperties), &Z_Construct_UClass_URevolveToolProperties_Statics::NewProp_bSnapToWorldGrid_SetBit, METADATA_PARAMS(Z_Construct_UClass_URevolveToolProperties_Statics::NewProp_bSnapToWorldGrid_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URevolveToolProperties_Statics::NewProp_bSnapToWorldGrid_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URevolveToolProperties_Statics::NewProp_DrawPlaneOrigin_MetaData[] = {
		{ "Category", "DrawPlane" },
		{ "Comment", "/** Sets the draw plane origin. The revolution axis is the X axis in the plane. */" },
		{ "EditCondition", "bAllowedToEditDrawPlane" },
		{ "HideEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/DrawAndRevolveTool.h" },
		{ "ToolTip", "Sets the draw plane origin. The revolution axis is the X axis in the plane." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URevolveToolProperties_Statics::NewProp_DrawPlaneOrigin = { "DrawPlaneOrigin", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URevolveToolProperties, DrawPlaneOrigin), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_URevolveToolProperties_Statics::NewProp_DrawPlaneOrigin_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URevolveToolProperties_Statics::NewProp_DrawPlaneOrigin_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URevolveToolProperties_Statics::NewProp_DrawPlaneOrientation_MetaData[] = {
		{ "Category", "DrawPlane" },
		{ "ClampMax", "18000" },
		{ "ClampMin", "-180000" },
		{ "Comment", "/** Sets the draw plane orientation. The revolution axis is the X axis in the plane. */" },
		{ "EditCondition", "bAllowedToEditDrawPlane" },
		{ "HideEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/DrawAndRevolveTool.h" },
		{ "ToolTip", "Sets the draw plane orientation. The revolution axis is the X axis in the plane." },
		{ "UIMax", "180" },
		{ "UIMin", "-180" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URevolveToolProperties_Statics::NewProp_DrawPlaneOrientation = { "DrawPlaneOrientation", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URevolveToolProperties, DrawPlaneOrientation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UClass_URevolveToolProperties_Statics::NewProp_DrawPlaneOrientation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URevolveToolProperties_Statics::NewProp_DrawPlaneOrientation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URevolveToolProperties_Statics::NewProp_bEnableSnapping_MetaData[] = {
		{ "Category", "ProfileCurve" },
		{ "Comment", "/** Enables/disables snapping while editing the profile curve. */" },
		{ "ModuleRelativePath", "Public/DrawAndRevolveTool.h" },
		{ "ToolTip", "Enables/disables snapping while editing the profile curve." },
	};
#endif
	void Z_Construct_UClass_URevolveToolProperties_Statics::NewProp_bEnableSnapping_SetBit(void* Obj)
	{
		((URevolveToolProperties*)Obj)->bEnableSnapping = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URevolveToolProperties_Statics::NewProp_bEnableSnapping = { "bEnableSnapping", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URevolveToolProperties), &Z_Construct_UClass_URevolveToolProperties_Statics::NewProp_bEnableSnapping_SetBit, METADATA_PARAMS(Z_Construct_UClass_URevolveToolProperties_Statics::NewProp_bEnableSnapping_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URevolveToolProperties_Statics::NewProp_bEnableSnapping_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URevolveToolProperties_Statics::NewProp_bAllowedToEditDrawPlane_MetaData[] = {
		{ "Comment", "// Not user visible- used to disallow draw plane modification.\n" },
		{ "ModuleRelativePath", "Public/DrawAndRevolveTool.h" },
		{ "ToolTip", "Not user visible- used to disallow draw plane modification." },
		{ "TransientToolProperty", "" },
	};
#endif
	void Z_Construct_UClass_URevolveToolProperties_Statics::NewProp_bAllowedToEditDrawPlane_SetBit(void* Obj)
	{
		((URevolveToolProperties*)Obj)->bAllowedToEditDrawPlane = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URevolveToolProperties_Statics::NewProp_bAllowedToEditDrawPlane = { "bAllowedToEditDrawPlane", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URevolveToolProperties), &Z_Construct_UClass_URevolveToolProperties_Statics::NewProp_bAllowedToEditDrawPlane_SetBit, METADATA_PARAMS(Z_Construct_UClass_URevolveToolProperties_Statics::NewProp_bAllowedToEditDrawPlane_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URevolveToolProperties_Statics::NewProp_bAllowedToEditDrawPlane_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URevolveToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URevolveToolProperties_Statics::NewProp_bConnectOpenProfileToAxis,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URevolveToolProperties_Statics::NewProp_bSnapToWorldGrid,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URevolveToolProperties_Statics::NewProp_DrawPlaneOrigin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URevolveToolProperties_Statics::NewProp_DrawPlaneOrientation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URevolveToolProperties_Statics::NewProp_bEnableSnapping,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URevolveToolProperties_Statics::NewProp_bAllowedToEditDrawPlane,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URevolveToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URevolveToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URevolveToolProperties_Statics::ClassParams = {
		&URevolveToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URevolveToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URevolveToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_URevolveToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URevolveToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URevolveToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URevolveToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URevolveToolProperties, 1648092401);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<URevolveToolProperties>()
	{
		return URevolveToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URevolveToolProperties(Z_Construct_UClass_URevolveToolProperties, &URevolveToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("URevolveToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URevolveToolProperties);
	void URevolveOperatorFactory::StaticRegisterNativesURevolveOperatorFactory()
	{
	}
	UClass* Z_Construct_UClass_URevolveOperatorFactory_NoRegister()
	{
		return URevolveOperatorFactory::StaticClass();
	}
	struct Z_Construct_UClass_URevolveOperatorFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RevolveTool_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RevolveTool;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URevolveOperatorFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URevolveOperatorFactory_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "DrawAndRevolveTool.h" },
		{ "ModuleRelativePath", "Public/DrawAndRevolveTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URevolveOperatorFactory_Statics::NewProp_RevolveTool_MetaData[] = {
		{ "ModuleRelativePath", "Public/DrawAndRevolveTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_URevolveOperatorFactory_Statics::NewProp_RevolveTool = { "RevolveTool", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URevolveOperatorFactory, RevolveTool), Z_Construct_UClass_UDrawAndRevolveTool_NoRegister, METADATA_PARAMS(Z_Construct_UClass_URevolveOperatorFactory_Statics::NewProp_RevolveTool_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URevolveOperatorFactory_Statics::NewProp_RevolveTool_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URevolveOperatorFactory_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URevolveOperatorFactory_Statics::NewProp_RevolveTool,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URevolveOperatorFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URevolveOperatorFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URevolveOperatorFactory_Statics::ClassParams = {
		&URevolveOperatorFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URevolveOperatorFactory_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URevolveOperatorFactory_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_URevolveOperatorFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URevolveOperatorFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URevolveOperatorFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URevolveOperatorFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URevolveOperatorFactory, 2872810059);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<URevolveOperatorFactory>()
	{
		return URevolveOperatorFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URevolveOperatorFactory(Z_Construct_UClass_URevolveOperatorFactory, &URevolveOperatorFactory::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("URevolveOperatorFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URevolveOperatorFactory);
	void UDrawAndRevolveTool::StaticRegisterNativesUDrawAndRevolveTool()
	{
	}
	UClass* Z_Construct_UClass_UDrawAndRevolveTool_NoRegister()
	{
		return UDrawAndRevolveTool::StaticClass();
	}
	struct Z_Construct_UClass_UDrawAndRevolveTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ControlPointsMechanic_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ControlPointsMechanic;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlaneMechanic_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PlaneMechanic;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Settings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Settings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaterialProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MaterialProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Preview_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Preview;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDrawAndRevolveTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawAndRevolveTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Draws a profile curve and revolves it around an axis. */" },
		{ "IncludePath", "DrawAndRevolveTool.h" },
		{ "ModuleRelativePath", "Public/DrawAndRevolveTool.h" },
		{ "ToolTip", "Draws a profile curve and revolves it around an axis." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawAndRevolveTool_Statics::NewProp_ControlPointsMechanic_MetaData[] = {
		{ "ModuleRelativePath", "Public/DrawAndRevolveTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDrawAndRevolveTool_Statics::NewProp_ControlPointsMechanic = { "ControlPointsMechanic", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawAndRevolveTool, ControlPointsMechanic), Z_Construct_UClass_UCurveControlPointsMechanic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDrawAndRevolveTool_Statics::NewProp_ControlPointsMechanic_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawAndRevolveTool_Statics::NewProp_ControlPointsMechanic_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawAndRevolveTool_Statics::NewProp_PlaneMechanic_MetaData[] = {
		{ "ModuleRelativePath", "Public/DrawAndRevolveTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDrawAndRevolveTool_Statics::NewProp_PlaneMechanic = { "PlaneMechanic", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawAndRevolveTool, PlaneMechanic), Z_Construct_UClass_UConstructionPlaneMechanic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDrawAndRevolveTool_Statics::NewProp_PlaneMechanic_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawAndRevolveTool_Statics::NewProp_PlaneMechanic_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawAndRevolveTool_Statics::NewProp_Settings_MetaData[] = {
		{ "ModuleRelativePath", "Public/DrawAndRevolveTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDrawAndRevolveTool_Statics::NewProp_Settings = { "Settings", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawAndRevolveTool, Settings), Z_Construct_UClass_URevolveToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDrawAndRevolveTool_Statics::NewProp_Settings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawAndRevolveTool_Statics::NewProp_Settings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawAndRevolveTool_Statics::NewProp_MaterialProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/DrawAndRevolveTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDrawAndRevolveTool_Statics::NewProp_MaterialProperties = { "MaterialProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawAndRevolveTool, MaterialProperties), Z_Construct_UClass_UNewMeshMaterialProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDrawAndRevolveTool_Statics::NewProp_MaterialProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawAndRevolveTool_Statics::NewProp_MaterialProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawAndRevolveTool_Statics::NewProp_Preview_MetaData[] = {
		{ "ModuleRelativePath", "Public/DrawAndRevolveTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDrawAndRevolveTool_Statics::NewProp_Preview = { "Preview", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawAndRevolveTool, Preview), Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDrawAndRevolveTool_Statics::NewProp_Preview_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawAndRevolveTool_Statics::NewProp_Preview_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDrawAndRevolveTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawAndRevolveTool_Statics::NewProp_ControlPointsMechanic,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawAndRevolveTool_Statics::NewProp_PlaneMechanic,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawAndRevolveTool_Statics::NewProp_Settings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawAndRevolveTool_Statics::NewProp_MaterialProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawAndRevolveTool_Statics::NewProp_Preview,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDrawAndRevolveTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDrawAndRevolveTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDrawAndRevolveTool_Statics::ClassParams = {
		&UDrawAndRevolveTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDrawAndRevolveTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDrawAndRevolveTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UDrawAndRevolveTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawAndRevolveTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDrawAndRevolveTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDrawAndRevolveTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDrawAndRevolveTool, 3006394126);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UDrawAndRevolveTool>()
	{
		return UDrawAndRevolveTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDrawAndRevolveTool(Z_Construct_UClass_UDrawAndRevolveTool, &UDrawAndRevolveTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UDrawAndRevolveTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDrawAndRevolveTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
