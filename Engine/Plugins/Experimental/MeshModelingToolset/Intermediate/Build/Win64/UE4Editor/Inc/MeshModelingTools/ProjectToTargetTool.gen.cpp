// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/ProjectToTargetTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeProjectToTargetTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UProjectToTargetToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UProjectToTargetToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UProjectToTargetToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UProjectToTargetToolProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_URemeshMeshToolProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UProjectToTargetTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UProjectToTargetTool();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_URemeshMeshTool();
// End Cross Module References
	void UProjectToTargetToolBuilder::StaticRegisterNativesUProjectToTargetToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UProjectToTargetToolBuilder_NoRegister()
	{
		return UProjectToTargetToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UProjectToTargetToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UProjectToTargetToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProjectToTargetToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Determine if/how we can build UProjectToTargetTool. It requires two selected mesh components.\n */" },
		{ "IncludePath", "ProjectToTargetTool.h" },
		{ "ModuleRelativePath", "Public/ProjectToTargetTool.h" },
		{ "ToolTip", "Determine if/how we can build UProjectToTargetTool. It requires two selected mesh components." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UProjectToTargetToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UProjectToTargetToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UProjectToTargetToolBuilder_Statics::ClassParams = {
		&UProjectToTargetToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UProjectToTargetToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UProjectToTargetToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UProjectToTargetToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UProjectToTargetToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UProjectToTargetToolBuilder, 3287970361);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UProjectToTargetToolBuilder>()
	{
		return UProjectToTargetToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UProjectToTargetToolBuilder(Z_Construct_UClass_UProjectToTargetToolBuilder, &UProjectToTargetToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UProjectToTargetToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UProjectToTargetToolBuilder);
	void UProjectToTargetToolProperties::StaticRegisterNativesUProjectToTargetToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UProjectToTargetToolProperties_NoRegister()
	{
		return UProjectToTargetToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UProjectToTargetToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UProjectToTargetToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_URemeshMeshToolProperties,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProjectToTargetToolProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Subclass URemeshMeshToolProperties just so we can set default values for some properties. Setting these values in the\n * Setup function of UProjectToTargetTool turns out to be tricky to achieve with the property cache.\n */" },
		{ "IncludePath", "ProjectToTargetTool.h" },
		{ "ModuleRelativePath", "Public/ProjectToTargetTool.h" },
		{ "ToolTip", "Subclass URemeshMeshToolProperties just so we can set default values for some properties. Setting these values in the\nSetup function of UProjectToTargetTool turns out to be tricky to achieve with the property cache." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UProjectToTargetToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UProjectToTargetToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UProjectToTargetToolProperties_Statics::ClassParams = {
		&UProjectToTargetToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UProjectToTargetToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UProjectToTargetToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UProjectToTargetToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UProjectToTargetToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UProjectToTargetToolProperties, 3520857962);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UProjectToTargetToolProperties>()
	{
		return UProjectToTargetToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UProjectToTargetToolProperties(Z_Construct_UClass_UProjectToTargetToolProperties, &UProjectToTargetToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UProjectToTargetToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UProjectToTargetToolProperties);
	void UProjectToTargetTool::StaticRegisterNativesUProjectToTargetTool()
	{
	}
	UClass* Z_Construct_UClass_UProjectToTargetTool_NoRegister()
	{
		return UProjectToTargetTool::StaticClass();
	}
	struct Z_Construct_UClass_UProjectToTargetTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UProjectToTargetTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_URemeshMeshTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProjectToTargetTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Project one mesh surface onto another, while undergoing remeshing. Subclass of URemeshMeshTool to avoid duplication.\n */" },
		{ "IncludePath", "ProjectToTargetTool.h" },
		{ "ModuleRelativePath", "Public/ProjectToTargetTool.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Project one mesh surface onto another, while undergoing remeshing. Subclass of URemeshMeshTool to avoid duplication." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UProjectToTargetTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UProjectToTargetTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UProjectToTargetTool_Statics::ClassParams = {
		&UProjectToTargetTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UProjectToTargetTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UProjectToTargetTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UProjectToTargetTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UProjectToTargetTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UProjectToTargetTool, 1228824902);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UProjectToTargetTool>()
	{
		return UProjectToTargetTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UProjectToTargetTool(Z_Construct_UClass_UProjectToTargetTool, &UProjectToTargetTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UProjectToTargetTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UProjectToTargetTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
