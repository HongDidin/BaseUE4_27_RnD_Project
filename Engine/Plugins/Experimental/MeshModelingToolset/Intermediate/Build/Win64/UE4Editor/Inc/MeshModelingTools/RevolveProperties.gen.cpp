// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/Properties/RevolveProperties.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRevolveProperties() {}
// Cross Module References
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_ERevolvePropertiesQuadSplit();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_ERevolvePropertiesPolygroupMode();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_ERevolvePropertiesCapFillMode();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_URevolveProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_URevolveProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
// End Cross Module References
	static UEnum* ERevolvePropertiesQuadSplit_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_ERevolvePropertiesQuadSplit, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("ERevolvePropertiesQuadSplit"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<ERevolvePropertiesQuadSplit>()
	{
		return ERevolvePropertiesQuadSplit_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ERevolvePropertiesQuadSplit(ERevolvePropertiesQuadSplit_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("ERevolvePropertiesQuadSplit"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_ERevolvePropertiesQuadSplit_Hash() { return 2484750782U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_ERevolvePropertiesQuadSplit()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ERevolvePropertiesQuadSplit"), 0, Get_Z_Construct_UEnum_MeshModelingTools_ERevolvePropertiesQuadSplit_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ERevolvePropertiesQuadSplit::Uniform", (int64)ERevolvePropertiesQuadSplit::Uniform },
				{ "ERevolvePropertiesQuadSplit::ShortestDiagonal", (int64)ERevolvePropertiesQuadSplit::ShortestDiagonal },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/Properties/RevolveProperties.h" },
				{ "ShortestDiagonal.Comment", "/** Quads will be split such that the shortest diagonal is connected. */" },
				{ "ShortestDiagonal.Name", "ERevolvePropertiesQuadSplit::ShortestDiagonal" },
				{ "ShortestDiagonal.ToolTip", "Quads will be split such that the shortest diagonal is connected." },
				{ "Uniform.Comment", "/** Quads will always be split the same way relative to an unrolled mesh, regardless of quad shape. */" },
				{ "Uniform.Name", "ERevolvePropertiesQuadSplit::Uniform" },
				{ "Uniform.ToolTip", "Quads will always be split the same way relative to an unrolled mesh, regardless of quad shape." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"ERevolvePropertiesQuadSplit",
				"ERevolvePropertiesQuadSplit",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ERevolvePropertiesPolygroupMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_ERevolvePropertiesPolygroupMode, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("ERevolvePropertiesPolygroupMode"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<ERevolvePropertiesPolygroupMode>()
	{
		return ERevolvePropertiesPolygroupMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ERevolvePropertiesPolygroupMode(ERevolvePropertiesPolygroupMode_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("ERevolvePropertiesPolygroupMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_ERevolvePropertiesPolygroupMode_Hash() { return 2032260846U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_ERevolvePropertiesPolygroupMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ERevolvePropertiesPolygroupMode"), 0, Get_Z_Construct_UEnum_MeshModelingTools_ERevolvePropertiesPolygroupMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ERevolvePropertiesPolygroupMode::Single", (int64)ERevolvePropertiesPolygroupMode::Single },
				{ "ERevolvePropertiesPolygroupMode::PerFace", (int64)ERevolvePropertiesPolygroupMode::PerFace },
				{ "ERevolvePropertiesPolygroupMode::PerStep", (int64)ERevolvePropertiesPolygroupMode::PerStep },
				{ "ERevolvePropertiesPolygroupMode::AccordingToProfileCurve", (int64)ERevolvePropertiesPolygroupMode::AccordingToProfileCurve },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "AccordingToProfileCurve.Comment", "/** Groups will be arranged in strips running along in the revolution direction according to profile curve. */" },
				{ "AccordingToProfileCurve.Name", "ERevolvePropertiesPolygroupMode::AccordingToProfileCurve" },
				{ "AccordingToProfileCurve.ToolTip", "Groups will be arranged in strips running along in the revolution direction according to profile curve." },
				{ "ModuleRelativePath", "Public/Properties/RevolveProperties.h" },
				{ "PerFace.Comment", "/** One polygroup per generated quad/triangle. */" },
				{ "PerFace.Name", "ERevolvePropertiesPolygroupMode::PerFace" },
				{ "PerFace.ToolTip", "One polygroup per generated quad/triangle." },
				{ "PerStep.Comment", "/** Groups will be arranged in strips running in the profile curve direction, one per revolution step. */" },
				{ "PerStep.Name", "ERevolvePropertiesPolygroupMode::PerStep" },
				{ "PerStep.ToolTip", "Groups will be arranged in strips running in the profile curve direction, one per revolution step." },
				{ "Single.Comment", "/** One polygroup for body of output mesh */" },
				{ "Single.Name", "ERevolvePropertiesPolygroupMode::Single" },
				{ "Single.ToolTip", "One polygroup for body of output mesh" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"ERevolvePropertiesPolygroupMode",
				"ERevolvePropertiesPolygroupMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ERevolvePropertiesCapFillMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_ERevolvePropertiesCapFillMode, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("ERevolvePropertiesCapFillMode"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<ERevolvePropertiesCapFillMode>()
	{
		return ERevolvePropertiesCapFillMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ERevolvePropertiesCapFillMode(ERevolvePropertiesCapFillMode_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("ERevolvePropertiesCapFillMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_ERevolvePropertiesCapFillMode_Hash() { return 3035784091U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_ERevolvePropertiesCapFillMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ERevolvePropertiesCapFillMode"), 0, Get_Z_Construct_UEnum_MeshModelingTools_ERevolvePropertiesCapFillMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ERevolvePropertiesCapFillMode::None", (int64)ERevolvePropertiesCapFillMode::None },
				{ "ERevolvePropertiesCapFillMode::Delaunay", (int64)ERevolvePropertiesCapFillMode::Delaunay },
				{ "ERevolvePropertiesCapFillMode::EarClipping", (int64)ERevolvePropertiesCapFillMode::EarClipping },
				{ "ERevolvePropertiesCapFillMode::CenterFan", (int64)ERevolvePropertiesCapFillMode::CenterFan },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "CenterFan.Comment", "/** A vertex is placed in the center and a fan is created to the boundary. This is nice if\n\x09   the cross section is convex, but creates invalid geometry if it isn't. */" },
				{ "CenterFan.Name", "ERevolvePropertiesCapFillMode::CenterFan" },
				{ "CenterFan.ToolTip", "A vertex is placed in the center and a fan is created to the boundary. This is nice if\n         the cross section is convex, but creates invalid geometry if it isn't." },
				{ "Delaunay.Comment", "/** Cap is triangulated to maximize the minimal angle in the triangles (if they were to be\n\x09   projected onto a best-fit plane). */" },
				{ "Delaunay.Name", "ERevolvePropertiesCapFillMode::Delaunay" },
				{ "Delaunay.ToolTip", "Cap is triangulated to maximize the minimal angle in the triangles (if they were to be\n         projected onto a best-fit plane)." },
				{ "EarClipping.Comment", "/** Cap is triangualted using a standard ear clipping approach. This could result in some\n\x09   very thin triangles. */" },
				{ "EarClipping.Name", "ERevolvePropertiesCapFillMode::EarClipping" },
				{ "EarClipping.ToolTip", "Cap is triangualted using a standard ear clipping approach. This could result in some\n         very thin triangles." },
				{ "ModuleRelativePath", "Public/Properties/RevolveProperties.h" },
				{ "None.Comment", "/** No cap. */" },
				{ "None.Name", "ERevolvePropertiesCapFillMode::None" },
				{ "None.ToolTip", "No cap." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"ERevolvePropertiesCapFillMode",
				"ERevolvePropertiesCapFillMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void URevolveProperties::StaticRegisterNativesURevolveProperties()
	{
	}
	UClass* Z_Construct_UClass_URevolveProperties_NoRegister()
	{
		return URevolveProperties::StaticClass();
	}
	struct Z_Construct_UClass_URevolveProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RevolutionDegrees_MetaData[];
#endif
		static const UE4CodeGen_Private::FDoublePropertyParams NewProp_RevolutionDegrees;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RevolutionDegreesOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FDoublePropertyParams NewProp_RevolutionDegreesOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Steps_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Steps;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bReverseRevolutionDirection_MetaData[];
#endif
		static void NewProp_bReverseRevolutionDirection_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bReverseRevolutionDirection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bFlipMesh_MetaData[];
#endif
		static void NewProp_bFlipMesh_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFlipMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bProfileIsCrossSectionOfSide_MetaData[];
#endif
		static void NewProp_bProfileIsCrossSectionOfSide_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bProfileIsCrossSectionOfSide;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_PolygroupMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PolygroupMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_PolygroupMode;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_QuadSplitMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_QuadSplitMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_QuadSplitMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DiagonalProportionTolerance_MetaData[];
#endif
		static const UE4CodeGen_Private::FDoublePropertyParams NewProp_DiagonalProportionTolerance;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_CapFillMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapFillMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_CapFillMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bWeldFullRevolution_MetaData[];
#endif
		static void NewProp_bWeldFullRevolution_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bWeldFullRevolution;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bWeldVertsOnAxis_MetaData[];
#endif
		static void NewProp_bWeldVertsOnAxis_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bWeldVertsOnAxis;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AxisWeldTolerance_MetaData[];
#endif
		static const UE4CodeGen_Private::FDoublePropertyParams NewProp_AxisWeldTolerance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSharpNormals_MetaData[];
#endif
		static void NewProp_bSharpNormals_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSharpNormals;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SharpNormalAngleTolerance_MetaData[];
#endif
		static const UE4CodeGen_Private::FDoublePropertyParams NewProp_SharpNormalAngleTolerance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bFlipVs_MetaData[];
#endif
		static void NewProp_bFlipVs_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFlipVs;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUVsSkipFullyWeldedEdges_MetaData[];
#endif
		static void NewProp_bUVsSkipFullyWeldedEdges_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUVsSkipFullyWeldedEdges;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URevolveProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URevolveProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Common properties for revolving a polyline to create a mesh.\n */" },
		{ "IncludePath", "Properties/RevolveProperties.h" },
		{ "ModuleRelativePath", "Public/Properties/RevolveProperties.h" },
		{ "ToolTip", "Common properties for revolving a polyline to create a mesh." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URevolveProperties_Statics::NewProp_RevolutionDegrees_MetaData[] = {
		{ "Category", "RevolveSettings" },
		{ "ClampMax", "360" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Revolution extent. */" },
		{ "ModuleRelativePath", "Public/Properties/RevolveProperties.h" },
		{ "ToolTip", "Revolution extent." },
		{ "UIMax", "360" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FDoublePropertyParams Z_Construct_UClass_URevolveProperties_Statics::NewProp_RevolutionDegrees = { "RevolutionDegrees", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URevolveProperties, RevolutionDegrees), METADATA_PARAMS(Z_Construct_UClass_URevolveProperties_Statics::NewProp_RevolutionDegrees_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URevolveProperties_Statics::NewProp_RevolutionDegrees_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URevolveProperties_Statics::NewProp_RevolutionDegreesOffset_MetaData[] = {
		{ "Category", "RevolveSettings" },
		{ "ClampMax", "36000" },
		{ "ClampMin", "-36000" },
		{ "Comment", "/** The angle by which to shift the profile curve around the axis before beginning the revolve */" },
		{ "ModuleRelativePath", "Public/Properties/RevolveProperties.h" },
		{ "ToolTip", "The angle by which to shift the profile curve around the axis before beginning the revolve" },
		{ "UIMax", "360" },
		{ "UIMin", "-360" },
	};
#endif
	const UE4CodeGen_Private::FDoublePropertyParams Z_Construct_UClass_URevolveProperties_Statics::NewProp_RevolutionDegreesOffset = { "RevolutionDegreesOffset", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URevolveProperties, RevolutionDegreesOffset), METADATA_PARAMS(Z_Construct_UClass_URevolveProperties_Statics::NewProp_RevolutionDegreesOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URevolveProperties_Statics::NewProp_RevolutionDegreesOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URevolveProperties_Statics::NewProp_Steps_MetaData[] = {
		{ "Category", "RevolveSettings" },
		{ "ClampMax", "5000" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Number of steps to take while revolving. */" },
		{ "ModuleRelativePath", "Public/Properties/RevolveProperties.h" },
		{ "ToolTip", "Number of steps to take while revolving." },
		{ "UIMax", "100" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_URevolveProperties_Statics::NewProp_Steps = { "Steps", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URevolveProperties, Steps), METADATA_PARAMS(Z_Construct_UClass_URevolveProperties_Statics::NewProp_Steps_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URevolveProperties_Statics::NewProp_Steps_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URevolveProperties_Statics::NewProp_bReverseRevolutionDirection_MetaData[] = {
		{ "Category", "RevolveSettings" },
		{ "Comment", "/** By default, revolution is done counterclockwise if looking down the revolution axis. This reverses the direction.*/" },
		{ "ModuleRelativePath", "Public/Properties/RevolveProperties.h" },
		{ "ToolTip", "By default, revolution is done counterclockwise if looking down the revolution axis. This reverses the direction." },
	};
#endif
	void Z_Construct_UClass_URevolveProperties_Statics::NewProp_bReverseRevolutionDirection_SetBit(void* Obj)
	{
		((URevolveProperties*)Obj)->bReverseRevolutionDirection = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URevolveProperties_Statics::NewProp_bReverseRevolutionDirection = { "bReverseRevolutionDirection", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URevolveProperties), &Z_Construct_UClass_URevolveProperties_Statics::NewProp_bReverseRevolutionDirection_SetBit, METADATA_PARAMS(Z_Construct_UClass_URevolveProperties_Statics::NewProp_bReverseRevolutionDirection_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URevolveProperties_Statics::NewProp_bReverseRevolutionDirection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URevolveProperties_Statics::NewProp_bFlipMesh_MetaData[] = {
		{ "Category", "RevolveSettings" },
		{ "Comment", "/** Flips the mesh inside out. */" },
		{ "ModuleRelativePath", "Public/Properties/RevolveProperties.h" },
		{ "ToolTip", "Flips the mesh inside out." },
	};
#endif
	void Z_Construct_UClass_URevolveProperties_Statics::NewProp_bFlipMesh_SetBit(void* Obj)
	{
		((URevolveProperties*)Obj)->bFlipMesh = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URevolveProperties_Statics::NewProp_bFlipMesh = { "bFlipMesh", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URevolveProperties), &Z_Construct_UClass_URevolveProperties_Statics::NewProp_bFlipMesh_SetBit, METADATA_PARAMS(Z_Construct_UClass_URevolveProperties_Statics::NewProp_bFlipMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URevolveProperties_Statics::NewProp_bFlipMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URevolveProperties_Statics::NewProp_bProfileIsCrossSectionOfSide_MetaData[] = {
		{ "Category", "RevolveSettings" },
		{ "Comment", "/** If true, then rather than revolving the profile directly, it is interpreted as the midpoint cross section of\n\x09 the first rotation step. Useful, for instance, for using the tool to create square columns. */" },
		{ "ModuleRelativePath", "Public/Properties/RevolveProperties.h" },
		{ "ToolTip", "If true, then rather than revolving the profile directly, it is interpreted as the midpoint cross section of\n       the first rotation step. Useful, for instance, for using the tool to create square columns." },
	};
#endif
	void Z_Construct_UClass_URevolveProperties_Statics::NewProp_bProfileIsCrossSectionOfSide_SetBit(void* Obj)
	{
		((URevolveProperties*)Obj)->bProfileIsCrossSectionOfSide = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URevolveProperties_Statics::NewProp_bProfileIsCrossSectionOfSide = { "bProfileIsCrossSectionOfSide", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URevolveProperties), &Z_Construct_UClass_URevolveProperties_Statics::NewProp_bProfileIsCrossSectionOfSide_SetBit, METADATA_PARAMS(Z_Construct_UClass_URevolveProperties_Statics::NewProp_bProfileIsCrossSectionOfSide_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URevolveProperties_Statics::NewProp_bProfileIsCrossSectionOfSide_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_URevolveProperties_Statics::NewProp_PolygroupMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URevolveProperties_Statics::NewProp_PolygroupMode_MetaData[] = {
		{ "Category", "RevolveSettings" },
		{ "Comment", "/** Determines grouping of generated triangles into polygroups. \n\x09   Caps (if present) will always be separate groups. */" },
		{ "ModuleRelativePath", "Public/Properties/RevolveProperties.h" },
		{ "ToolTip", "Determines grouping of generated triangles into polygroups.\n         Caps (if present) will always be separate groups." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_URevolveProperties_Statics::NewProp_PolygroupMode = { "PolygroupMode", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URevolveProperties, PolygroupMode), Z_Construct_UEnum_MeshModelingTools_ERevolvePropertiesPolygroupMode, METADATA_PARAMS(Z_Construct_UClass_URevolveProperties_Statics::NewProp_PolygroupMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URevolveProperties_Statics::NewProp_PolygroupMode_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_URevolveProperties_Statics::NewProp_QuadSplitMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URevolveProperties_Statics::NewProp_QuadSplitMode_MetaData[] = {
		{ "Category", "RevolveSettings" },
		{ "Comment", "/** Determines how any generated quads are split into triangles. */" },
		{ "ModuleRelativePath", "Public/Properties/RevolveProperties.h" },
		{ "ToolTip", "Determines how any generated quads are split into triangles." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_URevolveProperties_Statics::NewProp_QuadSplitMode = { "QuadSplitMode", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URevolveProperties, QuadSplitMode), Z_Construct_UEnum_MeshModelingTools_ERevolvePropertiesQuadSplit, METADATA_PARAMS(Z_Construct_UClass_URevolveProperties_Statics::NewProp_QuadSplitMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URevolveProperties_Statics::NewProp_QuadSplitMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URevolveProperties_Statics::NewProp_DiagonalProportionTolerance_MetaData[] = {
		{ "Category", "RevolveSettings" },
		{ "ClampMax", "2.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** When quads are generated using \"shortest\" diagonal, this biases the diagonal length comparison\n\x09 to prefer one slightly in the case of similar diagonals (for example, a value of 0.01 allows a\n\x09 1% difference in lengths before the triangulation is flipped). Helps symmetric quads be uniformly\n\x09 triangulated. */" },
		{ "EditCondition", "QuadSplitMode == ERevolvePropertiesQuadSplit::ShortestDiagonal" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/Properties/RevolveProperties.h" },
		{ "ToolTip", "When quads are generated using \"shortest\" diagonal, this biases the diagonal length comparison\n       to prefer one slightly in the case of similar diagonals (for example, a value of 0.01 allows a\n       1% difference in lengths before the triangulation is flipped). Helps symmetric quads be uniformly\n       triangulated." },
	};
#endif
	const UE4CodeGen_Private::FDoublePropertyParams Z_Construct_UClass_URevolveProperties_Statics::NewProp_DiagonalProportionTolerance = { "DiagonalProportionTolerance", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URevolveProperties, DiagonalProportionTolerance), METADATA_PARAMS(Z_Construct_UClass_URevolveProperties_Statics::NewProp_DiagonalProportionTolerance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URevolveProperties_Statics::NewProp_DiagonalProportionTolerance_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_URevolveProperties_Statics::NewProp_CapFillMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URevolveProperties_Statics::NewProp_CapFillMode_MetaData[] = {
		{ "Category", "RevolveSettings" },
		{ "Comment", "/** Determines how caps are created if the revolution is partial. Not relevant if the\n\x09  revolution is full and welded. */" },
		{ "ModuleRelativePath", "Public/Properties/RevolveProperties.h" },
		{ "ToolTip", "Determines how caps are created if the revolution is partial. Not relevant if the\n        revolution is full and welded." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_URevolveProperties_Statics::NewProp_CapFillMode = { "CapFillMode", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URevolveProperties, CapFillMode), Z_Construct_UEnum_MeshModelingTools_ERevolvePropertiesCapFillMode, METADATA_PARAMS(Z_Construct_UClass_URevolveProperties_Statics::NewProp_CapFillMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URevolveProperties_Statics::NewProp_CapFillMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URevolveProperties_Statics::NewProp_bWeldFullRevolution_MetaData[] = {
		{ "Category", "RevolveSettings" },
		{ "Comment", "/** If true, the ends of a fully revolved profile are welded together, rather than duplicating\n\x09  vertices at the seam. Not relevant if the revolution is not full. */" },
		{ "ModuleRelativePath", "Public/Properties/RevolveProperties.h" },
		{ "ToolTip", "If true, the ends of a fully revolved profile are welded together, rather than duplicating\n        vertices at the seam. Not relevant if the revolution is not full." },
	};
#endif
	void Z_Construct_UClass_URevolveProperties_Statics::NewProp_bWeldFullRevolution_SetBit(void* Obj)
	{
		((URevolveProperties*)Obj)->bWeldFullRevolution = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URevolveProperties_Statics::NewProp_bWeldFullRevolution = { "bWeldFullRevolution", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URevolveProperties), &Z_Construct_UClass_URevolveProperties_Statics::NewProp_bWeldFullRevolution_SetBit, METADATA_PARAMS(Z_Construct_UClass_URevolveProperties_Statics::NewProp_bWeldFullRevolution_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URevolveProperties_Statics::NewProp_bWeldFullRevolution_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URevolveProperties_Statics::NewProp_bWeldVertsOnAxis_MetaData[] = {
		{ "Category", "RevolveSettings" },
		{ "Comment", "/** If true, vertices sufficiently close to the axis will not be replicated, instead reusing\n\x09  the same vertex for any adjacent triangles. */" },
		{ "ModuleRelativePath", "Public/Properties/RevolveProperties.h" },
		{ "ToolTip", "If true, vertices sufficiently close to the axis will not be replicated, instead reusing\n        the same vertex for any adjacent triangles." },
	};
#endif
	void Z_Construct_UClass_URevolveProperties_Statics::NewProp_bWeldVertsOnAxis_SetBit(void* Obj)
	{
		((URevolveProperties*)Obj)->bWeldVertsOnAxis = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URevolveProperties_Statics::NewProp_bWeldVertsOnAxis = { "bWeldVertsOnAxis", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URevolveProperties), &Z_Construct_UClass_URevolveProperties_Statics::NewProp_bWeldVertsOnAxis_SetBit, METADATA_PARAMS(Z_Construct_UClass_URevolveProperties_Statics::NewProp_bWeldVertsOnAxis_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URevolveProperties_Statics::NewProp_bWeldVertsOnAxis_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URevolveProperties_Statics::NewProp_AxisWeldTolerance_MetaData[] = {
		{ "Category", "RevolveSettings" },
		{ "ClampMax", "20.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** If welding vertices on the axis, the distance that a vertex can be from the axis and still be welded */" },
		{ "EditCondition", "bWeldVertsOnAxis" },
		{ "ModuleRelativePath", "Public/Properties/RevolveProperties.h" },
		{ "ToolTip", "If welding vertices on the axis, the distance that a vertex can be from the axis and still be welded" },
	};
#endif
	const UE4CodeGen_Private::FDoublePropertyParams Z_Construct_UClass_URevolveProperties_Statics::NewProp_AxisWeldTolerance = { "AxisWeldTolerance", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URevolveProperties, AxisWeldTolerance), METADATA_PARAMS(Z_Construct_UClass_URevolveProperties_Statics::NewProp_AxisWeldTolerance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URevolveProperties_Statics::NewProp_AxisWeldTolerance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URevolveProperties_Statics::NewProp_bSharpNormals_MetaData[] = {
		{ "Category", "RevolveSettings" },
		{ "Comment", "/** If true, normals are not averaged or shared between triangles with sufficient angle difference. */" },
		{ "ModuleRelativePath", "Public/Properties/RevolveProperties.h" },
		{ "ToolTip", "If true, normals are not averaged or shared between triangles with sufficient angle difference." },
	};
#endif
	void Z_Construct_UClass_URevolveProperties_Statics::NewProp_bSharpNormals_SetBit(void* Obj)
	{
		((URevolveProperties*)Obj)->bSharpNormals = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URevolveProperties_Statics::NewProp_bSharpNormals = { "bSharpNormals", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URevolveProperties), &Z_Construct_UClass_URevolveProperties_Statics::NewProp_bSharpNormals_SetBit, METADATA_PARAMS(Z_Construct_UClass_URevolveProperties_Statics::NewProp_bSharpNormals_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URevolveProperties_Statics::NewProp_bSharpNormals_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URevolveProperties_Statics::NewProp_SharpNormalAngleTolerance_MetaData[] = {
		{ "Category", "RevolveSettings" },
		{ "ClampMax", "90.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** When using sharp normals, the degree difference to accept between adjacent triangle normals to allow them to share\n\x09 normals at their vertices. */" },
		{ "EditCondition", "bSharpNormals" },
		{ "ModuleRelativePath", "Public/Properties/RevolveProperties.h" },
		{ "ToolTip", "When using sharp normals, the degree difference to accept between adjacent triangle normals to allow them to share\n       normals at their vertices." },
	};
#endif
	const UE4CodeGen_Private::FDoublePropertyParams Z_Construct_UClass_URevolveProperties_Statics::NewProp_SharpNormalAngleTolerance = { "SharpNormalAngleTolerance", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URevolveProperties, SharpNormalAngleTolerance), METADATA_PARAMS(Z_Construct_UClass_URevolveProperties_Statics::NewProp_SharpNormalAngleTolerance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URevolveProperties_Statics::NewProp_SharpNormalAngleTolerance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URevolveProperties_Statics::NewProp_bFlipVs_MetaData[] = {
		{ "Category", "RevolveSettings" },
		{ "Comment", "/** If true, UV coordinates will be flipped in the V direction. */" },
		{ "ModuleRelativePath", "Public/Properties/RevolveProperties.h" },
		{ "ToolTip", "If true, UV coordinates will be flipped in the V direction." },
	};
#endif
	void Z_Construct_UClass_URevolveProperties_Statics::NewProp_bFlipVs_SetBit(void* Obj)
	{
		((URevolveProperties*)Obj)->bFlipVs = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URevolveProperties_Statics::NewProp_bFlipVs = { "bFlipVs", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URevolveProperties), &Z_Construct_UClass_URevolveProperties_Statics::NewProp_bFlipVs_SetBit, METADATA_PARAMS(Z_Construct_UClass_URevolveProperties_Statics::NewProp_bFlipVs_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URevolveProperties_Statics::NewProp_bFlipVs_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URevolveProperties_Statics::NewProp_bUVsSkipFullyWeldedEdges_MetaData[] = {
		{ "Category", "RevolveSettings" },
		{ "Comment", "/* If true, UV layout is not affected by segments of the profile curve that \n\x09  do not result in any triangles (i.e., when both ends of the segment are welded\n\x09  due to being on the revolution axis).*/" },
		{ "ModuleRelativePath", "Public/Properties/RevolveProperties.h" },
		{ "ToolTip", "If true, UV layout is not affected by segments of the profile curve that\n        do not result in any triangles (i.e., when both ends of the segment are welded\n        due to being on the revolution axis)." },
	};
#endif
	void Z_Construct_UClass_URevolveProperties_Statics::NewProp_bUVsSkipFullyWeldedEdges_SetBit(void* Obj)
	{
		((URevolveProperties*)Obj)->bUVsSkipFullyWeldedEdges = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URevolveProperties_Statics::NewProp_bUVsSkipFullyWeldedEdges = { "bUVsSkipFullyWeldedEdges", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URevolveProperties), &Z_Construct_UClass_URevolveProperties_Statics::NewProp_bUVsSkipFullyWeldedEdges_SetBit, METADATA_PARAMS(Z_Construct_UClass_URevolveProperties_Statics::NewProp_bUVsSkipFullyWeldedEdges_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URevolveProperties_Statics::NewProp_bUVsSkipFullyWeldedEdges_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URevolveProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URevolveProperties_Statics::NewProp_RevolutionDegrees,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URevolveProperties_Statics::NewProp_RevolutionDegreesOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URevolveProperties_Statics::NewProp_Steps,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URevolveProperties_Statics::NewProp_bReverseRevolutionDirection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URevolveProperties_Statics::NewProp_bFlipMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URevolveProperties_Statics::NewProp_bProfileIsCrossSectionOfSide,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URevolveProperties_Statics::NewProp_PolygroupMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URevolveProperties_Statics::NewProp_PolygroupMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URevolveProperties_Statics::NewProp_QuadSplitMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URevolveProperties_Statics::NewProp_QuadSplitMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URevolveProperties_Statics::NewProp_DiagonalProportionTolerance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URevolveProperties_Statics::NewProp_CapFillMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URevolveProperties_Statics::NewProp_CapFillMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URevolveProperties_Statics::NewProp_bWeldFullRevolution,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URevolveProperties_Statics::NewProp_bWeldVertsOnAxis,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URevolveProperties_Statics::NewProp_AxisWeldTolerance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URevolveProperties_Statics::NewProp_bSharpNormals,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URevolveProperties_Statics::NewProp_SharpNormalAngleTolerance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URevolveProperties_Statics::NewProp_bFlipVs,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URevolveProperties_Statics::NewProp_bUVsSkipFullyWeldedEdges,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URevolveProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URevolveProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URevolveProperties_Statics::ClassParams = {
		&URevolveProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URevolveProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URevolveProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_URevolveProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URevolveProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URevolveProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URevolveProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URevolveProperties, 468565690);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<URevolveProperties>()
	{
		return URevolveProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URevolveProperties(Z_Construct_UClass_URevolveProperties, &URevolveProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("URevolveProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URevolveProperties);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
