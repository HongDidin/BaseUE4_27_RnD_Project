// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ModelingComponents/Public/BaseTools/BaseCreateFromSelectedTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBaseCreateFromSelectedTool() {}
// Cross Module References
	MODELINGCOMPONENTS_API UEnum* Z_Construct_UEnum_ModelingComponents_EBaseCreateFromSelectedTargetType();
	UPackage* Z_Construct_UPackage__Script_ModelingComponents();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UBaseCreateFromSelectedToolBuilder_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UBaseCreateFromSelectedToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UBaseCreateFromSelectedHandleSourceProperties_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UBaseCreateFromSelectedHandleSourceProperties();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UOnAcceptHandleSourcesProperties();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UTransformInputsToolProperties_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UTransformInputsToolProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UBaseCreateFromSelectedTool_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UBaseCreateFromSelectedTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UMultiSelectionTool();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_NoRegister();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UTransformProxy_NoRegister();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UTransformGizmo_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
// End Cross Module References
	static UEnum* EBaseCreateFromSelectedTargetType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ModelingComponents_EBaseCreateFromSelectedTargetType, Z_Construct_UPackage__Script_ModelingComponents(), TEXT("EBaseCreateFromSelectedTargetType"));
		}
		return Singleton;
	}
	template<> MODELINGCOMPONENTS_API UEnum* StaticEnum<EBaseCreateFromSelectedTargetType>()
	{
		return EBaseCreateFromSelectedTargetType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EBaseCreateFromSelectedTargetType(EBaseCreateFromSelectedTargetType_StaticEnum, TEXT("/Script/ModelingComponents"), TEXT("EBaseCreateFromSelectedTargetType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ModelingComponents_EBaseCreateFromSelectedTargetType_Hash() { return 2996825302U; }
	UEnum* Z_Construct_UEnum_ModelingComponents_EBaseCreateFromSelectedTargetType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ModelingComponents();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EBaseCreateFromSelectedTargetType"), 0, Get_Z_Construct_UEnum_ModelingComponents_EBaseCreateFromSelectedTargetType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EBaseCreateFromSelectedTargetType::NewAsset", (int64)EBaseCreateFromSelectedTargetType::NewAsset },
				{ "EBaseCreateFromSelectedTargetType::FirstInputAsset", (int64)EBaseCreateFromSelectedTargetType::FirstInputAsset },
				{ "EBaseCreateFromSelectedTargetType::LastInputAsset", (int64)EBaseCreateFromSelectedTargetType::LastInputAsset },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "FirstInputAsset.Comment", "/** Store the result mesh in the first selected input asset */" },
				{ "FirstInputAsset.Name", "EBaseCreateFromSelectedTargetType::FirstInputAsset" },
				{ "FirstInputAsset.ToolTip", "Store the result mesh in the first selected input asset" },
				{ "LastInputAsset.Comment", "/** Store the result mesh in the last selected input asset */" },
				{ "LastInputAsset.Name", "EBaseCreateFromSelectedTargetType::LastInputAsset" },
				{ "LastInputAsset.ToolTip", "Store the result mesh in the last selected input asset" },
				{ "ModuleRelativePath", "Public/BaseTools/BaseCreateFromSelectedTool.h" },
				{ "NewAsset.Comment", "/** Create a new asset containing the result mesh */" },
				{ "NewAsset.Name", "EBaseCreateFromSelectedTargetType::NewAsset" },
				{ "NewAsset.ToolTip", "Create a new asset containing the result mesh" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ModelingComponents,
				nullptr,
				"EBaseCreateFromSelectedTargetType",
				"EBaseCreateFromSelectedTargetType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UBaseCreateFromSelectedToolBuilder::StaticRegisterNativesUBaseCreateFromSelectedToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UBaseCreateFromSelectedToolBuilder_NoRegister()
	{
		return UBaseCreateFromSelectedToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UBaseCreateFromSelectedToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBaseCreateFromSelectedToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_ModelingComponents,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBaseCreateFromSelectedToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * ToolBuilder for UBaseCreateFromSelectedTool\n */" },
		{ "IncludePath", "BaseTools/BaseCreateFromSelectedTool.h" },
		{ "ModuleRelativePath", "Public/BaseTools/BaseCreateFromSelectedTool.h" },
		{ "ToolTip", "ToolBuilder for UBaseCreateFromSelectedTool" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBaseCreateFromSelectedToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBaseCreateFromSelectedToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBaseCreateFromSelectedToolBuilder_Statics::ClassParams = {
		&UBaseCreateFromSelectedToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UBaseCreateFromSelectedToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBaseCreateFromSelectedToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBaseCreateFromSelectedToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBaseCreateFromSelectedToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBaseCreateFromSelectedToolBuilder, 1825806445);
	template<> MODELINGCOMPONENTS_API UClass* StaticClass<UBaseCreateFromSelectedToolBuilder>()
	{
		return UBaseCreateFromSelectedToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBaseCreateFromSelectedToolBuilder(Z_Construct_UClass_UBaseCreateFromSelectedToolBuilder, &UBaseCreateFromSelectedToolBuilder::StaticClass, TEXT("/Script/ModelingComponents"), TEXT("UBaseCreateFromSelectedToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBaseCreateFromSelectedToolBuilder);
	void UBaseCreateFromSelectedHandleSourceProperties::StaticRegisterNativesUBaseCreateFromSelectedHandleSourceProperties()
	{
	}
	UClass* Z_Construct_UClass_UBaseCreateFromSelectedHandleSourceProperties_NoRegister()
	{
		return UBaseCreateFromSelectedHandleSourceProperties::StaticClass();
	}
	struct Z_Construct_UClass_UBaseCreateFromSelectedHandleSourceProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_WriteOutputTo_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WriteOutputTo_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_WriteOutputTo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OutputName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_OutputName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OutputAsset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_OutputAsset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBaseCreateFromSelectedHandleSourceProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UOnAcceptHandleSourcesProperties,
		(UObject* (*)())Z_Construct_UPackage__Script_ModelingComponents,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBaseCreateFromSelectedHandleSourceProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "BaseTools/BaseCreateFromSelectedTool.h" },
		{ "ModuleRelativePath", "Public/BaseTools/BaseCreateFromSelectedTool.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UBaseCreateFromSelectedHandleSourceProperties_Statics::NewProp_WriteOutputTo_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBaseCreateFromSelectedHandleSourceProperties_Statics::NewProp_WriteOutputTo_MetaData[] = {
		{ "Category", "ToolOutputOptions" },
		{ "Comment", "/** Where should the output mesh produced by this operation be stored */" },
		{ "ModuleRelativePath", "Public/BaseTools/BaseCreateFromSelectedTool.h" },
		{ "ToolTip", "Where should the output mesh produced by this operation be stored" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UBaseCreateFromSelectedHandleSourceProperties_Statics::NewProp_WriteOutputTo = { "WriteOutputTo", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBaseCreateFromSelectedHandleSourceProperties, WriteOutputTo), Z_Construct_UEnum_ModelingComponents_EBaseCreateFromSelectedTargetType, METADATA_PARAMS(Z_Construct_UClass_UBaseCreateFromSelectedHandleSourceProperties_Statics::NewProp_WriteOutputTo_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBaseCreateFromSelectedHandleSourceProperties_Statics::NewProp_WriteOutputTo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBaseCreateFromSelectedHandleSourceProperties_Statics::NewProp_OutputName_MetaData[] = {
		{ "Category", "ToolOutputOptions" },
		{ "Comment", "/** Base name for newly-generated asset */" },
		{ "EditCondition", "WriteOutputTo == EBaseCreateFromSelectedTargetType::NewAsset" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/BaseTools/BaseCreateFromSelectedTool.h" },
		{ "ToolTip", "Base name for newly-generated asset" },
		{ "TransientToolProperty", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UBaseCreateFromSelectedHandleSourceProperties_Statics::NewProp_OutputName = { "OutputName", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBaseCreateFromSelectedHandleSourceProperties, OutputName), METADATA_PARAMS(Z_Construct_UClass_UBaseCreateFromSelectedHandleSourceProperties_Statics::NewProp_OutputName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBaseCreateFromSelectedHandleSourceProperties_Statics::NewProp_OutputName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBaseCreateFromSelectedHandleSourceProperties_Statics::NewProp_OutputAsset_MetaData[] = {
		{ "Category", "ToolOutputOptions" },
		{ "Comment", "/** Name of asset that will be updated */" },
		{ "EditCondition", "WriteOutputTo != EBaseCreateFromSelectedTargetType::NewAsset" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/BaseTools/BaseCreateFromSelectedTool.h" },
		{ "ToolTip", "Name of asset that will be updated" },
		{ "TransientToolProperty", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UBaseCreateFromSelectedHandleSourceProperties_Statics::NewProp_OutputAsset = { "OutputAsset", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBaseCreateFromSelectedHandleSourceProperties, OutputAsset), METADATA_PARAMS(Z_Construct_UClass_UBaseCreateFromSelectedHandleSourceProperties_Statics::NewProp_OutputAsset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBaseCreateFromSelectedHandleSourceProperties_Statics::NewProp_OutputAsset_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UBaseCreateFromSelectedHandleSourceProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBaseCreateFromSelectedHandleSourceProperties_Statics::NewProp_WriteOutputTo_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBaseCreateFromSelectedHandleSourceProperties_Statics::NewProp_WriteOutputTo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBaseCreateFromSelectedHandleSourceProperties_Statics::NewProp_OutputName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBaseCreateFromSelectedHandleSourceProperties_Statics::NewProp_OutputAsset,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBaseCreateFromSelectedHandleSourceProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBaseCreateFromSelectedHandleSourceProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBaseCreateFromSelectedHandleSourceProperties_Statics::ClassParams = {
		&UBaseCreateFromSelectedHandleSourceProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UBaseCreateFromSelectedHandleSourceProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UBaseCreateFromSelectedHandleSourceProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UBaseCreateFromSelectedHandleSourceProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBaseCreateFromSelectedHandleSourceProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBaseCreateFromSelectedHandleSourceProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBaseCreateFromSelectedHandleSourceProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBaseCreateFromSelectedHandleSourceProperties, 2553742331);
	template<> MODELINGCOMPONENTS_API UClass* StaticClass<UBaseCreateFromSelectedHandleSourceProperties>()
	{
		return UBaseCreateFromSelectedHandleSourceProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBaseCreateFromSelectedHandleSourceProperties(Z_Construct_UClass_UBaseCreateFromSelectedHandleSourceProperties, &UBaseCreateFromSelectedHandleSourceProperties::StaticClass, TEXT("/Script/ModelingComponents"), TEXT("UBaseCreateFromSelectedHandleSourceProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBaseCreateFromSelectedHandleSourceProperties);
	void UTransformInputsToolProperties::StaticRegisterNativesUTransformInputsToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UTransformInputsToolProperties_NoRegister()
	{
		return UTransformInputsToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UTransformInputsToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowTransformUI_MetaData[];
#endif
		static void NewProp_bShowTransformUI_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowTransformUI;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSnapToWorldGrid_MetaData[];
#endif
		static void NewProp_bSnapToWorldGrid_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSnapToWorldGrid;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTransformInputsToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_ModelingComponents,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTransformInputsToolProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Properties of UI to adjust input meshes\n */" },
		{ "IncludePath", "BaseTools/BaseCreateFromSelectedTool.h" },
		{ "ModuleRelativePath", "Public/BaseTools/BaseCreateFromSelectedTool.h" },
		{ "ToolTip", "Properties of UI to adjust input meshes" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTransformInputsToolProperties_Statics::NewProp_bShowTransformUI_MetaData[] = {
		{ "Category", "Transform" },
		{ "Comment", "/** Show UI to allow changing translation, rotation and scale of input meshes */" },
		{ "ModuleRelativePath", "Public/BaseTools/BaseCreateFromSelectedTool.h" },
		{ "ToolTip", "Show UI to allow changing translation, rotation and scale of input meshes" },
	};
#endif
	void Z_Construct_UClass_UTransformInputsToolProperties_Statics::NewProp_bShowTransformUI_SetBit(void* Obj)
	{
		((UTransformInputsToolProperties*)Obj)->bShowTransformUI = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UTransformInputsToolProperties_Statics::NewProp_bShowTransformUI = { "bShowTransformUI", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UTransformInputsToolProperties), &Z_Construct_UClass_UTransformInputsToolProperties_Statics::NewProp_bShowTransformUI_SetBit, METADATA_PARAMS(Z_Construct_UClass_UTransformInputsToolProperties_Statics::NewProp_bShowTransformUI_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTransformInputsToolProperties_Statics::NewProp_bShowTransformUI_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTransformInputsToolProperties_Statics::NewProp_bSnapToWorldGrid_MetaData[] = {
		{ "Category", "Transform" },
		{ "Comment", "/** Snap the cut plane to the world grid */" },
		{ "EditCondition", "bShowTransformUI == true" },
		{ "ModuleRelativePath", "Public/BaseTools/BaseCreateFromSelectedTool.h" },
		{ "ToolTip", "Snap the cut plane to the world grid" },
	};
#endif
	void Z_Construct_UClass_UTransformInputsToolProperties_Statics::NewProp_bSnapToWorldGrid_SetBit(void* Obj)
	{
		((UTransformInputsToolProperties*)Obj)->bSnapToWorldGrid = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UTransformInputsToolProperties_Statics::NewProp_bSnapToWorldGrid = { "bSnapToWorldGrid", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UTransformInputsToolProperties), &Z_Construct_UClass_UTransformInputsToolProperties_Statics::NewProp_bSnapToWorldGrid_SetBit, METADATA_PARAMS(Z_Construct_UClass_UTransformInputsToolProperties_Statics::NewProp_bSnapToWorldGrid_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTransformInputsToolProperties_Statics::NewProp_bSnapToWorldGrid_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTransformInputsToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTransformInputsToolProperties_Statics::NewProp_bShowTransformUI,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTransformInputsToolProperties_Statics::NewProp_bSnapToWorldGrid,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTransformInputsToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTransformInputsToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTransformInputsToolProperties_Statics::ClassParams = {
		&UTransformInputsToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UTransformInputsToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UTransformInputsToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UTransformInputsToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTransformInputsToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTransformInputsToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTransformInputsToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTransformInputsToolProperties, 1802973379);
	template<> MODELINGCOMPONENTS_API UClass* StaticClass<UTransformInputsToolProperties>()
	{
		return UTransformInputsToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTransformInputsToolProperties(Z_Construct_UClass_UTransformInputsToolProperties, &UTransformInputsToolProperties::StaticClass, TEXT("/Script/ModelingComponents"), TEXT("UTransformInputsToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTransformInputsToolProperties);
	void UBaseCreateFromSelectedTool::StaticRegisterNativesUBaseCreateFromSelectedTool()
	{
	}
	UClass* Z_Construct_UClass_UBaseCreateFromSelectedTool_NoRegister()
	{
		return UBaseCreateFromSelectedTool::StaticClass();
	}
	struct Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransformProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TransformProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HandleSourcesProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_HandleSourcesProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Preview_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Preview;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TransformProxies_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransformProxies_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_TransformProxies;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TransformGizmos_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransformGizmos_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_TransformGizmos;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TransformInitialScales_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransformInitialScales_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_TransformInitialScales;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMultiSelectionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_ModelingComponents,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * UBaseCreateFromSelectedTool is a base Tool (must be subclassed) \n * that provides support for common functionality in tools that create a new mesh from a selection of one or more existing meshes\n */" },
		{ "IncludePath", "BaseTools/BaseCreateFromSelectedTool.h" },
		{ "ModuleRelativePath", "Public/BaseTools/BaseCreateFromSelectedTool.h" },
		{ "ToolTip", "UBaseCreateFromSelectedTool is a base Tool (must be subclassed)\nthat provides support for common functionality in tools that create a new mesh from a selection of one or more existing meshes" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::NewProp_TransformProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/BaseTools/BaseCreateFromSelectedTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::NewProp_TransformProperties = { "TransformProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBaseCreateFromSelectedTool, TransformProperties), Z_Construct_UClass_UTransformInputsToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::NewProp_TransformProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::NewProp_TransformProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::NewProp_HandleSourcesProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/BaseTools/BaseCreateFromSelectedTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::NewProp_HandleSourcesProperties = { "HandleSourcesProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBaseCreateFromSelectedTool, HandleSourcesProperties), Z_Construct_UClass_UBaseCreateFromSelectedHandleSourceProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::NewProp_HandleSourcesProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::NewProp_HandleSourcesProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::NewProp_Preview_MetaData[] = {
		{ "ModuleRelativePath", "Public/BaseTools/BaseCreateFromSelectedTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::NewProp_Preview = { "Preview", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBaseCreateFromSelectedTool, Preview), Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::NewProp_Preview_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::NewProp_Preview_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::NewProp_TransformProxies_Inner = { "TransformProxies", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UTransformProxy_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::NewProp_TransformProxies_MetaData[] = {
		{ "ModuleRelativePath", "Public/BaseTools/BaseCreateFromSelectedTool.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::NewProp_TransformProxies = { "TransformProxies", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBaseCreateFromSelectedTool, TransformProxies), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::NewProp_TransformProxies_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::NewProp_TransformProxies_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::NewProp_TransformGizmos_Inner = { "TransformGizmos", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UTransformGizmo_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::NewProp_TransformGizmos_MetaData[] = {
		{ "ModuleRelativePath", "Public/BaseTools/BaseCreateFromSelectedTool.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::NewProp_TransformGizmos = { "TransformGizmos", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBaseCreateFromSelectedTool, TransformGizmos), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::NewProp_TransformGizmos_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::NewProp_TransformGizmos_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::NewProp_TransformInitialScales_Inner = { "TransformInitialScales", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::NewProp_TransformInitialScales_MetaData[] = {
		{ "ModuleRelativePath", "Public/BaseTools/BaseCreateFromSelectedTool.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::NewProp_TransformInitialScales = { "TransformInitialScales", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBaseCreateFromSelectedTool, TransformInitialScales), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::NewProp_TransformInitialScales_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::NewProp_TransformInitialScales_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::NewProp_TransformProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::NewProp_HandleSourcesProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::NewProp_Preview,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::NewProp_TransformProxies_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::NewProp_TransformProxies,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::NewProp_TransformGizmos_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::NewProp_TransformGizmos,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::NewProp_TransformInitialScales_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::NewProp_TransformInitialScales,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBaseCreateFromSelectedTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::ClassParams = {
		&UBaseCreateFromSelectedTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBaseCreateFromSelectedTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBaseCreateFromSelectedTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBaseCreateFromSelectedTool, 3137960010);
	template<> MODELINGCOMPONENTS_API UClass* StaticClass<UBaseCreateFromSelectedTool>()
	{
		return UBaseCreateFromSelectedTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBaseCreateFromSelectedTool(Z_Construct_UClass_UBaseCreateFromSelectedTool, &UBaseCreateFromSelectedTool::StaticClass, TEXT("/Script/ModelingComponents"), TEXT("UBaseCreateFromSelectedTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBaseCreateFromSelectedTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
