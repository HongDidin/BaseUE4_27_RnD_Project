// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/RemeshMeshTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRemeshMeshTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_URemeshMeshToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_URemeshMeshToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_URemeshMeshToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_URemeshMeshToolProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_URemeshProperties();
	MODELINGOPERATORS_API UEnum* Z_Construct_UEnum_ModelingOperators_ERemeshSmoothingType();
	MODELINGOPERATORS_API UEnum* Z_Construct_UEnum_ModelingOperators_ERemeshType();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_URemeshMeshTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_URemeshMeshTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UMultiSelectionTool();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshStatisticsProperties_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_NoRegister();
// End Cross Module References
	void URemeshMeshToolBuilder::StaticRegisterNativesURemeshMeshToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_URemeshMeshToolBuilder_NoRegister()
	{
		return URemeshMeshToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_URemeshMeshToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URemeshMeshToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemeshMeshToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "RemeshMeshTool.h" },
		{ "ModuleRelativePath", "Public/RemeshMeshTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_URemeshMeshToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URemeshMeshToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URemeshMeshToolBuilder_Statics::ClassParams = {
		&URemeshMeshToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_URemeshMeshToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URemeshMeshToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URemeshMeshToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URemeshMeshToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URemeshMeshToolBuilder, 3485762000);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<URemeshMeshToolBuilder>()
	{
		return URemeshMeshToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URemeshMeshToolBuilder(Z_Construct_UClass_URemeshMeshToolBuilder, &URemeshMeshToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("URemeshMeshToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URemeshMeshToolBuilder);
	void URemeshMeshToolProperties::StaticRegisterNativesURemeshMeshToolProperties()
	{
	}
	UClass* Z_Construct_UClass_URemeshMeshToolProperties_NoRegister()
	{
		return URemeshMeshToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_URemeshMeshToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetTriangleCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_TargetTriangleCount;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_SmoothingType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SmoothingType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_SmoothingType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDiscardAttributes_MetaData[];
#endif
		static void NewProp_bDiscardAttributes_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDiscardAttributes;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowWireframe_MetaData[];
#endif
		static void NewProp_bShowWireframe_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowWireframe;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowGroupColors_MetaData[];
#endif
		static void NewProp_bShowGroupColors_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowGroupColors;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_RemeshType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RemeshType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_RemeshType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RemeshIterations_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_RemeshIterations;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseTargetEdgeLength_MetaData[];
#endif
		static void NewProp_bUseTargetEdgeLength_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseTargetEdgeLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetEdgeLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TargetEdgeLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bReproject_MetaData[];
#endif
		static void NewProp_bReproject_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bReproject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URemeshMeshToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_URemeshProperties,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemeshMeshToolProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Standard properties of the Remesh operation\n */" },
		{ "IncludePath", "RemeshMeshTool.h" },
		{ "ModuleRelativePath", "Public/RemeshMeshTool.h" },
		{ "ToolTip", "Standard properties of the Remesh operation" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_TargetTriangleCount_MetaData[] = {
		{ "Category", "Remeshing" },
		{ "Comment", "/** Target triangle count */" },
		{ "EditCondition", "bUseTargetEdgeLength == false" },
		{ "ModuleRelativePath", "Public/RemeshMeshTool.h" },
		{ "ToolTip", "Target triangle count" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_TargetTriangleCount = { "TargetTriangleCount", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemeshMeshToolProperties, TargetTriangleCount), METADATA_PARAMS(Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_TargetTriangleCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_TargetTriangleCount_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_SmoothingType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_SmoothingType_MetaData[] = {
		{ "Category", "Remeshing" },
		{ "Comment", "/** Smoothing type */" },
		{ "ModuleRelativePath", "Public/RemeshMeshTool.h" },
		{ "ToolTip", "Smoothing type" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_SmoothingType = { "SmoothingType", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemeshMeshToolProperties, SmoothingType), Z_Construct_UEnum_ModelingOperators_ERemeshSmoothingType, METADATA_PARAMS(Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_SmoothingType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_SmoothingType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_bDiscardAttributes_MetaData[] = {
		{ "Category", "Remeshing" },
		{ "Comment", "/** If true, UVs and Normals are discarded  */" },
		{ "ModuleRelativePath", "Public/RemeshMeshTool.h" },
		{ "ToolTip", "If true, UVs and Normals are discarded" },
	};
#endif
	void Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_bDiscardAttributes_SetBit(void* Obj)
	{
		((URemeshMeshToolProperties*)Obj)->bDiscardAttributes = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_bDiscardAttributes = { "bDiscardAttributes", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URemeshMeshToolProperties), &Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_bDiscardAttributes_SetBit, METADATA_PARAMS(Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_bDiscardAttributes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_bDiscardAttributes_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_bShowWireframe_MetaData[] = {
		{ "Category", "Display" },
		{ "Comment", "/** If true, display wireframe */" },
		{ "ModuleRelativePath", "Public/RemeshMeshTool.h" },
		{ "ToolTip", "If true, display wireframe" },
	};
#endif
	void Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_bShowWireframe_SetBit(void* Obj)
	{
		((URemeshMeshToolProperties*)Obj)->bShowWireframe = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_bShowWireframe = { "bShowWireframe", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URemeshMeshToolProperties), &Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_bShowWireframe_SetBit, METADATA_PARAMS(Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_bShowWireframe_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_bShowWireframe_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_bShowGroupColors_MetaData[] = {
		{ "Category", "Display" },
		{ "Comment", "/** Display colors corresponding to the mesh's polygon groups */" },
		{ "ModuleRelativePath", "Public/RemeshMeshTool.h" },
		{ "ToolTip", "Display colors corresponding to the mesh's polygon groups" },
	};
#endif
	void Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_bShowGroupColors_SetBit(void* Obj)
	{
		((URemeshMeshToolProperties*)Obj)->bShowGroupColors = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_bShowGroupColors = { "bShowGroupColors", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URemeshMeshToolProperties), &Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_bShowGroupColors_SetBit, METADATA_PARAMS(Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_bShowGroupColors_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_bShowGroupColors_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_RemeshType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_RemeshType_MetaData[] = {
		{ "Category", "Remeshing" },
		{ "Comment", "/** Remeshing type */" },
		{ "ModuleRelativePath", "Public/RemeshMeshTool.h" },
		{ "ToolTip", "Remeshing type" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_RemeshType = { "RemeshType", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemeshMeshToolProperties, RemeshType), Z_Construct_UEnum_ModelingOperators_ERemeshType, METADATA_PARAMS(Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_RemeshType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_RemeshType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_RemeshIterations_MetaData[] = {
		{ "Category", "Remeshing" },
		{ "ClampMax", "1000" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Number of Remeshing passes */" },
		{ "EditCondition", "RemeshType == ERemeshType::FullPass" },
		{ "ModuleRelativePath", "Public/RemeshMeshTool.h" },
		{ "ToolTip", "Number of Remeshing passes" },
		{ "UIMax", "50" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_RemeshIterations = { "RemeshIterations", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemeshMeshToolProperties, RemeshIterations), METADATA_PARAMS(Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_RemeshIterations_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_RemeshIterations_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_bUseTargetEdgeLength_MetaData[] = {
		{ "Category", "Remeshing" },
		{ "Comment", "/** If true, the target count is ignored and the target edge length is used directly */" },
		{ "ModuleRelativePath", "Public/RemeshMeshTool.h" },
		{ "ToolTip", "If true, the target count is ignored and the target edge length is used directly" },
	};
#endif
	void Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_bUseTargetEdgeLength_SetBit(void* Obj)
	{
		((URemeshMeshToolProperties*)Obj)->bUseTargetEdgeLength = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_bUseTargetEdgeLength = { "bUseTargetEdgeLength", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URemeshMeshToolProperties), &Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_bUseTargetEdgeLength_SetBit, METADATA_PARAMS(Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_bUseTargetEdgeLength_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_bUseTargetEdgeLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_TargetEdgeLength_MetaData[] = {
		{ "Category", "Remeshing" },
		{ "Comment", "/** Target edge length */" },
		{ "EditCondition", "bUseTargetEdgeLength == true" },
		{ "ModuleRelativePath", "Public/RemeshMeshTool.h" },
		{ "NoSpinbox", "true" },
		{ "ToolTip", "Target edge length" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_TargetEdgeLength = { "TargetEdgeLength", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemeshMeshToolProperties, TargetEdgeLength), METADATA_PARAMS(Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_TargetEdgeLength_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_TargetEdgeLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_bReproject_MetaData[] = {
		{ "Category", "Remeshing" },
		{ "Comment", "/** Enable projection back to input mesh */" },
		{ "ModuleRelativePath", "Public/RemeshMeshTool.h" },
		{ "ToolTip", "Enable projection back to input mesh" },
	};
#endif
	void Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_bReproject_SetBit(void* Obj)
	{
		((URemeshMeshToolProperties*)Obj)->bReproject = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_bReproject = { "bReproject", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URemeshMeshToolProperties), &Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_bReproject_SetBit, METADATA_PARAMS(Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_bReproject_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_bReproject_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URemeshMeshToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_TargetTriangleCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_SmoothingType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_SmoothingType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_bDiscardAttributes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_bShowWireframe,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_bShowGroupColors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_RemeshType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_RemeshType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_RemeshIterations,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_bUseTargetEdgeLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_TargetEdgeLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemeshMeshToolProperties_Statics::NewProp_bReproject,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URemeshMeshToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URemeshMeshToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URemeshMeshToolProperties_Statics::ClassParams = {
		&URemeshMeshToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URemeshMeshToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URemeshMeshToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_URemeshMeshToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URemeshMeshToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URemeshMeshToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URemeshMeshToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URemeshMeshToolProperties, 4008722566);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<URemeshMeshToolProperties>()
	{
		return URemeshMeshToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URemeshMeshToolProperties(Z_Construct_UClass_URemeshMeshToolProperties, &URemeshMeshToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("URemeshMeshToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URemeshMeshToolProperties);
	void URemeshMeshTool::StaticRegisterNativesURemeshMeshTool()
	{
	}
	UClass* Z_Construct_UClass_URemeshMeshTool_NoRegister()
	{
		return URemeshMeshTool::StaticClass();
	}
	struct Z_Construct_UClass_URemeshMeshTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BasicProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BasicProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshStatisticsProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MeshStatisticsProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Preview_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Preview;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URemeshMeshTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMultiSelectionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemeshMeshTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Simple Mesh Remeshing Tool\n *\n * Note this is a subclass of UMultiSelectionTool, however we currently only ever apply it to one mesh at a time. The\n * function URemeshMeshToolBuilder::CanBuildTool will return true only when a single mesh is selected, and the tool will\n * only be applied to the first mesh in the selection list. The reason we inherit from UMultiSelectionTool is so \n * that subclasses of this class can work with multiple meshes (see, for example, UProjectToTargetTool.)\n */" },
		{ "IncludePath", "RemeshMeshTool.h" },
		{ "ModuleRelativePath", "Public/RemeshMeshTool.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Simple Mesh Remeshing Tool\n\nNote this is a subclass of UMultiSelectionTool, however we currently only ever apply it to one mesh at a time. The\nfunction URemeshMeshToolBuilder::CanBuildTool will return true only when a single mesh is selected, and the tool will\nonly be applied to the first mesh in the selection list. The reason we inherit from UMultiSelectionTool is so\nthat subclasses of this class can work with multiple meshes (see, for example, UProjectToTargetTool.)" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemeshMeshTool_Statics::NewProp_BasicProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/RemeshMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_URemeshMeshTool_Statics::NewProp_BasicProperties = { "BasicProperties", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemeshMeshTool, BasicProperties), Z_Construct_UClass_URemeshMeshToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_URemeshMeshTool_Statics::NewProp_BasicProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemeshMeshTool_Statics::NewProp_BasicProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemeshMeshTool_Statics::NewProp_MeshStatisticsProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/RemeshMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_URemeshMeshTool_Statics::NewProp_MeshStatisticsProperties = { "MeshStatisticsProperties", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemeshMeshTool, MeshStatisticsProperties), Z_Construct_UClass_UMeshStatisticsProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_URemeshMeshTool_Statics::NewProp_MeshStatisticsProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemeshMeshTool_Statics::NewProp_MeshStatisticsProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemeshMeshTool_Statics::NewProp_Preview_MetaData[] = {
		{ "ModuleRelativePath", "Public/RemeshMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_URemeshMeshTool_Statics::NewProp_Preview = { "Preview", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemeshMeshTool, Preview), Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_NoRegister, METADATA_PARAMS(Z_Construct_UClass_URemeshMeshTool_Statics::NewProp_Preview_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemeshMeshTool_Statics::NewProp_Preview_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URemeshMeshTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemeshMeshTool_Statics::NewProp_BasicProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemeshMeshTool_Statics::NewProp_MeshStatisticsProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemeshMeshTool_Statics::NewProp_Preview,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URemeshMeshTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URemeshMeshTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URemeshMeshTool_Statics::ClassParams = {
		&URemeshMeshTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URemeshMeshTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URemeshMeshTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_URemeshMeshTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URemeshMeshTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URemeshMeshTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URemeshMeshTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URemeshMeshTool, 3070839534);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<URemeshMeshTool>()
	{
		return URemeshMeshTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URemeshMeshTool(Z_Construct_UClass_URemeshMeshTool, &URemeshMeshTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("URemeshMeshTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URemeshMeshTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
