// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MODELINGCOMPONENTS_CurveControlPointsMechanic_generated_h
#error "CurveControlPointsMechanic.generated.h already included, missing '#pragma once' in CurveControlPointsMechanic.h"
#endif
#define MODELINGCOMPONENTS_CurveControlPointsMechanic_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_CurveControlPointsMechanic_h_45_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_CurveControlPointsMechanic_h_45_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_CurveControlPointsMechanic_h_45_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_CurveControlPointsMechanic_h_45_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCurveControlPointsMechanic(); \
	friend struct Z_Construct_UClass_UCurveControlPointsMechanic_Statics; \
public: \
	DECLARE_CLASS(UCurveControlPointsMechanic, UInteractionMechanic, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UCurveControlPointsMechanic)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_CurveControlPointsMechanic_h_45_INCLASS \
private: \
	static void StaticRegisterNativesUCurveControlPointsMechanic(); \
	friend struct Z_Construct_UClass_UCurveControlPointsMechanic_Statics; \
public: \
	DECLARE_CLASS(UCurveControlPointsMechanic, UInteractionMechanic, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UCurveControlPointsMechanic)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_CurveControlPointsMechanic_h_45_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCurveControlPointsMechanic(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCurveControlPointsMechanic) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCurveControlPointsMechanic); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCurveControlPointsMechanic); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCurveControlPointsMechanic(UCurveControlPointsMechanic&&); \
	NO_API UCurveControlPointsMechanic(const UCurveControlPointsMechanic&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_CurveControlPointsMechanic_h_45_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCurveControlPointsMechanic() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCurveControlPointsMechanic(UCurveControlPointsMechanic&&); \
	NO_API UCurveControlPointsMechanic(const UCurveControlPointsMechanic&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCurveControlPointsMechanic); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCurveControlPointsMechanic); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UCurveControlPointsMechanic)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_CurveControlPointsMechanic_h_45_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__PreviewGeometryActor() { return STRUCT_OFFSET(UCurveControlPointsMechanic, PreviewGeometryActor); } \
	FORCEINLINE static uint32 __PPO__DrawnControlPoints() { return STRUCT_OFFSET(UCurveControlPointsMechanic, DrawnControlPoints); } \
	FORCEINLINE static uint32 __PPO__DrawnControlSegments() { return STRUCT_OFFSET(UCurveControlPointsMechanic, DrawnControlSegments); } \
	FORCEINLINE static uint32 __PPO__PreviewPoint() { return STRUCT_OFFSET(UCurveControlPointsMechanic, PreviewPoint); } \
	FORCEINLINE static uint32 __PPO__PreviewSegment() { return STRUCT_OFFSET(UCurveControlPointsMechanic, PreviewSegment); } \
	FORCEINLINE static uint32 __PPO__PointTransformProxy() { return STRUCT_OFFSET(UCurveControlPointsMechanic, PointTransformProxy); } \
	FORCEINLINE static uint32 __PPO__PointTransformGizmo() { return STRUCT_OFFSET(UCurveControlPointsMechanic, PointTransformGizmo); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_CurveControlPointsMechanic_h_42_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_CurveControlPointsMechanic_h_45_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_CurveControlPointsMechanic_h_45_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_CurveControlPointsMechanic_h_45_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_CurveControlPointsMechanic_h_45_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_CurveControlPointsMechanic_h_45_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_CurveControlPointsMechanic_h_45_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_CurveControlPointsMechanic_h_45_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_CurveControlPointsMechanic_h_45_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_CurveControlPointsMechanic_h_45_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_CurveControlPointsMechanic_h_45_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_CurveControlPointsMechanic_h_45_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_CurveControlPointsMechanic_h_45_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MODELINGCOMPONENTS_API UClass* StaticClass<class UCurveControlPointsMechanic>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_CurveControlPointsMechanic_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
