// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/DrawPolygonTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDrawPolygonTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EDrawPolygonOutputMode();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EDrawPolygonDrawMode();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDrawPolygonToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDrawPolygonToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDrawPolygonToolStandardProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDrawPolygonToolStandardProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDrawPolygonToolSnapProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDrawPolygonToolSnapProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDrawPolygonTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDrawPolygonTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveTool();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UNewMeshMaterialProperties_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPreviewMesh_NoRegister();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UTransformGizmo_NoRegister();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UTransformProxy_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPlaneDistanceFromHitMechanic_NoRegister();
// End Cross Module References
	static UEnum* EDrawPolygonOutputMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EDrawPolygonOutputMode, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EDrawPolygonOutputMode"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EDrawPolygonOutputMode>()
	{
		return EDrawPolygonOutputMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDrawPolygonOutputMode(EDrawPolygonOutputMode_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EDrawPolygonOutputMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EDrawPolygonOutputMode_Hash() { return 1891830762U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EDrawPolygonOutputMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDrawPolygonOutputMode"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EDrawPolygonOutputMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDrawPolygonOutputMode::MeshedPolygon", (int64)EDrawPolygonOutputMode::MeshedPolygon },
				{ "EDrawPolygonOutputMode::ExtrudedConstant", (int64)EDrawPolygonOutputMode::ExtrudedConstant },
				{ "EDrawPolygonOutputMode::ExtrudedInteractive", (int64)EDrawPolygonOutputMode::ExtrudedInteractive },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** Output of Draw Polygon Tool */" },
				{ "ExtrudedConstant.Comment", "/** Extrude closed polygon to constant height determined by Extrude Height Property */" },
				{ "ExtrudedConstant.DisplayName", "Extrude To Height" },
				{ "ExtrudedConstant.Name", "EDrawPolygonOutputMode::ExtrudedConstant" },
				{ "ExtrudedConstant.ToolTip", "Extrude closed polygon to constant height determined by Extrude Height Property" },
				{ "ExtrudedInteractive.Comment", "/** Extrusion height is set via additional mouse input after closing polygon */" },
				{ "ExtrudedInteractive.DisplayName", "Interactive Extrude" },
				{ "ExtrudedInteractive.Name", "EDrawPolygonOutputMode::ExtrudedInteractive" },
				{ "ExtrudedInteractive.ToolTip", "Extrusion height is set via additional mouse input after closing polygon" },
				{ "MeshedPolygon.Comment", "/** Generate a meshed planar polygon */" },
				{ "MeshedPolygon.DisplayName", "Flat Mesh" },
				{ "MeshedPolygon.Name", "EDrawPolygonOutputMode::MeshedPolygon" },
				{ "MeshedPolygon.ToolTip", "Generate a meshed planar polygon" },
				{ "ModuleRelativePath", "Public/DrawPolygonTool.h" },
				{ "ToolTip", "Output of Draw Polygon Tool" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EDrawPolygonOutputMode",
				"EDrawPolygonOutputMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EDrawPolygonDrawMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EDrawPolygonDrawMode, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EDrawPolygonDrawMode"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EDrawPolygonDrawMode>()
	{
		return EDrawPolygonDrawMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDrawPolygonDrawMode(EDrawPolygonDrawMode_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EDrawPolygonDrawMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EDrawPolygonDrawMode_Hash() { return 3785004174U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EDrawPolygonDrawMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDrawPolygonDrawMode"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EDrawPolygonDrawMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDrawPolygonDrawMode::Freehand", (int64)EDrawPolygonDrawMode::Freehand },
				{ "EDrawPolygonDrawMode::Circle", (int64)EDrawPolygonDrawMode::Circle },
				{ "EDrawPolygonDrawMode::Square", (int64)EDrawPolygonDrawMode::Square },
				{ "EDrawPolygonDrawMode::Rectangle", (int64)EDrawPolygonDrawMode::Rectangle },
				{ "EDrawPolygonDrawMode::RoundedRectangle", (int64)EDrawPolygonDrawMode::RoundedRectangle },
				{ "EDrawPolygonDrawMode::HoleyCircle", (int64)EDrawPolygonDrawMode::HoleyCircle },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Circle.Comment", "/** Circle */" },
				{ "Circle.DisplayName", "Circle" },
				{ "Circle.Name", "EDrawPolygonDrawMode::Circle" },
				{ "Circle.ToolTip", "Circle" },
				{ "Comment", "/** Polygon Tool Draw Type */" },
				{ "Freehand.Comment", "/** Freehand Polygon Drawing */" },
				{ "Freehand.DisplayName", "Freehand" },
				{ "Freehand.Name", "EDrawPolygonDrawMode::Freehand" },
				{ "Freehand.ToolTip", "Freehand Polygon Drawing" },
				{ "HoleyCircle.Comment", "/** Circle w/ Hole */" },
				{ "HoleyCircle.DisplayName", "Circle w/ Hole" },
				{ "HoleyCircle.Name", "EDrawPolygonDrawMode::HoleyCircle" },
				{ "HoleyCircle.ToolTip", "Circle w/ Hole" },
				{ "ModuleRelativePath", "Public/DrawPolygonTool.h" },
				{ "Rectangle.Comment", "/** Rectangle */" },
				{ "Rectangle.DisplayName", "Rectangle" },
				{ "Rectangle.Name", "EDrawPolygonDrawMode::Rectangle" },
				{ "Rectangle.ToolTip", "Rectangle" },
				{ "RoundedRectangle.Comment", "/** Rounded Rectangle */" },
				{ "RoundedRectangle.DisplayName", "Rounded Rectangle" },
				{ "RoundedRectangle.Name", "EDrawPolygonDrawMode::RoundedRectangle" },
				{ "RoundedRectangle.ToolTip", "Rounded Rectangle" },
				{ "Square.Comment", "/** Square */" },
				{ "Square.DisplayName", "Square" },
				{ "Square.Name", "EDrawPolygonDrawMode::Square" },
				{ "Square.ToolTip", "Square" },
				{ "ToolTip", "Polygon Tool Draw Type" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EDrawPolygonDrawMode",
				"EDrawPolygonDrawMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UDrawPolygonToolBuilder::StaticRegisterNativesUDrawPolygonToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UDrawPolygonToolBuilder_NoRegister()
	{
		return UDrawPolygonToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UDrawPolygonToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDrawPolygonToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolygonToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "DrawPolygonTool.h" },
		{ "ModuleRelativePath", "Public/DrawPolygonTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDrawPolygonToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDrawPolygonToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDrawPolygonToolBuilder_Statics::ClassParams = {
		&UDrawPolygonToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UDrawPolygonToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolygonToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDrawPolygonToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDrawPolygonToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDrawPolygonToolBuilder, 690847622);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UDrawPolygonToolBuilder>()
	{
		return UDrawPolygonToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDrawPolygonToolBuilder(Z_Construct_UClass_UDrawPolygonToolBuilder, &UDrawPolygonToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UDrawPolygonToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDrawPolygonToolBuilder);
	void UDrawPolygonToolStandardProperties::StaticRegisterNativesUDrawPolygonToolStandardProperties()
	{
	}
	UClass* Z_Construct_UClass_UDrawPolygonToolStandardProperties_NoRegister()
	{
		return UDrawPolygonToolStandardProperties::StaticClass();
	}
	struct Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_PolygonType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PolygonType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_PolygonType;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_OutputMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OutputMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_OutputMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FeatureSizeRatio_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FeatureSizeRatio;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExtrudeHeight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ExtrudeHeight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Steps_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Steps;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAllowSelfIntersections_MetaData[];
#endif
		static void NewProp_bAllowSelfIntersections_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAllowSelfIntersections;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowGizmo_MetaData[];
#endif
		static void NewProp_bShowGizmo_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowGizmo;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "DrawPolygonTool.h" },
		{ "ModuleRelativePath", "Public/DrawPolygonTool.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_PolygonType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_PolygonType_MetaData[] = {
		{ "Category", "Polygon" },
		{ "ModuleRelativePath", "Public/DrawPolygonTool.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_PolygonType = { "PolygonType", nullptr, (EPropertyFlags)0x0010000400000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawPolygonToolStandardProperties, PolygonType), Z_Construct_UEnum_MeshModelingTools_EDrawPolygonDrawMode, METADATA_PARAMS(Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_PolygonType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_PolygonType_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_OutputMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_OutputMode_MetaData[] = {
		{ "Category", "Polygon" },
		{ "ModuleRelativePath", "Public/DrawPolygonTool.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_OutputMode = { "OutputMode", nullptr, (EPropertyFlags)0x0010000400000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawPolygonToolStandardProperties, OutputMode), Z_Construct_UEnum_MeshModelingTools_EDrawPolygonOutputMode, METADATA_PARAMS(Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_OutputMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_OutputMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_FeatureSizeRatio_MetaData[] = {
		{ "Category", "Polygon" },
		{ "ClampMax", "0.99" },
		{ "ClampMin", "0.01" },
		{ "Comment", "/** Feature size as fraction of overall shape size, for shapes with secondary features like the rounded corners of a Rounded Rectangle */" },
		{ "EditCondition", "PolygonType == EDrawPolygonDrawMode::RoundedRectangle || PolygonType == EDrawPolygonDrawMode::HoleyCircle" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/DrawPolygonTool.h" },
		{ "ToolTip", "Feature size as fraction of overall shape size, for shapes with secondary features like the rounded corners of a Rounded Rectangle" },
		{ "UIMax", "0.99" },
		{ "UIMin", "0.01" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_FeatureSizeRatio = { "FeatureSizeRatio", nullptr, (EPropertyFlags)0x0010000400000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawPolygonToolStandardProperties, FeatureSizeRatio), METADATA_PARAMS(Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_FeatureSizeRatio_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_FeatureSizeRatio_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_ExtrudeHeight_MetaData[] = {
		{ "Category", "Polygon" },
		{ "ClampMax", "10000" },
		{ "ClampMin", "-10000" },
		{ "Comment", "/** Extrusion Distance in non-interactive mode */" },
		{ "EditCondition", "OutputMode == EDrawPolygonOutputMode::ExtrudedConstant" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/DrawPolygonTool.h" },
		{ "ToolTip", "Extrusion Distance in non-interactive mode" },
		{ "UIMax", "1000" },
		{ "UIMin", "-1000" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_ExtrudeHeight = { "ExtrudeHeight", nullptr, (EPropertyFlags)0x0010000400000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawPolygonToolStandardProperties, ExtrudeHeight), METADATA_PARAMS(Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_ExtrudeHeight_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_ExtrudeHeight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_Steps_MetaData[] = {
		{ "Category", "Polygon" },
		{ "ClampMax", "10000" },
		{ "ClampMin", "3" },
		{ "Comment", "/** Number of sections in round features */" },
		{ "EditCondition", "PolygonType == EDrawPolygonDrawMode::Circle || PolygonType == EDrawPolygonDrawMode::RoundedRectangle || PolygonType == EDrawPolygonDrawMode::HoleyCircle" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/DrawPolygonTool.h" },
		{ "ToolTip", "Number of sections in round features" },
		{ "UIMax", "100" },
		{ "UIMin", "3" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_Steps = { "Steps", nullptr, (EPropertyFlags)0x0010000400000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawPolygonToolStandardProperties, Steps), METADATA_PARAMS(Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_Steps_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_Steps_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_bAllowSelfIntersections_MetaData[] = {
		{ "Category", "Polygon" },
		{ "EditCondition", "PolygonType == EDrawPolygonDrawMode::Freehand" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/DrawPolygonTool.h" },
	};
#endif
	void Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_bAllowSelfIntersections_SetBit(void* Obj)
	{
		((UDrawPolygonToolStandardProperties*)Obj)->bAllowSelfIntersections = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_bAllowSelfIntersections = { "bAllowSelfIntersections", nullptr, (EPropertyFlags)0x0010000400000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDrawPolygonToolStandardProperties), &Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_bAllowSelfIntersections_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_bAllowSelfIntersections_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_bAllowSelfIntersections_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_bShowGizmo_MetaData[] = {
		{ "Category", "Polygon" },
		{ "ModuleRelativePath", "Public/DrawPolygonTool.h" },
	};
#endif
	void Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_bShowGizmo_SetBit(void* Obj)
	{
		((UDrawPolygonToolStandardProperties*)Obj)->bShowGizmo = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_bShowGizmo = { "bShowGizmo", nullptr, (EPropertyFlags)0x0010000400000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDrawPolygonToolStandardProperties), &Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_bShowGizmo_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_bShowGizmo_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_bShowGizmo_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_PolygonType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_PolygonType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_OutputMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_OutputMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_FeatureSizeRatio,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_ExtrudeHeight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_Steps,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_bAllowSelfIntersections,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::NewProp_bShowGizmo,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDrawPolygonToolStandardProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::ClassParams = {
		&UDrawPolygonToolStandardProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDrawPolygonToolStandardProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDrawPolygonToolStandardProperties, 1797035661);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UDrawPolygonToolStandardProperties>()
	{
		return UDrawPolygonToolStandardProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDrawPolygonToolStandardProperties(Z_Construct_UClass_UDrawPolygonToolStandardProperties, &UDrawPolygonToolStandardProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UDrawPolygonToolStandardProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDrawPolygonToolStandardProperties);
	void UDrawPolygonToolSnapProperties::StaticRegisterNativesUDrawPolygonToolSnapProperties()
	{
	}
	UClass* Z_Construct_UClass_UDrawPolygonToolSnapProperties_NoRegister()
	{
		return UDrawPolygonToolSnapProperties::StaticClass();
	}
	struct Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableSnapping_MetaData[];
#endif
		static void NewProp_bEnableSnapping_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableSnapping;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSnapToWorldGrid_MetaData[];
#endif
		static void NewProp_bSnapToWorldGrid_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSnapToWorldGrid;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSnapToVertices_MetaData[];
#endif
		static void NewProp_bSnapToVertices_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSnapToVertices;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSnapToEdges_MetaData[];
#endif
		static void NewProp_bSnapToEdges_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSnapToEdges;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSnapToAngles_MetaData[];
#endif
		static void NewProp_bSnapToAngles_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSnapToAngles;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSnapToLengths_MetaData[];
#endif
		static void NewProp_bSnapToLengths_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSnapToLengths;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SegmentLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SegmentLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bHitSceneObjects_MetaData[];
#endif
		static void NewProp_bHitSceneObjects_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bHitSceneObjects;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HitNormalOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HitNormalOffset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "DrawPolygonTool.h" },
		{ "ModuleRelativePath", "Public/DrawPolygonTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bEnableSnapping_MetaData[] = {
		{ "Category", "Snapping" },
		{ "ModuleRelativePath", "Public/DrawPolygonTool.h" },
	};
#endif
	void Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bEnableSnapping_SetBit(void* Obj)
	{
		((UDrawPolygonToolSnapProperties*)Obj)->bEnableSnapping = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bEnableSnapping = { "bEnableSnapping", nullptr, (EPropertyFlags)0x0010000400000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDrawPolygonToolSnapProperties), &Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bEnableSnapping_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bEnableSnapping_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bEnableSnapping_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bSnapToWorldGrid_MetaData[] = {
		{ "Category", "Snapping" },
		{ "EditCondition", "bEnableSnapping" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/DrawPolygonTool.h" },
	};
#endif
	void Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bSnapToWorldGrid_SetBit(void* Obj)
	{
		((UDrawPolygonToolSnapProperties*)Obj)->bSnapToWorldGrid = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bSnapToWorldGrid = { "bSnapToWorldGrid", nullptr, (EPropertyFlags)0x0010000400000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDrawPolygonToolSnapProperties), &Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bSnapToWorldGrid_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bSnapToWorldGrid_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bSnapToWorldGrid_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bSnapToVertices_MetaData[] = {
		{ "Category", "Snapping" },
		{ "EditCondition", "bEnableSnapping" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/DrawPolygonTool.h" },
	};
#endif
	void Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bSnapToVertices_SetBit(void* Obj)
	{
		((UDrawPolygonToolSnapProperties*)Obj)->bSnapToVertices = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bSnapToVertices = { "bSnapToVertices", nullptr, (EPropertyFlags)0x0010000400000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDrawPolygonToolSnapProperties), &Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bSnapToVertices_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bSnapToVertices_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bSnapToVertices_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bSnapToEdges_MetaData[] = {
		{ "Category", "Snapping" },
		{ "EditCondition", "bEnableSnapping" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/DrawPolygonTool.h" },
	};
#endif
	void Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bSnapToEdges_SetBit(void* Obj)
	{
		((UDrawPolygonToolSnapProperties*)Obj)->bSnapToEdges = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bSnapToEdges = { "bSnapToEdges", nullptr, (EPropertyFlags)0x0010000400000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDrawPolygonToolSnapProperties), &Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bSnapToEdges_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bSnapToEdges_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bSnapToEdges_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bSnapToAngles_MetaData[] = {
		{ "Category", "Snapping" },
		{ "EditCondition", "bEnableSnapping" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/DrawPolygonTool.h" },
	};
#endif
	void Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bSnapToAngles_SetBit(void* Obj)
	{
		((UDrawPolygonToolSnapProperties*)Obj)->bSnapToAngles = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bSnapToAngles = { "bSnapToAngles", nullptr, (EPropertyFlags)0x0010000400000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDrawPolygonToolSnapProperties), &Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bSnapToAngles_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bSnapToAngles_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bSnapToAngles_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bSnapToLengths_MetaData[] = {
		{ "Category", "Snapping" },
		{ "EditCondition", "bEnableSnapping" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/DrawPolygonTool.h" },
	};
#endif
	void Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bSnapToLengths_SetBit(void* Obj)
	{
		((UDrawPolygonToolSnapProperties*)Obj)->bSnapToLengths = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bSnapToLengths = { "bSnapToLengths", nullptr, (EPropertyFlags)0x0010000400000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDrawPolygonToolSnapProperties), &Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bSnapToLengths_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bSnapToLengths_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bSnapToLengths_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_SegmentLength_MetaData[] = {
		{ "Category", "Snapping" },
		{ "ModuleRelativePath", "Public/DrawPolygonTool.h" },
		{ "TransientToolProperty", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_SegmentLength = { "SegmentLength", nullptr, (EPropertyFlags)0x0010000400020001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawPolygonToolSnapProperties, SegmentLength), METADATA_PARAMS(Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_SegmentLength_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_SegmentLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bHitSceneObjects_MetaData[] = {
		{ "Category", "Snapping" },
		{ "ModuleRelativePath", "Public/DrawPolygonTool.h" },
	};
#endif
	void Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bHitSceneObjects_SetBit(void* Obj)
	{
		((UDrawPolygonToolSnapProperties*)Obj)->bHitSceneObjects = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bHitSceneObjects = { "bHitSceneObjects", nullptr, (EPropertyFlags)0x0010000400000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDrawPolygonToolSnapProperties), &Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bHitSceneObjects_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bHitSceneObjects_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bHitSceneObjects_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_HitNormalOffset_MetaData[] = {
		{ "Category", "Snapping" },
		{ "EditCondition", "bHitSceneObjects" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/DrawPolygonTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_HitNormalOffset = { "HitNormalOffset", nullptr, (EPropertyFlags)0x0010000400000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawPolygonToolSnapProperties, HitNormalOffset), METADATA_PARAMS(Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_HitNormalOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_HitNormalOffset_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bEnableSnapping,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bSnapToWorldGrid,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bSnapToVertices,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bSnapToEdges,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bSnapToAngles,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bSnapToLengths,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_SegmentLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_bHitSceneObjects,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::NewProp_HitNormalOffset,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDrawPolygonToolSnapProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::ClassParams = {
		&UDrawPolygonToolSnapProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDrawPolygonToolSnapProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDrawPolygonToolSnapProperties, 3659772101);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UDrawPolygonToolSnapProperties>()
	{
		return UDrawPolygonToolSnapProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDrawPolygonToolSnapProperties(Z_Construct_UClass_UDrawPolygonToolSnapProperties, &UDrawPolygonToolSnapProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UDrawPolygonToolSnapProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDrawPolygonToolSnapProperties);
	void UDrawPolygonTool::StaticRegisterNativesUDrawPolygonTool()
	{
	}
	UClass* Z_Construct_UClass_UDrawPolygonTool_NoRegister()
	{
		return UDrawPolygonTool::StaticClass();
	}
	struct Z_Construct_UClass_UDrawPolygonTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PolygonProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PolygonProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SnapProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SnapProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaterialProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MaterialProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviewMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviewMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlaneTransformGizmo_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PlaneTransformGizmo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlaneTransformProxy_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PlaneTransformProxy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HeightMechanic_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_HeightMechanic;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDrawPolygonTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolygonTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * This tool allows the user to draw and extrude 2D polygons\n */" },
		{ "IncludePath", "DrawPolygonTool.h" },
		{ "ModuleRelativePath", "Public/DrawPolygonTool.h" },
		{ "ToolTip", "This tool allows the user to draw and extrude 2D polygons" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolygonTool_Statics::NewProp_PolygonProperties_MetaData[] = {
		{ "Comment", "/** Properties that control polygon generation exposed to user via detailsview */" },
		{ "ModuleRelativePath", "Public/DrawPolygonTool.h" },
		{ "ToolTip", "Properties that control polygon generation exposed to user via detailsview" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDrawPolygonTool_Statics::NewProp_PolygonProperties = { "PolygonProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawPolygonTool, PolygonProperties), Z_Construct_UClass_UDrawPolygonToolStandardProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDrawPolygonTool_Statics::NewProp_PolygonProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolygonTool_Statics::NewProp_PolygonProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolygonTool_Statics::NewProp_SnapProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/DrawPolygonTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDrawPolygonTool_Statics::NewProp_SnapProperties = { "SnapProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawPolygonTool, SnapProperties), Z_Construct_UClass_UDrawPolygonToolSnapProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDrawPolygonTool_Statics::NewProp_SnapProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolygonTool_Statics::NewProp_SnapProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolygonTool_Statics::NewProp_MaterialProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/DrawPolygonTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDrawPolygonTool_Statics::NewProp_MaterialProperties = { "MaterialProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawPolygonTool, MaterialProperties), Z_Construct_UClass_UNewMeshMaterialProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDrawPolygonTool_Statics::NewProp_MaterialProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolygonTool_Statics::NewProp_MaterialProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolygonTool_Statics::NewProp_PreviewMesh_MetaData[] = {
		{ "ModuleRelativePath", "Public/DrawPolygonTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDrawPolygonTool_Statics::NewProp_PreviewMesh = { "PreviewMesh", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawPolygonTool, PreviewMesh), Z_Construct_UClass_UPreviewMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDrawPolygonTool_Statics::NewProp_PreviewMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolygonTool_Statics::NewProp_PreviewMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolygonTool_Statics::NewProp_PlaneTransformGizmo_MetaData[] = {
		{ "Comment", "// drawing plane gizmo\n" },
		{ "ModuleRelativePath", "Public/DrawPolygonTool.h" },
		{ "ToolTip", "drawing plane gizmo" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDrawPolygonTool_Statics::NewProp_PlaneTransformGizmo = { "PlaneTransformGizmo", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawPolygonTool, PlaneTransformGizmo), Z_Construct_UClass_UTransformGizmo_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDrawPolygonTool_Statics::NewProp_PlaneTransformGizmo_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolygonTool_Statics::NewProp_PlaneTransformGizmo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolygonTool_Statics::NewProp_PlaneTransformProxy_MetaData[] = {
		{ "ModuleRelativePath", "Public/DrawPolygonTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDrawPolygonTool_Statics::NewProp_PlaneTransformProxy = { "PlaneTransformProxy", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawPolygonTool, PlaneTransformProxy), Z_Construct_UClass_UTransformProxy_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDrawPolygonTool_Statics::NewProp_PlaneTransformProxy_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolygonTool_Statics::NewProp_PlaneTransformProxy_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolygonTool_Statics::NewProp_HeightMechanic_MetaData[] = {
		{ "ModuleRelativePath", "Public/DrawPolygonTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDrawPolygonTool_Statics::NewProp_HeightMechanic = { "HeightMechanic", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawPolygonTool, HeightMechanic), Z_Construct_UClass_UPlaneDistanceFromHitMechanic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDrawPolygonTool_Statics::NewProp_HeightMechanic_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolygonTool_Statics::NewProp_HeightMechanic_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDrawPolygonTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolygonTool_Statics::NewProp_PolygonProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolygonTool_Statics::NewProp_SnapProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolygonTool_Statics::NewProp_MaterialProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolygonTool_Statics::NewProp_PreviewMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolygonTool_Statics::NewProp_PlaneTransformGizmo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolygonTool_Statics::NewProp_PlaneTransformProxy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolygonTool_Statics::NewProp_HeightMechanic,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDrawPolygonTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDrawPolygonTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDrawPolygonTool_Statics::ClassParams = {
		&UDrawPolygonTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDrawPolygonTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolygonTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UDrawPolygonTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolygonTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDrawPolygonTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDrawPolygonTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDrawPolygonTool, 2940212960);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UDrawPolygonTool>()
	{
		return UDrawPolygonTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDrawPolygonTool(Z_Construct_UClass_UDrawPolygonTool, &UDrawPolygonTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UDrawPolygonTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDrawPolygonTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
