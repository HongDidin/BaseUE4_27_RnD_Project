// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ModelingComponents/Public/Mechanics/PlaneDistanceFromHitMechanic.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePlaneDistanceFromHitMechanic() {}
// Cross Module References
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPlaneDistanceFromHitMechanic_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPlaneDistanceFromHitMechanic();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractionMechanic();
	UPackage* Z_Construct_UPackage__Script_ModelingComponents();
// End Cross Module References
	void UPlaneDistanceFromHitMechanic::StaticRegisterNativesUPlaneDistanceFromHitMechanic()
	{
	}
	UClass* Z_Construct_UClass_UPlaneDistanceFromHitMechanic_NoRegister()
	{
		return UPlaneDistanceFromHitMechanic::StaticClass();
	}
	struct Z_Construct_UClass_UPlaneDistanceFromHitMechanic_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPlaneDistanceFromHitMechanic_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractionMechanic,
		(UObject* (*)())Z_Construct_UPackage__Script_ModelingComponents,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlaneDistanceFromHitMechanic_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * UPlaneDistanceFromHitMechanic implements an interaction where a Height/Distance from a plane\n * is defined by intersecting a ray with a target mesh, and then using that hit point to define the distance.\n * Optionally the hit point can be snapped (eg to a world grid), and also the ray can hit other objects to define the height.\n *\n */" },
		{ "IncludePath", "Mechanics/PlaneDistanceFromHitMechanic.h" },
		{ "ModuleRelativePath", "Public/Mechanics/PlaneDistanceFromHitMechanic.h" },
		{ "ToolTip", "UPlaneDistanceFromHitMechanic implements an interaction where a Height/Distance from a plane\nis defined by intersecting a ray with a target mesh, and then using that hit point to define the distance.\nOptionally the hit point can be snapped (eg to a world grid), and also the ray can hit other objects to define the height." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPlaneDistanceFromHitMechanic_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPlaneDistanceFromHitMechanic>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPlaneDistanceFromHitMechanic_Statics::ClassParams = {
		&UPlaneDistanceFromHitMechanic::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UPlaneDistanceFromHitMechanic_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPlaneDistanceFromHitMechanic_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPlaneDistanceFromHitMechanic()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPlaneDistanceFromHitMechanic_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPlaneDistanceFromHitMechanic, 65869213);
	template<> MODELINGCOMPONENTS_API UClass* StaticClass<UPlaneDistanceFromHitMechanic>()
	{
		return UPlaneDistanceFromHitMechanic::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPlaneDistanceFromHitMechanic(Z_Construct_UClass_UPlaneDistanceFromHitMechanic, &UPlaneDistanceFromHitMechanic::StaticClass, TEXT("/Script/ModelingComponents"), TEXT("UPlaneDistanceFromHitMechanic"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPlaneDistanceFromHitMechanic);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
