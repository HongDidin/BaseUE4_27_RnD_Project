// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Private/Sculpting/KelvinletBrushOp.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeKelvinletBrushOp() {}
// Cross Module References
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UBaseKelvinletBrushOpProps_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UBaseKelvinletBrushOpProps();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshSculptBrushOpProps();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UScaleKelvinletBrushOpProps_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UScaleKelvinletBrushOpProps();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPullKelvinletBrushOpProps_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPullKelvinletBrushOpProps();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_USharpPullKelvinletBrushOpProps_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_USharpPullKelvinletBrushOpProps();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UTwistKelvinletBrushOpProps_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UTwistKelvinletBrushOpProps();
// End Cross Module References
	void UBaseKelvinletBrushOpProps::StaticRegisterNativesUBaseKelvinletBrushOpProps()
	{
	}
	UClass* Z_Construct_UClass_UBaseKelvinletBrushOpProps_NoRegister()
	{
		return UBaseKelvinletBrushOpProps::StaticClass();
	}
	struct Z_Construct_UClass_UBaseKelvinletBrushOpProps_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Stiffness_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Stiffness;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Incompressiblity_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Incompressiblity;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BrushSteps_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_BrushSteps;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBaseKelvinletBrushOpProps_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshSculptBrushOpProps,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBaseKelvinletBrushOpProps_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Sculpting/KelvinletBrushOp.h" },
		{ "ModuleRelativePath", "Private/Sculpting/KelvinletBrushOp.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBaseKelvinletBrushOpProps_Statics::NewProp_Stiffness_MetaData[] = {
		{ "Comment", "/** How much the mesh resists shear */" },
		{ "ModuleRelativePath", "Private/Sculpting/KelvinletBrushOp.h" },
		{ "ToolTip", "How much the mesh resists shear" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UBaseKelvinletBrushOpProps_Statics::NewProp_Stiffness = { "Stiffness", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBaseKelvinletBrushOpProps, Stiffness), METADATA_PARAMS(Z_Construct_UClass_UBaseKelvinletBrushOpProps_Statics::NewProp_Stiffness_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBaseKelvinletBrushOpProps_Statics::NewProp_Stiffness_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBaseKelvinletBrushOpProps_Statics::NewProp_Incompressiblity_MetaData[] = {
		{ "Comment", "/** How compressible the spatial region is: 1 - 2 x Poisson ratio */" },
		{ "ModuleRelativePath", "Private/Sculpting/KelvinletBrushOp.h" },
		{ "ToolTip", "How compressible the spatial region is: 1 - 2 x Poisson ratio" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UBaseKelvinletBrushOpProps_Statics::NewProp_Incompressiblity = { "Incompressiblity", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBaseKelvinletBrushOpProps, Incompressiblity), METADATA_PARAMS(Z_Construct_UClass_UBaseKelvinletBrushOpProps_Statics::NewProp_Incompressiblity_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBaseKelvinletBrushOpProps_Statics::NewProp_Incompressiblity_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBaseKelvinletBrushOpProps_Statics::NewProp_BrushSteps_MetaData[] = {
		{ "Comment", "/** Integration steps*/" },
		{ "ModuleRelativePath", "Private/Sculpting/KelvinletBrushOp.h" },
		{ "ToolTip", "Integration steps" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UBaseKelvinletBrushOpProps_Statics::NewProp_BrushSteps = { "BrushSteps", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBaseKelvinletBrushOpProps, BrushSteps), METADATA_PARAMS(Z_Construct_UClass_UBaseKelvinletBrushOpProps_Statics::NewProp_BrushSteps_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBaseKelvinletBrushOpProps_Statics::NewProp_BrushSteps_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UBaseKelvinletBrushOpProps_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBaseKelvinletBrushOpProps_Statics::NewProp_Stiffness,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBaseKelvinletBrushOpProps_Statics::NewProp_Incompressiblity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBaseKelvinletBrushOpProps_Statics::NewProp_BrushSteps,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBaseKelvinletBrushOpProps_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBaseKelvinletBrushOpProps>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBaseKelvinletBrushOpProps_Statics::ClassParams = {
		&UBaseKelvinletBrushOpProps::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UBaseKelvinletBrushOpProps_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UBaseKelvinletBrushOpProps_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UBaseKelvinletBrushOpProps_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBaseKelvinletBrushOpProps_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBaseKelvinletBrushOpProps()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBaseKelvinletBrushOpProps_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBaseKelvinletBrushOpProps, 3004316684);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UBaseKelvinletBrushOpProps>()
	{
		return UBaseKelvinletBrushOpProps::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBaseKelvinletBrushOpProps(Z_Construct_UClass_UBaseKelvinletBrushOpProps, &UBaseKelvinletBrushOpProps::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UBaseKelvinletBrushOpProps"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBaseKelvinletBrushOpProps);
	void UScaleKelvinletBrushOpProps::StaticRegisterNativesUScaleKelvinletBrushOpProps()
	{
	}
	UClass* Z_Construct_UClass_UScaleKelvinletBrushOpProps_NoRegister()
	{
		return UScaleKelvinletBrushOpProps::StaticClass();
	}
	struct Z_Construct_UClass_UScaleKelvinletBrushOpProps_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Strength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Strength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Falloff_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Falloff;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UScaleKelvinletBrushOpProps_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBaseKelvinletBrushOpProps,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UScaleKelvinletBrushOpProps_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Sculpting/KelvinletBrushOp.h" },
		{ "ModuleRelativePath", "Private/Sculpting/KelvinletBrushOp.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UScaleKelvinletBrushOpProps_Statics::NewProp_Strength_MetaData[] = {
		{ "Category", "KelvinScaleBrush" },
		{ "ClampMax", "10." },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Strength of the Brush */" },
		{ "DisplayName", "Strength" },
		{ "ModuleRelativePath", "Private/Sculpting/KelvinletBrushOp.h" },
		{ "ToolTip", "Strength of the Brush" },
		{ "UIMax", "10." },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UScaleKelvinletBrushOpProps_Statics::NewProp_Strength = { "Strength", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UScaleKelvinletBrushOpProps, Strength), METADATA_PARAMS(Z_Construct_UClass_UScaleKelvinletBrushOpProps_Statics::NewProp_Strength_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UScaleKelvinletBrushOpProps_Statics::NewProp_Strength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UScaleKelvinletBrushOpProps_Statics::NewProp_Falloff_MetaData[] = {
		{ "Category", "KelvinScaleBrush" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Amount of falloff to apply */" },
		{ "DisplayName", "Falloff" },
		{ "ModuleRelativePath", "Private/Sculpting/KelvinletBrushOp.h" },
		{ "ToolTip", "Amount of falloff to apply" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UScaleKelvinletBrushOpProps_Statics::NewProp_Falloff = { "Falloff", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UScaleKelvinletBrushOpProps, Falloff), METADATA_PARAMS(Z_Construct_UClass_UScaleKelvinletBrushOpProps_Statics::NewProp_Falloff_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UScaleKelvinletBrushOpProps_Statics::NewProp_Falloff_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UScaleKelvinletBrushOpProps_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UScaleKelvinletBrushOpProps_Statics::NewProp_Strength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UScaleKelvinletBrushOpProps_Statics::NewProp_Falloff,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UScaleKelvinletBrushOpProps_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UScaleKelvinletBrushOpProps>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UScaleKelvinletBrushOpProps_Statics::ClassParams = {
		&UScaleKelvinletBrushOpProps::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UScaleKelvinletBrushOpProps_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UScaleKelvinletBrushOpProps_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UScaleKelvinletBrushOpProps_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UScaleKelvinletBrushOpProps_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UScaleKelvinletBrushOpProps()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UScaleKelvinletBrushOpProps_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UScaleKelvinletBrushOpProps, 57231304);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UScaleKelvinletBrushOpProps>()
	{
		return UScaleKelvinletBrushOpProps::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UScaleKelvinletBrushOpProps(Z_Construct_UClass_UScaleKelvinletBrushOpProps, &UScaleKelvinletBrushOpProps::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UScaleKelvinletBrushOpProps"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UScaleKelvinletBrushOpProps);
	void UPullKelvinletBrushOpProps::StaticRegisterNativesUPullKelvinletBrushOpProps()
	{
	}
	UClass* Z_Construct_UClass_UPullKelvinletBrushOpProps_NoRegister()
	{
		return UPullKelvinletBrushOpProps::StaticClass();
	}
	struct Z_Construct_UClass_UPullKelvinletBrushOpProps_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Falloff_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Falloff;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Depth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Depth;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPullKelvinletBrushOpProps_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBaseKelvinletBrushOpProps,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPullKelvinletBrushOpProps_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Sculpting/KelvinletBrushOp.h" },
		{ "ModuleRelativePath", "Private/Sculpting/KelvinletBrushOp.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPullKelvinletBrushOpProps_Statics::NewProp_Falloff_MetaData[] = {
		{ "Category", "KelvinGrabBrush" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Amount of falloff to apply */" },
		{ "DisplayName", "Falloff" },
		{ "ModuleRelativePath", "Private/Sculpting/KelvinletBrushOp.h" },
		{ "ToolTip", "Amount of falloff to apply" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UPullKelvinletBrushOpProps_Statics::NewProp_Falloff = { "Falloff", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPullKelvinletBrushOpProps, Falloff), METADATA_PARAMS(Z_Construct_UClass_UPullKelvinletBrushOpProps_Statics::NewProp_Falloff_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPullKelvinletBrushOpProps_Statics::NewProp_Falloff_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPullKelvinletBrushOpProps_Statics::NewProp_Depth_MetaData[] = {
		{ "Category", "KelvinGrabBrush" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "-1.0" },
		{ "Comment", "/** Depth of Brush into surface along view ray */" },
		{ "ModuleRelativePath", "Private/Sculpting/KelvinletBrushOp.h" },
		{ "ToolTip", "Depth of Brush into surface along view ray" },
		{ "UIMax", "0.5" },
		{ "UIMin", "-0.5" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UPullKelvinletBrushOpProps_Statics::NewProp_Depth = { "Depth", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPullKelvinletBrushOpProps, Depth), METADATA_PARAMS(Z_Construct_UClass_UPullKelvinletBrushOpProps_Statics::NewProp_Depth_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPullKelvinletBrushOpProps_Statics::NewProp_Depth_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPullKelvinletBrushOpProps_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPullKelvinletBrushOpProps_Statics::NewProp_Falloff,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPullKelvinletBrushOpProps_Statics::NewProp_Depth,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPullKelvinletBrushOpProps_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPullKelvinletBrushOpProps>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPullKelvinletBrushOpProps_Statics::ClassParams = {
		&UPullKelvinletBrushOpProps::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UPullKelvinletBrushOpProps_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UPullKelvinletBrushOpProps_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UPullKelvinletBrushOpProps_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPullKelvinletBrushOpProps_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPullKelvinletBrushOpProps()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPullKelvinletBrushOpProps_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPullKelvinletBrushOpProps, 3817772179);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UPullKelvinletBrushOpProps>()
	{
		return UPullKelvinletBrushOpProps::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPullKelvinletBrushOpProps(Z_Construct_UClass_UPullKelvinletBrushOpProps, &UPullKelvinletBrushOpProps::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UPullKelvinletBrushOpProps"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPullKelvinletBrushOpProps);
	void USharpPullKelvinletBrushOpProps::StaticRegisterNativesUSharpPullKelvinletBrushOpProps()
	{
	}
	UClass* Z_Construct_UClass_USharpPullKelvinletBrushOpProps_NoRegister()
	{
		return USharpPullKelvinletBrushOpProps::StaticClass();
	}
	struct Z_Construct_UClass_USharpPullKelvinletBrushOpProps_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Falloff_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Falloff;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Depth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Depth;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USharpPullKelvinletBrushOpProps_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBaseKelvinletBrushOpProps,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USharpPullKelvinletBrushOpProps_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Sculpting/KelvinletBrushOp.h" },
		{ "ModuleRelativePath", "Private/Sculpting/KelvinletBrushOp.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USharpPullKelvinletBrushOpProps_Statics::NewProp_Falloff_MetaData[] = {
		{ "Category", "KelvinSharpGrabBrush" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Amount of falloff to apply */" },
		{ "DisplayName", "Falloff" },
		{ "ModuleRelativePath", "Private/Sculpting/KelvinletBrushOp.h" },
		{ "ToolTip", "Amount of falloff to apply" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_USharpPullKelvinletBrushOpProps_Statics::NewProp_Falloff = { "Falloff", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USharpPullKelvinletBrushOpProps, Falloff), METADATA_PARAMS(Z_Construct_UClass_USharpPullKelvinletBrushOpProps_Statics::NewProp_Falloff_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USharpPullKelvinletBrushOpProps_Statics::NewProp_Falloff_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USharpPullKelvinletBrushOpProps_Statics::NewProp_Depth_MetaData[] = {
		{ "Category", "KelvinSharpGrabBrush" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "-1.0" },
		{ "Comment", "/** Depth of Brush into surface along view ray */" },
		{ "ModuleRelativePath", "Private/Sculpting/KelvinletBrushOp.h" },
		{ "ToolTip", "Depth of Brush into surface along view ray" },
		{ "UIMax", "0.5" },
		{ "UIMin", "-0.5" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_USharpPullKelvinletBrushOpProps_Statics::NewProp_Depth = { "Depth", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USharpPullKelvinletBrushOpProps, Depth), METADATA_PARAMS(Z_Construct_UClass_USharpPullKelvinletBrushOpProps_Statics::NewProp_Depth_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USharpPullKelvinletBrushOpProps_Statics::NewProp_Depth_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USharpPullKelvinletBrushOpProps_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USharpPullKelvinletBrushOpProps_Statics::NewProp_Falloff,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USharpPullKelvinletBrushOpProps_Statics::NewProp_Depth,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USharpPullKelvinletBrushOpProps_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USharpPullKelvinletBrushOpProps>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USharpPullKelvinletBrushOpProps_Statics::ClassParams = {
		&USharpPullKelvinletBrushOpProps::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USharpPullKelvinletBrushOpProps_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USharpPullKelvinletBrushOpProps_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_USharpPullKelvinletBrushOpProps_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USharpPullKelvinletBrushOpProps_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USharpPullKelvinletBrushOpProps()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USharpPullKelvinletBrushOpProps_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USharpPullKelvinletBrushOpProps, 3260449448);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<USharpPullKelvinletBrushOpProps>()
	{
		return USharpPullKelvinletBrushOpProps::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USharpPullKelvinletBrushOpProps(Z_Construct_UClass_USharpPullKelvinletBrushOpProps, &USharpPullKelvinletBrushOpProps::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("USharpPullKelvinletBrushOpProps"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USharpPullKelvinletBrushOpProps);
	void UTwistKelvinletBrushOpProps::StaticRegisterNativesUTwistKelvinletBrushOpProps()
	{
	}
	UClass* Z_Construct_UClass_UTwistKelvinletBrushOpProps_NoRegister()
	{
		return UTwistKelvinletBrushOpProps::StaticClass();
	}
	struct Z_Construct_UClass_UTwistKelvinletBrushOpProps_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Strength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Strength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Falloff_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Falloff;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTwistKelvinletBrushOpProps_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBaseKelvinletBrushOpProps,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTwistKelvinletBrushOpProps_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Sculpting/KelvinletBrushOp.h" },
		{ "ModuleRelativePath", "Private/Sculpting/KelvinletBrushOp.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTwistKelvinletBrushOpProps_Statics::NewProp_Strength_MetaData[] = {
		{ "Category", "KelvinTwistBrush" },
		{ "ClampMax", "10." },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Twisting strength of the Brush */" },
		{ "DisplayName", "Strength" },
		{ "ModuleRelativePath", "Private/Sculpting/KelvinletBrushOp.h" },
		{ "ToolTip", "Twisting strength of the Brush" },
		{ "UIMax", "10." },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UTwistKelvinletBrushOpProps_Statics::NewProp_Strength = { "Strength", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTwistKelvinletBrushOpProps, Strength), METADATA_PARAMS(Z_Construct_UClass_UTwistKelvinletBrushOpProps_Statics::NewProp_Strength_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTwistKelvinletBrushOpProps_Statics::NewProp_Strength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTwistKelvinletBrushOpProps_Statics::NewProp_Falloff_MetaData[] = {
		{ "Category", "KelvinTwistBrush" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Amount of falloff to apply */" },
		{ "DisplayName", "Falloff" },
		{ "ModuleRelativePath", "Private/Sculpting/KelvinletBrushOp.h" },
		{ "ToolTip", "Amount of falloff to apply" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UTwistKelvinletBrushOpProps_Statics::NewProp_Falloff = { "Falloff", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTwistKelvinletBrushOpProps, Falloff), METADATA_PARAMS(Z_Construct_UClass_UTwistKelvinletBrushOpProps_Statics::NewProp_Falloff_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTwistKelvinletBrushOpProps_Statics::NewProp_Falloff_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTwistKelvinletBrushOpProps_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTwistKelvinletBrushOpProps_Statics::NewProp_Strength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTwistKelvinletBrushOpProps_Statics::NewProp_Falloff,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTwistKelvinletBrushOpProps_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTwistKelvinletBrushOpProps>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTwistKelvinletBrushOpProps_Statics::ClassParams = {
		&UTwistKelvinletBrushOpProps::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UTwistKelvinletBrushOpProps_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UTwistKelvinletBrushOpProps_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UTwistKelvinletBrushOpProps_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTwistKelvinletBrushOpProps_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTwistKelvinletBrushOpProps()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTwistKelvinletBrushOpProps_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTwistKelvinletBrushOpProps, 3340481516);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UTwistKelvinletBrushOpProps>()
	{
		return UTwistKelvinletBrushOpProps::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTwistKelvinletBrushOpProps(Z_Construct_UClass_UTwistKelvinletBrushOpProps, &UTwistKelvinletBrushOpProps::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UTwistKelvinletBrushOpProps"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTwistKelvinletBrushOpProps);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
