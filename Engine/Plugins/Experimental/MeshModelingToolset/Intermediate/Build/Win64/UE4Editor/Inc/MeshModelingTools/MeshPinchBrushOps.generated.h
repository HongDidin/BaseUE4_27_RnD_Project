// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLS_MeshPinchBrushOps_generated_h
#error "MeshPinchBrushOps.generated.h already included, missing '#pragma once' in MeshPinchBrushOps.h"
#endif
#define MESHMODELINGTOOLS_MeshPinchBrushOps_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshPinchBrushOps_h_15_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshPinchBrushOps_h_15_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshPinchBrushOps_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshPinchBrushOps_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPinchBrushOpProps(); \
	friend struct Z_Construct_UClass_UPinchBrushOpProps_Statics; \
public: \
	DECLARE_CLASS(UPinchBrushOpProps, UMeshSculptBrushOpProps, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UPinchBrushOpProps)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshPinchBrushOps_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUPinchBrushOpProps(); \
	friend struct Z_Construct_UClass_UPinchBrushOpProps_Statics; \
public: \
	DECLARE_CLASS(UPinchBrushOpProps, UMeshSculptBrushOpProps, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UPinchBrushOpProps)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshPinchBrushOps_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPinchBrushOpProps(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPinchBrushOpProps) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPinchBrushOpProps); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPinchBrushOpProps); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPinchBrushOpProps(UPinchBrushOpProps&&); \
	NO_API UPinchBrushOpProps(const UPinchBrushOpProps&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshPinchBrushOps_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPinchBrushOpProps(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPinchBrushOpProps(UPinchBrushOpProps&&); \
	NO_API UPinchBrushOpProps(const UPinchBrushOpProps&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPinchBrushOpProps); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPinchBrushOpProps); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPinchBrushOpProps)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshPinchBrushOps_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshPinchBrushOps_h_12_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshPinchBrushOps_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshPinchBrushOps_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshPinchBrushOps_h_15_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshPinchBrushOps_h_15_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshPinchBrushOps_h_15_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshPinchBrushOps_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshPinchBrushOps_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshPinchBrushOps_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshPinchBrushOps_h_15_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshPinchBrushOps_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshPinchBrushOps_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshPinchBrushOps_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UPinchBrushOpProps>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshPinchBrushOps_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
