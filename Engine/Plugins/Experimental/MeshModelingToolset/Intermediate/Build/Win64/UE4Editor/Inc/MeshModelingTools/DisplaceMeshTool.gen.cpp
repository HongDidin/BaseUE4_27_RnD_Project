// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/DisplaceMeshTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDisplaceMeshTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EDisplaceMeshToolDisplaceType();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UScriptStruct* Z_Construct_UScriptStruct_FPerlinLayerProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDisplaceMeshCommonProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDisplaceMeshCommonProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDisplaceMeshTextureMapProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDisplaceMeshTextureMapProperties();
	ENGINE_API UClass* Z_Construct_UClass_UTexture2D_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDisplaceMeshDirectionalFilterProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDisplaceMeshDirectionalFilterProperties();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDisplaceMeshPerlinNoiseProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDisplaceMeshPerlinNoiseProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDisplaceMeshSineWaveProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDisplaceMeshSineWaveProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDisplaceMeshToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDisplaceMeshToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDisplaceMeshTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDisplaceMeshTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_USingleSelectionTool();
// End Cross Module References
	static UEnum* EDisplaceMeshToolDisplaceType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EDisplaceMeshToolDisplaceType, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EDisplaceMeshToolDisplaceType"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EDisplaceMeshToolDisplaceType>()
	{
		return EDisplaceMeshToolDisplaceType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDisplaceMeshToolDisplaceType(EDisplaceMeshToolDisplaceType_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EDisplaceMeshToolDisplaceType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EDisplaceMeshToolDisplaceType_Hash() { return 2577712527U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EDisplaceMeshToolDisplaceType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDisplaceMeshToolDisplaceType"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EDisplaceMeshToolDisplaceType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDisplaceMeshToolDisplaceType::Constant", (int64)EDisplaceMeshToolDisplaceType::Constant },
				{ "EDisplaceMeshToolDisplaceType::RandomNoise", (int64)EDisplaceMeshToolDisplaceType::RandomNoise },
				{ "EDisplaceMeshToolDisplaceType::PerlinNoise", (int64)EDisplaceMeshToolDisplaceType::PerlinNoise },
				{ "EDisplaceMeshToolDisplaceType::DisplacementMap", (int64)EDisplaceMeshToolDisplaceType::DisplacementMap },
				{ "EDisplaceMeshToolDisplaceType::SineWave", (int64)EDisplaceMeshToolDisplaceType::SineWave },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Constant.Comment", "/** Displace with N iterations */" },
				{ "Constant.DisplayName", "Constant" },
				{ "Constant.Name", "EDisplaceMeshToolDisplaceType::Constant" },
				{ "Constant.ToolTip", "Displace with N iterations" },
				{ "DisplacementMap.Comment", "/** Displace with N iterations */" },
				{ "DisplacementMap.DisplayName", "Texture2D Map" },
				{ "DisplacementMap.Name", "EDisplaceMeshToolDisplaceType::DisplacementMap" },
				{ "DisplacementMap.ToolTip", "Displace with N iterations" },
				{ "ModuleRelativePath", "Public/DisplaceMeshTool.h" },
				{ "PerlinNoise.Comment", "/** Offset in the normal direction weighted by Perlin noise. \n\x09    We use the following formula to compute the weighting for each vertex:\n\x09\x09\x09w = PerlinNoise3D(f * (X + r))\n\x09\x09Where f is a frequency parameter, X is the vertex position, and r is a randomly-generated offset (using the Seed property).\n\x09\x09Note the range of 3D Perlin noise is [-sqrt(3/4), sqrt(3/4)].\n\x09*/" },
				{ "PerlinNoise.DisplayName", "Perlin Noise" },
				{ "PerlinNoise.Name", "EDisplaceMeshToolDisplaceType::PerlinNoise" },
				{ "PerlinNoise.ToolTip", "Offset in the normal direction weighted by Perlin noise.\n          We use the following formula to compute the weighting for each vertex:\n                      w = PerlinNoise3D(f * (X + r))\n              Where f is a frequency parameter, X is the vertex position, and r is a randomly-generated offset (using the Seed property).\n              Note the range of 3D Perlin noise is [-sqrt(3/4), sqrt(3/4)]." },
				{ "RandomNoise.Comment", "/** Displace with N iterations */" },
				{ "RandomNoise.DisplayName", "Random Noise" },
				{ "RandomNoise.Name", "EDisplaceMeshToolDisplaceType::RandomNoise" },
				{ "RandomNoise.ToolTip", "Displace with N iterations" },
				{ "SineWave.Comment", "/** Move vertices in spatial sine wave pattern */" },
				{ "SineWave.DisplayName", "Sine Wave" },
				{ "SineWave.Name", "EDisplaceMeshToolDisplaceType::SineWave" },
				{ "SineWave.ToolTip", "Move vertices in spatial sine wave pattern" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EDisplaceMeshToolDisplaceType",
				"EDisplaceMeshToolDisplaceType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FPerlinLayerProperties::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MESHMODELINGTOOLS_API uint32 Get_Z_Construct_UScriptStruct_FPerlinLayerProperties_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FPerlinLayerProperties, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("PerlinLayerProperties"), sizeof(FPerlinLayerProperties), Get_Z_Construct_UScriptStruct_FPerlinLayerProperties_Hash());
	}
	return Singleton;
}
template<> MESHMODELINGTOOLS_API UScriptStruct* StaticStruct<FPerlinLayerProperties>()
{
	return FPerlinLayerProperties::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FPerlinLayerProperties(FPerlinLayerProperties::StaticStruct, TEXT("/Script/MeshModelingTools"), TEXT("PerlinLayerProperties"), false, nullptr, nullptr);
static struct FScriptStruct_MeshModelingTools_StaticRegisterNativesFPerlinLayerProperties
{
	FScriptStruct_MeshModelingTools_StaticRegisterNativesFPerlinLayerProperties()
	{
		UScriptStruct::DeferCppStructOps<FPerlinLayerProperties>(FName(TEXT("PerlinLayerProperties")));
	}
} ScriptStruct_MeshModelingTools_StaticRegisterNativesFPerlinLayerProperties;
	struct Z_Construct_UScriptStruct_FPerlinLayerProperties_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Frequency_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Frequency;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Intensity_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Intensity;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPerlinLayerProperties_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Per-layer properties for Perlin noise. Each layer has independent Frequency and Intensity. */" },
		{ "ModuleRelativePath", "Public/DisplaceMeshTool.h" },
		{ "ToolTip", "Per-layer properties for Perlin noise. Each layer has independent Frequency and Intensity." },
	};
#endif
	void* Z_Construct_UScriptStruct_FPerlinLayerProperties_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FPerlinLayerProperties>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPerlinLayerProperties_Statics::NewProp_Frequency_MetaData[] = {
		{ "Category", "PerlinNoiseOptions" },
		{ "ClampMax", "10.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Frequency of Perlin noise layer */" },
		{ "ModuleRelativePath", "Public/DisplaceMeshTool.h" },
		{ "ToolTip", "Frequency of Perlin noise layer" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FPerlinLayerProperties_Statics::NewProp_Frequency = { "Frequency", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPerlinLayerProperties, Frequency), METADATA_PARAMS(Z_Construct_UScriptStruct_FPerlinLayerProperties_Statics::NewProp_Frequency_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPerlinLayerProperties_Statics::NewProp_Frequency_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPerlinLayerProperties_Statics::NewProp_Intensity_MetaData[] = {
		{ "Category", "PerlinNoiseOptions" },
		{ "ClampMax", "100.0" },
		{ "ClampMin", "-100.0" },
		{ "Comment", "/** Intensity/amplitude of Perlin noise layer */" },
		{ "ModuleRelativePath", "Public/DisplaceMeshTool.h" },
		{ "ToolTip", "Intensity/amplitude of Perlin noise layer" },
		{ "UIMax", "10.0" },
		{ "UIMin", "-10.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FPerlinLayerProperties_Statics::NewProp_Intensity = { "Intensity", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPerlinLayerProperties, Intensity), METADATA_PARAMS(Z_Construct_UScriptStruct_FPerlinLayerProperties_Statics::NewProp_Intensity_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPerlinLayerProperties_Statics::NewProp_Intensity_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FPerlinLayerProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPerlinLayerProperties_Statics::NewProp_Frequency,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPerlinLayerProperties_Statics::NewProp_Intensity,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FPerlinLayerProperties_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
		nullptr,
		&NewStructOps,
		"PerlinLayerProperties",
		sizeof(FPerlinLayerProperties),
		alignof(FPerlinLayerProperties),
		Z_Construct_UScriptStruct_FPerlinLayerProperties_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPerlinLayerProperties_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FPerlinLayerProperties_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPerlinLayerProperties_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FPerlinLayerProperties()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FPerlinLayerProperties_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("PerlinLayerProperties"), sizeof(FPerlinLayerProperties), Get_Z_Construct_UScriptStruct_FPerlinLayerProperties_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FPerlinLayerProperties_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FPerlinLayerProperties_Hash() { return 1134281358U; }
	DEFINE_FUNCTION(UDisplaceMeshCommonProperties::execGetWeightMapsFunc)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<FString>*)Z_Param__Result=P_THIS->GetWeightMapsFunc();
		P_NATIVE_END;
	}
	void UDisplaceMeshCommonProperties::StaticRegisterNativesUDisplaceMeshCommonProperties()
	{
		UClass* Class = UDisplaceMeshCommonProperties::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetWeightMapsFunc", &UDisplaceMeshCommonProperties::execGetWeightMapsFunc },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UDisplaceMeshCommonProperties_GetWeightMapsFunc_Statics
	{
		struct DisplaceMeshCommonProperties_eventGetWeightMapsFunc_Parms
		{
			TArray<FString> ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplaceMeshCommonProperties_GetWeightMapsFunc_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDisplaceMeshCommonProperties_GetWeightMapsFunc_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplaceMeshCommonProperties_eventGetWeightMapsFunc_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplaceMeshCommonProperties_GetWeightMapsFunc_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplaceMeshCommonProperties_GetWeightMapsFunc_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplaceMeshCommonProperties_GetWeightMapsFunc_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplaceMeshCommonProperties_GetWeightMapsFunc_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/DisplaceMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplaceMeshCommonProperties_GetWeightMapsFunc_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplaceMeshCommonProperties, nullptr, "GetWeightMapsFunc", nullptr, nullptr, sizeof(DisplaceMeshCommonProperties_eventGetWeightMapsFunc_Parms), Z_Construct_UFunction_UDisplaceMeshCommonProperties_GetWeightMapsFunc_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplaceMeshCommonProperties_GetWeightMapsFunc_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplaceMeshCommonProperties_GetWeightMapsFunc_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplaceMeshCommonProperties_GetWeightMapsFunc_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplaceMeshCommonProperties_GetWeightMapsFunc()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplaceMeshCommonProperties_GetWeightMapsFunc_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UDisplaceMeshCommonProperties_NoRegister()
	{
		return UDisplaceMeshCommonProperties::StaticClass();
	}
	struct Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DisplacementType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisplacementType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_DisplacementType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisplaceIntensity_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DisplaceIntensity;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RandomSeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_RandomSeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Subdivisions_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Subdivisions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WeightMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_WeightMap;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_WeightMapsList_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WeightMapsList_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_WeightMapsList;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bInvertWeightMap_MetaData[];
#endif
		static void NewProp_bInvertWeightMap_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInvertWeightMap;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDisableSizeWarning_MetaData[];
#endif
		static void NewProp_bDisableSizeWarning_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDisableSizeWarning;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UDisplaceMeshCommonProperties_GetWeightMapsFunc, "GetWeightMapsFunc" }, // 3716994180
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** The basic set of properties shared by (more or less) all DisplacementTypes. */" },
		{ "IncludePath", "DisplaceMeshTool.h" },
		{ "ModuleRelativePath", "Public/DisplaceMeshTool.h" },
		{ "ToolTip", "The basic set of properties shared by (more or less) all DisplacementTypes." },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_DisplacementType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_DisplacementType_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Displacement type */" },
		{ "ModuleRelativePath", "Public/DisplaceMeshTool.h" },
		{ "ToolTip", "Displacement type" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_DisplacementType = { "DisplacementType", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplaceMeshCommonProperties, DisplacementType), Z_Construct_UEnum_MeshModelingTools_EDisplaceMeshToolDisplaceType, METADATA_PARAMS(Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_DisplacementType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_DisplacementType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_DisplaceIntensity_MetaData[] = {
		{ "Category", "Options" },
		{ "ClampMax", "100000.0" },
		{ "ClampMin", "-10000.0" },
		{ "Comment", "/** Displacement intensity */" },
		{ "ModuleRelativePath", "Public/DisplaceMeshTool.h" },
		{ "ToolTip", "Displacement intensity" },
		{ "UIMax", "100.0" },
		{ "UIMin", "-100.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_DisplaceIntensity = { "DisplaceIntensity", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplaceMeshCommonProperties, DisplaceIntensity), METADATA_PARAMS(Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_DisplaceIntensity_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_DisplaceIntensity_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_RandomSeed_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Seed for randomization */" },
		{ "EditCondition", "DisplacementType == EDisplaceMeshToolDisplaceType::RandomNoise || DisplacementType == EDisplaceMeshToolDisplaceType::PerlinNoise" },
		{ "ModuleRelativePath", "Public/DisplaceMeshTool.h" },
		{ "ToolTip", "Seed for randomization" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_RandomSeed = { "RandomSeed", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplaceMeshCommonProperties, RandomSeed), METADATA_PARAMS(Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_RandomSeed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_RandomSeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_Subdivisions_MetaData[] = {
		{ "Category", "Options" },
		{ "ClampMax", "100" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Subdivision iterations for mesh */" },
		{ "ModuleRelativePath", "Public/DisplaceMeshTool.h" },
		{ "ToolTip", "Subdivision iterations for mesh" },
		{ "UIMax", "10" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_Subdivisions = { "Subdivisions", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplaceMeshCommonProperties, Subdivisions), METADATA_PARAMS(Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_Subdivisions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_Subdivisions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_WeightMap_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Select vertex weight map. If configured, the weight map value will be sampled to modulate displacement intensity. */" },
		{ "GetOptions", "GetWeightMapsFunc" },
		{ "ModuleRelativePath", "Public/DisplaceMeshTool.h" },
		{ "ToolTip", "Select vertex weight map. If configured, the weight map value will be sampled to modulate displacement intensity." },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_WeightMap = { "WeightMap", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplaceMeshCommonProperties, WeightMap), METADATA_PARAMS(Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_WeightMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_WeightMap_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_WeightMapsList_Inner = { "WeightMapsList", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_WeightMapsList_MetaData[] = {
		{ "ModuleRelativePath", "Public/DisplaceMeshTool.h" },
		{ "TransientToolProperty", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_WeightMapsList = { "WeightMapsList", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplaceMeshCommonProperties, WeightMapsList), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_WeightMapsList_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_WeightMapsList_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_bInvertWeightMap_MetaData[] = {
		{ "Category", "Options" },
		{ "ModuleRelativePath", "Public/DisplaceMeshTool.h" },
	};
#endif
	void Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_bInvertWeightMap_SetBit(void* Obj)
	{
		((UDisplaceMeshCommonProperties*)Obj)->bInvertWeightMap = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_bInvertWeightMap = { "bInvertWeightMap", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDisplaceMeshCommonProperties), &Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_bInvertWeightMap_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_bInvertWeightMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_bInvertWeightMap_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_bDisableSizeWarning_MetaData[] = {
		{ "Category", "Options" },
		{ "ModuleRelativePath", "Public/DisplaceMeshTool.h" },
		{ "TransientToolProperty", "" },
	};
#endif
	void Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_bDisableSizeWarning_SetBit(void* Obj)
	{
		((UDisplaceMeshCommonProperties*)Obj)->bDisableSizeWarning = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_bDisableSizeWarning = { "bDisableSizeWarning", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDisplaceMeshCommonProperties), &Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_bDisableSizeWarning_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_bDisableSizeWarning_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_bDisableSizeWarning_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_DisplacementType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_DisplacementType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_DisplaceIntensity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_RandomSeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_Subdivisions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_WeightMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_WeightMapsList_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_WeightMapsList,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_bInvertWeightMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::NewProp_bDisableSizeWarning,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDisplaceMeshCommonProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::ClassParams = {
		&UDisplaceMeshCommonProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDisplaceMeshCommonProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDisplaceMeshCommonProperties, 1545372939);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UDisplaceMeshCommonProperties>()
	{
		return UDisplaceMeshCommonProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDisplaceMeshCommonProperties(Z_Construct_UClass_UDisplaceMeshCommonProperties, &UDisplaceMeshCommonProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UDisplaceMeshCommonProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDisplaceMeshCommonProperties);
	void UDisplaceMeshTextureMapProperties::StaticRegisterNativesUDisplaceMeshTextureMapProperties()
	{
	}
	UClass* Z_Construct_UClass_UDisplaceMeshTextureMapProperties_NoRegister()
	{
		return UDisplaceMeshTextureMapProperties::StaticClass();
	}
	struct Z_Construct_UClass_UDisplaceMeshTextureMapProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisplacementMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DisplacementMap;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDisplaceMeshTextureMapProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplaceMeshTextureMapProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** PropertySet for properties affecting the Image Map displacement type. */" },
		{ "IncludePath", "DisplaceMeshTool.h" },
		{ "ModuleRelativePath", "Public/DisplaceMeshTool.h" },
		{ "ToolTip", "PropertySet for properties affecting the Image Map displacement type." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplaceMeshTextureMapProperties_Statics::NewProp_DisplacementMap_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Displacement map */" },
		{ "ModuleRelativePath", "Public/DisplaceMeshTool.h" },
		{ "ToolTip", "Displacement map" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDisplaceMeshTextureMapProperties_Statics::NewProp_DisplacementMap = { "DisplacementMap", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplaceMeshTextureMapProperties, DisplacementMap), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDisplaceMeshTextureMapProperties_Statics::NewProp_DisplacementMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplaceMeshTextureMapProperties_Statics::NewProp_DisplacementMap_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDisplaceMeshTextureMapProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplaceMeshTextureMapProperties_Statics::NewProp_DisplacementMap,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDisplaceMeshTextureMapProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDisplaceMeshTextureMapProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDisplaceMeshTextureMapProperties_Statics::ClassParams = {
		&UDisplaceMeshTextureMapProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDisplaceMeshTextureMapProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDisplaceMeshTextureMapProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UDisplaceMeshTextureMapProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplaceMeshTextureMapProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDisplaceMeshTextureMapProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDisplaceMeshTextureMapProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDisplaceMeshTextureMapProperties, 3269486952);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UDisplaceMeshTextureMapProperties>()
	{
		return UDisplaceMeshTextureMapProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDisplaceMeshTextureMapProperties(Z_Construct_UClass_UDisplaceMeshTextureMapProperties, &UDisplaceMeshTextureMapProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UDisplaceMeshTextureMapProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDisplaceMeshTextureMapProperties);
	void UDisplaceMeshDirectionalFilterProperties::StaticRegisterNativesUDisplaceMeshDirectionalFilterProperties()
	{
	}
	UClass* Z_Construct_UClass_UDisplaceMeshDirectionalFilterProperties_NoRegister()
	{
		return UDisplaceMeshDirectionalFilterProperties::StaticClass();
	}
	struct Z_Construct_UClass_UDisplaceMeshDirectionalFilterProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableFilter_MetaData[];
#endif
		static void NewProp_bEnableFilter_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableFilter;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FilterDirection_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FilterDirection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FilterWidth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FilterWidth;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDisplaceMeshDirectionalFilterProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplaceMeshDirectionalFilterProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Properties for a directional filter. Allows for displacement to be applied only to vertices whose normals point in a given direction */" },
		{ "IncludePath", "DisplaceMeshTool.h" },
		{ "ModuleRelativePath", "Public/DisplaceMeshTool.h" },
		{ "ToolTip", "Properties for a directional filter. Allows for displacement to be applied only to vertices whose normals point in a given direction" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplaceMeshDirectionalFilterProperties_Statics::NewProp_bEnableFilter_MetaData[] = {
		{ "Category", "DirectionalFilterOptions" },
		{ "Comment", "/** Whether the directional filter is active. */" },
		{ "ModuleRelativePath", "Public/DisplaceMeshTool.h" },
		{ "ToolTip", "Whether the directional filter is active." },
	};
#endif
	void Z_Construct_UClass_UDisplaceMeshDirectionalFilterProperties_Statics::NewProp_bEnableFilter_SetBit(void* Obj)
	{
		((UDisplaceMeshDirectionalFilterProperties*)Obj)->bEnableFilter = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDisplaceMeshDirectionalFilterProperties_Statics::NewProp_bEnableFilter = { "bEnableFilter", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDisplaceMeshDirectionalFilterProperties), &Z_Construct_UClass_UDisplaceMeshDirectionalFilterProperties_Statics::NewProp_bEnableFilter_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDisplaceMeshDirectionalFilterProperties_Statics::NewProp_bEnableFilter_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplaceMeshDirectionalFilterProperties_Statics::NewProp_bEnableFilter_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplaceMeshDirectionalFilterProperties_Statics::NewProp_FilterDirection_MetaData[] = {
		{ "Category", "DirectionalFilterOptions" },
		{ "Comment", "/** Unit vector representing the direction to filter along. */" },
		{ "EditCondition", "bEnableFilter == true" },
		{ "ModuleRelativePath", "Public/DisplaceMeshTool.h" },
		{ "ToolTip", "Unit vector representing the direction to filter along." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDisplaceMeshDirectionalFilterProperties_Statics::NewProp_FilterDirection = { "FilterDirection", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplaceMeshDirectionalFilterProperties, FilterDirection), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UDisplaceMeshDirectionalFilterProperties_Statics::NewProp_FilterDirection_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplaceMeshDirectionalFilterProperties_Statics::NewProp_FilterDirection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplaceMeshDirectionalFilterProperties_Statics::NewProp_FilterWidth_MetaData[] = {
		{ "Category", "DirectionalFilterOptions" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Scalar value determining how close to the filter direction the vertex normals must be in order to be displaced.\n\x09\x09""0: Only normals pointing exactly in the filter direction are displaced.\n\x09\x09""0.5: Normals forming angle up to 90 from the filter direction are displaced.\n\x09\x09""1.0: All vertices are displaced.\n\x09*/" },
		{ "EditCondition", "bEnableFilter == true" },
		{ "ModuleRelativePath", "Public/DisplaceMeshTool.h" },
		{ "ToolTip", "Scalar value determining how close to the filter direction the vertex normals must be in order to be displaced.\n              0: Only normals pointing exactly in the filter direction are displaced.\n              0.5: Normals forming angle up to 90 from the filter direction are displaced.\n              1.0: All vertices are displaced." },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDisplaceMeshDirectionalFilterProperties_Statics::NewProp_FilterWidth = { "FilterWidth", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplaceMeshDirectionalFilterProperties, FilterWidth), METADATA_PARAMS(Z_Construct_UClass_UDisplaceMeshDirectionalFilterProperties_Statics::NewProp_FilterWidth_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplaceMeshDirectionalFilterProperties_Statics::NewProp_FilterWidth_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDisplaceMeshDirectionalFilterProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplaceMeshDirectionalFilterProperties_Statics::NewProp_bEnableFilter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplaceMeshDirectionalFilterProperties_Statics::NewProp_FilterDirection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplaceMeshDirectionalFilterProperties_Statics::NewProp_FilterWidth,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDisplaceMeshDirectionalFilterProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDisplaceMeshDirectionalFilterProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDisplaceMeshDirectionalFilterProperties_Statics::ClassParams = {
		&UDisplaceMeshDirectionalFilterProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDisplaceMeshDirectionalFilterProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDisplaceMeshDirectionalFilterProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UDisplaceMeshDirectionalFilterProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplaceMeshDirectionalFilterProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDisplaceMeshDirectionalFilterProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDisplaceMeshDirectionalFilterProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDisplaceMeshDirectionalFilterProperties, 4048288142);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UDisplaceMeshDirectionalFilterProperties>()
	{
		return UDisplaceMeshDirectionalFilterProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDisplaceMeshDirectionalFilterProperties(Z_Construct_UClass_UDisplaceMeshDirectionalFilterProperties, &UDisplaceMeshDirectionalFilterProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UDisplaceMeshDirectionalFilterProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDisplaceMeshDirectionalFilterProperties);
	void UDisplaceMeshPerlinNoiseProperties::StaticRegisterNativesUDisplaceMeshPerlinNoiseProperties()
	{
	}
	UClass* Z_Construct_UClass_UDisplaceMeshPerlinNoiseProperties_NoRegister()
	{
		return UDisplaceMeshPerlinNoiseProperties::StaticClass();
	}
	struct Z_Construct_UClass_UDisplaceMeshPerlinNoiseProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PerlinLayerProperties_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PerlinLayerProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PerlinLayerProperties;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDisplaceMeshPerlinNoiseProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplaceMeshPerlinNoiseProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** PropertySet for properties affecting the Perlin Noise displacement type. */" },
		{ "IncludePath", "DisplaceMeshTool.h" },
		{ "ModuleRelativePath", "Public/DisplaceMeshTool.h" },
		{ "ToolTip", "PropertySet for properties affecting the Perlin Noise displacement type." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDisplaceMeshPerlinNoiseProperties_Statics::NewProp_PerlinLayerProperties_Inner = { "PerlinLayerProperties", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FPerlinLayerProperties, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplaceMeshPerlinNoiseProperties_Statics::NewProp_PerlinLayerProperties_MetaData[] = {
		{ "Category", "PerlinNoiseOptions" },
		{ "ModuleRelativePath", "Public/DisplaceMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDisplaceMeshPerlinNoiseProperties_Statics::NewProp_PerlinLayerProperties = { "PerlinLayerProperties", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplaceMeshPerlinNoiseProperties, PerlinLayerProperties), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDisplaceMeshPerlinNoiseProperties_Statics::NewProp_PerlinLayerProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplaceMeshPerlinNoiseProperties_Statics::NewProp_PerlinLayerProperties_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDisplaceMeshPerlinNoiseProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplaceMeshPerlinNoiseProperties_Statics::NewProp_PerlinLayerProperties_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplaceMeshPerlinNoiseProperties_Statics::NewProp_PerlinLayerProperties,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDisplaceMeshPerlinNoiseProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDisplaceMeshPerlinNoiseProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDisplaceMeshPerlinNoiseProperties_Statics::ClassParams = {
		&UDisplaceMeshPerlinNoiseProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDisplaceMeshPerlinNoiseProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDisplaceMeshPerlinNoiseProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UDisplaceMeshPerlinNoiseProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplaceMeshPerlinNoiseProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDisplaceMeshPerlinNoiseProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDisplaceMeshPerlinNoiseProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDisplaceMeshPerlinNoiseProperties, 2356484267);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UDisplaceMeshPerlinNoiseProperties>()
	{
		return UDisplaceMeshPerlinNoiseProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDisplaceMeshPerlinNoiseProperties(Z_Construct_UClass_UDisplaceMeshPerlinNoiseProperties, &UDisplaceMeshPerlinNoiseProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UDisplaceMeshPerlinNoiseProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDisplaceMeshPerlinNoiseProperties);
	void UDisplaceMeshSineWaveProperties::StaticRegisterNativesUDisplaceMeshSineWaveProperties()
	{
	}
	UClass* Z_Construct_UClass_UDisplaceMeshSineWaveProperties_NoRegister()
	{
		return UDisplaceMeshSineWaveProperties::StaticClass();
	}
	struct Z_Construct_UClass_UDisplaceMeshSineWaveProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SineWaveFrequency_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SineWaveFrequency;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SineWavePhaseShift_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SineWavePhaseShift;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SineWaveDirection_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SineWaveDirection;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDisplaceMeshSineWaveProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplaceMeshSineWaveProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** PropertySet for Sine wave displacement */" },
		{ "IncludePath", "DisplaceMeshTool.h" },
		{ "ModuleRelativePath", "Public/DisplaceMeshTool.h" },
		{ "ToolTip", "PropertySet for Sine wave displacement" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplaceMeshSineWaveProperties_Statics::NewProp_SineWaveFrequency_MetaData[] = {
		{ "Category", "SineWaveOptions" },
		{ "ClampMax", "10.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Sine wave displacement frequency */" },
		{ "ModuleRelativePath", "Public/DisplaceMeshTool.h" },
		{ "ToolTip", "Sine wave displacement frequency" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDisplaceMeshSineWaveProperties_Statics::NewProp_SineWaveFrequency = { "SineWaveFrequency", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplaceMeshSineWaveProperties, SineWaveFrequency), METADATA_PARAMS(Z_Construct_UClass_UDisplaceMeshSineWaveProperties_Statics::NewProp_SineWaveFrequency_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplaceMeshSineWaveProperties_Statics::NewProp_SineWaveFrequency_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplaceMeshSineWaveProperties_Statics::NewProp_SineWavePhaseShift_MetaData[] = {
		{ "Category", "SineWaveOptions" },
		{ "ClampMax", "6.28318531" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Sine wave phase shift */" },
		{ "ModuleRelativePath", "Public/DisplaceMeshTool.h" },
		{ "ToolTip", "Sine wave phase shift" },
		{ "UIMax", "6.28318531" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDisplaceMeshSineWaveProperties_Statics::NewProp_SineWavePhaseShift = { "SineWavePhaseShift", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplaceMeshSineWaveProperties, SineWavePhaseShift), METADATA_PARAMS(Z_Construct_UClass_UDisplaceMeshSineWaveProperties_Statics::NewProp_SineWavePhaseShift_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplaceMeshSineWaveProperties_Statics::NewProp_SineWavePhaseShift_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplaceMeshSineWaveProperties_Statics::NewProp_SineWaveDirection_MetaData[] = {
		{ "Category", "SineWaveOptions" },
		{ "Comment", "/** Unit vector representing the direction of wave displacement. */" },
		{ "ModuleRelativePath", "Public/DisplaceMeshTool.h" },
		{ "ToolTip", "Unit vector representing the direction of wave displacement." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDisplaceMeshSineWaveProperties_Statics::NewProp_SineWaveDirection = { "SineWaveDirection", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplaceMeshSineWaveProperties, SineWaveDirection), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UDisplaceMeshSineWaveProperties_Statics::NewProp_SineWaveDirection_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplaceMeshSineWaveProperties_Statics::NewProp_SineWaveDirection_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDisplaceMeshSineWaveProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplaceMeshSineWaveProperties_Statics::NewProp_SineWaveFrequency,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplaceMeshSineWaveProperties_Statics::NewProp_SineWavePhaseShift,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplaceMeshSineWaveProperties_Statics::NewProp_SineWaveDirection,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDisplaceMeshSineWaveProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDisplaceMeshSineWaveProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDisplaceMeshSineWaveProperties_Statics::ClassParams = {
		&UDisplaceMeshSineWaveProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDisplaceMeshSineWaveProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDisplaceMeshSineWaveProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UDisplaceMeshSineWaveProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplaceMeshSineWaveProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDisplaceMeshSineWaveProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDisplaceMeshSineWaveProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDisplaceMeshSineWaveProperties, 492811326);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UDisplaceMeshSineWaveProperties>()
	{
		return UDisplaceMeshSineWaveProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDisplaceMeshSineWaveProperties(Z_Construct_UClass_UDisplaceMeshSineWaveProperties, &UDisplaceMeshSineWaveProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UDisplaceMeshSineWaveProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDisplaceMeshSineWaveProperties);
	void UDisplaceMeshToolBuilder::StaticRegisterNativesUDisplaceMeshToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UDisplaceMeshToolBuilder_NoRegister()
	{
		return UDisplaceMeshToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UDisplaceMeshToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDisplaceMeshToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplaceMeshToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Builder for Simple Mesh Displacement Tool\n */" },
		{ "IncludePath", "DisplaceMeshTool.h" },
		{ "ModuleRelativePath", "Public/DisplaceMeshTool.h" },
		{ "ToolTip", "Builder for Simple Mesh Displacement Tool" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDisplaceMeshToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDisplaceMeshToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDisplaceMeshToolBuilder_Statics::ClassParams = {
		&UDisplaceMeshToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UDisplaceMeshToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplaceMeshToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDisplaceMeshToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDisplaceMeshToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDisplaceMeshToolBuilder, 4219625362);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UDisplaceMeshToolBuilder>()
	{
		return UDisplaceMeshToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDisplaceMeshToolBuilder(Z_Construct_UClass_UDisplaceMeshToolBuilder, &UDisplaceMeshToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UDisplaceMeshToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDisplaceMeshToolBuilder);
	void UDisplaceMeshTool::StaticRegisterNativesUDisplaceMeshTool()
	{
	}
	UClass* Z_Construct_UClass_UDisplaceMeshTool_NoRegister()
	{
		return UDisplaceMeshTool::StaticClass();
	}
	struct Z_Construct_UClass_UDisplaceMeshTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CommonProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CommonProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DirectionalFilterProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DirectionalFilterProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextureMapProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TextureMapProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NoiseProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_NoiseProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SineWaveProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SineWaveProperties;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDisplaceMeshTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USingleSelectionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplaceMeshTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Simple Mesh Displacement Tool\n */" },
		{ "IncludePath", "DisplaceMeshTool.h" },
		{ "ModuleRelativePath", "Public/DisplaceMeshTool.h" },
		{ "ToolTip", "Simple Mesh Displacement Tool" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplaceMeshTool_Statics::NewProp_CommonProperties_MetaData[] = {
		{ "Comment", "/** Shared properties for all displacement modes. */" },
		{ "ModuleRelativePath", "Public/DisplaceMeshTool.h" },
		{ "ToolTip", "Shared properties for all displacement modes." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDisplaceMeshTool_Statics::NewProp_CommonProperties = { "CommonProperties", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplaceMeshTool, CommonProperties), Z_Construct_UClass_UDisplaceMeshCommonProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDisplaceMeshTool_Statics::NewProp_CommonProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplaceMeshTool_Statics::NewProp_CommonProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplaceMeshTool_Statics::NewProp_DirectionalFilterProperties_MetaData[] = {
		{ "Comment", "/** Properties defining the directional filter. */" },
		{ "ModuleRelativePath", "Public/DisplaceMeshTool.h" },
		{ "ToolTip", "Properties defining the directional filter." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDisplaceMeshTool_Statics::NewProp_DirectionalFilterProperties = { "DirectionalFilterProperties", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplaceMeshTool, DirectionalFilterProperties), Z_Construct_UClass_UDisplaceMeshDirectionalFilterProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDisplaceMeshTool_Statics::NewProp_DirectionalFilterProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplaceMeshTool_Statics::NewProp_DirectionalFilterProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplaceMeshTool_Statics::NewProp_TextureMapProperties_MetaData[] = {
		{ "Comment", "/** Properties defining the texture map */" },
		{ "ModuleRelativePath", "Public/DisplaceMeshTool.h" },
		{ "ToolTip", "Properties defining the texture map" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDisplaceMeshTool_Statics::NewProp_TextureMapProperties = { "TextureMapProperties", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplaceMeshTool, TextureMapProperties), Z_Construct_UClass_UDisplaceMeshTextureMapProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDisplaceMeshTool_Statics::NewProp_TextureMapProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplaceMeshTool_Statics::NewProp_TextureMapProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplaceMeshTool_Statics::NewProp_NoiseProperties_MetaData[] = {
		{ "Comment", "/** Multi-layer Perlin noise frequencies and intensities */" },
		{ "ModuleRelativePath", "Public/DisplaceMeshTool.h" },
		{ "ToolTip", "Multi-layer Perlin noise frequencies and intensities" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDisplaceMeshTool_Statics::NewProp_NoiseProperties = { "NoiseProperties", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplaceMeshTool, NoiseProperties), Z_Construct_UClass_UDisplaceMeshPerlinNoiseProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDisplaceMeshTool_Statics::NewProp_NoiseProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplaceMeshTool_Statics::NewProp_NoiseProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplaceMeshTool_Statics::NewProp_SineWaveProperties_MetaData[] = {
		{ "Comment", "/** Sine wave parameters and direction of displacement */" },
		{ "ModuleRelativePath", "Public/DisplaceMeshTool.h" },
		{ "ToolTip", "Sine wave parameters and direction of displacement" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDisplaceMeshTool_Statics::NewProp_SineWaveProperties = { "SineWaveProperties", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplaceMeshTool, SineWaveProperties), Z_Construct_UClass_UDisplaceMeshSineWaveProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDisplaceMeshTool_Statics::NewProp_SineWaveProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplaceMeshTool_Statics::NewProp_SineWaveProperties_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDisplaceMeshTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplaceMeshTool_Statics::NewProp_CommonProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplaceMeshTool_Statics::NewProp_DirectionalFilterProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplaceMeshTool_Statics::NewProp_TextureMapProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplaceMeshTool_Statics::NewProp_NoiseProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplaceMeshTool_Statics::NewProp_SineWaveProperties,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDisplaceMeshTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDisplaceMeshTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDisplaceMeshTool_Statics::ClassParams = {
		&UDisplaceMeshTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDisplaceMeshTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDisplaceMeshTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UDisplaceMeshTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplaceMeshTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDisplaceMeshTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDisplaceMeshTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDisplaceMeshTool, 4225955552);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UDisplaceMeshTool>()
	{
		return UDisplaceMeshTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDisplaceMeshTool(Z_Construct_UClass_UDisplaceMeshTool, &UDisplaceMeshTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UDisplaceMeshTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDisplaceMeshTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
