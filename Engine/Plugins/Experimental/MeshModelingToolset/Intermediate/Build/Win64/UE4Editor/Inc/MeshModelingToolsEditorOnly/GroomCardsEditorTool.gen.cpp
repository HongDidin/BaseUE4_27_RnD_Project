// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingToolsEditorOnly/Public/Hair/GroomCardsEditorTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGroomCardsEditorTool() {}
// Cross Module References
	MESHMODELINGTOOLSEDITORONLY_API UEnum* Z_Construct_UEnum_MeshModelingToolsEditorOnly_EEditGroomCardsToolActions();
	UPackage* Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UGroomCardsEditorToolBuilder_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UGroomCardsEditorToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UEditGroomCardsToolActionPropertySet_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UEditGroomCardsToolActionPropertySet();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_USelectGroomCardsToolActions_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_USelectGroomCardsToolActions();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UEditGroomCardsToolActions_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UEditGroomCardsToolActions();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UGroomCardsInfoToolProperties_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UGroomCardsInfoToolProperties();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UGroomCardsEditorTool_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UGroomCardsEditorTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UMeshSurfacePointTool();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPreviewMesh_NoRegister();
	HAIRSTRANDSCORE_API UClass* Z_Construct_UClass_AGroomActor_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPreviewGeometry_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPolygonSelectionMechanic_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_USpaceCurveDeformationMechanic_NoRegister();
// End Cross Module References
	static UEnum* EEditGroomCardsToolActions_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingToolsEditorOnly_EEditGroomCardsToolActions, Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly(), TEXT("EEditGroomCardsToolActions"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLSEDITORONLY_API UEnum* StaticEnum<EEditGroomCardsToolActions>()
	{
		return EEditGroomCardsToolActions_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EEditGroomCardsToolActions(EEditGroomCardsToolActions_StaticEnum, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("EEditGroomCardsToolActions"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingToolsEditorOnly_EEditGroomCardsToolActions_Hash() { return 1004939281U; }
	UEnum* Z_Construct_UEnum_MeshModelingToolsEditorOnly_EEditGroomCardsToolActions()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EEditGroomCardsToolActions"), 0, Get_Z_Construct_UEnum_MeshModelingToolsEditorOnly_EEditGroomCardsToolActions_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EEditGroomCardsToolActions::NoAction", (int64)EEditGroomCardsToolActions::NoAction },
				{ "EEditGroomCardsToolActions::Delete", (int64)EEditGroomCardsToolActions::Delete },
				{ "EEditGroomCardsToolActions::SelectionClear", (int64)EEditGroomCardsToolActions::SelectionClear },
				{ "EEditGroomCardsToolActions::SelectionFill", (int64)EEditGroomCardsToolActions::SelectionFill },
				{ "EEditGroomCardsToolActions::SelectionAddNext", (int64)EEditGroomCardsToolActions::SelectionAddNext },
				{ "EEditGroomCardsToolActions::SelectionAddPrevious", (int64)EEditGroomCardsToolActions::SelectionAddPrevious },
				{ "EEditGroomCardsToolActions::SelectionAddToEnd", (int64)EEditGroomCardsToolActions::SelectionAddToEnd },
				{ "EEditGroomCardsToolActions::SelectionAddToStart", (int64)EEditGroomCardsToolActions::SelectionAddToStart },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Delete.Name", "EEditGroomCardsToolActions::Delete" },
				{ "ModuleRelativePath", "Public/Hair/GroomCardsEditorTool.h" },
				{ "NoAction.Name", "EEditGroomCardsToolActions::NoAction" },
				{ "SelectionAddNext.Name", "EEditGroomCardsToolActions::SelectionAddNext" },
				{ "SelectionAddPrevious.Name", "EEditGroomCardsToolActions::SelectionAddPrevious" },
				{ "SelectionAddToEnd.Name", "EEditGroomCardsToolActions::SelectionAddToEnd" },
				{ "SelectionAddToStart.Name", "EEditGroomCardsToolActions::SelectionAddToStart" },
				{ "SelectionClear.Name", "EEditGroomCardsToolActions::SelectionClear" },
				{ "SelectionFill.Name", "EEditGroomCardsToolActions::SelectionFill" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
				nullptr,
				"EEditGroomCardsToolActions",
				"EEditGroomCardsToolActions",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UGroomCardsEditorToolBuilder::StaticRegisterNativesUGroomCardsEditorToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UGroomCardsEditorToolBuilder_NoRegister()
	{
		return UGroomCardsEditorToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UGroomCardsEditorToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGroomCardsEditorToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCardsEditorToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "Hair/GroomCardsEditorTool.h" },
		{ "ModuleRelativePath", "Public/Hair/GroomCardsEditorTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGroomCardsEditorToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGroomCardsEditorToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGroomCardsEditorToolBuilder_Statics::ClassParams = {
		&UGroomCardsEditorToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UGroomCardsEditorToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCardsEditorToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGroomCardsEditorToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGroomCardsEditorToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGroomCardsEditorToolBuilder, 142329075);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UGroomCardsEditorToolBuilder>()
	{
		return UGroomCardsEditorToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGroomCardsEditorToolBuilder(Z_Construct_UClass_UGroomCardsEditorToolBuilder, &UGroomCardsEditorToolBuilder::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UGroomCardsEditorToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGroomCardsEditorToolBuilder);
	void UEditGroomCardsToolActionPropertySet::StaticRegisterNativesUEditGroomCardsToolActionPropertySet()
	{
	}
	UClass* Z_Construct_UClass_UEditGroomCardsToolActionPropertySet_NoRegister()
	{
		return UEditGroomCardsToolActionPropertySet::StaticClass();
	}
	struct Z_Construct_UClass_UEditGroomCardsToolActionPropertySet_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEditGroomCardsToolActionPropertySet_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditGroomCardsToolActionPropertySet_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Hair/GroomCardsEditorTool.h" },
		{ "ModuleRelativePath", "Public/Hair/GroomCardsEditorTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEditGroomCardsToolActionPropertySet_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEditGroomCardsToolActionPropertySet>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEditGroomCardsToolActionPropertySet_Statics::ClassParams = {
		&UEditGroomCardsToolActionPropertySet::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UEditGroomCardsToolActionPropertySet_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEditGroomCardsToolActionPropertySet_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEditGroomCardsToolActionPropertySet()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEditGroomCardsToolActionPropertySet_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEditGroomCardsToolActionPropertySet, 1605848019);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UEditGroomCardsToolActionPropertySet>()
	{
		return UEditGroomCardsToolActionPropertySet::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEditGroomCardsToolActionPropertySet(Z_Construct_UClass_UEditGroomCardsToolActionPropertySet, &UEditGroomCardsToolActionPropertySet::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UEditGroomCardsToolActionPropertySet"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEditGroomCardsToolActionPropertySet);
	DEFINE_FUNCTION(USelectGroomCardsToolActions::execToStart)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ToStart();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(USelectGroomCardsToolActions::execToEnd)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ToEnd();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(USelectGroomCardsToolActions::execAddPrevious)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AddPrevious();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(USelectGroomCardsToolActions::execAddNext)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AddNext();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(USelectGroomCardsToolActions::execFill)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Fill();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(USelectGroomCardsToolActions::execClear)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Clear();
		P_NATIVE_END;
	}
	void USelectGroomCardsToolActions::StaticRegisterNativesUSelectGroomCardsToolActions()
	{
		UClass* Class = USelectGroomCardsToolActions::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddNext", &USelectGroomCardsToolActions::execAddNext },
			{ "AddPrevious", &USelectGroomCardsToolActions::execAddPrevious },
			{ "Clear", &USelectGroomCardsToolActions::execClear },
			{ "Fill", &USelectGroomCardsToolActions::execFill },
			{ "ToEnd", &USelectGroomCardsToolActions::execToEnd },
			{ "ToStart", &USelectGroomCardsToolActions::execToStart },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_USelectGroomCardsToolActions_AddNext_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USelectGroomCardsToolActions_AddNext_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "Selection" },
		{ "Comment", "/** Add the next vertex along the card curve to the selection */" },
		{ "DisplayPriority", "3" },
		{ "ModuleRelativePath", "Public/Hair/GroomCardsEditorTool.h" },
		{ "ToolTip", "Add the next vertex along the card curve to the selection" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USelectGroomCardsToolActions_AddNext_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USelectGroomCardsToolActions, nullptr, "AddNext", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USelectGroomCardsToolActions_AddNext_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USelectGroomCardsToolActions_AddNext_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USelectGroomCardsToolActions_AddNext()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USelectGroomCardsToolActions_AddNext_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USelectGroomCardsToolActions_AddPrevious_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USelectGroomCardsToolActions_AddPrevious_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "Selection" },
		{ "Comment", "/** Add the previous vertex along the card curve to the selection */" },
		{ "DisplayPriority", "4" },
		{ "ModuleRelativePath", "Public/Hair/GroomCardsEditorTool.h" },
		{ "ToolTip", "Add the previous vertex along the card curve to the selection" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USelectGroomCardsToolActions_AddPrevious_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USelectGroomCardsToolActions, nullptr, "AddPrevious", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USelectGroomCardsToolActions_AddPrevious_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USelectGroomCardsToolActions_AddPrevious_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USelectGroomCardsToolActions_AddPrevious()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USelectGroomCardsToolActions_AddPrevious_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USelectGroomCardsToolActions_Clear_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USelectGroomCardsToolActions_Clear_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "Selection" },
		{ "Comment", "/** Clear the current selection */" },
		{ "DisplayPriority", "1" },
		{ "ModuleRelativePath", "Public/Hair/GroomCardsEditorTool.h" },
		{ "ToolTip", "Clear the current selection" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USelectGroomCardsToolActions_Clear_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USelectGroomCardsToolActions, nullptr, "Clear", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USelectGroomCardsToolActions_Clear_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USelectGroomCardsToolActions_Clear_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USelectGroomCardsToolActions_Clear()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USelectGroomCardsToolActions_Clear_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USelectGroomCardsToolActions_Fill_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USelectGroomCardsToolActions_Fill_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "Selection" },
		{ "Comment", "/** Select unselected points along curve between selected points */" },
		{ "DisplayPriority", "1" },
		{ "ModuleRelativePath", "Public/Hair/GroomCardsEditorTool.h" },
		{ "ToolTip", "Select unselected points along curve between selected points" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USelectGroomCardsToolActions_Fill_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USelectGroomCardsToolActions, nullptr, "Fill", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USelectGroomCardsToolActions_Fill_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USelectGroomCardsToolActions_Fill_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USelectGroomCardsToolActions_Fill()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USelectGroomCardsToolActions_Fill_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USelectGroomCardsToolActions_ToEnd_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USelectGroomCardsToolActions_ToEnd_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "Selection" },
		{ "Comment", "/** Select all vertices to the end of the card */" },
		{ "DisplayPriority", "5" },
		{ "ModuleRelativePath", "Public/Hair/GroomCardsEditorTool.h" },
		{ "ToolTip", "Select all vertices to the end of the card" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USelectGroomCardsToolActions_ToEnd_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USelectGroomCardsToolActions, nullptr, "ToEnd", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USelectGroomCardsToolActions_ToEnd_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USelectGroomCardsToolActions_ToEnd_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USelectGroomCardsToolActions_ToEnd()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USelectGroomCardsToolActions_ToEnd_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USelectGroomCardsToolActions_ToStart_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USelectGroomCardsToolActions_ToStart_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "Selection" },
		{ "Comment", "/** Select all vertices to the start of the card */" },
		{ "DisplayPriority", "6" },
		{ "ModuleRelativePath", "Public/Hair/GroomCardsEditorTool.h" },
		{ "ToolTip", "Select all vertices to the start of the card" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USelectGroomCardsToolActions_ToStart_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USelectGroomCardsToolActions, nullptr, "ToStart", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USelectGroomCardsToolActions_ToStart_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USelectGroomCardsToolActions_ToStart_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USelectGroomCardsToolActions_ToStart()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USelectGroomCardsToolActions_ToStart_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_USelectGroomCardsToolActions_NoRegister()
	{
		return USelectGroomCardsToolActions::StaticClass();
	}
	struct Z_Construct_UClass_USelectGroomCardsToolActions_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USelectGroomCardsToolActions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEditGroomCardsToolActionPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_USelectGroomCardsToolActions_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_USelectGroomCardsToolActions_AddNext, "AddNext" }, // 2291357594
		{ &Z_Construct_UFunction_USelectGroomCardsToolActions_AddPrevious, "AddPrevious" }, // 3377385789
		{ &Z_Construct_UFunction_USelectGroomCardsToolActions_Clear, "Clear" }, // 265520775
		{ &Z_Construct_UFunction_USelectGroomCardsToolActions_Fill, "Fill" }, // 3686379781
		{ &Z_Construct_UFunction_USelectGroomCardsToolActions_ToEnd, "ToEnd" }, // 3136189120
		{ &Z_Construct_UFunction_USelectGroomCardsToolActions_ToStart, "ToStart" }, // 4063610507
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USelectGroomCardsToolActions_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Hair/GroomCardsEditorTool.h" },
		{ "ModuleRelativePath", "Public/Hair/GroomCardsEditorTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USelectGroomCardsToolActions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USelectGroomCardsToolActions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USelectGroomCardsToolActions_Statics::ClassParams = {
		&USelectGroomCardsToolActions::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_USelectGroomCardsToolActions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USelectGroomCardsToolActions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USelectGroomCardsToolActions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USelectGroomCardsToolActions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USelectGroomCardsToolActions, 3701389481);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<USelectGroomCardsToolActions>()
	{
		return USelectGroomCardsToolActions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USelectGroomCardsToolActions(Z_Construct_UClass_USelectGroomCardsToolActions, &USelectGroomCardsToolActions::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("USelectGroomCardsToolActions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USelectGroomCardsToolActions);
	DEFINE_FUNCTION(UEditGroomCardsToolActions::execDelete)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Delete();
		P_NATIVE_END;
	}
	void UEditGroomCardsToolActions::StaticRegisterNativesUEditGroomCardsToolActions()
	{
		UClass* Class = UEditGroomCardsToolActions::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Delete", &UEditGroomCardsToolActions::execDelete },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UEditGroomCardsToolActions_Delete_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditGroomCardsToolActions_Delete_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "FaceEdits" },
		{ "Comment", "/** Delete the current selected cards */" },
		{ "DisplayName", "Delete" },
		{ "DisplayPriority", "10" },
		{ "ModuleRelativePath", "Public/Hair/GroomCardsEditorTool.h" },
		{ "ToolTip", "Delete the current selected cards" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditGroomCardsToolActions_Delete_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditGroomCardsToolActions, nullptr, "Delete", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditGroomCardsToolActions_Delete_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditGroomCardsToolActions_Delete_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditGroomCardsToolActions_Delete()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditGroomCardsToolActions_Delete_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UEditGroomCardsToolActions_NoRegister()
	{
		return UEditGroomCardsToolActions::StaticClass();
	}
	struct Z_Construct_UClass_UEditGroomCardsToolActions_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEditGroomCardsToolActions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEditGroomCardsToolActionPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UEditGroomCardsToolActions_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UEditGroomCardsToolActions_Delete, "Delete" }, // 3373591381
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditGroomCardsToolActions_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Hair/GroomCardsEditorTool.h" },
		{ "ModuleRelativePath", "Public/Hair/GroomCardsEditorTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEditGroomCardsToolActions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEditGroomCardsToolActions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEditGroomCardsToolActions_Statics::ClassParams = {
		&UEditGroomCardsToolActions::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UEditGroomCardsToolActions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEditGroomCardsToolActions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEditGroomCardsToolActions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEditGroomCardsToolActions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEditGroomCardsToolActions, 372987976);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UEditGroomCardsToolActions>()
	{
		return UEditGroomCardsToolActions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEditGroomCardsToolActions(Z_Construct_UClass_UEditGroomCardsToolActions, &UEditGroomCardsToolActions::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UEditGroomCardsToolActions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEditGroomCardsToolActions);
	void UGroomCardsInfoToolProperties::StaticRegisterNativesUGroomCardsInfoToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UGroomCardsInfoToolProperties_NoRegister()
	{
		return UGroomCardsInfoToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UGroomCardsInfoToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumCards_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumCards;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumVertices_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumVertices;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumTriangles_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumTriangles;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGroomCardsInfoToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCardsInfoToolProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Hair/GroomCardsEditorTool.h" },
		{ "ModuleRelativePath", "Public/Hair/GroomCardsEditorTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCardsInfoToolProperties_Statics::NewProp_NumCards_MetaData[] = {
		{ "Category", "Statistics" },
		{ "ModuleRelativePath", "Public/Hair/GroomCardsEditorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UGroomCardsInfoToolProperties_Statics::NewProp_NumCards = { "NumCards", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomCardsInfoToolProperties, NumCards), METADATA_PARAMS(Z_Construct_UClass_UGroomCardsInfoToolProperties_Statics::NewProp_NumCards_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCardsInfoToolProperties_Statics::NewProp_NumCards_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCardsInfoToolProperties_Statics::NewProp_NumVertices_MetaData[] = {
		{ "Category", "Statistics" },
		{ "ModuleRelativePath", "Public/Hair/GroomCardsEditorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UGroomCardsInfoToolProperties_Statics::NewProp_NumVertices = { "NumVertices", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomCardsInfoToolProperties, NumVertices), METADATA_PARAMS(Z_Construct_UClass_UGroomCardsInfoToolProperties_Statics::NewProp_NumVertices_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCardsInfoToolProperties_Statics::NewProp_NumVertices_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCardsInfoToolProperties_Statics::NewProp_NumTriangles_MetaData[] = {
		{ "Category", "Statistics" },
		{ "ModuleRelativePath", "Public/Hair/GroomCardsEditorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UGroomCardsInfoToolProperties_Statics::NewProp_NumTriangles = { "NumTriangles", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomCardsInfoToolProperties, NumTriangles), METADATA_PARAMS(Z_Construct_UClass_UGroomCardsInfoToolProperties_Statics::NewProp_NumTriangles_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCardsInfoToolProperties_Statics::NewProp_NumTriangles_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGroomCardsInfoToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomCardsInfoToolProperties_Statics::NewProp_NumCards,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomCardsInfoToolProperties_Statics::NewProp_NumVertices,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomCardsInfoToolProperties_Statics::NewProp_NumTriangles,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGroomCardsInfoToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGroomCardsInfoToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGroomCardsInfoToolProperties_Statics::ClassParams = {
		&UGroomCardsInfoToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UGroomCardsInfoToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCardsInfoToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UGroomCardsInfoToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCardsInfoToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGroomCardsInfoToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGroomCardsInfoToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGroomCardsInfoToolProperties, 2948967736);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UGroomCardsInfoToolProperties>()
	{
		return UGroomCardsInfoToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGroomCardsInfoToolProperties(Z_Construct_UClass_UGroomCardsInfoToolProperties, &UGroomCardsInfoToolProperties::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UGroomCardsInfoToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGroomCardsInfoToolProperties);
	void UGroomCardsEditorTool::StaticRegisterNativesUGroomCardsEditorTool()
	{
	}
	UClass* Z_Construct_UClass_UGroomCardsEditorTool_NoRegister()
	{
		return UGroomCardsEditorTool::StaticClass();
	}
	struct Z_Construct_UClass_UGroomCardsEditorTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SelectActions_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SelectActions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EditActions_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_EditActions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InfoProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InfoProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviewMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviewMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetGroom_MetaData[];
#endif
		static const UE4CodeGen_Private::FLazyObjectPropertyParams NewProp_TargetGroom;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviewGeom_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviewGeom;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MeshMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UVMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_UVMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CardMeshSelectionMechanic_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CardMeshSelectionMechanic;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ControlPointsMechanic_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ControlPointsMechanic;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGroomCardsEditorTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshSurfacePointTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCardsEditorTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "Hair/GroomCardsEditorTool.h" },
		{ "ModuleRelativePath", "Public/Hair/GroomCardsEditorTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_SelectActions_MetaData[] = {
		{ "ModuleRelativePath", "Public/Hair/GroomCardsEditorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_SelectActions = { "SelectActions", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomCardsEditorTool, SelectActions), Z_Construct_UClass_USelectGroomCardsToolActions_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_SelectActions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_SelectActions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_EditActions_MetaData[] = {
		{ "ModuleRelativePath", "Public/Hair/GroomCardsEditorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_EditActions = { "EditActions", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomCardsEditorTool, EditActions), Z_Construct_UClass_UEditGroomCardsToolActions_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_EditActions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_EditActions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_InfoProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/Hair/GroomCardsEditorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_InfoProperties = { "InfoProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomCardsEditorTool, InfoProperties), Z_Construct_UClass_UGroomCardsInfoToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_InfoProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_InfoProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_PreviewMesh_MetaData[] = {
		{ "ModuleRelativePath", "Public/Hair/GroomCardsEditorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_PreviewMesh = { "PreviewMesh", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomCardsEditorTool, PreviewMesh), Z_Construct_UClass_UPreviewMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_PreviewMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_PreviewMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_TargetGroom_MetaData[] = {
		{ "ModuleRelativePath", "Public/Hair/GroomCardsEditorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FLazyObjectPropertyParams Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_TargetGroom = { "TargetGroom", nullptr, (EPropertyFlags)0x0024080000000000, UE4CodeGen_Private::EPropertyGenFlags::LazyObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomCardsEditorTool, TargetGroom), Z_Construct_UClass_AGroomActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_TargetGroom_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_TargetGroom_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_PreviewGeom_MetaData[] = {
		{ "ModuleRelativePath", "Public/Hair/GroomCardsEditorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_PreviewGeom = { "PreviewGeom", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomCardsEditorTool, PreviewGeom), Z_Construct_UClass_UPreviewGeometry_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_PreviewGeom_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_PreviewGeom_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_MeshMaterial_MetaData[] = {
		{ "Comment", "// materials\n" },
		{ "ModuleRelativePath", "Public/Hair/GroomCardsEditorTool.h" },
		{ "ToolTip", "materials" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_MeshMaterial = { "MeshMaterial", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomCardsEditorTool, MeshMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_MeshMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_MeshMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_UVMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Public/Hair/GroomCardsEditorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_UVMaterial = { "UVMaterial", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomCardsEditorTool, UVMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_UVMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_UVMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_CardMeshSelectionMechanic_MetaData[] = {
		{ "Comment", "// mechanics\n" },
		{ "ModuleRelativePath", "Public/Hair/GroomCardsEditorTool.h" },
		{ "ToolTip", "mechanics" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_CardMeshSelectionMechanic = { "CardMeshSelectionMechanic", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomCardsEditorTool, CardMeshSelectionMechanic), Z_Construct_UClass_UPolygonSelectionMechanic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_CardMeshSelectionMechanic_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_CardMeshSelectionMechanic_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_ControlPointsMechanic_MetaData[] = {
		{ "ModuleRelativePath", "Public/Hair/GroomCardsEditorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_ControlPointsMechanic = { "ControlPointsMechanic", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomCardsEditorTool, ControlPointsMechanic), Z_Construct_UClass_USpaceCurveDeformationMechanic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_ControlPointsMechanic_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_ControlPointsMechanic_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGroomCardsEditorTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_SelectActions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_EditActions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_InfoProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_PreviewMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_TargetGroom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_PreviewGeom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_MeshMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_UVMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_CardMeshSelectionMechanic,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomCardsEditorTool_Statics::NewProp_ControlPointsMechanic,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGroomCardsEditorTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGroomCardsEditorTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGroomCardsEditorTool_Statics::ClassParams = {
		&UGroomCardsEditorTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UGroomCardsEditorTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCardsEditorTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UGroomCardsEditorTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCardsEditorTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGroomCardsEditorTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGroomCardsEditorTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGroomCardsEditorTool, 2570467767);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UGroomCardsEditorTool>()
	{
		return UGroomCardsEditorTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGroomCardsEditorTool(Z_Construct_UClass_UGroomCardsEditorTool, &UGroomCardsEditorTool::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UGroomCardsEditorTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGroomCardsEditorTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
