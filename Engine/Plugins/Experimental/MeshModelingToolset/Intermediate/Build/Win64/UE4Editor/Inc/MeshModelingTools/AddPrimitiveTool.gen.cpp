// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/AddPrimitiveTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAddPrimitiveTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EMakeMeshPolygroupMode();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EMakeMeshPivotLocation();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EMakeMeshPlacementType();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UAddPrimitiveToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UAddPrimitiveToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UProceduralShapeToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UProceduralShapeToolProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UProceduralRectangleToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UProceduralRectangleToolProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UProceduralBoxToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UProceduralBoxToolProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UProceduralRoundedRectangleToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UProceduralRoundedRectangleToolProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UProceduralDiscToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UProceduralDiscToolProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UProceduralPuncturedDiscToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UProceduralPuncturedDiscToolProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UProceduralTorusToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UProceduralTorusToolProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UProceduralCylinderToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UProceduralCylinderToolProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UProceduralConeToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UProceduralConeToolProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UProceduralArrowToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UProceduralArrowToolProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UProceduralSphereToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UProceduralSphereToolProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UProceduralSphericalBoxToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UProceduralSphericalBoxToolProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_ULastActorInfo_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_ULastActorInfo();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMesh_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UNewMeshMaterialProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UAddPrimitiveTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UAddPrimitiveTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_USingleClickTool();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPreviewMesh_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UAddBoxPrimitiveTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UAddBoxPrimitiveTool();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UAddCylinderPrimitiveTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UAddCylinderPrimitiveTool();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UAddConePrimitiveTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UAddConePrimitiveTool();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UAddRectanglePrimitiveTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UAddRectanglePrimitiveTool();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UAddRoundedRectanglePrimitiveTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UAddRoundedRectanglePrimitiveTool();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UAddDiscPrimitiveTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UAddDiscPrimitiveTool();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UAddPuncturedDiscPrimitiveTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UAddPuncturedDiscPrimitiveTool();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UAddTorusPrimitiveTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UAddTorusPrimitiveTool();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UAddArrowPrimitiveTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UAddArrowPrimitiveTool();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UAddSpherePrimitiveTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UAddSpherePrimitiveTool();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UAddSphericalBoxPrimitiveTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UAddSphericalBoxPrimitiveTool();
// End Cross Module References
	static UEnum* EMakeMeshPolygroupMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EMakeMeshPolygroupMode, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EMakeMeshPolygroupMode"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EMakeMeshPolygroupMode>()
	{
		return EMakeMeshPolygroupMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMakeMeshPolygroupMode(EMakeMeshPolygroupMode_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EMakeMeshPolygroupMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EMakeMeshPolygroupMode_Hash() { return 3294778529U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EMakeMeshPolygroupMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMakeMeshPolygroupMode"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EMakeMeshPolygroupMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMakeMeshPolygroupMode::Single", (int64)EMakeMeshPolygroupMode::Single },
				{ "EMakeMeshPolygroupMode::PerFace", (int64)EMakeMeshPolygroupMode::PerFace },
				{ "EMakeMeshPolygroupMode::PerQuad", (int64)EMakeMeshPolygroupMode::PerQuad },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** Polygroup mode for primitive */" },
				{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
				{ "PerFace.Comment", "/** One polygroup per geometric face of primitive */" },
				{ "PerFace.Name", "EMakeMeshPolygroupMode::PerFace" },
				{ "PerFace.ToolTip", "One polygroup per geometric face of primitive" },
				{ "PerQuad.Comment", "/** One polygroup per mesh quad/triangle */" },
				{ "PerQuad.Name", "EMakeMeshPolygroupMode::PerQuad" },
				{ "PerQuad.ToolTip", "One polygroup per mesh quad/triangle" },
				{ "Single.Comment", "/** One polygroup for entire output mesh */" },
				{ "Single.Name", "EMakeMeshPolygroupMode::Single" },
				{ "Single.ToolTip", "One polygroup for entire output mesh" },
				{ "ToolTip", "Polygroup mode for primitive" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EMakeMeshPolygroupMode",
				"EMakeMeshPolygroupMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EMakeMeshPivotLocation_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EMakeMeshPivotLocation, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EMakeMeshPivotLocation"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EMakeMeshPivotLocation>()
	{
		return EMakeMeshPivotLocation_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMakeMeshPivotLocation(EMakeMeshPivotLocation_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EMakeMeshPivotLocation"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EMakeMeshPivotLocation_Hash() { return 2577114256U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EMakeMeshPivotLocation()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMakeMeshPivotLocation"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EMakeMeshPivotLocation_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMakeMeshPivotLocation::Base", (int64)EMakeMeshPivotLocation::Base },
				{ "EMakeMeshPivotLocation::Centered", (int64)EMakeMeshPivotLocation::Centered },
				{ "EMakeMeshPivotLocation::Top", (int64)EMakeMeshPivotLocation::Top },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Base.Name", "EMakeMeshPivotLocation::Base" },
				{ "Centered.Name", "EMakeMeshPivotLocation::Centered" },
				{ "Comment", "/** Placement Pivot Location */" },
				{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
				{ "ToolTip", "Placement Pivot Location" },
				{ "Top.Name", "EMakeMeshPivotLocation::Top" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EMakeMeshPivotLocation",
				"EMakeMeshPivotLocation",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EMakeMeshPlacementType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EMakeMeshPlacementType, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EMakeMeshPlacementType"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EMakeMeshPlacementType>()
	{
		return EMakeMeshPlacementType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMakeMeshPlacementType(EMakeMeshPlacementType_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EMakeMeshPlacementType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EMakeMeshPlacementType_Hash() { return 230263904U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EMakeMeshPlacementType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMakeMeshPlacementType"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EMakeMeshPlacementType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMakeMeshPlacementType::GroundPlane", (int64)EMakeMeshPlacementType::GroundPlane },
				{ "EMakeMeshPlacementType::OnScene", (int64)EMakeMeshPlacementType::OnScene },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** Placement Target Types */" },
				{ "GroundPlane.Name", "EMakeMeshPlacementType::GroundPlane" },
				{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
				{ "OnScene.Name", "EMakeMeshPlacementType::OnScene" },
				{ "ToolTip", "Placement Target Types" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EMakeMeshPlacementType",
				"EMakeMeshPlacementType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UAddPrimitiveToolBuilder::StaticRegisterNativesUAddPrimitiveToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UAddPrimitiveToolBuilder_NoRegister()
	{
		return UAddPrimitiveToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UAddPrimitiveToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAddPrimitiveToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAddPrimitiveToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Builder\n */" },
		{ "IncludePath", "AddPrimitiveTool.h" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ToolTip", "Builder" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAddPrimitiveToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAddPrimitiveToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAddPrimitiveToolBuilder_Statics::ClassParams = {
		&UAddPrimitiveToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UAddPrimitiveToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAddPrimitiveToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAddPrimitiveToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAddPrimitiveToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAddPrimitiveToolBuilder, 3433017297);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UAddPrimitiveToolBuilder>()
	{
		return UAddPrimitiveToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAddPrimitiveToolBuilder(Z_Construct_UClass_UAddPrimitiveToolBuilder, &UAddPrimitiveToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UAddPrimitiveToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAddPrimitiveToolBuilder);
	void UProceduralShapeToolProperties::StaticRegisterNativesUProceduralShapeToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UProceduralShapeToolProperties_NoRegister()
	{
		return UProceduralShapeToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UProceduralShapeToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bInstanceIfPossible_MetaData[];
#endif
		static void NewProp_bInstanceIfPossible_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInstanceIfPossible;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_PolygroupMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PolygroupMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_PolygroupMode;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_PlaceMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlaceMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_PlaceMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSnapToGrid_MetaData[];
#endif
		static void NewProp_bSnapToGrid_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSnapToGrid;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_PivotLocation_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PivotLocation_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_PivotLocation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Rotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Rotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAlignShapeToPlacementSurface_MetaData[];
#endif
		static void NewProp_bAlignShapeToPlacementSurface_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAlignShapeToPlacementSurface;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UProceduralShapeToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralShapeToolProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AddPrimitiveTool.h" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_bInstanceIfPossible_MetaData[] = {
		{ "Category", "AssetSettings" },
		{ "Comment", "/** If the shape settings haven't changed, create instances of the last created asset rather than creating a whole new asset.  If false, all created actors will have separate underlying mesh assets. */" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ToolTip", "If the shape settings haven't changed, create instances of the last created asset rather than creating a whole new asset.  If false, all created actors will have separate underlying mesh assets." },
	};
#endif
	void Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_bInstanceIfPossible_SetBit(void* Obj)
	{
		((UProceduralShapeToolProperties*)Obj)->bInstanceIfPossible = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_bInstanceIfPossible = { "bInstanceIfPossible", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UProceduralShapeToolProperties), &Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_bInstanceIfPossible_SetBit, METADATA_PARAMS(Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_bInstanceIfPossible_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_bInstanceIfPossible_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_PolygroupMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_PolygroupMode_MetaData[] = {
		{ "Category", "ShapeSettings" },
		{ "Comment", "/** How should Polygroups be assigned to triangles of Primitive */" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ProceduralShapeSetting", "" },
		{ "ToolTip", "How should Polygroups be assigned to triangles of Primitive" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_PolygroupMode = { "PolygroupMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UProceduralShapeToolProperties, PolygroupMode), Z_Construct_UEnum_MeshModelingTools_EMakeMeshPolygroupMode, METADATA_PARAMS(Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_PolygroupMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_PolygroupMode_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_PlaceMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_PlaceMode_MetaData[] = {
		{ "Category", "Positioning" },
		{ "Comment", "/** How to place Primitive in the Scene */" },
		{ "DisplayName", "Target Surface" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ToolTip", "How to place Primitive in the Scene" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_PlaceMode = { "PlaceMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UProceduralShapeToolProperties, PlaceMode), Z_Construct_UEnum_MeshModelingTools_EMakeMeshPlacementType, METADATA_PARAMS(Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_PlaceMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_PlaceMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_bSnapToGrid_MetaData[] = {
		{ "Category", "Positioning" },
		{ "Comment", "/** If true, placement location will be snapped to grid. Only relevant when coordinate system is set to World. */" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ToolTip", "If true, placement location will be snapped to grid. Only relevant when coordinate system is set to World." },
	};
#endif
	void Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_bSnapToGrid_SetBit(void* Obj)
	{
		((UProceduralShapeToolProperties*)Obj)->bSnapToGrid = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_bSnapToGrid = { "bSnapToGrid", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UProceduralShapeToolProperties), &Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_bSnapToGrid_SetBit, METADATA_PARAMS(Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_bSnapToGrid_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_bSnapToGrid_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_PivotLocation_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_PivotLocation_MetaData[] = {
		{ "Category", "Positioning" },
		{ "Comment", "/** Location of Pivot within Primitive */" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ProceduralShapeSetting", "" },
		{ "ToolTip", "Location of Pivot within Primitive" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_PivotLocation = { "PivotLocation", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UProceduralShapeToolProperties, PivotLocation), Z_Construct_UEnum_MeshModelingTools_EMakeMeshPivotLocation, METADATA_PARAMS(Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_PivotLocation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_PivotLocation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_Rotation_MetaData[] = {
		{ "Category", "Positioning" },
		{ "Comment", "/** Rotation of Primitive around up axis */" },
		{ "DisplayName", "Rotation" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ToolTip", "Rotation of Primitive around up axis" },
		{ "UIMax", "360.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_Rotation = { "Rotation", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UProceduralShapeToolProperties, Rotation), METADATA_PARAMS(Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_Rotation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_Rotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_bAlignShapeToPlacementSurface_MetaData[] = {
		{ "Category", "Positioning" },
		{ "Comment", "/** Align shape to Placement Surface */" },
		{ "DisplayName", "Align to Normal" },
		{ "EditCondition", "PlaceMode == EMakeMeshPlacementType::OnScene" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ToolTip", "Align shape to Placement Surface" },
	};
#endif
	void Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_bAlignShapeToPlacementSurface_SetBit(void* Obj)
	{
		((UProceduralShapeToolProperties*)Obj)->bAlignShapeToPlacementSurface = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_bAlignShapeToPlacementSurface = { "bAlignShapeToPlacementSurface", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UProceduralShapeToolProperties), &Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_bAlignShapeToPlacementSurface_SetBit, METADATA_PARAMS(Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_bAlignShapeToPlacementSurface_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_bAlignShapeToPlacementSurface_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UProceduralShapeToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_bInstanceIfPossible,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_PolygroupMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_PolygroupMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_PlaceMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_PlaceMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_bSnapToGrid,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_PivotLocation_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_PivotLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_Rotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralShapeToolProperties_Statics::NewProp_bAlignShapeToPlacementSurface,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UProceduralShapeToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UProceduralShapeToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UProceduralShapeToolProperties_Statics::ClassParams = {
		&UProceduralShapeToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UProceduralShapeToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralShapeToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UProceduralShapeToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralShapeToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UProceduralShapeToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UProceduralShapeToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UProceduralShapeToolProperties, 2566747316);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UProceduralShapeToolProperties>()
	{
		return UProceduralShapeToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UProceduralShapeToolProperties(Z_Construct_UClass_UProceduralShapeToolProperties, &UProceduralShapeToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UProceduralShapeToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UProceduralShapeToolProperties);
	void UProceduralRectangleToolProperties::StaticRegisterNativesUProceduralRectangleToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UProceduralRectangleToolProperties_NoRegister()
	{
		return UProceduralRectangleToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UProceduralRectangleToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Width_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Width;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Depth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Depth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WidthSubdivisions_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_WidthSubdivisions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DepthSubdivisions_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_DepthSubdivisions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UProceduralRectangleToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UProceduralShapeToolProperties,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralRectangleToolProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AddPrimitiveTool.h" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralRectangleToolProperties_Statics::NewProp_Width_MetaData[] = {
		{ "Category", "ShapeSettings|Dimensions" },
		{ "ClampMax", "1000000.0" },
		{ "ClampMin", "0.0001" },
		{ "Comment", "/** Width of Shape */" },
		{ "DisplayName", "Width" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ProceduralShapeSetting", "" },
		{ "ToolTip", "Width of Shape" },
		{ "UIMax", "1000.0" },
		{ "UIMin", "1.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UProceduralRectangleToolProperties_Statics::NewProp_Width = { "Width", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UProceduralRectangleToolProperties, Width), METADATA_PARAMS(Z_Construct_UClass_UProceduralRectangleToolProperties_Statics::NewProp_Width_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralRectangleToolProperties_Statics::NewProp_Width_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralRectangleToolProperties_Statics::NewProp_Depth_MetaData[] = {
		{ "Category", "ShapeSettings|Dimensions" },
		{ "ClampMax", "1000000.0" },
		{ "ClampMin", "0.0001" },
		{ "Comment", "/** Depth of Shape */" },
		{ "DisplayName", "Depth" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ProceduralShapeSetting", "" },
		{ "ToolTip", "Depth of Shape" },
		{ "UIMax", "1000.0" },
		{ "UIMin", "1.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UProceduralRectangleToolProperties_Statics::NewProp_Depth = { "Depth", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UProceduralRectangleToolProperties, Depth), METADATA_PARAMS(Z_Construct_UClass_UProceduralRectangleToolProperties_Statics::NewProp_Depth_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralRectangleToolProperties_Statics::NewProp_Depth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralRectangleToolProperties_Statics::NewProp_WidthSubdivisions_MetaData[] = {
		{ "Category", "ShapeSettings|Subdivisions" },
		{ "ClampMax", "500" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Number of Subdivisions Along the Width */" },
		{ "DisplayName", "Width" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ProceduralShapeSetting", "" },
		{ "ToolTip", "Number of Subdivisions Along the Width" },
		{ "UIMax", "100" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UProceduralRectangleToolProperties_Statics::NewProp_WidthSubdivisions = { "WidthSubdivisions", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UProceduralRectangleToolProperties, WidthSubdivisions), METADATA_PARAMS(Z_Construct_UClass_UProceduralRectangleToolProperties_Statics::NewProp_WidthSubdivisions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralRectangleToolProperties_Statics::NewProp_WidthSubdivisions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralRectangleToolProperties_Statics::NewProp_DepthSubdivisions_MetaData[] = {
		{ "Category", "ShapeSettings|Subdivisions" },
		{ "ClampMax", "500" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Number of Subdivisions Along the Depth*/" },
		{ "DisplayName", "Depth" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ProceduralShapeSetting", "" },
		{ "ToolTip", "Number of Subdivisions Along the Depth" },
		{ "UIMax", "100" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UProceduralRectangleToolProperties_Statics::NewProp_DepthSubdivisions = { "DepthSubdivisions", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UProceduralRectangleToolProperties, DepthSubdivisions), METADATA_PARAMS(Z_Construct_UClass_UProceduralRectangleToolProperties_Statics::NewProp_DepthSubdivisions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralRectangleToolProperties_Statics::NewProp_DepthSubdivisions_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UProceduralRectangleToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralRectangleToolProperties_Statics::NewProp_Width,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralRectangleToolProperties_Statics::NewProp_Depth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralRectangleToolProperties_Statics::NewProp_WidthSubdivisions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralRectangleToolProperties_Statics::NewProp_DepthSubdivisions,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UProceduralRectangleToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UProceduralRectangleToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UProceduralRectangleToolProperties_Statics::ClassParams = {
		&UProceduralRectangleToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UProceduralRectangleToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralRectangleToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UProceduralRectangleToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralRectangleToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UProceduralRectangleToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UProceduralRectangleToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UProceduralRectangleToolProperties, 2333855998);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UProceduralRectangleToolProperties>()
	{
		return UProceduralRectangleToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UProceduralRectangleToolProperties(Z_Construct_UClass_UProceduralRectangleToolProperties, &UProceduralRectangleToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UProceduralRectangleToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UProceduralRectangleToolProperties);
	void UProceduralBoxToolProperties::StaticRegisterNativesUProceduralBoxToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UProceduralBoxToolProperties_NoRegister()
	{
		return UProceduralBoxToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UProceduralBoxToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Height_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Height;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HeightSubdivisions_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_HeightSubdivisions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UProceduralBoxToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UProceduralRectangleToolProperties,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralBoxToolProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AddPrimitiveTool.h" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralBoxToolProperties_Statics::NewProp_Height_MetaData[] = {
		{ "Category", "ShapeSettings|Dimensions" },
		{ "ClampMax", "1000000.0" },
		{ "ClampMin", "0.0001" },
		{ "Comment", "/** Height of Shape */" },
		{ "DisplayName", "Height" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ProceduralShapeSetting", "" },
		{ "ToolTip", "Height of Shape" },
		{ "UIMax", "1000.0" },
		{ "UIMin", "1.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UProceduralBoxToolProperties_Statics::NewProp_Height = { "Height", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UProceduralBoxToolProperties, Height), METADATA_PARAMS(Z_Construct_UClass_UProceduralBoxToolProperties_Statics::NewProp_Height_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralBoxToolProperties_Statics::NewProp_Height_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralBoxToolProperties_Statics::NewProp_HeightSubdivisions_MetaData[] = {
		{ "Category", "ShapeSettings|Subdivisions" },
		{ "ClampMax", "500" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Number of Subdivisions Along the Height */" },
		{ "DisplayName", "Height" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ProceduralShapeSetting", "" },
		{ "ToolTip", "Number of Subdivisions Along the Height" },
		{ "UIMax", "100" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UProceduralBoxToolProperties_Statics::NewProp_HeightSubdivisions = { "HeightSubdivisions", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UProceduralBoxToolProperties, HeightSubdivisions), METADATA_PARAMS(Z_Construct_UClass_UProceduralBoxToolProperties_Statics::NewProp_HeightSubdivisions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralBoxToolProperties_Statics::NewProp_HeightSubdivisions_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UProceduralBoxToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralBoxToolProperties_Statics::NewProp_Height,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralBoxToolProperties_Statics::NewProp_HeightSubdivisions,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UProceduralBoxToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UProceduralBoxToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UProceduralBoxToolProperties_Statics::ClassParams = {
		&UProceduralBoxToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UProceduralBoxToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralBoxToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UProceduralBoxToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralBoxToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UProceduralBoxToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UProceduralBoxToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UProceduralBoxToolProperties, 2025845132);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UProceduralBoxToolProperties>()
	{
		return UProceduralBoxToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UProceduralBoxToolProperties(Z_Construct_UClass_UProceduralBoxToolProperties, &UProceduralBoxToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UProceduralBoxToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UProceduralBoxToolProperties);
	void UProceduralRoundedRectangleToolProperties::StaticRegisterNativesUProceduralRoundedRectangleToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UProceduralRoundedRectangleToolProperties_NoRegister()
	{
		return UProceduralRoundedRectangleToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UProceduralRoundedRectangleToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CornerRadius_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CornerRadius;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CornerSlices_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_CornerSlices;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UProceduralRoundedRectangleToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UProceduralRectangleToolProperties,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralRoundedRectangleToolProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AddPrimitiveTool.h" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralRoundedRectangleToolProperties_Statics::NewProp_CornerRadius_MetaData[] = {
		{ "Category", "ShapeSettings|Dimensions" },
		{ "ClampMax", "1000000.0" },
		{ "ClampMin", "0.0001" },
		{ "Comment", "/** Radius of Rounded Corners */" },
		{ "DisplayName", "Corner Radius" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ProceduralShapeSetting", "" },
		{ "ToolTip", "Radius of Rounded Corners" },
		{ "UIMax", "1000.0" },
		{ "UIMin", "1.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UProceduralRoundedRectangleToolProperties_Statics::NewProp_CornerRadius = { "CornerRadius", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UProceduralRoundedRectangleToolProperties, CornerRadius), METADATA_PARAMS(Z_Construct_UClass_UProceduralRoundedRectangleToolProperties_Statics::NewProp_CornerRadius_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralRoundedRectangleToolProperties_Statics::NewProp_CornerRadius_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralRoundedRectangleToolProperties_Statics::NewProp_CornerSlices_MetaData[] = {
		{ "Category", "ShapeSettings|Slices" },
		{ "ClampMax", "500" },
		{ "ClampMin", "3" },
		{ "Comment", "/** Number of Angular Slices in Each Corner*/" },
		{ "DisplayName", "Corner" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ProceduralShapeSetting", "" },
		{ "ToolTip", "Number of Angular Slices in Each Corner" },
		{ "UIMax", "128" },
		{ "UIMin", "3" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UProceduralRoundedRectangleToolProperties_Statics::NewProp_CornerSlices = { "CornerSlices", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UProceduralRoundedRectangleToolProperties, CornerSlices), METADATA_PARAMS(Z_Construct_UClass_UProceduralRoundedRectangleToolProperties_Statics::NewProp_CornerSlices_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralRoundedRectangleToolProperties_Statics::NewProp_CornerSlices_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UProceduralRoundedRectangleToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralRoundedRectangleToolProperties_Statics::NewProp_CornerRadius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralRoundedRectangleToolProperties_Statics::NewProp_CornerSlices,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UProceduralRoundedRectangleToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UProceduralRoundedRectangleToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UProceduralRoundedRectangleToolProperties_Statics::ClassParams = {
		&UProceduralRoundedRectangleToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UProceduralRoundedRectangleToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralRoundedRectangleToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UProceduralRoundedRectangleToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralRoundedRectangleToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UProceduralRoundedRectangleToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UProceduralRoundedRectangleToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UProceduralRoundedRectangleToolProperties, 1551444346);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UProceduralRoundedRectangleToolProperties>()
	{
		return UProceduralRoundedRectangleToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UProceduralRoundedRectangleToolProperties(Z_Construct_UClass_UProceduralRoundedRectangleToolProperties, &UProceduralRoundedRectangleToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UProceduralRoundedRectangleToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UProceduralRoundedRectangleToolProperties);
	void UProceduralDiscToolProperties::StaticRegisterNativesUProceduralDiscToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UProceduralDiscToolProperties_NoRegister()
	{
		return UProceduralDiscToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UProceduralDiscToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Radius_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Radius;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RadialSlices_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_RadialSlices;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RadialSubdivisions_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_RadialSubdivisions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UProceduralDiscToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UProceduralShapeToolProperties,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralDiscToolProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AddPrimitiveTool.h" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralDiscToolProperties_Statics::NewProp_Radius_MetaData[] = {
		{ "Category", "ShapeSettings|Dimensions" },
		{ "ClampMax", "1000000.0" },
		{ "ClampMin", "0.0001" },
		{ "Comment", "/** Radius of Disc */" },
		{ "DisplayName", "Radius" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ProceduralShapeSetting", "" },
		{ "ToolTip", "Radius of Disc" },
		{ "UIMax", "1000.0" },
		{ "UIMin", "1.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UProceduralDiscToolProperties_Statics::NewProp_Radius = { "Radius", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UProceduralDiscToolProperties, Radius), METADATA_PARAMS(Z_Construct_UClass_UProceduralDiscToolProperties_Statics::NewProp_Radius_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralDiscToolProperties_Statics::NewProp_Radius_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralDiscToolProperties_Statics::NewProp_RadialSlices_MetaData[] = {
		{ "Category", "ShapeSettings|Slices" },
		{ "ClampMax", "500" },
		{ "ClampMin", "3" },
		{ "Comment", "/** Number of Angular Slices around the Disc */" },
		{ "DisplayName", "Radial" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ProceduralShapeSetting", "" },
		{ "ToolTip", "Number of Angular Slices around the Disc" },
		{ "UIMax", "128" },
		{ "UIMin", "3" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UProceduralDiscToolProperties_Statics::NewProp_RadialSlices = { "RadialSlices", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UProceduralDiscToolProperties, RadialSlices), METADATA_PARAMS(Z_Construct_UClass_UProceduralDiscToolProperties_Statics::NewProp_RadialSlices_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralDiscToolProperties_Statics::NewProp_RadialSlices_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralDiscToolProperties_Statics::NewProp_RadialSubdivisions_MetaData[] = {
		{ "Category", "ShapeSettings|Subdivisions" },
		{ "ClampMax", "500" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Number of Radial Subdivisions in the Disc */" },
		{ "DisplayName", "Radial" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ProceduralShapeSetting", "" },
		{ "ToolTip", "Number of Radial Subdivisions in the Disc" },
		{ "UIMax", "100" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UProceduralDiscToolProperties_Statics::NewProp_RadialSubdivisions = { "RadialSubdivisions", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UProceduralDiscToolProperties, RadialSubdivisions), METADATA_PARAMS(Z_Construct_UClass_UProceduralDiscToolProperties_Statics::NewProp_RadialSubdivisions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralDiscToolProperties_Statics::NewProp_RadialSubdivisions_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UProceduralDiscToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralDiscToolProperties_Statics::NewProp_Radius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralDiscToolProperties_Statics::NewProp_RadialSlices,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralDiscToolProperties_Statics::NewProp_RadialSubdivisions,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UProceduralDiscToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UProceduralDiscToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UProceduralDiscToolProperties_Statics::ClassParams = {
		&UProceduralDiscToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UProceduralDiscToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralDiscToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UProceduralDiscToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralDiscToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UProceduralDiscToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UProceduralDiscToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UProceduralDiscToolProperties, 1793492579);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UProceduralDiscToolProperties>()
	{
		return UProceduralDiscToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UProceduralDiscToolProperties(Z_Construct_UClass_UProceduralDiscToolProperties, &UProceduralDiscToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UProceduralDiscToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UProceduralDiscToolProperties);
	void UProceduralPuncturedDiscToolProperties::StaticRegisterNativesUProceduralPuncturedDiscToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UProceduralPuncturedDiscToolProperties_NoRegister()
	{
		return UProceduralPuncturedDiscToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UProceduralPuncturedDiscToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HoleRadius_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HoleRadius;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UProceduralPuncturedDiscToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UProceduralDiscToolProperties,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralPuncturedDiscToolProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AddPrimitiveTool.h" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralPuncturedDiscToolProperties_Statics::NewProp_HoleRadius_MetaData[] = {
		{ "Category", "ShapeSettings|Dimensions" },
		{ "ClampMax", "1000000.0" },
		{ "ClampMin", "0.0001" },
		{ "Comment", "/** Radius of the Disc's Hole */" },
		{ "DisplayName", "Hole Radius" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ProceduralShapeSetting", "" },
		{ "ToolTip", "Radius of the Disc's Hole" },
		{ "UIMax", "1000.0" },
		{ "UIMin", "1.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UProceduralPuncturedDiscToolProperties_Statics::NewProp_HoleRadius = { "HoleRadius", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UProceduralPuncturedDiscToolProperties, HoleRadius), METADATA_PARAMS(Z_Construct_UClass_UProceduralPuncturedDiscToolProperties_Statics::NewProp_HoleRadius_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralPuncturedDiscToolProperties_Statics::NewProp_HoleRadius_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UProceduralPuncturedDiscToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralPuncturedDiscToolProperties_Statics::NewProp_HoleRadius,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UProceduralPuncturedDiscToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UProceduralPuncturedDiscToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UProceduralPuncturedDiscToolProperties_Statics::ClassParams = {
		&UProceduralPuncturedDiscToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UProceduralPuncturedDiscToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralPuncturedDiscToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UProceduralPuncturedDiscToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralPuncturedDiscToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UProceduralPuncturedDiscToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UProceduralPuncturedDiscToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UProceduralPuncturedDiscToolProperties, 1478487619);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UProceduralPuncturedDiscToolProperties>()
	{
		return UProceduralPuncturedDiscToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UProceduralPuncturedDiscToolProperties(Z_Construct_UClass_UProceduralPuncturedDiscToolProperties, &UProceduralPuncturedDiscToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UProceduralPuncturedDiscToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UProceduralPuncturedDiscToolProperties);
	void UProceduralTorusToolProperties::StaticRegisterNativesUProceduralTorusToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UProceduralTorusToolProperties_NoRegister()
	{
		return UProceduralTorusToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UProceduralTorusToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MajorRadius_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MajorRadius;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MinorRadius_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MinorRadius;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TubeSlices_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_TubeSlices;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CrossSectionSlices_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_CrossSectionSlices;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UProceduralTorusToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UProceduralShapeToolProperties,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralTorusToolProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AddPrimitiveTool.h" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralTorusToolProperties_Statics::NewProp_MajorRadius_MetaData[] = {
		{ "Category", "ShapeSettings|Dimensions" },
		{ "ClampMax", "1000000.0" },
		{ "ClampMin", "0.0001" },
		{ "Comment", "/** Radius from the Torus Center to the Center of the Torus Tube */" },
		{ "DisplayName", "Major Radius" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ProceduralShapeSetting", "" },
		{ "ToolTip", "Radius from the Torus Center to the Center of the Torus Tube" },
		{ "UIMax", "1000.0" },
		{ "UIMin", "1.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UProceduralTorusToolProperties_Statics::NewProp_MajorRadius = { "MajorRadius", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UProceduralTorusToolProperties, MajorRadius), METADATA_PARAMS(Z_Construct_UClass_UProceduralTorusToolProperties_Statics::NewProp_MajorRadius_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralTorusToolProperties_Statics::NewProp_MajorRadius_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralTorusToolProperties_Statics::NewProp_MinorRadius_MetaData[] = {
		{ "Category", "ShapeSettings|Dimensions" },
		{ "ClampMax", "1000000.0" },
		{ "ClampMin", "0.0001" },
		{ "Comment", "/** Radius of the Torus Tube */" },
		{ "DisplayName", "Minor Radius" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ProceduralShapeSetting", "" },
		{ "ToolTip", "Radius of the Torus Tube" },
		{ "UIMax", "1000.0" },
		{ "UIMin", "1.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UProceduralTorusToolProperties_Statics::NewProp_MinorRadius = { "MinorRadius", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UProceduralTorusToolProperties, MinorRadius), METADATA_PARAMS(Z_Construct_UClass_UProceduralTorusToolProperties_Statics::NewProp_MinorRadius_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralTorusToolProperties_Statics::NewProp_MinorRadius_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralTorusToolProperties_Statics::NewProp_TubeSlices_MetaData[] = {
		{ "Category", "ShapeSettings|Slices" },
		{ "ClampMax", "500" },
		{ "ClampMin", "3" },
		{ "Comment", "/** Number of Angular Slices Along the Torus Tube */" },
		{ "DisplayName", "Major" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ProceduralShapeSetting", "" },
		{ "ToolTip", "Number of Angular Slices Along the Torus Tube" },
		{ "UIMax", "128" },
		{ "UIMin", "3" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UProceduralTorusToolProperties_Statics::NewProp_TubeSlices = { "TubeSlices", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UProceduralTorusToolProperties, TubeSlices), METADATA_PARAMS(Z_Construct_UClass_UProceduralTorusToolProperties_Statics::NewProp_TubeSlices_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralTorusToolProperties_Statics::NewProp_TubeSlices_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralTorusToolProperties_Statics::NewProp_CrossSectionSlices_MetaData[] = {
		{ "Category", "ShapeSettings|Slices" },
		{ "ClampMax", "500" },
		{ "ClampMin", "3" },
		{ "Comment", "/** Number of Angular Slices Around the Tube of the Torus */" },
		{ "DisplayName", "Minor" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ProceduralShapeSetting", "" },
		{ "ToolTip", "Number of Angular Slices Around the Tube of the Torus" },
		{ "UIMax", "128" },
		{ "UIMin", "3" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UProceduralTorusToolProperties_Statics::NewProp_CrossSectionSlices = { "CrossSectionSlices", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UProceduralTorusToolProperties, CrossSectionSlices), METADATA_PARAMS(Z_Construct_UClass_UProceduralTorusToolProperties_Statics::NewProp_CrossSectionSlices_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralTorusToolProperties_Statics::NewProp_CrossSectionSlices_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UProceduralTorusToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralTorusToolProperties_Statics::NewProp_MajorRadius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralTorusToolProperties_Statics::NewProp_MinorRadius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralTorusToolProperties_Statics::NewProp_TubeSlices,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralTorusToolProperties_Statics::NewProp_CrossSectionSlices,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UProceduralTorusToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UProceduralTorusToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UProceduralTorusToolProperties_Statics::ClassParams = {
		&UProceduralTorusToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UProceduralTorusToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralTorusToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UProceduralTorusToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralTorusToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UProceduralTorusToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UProceduralTorusToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UProceduralTorusToolProperties, 1361093506);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UProceduralTorusToolProperties>()
	{
		return UProceduralTorusToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UProceduralTorusToolProperties(Z_Construct_UClass_UProceduralTorusToolProperties, &UProceduralTorusToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UProceduralTorusToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UProceduralTorusToolProperties);
	void UProceduralCylinderToolProperties::StaticRegisterNativesUProceduralCylinderToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UProceduralCylinderToolProperties_NoRegister()
	{
		return UProceduralCylinderToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UProceduralCylinderToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Radius_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Radius;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Height_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Height;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RadialSlices_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_RadialSlices;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HeightSubdivisions_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_HeightSubdivisions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UProceduralCylinderToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UProceduralShapeToolProperties,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralCylinderToolProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AddPrimitiveTool.h" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralCylinderToolProperties_Statics::NewProp_Radius_MetaData[] = {
		{ "Category", "ShapeSettings|Dimensions" },
		{ "ClampMax", "1000000.0" },
		{ "ClampMin", "0.0001" },
		{ "Comment", "/** Radius of The Cylinder */" },
		{ "DisplayName", "Radius" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ProceduralShapeSetting", "" },
		{ "ToolTip", "Radius of The Cylinder" },
		{ "UIMax", "1000.0" },
		{ "UIMin", "1.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UProceduralCylinderToolProperties_Statics::NewProp_Radius = { "Radius", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UProceduralCylinderToolProperties, Radius), METADATA_PARAMS(Z_Construct_UClass_UProceduralCylinderToolProperties_Statics::NewProp_Radius_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralCylinderToolProperties_Statics::NewProp_Radius_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralCylinderToolProperties_Statics::NewProp_Height_MetaData[] = {
		{ "Category", "ShapeSettings|Dimensions" },
		{ "ClampMax", "1000000.0" },
		{ "ClampMin", "0.0001" },
		{ "Comment", "/** Height of Cylinder */" },
		{ "DisplayName", "Height" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ProceduralShapeSetting", "" },
		{ "ToolTip", "Height of Cylinder" },
		{ "UIMax", "1000.0" },
		{ "UIMin", "1.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UProceduralCylinderToolProperties_Statics::NewProp_Height = { "Height", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UProceduralCylinderToolProperties, Height), METADATA_PARAMS(Z_Construct_UClass_UProceduralCylinderToolProperties_Statics::NewProp_Height_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralCylinderToolProperties_Statics::NewProp_Height_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralCylinderToolProperties_Statics::NewProp_RadialSlices_MetaData[] = {
		{ "Category", "ShapeSettings|Slices" },
		{ "ClampMax", "500" },
		{ "ClampMin", "3" },
		{ "Comment", "/** Number of Slices on the Cylinder Caps */" },
		{ "DisplayName", "Radial" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ProceduralShapeSetting", "" },
		{ "ToolTip", "Number of Slices on the Cylinder Caps" },
		{ "UIMax", "128" },
		{ "UIMin", "3" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UProceduralCylinderToolProperties_Statics::NewProp_RadialSlices = { "RadialSlices", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UProceduralCylinderToolProperties, RadialSlices), METADATA_PARAMS(Z_Construct_UClass_UProceduralCylinderToolProperties_Statics::NewProp_RadialSlices_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralCylinderToolProperties_Statics::NewProp_RadialSlices_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralCylinderToolProperties_Statics::NewProp_HeightSubdivisions_MetaData[] = {
		{ "Category", "ShapeSettings|Subdivisions" },
		{ "ClampMax", "500" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Number of Vertical Subdivisions Along the Height of the Cylidner */" },
		{ "DisplayName", "Height" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ProceduralShapeSetting", "" },
		{ "ToolTip", "Number of Vertical Subdivisions Along the Height of the Cylidner" },
		{ "UIMax", "100" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UProceduralCylinderToolProperties_Statics::NewProp_HeightSubdivisions = { "HeightSubdivisions", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UProceduralCylinderToolProperties, HeightSubdivisions), METADATA_PARAMS(Z_Construct_UClass_UProceduralCylinderToolProperties_Statics::NewProp_HeightSubdivisions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralCylinderToolProperties_Statics::NewProp_HeightSubdivisions_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UProceduralCylinderToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralCylinderToolProperties_Statics::NewProp_Radius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralCylinderToolProperties_Statics::NewProp_Height,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralCylinderToolProperties_Statics::NewProp_RadialSlices,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralCylinderToolProperties_Statics::NewProp_HeightSubdivisions,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UProceduralCylinderToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UProceduralCylinderToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UProceduralCylinderToolProperties_Statics::ClassParams = {
		&UProceduralCylinderToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UProceduralCylinderToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralCylinderToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UProceduralCylinderToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralCylinderToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UProceduralCylinderToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UProceduralCylinderToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UProceduralCylinderToolProperties, 1838031728);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UProceduralCylinderToolProperties>()
	{
		return UProceduralCylinderToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UProceduralCylinderToolProperties(Z_Construct_UClass_UProceduralCylinderToolProperties, &UProceduralCylinderToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UProceduralCylinderToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UProceduralCylinderToolProperties);
	void UProceduralConeToolProperties::StaticRegisterNativesUProceduralConeToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UProceduralConeToolProperties_NoRegister()
	{
		return UProceduralConeToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UProceduralConeToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Radius_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Radius;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Height_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Height;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RadialSlices_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_RadialSlices;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HeightSubdivisions_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_HeightSubdivisions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UProceduralConeToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UProceduralShapeToolProperties,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralConeToolProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AddPrimitiveTool.h" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralConeToolProperties_Statics::NewProp_Radius_MetaData[] = {
		{ "Category", "ShapeSettings|Dimensions" },
		{ "ClampMax", "1000000.0" },
		{ "ClampMin", "0.0001" },
		{ "Comment", "/** Radius of the Cone */" },
		{ "DisplayName", "Radius" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ProceduralShapeSetting", "" },
		{ "ToolTip", "Radius of the Cone" },
		{ "UIMax", "1000.0" },
		{ "UIMin", "1.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UProceduralConeToolProperties_Statics::NewProp_Radius = { "Radius", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UProceduralConeToolProperties, Radius), METADATA_PARAMS(Z_Construct_UClass_UProceduralConeToolProperties_Statics::NewProp_Radius_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralConeToolProperties_Statics::NewProp_Radius_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralConeToolProperties_Statics::NewProp_Height_MetaData[] = {
		{ "Category", "ShapeSettings|Dimensions" },
		{ "ClampMax", "1000000.0" },
		{ "ClampMin", "0.0001" },
		{ "Comment", "/** Height of Cone */" },
		{ "DisplayName", "Height" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ProceduralShapeSetting", "" },
		{ "ToolTip", "Height of Cone" },
		{ "UIMax", "1000.0" },
		{ "UIMin", "1.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UProceduralConeToolProperties_Statics::NewProp_Height = { "Height", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UProceduralConeToolProperties, Height), METADATA_PARAMS(Z_Construct_UClass_UProceduralConeToolProperties_Statics::NewProp_Height_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralConeToolProperties_Statics::NewProp_Height_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralConeToolProperties_Statics::NewProp_RadialSlices_MetaData[] = {
		{ "Category", "ShapeSettings|Slices" },
		{ "ClampMax", "500" },
		{ "ClampMin", "3" },
		{ "Comment", "/** Number of Slices on the Cone Base */" },
		{ "DisplayName", "Radial" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ProceduralShapeSetting", "" },
		{ "ToolTip", "Number of Slices on the Cone Base" },
		{ "UIMax", "128" },
		{ "UIMin", "3" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UProceduralConeToolProperties_Statics::NewProp_RadialSlices = { "RadialSlices", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UProceduralConeToolProperties, RadialSlices), METADATA_PARAMS(Z_Construct_UClass_UProceduralConeToolProperties_Statics::NewProp_RadialSlices_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralConeToolProperties_Statics::NewProp_RadialSlices_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralConeToolProperties_Statics::NewProp_HeightSubdivisions_MetaData[] = {
		{ "Category", "ShapeSettings|Subdivisions" },
		{ "ClampMax", "500" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Number of Vertical Subdivisions Along the Hight of the Cone */" },
		{ "DisplayName", "Height" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ProceduralShapeSetting", "" },
		{ "ToolTip", "Number of Vertical Subdivisions Along the Hight of the Cone" },
		{ "UIMax", "100" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UProceduralConeToolProperties_Statics::NewProp_HeightSubdivisions = { "HeightSubdivisions", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UProceduralConeToolProperties, HeightSubdivisions), METADATA_PARAMS(Z_Construct_UClass_UProceduralConeToolProperties_Statics::NewProp_HeightSubdivisions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralConeToolProperties_Statics::NewProp_HeightSubdivisions_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UProceduralConeToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralConeToolProperties_Statics::NewProp_Radius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralConeToolProperties_Statics::NewProp_Height,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralConeToolProperties_Statics::NewProp_RadialSlices,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralConeToolProperties_Statics::NewProp_HeightSubdivisions,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UProceduralConeToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UProceduralConeToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UProceduralConeToolProperties_Statics::ClassParams = {
		&UProceduralConeToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UProceduralConeToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralConeToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UProceduralConeToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralConeToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UProceduralConeToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UProceduralConeToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UProceduralConeToolProperties, 3463323925);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UProceduralConeToolProperties>()
	{
		return UProceduralConeToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UProceduralConeToolProperties(Z_Construct_UClass_UProceduralConeToolProperties, &UProceduralConeToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UProceduralConeToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UProceduralConeToolProperties);
	void UProceduralArrowToolProperties::StaticRegisterNativesUProceduralArrowToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UProceduralArrowToolProperties_NoRegister()
	{
		return UProceduralArrowToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UProceduralArrowToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShaftRadius_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ShaftRadius;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShaftHeight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ShaftHeight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HeadRadius_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HeadRadius;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HeadHeight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HeadHeight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RadialSlices_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_RadialSlices;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TotalSubdivisions_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_TotalSubdivisions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UProceduralArrowToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UProceduralShapeToolProperties,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralArrowToolProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AddPrimitiveTool.h" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralArrowToolProperties_Statics::NewProp_ShaftRadius_MetaData[] = {
		{ "Category", "ShapeSettings|Dimensions" },
		{ "ClampMax", "1000000.0" },
		{ "ClampMin", "0.0001" },
		{ "Comment", "/** Radius of the Arrow Shaft */" },
		{ "DisplayName", "Shaft Radius" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ProceduralShapeSetting", "" },
		{ "ToolTip", "Radius of the Arrow Shaft" },
		{ "UIMax", "1000.0" },
		{ "UIMin", "1.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UProceduralArrowToolProperties_Statics::NewProp_ShaftRadius = { "ShaftRadius", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UProceduralArrowToolProperties, ShaftRadius), METADATA_PARAMS(Z_Construct_UClass_UProceduralArrowToolProperties_Statics::NewProp_ShaftRadius_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralArrowToolProperties_Statics::NewProp_ShaftRadius_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralArrowToolProperties_Statics::NewProp_ShaftHeight_MetaData[] = {
		{ "Category", "ShapeSettings|Dimensions" },
		{ "ClampMax", "1000000.0" },
		{ "ClampMin", "0.0001" },
		{ "Comment", "/** Height of Arrow Shaft */" },
		{ "DisplayName", "Shaft Height" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ProceduralShapeSetting", "" },
		{ "ToolTip", "Height of Arrow Shaft" },
		{ "UIMax", "1000.0" },
		{ "UIMin", "1.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UProceduralArrowToolProperties_Statics::NewProp_ShaftHeight = { "ShaftHeight", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UProceduralArrowToolProperties, ShaftHeight), METADATA_PARAMS(Z_Construct_UClass_UProceduralArrowToolProperties_Statics::NewProp_ShaftHeight_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralArrowToolProperties_Statics::NewProp_ShaftHeight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralArrowToolProperties_Statics::NewProp_HeadRadius_MetaData[] = {
		{ "Category", "ShapeSettings|Dimensions" },
		{ "ClampMax", "1000000.0" },
		{ "ClampMin", "0.0001" },
		{ "Comment", "/** Radius of the Arrow Head */" },
		{ "DisplayName", "Head Radius" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ProceduralShapeSetting", "" },
		{ "ToolTip", "Radius of the Arrow Head" },
		{ "UIMax", "1000.0" },
		{ "UIMin", "1.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UProceduralArrowToolProperties_Statics::NewProp_HeadRadius = { "HeadRadius", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UProceduralArrowToolProperties, HeadRadius), METADATA_PARAMS(Z_Construct_UClass_UProceduralArrowToolProperties_Statics::NewProp_HeadRadius_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralArrowToolProperties_Statics::NewProp_HeadRadius_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralArrowToolProperties_Statics::NewProp_HeadHeight_MetaData[] = {
		{ "Category", "ShapeSettings|Dimensions" },
		{ "ClampMax", "1000000.0" },
		{ "ClampMin", "0.0001" },
		{ "Comment", "/** Height of Arrow's Head */" },
		{ "DisplayName", "Head Height" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ProceduralShapeSetting", "" },
		{ "ToolTip", "Height of Arrow's Head" },
		{ "UIMax", "1000.0" },
		{ "UIMin", "1.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UProceduralArrowToolProperties_Statics::NewProp_HeadHeight = { "HeadHeight", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UProceduralArrowToolProperties, HeadHeight), METADATA_PARAMS(Z_Construct_UClass_UProceduralArrowToolProperties_Statics::NewProp_HeadHeight_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralArrowToolProperties_Statics::NewProp_HeadHeight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralArrowToolProperties_Statics::NewProp_RadialSlices_MetaData[] = {
		{ "Category", "ShapeSettings|Slices" },
		{ "ClampMax", "500" },
		{ "ClampMin", "3" },
		{ "Comment", "/** Number of Angular Slices Around the Arrow */" },
		{ "DisplayName", "Radial Slices" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ProceduralShapeSetting", "" },
		{ "ToolTip", "Number of Angular Slices Around the Arrow" },
		{ "UIMax", "100" },
		{ "UIMin", "3" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UProceduralArrowToolProperties_Statics::NewProp_RadialSlices = { "RadialSlices", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UProceduralArrowToolProperties, RadialSlices), METADATA_PARAMS(Z_Construct_UClass_UProceduralArrowToolProperties_Statics::NewProp_RadialSlices_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralArrowToolProperties_Statics::NewProp_RadialSlices_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralArrowToolProperties_Statics::NewProp_TotalSubdivisions_MetaData[] = {
		{ "Category", "ShapeSettings|Subdivisions" },
		{ "ClampMax", "500" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Number of Vertical Subdivisions Along in the Arrow */" },
		{ "DisplayName", "Total" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ProceduralShapeSetting", "" },
		{ "ToolTip", "Number of Vertical Subdivisions Along in the Arrow" },
		{ "UIMax", "100" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UProceduralArrowToolProperties_Statics::NewProp_TotalSubdivisions = { "TotalSubdivisions", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UProceduralArrowToolProperties, TotalSubdivisions), METADATA_PARAMS(Z_Construct_UClass_UProceduralArrowToolProperties_Statics::NewProp_TotalSubdivisions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralArrowToolProperties_Statics::NewProp_TotalSubdivisions_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UProceduralArrowToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralArrowToolProperties_Statics::NewProp_ShaftRadius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralArrowToolProperties_Statics::NewProp_ShaftHeight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralArrowToolProperties_Statics::NewProp_HeadRadius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralArrowToolProperties_Statics::NewProp_HeadHeight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralArrowToolProperties_Statics::NewProp_RadialSlices,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralArrowToolProperties_Statics::NewProp_TotalSubdivisions,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UProceduralArrowToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UProceduralArrowToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UProceduralArrowToolProperties_Statics::ClassParams = {
		&UProceduralArrowToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UProceduralArrowToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralArrowToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UProceduralArrowToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralArrowToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UProceduralArrowToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UProceduralArrowToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UProceduralArrowToolProperties, 3732641373);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UProceduralArrowToolProperties>()
	{
		return UProceduralArrowToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UProceduralArrowToolProperties(Z_Construct_UClass_UProceduralArrowToolProperties, &UProceduralArrowToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UProceduralArrowToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UProceduralArrowToolProperties);
	void UProceduralSphereToolProperties::StaticRegisterNativesUProceduralSphereToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UProceduralSphereToolProperties_NoRegister()
	{
		return UProceduralSphereToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UProceduralSphereToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Radius_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Radius;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LatitudeSlices_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_LatitudeSlices;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LongitudeSlices_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_LongitudeSlices;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UProceduralSphereToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UProceduralShapeToolProperties,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralSphereToolProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AddPrimitiveTool.h" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralSphereToolProperties_Statics::NewProp_Radius_MetaData[] = {
		{ "Category", "ShapeSettings|Dimensions" },
		{ "ClampMax", "1000000.0" },
		{ "ClampMin", "0.0001" },
		{ "Comment", "/** Radius of the Sphere */" },
		{ "DisplayName", "Radius" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ProceduralShapeSetting", "" },
		{ "ToolTip", "Radius of the Sphere" },
		{ "UIMax", "1000.0" },
		{ "UIMin", "1.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UProceduralSphereToolProperties_Statics::NewProp_Radius = { "Radius", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UProceduralSphereToolProperties, Radius), METADATA_PARAMS(Z_Construct_UClass_UProceduralSphereToolProperties_Statics::NewProp_Radius_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralSphereToolProperties_Statics::NewProp_Radius_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralSphereToolProperties_Statics::NewProp_LatitudeSlices_MetaData[] = {
		{ "Category", "ShapeSettings|Slices" },
		{ "ClampMax", "500" },
		{ "ClampMin", "4" },
		{ "Comment", "/** Number of Latitudinal Slices of the Sphere */" },
		{ "DisplayName", "Latitude Slices" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ProceduralShapeSetting", "" },
		{ "ToolTip", "Number of Latitudinal Slices of the Sphere" },
		{ "UIMax", "100" },
		{ "UIMin", "3" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UProceduralSphereToolProperties_Statics::NewProp_LatitudeSlices = { "LatitudeSlices", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UProceduralSphereToolProperties, LatitudeSlices), METADATA_PARAMS(Z_Construct_UClass_UProceduralSphereToolProperties_Statics::NewProp_LatitudeSlices_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralSphereToolProperties_Statics::NewProp_LatitudeSlices_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralSphereToolProperties_Statics::NewProp_LongitudeSlices_MetaData[] = {
		{ "Category", "ShapeSettings|Slices" },
		{ "ClampMax", "500" },
		{ "ClampMin", "4" },
		{ "Comment", "/** Number of Longitudinal Slices around the Sphere */" },
		{ "DisplayName", "Longitude Slices" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ProceduralShapeSetting", "" },
		{ "ToolTip", "Number of Longitudinal Slices around the Sphere" },
		{ "UIMax", "100" },
		{ "UIMin", "3" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UProceduralSphereToolProperties_Statics::NewProp_LongitudeSlices = { "LongitudeSlices", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UProceduralSphereToolProperties, LongitudeSlices), METADATA_PARAMS(Z_Construct_UClass_UProceduralSphereToolProperties_Statics::NewProp_LongitudeSlices_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralSphereToolProperties_Statics::NewProp_LongitudeSlices_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UProceduralSphereToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralSphereToolProperties_Statics::NewProp_Radius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralSphereToolProperties_Statics::NewProp_LatitudeSlices,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralSphereToolProperties_Statics::NewProp_LongitudeSlices,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UProceduralSphereToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UProceduralSphereToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UProceduralSphereToolProperties_Statics::ClassParams = {
		&UProceduralSphereToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UProceduralSphereToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralSphereToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UProceduralSphereToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralSphereToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UProceduralSphereToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UProceduralSphereToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UProceduralSphereToolProperties, 2897438298);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UProceduralSphereToolProperties>()
	{
		return UProceduralSphereToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UProceduralSphereToolProperties(Z_Construct_UClass_UProceduralSphereToolProperties, &UProceduralSphereToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UProceduralSphereToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UProceduralSphereToolProperties);
	void UProceduralSphericalBoxToolProperties::StaticRegisterNativesUProceduralSphericalBoxToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UProceduralSphericalBoxToolProperties_NoRegister()
	{
		return UProceduralSphericalBoxToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UProceduralSphericalBoxToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Radius_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Radius;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Subdivisions_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Subdivisions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UProceduralSphericalBoxToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UProceduralShapeToolProperties,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralSphericalBoxToolProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AddPrimitiveTool.h" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralSphericalBoxToolProperties_Statics::NewProp_Radius_MetaData[] = {
		{ "Category", "ShapeSettings|Dimensions" },
		{ "ClampMax", "1000000.0" },
		{ "ClampMin", "0.0001" },
		{ "Comment", "/** Radius of the Sphere */" },
		{ "DisplayName", "Radius" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ProceduralShapeSetting", "" },
		{ "ToolTip", "Radius of the Sphere" },
		{ "UIMax", "1000.0" },
		{ "UIMin", "1.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UProceduralSphericalBoxToolProperties_Statics::NewProp_Radius = { "Radius", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UProceduralSphericalBoxToolProperties, Radius), METADATA_PARAMS(Z_Construct_UClass_UProceduralSphericalBoxToolProperties_Statics::NewProp_Radius_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralSphericalBoxToolProperties_Statics::NewProp_Radius_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProceduralSphericalBoxToolProperties_Statics::NewProp_Subdivisions_MetaData[] = {
		{ "Category", "ShapeSettings|Subdivisions" },
		{ "ClampMax", "500" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Number of Subdivisions of each Side of the Sphere */" },
		{ "DisplayName", "Side" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ProceduralShapeSetting", "" },
		{ "ToolTip", "Number of Subdivisions of each Side of the Sphere" },
		{ "UIMax", "100" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UProceduralSphericalBoxToolProperties_Statics::NewProp_Subdivisions = { "Subdivisions", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UProceduralSphericalBoxToolProperties, Subdivisions), METADATA_PARAMS(Z_Construct_UClass_UProceduralSphericalBoxToolProperties_Statics::NewProp_Subdivisions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralSphericalBoxToolProperties_Statics::NewProp_Subdivisions_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UProceduralSphericalBoxToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralSphericalBoxToolProperties_Statics::NewProp_Radius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProceduralSphericalBoxToolProperties_Statics::NewProp_Subdivisions,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UProceduralSphericalBoxToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UProceduralSphericalBoxToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UProceduralSphericalBoxToolProperties_Statics::ClassParams = {
		&UProceduralSphericalBoxToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UProceduralSphericalBoxToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralSphericalBoxToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UProceduralSphericalBoxToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UProceduralSphericalBoxToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UProceduralSphericalBoxToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UProceduralSphericalBoxToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UProceduralSphericalBoxToolProperties, 2753011759);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UProceduralSphericalBoxToolProperties>()
	{
		return UProceduralSphericalBoxToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UProceduralSphericalBoxToolProperties(Z_Construct_UClass_UProceduralSphericalBoxToolProperties, &UProceduralSphericalBoxToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UProceduralSphericalBoxToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UProceduralSphericalBoxToolProperties);
	void ULastActorInfo::StaticRegisterNativesULastActorInfo()
	{
	}
	UClass* Z_Construct_UClass_ULastActorInfo_NoRegister()
	{
		return ULastActorInfo::StaticClass();
	}
	struct Z_Construct_UClass_ULastActorInfo_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Actor_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Actor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StaticMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShapeSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ShapeSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaterialProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MaterialProperties;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULastActorInfo_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULastActorInfo_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AddPrimitiveTool.h" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULastActorInfo_Statics::NewProp_Actor_MetaData[] = {
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ULastActorInfo_Statics::NewProp_Actor = { "Actor", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULastActorInfo, Actor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ULastActorInfo_Statics::NewProp_Actor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULastActorInfo_Statics::NewProp_Actor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULastActorInfo_Statics::NewProp_StaticMesh_MetaData[] = {
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ULastActorInfo_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULastActorInfo, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ULastActorInfo_Statics::NewProp_StaticMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULastActorInfo_Statics::NewProp_StaticMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULastActorInfo_Statics::NewProp_ShapeSettings_MetaData[] = {
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ULastActorInfo_Statics::NewProp_ShapeSettings = { "ShapeSettings", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULastActorInfo, ShapeSettings), Z_Construct_UClass_UProceduralShapeToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ULastActorInfo_Statics::NewProp_ShapeSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULastActorInfo_Statics::NewProp_ShapeSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULastActorInfo_Statics::NewProp_MaterialProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ULastActorInfo_Statics::NewProp_MaterialProperties = { "MaterialProperties", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULastActorInfo, MaterialProperties), Z_Construct_UClass_UNewMeshMaterialProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ULastActorInfo_Statics::NewProp_MaterialProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULastActorInfo_Statics::NewProp_MaterialProperties_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ULastActorInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULastActorInfo_Statics::NewProp_Actor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULastActorInfo_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULastActorInfo_Statics::NewProp_ShapeSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULastActorInfo_Statics::NewProp_MaterialProperties,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULastActorInfo_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULastActorInfo>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULastActorInfo_Statics::ClassParams = {
		&ULastActorInfo::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ULastActorInfo_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ULastActorInfo_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_ULastActorInfo_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULastActorInfo_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULastActorInfo()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULastActorInfo_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULastActorInfo, 3794858754);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<ULastActorInfo>()
	{
		return ULastActorInfo::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULastActorInfo(Z_Construct_UClass_ULastActorInfo, &ULastActorInfo::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("ULastActorInfo"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULastActorInfo);
	void UAddPrimitiveTool::StaticRegisterNativesUAddPrimitiveTool()
	{
	}
	UClass* Z_Construct_UClass_UAddPrimitiveTool_NoRegister()
	{
		return UAddPrimitiveTool::StaticClass();
	}
	struct Z_Construct_UClass_UAddPrimitiveTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShapeSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ShapeSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaterialProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MaterialProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviewMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviewMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastGenerated_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LastGenerated;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AssetName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AssetName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAddPrimitiveTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USingleClickTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAddPrimitiveTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Base tool to create primitives\n */" },
		{ "IncludePath", "AddPrimitiveTool.h" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Base tool to create primitives" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAddPrimitiveTool_Statics::NewProp_ShapeSettings_MetaData[] = {
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UAddPrimitiveTool_Statics::NewProp_ShapeSettings = { "ShapeSettings", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAddPrimitiveTool, ShapeSettings), Z_Construct_UClass_UProceduralShapeToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UAddPrimitiveTool_Statics::NewProp_ShapeSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAddPrimitiveTool_Statics::NewProp_ShapeSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAddPrimitiveTool_Statics::NewProp_MaterialProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UAddPrimitiveTool_Statics::NewProp_MaterialProperties = { "MaterialProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAddPrimitiveTool, MaterialProperties), Z_Construct_UClass_UNewMeshMaterialProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UAddPrimitiveTool_Statics::NewProp_MaterialProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAddPrimitiveTool_Statics::NewProp_MaterialProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAddPrimitiveTool_Statics::NewProp_PreviewMesh_MetaData[] = {
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UAddPrimitiveTool_Statics::NewProp_PreviewMesh = { "PreviewMesh", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAddPrimitiveTool, PreviewMesh), Z_Construct_UClass_UPreviewMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UAddPrimitiveTool_Statics::NewProp_PreviewMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAddPrimitiveTool_Statics::NewProp_PreviewMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAddPrimitiveTool_Statics::NewProp_LastGenerated_MetaData[] = {
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UAddPrimitiveTool_Statics::NewProp_LastGenerated = { "LastGenerated", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAddPrimitiveTool, LastGenerated), Z_Construct_UClass_ULastActorInfo_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UAddPrimitiveTool_Statics::NewProp_LastGenerated_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAddPrimitiveTool_Statics::NewProp_LastGenerated_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAddPrimitiveTool_Statics::NewProp_AssetName_MetaData[] = {
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UAddPrimitiveTool_Statics::NewProp_AssetName = { "AssetName", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAddPrimitiveTool, AssetName), METADATA_PARAMS(Z_Construct_UClass_UAddPrimitiveTool_Statics::NewProp_AssetName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAddPrimitiveTool_Statics::NewProp_AssetName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAddPrimitiveTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAddPrimitiveTool_Statics::NewProp_ShapeSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAddPrimitiveTool_Statics::NewProp_MaterialProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAddPrimitiveTool_Statics::NewProp_PreviewMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAddPrimitiveTool_Statics::NewProp_LastGenerated,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAddPrimitiveTool_Statics::NewProp_AssetName,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAddPrimitiveTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAddPrimitiveTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAddPrimitiveTool_Statics::ClassParams = {
		&UAddPrimitiveTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UAddPrimitiveTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UAddPrimitiveTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UAddPrimitiveTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAddPrimitiveTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAddPrimitiveTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAddPrimitiveTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAddPrimitiveTool, 626641330);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UAddPrimitiveTool>()
	{
		return UAddPrimitiveTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAddPrimitiveTool(Z_Construct_UClass_UAddPrimitiveTool, &UAddPrimitiveTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UAddPrimitiveTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAddPrimitiveTool);
	void UAddBoxPrimitiveTool::StaticRegisterNativesUAddBoxPrimitiveTool()
	{
	}
	UClass* Z_Construct_UClass_UAddBoxPrimitiveTool_NoRegister()
	{
		return UAddBoxPrimitiveTool::StaticClass();
	}
	struct Z_Construct_UClass_UAddBoxPrimitiveTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAddBoxPrimitiveTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAddPrimitiveTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAddBoxPrimitiveTool_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AddPrimitiveTool.h" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAddBoxPrimitiveTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAddBoxPrimitiveTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAddBoxPrimitiveTool_Statics::ClassParams = {
		&UAddBoxPrimitiveTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UAddBoxPrimitiveTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAddBoxPrimitiveTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAddBoxPrimitiveTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAddBoxPrimitiveTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAddBoxPrimitiveTool, 318040954);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UAddBoxPrimitiveTool>()
	{
		return UAddBoxPrimitiveTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAddBoxPrimitiveTool(Z_Construct_UClass_UAddBoxPrimitiveTool, &UAddBoxPrimitiveTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UAddBoxPrimitiveTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAddBoxPrimitiveTool);
	void UAddCylinderPrimitiveTool::StaticRegisterNativesUAddCylinderPrimitiveTool()
	{
	}
	UClass* Z_Construct_UClass_UAddCylinderPrimitiveTool_NoRegister()
	{
		return UAddCylinderPrimitiveTool::StaticClass();
	}
	struct Z_Construct_UClass_UAddCylinderPrimitiveTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAddCylinderPrimitiveTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAddPrimitiveTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAddCylinderPrimitiveTool_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AddPrimitiveTool.h" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAddCylinderPrimitiveTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAddCylinderPrimitiveTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAddCylinderPrimitiveTool_Statics::ClassParams = {
		&UAddCylinderPrimitiveTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UAddCylinderPrimitiveTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAddCylinderPrimitiveTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAddCylinderPrimitiveTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAddCylinderPrimitiveTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAddCylinderPrimitiveTool, 899718404);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UAddCylinderPrimitiveTool>()
	{
		return UAddCylinderPrimitiveTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAddCylinderPrimitiveTool(Z_Construct_UClass_UAddCylinderPrimitiveTool, &UAddCylinderPrimitiveTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UAddCylinderPrimitiveTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAddCylinderPrimitiveTool);
	void UAddConePrimitiveTool::StaticRegisterNativesUAddConePrimitiveTool()
	{
	}
	UClass* Z_Construct_UClass_UAddConePrimitiveTool_NoRegister()
	{
		return UAddConePrimitiveTool::StaticClass();
	}
	struct Z_Construct_UClass_UAddConePrimitiveTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAddConePrimitiveTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAddPrimitiveTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAddConePrimitiveTool_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AddPrimitiveTool.h" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAddConePrimitiveTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAddConePrimitiveTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAddConePrimitiveTool_Statics::ClassParams = {
		&UAddConePrimitiveTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UAddConePrimitiveTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAddConePrimitiveTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAddConePrimitiveTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAddConePrimitiveTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAddConePrimitiveTool, 98751035);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UAddConePrimitiveTool>()
	{
		return UAddConePrimitiveTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAddConePrimitiveTool(Z_Construct_UClass_UAddConePrimitiveTool, &UAddConePrimitiveTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UAddConePrimitiveTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAddConePrimitiveTool);
	void UAddRectanglePrimitiveTool::StaticRegisterNativesUAddRectanglePrimitiveTool()
	{
	}
	UClass* Z_Construct_UClass_UAddRectanglePrimitiveTool_NoRegister()
	{
		return UAddRectanglePrimitiveTool::StaticClass();
	}
	struct Z_Construct_UClass_UAddRectanglePrimitiveTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAddRectanglePrimitiveTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAddPrimitiveTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAddRectanglePrimitiveTool_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AddPrimitiveTool.h" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAddRectanglePrimitiveTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAddRectanglePrimitiveTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAddRectanglePrimitiveTool_Statics::ClassParams = {
		&UAddRectanglePrimitiveTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UAddRectanglePrimitiveTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAddRectanglePrimitiveTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAddRectanglePrimitiveTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAddRectanglePrimitiveTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAddRectanglePrimitiveTool, 3196173610);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UAddRectanglePrimitiveTool>()
	{
		return UAddRectanglePrimitiveTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAddRectanglePrimitiveTool(Z_Construct_UClass_UAddRectanglePrimitiveTool, &UAddRectanglePrimitiveTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UAddRectanglePrimitiveTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAddRectanglePrimitiveTool);
	void UAddRoundedRectanglePrimitiveTool::StaticRegisterNativesUAddRoundedRectanglePrimitiveTool()
	{
	}
	UClass* Z_Construct_UClass_UAddRoundedRectanglePrimitiveTool_NoRegister()
	{
		return UAddRoundedRectanglePrimitiveTool::StaticClass();
	}
	struct Z_Construct_UClass_UAddRoundedRectanglePrimitiveTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAddRoundedRectanglePrimitiveTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAddPrimitiveTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAddRoundedRectanglePrimitiveTool_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AddPrimitiveTool.h" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAddRoundedRectanglePrimitiveTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAddRoundedRectanglePrimitiveTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAddRoundedRectanglePrimitiveTool_Statics::ClassParams = {
		&UAddRoundedRectanglePrimitiveTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UAddRoundedRectanglePrimitiveTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAddRoundedRectanglePrimitiveTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAddRoundedRectanglePrimitiveTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAddRoundedRectanglePrimitiveTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAddRoundedRectanglePrimitiveTool, 2309680568);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UAddRoundedRectanglePrimitiveTool>()
	{
		return UAddRoundedRectanglePrimitiveTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAddRoundedRectanglePrimitiveTool(Z_Construct_UClass_UAddRoundedRectanglePrimitiveTool, &UAddRoundedRectanglePrimitiveTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UAddRoundedRectanglePrimitiveTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAddRoundedRectanglePrimitiveTool);
	void UAddDiscPrimitiveTool::StaticRegisterNativesUAddDiscPrimitiveTool()
	{
	}
	UClass* Z_Construct_UClass_UAddDiscPrimitiveTool_NoRegister()
	{
		return UAddDiscPrimitiveTool::StaticClass();
	}
	struct Z_Construct_UClass_UAddDiscPrimitiveTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAddDiscPrimitiveTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAddPrimitiveTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAddDiscPrimitiveTool_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AddPrimitiveTool.h" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAddDiscPrimitiveTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAddDiscPrimitiveTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAddDiscPrimitiveTool_Statics::ClassParams = {
		&UAddDiscPrimitiveTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UAddDiscPrimitiveTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAddDiscPrimitiveTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAddDiscPrimitiveTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAddDiscPrimitiveTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAddDiscPrimitiveTool, 1953635251);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UAddDiscPrimitiveTool>()
	{
		return UAddDiscPrimitiveTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAddDiscPrimitiveTool(Z_Construct_UClass_UAddDiscPrimitiveTool, &UAddDiscPrimitiveTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UAddDiscPrimitiveTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAddDiscPrimitiveTool);
	void UAddPuncturedDiscPrimitiveTool::StaticRegisterNativesUAddPuncturedDiscPrimitiveTool()
	{
	}
	UClass* Z_Construct_UClass_UAddPuncturedDiscPrimitiveTool_NoRegister()
	{
		return UAddPuncturedDiscPrimitiveTool::StaticClass();
	}
	struct Z_Construct_UClass_UAddPuncturedDiscPrimitiveTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAddPuncturedDiscPrimitiveTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAddPrimitiveTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAddPuncturedDiscPrimitiveTool_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AddPrimitiveTool.h" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAddPuncturedDiscPrimitiveTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAddPuncturedDiscPrimitiveTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAddPuncturedDiscPrimitiveTool_Statics::ClassParams = {
		&UAddPuncturedDiscPrimitiveTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UAddPuncturedDiscPrimitiveTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAddPuncturedDiscPrimitiveTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAddPuncturedDiscPrimitiveTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAddPuncturedDiscPrimitiveTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAddPuncturedDiscPrimitiveTool, 1994766056);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UAddPuncturedDiscPrimitiveTool>()
	{
		return UAddPuncturedDiscPrimitiveTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAddPuncturedDiscPrimitiveTool(Z_Construct_UClass_UAddPuncturedDiscPrimitiveTool, &UAddPuncturedDiscPrimitiveTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UAddPuncturedDiscPrimitiveTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAddPuncturedDiscPrimitiveTool);
	void UAddTorusPrimitiveTool::StaticRegisterNativesUAddTorusPrimitiveTool()
	{
	}
	UClass* Z_Construct_UClass_UAddTorusPrimitiveTool_NoRegister()
	{
		return UAddTorusPrimitiveTool::StaticClass();
	}
	struct Z_Construct_UClass_UAddTorusPrimitiveTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAddTorusPrimitiveTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAddPrimitiveTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAddTorusPrimitiveTool_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AddPrimitiveTool.h" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAddTorusPrimitiveTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAddTorusPrimitiveTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAddTorusPrimitiveTool_Statics::ClassParams = {
		&UAddTorusPrimitiveTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UAddTorusPrimitiveTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAddTorusPrimitiveTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAddTorusPrimitiveTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAddTorusPrimitiveTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAddTorusPrimitiveTool, 3727430748);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UAddTorusPrimitiveTool>()
	{
		return UAddTorusPrimitiveTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAddTorusPrimitiveTool(Z_Construct_UClass_UAddTorusPrimitiveTool, &UAddTorusPrimitiveTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UAddTorusPrimitiveTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAddTorusPrimitiveTool);
	void UAddArrowPrimitiveTool::StaticRegisterNativesUAddArrowPrimitiveTool()
	{
	}
	UClass* Z_Construct_UClass_UAddArrowPrimitiveTool_NoRegister()
	{
		return UAddArrowPrimitiveTool::StaticClass();
	}
	struct Z_Construct_UClass_UAddArrowPrimitiveTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAddArrowPrimitiveTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAddPrimitiveTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAddArrowPrimitiveTool_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AddPrimitiveTool.h" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAddArrowPrimitiveTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAddArrowPrimitiveTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAddArrowPrimitiveTool_Statics::ClassParams = {
		&UAddArrowPrimitiveTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UAddArrowPrimitiveTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAddArrowPrimitiveTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAddArrowPrimitiveTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAddArrowPrimitiveTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAddArrowPrimitiveTool, 1378941568);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UAddArrowPrimitiveTool>()
	{
		return UAddArrowPrimitiveTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAddArrowPrimitiveTool(Z_Construct_UClass_UAddArrowPrimitiveTool, &UAddArrowPrimitiveTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UAddArrowPrimitiveTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAddArrowPrimitiveTool);
	void UAddSpherePrimitiveTool::StaticRegisterNativesUAddSpherePrimitiveTool()
	{
	}
	UClass* Z_Construct_UClass_UAddSpherePrimitiveTool_NoRegister()
	{
		return UAddSpherePrimitiveTool::StaticClass();
	}
	struct Z_Construct_UClass_UAddSpherePrimitiveTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAddSpherePrimitiveTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAddPrimitiveTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAddSpherePrimitiveTool_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AddPrimitiveTool.h" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAddSpherePrimitiveTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAddSpherePrimitiveTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAddSpherePrimitiveTool_Statics::ClassParams = {
		&UAddSpherePrimitiveTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UAddSpherePrimitiveTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAddSpherePrimitiveTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAddSpherePrimitiveTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAddSpherePrimitiveTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAddSpherePrimitiveTool, 1829624186);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UAddSpherePrimitiveTool>()
	{
		return UAddSpherePrimitiveTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAddSpherePrimitiveTool(Z_Construct_UClass_UAddSpherePrimitiveTool, &UAddSpherePrimitiveTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UAddSpherePrimitiveTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAddSpherePrimitiveTool);
	void UAddSphericalBoxPrimitiveTool::StaticRegisterNativesUAddSphericalBoxPrimitiveTool()
	{
	}
	UClass* Z_Construct_UClass_UAddSphericalBoxPrimitiveTool_NoRegister()
	{
		return UAddSphericalBoxPrimitiveTool::StaticClass();
	}
	struct Z_Construct_UClass_UAddSphericalBoxPrimitiveTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAddSphericalBoxPrimitiveTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAddPrimitiveTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAddSphericalBoxPrimitiveTool_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AddPrimitiveTool.h" },
		{ "ModuleRelativePath", "Public/AddPrimitiveTool.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAddSphericalBoxPrimitiveTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAddSphericalBoxPrimitiveTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAddSphericalBoxPrimitiveTool_Statics::ClassParams = {
		&UAddSphericalBoxPrimitiveTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UAddSphericalBoxPrimitiveTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAddSphericalBoxPrimitiveTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAddSphericalBoxPrimitiveTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAddSphericalBoxPrimitiveTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAddSphericalBoxPrimitiveTool, 3343661724);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UAddSphericalBoxPrimitiveTool>()
	{
		return UAddSphericalBoxPrimitiveTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAddSphericalBoxPrimitiveTool(Z_Construct_UClass_UAddSphericalBoxPrimitiveTool, &UAddSphericalBoxPrimitiveTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UAddSphericalBoxPrimitiveTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAddSphericalBoxPrimitiveTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
