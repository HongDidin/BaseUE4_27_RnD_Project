// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/MirrorTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMirrorTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EMirrorToolAction();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EMirrorCtrlClickBehavior();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EMirrorOperationMode();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EMirrorSaveMode();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMirrorToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMirrorToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMirrorToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMirrorToolProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMirrorOperatorFactory_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMirrorOperatorFactory();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMirrorTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMirrorToolActionPropertySet_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMirrorToolActionPropertySet();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMirrorTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UMultiSelectionTool();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UDynamicMeshReplacementChangeTarget_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UConstructionPlaneMechanic_NoRegister();
// End Cross Module References
	static UEnum* EMirrorToolAction_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EMirrorToolAction, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EMirrorToolAction"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EMirrorToolAction>()
	{
		return EMirrorToolAction_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMirrorToolAction(EMirrorToolAction_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EMirrorToolAction"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EMirrorToolAction_Hash() { return 230661277U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EMirrorToolAction()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMirrorToolAction"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EMirrorToolAction_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMirrorToolAction::NoAction", (int64)EMirrorToolAction::NoAction },
				{ "EMirrorToolAction::ShiftToCenter", (int64)EMirrorToolAction::ShiftToCenter },
				{ "EMirrorToolAction::Left", (int64)EMirrorToolAction::Left },
				{ "EMirrorToolAction::Right", (int64)EMirrorToolAction::Right },
				{ "EMirrorToolAction::Up", (int64)EMirrorToolAction::Up },
				{ "EMirrorToolAction::Down", (int64)EMirrorToolAction::Down },
				{ "EMirrorToolAction::Forward", (int64)EMirrorToolAction::Forward },
				{ "EMirrorToolAction::Backward", (int64)EMirrorToolAction::Backward },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Backward.Name", "EMirrorToolAction::Backward" },
				{ "Down.Name", "EMirrorToolAction::Down" },
				{ "Forward.Name", "EMirrorToolAction::Forward" },
				{ "Left.Name", "EMirrorToolAction::Left" },
				{ "ModuleRelativePath", "Public/MirrorTool.h" },
				{ "NoAction.Name", "EMirrorToolAction::NoAction" },
				{ "Right.Name", "EMirrorToolAction::Right" },
				{ "ShiftToCenter.Name", "EMirrorToolAction::ShiftToCenter" },
				{ "Up.Name", "EMirrorToolAction::Up" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EMirrorToolAction",
				"EMirrorToolAction",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EMirrorCtrlClickBehavior_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EMirrorCtrlClickBehavior, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EMirrorCtrlClickBehavior"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EMirrorCtrlClickBehavior>()
	{
		return EMirrorCtrlClickBehavior_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMirrorCtrlClickBehavior(EMirrorCtrlClickBehavior_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EMirrorCtrlClickBehavior"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EMirrorCtrlClickBehavior_Hash() { return 729478623U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EMirrorCtrlClickBehavior()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMirrorCtrlClickBehavior"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EMirrorCtrlClickBehavior_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMirrorCtrlClickBehavior::Reposition", (int64)EMirrorCtrlClickBehavior::Reposition },
				{ "EMirrorCtrlClickBehavior::RepositionAndReorient", (int64)EMirrorCtrlClickBehavior::RepositionAndReorient },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/MirrorTool.h" },
				{ "Reposition.Comment", "/** Move the mirror plane to clicked location without adjusting its normal. */" },
				{ "Reposition.Name", "EMirrorCtrlClickBehavior::Reposition" },
				{ "Reposition.ToolTip", "Move the mirror plane to clicked location without adjusting its normal." },
				{ "RepositionAndReorient.Comment", "/** Move the mirror plane and adjust its normal according to click location. */" },
				{ "RepositionAndReorient.Name", "EMirrorCtrlClickBehavior::RepositionAndReorient" },
				{ "RepositionAndReorient.ToolTip", "Move the mirror plane and adjust its normal according to click location." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EMirrorCtrlClickBehavior",
				"EMirrorCtrlClickBehavior",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EMirrorOperationMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EMirrorOperationMode, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EMirrorOperationMode"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EMirrorOperationMode>()
	{
		return EMirrorOperationMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMirrorOperationMode(EMirrorOperationMode_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EMirrorOperationMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EMirrorOperationMode_Hash() { return 864053763U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EMirrorOperationMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMirrorOperationMode"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EMirrorOperationMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMirrorOperationMode::MirrorAndAppend", (int64)EMirrorOperationMode::MirrorAndAppend },
				{ "EMirrorOperationMode::MirrorExisting", (int64)EMirrorOperationMode::MirrorExisting },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "MirrorAndAppend.Comment", "/**  Append a mirrored version of the mesh to itself. */" },
				{ "MirrorAndAppend.Name", "EMirrorOperationMode::MirrorAndAppend" },
				{ "MirrorAndAppend.ToolTip", "Append a mirrored version of the mesh to itself." },
				{ "MirrorExisting.Comment", "/** Mirror the existing mesh. */" },
				{ "MirrorExisting.Name", "EMirrorOperationMode::MirrorExisting" },
				{ "MirrorExisting.ToolTip", "Mirror the existing mesh." },
				{ "ModuleRelativePath", "Public/MirrorTool.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EMirrorOperationMode",
				"EMirrorOperationMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EMirrorSaveMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EMirrorSaveMode, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EMirrorSaveMode"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EMirrorSaveMode>()
	{
		return EMirrorSaveMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMirrorSaveMode(EMirrorSaveMode_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EMirrorSaveMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EMirrorSaveMode_Hash() { return 3739732192U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EMirrorSaveMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMirrorSaveMode"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EMirrorSaveMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMirrorSaveMode::UpdateAssets", (int64)EMirrorSaveMode::UpdateAssets },
				{ "EMirrorSaveMode::CreateNewAssets", (int64)EMirrorSaveMode::CreateNewAssets },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "CreateNewAssets.Comment", "/** Save the results as new assets. */" },
				{ "CreateNewAssets.Name", "EMirrorSaveMode::CreateNewAssets" },
				{ "CreateNewAssets.ToolTip", "Save the results as new assets." },
				{ "ModuleRelativePath", "Public/MirrorTool.h" },
				{ "UpdateAssets.Comment", "/**  Save the results in place of the original assets. */" },
				{ "UpdateAssets.Name", "EMirrorSaveMode::UpdateAssets" },
				{ "UpdateAssets.ToolTip", "Save the results in place of the original assets." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EMirrorSaveMode",
				"EMirrorSaveMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UMirrorToolBuilder::StaticRegisterNativesUMirrorToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UMirrorToolBuilder_NoRegister()
	{
		return UMirrorToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UMirrorToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMirrorToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMirrorToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MirrorTool.h" },
		{ "ModuleRelativePath", "Public/MirrorTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMirrorToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMirrorToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMirrorToolBuilder_Statics::ClassParams = {
		&UMirrorToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMirrorToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMirrorToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMirrorToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMirrorToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMirrorToolBuilder, 2229917999);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UMirrorToolBuilder>()
	{
		return UMirrorToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMirrorToolBuilder(Z_Construct_UClass_UMirrorToolBuilder, &UMirrorToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UMirrorToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMirrorToolBuilder);
	void UMirrorToolProperties::StaticRegisterNativesUMirrorToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UMirrorToolProperties_NoRegister()
	{
		return UMirrorToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UMirrorToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_OperationMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OperationMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_OperationMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCropAlongMirrorPlaneFirst_MetaData[];
#endif
		static void NewProp_bCropAlongMirrorPlaneFirst_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCropAlongMirrorPlaneFirst;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bWeldVerticesOnMirrorPlane_MetaData[];
#endif
		static void NewProp_bWeldVerticesOnMirrorPlane_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bWeldVerticesOnMirrorPlane;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAllowBowtieVertexCreation_MetaData[];
#endif
		static void NewProp_bAllowBowtieVertexCreation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAllowBowtieVertexCreation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSnapToWorldGrid_MetaData[];
#endif
		static void NewProp_bSnapToWorldGrid_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSnapToWorldGrid;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_CtrlClickBehavior_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CtrlClickBehavior_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_CtrlClickBehavior;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bButtonsOnlyChangeOrientation_MetaData[];
#endif
		static void NewProp_bButtonsOnlyChangeOrientation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bButtonsOnlyChangeOrientation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowPreview_MetaData[];
#endif
		static void NewProp_bShowPreview_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowPreview;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_SaveMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SaveMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_SaveMode;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMirrorToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMirrorToolProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "MirrorTool.h" },
		{ "ModuleRelativePath", "Public/MirrorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_OperationMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_OperationMode_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Mode of operation. */" },
		{ "ModuleRelativePath", "Public/MirrorTool.h" },
		{ "ToolTip", "Mode of operation." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_OperationMode = { "OperationMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMirrorToolProperties, OperationMode), Z_Construct_UEnum_MeshModelingTools_EMirrorOperationMode, METADATA_PARAMS(Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_OperationMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_OperationMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bCropAlongMirrorPlaneFirst_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Cut off everything on the back side of the mirror plane before mirroring. */" },
		{ "ModuleRelativePath", "Public/MirrorTool.h" },
		{ "ToolTip", "Cut off everything on the back side of the mirror plane before mirroring." },
	};
#endif
	void Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bCropAlongMirrorPlaneFirst_SetBit(void* Obj)
	{
		((UMirrorToolProperties*)Obj)->bCropAlongMirrorPlaneFirst = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bCropAlongMirrorPlaneFirst = { "bCropAlongMirrorPlaneFirst", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMirrorToolProperties), &Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bCropAlongMirrorPlaneFirst_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bCropAlongMirrorPlaneFirst_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bCropAlongMirrorPlaneFirst_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bWeldVerticesOnMirrorPlane_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Weld vertices that lie on the mirror plane. Vertices will not be welded if doing so would give an edge more than two faces, or if they are part of a face in the plane. */" },
		{ "EditCondition", "OperationMode == EMirrorOperationMode::MirrorAndAppend" },
		{ "ModuleRelativePath", "Public/MirrorTool.h" },
		{ "ToolTip", "Weld vertices that lie on the mirror plane. Vertices will not be welded if doing so would give an edge more than two faces, or if they are part of a face in the plane." },
	};
#endif
	void Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bWeldVerticesOnMirrorPlane_SetBit(void* Obj)
	{
		((UMirrorToolProperties*)Obj)->bWeldVerticesOnMirrorPlane = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bWeldVerticesOnMirrorPlane = { "bWeldVerticesOnMirrorPlane", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMirrorToolProperties), &Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bWeldVerticesOnMirrorPlane_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bWeldVerticesOnMirrorPlane_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bWeldVerticesOnMirrorPlane_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bAllowBowtieVertexCreation_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** When welding, whether to allow bowtie vertices to be created, or to duplicate the vertex. */" },
		{ "EditCondition", "bWeldVerticesOnMirrorPlane && OperationMode == EMirrorOperationMode::MirrorAndAppend" },
		{ "ModuleRelativePath", "Public/MirrorTool.h" },
		{ "ToolTip", "When welding, whether to allow bowtie vertices to be created, or to duplicate the vertex." },
	};
#endif
	void Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bAllowBowtieVertexCreation_SetBit(void* Obj)
	{
		((UMirrorToolProperties*)Obj)->bAllowBowtieVertexCreation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bAllowBowtieVertexCreation = { "bAllowBowtieVertexCreation", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMirrorToolProperties), &Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bAllowBowtieVertexCreation_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bAllowBowtieVertexCreation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bAllowBowtieVertexCreation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bSnapToWorldGrid_MetaData[] = {
		{ "Category", "RepositionOptions" },
		{ "Comment", "/** Snap the mirror plane to the world grid. */" },
		{ "ModuleRelativePath", "Public/MirrorTool.h" },
		{ "ToolTip", "Snap the mirror plane to the world grid." },
	};
#endif
	void Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bSnapToWorldGrid_SetBit(void* Obj)
	{
		((UMirrorToolProperties*)Obj)->bSnapToWorldGrid = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bSnapToWorldGrid = { "bSnapToWorldGrid", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMirrorToolProperties), &Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bSnapToWorldGrid_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bSnapToWorldGrid_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bSnapToWorldGrid_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_CtrlClickBehavior_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_CtrlClickBehavior_MetaData[] = {
		{ "Category", "RepositionOptions" },
		{ "Comment", "/** What Ctrl + clicking does to the mirror plane. */" },
		{ "ModuleRelativePath", "Public/MirrorTool.h" },
		{ "ToolTip", "What Ctrl + clicking does to the mirror plane." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_CtrlClickBehavior = { "CtrlClickBehavior", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMirrorToolProperties, CtrlClickBehavior), Z_Construct_UEnum_MeshModelingTools_EMirrorCtrlClickBehavior, METADATA_PARAMS(Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_CtrlClickBehavior_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_CtrlClickBehavior_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bButtonsOnlyChangeOrientation_MetaData[] = {
		{ "Category", "RepositionOptions" },
		{ "Comment", "/** If true the \"Preset Mirror Directions\" buttons only change the plane orientation, not location. */" },
		{ "ModuleRelativePath", "Public/MirrorTool.h" },
		{ "ToolTip", "If true the \"Preset Mirror Directions\" buttons only change the plane orientation, not location." },
	};
#endif
	void Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bButtonsOnlyChangeOrientation_SetBit(void* Obj)
	{
		((UMirrorToolProperties*)Obj)->bButtonsOnlyChangeOrientation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bButtonsOnlyChangeOrientation = { "bButtonsOnlyChangeOrientation", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMirrorToolProperties), &Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bButtonsOnlyChangeOrientation_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bButtonsOnlyChangeOrientation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bButtonsOnlyChangeOrientation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bShowPreview_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Whether to show the preview. */" },
		{ "ModuleRelativePath", "Public/MirrorTool.h" },
		{ "ToolTip", "Whether to show the preview." },
	};
#endif
	void Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bShowPreview_SetBit(void* Obj)
	{
		((UMirrorToolProperties*)Obj)->bShowPreview = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bShowPreview = { "bShowPreview", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMirrorToolProperties), &Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bShowPreview_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bShowPreview_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bShowPreview_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_SaveMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_SaveMode_MetaData[] = {
		{ "Category", "ToolOutputOptions" },
		{ "Comment", "/** How to save the result. */" },
		{ "ModuleRelativePath", "Public/MirrorTool.h" },
		{ "ToolTip", "How to save the result." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_SaveMode = { "SaveMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMirrorToolProperties, SaveMode), Z_Construct_UEnum_MeshModelingTools_EMirrorSaveMode, METADATA_PARAMS(Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_SaveMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_SaveMode_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMirrorToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_OperationMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_OperationMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bCropAlongMirrorPlaneFirst,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bWeldVerticesOnMirrorPlane,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bAllowBowtieVertexCreation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bSnapToWorldGrid,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_CtrlClickBehavior_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_CtrlClickBehavior,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bButtonsOnlyChangeOrientation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_bShowPreview,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_SaveMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMirrorToolProperties_Statics::NewProp_SaveMode,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMirrorToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMirrorToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMirrorToolProperties_Statics::ClassParams = {
		&UMirrorToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMirrorToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMirrorToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMirrorToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMirrorToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMirrorToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMirrorToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMirrorToolProperties, 1301817367);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UMirrorToolProperties>()
	{
		return UMirrorToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMirrorToolProperties(Z_Construct_UClass_UMirrorToolProperties, &UMirrorToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UMirrorToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMirrorToolProperties);
	void UMirrorOperatorFactory::StaticRegisterNativesUMirrorOperatorFactory()
	{
	}
	UClass* Z_Construct_UClass_UMirrorOperatorFactory_NoRegister()
	{
		return UMirrorOperatorFactory::StaticClass();
	}
	struct Z_Construct_UClass_UMirrorOperatorFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MirrorTool_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MirrorTool;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMirrorOperatorFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMirrorOperatorFactory_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MirrorTool.h" },
		{ "ModuleRelativePath", "Public/MirrorTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMirrorOperatorFactory_Statics::NewProp_MirrorTool_MetaData[] = {
		{ "ModuleRelativePath", "Public/MirrorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMirrorOperatorFactory_Statics::NewProp_MirrorTool = { "MirrorTool", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMirrorOperatorFactory, MirrorTool), Z_Construct_UClass_UMirrorTool_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMirrorOperatorFactory_Statics::NewProp_MirrorTool_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMirrorOperatorFactory_Statics::NewProp_MirrorTool_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMirrorOperatorFactory_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMirrorOperatorFactory_Statics::NewProp_MirrorTool,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMirrorOperatorFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMirrorOperatorFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMirrorOperatorFactory_Statics::ClassParams = {
		&UMirrorOperatorFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMirrorOperatorFactory_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMirrorOperatorFactory_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMirrorOperatorFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMirrorOperatorFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMirrorOperatorFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMirrorOperatorFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMirrorOperatorFactory, 2234683659);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UMirrorOperatorFactory>()
	{
		return UMirrorOperatorFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMirrorOperatorFactory(Z_Construct_UClass_UMirrorOperatorFactory, &UMirrorOperatorFactory::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UMirrorOperatorFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMirrorOperatorFactory);
	DEFINE_FUNCTION(UMirrorToolActionPropertySet::execBackward)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Backward();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMirrorToolActionPropertySet::execForward)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Forward();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMirrorToolActionPropertySet::execDown)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Down();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMirrorToolActionPropertySet::execUp)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Up();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMirrorToolActionPropertySet::execRight)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Right();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMirrorToolActionPropertySet::execLeft)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Left();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMirrorToolActionPropertySet::execShiftToCenter)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ShiftToCenter();
		P_NATIVE_END;
	}
	void UMirrorToolActionPropertySet::StaticRegisterNativesUMirrorToolActionPropertySet()
	{
		UClass* Class = UMirrorToolActionPropertySet::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Backward", &UMirrorToolActionPropertySet::execBackward },
			{ "Down", &UMirrorToolActionPropertySet::execDown },
			{ "Forward", &UMirrorToolActionPropertySet::execForward },
			{ "Left", &UMirrorToolActionPropertySet::execLeft },
			{ "Right", &UMirrorToolActionPropertySet::execRight },
			{ "ShiftToCenter", &UMirrorToolActionPropertySet::execShiftToCenter },
			{ "Up", &UMirrorToolActionPropertySet::execUp },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMirrorToolActionPropertySet_Backward_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMirrorToolActionPropertySet_Backward_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "PresetMirrorDirections" },
		{ "Comment", "/** Move the mirror plane and adjust its normal to mirror entire selection backward. */" },
		{ "DisplayPriority", "6" },
		{ "ModuleRelativePath", "Public/MirrorTool.h" },
		{ "ToolTip", "Move the mirror plane and adjust its normal to mirror entire selection backward." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMirrorToolActionPropertySet_Backward_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMirrorToolActionPropertySet, nullptr, "Backward", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMirrorToolActionPropertySet_Backward_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMirrorToolActionPropertySet_Backward_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMirrorToolActionPropertySet_Backward()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMirrorToolActionPropertySet_Backward_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMirrorToolActionPropertySet_Down_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMirrorToolActionPropertySet_Down_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "PresetMirrorDirections" },
		{ "Comment", "/** Move the mirror plane and adjust its normal to mirror entire selection downward. */" },
		{ "DisplayPriority", "4" },
		{ "ModuleRelativePath", "Public/MirrorTool.h" },
		{ "ToolTip", "Move the mirror plane and adjust its normal to mirror entire selection downward." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMirrorToolActionPropertySet_Down_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMirrorToolActionPropertySet, nullptr, "Down", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMirrorToolActionPropertySet_Down_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMirrorToolActionPropertySet_Down_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMirrorToolActionPropertySet_Down()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMirrorToolActionPropertySet_Down_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMirrorToolActionPropertySet_Forward_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMirrorToolActionPropertySet_Forward_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "PresetMirrorDirections" },
		{ "Comment", "/** Move the mirror plane and adjust its normal to mirror entire selection forward. */" },
		{ "DisplayPriority", "5" },
		{ "ModuleRelativePath", "Public/MirrorTool.h" },
		{ "ToolTip", "Move the mirror plane and adjust its normal to mirror entire selection forward." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMirrorToolActionPropertySet_Forward_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMirrorToolActionPropertySet, nullptr, "Forward", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMirrorToolActionPropertySet_Forward_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMirrorToolActionPropertySet_Forward_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMirrorToolActionPropertySet_Forward()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMirrorToolActionPropertySet_Forward_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMirrorToolActionPropertySet_Left_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMirrorToolActionPropertySet_Left_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "PresetMirrorDirections" },
		{ "Comment", "/** Move the mirror plane and adjust its normal to mirror entire selection leftward. */" },
		{ "DisplayPriority", "1" },
		{ "ModuleRelativePath", "Public/MirrorTool.h" },
		{ "ToolTip", "Move the mirror plane and adjust its normal to mirror entire selection leftward." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMirrorToolActionPropertySet_Left_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMirrorToolActionPropertySet, nullptr, "Left", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMirrorToolActionPropertySet_Left_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMirrorToolActionPropertySet_Left_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMirrorToolActionPropertySet_Left()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMirrorToolActionPropertySet_Left_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMirrorToolActionPropertySet_Right_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMirrorToolActionPropertySet_Right_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "PresetMirrorDirections" },
		{ "Comment", "/** Move the mirror plane and adjust its normal to mirror entire selection rightward. */" },
		{ "DisplayPriority", "2" },
		{ "ModuleRelativePath", "Public/MirrorTool.h" },
		{ "ToolTip", "Move the mirror plane and adjust its normal to mirror entire selection rightward." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMirrorToolActionPropertySet_Right_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMirrorToolActionPropertySet, nullptr, "Right", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMirrorToolActionPropertySet_Right_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMirrorToolActionPropertySet_Right_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMirrorToolActionPropertySet_Right()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMirrorToolActionPropertySet_Right_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMirrorToolActionPropertySet_ShiftToCenter_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMirrorToolActionPropertySet_ShiftToCenter_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "RepositionPlane" },
		{ "Comment", "/** Move the mirror plane to center of bounding box without changing its normal. */" },
		{ "ModuleRelativePath", "Public/MirrorTool.h" },
		{ "ToolTip", "Move the mirror plane to center of bounding box without changing its normal." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMirrorToolActionPropertySet_ShiftToCenter_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMirrorToolActionPropertySet, nullptr, "ShiftToCenter", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMirrorToolActionPropertySet_ShiftToCenter_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMirrorToolActionPropertySet_ShiftToCenter_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMirrorToolActionPropertySet_ShiftToCenter()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMirrorToolActionPropertySet_ShiftToCenter_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMirrorToolActionPropertySet_Up_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMirrorToolActionPropertySet_Up_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "PresetMirrorDirections" },
		{ "Comment", "/** Move the mirror plane and adjust its normal to mirror entire selection upward. */" },
		{ "DisplayPriority", "3" },
		{ "ModuleRelativePath", "Public/MirrorTool.h" },
		{ "ToolTip", "Move the mirror plane and adjust its normal to mirror entire selection upward." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMirrorToolActionPropertySet_Up_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMirrorToolActionPropertySet, nullptr, "Up", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMirrorToolActionPropertySet_Up_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMirrorToolActionPropertySet_Up_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMirrorToolActionPropertySet_Up()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMirrorToolActionPropertySet_Up_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMirrorToolActionPropertySet_NoRegister()
	{
		return UMirrorToolActionPropertySet::StaticClass();
	}
	struct Z_Construct_UClass_UMirrorToolActionPropertySet_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMirrorToolActionPropertySet_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMirrorToolActionPropertySet_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMirrorToolActionPropertySet_Backward, "Backward" }, // 1121077107
		{ &Z_Construct_UFunction_UMirrorToolActionPropertySet_Down, "Down" }, // 3916647046
		{ &Z_Construct_UFunction_UMirrorToolActionPropertySet_Forward, "Forward" }, // 353584690
		{ &Z_Construct_UFunction_UMirrorToolActionPropertySet_Left, "Left" }, // 921163114
		{ &Z_Construct_UFunction_UMirrorToolActionPropertySet_Right, "Right" }, // 3967631621
		{ &Z_Construct_UFunction_UMirrorToolActionPropertySet_ShiftToCenter, "ShiftToCenter" }, // 4006095705
		{ &Z_Construct_UFunction_UMirrorToolActionPropertySet_Up, "Up" }, // 3239518317
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMirrorToolActionPropertySet_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MirrorTool.h" },
		{ "ModuleRelativePath", "Public/MirrorTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMirrorToolActionPropertySet_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMirrorToolActionPropertySet>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMirrorToolActionPropertySet_Statics::ClassParams = {
		&UMirrorToolActionPropertySet::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMirrorToolActionPropertySet_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMirrorToolActionPropertySet_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMirrorToolActionPropertySet()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMirrorToolActionPropertySet_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMirrorToolActionPropertySet, 738877350);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UMirrorToolActionPropertySet>()
	{
		return UMirrorToolActionPropertySet::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMirrorToolActionPropertySet(Z_Construct_UClass_UMirrorToolActionPropertySet, &UMirrorToolActionPropertySet::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UMirrorToolActionPropertySet"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMirrorToolActionPropertySet);
	void UMirrorTool::StaticRegisterNativesUMirrorTool()
	{
	}
	UClass* Z_Construct_UClass_UMirrorTool_NoRegister()
	{
		return UMirrorTool::StaticClass();
	}
	struct Z_Construct_UClass_UMirrorTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Settings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Settings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ToolActions_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ToolActions;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MeshesToMirror_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshesToMirror_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_MeshesToMirror;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Previews_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Previews_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Previews;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlaneMechanic_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PlaneMechanic;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMirrorTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMultiSelectionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMirrorTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Tool for mirroring one or more meshes across a plane. */" },
		{ "IncludePath", "MirrorTool.h" },
		{ "ModuleRelativePath", "Public/MirrorTool.h" },
		{ "ToolTip", "Tool for mirroring one or more meshes across a plane." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMirrorTool_Statics::NewProp_Settings_MetaData[] = {
		{ "ModuleRelativePath", "Public/MirrorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMirrorTool_Statics::NewProp_Settings = { "Settings", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMirrorTool, Settings), Z_Construct_UClass_UMirrorToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMirrorTool_Statics::NewProp_Settings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMirrorTool_Statics::NewProp_Settings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMirrorTool_Statics::NewProp_ToolActions_MetaData[] = {
		{ "ModuleRelativePath", "Public/MirrorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMirrorTool_Statics::NewProp_ToolActions = { "ToolActions", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMirrorTool, ToolActions), Z_Construct_UClass_UMirrorToolActionPropertySet_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMirrorTool_Statics::NewProp_ToolActions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMirrorTool_Statics::NewProp_ToolActions_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMirrorTool_Statics::NewProp_MeshesToMirror_Inner = { "MeshesToMirror", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UDynamicMeshReplacementChangeTarget_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMirrorTool_Statics::NewProp_MeshesToMirror_MetaData[] = {
		{ "ModuleRelativePath", "Public/MirrorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UMirrorTool_Statics::NewProp_MeshesToMirror = { "MeshesToMirror", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMirrorTool, MeshesToMirror), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UMirrorTool_Statics::NewProp_MeshesToMirror_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMirrorTool_Statics::NewProp_MeshesToMirror_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMirrorTool_Statics::NewProp_Previews_Inner = { "Previews", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMirrorTool_Statics::NewProp_Previews_MetaData[] = {
		{ "ModuleRelativePath", "Public/MirrorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UMirrorTool_Statics::NewProp_Previews = { "Previews", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMirrorTool, Previews), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UMirrorTool_Statics::NewProp_Previews_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMirrorTool_Statics::NewProp_Previews_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMirrorTool_Statics::NewProp_PlaneMechanic_MetaData[] = {
		{ "Comment", "// toggled by hotkey (shift)\n" },
		{ "ModuleRelativePath", "Public/MirrorTool.h" },
		{ "ToolTip", "toggled by hotkey (shift)" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMirrorTool_Statics::NewProp_PlaneMechanic = { "PlaneMechanic", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMirrorTool, PlaneMechanic), Z_Construct_UClass_UConstructionPlaneMechanic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMirrorTool_Statics::NewProp_PlaneMechanic_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMirrorTool_Statics::NewProp_PlaneMechanic_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMirrorTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMirrorTool_Statics::NewProp_Settings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMirrorTool_Statics::NewProp_ToolActions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMirrorTool_Statics::NewProp_MeshesToMirror_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMirrorTool_Statics::NewProp_MeshesToMirror,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMirrorTool_Statics::NewProp_Previews_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMirrorTool_Statics::NewProp_Previews,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMirrorTool_Statics::NewProp_PlaneMechanic,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMirrorTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMirrorTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMirrorTool_Statics::ClassParams = {
		&UMirrorTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMirrorTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMirrorTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMirrorTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMirrorTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMirrorTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMirrorTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMirrorTool, 1402617970);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UMirrorTool>()
	{
		return UMirrorTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMirrorTool(Z_Construct_UClass_UMirrorTool, &UMirrorTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UMirrorTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMirrorTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
