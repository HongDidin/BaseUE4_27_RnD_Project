// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLS_SetCollisionGeometryTool_generated_h
#error "SetCollisionGeometryTool.generated.h already included, missing '#pragma once' in SetCollisionGeometryTool.h"
#endif
#define MESHMODELINGTOOLS_SetCollisionGeometryTool_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_22_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_22_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_22_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSetCollisionGeometryToolBuilder(); \
	friend struct Z_Construct_UClass_USetCollisionGeometryToolBuilder_Statics; \
public: \
	DECLARE_CLASS(USetCollisionGeometryToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(USetCollisionGeometryToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_22_INCLASS \
private: \
	static void StaticRegisterNativesUSetCollisionGeometryToolBuilder(); \
	friend struct Z_Construct_UClass_USetCollisionGeometryToolBuilder_Statics; \
public: \
	DECLARE_CLASS(USetCollisionGeometryToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(USetCollisionGeometryToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USetCollisionGeometryToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USetCollisionGeometryToolBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USetCollisionGeometryToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USetCollisionGeometryToolBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USetCollisionGeometryToolBuilder(USetCollisionGeometryToolBuilder&&); \
	NO_API USetCollisionGeometryToolBuilder(const USetCollisionGeometryToolBuilder&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_22_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USetCollisionGeometryToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USetCollisionGeometryToolBuilder(USetCollisionGeometryToolBuilder&&); \
	NO_API USetCollisionGeometryToolBuilder(const USetCollisionGeometryToolBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USetCollisionGeometryToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USetCollisionGeometryToolBuilder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USetCollisionGeometryToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_22_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_19_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_22_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_22_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_22_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_22_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_22_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_22_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_22_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_22_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class USetCollisionGeometryToolBuilder>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_73_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_73_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_73_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_73_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSetCollisionGeometryToolProperties(); \
	friend struct Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics; \
public: \
	DECLARE_CLASS(USetCollisionGeometryToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(USetCollisionGeometryToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_73_INCLASS \
private: \
	static void StaticRegisterNativesUSetCollisionGeometryToolProperties(); \
	friend struct Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics; \
public: \
	DECLARE_CLASS(USetCollisionGeometryToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(USetCollisionGeometryToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_73_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USetCollisionGeometryToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USetCollisionGeometryToolProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USetCollisionGeometryToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USetCollisionGeometryToolProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USetCollisionGeometryToolProperties(USetCollisionGeometryToolProperties&&); \
	NO_API USetCollisionGeometryToolProperties(const USetCollisionGeometryToolProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_73_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USetCollisionGeometryToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USetCollisionGeometryToolProperties(USetCollisionGeometryToolProperties&&); \
	NO_API USetCollisionGeometryToolProperties(const USetCollisionGeometryToolProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USetCollisionGeometryToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USetCollisionGeometryToolProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USetCollisionGeometryToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_73_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_70_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_73_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_73_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_73_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_73_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_73_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_73_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_73_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_73_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_73_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_73_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_73_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_73_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class USetCollisionGeometryToolProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_141_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_141_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_141_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_141_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSetCollisionGeometryTool(); \
	friend struct Z_Construct_UClass_USetCollisionGeometryTool_Statics; \
public: \
	DECLARE_CLASS(USetCollisionGeometryTool, UMultiSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(USetCollisionGeometryTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_141_INCLASS \
private: \
	static void StaticRegisterNativesUSetCollisionGeometryTool(); \
	friend struct Z_Construct_UClass_USetCollisionGeometryTool_Statics; \
public: \
	DECLARE_CLASS(USetCollisionGeometryTool, UMultiSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(USetCollisionGeometryTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_141_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USetCollisionGeometryTool(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USetCollisionGeometryTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USetCollisionGeometryTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USetCollisionGeometryTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USetCollisionGeometryTool(USetCollisionGeometryTool&&); \
	NO_API USetCollisionGeometryTool(const USetCollisionGeometryTool&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_141_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USetCollisionGeometryTool() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USetCollisionGeometryTool(USetCollisionGeometryTool&&); \
	NO_API USetCollisionGeometryTool(const USetCollisionGeometryTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USetCollisionGeometryTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USetCollisionGeometryTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(USetCollisionGeometryTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_141_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Settings() { return STRUCT_OFFSET(USetCollisionGeometryTool, Settings); } \
	FORCEINLINE static uint32 __PPO__VizSettings() { return STRUCT_OFFSET(USetCollisionGeometryTool, VizSettings); } \
	FORCEINLINE static uint32 __PPO__CollisionProps() { return STRUCT_OFFSET(USetCollisionGeometryTool, CollisionProps); } \
	FORCEINLINE static uint32 __PPO__LineMaterial() { return STRUCT_OFFSET(USetCollisionGeometryTool, LineMaterial); } \
	FORCEINLINE static uint32 __PPO__PreviewGeom() { return STRUCT_OFFSET(USetCollisionGeometryTool, PreviewGeom); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_138_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_141_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_141_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_141_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_141_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_141_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_141_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_141_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_141_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_141_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_141_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_141_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h_141_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class USetCollisionGeometryTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_SetCollisionGeometryTool_h


#define FOREACH_ENUM_EPROJECTEDHULLAXIS(op) \
	op(EProjectedHullAxis::X) \
	op(EProjectedHullAxis::Y) \
	op(EProjectedHullAxis::Z) \
	op(EProjectedHullAxis::SmallestBoxDimension) \
	op(EProjectedHullAxis::SmallestVolume) 

enum class EProjectedHullAxis;
template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EProjectedHullAxis>();

#define FOREACH_ENUM_ECOLLISIONGEOMETRYTYPE(op) \
	op(ECollisionGeometryType::KeepExisting) \
	op(ECollisionGeometryType::AlignedBoxes) \
	op(ECollisionGeometryType::OrientedBoxes) \
	op(ECollisionGeometryType::MinimalSpheres) \
	op(ECollisionGeometryType::Capsules) \
	op(ECollisionGeometryType::ConvexHulls) \
	op(ECollisionGeometryType::SweptHulls) \
	op(ECollisionGeometryType::MinVolume) \
	op(ECollisionGeometryType::None) 

enum class ECollisionGeometryType;
template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<ECollisionGeometryType>();

#define FOREACH_ENUM_ESETCOLLISIONGEOMETRYINPUTMODE(op) \
	op(ESetCollisionGeometryInputMode::CombineAll) \
	op(ESetCollisionGeometryInputMode::PerInputObject) \
	op(ESetCollisionGeometryInputMode::PerMeshComponent) \
	op(ESetCollisionGeometryInputMode::PerMeshGroup) 

enum class ESetCollisionGeometryInputMode;
template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<ESetCollisionGeometryInputMode>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
