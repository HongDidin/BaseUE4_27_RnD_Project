// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MODELINGCOMPONENTS_WeightMapSetProperties_generated_h
#error "WeightMapSetProperties.generated.h already included, missing '#pragma once' in WeightMapSetProperties.h"
#endif
#define MODELINGCOMPONENTS_WeightMapSetProperties_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_WeightMapSetProperties_h_18_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_WeightMapSetProperties_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetWeightMapsFunc);


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_WeightMapSetProperties_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetWeightMapsFunc);


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_WeightMapSetProperties_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUWeightMapSetProperties(); \
	friend struct Z_Construct_UClass_UWeightMapSetProperties_Statics; \
public: \
	DECLARE_CLASS(UWeightMapSetProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UWeightMapSetProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_WeightMapSetProperties_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUWeightMapSetProperties(); \
	friend struct Z_Construct_UClass_UWeightMapSetProperties_Statics; \
public: \
	DECLARE_CLASS(UWeightMapSetProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UWeightMapSetProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_WeightMapSetProperties_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UWeightMapSetProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UWeightMapSetProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWeightMapSetProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWeightMapSetProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWeightMapSetProperties(UWeightMapSetProperties&&); \
	NO_API UWeightMapSetProperties(const UWeightMapSetProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_WeightMapSetProperties_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UWeightMapSetProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWeightMapSetProperties(UWeightMapSetProperties&&); \
	NO_API UWeightMapSetProperties(const UWeightMapSetProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWeightMapSetProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWeightMapSetProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UWeightMapSetProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_WeightMapSetProperties_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_WeightMapSetProperties_h_15_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_WeightMapSetProperties_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_WeightMapSetProperties_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_WeightMapSetProperties_h_18_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_WeightMapSetProperties_h_18_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_WeightMapSetProperties_h_18_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_WeightMapSetProperties_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_WeightMapSetProperties_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_WeightMapSetProperties_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_WeightMapSetProperties_h_18_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_WeightMapSetProperties_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_WeightMapSetProperties_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_WeightMapSetProperties_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MODELINGCOMPONENTS_API UClass* StaticClass<class UWeightMapSetProperties>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_WeightMapSetProperties_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
