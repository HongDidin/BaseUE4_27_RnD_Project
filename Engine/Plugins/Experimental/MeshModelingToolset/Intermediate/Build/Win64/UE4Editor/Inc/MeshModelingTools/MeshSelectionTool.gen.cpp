// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/MeshSelectionTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMeshSelectionTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EMeshFacesColorMode();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EMeshSelectionToolPrimaryMode();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EMeshSelectionToolActions();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshSelectionToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshSelectionToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UMeshSurfacePointToolBuilder();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshSelectionToolActionPropertySet_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshSelectionToolActionPropertySet();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshSelectionEditActions_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshSelectionEditActions();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshSelectionMeshEditActions_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshSelectionMeshEditActions();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshSelectionToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshSelectionToolProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshSelectionTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshSelectionTool();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDynamicMeshBrushTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UMeshSelectionSet_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
// End Cross Module References
	static UEnum* EMeshFacesColorMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EMeshFacesColorMode, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EMeshFacesColorMode"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EMeshFacesColorMode>()
	{
		return EMeshFacesColorMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMeshFacesColorMode(EMeshFacesColorMode_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EMeshFacesColorMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EMeshFacesColorMode_Hash() { return 1811099462U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EMeshFacesColorMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMeshFacesColorMode"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EMeshFacesColorMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMeshFacesColorMode::None", (int64)EMeshFacesColorMode::None },
				{ "EMeshFacesColorMode::ByGroup", (int64)EMeshFacesColorMode::ByGroup },
				{ "EMeshFacesColorMode::ByMaterialID", (int64)EMeshFacesColorMode::ByMaterialID },
				{ "EMeshFacesColorMode::ByUVIsland", (int64)EMeshFacesColorMode::ByUVIsland },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ByGroup.Comment", "/** Color mesh triangles by PolyGroup Color */" },
				{ "ByGroup.Name", "EMeshFacesColorMode::ByGroup" },
				{ "ByGroup.ToolTip", "Color mesh triangles by PolyGroup Color" },
				{ "ByMaterialID.Comment", "/** Color mesh triangles by Material ID */" },
				{ "ByMaterialID.Name", "EMeshFacesColorMode::ByMaterialID" },
				{ "ByMaterialID.ToolTip", "Color mesh triangles by Material ID" },
				{ "ByUVIsland.Comment", "/** Color mesh triangles by UV Island */" },
				{ "ByUVIsland.Name", "EMeshFacesColorMode::ByUVIsland" },
				{ "ByUVIsland.ToolTip", "Color mesh triangles by UV Island" },
				{ "ModuleRelativePath", "Public/MeshSelectionTool.h" },
				{ "None.Comment", "/** Display original mesh materials */" },
				{ "None.Name", "EMeshFacesColorMode::None" },
				{ "None.ToolTip", "Display original mesh materials" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EMeshFacesColorMode",
				"EMeshFacesColorMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EMeshSelectionToolPrimaryMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EMeshSelectionToolPrimaryMode, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EMeshSelectionToolPrimaryMode"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EMeshSelectionToolPrimaryMode>()
	{
		return EMeshSelectionToolPrimaryMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMeshSelectionToolPrimaryMode(EMeshSelectionToolPrimaryMode_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EMeshSelectionToolPrimaryMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EMeshSelectionToolPrimaryMode_Hash() { return 84770793U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EMeshSelectionToolPrimaryMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMeshSelectionToolPrimaryMode"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EMeshSelectionToolPrimaryMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMeshSelectionToolPrimaryMode::Brush", (int64)EMeshSelectionToolPrimaryMode::Brush },
				{ "EMeshSelectionToolPrimaryMode::VolumetricBrush", (int64)EMeshSelectionToolPrimaryMode::VolumetricBrush },
				{ "EMeshSelectionToolPrimaryMode::AngleFiltered", (int64)EMeshSelectionToolPrimaryMode::AngleFiltered },
				{ "EMeshSelectionToolPrimaryMode::Visible", (int64)EMeshSelectionToolPrimaryMode::Visible },
				{ "EMeshSelectionToolPrimaryMode::AllConnected", (int64)EMeshSelectionToolPrimaryMode::AllConnected },
				{ "EMeshSelectionToolPrimaryMode::AllInGroup", (int64)EMeshSelectionToolPrimaryMode::AllInGroup },
				{ "EMeshSelectionToolPrimaryMode::ByMaterial", (int64)EMeshSelectionToolPrimaryMode::ByMaterial },
				{ "EMeshSelectionToolPrimaryMode::ByUVIsland", (int64)EMeshSelectionToolPrimaryMode::ByUVIsland },
				{ "EMeshSelectionToolPrimaryMode::AllWithinAngle", (int64)EMeshSelectionToolPrimaryMode::AllWithinAngle },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "AllConnected.Comment", "/** Select all triangles connected to any triangle inside the brush */" },
				{ "AllConnected.Name", "EMeshSelectionToolPrimaryMode::AllConnected" },
				{ "AllConnected.ToolTip", "Select all triangles connected to any triangle inside the brush" },
				{ "AllInGroup.Comment", "/** Select all triangles in groups connected to any triangle inside the brush */" },
				{ "AllInGroup.Name", "EMeshSelectionToolPrimaryMode::AllInGroup" },
				{ "AllInGroup.ToolTip", "Select all triangles in groups connected to any triangle inside the brush" },
				{ "AllWithinAngle.Comment", "/** Select all triangles with normal within angular tolerance of hit triangle */" },
				{ "AllWithinAngle.Name", "EMeshSelectionToolPrimaryMode::AllWithinAngle" },
				{ "AllWithinAngle.ToolTip", "Select all triangles with normal within angular tolerance of hit triangle" },
				{ "AngleFiltered.Comment", "/** Select all triangles inside brush with normal within angular tolerance of hit triangle */" },
				{ "AngleFiltered.Name", "EMeshSelectionToolPrimaryMode::AngleFiltered" },
				{ "AngleFiltered.ToolTip", "Select all triangles inside brush with normal within angular tolerance of hit triangle" },
				{ "Brush.Comment", "/** Select all triangles inside the brush area */" },
				{ "Brush.Name", "EMeshSelectionToolPrimaryMode::Brush" },
				{ "Brush.ToolTip", "Select all triangles inside the brush area" },
				{ "ByMaterial.Comment", "/** Select all triangles with same material as hit triangle */" },
				{ "ByMaterial.Name", "EMeshSelectionToolPrimaryMode::ByMaterial" },
				{ "ByMaterial.ToolTip", "Select all triangles with same material as hit triangle" },
				{ "ByUVIsland.Comment", "/** Select all triangles in same UV island as hit triangle */" },
				{ "ByUVIsland.Name", "EMeshSelectionToolPrimaryMode::ByUVIsland" },
				{ "ByUVIsland.ToolTip", "Select all triangles in same UV island as hit triangle" },
				{ "ModuleRelativePath", "Public/MeshSelectionTool.h" },
				{ "Visible.Comment", "/** Select all triangles inside brush that are visible from current view */" },
				{ "Visible.Name", "EMeshSelectionToolPrimaryMode::Visible" },
				{ "Visible.ToolTip", "Select all triangles inside brush that are visible from current view" },
				{ "VolumetricBrush.Comment", "/** Select all triangles inside the brush volume */" },
				{ "VolumetricBrush.Name", "EMeshSelectionToolPrimaryMode::VolumetricBrush" },
				{ "VolumetricBrush.ToolTip", "Select all triangles inside the brush volume" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EMeshSelectionToolPrimaryMode",
				"EMeshSelectionToolPrimaryMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EMeshSelectionToolActions_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EMeshSelectionToolActions, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EMeshSelectionToolActions"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EMeshSelectionToolActions>()
	{
		return EMeshSelectionToolActions_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMeshSelectionToolActions(EMeshSelectionToolActions_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EMeshSelectionToolActions"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EMeshSelectionToolActions_Hash() { return 4160703533U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EMeshSelectionToolActions()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMeshSelectionToolActions"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EMeshSelectionToolActions_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMeshSelectionToolActions::NoAction", (int64)EMeshSelectionToolActions::NoAction },
				{ "EMeshSelectionToolActions::SelectAll", (int64)EMeshSelectionToolActions::SelectAll },
				{ "EMeshSelectionToolActions::ClearSelection", (int64)EMeshSelectionToolActions::ClearSelection },
				{ "EMeshSelectionToolActions::InvertSelection", (int64)EMeshSelectionToolActions::InvertSelection },
				{ "EMeshSelectionToolActions::GrowSelection", (int64)EMeshSelectionToolActions::GrowSelection },
				{ "EMeshSelectionToolActions::ShrinkSelection", (int64)EMeshSelectionToolActions::ShrinkSelection },
				{ "EMeshSelectionToolActions::ExpandToConnected", (int64)EMeshSelectionToolActions::ExpandToConnected },
				{ "EMeshSelectionToolActions::SelectLargestComponentByTriCount", (int64)EMeshSelectionToolActions::SelectLargestComponentByTriCount },
				{ "EMeshSelectionToolActions::SelectLargestComponentByArea", (int64)EMeshSelectionToolActions::SelectLargestComponentByArea },
				{ "EMeshSelectionToolActions::OptimizeSelection", (int64)EMeshSelectionToolActions::OptimizeSelection },
				{ "EMeshSelectionToolActions::DeleteSelected", (int64)EMeshSelectionToolActions::DeleteSelected },
				{ "EMeshSelectionToolActions::DisconnectSelected", (int64)EMeshSelectionToolActions::DisconnectSelected },
				{ "EMeshSelectionToolActions::SeparateSelected", (int64)EMeshSelectionToolActions::SeparateSelected },
				{ "EMeshSelectionToolActions::FlipSelected", (int64)EMeshSelectionToolActions::FlipSelected },
				{ "EMeshSelectionToolActions::CreateGroup", (int64)EMeshSelectionToolActions::CreateGroup },
				{ "EMeshSelectionToolActions::CycleSelectionMode", (int64)EMeshSelectionToolActions::CycleSelectionMode },
				{ "EMeshSelectionToolActions::CycleViewMode", (int64)EMeshSelectionToolActions::CycleViewMode },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ClearSelection.Name", "EMeshSelectionToolActions::ClearSelection" },
				{ "CreateGroup.Name", "EMeshSelectionToolActions::CreateGroup" },
				{ "CycleSelectionMode.Name", "EMeshSelectionToolActions::CycleSelectionMode" },
				{ "CycleViewMode.Name", "EMeshSelectionToolActions::CycleViewMode" },
				{ "DeleteSelected.Name", "EMeshSelectionToolActions::DeleteSelected" },
				{ "DisconnectSelected.Name", "EMeshSelectionToolActions::DisconnectSelected" },
				{ "ExpandToConnected.Name", "EMeshSelectionToolActions::ExpandToConnected" },
				{ "FlipSelected.Name", "EMeshSelectionToolActions::FlipSelected" },
				{ "GrowSelection.Name", "EMeshSelectionToolActions::GrowSelection" },
				{ "InvertSelection.Name", "EMeshSelectionToolActions::InvertSelection" },
				{ "ModuleRelativePath", "Public/MeshSelectionTool.h" },
				{ "NoAction.Name", "EMeshSelectionToolActions::NoAction" },
				{ "OptimizeSelection.Name", "EMeshSelectionToolActions::OptimizeSelection" },
				{ "SelectAll.Name", "EMeshSelectionToolActions::SelectAll" },
				{ "SelectLargestComponentByArea.Name", "EMeshSelectionToolActions::SelectLargestComponentByArea" },
				{ "SelectLargestComponentByTriCount.Name", "EMeshSelectionToolActions::SelectLargestComponentByTriCount" },
				{ "SeparateSelected.Name", "EMeshSelectionToolActions::SeparateSelected" },
				{ "ShrinkSelection.Name", "EMeshSelectionToolActions::ShrinkSelection" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EMeshSelectionToolActions",
				"EMeshSelectionToolActions",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UMeshSelectionToolBuilder::StaticRegisterNativesUMeshSelectionToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UMeshSelectionToolBuilder_NoRegister()
	{
		return UMeshSelectionToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UMeshSelectionToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshSelectionToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshSurfacePointToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSelectionToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "MeshSelectionTool.h" },
		{ "ModuleRelativePath", "Public/MeshSelectionTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshSelectionToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshSelectionToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshSelectionToolBuilder_Statics::ClassParams = {
		&UMeshSelectionToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshSelectionToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSelectionToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshSelectionToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshSelectionToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshSelectionToolBuilder, 1412921622);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UMeshSelectionToolBuilder>()
	{
		return UMeshSelectionToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshSelectionToolBuilder(Z_Construct_UClass_UMeshSelectionToolBuilder, &UMeshSelectionToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UMeshSelectionToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshSelectionToolBuilder);
	void UMeshSelectionToolActionPropertySet::StaticRegisterNativesUMeshSelectionToolActionPropertySet()
	{
	}
	UClass* Z_Construct_UClass_UMeshSelectionToolActionPropertySet_NoRegister()
	{
		return UMeshSelectionToolActionPropertySet::StaticClass();
	}
	struct Z_Construct_UClass_UMeshSelectionToolActionPropertySet_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshSelectionToolActionPropertySet_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSelectionToolActionPropertySet_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshSelectionTool.h" },
		{ "ModuleRelativePath", "Public/MeshSelectionTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshSelectionToolActionPropertySet_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshSelectionToolActionPropertySet>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshSelectionToolActionPropertySet_Statics::ClassParams = {
		&UMeshSelectionToolActionPropertySet::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshSelectionToolActionPropertySet_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSelectionToolActionPropertySet_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshSelectionToolActionPropertySet()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshSelectionToolActionPropertySet_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshSelectionToolActionPropertySet, 448134374);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UMeshSelectionToolActionPropertySet>()
	{
		return UMeshSelectionToolActionPropertySet::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshSelectionToolActionPropertySet(Z_Construct_UClass_UMeshSelectionToolActionPropertySet, &UMeshSelectionToolActionPropertySet::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UMeshSelectionToolActionPropertySet"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshSelectionToolActionPropertySet);
	DEFINE_FUNCTION(UMeshSelectionEditActions::execOptimizeSelection)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OptimizeSelection();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMeshSelectionEditActions::execSelectLargestComponentByArea)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SelectLargestComponentByArea();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMeshSelectionEditActions::execSelectLargestComponentByTriCount)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SelectLargestComponentByTriCount();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMeshSelectionEditActions::execExpandToConnected)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ExpandToConnected();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMeshSelectionEditActions::execShrink)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Shrink();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMeshSelectionEditActions::execGrow)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Grow();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMeshSelectionEditActions::execInvert)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Invert();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMeshSelectionEditActions::execClear)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Clear();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMeshSelectionEditActions::execSelectAll)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SelectAll();
		P_NATIVE_END;
	}
	void UMeshSelectionEditActions::StaticRegisterNativesUMeshSelectionEditActions()
	{
		UClass* Class = UMeshSelectionEditActions::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Clear", &UMeshSelectionEditActions::execClear },
			{ "ExpandToConnected", &UMeshSelectionEditActions::execExpandToConnected },
			{ "Grow", &UMeshSelectionEditActions::execGrow },
			{ "Invert", &UMeshSelectionEditActions::execInvert },
			{ "OptimizeSelection", &UMeshSelectionEditActions::execOptimizeSelection },
			{ "SelectAll", &UMeshSelectionEditActions::execSelectAll },
			{ "SelectLargestComponentByArea", &UMeshSelectionEditActions::execSelectLargestComponentByArea },
			{ "SelectLargestComponentByTriCount", &UMeshSelectionEditActions::execSelectLargestComponentByTriCount },
			{ "Shrink", &UMeshSelectionEditActions::execShrink },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMeshSelectionEditActions_Clear_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMeshSelectionEditActions_Clear_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "SelectionEdits" },
		{ "DisplayName", "Clear" },
		{ "DisplayPriority", "1" },
		{ "ModuleRelativePath", "Public/MeshSelectionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMeshSelectionEditActions_Clear_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMeshSelectionEditActions, nullptr, "Clear", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMeshSelectionEditActions_Clear_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMeshSelectionEditActions_Clear_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMeshSelectionEditActions_Clear()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMeshSelectionEditActions_Clear_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMeshSelectionEditActions_ExpandToConnected_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMeshSelectionEditActions_ExpandToConnected_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "SelectionEdits" },
		{ "DisplayName", "ExpandToConnected" },
		{ "DisplayPriority", "5" },
		{ "ModuleRelativePath", "Public/MeshSelectionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMeshSelectionEditActions_ExpandToConnected_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMeshSelectionEditActions, nullptr, "ExpandToConnected", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMeshSelectionEditActions_ExpandToConnected_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMeshSelectionEditActions_ExpandToConnected_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMeshSelectionEditActions_ExpandToConnected()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMeshSelectionEditActions_ExpandToConnected_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMeshSelectionEditActions_Grow_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMeshSelectionEditActions_Grow_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "SelectionEdits" },
		{ "DisplayName", "Grow" },
		{ "DisplayPriority", "3" },
		{ "ModuleRelativePath", "Public/MeshSelectionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMeshSelectionEditActions_Grow_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMeshSelectionEditActions, nullptr, "Grow", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMeshSelectionEditActions_Grow_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMeshSelectionEditActions_Grow_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMeshSelectionEditActions_Grow()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMeshSelectionEditActions_Grow_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMeshSelectionEditActions_Invert_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMeshSelectionEditActions_Invert_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "SelectionEdits" },
		{ "DisplayName", "Invert" },
		{ "DisplayPriority", "2" },
		{ "ModuleRelativePath", "Public/MeshSelectionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMeshSelectionEditActions_Invert_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMeshSelectionEditActions, nullptr, "Invert", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMeshSelectionEditActions_Invert_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMeshSelectionEditActions_Invert_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMeshSelectionEditActions_Invert()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMeshSelectionEditActions_Invert_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMeshSelectionEditActions_OptimizeSelection_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMeshSelectionEditActions_OptimizeSelection_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "SelectionEdits" },
		{ "DisplayName", "OptimizeSelection" },
		{ "DisplayPriority", "5" },
		{ "ModuleRelativePath", "Public/MeshSelectionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMeshSelectionEditActions_OptimizeSelection_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMeshSelectionEditActions, nullptr, "OptimizeSelection", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMeshSelectionEditActions_OptimizeSelection_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMeshSelectionEditActions_OptimizeSelection_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMeshSelectionEditActions_OptimizeSelection()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMeshSelectionEditActions_OptimizeSelection_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMeshSelectionEditActions_SelectAll_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMeshSelectionEditActions_SelectAll_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "SelectionEdits" },
		{ "DisplayName", "Select All" },
		{ "DisplayPriority", "1" },
		{ "ModuleRelativePath", "Public/MeshSelectionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMeshSelectionEditActions_SelectAll_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMeshSelectionEditActions, nullptr, "SelectAll", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMeshSelectionEditActions_SelectAll_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMeshSelectionEditActions_SelectAll_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMeshSelectionEditActions_SelectAll()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMeshSelectionEditActions_SelectAll_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMeshSelectionEditActions_SelectLargestComponentByArea_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMeshSelectionEditActions_SelectLargestComponentByArea_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "SelectionEdits" },
		{ "DisplayName", "SelectLargestComponentByArea" },
		{ "DisplayPriority", "5" },
		{ "ModuleRelativePath", "Public/MeshSelectionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMeshSelectionEditActions_SelectLargestComponentByArea_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMeshSelectionEditActions, nullptr, "SelectLargestComponentByArea", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMeshSelectionEditActions_SelectLargestComponentByArea_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMeshSelectionEditActions_SelectLargestComponentByArea_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMeshSelectionEditActions_SelectLargestComponentByArea()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMeshSelectionEditActions_SelectLargestComponentByArea_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMeshSelectionEditActions_SelectLargestComponentByTriCount_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMeshSelectionEditActions_SelectLargestComponentByTriCount_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "SelectionEdits" },
		{ "DisplayName", "SelectLargestComponentByTriCount" },
		{ "DisplayPriority", "5" },
		{ "ModuleRelativePath", "Public/MeshSelectionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMeshSelectionEditActions_SelectLargestComponentByTriCount_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMeshSelectionEditActions, nullptr, "SelectLargestComponentByTriCount", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMeshSelectionEditActions_SelectLargestComponentByTriCount_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMeshSelectionEditActions_SelectLargestComponentByTriCount_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMeshSelectionEditActions_SelectLargestComponentByTriCount()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMeshSelectionEditActions_SelectLargestComponentByTriCount_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMeshSelectionEditActions_Shrink_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMeshSelectionEditActions_Shrink_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "SelectionEdits" },
		{ "DisplayName", "Shrink" },
		{ "DisplayPriority", "4" },
		{ "ModuleRelativePath", "Public/MeshSelectionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMeshSelectionEditActions_Shrink_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMeshSelectionEditActions, nullptr, "Shrink", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMeshSelectionEditActions_Shrink_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMeshSelectionEditActions_Shrink_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMeshSelectionEditActions_Shrink()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMeshSelectionEditActions_Shrink_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMeshSelectionEditActions_NoRegister()
	{
		return UMeshSelectionEditActions::StaticClass();
	}
	struct Z_Construct_UClass_UMeshSelectionEditActions_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshSelectionEditActions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshSelectionToolActionPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMeshSelectionEditActions_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMeshSelectionEditActions_Clear, "Clear" }, // 1375005326
		{ &Z_Construct_UFunction_UMeshSelectionEditActions_ExpandToConnected, "ExpandToConnected" }, // 28715916
		{ &Z_Construct_UFunction_UMeshSelectionEditActions_Grow, "Grow" }, // 2533489633
		{ &Z_Construct_UFunction_UMeshSelectionEditActions_Invert, "Invert" }, // 4205674017
		{ &Z_Construct_UFunction_UMeshSelectionEditActions_OptimizeSelection, "OptimizeSelection" }, // 3391492346
		{ &Z_Construct_UFunction_UMeshSelectionEditActions_SelectAll, "SelectAll" }, // 1504277466
		{ &Z_Construct_UFunction_UMeshSelectionEditActions_SelectLargestComponentByArea, "SelectLargestComponentByArea" }, // 1489808124
		{ &Z_Construct_UFunction_UMeshSelectionEditActions_SelectLargestComponentByTriCount, "SelectLargestComponentByTriCount" }, // 2824315643
		{ &Z_Construct_UFunction_UMeshSelectionEditActions_Shrink, "Shrink" }, // 234235578
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSelectionEditActions_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshSelectionTool.h" },
		{ "ModuleRelativePath", "Public/MeshSelectionTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshSelectionEditActions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshSelectionEditActions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshSelectionEditActions_Statics::ClassParams = {
		&UMeshSelectionEditActions::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshSelectionEditActions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSelectionEditActions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshSelectionEditActions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshSelectionEditActions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshSelectionEditActions, 3935044343);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UMeshSelectionEditActions>()
	{
		return UMeshSelectionEditActions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshSelectionEditActions(Z_Construct_UClass_UMeshSelectionEditActions, &UMeshSelectionEditActions::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UMeshSelectionEditActions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshSelectionEditActions);
	DEFINE_FUNCTION(UMeshSelectionMeshEditActions::execCreatePolygroup)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->CreatePolygroup();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMeshSelectionMeshEditActions::execFlipNormals)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->FlipNormals();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMeshSelectionMeshEditActions::execDisconnectTriangles)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->DisconnectTriangles();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMeshSelectionMeshEditActions::execSeparateTriangles)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SeparateTriangles();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMeshSelectionMeshEditActions::execDeleteTriangles)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->DeleteTriangles();
		P_NATIVE_END;
	}
	void UMeshSelectionMeshEditActions::StaticRegisterNativesUMeshSelectionMeshEditActions()
	{
		UClass* Class = UMeshSelectionMeshEditActions::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CreatePolygroup", &UMeshSelectionMeshEditActions::execCreatePolygroup },
			{ "DeleteTriangles", &UMeshSelectionMeshEditActions::execDeleteTriangles },
			{ "DisconnectTriangles", &UMeshSelectionMeshEditActions::execDisconnectTriangles },
			{ "FlipNormals", &UMeshSelectionMeshEditActions::execFlipNormals },
			{ "SeparateTriangles", &UMeshSelectionMeshEditActions::execSeparateTriangles },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMeshSelectionMeshEditActions_CreatePolygroup_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMeshSelectionMeshEditActions_CreatePolygroup_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "MeshEdits" },
		{ "DisplayName", "Create Polygroup" },
		{ "DisplayPriority", "5" },
		{ "ModuleRelativePath", "Public/MeshSelectionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMeshSelectionMeshEditActions_CreatePolygroup_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMeshSelectionMeshEditActions, nullptr, "CreatePolygroup", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMeshSelectionMeshEditActions_CreatePolygroup_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMeshSelectionMeshEditActions_CreatePolygroup_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMeshSelectionMeshEditActions_CreatePolygroup()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMeshSelectionMeshEditActions_CreatePolygroup_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMeshSelectionMeshEditActions_DeleteTriangles_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMeshSelectionMeshEditActions_DeleteTriangles_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "MeshEdits" },
		{ "DisplayName", "Delete" },
		{ "DisplayPriority", "1" },
		{ "ModuleRelativePath", "Public/MeshSelectionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMeshSelectionMeshEditActions_DeleteTriangles_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMeshSelectionMeshEditActions, nullptr, "DeleteTriangles", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMeshSelectionMeshEditActions_DeleteTriangles_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMeshSelectionMeshEditActions_DeleteTriangles_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMeshSelectionMeshEditActions_DeleteTriangles()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMeshSelectionMeshEditActions_DeleteTriangles_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMeshSelectionMeshEditActions_DisconnectTriangles_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMeshSelectionMeshEditActions_DisconnectTriangles_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "MeshEdits" },
		{ "DisplayName", "Disconnect" },
		{ "DisplayPriority", "3" },
		{ "ModuleRelativePath", "Public/MeshSelectionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMeshSelectionMeshEditActions_DisconnectTriangles_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMeshSelectionMeshEditActions, nullptr, "DisconnectTriangles", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMeshSelectionMeshEditActions_DisconnectTriangles_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMeshSelectionMeshEditActions_DisconnectTriangles_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMeshSelectionMeshEditActions_DisconnectTriangles()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMeshSelectionMeshEditActions_DisconnectTriangles_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMeshSelectionMeshEditActions_FlipNormals_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMeshSelectionMeshEditActions_FlipNormals_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "MeshEdits" },
		{ "DisplayName", "Flip Normals" },
		{ "DisplayPriority", "4" },
		{ "ModuleRelativePath", "Public/MeshSelectionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMeshSelectionMeshEditActions_FlipNormals_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMeshSelectionMeshEditActions, nullptr, "FlipNormals", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMeshSelectionMeshEditActions_FlipNormals_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMeshSelectionMeshEditActions_FlipNormals_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMeshSelectionMeshEditActions_FlipNormals()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMeshSelectionMeshEditActions_FlipNormals_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMeshSelectionMeshEditActions_SeparateTriangles_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMeshSelectionMeshEditActions_SeparateTriangles_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "MeshEdits" },
		{ "DisplayName", "Separate" },
		{ "DisplayPriority", "2" },
		{ "ModuleRelativePath", "Public/MeshSelectionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMeshSelectionMeshEditActions_SeparateTriangles_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMeshSelectionMeshEditActions, nullptr, "SeparateTriangles", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMeshSelectionMeshEditActions_SeparateTriangles_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMeshSelectionMeshEditActions_SeparateTriangles_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMeshSelectionMeshEditActions_SeparateTriangles()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMeshSelectionMeshEditActions_SeparateTriangles_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMeshSelectionMeshEditActions_NoRegister()
	{
		return UMeshSelectionMeshEditActions::StaticClass();
	}
	struct Z_Construct_UClass_UMeshSelectionMeshEditActions_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshSelectionMeshEditActions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshSelectionToolActionPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMeshSelectionMeshEditActions_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMeshSelectionMeshEditActions_CreatePolygroup, "CreatePolygroup" }, // 870147978
		{ &Z_Construct_UFunction_UMeshSelectionMeshEditActions_DeleteTriangles, "DeleteTriangles" }, // 1533469937
		{ &Z_Construct_UFunction_UMeshSelectionMeshEditActions_DisconnectTriangles, "DisconnectTriangles" }, // 1511755762
		{ &Z_Construct_UFunction_UMeshSelectionMeshEditActions_FlipNormals, "FlipNormals" }, // 1920596738
		{ &Z_Construct_UFunction_UMeshSelectionMeshEditActions_SeparateTriangles, "SeparateTriangles" }, // 998525678
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSelectionMeshEditActions_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshSelectionTool.h" },
		{ "ModuleRelativePath", "Public/MeshSelectionTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshSelectionMeshEditActions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshSelectionMeshEditActions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshSelectionMeshEditActions_Statics::ClassParams = {
		&UMeshSelectionMeshEditActions::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshSelectionMeshEditActions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSelectionMeshEditActions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshSelectionMeshEditActions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshSelectionMeshEditActions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshSelectionMeshEditActions, 3531428898);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UMeshSelectionMeshEditActions>()
	{
		return UMeshSelectionMeshEditActions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshSelectionMeshEditActions(Z_Construct_UClass_UMeshSelectionMeshEditActions, &UMeshSelectionMeshEditActions::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UMeshSelectionMeshEditActions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshSelectionMeshEditActions);
	void UMeshSelectionToolProperties::StaticRegisterNativesUMeshSelectionToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UMeshSelectionToolProperties_NoRegister()
	{
		return UMeshSelectionToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UMeshSelectionToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_SelectionMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SelectionMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_SelectionMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AngleTolerance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AngleTolerance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bHitBackFaces_MetaData[];
#endif
		static void NewProp_bHitBackFaces_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bHitBackFaces;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowWireframe_MetaData[];
#endif
		static void NewProp_bShowWireframe_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowWireframe;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_FaceColorMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FaceColorMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_FaceColorMode;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshSelectionToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSelectionToolProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshSelectionTool.h" },
		{ "ModuleRelativePath", "Public/MeshSelectionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UMeshSelectionToolProperties_Statics::NewProp_SelectionMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSelectionToolProperties_Statics::NewProp_SelectionMode_MetaData[] = {
		{ "Category", "Selection" },
		{ "Comment", "/** The Selection Mode defines the behavior of the selection brush */" },
		{ "ModuleRelativePath", "Public/MeshSelectionTool.h" },
		{ "ToolTip", "The Selection Mode defines the behavior of the selection brush" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UMeshSelectionToolProperties_Statics::NewProp_SelectionMode = { "SelectionMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshSelectionToolProperties, SelectionMode), Z_Construct_UEnum_MeshModelingTools_EMeshSelectionToolPrimaryMode, METADATA_PARAMS(Z_Construct_UClass_UMeshSelectionToolProperties_Statics::NewProp_SelectionMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSelectionToolProperties_Statics::NewProp_SelectionMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSelectionToolProperties_Statics::NewProp_AngleTolerance_MetaData[] = {
		{ "Category", "Selection" },
		{ "Comment", "/** Angle in Degrees used for Angle-based Selection Modes */" },
		{ "ModuleRelativePath", "Public/MeshSelectionTool.h" },
		{ "ToolTip", "Angle in Degrees used for Angle-based Selection Modes" },
		{ "UIMax", "90.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UMeshSelectionToolProperties_Statics::NewProp_AngleTolerance = { "AngleTolerance", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshSelectionToolProperties, AngleTolerance), METADATA_PARAMS(Z_Construct_UClass_UMeshSelectionToolProperties_Statics::NewProp_AngleTolerance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSelectionToolProperties_Statics::NewProp_AngleTolerance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSelectionToolProperties_Statics::NewProp_bHitBackFaces_MetaData[] = {
		{ "Category", "Selection" },
		{ "Comment", "/** Allow the brush to hit back-facing parts of the surface  */" },
		{ "ModuleRelativePath", "Public/MeshSelectionTool.h" },
		{ "ToolTip", "Allow the brush to hit back-facing parts of the surface" },
	};
#endif
	void Z_Construct_UClass_UMeshSelectionToolProperties_Statics::NewProp_bHitBackFaces_SetBit(void* Obj)
	{
		((UMeshSelectionToolProperties*)Obj)->bHitBackFaces = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshSelectionToolProperties_Statics::NewProp_bHitBackFaces = { "bHitBackFaces", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshSelectionToolProperties), &Z_Construct_UClass_UMeshSelectionToolProperties_Statics::NewProp_bHitBackFaces_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshSelectionToolProperties_Statics::NewProp_bHitBackFaces_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSelectionToolProperties_Statics::NewProp_bHitBackFaces_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSelectionToolProperties_Statics::NewProp_bShowWireframe_MetaData[] = {
		{ "Category", "Selection" },
		{ "Comment", "/** Toggle drawing of wireframe overlay on/off [Alt+W] */" },
		{ "ModuleRelativePath", "Public/MeshSelectionTool.h" },
		{ "ToolTip", "Toggle drawing of wireframe overlay on/off [Alt+W]" },
	};
#endif
	void Z_Construct_UClass_UMeshSelectionToolProperties_Statics::NewProp_bShowWireframe_SetBit(void* Obj)
	{
		((UMeshSelectionToolProperties*)Obj)->bShowWireframe = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshSelectionToolProperties_Statics::NewProp_bShowWireframe = { "bShowWireframe", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshSelectionToolProperties), &Z_Construct_UClass_UMeshSelectionToolProperties_Statics::NewProp_bShowWireframe_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshSelectionToolProperties_Statics::NewProp_bShowWireframe_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSelectionToolProperties_Statics::NewProp_bShowWireframe_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UMeshSelectionToolProperties_Statics::NewProp_FaceColorMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSelectionToolProperties_Statics::NewProp_FaceColorMode_MetaData[] = {
		{ "Category", "Selection" },
		{ "ModuleRelativePath", "Public/MeshSelectionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UMeshSelectionToolProperties_Statics::NewProp_FaceColorMode = { "FaceColorMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshSelectionToolProperties, FaceColorMode), Z_Construct_UEnum_MeshModelingTools_EMeshFacesColorMode, METADATA_PARAMS(Z_Construct_UClass_UMeshSelectionToolProperties_Statics::NewProp_FaceColorMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSelectionToolProperties_Statics::NewProp_FaceColorMode_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshSelectionToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSelectionToolProperties_Statics::NewProp_SelectionMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSelectionToolProperties_Statics::NewProp_SelectionMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSelectionToolProperties_Statics::NewProp_AngleTolerance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSelectionToolProperties_Statics::NewProp_bHitBackFaces,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSelectionToolProperties_Statics::NewProp_bShowWireframe,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSelectionToolProperties_Statics::NewProp_FaceColorMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSelectionToolProperties_Statics::NewProp_FaceColorMode,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshSelectionToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshSelectionToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshSelectionToolProperties_Statics::ClassParams = {
		&UMeshSelectionToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshSelectionToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSelectionToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshSelectionToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSelectionToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshSelectionToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshSelectionToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshSelectionToolProperties, 394653519);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UMeshSelectionToolProperties>()
	{
		return UMeshSelectionToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshSelectionToolProperties(Z_Construct_UClass_UMeshSelectionToolProperties, &UMeshSelectionToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UMeshSelectionToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshSelectionToolProperties);
	void UMeshSelectionTool::StaticRegisterNativesUMeshSelectionTool()
	{
	}
	UClass* Z_Construct_UClass_UMeshSelectionTool_NoRegister()
	{
		return UMeshSelectionTool::StaticClass();
	}
	struct Z_Construct_UClass_UMeshSelectionTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SelectionProps_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SelectionProps;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SelectionActions_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SelectionActions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EditActions_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_EditActions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Selection_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Selection;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SpawnedActors_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpawnedActors_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SpawnedActors;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshSelectionTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDynamicMeshBrushTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSelectionTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "MeshSelectionTool.h" },
		{ "ModuleRelativePath", "Public/MeshSelectionTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSelectionTool_Statics::NewProp_SelectionProps_MetaData[] = {
		{ "ModuleRelativePath", "Public/MeshSelectionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshSelectionTool_Statics::NewProp_SelectionProps = { "SelectionProps", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshSelectionTool, SelectionProps), Z_Construct_UClass_UMeshSelectionToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshSelectionTool_Statics::NewProp_SelectionProps_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSelectionTool_Statics::NewProp_SelectionProps_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSelectionTool_Statics::NewProp_SelectionActions_MetaData[] = {
		{ "ModuleRelativePath", "Public/MeshSelectionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshSelectionTool_Statics::NewProp_SelectionActions = { "SelectionActions", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshSelectionTool, SelectionActions), Z_Construct_UClass_UMeshSelectionEditActions_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshSelectionTool_Statics::NewProp_SelectionActions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSelectionTool_Statics::NewProp_SelectionActions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSelectionTool_Statics::NewProp_EditActions_MetaData[] = {
		{ "ModuleRelativePath", "Public/MeshSelectionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshSelectionTool_Statics::NewProp_EditActions = { "EditActions", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshSelectionTool, EditActions), Z_Construct_UClass_UMeshSelectionToolActionPropertySet_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshSelectionTool_Statics::NewProp_EditActions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSelectionTool_Statics::NewProp_EditActions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSelectionTool_Statics::NewProp_Selection_MetaData[] = {
		{ "ModuleRelativePath", "Public/MeshSelectionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshSelectionTool_Statics::NewProp_Selection = { "Selection", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshSelectionTool, Selection), Z_Construct_UClass_UMeshSelectionSet_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshSelectionTool_Statics::NewProp_Selection_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSelectionTool_Statics::NewProp_Selection_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshSelectionTool_Statics::NewProp_SpawnedActors_Inner = { "SpawnedActors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSelectionTool_Statics::NewProp_SpawnedActors_MetaData[] = {
		{ "ModuleRelativePath", "Public/MeshSelectionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UMeshSelectionTool_Statics::NewProp_SpawnedActors = { "SpawnedActors", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshSelectionTool, SpawnedActors), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UMeshSelectionTool_Statics::NewProp_SpawnedActors_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSelectionTool_Statics::NewProp_SpawnedActors_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshSelectionTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSelectionTool_Statics::NewProp_SelectionProps,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSelectionTool_Statics::NewProp_SelectionActions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSelectionTool_Statics::NewProp_EditActions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSelectionTool_Statics::NewProp_Selection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSelectionTool_Statics::NewProp_SpawnedActors_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSelectionTool_Statics::NewProp_SpawnedActors,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshSelectionTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshSelectionTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshSelectionTool_Statics::ClassParams = {
		&UMeshSelectionTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshSelectionTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSelectionTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshSelectionTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSelectionTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshSelectionTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshSelectionTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshSelectionTool, 1471239969);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UMeshSelectionTool>()
	{
		return UMeshSelectionTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshSelectionTool(Z_Construct_UClass_UMeshSelectionTool, &UMeshSelectionTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UMeshSelectionTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshSelectionTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
