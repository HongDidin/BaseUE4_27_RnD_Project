// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/MeshBoundaryToolBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMeshBoundaryToolBase() {}
// Cross Module References
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshBoundaryToolBase_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshBoundaryToolBase();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_USingleSelectionTool();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPolygonSelectionMechanic_NoRegister();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_USingleClickInputBehavior_NoRegister();
// End Cross Module References
	void UMeshBoundaryToolBase::StaticRegisterNativesUMeshBoundaryToolBase()
	{
	}
	UClass* Z_Construct_UClass_UMeshBoundaryToolBase_NoRegister()
	{
		return UMeshBoundaryToolBase::StaticClass();
	}
	struct Z_Construct_UClass_UMeshBoundaryToolBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SelectionMechanic_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SelectionMechanic;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LoopSelectClickBehavior_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LoopSelectClickBehavior;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshBoundaryToolBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USingleSelectionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshBoundaryToolBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n  * Base class for tools that do things with a mesh boundary. Provides ability to select mesh boundaries\n  * and some other boilerplate code.\n  *\x09TODO: We can refactor to make the HoleFiller tool inherit from this.\n  */" },
		{ "IncludePath", "MeshBoundaryToolBase.h" },
		{ "ModuleRelativePath", "Public/MeshBoundaryToolBase.h" },
		{ "ToolTip", "Base class for tools that do things with a mesh boundary. Provides ability to select mesh boundaries\nand some other boilerplate code.\n   TODO: We can refactor to make the HoleFiller tool inherit from this." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshBoundaryToolBase_Statics::NewProp_SelectionMechanic_MetaData[] = {
		{ "ModuleRelativePath", "Public/MeshBoundaryToolBase.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshBoundaryToolBase_Statics::NewProp_SelectionMechanic = { "SelectionMechanic", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshBoundaryToolBase, SelectionMechanic), Z_Construct_UClass_UPolygonSelectionMechanic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshBoundaryToolBase_Statics::NewProp_SelectionMechanic_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshBoundaryToolBase_Statics::NewProp_SelectionMechanic_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshBoundaryToolBase_Statics::NewProp_LoopSelectClickBehavior_MetaData[] = {
		{ "Comment", "// Behavior that wraps this class's selection click operation. Useful to have a pointer to so that its\n// priority can be worked with.\n" },
		{ "ModuleRelativePath", "Public/MeshBoundaryToolBase.h" },
		{ "ToolTip", "Behavior that wraps this class's selection click operation. Useful to have a pointer to so that its\npriority can be worked with." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshBoundaryToolBase_Statics::NewProp_LoopSelectClickBehavior = { "LoopSelectClickBehavior", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshBoundaryToolBase, LoopSelectClickBehavior), Z_Construct_UClass_USingleClickInputBehavior_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshBoundaryToolBase_Statics::NewProp_LoopSelectClickBehavior_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshBoundaryToolBase_Statics::NewProp_LoopSelectClickBehavior_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshBoundaryToolBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshBoundaryToolBase_Statics::NewProp_SelectionMechanic,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshBoundaryToolBase_Statics::NewProp_LoopSelectClickBehavior,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshBoundaryToolBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshBoundaryToolBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshBoundaryToolBase_Statics::ClassParams = {
		&UMeshBoundaryToolBase::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshBoundaryToolBase_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshBoundaryToolBase_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshBoundaryToolBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshBoundaryToolBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshBoundaryToolBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshBoundaryToolBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshBoundaryToolBase, 4141344701);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UMeshBoundaryToolBase>()
	{
		return UMeshBoundaryToolBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshBoundaryToolBase(Z_Construct_UClass_UMeshBoundaryToolBase, &UMeshBoundaryToolBase::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UMeshBoundaryToolBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshBoundaryToolBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
