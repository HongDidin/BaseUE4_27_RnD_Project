// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ModelingComponents/Public/Drawing/PolyEditPreviewMesh.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePolyEditPreviewMesh() {}
// Cross Module References
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPolyEditPreviewMesh_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPolyEditPreviewMesh();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPreviewMesh();
	UPackage* Z_Construct_UPackage__Script_ModelingComponents();
// End Cross Module References
	void UPolyEditPreviewMesh::StaticRegisterNativesUPolyEditPreviewMesh()
	{
	}
	UClass* Z_Construct_UClass_UPolyEditPreviewMesh_NoRegister()
	{
		return UPolyEditPreviewMesh::StaticClass();
	}
	struct Z_Construct_UClass_UPolyEditPreviewMesh_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPolyEditPreviewMesh_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UPreviewMesh,
		(UObject* (*)())Z_Construct_UPackage__Script_ModelingComponents,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolyEditPreviewMesh_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * UPolyEditPreviewMesh is a variant of UPreviewMesh intended for use as a 'live preview' of\n * a mesh creation/editing operation. The class supports initializing the preview mesh in various\n * ways, generally as a submesh of a base mesh.\n */" },
		{ "IncludePath", "Drawing/PolyEditPreviewMesh.h" },
		{ "ModuleRelativePath", "Public/Drawing/PolyEditPreviewMesh.h" },
		{ "ToolTip", "UPolyEditPreviewMesh is a variant of UPreviewMesh intended for use as a 'live preview' of\na mesh creation/editing operation. The class supports initializing the preview mesh in various\nways, generally as a submesh of a base mesh." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPolyEditPreviewMesh_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPolyEditPreviewMesh>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPolyEditPreviewMesh_Statics::ClassParams = {
		&UPolyEditPreviewMesh::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UPolyEditPreviewMesh_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPolyEditPreviewMesh_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPolyEditPreviewMesh()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPolyEditPreviewMesh_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPolyEditPreviewMesh, 4093062740);
	template<> MODELINGCOMPONENTS_API UClass* StaticClass<UPolyEditPreviewMesh>()
	{
		return UPolyEditPreviewMesh::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPolyEditPreviewMesh(Z_Construct_UClass_UPolyEditPreviewMesh, &UPolyEditPreviewMesh::StaticClass, TEXT("/Script/ModelingComponents"), TEXT("UPolyEditPreviewMesh"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPolyEditPreviewMesh);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
