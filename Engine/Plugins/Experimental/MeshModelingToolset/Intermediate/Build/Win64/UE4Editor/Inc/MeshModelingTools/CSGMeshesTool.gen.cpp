// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/CSGMeshesTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCSGMeshesTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UCSGMeshesToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UCSGMeshesToolProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MODELINGOPERATORS_API UEnum* Z_Construct_UEnum_ModelingOperators_ECSGOperation();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UCSGMeshesTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UCSGMeshesTool();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UBaseCreateFromSelectedTool();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_ULineSetComponent_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UCSGMeshesToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UCSGMeshesToolBuilder();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UBaseCreateFromSelectedToolBuilder();
// End Cross Module References
	void UCSGMeshesToolProperties::StaticRegisterNativesUCSGMeshesToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UCSGMeshesToolProperties_NoRegister()
	{
		return UCSGMeshesToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UCSGMeshesToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Operation_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Operation_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Operation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowNewBoundaryEdges_MetaData[];
#endif
		static void NewProp_bShowNewBoundaryEdges_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowNewBoundaryEdges;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAttemptFixHoles_MetaData[];
#endif
		static void NewProp_bAttemptFixHoles_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAttemptFixHoles;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOnlyUseFirstMeshMaterials_MetaData[];
#endif
		static void NewProp_bOnlyUseFirstMeshMaterials_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOnlyUseFirstMeshMaterials;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCSGMeshesToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCSGMeshesToolProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Standard properties of the CSG operation\n */" },
		{ "IncludePath", "CSGMeshesTool.h" },
		{ "ModuleRelativePath", "Public/CSGMeshesTool.h" },
		{ "ToolTip", "Standard properties of the CSG operation" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UCSGMeshesToolProperties_Statics::NewProp_Operation_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCSGMeshesToolProperties_Statics::NewProp_Operation_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** The type of operation */" },
		{ "ModuleRelativePath", "Public/CSGMeshesTool.h" },
		{ "ToolTip", "The type of operation" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UCSGMeshesToolProperties_Statics::NewProp_Operation = { "Operation", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCSGMeshesToolProperties, Operation), Z_Construct_UEnum_ModelingOperators_ECSGOperation, METADATA_PARAMS(Z_Construct_UClass_UCSGMeshesToolProperties_Statics::NewProp_Operation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCSGMeshesToolProperties_Statics::NewProp_Operation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCSGMeshesToolProperties_Statics::NewProp_bShowNewBoundaryEdges_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Show boundary edges created by the CSG operation -- often due to numerical error */" },
		{ "ModuleRelativePath", "Public/CSGMeshesTool.h" },
		{ "ToolTip", "Show boundary edges created by the CSG operation -- often due to numerical error" },
	};
#endif
	void Z_Construct_UClass_UCSGMeshesToolProperties_Statics::NewProp_bShowNewBoundaryEdges_SetBit(void* Obj)
	{
		((UCSGMeshesToolProperties*)Obj)->bShowNewBoundaryEdges = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCSGMeshesToolProperties_Statics::NewProp_bShowNewBoundaryEdges = { "bShowNewBoundaryEdges", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCSGMeshesToolProperties), &Z_Construct_UClass_UCSGMeshesToolProperties_Statics::NewProp_bShowNewBoundaryEdges_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCSGMeshesToolProperties_Statics::NewProp_bShowNewBoundaryEdges_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCSGMeshesToolProperties_Statics::NewProp_bShowNewBoundaryEdges_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCSGMeshesToolProperties_Statics::NewProp_bAttemptFixHoles_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Automatically attempt to fill any holes left by CSG (e.g. due to numerical errors) */" },
		{ "ModuleRelativePath", "Public/CSGMeshesTool.h" },
		{ "ToolTip", "Automatically attempt to fill any holes left by CSG (e.g. due to numerical errors)" },
	};
#endif
	void Z_Construct_UClass_UCSGMeshesToolProperties_Statics::NewProp_bAttemptFixHoles_SetBit(void* Obj)
	{
		((UCSGMeshesToolProperties*)Obj)->bAttemptFixHoles = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCSGMeshesToolProperties_Statics::NewProp_bAttemptFixHoles = { "bAttemptFixHoles", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCSGMeshesToolProperties), &Z_Construct_UClass_UCSGMeshesToolProperties_Statics::NewProp_bAttemptFixHoles_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCSGMeshesToolProperties_Statics::NewProp_bAttemptFixHoles_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCSGMeshesToolProperties_Statics::NewProp_bAttemptFixHoles_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCSGMeshesToolProperties_Statics::NewProp_bOnlyUseFirstMeshMaterials_MetaData[] = {
		{ "Category", "Materials" },
		{ "Comment", "/** If true, only the first mesh will keep its materials assignments; all other triangles will be assigned material 0 */" },
		{ "ModuleRelativePath", "Public/CSGMeshesTool.h" },
		{ "ToolTip", "If true, only the first mesh will keep its materials assignments; all other triangles will be assigned material 0" },
	};
#endif
	void Z_Construct_UClass_UCSGMeshesToolProperties_Statics::NewProp_bOnlyUseFirstMeshMaterials_SetBit(void* Obj)
	{
		((UCSGMeshesToolProperties*)Obj)->bOnlyUseFirstMeshMaterials = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCSGMeshesToolProperties_Statics::NewProp_bOnlyUseFirstMeshMaterials = { "bOnlyUseFirstMeshMaterials", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCSGMeshesToolProperties), &Z_Construct_UClass_UCSGMeshesToolProperties_Statics::NewProp_bOnlyUseFirstMeshMaterials_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCSGMeshesToolProperties_Statics::NewProp_bOnlyUseFirstMeshMaterials_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCSGMeshesToolProperties_Statics::NewProp_bOnlyUseFirstMeshMaterials_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCSGMeshesToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCSGMeshesToolProperties_Statics::NewProp_Operation_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCSGMeshesToolProperties_Statics::NewProp_Operation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCSGMeshesToolProperties_Statics::NewProp_bShowNewBoundaryEdges,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCSGMeshesToolProperties_Statics::NewProp_bAttemptFixHoles,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCSGMeshesToolProperties_Statics::NewProp_bOnlyUseFirstMeshMaterials,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCSGMeshesToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCSGMeshesToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCSGMeshesToolProperties_Statics::ClassParams = {
		&UCSGMeshesToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UCSGMeshesToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UCSGMeshesToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UCSGMeshesToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCSGMeshesToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCSGMeshesToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCSGMeshesToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCSGMeshesToolProperties, 3445518524);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UCSGMeshesToolProperties>()
	{
		return UCSGMeshesToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCSGMeshesToolProperties(Z_Construct_UClass_UCSGMeshesToolProperties, &UCSGMeshesToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UCSGMeshesToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCSGMeshesToolProperties);
	void UCSGMeshesTool::StaticRegisterNativesUCSGMeshesTool()
	{
	}
	UClass* Z_Construct_UClass_UCSGMeshesTool_NoRegister()
	{
		return UCSGMeshesTool::StaticClass();
	}
	struct Z_Construct_UClass_UCSGMeshesTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CSGProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CSGProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DrawnLineSet_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DrawnLineSet;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCSGMeshesTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBaseCreateFromSelectedTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCSGMeshesTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Simple Mesh Plane Cutting Tool\n */" },
		{ "IncludePath", "CSGMeshesTool.h" },
		{ "ModuleRelativePath", "Public/CSGMeshesTool.h" },
		{ "ToolTip", "Simple Mesh Plane Cutting Tool" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCSGMeshesTool_Statics::NewProp_CSGProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/CSGMeshesTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UCSGMeshesTool_Statics::NewProp_CSGProperties = { "CSGProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCSGMeshesTool, CSGProperties), Z_Construct_UClass_UCSGMeshesToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCSGMeshesTool_Statics::NewProp_CSGProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCSGMeshesTool_Statics::NewProp_CSGProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCSGMeshesTool_Statics::NewProp_DrawnLineSet_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/CSGMeshesTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UCSGMeshesTool_Statics::NewProp_DrawnLineSet = { "DrawnLineSet", nullptr, (EPropertyFlags)0x0020080000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCSGMeshesTool, DrawnLineSet), Z_Construct_UClass_ULineSetComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCSGMeshesTool_Statics::NewProp_DrawnLineSet_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCSGMeshesTool_Statics::NewProp_DrawnLineSet_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCSGMeshesTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCSGMeshesTool_Statics::NewProp_CSGProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCSGMeshesTool_Statics::NewProp_DrawnLineSet,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCSGMeshesTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCSGMeshesTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCSGMeshesTool_Statics::ClassParams = {
		&UCSGMeshesTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UCSGMeshesTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UCSGMeshesTool_Statics::PropPointers),
		0,
		0x009000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UCSGMeshesTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCSGMeshesTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCSGMeshesTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCSGMeshesTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCSGMeshesTool, 2429794750);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UCSGMeshesTool>()
	{
		return UCSGMeshesTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCSGMeshesTool(Z_Construct_UClass_UCSGMeshesTool, &UCSGMeshesTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UCSGMeshesTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCSGMeshesTool);
	void UCSGMeshesToolBuilder::StaticRegisterNativesUCSGMeshesToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UCSGMeshesToolBuilder_NoRegister()
	{
		return UCSGMeshesToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UCSGMeshesToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCSGMeshesToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBaseCreateFromSelectedToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCSGMeshesToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "CSGMeshesTool.h" },
		{ "ModuleRelativePath", "Public/CSGMeshesTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCSGMeshesToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCSGMeshesToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCSGMeshesToolBuilder_Statics::ClassParams = {
		&UCSGMeshesToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UCSGMeshesToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCSGMeshesToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCSGMeshesToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCSGMeshesToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCSGMeshesToolBuilder, 1050664837);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UCSGMeshesToolBuilder>()
	{
		return UCSGMeshesToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCSGMeshesToolBuilder(Z_Construct_UClass_UCSGMeshesToolBuilder, &UCSGMeshesToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UCSGMeshesToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCSGMeshesToolBuilder);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
