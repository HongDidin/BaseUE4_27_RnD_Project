// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/MeshSpaceDeformerTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMeshSpaceDeformerTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_ENonlinearOperationType();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshSpaceDeformerToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshSpaceDeformerToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UMeshSurfacePointToolBuilder();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_USpaceDeformerOperatorFactory_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_USpaceDeformerOperatorFactory();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshSpaceDeformerTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshSpaceDeformerTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UMeshSurfacePointTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UGizmoTransformChangeStateTarget_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FQuat();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UIntervalGizmo_NoRegister();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UTransformGizmo_NoRegister();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UTransformProxy_NoRegister();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UGizmoLocalFloatParameterSource_NoRegister();
// End Cross Module References
	static UEnum* ENonlinearOperationType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_ENonlinearOperationType, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("ENonlinearOperationType"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<ENonlinearOperationType>()
	{
		return ENonlinearOperationType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ENonlinearOperationType(ENonlinearOperationType_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("ENonlinearOperationType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_ENonlinearOperationType_Hash() { return 1029631883U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_ENonlinearOperationType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ENonlinearOperationType"), 0, Get_Z_Construct_UEnum_MeshModelingTools_ENonlinearOperationType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ENonlinearOperationType::Bend", (int64)ENonlinearOperationType::Bend },
				{ "ENonlinearOperationType::Flare", (int64)ENonlinearOperationType::Flare },
				{ "ENonlinearOperationType::Twist", (int64)ENonlinearOperationType::Twist },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Bend.DisplayName", "Bend" },
				{ "Bend.Name", "ENonlinearOperationType::Bend" },
				{ "Comment", "/** ENonlinearOperation determines which type of nonlinear deformation will be applied*/" },
				{ "Flare.DisplayName", "Flare" },
				{ "Flare.Name", "ENonlinearOperationType::Flare" },
				{ "ModuleRelativePath", "Public/MeshSpaceDeformerTool.h" },
				{ "ToolTip", "ENonlinearOperation determines which type of nonlinear deformation will be applied" },
				{ "Twist.DisplayName", "Twist" },
				{ "Twist.Name", "ENonlinearOperationType::Twist" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"ENonlinearOperationType",
				"ENonlinearOperationType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UMeshSpaceDeformerToolBuilder::StaticRegisterNativesUMeshSpaceDeformerToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UMeshSpaceDeformerToolBuilder_NoRegister()
	{
		return UMeshSpaceDeformerToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UMeshSpaceDeformerToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshSpaceDeformerToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshSurfacePointToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSpaceDeformerToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * ToolBuilder\n */" },
		{ "IncludePath", "MeshSpaceDeformerTool.h" },
		{ "ModuleRelativePath", "Public/MeshSpaceDeformerTool.h" },
		{ "ToolTip", "ToolBuilder" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshSpaceDeformerToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshSpaceDeformerToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshSpaceDeformerToolBuilder_Statics::ClassParams = {
		&UMeshSpaceDeformerToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshSpaceDeformerToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSpaceDeformerToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshSpaceDeformerToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshSpaceDeformerToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshSpaceDeformerToolBuilder, 4264178060);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UMeshSpaceDeformerToolBuilder>()
	{
		return UMeshSpaceDeformerToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshSpaceDeformerToolBuilder(Z_Construct_UClass_UMeshSpaceDeformerToolBuilder, &UMeshSpaceDeformerToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UMeshSpaceDeformerToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshSpaceDeformerToolBuilder);
	void USpaceDeformerOperatorFactory::StaticRegisterNativesUSpaceDeformerOperatorFactory()
	{
	}
	UClass* Z_Construct_UClass_USpaceDeformerOperatorFactory_NoRegister()
	{
		return USpaceDeformerOperatorFactory::StaticClass();
	}
	struct Z_Construct_UClass_USpaceDeformerOperatorFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpaceDeformerTool_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SpaceDeformerTool;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USpaceDeformerOperatorFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USpaceDeformerOperatorFactory_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshSpaceDeformerTool.h" },
		{ "ModuleRelativePath", "Public/MeshSpaceDeformerTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USpaceDeformerOperatorFactory_Statics::NewProp_SpaceDeformerTool_MetaData[] = {
		{ "ModuleRelativePath", "Public/MeshSpaceDeformerTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USpaceDeformerOperatorFactory_Statics::NewProp_SpaceDeformerTool = { "SpaceDeformerTool", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USpaceDeformerOperatorFactory, SpaceDeformerTool), Z_Construct_UClass_UMeshSpaceDeformerTool_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USpaceDeformerOperatorFactory_Statics::NewProp_SpaceDeformerTool_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USpaceDeformerOperatorFactory_Statics::NewProp_SpaceDeformerTool_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USpaceDeformerOperatorFactory_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USpaceDeformerOperatorFactory_Statics::NewProp_SpaceDeformerTool,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USpaceDeformerOperatorFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USpaceDeformerOperatorFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USpaceDeformerOperatorFactory_Statics::ClassParams = {
		&USpaceDeformerOperatorFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USpaceDeformerOperatorFactory_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USpaceDeformerOperatorFactory_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_USpaceDeformerOperatorFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USpaceDeformerOperatorFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USpaceDeformerOperatorFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USpaceDeformerOperatorFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USpaceDeformerOperatorFactory, 631691920);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<USpaceDeformerOperatorFactory>()
	{
		return USpaceDeformerOperatorFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USpaceDeformerOperatorFactory(Z_Construct_UClass_USpaceDeformerOperatorFactory, &USpaceDeformerOperatorFactory::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("USpaceDeformerOperatorFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USpaceDeformerOperatorFactory);
	void UMeshSpaceDeformerTool::StaticRegisterNativesUMeshSpaceDeformerTool()
	{
	}
	UClass* Z_Construct_UClass_UMeshSpaceDeformerTool_NoRegister()
	{
		return UMeshSpaceDeformerTool::StaticClass();
	}
	struct Z_Construct_UClass_UMeshSpaceDeformerTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FInt8PropertyParams NewProp_SelectedOperationType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SelectedOperationType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_SelectedOperationType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UpperBoundsInterval_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_UpperBoundsInterval;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LowerBoundsInterval_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_LowerBoundsInterval;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ModifierPercent_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ModifierPercent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSnapToWorldGrid_MetaData[];
#endif
		static void NewProp_bSnapToWorldGrid_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSnapToWorldGrid;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StateTarget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StateTarget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Preview_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Preview;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GizmoCenter_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GizmoCenter;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GizmoOrientation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GizmoOrientation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IntervalGizmo_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_IntervalGizmo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransformGizmo_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TransformGizmo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransformProxy_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TransformProxy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UpIntervalSource_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_UpIntervalSource;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DownIntervalSource_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DownIntervalSource;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ForwardIntervalSource_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ForwardIntervalSource;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshSurfacePointTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Applies non-linear deformations to a mesh \n */" },
		{ "IncludePath", "MeshSpaceDeformerTool.h" },
		{ "ModuleRelativePath", "Public/MeshSpaceDeformerTool.h" },
		{ "ToolTip", "Applies non-linear deformations to a mesh" },
	};
#endif
	const UE4CodeGen_Private::FInt8PropertyParams Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_SelectedOperationType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int8, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_SelectedOperationType_MetaData[] = {
		{ "Category", "Options" },
		{ "DisplayName", "Operation Type" },
		{ "ModuleRelativePath", "Public/MeshSpaceDeformerTool.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_SelectedOperationType = { "SelectedOperationType", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshSpaceDeformerTool, SelectedOperationType), Z_Construct_UEnum_MeshModelingTools_ENonlinearOperationType, METADATA_PARAMS(Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_SelectedOperationType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_SelectedOperationType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_UpperBoundsInterval_MetaData[] = {
		{ "Category", "Options" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** The upper bounds interval corresponds to the region of space which the selected operator will affect. A setting of 1.0 should envelope all points in the \"upper\" half of the mesh given the axis has been auto-detected. The corresponding lower value of -1 will cover the entire mesh. */" },
		{ "DisplayName", "Upper Bound" },
		{ "ModuleRelativePath", "Public/MeshSpaceDeformerTool.h" },
		{ "ToolTip", "The upper bounds interval corresponds to the region of space which the selected operator will affect. A setting of 1.0 should envelope all points in the \"upper\" half of the mesh given the axis has been auto-detected. The corresponding lower value of -1 will cover the entire mesh." },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_UpperBoundsInterval = { "UpperBoundsInterval", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshSpaceDeformerTool, UpperBoundsInterval), METADATA_PARAMS(Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_UpperBoundsInterval_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_UpperBoundsInterval_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_LowerBoundsInterval_MetaData[] = {
		{ "Category", "Options" },
		{ "ClampMax", "0" },
		{ "Comment", "/** The upper bounds interval corresponds to the region of space which the selected operator will affect. A setting of -1.0 should envelope all points in the \"lower\" half of the mesh given the axis has been auto-detected. The corresponding upper value of 1 will cover the entire mesh. */" },
		{ "DisplayName", "Lower Bound" },
		{ "ModuleRelativePath", "Public/MeshSpaceDeformerTool.h" },
		{ "ToolTip", "The upper bounds interval corresponds to the region of space which the selected operator will affect. A setting of -1.0 should envelope all points in the \"lower\" half of the mesh given the axis has been auto-detected. The corresponding upper value of 1 will cover the entire mesh." },
		{ "UIMax", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_LowerBoundsInterval = { "LowerBoundsInterval", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshSpaceDeformerTool, LowerBoundsInterval), METADATA_PARAMS(Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_LowerBoundsInterval_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_LowerBoundsInterval_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_ModifierPercent_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** As each operator has a range of values (i.e. curvature, angle of twist, scale), this represents the percentage passed to the operator as a parameter. In the future, for more control, this should be separated into individual settings for each operator for more precise control */" },
		{ "DisplayName", "Modifier Percent" },
		{ "ModuleRelativePath", "Public/MeshSpaceDeformerTool.h" },
		{ "ToolTip", "As each operator has a range of values (i.e. curvature, angle of twist, scale), this represents the percentage passed to the operator as a parameter. In the future, for more control, this should be separated into individual settings for each operator for more precise control" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_ModifierPercent = { "ModifierPercent", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshSpaceDeformerTool, ModifierPercent), METADATA_PARAMS(Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_ModifierPercent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_ModifierPercent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_bSnapToWorldGrid_MetaData[] = {
		{ "Category", "Snapping" },
		{ "Comment", "/** Snap the deformer gizmo to the world grid */" },
		{ "ModuleRelativePath", "Public/MeshSpaceDeformerTool.h" },
		{ "ToolTip", "Snap the deformer gizmo to the world grid" },
	};
#endif
	void Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_bSnapToWorldGrid_SetBit(void* Obj)
	{
		((UMeshSpaceDeformerTool*)Obj)->bSnapToWorldGrid = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_bSnapToWorldGrid = { "bSnapToWorldGrid", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshSpaceDeformerTool), &Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_bSnapToWorldGrid_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_bSnapToWorldGrid_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_bSnapToWorldGrid_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_StateTarget_MetaData[] = {
		{ "ModuleRelativePath", "Public/MeshSpaceDeformerTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_StateTarget = { "StateTarget", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshSpaceDeformerTool, StateTarget), Z_Construct_UClass_UGizmoTransformChangeStateTarget_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_StateTarget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_StateTarget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_Preview_MetaData[] = {
		{ "ModuleRelativePath", "Public/MeshSpaceDeformerTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_Preview = { "Preview", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshSpaceDeformerTool, Preview), Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_Preview_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_Preview_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_GizmoCenter_MetaData[] = {
		{ "Comment", "/** offset to center of gizmo*/" },
		{ "ModuleRelativePath", "Public/MeshSpaceDeformerTool.h" },
		{ "ToolTip", "offset to center of gizmo" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_GizmoCenter = { "GizmoCenter", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshSpaceDeformerTool, GizmoCenter), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_GizmoCenter_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_GizmoCenter_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_GizmoOrientation_MetaData[] = {
		{ "Comment", "/** Gizmo Plane Orientation */" },
		{ "ModuleRelativePath", "Public/MeshSpaceDeformerTool.h" },
		{ "ToolTip", "Gizmo Plane Orientation" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_GizmoOrientation = { "GizmoOrientation", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshSpaceDeformerTool, GizmoOrientation), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_GizmoOrientation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_GizmoOrientation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_IntervalGizmo_MetaData[] = {
		{ "ModuleRelativePath", "Public/MeshSpaceDeformerTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_IntervalGizmo = { "IntervalGizmo", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshSpaceDeformerTool, IntervalGizmo), Z_Construct_UClass_UIntervalGizmo_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_IntervalGizmo_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_IntervalGizmo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_TransformGizmo_MetaData[] = {
		{ "ModuleRelativePath", "Public/MeshSpaceDeformerTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_TransformGizmo = { "TransformGizmo", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshSpaceDeformerTool, TransformGizmo), Z_Construct_UClass_UTransformGizmo_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_TransformGizmo_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_TransformGizmo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_TransformProxy_MetaData[] = {
		{ "ModuleRelativePath", "Public/MeshSpaceDeformerTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_TransformProxy = { "TransformProxy", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshSpaceDeformerTool, TransformProxy), Z_Construct_UClass_UTransformProxy_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_TransformProxy_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_TransformProxy_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_UpIntervalSource_MetaData[] = {
		{ "Comment", "/** Interval Parameter sources that reflect UI settings. */" },
		{ "ModuleRelativePath", "Public/MeshSpaceDeformerTool.h" },
		{ "ToolTip", "Interval Parameter sources that reflect UI settings." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_UpIntervalSource = { "UpIntervalSource", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshSpaceDeformerTool, UpIntervalSource), Z_Construct_UClass_UGizmoLocalFloatParameterSource_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_UpIntervalSource_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_UpIntervalSource_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_DownIntervalSource_MetaData[] = {
		{ "ModuleRelativePath", "Public/MeshSpaceDeformerTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_DownIntervalSource = { "DownIntervalSource", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshSpaceDeformerTool, DownIntervalSource), Z_Construct_UClass_UGizmoLocalFloatParameterSource_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_DownIntervalSource_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_DownIntervalSource_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_ForwardIntervalSource_MetaData[] = {
		{ "ModuleRelativePath", "Public/MeshSpaceDeformerTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_ForwardIntervalSource = { "ForwardIntervalSource", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshSpaceDeformerTool, ForwardIntervalSource), Z_Construct_UClass_UGizmoLocalFloatParameterSource_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_ForwardIntervalSource_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_ForwardIntervalSource_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_SelectedOperationType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_SelectedOperationType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_UpperBoundsInterval,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_LowerBoundsInterval,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_ModifierPercent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_bSnapToWorldGrid,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_StateTarget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_Preview,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_GizmoCenter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_GizmoOrientation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_IntervalGizmo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_TransformGizmo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_TransformProxy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_UpIntervalSource,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_DownIntervalSource,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::NewProp_ForwardIntervalSource,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshSpaceDeformerTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::ClassParams = {
		&UMeshSpaceDeformerTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshSpaceDeformerTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshSpaceDeformerTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshSpaceDeformerTool, 1914749071);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UMeshSpaceDeformerTool>()
	{
		return UMeshSpaceDeformerTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshSpaceDeformerTool(Z_Construct_UClass_UMeshSpaceDeformerTool, &UMeshSpaceDeformerTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UMeshSpaceDeformerTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshSpaceDeformerTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
