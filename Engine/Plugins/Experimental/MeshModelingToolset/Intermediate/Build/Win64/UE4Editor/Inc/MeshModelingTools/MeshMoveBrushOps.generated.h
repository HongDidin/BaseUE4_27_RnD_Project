// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLS_MeshMoveBrushOps_generated_h
#error "MeshMoveBrushOps.generated.h already included, missing '#pragma once' in MeshMoveBrushOps.h"
#endif
#define MESHMODELINGTOOLS_MeshMoveBrushOps_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshMoveBrushOps_h_16_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshMoveBrushOps_h_16_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshMoveBrushOps_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshMoveBrushOps_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMoveBrushOpProps(); \
	friend struct Z_Construct_UClass_UMoveBrushOpProps_Statics; \
public: \
	DECLARE_CLASS(UMoveBrushOpProps, UMeshSculptBrushOpProps, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMoveBrushOpProps)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshMoveBrushOps_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUMoveBrushOpProps(); \
	friend struct Z_Construct_UClass_UMoveBrushOpProps_Statics; \
public: \
	DECLARE_CLASS(UMoveBrushOpProps, UMeshSculptBrushOpProps, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMoveBrushOpProps)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshMoveBrushOps_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMoveBrushOpProps(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMoveBrushOpProps) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoveBrushOpProps); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoveBrushOpProps); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoveBrushOpProps(UMoveBrushOpProps&&); \
	NO_API UMoveBrushOpProps(const UMoveBrushOpProps&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshMoveBrushOps_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMoveBrushOpProps(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoveBrushOpProps(UMoveBrushOpProps&&); \
	NO_API UMoveBrushOpProps(const UMoveBrushOpProps&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoveBrushOpProps); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoveBrushOpProps); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMoveBrushOpProps)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshMoveBrushOps_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshMoveBrushOps_h_13_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshMoveBrushOps_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshMoveBrushOps_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshMoveBrushOps_h_16_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshMoveBrushOps_h_16_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshMoveBrushOps_h_16_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshMoveBrushOps_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshMoveBrushOps_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshMoveBrushOps_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshMoveBrushOps_h_16_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshMoveBrushOps_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshMoveBrushOps_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshMoveBrushOps_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UMoveBrushOpProps>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshMoveBrushOps_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
