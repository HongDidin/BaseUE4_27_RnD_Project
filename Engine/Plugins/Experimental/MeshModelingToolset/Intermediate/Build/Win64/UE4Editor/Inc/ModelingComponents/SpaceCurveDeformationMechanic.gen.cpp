// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ModelingComponents/Public/Mechanics/SpaceCurveDeformationMechanic.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSpaceCurveDeformationMechanic() {}
// Cross Module References
	MODELINGCOMPONENTS_API UEnum* Z_Construct_UEnum_ModelingComponents_ESpaceCurveControlPointFalloffType();
	UPackage* Z_Construct_UPackage__Script_ModelingComponents();
	MODELINGCOMPONENTS_API UEnum* Z_Construct_UEnum_ModelingComponents_ESpaceCurveControlPointOriginMode();
	MODELINGCOMPONENTS_API UEnum* Z_Construct_UEnum_ModelingComponents_ESpaceCurveControlPointTransformMode();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_USpaceCurveDeformationMechanic_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_USpaceCurveDeformationMechanic();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractionMechanic();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_USingleClickInputBehavior_NoRegister();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UMouseHoverBehavior_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_APreviewGeometryActor_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPointSetComponent_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_ULineSetComponent_NoRegister();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UTransformProxy_NoRegister();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UTransformGizmo_NoRegister();
// End Cross Module References
	static UEnum* ESpaceCurveControlPointFalloffType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ModelingComponents_ESpaceCurveControlPointFalloffType, Z_Construct_UPackage__Script_ModelingComponents(), TEXT("ESpaceCurveControlPointFalloffType"));
		}
		return Singleton;
	}
	template<> MODELINGCOMPONENTS_API UEnum* StaticEnum<ESpaceCurveControlPointFalloffType>()
	{
		return ESpaceCurveControlPointFalloffType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ESpaceCurveControlPointFalloffType(ESpaceCurveControlPointFalloffType_StaticEnum, TEXT("/Script/ModelingComponents"), TEXT("ESpaceCurveControlPointFalloffType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ModelingComponents_ESpaceCurveControlPointFalloffType_Hash() { return 4210110480U; }
	UEnum* Z_Construct_UEnum_ModelingComponents_ESpaceCurveControlPointFalloffType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ModelingComponents();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ESpaceCurveControlPointFalloffType"), 0, Get_Z_Construct_UEnum_ModelingComponents_ESpaceCurveControlPointFalloffType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ESpaceCurveControlPointFalloffType::Linear", (int64)ESpaceCurveControlPointFalloffType::Linear },
				{ "ESpaceCurveControlPointFalloffType::Smooth", (int64)ESpaceCurveControlPointFalloffType::Smooth },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Linear.Name", "ESpaceCurveControlPointFalloffType::Linear" },
				{ "ModuleRelativePath", "Public/Mechanics/SpaceCurveDeformationMechanic.h" },
				{ "Smooth.Name", "ESpaceCurveControlPointFalloffType::Smooth" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ModelingComponents,
				nullptr,
				"ESpaceCurveControlPointFalloffType",
				"ESpaceCurveControlPointFalloffType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ESpaceCurveControlPointOriginMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ModelingComponents_ESpaceCurveControlPointOriginMode, Z_Construct_UPackage__Script_ModelingComponents(), TEXT("ESpaceCurveControlPointOriginMode"));
		}
		return Singleton;
	}
	template<> MODELINGCOMPONENTS_API UEnum* StaticEnum<ESpaceCurveControlPointOriginMode>()
	{
		return ESpaceCurveControlPointOriginMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ESpaceCurveControlPointOriginMode(ESpaceCurveControlPointOriginMode_StaticEnum, TEXT("/Script/ModelingComponents"), TEXT("ESpaceCurveControlPointOriginMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ModelingComponents_ESpaceCurveControlPointOriginMode_Hash() { return 1859233439U; }
	UEnum* Z_Construct_UEnum_ModelingComponents_ESpaceCurveControlPointOriginMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ModelingComponents();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ESpaceCurveControlPointOriginMode"), 0, Get_Z_Construct_UEnum_ModelingComponents_ESpaceCurveControlPointOriginMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ESpaceCurveControlPointOriginMode::Shared", (int64)ESpaceCurveControlPointOriginMode::Shared },
				{ "ESpaceCurveControlPointOriginMode::First", (int64)ESpaceCurveControlPointOriginMode::First },
				{ "ESpaceCurveControlPointOriginMode::Last", (int64)ESpaceCurveControlPointOriginMode::Last },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "First.Name", "ESpaceCurveControlPointOriginMode::First" },
				{ "Last.Name", "ESpaceCurveControlPointOriginMode::Last" },
				{ "ModuleRelativePath", "Public/Mechanics/SpaceCurveDeformationMechanic.h" },
				{ "Shared.Name", "ESpaceCurveControlPointOriginMode::Shared" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ModelingComponents,
				nullptr,
				"ESpaceCurveControlPointOriginMode",
				"ESpaceCurveControlPointOriginMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ESpaceCurveControlPointTransformMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ModelingComponents_ESpaceCurveControlPointTransformMode, Z_Construct_UPackage__Script_ModelingComponents(), TEXT("ESpaceCurveControlPointTransformMode"));
		}
		return Singleton;
	}
	template<> MODELINGCOMPONENTS_API UEnum* StaticEnum<ESpaceCurveControlPointTransformMode>()
	{
		return ESpaceCurveControlPointTransformMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ESpaceCurveControlPointTransformMode(ESpaceCurveControlPointTransformMode_StaticEnum, TEXT("/Script/ModelingComponents"), TEXT("ESpaceCurveControlPointTransformMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ModelingComponents_ESpaceCurveControlPointTransformMode_Hash() { return 4207906905U; }
	UEnum* Z_Construct_UEnum_ModelingComponents_ESpaceCurveControlPointTransformMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ModelingComponents();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ESpaceCurveControlPointTransformMode"), 0, Get_Z_Construct_UEnum_ModelingComponents_ESpaceCurveControlPointTransformMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ESpaceCurveControlPointTransformMode::Shared", (int64)ESpaceCurveControlPointTransformMode::Shared },
				{ "ESpaceCurveControlPointTransformMode::PerVertex", (int64)ESpaceCurveControlPointTransformMode::PerVertex },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/Mechanics/SpaceCurveDeformationMechanic.h" },
				{ "PerVertex.Name", "ESpaceCurveControlPointTransformMode::PerVertex" },
				{ "Shared.Name", "ESpaceCurveControlPointTransformMode::Shared" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ModelingComponents,
				nullptr,
				"ESpaceCurveControlPointTransformMode",
				"ESpaceCurveControlPointTransformMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void USpaceCurveDeformationMechanicPropertySet::StaticRegisterNativesUSpaceCurveDeformationMechanicPropertySet()
	{
	}
	UClass* Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_NoRegister()
	{
		return USpaceCurveDeformationMechanicPropertySet::StaticClass();
	}
	struct Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_TransformMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransformMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TransformMode;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_TransformOrigin_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransformOrigin_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TransformOrigin;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Softness_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Softness;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_SoftFalloff_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SoftFalloff_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_SoftFalloff;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_ModelingComponents,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Mechanics/SpaceCurveDeformationMechanic.h" },
		{ "ModuleRelativePath", "Public/Mechanics/SpaceCurveDeformationMechanic.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_Statics::NewProp_TransformMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_Statics::NewProp_TransformMode_MetaData[] = {
		{ "Category", "Options" },
		{ "ModuleRelativePath", "Public/Mechanics/SpaceCurveDeformationMechanic.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_Statics::NewProp_TransformMode = { "TransformMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USpaceCurveDeformationMechanicPropertySet, TransformMode), Z_Construct_UEnum_ModelingComponents_ESpaceCurveControlPointTransformMode, METADATA_PARAMS(Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_Statics::NewProp_TransformMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_Statics::NewProp_TransformMode_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_Statics::NewProp_TransformOrigin_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_Statics::NewProp_TransformOrigin_MetaData[] = {
		{ "Category", "Options" },
		{ "ModuleRelativePath", "Public/Mechanics/SpaceCurveDeformationMechanic.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_Statics::NewProp_TransformOrigin = { "TransformOrigin", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USpaceCurveDeformationMechanicPropertySet, TransformOrigin), Z_Construct_UEnum_ModelingComponents_ESpaceCurveControlPointOriginMode, METADATA_PARAMS(Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_Statics::NewProp_TransformOrigin_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_Statics::NewProp_TransformOrigin_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_Statics::NewProp_Softness_MetaData[] = {
		{ "Category", "Options" },
		{ "ModuleRelativePath", "Public/Mechanics/SpaceCurveDeformationMechanic.h" },
		{ "UIMax", "1" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_Statics::NewProp_Softness = { "Softness", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USpaceCurveDeformationMechanicPropertySet, Softness), METADATA_PARAMS(Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_Statics::NewProp_Softness_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_Statics::NewProp_Softness_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_Statics::NewProp_SoftFalloff_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_Statics::NewProp_SoftFalloff_MetaData[] = {
		{ "Category", "Options" },
		{ "ModuleRelativePath", "Public/Mechanics/SpaceCurveDeformationMechanic.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_Statics::NewProp_SoftFalloff = { "SoftFalloff", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USpaceCurveDeformationMechanicPropertySet, SoftFalloff), Z_Construct_UEnum_ModelingComponents_ESpaceCurveControlPointFalloffType, METADATA_PARAMS(Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_Statics::NewProp_SoftFalloff_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_Statics::NewProp_SoftFalloff_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_Statics::NewProp_TransformMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_Statics::NewProp_TransformMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_Statics::NewProp_TransformOrigin_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_Statics::NewProp_TransformOrigin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_Statics::NewProp_Softness,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_Statics::NewProp_SoftFalloff_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_Statics::NewProp_SoftFalloff,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USpaceCurveDeformationMechanicPropertySet>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_Statics::ClassParams = {
		&USpaceCurveDeformationMechanicPropertySet::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USpaceCurveDeformationMechanicPropertySet, 4104628121);
	template<> MODELINGCOMPONENTS_API UClass* StaticClass<USpaceCurveDeformationMechanicPropertySet>()
	{
		return USpaceCurveDeformationMechanicPropertySet::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USpaceCurveDeformationMechanicPropertySet(Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet, &USpaceCurveDeformationMechanicPropertySet::StaticClass, TEXT("/Script/ModelingComponents"), TEXT("USpaceCurveDeformationMechanicPropertySet"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USpaceCurveDeformationMechanicPropertySet);
	void USpaceCurveDeformationMechanic::StaticRegisterNativesUSpaceCurveDeformationMechanic()
	{
	}
	UClass* Z_Construct_UClass_USpaceCurveDeformationMechanic_NoRegister()
	{
		return USpaceCurveDeformationMechanic::StaticClass();
	}
	struct Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClickBehavior_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ClickBehavior;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HoverBehavior_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_HoverBehavior;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransformProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TransformProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviewGeometryActor_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviewGeometryActor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RenderPoints_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RenderPoints;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RenderSegments_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RenderSegments;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PointTransformProxy_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PointTransformProxy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PointTransformGizmo_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PointTransformGizmo;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractionMechanic,
		(UObject* (*)())Z_Construct_UPackage__Script_ModelingComponents,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n\n */" },
		{ "IncludePath", "Mechanics/SpaceCurveDeformationMechanic.h" },
		{ "ModuleRelativePath", "Public/Mechanics/SpaceCurveDeformationMechanic.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::NewProp_ClickBehavior_MetaData[] = {
		{ "Comment", "// Behaviors used for moving points around and hovering them\n" },
		{ "ModuleRelativePath", "Public/Mechanics/SpaceCurveDeformationMechanic.h" },
		{ "ToolTip", "Behaviors used for moving points around and hovering them" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::NewProp_ClickBehavior = { "ClickBehavior", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USpaceCurveDeformationMechanic, ClickBehavior), Z_Construct_UClass_USingleClickInputBehavior_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::NewProp_ClickBehavior_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::NewProp_ClickBehavior_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::NewProp_HoverBehavior_MetaData[] = {
		{ "ModuleRelativePath", "Public/Mechanics/SpaceCurveDeformationMechanic.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::NewProp_HoverBehavior = { "HoverBehavior", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USpaceCurveDeformationMechanic, HoverBehavior), Z_Construct_UClass_UMouseHoverBehavior_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::NewProp_HoverBehavior_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::NewProp_HoverBehavior_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::NewProp_TransformProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/Mechanics/SpaceCurveDeformationMechanic.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::NewProp_TransformProperties = { "TransformProperties", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USpaceCurveDeformationMechanic, TransformProperties), Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::NewProp_TransformProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::NewProp_TransformProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::NewProp_PreviewGeometryActor_MetaData[] = {
		{ "Comment", "/** Used for displaying points/segments */" },
		{ "ModuleRelativePath", "Public/Mechanics/SpaceCurveDeformationMechanic.h" },
		{ "ToolTip", "Used for displaying points/segments" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::NewProp_PreviewGeometryActor = { "PreviewGeometryActor", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USpaceCurveDeformationMechanic, PreviewGeometryActor), Z_Construct_UClass_APreviewGeometryActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::NewProp_PreviewGeometryActor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::NewProp_PreviewGeometryActor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::NewProp_RenderPoints_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Mechanics/SpaceCurveDeformationMechanic.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::NewProp_RenderPoints = { "RenderPoints", nullptr, (EPropertyFlags)0x0020080000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USpaceCurveDeformationMechanic, RenderPoints), Z_Construct_UClass_UPointSetComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::NewProp_RenderPoints_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::NewProp_RenderPoints_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::NewProp_RenderSegments_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Mechanics/SpaceCurveDeformationMechanic.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::NewProp_RenderSegments = { "RenderSegments", nullptr, (EPropertyFlags)0x0020080000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USpaceCurveDeformationMechanic, RenderSegments), Z_Construct_UClass_ULineSetComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::NewProp_RenderSegments_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::NewProp_RenderSegments_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::NewProp_PointTransformProxy_MetaData[] = {
		{ "Comment", "// Support for gizmo. Since the points aren't individual components, we don't actually use UTransformProxy\n// for the transform forwarding- we just use it for the callbacks.\n" },
		{ "ModuleRelativePath", "Public/Mechanics/SpaceCurveDeformationMechanic.h" },
		{ "ToolTip", "Support for gizmo. Since the points aren't individual components, we don't actually use UTransformProxy\nfor the transform forwarding- we just use it for the callbacks." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::NewProp_PointTransformProxy = { "PointTransformProxy", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USpaceCurveDeformationMechanic, PointTransformProxy), Z_Construct_UClass_UTransformProxy_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::NewProp_PointTransformProxy_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::NewProp_PointTransformProxy_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::NewProp_PointTransformGizmo_MetaData[] = {
		{ "ModuleRelativePath", "Public/Mechanics/SpaceCurveDeformationMechanic.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::NewProp_PointTransformGizmo = { "PointTransformGizmo", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USpaceCurveDeformationMechanic, PointTransformGizmo), Z_Construct_UClass_UTransformGizmo_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::NewProp_PointTransformGizmo_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::NewProp_PointTransformGizmo_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::NewProp_ClickBehavior,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::NewProp_HoverBehavior,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::NewProp_TransformProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::NewProp_PreviewGeometryActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::NewProp_RenderPoints,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::NewProp_RenderSegments,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::NewProp_PointTransformProxy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::NewProp_PointTransformGizmo,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USpaceCurveDeformationMechanic>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::ClassParams = {
		&USpaceCurveDeformationMechanic::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::PropPointers),
		0,
		0x009000A8u,
		METADATA_PARAMS(Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USpaceCurveDeformationMechanic()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USpaceCurveDeformationMechanic, 1745486589);
	template<> MODELINGCOMPONENTS_API UClass* StaticClass<USpaceCurveDeformationMechanic>()
	{
		return USpaceCurveDeformationMechanic::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USpaceCurveDeformationMechanic(Z_Construct_UClass_USpaceCurveDeformationMechanic, &USpaceCurveDeformationMechanic::StaticClass, TEXT("/Script/ModelingComponents"), TEXT("USpaceCurveDeformationMechanic"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USpaceCurveDeformationMechanic);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
