// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/OffsetMeshTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOffsetMeshTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EOffsetMeshToolOffsetType();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UOffsetMeshToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UOffsetMeshToolProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UOffsetWeightMapSetProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UOffsetWeightMapSetProperties();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UWeightMapSetProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UIterativeOffsetProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UIterativeOffsetProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UImplicitOffsetProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UImplicitOffsetProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UOffsetMeshTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UOffsetMeshTool();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UBaseMeshProcessingTool();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UOffsetMeshToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UOffsetMeshToolBuilder();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UBaseMeshProcessingToolBuilder();
// End Cross Module References
	static UEnum* EOffsetMeshToolOffsetType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EOffsetMeshToolOffsetType, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EOffsetMeshToolOffsetType"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EOffsetMeshToolOffsetType>()
	{
		return EOffsetMeshToolOffsetType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EOffsetMeshToolOffsetType(EOffsetMeshToolOffsetType_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EOffsetMeshToolOffsetType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EOffsetMeshToolOffsetType_Hash() { return 1289806348U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EOffsetMeshToolOffsetType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EOffsetMeshToolOffsetType"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EOffsetMeshToolOffsetType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EOffsetMeshToolOffsetType::Iterative", (int64)EOffsetMeshToolOffsetType::Iterative },
				{ "EOffsetMeshToolOffsetType::Implicit", (int64)EOffsetMeshToolOffsetType::Implicit },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Implicit.Comment", "/** Implicit Offseting, produces smoother output and does a better job at preserving UVs, but can be very slow on large meshes */" },
				{ "Implicit.DisplayName", "Implicit" },
				{ "Implicit.Name", "EOffsetMeshToolOffsetType::Implicit" },
				{ "Implicit.ToolTip", "Implicit Offseting, produces smoother output and does a better job at preserving UVs, but can be very slow on large meshes" },
				{ "Iterative.Comment", "/** Iterative Offseting with N iterations */" },
				{ "Iterative.DisplayName", "Iterative" },
				{ "Iterative.Name", "EOffsetMeshToolOffsetType::Iterative" },
				{ "Iterative.ToolTip", "Iterative Offseting with N iterations" },
				{ "ModuleRelativePath", "Public/OffsetMeshTool.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EOffsetMeshToolOffsetType",
				"EOffsetMeshToolOffsetType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UOffsetMeshToolProperties::StaticRegisterNativesUOffsetMeshToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UOffsetMeshToolProperties_NoRegister()
	{
		return UOffsetMeshToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UOffsetMeshToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_OffsetType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OffsetType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_OffsetType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Distance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Distance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCreateShell_MetaData[];
#endif
		static void NewProp_bCreateShell_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCreateShell;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOffsetMeshToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOffsetMeshToolProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Base properties of Offset */" },
		{ "IncludePath", "OffsetMeshTool.h" },
		{ "ModuleRelativePath", "Public/OffsetMeshTool.h" },
		{ "ToolTip", "Base properties of Offset" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UOffsetMeshToolProperties_Statics::NewProp_OffsetType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOffsetMeshToolProperties_Statics::NewProp_OffsetType_MetaData[] = {
		{ "Category", "Offset" },
		{ "Comment", "/** Type of Offseting to apply */" },
		{ "ModuleRelativePath", "Public/OffsetMeshTool.h" },
		{ "ToolTip", "Type of Offseting to apply" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UOffsetMeshToolProperties_Statics::NewProp_OffsetType = { "OffsetType", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOffsetMeshToolProperties, OffsetType), Z_Construct_UEnum_MeshModelingTools_EOffsetMeshToolOffsetType, METADATA_PARAMS(Z_Construct_UClass_UOffsetMeshToolProperties_Statics::NewProp_OffsetType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOffsetMeshToolProperties_Statics::NewProp_OffsetType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOffsetMeshToolProperties_Statics::NewProp_Distance_MetaData[] = {
		{ "Category", "Offset" },
		{ "ClampMax", "100000.0" },
		{ "ClampMin", "-10000.0" },
		{ "Comment", "/** Offset Distance in World Units */" },
		{ "ModuleRelativePath", "Public/OffsetMeshTool.h" },
		{ "ToolTip", "Offset Distance in World Units" },
		{ "UIMax", "100.0" },
		{ "UIMin", "-100.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UOffsetMeshToolProperties_Statics::NewProp_Distance = { "Distance", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOffsetMeshToolProperties, Distance), METADATA_PARAMS(Z_Construct_UClass_UOffsetMeshToolProperties_Statics::NewProp_Distance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOffsetMeshToolProperties_Statics::NewProp_Distance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOffsetMeshToolProperties_Statics::NewProp_bCreateShell_MetaData[] = {
		{ "Category", "Offset" },
		{ "Comment", "/** If true, create a thickened shell, instead of only moving the input vertices */" },
		{ "EditCondition", "OffsetType == EOffsetMeshToolOffsetType::Iterative" },
		{ "ModuleRelativePath", "Public/OffsetMeshTool.h" },
		{ "ToolTip", "If true, create a thickened shell, instead of only moving the input vertices" },
	};
#endif
	void Z_Construct_UClass_UOffsetMeshToolProperties_Statics::NewProp_bCreateShell_SetBit(void* Obj)
	{
		((UOffsetMeshToolProperties*)Obj)->bCreateShell = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UOffsetMeshToolProperties_Statics::NewProp_bCreateShell = { "bCreateShell", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UOffsetMeshToolProperties), &Z_Construct_UClass_UOffsetMeshToolProperties_Statics::NewProp_bCreateShell_SetBit, METADATA_PARAMS(Z_Construct_UClass_UOffsetMeshToolProperties_Statics::NewProp_bCreateShell_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOffsetMeshToolProperties_Statics::NewProp_bCreateShell_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UOffsetMeshToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOffsetMeshToolProperties_Statics::NewProp_OffsetType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOffsetMeshToolProperties_Statics::NewProp_OffsetType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOffsetMeshToolProperties_Statics::NewProp_Distance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOffsetMeshToolProperties_Statics::NewProp_bCreateShell,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOffsetMeshToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOffsetMeshToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOffsetMeshToolProperties_Statics::ClassParams = {
		&UOffsetMeshToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UOffsetMeshToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UOffsetMeshToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UOffsetMeshToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UOffsetMeshToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOffsetMeshToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOffsetMeshToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOffsetMeshToolProperties, 630441303);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UOffsetMeshToolProperties>()
	{
		return UOffsetMeshToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOffsetMeshToolProperties(Z_Construct_UClass_UOffsetMeshToolProperties, &UOffsetMeshToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UOffsetMeshToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOffsetMeshToolProperties);
	void UOffsetWeightMapSetProperties::StaticRegisterNativesUOffsetWeightMapSetProperties()
	{
	}
	UClass* Z_Construct_UClass_UOffsetWeightMapSetProperties_NoRegister()
	{
		return UOffsetWeightMapSetProperties::StaticClass();
	}
	struct Z_Construct_UClass_UOffsetWeightMapSetProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MinDistance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MinDistance;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOffsetWeightMapSetProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UWeightMapSetProperties,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOffsetWeightMapSetProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "OffsetMeshTool.h" },
		{ "ModuleRelativePath", "Public/OffsetMeshTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOffsetWeightMapSetProperties_Statics::NewProp_MinDistance_MetaData[] = {
		{ "Category", "WeightMap" },
		{ "ClampMax", "100000.0" },
		{ "ClampMin", "-10000.0" },
		{ "Comment", "/** Minimum Offset Distance in World Units, for Weight Map values of zero (clamped to Distance) */" },
		{ "DisplayPriority", "5" },
		{ "ModuleRelativePath", "Public/OffsetMeshTool.h" },
		{ "ToolTip", "Minimum Offset Distance in World Units, for Weight Map values of zero (clamped to Distance)" },
		{ "UIMax", "100.0" },
		{ "UIMin", "-100.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UOffsetWeightMapSetProperties_Statics::NewProp_MinDistance = { "MinDistance", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOffsetWeightMapSetProperties, MinDistance), METADATA_PARAMS(Z_Construct_UClass_UOffsetWeightMapSetProperties_Statics::NewProp_MinDistance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOffsetWeightMapSetProperties_Statics::NewProp_MinDistance_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UOffsetWeightMapSetProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOffsetWeightMapSetProperties_Statics::NewProp_MinDistance,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOffsetWeightMapSetProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOffsetWeightMapSetProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOffsetWeightMapSetProperties_Statics::ClassParams = {
		&UOffsetWeightMapSetProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UOffsetWeightMapSetProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UOffsetWeightMapSetProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UOffsetWeightMapSetProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UOffsetWeightMapSetProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOffsetWeightMapSetProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOffsetWeightMapSetProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOffsetWeightMapSetProperties, 1068388607);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UOffsetWeightMapSetProperties>()
	{
		return UOffsetWeightMapSetProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOffsetWeightMapSetProperties(Z_Construct_UClass_UOffsetWeightMapSetProperties, &UOffsetWeightMapSetProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UOffsetWeightMapSetProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOffsetWeightMapSetProperties);
	void UIterativeOffsetProperties::StaticRegisterNativesUIterativeOffsetProperties()
	{
	}
	UClass* Z_Construct_UClass_UIterativeOffsetProperties_NoRegister()
	{
		return UIterativeOffsetProperties::StaticClass();
	}
	struct Z_Construct_UClass_UIterativeOffsetProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Steps_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Steps;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOffsetBoundaries_MetaData[];
#endif
		static void NewProp_bOffsetBoundaries_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOffsetBoundaries;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SmoothingPerStep_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SmoothingPerStep;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bReprojectSmooth_MetaData[];
#endif
		static void NewProp_bReprojectSmooth_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bReprojectSmooth;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UIterativeOffsetProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UIterativeOffsetProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Properties for Iterative Offseting */" },
		{ "IncludePath", "OffsetMeshTool.h" },
		{ "ModuleRelativePath", "Public/OffsetMeshTool.h" },
		{ "ToolTip", "Properties for Iterative Offseting" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UIterativeOffsetProperties_Statics::NewProp_Steps_MetaData[] = {
		{ "Category", "IterativeOffsetOptions" },
		{ "ClampMax", "1000" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Number of Offseting iterations */" },
		{ "ModuleRelativePath", "Public/OffsetMeshTool.h" },
		{ "ToolTip", "Number of Offseting iterations" },
		{ "UIMax", "100" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UIterativeOffsetProperties_Statics::NewProp_Steps = { "Steps", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UIterativeOffsetProperties, Steps), METADATA_PARAMS(Z_Construct_UClass_UIterativeOffsetProperties_Statics::NewProp_Steps_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UIterativeOffsetProperties_Statics::NewProp_Steps_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UIterativeOffsetProperties_Statics::NewProp_bOffsetBoundaries_MetaData[] = {
		{ "Category", "IterativeOffsetOptions" },
		{ "Comment", "/** Control whether the boundary is allowed to move */" },
		{ "ModuleRelativePath", "Public/OffsetMeshTool.h" },
		{ "ToolTip", "Control whether the boundary is allowed to move" },
	};
#endif
	void Z_Construct_UClass_UIterativeOffsetProperties_Statics::NewProp_bOffsetBoundaries_SetBit(void* Obj)
	{
		((UIterativeOffsetProperties*)Obj)->bOffsetBoundaries = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UIterativeOffsetProperties_Statics::NewProp_bOffsetBoundaries = { "bOffsetBoundaries", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UIterativeOffsetProperties), &Z_Construct_UClass_UIterativeOffsetProperties_Statics::NewProp_bOffsetBoundaries_SetBit, METADATA_PARAMS(Z_Construct_UClass_UIterativeOffsetProperties_Statics::NewProp_bOffsetBoundaries_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UIterativeOffsetProperties_Statics::NewProp_bOffsetBoundaries_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UIterativeOffsetProperties_Statics::NewProp_SmoothingPerStep_MetaData[] = {
		{ "Category", "IterativeOffsetOptions" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Amount of smoothing applied per Offset step */" },
		{ "ModuleRelativePath", "Public/OffsetMeshTool.h" },
		{ "ToolTip", "Amount of smoothing applied per Offset step" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UIterativeOffsetProperties_Statics::NewProp_SmoothingPerStep = { "SmoothingPerStep", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UIterativeOffsetProperties, SmoothingPerStep), METADATA_PARAMS(Z_Construct_UClass_UIterativeOffsetProperties_Statics::NewProp_SmoothingPerStep_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UIterativeOffsetProperties_Statics::NewProp_SmoothingPerStep_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UIterativeOffsetProperties_Statics::NewProp_bReprojectSmooth_MetaData[] = {
		{ "Category", "IterativeOffsetOptions" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Reproject smooth vertices onto non-smoothed Offset Surface at each step (expensive but better-preserves uniform distance) */" },
		{ "EditCondition", "SmoothingPerStep > 0" },
		{ "ModuleRelativePath", "Public/OffsetMeshTool.h" },
		{ "ToolTip", "Reproject smooth vertices onto non-smoothed Offset Surface at each step (expensive but better-preserves uniform distance)" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	void Z_Construct_UClass_UIterativeOffsetProperties_Statics::NewProp_bReprojectSmooth_SetBit(void* Obj)
	{
		((UIterativeOffsetProperties*)Obj)->bReprojectSmooth = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UIterativeOffsetProperties_Statics::NewProp_bReprojectSmooth = { "bReprojectSmooth", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UIterativeOffsetProperties), &Z_Construct_UClass_UIterativeOffsetProperties_Statics::NewProp_bReprojectSmooth_SetBit, METADATA_PARAMS(Z_Construct_UClass_UIterativeOffsetProperties_Statics::NewProp_bReprojectSmooth_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UIterativeOffsetProperties_Statics::NewProp_bReprojectSmooth_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UIterativeOffsetProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UIterativeOffsetProperties_Statics::NewProp_Steps,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UIterativeOffsetProperties_Statics::NewProp_bOffsetBoundaries,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UIterativeOffsetProperties_Statics::NewProp_SmoothingPerStep,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UIterativeOffsetProperties_Statics::NewProp_bReprojectSmooth,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UIterativeOffsetProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UIterativeOffsetProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UIterativeOffsetProperties_Statics::ClassParams = {
		&UIterativeOffsetProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UIterativeOffsetProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UIterativeOffsetProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UIterativeOffsetProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UIterativeOffsetProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UIterativeOffsetProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UIterativeOffsetProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UIterativeOffsetProperties, 1519744107);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UIterativeOffsetProperties>()
	{
		return UIterativeOffsetProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UIterativeOffsetProperties(Z_Construct_UClass_UIterativeOffsetProperties, &UIterativeOffsetProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UIterativeOffsetProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UIterativeOffsetProperties);
	void UImplicitOffsetProperties::StaticRegisterNativesUImplicitOffsetProperties()
	{
	}
	UClass* Z_Construct_UClass_UImplicitOffsetProperties_NoRegister()
	{
		return UImplicitOffsetProperties::StaticClass();
	}
	struct Z_Construct_UClass_UImplicitOffsetProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Smoothness_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Smoothness;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPreserveUVs_MetaData[];
#endif
		static void NewProp_bPreserveUVs_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPreserveUVs;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UImplicitOffsetProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UImplicitOffsetProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Properties for Implicit Offseting */" },
		{ "IncludePath", "OffsetMeshTool.h" },
		{ "ModuleRelativePath", "Public/OffsetMeshTool.h" },
		{ "ToolTip", "Properties for Implicit Offseting" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UImplicitOffsetProperties_Statics::NewProp_Smoothness_MetaData[] = {
		{ "Category", "ImplicitOffsetingOptions" },
		{ "ClampMax", "100.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** How tightly we should constrain the constrained implicit offset to the explicit offset */" },
		{ "ModuleRelativePath", "Public/OffsetMeshTool.h" },
		{ "ToolTip", "How tightly we should constrain the constrained implicit offset to the explicit offset" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UImplicitOffsetProperties_Statics::NewProp_Smoothness = { "Smoothness", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UImplicitOffsetProperties, Smoothness), METADATA_PARAMS(Z_Construct_UClass_UImplicitOffsetProperties_Statics::NewProp_Smoothness_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UImplicitOffsetProperties_Statics::NewProp_Smoothness_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UImplicitOffsetProperties_Statics::NewProp_bPreserveUVs_MetaData[] = {
		{ "Category", "ImplicitOffsetingOptions" },
		{ "Comment", "/** If this is false, triangles will be reshaped to be more regular, which will distort UVs */" },
		{ "ModuleRelativePath", "Public/OffsetMeshTool.h" },
		{ "ToolTip", "If this is false, triangles will be reshaped to be more regular, which will distort UVs" },
	};
#endif
	void Z_Construct_UClass_UImplicitOffsetProperties_Statics::NewProp_bPreserveUVs_SetBit(void* Obj)
	{
		((UImplicitOffsetProperties*)Obj)->bPreserveUVs = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UImplicitOffsetProperties_Statics::NewProp_bPreserveUVs = { "bPreserveUVs", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UImplicitOffsetProperties), &Z_Construct_UClass_UImplicitOffsetProperties_Statics::NewProp_bPreserveUVs_SetBit, METADATA_PARAMS(Z_Construct_UClass_UImplicitOffsetProperties_Statics::NewProp_bPreserveUVs_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UImplicitOffsetProperties_Statics::NewProp_bPreserveUVs_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UImplicitOffsetProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UImplicitOffsetProperties_Statics::NewProp_Smoothness,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UImplicitOffsetProperties_Statics::NewProp_bPreserveUVs,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UImplicitOffsetProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UImplicitOffsetProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UImplicitOffsetProperties_Statics::ClassParams = {
		&UImplicitOffsetProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UImplicitOffsetProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UImplicitOffsetProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UImplicitOffsetProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UImplicitOffsetProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UImplicitOffsetProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UImplicitOffsetProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UImplicitOffsetProperties, 2560509331);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UImplicitOffsetProperties>()
	{
		return UImplicitOffsetProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UImplicitOffsetProperties(Z_Construct_UClass_UImplicitOffsetProperties, &UImplicitOffsetProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UImplicitOffsetProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UImplicitOffsetProperties);
	void UOffsetMeshTool::StaticRegisterNativesUOffsetMeshTool()
	{
	}
	UClass* Z_Construct_UClass_UOffsetMeshTool_NoRegister()
	{
		return UOffsetMeshTool::StaticClass();
	}
	struct Z_Construct_UClass_UOffsetMeshTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OffsetProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OffsetProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IterativeProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_IterativeProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImplicitProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ImplicitProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WeightMapProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WeightMapProperties;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOffsetMeshTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBaseMeshProcessingTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOffsetMeshTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Mesh Offseting Tool\n */" },
		{ "IncludePath", "OffsetMeshTool.h" },
		{ "ModuleRelativePath", "Public/OffsetMeshTool.h" },
		{ "ToolTip", "Mesh Offseting Tool" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOffsetMeshTool_Statics::NewProp_OffsetProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/OffsetMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UOffsetMeshTool_Statics::NewProp_OffsetProperties = { "OffsetProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOffsetMeshTool, OffsetProperties), Z_Construct_UClass_UOffsetMeshToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UOffsetMeshTool_Statics::NewProp_OffsetProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOffsetMeshTool_Statics::NewProp_OffsetProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOffsetMeshTool_Statics::NewProp_IterativeProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/OffsetMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UOffsetMeshTool_Statics::NewProp_IterativeProperties = { "IterativeProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOffsetMeshTool, IterativeProperties), Z_Construct_UClass_UIterativeOffsetProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UOffsetMeshTool_Statics::NewProp_IterativeProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOffsetMeshTool_Statics::NewProp_IterativeProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOffsetMeshTool_Statics::NewProp_ImplicitProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/OffsetMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UOffsetMeshTool_Statics::NewProp_ImplicitProperties = { "ImplicitProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOffsetMeshTool, ImplicitProperties), Z_Construct_UClass_UImplicitOffsetProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UOffsetMeshTool_Statics::NewProp_ImplicitProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOffsetMeshTool_Statics::NewProp_ImplicitProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOffsetMeshTool_Statics::NewProp_WeightMapProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/OffsetMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UOffsetMeshTool_Statics::NewProp_WeightMapProperties = { "WeightMapProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOffsetMeshTool, WeightMapProperties), Z_Construct_UClass_UOffsetWeightMapSetProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UOffsetMeshTool_Statics::NewProp_WeightMapProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOffsetMeshTool_Statics::NewProp_WeightMapProperties_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UOffsetMeshTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOffsetMeshTool_Statics::NewProp_OffsetProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOffsetMeshTool_Statics::NewProp_IterativeProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOffsetMeshTool_Statics::NewProp_ImplicitProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOffsetMeshTool_Statics::NewProp_WeightMapProperties,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOffsetMeshTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOffsetMeshTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOffsetMeshTool_Statics::ClassParams = {
		&UOffsetMeshTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UOffsetMeshTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UOffsetMeshTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UOffsetMeshTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UOffsetMeshTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOffsetMeshTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOffsetMeshTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOffsetMeshTool, 1997242297);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UOffsetMeshTool>()
	{
		return UOffsetMeshTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOffsetMeshTool(Z_Construct_UClass_UOffsetMeshTool, &UOffsetMeshTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UOffsetMeshTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOffsetMeshTool);
	void UOffsetMeshToolBuilder::StaticRegisterNativesUOffsetMeshToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UOffsetMeshToolBuilder_NoRegister()
	{
		return UOffsetMeshToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UOffsetMeshToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOffsetMeshToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBaseMeshProcessingToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOffsetMeshToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "OffsetMeshTool.h" },
		{ "ModuleRelativePath", "Public/OffsetMeshTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOffsetMeshToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOffsetMeshToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOffsetMeshToolBuilder_Statics::ClassParams = {
		&UOffsetMeshToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UOffsetMeshToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UOffsetMeshToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOffsetMeshToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOffsetMeshToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOffsetMeshToolBuilder, 1436603401);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UOffsetMeshToolBuilder>()
	{
		return UOffsetMeshToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOffsetMeshToolBuilder(Z_Construct_UClass_UOffsetMeshToolBuilder, &UOffsetMeshToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UOffsetMeshToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOffsetMeshToolBuilder);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
