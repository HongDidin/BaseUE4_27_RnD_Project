// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLSEDITORONLY_ParameterizeMeshTool_generated_h
#error "ParameterizeMeshTool.generated.h already included, missing '#pragma once' in ParameterizeMeshTool.h"
#endif
#define MESHMODELINGTOOLSEDITORONLY_ParameterizeMeshTool_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_43_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_43_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_43_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_43_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUParameterizeMeshToolBuilder(); \
	friend struct Z_Construct_UClass_UParameterizeMeshToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UParameterizeMeshToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(UParameterizeMeshToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_43_INCLASS \
private: \
	static void StaticRegisterNativesUParameterizeMeshToolBuilder(); \
	friend struct Z_Construct_UClass_UParameterizeMeshToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UParameterizeMeshToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(UParameterizeMeshToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_43_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UParameterizeMeshToolBuilder(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UParameterizeMeshToolBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UParameterizeMeshToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UParameterizeMeshToolBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UParameterizeMeshToolBuilder(UParameterizeMeshToolBuilder&&); \
	NO_API UParameterizeMeshToolBuilder(const UParameterizeMeshToolBuilder&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_43_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UParameterizeMeshToolBuilder(UParameterizeMeshToolBuilder&&); \
	NO_API UParameterizeMeshToolBuilder(const UParameterizeMeshToolBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UParameterizeMeshToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UParameterizeMeshToolBuilder); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UParameterizeMeshToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_43_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_40_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_43_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_43_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_43_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_43_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_43_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_43_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_43_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_43_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_43_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_43_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_43_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_43_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<class UParameterizeMeshToolBuilder>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_105_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_105_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_105_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_105_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUParameterizeMeshToolProperties(); \
	friend struct Z_Construct_UClass_UParameterizeMeshToolProperties_Statics; \
public: \
	DECLARE_CLASS(UParameterizeMeshToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(UParameterizeMeshToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_105_INCLASS \
private: \
	static void StaticRegisterNativesUParameterizeMeshToolProperties(); \
	friend struct Z_Construct_UClass_UParameterizeMeshToolProperties_Statics; \
public: \
	DECLARE_CLASS(UParameterizeMeshToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(UParameterizeMeshToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_105_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UParameterizeMeshToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UParameterizeMeshToolProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UParameterizeMeshToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UParameterizeMeshToolProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UParameterizeMeshToolProperties(UParameterizeMeshToolProperties&&); \
	NO_API UParameterizeMeshToolProperties(const UParameterizeMeshToolProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_105_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UParameterizeMeshToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UParameterizeMeshToolProperties(UParameterizeMeshToolProperties&&); \
	NO_API UParameterizeMeshToolProperties(const UParameterizeMeshToolProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UParameterizeMeshToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UParameterizeMeshToolProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UParameterizeMeshToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_105_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_102_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_105_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_105_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_105_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_105_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_105_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_105_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_105_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_105_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_105_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_105_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_105_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_105_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<class UParameterizeMeshToolProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_141_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_141_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_141_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_141_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUParameterizeMeshTool(); \
	friend struct Z_Construct_UClass_UParameterizeMeshTool_Statics; \
public: \
	DECLARE_CLASS(UParameterizeMeshTool, USingleSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(UParameterizeMeshTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_141_INCLASS \
private: \
	static void StaticRegisterNativesUParameterizeMeshTool(); \
	friend struct Z_Construct_UClass_UParameterizeMeshTool_Statics; \
public: \
	DECLARE_CLASS(UParameterizeMeshTool, USingleSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(UParameterizeMeshTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_141_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UParameterizeMeshTool(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UParameterizeMeshTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UParameterizeMeshTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UParameterizeMeshTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UParameterizeMeshTool(UParameterizeMeshTool&&); \
	NO_API UParameterizeMeshTool(const UParameterizeMeshTool&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_141_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UParameterizeMeshTool(UParameterizeMeshTool&&); \
	NO_API UParameterizeMeshTool(const UParameterizeMeshTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UParameterizeMeshTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UParameterizeMeshTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UParameterizeMeshTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_141_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Settings() { return STRUCT_OFFSET(UParameterizeMeshTool, Settings); } \
	FORCEINLINE static uint32 __PPO__MaterialSettings() { return STRUCT_OFFSET(UParameterizeMeshTool, MaterialSettings); } \
	FORCEINLINE static uint32 __PPO__DefaultMaterial() { return STRUCT_OFFSET(UParameterizeMeshTool, DefaultMaterial); } \
	FORCEINLINE static uint32 __PPO__DisplayMaterial() { return STRUCT_OFFSET(UParameterizeMeshTool, DisplayMaterial); } \
	FORCEINLINE static uint32 __PPO__CheckerMaterial() { return STRUCT_OFFSET(UParameterizeMeshTool, CheckerMaterial); } \
	FORCEINLINE static uint32 __PPO__Preview() { return STRUCT_OFFSET(UParameterizeMeshTool, Preview); } \
	FORCEINLINE static uint32 __PPO__bDoAutomaticGlobalUnwrap() { return STRUCT_OFFSET(UParameterizeMeshTool, bDoAutomaticGlobalUnwrap); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_138_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_141_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_141_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_141_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_141_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_141_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_141_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_141_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_141_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_141_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_141_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_141_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h_141_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<class UParameterizeMeshTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ParameterizeMeshTool_h


#define FOREACH_ENUM_EPARAMETERIZEMESHTOOLUVSCALEMODE(op) \
	op(EParameterizeMeshToolUVScaleMode::NoScaling) \
	op(EParameterizeMeshToolUVScaleMode::NormalizeToBounds) \
	op(EParameterizeMeshToolUVScaleMode::NormalizeToWorld) 

enum class EParameterizeMeshToolUVScaleMode;
template<> MESHMODELINGTOOLSEDITORONLY_API UEnum* StaticEnum<EParameterizeMeshToolUVScaleMode>();

#define FOREACH_ENUM_EUVISLANDMODE(op) \
	op(EUVIslandMode::Auto) \
	op(EUVIslandMode::PolyGroups) \
	op(EUVIslandMode::ExistingUVs) 

enum class EUVIslandMode;
template<> MESHMODELINGTOOLSEDITORONLY_API UEnum* StaticEnum<EUVIslandMode>();

#define FOREACH_ENUM_EUVUNWRAPTYPE(op) \
	op(EUVUnwrapType::MinStretch) \
	op(EUVUnwrapType::ExpMap) \
	op(EUVUnwrapType::Conformal) 

enum class EUVUnwrapType;
template<> MESHMODELINGTOOLSEDITORONLY_API UEnum* StaticEnum<EUVUnwrapType>();

#define FOREACH_ENUM_EPARAMETERIZEMESHMATERIALMODE(op) \
	op(EParameterizeMeshMaterialMode::Default) \
	op(EParameterizeMeshMaterialMode::Checkerboard) \
	op(EParameterizeMeshMaterialMode::Override) 

enum class EParameterizeMeshMaterialMode : uint8;
template<> MESHMODELINGTOOLSEDITORONLY_API UEnum* StaticEnum<EParameterizeMeshMaterialMode>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
