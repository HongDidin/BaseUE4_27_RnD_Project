// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ModelingComponents/Public/BaseTools/BaseVoxelTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBaseVoxelTool() {}
// Cross Module References
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UBaseVoxelTool_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UBaseVoxelTool();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UBaseCreateFromSelectedTool();
	UPackage* Z_Construct_UPackage__Script_ModelingComponents();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UVoxelProperties_NoRegister();
// End Cross Module References
	void UBaseVoxelTool::StaticRegisterNativesUBaseVoxelTool()
	{
	}
	UClass* Z_Construct_UClass_UBaseVoxelTool_NoRegister()
	{
		return UBaseVoxelTool::StaticClass();
	}
	struct Z_Construct_UClass_UBaseVoxelTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VoxProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_VoxProperties;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBaseVoxelTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBaseCreateFromSelectedTool,
		(UObject* (*)())Z_Construct_UPackage__Script_ModelingComponents,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBaseVoxelTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Base for Voxel tools\n */" },
		{ "IncludePath", "BaseTools/BaseVoxelTool.h" },
		{ "ModuleRelativePath", "Public/BaseTools/BaseVoxelTool.h" },
		{ "ToolTip", "Base for Voxel tools" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBaseVoxelTool_Statics::NewProp_VoxProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/BaseTools/BaseVoxelTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UBaseVoxelTool_Statics::NewProp_VoxProperties = { "VoxProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBaseVoxelTool, VoxProperties), Z_Construct_UClass_UVoxelProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UBaseVoxelTool_Statics::NewProp_VoxProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBaseVoxelTool_Statics::NewProp_VoxProperties_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UBaseVoxelTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBaseVoxelTool_Statics::NewProp_VoxProperties,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBaseVoxelTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBaseVoxelTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBaseVoxelTool_Statics::ClassParams = {
		&UBaseVoxelTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UBaseVoxelTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UBaseVoxelTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UBaseVoxelTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBaseVoxelTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBaseVoxelTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBaseVoxelTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBaseVoxelTool, 2073179709);
	template<> MODELINGCOMPONENTS_API UClass* StaticClass<UBaseVoxelTool>()
	{
		return UBaseVoxelTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBaseVoxelTool(Z_Construct_UClass_UBaseVoxelTool, &UBaseVoxelTool::StaticClass, TEXT("/Script/ModelingComponents"), TEXT("UBaseVoxelTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBaseVoxelTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
