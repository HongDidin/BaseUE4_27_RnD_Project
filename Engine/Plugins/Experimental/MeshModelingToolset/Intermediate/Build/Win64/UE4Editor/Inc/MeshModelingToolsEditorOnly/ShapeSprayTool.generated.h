// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLSEDITORONLY_ShapeSprayTool_generated_h
#error "ShapeSprayTool.generated.h already included, missing '#pragma once' in ShapeSprayTool.h"
#endif
#define MESHMODELINGTOOLSEDITORONLY_ShapeSprayTool_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_18_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_18_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUShapeSprayToolBuilder(); \
	friend struct Z_Construct_UClass_UShapeSprayToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UShapeSprayToolBuilder, UMeshSurfacePointToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(UShapeSprayToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUShapeSprayToolBuilder(); \
	friend struct Z_Construct_UClass_UShapeSprayToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UShapeSprayToolBuilder, UMeshSurfacePointToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(UShapeSprayToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UShapeSprayToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UShapeSprayToolBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UShapeSprayToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShapeSprayToolBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UShapeSprayToolBuilder(UShapeSprayToolBuilder&&); \
	NO_API UShapeSprayToolBuilder(const UShapeSprayToolBuilder&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UShapeSprayToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UShapeSprayToolBuilder(UShapeSprayToolBuilder&&); \
	NO_API UShapeSprayToolBuilder(const UShapeSprayToolBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UShapeSprayToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShapeSprayToolBuilder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UShapeSprayToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_15_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_18_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_18_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_18_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_18_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<class UShapeSprayToolBuilder>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_33_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_33_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_33_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_33_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUShapeSprayToolProperties(); \
	friend struct Z_Construct_UClass_UShapeSprayToolProperties_Statics; \
public: \
	DECLARE_CLASS(UShapeSprayToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(UShapeSprayToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_33_INCLASS \
private: \
	static void StaticRegisterNativesUShapeSprayToolProperties(); \
	friend struct Z_Construct_UClass_UShapeSprayToolProperties_Statics; \
public: \
	DECLARE_CLASS(UShapeSprayToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(UShapeSprayToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_33_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UShapeSprayToolProperties(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UShapeSprayToolProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UShapeSprayToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShapeSprayToolProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UShapeSprayToolProperties(UShapeSprayToolProperties&&); \
	NO_API UShapeSprayToolProperties(const UShapeSprayToolProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_33_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UShapeSprayToolProperties(UShapeSprayToolProperties&&); \
	NO_API UShapeSprayToolProperties(const UShapeSprayToolProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UShapeSprayToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShapeSprayToolProperties); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UShapeSprayToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_33_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_30_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_33_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_33_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_33_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_33_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_33_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_33_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_33_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_33_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_33_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_33_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_33_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_33_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<class UShapeSprayToolProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_67_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_67_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_67_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_67_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUShapeSprayTool(); \
	friend struct Z_Construct_UClass_UShapeSprayTool_Statics; \
public: \
	DECLARE_CLASS(UShapeSprayTool, UDynamicMeshBrushTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(UShapeSprayTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_67_INCLASS \
private: \
	static void StaticRegisterNativesUShapeSprayTool(); \
	friend struct Z_Construct_UClass_UShapeSprayTool_Statics; \
public: \
	DECLARE_CLASS(UShapeSprayTool, UDynamicMeshBrushTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(UShapeSprayTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_67_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UShapeSprayTool(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UShapeSprayTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UShapeSprayTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShapeSprayTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UShapeSprayTool(UShapeSprayTool&&); \
	NO_API UShapeSprayTool(const UShapeSprayTool&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_67_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UShapeSprayTool(UShapeSprayTool&&); \
	NO_API UShapeSprayTool(const UShapeSprayTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UShapeSprayTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShapeSprayTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UShapeSprayTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_67_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Settings() { return STRUCT_OFFSET(UShapeSprayTool, Settings); } \
	FORCEINLINE static uint32 __PPO__AccumMeshComponent() { return STRUCT_OFFSET(UShapeSprayTool, AccumMeshComponent); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_64_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_67_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_67_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_67_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_67_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_67_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_67_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_67_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_67_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_67_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_67_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_67_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h_67_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<class UShapeSprayTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_ShapeSprayTool_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
