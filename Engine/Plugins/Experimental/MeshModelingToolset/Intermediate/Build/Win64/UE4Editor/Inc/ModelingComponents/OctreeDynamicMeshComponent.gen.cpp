// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ModelingComponents/Public/OctreeDynamicMeshComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOctreeDynamicMeshComponent() {}
// Cross Module References
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UOctreeDynamicMeshComponent_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UOctreeDynamicMeshComponent();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UBaseDynamicMeshComponent();
	UPackage* Z_Construct_UPackage__Script_ModelingComponents();
// End Cross Module References
	void UOctreeDynamicMeshComponent::StaticRegisterNativesUOctreeDynamicMeshComponent()
	{
	}
	UClass* Z_Construct_UClass_UOctreeDynamicMeshComponent_NoRegister()
	{
		return UOctreeDynamicMeshComponent::StaticClass();
	}
	struct Z_Construct_UClass_UOctreeDynamicMeshComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bExplicitShowWireframe_MetaData[];
#endif
		static void NewProp_bExplicitShowWireframe_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bExplicitShowWireframe;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOctreeDynamicMeshComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBaseDynamicMeshComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_ModelingComponents,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOctreeDynamicMeshComponent_Statics::Class_MetaDataParams[] = {
		{ "ClassGroupNames", "Rendering" },
		{ "Comment", "/** \n * UOctreeDynamicMeshComponent is a mesh component similar to UProceduralMeshComponent,\n * except it bases the renderable geometry off an internal FDynamicMesh3 instance.\n * The class generally has the same capabilities as USimpleDynamicMeshComponent.\n * \n * A FDynamicMeshOctree3 is available to dynamically track the triangles of the mesh\n * (however the client is responsible for updating this octree).\n * Based on the Octree, the mesh is partitioned into chunks that are stored in separate\n * RenderBuffers in the FOctreeDynamicMeshSceneProxy.\n * Calling NotifyMeshUpdated() will result in only the \"dirty\" chunks being updated,\n * rather than the entire mesh.\n * \n * (So, if you don't need this capability, and don't want to update an Octree, use USimpleDynamicMeshComponent!)\n */" },
		{ "HideCategories", "LOD Physics Collision LOD Physics Collision Mobility Trigger" },
		{ "IncludePath", "OctreeDynamicMeshComponent.h" },
		{ "ModuleRelativePath", "Public/OctreeDynamicMeshComponent.h" },
		{ "ToolTip", "UOctreeDynamicMeshComponent is a mesh component similar to UProceduralMeshComponent,\nexcept it bases the renderable geometry off an internal FDynamicMesh3 instance.\nThe class generally has the same capabilities as USimpleDynamicMeshComponent.\n\nA FDynamicMeshOctree3 is available to dynamically track the triangles of the mesh\n(however the client is responsible for updating this octree).\nBased on the Octree, the mesh is partitioned into chunks that are stored in separate\nRenderBuffers in the FOctreeDynamicMeshSceneProxy.\nCalling NotifyMeshUpdated() will result in only the \"dirty\" chunks being updated,\nrather than the entire mesh.\n\n(So, if you don't need this capability, and don't want to update an Octree, use USimpleDynamicMeshComponent!)" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOctreeDynamicMeshComponent_Statics::NewProp_bExplicitShowWireframe_MetaData[] = {
		{ "Comment", "/**\n\x09 * if true, we always show the wireframe on top of the shaded mesh, even when not in wireframe mode\n\x09 */" },
		{ "ModuleRelativePath", "Public/OctreeDynamicMeshComponent.h" },
		{ "ToolTip", "if true, we always show the wireframe on top of the shaded mesh, even when not in wireframe mode" },
	};
#endif
	void Z_Construct_UClass_UOctreeDynamicMeshComponent_Statics::NewProp_bExplicitShowWireframe_SetBit(void* Obj)
	{
		((UOctreeDynamicMeshComponent*)Obj)->bExplicitShowWireframe = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UOctreeDynamicMeshComponent_Statics::NewProp_bExplicitShowWireframe = { "bExplicitShowWireframe", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UOctreeDynamicMeshComponent), &Z_Construct_UClass_UOctreeDynamicMeshComponent_Statics::NewProp_bExplicitShowWireframe_SetBit, METADATA_PARAMS(Z_Construct_UClass_UOctreeDynamicMeshComponent_Statics::NewProp_bExplicitShowWireframe_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOctreeDynamicMeshComponent_Statics::NewProp_bExplicitShowWireframe_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UOctreeDynamicMeshComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOctreeDynamicMeshComponent_Statics::NewProp_bExplicitShowWireframe,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOctreeDynamicMeshComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOctreeDynamicMeshComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOctreeDynamicMeshComponent_Statics::ClassParams = {
		&UOctreeDynamicMeshComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UOctreeDynamicMeshComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UOctreeDynamicMeshComponent_Statics::PropPointers),
		0,
		0x00B010A4u,
		METADATA_PARAMS(Z_Construct_UClass_UOctreeDynamicMeshComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UOctreeDynamicMeshComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOctreeDynamicMeshComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOctreeDynamicMeshComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOctreeDynamicMeshComponent, 2464353319);
	template<> MODELINGCOMPONENTS_API UClass* StaticClass<UOctreeDynamicMeshComponent>()
	{
		return UOctreeDynamicMeshComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOctreeDynamicMeshComponent(Z_Construct_UClass_UOctreeDynamicMeshComponent, &UOctreeDynamicMeshComponent::StaticClass, TEXT("/Script/ModelingComponents"), TEXT("UOctreeDynamicMeshComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOctreeDynamicMeshComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
