// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ModelingComponents/Public/Mechanics/CollectSurfacePathMechanic.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCollectSurfacePathMechanic() {}
// Cross Module References
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UCollectSurfacePathMechanic_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UCollectSurfacePathMechanic();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractionMechanic();
	UPackage* Z_Construct_UPackage__Script_ModelingComponents();
// End Cross Module References
	void UCollectSurfacePathMechanic::StaticRegisterNativesUCollectSurfacePathMechanic()
	{
	}
	UClass* Z_Construct_UClass_UCollectSurfacePathMechanic_NoRegister()
	{
		return UCollectSurfacePathMechanic::StaticClass();
	}
	struct Z_Construct_UClass_UCollectSurfacePathMechanic_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCollectSurfacePathMechanic_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractionMechanic,
		(UObject* (*)())Z_Construct_UPackage__Script_ModelingComponents,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCollectSurfacePathMechanic_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n */" },
		{ "IncludePath", "Mechanics/CollectSurfacePathMechanic.h" },
		{ "ModuleRelativePath", "Public/Mechanics/CollectSurfacePathMechanic.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCollectSurfacePathMechanic_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCollectSurfacePathMechanic>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCollectSurfacePathMechanic_Statics::ClassParams = {
		&UCollectSurfacePathMechanic::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UCollectSurfacePathMechanic_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCollectSurfacePathMechanic_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCollectSurfacePathMechanic()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCollectSurfacePathMechanic_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCollectSurfacePathMechanic, 2984058033);
	template<> MODELINGCOMPONENTS_API UClass* StaticClass<UCollectSurfacePathMechanic>()
	{
		return UCollectSurfacePathMechanic::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCollectSurfacePathMechanic(Z_Construct_UClass_UCollectSurfacePathMechanic, &UCollectSurfacePathMechanic::StaticClass, TEXT("/Script/ModelingComponents"), TEXT("UCollectSurfacePathMechanic"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCollectSurfacePathMechanic);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
