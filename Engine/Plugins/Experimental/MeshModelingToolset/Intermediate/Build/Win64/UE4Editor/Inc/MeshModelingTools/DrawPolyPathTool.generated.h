// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLS_DrawPolyPathTool_generated_h
#error "DrawPolyPathTool.generated.h already included, missing '#pragma once' in DrawPolyPathTool.h"
#endif
#define MESHMODELINGTOOLS_DrawPolyPathTool_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_26_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_26_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_26_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_26_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDrawPolyPathToolBuilder(); \
	friend struct Z_Construct_UClass_UDrawPolyPathToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UDrawPolyPathToolBuilder, UMeshSurfacePointToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UDrawPolyPathToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_26_INCLASS \
private: \
	static void StaticRegisterNativesUDrawPolyPathToolBuilder(); \
	friend struct Z_Construct_UClass_UDrawPolyPathToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UDrawPolyPathToolBuilder, UMeshSurfacePointToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UDrawPolyPathToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_26_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDrawPolyPathToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDrawPolyPathToolBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDrawPolyPathToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDrawPolyPathToolBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDrawPolyPathToolBuilder(UDrawPolyPathToolBuilder&&); \
	NO_API UDrawPolyPathToolBuilder(const UDrawPolyPathToolBuilder&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_26_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDrawPolyPathToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDrawPolyPathToolBuilder(UDrawPolyPathToolBuilder&&); \
	NO_API UDrawPolyPathToolBuilder(const UDrawPolyPathToolBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDrawPolyPathToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDrawPolyPathToolBuilder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDrawPolyPathToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_26_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_23_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_26_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_26_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_26_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_26_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_26_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_26_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_26_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_26_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_26_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_26_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_26_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_26_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UDrawPolyPathToolBuilder>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_65_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_65_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_65_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_65_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDrawPolyPathProperties(); \
	friend struct Z_Construct_UClass_UDrawPolyPathProperties_Statics; \
public: \
	DECLARE_CLASS(UDrawPolyPathProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UDrawPolyPathProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_65_INCLASS \
private: \
	static void StaticRegisterNativesUDrawPolyPathProperties(); \
	friend struct Z_Construct_UClass_UDrawPolyPathProperties_Statics; \
public: \
	DECLARE_CLASS(UDrawPolyPathProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UDrawPolyPathProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_65_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDrawPolyPathProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDrawPolyPathProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDrawPolyPathProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDrawPolyPathProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDrawPolyPathProperties(UDrawPolyPathProperties&&); \
	NO_API UDrawPolyPathProperties(const UDrawPolyPathProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_65_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDrawPolyPathProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDrawPolyPathProperties(UDrawPolyPathProperties&&); \
	NO_API UDrawPolyPathProperties(const UDrawPolyPathProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDrawPolyPathProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDrawPolyPathProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDrawPolyPathProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_65_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_62_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_65_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_65_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_65_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_65_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_65_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_65_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_65_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_65_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_65_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_65_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_65_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_65_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UDrawPolyPathProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_114_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_114_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_114_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_114_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDrawPolyPathExtrudeProperties(); \
	friend struct Z_Construct_UClass_UDrawPolyPathExtrudeProperties_Statics; \
public: \
	DECLARE_CLASS(UDrawPolyPathExtrudeProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UDrawPolyPathExtrudeProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_114_INCLASS \
private: \
	static void StaticRegisterNativesUDrawPolyPathExtrudeProperties(); \
	friend struct Z_Construct_UClass_UDrawPolyPathExtrudeProperties_Statics; \
public: \
	DECLARE_CLASS(UDrawPolyPathExtrudeProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UDrawPolyPathExtrudeProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_114_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDrawPolyPathExtrudeProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDrawPolyPathExtrudeProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDrawPolyPathExtrudeProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDrawPolyPathExtrudeProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDrawPolyPathExtrudeProperties(UDrawPolyPathExtrudeProperties&&); \
	NO_API UDrawPolyPathExtrudeProperties(const UDrawPolyPathExtrudeProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_114_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDrawPolyPathExtrudeProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDrawPolyPathExtrudeProperties(UDrawPolyPathExtrudeProperties&&); \
	NO_API UDrawPolyPathExtrudeProperties(const UDrawPolyPathExtrudeProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDrawPolyPathExtrudeProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDrawPolyPathExtrudeProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDrawPolyPathExtrudeProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_114_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_111_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_114_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_114_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_114_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_114_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_114_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_114_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_114_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_114_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_114_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_114_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_114_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_114_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UDrawPolyPathExtrudeProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_131_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_131_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_131_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_131_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDrawPolyPathTool(); \
	friend struct Z_Construct_UClass_UDrawPolyPathTool_Statics; \
public: \
	DECLARE_CLASS(UDrawPolyPathTool, UInteractiveTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UDrawPolyPathTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_131_INCLASS \
private: \
	static void StaticRegisterNativesUDrawPolyPathTool(); \
	friend struct Z_Construct_UClass_UDrawPolyPathTool_Statics; \
public: \
	DECLARE_CLASS(UDrawPolyPathTool, UInteractiveTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UDrawPolyPathTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_131_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDrawPolyPathTool(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDrawPolyPathTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDrawPolyPathTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDrawPolyPathTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDrawPolyPathTool(UDrawPolyPathTool&&); \
	NO_API UDrawPolyPathTool(const UDrawPolyPathTool&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_131_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDrawPolyPathTool() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDrawPolyPathTool(UDrawPolyPathTool&&); \
	NO_API UDrawPolyPathTool(const UDrawPolyPathTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDrawPolyPathTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDrawPolyPathTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDrawPolyPathTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_131_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__TransformProps() { return STRUCT_OFFSET(UDrawPolyPathTool, TransformProps); } \
	FORCEINLINE static uint32 __PPO__ExtrudeProperties() { return STRUCT_OFFSET(UDrawPolyPathTool, ExtrudeProperties); } \
	FORCEINLINE static uint32 __PPO__MaterialProperties() { return STRUCT_OFFSET(UDrawPolyPathTool, MaterialProperties); } \
	FORCEINLINE static uint32 __PPO__PlaneMechanic() { return STRUCT_OFFSET(UDrawPolyPathTool, PlaneMechanic); } \
	FORCEINLINE static uint32 __PPO__EditPreview() { return STRUCT_OFFSET(UDrawPolyPathTool, EditPreview); } \
	FORCEINLINE static uint32 __PPO__ExtrudeHeightMechanic() { return STRUCT_OFFSET(UDrawPolyPathTool, ExtrudeHeightMechanic); } \
	FORCEINLINE static uint32 __PPO__CurveDistMechanic() { return STRUCT_OFFSET(UDrawPolyPathTool, CurveDistMechanic); } \
	FORCEINLINE static uint32 __PPO__SurfacePathMechanic() { return STRUCT_OFFSET(UDrawPolyPathTool, SurfacePathMechanic); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_128_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_131_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_131_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_131_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_131_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_131_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_131_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_131_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_131_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_131_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_131_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_131_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h_131_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UDrawPolyPathTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolyPathTool_h


#define FOREACH_ENUM_EDRAWPOLYPATHEXTRUDEDIRECTION(op) \
	op(EDrawPolyPathExtrudeDirection::SelectionNormal) \
	op(EDrawPolyPathExtrudeDirection::WorldX) \
	op(EDrawPolyPathExtrudeDirection::WorldY) \
	op(EDrawPolyPathExtrudeDirection::WorldZ) \
	op(EDrawPolyPathExtrudeDirection::LocalX) \
	op(EDrawPolyPathExtrudeDirection::LocalY) \
	op(EDrawPolyPathExtrudeDirection::LocalZ) 

enum class EDrawPolyPathExtrudeDirection;
template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EDrawPolyPathExtrudeDirection>();

#define FOREACH_ENUM_EDRAWPOLYPATHHEIGHTMODE(op) \
	op(EDrawPolyPathHeightMode::Interactive) \
	op(EDrawPolyPathHeightMode::Constant) 

enum class EDrawPolyPathHeightMode;
template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EDrawPolyPathHeightMode>();

#define FOREACH_ENUM_EDRAWPOLYPATHWIDTHMODE(op) \
	op(EDrawPolyPathWidthMode::Interactive) \
	op(EDrawPolyPathWidthMode::Constant) 

enum class EDrawPolyPathWidthMode;
template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EDrawPolyPathWidthMode>();

#define FOREACH_ENUM_EDRAWPOLYPATHOUTPUTMODE(op) \
	op(EDrawPolyPathOutputMode::Ribbon) \
	op(EDrawPolyPathOutputMode::Extrusion) \
	op(EDrawPolyPathOutputMode::Ramp) 

enum class EDrawPolyPathOutputMode;
template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EDrawPolyPathOutputMode>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
