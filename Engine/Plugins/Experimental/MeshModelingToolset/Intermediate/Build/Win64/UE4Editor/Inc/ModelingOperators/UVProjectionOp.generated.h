// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MODELINGOPERATORS_UVProjectionOp_generated_h
#error "UVProjectionOp.generated.h already included, missing '#pragma once' in UVProjectionOp.h"
#endif
#define MODELINGOPERATORS_UVProjectionOp_generated_h

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingOperators_Public_ParameterizationOps_UVProjectionOp_h


#define FOREACH_ENUM_EUVPROJECTIONMETHOD(op) \
	op(EUVProjectionMethod::Cube) \
	op(EUVProjectionMethod::Cylinder) \
	op(EUVProjectionMethod::Plane) 

enum class EUVProjectionMethod : uint8;
template<> MODELINGOPERATORS_API UEnum* StaticEnum<EUVProjectionMethod>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
