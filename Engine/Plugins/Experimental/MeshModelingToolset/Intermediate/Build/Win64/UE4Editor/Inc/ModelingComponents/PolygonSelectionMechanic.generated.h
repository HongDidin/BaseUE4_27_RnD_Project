// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MODELINGCOMPONENTS_PolygonSelectionMechanic_generated_h
#error "PolygonSelectionMechanic.generated.h already included, missing '#pragma once' in PolygonSelectionMechanic.h"
#endif
#define MODELINGCOMPONENTS_PolygonSelectionMechanic_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_21_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_21_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_21_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPolygonSelectionMechanicProperties(); \
	friend struct Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics; \
public: \
	DECLARE_CLASS(UPolygonSelectionMechanicProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UPolygonSelectionMechanicProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_21_INCLASS \
private: \
	static void StaticRegisterNativesUPolygonSelectionMechanicProperties(); \
	friend struct Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics; \
public: \
	DECLARE_CLASS(UPolygonSelectionMechanicProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UPolygonSelectionMechanicProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPolygonSelectionMechanicProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPolygonSelectionMechanicProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPolygonSelectionMechanicProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPolygonSelectionMechanicProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPolygonSelectionMechanicProperties(UPolygonSelectionMechanicProperties&&); \
	NO_API UPolygonSelectionMechanicProperties(const UPolygonSelectionMechanicProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_21_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPolygonSelectionMechanicProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPolygonSelectionMechanicProperties(UPolygonSelectionMechanicProperties&&); \
	NO_API UPolygonSelectionMechanicProperties(const UPolygonSelectionMechanicProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPolygonSelectionMechanicProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPolygonSelectionMechanicProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPolygonSelectionMechanicProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_21_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_18_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_21_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_21_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_21_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_21_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_21_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MODELINGCOMPONENTS_API UClass* StaticClass<class UPolygonSelectionMechanicProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_68_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_68_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_68_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_68_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPolygonSelectionMechanic(); \
	friend struct Z_Construct_UClass_UPolygonSelectionMechanic_Statics; \
public: \
	DECLARE_CLASS(UPolygonSelectionMechanic, UInteractionMechanic, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UPolygonSelectionMechanic)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_68_INCLASS \
private: \
	static void StaticRegisterNativesUPolygonSelectionMechanic(); \
	friend struct Z_Construct_UClass_UPolygonSelectionMechanic_Statics; \
public: \
	DECLARE_CLASS(UPolygonSelectionMechanic, UInteractionMechanic, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UPolygonSelectionMechanic)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_68_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPolygonSelectionMechanic(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPolygonSelectionMechanic) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPolygonSelectionMechanic); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPolygonSelectionMechanic); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPolygonSelectionMechanic(UPolygonSelectionMechanic&&); \
	NO_API UPolygonSelectionMechanic(const UPolygonSelectionMechanic&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_68_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPolygonSelectionMechanic() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPolygonSelectionMechanic(UPolygonSelectionMechanic&&); \
	NO_API UPolygonSelectionMechanic(const UPolygonSelectionMechanic&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPolygonSelectionMechanic); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPolygonSelectionMechanic); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UPolygonSelectionMechanic)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_68_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__PreviewGeometryActor() { return STRUCT_OFFSET(UPolygonSelectionMechanic, PreviewGeometryActor); } \
	FORCEINLINE static uint32 __PPO__DrawnTriangleSetComponent() { return STRUCT_OFFSET(UPolygonSelectionMechanic, DrawnTriangleSetComponent); } \
	FORCEINLINE static uint32 __PPO__HighlightedFaceMaterial() { return STRUCT_OFFSET(UPolygonSelectionMechanic, HighlightedFaceMaterial); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_65_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_68_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_68_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_68_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_68_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_68_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_68_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_68_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_68_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_68_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_68_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_68_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h_68_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MODELINGCOMPONENTS_API UClass* StaticClass<class UPolygonSelectionMechanic>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Selection_PolygonSelectionMechanic_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
