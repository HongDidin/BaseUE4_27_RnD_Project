// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLS_VoxelBlendMeshesTool_generated_h
#error "VoxelBlendMeshesTool.generated.h already included, missing '#pragma once' in VoxelBlendMeshesTool.h"
#endif
#define MESHMODELINGTOOLS_VoxelBlendMeshesTool_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_18_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_18_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVoxelBlendMeshesToolProperties(); \
	friend struct Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics; \
public: \
	DECLARE_CLASS(UVoxelBlendMeshesToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UVoxelBlendMeshesToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUVoxelBlendMeshesToolProperties(); \
	friend struct Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics; \
public: \
	DECLARE_CLASS(UVoxelBlendMeshesToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UVoxelBlendMeshesToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVoxelBlendMeshesToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVoxelBlendMeshesToolProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVoxelBlendMeshesToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVoxelBlendMeshesToolProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVoxelBlendMeshesToolProperties(UVoxelBlendMeshesToolProperties&&); \
	NO_API UVoxelBlendMeshesToolProperties(const UVoxelBlendMeshesToolProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVoxelBlendMeshesToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVoxelBlendMeshesToolProperties(UVoxelBlendMeshesToolProperties&&); \
	NO_API UVoxelBlendMeshesToolProperties(const UVoxelBlendMeshesToolProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVoxelBlendMeshesToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVoxelBlendMeshesToolProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVoxelBlendMeshesToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_15_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_18_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_18_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_18_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_18_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UVoxelBlendMeshesToolProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_50_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_50_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_50_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_50_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVoxelBlendMeshesTool(); \
	friend struct Z_Construct_UClass_UVoxelBlendMeshesTool_Statics; \
public: \
	DECLARE_CLASS(UVoxelBlendMeshesTool, UBaseVoxelTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UVoxelBlendMeshesTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_50_INCLASS \
private: \
	static void StaticRegisterNativesUVoxelBlendMeshesTool(); \
	friend struct Z_Construct_UClass_UVoxelBlendMeshesTool_Statics; \
public: \
	DECLARE_CLASS(UVoxelBlendMeshesTool, UBaseVoxelTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UVoxelBlendMeshesTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_50_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVoxelBlendMeshesTool(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVoxelBlendMeshesTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVoxelBlendMeshesTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVoxelBlendMeshesTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVoxelBlendMeshesTool(UVoxelBlendMeshesTool&&); \
	NO_API UVoxelBlendMeshesTool(const UVoxelBlendMeshesTool&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_50_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVoxelBlendMeshesTool(UVoxelBlendMeshesTool&&); \
	NO_API UVoxelBlendMeshesTool(const UVoxelBlendMeshesTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVoxelBlendMeshesTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVoxelBlendMeshesTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UVoxelBlendMeshesTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_50_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__BlendProperties() { return STRUCT_OFFSET(UVoxelBlendMeshesTool, BlendProperties); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_47_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_50_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_50_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_50_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_50_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_50_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_50_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_50_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_50_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_50_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_50_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_50_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_50_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UVoxelBlendMeshesTool>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_75_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_75_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_75_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_75_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVoxelBlendMeshesToolBuilder(); \
	friend struct Z_Construct_UClass_UVoxelBlendMeshesToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UVoxelBlendMeshesToolBuilder, UBaseCreateFromSelectedToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UVoxelBlendMeshesToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_75_INCLASS \
private: \
	static void StaticRegisterNativesUVoxelBlendMeshesToolBuilder(); \
	friend struct Z_Construct_UClass_UVoxelBlendMeshesToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UVoxelBlendMeshesToolBuilder, UBaseCreateFromSelectedToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UVoxelBlendMeshesToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_75_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVoxelBlendMeshesToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVoxelBlendMeshesToolBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVoxelBlendMeshesToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVoxelBlendMeshesToolBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVoxelBlendMeshesToolBuilder(UVoxelBlendMeshesToolBuilder&&); \
	NO_API UVoxelBlendMeshesToolBuilder(const UVoxelBlendMeshesToolBuilder&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_75_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVoxelBlendMeshesToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVoxelBlendMeshesToolBuilder(UVoxelBlendMeshesToolBuilder&&); \
	NO_API UVoxelBlendMeshesToolBuilder(const UVoxelBlendMeshesToolBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVoxelBlendMeshesToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVoxelBlendMeshesToolBuilder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVoxelBlendMeshesToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_75_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_72_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_75_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_75_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_75_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_75_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_75_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_75_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_75_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_75_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_75_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_75_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_75_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h_75_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UVoxelBlendMeshesToolBuilder>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_VoxelBlendMeshesTool_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
