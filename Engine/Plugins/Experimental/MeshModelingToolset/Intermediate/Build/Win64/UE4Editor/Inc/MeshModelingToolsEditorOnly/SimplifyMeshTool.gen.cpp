// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingToolsEditorOnly/Public/SimplifyMeshTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSimplifyMeshTool() {}
// Cross Module References
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_USimplifyMeshToolBuilder_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_USimplifyMeshToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	UPackage* Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_USimplifyMeshToolProperties_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_USimplifyMeshToolProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshConstraintProperties();
	MODELINGOPERATORSEDITORONLY_API UEnum* Z_Construct_UEnum_ModelingOperatorsEditorOnly_ESimplifyType();
	MODELINGOPERATORSEDITORONLY_API UEnum* Z_Construct_UEnum_ModelingOperatorsEditorOnly_ESimplifyTargetType();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_USimplifyMeshTool_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_USimplifyMeshTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_USingleSelectionTool();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshStatisticsProperties_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_NoRegister();
// End Cross Module References
	void USimplifyMeshToolBuilder::StaticRegisterNativesUSimplifyMeshToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_USimplifyMeshToolBuilder_NoRegister()
	{
		return USimplifyMeshToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_USimplifyMeshToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USimplifyMeshToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USimplifyMeshToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "SimplifyMeshTool.h" },
		{ "ModuleRelativePath", "Public/SimplifyMeshTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USimplifyMeshToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USimplifyMeshToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USimplifyMeshToolBuilder_Statics::ClassParams = {
		&USimplifyMeshToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_USimplifyMeshToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USimplifyMeshToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USimplifyMeshToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USimplifyMeshToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USimplifyMeshToolBuilder, 1989448911);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<USimplifyMeshToolBuilder>()
	{
		return USimplifyMeshToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USimplifyMeshToolBuilder(Z_Construct_UClass_USimplifyMeshToolBuilder, &USimplifyMeshToolBuilder::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("USimplifyMeshToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USimplifyMeshToolBuilder);
	void USimplifyMeshToolProperties::StaticRegisterNativesUSimplifyMeshToolProperties()
	{
	}
	UClass* Z_Construct_UClass_USimplifyMeshToolProperties_NoRegister()
	{
		return USimplifyMeshToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_USimplifyMeshToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_SimplifierType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SimplifierType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_SimplifierType;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TargetMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TargetMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetPercentage_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_TargetPercentage;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetEdgeLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TargetEdgeLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_TargetCount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDiscardAttributes_MetaData[];
#endif
		static void NewProp_bDiscardAttributes_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDiscardAttributes;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowWireframe_MetaData[];
#endif
		static void NewProp_bShowWireframe_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowWireframe;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowGroupColors_MetaData[];
#endif
		static void NewProp_bShowGroupColors_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowGroupColors;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bReproject_MetaData[];
#endif
		static void NewProp_bReproject_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bReproject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USimplifyMeshToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshConstraintProperties,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USimplifyMeshToolProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Standard properties of the Simplify operation\n */" },
		{ "IncludePath", "SimplifyMeshTool.h" },
		{ "ModuleRelativePath", "Public/SimplifyMeshTool.h" },
		{ "ToolTip", "Standard properties of the Simplify operation" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_SimplifierType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_SimplifierType_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Simplification Scheme  */" },
		{ "ModuleRelativePath", "Public/SimplifyMeshTool.h" },
		{ "ToolTip", "Simplification Scheme" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_SimplifierType = { "SimplifierType", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USimplifyMeshToolProperties, SimplifierType), Z_Construct_UEnum_ModelingOperatorsEditorOnly_ESimplifyType, METADATA_PARAMS(Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_SimplifierType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_SimplifierType_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_TargetMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_TargetMode_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Simplification Target Type  */" },
		{ "ModuleRelativePath", "Public/SimplifyMeshTool.h" },
		{ "ToolTip", "Simplification Target Type" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_TargetMode = { "TargetMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USimplifyMeshToolProperties, TargetMode), Z_Construct_UEnum_ModelingOperatorsEditorOnly_ESimplifyTargetType, METADATA_PARAMS(Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_TargetMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_TargetMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_TargetPercentage_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Target percentage of original triangle count */" },
		{ "EditCondition", "TargetMode == ESimplifyTargetType::Percentage" },
		{ "ModuleRelativePath", "Public/SimplifyMeshTool.h" },
		{ "ToolTip", "Target percentage of original triangle count" },
		{ "UIMax", "100" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_TargetPercentage = { "TargetPercentage", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USimplifyMeshToolProperties, TargetPercentage), METADATA_PARAMS(Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_TargetPercentage_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_TargetPercentage_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_TargetEdgeLength_MetaData[] = {
		{ "Category", "Options" },
		{ "ClampMax", "1000.0" },
		{ "ClampMin", "0.001" },
		{ "Comment", "/** Target edge length */" },
		{ "EditCondition", "TargetMode == ESimplifyTargetType::EdgeLength && SimplifierType != ESimplifyType::UE4Standard" },
		{ "ModuleRelativePath", "Public/SimplifyMeshTool.h" },
		{ "ToolTip", "Target edge length" },
		{ "UIMax", "10.0" },
		{ "UIMin", "3.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_TargetEdgeLength = { "TargetEdgeLength", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USimplifyMeshToolProperties, TargetEdgeLength), METADATA_PARAMS(Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_TargetEdgeLength_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_TargetEdgeLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_TargetCount_MetaData[] = {
		{ "Category", "Options" },
		{ "ClampMax", "9999999999" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Target triangle count */" },
		{ "EditCondition", "TargetMode == ESimplifyTargetType::TriangleCount || TargetMode == ESimplifyTargetType::VertexCount" },
		{ "ModuleRelativePath", "Public/SimplifyMeshTool.h" },
		{ "ToolTip", "Target triangle count" },
		{ "UIMax", "10000" },
		{ "UIMin", "4" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_TargetCount = { "TargetCount", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USimplifyMeshToolProperties, TargetCount), METADATA_PARAMS(Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_TargetCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_TargetCount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_bDiscardAttributes_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** If true, UVs and Normals are discarded  */" },
		{ "ModuleRelativePath", "Public/SimplifyMeshTool.h" },
		{ "ToolTip", "If true, UVs and Normals are discarded" },
	};
#endif
	void Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_bDiscardAttributes_SetBit(void* Obj)
	{
		((USimplifyMeshToolProperties*)Obj)->bDiscardAttributes = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_bDiscardAttributes = { "bDiscardAttributes", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(USimplifyMeshToolProperties), &Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_bDiscardAttributes_SetBit, METADATA_PARAMS(Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_bDiscardAttributes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_bDiscardAttributes_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_bShowWireframe_MetaData[] = {
		{ "Category", "Display" },
		{ "Comment", "/** If true, display wireframe */" },
		{ "ModuleRelativePath", "Public/SimplifyMeshTool.h" },
		{ "ToolTip", "If true, display wireframe" },
	};
#endif
	void Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_bShowWireframe_SetBit(void* Obj)
	{
		((USimplifyMeshToolProperties*)Obj)->bShowWireframe = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_bShowWireframe = { "bShowWireframe", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(USimplifyMeshToolProperties), &Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_bShowWireframe_SetBit, METADATA_PARAMS(Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_bShowWireframe_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_bShowWireframe_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_bShowGroupColors_MetaData[] = {
		{ "Category", "Display" },
		{ "Comment", "/** Display colors corresponding to the mesh's polygon groups */" },
		{ "ModuleRelativePath", "Public/SimplifyMeshTool.h" },
		{ "ToolTip", "Display colors corresponding to the mesh's polygon groups" },
	};
#endif
	void Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_bShowGroupColors_SetBit(void* Obj)
	{
		((USimplifyMeshToolProperties*)Obj)->bShowGroupColors = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_bShowGroupColors = { "bShowGroupColors", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(USimplifyMeshToolProperties), &Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_bShowGroupColors_SetBit, METADATA_PARAMS(Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_bShowGroupColors_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_bShowGroupColors_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_bReproject_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Enable projection back to input mesh */" },
		{ "ModuleRelativePath", "Public/SimplifyMeshTool.h" },
		{ "ToolTip", "Enable projection back to input mesh" },
	};
#endif
	void Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_bReproject_SetBit(void* Obj)
	{
		((USimplifyMeshToolProperties*)Obj)->bReproject = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_bReproject = { "bReproject", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(USimplifyMeshToolProperties), &Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_bReproject_SetBit, METADATA_PARAMS(Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_bReproject_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_bReproject_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USimplifyMeshToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_SimplifierType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_SimplifierType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_TargetMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_TargetMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_TargetPercentage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_TargetEdgeLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_TargetCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_bDiscardAttributes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_bShowWireframe,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_bShowGroupColors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USimplifyMeshToolProperties_Statics::NewProp_bReproject,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USimplifyMeshToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USimplifyMeshToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USimplifyMeshToolProperties_Statics::ClassParams = {
		&USimplifyMeshToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USimplifyMeshToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USimplifyMeshToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_USimplifyMeshToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USimplifyMeshToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USimplifyMeshToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USimplifyMeshToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USimplifyMeshToolProperties, 1559260173);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<USimplifyMeshToolProperties>()
	{
		return USimplifyMeshToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USimplifyMeshToolProperties(Z_Construct_UClass_USimplifyMeshToolProperties, &USimplifyMeshToolProperties::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("USimplifyMeshToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USimplifyMeshToolProperties);
	void USimplifyMeshTool::StaticRegisterNativesUSimplifyMeshTool()
	{
	}
	UClass* Z_Construct_UClass_USimplifyMeshTool_NoRegister()
	{
		return USimplifyMeshTool::StaticClass();
	}
	struct Z_Construct_UClass_USimplifyMeshTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SimplifyProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SimplifyProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshStatisticsProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MeshStatisticsProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Preview_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Preview;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USimplifyMeshTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USingleSelectionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USimplifyMeshTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Simple Mesh Simplifying Tool\n */" },
		{ "IncludePath", "SimplifyMeshTool.h" },
		{ "ModuleRelativePath", "Public/SimplifyMeshTool.h" },
		{ "ToolTip", "Simple Mesh Simplifying Tool" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USimplifyMeshTool_Statics::NewProp_SimplifyProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/SimplifyMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USimplifyMeshTool_Statics::NewProp_SimplifyProperties = { "SimplifyProperties", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USimplifyMeshTool, SimplifyProperties), Z_Construct_UClass_USimplifyMeshToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USimplifyMeshTool_Statics::NewProp_SimplifyProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USimplifyMeshTool_Statics::NewProp_SimplifyProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USimplifyMeshTool_Statics::NewProp_MeshStatisticsProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/SimplifyMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USimplifyMeshTool_Statics::NewProp_MeshStatisticsProperties = { "MeshStatisticsProperties", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USimplifyMeshTool, MeshStatisticsProperties), Z_Construct_UClass_UMeshStatisticsProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USimplifyMeshTool_Statics::NewProp_MeshStatisticsProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USimplifyMeshTool_Statics::NewProp_MeshStatisticsProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USimplifyMeshTool_Statics::NewProp_Preview_MetaData[] = {
		{ "ModuleRelativePath", "Public/SimplifyMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USimplifyMeshTool_Statics::NewProp_Preview = { "Preview", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USimplifyMeshTool, Preview), Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USimplifyMeshTool_Statics::NewProp_Preview_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USimplifyMeshTool_Statics::NewProp_Preview_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USimplifyMeshTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USimplifyMeshTool_Statics::NewProp_SimplifyProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USimplifyMeshTool_Statics::NewProp_MeshStatisticsProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USimplifyMeshTool_Statics::NewProp_Preview,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USimplifyMeshTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USimplifyMeshTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USimplifyMeshTool_Statics::ClassParams = {
		&USimplifyMeshTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USimplifyMeshTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USimplifyMeshTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_USimplifyMeshTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USimplifyMeshTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USimplifyMeshTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USimplifyMeshTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USimplifyMeshTool, 489822960);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<USimplifyMeshTool>()
	{
		return USimplifyMeshTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USimplifyMeshTool(Z_Construct_UClass_USimplifyMeshTool, &USimplifyMeshTool::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("USimplifyMeshTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USimplifyMeshTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
