// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLS_ConvertToPolygonsTool_generated_h
#error "ConvertToPolygonsTool.generated.h already included, missing '#pragma once' in ConvertToPolygonsTool.h"
#endif
#define MESHMODELINGTOOLS_ConvertToPolygonsTool_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_23_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_23_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_23_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUConvertToPolygonsToolBuilder(); \
	friend struct Z_Construct_UClass_UConvertToPolygonsToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UConvertToPolygonsToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UConvertToPolygonsToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_23_INCLASS \
private: \
	static void StaticRegisterNativesUConvertToPolygonsToolBuilder(); \
	friend struct Z_Construct_UClass_UConvertToPolygonsToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UConvertToPolygonsToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UConvertToPolygonsToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UConvertToPolygonsToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UConvertToPolygonsToolBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConvertToPolygonsToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConvertToPolygonsToolBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConvertToPolygonsToolBuilder(UConvertToPolygonsToolBuilder&&); \
	NO_API UConvertToPolygonsToolBuilder(const UConvertToPolygonsToolBuilder&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_23_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UConvertToPolygonsToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConvertToPolygonsToolBuilder(UConvertToPolygonsToolBuilder&&); \
	NO_API UConvertToPolygonsToolBuilder(const UConvertToPolygonsToolBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConvertToPolygonsToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConvertToPolygonsToolBuilder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UConvertToPolygonsToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_23_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_20_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_23_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_23_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_23_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_23_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_23_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_23_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UConvertToPolygonsToolBuilder>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_47_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_47_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_47_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_47_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUConvertToPolygonsToolProperties(); \
	friend struct Z_Construct_UClass_UConvertToPolygonsToolProperties_Statics; \
public: \
	DECLARE_CLASS(UConvertToPolygonsToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UConvertToPolygonsToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_47_INCLASS \
private: \
	static void StaticRegisterNativesUConvertToPolygonsToolProperties(); \
	friend struct Z_Construct_UClass_UConvertToPolygonsToolProperties_Statics; \
public: \
	DECLARE_CLASS(UConvertToPolygonsToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UConvertToPolygonsToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_47_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UConvertToPolygonsToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UConvertToPolygonsToolProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConvertToPolygonsToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConvertToPolygonsToolProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConvertToPolygonsToolProperties(UConvertToPolygonsToolProperties&&); \
	NO_API UConvertToPolygonsToolProperties(const UConvertToPolygonsToolProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_47_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UConvertToPolygonsToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConvertToPolygonsToolProperties(UConvertToPolygonsToolProperties&&); \
	NO_API UConvertToPolygonsToolProperties(const UConvertToPolygonsToolProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConvertToPolygonsToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConvertToPolygonsToolProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UConvertToPolygonsToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_47_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_44_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_47_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_47_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_47_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_47_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_47_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_47_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_47_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_47_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_47_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_47_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_47_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_47_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UConvertToPolygonsToolProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_73_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_73_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_73_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_73_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUConvertToPolygonsTool(); \
	friend struct Z_Construct_UClass_UConvertToPolygonsTool_Statics; \
public: \
	DECLARE_CLASS(UConvertToPolygonsTool, USingleSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UConvertToPolygonsTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_73_INCLASS \
private: \
	static void StaticRegisterNativesUConvertToPolygonsTool(); \
	friend struct Z_Construct_UClass_UConvertToPolygonsTool_Statics; \
public: \
	DECLARE_CLASS(UConvertToPolygonsTool, USingleSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UConvertToPolygonsTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_73_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UConvertToPolygonsTool(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UConvertToPolygonsTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConvertToPolygonsTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConvertToPolygonsTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConvertToPolygonsTool(UConvertToPolygonsTool&&); \
	NO_API UConvertToPolygonsTool(const UConvertToPolygonsTool&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_73_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConvertToPolygonsTool(UConvertToPolygonsTool&&); \
	NO_API UConvertToPolygonsTool(const UConvertToPolygonsTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConvertToPolygonsTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConvertToPolygonsTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UConvertToPolygonsTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_73_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Settings() { return STRUCT_OFFSET(UConvertToPolygonsTool, Settings); } \
	FORCEINLINE static uint32 __PPO__PreviewMesh() { return STRUCT_OFFSET(UConvertToPolygonsTool, PreviewMesh); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_70_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_73_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_73_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_73_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_73_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_73_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_73_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_73_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_73_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_73_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_73_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_73_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h_73_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UConvertToPolygonsTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_ConvertToPolygonsTool_h


#define FOREACH_ENUM_ECONVERTTOPOLYGONSMODE(op) \
	op(EConvertToPolygonsMode::FaceNormalDeviation) \
	op(EConvertToPolygonsMode::FromUVISlands) 

enum class EConvertToPolygonsMode;
template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EConvertToPolygonsMode>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
