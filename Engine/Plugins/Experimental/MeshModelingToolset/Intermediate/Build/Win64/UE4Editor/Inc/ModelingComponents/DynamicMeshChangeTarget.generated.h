// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MODELINGCOMPONENTS_DynamicMeshChangeTarget_generated_h
#error "DynamicMeshChangeTarget.generated.h already included, missing '#pragma once' in DynamicMeshChangeTarget.h"
#endif
#define MODELINGCOMPONENTS_DynamicMeshChangeTarget_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_DynamicMeshChangeTarget_h_25_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_DynamicMeshChangeTarget_h_25_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_DynamicMeshChangeTarget_h_25_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_DynamicMeshChangeTarget_h_25_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDynamicMeshReplacementChangeTarget(); \
	friend struct Z_Construct_UClass_UDynamicMeshReplacementChangeTarget_Statics; \
public: \
	DECLARE_CLASS(UDynamicMeshReplacementChangeTarget, UObject, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UDynamicMeshReplacementChangeTarget) \
	virtual UObject* _getUObject() const override { return const_cast<UDynamicMeshReplacementChangeTarget*>(this); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_DynamicMeshChangeTarget_h_25_INCLASS \
private: \
	static void StaticRegisterNativesUDynamicMeshReplacementChangeTarget(); \
	friend struct Z_Construct_UClass_UDynamicMeshReplacementChangeTarget_Statics; \
public: \
	DECLARE_CLASS(UDynamicMeshReplacementChangeTarget, UObject, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UDynamicMeshReplacementChangeTarget) \
	virtual UObject* _getUObject() const override { return const_cast<UDynamicMeshReplacementChangeTarget*>(this); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_DynamicMeshChangeTarget_h_25_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDynamicMeshReplacementChangeTarget(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDynamicMeshReplacementChangeTarget) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDynamicMeshReplacementChangeTarget); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDynamicMeshReplacementChangeTarget); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDynamicMeshReplacementChangeTarget(UDynamicMeshReplacementChangeTarget&&); \
	NO_API UDynamicMeshReplacementChangeTarget(const UDynamicMeshReplacementChangeTarget&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_DynamicMeshChangeTarget_h_25_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDynamicMeshReplacementChangeTarget(UDynamicMeshReplacementChangeTarget&&); \
	NO_API UDynamicMeshReplacementChangeTarget(const UDynamicMeshReplacementChangeTarget&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDynamicMeshReplacementChangeTarget); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDynamicMeshReplacementChangeTarget); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDynamicMeshReplacementChangeTarget)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_DynamicMeshChangeTarget_h_25_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_DynamicMeshChangeTarget_h_22_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_DynamicMeshChangeTarget_h_25_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_DynamicMeshChangeTarget_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_DynamicMeshChangeTarget_h_25_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_DynamicMeshChangeTarget_h_25_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_DynamicMeshChangeTarget_h_25_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_DynamicMeshChangeTarget_h_25_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_DynamicMeshChangeTarget_h_25_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_DynamicMeshChangeTarget_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_DynamicMeshChangeTarget_h_25_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_DynamicMeshChangeTarget_h_25_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_DynamicMeshChangeTarget_h_25_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_DynamicMeshChangeTarget_h_25_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MODELINGCOMPONENTS_API UClass* StaticClass<class UDynamicMeshReplacementChangeTarget>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_DynamicMeshChangeTarget_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
