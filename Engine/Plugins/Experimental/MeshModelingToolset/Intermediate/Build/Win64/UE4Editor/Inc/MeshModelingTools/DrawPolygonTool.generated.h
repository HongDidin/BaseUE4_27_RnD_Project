// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLS_DrawPolygonTool_generated_h
#error "DrawPolygonTool.generated.h already included, missing '#pragma once' in DrawPolygonTool.h"
#endif
#define MESHMODELINGTOOLS_DrawPolygonTool_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_28_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_28_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_28_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_28_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDrawPolygonToolBuilder(); \
	friend struct Z_Construct_UClass_UDrawPolygonToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UDrawPolygonToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UDrawPolygonToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_28_INCLASS \
private: \
	static void StaticRegisterNativesUDrawPolygonToolBuilder(); \
	friend struct Z_Construct_UClass_UDrawPolygonToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UDrawPolygonToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UDrawPolygonToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_28_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDrawPolygonToolBuilder(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDrawPolygonToolBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDrawPolygonToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDrawPolygonToolBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDrawPolygonToolBuilder(UDrawPolygonToolBuilder&&); \
	NO_API UDrawPolygonToolBuilder(const UDrawPolygonToolBuilder&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_28_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDrawPolygonToolBuilder(UDrawPolygonToolBuilder&&); \
	NO_API UDrawPolygonToolBuilder(const UDrawPolygonToolBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDrawPolygonToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDrawPolygonToolBuilder); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDrawPolygonToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_28_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_25_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_28_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_28_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_28_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_28_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_28_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_28_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_28_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_28_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_28_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_28_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_28_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_28_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UDrawPolygonToolBuilder>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_90_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_90_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_90_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_90_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDrawPolygonToolStandardProperties(); \
	friend struct Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics; \
public: \
	DECLARE_CLASS(UDrawPolygonToolStandardProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UDrawPolygonToolStandardProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_90_INCLASS \
private: \
	static void StaticRegisterNativesUDrawPolygonToolStandardProperties(); \
	friend struct Z_Construct_UClass_UDrawPolygonToolStandardProperties_Statics; \
public: \
	DECLARE_CLASS(UDrawPolygonToolStandardProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UDrawPolygonToolStandardProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_90_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDrawPolygonToolStandardProperties(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDrawPolygonToolStandardProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDrawPolygonToolStandardProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDrawPolygonToolStandardProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDrawPolygonToolStandardProperties(UDrawPolygonToolStandardProperties&&); \
	NO_API UDrawPolygonToolStandardProperties(const UDrawPolygonToolStandardProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_90_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDrawPolygonToolStandardProperties(UDrawPolygonToolStandardProperties&&); \
	NO_API UDrawPolygonToolStandardProperties(const UDrawPolygonToolStandardProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDrawPolygonToolStandardProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDrawPolygonToolStandardProperties); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDrawPolygonToolStandardProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_90_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_87_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_90_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_90_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_90_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_90_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_90_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_90_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_90_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_90_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_90_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_90_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_90_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_90_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UDrawPolygonToolStandardProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_126_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_126_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_126_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_126_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDrawPolygonToolSnapProperties(); \
	friend struct Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics; \
public: \
	DECLARE_CLASS(UDrawPolygonToolSnapProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UDrawPolygonToolSnapProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_126_INCLASS \
private: \
	static void StaticRegisterNativesUDrawPolygonToolSnapProperties(); \
	friend struct Z_Construct_UClass_UDrawPolygonToolSnapProperties_Statics; \
public: \
	DECLARE_CLASS(UDrawPolygonToolSnapProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UDrawPolygonToolSnapProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_126_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDrawPolygonToolSnapProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDrawPolygonToolSnapProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDrawPolygonToolSnapProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDrawPolygonToolSnapProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDrawPolygonToolSnapProperties(UDrawPolygonToolSnapProperties&&); \
	NO_API UDrawPolygonToolSnapProperties(const UDrawPolygonToolSnapProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_126_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDrawPolygonToolSnapProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDrawPolygonToolSnapProperties(UDrawPolygonToolSnapProperties&&); \
	NO_API UDrawPolygonToolSnapProperties(const UDrawPolygonToolSnapProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDrawPolygonToolSnapProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDrawPolygonToolSnapProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDrawPolygonToolSnapProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_126_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_123_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_126_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_126_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_126_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_126_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_126_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_126_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_126_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_126_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_126_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_126_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_126_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_126_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UDrawPolygonToolSnapProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_167_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_167_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_167_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_167_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDrawPolygonTool(); \
	friend struct Z_Construct_UClass_UDrawPolygonTool_Statics; \
public: \
	DECLARE_CLASS(UDrawPolygonTool, UInteractiveTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UDrawPolygonTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_167_INCLASS \
private: \
	static void StaticRegisterNativesUDrawPolygonTool(); \
	friend struct Z_Construct_UClass_UDrawPolygonTool_Statics; \
public: \
	DECLARE_CLASS(UDrawPolygonTool, UInteractiveTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UDrawPolygonTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_167_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDrawPolygonTool(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDrawPolygonTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDrawPolygonTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDrawPolygonTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDrawPolygonTool(UDrawPolygonTool&&); \
	NO_API UDrawPolygonTool(const UDrawPolygonTool&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_167_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDrawPolygonTool(UDrawPolygonTool&&); \
	NO_API UDrawPolygonTool(const UDrawPolygonTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDrawPolygonTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDrawPolygonTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDrawPolygonTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_167_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__PolygonProperties() { return STRUCT_OFFSET(UDrawPolygonTool, PolygonProperties); } \
	FORCEINLINE static uint32 __PPO__SnapProperties() { return STRUCT_OFFSET(UDrawPolygonTool, SnapProperties); } \
	FORCEINLINE static uint32 __PPO__MaterialProperties() { return STRUCT_OFFSET(UDrawPolygonTool, MaterialProperties); } \
	FORCEINLINE static uint32 __PPO__PreviewMesh() { return STRUCT_OFFSET(UDrawPolygonTool, PreviewMesh); } \
	FORCEINLINE static uint32 __PPO__PlaneTransformGizmo() { return STRUCT_OFFSET(UDrawPolygonTool, PlaneTransformGizmo); } \
	FORCEINLINE static uint32 __PPO__PlaneTransformProxy() { return STRUCT_OFFSET(UDrawPolygonTool, PlaneTransformProxy); } \
	FORCEINLINE static uint32 __PPO__HeightMechanic() { return STRUCT_OFFSET(UDrawPolygonTool, HeightMechanic); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_164_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_167_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_167_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_167_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_167_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_167_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_167_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_167_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_167_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_167_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_167_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_167_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h_167_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UDrawPolygonTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawPolygonTool_h


#define FOREACH_ENUM_EDRAWPOLYGONOUTPUTMODE(op) \
	op(EDrawPolygonOutputMode::MeshedPolygon) \
	op(EDrawPolygonOutputMode::ExtrudedConstant) \
	op(EDrawPolygonOutputMode::ExtrudedInteractive) 

enum class EDrawPolygonOutputMode : uint8;
template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EDrawPolygonOutputMode>();

#define FOREACH_ENUM_EDRAWPOLYGONDRAWMODE(op) \
	op(EDrawPolygonDrawMode::Freehand) \
	op(EDrawPolygonDrawMode::Circle) \
	op(EDrawPolygonDrawMode::Square) \
	op(EDrawPolygonDrawMode::Rectangle) \
	op(EDrawPolygonDrawMode::RoundedRectangle) \
	op(EDrawPolygonDrawMode::HoleyCircle) 

enum class EDrawPolygonDrawMode : uint8;
template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EDrawPolygonDrawMode>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
