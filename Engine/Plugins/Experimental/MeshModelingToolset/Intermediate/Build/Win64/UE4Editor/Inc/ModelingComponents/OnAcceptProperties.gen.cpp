// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ModelingComponents/Public/PropertySets/OnAcceptProperties.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOnAcceptProperties() {}
// Cross Module References
	MODELINGCOMPONENTS_API UEnum* Z_Construct_UEnum_ModelingComponents_EHandleSourcesMethod();
	UPackage* Z_Construct_UPackage__Script_ModelingComponents();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UOnAcceptHandleSourcesProperties_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UOnAcceptHandleSourcesProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
// End Cross Module References
	static UEnum* EHandleSourcesMethod_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ModelingComponents_EHandleSourcesMethod, Z_Construct_UPackage__Script_ModelingComponents(), TEXT("EHandleSourcesMethod"));
		}
		return Singleton;
	}
	template<> MODELINGCOMPONENTS_API UEnum* StaticEnum<EHandleSourcesMethod>()
	{
		return EHandleSourcesMethod_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EHandleSourcesMethod(EHandleSourcesMethod_StaticEnum, TEXT("/Script/ModelingComponents"), TEXT("EHandleSourcesMethod"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ModelingComponents_EHandleSourcesMethod_Hash() { return 3968979867U; }
	UEnum* Z_Construct_UEnum_ModelingComponents_EHandleSourcesMethod()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ModelingComponents();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EHandleSourcesMethod"), 0, Get_Z_Construct_UEnum_ModelingComponents_EHandleSourcesMethod_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EHandleSourcesMethod::DeleteSources", (int64)EHandleSourcesMethod::DeleteSources },
				{ "EHandleSourcesMethod::HideSources", (int64)EHandleSourcesMethod::HideSources },
				{ "EHandleSourcesMethod::KeepSources", (int64)EHandleSourcesMethod::KeepSources },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** Options to handle source meshes */" },
				{ "DeleteSources.DisplayName", "Delete Sources" },
				{ "DeleteSources.Name", "EHandleSourcesMethod::DeleteSources" },
				{ "HideSources.DisplayName", "Hide Sources" },
				{ "HideSources.Name", "EHandleSourcesMethod::HideSources" },
				{ "KeepSources.DisplayName", "Keep Sources" },
				{ "KeepSources.Name", "EHandleSourcesMethod::KeepSources" },
				{ "ModuleRelativePath", "Public/PropertySets/OnAcceptProperties.h" },
				{ "ToolTip", "Options to handle source meshes" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ModelingComponents,
				nullptr,
				"EHandleSourcesMethod",
				"EHandleSourcesMethod",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UOnAcceptHandleSourcesProperties::StaticRegisterNativesUOnAcceptHandleSourcesProperties()
	{
	}
	UClass* Z_Construct_UClass_UOnAcceptHandleSourcesProperties_NoRegister()
	{
		return UOnAcceptHandleSourcesProperties::StaticClass();
	}
	struct Z_Construct_UClass_UOnAcceptHandleSourcesProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_OnToolAccept_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnToolAccept_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_OnToolAccept;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOnAcceptHandleSourcesProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_ModelingComponents,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOnAcceptHandleSourcesProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "// Standard property settings for tools that create a new actor and need to decide what to do with the input (source) actor(s)\n" },
		{ "IncludePath", "PropertySets/OnAcceptProperties.h" },
		{ "ModuleRelativePath", "Public/PropertySets/OnAcceptProperties.h" },
		{ "ToolTip", "Standard property settings for tools that create a new actor and need to decide what to do with the input (source) actor(s)" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UOnAcceptHandleSourcesProperties_Statics::NewProp_OnToolAccept_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOnAcceptHandleSourcesProperties_Statics::NewProp_OnToolAccept_MetaData[] = {
		{ "Category", "ToolOutputOptions" },
		{ "Comment", "/** What to do with the source Actors/Components when accepting results of tool.*/" },
		{ "ModuleRelativePath", "Public/PropertySets/OnAcceptProperties.h" },
		{ "ToolTip", "What to do with the source Actors/Components when accepting results of tool." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UOnAcceptHandleSourcesProperties_Statics::NewProp_OnToolAccept = { "OnToolAccept", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOnAcceptHandleSourcesProperties, OnToolAccept), Z_Construct_UEnum_ModelingComponents_EHandleSourcesMethod, METADATA_PARAMS(Z_Construct_UClass_UOnAcceptHandleSourcesProperties_Statics::NewProp_OnToolAccept_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOnAcceptHandleSourcesProperties_Statics::NewProp_OnToolAccept_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UOnAcceptHandleSourcesProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOnAcceptHandleSourcesProperties_Statics::NewProp_OnToolAccept_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOnAcceptHandleSourcesProperties_Statics::NewProp_OnToolAccept,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOnAcceptHandleSourcesProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOnAcceptHandleSourcesProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOnAcceptHandleSourcesProperties_Statics::ClassParams = {
		&UOnAcceptHandleSourcesProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UOnAcceptHandleSourcesProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UOnAcceptHandleSourcesProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UOnAcceptHandleSourcesProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UOnAcceptHandleSourcesProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOnAcceptHandleSourcesProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOnAcceptHandleSourcesProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOnAcceptHandleSourcesProperties, 3418409914);
	template<> MODELINGCOMPONENTS_API UClass* StaticClass<UOnAcceptHandleSourcesProperties>()
	{
		return UOnAcceptHandleSourcesProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOnAcceptHandleSourcesProperties(Z_Construct_UClass_UOnAcceptHandleSourcesProperties, &UOnAcceptHandleSourcesProperties::StaticClass, TEXT("/Script/ModelingComponents"), TEXT("UOnAcceptHandleSourcesProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOnAcceptHandleSourcesProperties);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
