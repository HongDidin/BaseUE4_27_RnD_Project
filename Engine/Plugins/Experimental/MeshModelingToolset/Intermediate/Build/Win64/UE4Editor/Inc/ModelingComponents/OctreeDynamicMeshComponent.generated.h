// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MODELINGCOMPONENTS_OctreeDynamicMeshComponent_generated_h
#error "OctreeDynamicMeshComponent.generated.h already included, missing '#pragma once' in OctreeDynamicMeshComponent.h"
#endif
#define MODELINGCOMPONENTS_OctreeDynamicMeshComponent_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_OctreeDynamicMeshComponent_h_38_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_OctreeDynamicMeshComponent_h_38_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_OctreeDynamicMeshComponent_h_38_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_OctreeDynamicMeshComponent_h_38_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOctreeDynamicMeshComponent(); \
	friend struct Z_Construct_UClass_UOctreeDynamicMeshComponent_Statics; \
public: \
	DECLARE_CLASS(UOctreeDynamicMeshComponent, UBaseDynamicMeshComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UOctreeDynamicMeshComponent)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_OctreeDynamicMeshComponent_h_38_INCLASS \
private: \
	static void StaticRegisterNativesUOctreeDynamicMeshComponent(); \
	friend struct Z_Construct_UClass_UOctreeDynamicMeshComponent_Statics; \
public: \
	DECLARE_CLASS(UOctreeDynamicMeshComponent, UBaseDynamicMeshComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UOctreeDynamicMeshComponent)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_OctreeDynamicMeshComponent_h_38_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOctreeDynamicMeshComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOctreeDynamicMeshComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOctreeDynamicMeshComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOctreeDynamicMeshComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOctreeDynamicMeshComponent(UOctreeDynamicMeshComponent&&); \
	NO_API UOctreeDynamicMeshComponent(const UOctreeDynamicMeshComponent&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_OctreeDynamicMeshComponent_h_38_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOctreeDynamicMeshComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOctreeDynamicMeshComponent(UOctreeDynamicMeshComponent&&); \
	NO_API UOctreeDynamicMeshComponent(const UOctreeDynamicMeshComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOctreeDynamicMeshComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOctreeDynamicMeshComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOctreeDynamicMeshComponent)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_OctreeDynamicMeshComponent_h_38_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_OctreeDynamicMeshComponent_h_35_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_OctreeDynamicMeshComponent_h_38_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_OctreeDynamicMeshComponent_h_38_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_OctreeDynamicMeshComponent_h_38_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_OctreeDynamicMeshComponent_h_38_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_OctreeDynamicMeshComponent_h_38_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_OctreeDynamicMeshComponent_h_38_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_OctreeDynamicMeshComponent_h_38_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_OctreeDynamicMeshComponent_h_38_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_OctreeDynamicMeshComponent_h_38_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_OctreeDynamicMeshComponent_h_38_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_OctreeDynamicMeshComponent_h_38_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_OctreeDynamicMeshComponent_h_38_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class OctreeDynamicMeshComponent."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MODELINGCOMPONENTS_API UClass* StaticClass<class UOctreeDynamicMeshComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_OctreeDynamicMeshComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
