// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingToolsEditorOnly/Public/MeshToVolumeTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMeshToVolumeTool() {}
// Cross Module References
	MESHMODELINGTOOLSEDITORONLY_API UEnum* Z_Construct_UEnum_MeshModelingToolsEditorOnly_EMeshToVolumeMode();
	UPackage* Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UMeshToVolumeToolBuilder_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UMeshToVolumeToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UMeshToVolumeToolProperties_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UMeshToVolumeToolProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	ENGINE_API UClass* Z_Construct_UClass_AVolume_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UMeshToVolumeTool_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UMeshToVolumeTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_USingleSelectionTool();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UOnAcceptHandleSourcesProperties_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPreviewMesh_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_ULineSetComponent_NoRegister();
// End Cross Module References
	static UEnum* EMeshToVolumeMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingToolsEditorOnly_EMeshToVolumeMode, Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly(), TEXT("EMeshToVolumeMode"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLSEDITORONLY_API UEnum* StaticEnum<EMeshToVolumeMode>()
	{
		return EMeshToVolumeMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMeshToVolumeMode(EMeshToVolumeMode_StaticEnum, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("EMeshToVolumeMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingToolsEditorOnly_EMeshToVolumeMode_Hash() { return 1297357073U; }
	UEnum* Z_Construct_UEnum_MeshModelingToolsEditorOnly_EMeshToVolumeMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMeshToVolumeMode"), 0, Get_Z_Construct_UEnum_MeshModelingToolsEditorOnly_EMeshToVolumeMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMeshToVolumeMode::TriangulatePolygons", (int64)EMeshToVolumeMode::TriangulatePolygons },
				{ "EMeshToVolumeMode::MinimalPolygons", (int64)EMeshToVolumeMode::MinimalPolygons },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "MinimalPolygons.Comment", "/** Create Volume Faces based on Planar Polygons */" },
				{ "MinimalPolygons.Name", "EMeshToVolumeMode::MinimalPolygons" },
				{ "MinimalPolygons.ToolTip", "Create Volume Faces based on Planar Polygons" },
				{ "ModuleRelativePath", "Public/MeshToVolumeTool.h" },
				{ "TriangulatePolygons.Comment", "/** Create a separate Volume Face for each Triangle */" },
				{ "TriangulatePolygons.Name", "EMeshToVolumeMode::TriangulatePolygons" },
				{ "TriangulatePolygons.ToolTip", "Create a separate Volume Face for each Triangle" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
				nullptr,
				"EMeshToVolumeMode",
				"EMeshToVolumeMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UMeshToVolumeToolBuilder::StaticRegisterNativesUMeshToVolumeToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UMeshToVolumeToolBuilder_NoRegister()
	{
		return UMeshToVolumeToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UMeshToVolumeToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshToVolumeToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshToVolumeToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "MeshToVolumeTool.h" },
		{ "ModuleRelativePath", "Public/MeshToVolumeTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshToVolumeToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshToVolumeToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshToVolumeToolBuilder_Statics::ClassParams = {
		&UMeshToVolumeToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshToVolumeToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshToVolumeToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshToVolumeToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshToVolumeToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshToVolumeToolBuilder, 2598016830);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UMeshToVolumeToolBuilder>()
	{
		return UMeshToVolumeToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshToVolumeToolBuilder(Z_Construct_UClass_UMeshToVolumeToolBuilder, &UMeshToVolumeToolBuilder::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UMeshToVolumeToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshToVolumeToolBuilder);
	void UMeshToVolumeToolProperties::StaticRegisterNativesUMeshToVolumeToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UMeshToVolumeToolProperties_NoRegister()
	{
		return UMeshToVolumeToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UMeshToVolumeToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_ConversionMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConversionMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ConversionMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewVolumeType_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_NewVolumeType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetVolume_MetaData[];
#endif
		static const UE4CodeGen_Private::FLazyObjectPropertyParams NewProp_TargetVolume;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshToVolumeToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshToVolumeToolProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshToVolumeTool.h" },
		{ "ModuleRelativePath", "Public/MeshToVolumeTool.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UMeshToVolumeToolProperties_Statics::NewProp_ConversionMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshToVolumeToolProperties_Statics::NewProp_ConversionMode_MetaData[] = {
		{ "Category", "ConversionOptions" },
		{ "Comment", "/** Method for converting the input mesh to a set of Planar Polygonal Faces in the output Volume. */" },
		{ "ModuleRelativePath", "Public/MeshToVolumeTool.h" },
		{ "ToolTip", "Method for converting the input mesh to a set of Planar Polygonal Faces in the output Volume." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UMeshToVolumeToolProperties_Statics::NewProp_ConversionMode = { "ConversionMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshToVolumeToolProperties, ConversionMode), Z_Construct_UEnum_MeshModelingToolsEditorOnly_EMeshToVolumeMode, METADATA_PARAMS(Z_Construct_UClass_UMeshToVolumeToolProperties_Statics::NewProp_ConversionMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshToVolumeToolProperties_Statics::NewProp_ConversionMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshToVolumeToolProperties_Statics::NewProp_NewVolumeType_MetaData[] = {
		{ "Category", "NewVolume" },
		{ "Comment", "/** Type of new Volume to create on Accept */" },
		{ "EditCondition", "TargetVolume == nullptr" },
		{ "ModuleRelativePath", "Public/MeshToVolumeTool.h" },
		{ "ToolTip", "Type of new Volume to create on Accept" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UMeshToVolumeToolProperties_Statics::NewProp_NewVolumeType = { "NewVolumeType", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshToVolumeToolProperties, NewVolumeType), Z_Construct_UClass_AVolume_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_UMeshToVolumeToolProperties_Statics::NewProp_NewVolumeType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshToVolumeToolProperties_Statics::NewProp_NewVolumeType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshToVolumeToolProperties_Statics::NewProp_TargetVolume_MetaData[] = {
		{ "Category", "UpdateExisting" },
		{ "Comment", "/** If set, the target Volume will be updated, rather than creating a new Volume. */" },
		{ "ModuleRelativePath", "Public/MeshToVolumeTool.h" },
		{ "ToolTip", "If set, the target Volume will be updated, rather than creating a new Volume." },
	};
#endif
	const UE4CodeGen_Private::FLazyObjectPropertyParams Z_Construct_UClass_UMeshToVolumeToolProperties_Statics::NewProp_TargetVolume = { "TargetVolume", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::LazyObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshToVolumeToolProperties, TargetVolume), Z_Construct_UClass_AVolume_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshToVolumeToolProperties_Statics::NewProp_TargetVolume_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshToVolumeToolProperties_Statics::NewProp_TargetVolume_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshToVolumeToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshToVolumeToolProperties_Statics::NewProp_ConversionMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshToVolumeToolProperties_Statics::NewProp_ConversionMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshToVolumeToolProperties_Statics::NewProp_NewVolumeType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshToVolumeToolProperties_Statics::NewProp_TargetVolume,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshToVolumeToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshToVolumeToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshToVolumeToolProperties_Statics::ClassParams = {
		&UMeshToVolumeToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshToVolumeToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshToVolumeToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshToVolumeToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshToVolumeToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshToVolumeToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshToVolumeToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshToVolumeToolProperties, 640395823);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UMeshToVolumeToolProperties>()
	{
		return UMeshToVolumeToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshToVolumeToolProperties(Z_Construct_UClass_UMeshToVolumeToolProperties, &UMeshToVolumeToolProperties::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UMeshToVolumeToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshToVolumeToolProperties);
	void UMeshToVolumeTool::StaticRegisterNativesUMeshToVolumeTool()
	{
	}
	UClass* Z_Construct_UClass_UMeshToVolumeTool_NoRegister()
	{
		return UMeshToVolumeTool::StaticClass();
	}
	struct Z_Construct_UClass_UMeshToVolumeTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Settings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Settings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HandleSourcesProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_HandleSourcesProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviewMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviewMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VolumeEdgesSet_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_VolumeEdgesSet;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshToVolumeTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USingleSelectionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshToVolumeTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "MeshToVolumeTool.h" },
		{ "ModuleRelativePath", "Public/MeshToVolumeTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshToVolumeTool_Statics::NewProp_Settings_MetaData[] = {
		{ "ModuleRelativePath", "Public/MeshToVolumeTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshToVolumeTool_Statics::NewProp_Settings = { "Settings", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshToVolumeTool, Settings), Z_Construct_UClass_UMeshToVolumeToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshToVolumeTool_Statics::NewProp_Settings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshToVolumeTool_Statics::NewProp_Settings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshToVolumeTool_Statics::NewProp_HandleSourcesProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/MeshToVolumeTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshToVolumeTool_Statics::NewProp_HandleSourcesProperties = { "HandleSourcesProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshToVolumeTool, HandleSourcesProperties), Z_Construct_UClass_UOnAcceptHandleSourcesProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshToVolumeTool_Statics::NewProp_HandleSourcesProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshToVolumeTool_Statics::NewProp_HandleSourcesProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshToVolumeTool_Statics::NewProp_PreviewMesh_MetaData[] = {
		{ "ModuleRelativePath", "Public/MeshToVolumeTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshToVolumeTool_Statics::NewProp_PreviewMesh = { "PreviewMesh", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshToVolumeTool, PreviewMesh), Z_Construct_UClass_UPreviewMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshToVolumeTool_Statics::NewProp_PreviewMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshToVolumeTool_Statics::NewProp_PreviewMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshToVolumeTool_Statics::NewProp_VolumeEdgesSet_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/MeshToVolumeTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshToVolumeTool_Statics::NewProp_VolumeEdgesSet = { "VolumeEdgesSet", nullptr, (EPropertyFlags)0x0020080000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshToVolumeTool, VolumeEdgesSet), Z_Construct_UClass_ULineSetComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshToVolumeTool_Statics::NewProp_VolumeEdgesSet_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshToVolumeTool_Statics::NewProp_VolumeEdgesSet_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshToVolumeTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshToVolumeTool_Statics::NewProp_Settings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshToVolumeTool_Statics::NewProp_HandleSourcesProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshToVolumeTool_Statics::NewProp_PreviewMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshToVolumeTool_Statics::NewProp_VolumeEdgesSet,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshToVolumeTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshToVolumeTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshToVolumeTool_Statics::ClassParams = {
		&UMeshToVolumeTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshToVolumeTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshToVolumeTool_Statics::PropPointers),
		0,
		0x009000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshToVolumeTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshToVolumeTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshToVolumeTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshToVolumeTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshToVolumeTool, 2575582758);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UMeshToVolumeTool>()
	{
		return UMeshToVolumeTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshToVolumeTool(Z_Construct_UClass_UMeshToVolumeTool, &UMeshToVolumeTool::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UMeshToVolumeTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshToVolumeTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
