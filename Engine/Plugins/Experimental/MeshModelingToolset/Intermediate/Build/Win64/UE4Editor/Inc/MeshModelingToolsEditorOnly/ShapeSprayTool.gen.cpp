// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingToolsEditorOnly/Public/ShapeSprayTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeShapeSprayTool() {}
// Cross Module References
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UShapeSprayToolBuilder_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UShapeSprayToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UMeshSurfacePointToolBuilder();
	UPackage* Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UShapeSprayToolProperties_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UShapeSprayToolProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UShapeSprayTool_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UShapeSprayTool();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDynamicMeshBrushTool();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_USimpleDynamicMeshComponent_NoRegister();
// End Cross Module References
	void UShapeSprayToolBuilder::StaticRegisterNativesUShapeSprayToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UShapeSprayToolBuilder_NoRegister()
	{
		return UShapeSprayToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UShapeSprayToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UShapeSprayToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshSurfacePointToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UShapeSprayToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * UMeshSurfacePointToolBuilder override for UShapeSprayTool\n */" },
		{ "IncludePath", "ShapeSprayTool.h" },
		{ "ModuleRelativePath", "Public/ShapeSprayTool.h" },
		{ "ToolTip", "UMeshSurfacePointToolBuilder override for UShapeSprayTool" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UShapeSprayToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UShapeSprayToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UShapeSprayToolBuilder_Statics::ClassParams = {
		&UShapeSprayToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UShapeSprayToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UShapeSprayToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UShapeSprayToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UShapeSprayToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UShapeSprayToolBuilder, 3541318468);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UShapeSprayToolBuilder>()
	{
		return UShapeSprayToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UShapeSprayToolBuilder(Z_Construct_UClass_UShapeSprayToolBuilder, &UShapeSprayToolBuilder::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UShapeSprayToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UShapeSprayToolBuilder);
	void UShapeSprayToolProperties::StaticRegisterNativesUShapeSprayToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UShapeSprayToolProperties_NoRegister()
	{
		return UShapeSprayToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UShapeSprayToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Color_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Color;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRandomColor_MetaData[];
#endif
		static void NewProp_bRandomColor_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRandomColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DropSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DropSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObjectSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ObjectSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumSplats_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_NumSplats;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Material_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Material;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UShapeSprayToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UShapeSprayToolProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Settings UObject for UShapeSprayTool. \n */" },
		{ "IncludePath", "ShapeSprayTool.h" },
		{ "ModuleRelativePath", "Public/ShapeSprayTool.h" },
		{ "ToolTip", "Settings UObject for UShapeSprayTool." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UShapeSprayToolProperties_Statics::NewProp_Color_MetaData[] = {
		{ "Category", "Options" },
		{ "ModuleRelativePath", "Public/ShapeSprayTool.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UShapeSprayToolProperties_Statics::NewProp_Color = { "Color", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UShapeSprayToolProperties, Color), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UClass_UShapeSprayToolProperties_Statics::NewProp_Color_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UShapeSprayToolProperties_Statics::NewProp_Color_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UShapeSprayToolProperties_Statics::NewProp_bRandomColor_MetaData[] = {
		{ "Category", "Options" },
		{ "ModuleRelativePath", "Public/ShapeSprayTool.h" },
	};
#endif
	void Z_Construct_UClass_UShapeSprayToolProperties_Statics::NewProp_bRandomColor_SetBit(void* Obj)
	{
		((UShapeSprayToolProperties*)Obj)->bRandomColor = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UShapeSprayToolProperties_Statics::NewProp_bRandomColor = { "bRandomColor", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UShapeSprayToolProperties), &Z_Construct_UClass_UShapeSprayToolProperties_Statics::NewProp_bRandomColor_SetBit, METADATA_PARAMS(Z_Construct_UClass_UShapeSprayToolProperties_Statics::NewProp_bRandomColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UShapeSprayToolProperties_Statics::NewProp_bRandomColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UShapeSprayToolProperties_Statics::NewProp_DropSpeed_MetaData[] = {
		{ "Category", "Options" },
		{ "DisplayName", "Speed" },
		{ "ModuleRelativePath", "Public/ShapeSprayTool.h" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.01" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UShapeSprayToolProperties_Statics::NewProp_DropSpeed = { "DropSpeed", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UShapeSprayToolProperties, DropSpeed), METADATA_PARAMS(Z_Construct_UClass_UShapeSprayToolProperties_Statics::NewProp_DropSpeed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UShapeSprayToolProperties_Statics::NewProp_DropSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UShapeSprayToolProperties_Statics::NewProp_ObjectSize_MetaData[] = {
		{ "Category", "Options" },
		{ "DisplayName", "Shape Size" },
		{ "ModuleRelativePath", "Public/ShapeSprayTool.h" },
		{ "UIMax", "30.0" },
		{ "UIMin", "1.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UShapeSprayToolProperties_Statics::NewProp_ObjectSize = { "ObjectSize", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UShapeSprayToolProperties, ObjectSize), METADATA_PARAMS(Z_Construct_UClass_UShapeSprayToolProperties_Statics::NewProp_ObjectSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UShapeSprayToolProperties_Statics::NewProp_ObjectSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UShapeSprayToolProperties_Statics::NewProp_NumSplats_MetaData[] = {
		{ "Category", "Options" },
		{ "DisplayName", "Repeat Per Stamp" },
		{ "ModuleRelativePath", "Public/ShapeSprayTool.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UShapeSprayToolProperties_Statics::NewProp_NumSplats = { "NumSplats", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UShapeSprayToolProperties, NumSplats), METADATA_PARAMS(Z_Construct_UClass_UShapeSprayToolProperties_Statics::NewProp_NumSplats_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UShapeSprayToolProperties_Statics::NewProp_NumSplats_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UShapeSprayToolProperties_Statics::NewProp_Material_MetaData[] = {
		{ "Category", "Options" },
		{ "ModuleRelativePath", "Public/ShapeSprayTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UShapeSprayToolProperties_Statics::NewProp_Material = { "Material", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UShapeSprayToolProperties, Material), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UShapeSprayToolProperties_Statics::NewProp_Material_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UShapeSprayToolProperties_Statics::NewProp_Material_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UShapeSprayToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UShapeSprayToolProperties_Statics::NewProp_Color,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UShapeSprayToolProperties_Statics::NewProp_bRandomColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UShapeSprayToolProperties_Statics::NewProp_DropSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UShapeSprayToolProperties_Statics::NewProp_ObjectSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UShapeSprayToolProperties_Statics::NewProp_NumSplats,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UShapeSprayToolProperties_Statics::NewProp_Material,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UShapeSprayToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UShapeSprayToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UShapeSprayToolProperties_Statics::ClassParams = {
		&UShapeSprayToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UShapeSprayToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UShapeSprayToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UShapeSprayToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UShapeSprayToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UShapeSprayToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UShapeSprayToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UShapeSprayToolProperties, 2355255319);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UShapeSprayToolProperties>()
	{
		return UShapeSprayToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UShapeSprayToolProperties(Z_Construct_UClass_UShapeSprayToolProperties, &UShapeSprayToolProperties::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UShapeSprayToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UShapeSprayToolProperties);
	void UShapeSprayTool::StaticRegisterNativesUShapeSprayTool()
	{
	}
	UClass* Z_Construct_UClass_UShapeSprayTool_NoRegister()
	{
		return UShapeSprayTool::StaticClass();
	}
	struct Z_Construct_UClass_UShapeSprayTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Settings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Settings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AccumMeshComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AccumMeshComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UShapeSprayTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDynamicMeshBrushTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UShapeSprayTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * UShapeSprayTool is a brush-based tool that generates random points on the\n * target surface within the brush radius, and then creates small meshes\n * at those points. The accumulated meshes are appended and can\n * be emitted as a new StaticMeshComponent on Accept.\n */" },
		{ "IncludePath", "ShapeSprayTool.h" },
		{ "ModuleRelativePath", "Public/ShapeSprayTool.h" },
		{ "ToolTip", "UShapeSprayTool is a brush-based tool that generates random points on the\ntarget surface within the brush radius, and then creates small meshes\nat those points. The accumulated meshes are appended and can\nbe emitted as a new StaticMeshComponent on Accept." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UShapeSprayTool_Statics::NewProp_Settings_MetaData[] = {
		{ "ModuleRelativePath", "Public/ShapeSprayTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UShapeSprayTool_Statics::NewProp_Settings = { "Settings", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UShapeSprayTool, Settings), Z_Construct_UClass_UShapeSprayToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UShapeSprayTool_Statics::NewProp_Settings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UShapeSprayTool_Statics::NewProp_Settings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UShapeSprayTool_Statics::NewProp_AccumMeshComponent_MetaData[] = {
		{ "Comment", "// small meshes are accumulated here\n" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/ShapeSprayTool.h" },
		{ "ToolTip", "small meshes are accumulated here" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UShapeSprayTool_Statics::NewProp_AccumMeshComponent = { "AccumMeshComponent", nullptr, (EPropertyFlags)0x0020080000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UShapeSprayTool, AccumMeshComponent), Z_Construct_UClass_USimpleDynamicMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UShapeSprayTool_Statics::NewProp_AccumMeshComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UShapeSprayTool_Statics::NewProp_AccumMeshComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UShapeSprayTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UShapeSprayTool_Statics::NewProp_Settings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UShapeSprayTool_Statics::NewProp_AccumMeshComponent,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UShapeSprayTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UShapeSprayTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UShapeSprayTool_Statics::ClassParams = {
		&UShapeSprayTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UShapeSprayTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UShapeSprayTool_Statics::PropPointers),
		0,
		0x009000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UShapeSprayTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UShapeSprayTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UShapeSprayTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UShapeSprayTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UShapeSprayTool, 405445184);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UShapeSprayTool>()
	{
		return UShapeSprayTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UShapeSprayTool(Z_Construct_UClass_UShapeSprayTool, &UShapeSprayTool::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UShapeSprayTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UShapeSprayTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
