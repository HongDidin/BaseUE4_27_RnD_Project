// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MODELINGCOMPONENTS_PointSetComponent_generated_h
#error "PointSetComponent.generated.h already included, missing '#pragma once' in PointSetComponent.h"
#endif
#define MODELINGCOMPONENTS_PointSetComponent_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PointSetComponent_h_50_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PointSetComponent_h_50_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PointSetComponent_h_50_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PointSetComponent_h_50_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPointSetComponent(); \
	friend struct Z_Construct_UClass_UPointSetComponent_Statics; \
public: \
	DECLARE_CLASS(UPointSetComponent, UMeshComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UPointSetComponent)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PointSetComponent_h_50_INCLASS \
private: \
	static void StaticRegisterNativesUPointSetComponent(); \
	friend struct Z_Construct_UClass_UPointSetComponent_Statics; \
public: \
	DECLARE_CLASS(UPointSetComponent, UMeshComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UPointSetComponent)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PointSetComponent_h_50_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPointSetComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPointSetComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPointSetComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPointSetComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPointSetComponent(UPointSetComponent&&); \
	NO_API UPointSetComponent(const UPointSetComponent&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PointSetComponent_h_50_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPointSetComponent(UPointSetComponent&&); \
	NO_API UPointSetComponent(const UPointSetComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPointSetComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPointSetComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UPointSetComponent)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PointSetComponent_h_50_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__PointMaterial() { return STRUCT_OFFSET(UPointSetComponent, PointMaterial); } \
	FORCEINLINE static uint32 __PPO__Bounds() { return STRUCT_OFFSET(UPointSetComponent, Bounds); } \
	FORCEINLINE static uint32 __PPO__bBoundsDirty() { return STRUCT_OFFSET(UPointSetComponent, bBoundsDirty); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PointSetComponent_h_47_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PointSetComponent_h_50_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PointSetComponent_h_50_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PointSetComponent_h_50_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PointSetComponent_h_50_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PointSetComponent_h_50_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PointSetComponent_h_50_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PointSetComponent_h_50_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PointSetComponent_h_50_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PointSetComponent_h_50_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PointSetComponent_h_50_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PointSetComponent_h_50_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PointSetComponent_h_50_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MODELINGCOMPONENTS_API UClass* StaticClass<class UPointSetComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PointSetComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
