// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MODELINGCOMPONENTS_SimpleDynamicMeshComponent_generated_h
#error "SimpleDynamicMeshComponent.generated.h already included, missing '#pragma once' in SimpleDynamicMeshComponent.h"
#endif
#define MODELINGCOMPONENTS_SimpleDynamicMeshComponent_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_SimpleDynamicMeshComponent_h_34_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_SimpleDynamicMeshComponent_h_34_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_SimpleDynamicMeshComponent_h_34_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_SimpleDynamicMeshComponent_h_34_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSimpleDynamicMeshComponent(); \
	friend struct Z_Construct_UClass_USimpleDynamicMeshComponent_Statics; \
public: \
	DECLARE_CLASS(USimpleDynamicMeshComponent, UBaseDynamicMeshComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(USimpleDynamicMeshComponent)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_SimpleDynamicMeshComponent_h_34_INCLASS \
private: \
	static void StaticRegisterNativesUSimpleDynamicMeshComponent(); \
	friend struct Z_Construct_UClass_USimpleDynamicMeshComponent_Statics; \
public: \
	DECLARE_CLASS(USimpleDynamicMeshComponent, UBaseDynamicMeshComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(USimpleDynamicMeshComponent)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_SimpleDynamicMeshComponent_h_34_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USimpleDynamicMeshComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USimpleDynamicMeshComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USimpleDynamicMeshComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USimpleDynamicMeshComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USimpleDynamicMeshComponent(USimpleDynamicMeshComponent&&); \
	NO_API USimpleDynamicMeshComponent(const USimpleDynamicMeshComponent&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_SimpleDynamicMeshComponent_h_34_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USimpleDynamicMeshComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USimpleDynamicMeshComponent(USimpleDynamicMeshComponent&&); \
	NO_API USimpleDynamicMeshComponent(const USimpleDynamicMeshComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USimpleDynamicMeshComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USimpleDynamicMeshComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USimpleDynamicMeshComponent)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_SimpleDynamicMeshComponent_h_34_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_SimpleDynamicMeshComponent_h_31_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_SimpleDynamicMeshComponent_h_34_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_SimpleDynamicMeshComponent_h_34_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_SimpleDynamicMeshComponent_h_34_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_SimpleDynamicMeshComponent_h_34_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_SimpleDynamicMeshComponent_h_34_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_SimpleDynamicMeshComponent_h_34_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_SimpleDynamicMeshComponent_h_34_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_SimpleDynamicMeshComponent_h_34_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_SimpleDynamicMeshComponent_h_34_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_SimpleDynamicMeshComponent_h_34_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_SimpleDynamicMeshComponent_h_34_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_SimpleDynamicMeshComponent_h_34_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class SimpleDynamicMeshComponent."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MODELINGCOMPONENTS_API UClass* StaticClass<class USimpleDynamicMeshComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_SimpleDynamicMeshComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
