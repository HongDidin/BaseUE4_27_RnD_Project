// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ModelingComponents/Public/Drawing/PointSetComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePointSetComponent() {}
// Cross Module References
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPointSetComponent_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPointSetComponent();
	ENGINE_API UClass* Z_Construct_UClass_UMeshComponent();
	UPackage* Z_Construct_UPackage__Script_ModelingComponents();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FBoxSphereBounds();
// End Cross Module References
	void UPointSetComponent::StaticRegisterNativesUPointSetComponent()
	{
	}
	UClass* Z_Construct_UClass_UPointSetComponent_NoRegister()
	{
		return UPointSetComponent::StaticClass();
	}
	struct Z_Construct_UClass_UPointSetComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PointMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PointMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Bounds_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Bounds;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bBoundsDirty_MetaData[];
#endif
		static void NewProp_bBoundsDirty_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bBoundsDirty;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPointSetComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_ModelingComponents,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPointSetComponent_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * UPointSetComponent is a Component that draws a set of points, as small squares.\n * Per-point Color and (view-space) Size is supported. Normals are not supported.\n * \n * Points are inserted with an externally-defined ID, internally this is done via\n * a TSparseArray. This class allocates a contiguous TArray large enugh to hold the \n * largest ID. Using ReservePoints() may be beneficial for huge arrays.\n *\n * The points are drawn as two triangles (ie a square) orthogonal to the view direction. \n * The actual point size is calculated in the shader, and so a custom material must be used.\n */" },
		{ "HideCategories", "Mobility Trigger" },
		{ "IncludePath", "Drawing/PointSetComponent.h" },
		{ "ModuleRelativePath", "Public/Drawing/PointSetComponent.h" },
		{ "ToolTip", "UPointSetComponent is a Component that draws a set of points, as small squares.\nPer-point Color and (view-space) Size is supported. Normals are not supported.\n\nPoints are inserted with an externally-defined ID, internally this is done via\na TSparseArray. This class allocates a contiguous TArray large enugh to hold the\nlargest ID. Using ReservePoints() may be beneficial for huge arrays.\n\nThe points are drawn as two triangles (ie a square) orthogonal to the view direction.\nThe actual point size is calculated in the shader, and so a custom material must be used." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPointSetComponent_Statics::NewProp_PointMaterial_MetaData[] = {
		{ "Comment", "//~ Begin USceneComponent Interface.\n" },
		{ "ModuleRelativePath", "Public/Drawing/PointSetComponent.h" },
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPointSetComponent_Statics::NewProp_PointMaterial = { "PointMaterial", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPointSetComponent, PointMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPointSetComponent_Statics::NewProp_PointMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPointSetComponent_Statics::NewProp_PointMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPointSetComponent_Statics::NewProp_Bounds_MetaData[] = {
		{ "ModuleRelativePath", "Public/Drawing/PointSetComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UPointSetComponent_Statics::NewProp_Bounds = { "Bounds", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPointSetComponent, Bounds), Z_Construct_UScriptStruct_FBoxSphereBounds, METADATA_PARAMS(Z_Construct_UClass_UPointSetComponent_Statics::NewProp_Bounds_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPointSetComponent_Statics::NewProp_Bounds_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPointSetComponent_Statics::NewProp_bBoundsDirty_MetaData[] = {
		{ "ModuleRelativePath", "Public/Drawing/PointSetComponent.h" },
	};
#endif
	void Z_Construct_UClass_UPointSetComponent_Statics::NewProp_bBoundsDirty_SetBit(void* Obj)
	{
		((UPointSetComponent*)Obj)->bBoundsDirty = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPointSetComponent_Statics::NewProp_bBoundsDirty = { "bBoundsDirty", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPointSetComponent), &Z_Construct_UClass_UPointSetComponent_Statics::NewProp_bBoundsDirty_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPointSetComponent_Statics::NewProp_bBoundsDirty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPointSetComponent_Statics::NewProp_bBoundsDirty_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPointSetComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPointSetComponent_Statics::NewProp_PointMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPointSetComponent_Statics::NewProp_Bounds,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPointSetComponent_Statics::NewProp_bBoundsDirty,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPointSetComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPointSetComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPointSetComponent_Statics::ClassParams = {
		&UPointSetComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UPointSetComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UPointSetComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UPointSetComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPointSetComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPointSetComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPointSetComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPointSetComponent, 804275995);
	template<> MODELINGCOMPONENTS_API UClass* StaticClass<UPointSetComponent>()
	{
		return UPointSetComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPointSetComponent(Z_Construct_UClass_UPointSetComponent, &UPointSetComponent::StaticClass, TEXT("/Script/ModelingComponents"), TEXT("UPointSetComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPointSetComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
