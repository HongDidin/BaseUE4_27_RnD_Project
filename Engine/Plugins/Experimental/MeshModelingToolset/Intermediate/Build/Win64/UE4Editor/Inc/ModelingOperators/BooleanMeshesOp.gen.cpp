// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ModelingOperators/Public/CompositionOps/BooleanMeshesOp.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBooleanMeshesOp() {}
// Cross Module References
	MODELINGOPERATORS_API UEnum* Z_Construct_UEnum_ModelingOperators_ECSGOperation();
	UPackage* Z_Construct_UPackage__Script_ModelingOperators();
// End Cross Module References
	static UEnum* ECSGOperation_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ModelingOperators_ECSGOperation, Z_Construct_UPackage__Script_ModelingOperators(), TEXT("ECSGOperation"));
		}
		return Singleton;
	}
	template<> MODELINGOPERATORS_API UEnum* StaticEnum<ECSGOperation>()
	{
		return ECSGOperation_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ECSGOperation(ECSGOperation_StaticEnum, TEXT("/Script/ModelingOperators"), TEXT("ECSGOperation"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ModelingOperators_ECSGOperation_Hash() { return 3506101742U; }
	UEnum* Z_Construct_UEnum_ModelingOperators_ECSGOperation()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ModelingOperators();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ECSGOperation"), 0, Get_Z_Construct_UEnum_ModelingOperators_ECSGOperation_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ECSGOperation::DifferenceAB", (int64)ECSGOperation::DifferenceAB },
				{ "ECSGOperation::DifferenceBA", (int64)ECSGOperation::DifferenceBA },
				{ "ECSGOperation::Intersect", (int64)ECSGOperation::Intersect },
				{ "ECSGOperation::Union", (int64)ECSGOperation::Union },
				{ "ECSGOperation::TrimA", (int64)ECSGOperation::TrimA },
				{ "ECSGOperation::TrimB", (int64)ECSGOperation::TrimB },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** CSG operation types */" },
				{ "DifferenceAB.Comment", "/** Subtracts the first object from the second */" },
				{ "DifferenceAB.DisplayName", "A - B" },
				{ "DifferenceAB.Name", "ECSGOperation::DifferenceAB" },
				{ "DifferenceAB.ToolTip", "Subtracts the first object from the second" },
				{ "DifferenceBA.Comment", "/** Subtracts the second object from the first */" },
				{ "DifferenceBA.DisplayName", "B - A" },
				{ "DifferenceBA.Name", "ECSGOperation::DifferenceBA" },
				{ "DifferenceBA.ToolTip", "Subtracts the second object from the first" },
				{ "Intersect.Comment", "/** intersection of two objects */" },
				{ "Intersect.DisplayName", "Intersect" },
				{ "Intersect.Name", "ECSGOperation::Intersect" },
				{ "Intersect.ToolTip", "intersection of two objects" },
				{ "ModuleRelativePath", "Public/CompositionOps/BooleanMeshesOp.h" },
				{ "ToolTip", "CSG operation types" },
				{ "TrimA.Comment", "/** Trim the first object using the second */" },
				{ "TrimA.DisplayName", "Trim A" },
				{ "TrimA.Name", "ECSGOperation::TrimA" },
				{ "TrimA.ToolTip", "Trim the first object using the second" },
				{ "TrimB.Comment", "/** Trim the second object using the first */" },
				{ "TrimB.DisplayName", "Trim B" },
				{ "TrimB.Name", "ECSGOperation::TrimB" },
				{ "TrimB.ToolTip", "Trim the second object using the first" },
				{ "Union.Comment", "/** union of two objects */" },
				{ "Union.DisplayName", "Union" },
				{ "Union.Name", "ECSGOperation::Union" },
				{ "Union.ToolTip", "union of two objects" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ModelingOperators,
				nullptr,
				"ECSGOperation",
				"ECSGOperation",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
