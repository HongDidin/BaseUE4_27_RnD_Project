// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLS_MeshBoundaryToolBase_generated_h
#error "MeshBoundaryToolBase.generated.h already included, missing '#pragma once' in MeshBoundaryToolBase.h"
#endif
#define MESHMODELINGTOOLS_MeshBoundaryToolBase_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshBoundaryToolBase_h_27_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshBoundaryToolBase_h_27_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshBoundaryToolBase_h_27_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshBoundaryToolBase_h_27_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshBoundaryToolBase(); \
	friend struct Z_Construct_UClass_UMeshBoundaryToolBase_Statics; \
public: \
	DECLARE_CLASS(UMeshBoundaryToolBase, USingleSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMeshBoundaryToolBase)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshBoundaryToolBase_h_27_INCLASS \
private: \
	static void StaticRegisterNativesUMeshBoundaryToolBase(); \
	friend struct Z_Construct_UClass_UMeshBoundaryToolBase_Statics; \
public: \
	DECLARE_CLASS(UMeshBoundaryToolBase, USingleSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMeshBoundaryToolBase)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshBoundaryToolBase_h_27_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshBoundaryToolBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshBoundaryToolBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshBoundaryToolBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshBoundaryToolBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshBoundaryToolBase(UMeshBoundaryToolBase&&); \
	NO_API UMeshBoundaryToolBase(const UMeshBoundaryToolBase&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshBoundaryToolBase_h_27_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshBoundaryToolBase() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshBoundaryToolBase(UMeshBoundaryToolBase&&); \
	NO_API UMeshBoundaryToolBase(const UMeshBoundaryToolBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshBoundaryToolBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshBoundaryToolBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMeshBoundaryToolBase)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshBoundaryToolBase_h_27_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SelectionMechanic() { return STRUCT_OFFSET(UMeshBoundaryToolBase, SelectionMechanic); } \
	FORCEINLINE static uint32 __PPO__LoopSelectClickBehavior() { return STRUCT_OFFSET(UMeshBoundaryToolBase, LoopSelectClickBehavior); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshBoundaryToolBase_h_24_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshBoundaryToolBase_h_27_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshBoundaryToolBase_h_27_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshBoundaryToolBase_h_27_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshBoundaryToolBase_h_27_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshBoundaryToolBase_h_27_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshBoundaryToolBase_h_27_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshBoundaryToolBase_h_27_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshBoundaryToolBase_h_27_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshBoundaryToolBase_h_27_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshBoundaryToolBase_h_27_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshBoundaryToolBase_h_27_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshBoundaryToolBase_h_27_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UMeshBoundaryToolBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshBoundaryToolBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
