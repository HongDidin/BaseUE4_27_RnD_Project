// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLS_PlaneCutTool_generated_h
#error "PlaneCutTool.generated.h already included, missing '#pragma once' in PlaneCutTool.h"
#endif
#define MESHMODELINGTOOLS_PlaneCutTool_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_31_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_31_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_31_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_31_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPlaneCutToolBuilder(); \
	friend struct Z_Construct_UClass_UPlaneCutToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UPlaneCutToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UPlaneCutToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_31_INCLASS \
private: \
	static void StaticRegisterNativesUPlaneCutToolBuilder(); \
	friend struct Z_Construct_UClass_UPlaneCutToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UPlaneCutToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UPlaneCutToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_31_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPlaneCutToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPlaneCutToolBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPlaneCutToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPlaneCutToolBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPlaneCutToolBuilder(UPlaneCutToolBuilder&&); \
	NO_API UPlaneCutToolBuilder(const UPlaneCutToolBuilder&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_31_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPlaneCutToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPlaneCutToolBuilder(UPlaneCutToolBuilder&&); \
	NO_API UPlaneCutToolBuilder(const UPlaneCutToolBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPlaneCutToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPlaneCutToolBuilder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPlaneCutToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_31_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_28_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_31_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_31_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_31_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_31_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_31_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_31_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_31_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_31_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_31_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_31_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_31_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_31_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UPlaneCutToolBuilder>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_48_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_48_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_48_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_48_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAcceptOutputProperties(); \
	friend struct Z_Construct_UClass_UAcceptOutputProperties_Statics; \
public: \
	DECLARE_CLASS(UAcceptOutputProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UAcceptOutputProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_48_INCLASS \
private: \
	static void StaticRegisterNativesUAcceptOutputProperties(); \
	friend struct Z_Construct_UClass_UAcceptOutputProperties_Statics; \
public: \
	DECLARE_CLASS(UAcceptOutputProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UAcceptOutputProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_48_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAcceptOutputProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAcceptOutputProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAcceptOutputProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAcceptOutputProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAcceptOutputProperties(UAcceptOutputProperties&&); \
	NO_API UAcceptOutputProperties(const UAcceptOutputProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_48_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAcceptOutputProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAcceptOutputProperties(UAcceptOutputProperties&&); \
	NO_API UAcceptOutputProperties(const UAcceptOutputProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAcceptOutputProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAcceptOutputProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAcceptOutputProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_48_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_45_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_48_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_48_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_48_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_48_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_48_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_48_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_48_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_48_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_48_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_48_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_48_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_48_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UAcceptOutputProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_68_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_68_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_68_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_68_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPlaneCutToolProperties(); \
	friend struct Z_Construct_UClass_UPlaneCutToolProperties_Statics; \
public: \
	DECLARE_CLASS(UPlaneCutToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UPlaneCutToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_68_INCLASS \
private: \
	static void StaticRegisterNativesUPlaneCutToolProperties(); \
	friend struct Z_Construct_UClass_UPlaneCutToolProperties_Statics; \
public: \
	DECLARE_CLASS(UPlaneCutToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UPlaneCutToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_68_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPlaneCutToolProperties(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPlaneCutToolProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPlaneCutToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPlaneCutToolProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPlaneCutToolProperties(UPlaneCutToolProperties&&); \
	NO_API UPlaneCutToolProperties(const UPlaneCutToolProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_68_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPlaneCutToolProperties(UPlaneCutToolProperties&&); \
	NO_API UPlaneCutToolProperties(const UPlaneCutToolProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPlaneCutToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPlaneCutToolProperties); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UPlaneCutToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_68_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_65_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_68_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_68_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_68_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_68_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_68_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_68_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_68_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_68_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_68_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_68_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_68_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_68_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UPlaneCutToolProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_102_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_102_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_102_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_102_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPlaneCutOperatorFactory(); \
	friend struct Z_Construct_UClass_UPlaneCutOperatorFactory_Statics; \
public: \
	DECLARE_CLASS(UPlaneCutOperatorFactory, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UPlaneCutOperatorFactory)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_102_INCLASS \
private: \
	static void StaticRegisterNativesUPlaneCutOperatorFactory(); \
	friend struct Z_Construct_UClass_UPlaneCutOperatorFactory_Statics; \
public: \
	DECLARE_CLASS(UPlaneCutOperatorFactory, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UPlaneCutOperatorFactory)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_102_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPlaneCutOperatorFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPlaneCutOperatorFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPlaneCutOperatorFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPlaneCutOperatorFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPlaneCutOperatorFactory(UPlaneCutOperatorFactory&&); \
	NO_API UPlaneCutOperatorFactory(const UPlaneCutOperatorFactory&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_102_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPlaneCutOperatorFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPlaneCutOperatorFactory(UPlaneCutOperatorFactory&&); \
	NO_API UPlaneCutOperatorFactory(const UPlaneCutOperatorFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPlaneCutOperatorFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPlaneCutOperatorFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPlaneCutOperatorFactory)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_102_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_99_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_102_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_102_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_102_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_102_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_102_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_102_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_102_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_102_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_102_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_102_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_102_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_102_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UPlaneCutOperatorFactory>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_120_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_120_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execCut);


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_120_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execCut);


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_120_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPlaneCutTool(); \
	friend struct Z_Construct_UClass_UPlaneCutTool_Statics; \
public: \
	DECLARE_CLASS(UPlaneCutTool, UMultiSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UPlaneCutTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_120_INCLASS \
private: \
	static void StaticRegisterNativesUPlaneCutTool(); \
	friend struct Z_Construct_UClass_UPlaneCutTool_Statics; \
public: \
	DECLARE_CLASS(UPlaneCutTool, UMultiSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UPlaneCutTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_120_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPlaneCutTool(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPlaneCutTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPlaneCutTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPlaneCutTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPlaneCutTool(UPlaneCutTool&&); \
	NO_API UPlaneCutTool(const UPlaneCutTool&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_120_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPlaneCutTool(UPlaneCutTool&&); \
	NO_API UPlaneCutTool(const UPlaneCutTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPlaneCutTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPlaneCutTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UPlaneCutTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_120_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__BasicProperties() { return STRUCT_OFFSET(UPlaneCutTool, BasicProperties); } \
	FORCEINLINE static uint32 __PPO__AcceptProperties() { return STRUCT_OFFSET(UPlaneCutTool, AcceptProperties); } \
	FORCEINLINE static uint32 __PPO__CutPlaneOrigin() { return STRUCT_OFFSET(UPlaneCutTool, CutPlaneOrigin); } \
	FORCEINLINE static uint32 __PPO__CutPlaneOrientation() { return STRUCT_OFFSET(UPlaneCutTool, CutPlaneOrientation); } \
	FORCEINLINE static uint32 __PPO__Previews() { return STRUCT_OFFSET(UPlaneCutTool, Previews); } \
	FORCEINLINE static uint32 __PPO__MeshesToCut() { return STRUCT_OFFSET(UPlaneCutTool, MeshesToCut); } \
	FORCEINLINE static uint32 __PPO__PlaneTransformGizmo() { return STRUCT_OFFSET(UPlaneCutTool, PlaneTransformGizmo); } \
	FORCEINLINE static uint32 __PPO__PlaneTransformProxy() { return STRUCT_OFFSET(UPlaneCutTool, PlaneTransformProxy); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_117_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_120_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_120_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_120_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_120_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_120_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_120_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_120_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_120_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_120_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_120_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_120_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h_120_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UPlaneCutTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_PlaneCutTool_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
