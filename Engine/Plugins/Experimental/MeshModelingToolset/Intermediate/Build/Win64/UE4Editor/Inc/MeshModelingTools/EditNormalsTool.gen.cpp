// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/EditNormalsTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEditNormalsTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEditNormalsToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEditNormalsToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEditNormalsToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEditNormalsToolProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MODELINGOPERATORS_API UEnum* Z_Construct_UEnum_ModelingOperators_ENormalCalculationMethod();
	MODELINGOPERATORS_API UEnum* Z_Construct_UEnum_ModelingOperators_ESplitNormalMethod();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEditNormalsAdvancedProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEditNormalsAdvancedProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEditNormalsOperatorFactory_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEditNormalsOperatorFactory();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEditNormalsTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEditNormalsTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UMultiSelectionTool();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_NoRegister();
// End Cross Module References
	void UEditNormalsToolBuilder::StaticRegisterNativesUEditNormalsToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UEditNormalsToolBuilder_NoRegister()
	{
		return UEditNormalsToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UEditNormalsToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEditNormalsToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditNormalsToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "EditNormalsTool.h" },
		{ "ModuleRelativePath", "Public/EditNormalsTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEditNormalsToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEditNormalsToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEditNormalsToolBuilder_Statics::ClassParams = {
		&UEditNormalsToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UEditNormalsToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEditNormalsToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEditNormalsToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEditNormalsToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEditNormalsToolBuilder, 970419328);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UEditNormalsToolBuilder>()
	{
		return UEditNormalsToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEditNormalsToolBuilder(Z_Construct_UClass_UEditNormalsToolBuilder, &UEditNormalsToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UEditNormalsToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEditNormalsToolBuilder);
	void UEditNormalsToolProperties::StaticRegisterNativesUEditNormalsToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UEditNormalsToolProperties_NoRegister()
	{
		return UEditNormalsToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UEditNormalsToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRecomputeNormals_MetaData[];
#endif
		static void NewProp_bRecomputeNormals_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRecomputeNormals;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_NormalCalculationMethod_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NormalCalculationMethod_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_NormalCalculationMethod;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bFixInconsistentNormals_MetaData[];
#endif
		static void NewProp_bFixInconsistentNormals_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFixInconsistentNormals;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bInvertNormals_MetaData[];
#endif
		static void NewProp_bInvertNormals_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInvertNormals;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_SplitNormalMethod_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SplitNormalMethod_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_SplitNormalMethod;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SharpEdgeAngleThreshold_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SharpEdgeAngleThreshold;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAllowSharpVertices_MetaData[];
#endif
		static void NewProp_bAllowSharpVertices_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAllowSharpVertices;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEditNormalsToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditNormalsToolProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Standard properties\n */" },
		{ "IncludePath", "EditNormalsTool.h" },
		{ "ModuleRelativePath", "Public/EditNormalsTool.h" },
		{ "ToolTip", "Standard properties" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_bRecomputeNormals_MetaData[] = {
		{ "Category", "NormalsCalculation" },
		{ "Comment", "/** Recompute all mesh normals */" },
		{ "EditCondition", "SplitNormalMethod == ESplitNormalMethod::UseExistingTopology" },
		{ "ModuleRelativePath", "Public/EditNormalsTool.h" },
		{ "ToolTip", "Recompute all mesh normals" },
	};
#endif
	void Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_bRecomputeNormals_SetBit(void* Obj)
	{
		((UEditNormalsToolProperties*)Obj)->bRecomputeNormals = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_bRecomputeNormals = { "bRecomputeNormals", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UEditNormalsToolProperties), &Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_bRecomputeNormals_SetBit, METADATA_PARAMS(Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_bRecomputeNormals_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_bRecomputeNormals_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_NormalCalculationMethod_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_NormalCalculationMethod_MetaData[] = {
		{ "Category", "NormalsCalculation" },
		{ "Comment", "/** Choose the method for computing vertex normals */" },
		{ "ModuleRelativePath", "Public/EditNormalsTool.h" },
		{ "ToolTip", "Choose the method for computing vertex normals" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_NormalCalculationMethod = { "NormalCalculationMethod", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEditNormalsToolProperties, NormalCalculationMethod), Z_Construct_UEnum_ModelingOperators_ENormalCalculationMethod, METADATA_PARAMS(Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_NormalCalculationMethod_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_NormalCalculationMethod_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_bFixInconsistentNormals_MetaData[] = {
		{ "Category", "NormalsCalculation" },
		{ "Comment", "/** For meshes with inconsistent triangle orientations/normals, flip as needed to make the normals consistent */" },
		{ "ModuleRelativePath", "Public/EditNormalsTool.h" },
		{ "ToolTip", "For meshes with inconsistent triangle orientations/normals, flip as needed to make the normals consistent" },
	};
#endif
	void Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_bFixInconsistentNormals_SetBit(void* Obj)
	{
		((UEditNormalsToolProperties*)Obj)->bFixInconsistentNormals = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_bFixInconsistentNormals = { "bFixInconsistentNormals", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UEditNormalsToolProperties), &Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_bFixInconsistentNormals_SetBit, METADATA_PARAMS(Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_bFixInconsistentNormals_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_bFixInconsistentNormals_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_bInvertNormals_MetaData[] = {
		{ "Category", "NormalsCalculation" },
		{ "Comment", "/** Invert (flip) all mesh normals and associated triangle orientations */" },
		{ "ModuleRelativePath", "Public/EditNormalsTool.h" },
		{ "ToolTip", "Invert (flip) all mesh normals and associated triangle orientations" },
	};
#endif
	void Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_bInvertNormals_SetBit(void* Obj)
	{
		((UEditNormalsToolProperties*)Obj)->bInvertNormals = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_bInvertNormals = { "bInvertNormals", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UEditNormalsToolProperties), &Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_bInvertNormals_SetBit, METADATA_PARAMS(Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_bInvertNormals_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_bInvertNormals_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_SplitNormalMethod_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_SplitNormalMethod_MetaData[] = {
		{ "Category", "NormalsTopology" },
		{ "Comment", "/** Control whether and how the topology of the normals is recomputed, e.g. to create sharp edges where face normals change by a large amount or where face group IDs change.  Normals will always be recomputed unless SplitNormal Method is UseExistingTopology. */" },
		{ "ModuleRelativePath", "Public/EditNormalsTool.h" },
		{ "ToolTip", "Control whether and how the topology of the normals is recomputed, e.g. to create sharp edges where face normals change by a large amount or where face group IDs change.  Normals will always be recomputed unless SplitNormal Method is UseExistingTopology." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_SplitNormalMethod = { "SplitNormalMethod", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEditNormalsToolProperties, SplitNormalMethod), Z_Construct_UEnum_ModelingOperators_ESplitNormalMethod, METADATA_PARAMS(Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_SplitNormalMethod_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_SplitNormalMethod_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_SharpEdgeAngleThreshold_MetaData[] = {
		{ "Category", "NormalsTopology" },
		{ "ClampMax", "180.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Threshold on angle of change in face normals across an edge, above which we create a sharp edge if bSplitNormals is true */" },
		{ "EditCondition", "SplitNormalMethod == ESplitNormalMethod::FaceNormalThreshold" },
		{ "ModuleRelativePath", "Public/EditNormalsTool.h" },
		{ "ToolTip", "Threshold on angle of change in face normals across an edge, above which we create a sharp edge if bSplitNormals is true" },
		{ "UIMax", "180.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_SharpEdgeAngleThreshold = { "SharpEdgeAngleThreshold", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEditNormalsToolProperties, SharpEdgeAngleThreshold), METADATA_PARAMS(Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_SharpEdgeAngleThreshold_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_SharpEdgeAngleThreshold_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_bAllowSharpVertices_MetaData[] = {
		{ "Category", "NormalsTopology" },
		{ "Comment", "/** Assign separate normals at 'sharp' vertices -- for example, at the tip of a cone */" },
		{ "EditCondition", "SplitNormalMethod == ESplitNormalMethod::FaceNormalThreshold" },
		{ "ModuleRelativePath", "Public/EditNormalsTool.h" },
		{ "ToolTip", "Assign separate normals at 'sharp' vertices -- for example, at the tip of a cone" },
	};
#endif
	void Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_bAllowSharpVertices_SetBit(void* Obj)
	{
		((UEditNormalsToolProperties*)Obj)->bAllowSharpVertices = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_bAllowSharpVertices = { "bAllowSharpVertices", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UEditNormalsToolProperties), &Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_bAllowSharpVertices_SetBit, METADATA_PARAMS(Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_bAllowSharpVertices_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_bAllowSharpVertices_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UEditNormalsToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_bRecomputeNormals,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_NormalCalculationMethod_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_NormalCalculationMethod,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_bFixInconsistentNormals,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_bInvertNormals,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_SplitNormalMethod_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_SplitNormalMethod,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_SharpEdgeAngleThreshold,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditNormalsToolProperties_Statics::NewProp_bAllowSharpVertices,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEditNormalsToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEditNormalsToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEditNormalsToolProperties_Statics::ClassParams = {
		&UEditNormalsToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UEditNormalsToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UEditNormalsToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UEditNormalsToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEditNormalsToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEditNormalsToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEditNormalsToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEditNormalsToolProperties, 33543408);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UEditNormalsToolProperties>()
	{
		return UEditNormalsToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEditNormalsToolProperties(Z_Construct_UClass_UEditNormalsToolProperties, &UEditNormalsToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UEditNormalsToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEditNormalsToolProperties);
	void UEditNormalsAdvancedProperties::StaticRegisterNativesUEditNormalsAdvancedProperties()
	{
	}
	UClass* Z_Construct_UClass_UEditNormalsAdvancedProperties_NoRegister()
	{
		return UEditNormalsAdvancedProperties::StaticClass();
	}
	struct Z_Construct_UClass_UEditNormalsAdvancedProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEditNormalsAdvancedProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditNormalsAdvancedProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Advanced properties\n */" },
		{ "IncludePath", "EditNormalsTool.h" },
		{ "ModuleRelativePath", "Public/EditNormalsTool.h" },
		{ "ToolTip", "Advanced properties" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEditNormalsAdvancedProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEditNormalsAdvancedProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEditNormalsAdvancedProperties_Statics::ClassParams = {
		&UEditNormalsAdvancedProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UEditNormalsAdvancedProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEditNormalsAdvancedProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEditNormalsAdvancedProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEditNormalsAdvancedProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEditNormalsAdvancedProperties, 617092966);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UEditNormalsAdvancedProperties>()
	{
		return UEditNormalsAdvancedProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEditNormalsAdvancedProperties(Z_Construct_UClass_UEditNormalsAdvancedProperties, &UEditNormalsAdvancedProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UEditNormalsAdvancedProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEditNormalsAdvancedProperties);
	void UEditNormalsOperatorFactory::StaticRegisterNativesUEditNormalsOperatorFactory()
	{
	}
	UClass* Z_Construct_UClass_UEditNormalsOperatorFactory_NoRegister()
	{
		return UEditNormalsOperatorFactory::StaticClass();
	}
	struct Z_Construct_UClass_UEditNormalsOperatorFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Tool_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Tool;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEditNormalsOperatorFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditNormalsOperatorFactory_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Factory with enough info to spawn the background-thread Operator to do a chunk of work for the tool\n *  stores a pointer to the tool and enough info to know which specific operator it should spawn\n */" },
		{ "IncludePath", "EditNormalsTool.h" },
		{ "ModuleRelativePath", "Public/EditNormalsTool.h" },
		{ "ToolTip", "Factory with enough info to spawn the background-thread Operator to do a chunk of work for the tool\n stores a pointer to the tool and enough info to know which specific operator it should spawn" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditNormalsOperatorFactory_Statics::NewProp_Tool_MetaData[] = {
		{ "ModuleRelativePath", "Public/EditNormalsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UEditNormalsOperatorFactory_Statics::NewProp_Tool = { "Tool", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEditNormalsOperatorFactory, Tool), Z_Construct_UClass_UEditNormalsTool_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UEditNormalsOperatorFactory_Statics::NewProp_Tool_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditNormalsOperatorFactory_Statics::NewProp_Tool_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UEditNormalsOperatorFactory_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditNormalsOperatorFactory_Statics::NewProp_Tool,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEditNormalsOperatorFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEditNormalsOperatorFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEditNormalsOperatorFactory_Statics::ClassParams = {
		&UEditNormalsOperatorFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UEditNormalsOperatorFactory_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UEditNormalsOperatorFactory_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UEditNormalsOperatorFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEditNormalsOperatorFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEditNormalsOperatorFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEditNormalsOperatorFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEditNormalsOperatorFactory, 3497143940);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UEditNormalsOperatorFactory>()
	{
		return UEditNormalsOperatorFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEditNormalsOperatorFactory(Z_Construct_UClass_UEditNormalsOperatorFactory, &UEditNormalsOperatorFactory::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UEditNormalsOperatorFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEditNormalsOperatorFactory);
	void UEditNormalsTool::StaticRegisterNativesUEditNormalsTool()
	{
	}
	UClass* Z_Construct_UClass_UEditNormalsTool_NoRegister()
	{
		return UEditNormalsTool::StaticClass();
	}
	struct Z_Construct_UClass_UEditNormalsTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BasicProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BasicProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AdvancedProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AdvancedProperties;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Previews_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Previews_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Previews;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEditNormalsTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMultiSelectionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditNormalsTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Simple Mesh Normal Updating Tool\n */" },
		{ "IncludePath", "EditNormalsTool.h" },
		{ "ModuleRelativePath", "Public/EditNormalsTool.h" },
		{ "ToolTip", "Simple Mesh Normal Updating Tool" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditNormalsTool_Statics::NewProp_BasicProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/EditNormalsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UEditNormalsTool_Statics::NewProp_BasicProperties = { "BasicProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEditNormalsTool, BasicProperties), Z_Construct_UClass_UEditNormalsToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UEditNormalsTool_Statics::NewProp_BasicProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditNormalsTool_Statics::NewProp_BasicProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditNormalsTool_Statics::NewProp_AdvancedProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/EditNormalsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UEditNormalsTool_Statics::NewProp_AdvancedProperties = { "AdvancedProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEditNormalsTool, AdvancedProperties), Z_Construct_UClass_UEditNormalsAdvancedProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UEditNormalsTool_Statics::NewProp_AdvancedProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditNormalsTool_Statics::NewProp_AdvancedProperties_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UEditNormalsTool_Statics::NewProp_Previews_Inner = { "Previews", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditNormalsTool_Statics::NewProp_Previews_MetaData[] = {
		{ "ModuleRelativePath", "Public/EditNormalsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UEditNormalsTool_Statics::NewProp_Previews = { "Previews", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEditNormalsTool, Previews), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UEditNormalsTool_Statics::NewProp_Previews_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditNormalsTool_Statics::NewProp_Previews_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UEditNormalsTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditNormalsTool_Statics::NewProp_BasicProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditNormalsTool_Statics::NewProp_AdvancedProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditNormalsTool_Statics::NewProp_Previews_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditNormalsTool_Statics::NewProp_Previews,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEditNormalsTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEditNormalsTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEditNormalsTool_Statics::ClassParams = {
		&UEditNormalsTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UEditNormalsTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UEditNormalsTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UEditNormalsTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEditNormalsTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEditNormalsTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEditNormalsTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEditNormalsTool, 1223962186);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UEditNormalsTool>()
	{
		return UEditNormalsTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEditNormalsTool(Z_Construct_UClass_UEditNormalsTool, &UEditNormalsTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UEditNormalsTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEditNormalsTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
