// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/Physics/CollisionPropertySets.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCollisionPropertySets() {}
// Cross Module References
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_ECollisionGeometryMode();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UScriptStruct* Z_Construct_UScriptStruct_FPhysicsConvexData();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FKShapeElem();
	MESHMODELINGTOOLS_API UScriptStruct* Z_Construct_UScriptStruct_FPhysicsCapsuleData();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	MESHMODELINGTOOLS_API UScriptStruct* Z_Construct_UScriptStruct_FPhysicsBoxData();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	MESHMODELINGTOOLS_API UScriptStruct* Z_Construct_UScriptStruct_FPhysicsSphereData();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPhysicsObjectToolPropertySet_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPhysicsObjectToolPropertySet();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UCollisionGeometryVisualizationProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UCollisionGeometryVisualizationProperties();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FColor();
// End Cross Module References
	static UEnum* ECollisionGeometryMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_ECollisionGeometryMode, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("ECollisionGeometryMode"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<ECollisionGeometryMode>()
	{
		return ECollisionGeometryMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ECollisionGeometryMode(ECollisionGeometryMode_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("ECollisionGeometryMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_ECollisionGeometryMode_Hash() { return 3756674803U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_ECollisionGeometryMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ECollisionGeometryMode"), 0, Get_Z_Construct_UEnum_MeshModelingTools_ECollisionGeometryMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ECollisionGeometryMode::Default", (int64)ECollisionGeometryMode::Default },
				{ "ECollisionGeometryMode::SimpleAndComplex", (int64)ECollisionGeometryMode::SimpleAndComplex },
				{ "ECollisionGeometryMode::UseSimpleAsComplex", (int64)ECollisionGeometryMode::UseSimpleAsComplex },
				{ "ECollisionGeometryMode::UseComplexAsSimple", (int64)ECollisionGeometryMode::UseComplexAsSimple },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Default.Comment", "/** Use project physics settings (DefaultShapeComplexity) */" },
				{ "Default.Name", "ECollisionGeometryMode::Default" },
				{ "Default.ToolTip", "Use project physics settings (DefaultShapeComplexity)" },
				{ "ModuleRelativePath", "Public/Physics/CollisionPropertySets.h" },
				{ "SimpleAndComplex.Comment", "/** Create both simple and complex shapes. Simple shapes are used for regular scene queries and collision tests. Complex shape (per poly) is used for complex scene queries.*/" },
				{ "SimpleAndComplex.Name", "ECollisionGeometryMode::SimpleAndComplex" },
				{ "SimpleAndComplex.ToolTip", "Create both simple and complex shapes. Simple shapes are used for regular scene queries and collision tests. Complex shape (per poly) is used for complex scene queries." },
				{ "UseComplexAsSimple.Comment", "/** Create only complex shapes (per poly). Use complex shapes for all scene queries and collision tests. Can be used in simulation for static shapes only (i.e can be collided against but not moved through forces or velocity.) */" },
				{ "UseComplexAsSimple.Name", "ECollisionGeometryMode::UseComplexAsSimple" },
				{ "UseComplexAsSimple.ToolTip", "Create only complex shapes (per poly). Use complex shapes for all scene queries and collision tests. Can be used in simulation for static shapes only (i.e can be collided against but not moved through forces or velocity.)" },
				{ "UseSimpleAsComplex.Comment", "/** Create only simple shapes. Use simple shapes for all scene queries and collision tests.*/" },
				{ "UseSimpleAsComplex.Name", "ECollisionGeometryMode::UseSimpleAsComplex" },
				{ "UseSimpleAsComplex.ToolTip", "Create only simple shapes. Use simple shapes for all scene queries and collision tests." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"ECollisionGeometryMode",
				"ECollisionGeometryMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FPhysicsConvexData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MESHMODELINGTOOLS_API uint32 Get_Z_Construct_UScriptStruct_FPhysicsConvexData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FPhysicsConvexData, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("PhysicsConvexData"), sizeof(FPhysicsConvexData), Get_Z_Construct_UScriptStruct_FPhysicsConvexData_Hash());
	}
	return Singleton;
}
template<> MESHMODELINGTOOLS_API UScriptStruct* StaticStruct<FPhysicsConvexData>()
{
	return FPhysicsConvexData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FPhysicsConvexData(FPhysicsConvexData::StaticStruct, TEXT("/Script/MeshModelingTools"), TEXT("PhysicsConvexData"), false, nullptr, nullptr);
static struct FScriptStruct_MeshModelingTools_StaticRegisterNativesFPhysicsConvexData
{
	FScriptStruct_MeshModelingTools_StaticRegisterNativesFPhysicsConvexData()
	{
		UScriptStruct::DeferCppStructOps<FPhysicsConvexData>(FName(TEXT("PhysicsConvexData")));
	}
} ScriptStruct_MeshModelingTools_StaticRegisterNativesFPhysicsConvexData;
	struct Z_Construct_UScriptStruct_FPhysicsConvexData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumVertices_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumVertices;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumFaces_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumFaces;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Element_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Element;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPhysicsConvexData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Physics/CollisionPropertySets.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FPhysicsConvexData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FPhysicsConvexData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPhysicsConvexData_Statics::NewProp_NumVertices_MetaData[] = {
		{ "Category", "Convex" },
		{ "ModuleRelativePath", "Public/Physics/CollisionPropertySets.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FPhysicsConvexData_Statics::NewProp_NumVertices = { "NumVertices", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPhysicsConvexData, NumVertices), METADATA_PARAMS(Z_Construct_UScriptStruct_FPhysicsConvexData_Statics::NewProp_NumVertices_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPhysicsConvexData_Statics::NewProp_NumVertices_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPhysicsConvexData_Statics::NewProp_NumFaces_MetaData[] = {
		{ "Category", "Convex" },
		{ "ModuleRelativePath", "Public/Physics/CollisionPropertySets.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FPhysicsConvexData_Statics::NewProp_NumFaces = { "NumFaces", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPhysicsConvexData, NumFaces), METADATA_PARAMS(Z_Construct_UScriptStruct_FPhysicsConvexData_Statics::NewProp_NumFaces_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPhysicsConvexData_Statics::NewProp_NumFaces_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPhysicsConvexData_Statics::NewProp_Element_MetaData[] = {
		{ "Category", "Sphere" },
		{ "ModuleRelativePath", "Public/Physics/CollisionPropertySets.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FPhysicsConvexData_Statics::NewProp_Element = { "Element", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPhysicsConvexData, Element), Z_Construct_UScriptStruct_FKShapeElem, METADATA_PARAMS(Z_Construct_UScriptStruct_FPhysicsConvexData_Statics::NewProp_Element_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPhysicsConvexData_Statics::NewProp_Element_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FPhysicsConvexData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPhysicsConvexData_Statics::NewProp_NumVertices,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPhysicsConvexData_Statics::NewProp_NumFaces,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPhysicsConvexData_Statics::NewProp_Element,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FPhysicsConvexData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
		nullptr,
		&NewStructOps,
		"PhysicsConvexData",
		sizeof(FPhysicsConvexData),
		alignof(FPhysicsConvexData),
		Z_Construct_UScriptStruct_FPhysicsConvexData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPhysicsConvexData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FPhysicsConvexData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPhysicsConvexData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FPhysicsConvexData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FPhysicsConvexData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("PhysicsConvexData"), sizeof(FPhysicsConvexData), Get_Z_Construct_UScriptStruct_FPhysicsConvexData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FPhysicsConvexData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FPhysicsConvexData_Hash() { return 1386775367U; }
class UScriptStruct* FPhysicsCapsuleData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MESHMODELINGTOOLS_API uint32 Get_Z_Construct_UScriptStruct_FPhysicsCapsuleData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FPhysicsCapsuleData, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("PhysicsCapsuleData"), sizeof(FPhysicsCapsuleData), Get_Z_Construct_UScriptStruct_FPhysicsCapsuleData_Hash());
	}
	return Singleton;
}
template<> MESHMODELINGTOOLS_API UScriptStruct* StaticStruct<FPhysicsCapsuleData>()
{
	return FPhysicsCapsuleData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FPhysicsCapsuleData(FPhysicsCapsuleData::StaticStruct, TEXT("/Script/MeshModelingTools"), TEXT("PhysicsCapsuleData"), false, nullptr, nullptr);
static struct FScriptStruct_MeshModelingTools_StaticRegisterNativesFPhysicsCapsuleData
{
	FScriptStruct_MeshModelingTools_StaticRegisterNativesFPhysicsCapsuleData()
	{
		UScriptStruct::DeferCppStructOps<FPhysicsCapsuleData>(FName(TEXT("PhysicsCapsuleData")));
	}
} ScriptStruct_MeshModelingTools_StaticRegisterNativesFPhysicsCapsuleData;
	struct Z_Construct_UScriptStruct_FPhysicsCapsuleData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Radius_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Radius;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Length_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Length;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Transform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Transform;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Element_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Element;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPhysicsCapsuleData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Physics/CollisionPropertySets.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FPhysicsCapsuleData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FPhysicsCapsuleData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPhysicsCapsuleData_Statics::NewProp_Radius_MetaData[] = {
		{ "Category", "Sphere" },
		{ "ModuleRelativePath", "Public/Physics/CollisionPropertySets.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FPhysicsCapsuleData_Statics::NewProp_Radius = { "Radius", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPhysicsCapsuleData, Radius), METADATA_PARAMS(Z_Construct_UScriptStruct_FPhysicsCapsuleData_Statics::NewProp_Radius_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPhysicsCapsuleData_Statics::NewProp_Radius_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPhysicsCapsuleData_Statics::NewProp_Length_MetaData[] = {
		{ "Category", "Sphere" },
		{ "ModuleRelativePath", "Public/Physics/CollisionPropertySets.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FPhysicsCapsuleData_Statics::NewProp_Length = { "Length", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPhysicsCapsuleData, Length), METADATA_PARAMS(Z_Construct_UScriptStruct_FPhysicsCapsuleData_Statics::NewProp_Length_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPhysicsCapsuleData_Statics::NewProp_Length_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPhysicsCapsuleData_Statics::NewProp_Transform_MetaData[] = {
		{ "Category", "Sphere" },
		{ "ModuleRelativePath", "Public/Physics/CollisionPropertySets.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FPhysicsCapsuleData_Statics::NewProp_Transform = { "Transform", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPhysicsCapsuleData, Transform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FPhysicsCapsuleData_Statics::NewProp_Transform_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPhysicsCapsuleData_Statics::NewProp_Transform_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPhysicsCapsuleData_Statics::NewProp_Element_MetaData[] = {
		{ "Category", "Sphere" },
		{ "ModuleRelativePath", "Public/Physics/CollisionPropertySets.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FPhysicsCapsuleData_Statics::NewProp_Element = { "Element", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPhysicsCapsuleData, Element), Z_Construct_UScriptStruct_FKShapeElem, METADATA_PARAMS(Z_Construct_UScriptStruct_FPhysicsCapsuleData_Statics::NewProp_Element_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPhysicsCapsuleData_Statics::NewProp_Element_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FPhysicsCapsuleData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPhysicsCapsuleData_Statics::NewProp_Radius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPhysicsCapsuleData_Statics::NewProp_Length,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPhysicsCapsuleData_Statics::NewProp_Transform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPhysicsCapsuleData_Statics::NewProp_Element,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FPhysicsCapsuleData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
		nullptr,
		&NewStructOps,
		"PhysicsCapsuleData",
		sizeof(FPhysicsCapsuleData),
		alignof(FPhysicsCapsuleData),
		Z_Construct_UScriptStruct_FPhysicsCapsuleData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPhysicsCapsuleData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FPhysicsCapsuleData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPhysicsCapsuleData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FPhysicsCapsuleData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FPhysicsCapsuleData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("PhysicsCapsuleData"), sizeof(FPhysicsCapsuleData), Get_Z_Construct_UScriptStruct_FPhysicsCapsuleData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FPhysicsCapsuleData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FPhysicsCapsuleData_Hash() { return 1786045640U; }
class UScriptStruct* FPhysicsBoxData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MESHMODELINGTOOLS_API uint32 Get_Z_Construct_UScriptStruct_FPhysicsBoxData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FPhysicsBoxData, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("PhysicsBoxData"), sizeof(FPhysicsBoxData), Get_Z_Construct_UScriptStruct_FPhysicsBoxData_Hash());
	}
	return Singleton;
}
template<> MESHMODELINGTOOLS_API UScriptStruct* StaticStruct<FPhysicsBoxData>()
{
	return FPhysicsBoxData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FPhysicsBoxData(FPhysicsBoxData::StaticStruct, TEXT("/Script/MeshModelingTools"), TEXT("PhysicsBoxData"), false, nullptr, nullptr);
static struct FScriptStruct_MeshModelingTools_StaticRegisterNativesFPhysicsBoxData
{
	FScriptStruct_MeshModelingTools_StaticRegisterNativesFPhysicsBoxData()
	{
		UScriptStruct::DeferCppStructOps<FPhysicsBoxData>(FName(TEXT("PhysicsBoxData")));
	}
} ScriptStruct_MeshModelingTools_StaticRegisterNativesFPhysicsBoxData;
	struct Z_Construct_UScriptStruct_FPhysicsBoxData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Dimensions_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Dimensions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Transform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Transform;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Element_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Element;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPhysicsBoxData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Physics/CollisionPropertySets.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FPhysicsBoxData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FPhysicsBoxData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPhysicsBoxData_Statics::NewProp_Dimensions_MetaData[] = {
		{ "Category", "Sphere" },
		{ "ModuleRelativePath", "Public/Physics/CollisionPropertySets.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FPhysicsBoxData_Statics::NewProp_Dimensions = { "Dimensions", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPhysicsBoxData, Dimensions), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FPhysicsBoxData_Statics::NewProp_Dimensions_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPhysicsBoxData_Statics::NewProp_Dimensions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPhysicsBoxData_Statics::NewProp_Transform_MetaData[] = {
		{ "Category", "Sphere" },
		{ "ModuleRelativePath", "Public/Physics/CollisionPropertySets.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FPhysicsBoxData_Statics::NewProp_Transform = { "Transform", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPhysicsBoxData, Transform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FPhysicsBoxData_Statics::NewProp_Transform_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPhysicsBoxData_Statics::NewProp_Transform_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPhysicsBoxData_Statics::NewProp_Element_MetaData[] = {
		{ "Category", "Sphere" },
		{ "ModuleRelativePath", "Public/Physics/CollisionPropertySets.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FPhysicsBoxData_Statics::NewProp_Element = { "Element", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPhysicsBoxData, Element), Z_Construct_UScriptStruct_FKShapeElem, METADATA_PARAMS(Z_Construct_UScriptStruct_FPhysicsBoxData_Statics::NewProp_Element_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPhysicsBoxData_Statics::NewProp_Element_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FPhysicsBoxData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPhysicsBoxData_Statics::NewProp_Dimensions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPhysicsBoxData_Statics::NewProp_Transform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPhysicsBoxData_Statics::NewProp_Element,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FPhysicsBoxData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
		nullptr,
		&NewStructOps,
		"PhysicsBoxData",
		sizeof(FPhysicsBoxData),
		alignof(FPhysicsBoxData),
		Z_Construct_UScriptStruct_FPhysicsBoxData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPhysicsBoxData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FPhysicsBoxData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPhysicsBoxData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FPhysicsBoxData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FPhysicsBoxData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("PhysicsBoxData"), sizeof(FPhysicsBoxData), Get_Z_Construct_UScriptStruct_FPhysicsBoxData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FPhysicsBoxData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FPhysicsBoxData_Hash() { return 419340353U; }
class UScriptStruct* FPhysicsSphereData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MESHMODELINGTOOLS_API uint32 Get_Z_Construct_UScriptStruct_FPhysicsSphereData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FPhysicsSphereData, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("PhysicsSphereData"), sizeof(FPhysicsSphereData), Get_Z_Construct_UScriptStruct_FPhysicsSphereData_Hash());
	}
	return Singleton;
}
template<> MESHMODELINGTOOLS_API UScriptStruct* StaticStruct<FPhysicsSphereData>()
{
	return FPhysicsSphereData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FPhysicsSphereData(FPhysicsSphereData::StaticStruct, TEXT("/Script/MeshModelingTools"), TEXT("PhysicsSphereData"), false, nullptr, nullptr);
static struct FScriptStruct_MeshModelingTools_StaticRegisterNativesFPhysicsSphereData
{
	FScriptStruct_MeshModelingTools_StaticRegisterNativesFPhysicsSphereData()
	{
		UScriptStruct::DeferCppStructOps<FPhysicsSphereData>(FName(TEXT("PhysicsSphereData")));
	}
} ScriptStruct_MeshModelingTools_StaticRegisterNativesFPhysicsSphereData;
	struct Z_Construct_UScriptStruct_FPhysicsSphereData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Radius_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Radius;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Transform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Transform;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Element_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Element;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPhysicsSphereData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Physics/CollisionPropertySets.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FPhysicsSphereData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FPhysicsSphereData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPhysicsSphereData_Statics::NewProp_Radius_MetaData[] = {
		{ "Category", "Sphere" },
		{ "ModuleRelativePath", "Public/Physics/CollisionPropertySets.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FPhysicsSphereData_Statics::NewProp_Radius = { "Radius", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPhysicsSphereData, Radius), METADATA_PARAMS(Z_Construct_UScriptStruct_FPhysicsSphereData_Statics::NewProp_Radius_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPhysicsSphereData_Statics::NewProp_Radius_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPhysicsSphereData_Statics::NewProp_Transform_MetaData[] = {
		{ "Category", "Sphere" },
		{ "ModuleRelativePath", "Public/Physics/CollisionPropertySets.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FPhysicsSphereData_Statics::NewProp_Transform = { "Transform", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPhysicsSphereData, Transform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FPhysicsSphereData_Statics::NewProp_Transform_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPhysicsSphereData_Statics::NewProp_Transform_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPhysicsSphereData_Statics::NewProp_Element_MetaData[] = {
		{ "Category", "Sphere" },
		{ "ModuleRelativePath", "Public/Physics/CollisionPropertySets.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FPhysicsSphereData_Statics::NewProp_Element = { "Element", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPhysicsSphereData, Element), Z_Construct_UScriptStruct_FKShapeElem, METADATA_PARAMS(Z_Construct_UScriptStruct_FPhysicsSphereData_Statics::NewProp_Element_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPhysicsSphereData_Statics::NewProp_Element_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FPhysicsSphereData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPhysicsSphereData_Statics::NewProp_Radius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPhysicsSphereData_Statics::NewProp_Transform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPhysicsSphereData_Statics::NewProp_Element,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FPhysicsSphereData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
		nullptr,
		&NewStructOps,
		"PhysicsSphereData",
		sizeof(FPhysicsSphereData),
		alignof(FPhysicsSphereData),
		Z_Construct_UScriptStruct_FPhysicsSphereData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPhysicsSphereData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FPhysicsSphereData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPhysicsSphereData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FPhysicsSphereData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FPhysicsSphereData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("PhysicsSphereData"), sizeof(FPhysicsSphereData), Get_Z_Construct_UScriptStruct_FPhysicsSphereData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FPhysicsSphereData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FPhysicsSphereData_Hash() { return 1318154160U; }
	void UPhysicsObjectToolPropertySet::StaticRegisterNativesUPhysicsObjectToolPropertySet()
	{
	}
	UClass* Z_Construct_UClass_UPhysicsObjectToolPropertySet_NoRegister()
	{
		return UPhysicsObjectToolPropertySet::StaticClass();
	}
	struct Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObjectName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ObjectName;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_CollisionType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CollisionType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_CollisionType;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Spheres_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Spheres_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Spheres;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Boxes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Boxes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Boxes;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Capsules_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Capsules_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Capsules;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Convexes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Convexes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Convexes;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Physics/CollisionPropertySets.h" },
		{ "ModuleRelativePath", "Public/Physics/CollisionPropertySets.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::NewProp_ObjectName_MetaData[] = {
		{ "Category", "PhysicsData" },
		{ "ModuleRelativePath", "Public/Physics/CollisionPropertySets.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::NewProp_ObjectName = { "ObjectName", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPhysicsObjectToolPropertySet, ObjectName), METADATA_PARAMS(Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::NewProp_ObjectName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::NewProp_ObjectName_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::NewProp_CollisionType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::NewProp_CollisionType_MetaData[] = {
		{ "Category", "PhysicsData" },
		{ "ModuleRelativePath", "Public/Physics/CollisionPropertySets.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::NewProp_CollisionType = { "CollisionType", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPhysicsObjectToolPropertySet, CollisionType), Z_Construct_UEnum_MeshModelingTools_ECollisionGeometryMode, METADATA_PARAMS(Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::NewProp_CollisionType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::NewProp_CollisionType_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::NewProp_Spheres_Inner = { "Spheres", nullptr, (EPropertyFlags)0x0000000000020000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FPhysicsSphereData, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::NewProp_Spheres_MetaData[] = {
		{ "Category", "PhysicsData" },
		{ "ModuleRelativePath", "Public/Physics/CollisionPropertySets.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::NewProp_Spheres = { "Spheres", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPhysicsObjectToolPropertySet, Spheres), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::NewProp_Spheres_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::NewProp_Spheres_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::NewProp_Boxes_Inner = { "Boxes", nullptr, (EPropertyFlags)0x0000000000020000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FPhysicsBoxData, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::NewProp_Boxes_MetaData[] = {
		{ "Category", "PhysicsData" },
		{ "ModuleRelativePath", "Public/Physics/CollisionPropertySets.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::NewProp_Boxes = { "Boxes", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPhysicsObjectToolPropertySet, Boxes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::NewProp_Boxes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::NewProp_Boxes_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::NewProp_Capsules_Inner = { "Capsules", nullptr, (EPropertyFlags)0x0000000000020000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FPhysicsCapsuleData, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::NewProp_Capsules_MetaData[] = {
		{ "Category", "PhysicsData" },
		{ "ModuleRelativePath", "Public/Physics/CollisionPropertySets.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::NewProp_Capsules = { "Capsules", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPhysicsObjectToolPropertySet, Capsules), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::NewProp_Capsules_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::NewProp_Capsules_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::NewProp_Convexes_Inner = { "Convexes", nullptr, (EPropertyFlags)0x0000000000020000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FPhysicsConvexData, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::NewProp_Convexes_MetaData[] = {
		{ "Category", "PhysicsData" },
		{ "ModuleRelativePath", "Public/Physics/CollisionPropertySets.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::NewProp_Convexes = { "Convexes", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPhysicsObjectToolPropertySet, Convexes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::NewProp_Convexes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::NewProp_Convexes_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::NewProp_ObjectName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::NewProp_CollisionType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::NewProp_CollisionType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::NewProp_Spheres_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::NewProp_Spheres,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::NewProp_Boxes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::NewProp_Boxes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::NewProp_Capsules_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::NewProp_Capsules,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::NewProp_Convexes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::NewProp_Convexes,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPhysicsObjectToolPropertySet>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::ClassParams = {
		&UPhysicsObjectToolPropertySet::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPhysicsObjectToolPropertySet()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPhysicsObjectToolPropertySet, 674456576);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UPhysicsObjectToolPropertySet>()
	{
		return UPhysicsObjectToolPropertySet::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPhysicsObjectToolPropertySet(Z_Construct_UClass_UPhysicsObjectToolPropertySet, &UPhysicsObjectToolPropertySet::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UPhysicsObjectToolPropertySet"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPhysicsObjectToolPropertySet);
	void UCollisionGeometryVisualizationProperties::StaticRegisterNativesUCollisionGeometryVisualizationProperties()
	{
	}
	UClass* Z_Construct_UClass_UCollisionGeometryVisualizationProperties_NoRegister()
	{
		return UCollisionGeometryVisualizationProperties::StaticClass();
	}
	struct Z_Construct_UClass_UCollisionGeometryVisualizationProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LineThickness_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_LineThickness;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowHidden_MetaData[];
#endif
		static void NewProp_bShowHidden_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowHidden;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Color_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Color;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCollisionGeometryVisualizationProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCollisionGeometryVisualizationProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Physics/CollisionPropertySets.h" },
		{ "ModuleRelativePath", "Public/Physics/CollisionPropertySets.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCollisionGeometryVisualizationProperties_Statics::NewProp_LineThickness_MetaData[] = {
		{ "Category", "Visualization" },
		{ "ModuleRelativePath", "Public/Physics/CollisionPropertySets.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UCollisionGeometryVisualizationProperties_Statics::NewProp_LineThickness = { "LineThickness", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCollisionGeometryVisualizationProperties, LineThickness), METADATA_PARAMS(Z_Construct_UClass_UCollisionGeometryVisualizationProperties_Statics::NewProp_LineThickness_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCollisionGeometryVisualizationProperties_Statics::NewProp_LineThickness_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCollisionGeometryVisualizationProperties_Statics::NewProp_bShowHidden_MetaData[] = {
		{ "Category", "Visualization" },
		{ "ModuleRelativePath", "Public/Physics/CollisionPropertySets.h" },
	};
#endif
	void Z_Construct_UClass_UCollisionGeometryVisualizationProperties_Statics::NewProp_bShowHidden_SetBit(void* Obj)
	{
		((UCollisionGeometryVisualizationProperties*)Obj)->bShowHidden = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCollisionGeometryVisualizationProperties_Statics::NewProp_bShowHidden = { "bShowHidden", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCollisionGeometryVisualizationProperties), &Z_Construct_UClass_UCollisionGeometryVisualizationProperties_Statics::NewProp_bShowHidden_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCollisionGeometryVisualizationProperties_Statics::NewProp_bShowHidden_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCollisionGeometryVisualizationProperties_Statics::NewProp_bShowHidden_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCollisionGeometryVisualizationProperties_Statics::NewProp_Color_MetaData[] = {
		{ "Category", "Visualization" },
		{ "ModuleRelativePath", "Public/Physics/CollisionPropertySets.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCollisionGeometryVisualizationProperties_Statics::NewProp_Color = { "Color", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCollisionGeometryVisualizationProperties, Color), Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(Z_Construct_UClass_UCollisionGeometryVisualizationProperties_Statics::NewProp_Color_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCollisionGeometryVisualizationProperties_Statics::NewProp_Color_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCollisionGeometryVisualizationProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCollisionGeometryVisualizationProperties_Statics::NewProp_LineThickness,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCollisionGeometryVisualizationProperties_Statics::NewProp_bShowHidden,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCollisionGeometryVisualizationProperties_Statics::NewProp_Color,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCollisionGeometryVisualizationProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCollisionGeometryVisualizationProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCollisionGeometryVisualizationProperties_Statics::ClassParams = {
		&UCollisionGeometryVisualizationProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UCollisionGeometryVisualizationProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UCollisionGeometryVisualizationProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UCollisionGeometryVisualizationProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCollisionGeometryVisualizationProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCollisionGeometryVisualizationProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCollisionGeometryVisualizationProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCollisionGeometryVisualizationProperties, 2608088135);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UCollisionGeometryVisualizationProperties>()
	{
		return UCollisionGeometryVisualizationProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCollisionGeometryVisualizationProperties(Z_Construct_UClass_UCollisionGeometryVisualizationProperties, &UCollisionGeometryVisualizationProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UCollisionGeometryVisualizationProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCollisionGeometryVisualizationProperties);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
