// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLS_AddPatchTool_generated_h
#error "AddPatchTool.generated.h already included, missing '#pragma once' in AddPatchTool.h"
#endif
#define MESHMODELINGTOOLS_AddPatchTool_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_22_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_22_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_22_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAddPatchToolBuilder(); \
	friend struct Z_Construct_UClass_UAddPatchToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UAddPatchToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UAddPatchToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_22_INCLASS \
private: \
	static void StaticRegisterNativesUAddPatchToolBuilder(); \
	friend struct Z_Construct_UClass_UAddPatchToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UAddPatchToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UAddPatchToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAddPatchToolBuilder(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAddPatchToolBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAddPatchToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAddPatchToolBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAddPatchToolBuilder(UAddPatchToolBuilder&&); \
	NO_API UAddPatchToolBuilder(const UAddPatchToolBuilder&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_22_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAddPatchToolBuilder(UAddPatchToolBuilder&&); \
	NO_API UAddPatchToolBuilder(const UAddPatchToolBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAddPatchToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAddPatchToolBuilder); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UAddPatchToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_22_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_19_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_22_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_22_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_22_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_22_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_22_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_22_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_22_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_22_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UAddPatchToolBuilder>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_41_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_41_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_41_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_41_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAddPatchToolProperties(); \
	friend struct Z_Construct_UClass_UAddPatchToolProperties_Statics; \
public: \
	DECLARE_CLASS(UAddPatchToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UAddPatchToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_41_INCLASS \
private: \
	static void StaticRegisterNativesUAddPatchToolProperties(); \
	friend struct Z_Construct_UClass_UAddPatchToolProperties_Statics; \
public: \
	DECLARE_CLASS(UAddPatchToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UAddPatchToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_41_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAddPatchToolProperties(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAddPatchToolProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAddPatchToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAddPatchToolProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAddPatchToolProperties(UAddPatchToolProperties&&); \
	NO_API UAddPatchToolProperties(const UAddPatchToolProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_41_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAddPatchToolProperties(UAddPatchToolProperties&&); \
	NO_API UAddPatchToolProperties(const UAddPatchToolProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAddPatchToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAddPatchToolProperties); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UAddPatchToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_41_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_38_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_41_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_41_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_41_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_41_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_41_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_41_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_41_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_41_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_41_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_41_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_41_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_41_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UAddPatchToolProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_75_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_75_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_75_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_75_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAddPatchTool(); \
	friend struct Z_Construct_UClass_UAddPatchTool_Statics; \
public: \
	DECLARE_CLASS(UAddPatchTool, USingleClickTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UAddPatchTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_75_INCLASS \
private: \
	static void StaticRegisterNativesUAddPatchTool(); \
	friend struct Z_Construct_UClass_UAddPatchTool_Statics; \
public: \
	DECLARE_CLASS(UAddPatchTool, USingleClickTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UAddPatchTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_75_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAddPatchTool(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAddPatchTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAddPatchTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAddPatchTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAddPatchTool(UAddPatchTool&&); \
	NO_API UAddPatchTool(const UAddPatchTool&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_75_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAddPatchTool() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAddPatchTool(UAddPatchTool&&); \
	NO_API UAddPatchTool(const UAddPatchTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAddPatchTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAddPatchTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UAddPatchTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_75_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ShapeSettings() { return STRUCT_OFFSET(UAddPatchTool, ShapeSettings); } \
	FORCEINLINE static uint32 __PPO__MaterialProperties() { return STRUCT_OFFSET(UAddPatchTool, MaterialProperties); } \
	FORCEINLINE static uint32 __PPO__PreviewMesh() { return STRUCT_OFFSET(UAddPatchTool, PreviewMesh); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_72_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_75_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_75_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_75_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_75_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_75_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_75_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_75_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_75_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_75_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_75_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_75_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h_75_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UAddPatchTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_AddPatchTool_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
