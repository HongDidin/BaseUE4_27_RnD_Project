// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLS_DisplaceMeshTool_generated_h
#error "DisplaceMeshTool.generated.h already included, missing '#pragma once' in DisplaceMeshTool.h"
#endif
#define MESHMODELINGTOOLS_DisplaceMeshTool_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_134_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FPerlinLayerProperties_Statics; \
	MESHMODELINGTOOLS_API static class UScriptStruct* StaticStruct();


template<> MESHMODELINGTOOLS_API UScriptStruct* StaticStruct<struct FPerlinLayerProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_48_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_48_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetWeightMapsFunc);


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_48_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetWeightMapsFunc);


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_48_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDisplaceMeshCommonProperties(); \
	friend struct Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics; \
public: \
	DECLARE_CLASS(UDisplaceMeshCommonProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UDisplaceMeshCommonProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_48_INCLASS \
private: \
	static void StaticRegisterNativesUDisplaceMeshCommonProperties(); \
	friend struct Z_Construct_UClass_UDisplaceMeshCommonProperties_Statics; \
public: \
	DECLARE_CLASS(UDisplaceMeshCommonProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UDisplaceMeshCommonProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_48_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplaceMeshCommonProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplaceMeshCommonProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplaceMeshCommonProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplaceMeshCommonProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplaceMeshCommonProperties(UDisplaceMeshCommonProperties&&); \
	NO_API UDisplaceMeshCommonProperties(const UDisplaceMeshCommonProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_48_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplaceMeshCommonProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplaceMeshCommonProperties(UDisplaceMeshCommonProperties&&); \
	NO_API UDisplaceMeshCommonProperties(const UDisplaceMeshCommonProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplaceMeshCommonProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplaceMeshCommonProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplaceMeshCommonProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_48_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_45_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_48_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_48_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_48_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_48_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_48_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_48_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_48_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_48_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_48_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_48_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_48_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_48_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UDisplaceMeshCommonProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_93_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_93_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_93_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_93_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDisplaceMeshTextureMapProperties(); \
	friend struct Z_Construct_UClass_UDisplaceMeshTextureMapProperties_Statics; \
public: \
	DECLARE_CLASS(UDisplaceMeshTextureMapProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UDisplaceMeshTextureMapProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_93_INCLASS \
private: \
	static void StaticRegisterNativesUDisplaceMeshTextureMapProperties(); \
	friend struct Z_Construct_UClass_UDisplaceMeshTextureMapProperties_Statics; \
public: \
	DECLARE_CLASS(UDisplaceMeshTextureMapProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UDisplaceMeshTextureMapProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_93_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplaceMeshTextureMapProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplaceMeshTextureMapProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplaceMeshTextureMapProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplaceMeshTextureMapProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplaceMeshTextureMapProperties(UDisplaceMeshTextureMapProperties&&); \
	NO_API UDisplaceMeshTextureMapProperties(const UDisplaceMeshTextureMapProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_93_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplaceMeshTextureMapProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplaceMeshTextureMapProperties(UDisplaceMeshTextureMapProperties&&); \
	NO_API UDisplaceMeshTextureMapProperties(const UDisplaceMeshTextureMapProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplaceMeshTextureMapProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplaceMeshTextureMapProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplaceMeshTextureMapProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_93_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_90_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_93_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_93_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_93_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_93_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_93_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_93_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_93_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_93_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_93_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_93_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_93_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_93_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UDisplaceMeshTextureMapProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_107_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_107_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_107_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_107_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDisplaceMeshDirectionalFilterProperties(); \
	friend struct Z_Construct_UClass_UDisplaceMeshDirectionalFilterProperties_Statics; \
public: \
	DECLARE_CLASS(UDisplaceMeshDirectionalFilterProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UDisplaceMeshDirectionalFilterProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_107_INCLASS \
private: \
	static void StaticRegisterNativesUDisplaceMeshDirectionalFilterProperties(); \
	friend struct Z_Construct_UClass_UDisplaceMeshDirectionalFilterProperties_Statics; \
public: \
	DECLARE_CLASS(UDisplaceMeshDirectionalFilterProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UDisplaceMeshDirectionalFilterProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_107_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplaceMeshDirectionalFilterProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplaceMeshDirectionalFilterProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplaceMeshDirectionalFilterProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplaceMeshDirectionalFilterProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplaceMeshDirectionalFilterProperties(UDisplaceMeshDirectionalFilterProperties&&); \
	NO_API UDisplaceMeshDirectionalFilterProperties(const UDisplaceMeshDirectionalFilterProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_107_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplaceMeshDirectionalFilterProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplaceMeshDirectionalFilterProperties(UDisplaceMeshDirectionalFilterProperties&&); \
	NO_API UDisplaceMeshDirectionalFilterProperties(const UDisplaceMeshDirectionalFilterProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplaceMeshDirectionalFilterProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplaceMeshDirectionalFilterProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplaceMeshDirectionalFilterProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_107_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_104_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_107_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_107_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_107_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_107_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_107_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_107_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_107_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_107_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_107_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_107_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_107_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_107_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UDisplaceMeshDirectionalFilterProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_158_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_158_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_158_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_158_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDisplaceMeshPerlinNoiseProperties(); \
	friend struct Z_Construct_UClass_UDisplaceMeshPerlinNoiseProperties_Statics; \
public: \
	DECLARE_CLASS(UDisplaceMeshPerlinNoiseProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UDisplaceMeshPerlinNoiseProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_158_INCLASS \
private: \
	static void StaticRegisterNativesUDisplaceMeshPerlinNoiseProperties(); \
	friend struct Z_Construct_UClass_UDisplaceMeshPerlinNoiseProperties_Statics; \
public: \
	DECLARE_CLASS(UDisplaceMeshPerlinNoiseProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UDisplaceMeshPerlinNoiseProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_158_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplaceMeshPerlinNoiseProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplaceMeshPerlinNoiseProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplaceMeshPerlinNoiseProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplaceMeshPerlinNoiseProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplaceMeshPerlinNoiseProperties(UDisplaceMeshPerlinNoiseProperties&&); \
	NO_API UDisplaceMeshPerlinNoiseProperties(const UDisplaceMeshPerlinNoiseProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_158_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplaceMeshPerlinNoiseProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplaceMeshPerlinNoiseProperties(UDisplaceMeshPerlinNoiseProperties&&); \
	NO_API UDisplaceMeshPerlinNoiseProperties(const UDisplaceMeshPerlinNoiseProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplaceMeshPerlinNoiseProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplaceMeshPerlinNoiseProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplaceMeshPerlinNoiseProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_158_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_155_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_158_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_158_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_158_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_158_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_158_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_158_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_158_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_158_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_158_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_158_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_158_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_158_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UDisplaceMeshPerlinNoiseProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_171_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_171_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_171_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_171_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDisplaceMeshSineWaveProperties(); \
	friend struct Z_Construct_UClass_UDisplaceMeshSineWaveProperties_Statics; \
public: \
	DECLARE_CLASS(UDisplaceMeshSineWaveProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UDisplaceMeshSineWaveProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_171_INCLASS \
private: \
	static void StaticRegisterNativesUDisplaceMeshSineWaveProperties(); \
	friend struct Z_Construct_UClass_UDisplaceMeshSineWaveProperties_Statics; \
public: \
	DECLARE_CLASS(UDisplaceMeshSineWaveProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UDisplaceMeshSineWaveProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_171_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplaceMeshSineWaveProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplaceMeshSineWaveProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplaceMeshSineWaveProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplaceMeshSineWaveProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplaceMeshSineWaveProperties(UDisplaceMeshSineWaveProperties&&); \
	NO_API UDisplaceMeshSineWaveProperties(const UDisplaceMeshSineWaveProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_171_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplaceMeshSineWaveProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplaceMeshSineWaveProperties(UDisplaceMeshSineWaveProperties&&); \
	NO_API UDisplaceMeshSineWaveProperties(const UDisplaceMeshSineWaveProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplaceMeshSineWaveProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplaceMeshSineWaveProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplaceMeshSineWaveProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_171_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_168_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_171_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_171_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_171_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_171_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_171_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_171_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_171_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_171_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_171_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_171_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_171_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_171_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UDisplaceMeshSineWaveProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_195_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_195_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_195_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_195_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDisplaceMeshToolBuilder(); \
	friend struct Z_Construct_UClass_UDisplaceMeshToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UDisplaceMeshToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UDisplaceMeshToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_195_INCLASS \
private: \
	static void StaticRegisterNativesUDisplaceMeshToolBuilder(); \
	friend struct Z_Construct_UClass_UDisplaceMeshToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UDisplaceMeshToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UDisplaceMeshToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_195_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplaceMeshToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplaceMeshToolBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplaceMeshToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplaceMeshToolBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplaceMeshToolBuilder(UDisplaceMeshToolBuilder&&); \
	NO_API UDisplaceMeshToolBuilder(const UDisplaceMeshToolBuilder&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_195_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplaceMeshToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplaceMeshToolBuilder(UDisplaceMeshToolBuilder&&); \
	NO_API UDisplaceMeshToolBuilder(const UDisplaceMeshToolBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplaceMeshToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplaceMeshToolBuilder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplaceMeshToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_195_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_192_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_195_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_195_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_195_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_195_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_195_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_195_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_195_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_195_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_195_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_195_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_195_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_195_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UDisplaceMeshToolBuilder>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_207_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_207_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_207_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_207_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDisplaceMeshTool(); \
	friend struct Z_Construct_UClass_UDisplaceMeshTool_Statics; \
public: \
	DECLARE_CLASS(UDisplaceMeshTool, USingleSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UDisplaceMeshTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_207_INCLASS \
private: \
	static void StaticRegisterNativesUDisplaceMeshTool(); \
	friend struct Z_Construct_UClass_UDisplaceMeshTool_Statics; \
public: \
	DECLARE_CLASS(UDisplaceMeshTool, USingleSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UDisplaceMeshTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_207_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplaceMeshTool(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplaceMeshTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplaceMeshTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplaceMeshTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplaceMeshTool(UDisplaceMeshTool&&); \
	NO_API UDisplaceMeshTool(const UDisplaceMeshTool&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_207_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplaceMeshTool() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplaceMeshTool(UDisplaceMeshTool&&); \
	NO_API UDisplaceMeshTool(const UDisplaceMeshTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplaceMeshTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplaceMeshTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDisplaceMeshTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_207_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_204_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_207_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_207_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_207_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_207_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_207_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_207_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_207_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_207_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_207_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_207_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_207_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h_207_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UDisplaceMeshTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DisplaceMeshTool_h


#define FOREACH_ENUM_EDISPLACEMESHTOOLDISPLACETYPE(op) \
	op(EDisplaceMeshToolDisplaceType::Constant) \
	op(EDisplaceMeshToolDisplaceType::RandomNoise) \
	op(EDisplaceMeshToolDisplaceType::PerlinNoise) \
	op(EDisplaceMeshToolDisplaceType::DisplacementMap) \
	op(EDisplaceMeshToolDisplaceType::SineWave) 

enum class EDisplaceMeshToolDisplaceType : uint8;
template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EDisplaceMeshToolDisplaceType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
