// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MODELINGCOMPONENTS_PolyEditPreviewMesh_generated_h
#error "PolyEditPreviewMesh.generated.h already included, missing '#pragma once' in PolyEditPreviewMesh.h"
#endif
#define MODELINGCOMPONENTS_PolyEditPreviewMesh_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PolyEditPreviewMesh_h_20_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PolyEditPreviewMesh_h_20_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PolyEditPreviewMesh_h_20_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PolyEditPreviewMesh_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPolyEditPreviewMesh(); \
	friend struct Z_Construct_UClass_UPolyEditPreviewMesh_Statics; \
public: \
	DECLARE_CLASS(UPolyEditPreviewMesh, UPreviewMesh, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UPolyEditPreviewMesh)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PolyEditPreviewMesh_h_20_INCLASS \
private: \
	static void StaticRegisterNativesUPolyEditPreviewMesh(); \
	friend struct Z_Construct_UClass_UPolyEditPreviewMesh_Statics; \
public: \
	DECLARE_CLASS(UPolyEditPreviewMesh, UPreviewMesh, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UPolyEditPreviewMesh)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PolyEditPreviewMesh_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPolyEditPreviewMesh(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPolyEditPreviewMesh) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPolyEditPreviewMesh); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPolyEditPreviewMesh); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPolyEditPreviewMesh(UPolyEditPreviewMesh&&); \
	NO_API UPolyEditPreviewMesh(const UPolyEditPreviewMesh&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PolyEditPreviewMesh_h_20_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPolyEditPreviewMesh() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPolyEditPreviewMesh(UPolyEditPreviewMesh&&); \
	NO_API UPolyEditPreviewMesh(const UPolyEditPreviewMesh&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPolyEditPreviewMesh); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPolyEditPreviewMesh); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UPolyEditPreviewMesh)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PolyEditPreviewMesh_h_20_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PolyEditPreviewMesh_h_17_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PolyEditPreviewMesh_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PolyEditPreviewMesh_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PolyEditPreviewMesh_h_20_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PolyEditPreviewMesh_h_20_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PolyEditPreviewMesh_h_20_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PolyEditPreviewMesh_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PolyEditPreviewMesh_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PolyEditPreviewMesh_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PolyEditPreviewMesh_h_20_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PolyEditPreviewMesh_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PolyEditPreviewMesh_h_20_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PolyEditPreviewMesh_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MODELINGCOMPONENTS_API UClass* StaticClass<class UPolyEditPreviewMesh>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PolyEditPreviewMesh_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
