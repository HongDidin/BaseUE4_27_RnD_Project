// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MODELINGCOMPONENTS_UVLayoutPreview_generated_h
#error "UVLayoutPreview.generated.h already included, missing '#pragma once' in UVLayoutPreview.h"
#endif
#define MODELINGCOMPONENTS_UVLayoutPreview_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_32_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_32_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_32_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_32_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUUVLayoutPreviewProperties(); \
	friend struct Z_Construct_UClass_UUVLayoutPreviewProperties_Statics; \
public: \
	DECLARE_CLASS(UUVLayoutPreviewProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UUVLayoutPreviewProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_32_INCLASS \
private: \
	static void StaticRegisterNativesUUVLayoutPreviewProperties(); \
	friend struct Z_Construct_UClass_UUVLayoutPreviewProperties_Statics; \
public: \
	DECLARE_CLASS(UUVLayoutPreviewProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UUVLayoutPreviewProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_32_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUVLayoutPreviewProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUVLayoutPreviewProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUVLayoutPreviewProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUVLayoutPreviewProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUVLayoutPreviewProperties(UUVLayoutPreviewProperties&&); \
	NO_API UUVLayoutPreviewProperties(const UUVLayoutPreviewProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_32_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUVLayoutPreviewProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUVLayoutPreviewProperties(UUVLayoutPreviewProperties&&); \
	NO_API UUVLayoutPreviewProperties(const UUVLayoutPreviewProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUVLayoutPreviewProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUVLayoutPreviewProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUVLayoutPreviewProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_32_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_29_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_32_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_32_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_32_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_32_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_32_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_32_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_32_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_32_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_32_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_32_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_32_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_32_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MODELINGCOMPONENTS_API UClass* StaticClass<class UUVLayoutPreviewProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_64_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_64_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_64_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_64_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUUVLayoutPreview(); \
	friend struct Z_Construct_UClass_UUVLayoutPreview_Statics; \
public: \
	DECLARE_CLASS(UUVLayoutPreview, UObject, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UUVLayoutPreview)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_64_INCLASS \
private: \
	static void StaticRegisterNativesUUVLayoutPreview(); \
	friend struct Z_Construct_UClass_UUVLayoutPreview_Statics; \
public: \
	DECLARE_CLASS(UUVLayoutPreview, UObject, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UUVLayoutPreview)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_64_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUVLayoutPreview(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUVLayoutPreview) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUVLayoutPreview); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUVLayoutPreview); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUVLayoutPreview(UUVLayoutPreview&&); \
	NO_API UUVLayoutPreview(const UUVLayoutPreview&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_64_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUVLayoutPreview(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUVLayoutPreview(UUVLayoutPreview&&); \
	NO_API UUVLayoutPreview(const UUVLayoutPreview&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUVLayoutPreview); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUVLayoutPreview); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUVLayoutPreview)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_64_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_61_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_64_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_64_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_64_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_64_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_64_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_64_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_64_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_64_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_64_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_64_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_64_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h_64_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MODELINGCOMPONENTS_API UClass* StaticClass<class UUVLayoutPreview>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_UVLayoutPreview_h


#define FOREACH_ENUM_EUVLAYOUTPREVIEWSIDE(op) \
	op(EUVLayoutPreviewSide::Left) \
	op(EUVLayoutPreviewSide::Right) 

enum class EUVLayoutPreviewSide;
template<> MODELINGCOMPONENTS_API UEnum* StaticEnum<EUVLayoutPreviewSide>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
