// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingToolsEditorOnly/Public/BspConversionTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBspConversionTool() {}
// Cross Module References
	MESHMODELINGTOOLSEDITORONLY_API UEnum* Z_Construct_UEnum_MeshModelingToolsEditorOnly_EBspConversionToolAction();
	UPackage* Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly();
	MESHMODELINGTOOLSEDITORONLY_API UEnum* Z_Construct_UEnum_MeshModelingToolsEditorOnly_EBspConversionMode();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UBspConversionToolBuilder_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UBspConversionToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UBspConversionToolProperties_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UBspConversionToolProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UBspConversionToolActionPropertySet_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UBspConversionToolActionPropertySet();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UBspConversionTool_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UBspConversionTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveTool();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPreviewMesh_NoRegister();
// End Cross Module References
	static UEnum* EBspConversionToolAction_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingToolsEditorOnly_EBspConversionToolAction, Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly(), TEXT("EBspConversionToolAction"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLSEDITORONLY_API UEnum* StaticEnum<EBspConversionToolAction>()
	{
		return EBspConversionToolAction_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EBspConversionToolAction(EBspConversionToolAction_StaticEnum, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("EBspConversionToolAction"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingToolsEditorOnly_EBspConversionToolAction_Hash() { return 540769038U; }
	UEnum* Z_Construct_UEnum_MeshModelingToolsEditorOnly_EBspConversionToolAction()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EBspConversionToolAction"), 0, Get_Z_Construct_UEnum_MeshModelingToolsEditorOnly_EBspConversionToolAction_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EBspConversionToolAction::NoAction", (int64)EBspConversionToolAction::NoAction },
				{ "EBspConversionToolAction::SelectAllValidBrushes", (int64)EBspConversionToolAction::SelectAllValidBrushes },
				{ "EBspConversionToolAction::DeselectVolumes", (int64)EBspConversionToolAction::DeselectVolumes },
				{ "EBspConversionToolAction::DeselectNonValid", (int64)EBspConversionToolAction::DeselectNonValid },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "DeselectNonValid.Name", "EBspConversionToolAction::DeselectNonValid" },
				{ "DeselectVolumes.Name", "EBspConversionToolAction::DeselectVolumes" },
				{ "ModuleRelativePath", "Public/BspConversionTool.h" },
				{ "NoAction.Name", "EBspConversionToolAction::NoAction" },
				{ "SelectAllValidBrushes.Name", "EBspConversionToolAction::SelectAllValidBrushes" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
				nullptr,
				"EBspConversionToolAction",
				"EBspConversionToolAction",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EBspConversionMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingToolsEditorOnly_EBspConversionMode, Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly(), TEXT("EBspConversionMode"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLSEDITORONLY_API UEnum* StaticEnum<EBspConversionMode>()
	{
		return EBspConversionMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EBspConversionMode(EBspConversionMode_StaticEnum, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("EBspConversionMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingToolsEditorOnly_EBspConversionMode_Hash() { return 3647010789U; }
	UEnum* Z_Construct_UEnum_MeshModelingToolsEditorOnly_EBspConversionMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EBspConversionMode"), 0, Get_Z_Construct_UEnum_MeshModelingToolsEditorOnly_EBspConversionMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EBspConversionMode::ConvertFirst", (int64)EBspConversionMode::ConvertFirst },
				{ "EBspConversionMode::CombineFirst", (int64)EBspConversionMode::CombineFirst },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "CombineFirst.Comment", "/** First combines brushes, then converts result to static mesh. */" },
				{ "CombineFirst.DisplayName", "Combine, then Convert" },
				{ "CombineFirst.Name", "EBspConversionMode::CombineFirst" },
				{ "CombineFirst.ToolTip", "First combines brushes, then converts result to static mesh." },
				{ "ConvertFirst.Comment", "/** First converts the brushes to static meshes, then performs static mesh boolean operations. */" },
				{ "ConvertFirst.DisplayName", "Convert, then Combine" },
				{ "ConvertFirst.Name", "EBspConversionMode::ConvertFirst" },
				{ "ConvertFirst.ToolTip", "First converts the brushes to static meshes, then performs static mesh boolean operations." },
				{ "ModuleRelativePath", "Public/BspConversionTool.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
				nullptr,
				"EBspConversionMode",
				"EBspConversionMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UBspConversionToolBuilder::StaticRegisterNativesUBspConversionToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UBspConversionToolBuilder_NoRegister()
	{
		return UBspConversionToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UBspConversionToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBspConversionToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBspConversionToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Builder for UBspConversionTool.\n */" },
		{ "IncludePath", "BspConversionTool.h" },
		{ "ModuleRelativePath", "Public/BspConversionTool.h" },
		{ "ToolTip", "Builder for UBspConversionTool." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBspConversionToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBspConversionToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBspConversionToolBuilder_Statics::ClassParams = {
		&UBspConversionToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UBspConversionToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBspConversionToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBspConversionToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBspConversionToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBspConversionToolBuilder, 825906745);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UBspConversionToolBuilder>()
	{
		return UBspConversionToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBspConversionToolBuilder(Z_Construct_UClass_UBspConversionToolBuilder, &UBspConversionToolBuilder::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UBspConversionToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBspConversionToolBuilder);
	void UBspConversionToolProperties::StaticRegisterNativesUBspConversionToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UBspConversionToolProperties_NoRegister()
	{
		return UBspConversionToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UBspConversionToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ConversionMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConversionMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ConversionMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIncludeVolumes_MetaData[];
#endif
		static void NewProp_bIncludeVolumes_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIncludeVolumes;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRemoveConvertedVolumes_MetaData[];
#endif
		static void NewProp_bRemoveConvertedVolumes_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRemoveConvertedVolumes;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bExplicitSubtractiveBrushSelection_MetaData[];
#endif
		static void NewProp_bExplicitSubtractiveBrushSelection_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bExplicitSubtractiveBrushSelection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRemoveConvertedSubtractiveBrushes_MetaData[];
#endif
		static void NewProp_bRemoveConvertedSubtractiveBrushes_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRemoveConvertedSubtractiveBrushes;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCacheBrushes_MetaData[];
#endif
		static void NewProp_bCacheBrushes_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCacheBrushes;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowPreview_MetaData[];
#endif
		static void NewProp_bShowPreview_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowPreview;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBspConversionToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBspConversionToolProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "BspConversionTool.h" },
		{ "ModuleRelativePath", "Public/BspConversionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_ConversionMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_ConversionMode_MetaData[] = {
		{ "Category", "Options" },
		{ "ModuleRelativePath", "Public/BspConversionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_ConversionMode = { "ConversionMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBspConversionToolProperties, ConversionMode), Z_Construct_UEnum_MeshModelingToolsEditorOnly_EBspConversionMode, METADATA_PARAMS(Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_ConversionMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_ConversionMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bIncludeVolumes_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Whether to consider BSP volumes to be valid conversion targets. */" },
		{ "ModuleRelativePath", "Public/BspConversionTool.h" },
		{ "ToolTip", "Whether to consider BSP volumes to be valid conversion targets." },
	};
#endif
	void Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bIncludeVolumes_SetBit(void* Obj)
	{
		((UBspConversionToolProperties*)Obj)->bIncludeVolumes = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bIncludeVolumes = { "bIncludeVolumes", nullptr, (EPropertyFlags)0x0010000400000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UBspConversionToolProperties), &Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bIncludeVolumes_SetBit, METADATA_PARAMS(Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bIncludeVolumes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bIncludeVolumes_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bRemoveConvertedVolumes_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Whether to remove any selected BSP volumes after using them to create a static mesh. */" },
		{ "EditCondition", "bIncludeVolumes" },
		{ "ModuleRelativePath", "Public/BspConversionTool.h" },
		{ "ToolTip", "Whether to remove any selected BSP volumes after using them to create a static mesh." },
	};
#endif
	void Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bRemoveConvertedVolumes_SetBit(void* Obj)
	{
		((UBspConversionToolProperties*)Obj)->bRemoveConvertedVolumes = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bRemoveConvertedVolumes = { "bRemoveConvertedVolumes", nullptr, (EPropertyFlags)0x0010000400000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UBspConversionToolProperties), &Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bRemoveConvertedVolumes_SetBit, METADATA_PARAMS(Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bRemoveConvertedVolumes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bRemoveConvertedVolumes_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bExplicitSubtractiveBrushSelection_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Whether subtractive brushes have to be explicitly selected to be part of the conversion. If false, all\n\x09 subtractive brushes in the level will be used. */" },
		{ "ModuleRelativePath", "Public/BspConversionTool.h" },
		{ "ToolTip", "Whether subtractive brushes have to be explicitly selected to be part of the conversion. If false, all\n       subtractive brushes in the level will be used." },
	};
#endif
	void Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bExplicitSubtractiveBrushSelection_SetBit(void* Obj)
	{
		((UBspConversionToolProperties*)Obj)->bExplicitSubtractiveBrushSelection = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bExplicitSubtractiveBrushSelection = { "bExplicitSubtractiveBrushSelection", nullptr, (EPropertyFlags)0x0010000400000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UBspConversionToolProperties), &Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bExplicitSubtractiveBrushSelection_SetBit, METADATA_PARAMS(Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bExplicitSubtractiveBrushSelection_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bExplicitSubtractiveBrushSelection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bRemoveConvertedSubtractiveBrushes_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Whether subtractive brushes used in a conversion should be removed. Only acts on explicitly selected\n\x09 subtractive brushes. */" },
		{ "ModuleRelativePath", "Public/BspConversionTool.h" },
		{ "ToolTip", "Whether subtractive brushes used in a conversion should be removed. Only acts on explicitly selected\n       subtractive brushes." },
	};
#endif
	void Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bRemoveConvertedSubtractiveBrushes_SetBit(void* Obj)
	{
		((UBspConversionToolProperties*)Obj)->bRemoveConvertedSubtractiveBrushes = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bRemoveConvertedSubtractiveBrushes = { "bRemoveConvertedSubtractiveBrushes", nullptr, (EPropertyFlags)0x0010000400000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UBspConversionToolProperties), &Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bRemoveConvertedSubtractiveBrushes_SetBit, METADATA_PARAMS(Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bRemoveConvertedSubtractiveBrushes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bRemoveConvertedSubtractiveBrushes_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bCacheBrushes_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Caches individual brush conversions in \"convert then combine\" mode during a single invocation of \n\x09 the tool. Only useful if changing selections or properties after starting the tool. Cleared on tool shutdown. */" },
		{ "EditCondition", "ConversionMode == EBspConversionMode::ConvertFirst" },
		{ "ModuleRelativePath", "Public/BspConversionTool.h" },
		{ "ToolTip", "Caches individual brush conversions in \"convert then combine\" mode during a single invocation of\n       the tool. Only useful if changing selections or properties after starting the tool. Cleared on tool shutdown." },
	};
#endif
	void Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bCacheBrushes_SetBit(void* Obj)
	{
		((UBspConversionToolProperties*)Obj)->bCacheBrushes = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bCacheBrushes = { "bCacheBrushes", nullptr, (EPropertyFlags)0x0010000400000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UBspConversionToolProperties), &Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bCacheBrushes_SetBit, METADATA_PARAMS(Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bCacheBrushes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bCacheBrushes_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bShowPreview_MetaData[] = {
		{ "Category", "PreviewOptions" },
		{ "Comment", "/** Determines whether a dynamic preview is shown. Note that this introduces non-background computations \n\x09""at each event that changes the result, rather than only performing a computation on Accept. */" },
		{ "ModuleRelativePath", "Public/BspConversionTool.h" },
		{ "ToolTip", "Determines whether a dynamic preview is shown. Note that this introduces non-background computations\n      at each event that changes the result, rather than only performing a computation on Accept." },
	};
#endif
	void Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bShowPreview_SetBit(void* Obj)
	{
		((UBspConversionToolProperties*)Obj)->bShowPreview = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bShowPreview = { "bShowPreview", nullptr, (EPropertyFlags)0x0010000400000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UBspConversionToolProperties), &Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bShowPreview_SetBit, METADATA_PARAMS(Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bShowPreview_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bShowPreview_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UBspConversionToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_ConversionMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_ConversionMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bIncludeVolumes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bRemoveConvertedVolumes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bExplicitSubtractiveBrushSelection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bRemoveConvertedSubtractiveBrushes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bCacheBrushes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBspConversionToolProperties_Statics::NewProp_bShowPreview,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBspConversionToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBspConversionToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBspConversionToolProperties_Statics::ClassParams = {
		&UBspConversionToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UBspConversionToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UBspConversionToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UBspConversionToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBspConversionToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBspConversionToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBspConversionToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBspConversionToolProperties, 1193875770);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UBspConversionToolProperties>()
	{
		return UBspConversionToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBspConversionToolProperties(Z_Construct_UClass_UBspConversionToolProperties, &UBspConversionToolProperties::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UBspConversionToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBspConversionToolProperties);
	DEFINE_FUNCTION(UBspConversionToolActionPropertySet::execDeselectNonValid)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->DeselectNonValid();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UBspConversionToolActionPropertySet::execDeselectVolumes)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->DeselectVolumes();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UBspConversionToolActionPropertySet::execSelectAllValidBrushes)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SelectAllValidBrushes();
		P_NATIVE_END;
	}
	void UBspConversionToolActionPropertySet::StaticRegisterNativesUBspConversionToolActionPropertySet()
	{
		UClass* Class = UBspConversionToolActionPropertySet::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "DeselectNonValid", &UBspConversionToolActionPropertySet::execDeselectNonValid },
			{ "DeselectVolumes", &UBspConversionToolActionPropertySet::execDeselectVolumes },
			{ "SelectAllValidBrushes", &UBspConversionToolActionPropertySet::execSelectAllValidBrushes },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UBspConversionToolActionPropertySet_DeselectNonValid_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UBspConversionToolActionPropertySet_DeselectNonValid_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "SelectionOperations" },
		{ "Comment", "/** Deselect any currently selected brushes that would not be converted given current settings. */" },
		{ "DisplayName", "Deselect Non-Valid" },
		{ "DisplayPriority", "3" },
		{ "ModuleRelativePath", "Public/BspConversionTool.h" },
		{ "ToolTip", "Deselect any currently selected brushes that would not be converted given current settings." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UBspConversionToolActionPropertySet_DeselectNonValid_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UBspConversionToolActionPropertySet, nullptr, "DeselectNonValid", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UBspConversionToolActionPropertySet_DeselectNonValid_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UBspConversionToolActionPropertySet_DeselectNonValid_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UBspConversionToolActionPropertySet_DeselectNonValid()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UBspConversionToolActionPropertySet_DeselectNonValid_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UBspConversionToolActionPropertySet_DeselectVolumes_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UBspConversionToolActionPropertySet_DeselectVolumes_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "SelectionOperations" },
		{ "Comment", "/** Deselect any currently selected volume brushes. */" },
		{ "DisplayName", "Deselect Volumes" },
		{ "DisplayPriority", "2" },
		{ "ModuleRelativePath", "Public/BspConversionTool.h" },
		{ "ToolTip", "Deselect any currently selected volume brushes." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UBspConversionToolActionPropertySet_DeselectVolumes_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UBspConversionToolActionPropertySet, nullptr, "DeselectVolumes", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UBspConversionToolActionPropertySet_DeselectVolumes_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UBspConversionToolActionPropertySet_DeselectVolumes_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UBspConversionToolActionPropertySet_DeselectVolumes()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UBspConversionToolActionPropertySet_DeselectVolumes_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UBspConversionToolActionPropertySet_SelectAllValidBrushes_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UBspConversionToolActionPropertySet_SelectAllValidBrushes_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "SelectionOperations" },
		{ "Comment", "/** Select all brushes that satisfy the current settings. */" },
		{ "DisplayName", "Select All Valid Brushes" },
		{ "DisplayPriority", "1" },
		{ "ModuleRelativePath", "Public/BspConversionTool.h" },
		{ "ToolTip", "Select all brushes that satisfy the current settings." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UBspConversionToolActionPropertySet_SelectAllValidBrushes_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UBspConversionToolActionPropertySet, nullptr, "SelectAllValidBrushes", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UBspConversionToolActionPropertySet_SelectAllValidBrushes_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UBspConversionToolActionPropertySet_SelectAllValidBrushes_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UBspConversionToolActionPropertySet_SelectAllValidBrushes()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UBspConversionToolActionPropertySet_SelectAllValidBrushes_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UBspConversionToolActionPropertySet_NoRegister()
	{
		return UBspConversionToolActionPropertySet::StaticClass();
	}
	struct Z_Construct_UClass_UBspConversionToolActionPropertySet_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBspConversionToolActionPropertySet_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UBspConversionToolActionPropertySet_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UBspConversionToolActionPropertySet_DeselectNonValid, "DeselectNonValid" }, // 1793123538
		{ &Z_Construct_UFunction_UBspConversionToolActionPropertySet_DeselectVolumes, "DeselectVolumes" }, // 3940853558
		{ &Z_Construct_UFunction_UBspConversionToolActionPropertySet_SelectAllValidBrushes, "SelectAllValidBrushes" }, // 3974796963
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBspConversionToolActionPropertySet_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "BspConversionTool.h" },
		{ "ModuleRelativePath", "Public/BspConversionTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBspConversionToolActionPropertySet_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBspConversionToolActionPropertySet>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBspConversionToolActionPropertySet_Statics::ClassParams = {
		&UBspConversionToolActionPropertySet::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UBspConversionToolActionPropertySet_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBspConversionToolActionPropertySet_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBspConversionToolActionPropertySet()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBspConversionToolActionPropertySet_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBspConversionToolActionPropertySet, 1150626586);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UBspConversionToolActionPropertySet>()
	{
		return UBspConversionToolActionPropertySet::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBspConversionToolActionPropertySet(Z_Construct_UClass_UBspConversionToolActionPropertySet, &UBspConversionToolActionPropertySet::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UBspConversionToolActionPropertySet"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBspConversionToolActionPropertySet);
	void UBspConversionTool::StaticRegisterNativesUBspConversionTool()
	{
	}
	UClass* Z_Construct_UClass_UBspConversionTool_NoRegister()
	{
		return UBspConversionTool::StaticClass();
	}
	struct Z_Construct_UClass_UBspConversionTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Settings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Settings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ToolActions_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ToolActions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviewMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviewMesh;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBspConversionTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBspConversionTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Converts BSP brushes to static meshes.\n *\n * Known limitations:\n * - Preview does not respond to property changes in the brush detail panel while the tool is running. User would need\n *   to create some event that does change the preview (such as tool property change, or selection change).\n * - BSP brushes with non-manifold geometry (specifically, the stair brushes) cannot be used with the \"Convert, then combine\"\n *   path because boolean operations do not allow them. The user gets properly notified of this if it comes up.\n */" },
		{ "IncludePath", "BspConversionTool.h" },
		{ "ModuleRelativePath", "Public/BspConversionTool.h" },
		{ "ToolTip", "Converts BSP brushes to static meshes.\n\nKnown limitations:\n- Preview does not respond to property changes in the brush detail panel while the tool is running. User would need\n  to create some event that does change the preview (such as tool property change, or selection change).\n- BSP brushes with non-manifold geometry (specifically, the stair brushes) cannot be used with the \"Convert, then combine\"\n  path because boolean operations do not allow them. The user gets properly notified of this if it comes up." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBspConversionTool_Statics::NewProp_Settings_MetaData[] = {
		{ "ModuleRelativePath", "Public/BspConversionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UBspConversionTool_Statics::NewProp_Settings = { "Settings", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBspConversionTool, Settings), Z_Construct_UClass_UBspConversionToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UBspConversionTool_Statics::NewProp_Settings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBspConversionTool_Statics::NewProp_Settings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBspConversionTool_Statics::NewProp_ToolActions_MetaData[] = {
		{ "ModuleRelativePath", "Public/BspConversionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UBspConversionTool_Statics::NewProp_ToolActions = { "ToolActions", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBspConversionTool, ToolActions), Z_Construct_UClass_UBspConversionToolActionPropertySet_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UBspConversionTool_Statics::NewProp_ToolActions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBspConversionTool_Statics::NewProp_ToolActions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBspConversionTool_Statics::NewProp_PreviewMesh_MetaData[] = {
		{ "ModuleRelativePath", "Public/BspConversionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UBspConversionTool_Statics::NewProp_PreviewMesh = { "PreviewMesh", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBspConversionTool, PreviewMesh), Z_Construct_UClass_UPreviewMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UBspConversionTool_Statics::NewProp_PreviewMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBspConversionTool_Statics::NewProp_PreviewMesh_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UBspConversionTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBspConversionTool_Statics::NewProp_Settings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBspConversionTool_Statics::NewProp_ToolActions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBspConversionTool_Statics::NewProp_PreviewMesh,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBspConversionTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBspConversionTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBspConversionTool_Statics::ClassParams = {
		&UBspConversionTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UBspConversionTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UBspConversionTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UBspConversionTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBspConversionTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBspConversionTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBspConversionTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBspConversionTool, 3825387624);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UBspConversionTool>()
	{
		return UBspConversionTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBspConversionTool(Z_Construct_UClass_UBspConversionTool, &UBspConversionTool::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UBspConversionTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBspConversionTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
