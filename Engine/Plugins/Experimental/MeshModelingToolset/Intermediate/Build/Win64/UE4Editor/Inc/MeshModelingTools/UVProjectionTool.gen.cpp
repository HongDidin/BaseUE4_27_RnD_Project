// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/UVProjectionTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUVProjectionTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UUVProjectionToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UUVProjectionToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UUVProjectionToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UUVProjectionToolProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MODELINGOPERATORS_API UEnum* Z_Construct_UEnum_ModelingOperators_EUVProjectionMethod();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UUVProjectionAdvancedProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UUVProjectionAdvancedProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UUVProjectionOperatorFactory_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UUVProjectionOperatorFactory();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UUVProjectionTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UUVProjectionTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UMultiSelectionTool();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UExistingMeshMaterialProperties_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UTransformGizmo_NoRegister();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UTransformProxy_NoRegister();
// End Cross Module References
	void UUVProjectionToolBuilder::StaticRegisterNativesUUVProjectionToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UUVProjectionToolBuilder_NoRegister()
	{
		return UUVProjectionToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UUVProjectionToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUVProjectionToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVProjectionToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "UVProjectionTool.h" },
		{ "ModuleRelativePath", "Public/UVProjectionTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUVProjectionToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUVProjectionToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUVProjectionToolBuilder_Statics::ClassParams = {
		&UUVProjectionToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UUVProjectionToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUVProjectionToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUVProjectionToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUVProjectionToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUVProjectionToolBuilder, 2690749043);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UUVProjectionToolBuilder>()
	{
		return UUVProjectionToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUVProjectionToolBuilder(Z_Construct_UClass_UUVProjectionToolBuilder, &UUVProjectionToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UUVProjectionToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUVProjectionToolBuilder);
	void UUVProjectionToolProperties::StaticRegisterNativesUUVProjectionToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UUVProjectionToolProperties_NoRegister()
	{
		return UUVProjectionToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UUVProjectionToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_UVProjectionMethod_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UVProjectionMethod_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_UVProjectionMethod;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProjectionPrimitiveScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ProjectionPrimitiveScale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CylinderProjectToTopOrBottomAngleThreshold_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CylinderProjectToTopOrBottomAngleThreshold;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UVScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_UVScale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UVOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_UVOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bWorldSpaceUVScale_MetaData[];
#endif
		static void NewProp_bWorldSpaceUVScale_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bWorldSpaceUVScale;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUVProjectionToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVProjectionToolProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Standard properties\n */" },
		{ "IncludePath", "UVProjectionTool.h" },
		{ "ModuleRelativePath", "Public/UVProjectionTool.h" },
		{ "ToolTip", "Standard properties" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UUVProjectionToolProperties_Statics::NewProp_UVProjectionMethod_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVProjectionToolProperties_Statics::NewProp_UVProjectionMethod_MetaData[] = {
		{ "Category", "ProjectionSettings" },
		{ "Comment", "/** Choose the UV projection method (cube, cylinder, plane) */" },
		{ "ModuleRelativePath", "Public/UVProjectionTool.h" },
		{ "ToolTip", "Choose the UV projection method (cube, cylinder, plane)" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UUVProjectionToolProperties_Statics::NewProp_UVProjectionMethod = { "UVProjectionMethod", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUVProjectionToolProperties, UVProjectionMethod), Z_Construct_UEnum_ModelingOperators_EUVProjectionMethod, METADATA_PARAMS(Z_Construct_UClass_UUVProjectionToolProperties_Statics::NewProp_UVProjectionMethod_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUVProjectionToolProperties_Statics::NewProp_UVProjectionMethod_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVProjectionToolProperties_Statics::NewProp_ProjectionPrimitiveScale_MetaData[] = {
		{ "Category", "ProjectionSettings" },
		{ "Comment", "/** Per-axis scaling of projection primitive */" },
		{ "ModuleRelativePath", "Public/UVProjectionTool.h" },
		{ "ToolTip", "Per-axis scaling of projection primitive" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UUVProjectionToolProperties_Statics::NewProp_ProjectionPrimitiveScale = { "ProjectionPrimitiveScale", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUVProjectionToolProperties, ProjectionPrimitiveScale), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UUVProjectionToolProperties_Statics::NewProp_ProjectionPrimitiveScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUVProjectionToolProperties_Statics::NewProp_ProjectionPrimitiveScale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVProjectionToolProperties_Statics::NewProp_CylinderProjectToTopOrBottomAngleThreshold_MetaData[] = {
		{ "Category", "ProjectionSettings" },
		{ "ClampMax", "90.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** If triangle normal direction is within this threshold degrees of the cylinder top/bottom plane direction, project UVs to the top/bottom plane instead of the sides */" },
		{ "Default", "1" },
		{ "EditCondition", "UVProjectionMethod == EUVProjectionMethod::Cylinder" },
		{ "ModuleRelativePath", "Public/UVProjectionTool.h" },
		{ "ToolTip", "If triangle normal direction is within this threshold degrees of the cylinder top/bottom plane direction, project UVs to the top/bottom plane instead of the sides" },
		{ "UIMax", "20" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UUVProjectionToolProperties_Statics::NewProp_CylinderProjectToTopOrBottomAngleThreshold = { "CylinderProjectToTopOrBottomAngleThreshold", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUVProjectionToolProperties, CylinderProjectToTopOrBottomAngleThreshold), METADATA_PARAMS(Z_Construct_UClass_UUVProjectionToolProperties_Statics::NewProp_CylinderProjectToTopOrBottomAngleThreshold_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUVProjectionToolProperties_Statics::NewProp_CylinderProjectToTopOrBottomAngleThreshold_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVProjectionToolProperties_Statics::NewProp_UVScale_MetaData[] = {
		{ "Category", "ProjectionSettings" },
		{ "Comment", "/** Choose the UV scale factors */" },
		{ "ModuleRelativePath", "Public/UVProjectionTool.h" },
		{ "ToolTip", "Choose the UV scale factors" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UUVProjectionToolProperties_Statics::NewProp_UVScale = { "UVScale", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUVProjectionToolProperties, UVScale), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UClass_UUVProjectionToolProperties_Statics::NewProp_UVScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUVProjectionToolProperties_Statics::NewProp_UVScale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVProjectionToolProperties_Statics::NewProp_UVOffset_MetaData[] = {
		{ "Category", "ProjectionSettings" },
		{ "Comment", "/** Choose the UV offsets */" },
		{ "ModuleRelativePath", "Public/UVProjectionTool.h" },
		{ "ToolTip", "Choose the UV offsets" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UUVProjectionToolProperties_Statics::NewProp_UVOffset = { "UVOffset", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUVProjectionToolProperties, UVOffset), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UClass_UUVProjectionToolProperties_Statics::NewProp_UVOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUVProjectionToolProperties_Statics::NewProp_UVOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVProjectionToolProperties_Statics::NewProp_bWorldSpaceUVScale_MetaData[] = {
		{ "Category", "ProjectionSettings" },
		{ "Comment", "/** If set, UV scales will be relative to world space so different objects created with the same UV scale should have the same average texel size */" },
		{ "DisplayName", "UV Scale Relative to World Space" },
		{ "ModuleRelativePath", "Public/UVProjectionTool.h" },
		{ "ToolTip", "If set, UV scales will be relative to world space so different objects created with the same UV scale should have the same average texel size" },
	};
#endif
	void Z_Construct_UClass_UUVProjectionToolProperties_Statics::NewProp_bWorldSpaceUVScale_SetBit(void* Obj)
	{
		((UUVProjectionToolProperties*)Obj)->bWorldSpaceUVScale = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UUVProjectionToolProperties_Statics::NewProp_bWorldSpaceUVScale = { "bWorldSpaceUVScale", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UUVProjectionToolProperties), &Z_Construct_UClass_UUVProjectionToolProperties_Statics::NewProp_bWorldSpaceUVScale_SetBit, METADATA_PARAMS(Z_Construct_UClass_UUVProjectionToolProperties_Statics::NewProp_bWorldSpaceUVScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUVProjectionToolProperties_Statics::NewProp_bWorldSpaceUVScale_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UUVProjectionToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVProjectionToolProperties_Statics::NewProp_UVProjectionMethod_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVProjectionToolProperties_Statics::NewProp_UVProjectionMethod,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVProjectionToolProperties_Statics::NewProp_ProjectionPrimitiveScale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVProjectionToolProperties_Statics::NewProp_CylinderProjectToTopOrBottomAngleThreshold,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVProjectionToolProperties_Statics::NewProp_UVScale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVProjectionToolProperties_Statics::NewProp_UVOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVProjectionToolProperties_Statics::NewProp_bWorldSpaceUVScale,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUVProjectionToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUVProjectionToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUVProjectionToolProperties_Statics::ClassParams = {
		&UUVProjectionToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UUVProjectionToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UUVProjectionToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UUVProjectionToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUVProjectionToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUVProjectionToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUVProjectionToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUVProjectionToolProperties, 1348525634);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UUVProjectionToolProperties>()
	{
		return UUVProjectionToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUVProjectionToolProperties(Z_Construct_UClass_UUVProjectionToolProperties, &UUVProjectionToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UUVProjectionToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUVProjectionToolProperties);
	void UUVProjectionAdvancedProperties::StaticRegisterNativesUUVProjectionAdvancedProperties()
	{
	}
	UClass* Z_Construct_UClass_UUVProjectionAdvancedProperties_NoRegister()
	{
		return UUVProjectionAdvancedProperties::StaticClass();
	}
	struct Z_Construct_UClass_UUVProjectionAdvancedProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUVProjectionAdvancedProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVProjectionAdvancedProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Advanced properties\n */" },
		{ "IncludePath", "UVProjectionTool.h" },
		{ "ModuleRelativePath", "Public/UVProjectionTool.h" },
		{ "ToolTip", "Advanced properties" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUVProjectionAdvancedProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUVProjectionAdvancedProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUVProjectionAdvancedProperties_Statics::ClassParams = {
		&UUVProjectionAdvancedProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UUVProjectionAdvancedProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUVProjectionAdvancedProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUVProjectionAdvancedProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUVProjectionAdvancedProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUVProjectionAdvancedProperties, 3064644479);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UUVProjectionAdvancedProperties>()
	{
		return UUVProjectionAdvancedProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUVProjectionAdvancedProperties(Z_Construct_UClass_UUVProjectionAdvancedProperties, &UUVProjectionAdvancedProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UUVProjectionAdvancedProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUVProjectionAdvancedProperties);
	void UUVProjectionOperatorFactory::StaticRegisterNativesUUVProjectionOperatorFactory()
	{
	}
	UClass* Z_Construct_UClass_UUVProjectionOperatorFactory_NoRegister()
	{
		return UUVProjectionOperatorFactory::StaticClass();
	}
	struct Z_Construct_UClass_UUVProjectionOperatorFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Tool_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Tool;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUVProjectionOperatorFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVProjectionOperatorFactory_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Factory with enough info to spawn the background-thread Operator to do a chunk of work for the tool\n *  stores a pointer to the tool and enough info to know which specific operator it should spawn\n */" },
		{ "IncludePath", "UVProjectionTool.h" },
		{ "ModuleRelativePath", "Public/UVProjectionTool.h" },
		{ "ToolTip", "Factory with enough info to spawn the background-thread Operator to do a chunk of work for the tool\n stores a pointer to the tool and enough info to know which specific operator it should spawn" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVProjectionOperatorFactory_Statics::NewProp_Tool_MetaData[] = {
		{ "ModuleRelativePath", "Public/UVProjectionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUVProjectionOperatorFactory_Statics::NewProp_Tool = { "Tool", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUVProjectionOperatorFactory, Tool), Z_Construct_UClass_UUVProjectionTool_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUVProjectionOperatorFactory_Statics::NewProp_Tool_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUVProjectionOperatorFactory_Statics::NewProp_Tool_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UUVProjectionOperatorFactory_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVProjectionOperatorFactory_Statics::NewProp_Tool,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUVProjectionOperatorFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUVProjectionOperatorFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUVProjectionOperatorFactory_Statics::ClassParams = {
		&UUVProjectionOperatorFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UUVProjectionOperatorFactory_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UUVProjectionOperatorFactory_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UUVProjectionOperatorFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUVProjectionOperatorFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUVProjectionOperatorFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUVProjectionOperatorFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUVProjectionOperatorFactory, 3715470533);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UUVProjectionOperatorFactory>()
	{
		return UUVProjectionOperatorFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUVProjectionOperatorFactory(Z_Construct_UClass_UUVProjectionOperatorFactory, &UUVProjectionOperatorFactory::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UUVProjectionOperatorFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUVProjectionOperatorFactory);
	void UUVProjectionTool::StaticRegisterNativesUUVProjectionTool()
	{
	}
	UClass* Z_Construct_UClass_UUVProjectionTool_NoRegister()
	{
		return UUVProjectionTool::StaticClass();
	}
	struct Z_Construct_UClass_UUVProjectionTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BasicProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BasicProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AdvancedProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AdvancedProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaterialSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MaterialSettings;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Previews_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Previews_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Previews;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CheckerMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CheckerMaterial;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TransformGizmos_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransformGizmos_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_TransformGizmos;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TransformProxies_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransformProxies_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_TransformProxies;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUVProjectionTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMultiSelectionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVProjectionTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Simple Mesh Normal Updating Tool\n */" },
		{ "IncludePath", "UVProjectionTool.h" },
		{ "ModuleRelativePath", "Public/UVProjectionTool.h" },
		{ "ToolTip", "Simple Mesh Normal Updating Tool" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVProjectionTool_Statics::NewProp_BasicProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/UVProjectionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUVProjectionTool_Statics::NewProp_BasicProperties = { "BasicProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUVProjectionTool, BasicProperties), Z_Construct_UClass_UUVProjectionToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUVProjectionTool_Statics::NewProp_BasicProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUVProjectionTool_Statics::NewProp_BasicProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVProjectionTool_Statics::NewProp_AdvancedProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/UVProjectionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUVProjectionTool_Statics::NewProp_AdvancedProperties = { "AdvancedProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUVProjectionTool, AdvancedProperties), Z_Construct_UClass_UUVProjectionAdvancedProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUVProjectionTool_Statics::NewProp_AdvancedProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUVProjectionTool_Statics::NewProp_AdvancedProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVProjectionTool_Statics::NewProp_MaterialSettings_MetaData[] = {
		{ "ModuleRelativePath", "Public/UVProjectionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUVProjectionTool_Statics::NewProp_MaterialSettings = { "MaterialSettings", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUVProjectionTool, MaterialSettings), Z_Construct_UClass_UExistingMeshMaterialProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUVProjectionTool_Statics::NewProp_MaterialSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUVProjectionTool_Statics::NewProp_MaterialSettings_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUVProjectionTool_Statics::NewProp_Previews_Inner = { "Previews", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVProjectionTool_Statics::NewProp_Previews_MetaData[] = {
		{ "ModuleRelativePath", "Public/UVProjectionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UUVProjectionTool_Statics::NewProp_Previews = { "Previews", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUVProjectionTool, Previews), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UUVProjectionTool_Statics::NewProp_Previews_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUVProjectionTool_Statics::NewProp_Previews_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVProjectionTool_Statics::NewProp_CheckerMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Public/UVProjectionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUVProjectionTool_Statics::NewProp_CheckerMaterial = { "CheckerMaterial", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUVProjectionTool, CheckerMaterial), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUVProjectionTool_Statics::NewProp_CheckerMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUVProjectionTool_Statics::NewProp_CheckerMaterial_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUVProjectionTool_Statics::NewProp_TransformGizmos_Inner = { "TransformGizmos", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UTransformGizmo_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVProjectionTool_Statics::NewProp_TransformGizmos_MetaData[] = {
		{ "ModuleRelativePath", "Public/UVProjectionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UUVProjectionTool_Statics::NewProp_TransformGizmos = { "TransformGizmos", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUVProjectionTool, TransformGizmos), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UUVProjectionTool_Statics::NewProp_TransformGizmos_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUVProjectionTool_Statics::NewProp_TransformGizmos_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUVProjectionTool_Statics::NewProp_TransformProxies_Inner = { "TransformProxies", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UTransformProxy_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVProjectionTool_Statics::NewProp_TransformProxies_MetaData[] = {
		{ "ModuleRelativePath", "Public/UVProjectionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UUVProjectionTool_Statics::NewProp_TransformProxies = { "TransformProxies", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUVProjectionTool, TransformProxies), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UUVProjectionTool_Statics::NewProp_TransformProxies_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUVProjectionTool_Statics::NewProp_TransformProxies_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UUVProjectionTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVProjectionTool_Statics::NewProp_BasicProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVProjectionTool_Statics::NewProp_AdvancedProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVProjectionTool_Statics::NewProp_MaterialSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVProjectionTool_Statics::NewProp_Previews_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVProjectionTool_Statics::NewProp_Previews,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVProjectionTool_Statics::NewProp_CheckerMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVProjectionTool_Statics::NewProp_TransformGizmos_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVProjectionTool_Statics::NewProp_TransformGizmos,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVProjectionTool_Statics::NewProp_TransformProxies_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVProjectionTool_Statics::NewProp_TransformProxies,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUVProjectionTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUVProjectionTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUVProjectionTool_Statics::ClassParams = {
		&UUVProjectionTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UUVProjectionTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UUVProjectionTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UUVProjectionTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUVProjectionTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUVProjectionTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUVProjectionTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUVProjectionTool, 470951464);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UUVProjectionTool>()
	{
		return UUVProjectionTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUVProjectionTool(Z_Construct_UClass_UUVProjectionTool, &UUVProjectionTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UUVProjectionTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUVProjectionTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
