// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingToolsEditorOnly/Public/Hair/GroomToMeshTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGroomToMeshTool() {}
// Cross Module References
	MESHMODELINGTOOLSEDITORONLY_API UEnum* Z_Construct_UEnum_MeshModelingToolsEditorOnly_EGroomToMeshUVMode();
	UPackage* Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UGroomToMeshToolBuilder_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UGroomToMeshToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UGroomToMeshToolProperties_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UGroomToMeshToolProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	ENGINE_API UClass* Z_Construct_UClass_AStaticMeshActor_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UGroomToMeshTool_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UGroomToMeshTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveTool();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPreviewMesh_NoRegister();
	HAIRSTRANDSCORE_API UClass* Z_Construct_UClass_AGroomActor_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPreviewGeometry_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
// End Cross Module References
	static UEnum* EGroomToMeshUVMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingToolsEditorOnly_EGroomToMeshUVMode, Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly(), TEXT("EGroomToMeshUVMode"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLSEDITORONLY_API UEnum* StaticEnum<EGroomToMeshUVMode>()
	{
		return EGroomToMeshUVMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EGroomToMeshUVMode(EGroomToMeshUVMode_StaticEnum, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("EGroomToMeshUVMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingToolsEditorOnly_EGroomToMeshUVMode_Hash() { return 4134961426U; }
	UEnum* Z_Construct_UEnum_MeshModelingToolsEditorOnly_EGroomToMeshUVMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EGroomToMeshUVMode"), 0, Get_Z_Construct_UEnum_MeshModelingToolsEditorOnly_EGroomToMeshUVMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EGroomToMeshUVMode::PlanarSplitting", (int64)EGroomToMeshUVMode::PlanarSplitting },
				{ "EGroomToMeshUVMode::MinimalConformal", (int64)EGroomToMeshUVMode::MinimalConformal },
				{ "EGroomToMeshUVMode::PlanarSplitConformal", (int64)EGroomToMeshUVMode::PlanarSplitConformal },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "MinimalConformal.Name", "EGroomToMeshUVMode::MinimalConformal" },
				{ "ModuleRelativePath", "Public/Hair/GroomToMeshTool.h" },
				{ "PlanarSplitConformal.Name", "EGroomToMeshUVMode::PlanarSplitConformal" },
				{ "PlanarSplitting.Name", "EGroomToMeshUVMode::PlanarSplitting" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
				nullptr,
				"EGroomToMeshUVMode",
				"EGroomToMeshUVMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UGroomToMeshToolBuilder::StaticRegisterNativesUGroomToMeshToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UGroomToMeshToolBuilder_NoRegister()
	{
		return UGroomToMeshToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UGroomToMeshToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGroomToMeshToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomToMeshToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "Hair/GroomToMeshTool.h" },
		{ "ModuleRelativePath", "Public/Hair/GroomToMeshTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGroomToMeshToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGroomToMeshToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGroomToMeshToolBuilder_Statics::ClassParams = {
		&UGroomToMeshToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UGroomToMeshToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomToMeshToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGroomToMeshToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGroomToMeshToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGroomToMeshToolBuilder, 3044916642);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UGroomToMeshToolBuilder>()
	{
		return UGroomToMeshToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGroomToMeshToolBuilder(Z_Construct_UClass_UGroomToMeshToolBuilder, &UGroomToMeshToolBuilder::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UGroomToMeshToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGroomToMeshToolBuilder);
	void UGroomToMeshToolProperties::StaticRegisterNativesUGroomToMeshToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UGroomToMeshToolProperties_NoRegister()
	{
		return UGroomToMeshToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UGroomToMeshToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VoxelCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_VoxelCount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BlendPower_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BlendPower;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RadiusScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RadiusScale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bApplyMorphology_MetaData[];
#endif
		static void NewProp_bApplyMorphology_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bApplyMorphology;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClosingDist_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ClosingDist;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OpeningDist_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_OpeningDist;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bClipToHead_MetaData[];
#endif
		static void NewProp_bClipToHead_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bClipToHead;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClipMeshActor_MetaData[];
#endif
		static const UE4CodeGen_Private::FLazyObjectPropertyParams NewProp_ClipMeshActor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSmooth_MetaData[];
#endif
		static void NewProp_bSmooth_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSmooth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Smoothness_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Smoothness;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VolumeCorrection_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_VolumeCorrection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSimplify_MetaData[];
#endif
		static void NewProp_bSimplify_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSimplify;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VertexCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_VertexCount;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_UVMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UVMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_UVMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowSideBySide_MetaData[];
#endif
		static void NewProp_bShowSideBySide_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowSideBySide;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowGuides_MetaData[];
#endif
		static void NewProp_bShowGuides_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowGuides;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowUVs_MetaData[];
#endif
		static void NewProp_bShowUVs_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowUVs;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGroomToMeshToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomToMeshToolProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Hair/GroomToMeshTool.h" },
		{ "ModuleRelativePath", "Public/Hair/GroomToMeshTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_VoxelCount_MetaData[] = {
		{ "Category", "Meshing" },
		{ "ClampMax", "1024" },
		{ "ClampMin", "8" },
		{ "Comment", "/** The size of the geometry bounding box major axis measured in voxels */" },
		{ "ModuleRelativePath", "Public/Hair/GroomToMeshTool.h" },
		{ "ToolTip", "The size of the geometry bounding box major axis measured in voxels" },
		{ "UIMax", "512" },
		{ "UIMin", "8" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_VoxelCount = { "VoxelCount", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomToMeshToolProperties, VoxelCount), METADATA_PARAMS(Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_VoxelCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_VoxelCount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_BlendPower_MetaData[] = {
		{ "Category", "Meshing" },
		{ "ClampMax", "128.0" },
		{ "ClampMin", "0.1" },
		{ "ModuleRelativePath", "Public/Hair/GroomToMeshTool.h" },
		{ "UIMax", "16.0" },
		{ "UIMin", "0.5" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_BlendPower = { "BlendPower", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomToMeshToolProperties, BlendPower), METADATA_PARAMS(Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_BlendPower_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_BlendPower_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_RadiusScale_MetaData[] = {
		{ "Category", "Meshing" },
		{ "ClampMax", "128.0" },
		{ "ClampMin", "0.1" },
		{ "ModuleRelativePath", "Public/Hair/GroomToMeshTool.h" },
		{ "UIMax", "4.0" },
		{ "UIMin", "0.1" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_RadiusScale = { "RadiusScale", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomToMeshToolProperties, RadiusScale), METADATA_PARAMS(Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_RadiusScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_RadiusScale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bApplyMorphology_MetaData[] = {
		{ "Category", "Morphology" },
		{ "ModuleRelativePath", "Public/Hair/GroomToMeshTool.h" },
	};
#endif
	void Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bApplyMorphology_SetBit(void* Obj)
	{
		((UGroomToMeshToolProperties*)Obj)->bApplyMorphology = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bApplyMorphology = { "bApplyMorphology", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UGroomToMeshToolProperties), &Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bApplyMorphology_SetBit, METADATA_PARAMS(Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bApplyMorphology_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bApplyMorphology_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_ClosingDist_MetaData[] = {
		{ "Category", "Morphology" },
		{ "ClampMax", "128.0" },
		{ "ClampMin", "0.0" },
		{ "ModuleRelativePath", "Public/Hair/GroomToMeshTool.h" },
		{ "UIMax", "50.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_ClosingDist = { "ClosingDist", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomToMeshToolProperties, ClosingDist), METADATA_PARAMS(Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_ClosingDist_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_ClosingDist_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_OpeningDist_MetaData[] = {
		{ "Category", "Morphology" },
		{ "ClampMax", "128.0" },
		{ "ClampMin", "0.0" },
		{ "ModuleRelativePath", "Public/Hair/GroomToMeshTool.h" },
		{ "UIMax", "50.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_OpeningDist = { "OpeningDist", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomToMeshToolProperties, OpeningDist), METADATA_PARAMS(Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_OpeningDist_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_OpeningDist_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bClipToHead_MetaData[] = {
		{ "Category", "Clipping" },
		{ "ModuleRelativePath", "Public/Hair/GroomToMeshTool.h" },
	};
#endif
	void Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bClipToHead_SetBit(void* Obj)
	{
		((UGroomToMeshToolProperties*)Obj)->bClipToHead = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bClipToHead = { "bClipToHead", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UGroomToMeshToolProperties), &Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bClipToHead_SetBit, METADATA_PARAMS(Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bClipToHead_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bClipToHead_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_ClipMeshActor_MetaData[] = {
		{ "Category", "Clipping" },
		{ "Comment", "// todo: this probably also needs to support skeletal mesh\n" },
		{ "ModuleRelativePath", "Public/Hair/GroomToMeshTool.h" },
		{ "ToolTip", "todo: this probably also needs to support skeletal mesh" },
	};
#endif
	const UE4CodeGen_Private::FLazyObjectPropertyParams Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_ClipMeshActor = { "ClipMeshActor", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::LazyObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomToMeshToolProperties, ClipMeshActor), Z_Construct_UClass_AStaticMeshActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_ClipMeshActor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_ClipMeshActor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bSmooth_MetaData[] = {
		{ "Category", "Smoothing" },
		{ "ModuleRelativePath", "Public/Hair/GroomToMeshTool.h" },
	};
#endif
	void Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bSmooth_SetBit(void* Obj)
	{
		((UGroomToMeshToolProperties*)Obj)->bSmooth = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bSmooth = { "bSmooth", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UGroomToMeshToolProperties), &Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bSmooth_SetBit, METADATA_PARAMS(Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bSmooth_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bSmooth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_Smoothness_MetaData[] = {
		{ "Category", "Smoothing" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0.0" },
		{ "ModuleRelativePath", "Public/Hair/GroomToMeshTool.h" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_Smoothness = { "Smoothness", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomToMeshToolProperties, Smoothness), METADATA_PARAMS(Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_Smoothness_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_Smoothness_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_VolumeCorrection_MetaData[] = {
		{ "Category", "Smoothing" },
		{ "ClampMax", "2.0" },
		{ "ClampMin", "-2.0" },
		{ "ModuleRelativePath", "Public/Hair/GroomToMeshTool.h" },
		{ "UIMax", "1.0" },
		{ "UIMin", "-1.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_VolumeCorrection = { "VolumeCorrection", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomToMeshToolProperties, VolumeCorrection), METADATA_PARAMS(Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_VolumeCorrection_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_VolumeCorrection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bSimplify_MetaData[] = {
		{ "Category", "Simplification" },
		{ "ModuleRelativePath", "Public/Hair/GroomToMeshTool.h" },
	};
#endif
	void Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bSimplify_SetBit(void* Obj)
	{
		((UGroomToMeshToolProperties*)Obj)->bSimplify = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bSimplify = { "bSimplify", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UGroomToMeshToolProperties), &Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bSimplify_SetBit, METADATA_PARAMS(Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bSimplify_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bSimplify_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_VertexCount_MetaData[] = {
		{ "Category", "Simplification" },
		{ "ClampMax", "9999999999" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Target triangle count */" },
		{ "EditCondition", "bSimplify == true" },
		{ "ModuleRelativePath", "Public/Hair/GroomToMeshTool.h" },
		{ "ToolTip", "Target triangle count" },
		{ "UIMax", "5000" },
		{ "UIMin", "4" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_VertexCount = { "VertexCount", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomToMeshToolProperties, VertexCount), METADATA_PARAMS(Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_VertexCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_VertexCount_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_UVMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_UVMode_MetaData[] = {
		{ "Category", "UVGeneration" },
		{ "ModuleRelativePath", "Public/Hair/GroomToMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_UVMode = { "UVMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomToMeshToolProperties, UVMode), Z_Construct_UEnum_MeshModelingToolsEditorOnly_EGroomToMeshUVMode, METADATA_PARAMS(Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_UVMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_UVMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bShowSideBySide_MetaData[] = {
		{ "Category", "Visualization" },
		{ "ModuleRelativePath", "Public/Hair/GroomToMeshTool.h" },
	};
#endif
	void Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bShowSideBySide_SetBit(void* Obj)
	{
		((UGroomToMeshToolProperties*)Obj)->bShowSideBySide = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bShowSideBySide = { "bShowSideBySide", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UGroomToMeshToolProperties), &Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bShowSideBySide_SetBit, METADATA_PARAMS(Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bShowSideBySide_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bShowSideBySide_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bShowGuides_MetaData[] = {
		{ "Category", "Visualization" },
		{ "ModuleRelativePath", "Public/Hair/GroomToMeshTool.h" },
	};
#endif
	void Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bShowGuides_SetBit(void* Obj)
	{
		((UGroomToMeshToolProperties*)Obj)->bShowGuides = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bShowGuides = { "bShowGuides", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UGroomToMeshToolProperties), &Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bShowGuides_SetBit, METADATA_PARAMS(Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bShowGuides_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bShowGuides_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bShowUVs_MetaData[] = {
		{ "Category", "Visualization" },
		{ "ModuleRelativePath", "Public/Hair/GroomToMeshTool.h" },
	};
#endif
	void Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bShowUVs_SetBit(void* Obj)
	{
		((UGroomToMeshToolProperties*)Obj)->bShowUVs = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bShowUVs = { "bShowUVs", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UGroomToMeshToolProperties), &Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bShowUVs_SetBit, METADATA_PARAMS(Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bShowUVs_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bShowUVs_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGroomToMeshToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_VoxelCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_BlendPower,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_RadiusScale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bApplyMorphology,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_ClosingDist,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_OpeningDist,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bClipToHead,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_ClipMeshActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bSmooth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_Smoothness,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_VolumeCorrection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bSimplify,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_VertexCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_UVMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_UVMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bShowSideBySide,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bShowGuides,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomToMeshToolProperties_Statics::NewProp_bShowUVs,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGroomToMeshToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGroomToMeshToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGroomToMeshToolProperties_Statics::ClassParams = {
		&UGroomToMeshToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UGroomToMeshToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UGroomToMeshToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UGroomToMeshToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomToMeshToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGroomToMeshToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGroomToMeshToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGroomToMeshToolProperties, 793655969);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UGroomToMeshToolProperties>()
	{
		return UGroomToMeshToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGroomToMeshToolProperties(Z_Construct_UClass_UGroomToMeshToolProperties, &UGroomToMeshToolProperties::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UGroomToMeshToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGroomToMeshToolProperties);
	void UGroomToMeshTool::StaticRegisterNativesUGroomToMeshTool()
	{
	}
	UClass* Z_Construct_UClass_UGroomToMeshTool_NoRegister()
	{
		return UGroomToMeshTool::StaticClass();
	}
	struct Z_Construct_UClass_UGroomToMeshTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Settings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Settings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviewMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviewMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetGroom_MetaData[];
#endif
		static const UE4CodeGen_Private::FLazyObjectPropertyParams NewProp_TargetGroom;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviewGeom_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviewGeom;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MeshMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UVMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_UVMaterial;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGroomToMeshTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomToMeshTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "Hair/GroomToMeshTool.h" },
		{ "ModuleRelativePath", "Public/Hair/GroomToMeshTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomToMeshTool_Statics::NewProp_Settings_MetaData[] = {
		{ "ModuleRelativePath", "Public/Hair/GroomToMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomToMeshTool_Statics::NewProp_Settings = { "Settings", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomToMeshTool, Settings), Z_Construct_UClass_UGroomToMeshToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomToMeshTool_Statics::NewProp_Settings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomToMeshTool_Statics::NewProp_Settings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomToMeshTool_Statics::NewProp_PreviewMesh_MetaData[] = {
		{ "ModuleRelativePath", "Public/Hair/GroomToMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomToMeshTool_Statics::NewProp_PreviewMesh = { "PreviewMesh", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomToMeshTool, PreviewMesh), Z_Construct_UClass_UPreviewMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomToMeshTool_Statics::NewProp_PreviewMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomToMeshTool_Statics::NewProp_PreviewMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomToMeshTool_Statics::NewProp_TargetGroom_MetaData[] = {
		{ "ModuleRelativePath", "Public/Hair/GroomToMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FLazyObjectPropertyParams Z_Construct_UClass_UGroomToMeshTool_Statics::NewProp_TargetGroom = { "TargetGroom", nullptr, (EPropertyFlags)0x0024080000000000, UE4CodeGen_Private::EPropertyGenFlags::LazyObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomToMeshTool, TargetGroom), Z_Construct_UClass_AGroomActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomToMeshTool_Statics::NewProp_TargetGroom_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomToMeshTool_Statics::NewProp_TargetGroom_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomToMeshTool_Statics::NewProp_PreviewGeom_MetaData[] = {
		{ "ModuleRelativePath", "Public/Hair/GroomToMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomToMeshTool_Statics::NewProp_PreviewGeom = { "PreviewGeom", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomToMeshTool, PreviewGeom), Z_Construct_UClass_UPreviewGeometry_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomToMeshTool_Statics::NewProp_PreviewGeom_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomToMeshTool_Statics::NewProp_PreviewGeom_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomToMeshTool_Statics::NewProp_MeshMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Public/Hair/GroomToMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomToMeshTool_Statics::NewProp_MeshMaterial = { "MeshMaterial", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomToMeshTool, MeshMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomToMeshTool_Statics::NewProp_MeshMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomToMeshTool_Statics::NewProp_MeshMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomToMeshTool_Statics::NewProp_UVMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Public/Hair/GroomToMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomToMeshTool_Statics::NewProp_UVMaterial = { "UVMaterial", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomToMeshTool, UVMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomToMeshTool_Statics::NewProp_UVMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomToMeshTool_Statics::NewProp_UVMaterial_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGroomToMeshTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomToMeshTool_Statics::NewProp_Settings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomToMeshTool_Statics::NewProp_PreviewMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomToMeshTool_Statics::NewProp_TargetGroom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomToMeshTool_Statics::NewProp_PreviewGeom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomToMeshTool_Statics::NewProp_MeshMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomToMeshTool_Statics::NewProp_UVMaterial,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGroomToMeshTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGroomToMeshTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGroomToMeshTool_Statics::ClassParams = {
		&UGroomToMeshTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UGroomToMeshTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UGroomToMeshTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UGroomToMeshTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomToMeshTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGroomToMeshTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGroomToMeshTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGroomToMeshTool, 288855624);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UGroomToMeshTool>()
	{
		return UGroomToMeshTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGroomToMeshTool(Z_Construct_UClass_UGroomToMeshTool, &UGroomToMeshTool::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UGroomToMeshTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGroomToMeshTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
