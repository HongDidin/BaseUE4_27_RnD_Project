// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/BakeMeshAttributeMapsTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBakeMeshAttributeMapsTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EBakedCurvatureClampMode();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EBakedCurvatureColorMode();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EBakedCurvatureTypeMode();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EOcclusionMapPreview();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EOcclusionMapDistribution();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_ENormalMapSpace();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EBakeTextureResolution();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EBakeMapType();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UBakeMeshAttributeMapsToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UBakeMeshAttributeMapsToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	ENGINE_API UClass* Z_Construct_UClass_UTexture2D_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UBakedNormalMapToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UBakedNormalMapToolProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UBakedOcclusionMapToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UBakedOcclusionMapToolProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UBakedOcclusionMapVisualizationProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UBakedOcclusionMapVisualizationProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UBakedCurvatureMapToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UBakedCurvatureMapToolProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UBakedTexture2DImageProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UBakedTexture2DImageProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UBakeMeshAttributeMapsTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UBakeMeshAttributeMapsTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UMultiSelectionTool();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister();
// End Cross Module References
	static UEnum* EBakedCurvatureClampMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EBakedCurvatureClampMode, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EBakedCurvatureClampMode"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EBakedCurvatureClampMode>()
	{
		return EBakedCurvatureClampMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EBakedCurvatureClampMode(EBakedCurvatureClampMode_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EBakedCurvatureClampMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EBakedCurvatureClampMode_Hash() { return 291588759U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EBakedCurvatureClampMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EBakedCurvatureClampMode"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EBakedCurvatureClampMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EBakedCurvatureClampMode::None", (int64)EBakedCurvatureClampMode::None },
				{ "EBakedCurvatureClampMode::Positive", (int64)EBakedCurvatureClampMode::Positive },
				{ "EBakedCurvatureClampMode::Negative", (int64)EBakedCurvatureClampMode::Negative },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
				{ "Negative.Comment", "/** Clamp positive curvatures to zero */" },
				{ "Negative.Name", "EBakedCurvatureClampMode::Negative" },
				{ "Negative.ToolTip", "Clamp positive curvatures to zero" },
				{ "None.Comment", "/** Include both negative and positive curvatures */" },
				{ "None.Name", "EBakedCurvatureClampMode::None" },
				{ "None.ToolTip", "Include both negative and positive curvatures" },
				{ "Positive.Comment", "/** Clamp negative curvatures to zero */" },
				{ "Positive.Name", "EBakedCurvatureClampMode::Positive" },
				{ "Positive.ToolTip", "Clamp negative curvatures to zero" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EBakedCurvatureClampMode",
				"EBakedCurvatureClampMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EBakedCurvatureColorMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EBakedCurvatureColorMode, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EBakedCurvatureColorMode"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EBakedCurvatureColorMode>()
	{
		return EBakedCurvatureColorMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EBakedCurvatureColorMode(EBakedCurvatureColorMode_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EBakedCurvatureColorMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EBakedCurvatureColorMode_Hash() { return 2001264309U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EBakedCurvatureColorMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EBakedCurvatureColorMode"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EBakedCurvatureColorMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EBakedCurvatureColorMode::Grayscale", (int64)EBakedCurvatureColorMode::Grayscale },
				{ "EBakedCurvatureColorMode::RedBlue", (int64)EBakedCurvatureColorMode::RedBlue },
				{ "EBakedCurvatureColorMode::RedGreenBlue", (int64)EBakedCurvatureColorMode::RedGreenBlue },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Grayscale.Comment", "/** Map curvature values to grayscale such that black is negative, grey is zero, and white is positive */" },
				{ "Grayscale.Name", "EBakedCurvatureColorMode::Grayscale" },
				{ "Grayscale.ToolTip", "Map curvature values to grayscale such that black is negative, grey is zero, and white is positive" },
				{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
				{ "RedBlue.Comment", "/** Map curvature values to red/blue scale such that red is negative, black is zero, and blue is positive */" },
				{ "RedBlue.Name", "EBakedCurvatureColorMode::RedBlue" },
				{ "RedBlue.ToolTip", "Map curvature values to red/blue scale such that red is negative, black is zero, and blue is positive" },
				{ "RedGreenBlue.Comment", "/** Map curvature values to red/green/blue scale such that red is negative, green is zero, and blue is positive */" },
				{ "RedGreenBlue.Name", "EBakedCurvatureColorMode::RedGreenBlue" },
				{ "RedGreenBlue.ToolTip", "Map curvature values to red/green/blue scale such that red is negative, green is zero, and blue is positive" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EBakedCurvatureColorMode",
				"EBakedCurvatureColorMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EBakedCurvatureTypeMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EBakedCurvatureTypeMode, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EBakedCurvatureTypeMode"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EBakedCurvatureTypeMode>()
	{
		return EBakedCurvatureTypeMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EBakedCurvatureTypeMode(EBakedCurvatureTypeMode_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EBakedCurvatureTypeMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EBakedCurvatureTypeMode_Hash() { return 2855725909U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EBakedCurvatureTypeMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EBakedCurvatureTypeMode"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EBakedCurvatureTypeMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EBakedCurvatureTypeMode::MeanAverage", (int64)EBakedCurvatureTypeMode::MeanAverage },
				{ "EBakedCurvatureTypeMode::Max", (int64)EBakedCurvatureTypeMode::Max },
				{ "EBakedCurvatureTypeMode::Min", (int64)EBakedCurvatureTypeMode::Min },
				{ "EBakedCurvatureTypeMode::Gaussian", (int64)EBakedCurvatureTypeMode::Gaussian },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Gaussian.Comment", "/** Gaussian Curvature is the product of the Max and Min Principal curvatures */" },
				{ "Gaussian.Name", "EBakedCurvatureTypeMode::Gaussian" },
				{ "Gaussian.ToolTip", "Gaussian Curvature is the product of the Max and Min Principal curvatures" },
				{ "Max.Comment", "/** Max Principal Curvature */" },
				{ "Max.Name", "EBakedCurvatureTypeMode::Max" },
				{ "Max.ToolTip", "Max Principal Curvature" },
				{ "MeanAverage.Comment", "/** Mean Curvature is the average of the Max and Min Principal curvatures */" },
				{ "MeanAverage.Name", "EBakedCurvatureTypeMode::MeanAverage" },
				{ "MeanAverage.ToolTip", "Mean Curvature is the average of the Max and Min Principal curvatures" },
				{ "Min.Comment", "/** Min Principal Curvature */" },
				{ "Min.Name", "EBakedCurvatureTypeMode::Min" },
				{ "Min.ToolTip", "Min Principal Curvature" },
				{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EBakedCurvatureTypeMode",
				"EBakedCurvatureTypeMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EOcclusionMapPreview_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EOcclusionMapPreview, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EOcclusionMapPreview"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EOcclusionMapPreview>()
	{
		return EOcclusionMapPreview_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EOcclusionMapPreview(EOcclusionMapPreview_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EOcclusionMapPreview"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EOcclusionMapPreview_Hash() { return 2731717713U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EOcclusionMapPreview()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EOcclusionMapPreview"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EOcclusionMapPreview_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EOcclusionMapPreview::AmbientOcclusion", (int64)EOcclusionMapPreview::AmbientOcclusion },
				{ "EOcclusionMapPreview::BentNormal", (int64)EOcclusionMapPreview::BentNormal },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "AmbientOcclusion.Comment", "/** Ambient Occlusion */" },
				{ "AmbientOcclusion.DisplayName", "Ambient Occlusion" },
				{ "AmbientOcclusion.Name", "EOcclusionMapPreview::AmbientOcclusion" },
				{ "AmbientOcclusion.ToolTip", "Ambient Occlusion" },
				{ "BentNormal.Comment", "/** Bent Normal */" },
				{ "BentNormal.DisplayName", "Bent Normal" },
				{ "BentNormal.Name", "EOcclusionMapPreview::BentNormal" },
				{ "BentNormal.ToolTip", "Bent Normal" },
				{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EOcclusionMapPreview",
				"EOcclusionMapPreview",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EOcclusionMapDistribution_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EOcclusionMapDistribution, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EOcclusionMapDistribution"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EOcclusionMapDistribution>()
	{
		return EOcclusionMapDistribution_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EOcclusionMapDistribution(EOcclusionMapDistribution_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EOcclusionMapDistribution"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EOcclusionMapDistribution_Hash() { return 3648812038U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EOcclusionMapDistribution()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EOcclusionMapDistribution"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EOcclusionMapDistribution_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EOcclusionMapDistribution::Uniform", (int64)EOcclusionMapDistribution::Uniform },
				{ "EOcclusionMapDistribution::Cosine", (int64)EOcclusionMapDistribution::Cosine },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Cosine.Comment", "/** Cosine weighted occlusion rays */" },
				{ "Cosine.DisplayName", "Cosine" },
				{ "Cosine.Name", "EOcclusionMapDistribution::Cosine" },
				{ "Cosine.ToolTip", "Cosine weighted occlusion rays" },
				{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
				{ "Uniform.Comment", "/** Uniform occlusion rays */" },
				{ "Uniform.DisplayName", "Uniform" },
				{ "Uniform.Name", "EOcclusionMapDistribution::Uniform" },
				{ "Uniform.ToolTip", "Uniform occlusion rays" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EOcclusionMapDistribution",
				"EOcclusionMapDistribution",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ENormalMapSpace_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_ENormalMapSpace, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("ENormalMapSpace"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<ENormalMapSpace>()
	{
		return ENormalMapSpace_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ENormalMapSpace(ENormalMapSpace_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("ENormalMapSpace"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_ENormalMapSpace_Hash() { return 1820633532U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_ENormalMapSpace()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ENormalMapSpace"), 0, Get_Z_Construct_UEnum_MeshModelingTools_ENormalMapSpace_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ENormalMapSpace::Tangent", (int64)ENormalMapSpace::Tangent },
				{ "ENormalMapSpace::Object", (int64)ENormalMapSpace::Object },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
				{ "Object.Comment", "/** Object space */" },
				{ "Object.DisplayName", "Object space" },
				{ "Object.Name", "ENormalMapSpace::Object" },
				{ "Object.ToolTip", "Object space" },
				{ "Tangent.Comment", "/** Tangent space */" },
				{ "Tangent.DisplayName", "Tangent space" },
				{ "Tangent.Name", "ENormalMapSpace::Tangent" },
				{ "Tangent.ToolTip", "Tangent space" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"ENormalMapSpace",
				"ENormalMapSpace",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EBakeTextureResolution_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EBakeTextureResolution, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EBakeTextureResolution"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EBakeTextureResolution>()
	{
		return EBakeTextureResolution_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EBakeTextureResolution(EBakeTextureResolution_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EBakeTextureResolution"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EBakeTextureResolution_Hash() { return 3155392730U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EBakeTextureResolution()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EBakeTextureResolution"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EBakeTextureResolution_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EBakeTextureResolution::Resolution16", (int64)EBakeTextureResolution::Resolution16 },
				{ "EBakeTextureResolution::Resolution32", (int64)EBakeTextureResolution::Resolution32 },
				{ "EBakeTextureResolution::Resolution64", (int64)EBakeTextureResolution::Resolution64 },
				{ "EBakeTextureResolution::Resolution128", (int64)EBakeTextureResolution::Resolution128 },
				{ "EBakeTextureResolution::Resolution256", (int64)EBakeTextureResolution::Resolution256 },
				{ "EBakeTextureResolution::Resolution512", (int64)EBakeTextureResolution::Resolution512 },
				{ "EBakeTextureResolution::Resolution1024", (int64)EBakeTextureResolution::Resolution1024 },
				{ "EBakeTextureResolution::Resolution2048", (int64)EBakeTextureResolution::Resolution2048 },
				{ "EBakeTextureResolution::Resolution4096", (int64)EBakeTextureResolution::Resolution4096 },
				{ "EBakeTextureResolution::Resolution8192", (int64)EBakeTextureResolution::Resolution8192 },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
				{ "Resolution1024.DisplayName", "1024 x 1024" },
				{ "Resolution1024.Name", "EBakeTextureResolution::Resolution1024" },
				{ "Resolution128.DisplayName", "128 x 128" },
				{ "Resolution128.Name", "EBakeTextureResolution::Resolution128" },
				{ "Resolution16.DisplayName", "16 x 16" },
				{ "Resolution16.Name", "EBakeTextureResolution::Resolution16" },
				{ "Resolution2048.DisplayName", "2048 x 2048" },
				{ "Resolution2048.Name", "EBakeTextureResolution::Resolution2048" },
				{ "Resolution256.DisplayName", "256 x 256" },
				{ "Resolution256.Name", "EBakeTextureResolution::Resolution256" },
				{ "Resolution32.DisplayName", "32 x 32" },
				{ "Resolution32.Name", "EBakeTextureResolution::Resolution32" },
				{ "Resolution4096.DisplayName", "4096 x 4096" },
				{ "Resolution4096.Name", "EBakeTextureResolution::Resolution4096" },
				{ "Resolution512.DisplayName", "512 x 512" },
				{ "Resolution512.Name", "EBakeTextureResolution::Resolution512" },
				{ "Resolution64.DisplayName", "64 x 64" },
				{ "Resolution64.Name", "EBakeTextureResolution::Resolution64" },
				{ "Resolution8192.DisplayName", "8192 x 8192" },
				{ "Resolution8192.Name", "EBakeTextureResolution::Resolution8192" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EBakeTextureResolution",
				"EBakeTextureResolution",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EBakeMapType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EBakeMapType, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EBakeMapType"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EBakeMapType>()
	{
		return EBakeMapType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EBakeMapType(EBakeMapType_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EBakeMapType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EBakeMapType_Hash() { return 3157003361U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EBakeMapType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EBakeMapType"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EBakeMapType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EBakeMapType::TangentSpaceNormalMap", (int64)EBakeMapType::TangentSpaceNormalMap },
				{ "EBakeMapType::Occlusion", (int64)EBakeMapType::Occlusion },
				{ "EBakeMapType::Curvature", (int64)EBakeMapType::Curvature },
				{ "EBakeMapType::Texture2DImage", (int64)EBakeMapType::Texture2DImage },
				{ "EBakeMapType::NormalImage", (int64)EBakeMapType::NormalImage },
				{ "EBakeMapType::FaceNormalImage", (int64)EBakeMapType::FaceNormalImage },
				{ "EBakeMapType::PositionImage", (int64)EBakeMapType::PositionImage },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Curvature.Name", "EBakeMapType::Curvature" },
				{ "FaceNormalImage.Name", "EBakeMapType::FaceNormalImage" },
				{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
				{ "NormalImage.Name", "EBakeMapType::NormalImage" },
				{ "Occlusion.Name", "EBakeMapType::Occlusion" },
				{ "PositionImage.Name", "EBakeMapType::PositionImage" },
				{ "TangentSpaceNormalMap.Name", "EBakeMapType::TangentSpaceNormalMap" },
				{ "Texture2DImage.Name", "EBakeMapType::Texture2DImage" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EBakeMapType",
				"EBakeMapType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UBakeMeshAttributeMapsToolBuilder::StaticRegisterNativesUBakeMeshAttributeMapsToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UBakeMeshAttributeMapsToolBuilder_NoRegister()
	{
		return UBakeMeshAttributeMapsToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UBakeMeshAttributeMapsToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBakeMeshAttributeMapsToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakeMeshAttributeMapsToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "BakeMeshAttributeMapsTool.h" },
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBakeMeshAttributeMapsToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBakeMeshAttributeMapsToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBakeMeshAttributeMapsToolBuilder_Statics::ClassParams = {
		&UBakeMeshAttributeMapsToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UBakeMeshAttributeMapsToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBakeMeshAttributeMapsToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBakeMeshAttributeMapsToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBakeMeshAttributeMapsToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBakeMeshAttributeMapsToolBuilder, 103387309);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UBakeMeshAttributeMapsToolBuilder>()
	{
		return UBakeMeshAttributeMapsToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBakeMeshAttributeMapsToolBuilder(Z_Construct_UClass_UBakeMeshAttributeMapsToolBuilder, &UBakeMeshAttributeMapsToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UBakeMeshAttributeMapsToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBakeMeshAttributeMapsToolBuilder);
	DEFINE_FUNCTION(UBakeMeshAttributeMapsToolProperties::execGetUVLayerNamesFunc)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<FString>*)Z_Param__Result=P_THIS->GetUVLayerNamesFunc();
		P_NATIVE_END;
	}
	void UBakeMeshAttributeMapsToolProperties::StaticRegisterNativesUBakeMeshAttributeMapsToolProperties()
	{
		UClass* Class = UBakeMeshAttributeMapsToolProperties::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetUVLayerNamesFunc", &UBakeMeshAttributeMapsToolProperties::execGetUVLayerNamesFunc },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UBakeMeshAttributeMapsToolProperties_GetUVLayerNamesFunc_Statics
	{
		struct BakeMeshAttributeMapsToolProperties_eventGetUVLayerNamesFunc_Parms
		{
			TArray<FString> ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UBakeMeshAttributeMapsToolProperties_GetUVLayerNamesFunc_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UBakeMeshAttributeMapsToolProperties_GetUVLayerNamesFunc_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BakeMeshAttributeMapsToolProperties_eventGetUVLayerNamesFunc_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UBakeMeshAttributeMapsToolProperties_GetUVLayerNamesFunc_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UBakeMeshAttributeMapsToolProperties_GetUVLayerNamesFunc_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UBakeMeshAttributeMapsToolProperties_GetUVLayerNamesFunc_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UBakeMeshAttributeMapsToolProperties_GetUVLayerNamesFunc_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UBakeMeshAttributeMapsToolProperties_GetUVLayerNamesFunc_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties, nullptr, "GetUVLayerNamesFunc", nullptr, nullptr, sizeof(BakeMeshAttributeMapsToolProperties_eventGetUVLayerNamesFunc_Parms), Z_Construct_UFunction_UBakeMeshAttributeMapsToolProperties_GetUVLayerNamesFunc_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UBakeMeshAttributeMapsToolProperties_GetUVLayerNamesFunc_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UBakeMeshAttributeMapsToolProperties_GetUVLayerNamesFunc_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UBakeMeshAttributeMapsToolProperties_GetUVLayerNamesFunc_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UBakeMeshAttributeMapsToolProperties_GetUVLayerNamesFunc()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UBakeMeshAttributeMapsToolProperties_GetUVLayerNamesFunc_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_NoRegister()
	{
		return UBakeMeshAttributeMapsToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_MapType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MapType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_MapType;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Resolution_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Resolution_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Resolution;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseWorldSpace_MetaData[];
#endif
		static void NewProp_bUseWorldSpace_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseWorldSpace;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Thickness_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Thickness;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UVLayer_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_UVLayer;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_UVLayerNamesList_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UVLayerNamesList_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_UVLayerNamesList;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Result_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UBakeMeshAttributeMapsToolProperties_GetUVLayerNamesFunc, "GetUVLayerNamesFunc" }, // 1695886128
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "BakeMeshAttributeMapsTool.h" },
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_MapType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_MapType_MetaData[] = {
		{ "Category", "MapSettings" },
		{ "Comment", "/** The type of map to generate */" },
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
		{ "ToolTip", "The type of map to generate" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_MapType = { "MapType", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakeMeshAttributeMapsToolProperties, MapType), Z_Construct_UEnum_MeshModelingTools_EBakeMapType, METADATA_PARAMS(Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_MapType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_MapType_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_Resolution_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_Resolution_MetaData[] = {
		{ "Category", "MapSettings" },
		{ "Comment", "/** The pixel resolution of the generated map */" },
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
		{ "ToolTip", "The pixel resolution of the generated map" },
		{ "TransientToolProperty", "" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_Resolution = { "Resolution", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakeMeshAttributeMapsToolProperties, Resolution), Z_Construct_UEnum_MeshModelingTools_EBakeTextureResolution, METADATA_PARAMS(Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_Resolution_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_Resolution_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_bUseWorldSpace_MetaData[] = {
		{ "Category", "MapSettings" },
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
	};
#endif
	void Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_bUseWorldSpace_SetBit(void* Obj)
	{
		((UBakeMeshAttributeMapsToolProperties*)Obj)->bUseWorldSpace = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_bUseWorldSpace = { "bUseWorldSpace", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UBakeMeshAttributeMapsToolProperties), &Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_bUseWorldSpace_SetBit, METADATA_PARAMS(Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_bUseWorldSpace_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_bUseWorldSpace_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_Thickness_MetaData[] = {
		{ "Category", "MapSettings" },
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_Thickness = { "Thickness", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakeMeshAttributeMapsToolProperties, Thickness), METADATA_PARAMS(Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_Thickness_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_Thickness_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_UVLayer_MetaData[] = {
		{ "Category", "MapSettings" },
		{ "Comment", "/** Which UV layer to use to create the map */" },
		{ "GetOptions", "GetUVLayerNamesFunc" },
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
		{ "ToolTip", "Which UV layer to use to create the map" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_UVLayer = { "UVLayer", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakeMeshAttributeMapsToolProperties, UVLayer), METADATA_PARAMS(Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_UVLayer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_UVLayer_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_UVLayerNamesList_Inner = { "UVLayerNamesList", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_UVLayerNamesList_MetaData[] = {
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
		{ "TransientToolProperty", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_UVLayerNamesList = { "UVLayerNamesList", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakeMeshAttributeMapsToolProperties, UVLayerNamesList), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_UVLayerNamesList_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_UVLayerNamesList_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_Result_Inner = { "Result", nullptr, (EPropertyFlags)0x0000000000020000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_Result_MetaData[] = {
		{ "Category", "MapSettings" },
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
		{ "TransientToolProperty", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakeMeshAttributeMapsToolProperties, Result), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_MapType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_MapType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_Resolution_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_Resolution,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_bUseWorldSpace,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_Thickness,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_UVLayer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_UVLayerNamesList_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_UVLayerNamesList,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_Result_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::NewProp_Result,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBakeMeshAttributeMapsToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::ClassParams = {
		&UBakeMeshAttributeMapsToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBakeMeshAttributeMapsToolProperties, 3219323909);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UBakeMeshAttributeMapsToolProperties>()
	{
		return UBakeMeshAttributeMapsToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBakeMeshAttributeMapsToolProperties(Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties, &UBakeMeshAttributeMapsToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UBakeMeshAttributeMapsToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBakeMeshAttributeMapsToolProperties);
	void UBakedNormalMapToolProperties::StaticRegisterNativesUBakedNormalMapToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UBakedNormalMapToolProperties_NoRegister()
	{
		return UBakedNormalMapToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UBakedNormalMapToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBakedNormalMapToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakedNormalMapToolProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "BakeMeshAttributeMapsTool.h" },
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBakedNormalMapToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBakedNormalMapToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBakedNormalMapToolProperties_Statics::ClassParams = {
		&UBakedNormalMapToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UBakedNormalMapToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBakedNormalMapToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBakedNormalMapToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBakedNormalMapToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBakedNormalMapToolProperties, 465023314);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UBakedNormalMapToolProperties>()
	{
		return UBakedNormalMapToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBakedNormalMapToolProperties(Z_Construct_UClass_UBakedNormalMapToolProperties, &UBakedNormalMapToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UBakedNormalMapToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBakedNormalMapToolProperties);
	void UBakedOcclusionMapToolProperties::StaticRegisterNativesUBakedOcclusionMapToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UBakedOcclusionMapToolProperties_NoRegister()
	{
		return UBakedOcclusionMapToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Preview_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Preview_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Preview;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OcclusionRays_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_OcclusionRays;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxDistance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaxDistance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpreadAngle_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SpreadAngle;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Distribution_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Distribution_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Distribution;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bGaussianBlur_MetaData[];
#endif
		static void NewProp_bGaussianBlur_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bGaussianBlur;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BlurRadius_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BlurRadius;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BiasAngle_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BiasAngle;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_NormalSpace_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NormalSpace_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_NormalSpace;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "BakeMeshAttributeMapsTool.h" },
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_Preview_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_Preview_MetaData[] = {
		{ "Category", "OcclusionMap" },
		{ "Comment", "/** Occlusion map output to preview */" },
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
		{ "ToolTip", "Occlusion map output to preview" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_Preview = { "Preview", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakedOcclusionMapToolProperties, Preview), Z_Construct_UEnum_MeshModelingTools_EOcclusionMapPreview, METADATA_PARAMS(Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_Preview_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_Preview_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_OcclusionRays_MetaData[] = {
		{ "Category", "OcclusionMap" },
		{ "ClampMax", "50000" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Number of occlusion rays */" },
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
		{ "ToolTip", "Number of occlusion rays" },
		{ "UIMax", "1024" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_OcclusionRays = { "OcclusionRays", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakedOcclusionMapToolProperties, OcclusionRays), METADATA_PARAMS(Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_OcclusionRays_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_OcclusionRays_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_MaxDistance_MetaData[] = {
		{ "Category", "OcclusionMap" },
		{ "ClampMax", "99999999.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Maximum occlusion distance (0 = infinity) */" },
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
		{ "ToolTip", "Maximum occlusion distance (0 = infinity)" },
		{ "UIMax", "1000.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_MaxDistance = { "MaxDistance", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakedOcclusionMapToolProperties, MaxDistance), METADATA_PARAMS(Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_MaxDistance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_MaxDistance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_SpreadAngle_MetaData[] = {
		{ "Category", "OcclusionMap" },
		{ "ClampMax", "180.0" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Maximum spread angle of occlusion rays. */" },
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
		{ "ToolTip", "Maximum spread angle of occlusion rays." },
		{ "UIMax", "180.0" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_SpreadAngle = { "SpreadAngle", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakedOcclusionMapToolProperties, SpreadAngle), METADATA_PARAMS(Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_SpreadAngle_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_SpreadAngle_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_Distribution_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_Distribution_MetaData[] = {
		{ "Category", "OcclusionMap" },
		{ "Comment", "/** Angular distribution of occlusion rays in the spread angle. */" },
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
		{ "ToolTip", "Angular distribution of occlusion rays in the spread angle." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_Distribution = { "Distribution", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakedOcclusionMapToolProperties, Distribution), Z_Construct_UEnum_MeshModelingTools_EOcclusionMapDistribution, METADATA_PARAMS(Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_Distribution_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_Distribution_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_bGaussianBlur_MetaData[] = {
		{ "Category", "OcclusionMap|Ambient Occlusion" },
		{ "Comment", "/** Whether or not to apply Gaussian Blur to computed AO Map (recommended) */" },
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
		{ "ToolTip", "Whether or not to apply Gaussian Blur to computed AO Map (recommended)" },
	};
#endif
	void Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_bGaussianBlur_SetBit(void* Obj)
	{
		((UBakedOcclusionMapToolProperties*)Obj)->bGaussianBlur = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_bGaussianBlur = { "bGaussianBlur", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UBakedOcclusionMapToolProperties), &Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_bGaussianBlur_SetBit, METADATA_PARAMS(Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_bGaussianBlur_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_bGaussianBlur_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_BlurRadius_MetaData[] = {
		{ "Category", "OcclusionMap|Ambient Occlusion" },
		{ "ClampMax", "100.0" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Pixel Radius of Gaussian Blur Kernel */" },
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
		{ "ToolTip", "Pixel Radius of Gaussian Blur Kernel" },
		{ "UIMax", "10.0" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_BlurRadius = { "BlurRadius", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakedOcclusionMapToolProperties, BlurRadius), METADATA_PARAMS(Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_BlurRadius_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_BlurRadius_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_BiasAngle_MetaData[] = {
		{ "Category", "OcclusionMap|Ambient Occlusion" },
		{ "ClampMax", "89.9" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Contribution of AO rays that are within this angle (degrees) from horizontal are attenuated. This reduces faceting artifacts. */" },
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
		{ "ToolTip", "Contribution of AO rays that are within this angle (degrees) from horizontal are attenuated. This reduces faceting artifacts." },
		{ "UIMax", "45.0" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_BiasAngle = { "BiasAngle", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakedOcclusionMapToolProperties, BiasAngle), METADATA_PARAMS(Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_BiasAngle_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_BiasAngle_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_NormalSpace_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_NormalSpace_MetaData[] = {
		{ "Category", "OcclusionMap|Bent Normal" },
		{ "Comment", "/** Coordinate space of the bent normal map. */" },
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
		{ "ToolTip", "Coordinate space of the bent normal map." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_NormalSpace = { "NormalSpace", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakedOcclusionMapToolProperties, NormalSpace), Z_Construct_UEnum_MeshModelingTools_ENormalMapSpace, METADATA_PARAMS(Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_NormalSpace_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_NormalSpace_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_Preview_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_Preview,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_OcclusionRays,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_MaxDistance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_SpreadAngle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_Distribution_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_Distribution,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_bGaussianBlur,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_BlurRadius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_BiasAngle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_NormalSpace_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::NewProp_NormalSpace,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBakedOcclusionMapToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::ClassParams = {
		&UBakedOcclusionMapToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBakedOcclusionMapToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBakedOcclusionMapToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBakedOcclusionMapToolProperties, 1442657146);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UBakedOcclusionMapToolProperties>()
	{
		return UBakedOcclusionMapToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBakedOcclusionMapToolProperties(Z_Construct_UClass_UBakedOcclusionMapToolProperties, &UBakedOcclusionMapToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UBakedOcclusionMapToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBakedOcclusionMapToolProperties);
	void UBakedOcclusionMapVisualizationProperties::StaticRegisterNativesUBakedOcclusionMapVisualizationProperties()
	{
	}
	UClass* Z_Construct_UClass_UBakedOcclusionMapVisualizationProperties_NoRegister()
	{
		return UBakedOcclusionMapVisualizationProperties::StaticClass();
	}
	struct Z_Construct_UClass_UBakedOcclusionMapVisualizationProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseGrayLevel_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BaseGrayLevel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OcclusionMultiplier_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_OcclusionMultiplier;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBakedOcclusionMapVisualizationProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakedOcclusionMapVisualizationProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "BakeMeshAttributeMapsTool.h" },
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakedOcclusionMapVisualizationProperties_Statics::NewProp_BaseGrayLevel_MetaData[] = {
		{ "Category", "Visualization" },
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UBakedOcclusionMapVisualizationProperties_Statics::NewProp_BaseGrayLevel = { "BaseGrayLevel", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakedOcclusionMapVisualizationProperties, BaseGrayLevel), METADATA_PARAMS(Z_Construct_UClass_UBakedOcclusionMapVisualizationProperties_Statics::NewProp_BaseGrayLevel_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakedOcclusionMapVisualizationProperties_Statics::NewProp_BaseGrayLevel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakedOcclusionMapVisualizationProperties_Statics::NewProp_OcclusionMultiplier_MetaData[] = {
		{ "Category", "Visualization" },
		{ "Comment", "/** AO Multiplier in visualization (does not affect output) */" },
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
		{ "ToolTip", "AO Multiplier in visualization (does not affect output)" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UBakedOcclusionMapVisualizationProperties_Statics::NewProp_OcclusionMultiplier = { "OcclusionMultiplier", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakedOcclusionMapVisualizationProperties, OcclusionMultiplier), METADATA_PARAMS(Z_Construct_UClass_UBakedOcclusionMapVisualizationProperties_Statics::NewProp_OcclusionMultiplier_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakedOcclusionMapVisualizationProperties_Statics::NewProp_OcclusionMultiplier_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UBakedOcclusionMapVisualizationProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakedOcclusionMapVisualizationProperties_Statics::NewProp_BaseGrayLevel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakedOcclusionMapVisualizationProperties_Statics::NewProp_OcclusionMultiplier,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBakedOcclusionMapVisualizationProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBakedOcclusionMapVisualizationProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBakedOcclusionMapVisualizationProperties_Statics::ClassParams = {
		&UBakedOcclusionMapVisualizationProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UBakedOcclusionMapVisualizationProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UBakedOcclusionMapVisualizationProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UBakedOcclusionMapVisualizationProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBakedOcclusionMapVisualizationProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBakedOcclusionMapVisualizationProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBakedOcclusionMapVisualizationProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBakedOcclusionMapVisualizationProperties, 2802552095);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UBakedOcclusionMapVisualizationProperties>()
	{
		return UBakedOcclusionMapVisualizationProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBakedOcclusionMapVisualizationProperties(Z_Construct_UClass_UBakedOcclusionMapVisualizationProperties, &UBakedOcclusionMapVisualizationProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UBakedOcclusionMapVisualizationProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBakedOcclusionMapVisualizationProperties);
	void UBakedCurvatureMapToolProperties::StaticRegisterNativesUBakedCurvatureMapToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UBakedCurvatureMapToolProperties_NoRegister()
	{
		return UBakedCurvatureMapToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_CurvatureType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurvatureType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_CurvatureType;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_ColorMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ColorMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ColorMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RangeMultiplier_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RangeMultiplier;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MinRangeMultiplier_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MinRangeMultiplier;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Clamping_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Clamping_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Clamping;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bGaussianBlur_MetaData[];
#endif
		static void NewProp_bGaussianBlur_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bGaussianBlur;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BlurRadius_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BlurRadius;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "BakeMeshAttributeMapsTool.h" },
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_CurvatureType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_CurvatureType_MetaData[] = {
		{ "Category", "CurvatureMap" },
		{ "Comment", "/** Type of curvature to compute */" },
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
		{ "ToolTip", "Type of curvature to compute" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_CurvatureType = { "CurvatureType", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakedCurvatureMapToolProperties, CurvatureType), Z_Construct_UEnum_MeshModelingTools_EBakedCurvatureTypeMode, METADATA_PARAMS(Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_CurvatureType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_CurvatureType_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_ColorMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_ColorMode_MetaData[] = {
		{ "Category", "CurvatureMap" },
		{ "Comment", "/** Color mapping calculated from curvature values */" },
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
		{ "ToolTip", "Color mapping calculated from curvature values" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_ColorMode = { "ColorMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakedCurvatureMapToolProperties, ColorMode), Z_Construct_UEnum_MeshModelingTools_EBakedCurvatureColorMode, METADATA_PARAMS(Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_ColorMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_ColorMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_RangeMultiplier_MetaData[] = {
		{ "Category", "CurvatureMap" },
		{ "ClampMax", "100.0" },
		{ "ClampMin", "0.001" },
		{ "Comment", "/** Scale the maximum curvature value used to compute the mapping to grayscale/color */" },
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
		{ "ToolTip", "Scale the maximum curvature value used to compute the mapping to grayscale/color" },
		{ "UIMax", "2.0" },
		{ "UIMin", "0.1" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_RangeMultiplier = { "RangeMultiplier", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakedCurvatureMapToolProperties, RangeMultiplier), METADATA_PARAMS(Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_RangeMultiplier_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_RangeMultiplier_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_MinRangeMultiplier_MetaData[] = {
		{ "Category", "CurvatureMap" },
		{ "Comment", "/** Scale the minimum curvature value used to compute the mapping to grayscale/color (fraction of maximum) */" },
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
		{ "ToolTip", "Scale the minimum curvature value used to compute the mapping to grayscale/color (fraction of maximum)" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_MinRangeMultiplier = { "MinRangeMultiplier", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakedCurvatureMapToolProperties, MinRangeMultiplier), METADATA_PARAMS(Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_MinRangeMultiplier_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_MinRangeMultiplier_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_Clamping_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_Clamping_MetaData[] = {
		{ "Category", "CurvatureMap" },
		{ "Comment", "/** Clamping to apply to curvature values before scaling to color range */" },
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
		{ "ToolTip", "Clamping to apply to curvature values before scaling to color range" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_Clamping = { "Clamping", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakedCurvatureMapToolProperties, Clamping), Z_Construct_UEnum_MeshModelingTools_EBakedCurvatureClampMode, METADATA_PARAMS(Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_Clamping_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_Clamping_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_bGaussianBlur_MetaData[] = {
		{ "Category", "CurvatureMap" },
		{ "Comment", "/** Whether or not to apply Gaussian Blur to computed Map */" },
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
		{ "ToolTip", "Whether or not to apply Gaussian Blur to computed Map" },
	};
#endif
	void Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_bGaussianBlur_SetBit(void* Obj)
	{
		((UBakedCurvatureMapToolProperties*)Obj)->bGaussianBlur = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_bGaussianBlur = { "bGaussianBlur", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UBakedCurvatureMapToolProperties), &Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_bGaussianBlur_SetBit, METADATA_PARAMS(Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_bGaussianBlur_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_bGaussianBlur_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_BlurRadius_MetaData[] = {
		{ "Category", "CurvatureMap" },
		{ "ClampMax", "100.0" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Pixel Radius of Gaussian Blur Kernel */" },
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
		{ "ToolTip", "Pixel Radius of Gaussian Blur Kernel" },
		{ "UIMax", "10.0" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_BlurRadius = { "BlurRadius", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakedCurvatureMapToolProperties, BlurRadius), METADATA_PARAMS(Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_BlurRadius_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_BlurRadius_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_CurvatureType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_CurvatureType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_ColorMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_ColorMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_RangeMultiplier,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_MinRangeMultiplier,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_Clamping_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_Clamping,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_bGaussianBlur,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::NewProp_BlurRadius,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBakedCurvatureMapToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::ClassParams = {
		&UBakedCurvatureMapToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBakedCurvatureMapToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBakedCurvatureMapToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBakedCurvatureMapToolProperties, 2520546341);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UBakedCurvatureMapToolProperties>()
	{
		return UBakedCurvatureMapToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBakedCurvatureMapToolProperties(Z_Construct_UClass_UBakedCurvatureMapToolProperties, &UBakedCurvatureMapToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UBakedCurvatureMapToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBakedCurvatureMapToolProperties);
	void UBakedTexture2DImageProperties::StaticRegisterNativesUBakedTexture2DImageProperties()
	{
	}
	UClass* Z_Construct_UClass_UBakedTexture2DImageProperties_NoRegister()
	{
		return UBakedTexture2DImageProperties::StaticClass();
	}
	struct Z_Construct_UClass_UBakedTexture2DImageProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourceTexture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SourceTexture;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UVLayer_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_UVLayer;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBakedTexture2DImageProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakedTexture2DImageProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "BakeMeshAttributeMapsTool.h" },
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakedTexture2DImageProperties_Statics::NewProp_SourceTexture_MetaData[] = {
		{ "Category", "Texture2D" },
		{ "Comment", "/** The source texture that is to be resampled into a new texture map */" },
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
		{ "ToolTip", "The source texture that is to be resampled into a new texture map" },
		{ "TransientToolProperty", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UBakedTexture2DImageProperties_Statics::NewProp_SourceTexture = { "SourceTexture", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakedTexture2DImageProperties, SourceTexture), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UBakedTexture2DImageProperties_Statics::NewProp_SourceTexture_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakedTexture2DImageProperties_Statics::NewProp_SourceTexture_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakedTexture2DImageProperties_Statics::NewProp_UVLayer_MetaData[] = {
		{ "Category", "Texture2D" },
		{ "Comment", "/** The UV layer on the source mesh that corresponds to the SourceTexture */" },
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
		{ "ToolTip", "The UV layer on the source mesh that corresponds to the SourceTexture" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UBakedTexture2DImageProperties_Statics::NewProp_UVLayer = { "UVLayer", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakedTexture2DImageProperties, UVLayer), METADATA_PARAMS(Z_Construct_UClass_UBakedTexture2DImageProperties_Statics::NewProp_UVLayer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakedTexture2DImageProperties_Statics::NewProp_UVLayer_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UBakedTexture2DImageProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakedTexture2DImageProperties_Statics::NewProp_SourceTexture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakedTexture2DImageProperties_Statics::NewProp_UVLayer,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBakedTexture2DImageProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBakedTexture2DImageProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBakedTexture2DImageProperties_Statics::ClassParams = {
		&UBakedTexture2DImageProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UBakedTexture2DImageProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UBakedTexture2DImageProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UBakedTexture2DImageProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBakedTexture2DImageProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBakedTexture2DImageProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBakedTexture2DImageProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBakedTexture2DImageProperties, 3281678175);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UBakedTexture2DImageProperties>()
	{
		return UBakedTexture2DImageProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBakedTexture2DImageProperties(Z_Construct_UClass_UBakedTexture2DImageProperties, &UBakedTexture2DImageProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UBakedTexture2DImageProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBakedTexture2DImageProperties);
	void UBakeMeshAttributeMapsTool::StaticRegisterNativesUBakeMeshAttributeMapsTool()
	{
	}
	UClass* Z_Construct_UClass_UBakeMeshAttributeMapsTool_NoRegister()
	{
		return UBakeMeshAttributeMapsTool::StaticClass();
	}
	struct Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Settings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Settings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NormalMapProps_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_NormalMapProps;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OcclusionMapProps_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OcclusionMapProps;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurvatureMapProps_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CurvatureMapProps;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Texture2DProps_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Texture2DProps;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VisualizationProps_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_VisualizationProps;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviewMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviewMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BentNormalPreviewMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BentNormalPreviewMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedNormalMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CachedNormalMap;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedOcclusionMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CachedOcclusionMap;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedBentNormalMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CachedBentNormalMap;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedCurvatureMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CachedCurvatureMap;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedMeshPropertyMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CachedMeshPropertyMap;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedTexture2DImageMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CachedTexture2DImageMap;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EmptyNormalMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_EmptyNormalMap;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EmptyColorMapBlack_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_EmptyColorMapBlack;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EmptyColorMapWhite_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_EmptyColorMapWhite;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMultiSelectionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Detail Map Baking Tool\n */" },
		{ "IncludePath", "BakeMeshAttributeMapsTool.h" },
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
		{ "ToolTip", "Detail Map Baking Tool" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_Settings_MetaData[] = {
		{ "Comment", "// need to update bResultValid if these are modified, so we don't publicly expose them. \n// @todo setters/getters for these\n" },
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
		{ "ToolTip", "need to update bResultValid if these are modified, so we don't publicly expose them.\n@todo setters/getters for these" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_Settings = { "Settings", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakeMeshAttributeMapsTool, Settings), Z_Construct_UClass_UBakeMeshAttributeMapsToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_Settings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_Settings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_NormalMapProps_MetaData[] = {
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_NormalMapProps = { "NormalMapProps", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakeMeshAttributeMapsTool, NormalMapProps), Z_Construct_UClass_UBakedNormalMapToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_NormalMapProps_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_NormalMapProps_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_OcclusionMapProps_MetaData[] = {
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_OcclusionMapProps = { "OcclusionMapProps", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakeMeshAttributeMapsTool, OcclusionMapProps), Z_Construct_UClass_UBakedOcclusionMapToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_OcclusionMapProps_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_OcclusionMapProps_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_CurvatureMapProps_MetaData[] = {
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_CurvatureMapProps = { "CurvatureMapProps", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakeMeshAttributeMapsTool, CurvatureMapProps), Z_Construct_UClass_UBakedCurvatureMapToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_CurvatureMapProps_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_CurvatureMapProps_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_Texture2DProps_MetaData[] = {
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_Texture2DProps = { "Texture2DProps", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakeMeshAttributeMapsTool, Texture2DProps), Z_Construct_UClass_UBakedTexture2DImageProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_Texture2DProps_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_Texture2DProps_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_VisualizationProps_MetaData[] = {
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_VisualizationProps = { "VisualizationProps", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakeMeshAttributeMapsTool, VisualizationProps), Z_Construct_UClass_UBakedOcclusionMapVisualizationProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_VisualizationProps_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_VisualizationProps_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_PreviewMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_PreviewMaterial = { "PreviewMaterial", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakeMeshAttributeMapsTool, PreviewMaterial), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_PreviewMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_PreviewMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_BentNormalPreviewMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_BentNormalPreviewMaterial = { "BentNormalPreviewMaterial", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakeMeshAttributeMapsTool, BentNormalPreviewMaterial), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_BentNormalPreviewMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_BentNormalPreviewMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_CachedNormalMap_MetaData[] = {
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_CachedNormalMap = { "CachedNormalMap", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakeMeshAttributeMapsTool, CachedNormalMap), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_CachedNormalMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_CachedNormalMap_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_CachedOcclusionMap_MetaData[] = {
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_CachedOcclusionMap = { "CachedOcclusionMap", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakeMeshAttributeMapsTool, CachedOcclusionMap), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_CachedOcclusionMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_CachedOcclusionMap_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_CachedBentNormalMap_MetaData[] = {
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_CachedBentNormalMap = { "CachedBentNormalMap", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakeMeshAttributeMapsTool, CachedBentNormalMap), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_CachedBentNormalMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_CachedBentNormalMap_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_CachedCurvatureMap_MetaData[] = {
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_CachedCurvatureMap = { "CachedCurvatureMap", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakeMeshAttributeMapsTool, CachedCurvatureMap), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_CachedCurvatureMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_CachedCurvatureMap_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_CachedMeshPropertyMap_MetaData[] = {
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_CachedMeshPropertyMap = { "CachedMeshPropertyMap", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakeMeshAttributeMapsTool, CachedMeshPropertyMap), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_CachedMeshPropertyMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_CachedMeshPropertyMap_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_CachedTexture2DImageMap_MetaData[] = {
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_CachedTexture2DImageMap = { "CachedTexture2DImageMap", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakeMeshAttributeMapsTool, CachedTexture2DImageMap), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_CachedTexture2DImageMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_CachedTexture2DImageMap_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_EmptyNormalMap_MetaData[] = {
		{ "Comment", "// empty maps are shown when nothing is computed\n" },
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
		{ "ToolTip", "empty maps are shown when nothing is computed" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_EmptyNormalMap = { "EmptyNormalMap", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakeMeshAttributeMapsTool, EmptyNormalMap), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_EmptyNormalMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_EmptyNormalMap_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_EmptyColorMapBlack_MetaData[] = {
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_EmptyColorMapBlack = { "EmptyColorMapBlack", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakeMeshAttributeMapsTool, EmptyColorMapBlack), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_EmptyColorMapBlack_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_EmptyColorMapBlack_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_EmptyColorMapWhite_MetaData[] = {
		{ "ModuleRelativePath", "Public/BakeMeshAttributeMapsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_EmptyColorMapWhite = { "EmptyColorMapWhite", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakeMeshAttributeMapsTool, EmptyColorMapWhite), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_EmptyColorMapWhite_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_EmptyColorMapWhite_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_Settings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_NormalMapProps,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_OcclusionMapProps,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_CurvatureMapProps,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_Texture2DProps,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_VisualizationProps,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_PreviewMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_BentNormalPreviewMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_CachedNormalMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_CachedOcclusionMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_CachedBentNormalMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_CachedCurvatureMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_CachedMeshPropertyMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_CachedTexture2DImageMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_EmptyNormalMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_EmptyColorMapBlack,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::NewProp_EmptyColorMapWhite,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBakeMeshAttributeMapsTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::ClassParams = {
		&UBakeMeshAttributeMapsTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBakeMeshAttributeMapsTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBakeMeshAttributeMapsTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBakeMeshAttributeMapsTool, 4140093034);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UBakeMeshAttributeMapsTool>()
	{
		return UBakeMeshAttributeMapsTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBakeMeshAttributeMapsTool(Z_Construct_UClass_UBakeMeshAttributeMapsTool, &UBakeMeshAttributeMapsTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UBakeMeshAttributeMapsTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBakeMeshAttributeMapsTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
