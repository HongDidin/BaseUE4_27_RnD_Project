// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MODELINGCOMPONENTS_PreviewMesh_generated_h
#error "PreviewMesh.generated.h already included, missing '#pragma once' in PreviewMesh.h"
#endif
#define MODELINGCOMPONENTS_PreviewMesh_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_28_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_28_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_28_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_28_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPreviewMeshActor(); \
	friend struct Z_Construct_UClass_APreviewMeshActor_Statics; \
public: \
	DECLARE_CLASS(APreviewMeshActor, AInternalToolFrameworkActor, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(APreviewMeshActor)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_28_INCLASS \
private: \
	static void StaticRegisterNativesAPreviewMeshActor(); \
	friend struct Z_Construct_UClass_APreviewMeshActor_Statics; \
public: \
	DECLARE_CLASS(APreviewMeshActor, AInternalToolFrameworkActor, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(APreviewMeshActor)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_28_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APreviewMeshActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APreviewMeshActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APreviewMeshActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APreviewMeshActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APreviewMeshActor(APreviewMeshActor&&); \
	NO_API APreviewMeshActor(const APreviewMeshActor&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_28_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APreviewMeshActor(APreviewMeshActor&&); \
	NO_API APreviewMeshActor(const APreviewMeshActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APreviewMeshActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APreviewMeshActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APreviewMeshActor)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_28_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_25_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_28_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_28_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_28_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_28_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_28_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_28_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_28_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_28_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_28_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_28_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_28_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_28_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MODELINGCOMPONENTS_API UClass* StaticClass<class APreviewMeshActor>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_59_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_59_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_59_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_59_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPreviewMesh(); \
	friend struct Z_Construct_UClass_UPreviewMesh_Statics; \
public: \
	DECLARE_CLASS(UPreviewMesh, UObject, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UPreviewMesh) \
	virtual UObject* _getUObject() const override { return const_cast<UPreviewMesh*>(this); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_59_INCLASS \
private: \
	static void StaticRegisterNativesUPreviewMesh(); \
	friend struct Z_Construct_UClass_UPreviewMesh_Statics; \
public: \
	DECLARE_CLASS(UPreviewMesh, UObject, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UPreviewMesh) \
	virtual UObject* _getUObject() const override { return const_cast<UPreviewMesh*>(this); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_59_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPreviewMesh(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPreviewMesh) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPreviewMesh); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPreviewMesh); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPreviewMesh(UPreviewMesh&&); \
	NO_API UPreviewMesh(const UPreviewMesh&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_59_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPreviewMesh(UPreviewMesh&&); \
	NO_API UPreviewMesh(const UPreviewMesh&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPreviewMesh); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPreviewMesh); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UPreviewMesh)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_59_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__DynamicMeshComponent() { return STRUCT_OFFSET(UPreviewMesh, DynamicMeshComponent); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_56_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_59_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_59_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_59_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_59_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_59_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_59_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_59_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_59_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_59_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_59_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_59_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h_59_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MODELINGCOMPONENTS_API UClass* StaticClass<class UPreviewMesh>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PreviewMesh_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
