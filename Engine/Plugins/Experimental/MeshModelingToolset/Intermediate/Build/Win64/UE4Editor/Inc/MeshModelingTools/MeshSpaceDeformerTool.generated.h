// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLS_MeshSpaceDeformerTool_generated_h
#error "MeshSpaceDeformerTool.generated.h already included, missing '#pragma once' in MeshSpaceDeformerTool.h"
#endif
#define MESHMODELINGTOOLS_MeshSpaceDeformerTool_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_33_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_33_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_33_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_33_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshSpaceDeformerToolBuilder(); \
	friend struct Z_Construct_UClass_UMeshSpaceDeformerToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UMeshSpaceDeformerToolBuilder, UMeshSurfacePointToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMeshSpaceDeformerToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_33_INCLASS \
private: \
	static void StaticRegisterNativesUMeshSpaceDeformerToolBuilder(); \
	friend struct Z_Construct_UClass_UMeshSpaceDeformerToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UMeshSpaceDeformerToolBuilder, UMeshSurfacePointToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMeshSpaceDeformerToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_33_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshSpaceDeformerToolBuilder(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshSpaceDeformerToolBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshSpaceDeformerToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshSpaceDeformerToolBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshSpaceDeformerToolBuilder(UMeshSpaceDeformerToolBuilder&&); \
	NO_API UMeshSpaceDeformerToolBuilder(const UMeshSpaceDeformerToolBuilder&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_33_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshSpaceDeformerToolBuilder(UMeshSpaceDeformerToolBuilder&&); \
	NO_API UMeshSpaceDeformerToolBuilder(const UMeshSpaceDeformerToolBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshSpaceDeformerToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshSpaceDeformerToolBuilder); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMeshSpaceDeformerToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_33_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_30_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_33_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_33_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_33_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_33_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_33_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_33_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_33_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_33_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_33_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_33_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_33_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_33_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UMeshSpaceDeformerToolBuilder>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_62_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_62_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_62_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_62_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSpaceDeformerOperatorFactory(); \
	friend struct Z_Construct_UClass_USpaceDeformerOperatorFactory_Statics; \
public: \
	DECLARE_CLASS(USpaceDeformerOperatorFactory, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(USpaceDeformerOperatorFactory)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_62_INCLASS \
private: \
	static void StaticRegisterNativesUSpaceDeformerOperatorFactory(); \
	friend struct Z_Construct_UClass_USpaceDeformerOperatorFactory_Statics; \
public: \
	DECLARE_CLASS(USpaceDeformerOperatorFactory, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(USpaceDeformerOperatorFactory)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_62_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USpaceDeformerOperatorFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USpaceDeformerOperatorFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USpaceDeformerOperatorFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USpaceDeformerOperatorFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USpaceDeformerOperatorFactory(USpaceDeformerOperatorFactory&&); \
	NO_API USpaceDeformerOperatorFactory(const USpaceDeformerOperatorFactory&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_62_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USpaceDeformerOperatorFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USpaceDeformerOperatorFactory(USpaceDeformerOperatorFactory&&); \
	NO_API USpaceDeformerOperatorFactory(const USpaceDeformerOperatorFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USpaceDeformerOperatorFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USpaceDeformerOperatorFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USpaceDeformerOperatorFactory)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_62_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_59_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_62_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_62_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_62_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_62_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_62_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_62_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_62_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_62_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_62_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_62_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_62_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_62_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class USpaceDeformerOperatorFactory>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_79_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_79_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_79_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_79_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshSpaceDeformerTool(); \
	friend struct Z_Construct_UClass_UMeshSpaceDeformerTool_Statics; \
public: \
	DECLARE_CLASS(UMeshSpaceDeformerTool, UMeshSurfacePointTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMeshSpaceDeformerTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_79_INCLASS \
private: \
	static void StaticRegisterNativesUMeshSpaceDeformerTool(); \
	friend struct Z_Construct_UClass_UMeshSpaceDeformerTool_Statics; \
public: \
	DECLARE_CLASS(UMeshSpaceDeformerTool, UMeshSurfacePointTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMeshSpaceDeformerTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_79_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshSpaceDeformerTool(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshSpaceDeformerTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshSpaceDeformerTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshSpaceDeformerTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshSpaceDeformerTool(UMeshSpaceDeformerTool&&); \
	NO_API UMeshSpaceDeformerTool(const UMeshSpaceDeformerTool&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_79_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshSpaceDeformerTool(UMeshSpaceDeformerTool&&); \
	NO_API UMeshSpaceDeformerTool(const UMeshSpaceDeformerTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshSpaceDeformerTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshSpaceDeformerTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMeshSpaceDeformerTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_79_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__StateTarget() { return STRUCT_OFFSET(UMeshSpaceDeformerTool, StateTarget); } \
	FORCEINLINE static uint32 __PPO__Preview() { return STRUCT_OFFSET(UMeshSpaceDeformerTool, Preview); } \
	FORCEINLINE static uint32 __PPO__GizmoCenter() { return STRUCT_OFFSET(UMeshSpaceDeformerTool, GizmoCenter); } \
	FORCEINLINE static uint32 __PPO__GizmoOrientation() { return STRUCT_OFFSET(UMeshSpaceDeformerTool, GizmoOrientation); } \
	FORCEINLINE static uint32 __PPO__IntervalGizmo() { return STRUCT_OFFSET(UMeshSpaceDeformerTool, IntervalGizmo); } \
	FORCEINLINE static uint32 __PPO__TransformGizmo() { return STRUCT_OFFSET(UMeshSpaceDeformerTool, TransformGizmo); } \
	FORCEINLINE static uint32 __PPO__TransformProxy() { return STRUCT_OFFSET(UMeshSpaceDeformerTool, TransformProxy); } \
	FORCEINLINE static uint32 __PPO__UpIntervalSource() { return STRUCT_OFFSET(UMeshSpaceDeformerTool, UpIntervalSource); } \
	FORCEINLINE static uint32 __PPO__DownIntervalSource() { return STRUCT_OFFSET(UMeshSpaceDeformerTool, DownIntervalSource); } \
	FORCEINLINE static uint32 __PPO__ForwardIntervalSource() { return STRUCT_OFFSET(UMeshSpaceDeformerTool, ForwardIntervalSource); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_76_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_79_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_79_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_79_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_79_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_79_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_79_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_79_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_79_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_79_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_79_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_79_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h_79_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UMeshSpaceDeformerTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSpaceDeformerTool_h


#define FOREACH_ENUM_ENONLINEAROPERATIONTYPE(op) \
	op(ENonlinearOperationType::Bend) \
	op(ENonlinearOperationType::Flare) \
	op(ENonlinearOperationType::Twist) 

enum class ENonlinearOperationType : int8;
template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<ENonlinearOperationType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
