// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingToolsEditorOnly/Public/MeshTangentsTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMeshTangentsTool() {}
// Cross Module References
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UMeshTangentsToolBuilder_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UMeshTangentsToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	UPackage* Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UMeshTangentsToolProperties_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UMeshTangentsToolProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MODELINGOPERATORSEDITORONLY_API UEnum* Z_Construct_UEnum_ModelingOperatorsEditorOnly_EMeshTangentsType();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UMeshTangentsTool_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UMeshTangentsTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_USingleSelectionTool();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPreviewMesh_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPreviewGeometry_NoRegister();
// End Cross Module References
	void UMeshTangentsToolBuilder::StaticRegisterNativesUMeshTangentsToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UMeshTangentsToolBuilder_NoRegister()
	{
		return UMeshTangentsToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UMeshTangentsToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshTangentsToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshTangentsToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "MeshTangentsTool.h" },
		{ "ModuleRelativePath", "Public/MeshTangentsTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshTangentsToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshTangentsToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshTangentsToolBuilder_Statics::ClassParams = {
		&UMeshTangentsToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshTangentsToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshTangentsToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshTangentsToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshTangentsToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshTangentsToolBuilder, 3886466492);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UMeshTangentsToolBuilder>()
	{
		return UMeshTangentsToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshTangentsToolBuilder(Z_Construct_UClass_UMeshTangentsToolBuilder, &UMeshTangentsToolBuilder::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UMeshTangentsToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshTangentsToolBuilder);
	void UMeshTangentsToolProperties::StaticRegisterNativesUMeshTangentsToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UMeshTangentsToolProperties_NoRegister()
	{
		return UMeshTangentsToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UMeshTangentsToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TangentType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TangentType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TangentType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowTangents_MetaData[];
#endif
		static void NewProp_bShowTangents_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowTangents;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowNormals_MetaData[];
#endif
		static void NewProp_bShowNormals_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowNormals;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bHideDegenerates_MetaData[];
#endif
		static void NewProp_bHideDegenerates_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bHideDegenerates;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LineLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_LineLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LineThickness_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_LineThickness;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCompareWithMikkt_MetaData[];
#endif
		static void NewProp_bCompareWithMikkt_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCompareWithMikkt;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AngleThreshDeg_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AngleThreshDeg;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshTangentsToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshTangentsToolProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshTangentsTool.h" },
		{ "ModuleRelativePath", "Public/MeshTangentsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_TangentType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_TangentType_MetaData[] = {
		{ "Category", "Options" },
		{ "ModuleRelativePath", "Public/MeshTangentsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_TangentType = { "TangentType", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshTangentsToolProperties, TangentType), Z_Construct_UEnum_ModelingOperatorsEditorOnly_EMeshTangentsType, METADATA_PARAMS(Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_TangentType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_TangentType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_bShowTangents_MetaData[] = {
		{ "Category", "Visualization" },
		{ "ModuleRelativePath", "Public/MeshTangentsTool.h" },
	};
#endif
	void Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_bShowTangents_SetBit(void* Obj)
	{
		((UMeshTangentsToolProperties*)Obj)->bShowTangents = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_bShowTangents = { "bShowTangents", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshTangentsToolProperties), &Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_bShowTangents_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_bShowTangents_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_bShowTangents_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_bShowNormals_MetaData[] = {
		{ "Category", "Visualization" },
		{ "ModuleRelativePath", "Public/MeshTangentsTool.h" },
	};
#endif
	void Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_bShowNormals_SetBit(void* Obj)
	{
		((UMeshTangentsToolProperties*)Obj)->bShowNormals = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_bShowNormals = { "bShowNormals", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshTangentsToolProperties), &Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_bShowNormals_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_bShowNormals_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_bShowNormals_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_bHideDegenerates_MetaData[] = {
		{ "Category", "Visualization" },
		{ "ModuleRelativePath", "Public/MeshTangentsTool.h" },
	};
#endif
	void Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_bHideDegenerates_SetBit(void* Obj)
	{
		((UMeshTangentsToolProperties*)Obj)->bHideDegenerates = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_bHideDegenerates = { "bHideDegenerates", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshTangentsToolProperties), &Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_bHideDegenerates_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_bHideDegenerates_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_bHideDegenerates_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_LineLength_MetaData[] = {
		{ "Category", "Visualization" },
		{ "ClampMax", "10000000.0" },
		{ "ClampMin", "0.01" },
		{ "ModuleRelativePath", "Public/MeshTangentsTool.h" },
		{ "UIMax", "25.0" },
		{ "UIMin", "0.01" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_LineLength = { "LineLength", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshTangentsToolProperties, LineLength), METADATA_PARAMS(Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_LineLength_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_LineLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_LineThickness_MetaData[] = {
		{ "Category", "Visualization" },
		{ "ClampMax", "1000.0" },
		{ "ClampMin", "0" },
		{ "ModuleRelativePath", "Public/MeshTangentsTool.h" },
		{ "UIMax", "25.0" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_LineThickness = { "LineThickness", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshTangentsToolProperties, LineThickness), METADATA_PARAMS(Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_LineThickness_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_LineThickness_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_bCompareWithMikkt_MetaData[] = {
		{ "Category", "Visualization" },
		{ "ModuleRelativePath", "Public/MeshTangentsTool.h" },
	};
#endif
	void Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_bCompareWithMikkt_SetBit(void* Obj)
	{
		((UMeshTangentsToolProperties*)Obj)->bCompareWithMikkt = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_bCompareWithMikkt = { "bCompareWithMikkt", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshTangentsToolProperties), &Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_bCompareWithMikkt_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_bCompareWithMikkt_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_bCompareWithMikkt_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_AngleThreshDeg_MetaData[] = {
		{ "Category", "Visualization" },
		{ "ModuleRelativePath", "Public/MeshTangentsTool.h" },
		{ "UIMax", "90.0" },
		{ "UIMin", "0.5" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_AngleThreshDeg = { "AngleThreshDeg", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshTangentsToolProperties, AngleThreshDeg), METADATA_PARAMS(Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_AngleThreshDeg_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_AngleThreshDeg_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshTangentsToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_TangentType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_TangentType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_bShowTangents,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_bShowNormals,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_bHideDegenerates,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_LineLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_LineThickness,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_bCompareWithMikkt,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshTangentsToolProperties_Statics::NewProp_AngleThreshDeg,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshTangentsToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshTangentsToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshTangentsToolProperties_Statics::ClassParams = {
		&UMeshTangentsToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshTangentsToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshTangentsToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshTangentsToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshTangentsToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshTangentsToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshTangentsToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshTangentsToolProperties, 1811959815);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UMeshTangentsToolProperties>()
	{
		return UMeshTangentsToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshTangentsToolProperties(Z_Construct_UClass_UMeshTangentsToolProperties, &UMeshTangentsToolProperties::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UMeshTangentsToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshTangentsToolProperties);
	void UMeshTangentsTool::StaticRegisterNativesUMeshTangentsTool()
	{
	}
	UClass* Z_Construct_UClass_UMeshTangentsTool_NoRegister()
	{
		return UMeshTangentsTool::StaticClass();
	}
	struct Z_Construct_UClass_UMeshTangentsTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Settings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Settings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DefaultMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviewMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviewMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviewGeometry_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviewGeometry;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshTangentsTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USingleSelectionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshTangentsTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Simple Mesh Simplifying Tool\n */" },
		{ "IncludePath", "MeshTangentsTool.h" },
		{ "ModuleRelativePath", "Public/MeshTangentsTool.h" },
		{ "ToolTip", "Simple Mesh Simplifying Tool" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshTangentsTool_Statics::NewProp_Settings_MetaData[] = {
		{ "ModuleRelativePath", "Public/MeshTangentsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshTangentsTool_Statics::NewProp_Settings = { "Settings", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshTangentsTool, Settings), Z_Construct_UClass_UMeshTangentsToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshTangentsTool_Statics::NewProp_Settings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshTangentsTool_Statics::NewProp_Settings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshTangentsTool_Statics::NewProp_DefaultMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Public/MeshTangentsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshTangentsTool_Statics::NewProp_DefaultMaterial = { "DefaultMaterial", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshTangentsTool, DefaultMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshTangentsTool_Statics::NewProp_DefaultMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshTangentsTool_Statics::NewProp_DefaultMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshTangentsTool_Statics::NewProp_PreviewMesh_MetaData[] = {
		{ "ModuleRelativePath", "Public/MeshTangentsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshTangentsTool_Statics::NewProp_PreviewMesh = { "PreviewMesh", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshTangentsTool, PreviewMesh), Z_Construct_UClass_UPreviewMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshTangentsTool_Statics::NewProp_PreviewMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshTangentsTool_Statics::NewProp_PreviewMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshTangentsTool_Statics::NewProp_PreviewGeometry_MetaData[] = {
		{ "ModuleRelativePath", "Public/MeshTangentsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshTangentsTool_Statics::NewProp_PreviewGeometry = { "PreviewGeometry", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshTangentsTool, PreviewGeometry), Z_Construct_UClass_UPreviewGeometry_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshTangentsTool_Statics::NewProp_PreviewGeometry_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshTangentsTool_Statics::NewProp_PreviewGeometry_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshTangentsTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshTangentsTool_Statics::NewProp_Settings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshTangentsTool_Statics::NewProp_DefaultMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshTangentsTool_Statics::NewProp_PreviewMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshTangentsTool_Statics::NewProp_PreviewGeometry,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshTangentsTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshTangentsTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshTangentsTool_Statics::ClassParams = {
		&UMeshTangentsTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshTangentsTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshTangentsTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshTangentsTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshTangentsTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshTangentsTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshTangentsTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshTangentsTool, 1648034481);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UMeshTangentsTool>()
	{
		return UMeshTangentsTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshTangentsTool(Z_Construct_UClass_UMeshTangentsTool, &UMeshTangentsTool::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UMeshTangentsTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshTangentsTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
