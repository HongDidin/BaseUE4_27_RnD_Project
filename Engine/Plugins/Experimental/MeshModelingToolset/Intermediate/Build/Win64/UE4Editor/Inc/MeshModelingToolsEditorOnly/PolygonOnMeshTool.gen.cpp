// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingToolsEditorOnly/Public/PolygonOnMeshTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePolygonOnMeshTool() {}
// Cross Module References
	MESHMODELINGTOOLSEDITORONLY_API UEnum* Z_Construct_UEnum_MeshModelingToolsEditorOnly_EPolygonOnMeshToolActions();
	UPackage* Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly();
	MESHMODELINGTOOLSEDITORONLY_API UEnum* Z_Construct_UEnum_MeshModelingToolsEditorOnly_EPolygonType();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UPolygonOnMeshToolBuilder_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UPolygonOnMeshToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UPolygonOnMeshToolProperties_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UPolygonOnMeshToolProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MODELINGOPERATORSEDITORONLY_API UEnum* Z_Construct_UEnum_ModelingOperatorsEditorOnly_EEmbeddedPolygonOpMethod();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UPolygonOnMeshToolActionPropertySet_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UPolygonOnMeshToolActionPropertySet();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UPolygonOnMeshTool_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UPolygonOnMeshTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_USingleSelectionTool();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_ULineSetComponent_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UConstructionPlaneMechanic_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UCollectSurfacePathMechanic_NoRegister();
// End Cross Module References
	static UEnum* EPolygonOnMeshToolActions_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingToolsEditorOnly_EPolygonOnMeshToolActions, Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly(), TEXT("EPolygonOnMeshToolActions"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLSEDITORONLY_API UEnum* StaticEnum<EPolygonOnMeshToolActions>()
	{
		return EPolygonOnMeshToolActions_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EPolygonOnMeshToolActions(EPolygonOnMeshToolActions_StaticEnum, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("EPolygonOnMeshToolActions"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingToolsEditorOnly_EPolygonOnMeshToolActions_Hash() { return 605737520U; }
	UEnum* Z_Construct_UEnum_MeshModelingToolsEditorOnly_EPolygonOnMeshToolActions()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EPolygonOnMeshToolActions"), 0, Get_Z_Construct_UEnum_MeshModelingToolsEditorOnly_EPolygonOnMeshToolActions_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EPolygonOnMeshToolActions::NoAction", (int64)EPolygonOnMeshToolActions::NoAction },
				{ "EPolygonOnMeshToolActions::DrawPolygon", (int64)EPolygonOnMeshToolActions::DrawPolygon },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "DrawPolygon.Name", "EPolygonOnMeshToolActions::DrawPolygon" },
				{ "ModuleRelativePath", "Public/PolygonOnMeshTool.h" },
				{ "NoAction.Name", "EPolygonOnMeshToolActions::NoAction" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
				nullptr,
				"EPolygonOnMeshToolActions",
				"EPolygonOnMeshToolActions",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EPolygonType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingToolsEditorOnly_EPolygonType, Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly(), TEXT("EPolygonType"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLSEDITORONLY_API UEnum* StaticEnum<EPolygonType>()
	{
		return EPolygonType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EPolygonType(EPolygonType_StaticEnum, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("EPolygonType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingToolsEditorOnly_EPolygonType_Hash() { return 56964198U; }
	UEnum* Z_Construct_UEnum_MeshModelingToolsEditorOnly_EPolygonType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EPolygonType"), 0, Get_Z_Construct_UEnum_MeshModelingToolsEditorOnly_EPolygonType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EPolygonType::Circle", (int64)EPolygonType::Circle },
				{ "EPolygonType::Square", (int64)EPolygonType::Square },
				{ "EPolygonType::Rectangle", (int64)EPolygonType::Rectangle },
				{ "EPolygonType::RoundRect", (int64)EPolygonType::RoundRect },
				{ "EPolygonType::Custom", (int64)EPolygonType::Custom },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Circle.Name", "EPolygonType::Circle" },
				{ "Custom.Name", "EPolygonType::Custom" },
				{ "ModuleRelativePath", "Public/PolygonOnMeshTool.h" },
				{ "Rectangle.Name", "EPolygonType::Rectangle" },
				{ "RoundRect.Name", "EPolygonType::RoundRect" },
				{ "Square.Name", "EPolygonType::Square" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
				nullptr,
				"EPolygonType",
				"EPolygonType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UPolygonOnMeshToolBuilder::StaticRegisterNativesUPolygonOnMeshToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UPolygonOnMeshToolBuilder_NoRegister()
	{
		return UPolygonOnMeshToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UPolygonOnMeshToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPolygonOnMeshToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolygonOnMeshToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "PolygonOnMeshTool.h" },
		{ "ModuleRelativePath", "Public/PolygonOnMeshTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPolygonOnMeshToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPolygonOnMeshToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPolygonOnMeshToolBuilder_Statics::ClassParams = {
		&UPolygonOnMeshToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UPolygonOnMeshToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPolygonOnMeshToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPolygonOnMeshToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPolygonOnMeshToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPolygonOnMeshToolBuilder, 2132302863);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UPolygonOnMeshToolBuilder>()
	{
		return UPolygonOnMeshToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPolygonOnMeshToolBuilder(Z_Construct_UClass_UPolygonOnMeshToolBuilder, &UPolygonOnMeshToolBuilder::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UPolygonOnMeshToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPolygonOnMeshToolBuilder);
	void UPolygonOnMeshToolProperties::StaticRegisterNativesUPolygonOnMeshToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UPolygonOnMeshToolProperties_NoRegister()
	{
		return UPolygonOnMeshToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Operation_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Operation_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Operation;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Shape_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Shape_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Shape;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PolygonScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PolygonScale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Width_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Width;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Height_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Height;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CornerRatio_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CornerRatio;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Subdivisions_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Subdivisions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Standard properties of the polygon-on-mesh operations\n */" },
		{ "IncludePath", "PolygonOnMeshTool.h" },
		{ "ModuleRelativePath", "Public/PolygonOnMeshTool.h" },
		{ "ToolTip", "Standard properties of the polygon-on-mesh operations" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::NewProp_Operation_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::NewProp_Operation_MetaData[] = {
		{ "Category", "Operation" },
		{ "Comment", "/** What operation to apply using the Polygon */" },
		{ "ModuleRelativePath", "Public/PolygonOnMeshTool.h" },
		{ "ToolTip", "What operation to apply using the Polygon" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::NewProp_Operation = { "Operation", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPolygonOnMeshToolProperties, Operation), Z_Construct_UEnum_ModelingOperatorsEditorOnly_EEmbeddedPolygonOpMethod, METADATA_PARAMS(Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::NewProp_Operation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::NewProp_Operation_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::NewProp_Shape_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::NewProp_Shape_MetaData[] = {
		{ "Category", "Shape" },
		{ "Comment", "/** Polygon Shape to use in this Operation */" },
		{ "ModuleRelativePath", "Public/PolygonOnMeshTool.h" },
		{ "ToolTip", "Polygon Shape to use in this Operation" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::NewProp_Shape = { "Shape", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPolygonOnMeshToolProperties, Shape), Z_Construct_UEnum_MeshModelingToolsEditorOnly_EPolygonType, METADATA_PARAMS(Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::NewProp_Shape_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::NewProp_Shape_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::NewProp_PolygonScale_MetaData[] = {
		{ "Category", "Shape" },
		{ "ClampMax", "10000" },
		{ "ClampMin", "0.00001" },
		{ "Comment", "/** Scale of polygon to embed */" },
		{ "ModuleRelativePath", "Public/PolygonOnMeshTool.h" },
		{ "ToolTip", "Scale of polygon to embed" },
		{ "UIMax", "10.0" },
		{ "UIMin", "0.01" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::NewProp_PolygonScale = { "PolygonScale", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPolygonOnMeshToolProperties, PolygonScale), METADATA_PARAMS(Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::NewProp_PolygonScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::NewProp_PolygonScale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::NewProp_Width_MetaData[] = {
		{ "Category", "Shape" },
		{ "ClampMax", "10000" },
		{ "ClampMin", "0.00001" },
		{ "Comment", "/** Width of Polygon */" },
		{ "ModuleRelativePath", "Public/PolygonOnMeshTool.h" },
		{ "ToolTip", "Width of Polygon" },
		{ "UIMax", "1000.0" },
		{ "UIMin", "0.001" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::NewProp_Width = { "Width", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPolygonOnMeshToolProperties, Width), METADATA_PARAMS(Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::NewProp_Width_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::NewProp_Width_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::NewProp_Height_MetaData[] = {
		{ "Category", "Shape" },
		{ "ClampMax", "10000" },
		{ "ClampMin", "0.00001" },
		{ "Comment", "/** Height of Polygon */" },
		{ "EditCondition", "Shape == EPolygonType::Rectangle || Shape == EPolygonType::RoundRect" },
		{ "ModuleRelativePath", "Public/PolygonOnMeshTool.h" },
		{ "ToolTip", "Height of Polygon" },
		{ "UIMax", "1000.0" },
		{ "UIMin", "0.001" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::NewProp_Height = { "Height", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPolygonOnMeshToolProperties, Height), METADATA_PARAMS(Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::NewProp_Height_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::NewProp_Height_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::NewProp_CornerRatio_MetaData[] = {
		{ "Category", "Shape" },
		{ "Comment", "/** Corner Ratio of RoundRect Polygon */" },
		{ "EditCondition", "Shape == EPolygonType::RoundRect" },
		{ "ModuleRelativePath", "Public/PolygonOnMeshTool.h" },
		{ "ToolTip", "Corner Ratio of RoundRect Polygon" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::NewProp_CornerRatio = { "CornerRatio", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPolygonOnMeshToolProperties, CornerRatio), METADATA_PARAMS(Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::NewProp_CornerRatio_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::NewProp_CornerRatio_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::NewProp_Subdivisions_MetaData[] = {
		{ "Category", "Shape" },
		{ "ClampMax", "10000" },
		{ "ClampMin", "3" },
		{ "Comment", "/** Number of sides in Circle or RoundRect Corner */" },
		{ "EditCondition", "Shape == EPolygonType::Circle || Shape == EPolygonType::RoundRect" },
		{ "ModuleRelativePath", "Public/PolygonOnMeshTool.h" },
		{ "ToolTip", "Number of sides in Circle or RoundRect Corner" },
		{ "UIMax", "20" },
		{ "UIMin", "3" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::NewProp_Subdivisions = { "Subdivisions", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPolygonOnMeshToolProperties, Subdivisions), METADATA_PARAMS(Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::NewProp_Subdivisions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::NewProp_Subdivisions_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::NewProp_Operation_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::NewProp_Operation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::NewProp_Shape_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::NewProp_Shape,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::NewProp_PolygonScale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::NewProp_Width,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::NewProp_Height,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::NewProp_CornerRatio,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::NewProp_Subdivisions,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPolygonOnMeshToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::ClassParams = {
		&UPolygonOnMeshToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPolygonOnMeshToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPolygonOnMeshToolProperties, 3836351421);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UPolygonOnMeshToolProperties>()
	{
		return UPolygonOnMeshToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPolygonOnMeshToolProperties(Z_Construct_UClass_UPolygonOnMeshToolProperties, &UPolygonOnMeshToolProperties::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UPolygonOnMeshToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPolygonOnMeshToolProperties);
	DEFINE_FUNCTION(UPolygonOnMeshToolActionPropertySet::execDrawPolygon)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->DrawPolygon();
		P_NATIVE_END;
	}
	void UPolygonOnMeshToolActionPropertySet::StaticRegisterNativesUPolygonOnMeshToolActionPropertySet()
	{
		UClass* Class = UPolygonOnMeshToolActionPropertySet::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "DrawPolygon", &UPolygonOnMeshToolActionPropertySet::execDrawPolygon },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UPolygonOnMeshToolActionPropertySet_DrawPolygon_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPolygonOnMeshToolActionPropertySet_DrawPolygon_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "Actions" },
		{ "Comment", "/** Extrude the current set of selected faces. Click in viewport to confirm extrude height. */" },
		{ "DisplayName", "Draw Polygon" },
		{ "DisplayPriority", "1" },
		{ "ModuleRelativePath", "Public/PolygonOnMeshTool.h" },
		{ "ToolTip", "Extrude the current set of selected faces. Click in viewport to confirm extrude height." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPolygonOnMeshToolActionPropertySet_DrawPolygon_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPolygonOnMeshToolActionPropertySet, nullptr, "DrawPolygon", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPolygonOnMeshToolActionPropertySet_DrawPolygon_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPolygonOnMeshToolActionPropertySet_DrawPolygon_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPolygonOnMeshToolActionPropertySet_DrawPolygon()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPolygonOnMeshToolActionPropertySet_DrawPolygon_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UPolygonOnMeshToolActionPropertySet_NoRegister()
	{
		return UPolygonOnMeshToolActionPropertySet::StaticClass();
	}
	struct Z_Construct_UClass_UPolygonOnMeshToolActionPropertySet_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPolygonOnMeshToolActionPropertySet_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UPolygonOnMeshToolActionPropertySet_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UPolygonOnMeshToolActionPropertySet_DrawPolygon, "DrawPolygon" }, // 3250136363
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolygonOnMeshToolActionPropertySet_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "PolygonOnMeshTool.h" },
		{ "ModuleRelativePath", "Public/PolygonOnMeshTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPolygonOnMeshToolActionPropertySet_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPolygonOnMeshToolActionPropertySet>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPolygonOnMeshToolActionPropertySet_Statics::ClassParams = {
		&UPolygonOnMeshToolActionPropertySet::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UPolygonOnMeshToolActionPropertySet_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPolygonOnMeshToolActionPropertySet_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPolygonOnMeshToolActionPropertySet()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPolygonOnMeshToolActionPropertySet_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPolygonOnMeshToolActionPropertySet, 2775530250);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UPolygonOnMeshToolActionPropertySet>()
	{
		return UPolygonOnMeshToolActionPropertySet::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPolygonOnMeshToolActionPropertySet(Z_Construct_UClass_UPolygonOnMeshToolActionPropertySet, &UPolygonOnMeshToolActionPropertySet::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UPolygonOnMeshToolActionPropertySet"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPolygonOnMeshToolActionPropertySet);
	void UPolygonOnMeshTool::StaticRegisterNativesUPolygonOnMeshTool()
	{
	}
	UClass* Z_Construct_UClass_UPolygonOnMeshTool_NoRegister()
	{
		return UPolygonOnMeshTool::StaticClass();
	}
	struct Z_Construct_UClass_UPolygonOnMeshTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BasicProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BasicProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActionProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ActionProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Preview_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Preview;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DrawnLineSet_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DrawnLineSet;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlaneMechanic_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PlaneMechanic;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DrawPolygonMechanic_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DrawPolygonMechanic;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPolygonOnMeshTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USingleSelectionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolygonOnMeshTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Simple Mesh Plane Cutting Tool\n */" },
		{ "IncludePath", "PolygonOnMeshTool.h" },
		{ "ModuleRelativePath", "Public/PolygonOnMeshTool.h" },
		{ "ToolTip", "Simple Mesh Plane Cutting Tool" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolygonOnMeshTool_Statics::NewProp_BasicProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/PolygonOnMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPolygonOnMeshTool_Statics::NewProp_BasicProperties = { "BasicProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPolygonOnMeshTool, BasicProperties), Z_Construct_UClass_UPolygonOnMeshToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPolygonOnMeshTool_Statics::NewProp_BasicProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolygonOnMeshTool_Statics::NewProp_BasicProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolygonOnMeshTool_Statics::NewProp_ActionProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/PolygonOnMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPolygonOnMeshTool_Statics::NewProp_ActionProperties = { "ActionProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPolygonOnMeshTool, ActionProperties), Z_Construct_UClass_UPolygonOnMeshToolActionPropertySet_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPolygonOnMeshTool_Statics::NewProp_ActionProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolygonOnMeshTool_Statics::NewProp_ActionProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolygonOnMeshTool_Statics::NewProp_Preview_MetaData[] = {
		{ "ModuleRelativePath", "Public/PolygonOnMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPolygonOnMeshTool_Statics::NewProp_Preview = { "Preview", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPolygonOnMeshTool, Preview), Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPolygonOnMeshTool_Statics::NewProp_Preview_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolygonOnMeshTool_Statics::NewProp_Preview_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolygonOnMeshTool_Statics::NewProp_DrawnLineSet_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/PolygonOnMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPolygonOnMeshTool_Statics::NewProp_DrawnLineSet = { "DrawnLineSet", nullptr, (EPropertyFlags)0x0020080000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPolygonOnMeshTool, DrawnLineSet), Z_Construct_UClass_ULineSetComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPolygonOnMeshTool_Statics::NewProp_DrawnLineSet_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolygonOnMeshTool_Statics::NewProp_DrawnLineSet_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolygonOnMeshTool_Statics::NewProp_PlaneMechanic_MetaData[] = {
		{ "ModuleRelativePath", "Public/PolygonOnMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPolygonOnMeshTool_Statics::NewProp_PlaneMechanic = { "PlaneMechanic", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPolygonOnMeshTool, PlaneMechanic), Z_Construct_UClass_UConstructionPlaneMechanic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPolygonOnMeshTool_Statics::NewProp_PlaneMechanic_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolygonOnMeshTool_Statics::NewProp_PlaneMechanic_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolygonOnMeshTool_Statics::NewProp_DrawPolygonMechanic_MetaData[] = {
		{ "ModuleRelativePath", "Public/PolygonOnMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPolygonOnMeshTool_Statics::NewProp_DrawPolygonMechanic = { "DrawPolygonMechanic", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPolygonOnMeshTool, DrawPolygonMechanic), Z_Construct_UClass_UCollectSurfacePathMechanic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPolygonOnMeshTool_Statics::NewProp_DrawPolygonMechanic_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolygonOnMeshTool_Statics::NewProp_DrawPolygonMechanic_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPolygonOnMeshTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolygonOnMeshTool_Statics::NewProp_BasicProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolygonOnMeshTool_Statics::NewProp_ActionProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolygonOnMeshTool_Statics::NewProp_Preview,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolygonOnMeshTool_Statics::NewProp_DrawnLineSet,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolygonOnMeshTool_Statics::NewProp_PlaneMechanic,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolygonOnMeshTool_Statics::NewProp_DrawPolygonMechanic,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPolygonOnMeshTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPolygonOnMeshTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPolygonOnMeshTool_Statics::ClassParams = {
		&UPolygonOnMeshTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UPolygonOnMeshTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UPolygonOnMeshTool_Statics::PropPointers),
		0,
		0x009000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UPolygonOnMeshTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPolygonOnMeshTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPolygonOnMeshTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPolygonOnMeshTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPolygonOnMeshTool, 3216684097);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UPolygonOnMeshTool>()
	{
		return UPolygonOnMeshTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPolygonOnMeshTool(Z_Construct_UClass_UPolygonOnMeshTool, &UPolygonOnMeshTool::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UPolygonOnMeshTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPolygonOnMeshTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
