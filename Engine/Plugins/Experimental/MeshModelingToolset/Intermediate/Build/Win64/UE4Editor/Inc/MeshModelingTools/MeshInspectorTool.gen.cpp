// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/MeshInspectorTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMeshInspectorTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshInspectorToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshInspectorToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshInspectorProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshInspectorProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshInspectorTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshInspectorTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_USingleSelectionTool();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UExistingMeshMaterialProperties_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPreviewMesh_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_ULineSetComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
// End Cross Module References
	void UMeshInspectorToolBuilder::StaticRegisterNativesUMeshInspectorToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UMeshInspectorToolBuilder_NoRegister()
	{
		return UMeshInspectorToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UMeshInspectorToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshInspectorToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshInspectorToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "MeshInspectorTool.h" },
		{ "ModuleRelativePath", "Public/MeshInspectorTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshInspectorToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshInspectorToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshInspectorToolBuilder_Statics::ClassParams = {
		&UMeshInspectorToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshInspectorToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshInspectorToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshInspectorToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshInspectorToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshInspectorToolBuilder, 425697971);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UMeshInspectorToolBuilder>()
	{
		return UMeshInspectorToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshInspectorToolBuilder(Z_Construct_UClass_UMeshInspectorToolBuilder, &UMeshInspectorToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UMeshInspectorToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshInspectorToolBuilder);
	void UMeshInspectorProperties::StaticRegisterNativesUMeshInspectorProperties()
	{
	}
	UClass* Z_Construct_UClass_UMeshInspectorProperties_NoRegister()
	{
		return UMeshInspectorProperties::StaticClass();
	}
	struct Z_Construct_UClass_UMeshInspectorProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bWireframe_MetaData[];
#endif
		static void NewProp_bWireframe_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bWireframe;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bBoundaryEdges_MetaData[];
#endif
		static void NewProp_bBoundaryEdges_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bBoundaryEdges;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bBowtieVertices_MetaData[];
#endif
		static void NewProp_bBowtieVertices_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bBowtieVertices;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPolygonBorders_MetaData[];
#endif
		static void NewProp_bPolygonBorders_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPolygonBorders;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUVSeams_MetaData[];
#endif
		static void NewProp_bUVSeams_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUVSeams;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUVBowties_MetaData[];
#endif
		static void NewProp_bUVBowties_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUVBowties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bNormalSeams_MetaData[];
#endif
		static void NewProp_bNormalSeams_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bNormalSeams;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bNormalVectors_MetaData[];
#endif
		static void NewProp_bNormalVectors_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bNormalVectors;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bTangentVectors_MetaData[];
#endif
		static void NewProp_bTangentVectors_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bTangentVectors;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NormalLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_NormalLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TangentLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TangentLength;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshInspectorProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshInspectorProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshInspectorTool.h" },
		{ "ModuleRelativePath", "Public/MeshInspectorTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bWireframe_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Toggle visibility of all mesh edges */" },
		{ "ModuleRelativePath", "Public/MeshInspectorTool.h" },
		{ "ToolTip", "Toggle visibility of all mesh edges" },
	};
#endif
	void Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bWireframe_SetBit(void* Obj)
	{
		((UMeshInspectorProperties*)Obj)->bWireframe = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bWireframe = { "bWireframe", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshInspectorProperties), &Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bWireframe_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bWireframe_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bWireframe_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bBoundaryEdges_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Toggle visibility of open boundary edges */" },
		{ "ModuleRelativePath", "Public/MeshInspectorTool.h" },
		{ "ToolTip", "Toggle visibility of open boundary edges" },
	};
#endif
	void Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bBoundaryEdges_SetBit(void* Obj)
	{
		((UMeshInspectorProperties*)Obj)->bBoundaryEdges = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bBoundaryEdges = { "bBoundaryEdges", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshInspectorProperties), &Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bBoundaryEdges_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bBoundaryEdges_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bBoundaryEdges_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bBowtieVertices_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Toggle visibility of bowtie vertices */" },
		{ "ModuleRelativePath", "Public/MeshInspectorTool.h" },
		{ "ToolTip", "Toggle visibility of bowtie vertices" },
	};
#endif
	void Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bBowtieVertices_SetBit(void* Obj)
	{
		((UMeshInspectorProperties*)Obj)->bBowtieVertices = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bBowtieVertices = { "bBowtieVertices", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshInspectorProperties), &Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bBowtieVertices_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bBowtieVertices_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bBowtieVertices_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bPolygonBorders_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Toggle visibility of polygon borders */" },
		{ "ModuleRelativePath", "Public/MeshInspectorTool.h" },
		{ "ToolTip", "Toggle visibility of polygon borders" },
	};
#endif
	void Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bPolygonBorders_SetBit(void* Obj)
	{
		((UMeshInspectorProperties*)Obj)->bPolygonBorders = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bPolygonBorders = { "bPolygonBorders", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshInspectorProperties), &Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bPolygonBorders_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bPolygonBorders_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bPolygonBorders_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bUVSeams_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Toggle visibility of UV seam edges */" },
		{ "ModuleRelativePath", "Public/MeshInspectorTool.h" },
		{ "ToolTip", "Toggle visibility of UV seam edges" },
	};
#endif
	void Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bUVSeams_SetBit(void* Obj)
	{
		((UMeshInspectorProperties*)Obj)->bUVSeams = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bUVSeams = { "bUVSeams", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshInspectorProperties), &Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bUVSeams_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bUVSeams_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bUVSeams_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bUVBowties_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Toggle visibility of UV bowtie vertices */" },
		{ "ModuleRelativePath", "Public/MeshInspectorTool.h" },
		{ "ToolTip", "Toggle visibility of UV bowtie vertices" },
	};
#endif
	void Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bUVBowties_SetBit(void* Obj)
	{
		((UMeshInspectorProperties*)Obj)->bUVBowties = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bUVBowties = { "bUVBowties", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshInspectorProperties), &Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bUVBowties_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bUVBowties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bUVBowties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bNormalSeams_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Toggle visibility of Normal seam edges */" },
		{ "ModuleRelativePath", "Public/MeshInspectorTool.h" },
		{ "ToolTip", "Toggle visibility of Normal seam edges" },
	};
#endif
	void Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bNormalSeams_SetBit(void* Obj)
	{
		((UMeshInspectorProperties*)Obj)->bNormalSeams = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bNormalSeams = { "bNormalSeams", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshInspectorProperties), &Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bNormalSeams_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bNormalSeams_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bNormalSeams_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bNormalVectors_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Toggle visibility of normal vectors */" },
		{ "ModuleRelativePath", "Public/MeshInspectorTool.h" },
		{ "ToolTip", "Toggle visibility of normal vectors" },
	};
#endif
	void Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bNormalVectors_SetBit(void* Obj)
	{
		((UMeshInspectorProperties*)Obj)->bNormalVectors = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bNormalVectors = { "bNormalVectors", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshInspectorProperties), &Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bNormalVectors_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bNormalVectors_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bNormalVectors_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bTangentVectors_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Toggle visibility of tangent vectors */" },
		{ "ModuleRelativePath", "Public/MeshInspectorTool.h" },
		{ "ToolTip", "Toggle visibility of tangent vectors" },
	};
#endif
	void Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bTangentVectors_SetBit(void* Obj)
	{
		((UMeshInspectorProperties*)Obj)->bTangentVectors = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bTangentVectors = { "bTangentVectors", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshInspectorProperties), &Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bTangentVectors_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bTangentVectors_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bTangentVectors_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_NormalLength_MetaData[] = {
		{ "Category", "Options" },
		{ "ClampMax", "1000000000.0" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Length of line segments representing normal vectors */" },
		{ "EditCondition", "bNormalVectors" },
		{ "ModuleRelativePath", "Public/MeshInspectorTool.h" },
		{ "ToolTip", "Length of line segments representing normal vectors" },
		{ "UIMax", "400" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_NormalLength = { "NormalLength", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshInspectorProperties, NormalLength), METADATA_PARAMS(Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_NormalLength_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_NormalLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_TangentLength_MetaData[] = {
		{ "Category", "Options" },
		{ "ClampMax", "1000000000.0" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Length of line segments representing tangent vectors */" },
		{ "EditCondition", "bTangentVectors" },
		{ "ModuleRelativePath", "Public/MeshInspectorTool.h" },
		{ "ToolTip", "Length of line segments representing tangent vectors" },
		{ "UIMax", "400" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_TangentLength = { "TangentLength", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshInspectorProperties, TangentLength), METADATA_PARAMS(Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_TangentLength_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_TangentLength_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshInspectorProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bWireframe,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bBoundaryEdges,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bBowtieVertices,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bPolygonBorders,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bUVSeams,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bUVBowties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bNormalSeams,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bNormalVectors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_bTangentVectors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_NormalLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshInspectorProperties_Statics::NewProp_TangentLength,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshInspectorProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshInspectorProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshInspectorProperties_Statics::ClassParams = {
		&UMeshInspectorProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshInspectorProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshInspectorProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshInspectorProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshInspectorProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshInspectorProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshInspectorProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshInspectorProperties, 3958843793);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UMeshInspectorProperties>()
	{
		return UMeshInspectorProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshInspectorProperties(Z_Construct_UClass_UMeshInspectorProperties, &UMeshInspectorProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UMeshInspectorProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshInspectorProperties);
	void UMeshInspectorTool::StaticRegisterNativesUMeshInspectorTool()
	{
	}
	UClass* Z_Construct_UClass_UMeshInspectorTool_NoRegister()
	{
		return UMeshInspectorTool::StaticClass();
	}
	struct Z_Construct_UClass_UMeshInspectorTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Settings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Settings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaterialSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MaterialSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviewMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviewMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DrawnLineSet_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DrawnLineSet;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DefaultMaterial;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshInspectorTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USingleSelectionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshInspectorTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Mesh Inspector Tool for visualizing mesh information\n */" },
		{ "IncludePath", "MeshInspectorTool.h" },
		{ "ModuleRelativePath", "Public/MeshInspectorTool.h" },
		{ "ToolTip", "Mesh Inspector Tool for visualizing mesh information" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshInspectorTool_Statics::NewProp_Settings_MetaData[] = {
		{ "ModuleRelativePath", "Public/MeshInspectorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshInspectorTool_Statics::NewProp_Settings = { "Settings", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshInspectorTool, Settings), Z_Construct_UClass_UMeshInspectorProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshInspectorTool_Statics::NewProp_Settings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshInspectorTool_Statics::NewProp_Settings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshInspectorTool_Statics::NewProp_MaterialSettings_MetaData[] = {
		{ "ModuleRelativePath", "Public/MeshInspectorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshInspectorTool_Statics::NewProp_MaterialSettings = { "MaterialSettings", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshInspectorTool, MaterialSettings), Z_Construct_UClass_UExistingMeshMaterialProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshInspectorTool_Statics::NewProp_MaterialSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshInspectorTool_Statics::NewProp_MaterialSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshInspectorTool_Statics::NewProp_PreviewMesh_MetaData[] = {
		{ "ModuleRelativePath", "Public/MeshInspectorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshInspectorTool_Statics::NewProp_PreviewMesh = { "PreviewMesh", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshInspectorTool, PreviewMesh), Z_Construct_UClass_UPreviewMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshInspectorTool_Statics::NewProp_PreviewMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshInspectorTool_Statics::NewProp_PreviewMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshInspectorTool_Statics::NewProp_DrawnLineSet_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/MeshInspectorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshInspectorTool_Statics::NewProp_DrawnLineSet = { "DrawnLineSet", nullptr, (EPropertyFlags)0x0020080000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshInspectorTool, DrawnLineSet), Z_Construct_UClass_ULineSetComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshInspectorTool_Statics::NewProp_DrawnLineSet_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshInspectorTool_Statics::NewProp_DrawnLineSet_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshInspectorTool_Statics::NewProp_DefaultMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Public/MeshInspectorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshInspectorTool_Statics::NewProp_DefaultMaterial = { "DefaultMaterial", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshInspectorTool, DefaultMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshInspectorTool_Statics::NewProp_DefaultMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshInspectorTool_Statics::NewProp_DefaultMaterial_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshInspectorTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshInspectorTool_Statics::NewProp_Settings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshInspectorTool_Statics::NewProp_MaterialSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshInspectorTool_Statics::NewProp_PreviewMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshInspectorTool_Statics::NewProp_DrawnLineSet,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshInspectorTool_Statics::NewProp_DefaultMaterial,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshInspectorTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshInspectorTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshInspectorTool_Statics::ClassParams = {
		&UMeshInspectorTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshInspectorTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshInspectorTool_Statics::PropPointers),
		0,
		0x009000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshInspectorTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshInspectorTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshInspectorTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshInspectorTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshInspectorTool, 2805195566);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UMeshInspectorTool>()
	{
		return UMeshInspectorTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshInspectorTool(Z_Construct_UClass_UMeshInspectorTool, &UMeshInspectorTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UMeshInspectorTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshInspectorTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
