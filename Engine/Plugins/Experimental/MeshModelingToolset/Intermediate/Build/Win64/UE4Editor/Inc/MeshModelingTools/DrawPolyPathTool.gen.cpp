// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/DrawPolyPathTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDrawPolyPathTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EDrawPolyPathExtrudeDirection();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EDrawPolyPathHeightMode();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EDrawPolyPathWidthMode();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EDrawPolyPathOutputMode();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDrawPolyPathToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDrawPolyPathToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UMeshSurfacePointToolBuilder();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDrawPolyPathProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDrawPolyPathProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDrawPolyPathExtrudeProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDrawPolyPathExtrudeProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDrawPolyPathTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDrawPolyPathTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveTool();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UNewMeshMaterialProperties_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UConstructionPlaneMechanic_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPolyEditPreviewMesh_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPlaneDistanceFromHitMechanic_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_USpatialCurveDistanceMechanic_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UCollectSurfacePathMechanic_NoRegister();
// End Cross Module References
	static UEnum* EDrawPolyPathExtrudeDirection_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EDrawPolyPathExtrudeDirection, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EDrawPolyPathExtrudeDirection"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EDrawPolyPathExtrudeDirection>()
	{
		return EDrawPolyPathExtrudeDirection_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDrawPolyPathExtrudeDirection(EDrawPolyPathExtrudeDirection_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EDrawPolyPathExtrudeDirection"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EDrawPolyPathExtrudeDirection_Hash() { return 2706608977U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EDrawPolyPathExtrudeDirection()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDrawPolyPathExtrudeDirection"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EDrawPolyPathExtrudeDirection_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDrawPolyPathExtrudeDirection::SelectionNormal", (int64)EDrawPolyPathExtrudeDirection::SelectionNormal },
				{ "EDrawPolyPathExtrudeDirection::WorldX", (int64)EDrawPolyPathExtrudeDirection::WorldX },
				{ "EDrawPolyPathExtrudeDirection::WorldY", (int64)EDrawPolyPathExtrudeDirection::WorldY },
				{ "EDrawPolyPathExtrudeDirection::WorldZ", (int64)EDrawPolyPathExtrudeDirection::WorldZ },
				{ "EDrawPolyPathExtrudeDirection::LocalX", (int64)EDrawPolyPathExtrudeDirection::LocalX },
				{ "EDrawPolyPathExtrudeDirection::LocalY", (int64)EDrawPolyPathExtrudeDirection::LocalY },
				{ "EDrawPolyPathExtrudeDirection::LocalZ", (int64)EDrawPolyPathExtrudeDirection::LocalZ },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "LocalX.Name", "EDrawPolyPathExtrudeDirection::LocalX" },
				{ "LocalY.Name", "EDrawPolyPathExtrudeDirection::LocalY" },
				{ "LocalZ.Name", "EDrawPolyPathExtrudeDirection::LocalZ" },
				{ "ModuleRelativePath", "Public/DrawPolyPathTool.h" },
				{ "SelectionNormal.Name", "EDrawPolyPathExtrudeDirection::SelectionNormal" },
				{ "WorldX.Name", "EDrawPolyPathExtrudeDirection::WorldX" },
				{ "WorldY.Name", "EDrawPolyPathExtrudeDirection::WorldY" },
				{ "WorldZ.Name", "EDrawPolyPathExtrudeDirection::WorldZ" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EDrawPolyPathExtrudeDirection",
				"EDrawPolyPathExtrudeDirection",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EDrawPolyPathHeightMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EDrawPolyPathHeightMode, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EDrawPolyPathHeightMode"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EDrawPolyPathHeightMode>()
	{
		return EDrawPolyPathHeightMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDrawPolyPathHeightMode(EDrawPolyPathHeightMode_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EDrawPolyPathHeightMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EDrawPolyPathHeightMode_Hash() { return 3708369593U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EDrawPolyPathHeightMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDrawPolyPathHeightMode"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EDrawPolyPathHeightMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDrawPolyPathHeightMode::Interactive", (int64)EDrawPolyPathHeightMode::Interactive },
				{ "EDrawPolyPathHeightMode::Constant", (int64)EDrawPolyPathHeightMode::Constant },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Constant.Name", "EDrawPolyPathHeightMode::Constant" },
				{ "Interactive.Name", "EDrawPolyPathHeightMode::Interactive" },
				{ "ModuleRelativePath", "Public/DrawPolyPathTool.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EDrawPolyPathHeightMode",
				"EDrawPolyPathHeightMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EDrawPolyPathWidthMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EDrawPolyPathWidthMode, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EDrawPolyPathWidthMode"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EDrawPolyPathWidthMode>()
	{
		return EDrawPolyPathWidthMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDrawPolyPathWidthMode(EDrawPolyPathWidthMode_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EDrawPolyPathWidthMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EDrawPolyPathWidthMode_Hash() { return 1497962198U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EDrawPolyPathWidthMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDrawPolyPathWidthMode"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EDrawPolyPathWidthMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDrawPolyPathWidthMode::Interactive", (int64)EDrawPolyPathWidthMode::Interactive },
				{ "EDrawPolyPathWidthMode::Constant", (int64)EDrawPolyPathWidthMode::Constant },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Constant.Name", "EDrawPolyPathWidthMode::Constant" },
				{ "Interactive.Name", "EDrawPolyPathWidthMode::Interactive" },
				{ "ModuleRelativePath", "Public/DrawPolyPathTool.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EDrawPolyPathWidthMode",
				"EDrawPolyPathWidthMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EDrawPolyPathOutputMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EDrawPolyPathOutputMode, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EDrawPolyPathOutputMode"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EDrawPolyPathOutputMode>()
	{
		return EDrawPolyPathOutputMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDrawPolyPathOutputMode(EDrawPolyPathOutputMode_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EDrawPolyPathOutputMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EDrawPolyPathOutputMode_Hash() { return 2990946516U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EDrawPolyPathOutputMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDrawPolyPathOutputMode"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EDrawPolyPathOutputMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDrawPolyPathOutputMode::Ribbon", (int64)EDrawPolyPathOutputMode::Ribbon },
				{ "EDrawPolyPathOutputMode::Extrusion", (int64)EDrawPolyPathOutputMode::Extrusion },
				{ "EDrawPolyPathOutputMode::Ramp", (int64)EDrawPolyPathOutputMode::Ramp },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Extrusion.Name", "EDrawPolyPathOutputMode::Extrusion" },
				{ "ModuleRelativePath", "Public/DrawPolyPathTool.h" },
				{ "Ramp.Name", "EDrawPolyPathOutputMode::Ramp" },
				{ "Ribbon.Name", "EDrawPolyPathOutputMode::Ribbon" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EDrawPolyPathOutputMode",
				"EDrawPolyPathOutputMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UDrawPolyPathToolBuilder::StaticRegisterNativesUDrawPolyPathToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UDrawPolyPathToolBuilder_NoRegister()
	{
		return UDrawPolyPathToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UDrawPolyPathToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDrawPolyPathToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshSurfacePointToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolyPathToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * ToolBuilder\n */" },
		{ "IncludePath", "DrawPolyPathTool.h" },
		{ "ModuleRelativePath", "Public/DrawPolyPathTool.h" },
		{ "ToolTip", "ToolBuilder" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDrawPolyPathToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDrawPolyPathToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDrawPolyPathToolBuilder_Statics::ClassParams = {
		&UDrawPolyPathToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UDrawPolyPathToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolyPathToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDrawPolyPathToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDrawPolyPathToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDrawPolyPathToolBuilder, 1344567189);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UDrawPolyPathToolBuilder>()
	{
		return UDrawPolyPathToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDrawPolyPathToolBuilder(Z_Construct_UClass_UDrawPolyPathToolBuilder, &UDrawPolyPathToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UDrawPolyPathToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDrawPolyPathToolBuilder);
	void UDrawPolyPathProperties::StaticRegisterNativesUDrawPolyPathProperties()
	{
	}
	UClass* Z_Construct_UClass_UDrawPolyPathProperties_NoRegister()
	{
		return UDrawPolyPathProperties::StaticClass();
	}
	struct Z_Construct_UClass_UDrawPolyPathProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_OutputType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OutputType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_OutputType;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_WidthMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WidthMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_WidthMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Width_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Width;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_HeightMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HeightMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_HeightMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Height_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Height;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RampStartRatio_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RampStartRatio;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSnapToWorldGrid_MetaData[];
#endif
		static void NewProp_bSnapToWorldGrid_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSnapToWorldGrid;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDrawPolyPathProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolyPathProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "DrawPolyPathTool.h" },
		{ "ModuleRelativePath", "Public/DrawPolyPathTool.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_OutputType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_OutputType_MetaData[] = {
		{ "Category", "Shape" },
		{ "ModuleRelativePath", "Public/DrawPolyPathTool.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_OutputType = { "OutputType", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawPolyPathProperties, OutputType), Z_Construct_UEnum_MeshModelingTools_EDrawPolyPathOutputMode, METADATA_PARAMS(Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_OutputType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_OutputType_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_WidthMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_WidthMode_MetaData[] = {
		{ "Category", "Shape" },
		{ "ModuleRelativePath", "Public/DrawPolyPathTool.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_WidthMode = { "WidthMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawPolyPathProperties, WidthMode), Z_Construct_UEnum_MeshModelingTools_EDrawPolyPathWidthMode, METADATA_PARAMS(Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_WidthMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_WidthMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_Width_MetaData[] = {
		{ "Category", "Shape" },
		{ "ClampMax", "999999" },
		{ "ClampMin", "0" },
		{ "ModuleRelativePath", "Public/DrawPolyPathTool.h" },
		{ "UIMax", "1000" },
		{ "UIMin", "0.0001" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_Width = { "Width", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawPolyPathProperties, Width), METADATA_PARAMS(Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_Width_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_Width_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_HeightMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_HeightMode_MetaData[] = {
		{ "Category", "Shape" },
		{ "EditCondition", "OutputType != EDrawPolyPathOutputMode::Ribbon" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/DrawPolyPathTool.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_HeightMode = { "HeightMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawPolyPathProperties, HeightMode), Z_Construct_UEnum_MeshModelingTools_EDrawPolyPathHeightMode, METADATA_PARAMS(Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_HeightMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_HeightMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_Height_MetaData[] = {
		{ "Category", "Shape" },
		{ "ClampMax", "10000" },
		{ "ClampMin", "-10000" },
		{ "EditCondition", "OutputType != EDrawPolyPathOutputMode::Ribbon" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/DrawPolyPathTool.h" },
		{ "UIMax", "1000" },
		{ "UIMin", "-1000" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_Height = { "Height", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawPolyPathProperties, Height), METADATA_PARAMS(Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_Height_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_Height_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_RampStartRatio_MetaData[] = {
		{ "Category", "Shape" },
		{ "ClampMax", "100.0" },
		{ "ClampMin", "0" },
		{ "EditCondition", "OutputType == EDrawPolyPathOutputMode::Ramp" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/DrawPolyPathTool.h" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.01" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_RampStartRatio = { "RampStartRatio", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawPolyPathProperties, RampStartRatio), METADATA_PARAMS(Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_RampStartRatio_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_RampStartRatio_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_bSnapToWorldGrid_MetaData[] = {
		{ "Category", "Snapping" },
		{ "ModuleRelativePath", "Public/DrawPolyPathTool.h" },
	};
#endif
	void Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_bSnapToWorldGrid_SetBit(void* Obj)
	{
		((UDrawPolyPathProperties*)Obj)->bSnapToWorldGrid = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_bSnapToWorldGrid = { "bSnapToWorldGrid", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDrawPolyPathProperties), &Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_bSnapToWorldGrid_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_bSnapToWorldGrid_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_bSnapToWorldGrid_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDrawPolyPathProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_OutputType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_OutputType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_WidthMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_WidthMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_Width,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_HeightMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_HeightMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_Height,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_RampStartRatio,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolyPathProperties_Statics::NewProp_bSnapToWorldGrid,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDrawPolyPathProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDrawPolyPathProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDrawPolyPathProperties_Statics::ClassParams = {
		&UDrawPolyPathProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDrawPolyPathProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolyPathProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UDrawPolyPathProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolyPathProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDrawPolyPathProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDrawPolyPathProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDrawPolyPathProperties, 2426233825);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UDrawPolyPathProperties>()
	{
		return UDrawPolyPathProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDrawPolyPathProperties(Z_Construct_UClass_UDrawPolyPathProperties, &UDrawPolyPathProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UDrawPolyPathProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDrawPolyPathProperties);
	void UDrawPolyPathExtrudeProperties::StaticRegisterNativesUDrawPolyPathExtrudeProperties()
	{
	}
	UClass* Z_Construct_UClass_UDrawPolyPathExtrudeProperties_NoRegister()
	{
		return UDrawPolyPathExtrudeProperties::StaticClass();
	}
	struct Z_Construct_UClass_UDrawPolyPathExtrudeProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Direction_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Direction_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Direction;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDrawPolyPathExtrudeProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolyPathExtrudeProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "DrawPolyPathTool.h" },
		{ "ModuleRelativePath", "Public/DrawPolyPathTool.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UDrawPolyPathExtrudeProperties_Statics::NewProp_Direction_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolyPathExtrudeProperties_Statics::NewProp_Direction_MetaData[] = {
		{ "Category", "Extrude" },
		{ "ModuleRelativePath", "Public/DrawPolyPathTool.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDrawPolyPathExtrudeProperties_Statics::NewProp_Direction = { "Direction", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawPolyPathExtrudeProperties, Direction), Z_Construct_UEnum_MeshModelingTools_EDrawPolyPathExtrudeDirection, METADATA_PARAMS(Z_Construct_UClass_UDrawPolyPathExtrudeProperties_Statics::NewProp_Direction_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolyPathExtrudeProperties_Statics::NewProp_Direction_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDrawPolyPathExtrudeProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolyPathExtrudeProperties_Statics::NewProp_Direction_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolyPathExtrudeProperties_Statics::NewProp_Direction,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDrawPolyPathExtrudeProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDrawPolyPathExtrudeProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDrawPolyPathExtrudeProperties_Statics::ClassParams = {
		&UDrawPolyPathExtrudeProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDrawPolyPathExtrudeProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolyPathExtrudeProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UDrawPolyPathExtrudeProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolyPathExtrudeProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDrawPolyPathExtrudeProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDrawPolyPathExtrudeProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDrawPolyPathExtrudeProperties, 284045406);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UDrawPolyPathExtrudeProperties>()
	{
		return UDrawPolyPathExtrudeProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDrawPolyPathExtrudeProperties(Z_Construct_UClass_UDrawPolyPathExtrudeProperties, &UDrawPolyPathExtrudeProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UDrawPolyPathExtrudeProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDrawPolyPathExtrudeProperties);
	void UDrawPolyPathTool::StaticRegisterNativesUDrawPolyPathTool()
	{
	}
	UClass* Z_Construct_UClass_UDrawPolyPathTool_NoRegister()
	{
		return UDrawPolyPathTool::StaticClass();
	}
	struct Z_Construct_UClass_UDrawPolyPathTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransformProps_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TransformProps;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExtrudeProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ExtrudeProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaterialProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MaterialProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlaneMechanic_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PlaneMechanic;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EditPreview_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_EditPreview;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExtrudeHeightMechanic_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ExtrudeHeightMechanic;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurveDistMechanic_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CurveDistMechanic;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SurfacePathMechanic_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SurfacePathMechanic;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDrawPolyPathTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolyPathTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "DrawPolyPathTool.h" },
		{ "ModuleRelativePath", "Public/DrawPolyPathTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolyPathTool_Statics::NewProp_TransformProps_MetaData[] = {
		{ "ModuleRelativePath", "Public/DrawPolyPathTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDrawPolyPathTool_Statics::NewProp_TransformProps = { "TransformProps", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawPolyPathTool, TransformProps), Z_Construct_UClass_UDrawPolyPathProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDrawPolyPathTool_Statics::NewProp_TransformProps_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolyPathTool_Statics::NewProp_TransformProps_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolyPathTool_Statics::NewProp_ExtrudeProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/DrawPolyPathTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDrawPolyPathTool_Statics::NewProp_ExtrudeProperties = { "ExtrudeProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawPolyPathTool, ExtrudeProperties), Z_Construct_UClass_UDrawPolyPathExtrudeProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDrawPolyPathTool_Statics::NewProp_ExtrudeProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolyPathTool_Statics::NewProp_ExtrudeProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolyPathTool_Statics::NewProp_MaterialProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/DrawPolyPathTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDrawPolyPathTool_Statics::NewProp_MaterialProperties = { "MaterialProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawPolyPathTool, MaterialProperties), Z_Construct_UClass_UNewMeshMaterialProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDrawPolyPathTool_Statics::NewProp_MaterialProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolyPathTool_Statics::NewProp_MaterialProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolyPathTool_Statics::NewProp_PlaneMechanic_MetaData[] = {
		{ "Comment", "// drawing plane and gizmo\n" },
		{ "ModuleRelativePath", "Public/DrawPolyPathTool.h" },
		{ "ToolTip", "drawing plane and gizmo" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDrawPolyPathTool_Statics::NewProp_PlaneMechanic = { "PlaneMechanic", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawPolyPathTool, PlaneMechanic), Z_Construct_UClass_UConstructionPlaneMechanic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDrawPolyPathTool_Statics::NewProp_PlaneMechanic_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolyPathTool_Statics::NewProp_PlaneMechanic_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolyPathTool_Statics::NewProp_EditPreview_MetaData[] = {
		{ "Comment", "// If true, CurPathPoints are assumed to define a closed path\n" },
		{ "ModuleRelativePath", "Public/DrawPolyPathTool.h" },
		{ "ToolTip", "If true, CurPathPoints are assumed to define a closed path" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDrawPolyPathTool_Statics::NewProp_EditPreview = { "EditPreview", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawPolyPathTool, EditPreview), Z_Construct_UClass_UPolyEditPreviewMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDrawPolyPathTool_Statics::NewProp_EditPreview_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolyPathTool_Statics::NewProp_EditPreview_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolyPathTool_Statics::NewProp_ExtrudeHeightMechanic_MetaData[] = {
		{ "ModuleRelativePath", "Public/DrawPolyPathTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDrawPolyPathTool_Statics::NewProp_ExtrudeHeightMechanic = { "ExtrudeHeightMechanic", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawPolyPathTool, ExtrudeHeightMechanic), Z_Construct_UClass_UPlaneDistanceFromHitMechanic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDrawPolyPathTool_Statics::NewProp_ExtrudeHeightMechanic_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolyPathTool_Statics::NewProp_ExtrudeHeightMechanic_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolyPathTool_Statics::NewProp_CurveDistMechanic_MetaData[] = {
		{ "ModuleRelativePath", "Public/DrawPolyPathTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDrawPolyPathTool_Statics::NewProp_CurveDistMechanic = { "CurveDistMechanic", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawPolyPathTool, CurveDistMechanic), Z_Construct_UClass_USpatialCurveDistanceMechanic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDrawPolyPathTool_Statics::NewProp_CurveDistMechanic_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolyPathTool_Statics::NewProp_CurveDistMechanic_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawPolyPathTool_Statics::NewProp_SurfacePathMechanic_MetaData[] = {
		{ "ModuleRelativePath", "Public/DrawPolyPathTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDrawPolyPathTool_Statics::NewProp_SurfacePathMechanic = { "SurfacePathMechanic", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawPolyPathTool, SurfacePathMechanic), Z_Construct_UClass_UCollectSurfacePathMechanic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDrawPolyPathTool_Statics::NewProp_SurfacePathMechanic_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolyPathTool_Statics::NewProp_SurfacePathMechanic_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDrawPolyPathTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolyPathTool_Statics::NewProp_TransformProps,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolyPathTool_Statics::NewProp_ExtrudeProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolyPathTool_Statics::NewProp_MaterialProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolyPathTool_Statics::NewProp_PlaneMechanic,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolyPathTool_Statics::NewProp_EditPreview,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolyPathTool_Statics::NewProp_ExtrudeHeightMechanic,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolyPathTool_Statics::NewProp_CurveDistMechanic,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawPolyPathTool_Statics::NewProp_SurfacePathMechanic,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDrawPolyPathTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDrawPolyPathTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDrawPolyPathTool_Statics::ClassParams = {
		&UDrawPolyPathTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDrawPolyPathTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolyPathTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UDrawPolyPathTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawPolyPathTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDrawPolyPathTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDrawPolyPathTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDrawPolyPathTool, 3684986565);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UDrawPolyPathTool>()
	{
		return UDrawPolyPathTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDrawPolyPathTool(Z_Construct_UClass_UDrawPolyPathTool, &UDrawPolyPathTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UDrawPolyPathTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDrawPolyPathTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
