// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingToolsEditorOnly/Public/ParameterizeMeshTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeParameterizeMeshTool() {}
// Cross Module References
	MESHMODELINGTOOLSEDITORONLY_API UEnum* Z_Construct_UEnum_MeshModelingToolsEditorOnly_EParameterizeMeshToolUVScaleMode();
	UPackage* Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly();
	MESHMODELINGTOOLSEDITORONLY_API UEnum* Z_Construct_UEnum_MeshModelingToolsEditorOnly_EUVIslandMode();
	MESHMODELINGTOOLSEDITORONLY_API UEnum* Z_Construct_UEnum_MeshModelingToolsEditorOnly_EUVUnwrapType();
	MESHMODELINGTOOLSEDITORONLY_API UEnum* Z_Construct_UEnum_MeshModelingToolsEditorOnly_EParameterizeMeshMaterialMode();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UParameterizeMeshToolBuilder_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UParameterizeMeshToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UParameterizeMeshToolProperties_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UParameterizeMeshToolProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UParameterizeMeshTool_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UParameterizeMeshTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_USingleSelectionTool();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UExistingMeshMaterialProperties_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_NoRegister();
// End Cross Module References
	static UEnum* EParameterizeMeshToolUVScaleMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingToolsEditorOnly_EParameterizeMeshToolUVScaleMode, Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly(), TEXT("EParameterizeMeshToolUVScaleMode"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLSEDITORONLY_API UEnum* StaticEnum<EParameterizeMeshToolUVScaleMode>()
	{
		return EParameterizeMeshToolUVScaleMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EParameterizeMeshToolUVScaleMode(EParameterizeMeshToolUVScaleMode_StaticEnum, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("EParameterizeMeshToolUVScaleMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingToolsEditorOnly_EParameterizeMeshToolUVScaleMode_Hash() { return 284245894U; }
	UEnum* Z_Construct_UEnum_MeshModelingToolsEditorOnly_EParameterizeMeshToolUVScaleMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EParameterizeMeshToolUVScaleMode"), 0, Get_Z_Construct_UEnum_MeshModelingToolsEditorOnly_EParameterizeMeshToolUVScaleMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EParameterizeMeshToolUVScaleMode::NoScaling", (int64)EParameterizeMeshToolUVScaleMode::NoScaling },
				{ "EParameterizeMeshToolUVScaleMode::NormalizeToBounds", (int64)EParameterizeMeshToolUVScaleMode::NormalizeToBounds },
				{ "EParameterizeMeshToolUVScaleMode::NormalizeToWorld", (int64)EParameterizeMeshToolUVScaleMode::NormalizeToWorld },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/ParameterizeMeshTool.h" },
				{ "NormalizeToBounds.Comment", "/** Scale UV islands such that they have constant relative area, relative to object bounds */" },
				{ "NormalizeToBounds.Name", "EParameterizeMeshToolUVScaleMode::NormalizeToBounds" },
				{ "NormalizeToBounds.ToolTip", "Scale UV islands such that they have constant relative area, relative to object bounds" },
				{ "NormalizeToWorld.Comment", "/** Scale UV islands such that they have constant relative area, relative to world space */" },
				{ "NormalizeToWorld.Name", "EParameterizeMeshToolUVScaleMode::NormalizeToWorld" },
				{ "NormalizeToWorld.ToolTip", "Scale UV islands such that they have constant relative area, relative to world space" },
				{ "NoScaling.Comment", "/** No scaling is applied to UV islands */" },
				{ "NoScaling.Name", "EParameterizeMeshToolUVScaleMode::NoScaling" },
				{ "NoScaling.ToolTip", "No scaling is applied to UV islands" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
				nullptr,
				"EParameterizeMeshToolUVScaleMode",
				"EParameterizeMeshToolUVScaleMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EUVIslandMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingToolsEditorOnly_EUVIslandMode, Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly(), TEXT("EUVIslandMode"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLSEDITORONLY_API UEnum* StaticEnum<EUVIslandMode>()
	{
		return EUVIslandMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EUVIslandMode(EUVIslandMode_StaticEnum, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("EUVIslandMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingToolsEditorOnly_EUVIslandMode_Hash() { return 4151562045U; }
	UEnum* Z_Construct_UEnum_MeshModelingToolsEditorOnly_EUVIslandMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EUVIslandMode"), 0, Get_Z_Construct_UEnum_MeshModelingToolsEditorOnly_EUVIslandMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EUVIslandMode::Auto", (int64)EUVIslandMode::Auto },
				{ "EUVIslandMode::PolyGroups", (int64)EUVIslandMode::PolyGroups },
				{ "EUVIslandMode::ExistingUVs", (int64)EUVIslandMode::ExistingUVs },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Auto.Comment", "/**  */" },
				{ "Auto.Name", "EUVIslandMode::Auto" },
				{ "ExistingUVs.Comment", "/** */" },
				{ "ExistingUVs.Name", "EUVIslandMode::ExistingUVs" },
				{ "ModuleRelativePath", "Public/ParameterizeMeshTool.h" },
				{ "PolyGroups.Comment", "/** */" },
				{ "PolyGroups.Name", "EUVIslandMode::PolyGroups" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
				nullptr,
				"EUVIslandMode",
				"EUVIslandMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EUVUnwrapType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingToolsEditorOnly_EUVUnwrapType, Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly(), TEXT("EUVUnwrapType"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLSEDITORONLY_API UEnum* StaticEnum<EUVUnwrapType>()
	{
		return EUVUnwrapType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EUVUnwrapType(EUVUnwrapType_StaticEnum, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("EUVUnwrapType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingToolsEditorOnly_EUVUnwrapType_Hash() { return 3800660807U; }
	UEnum* Z_Construct_UEnum_MeshModelingToolsEditorOnly_EUVUnwrapType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EUVUnwrapType"), 0, Get_Z_Construct_UEnum_MeshModelingToolsEditorOnly_EUVUnwrapType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EUVUnwrapType::MinStretch", (int64)EUVUnwrapType::MinStretch },
				{ "EUVUnwrapType::ExpMap", (int64)EUVUnwrapType::ExpMap },
				{ "EUVUnwrapType::Conformal", (int64)EUVUnwrapType::Conformal },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Conformal.Comment", "/** */" },
				{ "Conformal.Name", "EUVUnwrapType::Conformal" },
				{ "ExpMap.Comment", "/** */" },
				{ "ExpMap.Name", "EUVUnwrapType::ExpMap" },
				{ "MinStretch.Comment", "/**  */" },
				{ "MinStretch.Name", "EUVUnwrapType::MinStretch" },
				{ "ModuleRelativePath", "Public/ParameterizeMeshTool.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
				nullptr,
				"EUVUnwrapType",
				"EUVUnwrapType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EParameterizeMeshMaterialMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingToolsEditorOnly_EParameterizeMeshMaterialMode, Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly(), TEXT("EParameterizeMeshMaterialMode"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLSEDITORONLY_API UEnum* StaticEnum<EParameterizeMeshMaterialMode>()
	{
		return EParameterizeMeshMaterialMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EParameterizeMeshMaterialMode(EParameterizeMeshMaterialMode_StaticEnum, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("EParameterizeMeshMaterialMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingToolsEditorOnly_EParameterizeMeshMaterialMode_Hash() { return 3750696372U; }
	UEnum* Z_Construct_UEnum_MeshModelingToolsEditorOnly_EParameterizeMeshMaterialMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EParameterizeMeshMaterialMode"), 0, Get_Z_Construct_UEnum_MeshModelingToolsEditorOnly_EParameterizeMeshMaterialMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EParameterizeMeshMaterialMode::Default", (int64)EParameterizeMeshMaterialMode::Default },
				{ "EParameterizeMeshMaterialMode::Checkerboard", (int64)EParameterizeMeshMaterialMode::Checkerboard },
				{ "EParameterizeMeshMaterialMode::Override", (int64)EParameterizeMeshMaterialMode::Override },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Checkerboard.Comment", "/** Checkerboard material */" },
				{ "Checkerboard.Name", "EParameterizeMeshMaterialMode::Checkerboard" },
				{ "Checkerboard.ToolTip", "Checkerboard material" },
				{ "Comment", "/** Material modes for MeshInspectorTool */" },
				{ "Default.Comment", "/** Input material */" },
				{ "Default.Name", "EParameterizeMeshMaterialMode::Default" },
				{ "Default.ToolTip", "Input material" },
				{ "ModuleRelativePath", "Public/ParameterizeMeshTool.h" },
				{ "Override.Comment", "/** Override material */" },
				{ "Override.Name", "EParameterizeMeshMaterialMode::Override" },
				{ "Override.ToolTip", "Override material" },
				{ "ToolTip", "Material modes for MeshInspectorTool" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
				nullptr,
				"EParameterizeMeshMaterialMode",
				"EParameterizeMeshMaterialMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UParameterizeMeshToolBuilder::StaticRegisterNativesUParameterizeMeshToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UParameterizeMeshToolBuilder_NoRegister()
	{
		return UParameterizeMeshToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UParameterizeMeshToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UParameterizeMeshToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UParameterizeMeshToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "ParameterizeMeshTool.h" },
		{ "ModuleRelativePath", "Public/ParameterizeMeshTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UParameterizeMeshToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UParameterizeMeshToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UParameterizeMeshToolBuilder_Statics::ClassParams = {
		&UParameterizeMeshToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UParameterizeMeshToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UParameterizeMeshToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UParameterizeMeshToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UParameterizeMeshToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UParameterizeMeshToolBuilder, 1672958696);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UParameterizeMeshToolBuilder>()
	{
		return UParameterizeMeshToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UParameterizeMeshToolBuilder(Z_Construct_UClass_UParameterizeMeshToolBuilder, &UParameterizeMeshToolBuilder::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UParameterizeMeshToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UParameterizeMeshToolBuilder);
	void UParameterizeMeshToolProperties::StaticRegisterNativesUParameterizeMeshToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UParameterizeMeshToolProperties_NoRegister()
	{
		return UParameterizeMeshToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UParameterizeMeshToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_IslandMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IslandMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_IslandMode;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_UnwrapType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UnwrapType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_UnwrapType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChartStretch_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ChartStretch;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_UVScaleMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UVScaleMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_UVScaleMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UVScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_UVScale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsGlobalMode_MetaData[];
#endif
		static void NewProp_bIsGlobalMode_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsGlobalMode;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ParameterizeMeshTool.h" },
		{ "ModuleRelativePath", "Public/ParameterizeMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::NewProp_IslandMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::NewProp_IslandMode_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "//UPROPERTY(EditAnywhere, Category = Options)\n" },
		{ "EditCondition", "bIsGlobalMode == false" },
		{ "EditConditionHides", "" },
		{ "HideEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/ParameterizeMeshTool.h" },
		{ "ToolTip", "UPROPERTY(EditAnywhere, Category = Options)" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::NewProp_IslandMode = { "IslandMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UParameterizeMeshToolProperties, IslandMode), Z_Construct_UEnum_MeshModelingToolsEditorOnly_EUVIslandMode, METADATA_PARAMS(Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::NewProp_IslandMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::NewProp_IslandMode_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::NewProp_UnwrapType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::NewProp_UnwrapType_MetaData[] = {
		{ "Category", "Options" },
		{ "EditCondition", "bIsGlobalMode == false" },
		{ "EditConditionHides", "" },
		{ "HideEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/ParameterizeMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::NewProp_UnwrapType = { "UnwrapType", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UParameterizeMeshToolProperties, UnwrapType), Z_Construct_UEnum_MeshModelingToolsEditorOnly_EUVUnwrapType, METADATA_PARAMS(Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::NewProp_UnwrapType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::NewProp_UnwrapType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::NewProp_ChartStretch_MetaData[] = {
		{ "Category", "Options" },
		{ "ClampMax", "1" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Maximum amount of stretch, from none to any.  If zero stretch is specified each triangle will likey be its own chart */" },
		{ "Default", "0.166" },
		{ "EditCondition", "bIsGlobalMode || UnwrapType == EUVUnwrapType::MinStretch" },
		{ "ModuleRelativePath", "Public/ParameterizeMeshTool.h" },
		{ "ToolTip", "Maximum amount of stretch, from none to any.  If zero stretch is specified each triangle will likey be its own chart" },
		{ "UIMax", "1" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::NewProp_ChartStretch = { "ChartStretch", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UParameterizeMeshToolProperties, ChartStretch), METADATA_PARAMS(Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::NewProp_ChartStretch_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::NewProp_ChartStretch_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::NewProp_UVScaleMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::NewProp_UVScaleMode_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Scaling applied to UV islands */" },
		{ "ModuleRelativePath", "Public/ParameterizeMeshTool.h" },
		{ "ToolTip", "Scaling applied to UV islands" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::NewProp_UVScaleMode = { "UVScaleMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UParameterizeMeshToolProperties, UVScaleMode), Z_Construct_UEnum_MeshModelingToolsEditorOnly_EParameterizeMeshToolUVScaleMode, METADATA_PARAMS(Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::NewProp_UVScaleMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::NewProp_UVScaleMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::NewProp_UVScale_MetaData[] = {
		{ "Category", "Options" },
		{ "ClampMax", "1000000.0" },
		{ "ClampMin", "0.00001" },
		{ "Comment", "/** Scaling factor used for UV island normalization/scaling */" },
		{ "EditCondition", "UVScaleMode!=EParameterizeMeshToolUVScaleMode::NoScaling" },
		{ "ModuleRelativePath", "Public/ParameterizeMeshTool.h" },
		{ "ToolTip", "Scaling factor used for UV island normalization/scaling" },
		{ "UIMax", "10" },
		{ "UIMin", "0.001" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::NewProp_UVScale = { "UVScale", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UParameterizeMeshToolProperties, UVScale), METADATA_PARAMS(Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::NewProp_UVScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::NewProp_UVScale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::NewProp_bIsGlobalMode_MetaData[] = {
		{ "ModuleRelativePath", "Public/ParameterizeMeshTool.h" },
		{ "TransientToolProperty", "" },
	};
#endif
	void Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::NewProp_bIsGlobalMode_SetBit(void* Obj)
	{
		((UParameterizeMeshToolProperties*)Obj)->bIsGlobalMode = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::NewProp_bIsGlobalMode = { "bIsGlobalMode", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UParameterizeMeshToolProperties), &Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::NewProp_bIsGlobalMode_SetBit, METADATA_PARAMS(Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::NewProp_bIsGlobalMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::NewProp_bIsGlobalMode_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::NewProp_IslandMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::NewProp_IslandMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::NewProp_UnwrapType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::NewProp_UnwrapType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::NewProp_ChartStretch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::NewProp_UVScaleMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::NewProp_UVScaleMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::NewProp_UVScale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::NewProp_bIsGlobalMode,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UParameterizeMeshToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::ClassParams = {
		&UParameterizeMeshToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UParameterizeMeshToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UParameterizeMeshToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UParameterizeMeshToolProperties, 2955169557);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UParameterizeMeshToolProperties>()
	{
		return UParameterizeMeshToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UParameterizeMeshToolProperties(Z_Construct_UClass_UParameterizeMeshToolProperties, &UParameterizeMeshToolProperties::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UParameterizeMeshToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UParameterizeMeshToolProperties);
	void UParameterizeMeshTool::StaticRegisterNativesUParameterizeMeshTool()
	{
	}
	UClass* Z_Construct_UClass_UParameterizeMeshTool_NoRegister()
	{
		return UParameterizeMeshTool::StaticClass();
	}
	struct Z_Construct_UClass_UParameterizeMeshTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Settings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Settings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaterialSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MaterialSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DefaultMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisplayMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DisplayMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CheckerMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CheckerMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Preview_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Preview;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDoAutomaticGlobalUnwrap_MetaData[];
#endif
		static void NewProp_bDoAutomaticGlobalUnwrap_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDoAutomaticGlobalUnwrap;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UParameterizeMeshTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USingleSelectionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UParameterizeMeshTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Simple Mesh Simplifying Tool\n */" },
		{ "IncludePath", "ParameterizeMeshTool.h" },
		{ "ModuleRelativePath", "Public/ParameterizeMeshTool.h" },
		{ "ToolTip", "Simple Mesh Simplifying Tool" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UParameterizeMeshTool_Statics::NewProp_Settings_MetaData[] = {
		{ "ModuleRelativePath", "Public/ParameterizeMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UParameterizeMeshTool_Statics::NewProp_Settings = { "Settings", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UParameterizeMeshTool, Settings), Z_Construct_UClass_UParameterizeMeshToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UParameterizeMeshTool_Statics::NewProp_Settings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UParameterizeMeshTool_Statics::NewProp_Settings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UParameterizeMeshTool_Statics::NewProp_MaterialSettings_MetaData[] = {
		{ "ModuleRelativePath", "Public/ParameterizeMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UParameterizeMeshTool_Statics::NewProp_MaterialSettings = { "MaterialSettings", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UParameterizeMeshTool, MaterialSettings), Z_Construct_UClass_UExistingMeshMaterialProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UParameterizeMeshTool_Statics::NewProp_MaterialSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UParameterizeMeshTool_Statics::NewProp_MaterialSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UParameterizeMeshTool_Statics::NewProp_DefaultMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Public/ParameterizeMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UParameterizeMeshTool_Statics::NewProp_DefaultMaterial = { "DefaultMaterial", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UParameterizeMeshTool, DefaultMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UParameterizeMeshTool_Statics::NewProp_DefaultMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UParameterizeMeshTool_Statics::NewProp_DefaultMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UParameterizeMeshTool_Statics::NewProp_DisplayMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Public/ParameterizeMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UParameterizeMeshTool_Statics::NewProp_DisplayMaterial = { "DisplayMaterial", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UParameterizeMeshTool, DisplayMaterial), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UParameterizeMeshTool_Statics::NewProp_DisplayMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UParameterizeMeshTool_Statics::NewProp_DisplayMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UParameterizeMeshTool_Statics::NewProp_CheckerMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Public/ParameterizeMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UParameterizeMeshTool_Statics::NewProp_CheckerMaterial = { "CheckerMaterial", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UParameterizeMeshTool, CheckerMaterial), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UParameterizeMeshTool_Statics::NewProp_CheckerMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UParameterizeMeshTool_Statics::NewProp_CheckerMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UParameterizeMeshTool_Statics::NewProp_Preview_MetaData[] = {
		{ "ModuleRelativePath", "Public/ParameterizeMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UParameterizeMeshTool_Statics::NewProp_Preview = { "Preview", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UParameterizeMeshTool, Preview), Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UParameterizeMeshTool_Statics::NewProp_Preview_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UParameterizeMeshTool_Statics::NewProp_Preview_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UParameterizeMeshTool_Statics::NewProp_bDoAutomaticGlobalUnwrap_MetaData[] = {
		{ "ModuleRelativePath", "Public/ParameterizeMeshTool.h" },
	};
#endif
	void Z_Construct_UClass_UParameterizeMeshTool_Statics::NewProp_bDoAutomaticGlobalUnwrap_SetBit(void* Obj)
	{
		((UParameterizeMeshTool*)Obj)->bDoAutomaticGlobalUnwrap = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UParameterizeMeshTool_Statics::NewProp_bDoAutomaticGlobalUnwrap = { "bDoAutomaticGlobalUnwrap", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UParameterizeMeshTool), &Z_Construct_UClass_UParameterizeMeshTool_Statics::NewProp_bDoAutomaticGlobalUnwrap_SetBit, METADATA_PARAMS(Z_Construct_UClass_UParameterizeMeshTool_Statics::NewProp_bDoAutomaticGlobalUnwrap_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UParameterizeMeshTool_Statics::NewProp_bDoAutomaticGlobalUnwrap_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UParameterizeMeshTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UParameterizeMeshTool_Statics::NewProp_Settings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UParameterizeMeshTool_Statics::NewProp_MaterialSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UParameterizeMeshTool_Statics::NewProp_DefaultMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UParameterizeMeshTool_Statics::NewProp_DisplayMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UParameterizeMeshTool_Statics::NewProp_CheckerMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UParameterizeMeshTool_Statics::NewProp_Preview,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UParameterizeMeshTool_Statics::NewProp_bDoAutomaticGlobalUnwrap,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UParameterizeMeshTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UParameterizeMeshTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UParameterizeMeshTool_Statics::ClassParams = {
		&UParameterizeMeshTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UParameterizeMeshTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UParameterizeMeshTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UParameterizeMeshTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UParameterizeMeshTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UParameterizeMeshTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UParameterizeMeshTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UParameterizeMeshTool, 1508084391);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UParameterizeMeshTool>()
	{
		return UParameterizeMeshTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UParameterizeMeshTool(Z_Construct_UClass_UParameterizeMeshTool, &UParameterizeMeshTool::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UParameterizeMeshTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UParameterizeMeshTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
