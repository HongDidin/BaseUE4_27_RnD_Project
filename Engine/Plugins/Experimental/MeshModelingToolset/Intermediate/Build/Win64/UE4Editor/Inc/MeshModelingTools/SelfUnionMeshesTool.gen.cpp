// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/SelfUnionMeshesTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSelfUnionMeshesTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_USelfUnionMeshesToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_USelfUnionMeshesToolProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_USelfUnionMeshesTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_USelfUnionMeshesTool();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UBaseCreateFromSelectedTool();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_ULineSetComponent_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_USelfUnionMeshesToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_USelfUnionMeshesToolBuilder();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UBaseCreateFromSelectedToolBuilder();
// End Cross Module References
	void USelfUnionMeshesToolProperties::StaticRegisterNativesUSelfUnionMeshesToolProperties()
	{
	}
	UClass* Z_Construct_UClass_USelfUnionMeshesToolProperties_NoRegister()
	{
		return USelfUnionMeshesToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAttemptFixHoles_MetaData[];
#endif
		static void NewProp_bAttemptFixHoles_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAttemptFixHoles;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowNewBoundaryEdges_MetaData[];
#endif
		static void NewProp_bShowNewBoundaryEdges_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowNewBoundaryEdges;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bTrimFlaps_MetaData[];
#endif
		static void NewProp_bTrimFlaps_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bTrimFlaps;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WindingNumberThreshold_MetaData[];
#endif
		static const UE4CodeGen_Private::FDoublePropertyParams NewProp_WindingNumberThreshold;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOnlyUseFirstMeshMaterials_MetaData[];
#endif
		static void NewProp_bOnlyUseFirstMeshMaterials_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOnlyUseFirstMeshMaterials;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Standard properties of the self-union operation\n */" },
		{ "IncludePath", "SelfUnionMeshesTool.h" },
		{ "ModuleRelativePath", "Public/SelfUnionMeshesTool.h" },
		{ "ToolTip", "Standard properties of the self-union operation" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::NewProp_bAttemptFixHoles_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Automatically attempt to fill any holes left by merging (e.g. due to numerical errors) */" },
		{ "ModuleRelativePath", "Public/SelfUnionMeshesTool.h" },
		{ "ToolTip", "Automatically attempt to fill any holes left by merging (e.g. due to numerical errors)" },
	};
#endif
	void Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::NewProp_bAttemptFixHoles_SetBit(void* Obj)
	{
		((USelfUnionMeshesToolProperties*)Obj)->bAttemptFixHoles = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::NewProp_bAttemptFixHoles = { "bAttemptFixHoles", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(USelfUnionMeshesToolProperties), &Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::NewProp_bAttemptFixHoles_SetBit, METADATA_PARAMS(Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::NewProp_bAttemptFixHoles_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::NewProp_bAttemptFixHoles_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::NewProp_bShowNewBoundaryEdges_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Show boundary edges created by the union operation -- often due to numerical error */" },
		{ "ModuleRelativePath", "Public/SelfUnionMeshesTool.h" },
		{ "ToolTip", "Show boundary edges created by the union operation -- often due to numerical error" },
	};
#endif
	void Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::NewProp_bShowNewBoundaryEdges_SetBit(void* Obj)
	{
		((USelfUnionMeshesToolProperties*)Obj)->bShowNewBoundaryEdges = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::NewProp_bShowNewBoundaryEdges = { "bShowNewBoundaryEdges", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(USelfUnionMeshesToolProperties), &Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::NewProp_bShowNewBoundaryEdges_SetBit, METADATA_PARAMS(Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::NewProp_bShowNewBoundaryEdges_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::NewProp_bShowNewBoundaryEdges_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::NewProp_bTrimFlaps_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** If true, remove open, visible geometry */" },
		{ "ModuleRelativePath", "Public/SelfUnionMeshesTool.h" },
		{ "ToolTip", "If true, remove open, visible geometry" },
	};
#endif
	void Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::NewProp_bTrimFlaps_SetBit(void* Obj)
	{
		((USelfUnionMeshesToolProperties*)Obj)->bTrimFlaps = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::NewProp_bTrimFlaps = { "bTrimFlaps", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(USelfUnionMeshesToolProperties), &Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::NewProp_bTrimFlaps_SetBit, METADATA_PARAMS(Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::NewProp_bTrimFlaps_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::NewProp_bTrimFlaps_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::NewProp_WindingNumberThreshold_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Winding number threshold to determine what is consider inside the mesh */" },
		{ "ModuleRelativePath", "Public/SelfUnionMeshesTool.h" },
		{ "ToolTip", "Winding number threshold to determine what is consider inside the mesh" },
	};
#endif
	const UE4CodeGen_Private::FDoublePropertyParams Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::NewProp_WindingNumberThreshold = { "WindingNumberThreshold", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USelfUnionMeshesToolProperties, WindingNumberThreshold), METADATA_PARAMS(Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::NewProp_WindingNumberThreshold_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::NewProp_WindingNumberThreshold_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::NewProp_bOnlyUseFirstMeshMaterials_MetaData[] = {
		{ "Category", "Materials" },
		{ "Comment", "/** If true, only the first mesh will keep its materials assignments; all other triangles will be assigned material 0 */" },
		{ "ModuleRelativePath", "Public/SelfUnionMeshesTool.h" },
		{ "ToolTip", "If true, only the first mesh will keep its materials assignments; all other triangles will be assigned material 0" },
	};
#endif
	void Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::NewProp_bOnlyUseFirstMeshMaterials_SetBit(void* Obj)
	{
		((USelfUnionMeshesToolProperties*)Obj)->bOnlyUseFirstMeshMaterials = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::NewProp_bOnlyUseFirstMeshMaterials = { "bOnlyUseFirstMeshMaterials", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(USelfUnionMeshesToolProperties), &Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::NewProp_bOnlyUseFirstMeshMaterials_SetBit, METADATA_PARAMS(Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::NewProp_bOnlyUseFirstMeshMaterials_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::NewProp_bOnlyUseFirstMeshMaterials_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::NewProp_bAttemptFixHoles,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::NewProp_bShowNewBoundaryEdges,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::NewProp_bTrimFlaps,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::NewProp_WindingNumberThreshold,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::NewProp_bOnlyUseFirstMeshMaterials,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USelfUnionMeshesToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::ClassParams = {
		&USelfUnionMeshesToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USelfUnionMeshesToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USelfUnionMeshesToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USelfUnionMeshesToolProperties, 2486245930);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<USelfUnionMeshesToolProperties>()
	{
		return USelfUnionMeshesToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USelfUnionMeshesToolProperties(Z_Construct_UClass_USelfUnionMeshesToolProperties, &USelfUnionMeshesToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("USelfUnionMeshesToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USelfUnionMeshesToolProperties);
	void USelfUnionMeshesTool::StaticRegisterNativesUSelfUnionMeshesTool()
	{
	}
	UClass* Z_Construct_UClass_USelfUnionMeshesTool_NoRegister()
	{
		return USelfUnionMeshesTool::StaticClass();
	}
	struct Z_Construct_UClass_USelfUnionMeshesTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Properties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Properties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DrawnLineSet_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DrawnLineSet;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USelfUnionMeshesTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBaseCreateFromSelectedTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USelfUnionMeshesTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Union of meshes, resolving self intersections\n */" },
		{ "IncludePath", "SelfUnionMeshesTool.h" },
		{ "ModuleRelativePath", "Public/SelfUnionMeshesTool.h" },
		{ "ToolTip", "Union of meshes, resolving self intersections" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USelfUnionMeshesTool_Statics::NewProp_Properties_MetaData[] = {
		{ "ModuleRelativePath", "Public/SelfUnionMeshesTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USelfUnionMeshesTool_Statics::NewProp_Properties = { "Properties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USelfUnionMeshesTool, Properties), Z_Construct_UClass_USelfUnionMeshesToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USelfUnionMeshesTool_Statics::NewProp_Properties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USelfUnionMeshesTool_Statics::NewProp_Properties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USelfUnionMeshesTool_Statics::NewProp_DrawnLineSet_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/SelfUnionMeshesTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USelfUnionMeshesTool_Statics::NewProp_DrawnLineSet = { "DrawnLineSet", nullptr, (EPropertyFlags)0x0020080000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USelfUnionMeshesTool, DrawnLineSet), Z_Construct_UClass_ULineSetComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USelfUnionMeshesTool_Statics::NewProp_DrawnLineSet_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USelfUnionMeshesTool_Statics::NewProp_DrawnLineSet_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USelfUnionMeshesTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USelfUnionMeshesTool_Statics::NewProp_Properties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USelfUnionMeshesTool_Statics::NewProp_DrawnLineSet,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USelfUnionMeshesTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USelfUnionMeshesTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USelfUnionMeshesTool_Statics::ClassParams = {
		&USelfUnionMeshesTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USelfUnionMeshesTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USelfUnionMeshesTool_Statics::PropPointers),
		0,
		0x009000A8u,
		METADATA_PARAMS(Z_Construct_UClass_USelfUnionMeshesTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USelfUnionMeshesTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USelfUnionMeshesTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USelfUnionMeshesTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USelfUnionMeshesTool, 1563488411);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<USelfUnionMeshesTool>()
	{
		return USelfUnionMeshesTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USelfUnionMeshesTool(Z_Construct_UClass_USelfUnionMeshesTool, &USelfUnionMeshesTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("USelfUnionMeshesTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USelfUnionMeshesTool);
	void USelfUnionMeshesToolBuilder::StaticRegisterNativesUSelfUnionMeshesToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_USelfUnionMeshesToolBuilder_NoRegister()
	{
		return USelfUnionMeshesToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_USelfUnionMeshesToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USelfUnionMeshesToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBaseCreateFromSelectedToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USelfUnionMeshesToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "SelfUnionMeshesTool.h" },
		{ "ModuleRelativePath", "Public/SelfUnionMeshesTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USelfUnionMeshesToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USelfUnionMeshesToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USelfUnionMeshesToolBuilder_Statics::ClassParams = {
		&USelfUnionMeshesToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_USelfUnionMeshesToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USelfUnionMeshesToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USelfUnionMeshesToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USelfUnionMeshesToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USelfUnionMeshesToolBuilder, 3743276506);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<USelfUnionMeshesToolBuilder>()
	{
		return USelfUnionMeshesToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USelfUnionMeshesToolBuilder(Z_Construct_UClass_USelfUnionMeshesToolBuilder, &USelfUnionMeshesToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("USelfUnionMeshesToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USelfUnionMeshesToolBuilder);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
