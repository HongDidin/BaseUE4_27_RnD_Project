// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLS_CombineMeshesTool_generated_h
#error "CombineMeshesTool.generated.h already included, missing '#pragma once' in CombineMeshesTool.h"
#endif
#define MESHMODELINGTOOLS_CombineMeshesTool_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_30_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_30_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_30_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_30_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCombineMeshesToolBuilder(); \
	friend struct Z_Construct_UClass_UCombineMeshesToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UCombineMeshesToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UCombineMeshesToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_30_INCLASS \
private: \
	static void StaticRegisterNativesUCombineMeshesToolBuilder(); \
	friend struct Z_Construct_UClass_UCombineMeshesToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UCombineMeshesToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UCombineMeshesToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_30_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCombineMeshesToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCombineMeshesToolBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCombineMeshesToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCombineMeshesToolBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCombineMeshesToolBuilder(UCombineMeshesToolBuilder&&); \
	NO_API UCombineMeshesToolBuilder(const UCombineMeshesToolBuilder&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_30_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCombineMeshesToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCombineMeshesToolBuilder(UCombineMeshesToolBuilder&&); \
	NO_API UCombineMeshesToolBuilder(const UCombineMeshesToolBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCombineMeshesToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCombineMeshesToolBuilder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCombineMeshesToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_30_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_27_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_30_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_30_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_30_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_30_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_30_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_30_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_30_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_30_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_30_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_30_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_30_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_30_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UCombineMeshesToolBuilder>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_58_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_58_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_58_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_58_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCombineMeshesToolProperties(); \
	friend struct Z_Construct_UClass_UCombineMeshesToolProperties_Statics; \
public: \
	DECLARE_CLASS(UCombineMeshesToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UCombineMeshesToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_58_INCLASS \
private: \
	static void StaticRegisterNativesUCombineMeshesToolProperties(); \
	friend struct Z_Construct_UClass_UCombineMeshesToolProperties_Statics; \
public: \
	DECLARE_CLASS(UCombineMeshesToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UCombineMeshesToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_58_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCombineMeshesToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCombineMeshesToolProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCombineMeshesToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCombineMeshesToolProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCombineMeshesToolProperties(UCombineMeshesToolProperties&&); \
	NO_API UCombineMeshesToolProperties(const UCombineMeshesToolProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_58_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCombineMeshesToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCombineMeshesToolProperties(UCombineMeshesToolProperties&&); \
	NO_API UCombineMeshesToolProperties(const UCombineMeshesToolProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCombineMeshesToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCombineMeshesToolProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCombineMeshesToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_58_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_55_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_58_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_58_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_58_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_58_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_58_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_58_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_58_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_58_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_58_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_58_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_58_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_58_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UCombineMeshesToolProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_87_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_87_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_87_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_87_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCombineMeshesTool(); \
	friend struct Z_Construct_UClass_UCombineMeshesTool_Statics; \
public: \
	DECLARE_CLASS(UCombineMeshesTool, UMultiSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UCombineMeshesTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_87_INCLASS \
private: \
	static void StaticRegisterNativesUCombineMeshesTool(); \
	friend struct Z_Construct_UClass_UCombineMeshesTool_Statics; \
public: \
	DECLARE_CLASS(UCombineMeshesTool, UMultiSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UCombineMeshesTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_87_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCombineMeshesTool(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCombineMeshesTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCombineMeshesTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCombineMeshesTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCombineMeshesTool(UCombineMeshesTool&&); \
	NO_API UCombineMeshesTool(const UCombineMeshesTool&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_87_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCombineMeshesTool() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCombineMeshesTool(UCombineMeshesTool&&); \
	NO_API UCombineMeshesTool(const UCombineMeshesTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCombineMeshesTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCombineMeshesTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UCombineMeshesTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_87_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__BasicProperties() { return STRUCT_OFFSET(UCombineMeshesTool, BasicProperties); } \
	FORCEINLINE static uint32 __PPO__HandleSourceProperties() { return STRUCT_OFFSET(UCombineMeshesTool, HandleSourceProperties); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_84_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_87_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_87_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_87_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_87_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_87_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_87_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_87_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_87_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_87_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_87_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_87_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h_87_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UCombineMeshesTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CombineMeshesTool_h


#define FOREACH_ENUM_ECOMBINETARGETTYPE(op) \
	op(ECombineTargetType::NewAsset) \
	op(ECombineTargetType::FirstInputAsset) \
	op(ECombineTargetType::LastInputAsset) 

enum class ECombineTargetType;
template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<ECombineTargetType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
