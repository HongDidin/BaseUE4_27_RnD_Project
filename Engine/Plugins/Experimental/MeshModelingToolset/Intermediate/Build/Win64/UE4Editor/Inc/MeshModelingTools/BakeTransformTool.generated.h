// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLS_BakeTransformTool_generated_h
#error "BakeTransformTool.generated.h already included, missing '#pragma once' in BakeTransformTool.h"
#endif
#define MESHMODELINGTOOLS_BakeTransformTool_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_29_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_29_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_29_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_29_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUBakeTransformToolBuilder(); \
	friend struct Z_Construct_UClass_UBakeTransformToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UBakeTransformToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UBakeTransformToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_29_INCLASS \
private: \
	static void StaticRegisterNativesUBakeTransformToolBuilder(); \
	friend struct Z_Construct_UClass_UBakeTransformToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UBakeTransformToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UBakeTransformToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_29_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBakeTransformToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBakeTransformToolBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBakeTransformToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBakeTransformToolBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBakeTransformToolBuilder(UBakeTransformToolBuilder&&); \
	NO_API UBakeTransformToolBuilder(const UBakeTransformToolBuilder&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_29_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBakeTransformToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBakeTransformToolBuilder(UBakeTransformToolBuilder&&); \
	NO_API UBakeTransformToolBuilder(const UBakeTransformToolBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBakeTransformToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBakeTransformToolBuilder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBakeTransformToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_29_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_26_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_29_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_29_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_29_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_29_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_29_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_29_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_29_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_29_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_29_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_29_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_29_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_29_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UBakeTransformToolBuilder>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_54_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_54_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_54_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_54_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUBakeTransformToolProperties(); \
	friend struct Z_Construct_UClass_UBakeTransformToolProperties_Statics; \
public: \
	DECLARE_CLASS(UBakeTransformToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UBakeTransformToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_54_INCLASS \
private: \
	static void StaticRegisterNativesUBakeTransformToolProperties(); \
	friend struct Z_Construct_UClass_UBakeTransformToolProperties_Statics; \
public: \
	DECLARE_CLASS(UBakeTransformToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UBakeTransformToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_54_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBakeTransformToolProperties(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBakeTransformToolProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBakeTransformToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBakeTransformToolProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBakeTransformToolProperties(UBakeTransformToolProperties&&); \
	NO_API UBakeTransformToolProperties(const UBakeTransformToolProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_54_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBakeTransformToolProperties(UBakeTransformToolProperties&&); \
	NO_API UBakeTransformToolProperties(const UBakeTransformToolProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBakeTransformToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBakeTransformToolProperties); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UBakeTransformToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_54_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_51_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_54_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_54_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_54_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_54_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_54_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_54_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_54_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_54_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_54_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_54_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_54_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_54_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UBakeTransformToolProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_80_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_80_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_80_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_80_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUBakeTransformTool(); \
	friend struct Z_Construct_UClass_UBakeTransformTool_Statics; \
public: \
	DECLARE_CLASS(UBakeTransformTool, UMultiSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UBakeTransformTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_80_INCLASS \
private: \
	static void StaticRegisterNativesUBakeTransformTool(); \
	friend struct Z_Construct_UClass_UBakeTransformTool_Statics; \
public: \
	DECLARE_CLASS(UBakeTransformTool, UMultiSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UBakeTransformTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_80_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBakeTransformTool(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBakeTransformTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBakeTransformTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBakeTransformTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBakeTransformTool(UBakeTransformTool&&); \
	NO_API UBakeTransformTool(const UBakeTransformTool&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_80_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBakeTransformTool(UBakeTransformTool&&); \
	NO_API UBakeTransformTool(const UBakeTransformTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBakeTransformTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBakeTransformTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UBakeTransformTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_80_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__BasicProperties() { return STRUCT_OFFSET(UBakeTransformTool, BasicProperties); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_77_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_80_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_80_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_80_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_80_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_80_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_80_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_80_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_80_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_80_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_80_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_80_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h_80_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UBakeTransformTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_BakeTransformTool_h


#define FOREACH_ENUM_EBAKESCALEMETHOD(op) \
	op(EBakeScaleMethod::BakeFullScale) \
	op(EBakeScaleMethod::BakeNonuniformScale) \
	op(EBakeScaleMethod::DoNotBakeScale) 

enum class EBakeScaleMethod : uint8;
template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EBakeScaleMethod>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
