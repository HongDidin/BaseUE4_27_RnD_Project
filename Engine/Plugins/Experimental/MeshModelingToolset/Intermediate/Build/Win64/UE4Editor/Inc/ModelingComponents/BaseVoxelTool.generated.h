// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MODELINGCOMPONENTS_BaseVoxelTool_generated_h
#error "BaseVoxelTool.generated.h already included, missing '#pragma once' in BaseVoxelTool.h"
#endif
#define MODELINGCOMPONENTS_BaseVoxelTool_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseVoxelTool_h_20_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseVoxelTool_h_20_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseVoxelTool_h_20_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseVoxelTool_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUBaseVoxelTool(); \
	friend struct Z_Construct_UClass_UBaseVoxelTool_Statics; \
public: \
	DECLARE_CLASS(UBaseVoxelTool, UBaseCreateFromSelectedTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UBaseVoxelTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseVoxelTool_h_20_INCLASS \
private: \
	static void StaticRegisterNativesUBaseVoxelTool(); \
	friend struct Z_Construct_UClass_UBaseVoxelTool_Statics; \
public: \
	DECLARE_CLASS(UBaseVoxelTool, UBaseCreateFromSelectedTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UBaseVoxelTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseVoxelTool_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBaseVoxelTool(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBaseVoxelTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBaseVoxelTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBaseVoxelTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBaseVoxelTool(UBaseVoxelTool&&); \
	NO_API UBaseVoxelTool(const UBaseVoxelTool&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseVoxelTool_h_20_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBaseVoxelTool() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBaseVoxelTool(UBaseVoxelTool&&); \
	NO_API UBaseVoxelTool(const UBaseVoxelTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBaseVoxelTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBaseVoxelTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UBaseVoxelTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseVoxelTool_h_20_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__VoxProperties() { return STRUCT_OFFSET(UBaseVoxelTool, VoxProperties); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseVoxelTool_h_17_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseVoxelTool_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseVoxelTool_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseVoxelTool_h_20_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseVoxelTool_h_20_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseVoxelTool_h_20_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseVoxelTool_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseVoxelTool_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseVoxelTool_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseVoxelTool_h_20_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseVoxelTool_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseVoxelTool_h_20_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseVoxelTool_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MODELINGCOMPONENTS_API UClass* StaticClass<class UBaseVoxelTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_BaseTools_BaseVoxelTool_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
