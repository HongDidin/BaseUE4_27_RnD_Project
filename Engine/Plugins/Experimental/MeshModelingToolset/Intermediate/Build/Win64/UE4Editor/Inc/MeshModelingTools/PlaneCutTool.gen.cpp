// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/PlaneCutTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePlaneCutTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPlaneCutToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPlaneCutToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UAcceptOutputProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UAcceptOutputProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPlaneCutToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPlaneCutToolProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPlaneCutOperatorFactory_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPlaneCutOperatorFactory();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPlaneCutTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPlaneCutTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UMultiSelectionTool();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FQuat();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UDynamicMeshReplacementChangeTarget_NoRegister();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UTransformGizmo_NoRegister();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UTransformProxy_NoRegister();
// End Cross Module References
	void UPlaneCutToolBuilder::StaticRegisterNativesUPlaneCutToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UPlaneCutToolBuilder_NoRegister()
	{
		return UPlaneCutToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UPlaneCutToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPlaneCutToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlaneCutToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "PlaneCutTool.h" },
		{ "ModuleRelativePath", "Public/PlaneCutTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPlaneCutToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPlaneCutToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPlaneCutToolBuilder_Statics::ClassParams = {
		&UPlaneCutToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UPlaneCutToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPlaneCutToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPlaneCutToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPlaneCutToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPlaneCutToolBuilder, 1508693944);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UPlaneCutToolBuilder>()
	{
		return UPlaneCutToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPlaneCutToolBuilder(Z_Construct_UClass_UPlaneCutToolBuilder, &UPlaneCutToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UPlaneCutToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPlaneCutToolBuilder);
	void UAcceptOutputProperties::StaticRegisterNativesUAcceptOutputProperties()
	{
	}
	UClass* Z_Construct_UClass_UAcceptOutputProperties_NoRegister()
	{
		return UAcceptOutputProperties::StaticClass();
	}
	struct Z_Construct_UClass_UAcceptOutputProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bExportSeparatedPiecesAsNewMeshAssets_MetaData[];
#endif
		static void NewProp_bExportSeparatedPiecesAsNewMeshAssets_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bExportSeparatedPiecesAsNewMeshAssets;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAcceptOutputProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAcceptOutputProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n* Properties controlling how changes are baked out to static meshes on tool accept\n*/" },
		{ "IncludePath", "PlaneCutTool.h" },
		{ "ModuleRelativePath", "Public/PlaneCutTool.h" },
		{ "ToolTip", "Properties controlling how changes are baked out to static meshes on tool accept" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAcceptOutputProperties_Statics::NewProp_bExportSeparatedPiecesAsNewMeshAssets_MetaData[] = {
		{ "Category", "ToolOutputOptions" },
		{ "Comment", "/** If true, meshes cut into multiple pieces will be saved as separate assets on 'accept'. */" },
		{ "ModuleRelativePath", "Public/PlaneCutTool.h" },
		{ "ToolTip", "If true, meshes cut into multiple pieces will be saved as separate assets on 'accept'." },
	};
#endif
	void Z_Construct_UClass_UAcceptOutputProperties_Statics::NewProp_bExportSeparatedPiecesAsNewMeshAssets_SetBit(void* Obj)
	{
		((UAcceptOutputProperties*)Obj)->bExportSeparatedPiecesAsNewMeshAssets = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAcceptOutputProperties_Statics::NewProp_bExportSeparatedPiecesAsNewMeshAssets = { "bExportSeparatedPiecesAsNewMeshAssets", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UAcceptOutputProperties), &Z_Construct_UClass_UAcceptOutputProperties_Statics::NewProp_bExportSeparatedPiecesAsNewMeshAssets_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAcceptOutputProperties_Statics::NewProp_bExportSeparatedPiecesAsNewMeshAssets_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAcceptOutputProperties_Statics::NewProp_bExportSeparatedPiecesAsNewMeshAssets_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAcceptOutputProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAcceptOutputProperties_Statics::NewProp_bExportSeparatedPiecesAsNewMeshAssets,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAcceptOutputProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAcceptOutputProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAcceptOutputProperties_Statics::ClassParams = {
		&UAcceptOutputProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UAcceptOutputProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UAcceptOutputProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UAcceptOutputProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAcceptOutputProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAcceptOutputProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAcceptOutputProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAcceptOutputProperties, 2523240667);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UAcceptOutputProperties>()
	{
		return UAcceptOutputProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAcceptOutputProperties(Z_Construct_UClass_UAcceptOutputProperties, &UAcceptOutputProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UAcceptOutputProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAcceptOutputProperties);
	void UPlaneCutToolProperties::StaticRegisterNativesUPlaneCutToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UPlaneCutToolProperties_NoRegister()
	{
		return UPlaneCutToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UPlaneCutToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSnapToWorldGrid_MetaData[];
#endif
		static void NewProp_bSnapToWorldGrid_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSnapToWorldGrid;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bKeepBothHalves_MetaData[];
#endif
		static void NewProp_bKeepBothHalves_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bKeepBothHalves;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpacingBetweenHalves_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SpacingBetweenHalves;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bFillCutHole_MetaData[];
#endif
		static void NewProp_bFillCutHole_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFillCutHole;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowPreview_MetaData[];
#endif
		static void NewProp_bShowPreview_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowPreview;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bFillSpans_MetaData[];
#endif
		static void NewProp_bFillSpans_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFillSpans;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPlaneCutToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlaneCutToolProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Standard properties of the plane cut operation\n */" },
		{ "IncludePath", "PlaneCutTool.h" },
		{ "ModuleRelativePath", "Public/PlaneCutTool.h" },
		{ "ToolTip", "Standard properties of the plane cut operation" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlaneCutToolProperties_Statics::NewProp_bSnapToWorldGrid_MetaData[] = {
		{ "Category", "Snapping" },
		{ "Comment", "/** Snap the cut plane to the world grid */" },
		{ "ModuleRelativePath", "Public/PlaneCutTool.h" },
		{ "ToolTip", "Snap the cut plane to the world grid" },
	};
#endif
	void Z_Construct_UClass_UPlaneCutToolProperties_Statics::NewProp_bSnapToWorldGrid_SetBit(void* Obj)
	{
		((UPlaneCutToolProperties*)Obj)->bSnapToWorldGrid = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPlaneCutToolProperties_Statics::NewProp_bSnapToWorldGrid = { "bSnapToWorldGrid", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPlaneCutToolProperties), &Z_Construct_UClass_UPlaneCutToolProperties_Statics::NewProp_bSnapToWorldGrid_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPlaneCutToolProperties_Statics::NewProp_bSnapToWorldGrid_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlaneCutToolProperties_Statics::NewProp_bSnapToWorldGrid_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlaneCutToolProperties_Statics::NewProp_bKeepBothHalves_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** If true, both halves of the cut are computed */" },
		{ "ModuleRelativePath", "Public/PlaneCutTool.h" },
		{ "ToolTip", "If true, both halves of the cut are computed" },
	};
#endif
	void Z_Construct_UClass_UPlaneCutToolProperties_Statics::NewProp_bKeepBothHalves_SetBit(void* Obj)
	{
		((UPlaneCutToolProperties*)Obj)->bKeepBothHalves = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPlaneCutToolProperties_Statics::NewProp_bKeepBothHalves = { "bKeepBothHalves", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPlaneCutToolProperties), &Z_Construct_UClass_UPlaneCutToolProperties_Statics::NewProp_bKeepBothHalves_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPlaneCutToolProperties_Statics::NewProp_bKeepBothHalves_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlaneCutToolProperties_Statics::NewProp_bKeepBothHalves_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlaneCutToolProperties_Statics::NewProp_SpacingBetweenHalves_MetaData[] = {
		{ "Category", "Options" },
		{ "ClampMin", "0" },
		{ "Comment", "/** If keeping both halves, separate the two pieces by this amount */" },
		{ "EditCondition", "bKeepBothHalves == true" },
		{ "ModuleRelativePath", "Public/PlaneCutTool.h" },
		{ "ToolTip", "If keeping both halves, separate the two pieces by this amount" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UPlaneCutToolProperties_Statics::NewProp_SpacingBetweenHalves = { "SpacingBetweenHalves", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPlaneCutToolProperties, SpacingBetweenHalves), METADATA_PARAMS(Z_Construct_UClass_UPlaneCutToolProperties_Statics::NewProp_SpacingBetweenHalves_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlaneCutToolProperties_Statics::NewProp_SpacingBetweenHalves_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlaneCutToolProperties_Statics::NewProp_bFillCutHole_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** If true, the cut surface is filled with simple planar hole fill surface(s) */" },
		{ "ModuleRelativePath", "Public/PlaneCutTool.h" },
		{ "ToolTip", "If true, the cut surface is filled with simple planar hole fill surface(s)" },
	};
#endif
	void Z_Construct_UClass_UPlaneCutToolProperties_Statics::NewProp_bFillCutHole_SetBit(void* Obj)
	{
		((UPlaneCutToolProperties*)Obj)->bFillCutHole = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPlaneCutToolProperties_Statics::NewProp_bFillCutHole = { "bFillCutHole", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPlaneCutToolProperties), &Z_Construct_UClass_UPlaneCutToolProperties_Statics::NewProp_bFillCutHole_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPlaneCutToolProperties_Statics::NewProp_bFillCutHole_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlaneCutToolProperties_Statics::NewProp_bFillCutHole_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlaneCutToolProperties_Statics::NewProp_bShowPreview_MetaData[] = {
		{ "Category", "Options" },
		{ "ModuleRelativePath", "Public/PlaneCutTool.h" },
	};
#endif
	void Z_Construct_UClass_UPlaneCutToolProperties_Statics::NewProp_bShowPreview_SetBit(void* Obj)
	{
		((UPlaneCutToolProperties*)Obj)->bShowPreview = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPlaneCutToolProperties_Statics::NewProp_bShowPreview = { "bShowPreview", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPlaneCutToolProperties), &Z_Construct_UClass_UPlaneCutToolProperties_Statics::NewProp_bShowPreview_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPlaneCutToolProperties_Statics::NewProp_bShowPreview_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlaneCutToolProperties_Statics::NewProp_bShowPreview_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlaneCutToolProperties_Statics::NewProp_bFillSpans_MetaData[] = {
		{ "Category", "Options" },
		{ "ModuleRelativePath", "Public/PlaneCutTool.h" },
	};
#endif
	void Z_Construct_UClass_UPlaneCutToolProperties_Statics::NewProp_bFillSpans_SetBit(void* Obj)
	{
		((UPlaneCutToolProperties*)Obj)->bFillSpans = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPlaneCutToolProperties_Statics::NewProp_bFillSpans = { "bFillSpans", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPlaneCutToolProperties), &Z_Construct_UClass_UPlaneCutToolProperties_Statics::NewProp_bFillSpans_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPlaneCutToolProperties_Statics::NewProp_bFillSpans_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlaneCutToolProperties_Statics::NewProp_bFillSpans_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPlaneCutToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlaneCutToolProperties_Statics::NewProp_bSnapToWorldGrid,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlaneCutToolProperties_Statics::NewProp_bKeepBothHalves,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlaneCutToolProperties_Statics::NewProp_SpacingBetweenHalves,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlaneCutToolProperties_Statics::NewProp_bFillCutHole,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlaneCutToolProperties_Statics::NewProp_bShowPreview,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlaneCutToolProperties_Statics::NewProp_bFillSpans,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPlaneCutToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPlaneCutToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPlaneCutToolProperties_Statics::ClassParams = {
		&UPlaneCutToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UPlaneCutToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UPlaneCutToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UPlaneCutToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPlaneCutToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPlaneCutToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPlaneCutToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPlaneCutToolProperties, 3925582382);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UPlaneCutToolProperties>()
	{
		return UPlaneCutToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPlaneCutToolProperties(Z_Construct_UClass_UPlaneCutToolProperties, &UPlaneCutToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UPlaneCutToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPlaneCutToolProperties);
	void UPlaneCutOperatorFactory::StaticRegisterNativesUPlaneCutOperatorFactory()
	{
	}
	UClass* Z_Construct_UClass_UPlaneCutOperatorFactory_NoRegister()
	{
		return UPlaneCutOperatorFactory::StaticClass();
	}
	struct Z_Construct_UClass_UPlaneCutOperatorFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CutTool_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CutTool;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPlaneCutOperatorFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlaneCutOperatorFactory_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "PlaneCutTool.h" },
		{ "ModuleRelativePath", "Public/PlaneCutTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlaneCutOperatorFactory_Statics::NewProp_CutTool_MetaData[] = {
		{ "ModuleRelativePath", "Public/PlaneCutTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPlaneCutOperatorFactory_Statics::NewProp_CutTool = { "CutTool", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPlaneCutOperatorFactory, CutTool), Z_Construct_UClass_UPlaneCutTool_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPlaneCutOperatorFactory_Statics::NewProp_CutTool_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlaneCutOperatorFactory_Statics::NewProp_CutTool_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPlaneCutOperatorFactory_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlaneCutOperatorFactory_Statics::NewProp_CutTool,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPlaneCutOperatorFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPlaneCutOperatorFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPlaneCutOperatorFactory_Statics::ClassParams = {
		&UPlaneCutOperatorFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UPlaneCutOperatorFactory_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UPlaneCutOperatorFactory_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UPlaneCutOperatorFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPlaneCutOperatorFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPlaneCutOperatorFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPlaneCutOperatorFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPlaneCutOperatorFactory, 338221884);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UPlaneCutOperatorFactory>()
	{
		return UPlaneCutOperatorFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPlaneCutOperatorFactory(Z_Construct_UClass_UPlaneCutOperatorFactory, &UPlaneCutOperatorFactory::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UPlaneCutOperatorFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPlaneCutOperatorFactory);
	DEFINE_FUNCTION(UPlaneCutTool::execCut)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Cut();
		P_NATIVE_END;
	}
	void UPlaneCutTool::StaticRegisterNativesUPlaneCutTool()
	{
		UClass* Class = UPlaneCutTool::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Cut", &UPlaneCutTool::execCut },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UPlaneCutTool_Cut_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPlaneCutTool_Cut_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "Actions" },
		{ "Comment", "/** Cut with the current plane without exiting the tool */" },
		{ "DisplayName", "Cut" },
		{ "ModuleRelativePath", "Public/PlaneCutTool.h" },
		{ "ToolTip", "Cut with the current plane without exiting the tool" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPlaneCutTool_Cut_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPlaneCutTool, nullptr, "Cut", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPlaneCutTool_Cut_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPlaneCutTool_Cut_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPlaneCutTool_Cut()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPlaneCutTool_Cut_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UPlaneCutTool_NoRegister()
	{
		return UPlaneCutTool::StaticClass();
	}
	struct Z_Construct_UClass_UPlaneCutTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BasicProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BasicProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AcceptProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AcceptProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CutPlaneOrigin_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CutPlaneOrigin;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CutPlaneOrientation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CutPlaneOrientation;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Previews_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Previews_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Previews;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MeshesToCut_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshesToCut_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_MeshesToCut;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlaneTransformGizmo_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PlaneTransformGizmo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlaneTransformProxy_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PlaneTransformProxy;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPlaneCutTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMultiSelectionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UPlaneCutTool_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UPlaneCutTool_Cut, "Cut" }, // 521087677
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlaneCutTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Simple Mesh Plane Cutting Tool\n */" },
		{ "IncludePath", "PlaneCutTool.h" },
		{ "ModuleRelativePath", "Public/PlaneCutTool.h" },
		{ "ToolTip", "Simple Mesh Plane Cutting Tool" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_BasicProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/PlaneCutTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_BasicProperties = { "BasicProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPlaneCutTool, BasicProperties), Z_Construct_UClass_UPlaneCutToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_BasicProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_BasicProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_AcceptProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/PlaneCutTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_AcceptProperties = { "AcceptProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPlaneCutTool, AcceptProperties), Z_Construct_UClass_UAcceptOutputProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_AcceptProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_AcceptProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_CutPlaneOrigin_MetaData[] = {
		{ "Comment", "/** Origin of cutting plane */" },
		{ "ModuleRelativePath", "Public/PlaneCutTool.h" },
		{ "ToolTip", "Origin of cutting plane" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_CutPlaneOrigin = { "CutPlaneOrigin", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPlaneCutTool, CutPlaneOrigin), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_CutPlaneOrigin_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_CutPlaneOrigin_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_CutPlaneOrientation_MetaData[] = {
		{ "Comment", "/** Orientation of cutting plane */" },
		{ "ModuleRelativePath", "Public/PlaneCutTool.h" },
		{ "ToolTip", "Orientation of cutting plane" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_CutPlaneOrientation = { "CutPlaneOrientation", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPlaneCutTool, CutPlaneOrientation), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_CutPlaneOrientation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_CutPlaneOrientation_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_Previews_Inner = { "Previews", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_Previews_MetaData[] = {
		{ "ModuleRelativePath", "Public/PlaneCutTool.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_Previews = { "Previews", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPlaneCutTool, Previews), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_Previews_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_Previews_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_MeshesToCut_Inner = { "MeshesToCut", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UDynamicMeshReplacementChangeTarget_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_MeshesToCut_MetaData[] = {
		{ "ModuleRelativePath", "Public/PlaneCutTool.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_MeshesToCut = { "MeshesToCut", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPlaneCutTool, MeshesToCut), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_MeshesToCut_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_MeshesToCut_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_PlaneTransformGizmo_MetaData[] = {
		{ "Comment", "// toggled by hotkey (shift)\n" },
		{ "ModuleRelativePath", "Public/PlaneCutTool.h" },
		{ "ToolTip", "toggled by hotkey (shift)" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_PlaneTransformGizmo = { "PlaneTransformGizmo", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPlaneCutTool, PlaneTransformGizmo), Z_Construct_UClass_UTransformGizmo_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_PlaneTransformGizmo_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_PlaneTransformGizmo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_PlaneTransformProxy_MetaData[] = {
		{ "ModuleRelativePath", "Public/PlaneCutTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_PlaneTransformProxy = { "PlaneTransformProxy", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPlaneCutTool, PlaneTransformProxy), Z_Construct_UClass_UTransformProxy_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_PlaneTransformProxy_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_PlaneTransformProxy_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPlaneCutTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_BasicProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_AcceptProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_CutPlaneOrigin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_CutPlaneOrientation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_Previews_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_Previews,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_MeshesToCut_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_MeshesToCut,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_PlaneTransformGizmo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlaneCutTool_Statics::NewProp_PlaneTransformProxy,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPlaneCutTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPlaneCutTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPlaneCutTool_Statics::ClassParams = {
		&UPlaneCutTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UPlaneCutTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UPlaneCutTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UPlaneCutTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPlaneCutTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPlaneCutTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPlaneCutTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPlaneCutTool, 1404635295);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UPlaneCutTool>()
	{
		return UPlaneCutTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPlaneCutTool(Z_Construct_UClass_UPlaneCutTool, &UPlaneCutTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UPlaneCutTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPlaneCutTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
