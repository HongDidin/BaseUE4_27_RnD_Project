// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/TransformMeshesTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTransformMeshesTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_ETransformMeshesSnapDragRotationMode();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_ETransformMeshesSnapDragSource();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_ETransformMeshesTransformMode();
	MESHMODELINGTOOLS_API UScriptStruct* Z_Construct_UScriptStruct_FTransformMeshesTarget();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UTransformProxy_NoRegister();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UTransformGizmo_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UTransformMeshesToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UTransformMeshesToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UTransformMeshesToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UTransformMeshesToolProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UTransformMeshesTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UTransformMeshesTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UMultiSelectionTool();
// End Cross Module References
	static UEnum* ETransformMeshesSnapDragRotationMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_ETransformMeshesSnapDragRotationMode, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("ETransformMeshesSnapDragRotationMode"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<ETransformMeshesSnapDragRotationMode>()
	{
		return ETransformMeshesSnapDragRotationMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETransformMeshesSnapDragRotationMode(ETransformMeshesSnapDragRotationMode_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("ETransformMeshesSnapDragRotationMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_ETransformMeshesSnapDragRotationMode_Hash() { return 4176897457U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_ETransformMeshesSnapDragRotationMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETransformMeshesSnapDragRotationMode"), 0, Get_Z_Construct_UEnum_MeshModelingTools_ETransformMeshesSnapDragRotationMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETransformMeshesSnapDragRotationMode::Ignore", (int64)ETransformMeshesSnapDragRotationMode::Ignore },
				{ "ETransformMeshesSnapDragRotationMode::Align", (int64)ETransformMeshesSnapDragRotationMode::Align },
				{ "ETransformMeshesSnapDragRotationMode::AlignFlipped", (int64)ETransformMeshesSnapDragRotationMode::AlignFlipped },
				{ "ETransformMeshesSnapDragRotationMode::LastValue", (int64)ETransformMeshesSnapDragRotationMode::LastValue },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Align.Comment", "/** Snap-Drag aligns the Source and Target Normals to point in the same direction */" },
				{ "Align.DisplayName", "Align" },
				{ "Align.Name", "ETransformMeshesSnapDragRotationMode::Align" },
				{ "Align.ToolTip", "Snap-Drag aligns the Source and Target Normals to point in the same direction" },
				{ "AlignFlipped.Comment", "/** Snap-Drag aligns the Source Normal to the opposite of the Target Normal direction */" },
				{ "AlignFlipped.DisplayName", "Align Flipped" },
				{ "AlignFlipped.Name", "ETransformMeshesSnapDragRotationMode::AlignFlipped" },
				{ "AlignFlipped.ToolTip", "Snap-Drag aligns the Source Normal to the opposite of the Target Normal direction" },
				{ "Comment", "/** Snap-Drag Rotation Mode */" },
				{ "Ignore.Comment", "/** Snap-Drag only translates, ignoring Normals */" },
				{ "Ignore.DisplayName", "Ignore" },
				{ "Ignore.Name", "ETransformMeshesSnapDragRotationMode::Ignore" },
				{ "Ignore.ToolTip", "Snap-Drag only translates, ignoring Normals" },
				{ "LastValue.Hidden", "" },
				{ "LastValue.Name", "ETransformMeshesSnapDragRotationMode::LastValue" },
				{ "ModuleRelativePath", "Public/TransformMeshesTool.h" },
				{ "ToolTip", "Snap-Drag Rotation Mode" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"ETransformMeshesSnapDragRotationMode",
				"ETransformMeshesSnapDragRotationMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ETransformMeshesSnapDragSource_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_ETransformMeshesSnapDragSource, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("ETransformMeshesSnapDragSource"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<ETransformMeshesSnapDragSource>()
	{
		return ETransformMeshesSnapDragSource_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETransformMeshesSnapDragSource(ETransformMeshesSnapDragSource_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("ETransformMeshesSnapDragSource"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_ETransformMeshesSnapDragSource_Hash() { return 1670118404U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_ETransformMeshesSnapDragSource()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETransformMeshesSnapDragSource"), 0, Get_Z_Construct_UEnum_MeshModelingTools_ETransformMeshesSnapDragSource_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETransformMeshesSnapDragSource::ClickPoint", (int64)ETransformMeshesSnapDragSource::ClickPoint },
				{ "ETransformMeshesSnapDragSource::Pivot", (int64)ETransformMeshesSnapDragSource::Pivot },
				{ "ETransformMeshesSnapDragSource::LastValue", (int64)ETransformMeshesSnapDragSource::LastValue },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ClickPoint.Comment", "/** Snap-Drag moves the Clicked Point to the Target Location */" },
				{ "ClickPoint.DisplayName", "Click Point" },
				{ "ClickPoint.Name", "ETransformMeshesSnapDragSource::ClickPoint" },
				{ "ClickPoint.ToolTip", "Snap-Drag moves the Clicked Point to the Target Location" },
				{ "Comment", "/** Snap-Drag Source Point */" },
				{ "LastValue.Hidden", "" },
				{ "LastValue.Name", "ETransformMeshesSnapDragSource::LastValue" },
				{ "ModuleRelativePath", "Public/TransformMeshesTool.h" },
				{ "Pivot.Comment", "/** Snap-Drag moves the Gizmo/Pivot to the Target Location */" },
				{ "Pivot.DisplayName", "Pivot" },
				{ "Pivot.Name", "ETransformMeshesSnapDragSource::Pivot" },
				{ "Pivot.ToolTip", "Snap-Drag moves the Gizmo/Pivot to the Target Location" },
				{ "ToolTip", "Snap-Drag Source Point" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"ETransformMeshesSnapDragSource",
				"ETransformMeshesSnapDragSource",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ETransformMeshesTransformMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_ETransformMeshesTransformMode, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("ETransformMeshesTransformMode"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<ETransformMeshesTransformMode>()
	{
		return ETransformMeshesTransformMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETransformMeshesTransformMode(ETransformMeshesTransformMode_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("ETransformMeshesTransformMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_ETransformMeshesTransformMode_Hash() { return 3663909408U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_ETransformMeshesTransformMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETransformMeshesTransformMode"), 0, Get_Z_Construct_UEnum_MeshModelingTools_ETransformMeshesTransformMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETransformMeshesTransformMode::SharedGizmo", (int64)ETransformMeshesTransformMode::SharedGizmo },
				{ "ETransformMeshesTransformMode::SharedGizmoLocal", (int64)ETransformMeshesTransformMode::SharedGizmoLocal },
				{ "ETransformMeshesTransformMode::PerObjectGizmo", (int64)ETransformMeshesTransformMode::PerObjectGizmo },
				{ "ETransformMeshesTransformMode::LastValue", (int64)ETransformMeshesTransformMode::LastValue },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** Mesh Transform modes */" },
				{ "LastValue.Hidden", "" },
				{ "LastValue.Name", "ETransformMeshesTransformMode::LastValue" },
				{ "ModuleRelativePath", "Public/TransformMeshesTool.h" },
				{ "PerObjectGizmo.Comment", "/** Separate Gizmo for each Object */" },
				{ "PerObjectGizmo.DisplayName", "Multi-Gizmo" },
				{ "PerObjectGizmo.Name", "ETransformMeshesTransformMode::PerObjectGizmo" },
				{ "PerObjectGizmo.ToolTip", "Separate Gizmo for each Object" },
				{ "SharedGizmo.Comment", "/** Single Gizmo for all Objects */" },
				{ "SharedGizmo.DisplayName", "Shared Gizmo" },
				{ "SharedGizmo.Name", "ETransformMeshesTransformMode::SharedGizmo" },
				{ "SharedGizmo.ToolTip", "Single Gizmo for all Objects" },
				{ "SharedGizmoLocal.Comment", "/** Single Gizmo for all Objects, Rotations applied per-Object */" },
				{ "SharedGizmoLocal.DisplayName", "Shared Gizmo (Local)" },
				{ "SharedGizmoLocal.Name", "ETransformMeshesTransformMode::SharedGizmoLocal" },
				{ "SharedGizmoLocal.ToolTip", "Single Gizmo for all Objects, Rotations applied per-Object" },
				{ "ToolTip", "Mesh Transform modes" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"ETransformMeshesTransformMode",
				"ETransformMeshesTransformMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FTransformMeshesTarget::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MESHMODELINGTOOLS_API uint32 Get_Z_Construct_UScriptStruct_FTransformMeshesTarget_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FTransformMeshesTarget, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("TransformMeshesTarget"), sizeof(FTransformMeshesTarget), Get_Z_Construct_UScriptStruct_FTransformMeshesTarget_Hash());
	}
	return Singleton;
}
template<> MESHMODELINGTOOLS_API UScriptStruct* StaticStruct<FTransformMeshesTarget>()
{
	return FTransformMeshesTarget::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FTransformMeshesTarget(FTransformMeshesTarget::StaticStruct, TEXT("/Script/MeshModelingTools"), TEXT("TransformMeshesTarget"), false, nullptr, nullptr);
static struct FScriptStruct_MeshModelingTools_StaticRegisterNativesFTransformMeshesTarget
{
	FScriptStruct_MeshModelingTools_StaticRegisterNativesFTransformMeshesTarget()
	{
		UScriptStruct::DeferCppStructOps<FTransformMeshesTarget>(FName(TEXT("TransformMeshesTarget")));
	}
} ScriptStruct_MeshModelingTools_StaticRegisterNativesFTransformMeshesTarget;
	struct Z_Construct_UScriptStruct_FTransformMeshesTarget_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransformProxy_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TransformProxy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransformGizmo_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TransformGizmo;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTransformMeshesTarget_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/TransformMeshesTool.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FTransformMeshesTarget_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FTransformMeshesTarget>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTransformMeshesTarget_Statics::NewProp_TransformProxy_MetaData[] = {
		{ "ModuleRelativePath", "Public/TransformMeshesTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FTransformMeshesTarget_Statics::NewProp_TransformProxy = { "TransformProxy", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTransformMeshesTarget, TransformProxy), Z_Construct_UClass_UTransformProxy_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FTransformMeshesTarget_Statics::NewProp_TransformProxy_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTransformMeshesTarget_Statics::NewProp_TransformProxy_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTransformMeshesTarget_Statics::NewProp_TransformGizmo_MetaData[] = {
		{ "ModuleRelativePath", "Public/TransformMeshesTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FTransformMeshesTarget_Statics::NewProp_TransformGizmo = { "TransformGizmo", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTransformMeshesTarget, TransformGizmo), Z_Construct_UClass_UTransformGizmo_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FTransformMeshesTarget_Statics::NewProp_TransformGizmo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTransformMeshesTarget_Statics::NewProp_TransformGizmo_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FTransformMeshesTarget_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTransformMeshesTarget_Statics::NewProp_TransformProxy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTransformMeshesTarget_Statics::NewProp_TransformGizmo,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FTransformMeshesTarget_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
		nullptr,
		&NewStructOps,
		"TransformMeshesTarget",
		sizeof(FTransformMeshesTarget),
		alignof(FTransformMeshesTarget),
		Z_Construct_UScriptStruct_FTransformMeshesTarget_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTransformMeshesTarget_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FTransformMeshesTarget_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTransformMeshesTarget_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FTransformMeshesTarget()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FTransformMeshesTarget_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("TransformMeshesTarget"), sizeof(FTransformMeshesTarget), Get_Z_Construct_UScriptStruct_FTransformMeshesTarget_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FTransformMeshesTarget_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FTransformMeshesTarget_Hash() { return 2471255921U; }
	void UTransformMeshesToolBuilder::StaticRegisterNativesUTransformMeshesToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UTransformMeshesToolBuilder_NoRegister()
	{
		return UTransformMeshesToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UTransformMeshesToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTransformMeshesToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTransformMeshesToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "TransformMeshesTool.h" },
		{ "ModuleRelativePath", "Public/TransformMeshesTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTransformMeshesToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTransformMeshesToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTransformMeshesToolBuilder_Statics::ClassParams = {
		&UTransformMeshesToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UTransformMeshesToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTransformMeshesToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTransformMeshesToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTransformMeshesToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTransformMeshesToolBuilder, 3089344140);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UTransformMeshesToolBuilder>()
	{
		return UTransformMeshesToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTransformMeshesToolBuilder(Z_Construct_UClass_UTransformMeshesToolBuilder, &UTransformMeshesToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UTransformMeshesToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTransformMeshesToolBuilder);
	void UTransformMeshesToolProperties::StaticRegisterNativesUTransformMeshesToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UTransformMeshesToolProperties_NoRegister()
	{
		return UTransformMeshesToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UTransformMeshesToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TransformMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransformMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TransformMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSetPivot_MetaData[];
#endif
		static void NewProp_bSetPivot_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSetPivot;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableSnapDragging_MetaData[];
#endif
		static void NewProp_bEnableSnapDragging_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableSnapDragging;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_SnapDragSource_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SnapDragSource_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_SnapDragSource;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_RotationMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RotationMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_RotationMode;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTransformMeshesToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTransformMeshesToolProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Standard properties of the Transform Meshes operation\n */" },
		{ "IncludePath", "TransformMeshesTool.h" },
		{ "ModuleRelativePath", "Public/TransformMeshesTool.h" },
		{ "ToolTip", "Standard properties of the Transform Meshes operation" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UTransformMeshesToolProperties_Statics::NewProp_TransformMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTransformMeshesToolProperties_Statics::NewProp_TransformMode_MetaData[] = {
		{ "Category", "Options" },
		{ "ModuleRelativePath", "Public/TransformMeshesTool.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UTransformMeshesToolProperties_Statics::NewProp_TransformMode = { "TransformMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTransformMeshesToolProperties, TransformMode), Z_Construct_UEnum_MeshModelingTools_ETransformMeshesTransformMode, METADATA_PARAMS(Z_Construct_UClass_UTransformMeshesToolProperties_Statics::NewProp_TransformMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTransformMeshesToolProperties_Statics::NewProp_TransformMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTransformMeshesToolProperties_Statics::NewProp_bSetPivot_MetaData[] = {
		{ "Category", "Options" },
		{ "EditCondition", "TransformMode == ETransformMeshesTransformMode::SharedGizmo || TransformMode == ETransformMeshesTransformMode::PerObjectGizmo" },
		{ "ModuleRelativePath", "Public/TransformMeshesTool.h" },
	};
#endif
	void Z_Construct_UClass_UTransformMeshesToolProperties_Statics::NewProp_bSetPivot_SetBit(void* Obj)
	{
		((UTransformMeshesToolProperties*)Obj)->bSetPivot = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UTransformMeshesToolProperties_Statics::NewProp_bSetPivot = { "bSetPivot", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UTransformMeshesToolProperties), &Z_Construct_UClass_UTransformMeshesToolProperties_Statics::NewProp_bSetPivot_SetBit, METADATA_PARAMS(Z_Construct_UClass_UTransformMeshesToolProperties_Statics::NewProp_bSetPivot_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTransformMeshesToolProperties_Statics::NewProp_bSetPivot_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTransformMeshesToolProperties_Statics::NewProp_bEnableSnapDragging_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Click-drag starting on the target objects to reposition them on the rest of the scene */" },
		{ "ModuleRelativePath", "Public/TransformMeshesTool.h" },
		{ "ToolTip", "Click-drag starting on the target objects to reposition them on the rest of the scene" },
	};
#endif
	void Z_Construct_UClass_UTransformMeshesToolProperties_Statics::NewProp_bEnableSnapDragging_SetBit(void* Obj)
	{
		((UTransformMeshesToolProperties*)Obj)->bEnableSnapDragging = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UTransformMeshesToolProperties_Statics::NewProp_bEnableSnapDragging = { "bEnableSnapDragging", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UTransformMeshesToolProperties), &Z_Construct_UClass_UTransformMeshesToolProperties_Statics::NewProp_bEnableSnapDragging_SetBit, METADATA_PARAMS(Z_Construct_UClass_UTransformMeshesToolProperties_Statics::NewProp_bEnableSnapDragging_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTransformMeshesToolProperties_Statics::NewProp_bEnableSnapDragging_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UTransformMeshesToolProperties_Statics::NewProp_SnapDragSource_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTransformMeshesToolProperties_Statics::NewProp_SnapDragSource_MetaData[] = {
		{ "Category", "Options" },
		{ "EditCondition", "bEnableSnapDragging == true" },
		{ "ModuleRelativePath", "Public/TransformMeshesTool.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UTransformMeshesToolProperties_Statics::NewProp_SnapDragSource = { "SnapDragSource", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTransformMeshesToolProperties, SnapDragSource), Z_Construct_UEnum_MeshModelingTools_ETransformMeshesSnapDragSource, METADATA_PARAMS(Z_Construct_UClass_UTransformMeshesToolProperties_Statics::NewProp_SnapDragSource_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTransformMeshesToolProperties_Statics::NewProp_SnapDragSource_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UTransformMeshesToolProperties_Statics::NewProp_RotationMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTransformMeshesToolProperties_Statics::NewProp_RotationMode_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** When Snap-Dragging, align source and target normals */" },
		{ "EditCondition", "bEnableSnapDragging == true" },
		{ "ModuleRelativePath", "Public/TransformMeshesTool.h" },
		{ "ToolTip", "When Snap-Dragging, align source and target normals" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UTransformMeshesToolProperties_Statics::NewProp_RotationMode = { "RotationMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTransformMeshesToolProperties, RotationMode), Z_Construct_UEnum_MeshModelingTools_ETransformMeshesSnapDragRotationMode, METADATA_PARAMS(Z_Construct_UClass_UTransformMeshesToolProperties_Statics::NewProp_RotationMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTransformMeshesToolProperties_Statics::NewProp_RotationMode_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTransformMeshesToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTransformMeshesToolProperties_Statics::NewProp_TransformMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTransformMeshesToolProperties_Statics::NewProp_TransformMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTransformMeshesToolProperties_Statics::NewProp_bSetPivot,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTransformMeshesToolProperties_Statics::NewProp_bEnableSnapDragging,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTransformMeshesToolProperties_Statics::NewProp_SnapDragSource_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTransformMeshesToolProperties_Statics::NewProp_SnapDragSource,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTransformMeshesToolProperties_Statics::NewProp_RotationMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTransformMeshesToolProperties_Statics::NewProp_RotationMode,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTransformMeshesToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTransformMeshesToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTransformMeshesToolProperties_Statics::ClassParams = {
		&UTransformMeshesToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UTransformMeshesToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UTransformMeshesToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UTransformMeshesToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTransformMeshesToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTransformMeshesToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTransformMeshesToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTransformMeshesToolProperties, 2918667184);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UTransformMeshesToolProperties>()
	{
		return UTransformMeshesToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTransformMeshesToolProperties(Z_Construct_UClass_UTransformMeshesToolProperties, &UTransformMeshesToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UTransformMeshesToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTransformMeshesToolProperties);
	void UTransformMeshesTool::StaticRegisterNativesUTransformMeshesTool()
	{
	}
	UClass* Z_Construct_UClass_UTransformMeshesTool_NoRegister()
	{
		return UTransformMeshesTool::StaticClass();
	}
	struct Z_Construct_UClass_UTransformMeshesTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransformProps_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TransformProps;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ActiveGizmos_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActiveGizmos_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ActiveGizmos;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTransformMeshesTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMultiSelectionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTransformMeshesTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "TransformMeshesTool.h" },
		{ "ModuleRelativePath", "Public/TransformMeshesTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTransformMeshesTool_Statics::NewProp_TransformProps_MetaData[] = {
		{ "ModuleRelativePath", "Public/TransformMeshesTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UTransformMeshesTool_Statics::NewProp_TransformProps = { "TransformProps", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTransformMeshesTool, TransformProps), Z_Construct_UClass_UTransformMeshesToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UTransformMeshesTool_Statics::NewProp_TransformProps_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTransformMeshesTool_Statics::NewProp_TransformProps_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UTransformMeshesTool_Statics::NewProp_ActiveGizmos_Inner = { "ActiveGizmos", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FTransformMeshesTarget, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTransformMeshesTool_Statics::NewProp_ActiveGizmos_MetaData[] = {
		{ "ModuleRelativePath", "Public/TransformMeshesTool.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UTransformMeshesTool_Statics::NewProp_ActiveGizmos = { "ActiveGizmos", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTransformMeshesTool, ActiveGizmos), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UTransformMeshesTool_Statics::NewProp_ActiveGizmos_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTransformMeshesTool_Statics::NewProp_ActiveGizmos_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTransformMeshesTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTransformMeshesTool_Statics::NewProp_TransformProps,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTransformMeshesTool_Statics::NewProp_ActiveGizmos_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTransformMeshesTool_Statics::NewProp_ActiveGizmos,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTransformMeshesTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTransformMeshesTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTransformMeshesTool_Statics::ClassParams = {
		&UTransformMeshesTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UTransformMeshesTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UTransformMeshesTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UTransformMeshesTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTransformMeshesTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTransformMeshesTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTransformMeshesTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTransformMeshesTool, 2903876831);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UTransformMeshesTool>()
	{
		return UTransformMeshesTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTransformMeshesTool(Z_Construct_UClass_UTransformMeshesTool, &UTransformMeshesTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UTransformMeshesTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTransformMeshesTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
