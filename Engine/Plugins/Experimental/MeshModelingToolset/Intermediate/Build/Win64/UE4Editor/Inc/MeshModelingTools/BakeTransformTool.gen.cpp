// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/BakeTransformTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBakeTransformTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EBakeScaleMethod();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UBakeTransformToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UBakeTransformToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UBakeTransformToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UBakeTransformToolProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UBakeTransformTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UBakeTransformTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UMultiSelectionTool();
// End Cross Module References
	static UEnum* EBakeScaleMethod_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EBakeScaleMethod, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EBakeScaleMethod"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EBakeScaleMethod>()
	{
		return EBakeScaleMethod_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EBakeScaleMethod(EBakeScaleMethod_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EBakeScaleMethod"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EBakeScaleMethod_Hash() { return 1751433145U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EBakeScaleMethod()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EBakeScaleMethod"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EBakeScaleMethod_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EBakeScaleMethod::BakeFullScale", (int64)EBakeScaleMethod::BakeFullScale },
				{ "EBakeScaleMethod::BakeNonuniformScale", (int64)EBakeScaleMethod::BakeNonuniformScale },
				{ "EBakeScaleMethod::DoNotBakeScale", (int64)EBakeScaleMethod::DoNotBakeScale },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BakeFullScale.Name", "EBakeScaleMethod::BakeFullScale" },
				{ "BakeNonuniformScale.Comment", "// bake all scale information, so the component has scale of 1 on all axes\n" },
				{ "BakeNonuniformScale.Name", "EBakeScaleMethod::BakeNonuniformScale" },
				{ "BakeNonuniformScale.ToolTip", "bake all scale information, so the component has scale of 1 on all axes" },
				{ "DoNotBakeScale.Comment", "// bake the non-uniform scale, so the component has a uniform scale\n" },
				{ "DoNotBakeScale.Name", "EBakeScaleMethod::DoNotBakeScale" },
				{ "DoNotBakeScale.ToolTip", "bake the non-uniform scale, so the component has a uniform scale" },
				{ "ModuleRelativePath", "Public/BakeTransformTool.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EBakeScaleMethod",
				"EBakeScaleMethod",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UBakeTransformToolBuilder::StaticRegisterNativesUBakeTransformToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UBakeTransformToolBuilder_NoRegister()
	{
		return UBakeTransformToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UBakeTransformToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBakeTransformToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakeTransformToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "BakeTransformTool.h" },
		{ "ModuleRelativePath", "Public/BakeTransformTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBakeTransformToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBakeTransformToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBakeTransformToolBuilder_Statics::ClassParams = {
		&UBakeTransformToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UBakeTransformToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBakeTransformToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBakeTransformToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBakeTransformToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBakeTransformToolBuilder, 1495974119);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UBakeTransformToolBuilder>()
	{
		return UBakeTransformToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBakeTransformToolBuilder(Z_Construct_UClass_UBakeTransformToolBuilder, &UBakeTransformToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UBakeTransformToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBakeTransformToolBuilder);
	void UBakeTransformToolProperties::StaticRegisterNativesUBakeTransformToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UBakeTransformToolProperties_NoRegister()
	{
		return UBakeTransformToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UBakeTransformToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bBakeRotation_MetaData[];
#endif
		static void NewProp_bBakeRotation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bBakeRotation;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_BakeScale_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BakeScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_BakeScale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRecenterPivot_MetaData[];
#endif
		static void NewProp_bRecenterPivot_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRecenterPivot;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBakeTransformToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakeTransformToolProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Standard properties\n */" },
		{ "IncludePath", "BakeTransformTool.h" },
		{ "ModuleRelativePath", "Public/BakeTransformTool.h" },
		{ "ToolTip", "Standard properties" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakeTransformToolProperties_Statics::NewProp_bBakeRotation_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Bake rotation */" },
		{ "ModuleRelativePath", "Public/BakeTransformTool.h" },
		{ "ToolTip", "Bake rotation" },
	};
#endif
	void Z_Construct_UClass_UBakeTransformToolProperties_Statics::NewProp_bBakeRotation_SetBit(void* Obj)
	{
		((UBakeTransformToolProperties*)Obj)->bBakeRotation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UBakeTransformToolProperties_Statics::NewProp_bBakeRotation = { "bBakeRotation", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UBakeTransformToolProperties), &Z_Construct_UClass_UBakeTransformToolProperties_Statics::NewProp_bBakeRotation_SetBit, METADATA_PARAMS(Z_Construct_UClass_UBakeTransformToolProperties_Statics::NewProp_bBakeRotation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakeTransformToolProperties_Statics::NewProp_bBakeRotation_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UBakeTransformToolProperties_Statics::NewProp_BakeScale_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakeTransformToolProperties_Statics::NewProp_BakeScale_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Bake scale */" },
		{ "ModuleRelativePath", "Public/BakeTransformTool.h" },
		{ "ToolTip", "Bake scale" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UBakeTransformToolProperties_Statics::NewProp_BakeScale = { "BakeScale", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakeTransformToolProperties, BakeScale), Z_Construct_UEnum_MeshModelingTools_EBakeScaleMethod, METADATA_PARAMS(Z_Construct_UClass_UBakeTransformToolProperties_Statics::NewProp_BakeScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakeTransformToolProperties_Statics::NewProp_BakeScale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakeTransformToolProperties_Statics::NewProp_bRecenterPivot_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Recenter pivot after baking transform */" },
		{ "ModuleRelativePath", "Public/BakeTransformTool.h" },
		{ "ToolTip", "Recenter pivot after baking transform" },
	};
#endif
	void Z_Construct_UClass_UBakeTransformToolProperties_Statics::NewProp_bRecenterPivot_SetBit(void* Obj)
	{
		((UBakeTransformToolProperties*)Obj)->bRecenterPivot = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UBakeTransformToolProperties_Statics::NewProp_bRecenterPivot = { "bRecenterPivot", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UBakeTransformToolProperties), &Z_Construct_UClass_UBakeTransformToolProperties_Statics::NewProp_bRecenterPivot_SetBit, METADATA_PARAMS(Z_Construct_UClass_UBakeTransformToolProperties_Statics::NewProp_bRecenterPivot_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakeTransformToolProperties_Statics::NewProp_bRecenterPivot_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UBakeTransformToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakeTransformToolProperties_Statics::NewProp_bBakeRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakeTransformToolProperties_Statics::NewProp_BakeScale_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakeTransformToolProperties_Statics::NewProp_BakeScale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakeTransformToolProperties_Statics::NewProp_bRecenterPivot,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBakeTransformToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBakeTransformToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBakeTransformToolProperties_Statics::ClassParams = {
		&UBakeTransformToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UBakeTransformToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UBakeTransformToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UBakeTransformToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBakeTransformToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBakeTransformToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBakeTransformToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBakeTransformToolProperties, 10553256);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UBakeTransformToolProperties>()
	{
		return UBakeTransformToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBakeTransformToolProperties(Z_Construct_UClass_UBakeTransformToolProperties, &UBakeTransformToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UBakeTransformToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBakeTransformToolProperties);
	void UBakeTransformTool::StaticRegisterNativesUBakeTransformTool()
	{
	}
	UClass* Z_Construct_UClass_UBakeTransformTool_NoRegister()
	{
		return UBakeTransformTool::StaticClass();
	}
	struct Z_Construct_UClass_UBakeTransformTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BasicProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BasicProperties;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBakeTransformTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMultiSelectionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakeTransformTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Simple tool to bake scene transform on meshes into the mesh assets\n */" },
		{ "IncludePath", "BakeTransformTool.h" },
		{ "ModuleRelativePath", "Public/BakeTransformTool.h" },
		{ "ToolTip", "Simple tool to bake scene transform on meshes into the mesh assets" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakeTransformTool_Statics::NewProp_BasicProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/BakeTransformTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UBakeTransformTool_Statics::NewProp_BasicProperties = { "BasicProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakeTransformTool, BasicProperties), Z_Construct_UClass_UBakeTransformToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UBakeTransformTool_Statics::NewProp_BasicProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakeTransformTool_Statics::NewProp_BasicProperties_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UBakeTransformTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakeTransformTool_Statics::NewProp_BasicProperties,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBakeTransformTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBakeTransformTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBakeTransformTool_Statics::ClassParams = {
		&UBakeTransformTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UBakeTransformTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UBakeTransformTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UBakeTransformTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBakeTransformTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBakeTransformTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBakeTransformTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBakeTransformTool, 4041712080);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UBakeTransformTool>()
	{
		return UBakeTransformTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBakeTransformTool(Z_Construct_UClass_UBakeTransformTool, &UBakeTransformTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UBakeTransformTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBakeTransformTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
