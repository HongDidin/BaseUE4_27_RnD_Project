// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MODELINGCOMPONENTS_VoxelProperties_generated_h
#error "VoxelProperties.generated.h already included, missing '#pragma once' in VoxelProperties.h"
#endif
#define MODELINGCOMPONENTS_VoxelProperties_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_VoxelProperties_h_16_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_VoxelProperties_h_16_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_VoxelProperties_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_VoxelProperties_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVoxelProperties(); \
	friend struct Z_Construct_UClass_UVoxelProperties_Statics; \
public: \
	DECLARE_CLASS(UVoxelProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UVoxelProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_VoxelProperties_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUVoxelProperties(); \
	friend struct Z_Construct_UClass_UVoxelProperties_Statics; \
public: \
	DECLARE_CLASS(UVoxelProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UVoxelProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_VoxelProperties_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVoxelProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVoxelProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVoxelProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVoxelProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVoxelProperties(UVoxelProperties&&); \
	NO_API UVoxelProperties(const UVoxelProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_VoxelProperties_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVoxelProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVoxelProperties(UVoxelProperties&&); \
	NO_API UVoxelProperties(const UVoxelProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVoxelProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVoxelProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVoxelProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_VoxelProperties_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_VoxelProperties_h_13_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_VoxelProperties_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_VoxelProperties_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_VoxelProperties_h_16_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_VoxelProperties_h_16_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_VoxelProperties_h_16_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_VoxelProperties_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_VoxelProperties_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_VoxelProperties_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_VoxelProperties_h_16_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_VoxelProperties_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_VoxelProperties_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_VoxelProperties_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MODELINGCOMPONENTS_API UClass* StaticClass<class UVoxelProperties>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_VoxelProperties_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
