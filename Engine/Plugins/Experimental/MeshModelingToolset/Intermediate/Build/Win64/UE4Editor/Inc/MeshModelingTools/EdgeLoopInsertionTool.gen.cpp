// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/EdgeLoopInsertionTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEdgeLoopInsertionTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EEdgeLoopInsertionMode();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EEdgeLoopPositioningMode();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEdgeLoopInsertionToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEdgeLoopInsertionToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEdgeLoopInsertionProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEdgeLoopInsertionProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEdgeLoopInsertionOperatorFactory_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEdgeLoopInsertionOperatorFactory();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEdgeLoopInsertionTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEdgeLoopInsertionTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_USingleSelectionTool();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_NoRegister();
// End Cross Module References
	static UEnum* EEdgeLoopInsertionMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EEdgeLoopInsertionMode, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EEdgeLoopInsertionMode"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EEdgeLoopInsertionMode>()
	{
		return EEdgeLoopInsertionMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EEdgeLoopInsertionMode(EEdgeLoopInsertionMode_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EEdgeLoopInsertionMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EEdgeLoopInsertionMode_Hash() { return 1448558436U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EEdgeLoopInsertionMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EEdgeLoopInsertionMode"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EEdgeLoopInsertionMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EEdgeLoopInsertionMode::Retriangulate", (int64)EEdgeLoopInsertionMode::Retriangulate },
				{ "EEdgeLoopInsertionMode::PlaneCut", (int64)EEdgeLoopInsertionMode::PlaneCut },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/EdgeLoopInsertionTool.h" },
				{ "PlaneCut.Comment", "/** Keeps existing triangles and cuts them to create a new path. May result in fragmented triangles over time.*/" },
				{ "PlaneCut.Name", "EEdgeLoopInsertionMode::PlaneCut" },
				{ "PlaneCut.ToolTip", "Keeps existing triangles and cuts them to create a new path. May result in fragmented triangles over time." },
				{ "Retriangulate.Comment", "/** Existing groups will be deleted and new triangles will be created for the new groups. \n\x09 Keeps topology simple but breaks non-planar groups and loses the UV's. */" },
				{ "Retriangulate.Name", "EEdgeLoopInsertionMode::Retriangulate" },
				{ "Retriangulate.ToolTip", "Existing groups will be deleted and new triangles will be created for the new groups.\n       Keeps topology simple but breaks non-planar groups and loses the UV's." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EEdgeLoopInsertionMode",
				"EEdgeLoopInsertionMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EEdgeLoopPositioningMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EEdgeLoopPositioningMode, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EEdgeLoopPositioningMode"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EEdgeLoopPositioningMode>()
	{
		return EEdgeLoopPositioningMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EEdgeLoopPositioningMode(EEdgeLoopPositioningMode_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EEdgeLoopPositioningMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EEdgeLoopPositioningMode_Hash() { return 700551363U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EEdgeLoopPositioningMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EEdgeLoopPositioningMode"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EEdgeLoopPositioningMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EEdgeLoopPositioningMode::Even", (int64)EEdgeLoopPositioningMode::Even },
				{ "EEdgeLoopPositioningMode::ProportionOffset", (int64)EEdgeLoopPositioningMode::ProportionOffset },
				{ "EEdgeLoopPositioningMode::DistanceOffset", (int64)EEdgeLoopPositioningMode::DistanceOffset },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "DistanceOffset.Comment", "/** Edge loops will fall a constant distance away from the start of each edge they intersect \n\x09 (e.g., 20 units down). Clamps to end if edge is too short. */" },
				{ "DistanceOffset.Name", "EEdgeLoopPositioningMode::DistanceOffset" },
				{ "DistanceOffset.ToolTip", "Edge loops will fall a constant distance away from the start of each edge they intersect\n       (e.g., 20 units down). Clamps to end if edge is too short." },
				{ "Even.Comment", "/** Edge loops will be evenly centered within a group. Allows for multiple insertions at a time. */" },
				{ "Even.Name", "EEdgeLoopPositioningMode::Even" },
				{ "Even.ToolTip", "Edge loops will be evenly centered within a group. Allows for multiple insertions at a time." },
				{ "ModuleRelativePath", "Public/EdgeLoopInsertionTool.h" },
				{ "ProportionOffset.Comment", "/** Edge loops will fall at the same length proportion at each edge they intersect (e.g., a quarter way down). */" },
				{ "ProportionOffset.Name", "EEdgeLoopPositioningMode::ProportionOffset" },
				{ "ProportionOffset.ToolTip", "Edge loops will fall at the same length proportion at each edge they intersect (e.g., a quarter way down)." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EEdgeLoopPositioningMode",
				"EEdgeLoopPositioningMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UEdgeLoopInsertionToolBuilder::StaticRegisterNativesUEdgeLoopInsertionToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UEdgeLoopInsertionToolBuilder_NoRegister()
	{
		return UEdgeLoopInsertionToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UEdgeLoopInsertionToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEdgeLoopInsertionToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEdgeLoopInsertionToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "EdgeLoopInsertionTool.h" },
		{ "ModuleRelativePath", "Public/EdgeLoopInsertionTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEdgeLoopInsertionToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEdgeLoopInsertionToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEdgeLoopInsertionToolBuilder_Statics::ClassParams = {
		&UEdgeLoopInsertionToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UEdgeLoopInsertionToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEdgeLoopInsertionToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEdgeLoopInsertionToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEdgeLoopInsertionToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEdgeLoopInsertionToolBuilder, 4232747799);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UEdgeLoopInsertionToolBuilder>()
	{
		return UEdgeLoopInsertionToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEdgeLoopInsertionToolBuilder(Z_Construct_UClass_UEdgeLoopInsertionToolBuilder, &UEdgeLoopInsertionToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UEdgeLoopInsertionToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEdgeLoopInsertionToolBuilder);
	void UEdgeLoopInsertionProperties::StaticRegisterNativesUEdgeLoopInsertionProperties()
	{
	}
	UClass* Z_Construct_UClass_UEdgeLoopInsertionProperties_NoRegister()
	{
		return UEdgeLoopInsertionProperties::StaticClass();
	}
	struct Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_PositionMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PositionMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_PositionMode;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_InsertionMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InsertionMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InsertionMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumLoops_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumLoops;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProportionOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FDoublePropertyParams NewProp_ProportionOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DistanceOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FDoublePropertyParams NewProp_DistanceOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bInteractive_MetaData[];
#endif
		static void NewProp_bInteractive_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInteractive;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bFlipOffsetDirection_MetaData[];
#endif
		static void NewProp_bFlipOffsetDirection_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFlipOffsetDirection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bWireframe_MetaData[];
#endif
		static void NewProp_bWireframe_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bWireframe;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VertexTolerance_MetaData[];
#endif
		static const UE4CodeGen_Private::FDoublePropertyParams NewProp_VertexTolerance;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "EdgeLoopInsertionTool.h" },
		{ "ModuleRelativePath", "Public/EdgeLoopInsertionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_PositionMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_PositionMode_MetaData[] = {
		{ "Category", "InsertEdgeLoop" },
		{ "Comment", "/** Determines how edge loops position themselves vertically relative to loop direction. */" },
		{ "ModuleRelativePath", "Public/EdgeLoopInsertionTool.h" },
		{ "ToolTip", "Determines how edge loops position themselves vertically relative to loop direction." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_PositionMode = { "PositionMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEdgeLoopInsertionProperties, PositionMode), Z_Construct_UEnum_MeshModelingTools_EEdgeLoopPositioningMode, METADATA_PARAMS(Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_PositionMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_PositionMode_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_InsertionMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_InsertionMode_MetaData[] = {
		{ "Category", "InsertEdgeLoop" },
		{ "Comment", "/** Determines how edge loops are added to the geometry */" },
		{ "ModuleRelativePath", "Public/EdgeLoopInsertionTool.h" },
		{ "ToolTip", "Determines how edge loops are added to the geometry" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_InsertionMode = { "InsertionMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEdgeLoopInsertionProperties, InsertionMode), Z_Construct_UEnum_MeshModelingTools_EEdgeLoopInsertionMode, METADATA_PARAMS(Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_InsertionMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_InsertionMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_NumLoops_MetaData[] = {
		{ "Category", "InsertEdgeLoop" },
		{ "ClampMax", "500" },
		{ "ClampMin", "0" },
		{ "Comment", "/** How many loops to insert at a time. Only used with \"even\" positioning mode. */" },
		{ "EditCondition", "PositionMode == EEdgeLoopPositioningMode::Even" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/EdgeLoopInsertionTool.h" },
		{ "ToolTip", "How many loops to insert at a time. Only used with \"even\" positioning mode." },
		{ "UIMax", "20" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_NumLoops = { "NumLoops", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEdgeLoopInsertionProperties, NumLoops), METADATA_PARAMS(Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_NumLoops_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_NumLoops_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_ProportionOffset_MetaData[] = {
		{ "Category", "InsertEdgeLoop" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0" },
		{ "EditCondition", "PositionMode == EEdgeLoopPositioningMode::ProportionOffset && !bInteractive" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/EdgeLoopInsertionTool.h" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FDoublePropertyParams Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_ProportionOffset = { "ProportionOffset", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEdgeLoopInsertionProperties, ProportionOffset), METADATA_PARAMS(Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_ProportionOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_ProportionOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_DistanceOffset_MetaData[] = {
		{ "Category", "InsertEdgeLoop" },
		{ "ClampMin", "0" },
		{ "EditCondition", "PositionMode == EEdgeLoopPositioningMode::DistanceOffset && !bInteractive" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/EdgeLoopInsertionTool.h" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FDoublePropertyParams Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_DistanceOffset = { "DistanceOffset", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEdgeLoopInsertionProperties, DistanceOffset), METADATA_PARAMS(Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_DistanceOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_DistanceOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_bInteractive_MetaData[] = {
		{ "Category", "InsertEdgeLoop" },
		{ "Comment", "/** When false, the distance/proportion offset is numerically specified, and mouse clicks just choose the edge. */" },
		{ "EditCondition", "PositionMode != EEdgeLoopPositioningMode::Even" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/EdgeLoopInsertionTool.h" },
		{ "ToolTip", "When false, the distance/proportion offset is numerically specified, and mouse clicks just choose the edge." },
	};
#endif
	void Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_bInteractive_SetBit(void* Obj)
	{
		((UEdgeLoopInsertionProperties*)Obj)->bInteractive = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_bInteractive = { "bInteractive", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UEdgeLoopInsertionProperties), &Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_bInteractive_SetBit, METADATA_PARAMS(Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_bInteractive_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_bInteractive_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_bFlipOffsetDirection_MetaData[] = {
		{ "Category", "InsertEdgeLoop" },
		{ "Comment", "/** Measure the distance offset from the opposite side of the edges. */" },
		{ "EditCondition", "PositionMode == EEdgeLoopPositioningMode::DistanceOffset" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/EdgeLoopInsertionTool.h" },
		{ "ToolTip", "Measure the distance offset from the opposite side of the edges." },
	};
#endif
	void Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_bFlipOffsetDirection_SetBit(void* Obj)
	{
		((UEdgeLoopInsertionProperties*)Obj)->bFlipOffsetDirection = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_bFlipOffsetDirection = { "bFlipOffsetDirection", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UEdgeLoopInsertionProperties), &Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_bFlipOffsetDirection_SetBit, METADATA_PARAMS(Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_bFlipOffsetDirection_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_bFlipOffsetDirection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_bWireframe_MetaData[] = {
		{ "Category", "InsertEdgeLoop" },
		{ "ModuleRelativePath", "Public/EdgeLoopInsertionTool.h" },
	};
#endif
	void Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_bWireframe_SetBit(void* Obj)
	{
		((UEdgeLoopInsertionProperties*)Obj)->bWireframe = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_bWireframe = { "bWireframe", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UEdgeLoopInsertionProperties), &Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_bWireframe_SetBit, METADATA_PARAMS(Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_bWireframe_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_bWireframe_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_VertexTolerance_MetaData[] = {
		{ "Category", "InsertEdgeLoop" },
		{ "Comment", "/** How close a new loop edge needs to pass next to an existing vertex to use that vertex rather than creating a new one. */" },
		{ "ModuleRelativePath", "Public/EdgeLoopInsertionTool.h" },
		{ "ToolTip", "How close a new loop edge needs to pass next to an existing vertex to use that vertex rather than creating a new one." },
	};
#endif
	const UE4CodeGen_Private::FDoublePropertyParams Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_VertexTolerance = { "VertexTolerance", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEdgeLoopInsertionProperties, VertexTolerance), METADATA_PARAMS(Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_VertexTolerance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_VertexTolerance_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_PositionMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_PositionMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_InsertionMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_InsertionMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_NumLoops,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_ProportionOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_DistanceOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_bInteractive,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_bFlipOffsetDirection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_bWireframe,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::NewProp_VertexTolerance,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEdgeLoopInsertionProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::ClassParams = {
		&UEdgeLoopInsertionProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEdgeLoopInsertionProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEdgeLoopInsertionProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEdgeLoopInsertionProperties, 3690723179);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UEdgeLoopInsertionProperties>()
	{
		return UEdgeLoopInsertionProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEdgeLoopInsertionProperties(Z_Construct_UClass_UEdgeLoopInsertionProperties, &UEdgeLoopInsertionProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UEdgeLoopInsertionProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEdgeLoopInsertionProperties);
	void UEdgeLoopInsertionOperatorFactory::StaticRegisterNativesUEdgeLoopInsertionOperatorFactory()
	{
	}
	UClass* Z_Construct_UClass_UEdgeLoopInsertionOperatorFactory_NoRegister()
	{
		return UEdgeLoopInsertionOperatorFactory::StaticClass();
	}
	struct Z_Construct_UClass_UEdgeLoopInsertionOperatorFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Tool_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Tool;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEdgeLoopInsertionOperatorFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEdgeLoopInsertionOperatorFactory_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "EdgeLoopInsertionTool.h" },
		{ "ModuleRelativePath", "Public/EdgeLoopInsertionTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEdgeLoopInsertionOperatorFactory_Statics::NewProp_Tool_MetaData[] = {
		{ "ModuleRelativePath", "Public/EdgeLoopInsertionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UEdgeLoopInsertionOperatorFactory_Statics::NewProp_Tool = { "Tool", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEdgeLoopInsertionOperatorFactory, Tool), Z_Construct_UClass_UEdgeLoopInsertionTool_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UEdgeLoopInsertionOperatorFactory_Statics::NewProp_Tool_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEdgeLoopInsertionOperatorFactory_Statics::NewProp_Tool_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UEdgeLoopInsertionOperatorFactory_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEdgeLoopInsertionOperatorFactory_Statics::NewProp_Tool,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEdgeLoopInsertionOperatorFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEdgeLoopInsertionOperatorFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEdgeLoopInsertionOperatorFactory_Statics::ClassParams = {
		&UEdgeLoopInsertionOperatorFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UEdgeLoopInsertionOperatorFactory_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UEdgeLoopInsertionOperatorFactory_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UEdgeLoopInsertionOperatorFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEdgeLoopInsertionOperatorFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEdgeLoopInsertionOperatorFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEdgeLoopInsertionOperatorFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEdgeLoopInsertionOperatorFactory, 816357951);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UEdgeLoopInsertionOperatorFactory>()
	{
		return UEdgeLoopInsertionOperatorFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEdgeLoopInsertionOperatorFactory(Z_Construct_UClass_UEdgeLoopInsertionOperatorFactory, &UEdgeLoopInsertionOperatorFactory::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UEdgeLoopInsertionOperatorFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEdgeLoopInsertionOperatorFactory);
	void UEdgeLoopInsertionTool::StaticRegisterNativesUEdgeLoopInsertionTool()
	{
	}
	UClass* Z_Construct_UClass_UEdgeLoopInsertionTool_NoRegister()
	{
		return UEdgeLoopInsertionTool::StaticClass();
	}
	struct Z_Construct_UClass_UEdgeLoopInsertionTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Settings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Settings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Preview_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Preview;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEdgeLoopInsertionTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USingleSelectionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEdgeLoopInsertionTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Tool for inserting (group) edge loops into a mesh. */" },
		{ "IncludePath", "EdgeLoopInsertionTool.h" },
		{ "ModuleRelativePath", "Public/EdgeLoopInsertionTool.h" },
		{ "ToolTip", "Tool for inserting (group) edge loops into a mesh." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEdgeLoopInsertionTool_Statics::NewProp_Settings_MetaData[] = {
		{ "ModuleRelativePath", "Public/EdgeLoopInsertionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UEdgeLoopInsertionTool_Statics::NewProp_Settings = { "Settings", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEdgeLoopInsertionTool, Settings), Z_Construct_UClass_UEdgeLoopInsertionProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UEdgeLoopInsertionTool_Statics::NewProp_Settings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEdgeLoopInsertionTool_Statics::NewProp_Settings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEdgeLoopInsertionTool_Statics::NewProp_Preview_MetaData[] = {
		{ "ModuleRelativePath", "Public/EdgeLoopInsertionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UEdgeLoopInsertionTool_Statics::NewProp_Preview = { "Preview", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEdgeLoopInsertionTool, Preview), Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UEdgeLoopInsertionTool_Statics::NewProp_Preview_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEdgeLoopInsertionTool_Statics::NewProp_Preview_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UEdgeLoopInsertionTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEdgeLoopInsertionTool_Statics::NewProp_Settings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEdgeLoopInsertionTool_Statics::NewProp_Preview,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEdgeLoopInsertionTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEdgeLoopInsertionTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEdgeLoopInsertionTool_Statics::ClassParams = {
		&UEdgeLoopInsertionTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UEdgeLoopInsertionTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UEdgeLoopInsertionTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UEdgeLoopInsertionTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEdgeLoopInsertionTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEdgeLoopInsertionTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEdgeLoopInsertionTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEdgeLoopInsertionTool, 3821954340);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UEdgeLoopInsertionTool>()
	{
		return UEdgeLoopInsertionTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEdgeLoopInsertionTool(Z_Construct_UClass_UEdgeLoopInsertionTool, &UEdgeLoopInsertionTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UEdgeLoopInsertionTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEdgeLoopInsertionTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
