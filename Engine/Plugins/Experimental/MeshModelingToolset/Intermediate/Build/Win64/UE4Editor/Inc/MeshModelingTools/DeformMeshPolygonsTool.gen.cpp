// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/DeformMeshPolygonsTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDeformMeshPolygonsTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EQuickTransformerMode();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EWeightScheme();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EGroupTopologyDeformationStrategy();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDeformMeshPolygonsToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDeformMeshPolygonsToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UMeshSurfacePointToolBuilder();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDeformMeshPolygonsTransformProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDeformMeshPolygonsTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDeformMeshPolygonsTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UMeshSurfacePointTool();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_USimpleDynamicMeshComponent_NoRegister();
// End Cross Module References
	static UEnum* EQuickTransformerMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EQuickTransformerMode, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EQuickTransformerMode"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EQuickTransformerMode>()
	{
		return EQuickTransformerMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EQuickTransformerMode(EQuickTransformerMode_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EQuickTransformerMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EQuickTransformerMode_Hash() { return 792565862U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EQuickTransformerMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EQuickTransformerMode"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EQuickTransformerMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EQuickTransformerMode::AxisTranslation", (int64)EQuickTransformerMode::AxisTranslation },
				{ "EQuickTransformerMode::AxisRotation", (int64)EQuickTransformerMode::AxisRotation },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "AxisRotation.Comment", "/** Rotation around frame axes*/" },
				{ "AxisRotation.DisplayName", "Rotate" },
				{ "AxisRotation.Name", "EQuickTransformerMode::AxisRotation" },
				{ "AxisRotation.ToolTip", "Rotation around frame axes" },
				{ "AxisTranslation.Comment", "/** Translation along frame axes */" },
				{ "AxisTranslation.DisplayName", "Translate" },
				{ "AxisTranslation.Name", "EQuickTransformerMode::AxisTranslation" },
				{ "AxisTranslation.ToolTip", "Translation along frame axes" },
				{ "Comment", "/** Modes for quick transformer */" },
				{ "ModuleRelativePath", "Public/DeformMeshPolygonsTool.h" },
				{ "ToolTip", "Modes for quick transformer" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EQuickTransformerMode",
				"EQuickTransformerMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EWeightScheme_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EWeightScheme, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EWeightScheme"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EWeightScheme>()
	{
		return EWeightScheme_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EWeightScheme(EWeightScheme_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EWeightScheme"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EWeightScheme_Hash() { return 3925875010U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EWeightScheme()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EWeightScheme"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EWeightScheme_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EWeightScheme::Uniform", (int64)EWeightScheme::Uniform },
				{ "EWeightScheme::Umbrella", (int64)EWeightScheme::Umbrella },
				{ "EWeightScheme::Valence", (int64)EWeightScheme::Valence },
				{ "EWeightScheme::MeanValue", (int64)EWeightScheme::MeanValue },
				{ "EWeightScheme::Cotangent", (int64)EWeightScheme::Cotangent },
				{ "EWeightScheme::ClampedCotangent", (int64)EWeightScheme::ClampedCotangent },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ClampedCotangent.DisplayName", "ClampedCotangent" },
				{ "ClampedCotangent.Name", "EWeightScheme::ClampedCotangent" },
				{ "Comment", "/** Laplacian weight schemes determine how we will look at the curvature at a given vertex in relation to its neighborhood*/" },
				{ "Cotangent.DisplayName", "Cotangent" },
				{ "Cotangent.Name", "EWeightScheme::Cotangent" },
				{ "MeanValue.DisplayName", "MeanValue" },
				{ "MeanValue.Name", "EWeightScheme::MeanValue" },
				{ "ModuleRelativePath", "Public/DeformMeshPolygonsTool.h" },
				{ "ToolTip", "Laplacian weight schemes determine how we will look at the curvature at a given vertex in relation to its neighborhood" },
				{ "Umbrella.DisplayName", "Umbrella" },
				{ "Umbrella.Name", "EWeightScheme::Umbrella" },
				{ "Uniform.DisplayName", "Uniform" },
				{ "Uniform.Name", "EWeightScheme::Uniform" },
				{ "Valence.DisplayName", "Valence" },
				{ "Valence.Name", "EWeightScheme::Valence" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EWeightScheme",
				"EWeightScheme",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EGroupTopologyDeformationStrategy_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EGroupTopologyDeformationStrategy, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EGroupTopologyDeformationStrategy"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EGroupTopologyDeformationStrategy>()
	{
		return EGroupTopologyDeformationStrategy_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EGroupTopologyDeformationStrategy(EGroupTopologyDeformationStrategy_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EGroupTopologyDeformationStrategy"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EGroupTopologyDeformationStrategy_Hash() { return 1151824211U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EGroupTopologyDeformationStrategy()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EGroupTopologyDeformationStrategy"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EGroupTopologyDeformationStrategy_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EGroupTopologyDeformationStrategy::Linear", (int64)EGroupTopologyDeformationStrategy::Linear },
				{ "EGroupTopologyDeformationStrategy::Laplacian", (int64)EGroupTopologyDeformationStrategy::Laplacian },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** Deformation strategies */" },
				{ "Laplacian.Comment", "/** Deforms the mesh using laplacian deformation*/" },
				{ "Laplacian.DisplayName", "Smooth" },
				{ "Laplacian.Name", "EGroupTopologyDeformationStrategy::Laplacian" },
				{ "Laplacian.ToolTip", "Deforms the mesh using laplacian deformation" },
				{ "Linear.Comment", "/** Deforms the mesh using linear translations*/" },
				{ "Linear.DisplayName", "Linear" },
				{ "Linear.Name", "EGroupTopologyDeformationStrategy::Linear" },
				{ "Linear.ToolTip", "Deforms the mesh using linear translations" },
				{ "ModuleRelativePath", "Public/DeformMeshPolygonsTool.h" },
				{ "ToolTip", "Deformation strategies" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EGroupTopologyDeformationStrategy",
				"EGroupTopologyDeformationStrategy",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UDeformMeshPolygonsToolBuilder::StaticRegisterNativesUDeformMeshPolygonsToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UDeformMeshPolygonsToolBuilder_NoRegister()
	{
		return UDeformMeshPolygonsToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UDeformMeshPolygonsToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDeformMeshPolygonsToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshSurfacePointToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDeformMeshPolygonsToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * ToolBuilder\n */" },
		{ "IncludePath", "DeformMeshPolygonsTool.h" },
		{ "ModuleRelativePath", "Public/DeformMeshPolygonsTool.h" },
		{ "ToolTip", "ToolBuilder" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDeformMeshPolygonsToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDeformMeshPolygonsToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDeformMeshPolygonsToolBuilder_Statics::ClassParams = {
		&UDeformMeshPolygonsToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UDeformMeshPolygonsToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDeformMeshPolygonsToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDeformMeshPolygonsToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDeformMeshPolygonsToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDeformMeshPolygonsToolBuilder, 2391317213);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UDeformMeshPolygonsToolBuilder>()
	{
		return UDeformMeshPolygonsToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDeformMeshPolygonsToolBuilder(Z_Construct_UClass_UDeformMeshPolygonsToolBuilder, &UDeformMeshPolygonsToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UDeformMeshPolygonsToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDeformMeshPolygonsToolBuilder);
	void UDeformMeshPolygonsTransformProperties::StaticRegisterNativesUDeformMeshPolygonsTransformProperties()
	{
	}
	UClass* Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_NoRegister()
	{
		return UDeformMeshPolygonsTransformProperties::StaticClass();
	}
	struct Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DeformationStrategy_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeformationStrategy_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_DeformationStrategy;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TransformMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransformMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TransformMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSelectFaces_MetaData[];
#endif
		static void NewProp_bSelectFaces_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSelectFaces;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSelectEdges_MetaData[];
#endif
		static void NewProp_bSelectEdges_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSelectEdges;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSelectVertices_MetaData[];
#endif
		static void NewProp_bSelectVertices_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSelectVertices;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSnapToWorldGrid_MetaData[];
#endif
		static void NewProp_bSnapToWorldGrid_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSnapToWorldGrid;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowWireframe_MetaData[];
#endif
		static void NewProp_bShowWireframe_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowWireframe;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_SelectedWeightScheme_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SelectedWeightScheme_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_SelectedWeightScheme;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HandleWeight_MetaData[];
#endif
		static const UE4CodeGen_Private::FDoublePropertyParams NewProp_HandleWeight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPostFixHandles_MetaData[];
#endif
		static void NewProp_bPostFixHandles_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPostFixHandles;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "DeformMeshPolygonsTool.h" },
		{ "ModuleRelativePath", "Public/DeformMeshPolygonsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_DeformationStrategy_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_DeformationStrategy_MetaData[] = {
		{ "Category", "Options" },
		{ "DisplayName", "Deformation Type" },
		{ "ModuleRelativePath", "Public/DeformMeshPolygonsTool.h" },
		{ "ToolTip", "Select the type of deformation you wish to employ on a polygroup." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_DeformationStrategy = { "DeformationStrategy", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDeformMeshPolygonsTransformProperties, DeformationStrategy), Z_Construct_UEnum_MeshModelingTools_EGroupTopologyDeformationStrategy, METADATA_PARAMS(Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_DeformationStrategy_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_DeformationStrategy_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_TransformMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_TransformMode_MetaData[] = {
		{ "Category", "Options" },
		{ "ModuleRelativePath", "Public/DeformMeshPolygonsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_TransformMode = { "TransformMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDeformMeshPolygonsTransformProperties, TransformMode), Z_Construct_UEnum_MeshModelingTools_EQuickTransformerMode, METADATA_PARAMS(Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_TransformMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_TransformMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bSelectFaces_MetaData[] = {
		{ "Category", "SelectionFilter" },
		{ "ModuleRelativePath", "Public/DeformMeshPolygonsTool.h" },
	};
#endif
	void Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bSelectFaces_SetBit(void* Obj)
	{
		((UDeformMeshPolygonsTransformProperties*)Obj)->bSelectFaces = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bSelectFaces = { "bSelectFaces", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDeformMeshPolygonsTransformProperties), &Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bSelectFaces_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bSelectFaces_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bSelectFaces_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bSelectEdges_MetaData[] = {
		{ "Category", "SelectionFilter" },
		{ "ModuleRelativePath", "Public/DeformMeshPolygonsTool.h" },
	};
#endif
	void Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bSelectEdges_SetBit(void* Obj)
	{
		((UDeformMeshPolygonsTransformProperties*)Obj)->bSelectEdges = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bSelectEdges = { "bSelectEdges", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDeformMeshPolygonsTransformProperties), &Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bSelectEdges_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bSelectEdges_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bSelectEdges_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bSelectVertices_MetaData[] = {
		{ "Category", "SelectionFilter" },
		{ "ModuleRelativePath", "Public/DeformMeshPolygonsTool.h" },
	};
#endif
	void Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bSelectVertices_SetBit(void* Obj)
	{
		((UDeformMeshPolygonsTransformProperties*)Obj)->bSelectVertices = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bSelectVertices = { "bSelectVertices", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDeformMeshPolygonsTransformProperties), &Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bSelectVertices_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bSelectVertices_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bSelectVertices_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bSnapToWorldGrid_MetaData[] = {
		{ "Category", "Snapping" },
		{ "ModuleRelativePath", "Public/DeformMeshPolygonsTool.h" },
	};
#endif
	void Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bSnapToWorldGrid_SetBit(void* Obj)
	{
		((UDeformMeshPolygonsTransformProperties*)Obj)->bSnapToWorldGrid = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bSnapToWorldGrid = { "bSnapToWorldGrid", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDeformMeshPolygonsTransformProperties), &Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bSnapToWorldGrid_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bSnapToWorldGrid_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bSnapToWorldGrid_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bShowWireframe_MetaData[] = {
		{ "Category", "Display" },
		{ "ModuleRelativePath", "Public/DeformMeshPolygonsTool.h" },
	};
#endif
	void Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bShowWireframe_SetBit(void* Obj)
	{
		((UDeformMeshPolygonsTransformProperties*)Obj)->bShowWireframe = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bShowWireframe = { "bShowWireframe", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDeformMeshPolygonsTransformProperties), &Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bShowWireframe_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bShowWireframe_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bShowWireframe_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_SelectedWeightScheme_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_SelectedWeightScheme_MetaData[] = {
		{ "Comment", "//Laplacian Deformation Options, currently not exposed.\n" },
		{ "ModuleRelativePath", "Public/DeformMeshPolygonsTool.h" },
		{ "ToolTip", "Laplacian Deformation Options, currently not exposed." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_SelectedWeightScheme = { "SelectedWeightScheme", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDeformMeshPolygonsTransformProperties, SelectedWeightScheme), Z_Construct_UEnum_MeshModelingTools_EWeightScheme, METADATA_PARAMS(Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_SelectedWeightScheme_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_SelectedWeightScheme_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_HandleWeight_MetaData[] = {
		{ "ModuleRelativePath", "Public/DeformMeshPolygonsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FDoublePropertyParams Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_HandleWeight = { "HandleWeight", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDeformMeshPolygonsTransformProperties, HandleWeight), METADATA_PARAMS(Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_HandleWeight_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_HandleWeight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bPostFixHandles_MetaData[] = {
		{ "ModuleRelativePath", "Public/DeformMeshPolygonsTool.h" },
	};
#endif
	void Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bPostFixHandles_SetBit(void* Obj)
	{
		((UDeformMeshPolygonsTransformProperties*)Obj)->bPostFixHandles = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bPostFixHandles = { "bPostFixHandles", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDeformMeshPolygonsTransformProperties), &Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bPostFixHandles_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bPostFixHandles_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bPostFixHandles_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_DeformationStrategy_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_DeformationStrategy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_TransformMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_TransformMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bSelectFaces,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bSelectEdges,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bSelectVertices,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bSnapToWorldGrid,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bShowWireframe,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_SelectedWeightScheme_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_SelectedWeightScheme,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_HandleWeight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::NewProp_bPostFixHandles,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDeformMeshPolygonsTransformProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::ClassParams = {
		&UDeformMeshPolygonsTransformProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDeformMeshPolygonsTransformProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDeformMeshPolygonsTransformProperties, 4289052212);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UDeformMeshPolygonsTransformProperties>()
	{
		return UDeformMeshPolygonsTransformProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDeformMeshPolygonsTransformProperties(Z_Construct_UClass_UDeformMeshPolygonsTransformProperties, &UDeformMeshPolygonsTransformProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UDeformMeshPolygonsTransformProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDeformMeshPolygonsTransformProperties);
	void UDeformMeshPolygonsTool::StaticRegisterNativesUDeformMeshPolygonsTool()
	{
	}
	UClass* Z_Construct_UClass_UDeformMeshPolygonsTool_NoRegister()
	{
		return UDeformMeshPolygonsTool::StaticClass();
	}
	struct Z_Construct_UClass_UDeformMeshPolygonsTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DynamicMeshComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DynamicMeshComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransformProps_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TransformProps;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDeformMeshPolygonsTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshSurfacePointTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDeformMeshPolygonsTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "DeformMeshPolygonsTool.h" },
		{ "ModuleRelativePath", "Public/DeformMeshPolygonsTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDeformMeshPolygonsTool_Statics::NewProp_DynamicMeshComponent_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DeformMeshPolygonsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDeformMeshPolygonsTool_Statics::NewProp_DynamicMeshComponent = { "DynamicMeshComponent", nullptr, (EPropertyFlags)0x0020080000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDeformMeshPolygonsTool, DynamicMeshComponent), Z_Construct_UClass_USimpleDynamicMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDeformMeshPolygonsTool_Statics::NewProp_DynamicMeshComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDeformMeshPolygonsTool_Statics::NewProp_DynamicMeshComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDeformMeshPolygonsTool_Statics::NewProp_TransformProps_MetaData[] = {
		{ "ModuleRelativePath", "Public/DeformMeshPolygonsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDeformMeshPolygonsTool_Statics::NewProp_TransformProps = { "TransformProps", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDeformMeshPolygonsTool, TransformProps), Z_Construct_UClass_UDeformMeshPolygonsTransformProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDeformMeshPolygonsTool_Statics::NewProp_TransformProps_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDeformMeshPolygonsTool_Statics::NewProp_TransformProps_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDeformMeshPolygonsTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDeformMeshPolygonsTool_Statics::NewProp_DynamicMeshComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDeformMeshPolygonsTool_Statics::NewProp_TransformProps,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDeformMeshPolygonsTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDeformMeshPolygonsTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDeformMeshPolygonsTool_Statics::ClassParams = {
		&UDeformMeshPolygonsTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDeformMeshPolygonsTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDeformMeshPolygonsTool_Statics::PropPointers),
		0,
		0x009000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UDeformMeshPolygonsTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDeformMeshPolygonsTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDeformMeshPolygonsTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDeformMeshPolygonsTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDeformMeshPolygonsTool, 3686377415);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UDeformMeshPolygonsTool>()
	{
		return UDeformMeshPolygonsTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDeformMeshPolygonsTool(Z_Construct_UClass_UDeformMeshPolygonsTool, &UDeformMeshPolygonsTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UDeformMeshPolygonsTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDeformMeshPolygonsTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
