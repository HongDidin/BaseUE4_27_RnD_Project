// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/SmoothMeshTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSmoothMeshTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_ESmoothMeshToolSmoothType();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_USmoothMeshToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_USmoothMeshToolProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UIterativeSmoothProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UIterativeSmoothProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDiffusionSmoothProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDiffusionSmoothProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UImplicitSmoothProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UImplicitSmoothProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_USmoothWeightMapSetProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_USmoothWeightMapSetProperties();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UWeightMapSetProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_USmoothMeshTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_USmoothMeshTool();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UBaseMeshProcessingTool();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_USmoothMeshToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_USmoothMeshToolBuilder();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UBaseMeshProcessingToolBuilder();
// End Cross Module References
	static UEnum* ESmoothMeshToolSmoothType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_ESmoothMeshToolSmoothType, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("ESmoothMeshToolSmoothType"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<ESmoothMeshToolSmoothType>()
	{
		return ESmoothMeshToolSmoothType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ESmoothMeshToolSmoothType(ESmoothMeshToolSmoothType_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("ESmoothMeshToolSmoothType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_ESmoothMeshToolSmoothType_Hash() { return 2398618780U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_ESmoothMeshToolSmoothType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ESmoothMeshToolSmoothType"), 0, Get_Z_Construct_UEnum_MeshModelingTools_ESmoothMeshToolSmoothType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ESmoothMeshToolSmoothType::Iterative", (int64)ESmoothMeshToolSmoothType::Iterative },
				{ "ESmoothMeshToolSmoothType::Implicit", (int64)ESmoothMeshToolSmoothType::Implicit },
				{ "ESmoothMeshToolSmoothType::Diffusion", (int64)ESmoothMeshToolSmoothType::Diffusion },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Diffusion.Comment", "/** Iterative implicit-diffusion smoothing with N iterations */" },
				{ "Diffusion.DisplayName", "Iterative Diffusion" },
				{ "Diffusion.Name", "ESmoothMeshToolSmoothType::Diffusion" },
				{ "Diffusion.ToolTip", "Iterative implicit-diffusion smoothing with N iterations" },
				{ "Implicit.Comment", "/** Implicit smoothing, produces smoother output and does a better job at preserving UVs, but can be very slow on large meshes */" },
				{ "Implicit.DisplayName", "Fast Implicit" },
				{ "Implicit.Name", "ESmoothMeshToolSmoothType::Implicit" },
				{ "Implicit.ToolTip", "Implicit smoothing, produces smoother output and does a better job at preserving UVs, but can be very slow on large meshes" },
				{ "Iterative.Comment", "/** Iterative smoothing with N iterations */" },
				{ "Iterative.DisplayName", "Fast Iterative" },
				{ "Iterative.Name", "ESmoothMeshToolSmoothType::Iterative" },
				{ "Iterative.ToolTip", "Iterative smoothing with N iterations" },
				{ "ModuleRelativePath", "Public/SmoothMeshTool.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"ESmoothMeshToolSmoothType",
				"ESmoothMeshToolSmoothType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void USmoothMeshToolProperties::StaticRegisterNativesUSmoothMeshToolProperties()
	{
	}
	UClass* Z_Construct_UClass_USmoothMeshToolProperties_NoRegister()
	{
		return USmoothMeshToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_USmoothMeshToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_SmoothingType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SmoothingType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_SmoothingType;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USmoothMeshToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USmoothMeshToolProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** PropertySet for properties affecting the Smoother. */" },
		{ "IncludePath", "SmoothMeshTool.h" },
		{ "ModuleRelativePath", "Public/SmoothMeshTool.h" },
		{ "ToolTip", "PropertySet for properties affecting the Smoother." },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_USmoothMeshToolProperties_Statics::NewProp_SmoothingType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USmoothMeshToolProperties_Statics::NewProp_SmoothingType_MetaData[] = {
		{ "Category", "SmoothingType" },
		{ "Comment", "/** Type of smoothing to apply */" },
		{ "ModuleRelativePath", "Public/SmoothMeshTool.h" },
		{ "ToolTip", "Type of smoothing to apply" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_USmoothMeshToolProperties_Statics::NewProp_SmoothingType = { "SmoothingType", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USmoothMeshToolProperties, SmoothingType), Z_Construct_UEnum_MeshModelingTools_ESmoothMeshToolSmoothType, METADATA_PARAMS(Z_Construct_UClass_USmoothMeshToolProperties_Statics::NewProp_SmoothingType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USmoothMeshToolProperties_Statics::NewProp_SmoothingType_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USmoothMeshToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USmoothMeshToolProperties_Statics::NewProp_SmoothingType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USmoothMeshToolProperties_Statics::NewProp_SmoothingType,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USmoothMeshToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USmoothMeshToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USmoothMeshToolProperties_Statics::ClassParams = {
		&USmoothMeshToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USmoothMeshToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USmoothMeshToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_USmoothMeshToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USmoothMeshToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USmoothMeshToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USmoothMeshToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USmoothMeshToolProperties, 995577116);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<USmoothMeshToolProperties>()
	{
		return USmoothMeshToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USmoothMeshToolProperties(Z_Construct_UClass_USmoothMeshToolProperties, &USmoothMeshToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("USmoothMeshToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USmoothMeshToolProperties);
	void UIterativeSmoothProperties::StaticRegisterNativesUIterativeSmoothProperties()
	{
	}
	UClass* Z_Construct_UClass_UIterativeSmoothProperties_NoRegister()
	{
		return UIterativeSmoothProperties::StaticClass();
	}
	struct Z_Construct_UClass_UIterativeSmoothProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SmoothingPerStep_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SmoothingPerStep;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Steps_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Steps;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSmoothBoundary_MetaData[];
#endif
		static void NewProp_bSmoothBoundary_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSmoothBoundary;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UIterativeSmoothProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UIterativeSmoothProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Properties for Iterative Smoothing */" },
		{ "IncludePath", "SmoothMeshTool.h" },
		{ "ModuleRelativePath", "Public/SmoothMeshTool.h" },
		{ "ToolTip", "Properties for Iterative Smoothing" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UIterativeSmoothProperties_Statics::NewProp_SmoothingPerStep_MetaData[] = {
		{ "Category", "IterativeSmoothingOptions" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Amount of smoothing allowed per step. Smaller steps will avoid things like collapse of small/thin features. */" },
		{ "ModuleRelativePath", "Public/SmoothMeshTool.h" },
		{ "ToolTip", "Amount of smoothing allowed per step. Smaller steps will avoid things like collapse of small/thin features." },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UIterativeSmoothProperties_Statics::NewProp_SmoothingPerStep = { "SmoothingPerStep", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UIterativeSmoothProperties, SmoothingPerStep), METADATA_PARAMS(Z_Construct_UClass_UIterativeSmoothProperties_Statics::NewProp_SmoothingPerStep_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UIterativeSmoothProperties_Statics::NewProp_SmoothingPerStep_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UIterativeSmoothProperties_Statics::NewProp_Steps_MetaData[] = {
		{ "Category", "IterativeSmoothingOptions" },
		{ "ClampMax", "1000" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Number of Smoothing iterations */" },
		{ "ModuleRelativePath", "Public/SmoothMeshTool.h" },
		{ "ToolTip", "Number of Smoothing iterations" },
		{ "UIMax", "100" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UIterativeSmoothProperties_Statics::NewProp_Steps = { "Steps", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UIterativeSmoothProperties, Steps), METADATA_PARAMS(Z_Construct_UClass_UIterativeSmoothProperties_Statics::NewProp_Steps_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UIterativeSmoothProperties_Statics::NewProp_Steps_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UIterativeSmoothProperties_Statics::NewProp_bSmoothBoundary_MetaData[] = {
		{ "Category", "IterativeSmoothingOptions" },
		{ "Comment", "/** If this is false, the smoother will try to reshape the triangles to be more regular, which will distort UVs */" },
		{ "ModuleRelativePath", "Public/SmoothMeshTool.h" },
		{ "ToolTip", "If this is false, the smoother will try to reshape the triangles to be more regular, which will distort UVs" },
	};
#endif
	void Z_Construct_UClass_UIterativeSmoothProperties_Statics::NewProp_bSmoothBoundary_SetBit(void* Obj)
	{
		((UIterativeSmoothProperties*)Obj)->bSmoothBoundary = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UIterativeSmoothProperties_Statics::NewProp_bSmoothBoundary = { "bSmoothBoundary", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UIterativeSmoothProperties), &Z_Construct_UClass_UIterativeSmoothProperties_Statics::NewProp_bSmoothBoundary_SetBit, METADATA_PARAMS(Z_Construct_UClass_UIterativeSmoothProperties_Statics::NewProp_bSmoothBoundary_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UIterativeSmoothProperties_Statics::NewProp_bSmoothBoundary_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UIterativeSmoothProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UIterativeSmoothProperties_Statics::NewProp_SmoothingPerStep,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UIterativeSmoothProperties_Statics::NewProp_Steps,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UIterativeSmoothProperties_Statics::NewProp_bSmoothBoundary,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UIterativeSmoothProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UIterativeSmoothProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UIterativeSmoothProperties_Statics::ClassParams = {
		&UIterativeSmoothProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UIterativeSmoothProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UIterativeSmoothProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UIterativeSmoothProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UIterativeSmoothProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UIterativeSmoothProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UIterativeSmoothProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UIterativeSmoothProperties, 3097455681);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UIterativeSmoothProperties>()
	{
		return UIterativeSmoothProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UIterativeSmoothProperties(Z_Construct_UClass_UIterativeSmoothProperties, &UIterativeSmoothProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UIterativeSmoothProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UIterativeSmoothProperties);
	void UDiffusionSmoothProperties::StaticRegisterNativesUDiffusionSmoothProperties()
	{
	}
	UClass* Z_Construct_UClass_UDiffusionSmoothProperties_NoRegister()
	{
		return UDiffusionSmoothProperties::StaticClass();
	}
	struct Z_Construct_UClass_UDiffusionSmoothProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SmoothingPerStep_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SmoothingPerStep;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Steps_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Steps;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPreserveUVs_MetaData[];
#endif
		static void NewProp_bPreserveUVs_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPreserveUVs;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDiffusionSmoothProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDiffusionSmoothProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Properties for Diffusion Smoothing */" },
		{ "IncludePath", "SmoothMeshTool.h" },
		{ "ModuleRelativePath", "Public/SmoothMeshTool.h" },
		{ "ToolTip", "Properties for Diffusion Smoothing" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDiffusionSmoothProperties_Statics::NewProp_SmoothingPerStep_MetaData[] = {
		{ "Category", "DiffusionSmoothingOptions" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Amount of smoothing allowed per step. Smaller steps will avoid things like collapse of small/thin features. */" },
		{ "ModuleRelativePath", "Public/SmoothMeshTool.h" },
		{ "ToolTip", "Amount of smoothing allowed per step. Smaller steps will avoid things like collapse of small/thin features." },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDiffusionSmoothProperties_Statics::NewProp_SmoothingPerStep = { "SmoothingPerStep", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDiffusionSmoothProperties, SmoothingPerStep), METADATA_PARAMS(Z_Construct_UClass_UDiffusionSmoothProperties_Statics::NewProp_SmoothingPerStep_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDiffusionSmoothProperties_Statics::NewProp_SmoothingPerStep_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDiffusionSmoothProperties_Statics::NewProp_Steps_MetaData[] = {
		{ "Category", "DiffusionSmoothingOptions" },
		{ "ClampMax", "1000" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Number of Smoothing iterations */" },
		{ "ModuleRelativePath", "Public/SmoothMeshTool.h" },
		{ "ToolTip", "Number of Smoothing iterations" },
		{ "UIMax", "100" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UDiffusionSmoothProperties_Statics::NewProp_Steps = { "Steps", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDiffusionSmoothProperties, Steps), METADATA_PARAMS(Z_Construct_UClass_UDiffusionSmoothProperties_Statics::NewProp_Steps_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDiffusionSmoothProperties_Statics::NewProp_Steps_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDiffusionSmoothProperties_Statics::NewProp_bPreserveUVs_MetaData[] = {
		{ "Category", "DiffusionSmoothingOptions" },
		{ "Comment", "/** If this is false, the smoother will try to reshape the triangles to be more regular, which will distort UVs */" },
		{ "ModuleRelativePath", "Public/SmoothMeshTool.h" },
		{ "ToolTip", "If this is false, the smoother will try to reshape the triangles to be more regular, which will distort UVs" },
	};
#endif
	void Z_Construct_UClass_UDiffusionSmoothProperties_Statics::NewProp_bPreserveUVs_SetBit(void* Obj)
	{
		((UDiffusionSmoothProperties*)Obj)->bPreserveUVs = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDiffusionSmoothProperties_Statics::NewProp_bPreserveUVs = { "bPreserveUVs", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDiffusionSmoothProperties), &Z_Construct_UClass_UDiffusionSmoothProperties_Statics::NewProp_bPreserveUVs_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDiffusionSmoothProperties_Statics::NewProp_bPreserveUVs_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDiffusionSmoothProperties_Statics::NewProp_bPreserveUVs_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDiffusionSmoothProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDiffusionSmoothProperties_Statics::NewProp_SmoothingPerStep,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDiffusionSmoothProperties_Statics::NewProp_Steps,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDiffusionSmoothProperties_Statics::NewProp_bPreserveUVs,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDiffusionSmoothProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDiffusionSmoothProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDiffusionSmoothProperties_Statics::ClassParams = {
		&UDiffusionSmoothProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDiffusionSmoothProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDiffusionSmoothProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UDiffusionSmoothProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDiffusionSmoothProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDiffusionSmoothProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDiffusionSmoothProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDiffusionSmoothProperties, 2948703536);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UDiffusionSmoothProperties>()
	{
		return UDiffusionSmoothProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDiffusionSmoothProperties(Z_Construct_UClass_UDiffusionSmoothProperties, &UDiffusionSmoothProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UDiffusionSmoothProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDiffusionSmoothProperties);
	void UImplicitSmoothProperties::StaticRegisterNativesUImplicitSmoothProperties()
	{
	}
	UClass* Z_Construct_UClass_UImplicitSmoothProperties_NoRegister()
	{
		return UImplicitSmoothProperties::StaticClass();
	}
	struct Z_Construct_UClass_UImplicitSmoothProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SmoothSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SmoothSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Smoothness_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Smoothness;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPreserveUVs_MetaData[];
#endif
		static void NewProp_bPreserveUVs_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPreserveUVs;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VolumeCorrection_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_VolumeCorrection;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UImplicitSmoothProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UImplicitSmoothProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Properties for Implicit smoothing */" },
		{ "IncludePath", "SmoothMeshTool.h" },
		{ "ModuleRelativePath", "Public/SmoothMeshTool.h" },
		{ "ToolTip", "Properties for Implicit smoothing" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UImplicitSmoothProperties_Statics::NewProp_SmoothSpeed_MetaData[] = {
		{ "Comment", "/** Smoothing speed *///UPROPERTY(EditAnywhere, Category = ImplicitSmoothing, meta = (UIMin = \"0.0\", UIMax = \"1.0\", ClampMin = \"0.0\", ClampMax = \"1.0\"))\n" },
		{ "ModuleRelativePath", "Public/SmoothMeshTool.h" },
		{ "ToolTip", "Smoothing speed //UPROPERTY(EditAnywhere, Category = ImplicitSmoothing, meta = (UIMin = \"0.0\", UIMax = \"1.0\", ClampMin = \"0.0\", ClampMax = \"1.0\"))" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UImplicitSmoothProperties_Statics::NewProp_SmoothSpeed = { "SmoothSpeed", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UImplicitSmoothProperties, SmoothSpeed), METADATA_PARAMS(Z_Construct_UClass_UImplicitSmoothProperties_Statics::NewProp_SmoothSpeed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UImplicitSmoothProperties_Statics::NewProp_SmoothSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UImplicitSmoothProperties_Statics::NewProp_Smoothness_MetaData[] = {
		{ "Category", "ImplicitSmoothingOptions" },
		{ "ClampMax", "100.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Desired Smoothness. This is not a linear quantity, but larger numbers produce smoother results */" },
		{ "ModuleRelativePath", "Public/SmoothMeshTool.h" },
		{ "ToolTip", "Desired Smoothness. This is not a linear quantity, but larger numbers produce smoother results" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UImplicitSmoothProperties_Statics::NewProp_Smoothness = { "Smoothness", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UImplicitSmoothProperties, Smoothness), METADATA_PARAMS(Z_Construct_UClass_UImplicitSmoothProperties_Statics::NewProp_Smoothness_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UImplicitSmoothProperties_Statics::NewProp_Smoothness_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UImplicitSmoothProperties_Statics::NewProp_bPreserveUVs_MetaData[] = {
		{ "Category", "ImplicitSmoothingOptions" },
		{ "Comment", "/** If this is false, the smoother will try to reshape the triangles to be more regular, which will distort UVs */" },
		{ "ModuleRelativePath", "Public/SmoothMeshTool.h" },
		{ "ToolTip", "If this is false, the smoother will try to reshape the triangles to be more regular, which will distort UVs" },
	};
#endif
	void Z_Construct_UClass_UImplicitSmoothProperties_Statics::NewProp_bPreserveUVs_SetBit(void* Obj)
	{
		((UImplicitSmoothProperties*)Obj)->bPreserveUVs = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UImplicitSmoothProperties_Statics::NewProp_bPreserveUVs = { "bPreserveUVs", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UImplicitSmoothProperties), &Z_Construct_UClass_UImplicitSmoothProperties_Statics::NewProp_bPreserveUVs_SetBit, METADATA_PARAMS(Z_Construct_UClass_UImplicitSmoothProperties_Statics::NewProp_bPreserveUVs_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UImplicitSmoothProperties_Statics::NewProp_bPreserveUVs_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UImplicitSmoothProperties_Statics::NewProp_VolumeCorrection_MetaData[] = {
		{ "Category", "ImplicitSmoothingOptions" },
		{ "ClampMax", "10.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Magic number that allows you to try to correct for shrinking caused by smoothing */" },
		{ "ModuleRelativePath", "Public/SmoothMeshTool.h" },
		{ "ToolTip", "Magic number that allows you to try to correct for shrinking caused by smoothing" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UImplicitSmoothProperties_Statics::NewProp_VolumeCorrection = { "VolumeCorrection", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UImplicitSmoothProperties, VolumeCorrection), METADATA_PARAMS(Z_Construct_UClass_UImplicitSmoothProperties_Statics::NewProp_VolumeCorrection_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UImplicitSmoothProperties_Statics::NewProp_VolumeCorrection_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UImplicitSmoothProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UImplicitSmoothProperties_Statics::NewProp_SmoothSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UImplicitSmoothProperties_Statics::NewProp_Smoothness,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UImplicitSmoothProperties_Statics::NewProp_bPreserveUVs,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UImplicitSmoothProperties_Statics::NewProp_VolumeCorrection,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UImplicitSmoothProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UImplicitSmoothProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UImplicitSmoothProperties_Statics::ClassParams = {
		&UImplicitSmoothProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UImplicitSmoothProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UImplicitSmoothProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UImplicitSmoothProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UImplicitSmoothProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UImplicitSmoothProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UImplicitSmoothProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UImplicitSmoothProperties, 2931352127);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UImplicitSmoothProperties>()
	{
		return UImplicitSmoothProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UImplicitSmoothProperties(Z_Construct_UClass_UImplicitSmoothProperties, &UImplicitSmoothProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UImplicitSmoothProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UImplicitSmoothProperties);
	void USmoothWeightMapSetProperties::StaticRegisterNativesUSmoothWeightMapSetProperties()
	{
	}
	UClass* Z_Construct_UClass_USmoothWeightMapSetProperties_NoRegister()
	{
		return USmoothWeightMapSetProperties::StaticClass();
	}
	struct Z_Construct_UClass_USmoothWeightMapSetProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MinSmoothMultiplier_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MinSmoothMultiplier;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USmoothWeightMapSetProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UWeightMapSetProperties,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USmoothWeightMapSetProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "SmoothMeshTool.h" },
		{ "ModuleRelativePath", "Public/SmoothMeshTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USmoothWeightMapSetProperties_Statics::NewProp_MinSmoothMultiplier_MetaData[] = {
		{ "Category", "WeightMap" },
		{ "Comment", "/** Fractional Minimum Smoothing Parameter in World Units, for Weight Map values of zero */" },
		{ "DisplayPriority", "5" },
		{ "ModuleRelativePath", "Public/SmoothMeshTool.h" },
		{ "ToolTip", "Fractional Minimum Smoothing Parameter in World Units, for Weight Map values of zero" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_USmoothWeightMapSetProperties_Statics::NewProp_MinSmoothMultiplier = { "MinSmoothMultiplier", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USmoothWeightMapSetProperties, MinSmoothMultiplier), METADATA_PARAMS(Z_Construct_UClass_USmoothWeightMapSetProperties_Statics::NewProp_MinSmoothMultiplier_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USmoothWeightMapSetProperties_Statics::NewProp_MinSmoothMultiplier_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USmoothWeightMapSetProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USmoothWeightMapSetProperties_Statics::NewProp_MinSmoothMultiplier,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USmoothWeightMapSetProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USmoothWeightMapSetProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USmoothWeightMapSetProperties_Statics::ClassParams = {
		&USmoothWeightMapSetProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USmoothWeightMapSetProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USmoothWeightMapSetProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_USmoothWeightMapSetProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USmoothWeightMapSetProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USmoothWeightMapSetProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USmoothWeightMapSetProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USmoothWeightMapSetProperties, 2932687275);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<USmoothWeightMapSetProperties>()
	{
		return USmoothWeightMapSetProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USmoothWeightMapSetProperties(Z_Construct_UClass_USmoothWeightMapSetProperties, &USmoothWeightMapSetProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("USmoothWeightMapSetProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USmoothWeightMapSetProperties);
	void USmoothMeshTool::StaticRegisterNativesUSmoothMeshTool()
	{
	}
	UClass* Z_Construct_UClass_USmoothMeshTool_NoRegister()
	{
		return USmoothMeshTool::StaticClass();
	}
	struct Z_Construct_UClass_USmoothMeshTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SmoothProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SmoothProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IterativeProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_IterativeProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DiffusionProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DiffusionProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImplicitProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ImplicitProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WeightMapProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WeightMapProperties;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USmoothMeshTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBaseMeshProcessingTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USmoothMeshTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Mesh Smoothing Tool\n */" },
		{ "IncludePath", "SmoothMeshTool.h" },
		{ "ModuleRelativePath", "Public/SmoothMeshTool.h" },
		{ "ToolTip", "Mesh Smoothing Tool" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USmoothMeshTool_Statics::NewProp_SmoothProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/SmoothMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USmoothMeshTool_Statics::NewProp_SmoothProperties = { "SmoothProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USmoothMeshTool, SmoothProperties), Z_Construct_UClass_USmoothMeshToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USmoothMeshTool_Statics::NewProp_SmoothProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USmoothMeshTool_Statics::NewProp_SmoothProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USmoothMeshTool_Statics::NewProp_IterativeProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/SmoothMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USmoothMeshTool_Statics::NewProp_IterativeProperties = { "IterativeProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USmoothMeshTool, IterativeProperties), Z_Construct_UClass_UIterativeSmoothProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USmoothMeshTool_Statics::NewProp_IterativeProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USmoothMeshTool_Statics::NewProp_IterativeProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USmoothMeshTool_Statics::NewProp_DiffusionProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/SmoothMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USmoothMeshTool_Statics::NewProp_DiffusionProperties = { "DiffusionProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USmoothMeshTool, DiffusionProperties), Z_Construct_UClass_UDiffusionSmoothProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USmoothMeshTool_Statics::NewProp_DiffusionProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USmoothMeshTool_Statics::NewProp_DiffusionProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USmoothMeshTool_Statics::NewProp_ImplicitProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/SmoothMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USmoothMeshTool_Statics::NewProp_ImplicitProperties = { "ImplicitProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USmoothMeshTool, ImplicitProperties), Z_Construct_UClass_UImplicitSmoothProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USmoothMeshTool_Statics::NewProp_ImplicitProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USmoothMeshTool_Statics::NewProp_ImplicitProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USmoothMeshTool_Statics::NewProp_WeightMapProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/SmoothMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USmoothMeshTool_Statics::NewProp_WeightMapProperties = { "WeightMapProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USmoothMeshTool, WeightMapProperties), Z_Construct_UClass_USmoothWeightMapSetProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USmoothMeshTool_Statics::NewProp_WeightMapProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USmoothMeshTool_Statics::NewProp_WeightMapProperties_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USmoothMeshTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USmoothMeshTool_Statics::NewProp_SmoothProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USmoothMeshTool_Statics::NewProp_IterativeProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USmoothMeshTool_Statics::NewProp_DiffusionProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USmoothMeshTool_Statics::NewProp_ImplicitProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USmoothMeshTool_Statics::NewProp_WeightMapProperties,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USmoothMeshTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USmoothMeshTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USmoothMeshTool_Statics::ClassParams = {
		&USmoothMeshTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USmoothMeshTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USmoothMeshTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_USmoothMeshTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USmoothMeshTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USmoothMeshTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USmoothMeshTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USmoothMeshTool, 3692231161);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<USmoothMeshTool>()
	{
		return USmoothMeshTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USmoothMeshTool(Z_Construct_UClass_USmoothMeshTool, &USmoothMeshTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("USmoothMeshTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USmoothMeshTool);
	void USmoothMeshToolBuilder::StaticRegisterNativesUSmoothMeshToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_USmoothMeshToolBuilder_NoRegister()
	{
		return USmoothMeshToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_USmoothMeshToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USmoothMeshToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBaseMeshProcessingToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USmoothMeshToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "SmoothMeshTool.h" },
		{ "ModuleRelativePath", "Public/SmoothMeshTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USmoothMeshToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USmoothMeshToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USmoothMeshToolBuilder_Statics::ClassParams = {
		&USmoothMeshToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_USmoothMeshToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USmoothMeshToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USmoothMeshToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USmoothMeshToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USmoothMeshToolBuilder, 2315929125);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<USmoothMeshToolBuilder>()
	{
		return USmoothMeshToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USmoothMeshToolBuilder(Z_Construct_UClass_USmoothMeshToolBuilder, &USmoothMeshToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("USmoothMeshToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USmoothMeshToolBuilder);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
