// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MODELINGCOMPONENTS_ConstructionPlaneMechanic_generated_h
#error "ConstructionPlaneMechanic.generated.h already included, missing '#pragma once' in ConstructionPlaneMechanic.h"
#endif
#define MODELINGCOMPONENTS_ConstructionPlaneMechanic_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_ConstructionPlaneMechanic_h_27_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_ConstructionPlaneMechanic_h_27_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_ConstructionPlaneMechanic_h_27_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_ConstructionPlaneMechanic_h_27_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUConstructionPlaneMechanic(); \
	friend struct Z_Construct_UClass_UConstructionPlaneMechanic_Statics; \
public: \
	DECLARE_CLASS(UConstructionPlaneMechanic, UInteractionMechanic, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UConstructionPlaneMechanic)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_ConstructionPlaneMechanic_h_27_INCLASS \
private: \
	static void StaticRegisterNativesUConstructionPlaneMechanic(); \
	friend struct Z_Construct_UClass_UConstructionPlaneMechanic_Statics; \
public: \
	DECLARE_CLASS(UConstructionPlaneMechanic, UInteractionMechanic, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UConstructionPlaneMechanic)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_ConstructionPlaneMechanic_h_27_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UConstructionPlaneMechanic(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UConstructionPlaneMechanic) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConstructionPlaneMechanic); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConstructionPlaneMechanic); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConstructionPlaneMechanic(UConstructionPlaneMechanic&&); \
	NO_API UConstructionPlaneMechanic(const UConstructionPlaneMechanic&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_ConstructionPlaneMechanic_h_27_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UConstructionPlaneMechanic() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConstructionPlaneMechanic(UConstructionPlaneMechanic&&); \
	NO_API UConstructionPlaneMechanic(const UConstructionPlaneMechanic&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConstructionPlaneMechanic); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConstructionPlaneMechanic); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UConstructionPlaneMechanic)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_ConstructionPlaneMechanic_h_27_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ClickToSetPlaneBehavior() { return STRUCT_OFFSET(UConstructionPlaneMechanic, ClickToSetPlaneBehavior); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_ConstructionPlaneMechanic_h_24_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_ConstructionPlaneMechanic_h_27_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_ConstructionPlaneMechanic_h_27_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_ConstructionPlaneMechanic_h_27_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_ConstructionPlaneMechanic_h_27_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_ConstructionPlaneMechanic_h_27_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_ConstructionPlaneMechanic_h_27_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_ConstructionPlaneMechanic_h_27_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_ConstructionPlaneMechanic_h_27_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_ConstructionPlaneMechanic_h_27_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_ConstructionPlaneMechanic_h_27_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_ConstructionPlaneMechanic_h_27_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_ConstructionPlaneMechanic_h_27_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MODELINGCOMPONENTS_API UClass* StaticClass<class UConstructionPlaneMechanic>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_ConstructionPlaneMechanic_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
