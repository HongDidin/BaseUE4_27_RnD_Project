// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLSEDITORONLY_MeshTangentsTool_generated_h
#error "MeshTangentsTool.generated.h already included, missing '#pragma once' in MeshTangentsTool.h"
#endif
#define MESHMODELINGTOOLSEDITORONLY_MeshTangentsTool_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_30_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_30_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_30_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_30_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshTangentsToolBuilder(); \
	friend struct Z_Construct_UClass_UMeshTangentsToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UMeshTangentsToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(UMeshTangentsToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_30_INCLASS \
private: \
	static void StaticRegisterNativesUMeshTangentsToolBuilder(); \
	friend struct Z_Construct_UClass_UMeshTangentsToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UMeshTangentsToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(UMeshTangentsToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_30_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshTangentsToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshTangentsToolBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshTangentsToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshTangentsToolBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshTangentsToolBuilder(UMeshTangentsToolBuilder&&); \
	NO_API UMeshTangentsToolBuilder(const UMeshTangentsToolBuilder&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_30_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshTangentsToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshTangentsToolBuilder(UMeshTangentsToolBuilder&&); \
	NO_API UMeshTangentsToolBuilder(const UMeshTangentsToolBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshTangentsToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshTangentsToolBuilder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshTangentsToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_30_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_27_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_30_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_30_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_30_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_30_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_30_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_30_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_30_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_30_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_30_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_30_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_30_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_30_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<class UMeshTangentsToolBuilder>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_42_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_42_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_42_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_42_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshTangentsToolProperties(); \
	friend struct Z_Construct_UClass_UMeshTangentsToolProperties_Statics; \
public: \
	DECLARE_CLASS(UMeshTangentsToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(UMeshTangentsToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_42_INCLASS \
private: \
	static void StaticRegisterNativesUMeshTangentsToolProperties(); \
	friend struct Z_Construct_UClass_UMeshTangentsToolProperties_Statics; \
public: \
	DECLARE_CLASS(UMeshTangentsToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(UMeshTangentsToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_42_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshTangentsToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshTangentsToolProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshTangentsToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshTangentsToolProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshTangentsToolProperties(UMeshTangentsToolProperties&&); \
	NO_API UMeshTangentsToolProperties(const UMeshTangentsToolProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_42_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshTangentsToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshTangentsToolProperties(UMeshTangentsToolProperties&&); \
	NO_API UMeshTangentsToolProperties(const UMeshTangentsToolProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshTangentsToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshTangentsToolProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshTangentsToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_42_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_39_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_42_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_42_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_42_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_42_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_42_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_42_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_42_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_42_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_42_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_42_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_42_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_42_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<class UMeshTangentsToolProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_83_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_83_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_83_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_83_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshTangentsTool(); \
	friend struct Z_Construct_UClass_UMeshTangentsTool_Statics; \
public: \
	DECLARE_CLASS(UMeshTangentsTool, USingleSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(UMeshTangentsTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_83_INCLASS \
private: \
	static void StaticRegisterNativesUMeshTangentsTool(); \
	friend struct Z_Construct_UClass_UMeshTangentsTool_Statics; \
public: \
	DECLARE_CLASS(UMeshTangentsTool, USingleSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(UMeshTangentsTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_83_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshTangentsTool(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshTangentsTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshTangentsTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshTangentsTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshTangentsTool(UMeshTangentsTool&&); \
	NO_API UMeshTangentsTool(const UMeshTangentsTool&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_83_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshTangentsTool(UMeshTangentsTool&&); \
	NO_API UMeshTangentsTool(const UMeshTangentsTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshTangentsTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshTangentsTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMeshTangentsTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_83_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Settings() { return STRUCT_OFFSET(UMeshTangentsTool, Settings); } \
	FORCEINLINE static uint32 __PPO__DefaultMaterial() { return STRUCT_OFFSET(UMeshTangentsTool, DefaultMaterial); } \
	FORCEINLINE static uint32 __PPO__PreviewMesh() { return STRUCT_OFFSET(UMeshTangentsTool, PreviewMesh); } \
	FORCEINLINE static uint32 __PPO__PreviewGeometry() { return STRUCT_OFFSET(UMeshTangentsTool, PreviewGeometry); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_80_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_83_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_83_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_83_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_83_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_83_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_83_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_83_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_83_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_83_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_83_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_83_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h_83_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<class UMeshTangentsTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshTangentsTool_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
