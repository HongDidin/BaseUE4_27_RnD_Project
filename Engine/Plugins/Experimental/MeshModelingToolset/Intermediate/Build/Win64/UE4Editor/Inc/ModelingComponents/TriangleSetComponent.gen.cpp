// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ModelingComponents/Public/Drawing/TriangleSetComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTriangleSetComponent() {}
// Cross Module References
	MODELINGCOMPONENTS_API UScriptStruct* Z_Construct_UScriptStruct_FRenderableTriangle();
	UPackage* Z_Construct_UPackage__Script_ModelingComponents();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	MODELINGCOMPONENTS_API UScriptStruct* Z_Construct_UScriptStruct_FRenderableTriangleVertex();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FColor();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UTriangleSetComponent_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UTriangleSetComponent();
	ENGINE_API UClass* Z_Construct_UClass_UMeshComponent();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FBoxSphereBounds();
// End Cross Module References
class UScriptStruct* FRenderableTriangle::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MODELINGCOMPONENTS_API uint32 Get_Z_Construct_UScriptStruct_FRenderableTriangle_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRenderableTriangle, Z_Construct_UPackage__Script_ModelingComponents(), TEXT("RenderableTriangle"), sizeof(FRenderableTriangle), Get_Z_Construct_UScriptStruct_FRenderableTriangle_Hash());
	}
	return Singleton;
}
template<> MODELINGCOMPONENTS_API UScriptStruct* StaticStruct<FRenderableTriangle>()
{
	return FRenderableTriangle::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRenderableTriangle(FRenderableTriangle::StaticStruct, TEXT("/Script/ModelingComponents"), TEXT("RenderableTriangle"), false, nullptr, nullptr);
static struct FScriptStruct_ModelingComponents_StaticRegisterNativesFRenderableTriangle
{
	FScriptStruct_ModelingComponents_StaticRegisterNativesFRenderableTriangle()
	{
		UScriptStruct::DeferCppStructOps<FRenderableTriangle>(FName(TEXT("RenderableTriangle")));
	}
} ScriptStruct_ModelingComponents_StaticRegisterNativesFRenderableTriangle;
	struct Z_Construct_UScriptStruct_FRenderableTriangle_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Material_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Material;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Vertex0_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Vertex0;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Vertex1_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Vertex1;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Vertex2_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Vertex2;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRenderableTriangle_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Drawing/TriangleSetComponent.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRenderableTriangle_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRenderableTriangle>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRenderableTriangle_Statics::NewProp_Material_MetaData[] = {
		{ "ModuleRelativePath", "Public/Drawing/TriangleSetComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FRenderableTriangle_Statics::NewProp_Material = { "Material", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRenderableTriangle, Material), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FRenderableTriangle_Statics::NewProp_Material_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRenderableTriangle_Statics::NewProp_Material_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRenderableTriangle_Statics::NewProp_Vertex0_MetaData[] = {
		{ "ModuleRelativePath", "Public/Drawing/TriangleSetComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRenderableTriangle_Statics::NewProp_Vertex0 = { "Vertex0", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRenderableTriangle, Vertex0), Z_Construct_UScriptStruct_FRenderableTriangleVertex, METADATA_PARAMS(Z_Construct_UScriptStruct_FRenderableTriangle_Statics::NewProp_Vertex0_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRenderableTriangle_Statics::NewProp_Vertex0_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRenderableTriangle_Statics::NewProp_Vertex1_MetaData[] = {
		{ "ModuleRelativePath", "Public/Drawing/TriangleSetComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRenderableTriangle_Statics::NewProp_Vertex1 = { "Vertex1", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRenderableTriangle, Vertex1), Z_Construct_UScriptStruct_FRenderableTriangleVertex, METADATA_PARAMS(Z_Construct_UScriptStruct_FRenderableTriangle_Statics::NewProp_Vertex1_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRenderableTriangle_Statics::NewProp_Vertex1_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRenderableTriangle_Statics::NewProp_Vertex2_MetaData[] = {
		{ "ModuleRelativePath", "Public/Drawing/TriangleSetComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRenderableTriangle_Statics::NewProp_Vertex2 = { "Vertex2", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRenderableTriangle, Vertex2), Z_Construct_UScriptStruct_FRenderableTriangleVertex, METADATA_PARAMS(Z_Construct_UScriptStruct_FRenderableTriangle_Statics::NewProp_Vertex2_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRenderableTriangle_Statics::NewProp_Vertex2_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRenderableTriangle_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRenderableTriangle_Statics::NewProp_Material,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRenderableTriangle_Statics::NewProp_Vertex0,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRenderableTriangle_Statics::NewProp_Vertex1,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRenderableTriangle_Statics::NewProp_Vertex2,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRenderableTriangle_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ModelingComponents,
		nullptr,
		&NewStructOps,
		"RenderableTriangle",
		sizeof(FRenderableTriangle),
		alignof(FRenderableTriangle),
		Z_Construct_UScriptStruct_FRenderableTriangle_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRenderableTriangle_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRenderableTriangle_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRenderableTriangle_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRenderableTriangle()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRenderableTriangle_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ModelingComponents();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RenderableTriangle"), sizeof(FRenderableTriangle), Get_Z_Construct_UScriptStruct_FRenderableTriangle_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRenderableTriangle_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRenderableTriangle_Hash() { return 2961394680U; }
class UScriptStruct* FRenderableTriangleVertex::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MODELINGCOMPONENTS_API uint32 Get_Z_Construct_UScriptStruct_FRenderableTriangleVertex_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRenderableTriangleVertex, Z_Construct_UPackage__Script_ModelingComponents(), TEXT("RenderableTriangleVertex"), sizeof(FRenderableTriangleVertex), Get_Z_Construct_UScriptStruct_FRenderableTriangleVertex_Hash());
	}
	return Singleton;
}
template<> MODELINGCOMPONENTS_API UScriptStruct* StaticStruct<FRenderableTriangleVertex>()
{
	return FRenderableTriangleVertex::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRenderableTriangleVertex(FRenderableTriangleVertex::StaticStruct, TEXT("/Script/ModelingComponents"), TEXT("RenderableTriangleVertex"), false, nullptr, nullptr);
static struct FScriptStruct_ModelingComponents_StaticRegisterNativesFRenderableTriangleVertex
{
	FScriptStruct_ModelingComponents_StaticRegisterNativesFRenderableTriangleVertex()
	{
		UScriptStruct::DeferCppStructOps<FRenderableTriangleVertex>(FName(TEXT("RenderableTriangleVertex")));
	}
} ScriptStruct_ModelingComponents_StaticRegisterNativesFRenderableTriangleVertex;
	struct Z_Construct_UScriptStruct_FRenderableTriangleVertex_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Position_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Position;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UV_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_UV;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Normal_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Normal;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Color_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Color;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRenderableTriangleVertex_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Drawing/TriangleSetComponent.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRenderableTriangleVertex_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRenderableTriangleVertex>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRenderableTriangleVertex_Statics::NewProp_Position_MetaData[] = {
		{ "ModuleRelativePath", "Public/Drawing/TriangleSetComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRenderableTriangleVertex_Statics::NewProp_Position = { "Position", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRenderableTriangleVertex, Position), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRenderableTriangleVertex_Statics::NewProp_Position_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRenderableTriangleVertex_Statics::NewProp_Position_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRenderableTriangleVertex_Statics::NewProp_UV_MetaData[] = {
		{ "ModuleRelativePath", "Public/Drawing/TriangleSetComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRenderableTriangleVertex_Statics::NewProp_UV = { "UV", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRenderableTriangleVertex, UV), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UScriptStruct_FRenderableTriangleVertex_Statics::NewProp_UV_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRenderableTriangleVertex_Statics::NewProp_UV_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRenderableTriangleVertex_Statics::NewProp_Normal_MetaData[] = {
		{ "ModuleRelativePath", "Public/Drawing/TriangleSetComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRenderableTriangleVertex_Statics::NewProp_Normal = { "Normal", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRenderableTriangleVertex, Normal), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRenderableTriangleVertex_Statics::NewProp_Normal_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRenderableTriangleVertex_Statics::NewProp_Normal_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRenderableTriangleVertex_Statics::NewProp_Color_MetaData[] = {
		{ "ModuleRelativePath", "Public/Drawing/TriangleSetComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRenderableTriangleVertex_Statics::NewProp_Color = { "Color", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRenderableTriangleVertex, Color), Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(Z_Construct_UScriptStruct_FRenderableTriangleVertex_Statics::NewProp_Color_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRenderableTriangleVertex_Statics::NewProp_Color_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRenderableTriangleVertex_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRenderableTriangleVertex_Statics::NewProp_Position,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRenderableTriangleVertex_Statics::NewProp_UV,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRenderableTriangleVertex_Statics::NewProp_Normal,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRenderableTriangleVertex_Statics::NewProp_Color,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRenderableTriangleVertex_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ModelingComponents,
		nullptr,
		&NewStructOps,
		"RenderableTriangleVertex",
		sizeof(FRenderableTriangleVertex),
		alignof(FRenderableTriangleVertex),
		Z_Construct_UScriptStruct_FRenderableTriangleVertex_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRenderableTriangleVertex_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRenderableTriangleVertex_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRenderableTriangleVertex_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRenderableTriangleVertex()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRenderableTriangleVertex_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ModelingComponents();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RenderableTriangleVertex"), sizeof(FRenderableTriangleVertex), Get_Z_Construct_UScriptStruct_FRenderableTriangleVertex_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRenderableTriangleVertex_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRenderableTriangleVertex_Hash() { return 3370593619U; }
	void UTriangleSetComponent::StaticRegisterNativesUTriangleSetComponent()
	{
	}
	UClass* Z_Construct_UClass_UTriangleSetComponent_NoRegister()
	{
		return UTriangleSetComponent::StaticClass();
	}
	struct Z_Construct_UClass_UTriangleSetComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Bounds_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Bounds;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bBoundsDirty_MetaData[];
#endif
		static void NewProp_bBoundsDirty_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bBoundsDirty;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTriangleSetComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_ModelingComponents,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTriangleSetComponent_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n* A component for rendering an arbitrary assortment of triangles. Suitable, for instance, for rendering highlighted faces.\n*/" },
		{ "HideCategories", "Mobility Trigger" },
		{ "IncludePath", "Drawing/TriangleSetComponent.h" },
		{ "ModuleRelativePath", "Public/Drawing/TriangleSetComponent.h" },
		{ "ToolTip", "A component for rendering an arbitrary assortment of triangles. Suitable, for instance, for rendering highlighted faces." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTriangleSetComponent_Statics::NewProp_Bounds_MetaData[] = {
		{ "ModuleRelativePath", "Public/Drawing/TriangleSetComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UTriangleSetComponent_Statics::NewProp_Bounds = { "Bounds", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTriangleSetComponent, Bounds), Z_Construct_UScriptStruct_FBoxSphereBounds, METADATA_PARAMS(Z_Construct_UClass_UTriangleSetComponent_Statics::NewProp_Bounds_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTriangleSetComponent_Statics::NewProp_Bounds_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTriangleSetComponent_Statics::NewProp_bBoundsDirty_MetaData[] = {
		{ "ModuleRelativePath", "Public/Drawing/TriangleSetComponent.h" },
	};
#endif
	void Z_Construct_UClass_UTriangleSetComponent_Statics::NewProp_bBoundsDirty_SetBit(void* Obj)
	{
		((UTriangleSetComponent*)Obj)->bBoundsDirty = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UTriangleSetComponent_Statics::NewProp_bBoundsDirty = { "bBoundsDirty", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UTriangleSetComponent), &Z_Construct_UClass_UTriangleSetComponent_Statics::NewProp_bBoundsDirty_SetBit, METADATA_PARAMS(Z_Construct_UClass_UTriangleSetComponent_Statics::NewProp_bBoundsDirty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTriangleSetComponent_Statics::NewProp_bBoundsDirty_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTriangleSetComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTriangleSetComponent_Statics::NewProp_Bounds,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTriangleSetComponent_Statics::NewProp_bBoundsDirty,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTriangleSetComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTriangleSetComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTriangleSetComponent_Statics::ClassParams = {
		&UTriangleSetComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UTriangleSetComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UTriangleSetComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UTriangleSetComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTriangleSetComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTriangleSetComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTriangleSetComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTriangleSetComponent, 3978518380);
	template<> MODELINGCOMPONENTS_API UClass* StaticClass<UTriangleSetComponent>()
	{
		return UTriangleSetComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTriangleSetComponent(Z_Construct_UClass_UTriangleSetComponent, &UTriangleSetComponent::StaticClass, TEXT("/Script/ModelingComponents"), TEXT("UTriangleSetComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTriangleSetComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
