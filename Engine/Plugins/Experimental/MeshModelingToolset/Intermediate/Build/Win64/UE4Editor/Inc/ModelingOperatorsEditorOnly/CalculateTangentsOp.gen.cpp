// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ModelingOperatorsEditorOnly/Public/ParameterizationOps/CalculateTangentsOp.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCalculateTangentsOp() {}
// Cross Module References
	MODELINGOPERATORSEDITORONLY_API UEnum* Z_Construct_UEnum_ModelingOperatorsEditorOnly_EMeshTangentsType();
	UPackage* Z_Construct_UPackage__Script_ModelingOperatorsEditorOnly();
// End Cross Module References
	static UEnum* EMeshTangentsType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ModelingOperatorsEditorOnly_EMeshTangentsType, Z_Construct_UPackage__Script_ModelingOperatorsEditorOnly(), TEXT("EMeshTangentsType"));
		}
		return Singleton;
	}
	template<> MODELINGOPERATORSEDITORONLY_API UEnum* StaticEnum<EMeshTangentsType>()
	{
		return EMeshTangentsType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMeshTangentsType(EMeshTangentsType_StaticEnum, TEXT("/Script/ModelingOperatorsEditorOnly"), TEXT("EMeshTangentsType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ModelingOperatorsEditorOnly_EMeshTangentsType_Hash() { return 26565319U; }
	UEnum* Z_Construct_UEnum_ModelingOperatorsEditorOnly_EMeshTangentsType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ModelingOperatorsEditorOnly();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMeshTangentsType"), 0, Get_Z_Construct_UEnum_ModelingOperatorsEditorOnly_EMeshTangentsType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMeshTangentsType::MikkTSpace", (int64)EMeshTangentsType::MikkTSpace },
				{ "EMeshTangentsType::FastMikkTSpace", (int64)EMeshTangentsType::FastMikkTSpace },
				{ "EMeshTangentsType::PerTriangle", (int64)EMeshTangentsType::PerTriangle },
				{ "EMeshTangentsType::CopyExisting", (int64)EMeshTangentsType::CopyExisting },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "CopyExisting.Comment", "/** Use Source Mesh Tangents */" },
				{ "CopyExisting.Name", "EMeshTangentsType::CopyExisting" },
				{ "CopyExisting.ToolTip", "Use Source Mesh Tangents" },
				{ "FastMikkTSpace.Comment", "/** MikkT-like Blended Per-Triangle Tangents, with Blending based on existing Mesh/UV/Normal Topology */" },
				{ "FastMikkTSpace.Name", "EMeshTangentsType::FastMikkTSpace" },
				{ "FastMikkTSpace.ToolTip", "MikkT-like Blended Per-Triangle Tangents, with Blending based on existing Mesh/UV/Normal Topology" },
				{ "MikkTSpace.Comment", "/** Standard MikkTSpace Tangent Calculation */" },
				{ "MikkTSpace.Name", "EMeshTangentsType::MikkTSpace" },
				{ "MikkTSpace.ToolTip", "Standard MikkTSpace Tangent Calculation" },
				{ "ModuleRelativePath", "Public/ParameterizationOps/CalculateTangentsOp.h" },
				{ "PerTriangle.Comment", "/** Project per-triangle Tangents projected onto Normals */" },
				{ "PerTriangle.Name", "EMeshTangentsType::PerTriangle" },
				{ "PerTriangle.ToolTip", "Project per-triangle Tangents projected onto Normals" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ModelingOperatorsEditorOnly,
				nullptr,
				"EMeshTangentsType",
				"EMeshTangentsType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
