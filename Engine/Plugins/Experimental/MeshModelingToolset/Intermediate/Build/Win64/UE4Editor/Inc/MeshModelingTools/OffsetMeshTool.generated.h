// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLS_OffsetMeshTool_generated_h
#error "OffsetMeshTool.generated.h already included, missing '#pragma once' in OffsetMeshTool.h"
#endif
#define MESHMODELINGTOOLS_OffsetMeshTool_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_27_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_27_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_27_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_27_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOffsetMeshToolProperties(); \
	friend struct Z_Construct_UClass_UOffsetMeshToolProperties_Statics; \
public: \
	DECLARE_CLASS(UOffsetMeshToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UOffsetMeshToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_27_INCLASS \
private: \
	static void StaticRegisterNativesUOffsetMeshToolProperties(); \
	friend struct Z_Construct_UClass_UOffsetMeshToolProperties_Statics; \
public: \
	DECLARE_CLASS(UOffsetMeshToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UOffsetMeshToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_27_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOffsetMeshToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOffsetMeshToolProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOffsetMeshToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOffsetMeshToolProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOffsetMeshToolProperties(UOffsetMeshToolProperties&&); \
	NO_API UOffsetMeshToolProperties(const UOffsetMeshToolProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_27_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOffsetMeshToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOffsetMeshToolProperties(UOffsetMeshToolProperties&&); \
	NO_API UOffsetMeshToolProperties(const UOffsetMeshToolProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOffsetMeshToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOffsetMeshToolProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOffsetMeshToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_27_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_24_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_27_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_27_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_27_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_27_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_27_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_27_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_27_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_27_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_27_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_27_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_27_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_27_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UOffsetMeshToolProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_48_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_48_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_48_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_48_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOffsetWeightMapSetProperties(); \
	friend struct Z_Construct_UClass_UOffsetWeightMapSetProperties_Statics; \
public: \
	DECLARE_CLASS(UOffsetWeightMapSetProperties, UWeightMapSetProperties, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UOffsetWeightMapSetProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_48_INCLASS \
private: \
	static void StaticRegisterNativesUOffsetWeightMapSetProperties(); \
	friend struct Z_Construct_UClass_UOffsetWeightMapSetProperties_Statics; \
public: \
	DECLARE_CLASS(UOffsetWeightMapSetProperties, UWeightMapSetProperties, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UOffsetWeightMapSetProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_48_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOffsetWeightMapSetProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOffsetWeightMapSetProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOffsetWeightMapSetProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOffsetWeightMapSetProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOffsetWeightMapSetProperties(UOffsetWeightMapSetProperties&&); \
	NO_API UOffsetWeightMapSetProperties(const UOffsetWeightMapSetProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_48_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOffsetWeightMapSetProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOffsetWeightMapSetProperties(UOffsetWeightMapSetProperties&&); \
	NO_API UOffsetWeightMapSetProperties(const UOffsetWeightMapSetProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOffsetWeightMapSetProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOffsetWeightMapSetProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOffsetWeightMapSetProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_48_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_45_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_48_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_48_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_48_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_48_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_48_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_48_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_48_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_48_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_48_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_48_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_48_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_48_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UOffsetWeightMapSetProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_61_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_61_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_61_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_61_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUIterativeOffsetProperties(); \
	friend struct Z_Construct_UClass_UIterativeOffsetProperties_Statics; \
public: \
	DECLARE_CLASS(UIterativeOffsetProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UIterativeOffsetProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_61_INCLASS \
private: \
	static void StaticRegisterNativesUIterativeOffsetProperties(); \
	friend struct Z_Construct_UClass_UIterativeOffsetProperties_Statics; \
public: \
	DECLARE_CLASS(UIterativeOffsetProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UIterativeOffsetProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_61_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UIterativeOffsetProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UIterativeOffsetProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UIterativeOffsetProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UIterativeOffsetProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UIterativeOffsetProperties(UIterativeOffsetProperties&&); \
	NO_API UIterativeOffsetProperties(const UIterativeOffsetProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_61_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UIterativeOffsetProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UIterativeOffsetProperties(UIterativeOffsetProperties&&); \
	NO_API UIterativeOffsetProperties(const UIterativeOffsetProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UIterativeOffsetProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UIterativeOffsetProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UIterativeOffsetProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_61_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_58_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_61_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_61_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_61_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_61_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_61_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_61_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_61_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_61_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_61_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_61_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_61_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_61_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UIterativeOffsetProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_86_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_86_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_86_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_86_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUImplicitOffsetProperties(); \
	friend struct Z_Construct_UClass_UImplicitOffsetProperties_Statics; \
public: \
	DECLARE_CLASS(UImplicitOffsetProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UImplicitOffsetProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_86_INCLASS \
private: \
	static void StaticRegisterNativesUImplicitOffsetProperties(); \
	friend struct Z_Construct_UClass_UImplicitOffsetProperties_Statics; \
public: \
	DECLARE_CLASS(UImplicitOffsetProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UImplicitOffsetProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_86_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UImplicitOffsetProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UImplicitOffsetProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UImplicitOffsetProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UImplicitOffsetProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UImplicitOffsetProperties(UImplicitOffsetProperties&&); \
	NO_API UImplicitOffsetProperties(const UImplicitOffsetProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_86_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UImplicitOffsetProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UImplicitOffsetProperties(UImplicitOffsetProperties&&); \
	NO_API UImplicitOffsetProperties(const UImplicitOffsetProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UImplicitOffsetProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UImplicitOffsetProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UImplicitOffsetProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_86_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_83_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_86_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_86_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_86_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_86_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_86_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_86_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_86_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_86_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_86_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_86_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_86_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_86_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UImplicitOffsetProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_109_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_109_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_109_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_109_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOffsetMeshTool(); \
	friend struct Z_Construct_UClass_UOffsetMeshTool_Statics; \
public: \
	DECLARE_CLASS(UOffsetMeshTool, UBaseMeshProcessingTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UOffsetMeshTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_109_INCLASS \
private: \
	static void StaticRegisterNativesUOffsetMeshTool(); \
	friend struct Z_Construct_UClass_UOffsetMeshTool_Statics; \
public: \
	DECLARE_CLASS(UOffsetMeshTool, UBaseMeshProcessingTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UOffsetMeshTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_109_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOffsetMeshTool(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOffsetMeshTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOffsetMeshTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOffsetMeshTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOffsetMeshTool(UOffsetMeshTool&&); \
	NO_API UOffsetMeshTool(const UOffsetMeshTool&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_109_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOffsetMeshTool(UOffsetMeshTool&&); \
	NO_API UOffsetMeshTool(const UOffsetMeshTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOffsetMeshTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOffsetMeshTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UOffsetMeshTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_109_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__OffsetProperties() { return STRUCT_OFFSET(UOffsetMeshTool, OffsetProperties); } \
	FORCEINLINE static uint32 __PPO__IterativeProperties() { return STRUCT_OFFSET(UOffsetMeshTool, IterativeProperties); } \
	FORCEINLINE static uint32 __PPO__ImplicitProperties() { return STRUCT_OFFSET(UOffsetMeshTool, ImplicitProperties); } \
	FORCEINLINE static uint32 __PPO__WeightMapProperties() { return STRUCT_OFFSET(UOffsetMeshTool, WeightMapProperties); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_106_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_109_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_109_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_109_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_109_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_109_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_109_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_109_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_109_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_109_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_109_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_109_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_109_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UOffsetMeshTool>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_149_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_149_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_149_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_149_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOffsetMeshToolBuilder(); \
	friend struct Z_Construct_UClass_UOffsetMeshToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UOffsetMeshToolBuilder, UBaseMeshProcessingToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UOffsetMeshToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_149_INCLASS \
private: \
	static void StaticRegisterNativesUOffsetMeshToolBuilder(); \
	friend struct Z_Construct_UClass_UOffsetMeshToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UOffsetMeshToolBuilder, UBaseMeshProcessingToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UOffsetMeshToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_149_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOffsetMeshToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOffsetMeshToolBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOffsetMeshToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOffsetMeshToolBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOffsetMeshToolBuilder(UOffsetMeshToolBuilder&&); \
	NO_API UOffsetMeshToolBuilder(const UOffsetMeshToolBuilder&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_149_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOffsetMeshToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOffsetMeshToolBuilder(UOffsetMeshToolBuilder&&); \
	NO_API UOffsetMeshToolBuilder(const UOffsetMeshToolBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOffsetMeshToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOffsetMeshToolBuilder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOffsetMeshToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_149_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_146_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_149_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_149_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_149_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_149_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_149_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_149_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_149_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_149_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_149_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_149_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_149_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h_149_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UOffsetMeshToolBuilder>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_OffsetMeshTool_h


#define FOREACH_ENUM_EOFFSETMESHTOOLOFFSETTYPE(op) \
	op(EOffsetMeshToolOffsetType::Iterative) \
	op(EOffsetMeshToolOffsetType::Implicit) 

enum class EOffsetMeshToolOffsetType : uint8;
template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EOffsetMeshToolOffsetType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
