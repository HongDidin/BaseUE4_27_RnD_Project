// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ModelingComponents/Public/Mechanics/SpatialCurveDistanceMechanic.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSpatialCurveDistanceMechanic() {}
// Cross Module References
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_USpatialCurveDistanceMechanic_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_USpatialCurveDistanceMechanic();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractionMechanic();
	UPackage* Z_Construct_UPackage__Script_ModelingComponents();
// End Cross Module References
	void USpatialCurveDistanceMechanic::StaticRegisterNativesUSpatialCurveDistanceMechanic()
	{
	}
	UClass* Z_Construct_UClass_USpatialCurveDistanceMechanic_NoRegister()
	{
		return USpatialCurveDistanceMechanic::StaticClass();
	}
	struct Z_Construct_UClass_USpatialCurveDistanceMechanic_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USpatialCurveDistanceMechanic_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractionMechanic,
		(UObject* (*)())Z_Construct_UPackage__Script_ModelingComponents,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USpatialCurveDistanceMechanic_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "Mechanics/SpatialCurveDistanceMechanic.h" },
		{ "ModuleRelativePath", "Public/Mechanics/SpatialCurveDistanceMechanic.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USpatialCurveDistanceMechanic_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USpatialCurveDistanceMechanic>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USpatialCurveDistanceMechanic_Statics::ClassParams = {
		&USpatialCurveDistanceMechanic::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_USpatialCurveDistanceMechanic_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USpatialCurveDistanceMechanic_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USpatialCurveDistanceMechanic()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USpatialCurveDistanceMechanic_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USpatialCurveDistanceMechanic, 233643761);
	template<> MODELINGCOMPONENTS_API UClass* StaticClass<USpatialCurveDistanceMechanic>()
	{
		return USpatialCurveDistanceMechanic::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USpatialCurveDistanceMechanic(Z_Construct_UClass_USpatialCurveDistanceMechanic, &USpatialCurveDistanceMechanic::StaticClass, TEXT("/Script/ModelingComponents"), TEXT("USpatialCurveDistanceMechanic"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USpatialCurveDistanceMechanic);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
