// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLSEDITORONLY_SimplifyMeshTool_generated_h
#error "SimplifyMeshTool.generated.h already included, missing '#pragma once' in SimplifyMeshTool.h"
#endif
#define MESHMODELINGTOOLSEDITORONLY_SimplifyMeshTool_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_27_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_27_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_27_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_27_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSimplifyMeshToolBuilder(); \
	friend struct Z_Construct_UClass_USimplifyMeshToolBuilder_Statics; \
public: \
	DECLARE_CLASS(USimplifyMeshToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(USimplifyMeshToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_27_INCLASS \
private: \
	static void StaticRegisterNativesUSimplifyMeshToolBuilder(); \
	friend struct Z_Construct_UClass_USimplifyMeshToolBuilder_Statics; \
public: \
	DECLARE_CLASS(USimplifyMeshToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(USimplifyMeshToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_27_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USimplifyMeshToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USimplifyMeshToolBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USimplifyMeshToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USimplifyMeshToolBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USimplifyMeshToolBuilder(USimplifyMeshToolBuilder&&); \
	NO_API USimplifyMeshToolBuilder(const USimplifyMeshToolBuilder&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_27_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USimplifyMeshToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USimplifyMeshToolBuilder(USimplifyMeshToolBuilder&&); \
	NO_API USimplifyMeshToolBuilder(const USimplifyMeshToolBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USimplifyMeshToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USimplifyMeshToolBuilder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USimplifyMeshToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_27_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_24_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_27_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_27_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_27_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_27_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_27_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_27_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_27_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_27_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_27_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_27_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_27_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_27_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<class USimplifyMeshToolBuilder>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_44_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_44_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_44_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_44_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSimplifyMeshToolProperties(); \
	friend struct Z_Construct_UClass_USimplifyMeshToolProperties_Statics; \
public: \
	DECLARE_CLASS(USimplifyMeshToolProperties, UMeshConstraintProperties, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(USimplifyMeshToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_44_INCLASS \
private: \
	static void StaticRegisterNativesUSimplifyMeshToolProperties(); \
	friend struct Z_Construct_UClass_USimplifyMeshToolProperties_Statics; \
public: \
	DECLARE_CLASS(USimplifyMeshToolProperties, UMeshConstraintProperties, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(USimplifyMeshToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_44_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USimplifyMeshToolProperties(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USimplifyMeshToolProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USimplifyMeshToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USimplifyMeshToolProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USimplifyMeshToolProperties(USimplifyMeshToolProperties&&); \
	NO_API USimplifyMeshToolProperties(const USimplifyMeshToolProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_44_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USimplifyMeshToolProperties(USimplifyMeshToolProperties&&); \
	NO_API USimplifyMeshToolProperties(const USimplifyMeshToolProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USimplifyMeshToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USimplifyMeshToolProperties); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(USimplifyMeshToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_44_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_41_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_44_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_44_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_44_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_44_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_44_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_44_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_44_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_44_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_44_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_44_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_44_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_44_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<class USimplifyMeshToolProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_94_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_94_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_94_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_94_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSimplifyMeshTool(); \
	friend struct Z_Construct_UClass_USimplifyMeshTool_Statics; \
public: \
	DECLARE_CLASS(USimplifyMeshTool, USingleSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(USimplifyMeshTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_94_INCLASS \
private: \
	static void StaticRegisterNativesUSimplifyMeshTool(); \
	friend struct Z_Construct_UClass_USimplifyMeshTool_Statics; \
public: \
	DECLARE_CLASS(USimplifyMeshTool, USingleSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(USimplifyMeshTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_94_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USimplifyMeshTool(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USimplifyMeshTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USimplifyMeshTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USimplifyMeshTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USimplifyMeshTool(USimplifyMeshTool&&); \
	NO_API USimplifyMeshTool(const USimplifyMeshTool&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_94_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USimplifyMeshTool() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USimplifyMeshTool(USimplifyMeshTool&&); \
	NO_API USimplifyMeshTool(const USimplifyMeshTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USimplifyMeshTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USimplifyMeshTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(USimplifyMeshTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_94_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SimplifyProperties() { return STRUCT_OFFSET(USimplifyMeshTool, SimplifyProperties); } \
	FORCEINLINE static uint32 __PPO__MeshStatisticsProperties() { return STRUCT_OFFSET(USimplifyMeshTool, MeshStatisticsProperties); } \
	FORCEINLINE static uint32 __PPO__Preview() { return STRUCT_OFFSET(USimplifyMeshTool, Preview); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_91_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_94_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_94_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_94_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_94_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_94_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_94_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_94_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_94_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_94_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_94_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_94_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h_94_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<class USimplifyMeshTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_SimplifyMeshTool_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
