// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLS_MeshVertexSculptTool_generated_h
#error "MeshVertexSculptTool.generated.h already included, missing '#pragma once' in MeshVertexSculptTool.h"
#endif
#define MESHMODELINGTOOLS_MeshVertexSculptTool_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_47_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_47_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_47_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_47_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshVertexSculptToolBuilder(); \
	friend struct Z_Construct_UClass_UMeshVertexSculptToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UMeshVertexSculptToolBuilder, UMeshSurfacePointToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMeshVertexSculptToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_47_INCLASS \
private: \
	static void StaticRegisterNativesUMeshVertexSculptToolBuilder(); \
	friend struct Z_Construct_UClass_UMeshVertexSculptToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UMeshVertexSculptToolBuilder, UMeshSurfacePointToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMeshVertexSculptToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_47_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshVertexSculptToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshVertexSculptToolBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshVertexSculptToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshVertexSculptToolBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshVertexSculptToolBuilder(UMeshVertexSculptToolBuilder&&); \
	NO_API UMeshVertexSculptToolBuilder(const UMeshVertexSculptToolBuilder&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_47_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshVertexSculptToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshVertexSculptToolBuilder(UMeshVertexSculptToolBuilder&&); \
	NO_API UMeshVertexSculptToolBuilder(const UMeshVertexSculptToolBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshVertexSculptToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshVertexSculptToolBuilder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshVertexSculptToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_47_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_44_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_47_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_47_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_47_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_47_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_47_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_47_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_47_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_47_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_47_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_47_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_47_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_47_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UMeshVertexSculptToolBuilder>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_121_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_121_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_121_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_121_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVertexBrushSculptProperties(); \
	friend struct Z_Construct_UClass_UVertexBrushSculptProperties_Statics; \
public: \
	DECLARE_CLASS(UVertexBrushSculptProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UVertexBrushSculptProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_121_INCLASS \
private: \
	static void StaticRegisterNativesUVertexBrushSculptProperties(); \
	friend struct Z_Construct_UClass_UVertexBrushSculptProperties_Statics; \
public: \
	DECLARE_CLASS(UVertexBrushSculptProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UVertexBrushSculptProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_121_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVertexBrushSculptProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVertexBrushSculptProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVertexBrushSculptProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVertexBrushSculptProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVertexBrushSculptProperties(UVertexBrushSculptProperties&&); \
	NO_API UVertexBrushSculptProperties(const UVertexBrushSculptProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_121_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVertexBrushSculptProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVertexBrushSculptProperties(UVertexBrushSculptProperties&&); \
	NO_API UVertexBrushSculptProperties(const UVertexBrushSculptProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVertexBrushSculptProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVertexBrushSculptProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVertexBrushSculptProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_121_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_118_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_121_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_121_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_121_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_121_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_121_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_121_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_121_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_121_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_121_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_121_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_121_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_121_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UVertexBrushSculptProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_152_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_152_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_152_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_152_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshVertexSculptTool(); \
	friend struct Z_Construct_UClass_UMeshVertexSculptTool_Statics; \
public: \
	DECLARE_CLASS(UMeshVertexSculptTool, UMeshSculptToolBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMeshVertexSculptTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_152_INCLASS \
private: \
	static void StaticRegisterNativesUMeshVertexSculptTool(); \
	friend struct Z_Construct_UClass_UMeshVertexSculptTool_Statics; \
public: \
	DECLARE_CLASS(UMeshVertexSculptTool, UMeshSculptToolBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMeshVertexSculptTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_152_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshVertexSculptTool(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshVertexSculptTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshVertexSculptTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshVertexSculptTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshVertexSculptTool(UMeshVertexSculptTool&&); \
	NO_API UMeshVertexSculptTool(const UMeshVertexSculptTool&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_152_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshVertexSculptTool() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshVertexSculptTool(UMeshVertexSculptTool&&); \
	NO_API UMeshVertexSculptTool(const UMeshVertexSculptTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshVertexSculptTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshVertexSculptTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMeshVertexSculptTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_152_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__DynamicMeshComponent() { return STRUCT_OFFSET(UMeshVertexSculptTool, DynamicMeshComponent); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_149_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_152_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_152_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_152_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_152_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_152_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_152_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_152_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_152_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_152_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_152_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_152_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h_152_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UMeshVertexSculptTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshVertexSculptTool_h


#define FOREACH_ENUM_EMESHVERTEXSCULPTBRUSHTYPE(op) \
	op(EMeshVertexSculptBrushType::Move) \
	op(EMeshVertexSculptBrushType::PullKelvin) \
	op(EMeshVertexSculptBrushType::PullSharpKelvin) \
	op(EMeshVertexSculptBrushType::Smooth) \
	op(EMeshVertexSculptBrushType::SmoothFill) \
	op(EMeshVertexSculptBrushType::Offset) \
	op(EMeshVertexSculptBrushType::SculptView) \
	op(EMeshVertexSculptBrushType::SculptMax) \
	op(EMeshVertexSculptBrushType::Inflate) \
	op(EMeshVertexSculptBrushType::ScaleKelvin) \
	op(EMeshVertexSculptBrushType::Pinch) \
	op(EMeshVertexSculptBrushType::TwistKelvin) \
	op(EMeshVertexSculptBrushType::Flatten) \
	op(EMeshVertexSculptBrushType::Plane) \
	op(EMeshVertexSculptBrushType::PlaneViewAligned) \
	op(EMeshVertexSculptBrushType::FixedPlane) \
	op(EMeshVertexSculptBrushType::LastValue) 

enum class EMeshVertexSculptBrushType : uint8;
template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EMeshVertexSculptBrushType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
