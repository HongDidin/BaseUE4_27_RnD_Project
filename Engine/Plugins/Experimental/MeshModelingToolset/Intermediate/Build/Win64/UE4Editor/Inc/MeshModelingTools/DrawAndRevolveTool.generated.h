// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLS_DrawAndRevolveTool_generated_h
#error "DrawAndRevolveTool.generated.h already included, missing '#pragma once' in DrawAndRevolveTool.h"
#endif
#define MESHMODELINGTOOLS_DrawAndRevolveTool_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_26_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_26_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_26_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_26_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDrawAndRevolveToolBuilder(); \
	friend struct Z_Construct_UClass_UDrawAndRevolveToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UDrawAndRevolveToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UDrawAndRevolveToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_26_INCLASS \
private: \
	static void StaticRegisterNativesUDrawAndRevolveToolBuilder(); \
	friend struct Z_Construct_UClass_UDrawAndRevolveToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UDrawAndRevolveToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UDrawAndRevolveToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_26_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDrawAndRevolveToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDrawAndRevolveToolBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDrawAndRevolveToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDrawAndRevolveToolBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDrawAndRevolveToolBuilder(UDrawAndRevolveToolBuilder&&); \
	NO_API UDrawAndRevolveToolBuilder(const UDrawAndRevolveToolBuilder&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_26_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDrawAndRevolveToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDrawAndRevolveToolBuilder(UDrawAndRevolveToolBuilder&&); \
	NO_API UDrawAndRevolveToolBuilder(const UDrawAndRevolveToolBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDrawAndRevolveToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDrawAndRevolveToolBuilder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDrawAndRevolveToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_26_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_23_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_26_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_26_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_26_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_26_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_26_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_26_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_26_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_26_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_26_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_26_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_26_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_26_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UDrawAndRevolveToolBuilder>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_39_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_39_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_39_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_39_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURevolveToolProperties(); \
	friend struct Z_Construct_UClass_URevolveToolProperties_Statics; \
public: \
	DECLARE_CLASS(URevolveToolProperties, URevolveProperties, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(URevolveToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_39_INCLASS \
private: \
	static void StaticRegisterNativesURevolveToolProperties(); \
	friend struct Z_Construct_UClass_URevolveToolProperties_Statics; \
public: \
	DECLARE_CLASS(URevolveToolProperties, URevolveProperties, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(URevolveToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_39_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URevolveToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URevolveToolProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URevolveToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URevolveToolProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URevolveToolProperties(URevolveToolProperties&&); \
	NO_API URevolveToolProperties(const URevolveToolProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_39_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URevolveToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URevolveToolProperties(URevolveToolProperties&&); \
	NO_API URevolveToolProperties(const URevolveToolProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URevolveToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URevolveToolProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URevolveToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_39_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_36_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_39_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_39_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_39_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_39_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_39_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_39_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_39_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_39_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_39_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_39_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_39_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_39_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class URevolveToolProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_74_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_74_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_74_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_74_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURevolveOperatorFactory(); \
	friend struct Z_Construct_UClass_URevolveOperatorFactory_Statics; \
public: \
	DECLARE_CLASS(URevolveOperatorFactory, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(URevolveOperatorFactory)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_74_INCLASS \
private: \
	static void StaticRegisterNativesURevolveOperatorFactory(); \
	friend struct Z_Construct_UClass_URevolveOperatorFactory_Statics; \
public: \
	DECLARE_CLASS(URevolveOperatorFactory, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(URevolveOperatorFactory)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_74_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URevolveOperatorFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URevolveOperatorFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URevolveOperatorFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URevolveOperatorFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URevolveOperatorFactory(URevolveOperatorFactory&&); \
	NO_API URevolveOperatorFactory(const URevolveOperatorFactory&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_74_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URevolveOperatorFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URevolveOperatorFactory(URevolveOperatorFactory&&); \
	NO_API URevolveOperatorFactory(const URevolveOperatorFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URevolveOperatorFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URevolveOperatorFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URevolveOperatorFactory)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_74_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_71_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_74_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_74_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_74_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_74_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_74_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_74_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_74_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_74_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_74_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_74_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_74_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_74_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class URevolveOperatorFactory>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_89_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_89_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_89_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_89_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDrawAndRevolveTool(); \
	friend struct Z_Construct_UClass_UDrawAndRevolveTool_Statics; \
public: \
	DECLARE_CLASS(UDrawAndRevolveTool, UInteractiveTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UDrawAndRevolveTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_89_INCLASS \
private: \
	static void StaticRegisterNativesUDrawAndRevolveTool(); \
	friend struct Z_Construct_UClass_UDrawAndRevolveTool_Statics; \
public: \
	DECLARE_CLASS(UDrawAndRevolveTool, UInteractiveTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UDrawAndRevolveTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_89_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDrawAndRevolveTool(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDrawAndRevolveTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDrawAndRevolveTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDrawAndRevolveTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDrawAndRevolveTool(UDrawAndRevolveTool&&); \
	NO_API UDrawAndRevolveTool(const UDrawAndRevolveTool&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_89_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDrawAndRevolveTool() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDrawAndRevolveTool(UDrawAndRevolveTool&&); \
	NO_API UDrawAndRevolveTool(const UDrawAndRevolveTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDrawAndRevolveTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDrawAndRevolveTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDrawAndRevolveTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_89_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ControlPointsMechanic() { return STRUCT_OFFSET(UDrawAndRevolveTool, ControlPointsMechanic); } \
	FORCEINLINE static uint32 __PPO__PlaneMechanic() { return STRUCT_OFFSET(UDrawAndRevolveTool, PlaneMechanic); } \
	FORCEINLINE static uint32 __PPO__Settings() { return STRUCT_OFFSET(UDrawAndRevolveTool, Settings); } \
	FORCEINLINE static uint32 __PPO__MaterialProperties() { return STRUCT_OFFSET(UDrawAndRevolveTool, MaterialProperties); } \
	FORCEINLINE static uint32 __PPO__Preview() { return STRUCT_OFFSET(UDrawAndRevolveTool, Preview); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_86_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_89_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_89_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_89_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_89_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_89_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_89_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_89_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_89_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_89_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_89_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_89_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h_89_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UDrawAndRevolveTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DrawAndRevolveTool_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
