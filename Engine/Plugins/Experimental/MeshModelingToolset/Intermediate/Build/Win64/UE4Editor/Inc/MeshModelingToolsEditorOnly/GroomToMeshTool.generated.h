// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLSEDITORONLY_GroomToMeshTool_generated_h
#error "GroomToMeshTool.generated.h already included, missing '#pragma once' in GroomToMeshTool.h"
#endif
#define MESHMODELINGTOOLSEDITORONLY_GroomToMeshTool_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_23_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_23_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_23_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGroomToMeshToolBuilder(); \
	friend struct Z_Construct_UClass_UGroomToMeshToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UGroomToMeshToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(UGroomToMeshToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_23_INCLASS \
private: \
	static void StaticRegisterNativesUGroomToMeshToolBuilder(); \
	friend struct Z_Construct_UClass_UGroomToMeshToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UGroomToMeshToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(UGroomToMeshToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGroomToMeshToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGroomToMeshToolBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGroomToMeshToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGroomToMeshToolBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGroomToMeshToolBuilder(UGroomToMeshToolBuilder&&); \
	NO_API UGroomToMeshToolBuilder(const UGroomToMeshToolBuilder&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_23_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGroomToMeshToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGroomToMeshToolBuilder(UGroomToMeshToolBuilder&&); \
	NO_API UGroomToMeshToolBuilder(const UGroomToMeshToolBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGroomToMeshToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGroomToMeshToolBuilder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGroomToMeshToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_23_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_20_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_23_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_23_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_23_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_23_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_23_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_23_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<class UGroomToMeshToolBuilder>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_47_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_47_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_47_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_47_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGroomToMeshToolProperties(); \
	friend struct Z_Construct_UClass_UGroomToMeshToolProperties_Statics; \
public: \
	DECLARE_CLASS(UGroomToMeshToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(UGroomToMeshToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_47_INCLASS \
private: \
	static void StaticRegisterNativesUGroomToMeshToolProperties(); \
	friend struct Z_Construct_UClass_UGroomToMeshToolProperties_Statics; \
public: \
	DECLARE_CLASS(UGroomToMeshToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(UGroomToMeshToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_47_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGroomToMeshToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGroomToMeshToolProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGroomToMeshToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGroomToMeshToolProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGroomToMeshToolProperties(UGroomToMeshToolProperties&&); \
	NO_API UGroomToMeshToolProperties(const UGroomToMeshToolProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_47_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGroomToMeshToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGroomToMeshToolProperties(UGroomToMeshToolProperties&&); \
	NO_API UGroomToMeshToolProperties(const UGroomToMeshToolProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGroomToMeshToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGroomToMeshToolProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGroomToMeshToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_47_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_44_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_47_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_47_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_47_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_47_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_47_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_47_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_47_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_47_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_47_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_47_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_47_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_47_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<class UGroomToMeshToolProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_121_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_121_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_121_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_121_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGroomToMeshTool(); \
	friend struct Z_Construct_UClass_UGroomToMeshTool_Statics; \
public: \
	DECLARE_CLASS(UGroomToMeshTool, UInteractiveTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(UGroomToMeshTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_121_INCLASS \
private: \
	static void StaticRegisterNativesUGroomToMeshTool(); \
	friend struct Z_Construct_UClass_UGroomToMeshTool_Statics; \
public: \
	DECLARE_CLASS(UGroomToMeshTool, UInteractiveTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(UGroomToMeshTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_121_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGroomToMeshTool(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGroomToMeshTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGroomToMeshTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGroomToMeshTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGroomToMeshTool(UGroomToMeshTool&&); \
	NO_API UGroomToMeshTool(const UGroomToMeshTool&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_121_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGroomToMeshTool(UGroomToMeshTool&&); \
	NO_API UGroomToMeshTool(const UGroomToMeshTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGroomToMeshTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGroomToMeshTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UGroomToMeshTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_121_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Settings() { return STRUCT_OFFSET(UGroomToMeshTool, Settings); } \
	FORCEINLINE static uint32 __PPO__PreviewMesh() { return STRUCT_OFFSET(UGroomToMeshTool, PreviewMesh); } \
	FORCEINLINE static uint32 __PPO__TargetGroom() { return STRUCT_OFFSET(UGroomToMeshTool, TargetGroom); } \
	FORCEINLINE static uint32 __PPO__PreviewGeom() { return STRUCT_OFFSET(UGroomToMeshTool, PreviewGeom); } \
	FORCEINLINE static uint32 __PPO__MeshMaterial() { return STRUCT_OFFSET(UGroomToMeshTool, MeshMaterial); } \
	FORCEINLINE static uint32 __PPO__UVMaterial() { return STRUCT_OFFSET(UGroomToMeshTool, UVMaterial); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_118_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_121_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_121_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_121_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_121_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_121_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_121_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_121_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_121_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_121_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_121_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_121_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h_121_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<class UGroomToMeshTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_Hair_GroomToMeshTool_h


#define FOREACH_ENUM_EGROOMTOMESHUVMODE(op) \
	op(EGroomToMeshUVMode::PlanarSplitting) \
	op(EGroomToMeshUVMode::MinimalConformal) \
	op(EGroomToMeshUVMode::PlanarSplitConformal) 

enum class EGroomToMeshUVMode;
template<> MESHMODELINGTOOLSEDITORONLY_API UEnum* StaticEnum<EGroomToMeshUVMode>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
