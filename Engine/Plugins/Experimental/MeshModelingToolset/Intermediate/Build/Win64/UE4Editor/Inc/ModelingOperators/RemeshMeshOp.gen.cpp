// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ModelingOperators/Public/CleaningOps/RemeshMeshOp.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRemeshMeshOp() {}
// Cross Module References
	MODELINGOPERATORS_API UEnum* Z_Construct_UEnum_ModelingOperators_ERemeshSmoothingType();
	UPackage* Z_Construct_UPackage__Script_ModelingOperators();
	MODELINGOPERATORS_API UEnum* Z_Construct_UEnum_ModelingOperators_ERemeshType();
// End Cross Module References
	static UEnum* ERemeshSmoothingType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ModelingOperators_ERemeshSmoothingType, Z_Construct_UPackage__Script_ModelingOperators(), TEXT("ERemeshSmoothingType"));
		}
		return Singleton;
	}
	template<> MODELINGOPERATORS_API UEnum* StaticEnum<ERemeshSmoothingType>()
	{
		return ERemeshSmoothingType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ERemeshSmoothingType(ERemeshSmoothingType_StaticEnum, TEXT("/Script/ModelingOperators"), TEXT("ERemeshSmoothingType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ModelingOperators_ERemeshSmoothingType_Hash() { return 156538864U; }
	UEnum* Z_Construct_UEnum_ModelingOperators_ERemeshSmoothingType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ModelingOperators();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ERemeshSmoothingType"), 0, Get_Z_Construct_UEnum_ModelingOperators_ERemeshSmoothingType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ERemeshSmoothingType::Uniform", (int64)ERemeshSmoothingType::Uniform },
				{ "ERemeshSmoothingType::Cotangent", (int64)ERemeshSmoothingType::Cotangent },
				{ "ERemeshSmoothingType::MeanValue", (int64)ERemeshSmoothingType::MeanValue },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** Smoothing modes */" },
				{ "Cotangent.Comment", "/** Cotangent Smoothing */" },
				{ "Cotangent.DisplayName", "Shape Preserving" },
				{ "Cotangent.Name", "ERemeshSmoothingType::Cotangent" },
				{ "Cotangent.ToolTip", "Cotangent Smoothing" },
				{ "MeanValue.Comment", "/** Mean Value Smoothing */" },
				{ "MeanValue.DisplayName", "Mixed" },
				{ "MeanValue.Name", "ERemeshSmoothingType::MeanValue" },
				{ "MeanValue.ToolTip", "Mean Value Smoothing" },
				{ "ModuleRelativePath", "Public/CleaningOps/RemeshMeshOp.h" },
				{ "ToolTip", "Smoothing modes" },
				{ "Uniform.Comment", "/** Uniform Smoothing */" },
				{ "Uniform.DisplayName", "Uniform" },
				{ "Uniform.Name", "ERemeshSmoothingType::Uniform" },
				{ "Uniform.ToolTip", "Uniform Smoothing" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ModelingOperators,
				nullptr,
				"ERemeshSmoothingType",
				"ERemeshSmoothingType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ERemeshType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ModelingOperators_ERemeshType, Z_Construct_UPackage__Script_ModelingOperators(), TEXT("ERemeshType"));
		}
		return Singleton;
	}
	template<> MODELINGOPERATORS_API UEnum* StaticEnum<ERemeshType>()
	{
		return ERemeshType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ERemeshType(ERemeshType_StaticEnum, TEXT("/Script/ModelingOperators"), TEXT("ERemeshType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ModelingOperators_ERemeshType_Hash() { return 3745292893U; }
	UEnum* Z_Construct_UEnum_ModelingOperators_ERemeshType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ModelingOperators();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ERemeshType"), 0, Get_Z_Construct_UEnum_ModelingOperators_ERemeshType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ERemeshType::Standard", (int64)ERemeshType::Standard },
				{ "ERemeshType::FullPass", (int64)ERemeshType::FullPass },
				{ "ERemeshType::NormalFlow", (int64)ERemeshType::NormalFlow },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** Remeshing modes */" },
				{ "FullPass.Comment", "/** Multiple full passes over the entire mesh */" },
				{ "FullPass.DisplayName", "Full Pass" },
				{ "FullPass.Name", "ERemeshType::FullPass" },
				{ "FullPass.ToolTip", "Multiple full passes over the entire mesh" },
				{ "ModuleRelativePath", "Public/CleaningOps/RemeshMeshOp.h" },
				{ "NormalFlow.Comment", "/** One pass over the entire mesh, then remesh only changed edges. Use Normal flow to align triangles with input.*/" },
				{ "NormalFlow.DisplayName", "Normal Flow" },
				{ "NormalFlow.Name", "ERemeshType::NormalFlow" },
				{ "NormalFlow.ToolTip", "One pass over the entire mesh, then remesh only changed edges. Use Normal flow to align triangles with input." },
				{ "Standard.Comment", "/** One pass over the entire mesh, then remesh only changed edges */" },
				{ "Standard.DisplayName", "Standard" },
				{ "Standard.Name", "ERemeshType::Standard" },
				{ "Standard.ToolTip", "One pass over the entire mesh, then remesh only changed edges" },
				{ "ToolTip", "Remeshing modes" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ModelingOperators,
				nullptr,
				"ERemeshType",
				"ERemeshType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
