// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ModelingComponents/Public/Changes/MeshVertexChange.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMeshVertexChange() {}
// Cross Module References
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UMeshVertexCommandChangeTarget_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UMeshVertexCommandChangeTarget();
	COREUOBJECT_API UClass* Z_Construct_UClass_UInterface();
	UPackage* Z_Construct_UPackage__Script_ModelingComponents();
// End Cross Module References
	void UMeshVertexCommandChangeTarget::StaticRegisterNativesUMeshVertexCommandChangeTarget()
	{
	}
	UClass* Z_Construct_UClass_UMeshVertexCommandChangeTarget_NoRegister()
	{
		return UMeshVertexCommandChangeTarget::StaticClass();
	}
	struct Z_Construct_UClass_UMeshVertexCommandChangeTarget_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshVertexCommandChangeTarget_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInterface,
		(UObject* (*)())Z_Construct_UPackage__Script_ModelingComponents,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshVertexCommandChangeTarget_Statics::Class_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Changes/MeshVertexChange.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshVertexCommandChangeTarget_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<IMeshVertexCommandChangeTarget>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshVertexCommandChangeTarget_Statics::ClassParams = {
		&UMeshVertexCommandChangeTarget::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001040A1u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshVertexCommandChangeTarget_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshVertexCommandChangeTarget_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshVertexCommandChangeTarget()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshVertexCommandChangeTarget_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshVertexCommandChangeTarget, 507117509);
	template<> MODELINGCOMPONENTS_API UClass* StaticClass<UMeshVertexCommandChangeTarget>()
	{
		return UMeshVertexCommandChangeTarget::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshVertexCommandChangeTarget(Z_Construct_UClass_UMeshVertexCommandChangeTarget, &UMeshVertexCommandChangeTarget::StaticClass, TEXT("/Script/ModelingComponents"), TEXT("UMeshVertexCommandChangeTarget"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshVertexCommandChangeTarget);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
