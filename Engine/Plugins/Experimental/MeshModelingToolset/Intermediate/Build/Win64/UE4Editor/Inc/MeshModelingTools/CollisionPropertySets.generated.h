// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLS_CollisionPropertySets_generated_h
#error "CollisionPropertySets.generated.h already included, missing '#pragma once' in CollisionPropertySets.h"
#endif
#define MESHMODELINGTOOLS_CollisionPropertySets_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_80_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FPhysicsConvexData_Statics; \
	static class UScriptStruct* StaticStruct();


template<> MESHMODELINGTOOLS_API UScriptStruct* StaticStruct<struct FPhysicsConvexData>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_62_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FPhysicsCapsuleData_Statics; \
	static class UScriptStruct* StaticStruct();


template<> MESHMODELINGTOOLS_API UScriptStruct* StaticStruct<struct FPhysicsCapsuleData>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_47_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FPhysicsBoxData_Statics; \
	static class UScriptStruct* StaticStruct();


template<> MESHMODELINGTOOLS_API UScriptStruct* StaticStruct<struct FPhysicsBoxData>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_32_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FPhysicsSphereData_Statics; \
	static class UScriptStruct* StaticStruct();


template<> MESHMODELINGTOOLS_API UScriptStruct* StaticStruct<struct FPhysicsSphereData>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_96_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_96_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_96_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_96_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPhysicsObjectToolPropertySet(); \
	friend struct Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics; \
public: \
	DECLARE_CLASS(UPhysicsObjectToolPropertySet, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UPhysicsObjectToolPropertySet)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_96_INCLASS \
private: \
	static void StaticRegisterNativesUPhysicsObjectToolPropertySet(); \
	friend struct Z_Construct_UClass_UPhysicsObjectToolPropertySet_Statics; \
public: \
	DECLARE_CLASS(UPhysicsObjectToolPropertySet, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UPhysicsObjectToolPropertySet)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_96_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPhysicsObjectToolPropertySet(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPhysicsObjectToolPropertySet) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPhysicsObjectToolPropertySet); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPhysicsObjectToolPropertySet); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPhysicsObjectToolPropertySet(UPhysicsObjectToolPropertySet&&); \
	NO_API UPhysicsObjectToolPropertySet(const UPhysicsObjectToolPropertySet&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_96_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPhysicsObjectToolPropertySet(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPhysicsObjectToolPropertySet(UPhysicsObjectToolPropertySet&&); \
	NO_API UPhysicsObjectToolPropertySet(const UPhysicsObjectToolPropertySet&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPhysicsObjectToolPropertySet); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPhysicsObjectToolPropertySet); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPhysicsObjectToolPropertySet)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_96_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_93_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_96_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_96_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_96_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_96_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_96_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_96_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_96_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_96_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_96_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_96_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_96_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_96_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UPhysicsObjectToolPropertySet>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_125_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_125_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_125_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_125_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCollisionGeometryVisualizationProperties(); \
	friend struct Z_Construct_UClass_UCollisionGeometryVisualizationProperties_Statics; \
public: \
	DECLARE_CLASS(UCollisionGeometryVisualizationProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UCollisionGeometryVisualizationProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_125_INCLASS \
private: \
	static void StaticRegisterNativesUCollisionGeometryVisualizationProperties(); \
	friend struct Z_Construct_UClass_UCollisionGeometryVisualizationProperties_Statics; \
public: \
	DECLARE_CLASS(UCollisionGeometryVisualizationProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UCollisionGeometryVisualizationProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_125_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCollisionGeometryVisualizationProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCollisionGeometryVisualizationProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCollisionGeometryVisualizationProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCollisionGeometryVisualizationProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCollisionGeometryVisualizationProperties(UCollisionGeometryVisualizationProperties&&); \
	NO_API UCollisionGeometryVisualizationProperties(const UCollisionGeometryVisualizationProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_125_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCollisionGeometryVisualizationProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCollisionGeometryVisualizationProperties(UCollisionGeometryVisualizationProperties&&); \
	NO_API UCollisionGeometryVisualizationProperties(const UCollisionGeometryVisualizationProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCollisionGeometryVisualizationProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCollisionGeometryVisualizationProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCollisionGeometryVisualizationProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_125_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_122_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_125_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_125_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_125_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_125_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_125_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_125_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_125_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_125_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_125_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_125_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_125_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h_125_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UCollisionGeometryVisualizationProperties>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_CollisionPropertySets_h


#define FOREACH_ENUM_ECOLLISIONGEOMETRYMODE(op) \
	op(ECollisionGeometryMode::Default) \
	op(ECollisionGeometryMode::SimpleAndComplex) \
	op(ECollisionGeometryMode::UseSimpleAsComplex) \
	op(ECollisionGeometryMode::UseComplexAsSimple) 

enum class ECollisionGeometryMode;
template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<ECollisionGeometryMode>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
