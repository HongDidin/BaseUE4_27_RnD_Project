// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MODELINGCOMPONENTS_MultiTransformer_generated_h
#error "MultiTransformer.generated.h already included, missing '#pragma once' in MultiTransformer.h"
#endif
#define MODELINGCOMPONENTS_MultiTransformer_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Transforms_MultiTransformer_h_29_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Transforms_MultiTransformer_h_29_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Transforms_MultiTransformer_h_29_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Transforms_MultiTransformer_h_29_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMultiTransformer(); \
	friend struct Z_Construct_UClass_UMultiTransformer_Statics; \
public: \
	DECLARE_CLASS(UMultiTransformer, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UMultiTransformer)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Transforms_MultiTransformer_h_29_INCLASS \
private: \
	static void StaticRegisterNativesUMultiTransformer(); \
	friend struct Z_Construct_UClass_UMultiTransformer_Statics; \
public: \
	DECLARE_CLASS(UMultiTransformer, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UMultiTransformer)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Transforms_MultiTransformer_h_29_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMultiTransformer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMultiTransformer) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMultiTransformer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMultiTransformer); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMultiTransformer(UMultiTransformer&&); \
	NO_API UMultiTransformer(const UMultiTransformer&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Transforms_MultiTransformer_h_29_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMultiTransformer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMultiTransformer(UMultiTransformer&&); \
	NO_API UMultiTransformer(const UMultiTransformer&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMultiTransformer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMultiTransformer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMultiTransformer)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Transforms_MultiTransformer_h_29_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Transforms_MultiTransformer_h_26_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Transforms_MultiTransformer_h_29_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Transforms_MultiTransformer_h_29_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Transforms_MultiTransformer_h_29_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Transforms_MultiTransformer_h_29_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Transforms_MultiTransformer_h_29_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Transforms_MultiTransformer_h_29_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Transforms_MultiTransformer_h_29_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Transforms_MultiTransformer_h_29_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Transforms_MultiTransformer_h_29_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Transforms_MultiTransformer_h_29_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Transforms_MultiTransformer_h_29_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Transforms_MultiTransformer_h_29_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MODELINGCOMPONENTS_API UClass* StaticClass<class UMultiTransformer>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Transforms_MultiTransformer_h


#define FOREACH_ENUM_EMULTITRANSFORMERMODE(op) \
	op(EMultiTransformerMode::DefaultGizmo) \
	op(EMultiTransformerMode::QuickAxisTranslation) 

enum class EMultiTransformerMode;
template<> MODELINGCOMPONENTS_API UEnum* StaticEnum<EMultiTransformerMode>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
