// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MODELINGCOMPONENTS_SpatialCurveDistanceMechanic_generated_h
#error "SpatialCurveDistanceMechanic.generated.h already included, missing '#pragma once' in SpatialCurveDistanceMechanic.h"
#endif
#define MODELINGCOMPONENTS_SpatialCurveDistanceMechanic_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpatialCurveDistanceMechanic_h_17_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpatialCurveDistanceMechanic_h_17_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpatialCurveDistanceMechanic_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpatialCurveDistanceMechanic_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSpatialCurveDistanceMechanic(); \
	friend struct Z_Construct_UClass_USpatialCurveDistanceMechanic_Statics; \
public: \
	DECLARE_CLASS(USpatialCurveDistanceMechanic, UInteractionMechanic, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(USpatialCurveDistanceMechanic)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpatialCurveDistanceMechanic_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUSpatialCurveDistanceMechanic(); \
	friend struct Z_Construct_UClass_USpatialCurveDistanceMechanic_Statics; \
public: \
	DECLARE_CLASS(USpatialCurveDistanceMechanic, UInteractionMechanic, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(USpatialCurveDistanceMechanic)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpatialCurveDistanceMechanic_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USpatialCurveDistanceMechanic(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USpatialCurveDistanceMechanic) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USpatialCurveDistanceMechanic); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USpatialCurveDistanceMechanic); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USpatialCurveDistanceMechanic(USpatialCurveDistanceMechanic&&); \
	NO_API USpatialCurveDistanceMechanic(const USpatialCurveDistanceMechanic&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpatialCurveDistanceMechanic_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USpatialCurveDistanceMechanic() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USpatialCurveDistanceMechanic(USpatialCurveDistanceMechanic&&); \
	NO_API USpatialCurveDistanceMechanic(const USpatialCurveDistanceMechanic&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USpatialCurveDistanceMechanic); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USpatialCurveDistanceMechanic); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(USpatialCurveDistanceMechanic)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpatialCurveDistanceMechanic_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpatialCurveDistanceMechanic_h_14_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpatialCurveDistanceMechanic_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpatialCurveDistanceMechanic_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpatialCurveDistanceMechanic_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpatialCurveDistanceMechanic_h_17_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpatialCurveDistanceMechanic_h_17_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpatialCurveDistanceMechanic_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpatialCurveDistanceMechanic_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpatialCurveDistanceMechanic_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpatialCurveDistanceMechanic_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpatialCurveDistanceMechanic_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpatialCurveDistanceMechanic_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpatialCurveDistanceMechanic_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MODELINGCOMPONENTS_API UClass* StaticClass<class USpatialCurveDistanceMechanic>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpatialCurveDistanceMechanic_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
