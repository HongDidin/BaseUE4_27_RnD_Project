// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ModelingComponents/Public/Transforms/MultiTransformer.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMultiTransformer() {}
// Cross Module References
	MODELINGCOMPONENTS_API UEnum* Z_Construct_UEnum_ModelingComponents_EMultiTransformerMode();
	UPackage* Z_Construct_UPackage__Script_ModelingComponents();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UMultiTransformer_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UMultiTransformer();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveGizmoManager_NoRegister();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UTransformGizmo_NoRegister();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UTransformProxy_NoRegister();
// End Cross Module References
	static UEnum* EMultiTransformerMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ModelingComponents_EMultiTransformerMode, Z_Construct_UPackage__Script_ModelingComponents(), TEXT("EMultiTransformerMode"));
		}
		return Singleton;
	}
	template<> MODELINGCOMPONENTS_API UEnum* StaticEnum<EMultiTransformerMode>()
	{
		return EMultiTransformerMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMultiTransformerMode(EMultiTransformerMode_StaticEnum, TEXT("/Script/ModelingComponents"), TEXT("EMultiTransformerMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ModelingComponents_EMultiTransformerMode_Hash() { return 1112598893U; }
	UEnum* Z_Construct_UEnum_ModelingComponents_EMultiTransformerMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ModelingComponents();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMultiTransformerMode"), 0, Get_Z_Construct_UEnum_ModelingComponents_EMultiTransformerMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMultiTransformerMode::DefaultGizmo", (int64)EMultiTransformerMode::DefaultGizmo },
				{ "EMultiTransformerMode::QuickAxisTranslation", (int64)EMultiTransformerMode::QuickAxisTranslation },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "DefaultGizmo.Name", "EMultiTransformerMode::DefaultGizmo" },
				{ "ModuleRelativePath", "Public/Transforms/MultiTransformer.h" },
				{ "QuickAxisTranslation.Name", "EMultiTransformerMode::QuickAxisTranslation" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ModelingComponents,
				nullptr,
				"EMultiTransformerMode",
				"EMultiTransformerMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UMultiTransformer::StaticRegisterNativesUMultiTransformer()
	{
	}
	UClass* Z_Construct_UClass_UMultiTransformer_NoRegister()
	{
		return UMultiTransformer::StaticClass();
	}
	struct Z_Construct_UClass_UMultiTransformer_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GizmoManager_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_GizmoManager;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransformGizmo_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TransformGizmo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransformProxy_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TransformProxy;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMultiTransformer_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_ModelingComponents,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMultiTransformer_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * UMultiTransformer abstracts both a default TRS Gizmo, and the \"Quick\" translate/rotate Gizmos.\n */" },
		{ "IncludePath", "Transforms/MultiTransformer.h" },
		{ "ModuleRelativePath", "Public/Transforms/MultiTransformer.h" },
		{ "ToolTip", "UMultiTransformer abstracts both a default TRS Gizmo, and the \"Quick\" translate/rotate Gizmos." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMultiTransformer_Statics::NewProp_GizmoManager_MetaData[] = {
		{ "ModuleRelativePath", "Public/Transforms/MultiTransformer.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMultiTransformer_Statics::NewProp_GizmoManager = { "GizmoManager", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMultiTransformer, GizmoManager), Z_Construct_UClass_UInteractiveGizmoManager_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMultiTransformer_Statics::NewProp_GizmoManager_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMultiTransformer_Statics::NewProp_GizmoManager_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMultiTransformer_Statics::NewProp_TransformGizmo_MetaData[] = {
		{ "ModuleRelativePath", "Public/Transforms/MultiTransformer.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMultiTransformer_Statics::NewProp_TransformGizmo = { "TransformGizmo", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMultiTransformer, TransformGizmo), Z_Construct_UClass_UTransformGizmo_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMultiTransformer_Statics::NewProp_TransformGizmo_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMultiTransformer_Statics::NewProp_TransformGizmo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMultiTransformer_Statics::NewProp_TransformProxy_MetaData[] = {
		{ "ModuleRelativePath", "Public/Transforms/MultiTransformer.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMultiTransformer_Statics::NewProp_TransformProxy = { "TransformProxy", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMultiTransformer, TransformProxy), Z_Construct_UClass_UTransformProxy_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMultiTransformer_Statics::NewProp_TransformProxy_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMultiTransformer_Statics::NewProp_TransformProxy_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMultiTransformer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMultiTransformer_Statics::NewProp_GizmoManager,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMultiTransformer_Statics::NewProp_TransformGizmo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMultiTransformer_Statics::NewProp_TransformProxy,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMultiTransformer_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMultiTransformer>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMultiTransformer_Statics::ClassParams = {
		&UMultiTransformer::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMultiTransformer_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMultiTransformer_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMultiTransformer_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMultiTransformer_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMultiTransformer()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMultiTransformer_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMultiTransformer, 2095152842);
	template<> MODELINGCOMPONENTS_API UClass* StaticClass<UMultiTransformer>()
	{
		return UMultiTransformer::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMultiTransformer(Z_Construct_UClass_UMultiTransformer, &UMultiTransformer::StaticClass, TEXT("/Script/ModelingComponents"), TEXT("UMultiTransformer"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMultiTransformer);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
