// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLS_MeshSelectionTool_generated_h
#error "MeshSelectionTool.generated.h already included, missing '#pragma once' in MeshSelectionTool.h"
#endif
#define MESHMODELINGTOOLS_MeshSelectionTool_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_18_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_18_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshSelectionToolBuilder(); \
	friend struct Z_Construct_UClass_UMeshSelectionToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UMeshSelectionToolBuilder, UMeshSurfacePointToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMeshSelectionToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUMeshSelectionToolBuilder(); \
	friend struct Z_Construct_UClass_UMeshSelectionToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UMeshSelectionToolBuilder, UMeshSurfacePointToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMeshSelectionToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshSelectionToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshSelectionToolBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshSelectionToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshSelectionToolBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshSelectionToolBuilder(UMeshSelectionToolBuilder&&); \
	NO_API UMeshSelectionToolBuilder(const UMeshSelectionToolBuilder&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshSelectionToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshSelectionToolBuilder(UMeshSelectionToolBuilder&&); \
	NO_API UMeshSelectionToolBuilder(const UMeshSelectionToolBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshSelectionToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshSelectionToolBuilder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshSelectionToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_15_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_18_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_18_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_18_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_18_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UMeshSelectionToolBuilder>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_60_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_60_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_60_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_60_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshSelectionToolActionPropertySet(); \
	friend struct Z_Construct_UClass_UMeshSelectionToolActionPropertySet_Statics; \
public: \
	DECLARE_CLASS(UMeshSelectionToolActionPropertySet, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMeshSelectionToolActionPropertySet)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_60_INCLASS \
private: \
	static void StaticRegisterNativesUMeshSelectionToolActionPropertySet(); \
	friend struct Z_Construct_UClass_UMeshSelectionToolActionPropertySet_Statics; \
public: \
	DECLARE_CLASS(UMeshSelectionToolActionPropertySet, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMeshSelectionToolActionPropertySet)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_60_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshSelectionToolActionPropertySet(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshSelectionToolActionPropertySet) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshSelectionToolActionPropertySet); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshSelectionToolActionPropertySet); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshSelectionToolActionPropertySet(UMeshSelectionToolActionPropertySet&&); \
	NO_API UMeshSelectionToolActionPropertySet(const UMeshSelectionToolActionPropertySet&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_60_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshSelectionToolActionPropertySet(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshSelectionToolActionPropertySet(UMeshSelectionToolActionPropertySet&&); \
	NO_API UMeshSelectionToolActionPropertySet(const UMeshSelectionToolActionPropertySet&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshSelectionToolActionPropertySet); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshSelectionToolActionPropertySet); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshSelectionToolActionPropertySet)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_60_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_57_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_60_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_60_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_60_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_60_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_60_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_60_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_60_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_60_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_60_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_60_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_60_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_60_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UMeshSelectionToolActionPropertySet>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_76_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_76_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOptimizeSelection); \
	DECLARE_FUNCTION(execSelectLargestComponentByArea); \
	DECLARE_FUNCTION(execSelectLargestComponentByTriCount); \
	DECLARE_FUNCTION(execExpandToConnected); \
	DECLARE_FUNCTION(execShrink); \
	DECLARE_FUNCTION(execGrow); \
	DECLARE_FUNCTION(execInvert); \
	DECLARE_FUNCTION(execClear); \
	DECLARE_FUNCTION(execSelectAll);


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_76_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOptimizeSelection); \
	DECLARE_FUNCTION(execSelectLargestComponentByArea); \
	DECLARE_FUNCTION(execSelectLargestComponentByTriCount); \
	DECLARE_FUNCTION(execExpandToConnected); \
	DECLARE_FUNCTION(execShrink); \
	DECLARE_FUNCTION(execGrow); \
	DECLARE_FUNCTION(execInvert); \
	DECLARE_FUNCTION(execClear); \
	DECLARE_FUNCTION(execSelectAll);


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_76_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshSelectionEditActions(); \
	friend struct Z_Construct_UClass_UMeshSelectionEditActions_Statics; \
public: \
	DECLARE_CLASS(UMeshSelectionEditActions, UMeshSelectionToolActionPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMeshSelectionEditActions)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_76_INCLASS \
private: \
	static void StaticRegisterNativesUMeshSelectionEditActions(); \
	friend struct Z_Construct_UClass_UMeshSelectionEditActions_Statics; \
public: \
	DECLARE_CLASS(UMeshSelectionEditActions, UMeshSelectionToolActionPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMeshSelectionEditActions)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_76_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshSelectionEditActions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshSelectionEditActions) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshSelectionEditActions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshSelectionEditActions); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshSelectionEditActions(UMeshSelectionEditActions&&); \
	NO_API UMeshSelectionEditActions(const UMeshSelectionEditActions&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_76_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshSelectionEditActions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshSelectionEditActions(UMeshSelectionEditActions&&); \
	NO_API UMeshSelectionEditActions(const UMeshSelectionEditActions&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshSelectionEditActions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshSelectionEditActions); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshSelectionEditActions)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_76_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_73_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_76_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_76_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_76_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_76_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_76_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_76_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_76_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_76_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_76_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_76_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_76_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_76_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UMeshSelectionEditActions>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_142_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_142_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execCreatePolygroup); \
	DECLARE_FUNCTION(execFlipNormals); \
	DECLARE_FUNCTION(execDisconnectTriangles); \
	DECLARE_FUNCTION(execSeparateTriangles); \
	DECLARE_FUNCTION(execDeleteTriangles);


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_142_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execCreatePolygroup); \
	DECLARE_FUNCTION(execFlipNormals); \
	DECLARE_FUNCTION(execDisconnectTriangles); \
	DECLARE_FUNCTION(execSeparateTriangles); \
	DECLARE_FUNCTION(execDeleteTriangles);


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_142_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshSelectionMeshEditActions(); \
	friend struct Z_Construct_UClass_UMeshSelectionMeshEditActions_Statics; \
public: \
	DECLARE_CLASS(UMeshSelectionMeshEditActions, UMeshSelectionToolActionPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMeshSelectionMeshEditActions)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_142_INCLASS \
private: \
	static void StaticRegisterNativesUMeshSelectionMeshEditActions(); \
	friend struct Z_Construct_UClass_UMeshSelectionMeshEditActions_Statics; \
public: \
	DECLARE_CLASS(UMeshSelectionMeshEditActions, UMeshSelectionToolActionPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMeshSelectionMeshEditActions)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_142_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshSelectionMeshEditActions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshSelectionMeshEditActions) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshSelectionMeshEditActions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshSelectionMeshEditActions); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshSelectionMeshEditActions(UMeshSelectionMeshEditActions&&); \
	NO_API UMeshSelectionMeshEditActions(const UMeshSelectionMeshEditActions&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_142_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshSelectionMeshEditActions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshSelectionMeshEditActions(UMeshSelectionMeshEditActions&&); \
	NO_API UMeshSelectionMeshEditActions(const UMeshSelectionMeshEditActions&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshSelectionMeshEditActions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshSelectionMeshEditActions); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshSelectionMeshEditActions)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_142_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_139_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_142_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_142_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_142_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_142_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_142_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_142_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_142_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_142_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_142_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_142_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_142_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_142_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UMeshSelectionMeshEditActions>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_231_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_231_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_231_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_231_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshSelectionToolProperties(); \
	friend struct Z_Construct_UClass_UMeshSelectionToolProperties_Statics; \
public: \
	DECLARE_CLASS(UMeshSelectionToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMeshSelectionToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_231_INCLASS \
private: \
	static void StaticRegisterNativesUMeshSelectionToolProperties(); \
	friend struct Z_Construct_UClass_UMeshSelectionToolProperties_Statics; \
public: \
	DECLARE_CLASS(UMeshSelectionToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMeshSelectionToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_231_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshSelectionToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshSelectionToolProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshSelectionToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshSelectionToolProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshSelectionToolProperties(UMeshSelectionToolProperties&&); \
	NO_API UMeshSelectionToolProperties(const UMeshSelectionToolProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_231_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshSelectionToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshSelectionToolProperties(UMeshSelectionToolProperties&&); \
	NO_API UMeshSelectionToolProperties(const UMeshSelectionToolProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshSelectionToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshSelectionToolProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshSelectionToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_231_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_228_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_231_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_231_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_231_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_231_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_231_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_231_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_231_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_231_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_231_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_231_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_231_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_231_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UMeshSelectionToolProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_262_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_262_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_262_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_262_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshSelectionTool(); \
	friend struct Z_Construct_UClass_UMeshSelectionTool_Statics; \
public: \
	DECLARE_CLASS(UMeshSelectionTool, UDynamicMeshBrushTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMeshSelectionTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_262_INCLASS \
private: \
	static void StaticRegisterNativesUMeshSelectionTool(); \
	friend struct Z_Construct_UClass_UMeshSelectionTool_Statics; \
public: \
	DECLARE_CLASS(UMeshSelectionTool, UDynamicMeshBrushTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMeshSelectionTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_262_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshSelectionTool(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshSelectionTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshSelectionTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshSelectionTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshSelectionTool(UMeshSelectionTool&&); \
	NO_API UMeshSelectionTool(const UMeshSelectionTool&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_262_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshSelectionTool(UMeshSelectionTool&&); \
	NO_API UMeshSelectionTool(const UMeshSelectionTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshSelectionTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshSelectionTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMeshSelectionTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_262_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Selection() { return STRUCT_OFFSET(UMeshSelectionTool, Selection); } \
	FORCEINLINE static uint32 __PPO__SpawnedActors() { return STRUCT_OFFSET(UMeshSelectionTool, SpawnedActors); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_259_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_262_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_262_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_262_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_262_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_262_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_262_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_262_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_262_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_262_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_262_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_262_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h_262_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UMeshSelectionTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MeshSelectionTool_h


#define FOREACH_ENUM_EMESHFACESCOLORMODE(op) \
	op(EMeshFacesColorMode::None) \
	op(EMeshFacesColorMode::ByGroup) \
	op(EMeshFacesColorMode::ByMaterialID) \
	op(EMeshFacesColorMode::ByUVIsland) 

enum class EMeshFacesColorMode;
template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EMeshFacesColorMode>();

#define FOREACH_ENUM_EMESHSELECTIONTOOLPRIMARYMODE(op) \
	op(EMeshSelectionToolPrimaryMode::Brush) \
	op(EMeshSelectionToolPrimaryMode::VolumetricBrush) \
	op(EMeshSelectionToolPrimaryMode::AngleFiltered) \
	op(EMeshSelectionToolPrimaryMode::Visible) \
	op(EMeshSelectionToolPrimaryMode::AllConnected) \
	op(EMeshSelectionToolPrimaryMode::AllInGroup) \
	op(EMeshSelectionToolPrimaryMode::ByMaterial) \
	op(EMeshSelectionToolPrimaryMode::ByUVIsland) \
	op(EMeshSelectionToolPrimaryMode::AllWithinAngle) 

enum class EMeshSelectionToolPrimaryMode;
template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EMeshSelectionToolPrimaryMode>();

#define FOREACH_ENUM_EMESHSELECTIONTOOLACTIONS(op) \
	op(EMeshSelectionToolActions::NoAction) \
	op(EMeshSelectionToolActions::SelectAll) \
	op(EMeshSelectionToolActions::ClearSelection) \
	op(EMeshSelectionToolActions::InvertSelection) \
	op(EMeshSelectionToolActions::GrowSelection) \
	op(EMeshSelectionToolActions::ShrinkSelection) \
	op(EMeshSelectionToolActions::ExpandToConnected) \
	op(EMeshSelectionToolActions::SelectLargestComponentByTriCount) \
	op(EMeshSelectionToolActions::SelectLargestComponentByArea) \
	op(EMeshSelectionToolActions::OptimizeSelection) \
	op(EMeshSelectionToolActions::DeleteSelected) \
	op(EMeshSelectionToolActions::DisconnectSelected) \
	op(EMeshSelectionToolActions::SeparateSelected) \
	op(EMeshSelectionToolActions::FlipSelected) \
	op(EMeshSelectionToolActions::CreateGroup) \
	op(EMeshSelectionToolActions::CycleSelectionMode) \
	op(EMeshSelectionToolActions::CycleViewMode) 

enum class EMeshSelectionToolActions;
template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EMeshSelectionToolActions>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
