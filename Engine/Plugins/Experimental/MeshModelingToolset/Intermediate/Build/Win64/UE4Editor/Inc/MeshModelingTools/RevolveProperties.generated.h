// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLS_RevolveProperties_generated_h
#error "RevolveProperties.generated.h already included, missing '#pragma once' in RevolveProperties.h"
#endif
#define MESHMODELINGTOOLS_RevolveProperties_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RevolveProperties_h_61_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RevolveProperties_h_61_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RevolveProperties_h_61_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RevolveProperties_h_61_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURevolveProperties(); \
	friend struct Z_Construct_UClass_URevolveProperties_Statics; \
public: \
	DECLARE_CLASS(URevolveProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(URevolveProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RevolveProperties_h_61_INCLASS \
private: \
	static void StaticRegisterNativesURevolveProperties(); \
	friend struct Z_Construct_UClass_URevolveProperties_Statics; \
public: \
	DECLARE_CLASS(URevolveProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(URevolveProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RevolveProperties_h_61_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URevolveProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URevolveProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URevolveProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URevolveProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URevolveProperties(URevolveProperties&&); \
	NO_API URevolveProperties(const URevolveProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RevolveProperties_h_61_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URevolveProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URevolveProperties(URevolveProperties&&); \
	NO_API URevolveProperties(const URevolveProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URevolveProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URevolveProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URevolveProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RevolveProperties_h_61_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RevolveProperties_h_58_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RevolveProperties_h_61_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RevolveProperties_h_61_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RevolveProperties_h_61_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RevolveProperties_h_61_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RevolveProperties_h_61_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RevolveProperties_h_61_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RevolveProperties_h_61_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RevolveProperties_h_61_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RevolveProperties_h_61_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RevolveProperties_h_61_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RevolveProperties_h_61_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RevolveProperties_h_61_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class URevolveProperties>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RevolveProperties_h


#define FOREACH_ENUM_EREVOLVEPROPERTIESQUADSPLIT(op) \
	op(ERevolvePropertiesQuadSplit::Uniform) \
	op(ERevolvePropertiesQuadSplit::ShortestDiagonal) 

enum class ERevolvePropertiesQuadSplit : uint8;
template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<ERevolvePropertiesQuadSplit>();

#define FOREACH_ENUM_EREVOLVEPROPERTIESPOLYGROUPMODE(op) \
	op(ERevolvePropertiesPolygroupMode::Single) \
	op(ERevolvePropertiesPolygroupMode::PerFace) \
	op(ERevolvePropertiesPolygroupMode::PerStep) \
	op(ERevolvePropertiesPolygroupMode::AccordingToProfileCurve) 

enum class ERevolvePropertiesPolygroupMode : uint8;
template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<ERevolvePropertiesPolygroupMode>();

#define FOREACH_ENUM_EREVOLVEPROPERTIESCAPFILLMODE(op) \
	op(ERevolvePropertiesCapFillMode::None) \
	op(ERevolvePropertiesCapFillMode::Delaunay) \
	op(ERevolvePropertiesCapFillMode::EarClipping) \
	op(ERevolvePropertiesCapFillMode::CenterFan) 

enum class ERevolvePropertiesCapFillMode : uint8;
template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<ERevolvePropertiesCapFillMode>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
