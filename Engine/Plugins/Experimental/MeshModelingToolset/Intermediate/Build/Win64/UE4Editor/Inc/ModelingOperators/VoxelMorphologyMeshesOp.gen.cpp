// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ModelingOperators/Public/CompositionOps/VoxelMorphologyMeshesOp.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVoxelMorphologyMeshesOp() {}
// Cross Module References
	MODELINGOPERATORS_API UEnum* Z_Construct_UEnum_ModelingOperators_EMorphologyOperation();
	UPackage* Z_Construct_UPackage__Script_ModelingOperators();
// End Cross Module References
	static UEnum* EMorphologyOperation_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ModelingOperators_EMorphologyOperation, Z_Construct_UPackage__Script_ModelingOperators(), TEXT("EMorphologyOperation"));
		}
		return Singleton;
	}
	template<> MODELINGOPERATORS_API UEnum* StaticEnum<EMorphologyOperation>()
	{
		return EMorphologyOperation_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMorphologyOperation(EMorphologyOperation_StaticEnum, TEXT("/Script/ModelingOperators"), TEXT("EMorphologyOperation"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ModelingOperators_EMorphologyOperation_Hash() { return 3215182299U; }
	UEnum* Z_Construct_UEnum_ModelingOperators_EMorphologyOperation()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ModelingOperators();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMorphologyOperation"), 0, Get_Z_Construct_UEnum_ModelingOperators_EMorphologyOperation_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMorphologyOperation::Dilate", (int64)EMorphologyOperation::Dilate },
				{ "EMorphologyOperation::Contract", (int64)EMorphologyOperation::Contract },
				{ "EMorphologyOperation::Close", (int64)EMorphologyOperation::Close },
				{ "EMorphologyOperation::Open", (int64)EMorphologyOperation::Open },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Close.Comment", "/** Dilate and then contract, to delete small negative features (sharp inner corners, small holes) */" },
				{ "Close.Name", "EMorphologyOperation::Close" },
				{ "Close.ToolTip", "Dilate and then contract, to delete small negative features (sharp inner corners, small holes)" },
				{ "Comment", "/** Morphology operation types */" },
				{ "Contract.Comment", "/** Shrink the shapes inward */" },
				{ "Contract.Name", "EMorphologyOperation::Contract" },
				{ "Contract.ToolTip", "Shrink the shapes inward" },
				{ "Dilate.Comment", "/** Expand the shapes outward */" },
				{ "Dilate.Name", "EMorphologyOperation::Dilate" },
				{ "Dilate.ToolTip", "Expand the shapes outward" },
				{ "ModuleRelativePath", "Public/CompositionOps/VoxelMorphologyMeshesOp.h" },
				{ "Open.Comment", "/** Contract and then dilate, to delete small positive features (sharp outer corners, small isolated pieces) */" },
				{ "Open.Name", "EMorphologyOperation::Open" },
				{ "Open.ToolTip", "Contract and then dilate, to delete small positive features (sharp outer corners, small isolated pieces)" },
				{ "ToolTip", "Morphology operation types" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ModelingOperators,
				nullptr,
				"EMorphologyOperation",
				"EMorphologyOperation",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
