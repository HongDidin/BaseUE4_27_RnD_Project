// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/Physics/ExtractCollisionGeometryTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeExtractCollisionGeometryTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UExtractCollisionGeometryToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UExtractCollisionGeometryToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UExtractCollisionGeometryTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UExtractCollisionGeometryTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_USingleSelectionTool();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UCollisionGeometryVisualizationProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPhysicsObjectToolPropertySet_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPreviewGeometry_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPreviewMesh_NoRegister();
// End Cross Module References
	void UExtractCollisionGeometryToolBuilder::StaticRegisterNativesUExtractCollisionGeometryToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UExtractCollisionGeometryToolBuilder_NoRegister()
	{
		return UExtractCollisionGeometryToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UExtractCollisionGeometryToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UExtractCollisionGeometryToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UExtractCollisionGeometryToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Physics/ExtractCollisionGeometryTool.h" },
		{ "ModuleRelativePath", "Public/Physics/ExtractCollisionGeometryTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UExtractCollisionGeometryToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UExtractCollisionGeometryToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UExtractCollisionGeometryToolBuilder_Statics::ClassParams = {
		&UExtractCollisionGeometryToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UExtractCollisionGeometryToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UExtractCollisionGeometryToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UExtractCollisionGeometryToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UExtractCollisionGeometryToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UExtractCollisionGeometryToolBuilder, 276500843);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UExtractCollisionGeometryToolBuilder>()
	{
		return UExtractCollisionGeometryToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UExtractCollisionGeometryToolBuilder(Z_Construct_UClass_UExtractCollisionGeometryToolBuilder, &UExtractCollisionGeometryToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UExtractCollisionGeometryToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UExtractCollisionGeometryToolBuilder);
	void UExtractCollisionGeometryTool::StaticRegisterNativesUExtractCollisionGeometryTool()
	{
	}
	UClass* Z_Construct_UClass_UExtractCollisionGeometryTool_NoRegister()
	{
		return UExtractCollisionGeometryTool::StaticClass();
	}
	struct Z_Construct_UClass_UExtractCollisionGeometryTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VizSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_VizSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObjectProps_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ObjectProps;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviewElements_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviewElements;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviewMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviewMesh;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UExtractCollisionGeometryTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USingleSelectionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UExtractCollisionGeometryTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Mesh Inspector Tool for visualizing mesh information\n */" },
		{ "IncludePath", "Physics/ExtractCollisionGeometryTool.h" },
		{ "ModuleRelativePath", "Public/Physics/ExtractCollisionGeometryTool.h" },
		{ "ToolTip", "Mesh Inspector Tool for visualizing mesh information" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UExtractCollisionGeometryTool_Statics::NewProp_VizSettings_MetaData[] = {
		{ "ModuleRelativePath", "Public/Physics/ExtractCollisionGeometryTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UExtractCollisionGeometryTool_Statics::NewProp_VizSettings = { "VizSettings", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UExtractCollisionGeometryTool, VizSettings), Z_Construct_UClass_UCollisionGeometryVisualizationProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UExtractCollisionGeometryTool_Statics::NewProp_VizSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UExtractCollisionGeometryTool_Statics::NewProp_VizSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UExtractCollisionGeometryTool_Statics::NewProp_ObjectProps_MetaData[] = {
		{ "ModuleRelativePath", "Public/Physics/ExtractCollisionGeometryTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UExtractCollisionGeometryTool_Statics::NewProp_ObjectProps = { "ObjectProps", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UExtractCollisionGeometryTool, ObjectProps), Z_Construct_UClass_UPhysicsObjectToolPropertySet_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UExtractCollisionGeometryTool_Statics::NewProp_ObjectProps_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UExtractCollisionGeometryTool_Statics::NewProp_ObjectProps_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UExtractCollisionGeometryTool_Statics::NewProp_PreviewElements_MetaData[] = {
		{ "ModuleRelativePath", "Public/Physics/ExtractCollisionGeometryTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UExtractCollisionGeometryTool_Statics::NewProp_PreviewElements = { "PreviewElements", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UExtractCollisionGeometryTool, PreviewElements), Z_Construct_UClass_UPreviewGeometry_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UExtractCollisionGeometryTool_Statics::NewProp_PreviewElements_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UExtractCollisionGeometryTool_Statics::NewProp_PreviewElements_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UExtractCollisionGeometryTool_Statics::NewProp_PreviewMesh_MetaData[] = {
		{ "ModuleRelativePath", "Public/Physics/ExtractCollisionGeometryTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UExtractCollisionGeometryTool_Statics::NewProp_PreviewMesh = { "PreviewMesh", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UExtractCollisionGeometryTool, PreviewMesh), Z_Construct_UClass_UPreviewMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UExtractCollisionGeometryTool_Statics::NewProp_PreviewMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UExtractCollisionGeometryTool_Statics::NewProp_PreviewMesh_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UExtractCollisionGeometryTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UExtractCollisionGeometryTool_Statics::NewProp_VizSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UExtractCollisionGeometryTool_Statics::NewProp_ObjectProps,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UExtractCollisionGeometryTool_Statics::NewProp_PreviewElements,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UExtractCollisionGeometryTool_Statics::NewProp_PreviewMesh,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UExtractCollisionGeometryTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UExtractCollisionGeometryTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UExtractCollisionGeometryTool_Statics::ClassParams = {
		&UExtractCollisionGeometryTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UExtractCollisionGeometryTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UExtractCollisionGeometryTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UExtractCollisionGeometryTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UExtractCollisionGeometryTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UExtractCollisionGeometryTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UExtractCollisionGeometryTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UExtractCollisionGeometryTool, 3499326462);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UExtractCollisionGeometryTool>()
	{
		return UExtractCollisionGeometryTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UExtractCollisionGeometryTool(Z_Construct_UClass_UExtractCollisionGeometryTool, &UExtractCollisionGeometryTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UExtractCollisionGeometryTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UExtractCollisionGeometryTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
