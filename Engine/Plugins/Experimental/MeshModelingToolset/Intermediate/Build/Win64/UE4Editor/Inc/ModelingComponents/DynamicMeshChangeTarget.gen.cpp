// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ModelingComponents/Public/Changes/DynamicMeshChangeTarget.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDynamicMeshChangeTarget() {}
// Cross Module References
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UDynamicMeshReplacementChangeTarget_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UDynamicMeshReplacementChangeTarget();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_ModelingComponents();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UMeshReplacementCommandChangeTarget_NoRegister();
// End Cross Module References
	void UDynamicMeshReplacementChangeTarget::StaticRegisterNativesUDynamicMeshReplacementChangeTarget()
	{
	}
	UClass* Z_Construct_UClass_UDynamicMeshReplacementChangeTarget_NoRegister()
	{
		return UDynamicMeshReplacementChangeTarget::StaticClass();
	}
	struct Z_Construct_UClass_UDynamicMeshReplacementChangeTarget_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDynamicMeshReplacementChangeTarget_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_ModelingComponents,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDynamicMeshReplacementChangeTarget_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Bare bones wrapper of FDynamicMesh3 that supports MeshReplacementChange-based updates.  Shares the mesh ptr with the MeshReplacementChange objects, so it must not be changed directly\n * TODO: also support other MeshChange objects (by making a copy of the mesh when applying these changes)\n */" },
		{ "IncludePath", "Changes/DynamicMeshChangeTarget.h" },
		{ "ModuleRelativePath", "Public/Changes/DynamicMeshChangeTarget.h" },
		{ "ToolTip", "Bare bones wrapper of FDynamicMesh3 that supports MeshReplacementChange-based updates.  Shares the mesh ptr with the MeshReplacementChange objects, so it must not be changed directly\nTODO: also support other MeshChange objects (by making a copy of the mesh when applying these changes)" },
	};
#endif
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_UDynamicMeshReplacementChangeTarget_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UMeshReplacementCommandChangeTarget_NoRegister, (int32)VTABLE_OFFSET(UDynamicMeshReplacementChangeTarget, IMeshReplacementCommandChangeTarget), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDynamicMeshReplacementChangeTarget_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDynamicMeshReplacementChangeTarget>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDynamicMeshReplacementChangeTarget_Statics::ClassParams = {
		&UDynamicMeshReplacementChangeTarget::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		UE_ARRAY_COUNT(InterfaceParams),
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UDynamicMeshReplacementChangeTarget_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDynamicMeshReplacementChangeTarget_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDynamicMeshReplacementChangeTarget()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDynamicMeshReplacementChangeTarget_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDynamicMeshReplacementChangeTarget, 196260926);
	template<> MODELINGCOMPONENTS_API UClass* StaticClass<UDynamicMeshReplacementChangeTarget>()
	{
		return UDynamicMeshReplacementChangeTarget::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDynamicMeshReplacementChangeTarget(Z_Construct_UClass_UDynamicMeshReplacementChangeTarget, &UDynamicMeshReplacementChangeTarget::StaticClass, TEXT("/Script/ModelingComponents"), TEXT("UDynamicMeshReplacementChangeTarget"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDynamicMeshReplacementChangeTarget);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
