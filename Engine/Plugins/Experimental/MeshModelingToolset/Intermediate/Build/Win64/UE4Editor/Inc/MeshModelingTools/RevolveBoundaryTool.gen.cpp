// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/RevolveBoundaryTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRevolveBoundaryTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_URevolveBoundaryToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_URevolveBoundaryToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_URevolveBoundaryOperatorFactory_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_URevolveBoundaryOperatorFactory();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_URevolveBoundaryTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_URevolveBoundaryToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_URevolveBoundaryToolProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_URevolveProperties();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_URevolveBoundaryTool();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshBoundaryToolBase();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UNewMeshMaterialProperties_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UConstructionPlaneMechanic_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_NoRegister();
// End Cross Module References
	void URevolveBoundaryToolBuilder::StaticRegisterNativesURevolveBoundaryToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_URevolveBoundaryToolBuilder_NoRegister()
	{
		return URevolveBoundaryToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_URevolveBoundaryToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URevolveBoundaryToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URevolveBoundaryToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "// Tool Builder\n" },
		{ "IncludePath", "RevolveBoundaryTool.h" },
		{ "ModuleRelativePath", "Public/RevolveBoundaryTool.h" },
		{ "ToolTip", "Tool Builder" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_URevolveBoundaryToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URevolveBoundaryToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URevolveBoundaryToolBuilder_Statics::ClassParams = {
		&URevolveBoundaryToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_URevolveBoundaryToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URevolveBoundaryToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URevolveBoundaryToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URevolveBoundaryToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URevolveBoundaryToolBuilder, 1028401141);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<URevolveBoundaryToolBuilder>()
	{
		return URevolveBoundaryToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URevolveBoundaryToolBuilder(Z_Construct_UClass_URevolveBoundaryToolBuilder, &URevolveBoundaryToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("URevolveBoundaryToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URevolveBoundaryToolBuilder);
	void URevolveBoundaryOperatorFactory::StaticRegisterNativesURevolveBoundaryOperatorFactory()
	{
	}
	UClass* Z_Construct_UClass_URevolveBoundaryOperatorFactory_NoRegister()
	{
		return URevolveBoundaryOperatorFactory::StaticClass();
	}
	struct Z_Construct_UClass_URevolveBoundaryOperatorFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RevolveBoundaryTool_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RevolveBoundaryTool;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URevolveBoundaryOperatorFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URevolveBoundaryOperatorFactory_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "RevolveBoundaryTool.h" },
		{ "ModuleRelativePath", "Public/RevolveBoundaryTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URevolveBoundaryOperatorFactory_Statics::NewProp_RevolveBoundaryTool_MetaData[] = {
		{ "ModuleRelativePath", "Public/RevolveBoundaryTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_URevolveBoundaryOperatorFactory_Statics::NewProp_RevolveBoundaryTool = { "RevolveBoundaryTool", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URevolveBoundaryOperatorFactory, RevolveBoundaryTool), Z_Construct_UClass_URevolveBoundaryTool_NoRegister, METADATA_PARAMS(Z_Construct_UClass_URevolveBoundaryOperatorFactory_Statics::NewProp_RevolveBoundaryTool_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URevolveBoundaryOperatorFactory_Statics::NewProp_RevolveBoundaryTool_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URevolveBoundaryOperatorFactory_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URevolveBoundaryOperatorFactory_Statics::NewProp_RevolveBoundaryTool,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URevolveBoundaryOperatorFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URevolveBoundaryOperatorFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URevolveBoundaryOperatorFactory_Statics::ClassParams = {
		&URevolveBoundaryOperatorFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URevolveBoundaryOperatorFactory_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URevolveBoundaryOperatorFactory_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_URevolveBoundaryOperatorFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URevolveBoundaryOperatorFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URevolveBoundaryOperatorFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URevolveBoundaryOperatorFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URevolveBoundaryOperatorFactory, 2924025298);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<URevolveBoundaryOperatorFactory>()
	{
		return URevolveBoundaryOperatorFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URevolveBoundaryOperatorFactory(Z_Construct_UClass_URevolveBoundaryOperatorFactory, &URevolveBoundaryOperatorFactory::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("URevolveBoundaryOperatorFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URevolveBoundaryOperatorFactory);
	void URevolveBoundaryToolProperties::StaticRegisterNativesURevolveBoundaryToolProperties()
	{
	}
	UClass* Z_Construct_UClass_URevolveBoundaryToolProperties_NoRegister()
	{
		return URevolveBoundaryToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_URevolveBoundaryToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDisplayOriginalMesh_MetaData[];
#endif
		static void NewProp_bDisplayOriginalMesh_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDisplayOriginalMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AxisOrigin_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AxisOrigin;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AxisYaw_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AxisYaw;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AxisPitch_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AxisPitch;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSnapToWorldGrid_MetaData[];
#endif
		static void NewProp_bSnapToWorldGrid_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSnapToWorldGrid;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URevolveBoundaryToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_URevolveProperties,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URevolveBoundaryToolProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "RevolveBoundaryTool.h" },
		{ "ModuleRelativePath", "Public/RevolveBoundaryTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URevolveBoundaryToolProperties_Statics::NewProp_bDisplayOriginalMesh_MetaData[] = {
		{ "Category", "RevolveSettings" },
		{ "ModuleRelativePath", "Public/RevolveBoundaryTool.h" },
	};
#endif
	void Z_Construct_UClass_URevolveBoundaryToolProperties_Statics::NewProp_bDisplayOriginalMesh_SetBit(void* Obj)
	{
		((URevolveBoundaryToolProperties*)Obj)->bDisplayOriginalMesh = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URevolveBoundaryToolProperties_Statics::NewProp_bDisplayOriginalMesh = { "bDisplayOriginalMesh", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URevolveBoundaryToolProperties), &Z_Construct_UClass_URevolveBoundaryToolProperties_Statics::NewProp_bDisplayOriginalMesh_SetBit, METADATA_PARAMS(Z_Construct_UClass_URevolveBoundaryToolProperties_Statics::NewProp_bDisplayOriginalMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URevolveBoundaryToolProperties_Statics::NewProp_bDisplayOriginalMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URevolveBoundaryToolProperties_Statics::NewProp_AxisOrigin_MetaData[] = {
		{ "Category", "RevolutionAxis" },
		{ "ModuleRelativePath", "Public/RevolveBoundaryTool.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URevolveBoundaryToolProperties_Statics::NewProp_AxisOrigin = { "AxisOrigin", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URevolveBoundaryToolProperties, AxisOrigin), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_URevolveBoundaryToolProperties_Statics::NewProp_AxisOrigin_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URevolveBoundaryToolProperties_Statics::NewProp_AxisOrigin_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URevolveBoundaryToolProperties_Statics::NewProp_AxisYaw_MetaData[] = {
		{ "Category", "RevolutionAxis" },
		{ "ClampMax", "18000" },
		{ "ClampMin", "-180000" },
		{ "Comment", "//~ We don't use a rotator for axis orientation because one of the components (roll) \n//~ will never do anything in the case of our axis.\n" },
		{ "ModuleRelativePath", "Public/RevolveBoundaryTool.h" },
		{ "UIMax", "180" },
		{ "UIMin", "-180" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_URevolveBoundaryToolProperties_Statics::NewProp_AxisYaw = { "AxisYaw", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URevolveBoundaryToolProperties, AxisYaw), METADATA_PARAMS(Z_Construct_UClass_URevolveBoundaryToolProperties_Statics::NewProp_AxisYaw_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URevolveBoundaryToolProperties_Statics::NewProp_AxisYaw_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URevolveBoundaryToolProperties_Statics::NewProp_AxisPitch_MetaData[] = {
		{ "Category", "RevolutionAxis" },
		{ "ClampMax", "18000" },
		{ "ClampMin", "-180000" },
		{ "ModuleRelativePath", "Public/RevolveBoundaryTool.h" },
		{ "UIMax", "180" },
		{ "UIMin", "-180" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_URevolveBoundaryToolProperties_Statics::NewProp_AxisPitch = { "AxisPitch", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URevolveBoundaryToolProperties, AxisPitch), METADATA_PARAMS(Z_Construct_UClass_URevolveBoundaryToolProperties_Statics::NewProp_AxisPitch_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URevolveBoundaryToolProperties_Statics::NewProp_AxisPitch_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URevolveBoundaryToolProperties_Statics::NewProp_bSnapToWorldGrid_MetaData[] = {
		{ "Category", "RevolutionAxis" },
		{ "Comment", "/** Determines whether the axis control widget snaps to world grid (only relevant if world coordinate mode is active in viewport) .*/" },
		{ "ModuleRelativePath", "Public/RevolveBoundaryTool.h" },
		{ "ToolTip", "Determines whether the axis control widget snaps to world grid (only relevant if world coordinate mode is active in viewport) ." },
	};
#endif
	void Z_Construct_UClass_URevolveBoundaryToolProperties_Statics::NewProp_bSnapToWorldGrid_SetBit(void* Obj)
	{
		((URevolveBoundaryToolProperties*)Obj)->bSnapToWorldGrid = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URevolveBoundaryToolProperties_Statics::NewProp_bSnapToWorldGrid = { "bSnapToWorldGrid", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URevolveBoundaryToolProperties), &Z_Construct_UClass_URevolveBoundaryToolProperties_Statics::NewProp_bSnapToWorldGrid_SetBit, METADATA_PARAMS(Z_Construct_UClass_URevolveBoundaryToolProperties_Statics::NewProp_bSnapToWorldGrid_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URevolveBoundaryToolProperties_Statics::NewProp_bSnapToWorldGrid_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URevolveBoundaryToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URevolveBoundaryToolProperties_Statics::NewProp_bDisplayOriginalMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URevolveBoundaryToolProperties_Statics::NewProp_AxisOrigin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URevolveBoundaryToolProperties_Statics::NewProp_AxisYaw,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URevolveBoundaryToolProperties_Statics::NewProp_AxisPitch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URevolveBoundaryToolProperties_Statics::NewProp_bSnapToWorldGrid,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URevolveBoundaryToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URevolveBoundaryToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URevolveBoundaryToolProperties_Statics::ClassParams = {
		&URevolveBoundaryToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URevolveBoundaryToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URevolveBoundaryToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_URevolveBoundaryToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URevolveBoundaryToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URevolveBoundaryToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URevolveBoundaryToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URevolveBoundaryToolProperties, 3024268694);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<URevolveBoundaryToolProperties>()
	{
		return URevolveBoundaryToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URevolveBoundaryToolProperties(Z_Construct_UClass_URevolveBoundaryToolProperties, &URevolveBoundaryToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("URevolveBoundaryToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URevolveBoundaryToolProperties);
	void URevolveBoundaryTool::StaticRegisterNativesURevolveBoundaryTool()
	{
	}
	UClass* Z_Construct_UClass_URevolveBoundaryTool_NoRegister()
	{
		return URevolveBoundaryTool::StaticClass();
	}
	struct Z_Construct_UClass_URevolveBoundaryTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Settings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Settings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaterialProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MaterialProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlaneMechanic_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PlaneMechanic;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Preview_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Preview;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URevolveBoundaryTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshBoundaryToolBase,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URevolveBoundaryTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** \n * Tool that revolves the boundary of a mesh around an axis to create a new mesh. Mainly useful for\n * revolving planar meshes. \n */" },
		{ "IncludePath", "RevolveBoundaryTool.h" },
		{ "ModuleRelativePath", "Public/RevolveBoundaryTool.h" },
		{ "ToolTip", "Tool that revolves the boundary of a mesh around an axis to create a new mesh. Mainly useful for\nrevolving planar meshes." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URevolveBoundaryTool_Statics::NewProp_Settings_MetaData[] = {
		{ "ModuleRelativePath", "Public/RevolveBoundaryTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_URevolveBoundaryTool_Statics::NewProp_Settings = { "Settings", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URevolveBoundaryTool, Settings), Z_Construct_UClass_URevolveBoundaryToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_URevolveBoundaryTool_Statics::NewProp_Settings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URevolveBoundaryTool_Statics::NewProp_Settings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URevolveBoundaryTool_Statics::NewProp_MaterialProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/RevolveBoundaryTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_URevolveBoundaryTool_Statics::NewProp_MaterialProperties = { "MaterialProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URevolveBoundaryTool, MaterialProperties), Z_Construct_UClass_UNewMeshMaterialProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_URevolveBoundaryTool_Statics::NewProp_MaterialProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URevolveBoundaryTool_Statics::NewProp_MaterialProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URevolveBoundaryTool_Statics::NewProp_PlaneMechanic_MetaData[] = {
		{ "ModuleRelativePath", "Public/RevolveBoundaryTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_URevolveBoundaryTool_Statics::NewProp_PlaneMechanic = { "PlaneMechanic", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URevolveBoundaryTool, PlaneMechanic), Z_Construct_UClass_UConstructionPlaneMechanic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_URevolveBoundaryTool_Statics::NewProp_PlaneMechanic_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URevolveBoundaryTool_Statics::NewProp_PlaneMechanic_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URevolveBoundaryTool_Statics::NewProp_Preview_MetaData[] = {
		{ "ModuleRelativePath", "Public/RevolveBoundaryTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_URevolveBoundaryTool_Statics::NewProp_Preview = { "Preview", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URevolveBoundaryTool, Preview), Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_NoRegister, METADATA_PARAMS(Z_Construct_UClass_URevolveBoundaryTool_Statics::NewProp_Preview_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URevolveBoundaryTool_Statics::NewProp_Preview_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URevolveBoundaryTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URevolveBoundaryTool_Statics::NewProp_Settings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URevolveBoundaryTool_Statics::NewProp_MaterialProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URevolveBoundaryTool_Statics::NewProp_PlaneMechanic,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URevolveBoundaryTool_Statics::NewProp_Preview,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URevolveBoundaryTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URevolveBoundaryTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URevolveBoundaryTool_Statics::ClassParams = {
		&URevolveBoundaryTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URevolveBoundaryTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URevolveBoundaryTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_URevolveBoundaryTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URevolveBoundaryTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URevolveBoundaryTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URevolveBoundaryTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URevolveBoundaryTool, 1698469747);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<URevolveBoundaryTool>()
	{
		return URevolveBoundaryTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URevolveBoundaryTool(Z_Construct_UClass_URevolveBoundaryTool, &URevolveBoundaryTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("URevolveBoundaryTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URevolveBoundaryTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
