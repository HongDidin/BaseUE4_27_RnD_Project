// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ModelingComponents/Public/Drawing/PreviewGeometryActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePreviewGeometryActor() {}
// Cross Module References
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_APreviewGeometryActor_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_APreviewGeometryActor();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_AInternalToolFrameworkActor();
	UPackage* Z_Construct_UPackage__Script_ModelingComponents();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPreviewGeometry_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPreviewGeometry();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_ULineSetComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UWorld_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
// End Cross Module References
	void APreviewGeometryActor::StaticRegisterNativesAPreviewGeometryActor()
	{
	}
	UClass* Z_Construct_UClass_APreviewGeometryActor_NoRegister()
	{
		return APreviewGeometryActor::StaticClass();
	}
	struct Z_Construct_UClass_APreviewGeometryActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APreviewGeometryActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AInternalToolFrameworkActor,
		(UObject* (*)())Z_Construct_UPackage__Script_ModelingComponents,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APreviewGeometryActor_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * An actor suitable for attaching components used to draw preview elements, such as LineSetComponent and TriangleSetComponent.\n */" },
		{ "IncludePath", "Drawing/PreviewGeometryActor.h" },
		{ "ModuleRelativePath", "Public/Drawing/PreviewGeometryActor.h" },
		{ "ToolTip", "An actor suitable for attaching components used to draw preview elements, such as LineSetComponent and TriangleSetComponent." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_APreviewGeometryActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APreviewGeometryActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APreviewGeometryActor_Statics::ClassParams = {
		&APreviewGeometryActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000ACu,
		METADATA_PARAMS(Z_Construct_UClass_APreviewGeometryActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_APreviewGeometryActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APreviewGeometryActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APreviewGeometryActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APreviewGeometryActor, 2549694592);
	template<> MODELINGCOMPONENTS_API UClass* StaticClass<APreviewGeometryActor>()
	{
		return APreviewGeometryActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APreviewGeometryActor(Z_Construct_UClass_APreviewGeometryActor, &APreviewGeometryActor::StaticClass, TEXT("/Script/ModelingComponents"), TEXT("APreviewGeometryActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APreviewGeometryActor);
	DEFINE_FUNCTION(UPreviewGeometry::execSetAllLineSetsMaterial)
	{
		P_GET_OBJECT(UMaterialInterface,Z_Param_Material);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetAllLineSetsMaterial(Z_Param_Material);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPreviewGeometry::execSetLineSetMaterial)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_LineSetIdentifier);
		P_GET_OBJECT(UMaterialInterface,Z_Param_NewMaterial);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->SetLineSetMaterial(Z_Param_LineSetIdentifier,Z_Param_NewMaterial);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPreviewGeometry::execSetLineSetVisibility)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_LineSetIdentifier);
		P_GET_UBOOL(Z_Param_bVisible);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->SetLineSetVisibility(Z_Param_LineSetIdentifier,Z_Param_bVisible);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPreviewGeometry::execRemoveAllLineSets)
	{
		P_GET_UBOOL(Z_Param_bDestroy);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->RemoveAllLineSets(Z_Param_bDestroy);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPreviewGeometry::execRemoveLineSet)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_LineSetIdentifier);
		P_GET_UBOOL(Z_Param_bDestroy);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->RemoveLineSet(Z_Param_LineSetIdentifier,Z_Param_bDestroy);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPreviewGeometry::execFindLineSet)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_LineSetIdentifier);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(ULineSetComponent**)Z_Param__Result=P_THIS->FindLineSet(Z_Param_LineSetIdentifier);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPreviewGeometry::execAddLineSet)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_LineSetIdentifier);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(ULineSetComponent**)Z_Param__Result=P_THIS->AddLineSet(Z_Param_LineSetIdentifier);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPreviewGeometry::execGetActor)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(APreviewGeometryActor**)Z_Param__Result=P_THIS->GetActor();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPreviewGeometry::execDisconnect)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Disconnect();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPreviewGeometry::execCreateInWorld)
	{
		P_GET_OBJECT(UWorld,Z_Param_World);
		P_GET_STRUCT_REF(FTransform,Z_Param_Out_WithTransform);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->CreateInWorld(Z_Param_World,Z_Param_Out_WithTransform);
		P_NATIVE_END;
	}
	void UPreviewGeometry::StaticRegisterNativesUPreviewGeometry()
	{
		UClass* Class = UPreviewGeometry::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddLineSet", &UPreviewGeometry::execAddLineSet },
			{ "CreateInWorld", &UPreviewGeometry::execCreateInWorld },
			{ "Disconnect", &UPreviewGeometry::execDisconnect },
			{ "FindLineSet", &UPreviewGeometry::execFindLineSet },
			{ "GetActor", &UPreviewGeometry::execGetActor },
			{ "RemoveAllLineSets", &UPreviewGeometry::execRemoveAllLineSets },
			{ "RemoveLineSet", &UPreviewGeometry::execRemoveLineSet },
			{ "SetAllLineSetsMaterial", &UPreviewGeometry::execSetAllLineSetsMaterial },
			{ "SetLineSetMaterial", &UPreviewGeometry::execSetLineSetMaterial },
			{ "SetLineSetVisibility", &UPreviewGeometry::execSetLineSetVisibility },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UPreviewGeometry_AddLineSet_Statics
	{
		struct PreviewGeometry_eventAddLineSet_Parms
		{
			FString LineSetIdentifier;
			ULineSetComponent* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LineSetIdentifier_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_LineSetIdentifier;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPreviewGeometry_AddLineSet_Statics::NewProp_LineSetIdentifier_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UPreviewGeometry_AddLineSet_Statics::NewProp_LineSetIdentifier = { "LineSetIdentifier", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PreviewGeometry_eventAddLineSet_Parms, LineSetIdentifier), METADATA_PARAMS(Z_Construct_UFunction_UPreviewGeometry_AddLineSet_Statics::NewProp_LineSetIdentifier_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPreviewGeometry_AddLineSet_Statics::NewProp_LineSetIdentifier_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPreviewGeometry_AddLineSet_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UPreviewGeometry_AddLineSet_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PreviewGeometry_eventAddLineSet_Parms, ReturnValue), Z_Construct_UClass_ULineSetComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UPreviewGeometry_AddLineSet_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPreviewGeometry_AddLineSet_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPreviewGeometry_AddLineSet_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPreviewGeometry_AddLineSet_Statics::NewProp_LineSetIdentifier,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPreviewGeometry_AddLineSet_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPreviewGeometry_AddLineSet_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Create a new line set with the given LineSetIdentifier and return it */" },
		{ "ModuleRelativePath", "Public/Drawing/PreviewGeometryActor.h" },
		{ "ToolTip", "Create a new line set with the given LineSetIdentifier and return it" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPreviewGeometry_AddLineSet_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPreviewGeometry, nullptr, "AddLineSet", nullptr, nullptr, sizeof(PreviewGeometry_eventAddLineSet_Parms), Z_Construct_UFunction_UPreviewGeometry_AddLineSet_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPreviewGeometry_AddLineSet_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPreviewGeometry_AddLineSet_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPreviewGeometry_AddLineSet_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPreviewGeometry_AddLineSet()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPreviewGeometry_AddLineSet_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPreviewGeometry_CreateInWorld_Statics
	{
		struct PreviewGeometry_eventCreateInWorld_Parms
		{
			UWorld* World;
			FTransform WithTransform;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_World;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WithTransform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WithTransform;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UPreviewGeometry_CreateInWorld_Statics::NewProp_World = { "World", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PreviewGeometry_eventCreateInWorld_Parms, World), Z_Construct_UClass_UWorld_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPreviewGeometry_CreateInWorld_Statics::NewProp_WithTransform_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UPreviewGeometry_CreateInWorld_Statics::NewProp_WithTransform = { "WithTransform", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PreviewGeometry_eventCreateInWorld_Parms, WithTransform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UFunction_UPreviewGeometry_CreateInWorld_Statics::NewProp_WithTransform_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPreviewGeometry_CreateInWorld_Statics::NewProp_WithTransform_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPreviewGeometry_CreateInWorld_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPreviewGeometry_CreateInWorld_Statics::NewProp_World,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPreviewGeometry_CreateInWorld_Statics::NewProp_WithTransform,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPreviewGeometry_CreateInWorld_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n\x09 * Create preview mesh in the World with the given transform\n\x09 */" },
		{ "ModuleRelativePath", "Public/Drawing/PreviewGeometryActor.h" },
		{ "ToolTip", "Create preview mesh in the World with the given transform" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPreviewGeometry_CreateInWorld_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPreviewGeometry, nullptr, "CreateInWorld", nullptr, nullptr, sizeof(PreviewGeometry_eventCreateInWorld_Parms), Z_Construct_UFunction_UPreviewGeometry_CreateInWorld_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPreviewGeometry_CreateInWorld_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPreviewGeometry_CreateInWorld_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPreviewGeometry_CreateInWorld_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPreviewGeometry_CreateInWorld()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPreviewGeometry_CreateInWorld_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPreviewGeometry_Disconnect_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPreviewGeometry_Disconnect_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n\x09 * Remove and destroy preview mesh\n\x09 */" },
		{ "ModuleRelativePath", "Public/Drawing/PreviewGeometryActor.h" },
		{ "ToolTip", "Remove and destroy preview mesh" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPreviewGeometry_Disconnect_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPreviewGeometry, nullptr, "Disconnect", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPreviewGeometry_Disconnect_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPreviewGeometry_Disconnect_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPreviewGeometry_Disconnect()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPreviewGeometry_Disconnect_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPreviewGeometry_FindLineSet_Statics
	{
		struct PreviewGeometry_eventFindLineSet_Parms
		{
			FString LineSetIdentifier;
			ULineSetComponent* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LineSetIdentifier_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_LineSetIdentifier;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPreviewGeometry_FindLineSet_Statics::NewProp_LineSetIdentifier_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UPreviewGeometry_FindLineSet_Statics::NewProp_LineSetIdentifier = { "LineSetIdentifier", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PreviewGeometry_eventFindLineSet_Parms, LineSetIdentifier), METADATA_PARAMS(Z_Construct_UFunction_UPreviewGeometry_FindLineSet_Statics::NewProp_LineSetIdentifier_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPreviewGeometry_FindLineSet_Statics::NewProp_LineSetIdentifier_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPreviewGeometry_FindLineSet_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UPreviewGeometry_FindLineSet_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PreviewGeometry_eventFindLineSet_Parms, ReturnValue), Z_Construct_UClass_ULineSetComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UPreviewGeometry_FindLineSet_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPreviewGeometry_FindLineSet_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPreviewGeometry_FindLineSet_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPreviewGeometry_FindLineSet_Statics::NewProp_LineSetIdentifier,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPreviewGeometry_FindLineSet_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPreviewGeometry_FindLineSet_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** @return the LineSetComponent with the given LineSetIdentifier, or nullptr if not found */" },
		{ "ModuleRelativePath", "Public/Drawing/PreviewGeometryActor.h" },
		{ "ToolTip", "@return the LineSetComponent with the given LineSetIdentifier, or nullptr if not found" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPreviewGeometry_FindLineSet_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPreviewGeometry, nullptr, "FindLineSet", nullptr, nullptr, sizeof(PreviewGeometry_eventFindLineSet_Parms), Z_Construct_UFunction_UPreviewGeometry_FindLineSet_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPreviewGeometry_FindLineSet_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPreviewGeometry_FindLineSet_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPreviewGeometry_FindLineSet_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPreviewGeometry_FindLineSet()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPreviewGeometry_FindLineSet_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPreviewGeometry_GetActor_Statics
	{
		struct PreviewGeometry_eventGetActor_Parms
		{
			APreviewGeometryActor* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UPreviewGeometry_GetActor_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PreviewGeometry_eventGetActor_Parms, ReturnValue), Z_Construct_UClass_APreviewGeometryActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPreviewGeometry_GetActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPreviewGeometry_GetActor_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPreviewGeometry_GetActor_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** @return the preview geometry actor created by this class */" },
		{ "ModuleRelativePath", "Public/Drawing/PreviewGeometryActor.h" },
		{ "ToolTip", "@return the preview geometry actor created by this class" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPreviewGeometry_GetActor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPreviewGeometry, nullptr, "GetActor", nullptr, nullptr, sizeof(PreviewGeometry_eventGetActor_Parms), Z_Construct_UFunction_UPreviewGeometry_GetActor_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPreviewGeometry_GetActor_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x40020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPreviewGeometry_GetActor_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPreviewGeometry_GetActor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPreviewGeometry_GetActor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPreviewGeometry_GetActor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPreviewGeometry_RemoveAllLineSets_Statics
	{
		struct PreviewGeometry_eventRemoveAllLineSets_Parms
		{
			bool bDestroy;
		};
		static void NewProp_bDestroy_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDestroy;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UPreviewGeometry_RemoveAllLineSets_Statics::NewProp_bDestroy_SetBit(void* Obj)
	{
		((PreviewGeometry_eventRemoveAllLineSets_Parms*)Obj)->bDestroy = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UPreviewGeometry_RemoveAllLineSets_Statics::NewProp_bDestroy = { "bDestroy", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(PreviewGeometry_eventRemoveAllLineSets_Parms), &Z_Construct_UFunction_UPreviewGeometry_RemoveAllLineSets_Statics::NewProp_bDestroy_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPreviewGeometry_RemoveAllLineSets_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPreviewGeometry_RemoveAllLineSets_Statics::NewProp_bDestroy,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPreviewGeometry_RemoveAllLineSets_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n\x09 * Remove all LineSetComponents\n\x09 * @param bDestroy if true, the components will unregistered and destroyed.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Drawing/PreviewGeometryActor.h" },
		{ "ToolTip", "Remove all LineSetComponents\n@param bDestroy if true, the components will unregistered and destroyed." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPreviewGeometry_RemoveAllLineSets_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPreviewGeometry, nullptr, "RemoveAllLineSets", nullptr, nullptr, sizeof(PreviewGeometry_eventRemoveAllLineSets_Parms), Z_Construct_UFunction_UPreviewGeometry_RemoveAllLineSets_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPreviewGeometry_RemoveAllLineSets_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPreviewGeometry_RemoveAllLineSets_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPreviewGeometry_RemoveAllLineSets_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPreviewGeometry_RemoveAllLineSets()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPreviewGeometry_RemoveAllLineSets_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPreviewGeometry_RemoveLineSet_Statics
	{
		struct PreviewGeometry_eventRemoveLineSet_Parms
		{
			FString LineSetIdentifier;
			bool bDestroy;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LineSetIdentifier_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_LineSetIdentifier;
		static void NewProp_bDestroy_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDestroy;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPreviewGeometry_RemoveLineSet_Statics::NewProp_LineSetIdentifier_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UPreviewGeometry_RemoveLineSet_Statics::NewProp_LineSetIdentifier = { "LineSetIdentifier", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PreviewGeometry_eventRemoveLineSet_Parms, LineSetIdentifier), METADATA_PARAMS(Z_Construct_UFunction_UPreviewGeometry_RemoveLineSet_Statics::NewProp_LineSetIdentifier_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPreviewGeometry_RemoveLineSet_Statics::NewProp_LineSetIdentifier_MetaData)) };
	void Z_Construct_UFunction_UPreviewGeometry_RemoveLineSet_Statics::NewProp_bDestroy_SetBit(void* Obj)
	{
		((PreviewGeometry_eventRemoveLineSet_Parms*)Obj)->bDestroy = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UPreviewGeometry_RemoveLineSet_Statics::NewProp_bDestroy = { "bDestroy", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(PreviewGeometry_eventRemoveLineSet_Parms), &Z_Construct_UFunction_UPreviewGeometry_RemoveLineSet_Statics::NewProp_bDestroy_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UPreviewGeometry_RemoveLineSet_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((PreviewGeometry_eventRemoveLineSet_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UPreviewGeometry_RemoveLineSet_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(PreviewGeometry_eventRemoveLineSet_Parms), &Z_Construct_UFunction_UPreviewGeometry_RemoveLineSet_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPreviewGeometry_RemoveLineSet_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPreviewGeometry_RemoveLineSet_Statics::NewProp_LineSetIdentifier,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPreviewGeometry_RemoveLineSet_Statics::NewProp_bDestroy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPreviewGeometry_RemoveLineSet_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPreviewGeometry_RemoveLineSet_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** \n\x09 * Remove the LineSetComponent with the given LineSetIdentifier\n\x09 * @param bDestroy if true, component will unregistered and destroyed. \n\x09 * @return true if the LineSetComponent was found and removed\n\x09 */" },
		{ "ModuleRelativePath", "Public/Drawing/PreviewGeometryActor.h" },
		{ "ToolTip", "Remove the LineSetComponent with the given LineSetIdentifier\n@param bDestroy if true, component will unregistered and destroyed.\n@return true if the LineSetComponent was found and removed" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPreviewGeometry_RemoveLineSet_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPreviewGeometry, nullptr, "RemoveLineSet", nullptr, nullptr, sizeof(PreviewGeometry_eventRemoveLineSet_Parms), Z_Construct_UFunction_UPreviewGeometry_RemoveLineSet_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPreviewGeometry_RemoveLineSet_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPreviewGeometry_RemoveLineSet_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPreviewGeometry_RemoveLineSet_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPreviewGeometry_RemoveLineSet()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPreviewGeometry_RemoveLineSet_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPreviewGeometry_SetAllLineSetsMaterial_Statics
	{
		struct PreviewGeometry_eventSetAllLineSetsMaterial_Parms
		{
			UMaterialInterface* Material;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Material;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UPreviewGeometry_SetAllLineSetsMaterial_Statics::NewProp_Material = { "Material", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PreviewGeometry_eventSetAllLineSetsMaterial_Parms, Material), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPreviewGeometry_SetAllLineSetsMaterial_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPreviewGeometry_SetAllLineSetsMaterial_Statics::NewProp_Material,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPreviewGeometry_SetAllLineSetsMaterial_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n\x09 * Set the Material of all LineSetComponents\n\x09 */" },
		{ "ModuleRelativePath", "Public/Drawing/PreviewGeometryActor.h" },
		{ "ToolTip", "Set the Material of all LineSetComponents" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPreviewGeometry_SetAllLineSetsMaterial_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPreviewGeometry, nullptr, "SetAllLineSetsMaterial", nullptr, nullptr, sizeof(PreviewGeometry_eventSetAllLineSetsMaterial_Parms), Z_Construct_UFunction_UPreviewGeometry_SetAllLineSetsMaterial_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPreviewGeometry_SetAllLineSetsMaterial_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPreviewGeometry_SetAllLineSetsMaterial_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPreviewGeometry_SetAllLineSetsMaterial_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPreviewGeometry_SetAllLineSetsMaterial()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPreviewGeometry_SetAllLineSetsMaterial_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPreviewGeometry_SetLineSetMaterial_Statics
	{
		struct PreviewGeometry_eventSetLineSetMaterial_Parms
		{
			FString LineSetIdentifier;
			UMaterialInterface* NewMaterial;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LineSetIdentifier_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_LineSetIdentifier;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_NewMaterial;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPreviewGeometry_SetLineSetMaterial_Statics::NewProp_LineSetIdentifier_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UPreviewGeometry_SetLineSetMaterial_Statics::NewProp_LineSetIdentifier = { "LineSetIdentifier", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PreviewGeometry_eventSetLineSetMaterial_Parms, LineSetIdentifier), METADATA_PARAMS(Z_Construct_UFunction_UPreviewGeometry_SetLineSetMaterial_Statics::NewProp_LineSetIdentifier_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPreviewGeometry_SetLineSetMaterial_Statics::NewProp_LineSetIdentifier_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UPreviewGeometry_SetLineSetMaterial_Statics::NewProp_NewMaterial = { "NewMaterial", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PreviewGeometry_eventSetLineSetMaterial_Parms, NewMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UPreviewGeometry_SetLineSetMaterial_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((PreviewGeometry_eventSetLineSetMaterial_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UPreviewGeometry_SetLineSetMaterial_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(PreviewGeometry_eventSetLineSetMaterial_Parms), &Z_Construct_UFunction_UPreviewGeometry_SetLineSetMaterial_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPreviewGeometry_SetLineSetMaterial_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPreviewGeometry_SetLineSetMaterial_Statics::NewProp_LineSetIdentifier,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPreviewGeometry_SetLineSetMaterial_Statics::NewProp_NewMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPreviewGeometry_SetLineSetMaterial_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPreviewGeometry_SetLineSetMaterial_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n\x09 * Set the Material of the LineSetComponent with the given LineSetIdentifier\n\x09 * @return true if the LineSetComponent was found and updated\n\x09 */" },
		{ "ModuleRelativePath", "Public/Drawing/PreviewGeometryActor.h" },
		{ "ToolTip", "Set the Material of the LineSetComponent with the given LineSetIdentifier\n@return true if the LineSetComponent was found and updated" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPreviewGeometry_SetLineSetMaterial_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPreviewGeometry, nullptr, "SetLineSetMaterial", nullptr, nullptr, sizeof(PreviewGeometry_eventSetLineSetMaterial_Parms), Z_Construct_UFunction_UPreviewGeometry_SetLineSetMaterial_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPreviewGeometry_SetLineSetMaterial_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPreviewGeometry_SetLineSetMaterial_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPreviewGeometry_SetLineSetMaterial_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPreviewGeometry_SetLineSetMaterial()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPreviewGeometry_SetLineSetMaterial_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPreviewGeometry_SetLineSetVisibility_Statics
	{
		struct PreviewGeometry_eventSetLineSetVisibility_Parms
		{
			FString LineSetIdentifier;
			bool bVisible;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LineSetIdentifier_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_LineSetIdentifier;
		static void NewProp_bVisible_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bVisible;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPreviewGeometry_SetLineSetVisibility_Statics::NewProp_LineSetIdentifier_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UPreviewGeometry_SetLineSetVisibility_Statics::NewProp_LineSetIdentifier = { "LineSetIdentifier", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PreviewGeometry_eventSetLineSetVisibility_Parms, LineSetIdentifier), METADATA_PARAMS(Z_Construct_UFunction_UPreviewGeometry_SetLineSetVisibility_Statics::NewProp_LineSetIdentifier_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPreviewGeometry_SetLineSetVisibility_Statics::NewProp_LineSetIdentifier_MetaData)) };
	void Z_Construct_UFunction_UPreviewGeometry_SetLineSetVisibility_Statics::NewProp_bVisible_SetBit(void* Obj)
	{
		((PreviewGeometry_eventSetLineSetVisibility_Parms*)Obj)->bVisible = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UPreviewGeometry_SetLineSetVisibility_Statics::NewProp_bVisible = { "bVisible", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(PreviewGeometry_eventSetLineSetVisibility_Parms), &Z_Construct_UFunction_UPreviewGeometry_SetLineSetVisibility_Statics::NewProp_bVisible_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UPreviewGeometry_SetLineSetVisibility_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((PreviewGeometry_eventSetLineSetVisibility_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UPreviewGeometry_SetLineSetVisibility_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(PreviewGeometry_eventSetLineSetVisibility_Parms), &Z_Construct_UFunction_UPreviewGeometry_SetLineSetVisibility_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPreviewGeometry_SetLineSetVisibility_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPreviewGeometry_SetLineSetVisibility_Statics::NewProp_LineSetIdentifier,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPreviewGeometry_SetLineSetVisibility_Statics::NewProp_bVisible,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPreviewGeometry_SetLineSetVisibility_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPreviewGeometry_SetLineSetVisibility_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n\x09 * Set the visibility of the LineSetComponent with the given LineSetIdentifier\n\x09 * @return true if the LineSetComponent was found and updated\n\x09 */" },
		{ "ModuleRelativePath", "Public/Drawing/PreviewGeometryActor.h" },
		{ "ToolTip", "Set the visibility of the LineSetComponent with the given LineSetIdentifier\n@return true if the LineSetComponent was found and updated" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPreviewGeometry_SetLineSetVisibility_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPreviewGeometry, nullptr, "SetLineSetVisibility", nullptr, nullptr, sizeof(PreviewGeometry_eventSetLineSetVisibility_Parms), Z_Construct_UFunction_UPreviewGeometry_SetLineSetVisibility_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPreviewGeometry_SetLineSetVisibility_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPreviewGeometry_SetLineSetVisibility_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPreviewGeometry_SetLineSetVisibility_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPreviewGeometry_SetLineSetVisibility()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPreviewGeometry_SetLineSetVisibility_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UPreviewGeometry_NoRegister()
	{
		return UPreviewGeometry::StaticClass();
	}
	struct Z_Construct_UClass_UPreviewGeometry_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParentActor_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ParentActor;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LineSets_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_LineSets_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LineSets_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_LineSets;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPreviewGeometry_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_ModelingComponents,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UPreviewGeometry_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UPreviewGeometry_AddLineSet, "AddLineSet" }, // 629013028
		{ &Z_Construct_UFunction_UPreviewGeometry_CreateInWorld, "CreateInWorld" }, // 1203018363
		{ &Z_Construct_UFunction_UPreviewGeometry_Disconnect, "Disconnect" }, // 2515448023
		{ &Z_Construct_UFunction_UPreviewGeometry_FindLineSet, "FindLineSet" }, // 1575174174
		{ &Z_Construct_UFunction_UPreviewGeometry_GetActor, "GetActor" }, // 2502087296
		{ &Z_Construct_UFunction_UPreviewGeometry_RemoveAllLineSets, "RemoveAllLineSets" }, // 2276595801
		{ &Z_Construct_UFunction_UPreviewGeometry_RemoveLineSet, "RemoveLineSet" }, // 4239267042
		{ &Z_Construct_UFunction_UPreviewGeometry_SetAllLineSetsMaterial, "SetAllLineSetsMaterial" }, // 712902817
		{ &Z_Construct_UFunction_UPreviewGeometry_SetLineSetMaterial, "SetLineSetMaterial" }, // 1297658339
		{ &Z_Construct_UFunction_UPreviewGeometry_SetLineSetVisibility, "SetLineSetVisibility" }, // 2498179201
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPreviewGeometry_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * UPreviewGeometry creates and manages an APreviewGeometryActor and a set of preview geometry Components.\n * Preview geometry Components are identified by strings.\n */" },
		{ "IncludePath", "Drawing/PreviewGeometryActor.h" },
		{ "ModuleRelativePath", "Public/Drawing/PreviewGeometryActor.h" },
		{ "ToolTip", "UPreviewGeometry creates and manages an APreviewGeometryActor and a set of preview geometry Components.\nPreview geometry Components are identified by strings." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPreviewGeometry_Statics::NewProp_ParentActor_MetaData[] = {
		{ "Comment", "/**\n\x09 * Actor created and managed by the UPreviewGeometry\n\x09 */" },
		{ "ModuleRelativePath", "Public/Drawing/PreviewGeometryActor.h" },
		{ "ToolTip", "Actor created and managed by the UPreviewGeometry" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPreviewGeometry_Statics::NewProp_ParentActor = { "ParentActor", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPreviewGeometry, ParentActor), Z_Construct_UClass_APreviewGeometryActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPreviewGeometry_Statics::NewProp_ParentActor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPreviewGeometry_Statics::NewProp_ParentActor_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPreviewGeometry_Statics::NewProp_LineSets_ValueProp = { "LineSets", nullptr, (EPropertyFlags)0x0000000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_ULineSetComponent_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UPreviewGeometry_Statics::NewProp_LineSets_Key_KeyProp = { "LineSets_Key", nullptr, (EPropertyFlags)0x0000000000080008, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPreviewGeometry_Statics::NewProp_LineSets_MetaData[] = {
		{ "Comment", "/**\n\x09 * LineSetComponents created and owned by the UPreviewGeometry, and added as child components of the ParentActor\n\x09 */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Drawing/PreviewGeometryActor.h" },
		{ "ToolTip", "LineSetComponents created and owned by the UPreviewGeometry, and added as child components of the ParentActor" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UPreviewGeometry_Statics::NewProp_LineSets = { "LineSets", nullptr, (EPropertyFlags)0x0010008000000008, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPreviewGeometry, LineSets), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UPreviewGeometry_Statics::NewProp_LineSets_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPreviewGeometry_Statics::NewProp_LineSets_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPreviewGeometry_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPreviewGeometry_Statics::NewProp_ParentActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPreviewGeometry_Statics::NewProp_LineSets_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPreviewGeometry_Statics::NewProp_LineSets_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPreviewGeometry_Statics::NewProp_LineSets,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPreviewGeometry_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPreviewGeometry>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPreviewGeometry_Statics::ClassParams = {
		&UPreviewGeometry::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UPreviewGeometry_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UPreviewGeometry_Statics::PropPointers),
		0,
		0x009000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UPreviewGeometry_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPreviewGeometry_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPreviewGeometry()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPreviewGeometry_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPreviewGeometry, 2607322170);
	template<> MODELINGCOMPONENTS_API UClass* StaticClass<UPreviewGeometry>()
	{
		return UPreviewGeometry::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPreviewGeometry(Z_Construct_UClass_UPreviewGeometry, &UPreviewGeometry::StaticClass, TEXT("/Script/ModelingComponents"), TEXT("UPreviewGeometry"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPreviewGeometry);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
