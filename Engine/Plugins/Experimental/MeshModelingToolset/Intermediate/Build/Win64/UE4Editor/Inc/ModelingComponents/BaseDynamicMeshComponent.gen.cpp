// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ModelingComponents/Public/BaseDynamicMeshComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBaseDynamicMeshComponent() {}
// Cross Module References
	MODELINGCOMPONENTS_API UEnum* Z_Construct_UEnum_ModelingComponents_EDynamicMeshTangentCalcType();
	UPackage* Z_Construct_UPackage__Script_ModelingComponents();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UBaseDynamicMeshComponent_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UBaseDynamicMeshComponent();
	ENGINE_API UClass* Z_Construct_UClass_UMeshComponent();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UToolFrameworkComponent_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UMeshVertexCommandChangeTarget_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UMeshCommandChangeTarget_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UMeshReplacementCommandChangeTarget_NoRegister();
// End Cross Module References
	static UEnum* EDynamicMeshTangentCalcType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ModelingComponents_EDynamicMeshTangentCalcType, Z_Construct_UPackage__Script_ModelingComponents(), TEXT("EDynamicMeshTangentCalcType"));
		}
		return Singleton;
	}
	template<> MODELINGCOMPONENTS_API UEnum* StaticEnum<EDynamicMeshTangentCalcType>()
	{
		return EDynamicMeshTangentCalcType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDynamicMeshTangentCalcType(EDynamicMeshTangentCalcType_StaticEnum, TEXT("/Script/ModelingComponents"), TEXT("EDynamicMeshTangentCalcType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ModelingComponents_EDynamicMeshTangentCalcType_Hash() { return 3487103649U; }
	UEnum* Z_Construct_UEnum_ModelingComponents_EDynamicMeshTangentCalcType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ModelingComponents();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDynamicMeshTangentCalcType"), 0, Get_Z_Construct_UEnum_ModelingComponents_EDynamicMeshTangentCalcType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDynamicMeshTangentCalcType::NoTangents", (int64)EDynamicMeshTangentCalcType::NoTangents },
				{ "EDynamicMeshTangentCalcType::AutoCalculated", (int64)EDynamicMeshTangentCalcType::AutoCalculated },
				{ "EDynamicMeshTangentCalcType::ExternallyCalculated", (int64)EDynamicMeshTangentCalcType::ExternallyCalculated },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "AutoCalculated.Comment", "/** Tangents should be automatically calculated on demand */" },
				{ "AutoCalculated.Name", "EDynamicMeshTangentCalcType::AutoCalculated" },
				{ "AutoCalculated.ToolTip", "Tangents should be automatically calculated on demand" },
				{ "Comment", "/**\n * Tangent calculation modes\n */" },
				{ "ExternallyCalculated.Comment", "/** Tangents are externally calculated (behavior undefined if they are not actually externally calculated!) */" },
				{ "ExternallyCalculated.Name", "EDynamicMeshTangentCalcType::ExternallyCalculated" },
				{ "ExternallyCalculated.ToolTip", "Tangents are externally calculated (behavior undefined if they are not actually externally calculated!)" },
				{ "ModuleRelativePath", "Public/BaseDynamicMeshComponent.h" },
				{ "NoTangents.Comment", "/** Tangents are not used/available, proceed accordingly (eg generate arbitrary orthogonal basis) */" },
				{ "NoTangents.Name", "EDynamicMeshTangentCalcType::NoTangents" },
				{ "NoTangents.ToolTip", "Tangents are not used/available, proceed accordingly (eg generate arbitrary orthogonal basis)" },
				{ "ToolTip", "Tangent calculation modes" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ModelingComponents,
				nullptr,
				"EDynamicMeshTangentCalcType",
				"EDynamicMeshTangentCalcType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UBaseDynamicMeshComponent::StaticRegisterNativesUBaseDynamicMeshComponent()
	{
	}
	UClass* Z_Construct_UClass_UBaseDynamicMeshComponent_NoRegister()
	{
		return UBaseDynamicMeshComponent::StaticClass();
	}
	struct Z_Construct_UClass_UBaseDynamicMeshComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBaseDynamicMeshComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_ModelingComponents,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBaseDynamicMeshComponent_Statics::Class_MetaDataParams[] = {
		{ "ClassGroupNames", "Rendering" },
		{ "Comment", "/**\n * UBaseDynamicMeshComponent is a base interface for a UMeshComponent based on a FDynamicMesh.\n * Currently no functionality lives here, only some interface functions are defined that various subclasses implement.\n */" },
		{ "HideCategories", "LOD Physics Collision Mobility Trigger" },
		{ "IncludePath", "BaseDynamicMeshComponent.h" },
		{ "ModuleRelativePath", "Public/BaseDynamicMeshComponent.h" },
		{ "ToolTip", "UBaseDynamicMeshComponent is a base interface for a UMeshComponent based on a FDynamicMesh.\nCurrently no functionality lives here, only some interface functions are defined that various subclasses implement." },
	};
#endif
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_UBaseDynamicMeshComponent_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UToolFrameworkComponent_NoRegister, (int32)VTABLE_OFFSET(UBaseDynamicMeshComponent, IToolFrameworkComponent), false },
			{ Z_Construct_UClass_UMeshVertexCommandChangeTarget_NoRegister, (int32)VTABLE_OFFSET(UBaseDynamicMeshComponent, IMeshVertexCommandChangeTarget), false },
			{ Z_Construct_UClass_UMeshCommandChangeTarget_NoRegister, (int32)VTABLE_OFFSET(UBaseDynamicMeshComponent, IMeshCommandChangeTarget), false },
			{ Z_Construct_UClass_UMeshReplacementCommandChangeTarget_NoRegister, (int32)VTABLE_OFFSET(UBaseDynamicMeshComponent, IMeshReplacementCommandChangeTarget), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBaseDynamicMeshComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBaseDynamicMeshComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBaseDynamicMeshComponent_Statics::ClassParams = {
		&UBaseDynamicMeshComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		UE_ARRAY_COUNT(InterfaceParams),
		0x00B010A4u,
		METADATA_PARAMS(Z_Construct_UClass_UBaseDynamicMeshComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBaseDynamicMeshComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBaseDynamicMeshComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBaseDynamicMeshComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBaseDynamicMeshComponent, 253068755);
	template<> MODELINGCOMPONENTS_API UClass* StaticClass<UBaseDynamicMeshComponent>()
	{
		return UBaseDynamicMeshComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBaseDynamicMeshComponent(Z_Construct_UClass_UBaseDynamicMeshComponent, &UBaseDynamicMeshComponent::StaticClass, TEXT("/Script/ModelingComponents"), TEXT("UBaseDynamicMeshComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBaseDynamicMeshComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
