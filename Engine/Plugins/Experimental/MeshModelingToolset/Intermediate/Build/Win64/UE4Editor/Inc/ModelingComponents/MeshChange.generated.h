// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MODELINGCOMPONENTS_MeshChange_generated_h
#error "MeshChange.generated.h already included, missing '#pragma once' in MeshChange.h"
#endif
#define MODELINGCOMPONENTS_MeshChange_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshChange_h_49_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshChange_h_49_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshChange_h_49_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshChange_h_49_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshCommandChangeTarget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshCommandChangeTarget) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshCommandChangeTarget); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshCommandChangeTarget); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshCommandChangeTarget(UMeshCommandChangeTarget&&); \
	NO_API UMeshCommandChangeTarget(const UMeshCommandChangeTarget&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshChange_h_49_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshCommandChangeTarget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshCommandChangeTarget(UMeshCommandChangeTarget&&); \
	NO_API UMeshCommandChangeTarget(const UMeshCommandChangeTarget&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshCommandChangeTarget); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshCommandChangeTarget); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshCommandChangeTarget)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshChange_h_49_GENERATED_UINTERFACE_BODY() \
private: \
	static void StaticRegisterNativesUMeshCommandChangeTarget(); \
	friend struct Z_Construct_UClass_UMeshCommandChangeTarget_Statics; \
public: \
	DECLARE_CLASS(UMeshCommandChangeTarget, UInterface, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UMeshCommandChangeTarget)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshChange_h_49_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshChange_h_49_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshChange_h_49_STANDARD_CONSTRUCTORS \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshChange_h_49_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshChange_h_49_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshChange_h_49_ENHANCED_CONSTRUCTORS \
private: \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshChange_h_49_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~IMeshCommandChangeTarget() {} \
public: \
	typedef UMeshCommandChangeTarget UClassType; \
	typedef IMeshCommandChangeTarget ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshChange_h_49_INCLASS_IINTERFACE \
protected: \
	virtual ~IMeshCommandChangeTarget() {} \
public: \
	typedef UMeshCommandChangeTarget UClassType; \
	typedef IMeshCommandChangeTarget ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshChange_h_46_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshChange_h_56_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshChange_h_49_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshChange_h_49_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshChange_h_49_INCLASS_IINTERFACE \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshChange_h_56_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshChange_h_49_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshChange_h_49_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshChange_h_49_INCLASS_IINTERFACE_NO_PURE_DECLS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MODELINGCOMPONENTS_API UClass* StaticClass<class UMeshCommandChangeTarget>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshChange_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
