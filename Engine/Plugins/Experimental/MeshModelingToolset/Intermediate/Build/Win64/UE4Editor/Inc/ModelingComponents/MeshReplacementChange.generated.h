// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MODELINGCOMPONENTS_MeshReplacementChange_generated_h
#error "MeshReplacementChange.generated.h already included, missing '#pragma once' in MeshReplacementChange.h"
#endif
#define MODELINGCOMPONENTS_MeshReplacementChange_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshReplacementChange_h_53_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshReplacementChange_h_53_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshReplacementChange_h_53_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshReplacementChange_h_53_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshReplacementCommandChangeTarget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshReplacementCommandChangeTarget) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshReplacementCommandChangeTarget); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshReplacementCommandChangeTarget); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshReplacementCommandChangeTarget(UMeshReplacementCommandChangeTarget&&); \
	NO_API UMeshReplacementCommandChangeTarget(const UMeshReplacementCommandChangeTarget&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshReplacementChange_h_53_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshReplacementCommandChangeTarget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshReplacementCommandChangeTarget(UMeshReplacementCommandChangeTarget&&); \
	NO_API UMeshReplacementCommandChangeTarget(const UMeshReplacementCommandChangeTarget&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshReplacementCommandChangeTarget); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshReplacementCommandChangeTarget); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshReplacementCommandChangeTarget)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshReplacementChange_h_53_GENERATED_UINTERFACE_BODY() \
private: \
	static void StaticRegisterNativesUMeshReplacementCommandChangeTarget(); \
	friend struct Z_Construct_UClass_UMeshReplacementCommandChangeTarget_Statics; \
public: \
	DECLARE_CLASS(UMeshReplacementCommandChangeTarget, UInterface, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UMeshReplacementCommandChangeTarget)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshReplacementChange_h_53_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshReplacementChange_h_53_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshReplacementChange_h_53_STANDARD_CONSTRUCTORS \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshReplacementChange_h_53_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshReplacementChange_h_53_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshReplacementChange_h_53_ENHANCED_CONSTRUCTORS \
private: \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshReplacementChange_h_53_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~IMeshReplacementCommandChangeTarget() {} \
public: \
	typedef UMeshReplacementCommandChangeTarget UClassType; \
	typedef IMeshReplacementCommandChangeTarget ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshReplacementChange_h_53_INCLASS_IINTERFACE \
protected: \
	virtual ~IMeshReplacementCommandChangeTarget() {} \
public: \
	typedef UMeshReplacementCommandChangeTarget UClassType; \
	typedef IMeshReplacementCommandChangeTarget ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshReplacementChange_h_50_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshReplacementChange_h_60_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshReplacementChange_h_53_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshReplacementChange_h_53_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshReplacementChange_h_53_INCLASS_IINTERFACE \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshReplacementChange_h_60_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshReplacementChange_h_53_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshReplacementChange_h_53_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshReplacementChange_h_53_INCLASS_IINTERFACE_NO_PURE_DECLS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MODELINGCOMPONENTS_API UClass* StaticClass<class UMeshReplacementCommandChangeTarget>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshReplacementChange_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
