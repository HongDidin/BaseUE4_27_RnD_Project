// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLS_RemeshProperties_generated_h
#error "RemeshProperties.generated.h already included, missing '#pragma once' in RemeshProperties.h"
#endif
#define MESHMODELINGTOOLS_RemeshProperties_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_42_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_42_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_42_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_42_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshConstraintProperties(); \
	friend struct Z_Construct_UClass_UMeshConstraintProperties_Statics; \
public: \
	DECLARE_CLASS(UMeshConstraintProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMeshConstraintProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_42_INCLASS \
private: \
	static void StaticRegisterNativesUMeshConstraintProperties(); \
	friend struct Z_Construct_UClass_UMeshConstraintProperties_Statics; \
public: \
	DECLARE_CLASS(UMeshConstraintProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMeshConstraintProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_42_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshConstraintProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshConstraintProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshConstraintProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshConstraintProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshConstraintProperties(UMeshConstraintProperties&&); \
	NO_API UMeshConstraintProperties(const UMeshConstraintProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_42_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshConstraintProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshConstraintProperties(UMeshConstraintProperties&&); \
	NO_API UMeshConstraintProperties(const UMeshConstraintProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshConstraintProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshConstraintProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshConstraintProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_42_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_39_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_42_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_42_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_42_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_42_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_42_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_42_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_42_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_42_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_42_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_42_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_42_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_42_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UMeshConstraintProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_69_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_69_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_69_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_69_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURemeshProperties(); \
	friend struct Z_Construct_UClass_URemeshProperties_Statics; \
public: \
	DECLARE_CLASS(URemeshProperties, UMeshConstraintProperties, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(URemeshProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_69_INCLASS \
private: \
	static void StaticRegisterNativesURemeshProperties(); \
	friend struct Z_Construct_UClass_URemeshProperties_Statics; \
public: \
	DECLARE_CLASS(URemeshProperties, UMeshConstraintProperties, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(URemeshProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_69_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URemeshProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URemeshProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URemeshProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URemeshProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URemeshProperties(URemeshProperties&&); \
	NO_API URemeshProperties(const URemeshProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_69_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URemeshProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URemeshProperties(URemeshProperties&&); \
	NO_API URemeshProperties(const URemeshProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URemeshProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URemeshProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URemeshProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_69_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_66_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_69_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_69_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_69_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_69_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_69_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_69_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_69_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_69_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_69_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_69_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_69_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h_69_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class URemeshProperties>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_RemeshProperties_h


#define FOREACH_ENUM_EMATERIALBOUNDARYCONSTRAINT(op) \
	op(EMaterialBoundaryConstraint::Fixed) \
	op(EMaterialBoundaryConstraint::Refine) \
	op(EMaterialBoundaryConstraint::Free) \
	op(EMaterialBoundaryConstraint::Ignore) 

enum class EMaterialBoundaryConstraint : uint8;
template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EMaterialBoundaryConstraint>();

#define FOREACH_ENUM_EGROUPBOUNDARYCONSTRAINT(op) \
	op(EGroupBoundaryConstraint::Fixed) \
	op(EGroupBoundaryConstraint::Refine) \
	op(EGroupBoundaryConstraint::Free) \
	op(EGroupBoundaryConstraint::Ignore) 

enum class EGroupBoundaryConstraint : uint8;
template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EGroupBoundaryConstraint>();

#define FOREACH_ENUM_EMESHBOUNDARYCONSTRAINT(op) \
	op(EMeshBoundaryConstraint::Fixed) \
	op(EMeshBoundaryConstraint::Refine) \
	op(EMeshBoundaryConstraint::Free) 

enum class EMeshBoundaryConstraint : uint8;
template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EMeshBoundaryConstraint>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
