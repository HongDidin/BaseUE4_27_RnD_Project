// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UMaterialInterface;
class ULineSetComponent;
class APreviewGeometryActor;
class UWorld;
struct FTransform;
#ifdef MODELINGCOMPONENTS_PreviewGeometryActor_generated_h
#error "PreviewGeometryActor.generated.h already included, missing '#pragma once' in PreviewGeometryActor.h"
#endif
#define MODELINGCOMPONENTS_PreviewGeometryActor_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_17_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_17_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPreviewGeometryActor(); \
	friend struct Z_Construct_UClass_APreviewGeometryActor_Statics; \
public: \
	DECLARE_CLASS(APreviewGeometryActor, AInternalToolFrameworkActor, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(APreviewGeometryActor)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_17_INCLASS \
private: \
	static void StaticRegisterNativesAPreviewGeometryActor(); \
	friend struct Z_Construct_UClass_APreviewGeometryActor_Statics; \
public: \
	DECLARE_CLASS(APreviewGeometryActor, AInternalToolFrameworkActor, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(APreviewGeometryActor)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APreviewGeometryActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APreviewGeometryActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APreviewGeometryActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APreviewGeometryActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APreviewGeometryActor(APreviewGeometryActor&&); \
	NO_API APreviewGeometryActor(const APreviewGeometryActor&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APreviewGeometryActor(APreviewGeometryActor&&); \
	NO_API APreviewGeometryActor(const APreviewGeometryActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APreviewGeometryActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APreviewGeometryActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APreviewGeometryActor)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_14_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_17_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_17_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MODELINGCOMPONENTS_API UClass* StaticClass<class APreviewGeometryActor>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_38_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_38_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetAllLineSetsMaterial); \
	DECLARE_FUNCTION(execSetLineSetMaterial); \
	DECLARE_FUNCTION(execSetLineSetVisibility); \
	DECLARE_FUNCTION(execRemoveAllLineSets); \
	DECLARE_FUNCTION(execRemoveLineSet); \
	DECLARE_FUNCTION(execFindLineSet); \
	DECLARE_FUNCTION(execAddLineSet); \
	DECLARE_FUNCTION(execGetActor); \
	DECLARE_FUNCTION(execDisconnect); \
	DECLARE_FUNCTION(execCreateInWorld);


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_38_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetAllLineSetsMaterial); \
	DECLARE_FUNCTION(execSetLineSetMaterial); \
	DECLARE_FUNCTION(execSetLineSetVisibility); \
	DECLARE_FUNCTION(execRemoveAllLineSets); \
	DECLARE_FUNCTION(execRemoveLineSet); \
	DECLARE_FUNCTION(execFindLineSet); \
	DECLARE_FUNCTION(execAddLineSet); \
	DECLARE_FUNCTION(execGetActor); \
	DECLARE_FUNCTION(execDisconnect); \
	DECLARE_FUNCTION(execCreateInWorld);


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_38_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPreviewGeometry(); \
	friend struct Z_Construct_UClass_UPreviewGeometry_Statics; \
public: \
	DECLARE_CLASS(UPreviewGeometry, UObject, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UPreviewGeometry)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_38_INCLASS \
private: \
	static void StaticRegisterNativesUPreviewGeometry(); \
	friend struct Z_Construct_UClass_UPreviewGeometry_Statics; \
public: \
	DECLARE_CLASS(UPreviewGeometry, UObject, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UPreviewGeometry)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_38_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPreviewGeometry(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPreviewGeometry) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPreviewGeometry); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPreviewGeometry); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPreviewGeometry(UPreviewGeometry&&); \
	NO_API UPreviewGeometry(const UPreviewGeometry&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_38_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPreviewGeometry(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPreviewGeometry(UPreviewGeometry&&); \
	NO_API UPreviewGeometry(const UPreviewGeometry&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPreviewGeometry); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPreviewGeometry); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPreviewGeometry)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_38_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_35_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_38_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_38_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_38_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_38_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_38_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_38_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_38_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_38_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_38_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_38_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_38_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h_38_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MODELINGCOMPONENTS_API UClass* StaticClass<class UPreviewGeometry>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_PreviewGeometryActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
