// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/EditMeshPolygonsTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEditMeshPolygonsTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EPolyEditCutPlaneOrientation();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EPolyEditExtrudeDirection();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EEditMeshPolygonsToolActions();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_ELocalFrameMode();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEditMeshPolygonsToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEditMeshPolygonsToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UMeshSurfacePointToolBuilder();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPolyEditCommonProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPolyEditCommonProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEditMeshPolygonsToolActionPropertySet_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEditMeshPolygonsToolActionPropertySet();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEditMeshPolygonsToolActions_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEditMeshPolygonsToolActions();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEditMeshPolygonsToolActions_Triangles_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEditMeshPolygonsToolActions_Triangles();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEditMeshPolygonsToolUVActions_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEditMeshPolygonsToolUVActions();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEditMeshPolygonsToolEdgeActions_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEditMeshPolygonsToolEdgeActions();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEditMeshPolygonsToolEdgeActions_Triangles_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEditMeshPolygonsToolEdgeActions_Triangles();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPolyEditExtrudeProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPolyEditExtrudeProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPolyEditOffsetProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPolyEditOffsetProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPolyEditInsetProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPolyEditInsetProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPolyEditOutsetProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPolyEditOutsetProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPolyEditCutProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPolyEditCutProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPolyEditSetUVProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPolyEditSetUVProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEditMeshPolygonsTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEditMeshPolygonsTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UMeshSurfacePointTool();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_USimpleDynamicMeshComponent_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPolygonSelectionMechanic_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UMultiTransformer_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPolyEditPreviewMesh_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPlaneDistanceFromHitMechanic_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_USpatialCurveDistanceMechanic_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UCollectSurfacePathMechanic_NoRegister();
// End Cross Module References
	static UEnum* EPolyEditCutPlaneOrientation_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EPolyEditCutPlaneOrientation, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EPolyEditCutPlaneOrientation"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EPolyEditCutPlaneOrientation>()
	{
		return EPolyEditCutPlaneOrientation_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EPolyEditCutPlaneOrientation(EPolyEditCutPlaneOrientation_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EPolyEditCutPlaneOrientation"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EPolyEditCutPlaneOrientation_Hash() { return 3179505778U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EPolyEditCutPlaneOrientation()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EPolyEditCutPlaneOrientation"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EPolyEditCutPlaneOrientation_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EPolyEditCutPlaneOrientation::FaceNormals", (int64)EPolyEditCutPlaneOrientation::FaceNormals },
				{ "EPolyEditCutPlaneOrientation::ViewDirection", (int64)EPolyEditCutPlaneOrientation::ViewDirection },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "FaceNormals.Name", "EPolyEditCutPlaneOrientation::FaceNormals" },
				{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
				{ "ViewDirection.Name", "EPolyEditCutPlaneOrientation::ViewDirection" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EPolyEditCutPlaneOrientation",
				"EPolyEditCutPlaneOrientation",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EPolyEditExtrudeDirection_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EPolyEditExtrudeDirection, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EPolyEditExtrudeDirection"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EPolyEditExtrudeDirection>()
	{
		return EPolyEditExtrudeDirection_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EPolyEditExtrudeDirection(EPolyEditExtrudeDirection_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EPolyEditExtrudeDirection"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EPolyEditExtrudeDirection_Hash() { return 4155319899U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EPolyEditExtrudeDirection()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EPolyEditExtrudeDirection"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EPolyEditExtrudeDirection_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EPolyEditExtrudeDirection::SelectionNormal", (int64)EPolyEditExtrudeDirection::SelectionNormal },
				{ "EPolyEditExtrudeDirection::WorldX", (int64)EPolyEditExtrudeDirection::WorldX },
				{ "EPolyEditExtrudeDirection::WorldY", (int64)EPolyEditExtrudeDirection::WorldY },
				{ "EPolyEditExtrudeDirection::WorldZ", (int64)EPolyEditExtrudeDirection::WorldZ },
				{ "EPolyEditExtrudeDirection::LocalX", (int64)EPolyEditExtrudeDirection::LocalX },
				{ "EPolyEditExtrudeDirection::LocalY", (int64)EPolyEditExtrudeDirection::LocalY },
				{ "EPolyEditExtrudeDirection::LocalZ", (int64)EPolyEditExtrudeDirection::LocalZ },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "LocalX.Name", "EPolyEditExtrudeDirection::LocalX" },
				{ "LocalY.Name", "EPolyEditExtrudeDirection::LocalY" },
				{ "LocalZ.Name", "EPolyEditExtrudeDirection::LocalZ" },
				{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
				{ "SelectionNormal.Name", "EPolyEditExtrudeDirection::SelectionNormal" },
				{ "WorldX.Name", "EPolyEditExtrudeDirection::WorldX" },
				{ "WorldY.Name", "EPolyEditExtrudeDirection::WorldY" },
				{ "WorldZ.Name", "EPolyEditExtrudeDirection::WorldZ" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EPolyEditExtrudeDirection",
				"EPolyEditExtrudeDirection",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EEditMeshPolygonsToolActions_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EEditMeshPolygonsToolActions, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EEditMeshPolygonsToolActions"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EEditMeshPolygonsToolActions>()
	{
		return EEditMeshPolygonsToolActions_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EEditMeshPolygonsToolActions(EEditMeshPolygonsToolActions_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EEditMeshPolygonsToolActions"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EEditMeshPolygonsToolActions_Hash() { return 2886221176U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EEditMeshPolygonsToolActions()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EEditMeshPolygonsToolActions"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EEditMeshPolygonsToolActions_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EEditMeshPolygonsToolActions::NoAction", (int64)EEditMeshPolygonsToolActions::NoAction },
				{ "EEditMeshPolygonsToolActions::PlaneCut", (int64)EEditMeshPolygonsToolActions::PlaneCut },
				{ "EEditMeshPolygonsToolActions::Extrude", (int64)EEditMeshPolygonsToolActions::Extrude },
				{ "EEditMeshPolygonsToolActions::Offset", (int64)EEditMeshPolygonsToolActions::Offset },
				{ "EEditMeshPolygonsToolActions::Inset", (int64)EEditMeshPolygonsToolActions::Inset },
				{ "EEditMeshPolygonsToolActions::Outset", (int64)EEditMeshPolygonsToolActions::Outset },
				{ "EEditMeshPolygonsToolActions::Merge", (int64)EEditMeshPolygonsToolActions::Merge },
				{ "EEditMeshPolygonsToolActions::Delete", (int64)EEditMeshPolygonsToolActions::Delete },
				{ "EEditMeshPolygonsToolActions::CutFaces", (int64)EEditMeshPolygonsToolActions::CutFaces },
				{ "EEditMeshPolygonsToolActions::RecalculateNormals", (int64)EEditMeshPolygonsToolActions::RecalculateNormals },
				{ "EEditMeshPolygonsToolActions::FlipNormals", (int64)EEditMeshPolygonsToolActions::FlipNormals },
				{ "EEditMeshPolygonsToolActions::Retriangulate", (int64)EEditMeshPolygonsToolActions::Retriangulate },
				{ "EEditMeshPolygonsToolActions::Decompose", (int64)EEditMeshPolygonsToolActions::Decompose },
				{ "EEditMeshPolygonsToolActions::Disconnect", (int64)EEditMeshPolygonsToolActions::Disconnect },
				{ "EEditMeshPolygonsToolActions::CollapseEdge", (int64)EEditMeshPolygonsToolActions::CollapseEdge },
				{ "EEditMeshPolygonsToolActions::WeldEdges", (int64)EEditMeshPolygonsToolActions::WeldEdges },
				{ "EEditMeshPolygonsToolActions::StraightenEdge", (int64)EEditMeshPolygonsToolActions::StraightenEdge },
				{ "EEditMeshPolygonsToolActions::FillHole", (int64)EEditMeshPolygonsToolActions::FillHole },
				{ "EEditMeshPolygonsToolActions::PlanarProjectionUV", (int64)EEditMeshPolygonsToolActions::PlanarProjectionUV },
				{ "EEditMeshPolygonsToolActions::PokeSingleFace", (int64)EEditMeshPolygonsToolActions::PokeSingleFace },
				{ "EEditMeshPolygonsToolActions::SplitSingleEdge", (int64)EEditMeshPolygonsToolActions::SplitSingleEdge },
				{ "EEditMeshPolygonsToolActions::FlipSingleEdge", (int64)EEditMeshPolygonsToolActions::FlipSingleEdge },
				{ "EEditMeshPolygonsToolActions::CollapseSingleEdge", (int64)EEditMeshPolygonsToolActions::CollapseSingleEdge },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "CollapseEdge.Name", "EEditMeshPolygonsToolActions::CollapseEdge" },
				{ "CollapseSingleEdge.Name", "EEditMeshPolygonsToolActions::CollapseSingleEdge" },
				{ "CutFaces.Name", "EEditMeshPolygonsToolActions::CutFaces" },
				{ "Decompose.Name", "EEditMeshPolygonsToolActions::Decompose" },
				{ "Delete.Name", "EEditMeshPolygonsToolActions::Delete" },
				{ "Disconnect.Name", "EEditMeshPolygonsToolActions::Disconnect" },
				{ "Extrude.Name", "EEditMeshPolygonsToolActions::Extrude" },
				{ "FillHole.Name", "EEditMeshPolygonsToolActions::FillHole" },
				{ "FlipNormals.Name", "EEditMeshPolygonsToolActions::FlipNormals" },
				{ "FlipSingleEdge.Name", "EEditMeshPolygonsToolActions::FlipSingleEdge" },
				{ "Inset.Name", "EEditMeshPolygonsToolActions::Inset" },
				{ "Merge.Name", "EEditMeshPolygonsToolActions::Merge" },
				{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
				{ "NoAction.Name", "EEditMeshPolygonsToolActions::NoAction" },
				{ "Offset.Name", "EEditMeshPolygonsToolActions::Offset" },
				{ "Outset.Name", "EEditMeshPolygonsToolActions::Outset" },
				{ "PlanarProjectionUV.Name", "EEditMeshPolygonsToolActions::PlanarProjectionUV" },
				{ "PlaneCut.Name", "EEditMeshPolygonsToolActions::PlaneCut" },
				{ "PokeSingleFace.Comment", "// triangle-specific edits\n" },
				{ "PokeSingleFace.Name", "EEditMeshPolygonsToolActions::PokeSingleFace" },
				{ "PokeSingleFace.ToolTip", "triangle-specific edits" },
				{ "RecalculateNormals.Name", "EEditMeshPolygonsToolActions::RecalculateNormals" },
				{ "Retriangulate.Name", "EEditMeshPolygonsToolActions::Retriangulate" },
				{ "SplitSingleEdge.Name", "EEditMeshPolygonsToolActions::SplitSingleEdge" },
				{ "StraightenEdge.Name", "EEditMeshPolygonsToolActions::StraightenEdge" },
				{ "WeldEdges.Name", "EEditMeshPolygonsToolActions::WeldEdges" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EEditMeshPolygonsToolActions",
				"EEditMeshPolygonsToolActions",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ELocalFrameMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_ELocalFrameMode, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("ELocalFrameMode"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<ELocalFrameMode>()
	{
		return ELocalFrameMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ELocalFrameMode(ELocalFrameMode_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("ELocalFrameMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_ELocalFrameMode_Hash() { return 4089061944U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_ELocalFrameMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ELocalFrameMode"), 0, Get_Z_Construct_UEnum_MeshModelingTools_ELocalFrameMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ELocalFrameMode::FromObject", (int64)ELocalFrameMode::FromObject },
				{ "ELocalFrameMode::FromGeometry", (int64)ELocalFrameMode::FromGeometry },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "FromGeometry.Name", "ELocalFrameMode::FromGeometry" },
				{ "FromObject.Name", "ELocalFrameMode::FromObject" },
				{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"ELocalFrameMode",
				"ELocalFrameMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UEditMeshPolygonsToolBuilder::StaticRegisterNativesUEditMeshPolygonsToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UEditMeshPolygonsToolBuilder_NoRegister()
	{
		return UEditMeshPolygonsToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UEditMeshPolygonsToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEditMeshPolygonsToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshSurfacePointToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditMeshPolygonsToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * ToolBuilder\n */" },
		{ "IncludePath", "EditMeshPolygonsTool.h" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
		{ "ToolTip", "ToolBuilder" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEditMeshPolygonsToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEditMeshPolygonsToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEditMeshPolygonsToolBuilder_Statics::ClassParams = {
		&UEditMeshPolygonsToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UEditMeshPolygonsToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEditMeshPolygonsToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEditMeshPolygonsToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEditMeshPolygonsToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEditMeshPolygonsToolBuilder, 4132551892);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UEditMeshPolygonsToolBuilder>()
	{
		return UEditMeshPolygonsToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEditMeshPolygonsToolBuilder(Z_Construct_UClass_UEditMeshPolygonsToolBuilder, &UEditMeshPolygonsToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UEditMeshPolygonsToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEditMeshPolygonsToolBuilder);
	void UPolyEditCommonProperties::StaticRegisterNativesUPolyEditCommonProperties()
	{
	}
	UClass* Z_Construct_UClass_UPolyEditCommonProperties_NoRegister()
	{
		return UPolyEditCommonProperties::StaticClass();
	}
	struct Z_Construct_UClass_UPolyEditCommonProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowWireframe_MetaData[];
#endif
		static void NewProp_bShowWireframe_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowWireframe;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_LocalFrameMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LocalFrameMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_LocalFrameMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bLockRotation_MetaData[];
#endif
		static void NewProp_bLockRotation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bLockRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSnapToWorldGrid_MetaData[];
#endif
		static void NewProp_bSnapToWorldGrid_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSnapToWorldGrid;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPolyEditCommonProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolyEditCommonProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** \n * These are properties that do not get enabled/disabled based on the action \n */" },
		{ "IncludePath", "EditMeshPolygonsTool.h" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
		{ "ToolTip", "These are properties that do not get enabled/disabled based on the action" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolyEditCommonProperties_Statics::NewProp_bShowWireframe_MetaData[] = {
		{ "Category", "Options" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
	void Z_Construct_UClass_UPolyEditCommonProperties_Statics::NewProp_bShowWireframe_SetBit(void* Obj)
	{
		((UPolyEditCommonProperties*)Obj)->bShowWireframe = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPolyEditCommonProperties_Statics::NewProp_bShowWireframe = { "bShowWireframe", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPolyEditCommonProperties), &Z_Construct_UClass_UPolyEditCommonProperties_Statics::NewProp_bShowWireframe_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPolyEditCommonProperties_Statics::NewProp_bShowWireframe_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolyEditCommonProperties_Statics::NewProp_bShowWireframe_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UPolyEditCommonProperties_Statics::NewProp_LocalFrameMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolyEditCommonProperties_Statics::NewProp_LocalFrameMode_MetaData[] = {
		{ "Category", "Gizmo" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UPolyEditCommonProperties_Statics::NewProp_LocalFrameMode = { "LocalFrameMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPolyEditCommonProperties, LocalFrameMode), Z_Construct_UEnum_MeshModelingTools_ELocalFrameMode, METADATA_PARAMS(Z_Construct_UClass_UPolyEditCommonProperties_Statics::NewProp_LocalFrameMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolyEditCommonProperties_Statics::NewProp_LocalFrameMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolyEditCommonProperties_Statics::NewProp_bLockRotation_MetaData[] = {
		{ "Category", "Gizmo" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
	void Z_Construct_UClass_UPolyEditCommonProperties_Statics::NewProp_bLockRotation_SetBit(void* Obj)
	{
		((UPolyEditCommonProperties*)Obj)->bLockRotation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPolyEditCommonProperties_Statics::NewProp_bLockRotation = { "bLockRotation", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPolyEditCommonProperties), &Z_Construct_UClass_UPolyEditCommonProperties_Statics::NewProp_bLockRotation_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPolyEditCommonProperties_Statics::NewProp_bLockRotation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolyEditCommonProperties_Statics::NewProp_bLockRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolyEditCommonProperties_Statics::NewProp_bSnapToWorldGrid_MetaData[] = {
		{ "Category", "Gizmo" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
	void Z_Construct_UClass_UPolyEditCommonProperties_Statics::NewProp_bSnapToWorldGrid_SetBit(void* Obj)
	{
		((UPolyEditCommonProperties*)Obj)->bSnapToWorldGrid = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPolyEditCommonProperties_Statics::NewProp_bSnapToWorldGrid = { "bSnapToWorldGrid", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPolyEditCommonProperties), &Z_Construct_UClass_UPolyEditCommonProperties_Statics::NewProp_bSnapToWorldGrid_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPolyEditCommonProperties_Statics::NewProp_bSnapToWorldGrid_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolyEditCommonProperties_Statics::NewProp_bSnapToWorldGrid_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPolyEditCommonProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolyEditCommonProperties_Statics::NewProp_bShowWireframe,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolyEditCommonProperties_Statics::NewProp_LocalFrameMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolyEditCommonProperties_Statics::NewProp_LocalFrameMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolyEditCommonProperties_Statics::NewProp_bLockRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolyEditCommonProperties_Statics::NewProp_bSnapToWorldGrid,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPolyEditCommonProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPolyEditCommonProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPolyEditCommonProperties_Statics::ClassParams = {
		&UPolyEditCommonProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UPolyEditCommonProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UPolyEditCommonProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UPolyEditCommonProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPolyEditCommonProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPolyEditCommonProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPolyEditCommonProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPolyEditCommonProperties, 726623610);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UPolyEditCommonProperties>()
	{
		return UPolyEditCommonProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPolyEditCommonProperties(Z_Construct_UClass_UPolyEditCommonProperties, &UPolyEditCommonProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UPolyEditCommonProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPolyEditCommonProperties);
	void UEditMeshPolygonsToolActionPropertySet::StaticRegisterNativesUEditMeshPolygonsToolActionPropertySet()
	{
	}
	UClass* Z_Construct_UClass_UEditMeshPolygonsToolActionPropertySet_NoRegister()
	{
		return UEditMeshPolygonsToolActionPropertySet::StaticClass();
	}
	struct Z_Construct_UClass_UEditMeshPolygonsToolActionPropertySet_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEditMeshPolygonsToolActionPropertySet_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditMeshPolygonsToolActionPropertySet_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "EditMeshPolygonsTool.h" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEditMeshPolygonsToolActionPropertySet_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEditMeshPolygonsToolActionPropertySet>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEditMeshPolygonsToolActionPropertySet_Statics::ClassParams = {
		&UEditMeshPolygonsToolActionPropertySet::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UEditMeshPolygonsToolActionPropertySet_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEditMeshPolygonsToolActionPropertySet_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEditMeshPolygonsToolActionPropertySet()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEditMeshPolygonsToolActionPropertySet_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEditMeshPolygonsToolActionPropertySet, 1398322899);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UEditMeshPolygonsToolActionPropertySet>()
	{
		return UEditMeshPolygonsToolActionPropertySet::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEditMeshPolygonsToolActionPropertySet(Z_Construct_UClass_UEditMeshPolygonsToolActionPropertySet, &UEditMeshPolygonsToolActionPropertySet::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UEditMeshPolygonsToolActionPropertySet"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEditMeshPolygonsToolActionPropertySet);
	DEFINE_FUNCTION(UEditMeshPolygonsToolActions::execDisconnect)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Disconnect();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditMeshPolygonsToolActions::execDecompose)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Decompose();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditMeshPolygonsToolActions::execRetriangulate)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Retriangulate();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditMeshPolygonsToolActions::execFlip)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Flip();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditMeshPolygonsToolActions::execRecalcNormals)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->RecalcNormals();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditMeshPolygonsToolActions::execCutFaces)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->CutFaces();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditMeshPolygonsToolActions::execDelete)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Delete();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditMeshPolygonsToolActions::execMerge)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Merge();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditMeshPolygonsToolActions::execOutset)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Outset();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditMeshPolygonsToolActions::execInset)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Inset();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditMeshPolygonsToolActions::execOffset)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Offset();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditMeshPolygonsToolActions::execExtrude)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Extrude();
		P_NATIVE_END;
	}
	void UEditMeshPolygonsToolActions::StaticRegisterNativesUEditMeshPolygonsToolActions()
	{
		UClass* Class = UEditMeshPolygonsToolActions::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CutFaces", &UEditMeshPolygonsToolActions::execCutFaces },
			{ "Decompose", &UEditMeshPolygonsToolActions::execDecompose },
			{ "Delete", &UEditMeshPolygonsToolActions::execDelete },
			{ "Disconnect", &UEditMeshPolygonsToolActions::execDisconnect },
			{ "Extrude", &UEditMeshPolygonsToolActions::execExtrude },
			{ "Flip", &UEditMeshPolygonsToolActions::execFlip },
			{ "Inset", &UEditMeshPolygonsToolActions::execInset },
			{ "Merge", &UEditMeshPolygonsToolActions::execMerge },
			{ "Offset", &UEditMeshPolygonsToolActions::execOffset },
			{ "Outset", &UEditMeshPolygonsToolActions::execOutset },
			{ "RecalcNormals", &UEditMeshPolygonsToolActions::execRecalcNormals },
			{ "Retriangulate", &UEditMeshPolygonsToolActions::execRetriangulate },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UEditMeshPolygonsToolActions_CutFaces_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditMeshPolygonsToolActions_CutFaces_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "FaceEdits" },
		{ "Comment", "/** Cut the current set of selected faces. Click twice in viewport to set cut line. */" },
		{ "DisplayName", "CutFaces" },
		{ "DisplayPriority", "5" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
		{ "ToolTip", "Cut the current set of selected faces. Click twice in viewport to set cut line." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditMeshPolygonsToolActions_CutFaces_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditMeshPolygonsToolActions, nullptr, "CutFaces", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditMeshPolygonsToolActions_CutFaces_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditMeshPolygonsToolActions_CutFaces_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditMeshPolygonsToolActions_CutFaces()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditMeshPolygonsToolActions_CutFaces_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditMeshPolygonsToolActions_Decompose_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditMeshPolygonsToolActions_Decompose_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "FaceEdits" },
		{ "Comment", "/** Split each of the selected faces into a separate polygon for each triangle */" },
		{ "DisplayName", "Decompose" },
		{ "DisplayPriority", "10" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
		{ "ToolTip", "Split each of the selected faces into a separate polygon for each triangle" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditMeshPolygonsToolActions_Decompose_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditMeshPolygonsToolActions, nullptr, "Decompose", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditMeshPolygonsToolActions_Decompose_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditMeshPolygonsToolActions_Decompose_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditMeshPolygonsToolActions_Decompose()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditMeshPolygonsToolActions_Decompose_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditMeshPolygonsToolActions_Delete_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditMeshPolygonsToolActions_Delete_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "FaceEdits" },
		{ "Comment", "/** Delete the current set of selected faces */" },
		{ "DisplayName", "Delete" },
		{ "DisplayPriority", "4" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
		{ "ToolTip", "Delete the current set of selected faces" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditMeshPolygonsToolActions_Delete_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditMeshPolygonsToolActions, nullptr, "Delete", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditMeshPolygonsToolActions_Delete_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditMeshPolygonsToolActions_Delete_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditMeshPolygonsToolActions_Delete()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditMeshPolygonsToolActions_Delete_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditMeshPolygonsToolActions_Disconnect_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditMeshPolygonsToolActions_Disconnect_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "FaceEdits" },
		{ "Comment", "/** Separate the selected faces at their borders */" },
		{ "DisplayName", "Disconnect" },
		{ "DisplayPriority", "11" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
		{ "ToolTip", "Separate the selected faces at their borders" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditMeshPolygonsToolActions_Disconnect_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditMeshPolygonsToolActions, nullptr, "Disconnect", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditMeshPolygonsToolActions_Disconnect_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditMeshPolygonsToolActions_Disconnect_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditMeshPolygonsToolActions_Disconnect()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditMeshPolygonsToolActions_Disconnect_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditMeshPolygonsToolActions_Extrude_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditMeshPolygonsToolActions_Extrude_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "FaceEdits" },
		{ "Comment", "/** Extrude the current set of selected faces. Click in viewport to confirm extrude height. */" },
		{ "DisplayName", "Extrude" },
		{ "DisplayPriority", "1" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
		{ "ToolTip", "Extrude the current set of selected faces. Click in viewport to confirm extrude height." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditMeshPolygonsToolActions_Extrude_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditMeshPolygonsToolActions, nullptr, "Extrude", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditMeshPolygonsToolActions_Extrude_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditMeshPolygonsToolActions_Extrude_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditMeshPolygonsToolActions_Extrude()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditMeshPolygonsToolActions_Extrude_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditMeshPolygonsToolActions_Flip_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditMeshPolygonsToolActions_Flip_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "FaceEdits" },
		{ "Comment", "/** Flip normals and face orientation for the current set of selected faces */" },
		{ "DisplayName", "Flip" },
		{ "DisplayPriority", "7" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
		{ "ToolTip", "Flip normals and face orientation for the current set of selected faces" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditMeshPolygonsToolActions_Flip_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditMeshPolygonsToolActions, nullptr, "Flip", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditMeshPolygonsToolActions_Flip_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditMeshPolygonsToolActions_Flip_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditMeshPolygonsToolActions_Flip()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditMeshPolygonsToolActions_Flip_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditMeshPolygonsToolActions_Inset_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditMeshPolygonsToolActions_Inset_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "FaceEdits" },
		{ "Comment", "/** Inset the current set of selected faces. Click in viewport to confirm inset distance. */" },
		{ "DisplayName", "Inset" },
		{ "DisplayPriority", "3" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
		{ "ToolTip", "Inset the current set of selected faces. Click in viewport to confirm inset distance." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditMeshPolygonsToolActions_Inset_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditMeshPolygonsToolActions, nullptr, "Inset", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditMeshPolygonsToolActions_Inset_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditMeshPolygonsToolActions_Inset_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditMeshPolygonsToolActions_Inset()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditMeshPolygonsToolActions_Inset_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditMeshPolygonsToolActions_Merge_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditMeshPolygonsToolActions_Merge_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "FaceEdits" },
		{ "Comment", "/** Merge the current set of selected faces into a single face. */" },
		{ "DisplayName", "Merge" },
		{ "DisplayPriority", "4" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
		{ "ToolTip", "Merge the current set of selected faces into a single face." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditMeshPolygonsToolActions_Merge_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditMeshPolygonsToolActions, nullptr, "Merge", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditMeshPolygonsToolActions_Merge_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditMeshPolygonsToolActions_Merge_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditMeshPolygonsToolActions_Merge()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditMeshPolygonsToolActions_Merge_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditMeshPolygonsToolActions_Offset_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditMeshPolygonsToolActions_Offset_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "FaceEdits" },
		{ "Comment", "/** Offset the current set of selected faces. Click in viewport to confirm offset distance. */" },
		{ "DisplayName", "Offset" },
		{ "DisplayPriority", "2" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
		{ "ToolTip", "Offset the current set of selected faces. Click in viewport to confirm offset distance." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditMeshPolygonsToolActions_Offset_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditMeshPolygonsToolActions, nullptr, "Offset", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditMeshPolygonsToolActions_Offset_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditMeshPolygonsToolActions_Offset_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditMeshPolygonsToolActions_Offset()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditMeshPolygonsToolActions_Offset_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditMeshPolygonsToolActions_Outset_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditMeshPolygonsToolActions_Outset_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "FaceEdits" },
		{ "Comment", "/** Outset the current set of selected faces. Click in viewport to confirm outset distance. */" },
		{ "DisplayName", "Outset" },
		{ "DisplayPriority", "3" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
		{ "ToolTip", "Outset the current set of selected faces. Click in viewport to confirm outset distance." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditMeshPolygonsToolActions_Outset_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditMeshPolygonsToolActions, nullptr, "Outset", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditMeshPolygonsToolActions_Outset_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditMeshPolygonsToolActions_Outset_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditMeshPolygonsToolActions_Outset()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditMeshPolygonsToolActions_Outset_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditMeshPolygonsToolActions_RecalcNormals_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditMeshPolygonsToolActions_RecalcNormals_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "FaceEdits" },
		{ "Comment", "/** Recalculate normals for the current set of selected faces */" },
		{ "DisplayName", "RecalcNormals" },
		{ "DisplayPriority", "6" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
		{ "ToolTip", "Recalculate normals for the current set of selected faces" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditMeshPolygonsToolActions_RecalcNormals_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditMeshPolygonsToolActions, nullptr, "RecalcNormals", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditMeshPolygonsToolActions_RecalcNormals_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditMeshPolygonsToolActions_RecalcNormals_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditMeshPolygonsToolActions_RecalcNormals()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditMeshPolygonsToolActions_RecalcNormals_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditMeshPolygonsToolActions_Retriangulate_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditMeshPolygonsToolActions_Retriangulate_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "FaceEdits" },
		{ "Comment", "/** Retriangulate each of the selected faces */" },
		{ "DisplayName", "Retriangulate" },
		{ "DisplayPriority", "9" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
		{ "ToolTip", "Retriangulate each of the selected faces" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditMeshPolygonsToolActions_Retriangulate_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditMeshPolygonsToolActions, nullptr, "Retriangulate", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditMeshPolygonsToolActions_Retriangulate_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditMeshPolygonsToolActions_Retriangulate_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditMeshPolygonsToolActions_Retriangulate()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditMeshPolygonsToolActions_Retriangulate_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UEditMeshPolygonsToolActions_NoRegister()
	{
		return UEditMeshPolygonsToolActions::StaticClass();
	}
	struct Z_Construct_UClass_UEditMeshPolygonsToolActions_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEditMeshPolygonsToolActions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEditMeshPolygonsToolActionPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UEditMeshPolygonsToolActions_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UEditMeshPolygonsToolActions_CutFaces, "CutFaces" }, // 2551564483
		{ &Z_Construct_UFunction_UEditMeshPolygonsToolActions_Decompose, "Decompose" }, // 3286204027
		{ &Z_Construct_UFunction_UEditMeshPolygonsToolActions_Delete, "Delete" }, // 2011776004
		{ &Z_Construct_UFunction_UEditMeshPolygonsToolActions_Disconnect, "Disconnect" }, // 3167466991
		{ &Z_Construct_UFunction_UEditMeshPolygonsToolActions_Extrude, "Extrude" }, // 968545595
		{ &Z_Construct_UFunction_UEditMeshPolygonsToolActions_Flip, "Flip" }, // 3459127664
		{ &Z_Construct_UFunction_UEditMeshPolygonsToolActions_Inset, "Inset" }, // 686089303
		{ &Z_Construct_UFunction_UEditMeshPolygonsToolActions_Merge, "Merge" }, // 2695713789
		{ &Z_Construct_UFunction_UEditMeshPolygonsToolActions_Offset, "Offset" }, // 2608069450
		{ &Z_Construct_UFunction_UEditMeshPolygonsToolActions_Outset, "Outset" }, // 107883620
		{ &Z_Construct_UFunction_UEditMeshPolygonsToolActions_RecalcNormals, "RecalcNormals" }, // 2624008098
		{ &Z_Construct_UFunction_UEditMeshPolygonsToolActions_Retriangulate, "Retriangulate" }, // 4141493878
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditMeshPolygonsToolActions_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "EditMeshPolygonsTool.h" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEditMeshPolygonsToolActions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEditMeshPolygonsToolActions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEditMeshPolygonsToolActions_Statics::ClassParams = {
		&UEditMeshPolygonsToolActions::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UEditMeshPolygonsToolActions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEditMeshPolygonsToolActions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEditMeshPolygonsToolActions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEditMeshPolygonsToolActions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEditMeshPolygonsToolActions, 1620418243);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UEditMeshPolygonsToolActions>()
	{
		return UEditMeshPolygonsToolActions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEditMeshPolygonsToolActions(Z_Construct_UClass_UEditMeshPolygonsToolActions, &UEditMeshPolygonsToolActions::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UEditMeshPolygonsToolActions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEditMeshPolygonsToolActions);
	DEFINE_FUNCTION(UEditMeshPolygonsToolActions_Triangles::execPoke)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Poke();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditMeshPolygonsToolActions_Triangles::execDisconnect)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Disconnect();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditMeshPolygonsToolActions_Triangles::execFlip)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Flip();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditMeshPolygonsToolActions_Triangles::execRecalcNormals)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->RecalcNormals();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditMeshPolygonsToolActions_Triangles::execCutFaces)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->CutFaces();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditMeshPolygonsToolActions_Triangles::execDelete)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Delete();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditMeshPolygonsToolActions_Triangles::execOutset)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Outset();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditMeshPolygonsToolActions_Triangles::execInset)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Inset();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditMeshPolygonsToolActions_Triangles::execOffset)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Offset();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditMeshPolygonsToolActions_Triangles::execExtrude)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Extrude();
		P_NATIVE_END;
	}
	void UEditMeshPolygonsToolActions_Triangles::StaticRegisterNativesUEditMeshPolygonsToolActions_Triangles()
	{
		UClass* Class = UEditMeshPolygonsToolActions_Triangles::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CutFaces", &UEditMeshPolygonsToolActions_Triangles::execCutFaces },
			{ "Delete", &UEditMeshPolygonsToolActions_Triangles::execDelete },
			{ "Disconnect", &UEditMeshPolygonsToolActions_Triangles::execDisconnect },
			{ "Extrude", &UEditMeshPolygonsToolActions_Triangles::execExtrude },
			{ "Flip", &UEditMeshPolygonsToolActions_Triangles::execFlip },
			{ "Inset", &UEditMeshPolygonsToolActions_Triangles::execInset },
			{ "Offset", &UEditMeshPolygonsToolActions_Triangles::execOffset },
			{ "Outset", &UEditMeshPolygonsToolActions_Triangles::execOutset },
			{ "Poke", &UEditMeshPolygonsToolActions_Triangles::execPoke },
			{ "RecalcNormals", &UEditMeshPolygonsToolActions_Triangles::execRecalcNormals },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_CutFaces_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_CutFaces_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "TriangleEdits" },
		{ "Comment", "/** Cut the current set of selected faces. Click twice in viewport to set cut line. */" },
		{ "DisplayName", "CutFaces" },
		{ "DisplayPriority", "5" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
		{ "ToolTip", "Cut the current set of selected faces. Click twice in viewport to set cut line." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_CutFaces_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditMeshPolygonsToolActions_Triangles, nullptr, "CutFaces", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_CutFaces_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_CutFaces_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_CutFaces()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_CutFaces_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Delete_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Delete_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "TriangleEdits" },
		{ "Comment", "/** Delete the current set of selected faces */" },
		{ "DisplayName", "Delete" },
		{ "DisplayPriority", "4" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
		{ "ToolTip", "Delete the current set of selected faces" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Delete_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditMeshPolygonsToolActions_Triangles, nullptr, "Delete", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Delete_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Delete_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Delete()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Delete_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Disconnect_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Disconnect_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "TriangleEdits" },
		{ "Comment", "/** Separate the selected faces at their borders */" },
		{ "DisplayName", "Disconnect" },
		{ "DisplayPriority", "11" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
		{ "ToolTip", "Separate the selected faces at their borders" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Disconnect_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditMeshPolygonsToolActions_Triangles, nullptr, "Disconnect", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Disconnect_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Disconnect_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Disconnect()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Disconnect_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Extrude_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Extrude_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "TriangleEdits" },
		{ "Comment", "/** Extrude the current set of selected faces. Click in viewport to confirm extrude height. */" },
		{ "DisplayName", "Extrude" },
		{ "DisplayPriority", "1" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
		{ "ToolTip", "Extrude the current set of selected faces. Click in viewport to confirm extrude height." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Extrude_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditMeshPolygonsToolActions_Triangles, nullptr, "Extrude", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Extrude_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Extrude_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Extrude()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Extrude_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Flip_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Flip_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "TriangleEdits" },
		{ "Comment", "/** Flip normals and face orientation for the current set of selected faces */" },
		{ "DisplayName", "Flip" },
		{ "DisplayPriority", "7" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
		{ "ToolTip", "Flip normals and face orientation for the current set of selected faces" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Flip_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditMeshPolygonsToolActions_Triangles, nullptr, "Flip", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Flip_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Flip_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Flip()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Flip_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Inset_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Inset_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "TriangleEdits" },
		{ "Comment", "/** Inset the current set of selected faces. Click in viewport to confirm inset distance. */" },
		{ "DisplayName", "Inset" },
		{ "DisplayPriority", "3" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
		{ "ToolTip", "Inset the current set of selected faces. Click in viewport to confirm inset distance." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Inset_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditMeshPolygonsToolActions_Triangles, nullptr, "Inset", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Inset_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Inset_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Inset()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Inset_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Offset_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Offset_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "TriangleEdits" },
		{ "Comment", "/** Offset the current set of selected faces. Click in viewport to confirm offset distance. */" },
		{ "DisplayName", "Offset" },
		{ "DisplayPriority", "2" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
		{ "ToolTip", "Offset the current set of selected faces. Click in viewport to confirm offset distance." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Offset_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditMeshPolygonsToolActions_Triangles, nullptr, "Offset", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Offset_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Offset_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Offset()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Offset_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Outset_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Outset_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "TriangleEdits" },
		{ "Comment", "/** Outset the current set of selected faces. Click in viewport to confirm outset distance. */" },
		{ "DisplayName", "Outset" },
		{ "DisplayPriority", "3" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
		{ "ToolTip", "Outset the current set of selected faces. Click in viewport to confirm outset distance." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Outset_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditMeshPolygonsToolActions_Triangles, nullptr, "Outset", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Outset_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Outset_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Outset()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Outset_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Poke_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Poke_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "TriangleEdits" },
		{ "Comment", "/** Poke each face at its center point */" },
		{ "DisplayName", "Poke" },
		{ "DisplayPriority", "12" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
		{ "ToolTip", "Poke each face at its center point" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Poke_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditMeshPolygonsToolActions_Triangles, nullptr, "Poke", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Poke_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Poke_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Poke()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Poke_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_RecalcNormals_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_RecalcNormals_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "TriangleEdits" },
		{ "Comment", "/** Recalculate normals for the current set of selected faces */" },
		{ "DisplayName", "RecalcNormals" },
		{ "DisplayPriority", "6" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
		{ "ToolTip", "Recalculate normals for the current set of selected faces" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_RecalcNormals_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditMeshPolygonsToolActions_Triangles, nullptr, "RecalcNormals", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_RecalcNormals_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_RecalcNormals_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_RecalcNormals()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_RecalcNormals_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UEditMeshPolygonsToolActions_Triangles_NoRegister()
	{
		return UEditMeshPolygonsToolActions_Triangles::StaticClass();
	}
	struct Z_Construct_UClass_UEditMeshPolygonsToolActions_Triangles_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEditMeshPolygonsToolActions_Triangles_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEditMeshPolygonsToolActionPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UEditMeshPolygonsToolActions_Triangles_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_CutFaces, "CutFaces" }, // 290942418
		{ &Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Delete, "Delete" }, // 2444051477
		{ &Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Disconnect, "Disconnect" }, // 582536255
		{ &Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Extrude, "Extrude" }, // 1046818761
		{ &Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Flip, "Flip" }, // 4257053561
		{ &Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Inset, "Inset" }, // 3579473572
		{ &Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Offset, "Offset" }, // 2342437836
		{ &Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Outset, "Outset" }, // 232843083
		{ &Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_Poke, "Poke" }, // 1530192692
		{ &Z_Construct_UFunction_UEditMeshPolygonsToolActions_Triangles_RecalcNormals, "RecalcNormals" }, // 104183170
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditMeshPolygonsToolActions_Triangles_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "EditMeshPolygonsTool.h" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEditMeshPolygonsToolActions_Triangles_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEditMeshPolygonsToolActions_Triangles>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEditMeshPolygonsToolActions_Triangles_Statics::ClassParams = {
		&UEditMeshPolygonsToolActions_Triangles::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UEditMeshPolygonsToolActions_Triangles_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEditMeshPolygonsToolActions_Triangles_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEditMeshPolygonsToolActions_Triangles()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEditMeshPolygonsToolActions_Triangles_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEditMeshPolygonsToolActions_Triangles, 1271259974);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UEditMeshPolygonsToolActions_Triangles>()
	{
		return UEditMeshPolygonsToolActions_Triangles::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEditMeshPolygonsToolActions_Triangles(Z_Construct_UClass_UEditMeshPolygonsToolActions_Triangles, &UEditMeshPolygonsToolActions_Triangles::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UEditMeshPolygonsToolActions_Triangles"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEditMeshPolygonsToolActions_Triangles);
	DEFINE_FUNCTION(UEditMeshPolygonsToolUVActions::execPlanarProjection)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->PlanarProjection();
		P_NATIVE_END;
	}
	void UEditMeshPolygonsToolUVActions::StaticRegisterNativesUEditMeshPolygonsToolUVActions()
	{
		UClass* Class = UEditMeshPolygonsToolUVActions::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "PlanarProjection", &UEditMeshPolygonsToolUVActions::execPlanarProjection },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UEditMeshPolygonsToolUVActions_PlanarProjection_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditMeshPolygonsToolUVActions_PlanarProjection_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "UVs" },
		{ "Comment", "/** Assign planar-projection UVs to mesh */" },
		{ "DisplayName", "PlanarProjection" },
		{ "DisplayPriority", "11" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
		{ "ToolTip", "Assign planar-projection UVs to mesh" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditMeshPolygonsToolUVActions_PlanarProjection_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditMeshPolygonsToolUVActions, nullptr, "PlanarProjection", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditMeshPolygonsToolUVActions_PlanarProjection_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditMeshPolygonsToolUVActions_PlanarProjection_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditMeshPolygonsToolUVActions_PlanarProjection()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditMeshPolygonsToolUVActions_PlanarProjection_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UEditMeshPolygonsToolUVActions_NoRegister()
	{
		return UEditMeshPolygonsToolUVActions::StaticClass();
	}
	struct Z_Construct_UClass_UEditMeshPolygonsToolUVActions_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEditMeshPolygonsToolUVActions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEditMeshPolygonsToolActionPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UEditMeshPolygonsToolUVActions_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UEditMeshPolygonsToolUVActions_PlanarProjection, "PlanarProjection" }, // 562294342
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditMeshPolygonsToolUVActions_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "EditMeshPolygonsTool.h" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEditMeshPolygonsToolUVActions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEditMeshPolygonsToolUVActions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEditMeshPolygonsToolUVActions_Statics::ClassParams = {
		&UEditMeshPolygonsToolUVActions::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UEditMeshPolygonsToolUVActions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEditMeshPolygonsToolUVActions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEditMeshPolygonsToolUVActions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEditMeshPolygonsToolUVActions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEditMeshPolygonsToolUVActions, 1242129238);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UEditMeshPolygonsToolUVActions>()
	{
		return UEditMeshPolygonsToolUVActions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEditMeshPolygonsToolUVActions(Z_Construct_UClass_UEditMeshPolygonsToolUVActions, &UEditMeshPolygonsToolUVActions::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UEditMeshPolygonsToolUVActions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEditMeshPolygonsToolUVActions);
	DEFINE_FUNCTION(UEditMeshPolygonsToolEdgeActions::execFillHole)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->FillHole();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditMeshPolygonsToolEdgeActions::execStraighten)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Straighten();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditMeshPolygonsToolEdgeActions::execWeld)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Weld();
		P_NATIVE_END;
	}
	void UEditMeshPolygonsToolEdgeActions::StaticRegisterNativesUEditMeshPolygonsToolEdgeActions()
	{
		UClass* Class = UEditMeshPolygonsToolEdgeActions::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "FillHole", &UEditMeshPolygonsToolEdgeActions::execFillHole },
			{ "Straighten", &UEditMeshPolygonsToolEdgeActions::execStraighten },
			{ "Weld", &UEditMeshPolygonsToolEdgeActions::execWeld },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_FillHole_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_FillHole_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "EdgeEdits" },
		{ "Comment", "/** Fill the adjacent hole for any selected boundary edges */" },
		{ "DisplayName", "Fill Hole" },
		{ "DisplayPriority", "1" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
		{ "ToolTip", "Fill the adjacent hole for any selected boundary edges" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_FillHole_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditMeshPolygonsToolEdgeActions, nullptr, "FillHole", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_FillHole_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_FillHole_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_FillHole()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_FillHole_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Straighten_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Straighten_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "EdgeEdits" },
		{ "DisplayName", "Straighten" },
		{ "DisplayPriority", "1" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Straighten_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditMeshPolygonsToolEdgeActions, nullptr, "Straighten", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Straighten_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Straighten_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Straighten()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Straighten_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Weld_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Weld_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "EdgeEdits" },
		{ "DisplayName", "Weld" },
		{ "DisplayPriority", "1" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Weld_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditMeshPolygonsToolEdgeActions, nullptr, "Weld", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Weld_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Weld_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Weld()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Weld_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UEditMeshPolygonsToolEdgeActions_NoRegister()
	{
		return UEditMeshPolygonsToolEdgeActions::StaticClass();
	}
	struct Z_Construct_UClass_UEditMeshPolygonsToolEdgeActions_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEditMeshPolygonsToolEdgeActions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEditMeshPolygonsToolActionPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UEditMeshPolygonsToolEdgeActions_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_FillHole, "FillHole" }, // 757672908
		{ &Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Straighten, "Straighten" }, // 4151165227
		{ &Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Weld, "Weld" }, // 2394251226
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditMeshPolygonsToolEdgeActions_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "EditMeshPolygonsTool.h" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEditMeshPolygonsToolEdgeActions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEditMeshPolygonsToolEdgeActions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEditMeshPolygonsToolEdgeActions_Statics::ClassParams = {
		&UEditMeshPolygonsToolEdgeActions::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UEditMeshPolygonsToolEdgeActions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEditMeshPolygonsToolEdgeActions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEditMeshPolygonsToolEdgeActions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEditMeshPolygonsToolEdgeActions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEditMeshPolygonsToolEdgeActions, 3866753291);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UEditMeshPolygonsToolEdgeActions>()
	{
		return UEditMeshPolygonsToolEdgeActions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEditMeshPolygonsToolEdgeActions(Z_Construct_UClass_UEditMeshPolygonsToolEdgeActions, &UEditMeshPolygonsToolEdgeActions::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UEditMeshPolygonsToolEdgeActions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEditMeshPolygonsToolEdgeActions);
	DEFINE_FUNCTION(UEditMeshPolygonsToolEdgeActions_Triangles::execSplit)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Split();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditMeshPolygonsToolEdgeActions_Triangles::execFlip)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Flip();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditMeshPolygonsToolEdgeActions_Triangles::execCollapse)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Collapse();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditMeshPolygonsToolEdgeActions_Triangles::execFillHole)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->FillHole();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditMeshPolygonsToolEdgeActions_Triangles::execWeld)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Weld();
		P_NATIVE_END;
	}
	void UEditMeshPolygonsToolEdgeActions_Triangles::StaticRegisterNativesUEditMeshPolygonsToolEdgeActions_Triangles()
	{
		UClass* Class = UEditMeshPolygonsToolEdgeActions_Triangles::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Collapse", &UEditMeshPolygonsToolEdgeActions_Triangles::execCollapse },
			{ "FillHole", &UEditMeshPolygonsToolEdgeActions_Triangles::execFillHole },
			{ "Flip", &UEditMeshPolygonsToolEdgeActions_Triangles::execFlip },
			{ "Split", &UEditMeshPolygonsToolEdgeActions_Triangles::execSplit },
			{ "Weld", &UEditMeshPolygonsToolEdgeActions_Triangles::execWeld },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Triangles_Collapse_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Triangles_Collapse_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "EdgeEdits" },
		{ "DisplayName", "Collapse" },
		{ "DisplayPriority", "1" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Triangles_Collapse_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditMeshPolygonsToolEdgeActions_Triangles, nullptr, "Collapse", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Triangles_Collapse_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Triangles_Collapse_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Triangles_Collapse()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Triangles_Collapse_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Triangles_FillHole_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Triangles_FillHole_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "EdgeEdits" },
		{ "Comment", "/** Fill the adjacent hole for any selected boundary edges */" },
		{ "DisplayName", "Fill Hole" },
		{ "DisplayPriority", "1" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
		{ "ToolTip", "Fill the adjacent hole for any selected boundary edges" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Triangles_FillHole_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditMeshPolygonsToolEdgeActions_Triangles, nullptr, "FillHole", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Triangles_FillHole_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Triangles_FillHole_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Triangles_FillHole()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Triangles_FillHole_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Triangles_Flip_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Triangles_Flip_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "EdgeEdits" },
		{ "DisplayName", "Flip" },
		{ "DisplayPriority", "1" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Triangles_Flip_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditMeshPolygonsToolEdgeActions_Triangles, nullptr, "Flip", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Triangles_Flip_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Triangles_Flip_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Triangles_Flip()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Triangles_Flip_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Triangles_Split_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Triangles_Split_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "EdgeEdits" },
		{ "DisplayName", "Split" },
		{ "DisplayPriority", "1" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Triangles_Split_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditMeshPolygonsToolEdgeActions_Triangles, nullptr, "Split", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Triangles_Split_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Triangles_Split_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Triangles_Split()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Triangles_Split_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Triangles_Weld_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Triangles_Weld_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "EdgeEdits" },
		{ "DisplayName", "Weld" },
		{ "DisplayPriority", "1" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Triangles_Weld_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditMeshPolygonsToolEdgeActions_Triangles, nullptr, "Weld", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Triangles_Weld_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Triangles_Weld_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Triangles_Weld()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Triangles_Weld_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UEditMeshPolygonsToolEdgeActions_Triangles_NoRegister()
	{
		return UEditMeshPolygonsToolEdgeActions_Triangles::StaticClass();
	}
	struct Z_Construct_UClass_UEditMeshPolygonsToolEdgeActions_Triangles_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEditMeshPolygonsToolEdgeActions_Triangles_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEditMeshPolygonsToolActionPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UEditMeshPolygonsToolEdgeActions_Triangles_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Triangles_Collapse, "Collapse" }, // 2218258018
		{ &Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Triangles_FillHole, "FillHole" }, // 1521038906
		{ &Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Triangles_Flip, "Flip" }, // 604637027
		{ &Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Triangles_Split, "Split" }, // 2970087155
		{ &Z_Construct_UFunction_UEditMeshPolygonsToolEdgeActions_Triangles_Weld, "Weld" }, // 1163624051
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditMeshPolygonsToolEdgeActions_Triangles_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "EditMeshPolygonsTool.h" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEditMeshPolygonsToolEdgeActions_Triangles_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEditMeshPolygonsToolEdgeActions_Triangles>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEditMeshPolygonsToolEdgeActions_Triangles_Statics::ClassParams = {
		&UEditMeshPolygonsToolEdgeActions_Triangles::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UEditMeshPolygonsToolEdgeActions_Triangles_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEditMeshPolygonsToolEdgeActions_Triangles_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEditMeshPolygonsToolEdgeActions_Triangles()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEditMeshPolygonsToolEdgeActions_Triangles_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEditMeshPolygonsToolEdgeActions_Triangles, 311303239);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UEditMeshPolygonsToolEdgeActions_Triangles>()
	{
		return UEditMeshPolygonsToolEdgeActions_Triangles::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEditMeshPolygonsToolEdgeActions_Triangles(Z_Construct_UClass_UEditMeshPolygonsToolEdgeActions_Triangles, &UEditMeshPolygonsToolEdgeActions_Triangles::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UEditMeshPolygonsToolEdgeActions_Triangles"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEditMeshPolygonsToolEdgeActions_Triangles);
	void UPolyEditExtrudeProperties::StaticRegisterNativesUPolyEditExtrudeProperties()
	{
	}
	UClass* Z_Construct_UClass_UPolyEditExtrudeProperties_NoRegister()
	{
		return UPolyEditExtrudeProperties::StaticClass();
	}
	struct Z_Construct_UClass_UPolyEditExtrudeProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Direction_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Direction_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Direction;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShellsToSolids_MetaData[];
#endif
		static void NewProp_bShellsToSolids_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShellsToSolids;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPolyEditExtrudeProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolyEditExtrudeProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "EditMeshPolygonsTool.h" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UPolyEditExtrudeProperties_Statics::NewProp_Direction_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolyEditExtrudeProperties_Statics::NewProp_Direction_MetaData[] = {
		{ "Category", "Extrude" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UPolyEditExtrudeProperties_Statics::NewProp_Direction = { "Direction", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPolyEditExtrudeProperties, Direction), Z_Construct_UEnum_MeshModelingTools_EPolyEditExtrudeDirection, METADATA_PARAMS(Z_Construct_UClass_UPolyEditExtrudeProperties_Statics::NewProp_Direction_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolyEditExtrudeProperties_Statics::NewProp_Direction_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolyEditExtrudeProperties_Statics::NewProp_bShellsToSolids_MetaData[] = {
		{ "Category", "Extrude" },
		{ "Comment", "/** Controls whether extruding an entire patch should create a solid or an open shell */" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
		{ "ToolTip", "Controls whether extruding an entire patch should create a solid or an open shell" },
	};
#endif
	void Z_Construct_UClass_UPolyEditExtrudeProperties_Statics::NewProp_bShellsToSolids_SetBit(void* Obj)
	{
		((UPolyEditExtrudeProperties*)Obj)->bShellsToSolids = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPolyEditExtrudeProperties_Statics::NewProp_bShellsToSolids = { "bShellsToSolids", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPolyEditExtrudeProperties), &Z_Construct_UClass_UPolyEditExtrudeProperties_Statics::NewProp_bShellsToSolids_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPolyEditExtrudeProperties_Statics::NewProp_bShellsToSolids_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolyEditExtrudeProperties_Statics::NewProp_bShellsToSolids_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPolyEditExtrudeProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolyEditExtrudeProperties_Statics::NewProp_Direction_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolyEditExtrudeProperties_Statics::NewProp_Direction,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolyEditExtrudeProperties_Statics::NewProp_bShellsToSolids,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPolyEditExtrudeProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPolyEditExtrudeProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPolyEditExtrudeProperties_Statics::ClassParams = {
		&UPolyEditExtrudeProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UPolyEditExtrudeProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UPolyEditExtrudeProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UPolyEditExtrudeProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPolyEditExtrudeProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPolyEditExtrudeProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPolyEditExtrudeProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPolyEditExtrudeProperties, 166072621);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UPolyEditExtrudeProperties>()
	{
		return UPolyEditExtrudeProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPolyEditExtrudeProperties(Z_Construct_UClass_UPolyEditExtrudeProperties, &UPolyEditExtrudeProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UPolyEditExtrudeProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPolyEditExtrudeProperties);
	void UPolyEditOffsetProperties::StaticRegisterNativesUPolyEditOffsetProperties()
	{
	}
	UClass* Z_Construct_UClass_UPolyEditOffsetProperties_NoRegister()
	{
		return UPolyEditOffsetProperties::StaticClass();
	}
	struct Z_Construct_UClass_UPolyEditOffsetProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseFaceNormals_MetaData[];
#endif
		static void NewProp_bUseFaceNormals_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseFaceNormals;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPolyEditOffsetProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolyEditOffsetProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "EditMeshPolygonsTool.h" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolyEditOffsetProperties_Statics::NewProp_bUseFaceNormals_MetaData[] = {
		{ "Category", "Offset" },
		{ "Comment", "/** Offset by averaged face normals instead of per-vertex normals */" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
		{ "ToolTip", "Offset by averaged face normals instead of per-vertex normals" },
	};
#endif
	void Z_Construct_UClass_UPolyEditOffsetProperties_Statics::NewProp_bUseFaceNormals_SetBit(void* Obj)
	{
		((UPolyEditOffsetProperties*)Obj)->bUseFaceNormals = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPolyEditOffsetProperties_Statics::NewProp_bUseFaceNormals = { "bUseFaceNormals", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPolyEditOffsetProperties), &Z_Construct_UClass_UPolyEditOffsetProperties_Statics::NewProp_bUseFaceNormals_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPolyEditOffsetProperties_Statics::NewProp_bUseFaceNormals_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolyEditOffsetProperties_Statics::NewProp_bUseFaceNormals_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPolyEditOffsetProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolyEditOffsetProperties_Statics::NewProp_bUseFaceNormals,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPolyEditOffsetProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPolyEditOffsetProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPolyEditOffsetProperties_Statics::ClassParams = {
		&UPolyEditOffsetProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UPolyEditOffsetProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UPolyEditOffsetProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UPolyEditOffsetProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPolyEditOffsetProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPolyEditOffsetProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPolyEditOffsetProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPolyEditOffsetProperties, 3145513664);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UPolyEditOffsetProperties>()
	{
		return UPolyEditOffsetProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPolyEditOffsetProperties(Z_Construct_UClass_UPolyEditOffsetProperties, &UPolyEditOffsetProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UPolyEditOffsetProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPolyEditOffsetProperties);
	void UPolyEditInsetProperties::StaticRegisterNativesUPolyEditInsetProperties()
	{
	}
	UClass* Z_Construct_UClass_UPolyEditInsetProperties_NoRegister()
	{
		return UPolyEditInsetProperties::StaticClass();
	}
	struct Z_Construct_UClass_UPolyEditInsetProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bReproject_MetaData[];
#endif
		static void NewProp_bReproject_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bReproject;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Softness_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Softness;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bBoundaryOnly_MetaData[];
#endif
		static void NewProp_bBoundaryOnly_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bBoundaryOnly;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AreaScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AreaScale;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPolyEditInsetProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolyEditInsetProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Settings for Inset operation\n */" },
		{ "IncludePath", "EditMeshPolygonsTool.h" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
		{ "ToolTip", "Settings for Inset operation" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolyEditInsetProperties_Statics::NewProp_bReproject_MetaData[] = {
		{ "Category", "Inset" },
		{ "Comment", "/** Determines whether vertices in inset region should be projected back onto input surface */" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
		{ "ToolTip", "Determines whether vertices in inset region should be projected back onto input surface" },
	};
#endif
	void Z_Construct_UClass_UPolyEditInsetProperties_Statics::NewProp_bReproject_SetBit(void* Obj)
	{
		((UPolyEditInsetProperties*)Obj)->bReproject = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPolyEditInsetProperties_Statics::NewProp_bReproject = { "bReproject", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPolyEditInsetProperties), &Z_Construct_UClass_UPolyEditInsetProperties_Statics::NewProp_bReproject_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPolyEditInsetProperties_Statics::NewProp_bReproject_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolyEditInsetProperties_Statics::NewProp_bReproject_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolyEditInsetProperties_Statics::NewProp_Softness_MetaData[] = {
		{ "Category", "Inset" },
		{ "Comment", "/** Amount of smoothing applied to inset boundary */" },
		{ "EditCondition", "bBoundaryOnly == false" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
		{ "ToolTip", "Amount of smoothing applied to inset boundary" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UPolyEditInsetProperties_Statics::NewProp_Softness = { "Softness", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPolyEditInsetProperties, Softness), METADATA_PARAMS(Z_Construct_UClass_UPolyEditInsetProperties_Statics::NewProp_Softness_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolyEditInsetProperties_Statics::NewProp_Softness_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolyEditInsetProperties_Statics::NewProp_bBoundaryOnly_MetaData[] = {
		{ "Category", "Inset" },
		{ "Comment", "/** Controls whether inset operation will move interior vertices as well as border vertices */" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
		{ "ToolTip", "Controls whether inset operation will move interior vertices as well as border vertices" },
	};
#endif
	void Z_Construct_UClass_UPolyEditInsetProperties_Statics::NewProp_bBoundaryOnly_SetBit(void* Obj)
	{
		((UPolyEditInsetProperties*)Obj)->bBoundaryOnly = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPolyEditInsetProperties_Statics::NewProp_bBoundaryOnly = { "bBoundaryOnly", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPolyEditInsetProperties), &Z_Construct_UClass_UPolyEditInsetProperties_Statics::NewProp_bBoundaryOnly_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPolyEditInsetProperties_Statics::NewProp_bBoundaryOnly_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolyEditInsetProperties_Statics::NewProp_bBoundaryOnly_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolyEditInsetProperties_Statics::NewProp_AreaScale_MetaData[] = {
		{ "Category", "Inset" },
		{ "Comment", "/** Tweak area scaling when solving for interior vertices */" },
		{ "EditCondition", "bBoundaryOnly == false" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
		{ "ToolTip", "Tweak area scaling when solving for interior vertices" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UPolyEditInsetProperties_Statics::NewProp_AreaScale = { "AreaScale", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPolyEditInsetProperties, AreaScale), METADATA_PARAMS(Z_Construct_UClass_UPolyEditInsetProperties_Statics::NewProp_AreaScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolyEditInsetProperties_Statics::NewProp_AreaScale_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPolyEditInsetProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolyEditInsetProperties_Statics::NewProp_bReproject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolyEditInsetProperties_Statics::NewProp_Softness,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolyEditInsetProperties_Statics::NewProp_bBoundaryOnly,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolyEditInsetProperties_Statics::NewProp_AreaScale,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPolyEditInsetProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPolyEditInsetProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPolyEditInsetProperties_Statics::ClassParams = {
		&UPolyEditInsetProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UPolyEditInsetProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UPolyEditInsetProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UPolyEditInsetProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPolyEditInsetProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPolyEditInsetProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPolyEditInsetProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPolyEditInsetProperties, 2196421091);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UPolyEditInsetProperties>()
	{
		return UPolyEditInsetProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPolyEditInsetProperties(Z_Construct_UClass_UPolyEditInsetProperties, &UPolyEditInsetProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UPolyEditInsetProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPolyEditInsetProperties);
	void UPolyEditOutsetProperties::StaticRegisterNativesUPolyEditOutsetProperties()
	{
	}
	UClass* Z_Construct_UClass_UPolyEditOutsetProperties_NoRegister()
	{
		return UPolyEditOutsetProperties::StaticClass();
	}
	struct Z_Construct_UClass_UPolyEditOutsetProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Softness_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Softness;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bBoundaryOnly_MetaData[];
#endif
		static void NewProp_bBoundaryOnly_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bBoundaryOnly;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AreaScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AreaScale;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPolyEditOutsetProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolyEditOutsetProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "EditMeshPolygonsTool.h" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolyEditOutsetProperties_Statics::NewProp_Softness_MetaData[] = {
		{ "Category", "Inset" },
		{ "Comment", "/** Amount of smoothing applied to outset boundary */" },
		{ "EditCondition", "bBoundaryOnly == false" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
		{ "ToolTip", "Amount of smoothing applied to outset boundary" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UPolyEditOutsetProperties_Statics::NewProp_Softness = { "Softness", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPolyEditOutsetProperties, Softness), METADATA_PARAMS(Z_Construct_UClass_UPolyEditOutsetProperties_Statics::NewProp_Softness_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolyEditOutsetProperties_Statics::NewProp_Softness_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolyEditOutsetProperties_Statics::NewProp_bBoundaryOnly_MetaData[] = {
		{ "Category", "Inset" },
		{ "Comment", "/** Controls whether outset operation will move interior vertices as well as border vertices */" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
		{ "ToolTip", "Controls whether outset operation will move interior vertices as well as border vertices" },
	};
#endif
	void Z_Construct_UClass_UPolyEditOutsetProperties_Statics::NewProp_bBoundaryOnly_SetBit(void* Obj)
	{
		((UPolyEditOutsetProperties*)Obj)->bBoundaryOnly = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPolyEditOutsetProperties_Statics::NewProp_bBoundaryOnly = { "bBoundaryOnly", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPolyEditOutsetProperties), &Z_Construct_UClass_UPolyEditOutsetProperties_Statics::NewProp_bBoundaryOnly_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPolyEditOutsetProperties_Statics::NewProp_bBoundaryOnly_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolyEditOutsetProperties_Statics::NewProp_bBoundaryOnly_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolyEditOutsetProperties_Statics::NewProp_AreaScale_MetaData[] = {
		{ "Category", "Inset" },
		{ "Comment", "/** Tweak area scaling when solving for interior vertices */" },
		{ "EditCondition", "bBoundaryOnly == false" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
		{ "ToolTip", "Tweak area scaling when solving for interior vertices" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UPolyEditOutsetProperties_Statics::NewProp_AreaScale = { "AreaScale", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPolyEditOutsetProperties, AreaScale), METADATA_PARAMS(Z_Construct_UClass_UPolyEditOutsetProperties_Statics::NewProp_AreaScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolyEditOutsetProperties_Statics::NewProp_AreaScale_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPolyEditOutsetProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolyEditOutsetProperties_Statics::NewProp_Softness,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolyEditOutsetProperties_Statics::NewProp_bBoundaryOnly,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolyEditOutsetProperties_Statics::NewProp_AreaScale,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPolyEditOutsetProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPolyEditOutsetProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPolyEditOutsetProperties_Statics::ClassParams = {
		&UPolyEditOutsetProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UPolyEditOutsetProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UPolyEditOutsetProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UPolyEditOutsetProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPolyEditOutsetProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPolyEditOutsetProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPolyEditOutsetProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPolyEditOutsetProperties, 2756175728);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UPolyEditOutsetProperties>()
	{
		return UPolyEditOutsetProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPolyEditOutsetProperties(Z_Construct_UClass_UPolyEditOutsetProperties, &UPolyEditOutsetProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UPolyEditOutsetProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPolyEditOutsetProperties);
	void UPolyEditCutProperties::StaticRegisterNativesUPolyEditCutProperties()
	{
	}
	UClass* Z_Construct_UClass_UPolyEditCutProperties_NoRegister()
	{
		return UPolyEditCutProperties::StaticClass();
	}
	struct Z_Construct_UClass_UPolyEditCutProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Orientation_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Orientation_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Orientation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSnapToVertices_MetaData[];
#endif
		static void NewProp_bSnapToVertices_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSnapToVertices;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPolyEditCutProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolyEditCutProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "EditMeshPolygonsTool.h" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UPolyEditCutProperties_Statics::NewProp_Orientation_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolyEditCutProperties_Statics::NewProp_Orientation_MetaData[] = {
		{ "Category", "Cut" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UPolyEditCutProperties_Statics::NewProp_Orientation = { "Orientation", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPolyEditCutProperties, Orientation), Z_Construct_UEnum_MeshModelingTools_EPolyEditCutPlaneOrientation, METADATA_PARAMS(Z_Construct_UClass_UPolyEditCutProperties_Statics::NewProp_Orientation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolyEditCutProperties_Statics::NewProp_Orientation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolyEditCutProperties_Statics::NewProp_bSnapToVertices_MetaData[] = {
		{ "Category", "Cut" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
	void Z_Construct_UClass_UPolyEditCutProperties_Statics::NewProp_bSnapToVertices_SetBit(void* Obj)
	{
		((UPolyEditCutProperties*)Obj)->bSnapToVertices = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPolyEditCutProperties_Statics::NewProp_bSnapToVertices = { "bSnapToVertices", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPolyEditCutProperties), &Z_Construct_UClass_UPolyEditCutProperties_Statics::NewProp_bSnapToVertices_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPolyEditCutProperties_Statics::NewProp_bSnapToVertices_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolyEditCutProperties_Statics::NewProp_bSnapToVertices_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPolyEditCutProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolyEditCutProperties_Statics::NewProp_Orientation_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolyEditCutProperties_Statics::NewProp_Orientation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolyEditCutProperties_Statics::NewProp_bSnapToVertices,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPolyEditCutProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPolyEditCutProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPolyEditCutProperties_Statics::ClassParams = {
		&UPolyEditCutProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UPolyEditCutProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UPolyEditCutProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UPolyEditCutProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPolyEditCutProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPolyEditCutProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPolyEditCutProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPolyEditCutProperties, 269060614);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UPolyEditCutProperties>()
	{
		return UPolyEditCutProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPolyEditCutProperties(Z_Construct_UClass_UPolyEditCutProperties, &UPolyEditCutProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UPolyEditCutProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPolyEditCutProperties);
	void UPolyEditSetUVProperties::StaticRegisterNativesUPolyEditSetUVProperties()
	{
	}
	UClass* Z_Construct_UClass_UPolyEditSetUVProperties_NoRegister()
	{
		return UPolyEditSetUVProperties::StaticClass();
	}
	struct Z_Construct_UClass_UPolyEditSetUVProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowMaterial_MetaData[];
#endif
		static void NewProp_bShowMaterial_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowMaterial;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPolyEditSetUVProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolyEditSetUVProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "EditMeshPolygonsTool.h" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolyEditSetUVProperties_Statics::NewProp_bShowMaterial_MetaData[] = {
		{ "Category", "PlanarProjectUV" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
	void Z_Construct_UClass_UPolyEditSetUVProperties_Statics::NewProp_bShowMaterial_SetBit(void* Obj)
	{
		((UPolyEditSetUVProperties*)Obj)->bShowMaterial = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPolyEditSetUVProperties_Statics::NewProp_bShowMaterial = { "bShowMaterial", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPolyEditSetUVProperties), &Z_Construct_UClass_UPolyEditSetUVProperties_Statics::NewProp_bShowMaterial_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPolyEditSetUVProperties_Statics::NewProp_bShowMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolyEditSetUVProperties_Statics::NewProp_bShowMaterial_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPolyEditSetUVProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolyEditSetUVProperties_Statics::NewProp_bShowMaterial,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPolyEditSetUVProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPolyEditSetUVProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPolyEditSetUVProperties_Statics::ClassParams = {
		&UPolyEditSetUVProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UPolyEditSetUVProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UPolyEditSetUVProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UPolyEditSetUVProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPolyEditSetUVProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPolyEditSetUVProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPolyEditSetUVProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPolyEditSetUVProperties, 2463543184);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UPolyEditSetUVProperties>()
	{
		return UPolyEditSetUVProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPolyEditSetUVProperties(Z_Construct_UClass_UPolyEditSetUVProperties, &UPolyEditSetUVProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UPolyEditSetUVProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPolyEditSetUVProperties);
	void UEditMeshPolygonsTool::StaticRegisterNativesUEditMeshPolygonsTool()
	{
	}
	UClass* Z_Construct_UClass_UEditMeshPolygonsTool_NoRegister()
	{
		return UEditMeshPolygonsTool::StaticClass();
	}
	struct Z_Construct_UClass_UEditMeshPolygonsTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DynamicMeshComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DynamicMeshComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CommonProps_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CommonProps;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EditActions_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_EditActions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EditActions_Triangles_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_EditActions_Triangles;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EditEdgeActions_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_EditEdgeActions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EditEdgeActions_Triangles_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_EditEdgeActions_Triangles;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EditUVActions_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_EditUVActions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExtrudeProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ExtrudeProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OffsetProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OffsetProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InsetProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InsetProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OutsetProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OutsetProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CutProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CutProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SetUVProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SetUVProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SelectionMechanic_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SelectionMechanic;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MultiTransformer_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MultiTransformer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EditPreview_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_EditPreview;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExtrudeHeightMechanic_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ExtrudeHeightMechanic;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurveDistMechanic_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CurveDistMechanic;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SurfacePathMechanic_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SurfacePathMechanic;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEditMeshPolygonsTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshSurfacePointTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditMeshPolygonsTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "EditMeshPolygonsTool.h" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_DynamicMeshComponent_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_DynamicMeshComponent = { "DynamicMeshComponent", nullptr, (EPropertyFlags)0x0020080000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEditMeshPolygonsTool, DynamicMeshComponent), Z_Construct_UClass_USimpleDynamicMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_DynamicMeshComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_DynamicMeshComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_CommonProps_MetaData[] = {
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_CommonProps = { "CommonProps", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEditMeshPolygonsTool, CommonProps), Z_Construct_UClass_UPolyEditCommonProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_CommonProps_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_CommonProps_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_EditActions_MetaData[] = {
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_EditActions = { "EditActions", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEditMeshPolygonsTool, EditActions), Z_Construct_UClass_UEditMeshPolygonsToolActions_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_EditActions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_EditActions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_EditActions_Triangles_MetaData[] = {
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_EditActions_Triangles = { "EditActions_Triangles", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEditMeshPolygonsTool, EditActions_Triangles), Z_Construct_UClass_UEditMeshPolygonsToolActions_Triangles_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_EditActions_Triangles_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_EditActions_Triangles_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_EditEdgeActions_MetaData[] = {
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_EditEdgeActions = { "EditEdgeActions", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEditMeshPolygonsTool, EditEdgeActions), Z_Construct_UClass_UEditMeshPolygonsToolEdgeActions_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_EditEdgeActions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_EditEdgeActions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_EditEdgeActions_Triangles_MetaData[] = {
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_EditEdgeActions_Triangles = { "EditEdgeActions_Triangles", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEditMeshPolygonsTool, EditEdgeActions_Triangles), Z_Construct_UClass_UEditMeshPolygonsToolEdgeActions_Triangles_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_EditEdgeActions_Triangles_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_EditEdgeActions_Triangles_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_EditUVActions_MetaData[] = {
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_EditUVActions = { "EditUVActions", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEditMeshPolygonsTool, EditUVActions), Z_Construct_UClass_UEditMeshPolygonsToolUVActions_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_EditUVActions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_EditUVActions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_ExtrudeProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_ExtrudeProperties = { "ExtrudeProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEditMeshPolygonsTool, ExtrudeProperties), Z_Construct_UClass_UPolyEditExtrudeProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_ExtrudeProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_ExtrudeProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_OffsetProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_OffsetProperties = { "OffsetProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEditMeshPolygonsTool, OffsetProperties), Z_Construct_UClass_UPolyEditOffsetProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_OffsetProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_OffsetProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_InsetProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_InsetProperties = { "InsetProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEditMeshPolygonsTool, InsetProperties), Z_Construct_UClass_UPolyEditInsetProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_InsetProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_InsetProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_OutsetProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_OutsetProperties = { "OutsetProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEditMeshPolygonsTool, OutsetProperties), Z_Construct_UClass_UPolyEditOutsetProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_OutsetProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_OutsetProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_CutProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_CutProperties = { "CutProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEditMeshPolygonsTool, CutProperties), Z_Construct_UClass_UPolyEditCutProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_CutProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_CutProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_SetUVProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_SetUVProperties = { "SetUVProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEditMeshPolygonsTool, SetUVProperties), Z_Construct_UClass_UPolyEditSetUVProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_SetUVProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_SetUVProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_SelectionMechanic_MetaData[] = {
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_SelectionMechanic = { "SelectionMechanic", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEditMeshPolygonsTool, SelectionMechanic), Z_Construct_UClass_UPolygonSelectionMechanic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_SelectionMechanic_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_SelectionMechanic_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_MultiTransformer_MetaData[] = {
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_MultiTransformer = { "MultiTransformer", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEditMeshPolygonsTool, MultiTransformer), Z_Construct_UClass_UMultiTransformer_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_MultiTransformer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_MultiTransformer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_EditPreview_MetaData[] = {
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_EditPreview = { "EditPreview", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEditMeshPolygonsTool, EditPreview), Z_Construct_UClass_UPolyEditPreviewMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_EditPreview_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_EditPreview_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_ExtrudeHeightMechanic_MetaData[] = {
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_ExtrudeHeightMechanic = { "ExtrudeHeightMechanic", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEditMeshPolygonsTool, ExtrudeHeightMechanic), Z_Construct_UClass_UPlaneDistanceFromHitMechanic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_ExtrudeHeightMechanic_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_ExtrudeHeightMechanic_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_CurveDistMechanic_MetaData[] = {
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_CurveDistMechanic = { "CurveDistMechanic", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEditMeshPolygonsTool, CurveDistMechanic), Z_Construct_UClass_USpatialCurveDistanceMechanic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_CurveDistMechanic_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_CurveDistMechanic_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_SurfacePathMechanic_MetaData[] = {
		{ "ModuleRelativePath", "Public/EditMeshPolygonsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_SurfacePathMechanic = { "SurfacePathMechanic", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEditMeshPolygonsTool, SurfacePathMechanic), Z_Construct_UClass_UCollectSurfacePathMechanic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_SurfacePathMechanic_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_SurfacePathMechanic_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UEditMeshPolygonsTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_DynamicMeshComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_CommonProps,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_EditActions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_EditActions_Triangles,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_EditEdgeActions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_EditEdgeActions_Triangles,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_EditUVActions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_ExtrudeProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_OffsetProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_InsetProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_OutsetProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_CutProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_SetUVProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_SelectionMechanic,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_MultiTransformer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_EditPreview,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_ExtrudeHeightMechanic,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_CurveDistMechanic,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditMeshPolygonsTool_Statics::NewProp_SurfacePathMechanic,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEditMeshPolygonsTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEditMeshPolygonsTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEditMeshPolygonsTool_Statics::ClassParams = {
		&UEditMeshPolygonsTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UEditMeshPolygonsTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UEditMeshPolygonsTool_Statics::PropPointers),
		0,
		0x009000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UEditMeshPolygonsTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEditMeshPolygonsTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEditMeshPolygonsTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEditMeshPolygonsTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEditMeshPolygonsTool, 2315764783);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UEditMeshPolygonsTool>()
	{
		return UEditMeshPolygonsTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEditMeshPolygonsTool(Z_Construct_UClass_UEditMeshPolygonsTool, &UEditMeshPolygonsTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UEditMeshPolygonsTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEditMeshPolygonsTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
