// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/HoleFillTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeHoleFillTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EHoleFillToolActions();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UHoleFillToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UHoleFillToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_USmoothHoleFillProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_USmoothHoleFillProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UHoleFillToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UHoleFillToolProperties();
	MODELINGOPERATORS_API UEnum* Z_Construct_UEnum_ModelingOperators_EHoleFillOpFillType();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UHoleFillToolActions_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UHoleFillToolActions();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UHoleFillStatisticsProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UHoleFillStatisticsProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UHoleFillOperatorFactory_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UHoleFillOperatorFactory();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UHoleFillTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UHoleFillTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_USingleSelectionTool();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPolygonSelectionMechanic_NoRegister();
// End Cross Module References
	static UEnum* EHoleFillToolActions_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EHoleFillToolActions, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EHoleFillToolActions"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EHoleFillToolActions>()
	{
		return EHoleFillToolActions_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EHoleFillToolActions(EHoleFillToolActions_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EHoleFillToolActions"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EHoleFillToolActions_Hash() { return 2893938947U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EHoleFillToolActions()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EHoleFillToolActions"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EHoleFillToolActions_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EHoleFillToolActions::NoAction", (int64)EHoleFillToolActions::NoAction },
				{ "EHoleFillToolActions::SelectAll", (int64)EHoleFillToolActions::SelectAll },
				{ "EHoleFillToolActions::ClearSelection", (int64)EHoleFillToolActions::ClearSelection },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ClearSelection.Name", "EHoleFillToolActions::ClearSelection" },
				{ "ModuleRelativePath", "Public/HoleFillTool.h" },
				{ "NoAction.Name", "EHoleFillToolActions::NoAction" },
				{ "SelectAll.Name", "EHoleFillToolActions::SelectAll" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EHoleFillToolActions",
				"EHoleFillToolActions",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UHoleFillToolBuilder::StaticRegisterNativesUHoleFillToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UHoleFillToolBuilder_NoRegister()
	{
		return UHoleFillToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UHoleFillToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UHoleFillToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoleFillToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/*\n * Tool builder\n */" },
		{ "IncludePath", "HoleFillTool.h" },
		{ "ModuleRelativePath", "Public/HoleFillTool.h" },
		{ "ToolTip", "* Tool builder" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UHoleFillToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UHoleFillToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UHoleFillToolBuilder_Statics::ClassParams = {
		&UHoleFillToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UHoleFillToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UHoleFillToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UHoleFillToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UHoleFillToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UHoleFillToolBuilder, 3739009975);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UHoleFillToolBuilder>()
	{
		return UHoleFillToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UHoleFillToolBuilder(Z_Construct_UClass_UHoleFillToolBuilder, &UHoleFillToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UHoleFillToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UHoleFillToolBuilder);
	void USmoothHoleFillProperties::StaticRegisterNativesUSmoothHoleFillProperties()
	{
	}
	UClass* Z_Construct_UClass_USmoothHoleFillProperties_NoRegister()
	{
		return USmoothHoleFillProperties::StaticClass();
	}
	struct Z_Construct_UClass_USmoothHoleFillProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bConstrainToHoleInterior_MetaData[];
#endif
		static void NewProp_bConstrainToHoleInterior_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bConstrainToHoleInterior;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RemeshingExteriorRegionWidth_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_RemeshingExteriorRegionWidth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SmoothingExteriorRegionWidth_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_SmoothingExteriorRegionWidth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SmoothingInteriorRegionWidth_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_SmoothingInteriorRegionWidth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InteriorSmoothness_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InteriorSmoothness;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FillDensityScalar_MetaData[];
#endif
		static const UE4CodeGen_Private::FDoublePropertyParams NewProp_FillDensityScalar;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bProjectDuringRemesh_MetaData[];
#endif
		static void NewProp_bProjectDuringRemesh_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bProjectDuringRemesh;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USmoothHoleFillProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USmoothHoleFillProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/*\n * Properties. This class reflects the parameters in FSmoothFillOptions, but is decorated to allow use in the UI system.\n */" },
		{ "IncludePath", "HoleFillTool.h" },
		{ "ModuleRelativePath", "Public/HoleFillTool.h" },
		{ "ToolTip", "* Properties. This class reflects the parameters in FSmoothFillOptions, but is decorated to allow use in the UI system." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USmoothHoleFillProperties_Statics::NewProp_bConstrainToHoleInterior_MetaData[] = {
		{ "Category", "SmoothHoleFillOptions" },
		{ "Comment", "/** Allow smoothing and remeshing of triangles outside of the fill region */" },
		{ "ModuleRelativePath", "Public/HoleFillTool.h" },
		{ "ToolTip", "Allow smoothing and remeshing of triangles outside of the fill region" },
	};
#endif
	void Z_Construct_UClass_USmoothHoleFillProperties_Statics::NewProp_bConstrainToHoleInterior_SetBit(void* Obj)
	{
		((USmoothHoleFillProperties*)Obj)->bConstrainToHoleInterior = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USmoothHoleFillProperties_Statics::NewProp_bConstrainToHoleInterior = { "bConstrainToHoleInterior", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(USmoothHoleFillProperties), &Z_Construct_UClass_USmoothHoleFillProperties_Statics::NewProp_bConstrainToHoleInterior_SetBit, METADATA_PARAMS(Z_Construct_UClass_USmoothHoleFillProperties_Statics::NewProp_bConstrainToHoleInterior_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USmoothHoleFillProperties_Statics::NewProp_bConstrainToHoleInterior_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USmoothHoleFillProperties_Statics::NewProp_RemeshingExteriorRegionWidth_MetaData[] = {
		{ "Category", "SmoothHoleFillOptions" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Number of vertex rings outside of the fill region to allow remeshing */" },
		{ "EditCondition", "!bConstrainToHoleInterior" },
		{ "ModuleRelativePath", "Public/HoleFillTool.h" },
		{ "ToolTip", "Number of vertex rings outside of the fill region to allow remeshing" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_USmoothHoleFillProperties_Statics::NewProp_RemeshingExteriorRegionWidth = { "RemeshingExteriorRegionWidth", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USmoothHoleFillProperties, RemeshingExteriorRegionWidth), METADATA_PARAMS(Z_Construct_UClass_USmoothHoleFillProperties_Statics::NewProp_RemeshingExteriorRegionWidth_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USmoothHoleFillProperties_Statics::NewProp_RemeshingExteriorRegionWidth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USmoothHoleFillProperties_Statics::NewProp_SmoothingExteriorRegionWidth_MetaData[] = {
		{ "Category", "SmoothHoleFillOptions" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Number of vertex rings outside of the fill region to perform smoothing */" },
		{ "ModuleRelativePath", "Public/HoleFillTool.h" },
		{ "ToolTip", "Number of vertex rings outside of the fill region to perform smoothing" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_USmoothHoleFillProperties_Statics::NewProp_SmoothingExteriorRegionWidth = { "SmoothingExteriorRegionWidth", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USmoothHoleFillProperties, SmoothingExteriorRegionWidth), METADATA_PARAMS(Z_Construct_UClass_USmoothHoleFillProperties_Statics::NewProp_SmoothingExteriorRegionWidth_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USmoothHoleFillProperties_Statics::NewProp_SmoothingExteriorRegionWidth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USmoothHoleFillProperties_Statics::NewProp_SmoothingInteriorRegionWidth_MetaData[] = {
		{ "Category", "SmoothHoleFillOptions" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Number of vertex rings away from the fill region boundary to constrain smoothing */" },
		{ "ModuleRelativePath", "Public/HoleFillTool.h" },
		{ "ToolTip", "Number of vertex rings away from the fill region boundary to constrain smoothing" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_USmoothHoleFillProperties_Statics::NewProp_SmoothingInteriorRegionWidth = { "SmoothingInteriorRegionWidth", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USmoothHoleFillProperties, SmoothingInteriorRegionWidth), METADATA_PARAMS(Z_Construct_UClass_USmoothHoleFillProperties_Statics::NewProp_SmoothingInteriorRegionWidth_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USmoothHoleFillProperties_Statics::NewProp_SmoothingInteriorRegionWidth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USmoothHoleFillProperties_Statics::NewProp_InteriorSmoothness_MetaData[] = {
		{ "Category", "SmoothHoleFillOptions" },
		{ "ClampMax", "100.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Desired Smoothness. This is not a linear quantity, but larger numbers produce smoother results */" },
		{ "ModuleRelativePath", "Public/HoleFillTool.h" },
		{ "ToolTip", "Desired Smoothness. This is not a linear quantity, but larger numbers produce smoother results" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_USmoothHoleFillProperties_Statics::NewProp_InteriorSmoothness = { "InteriorSmoothness", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USmoothHoleFillProperties, InteriorSmoothness), METADATA_PARAMS(Z_Construct_UClass_USmoothHoleFillProperties_Statics::NewProp_InteriorSmoothness_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USmoothHoleFillProperties_Statics::NewProp_InteriorSmoothness_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USmoothHoleFillProperties_Statics::NewProp_FillDensityScalar_MetaData[] = {
		{ "Category", "SmoothHoleFillOptions" },
		{ "ClampMax", "10.0" },
		{ "ClampMin", "0.001" },
		{ "Comment", "/** Relative triangle density of fill region */" },
		{ "ModuleRelativePath", "Public/HoleFillTool.h" },
		{ "ToolTip", "Relative triangle density of fill region" },
		{ "UIMax", "10.0" },
		{ "UIMin", "0.001" },
	};
#endif
	const UE4CodeGen_Private::FDoublePropertyParams Z_Construct_UClass_USmoothHoleFillProperties_Statics::NewProp_FillDensityScalar = { "FillDensityScalar", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USmoothHoleFillProperties, FillDensityScalar), METADATA_PARAMS(Z_Construct_UClass_USmoothHoleFillProperties_Statics::NewProp_FillDensityScalar_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USmoothHoleFillProperties_Statics::NewProp_FillDensityScalar_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USmoothHoleFillProperties_Statics::NewProp_bProjectDuringRemesh_MetaData[] = {
		{ "Category", "SmoothHoleFillOptions" },
		{ "Comment", "/** \n\x09 * Whether to project to the original mesh during post-smooth remeshing. This can be expensive on large meshes with \n\x09 * many holes. \n\x09 */" },
		{ "ModuleRelativePath", "Public/HoleFillTool.h" },
		{ "ToolTip", "Whether to project to the original mesh during post-smooth remeshing. This can be expensive on large meshes with\nmany holes." },
	};
#endif
	void Z_Construct_UClass_USmoothHoleFillProperties_Statics::NewProp_bProjectDuringRemesh_SetBit(void* Obj)
	{
		((USmoothHoleFillProperties*)Obj)->bProjectDuringRemesh = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USmoothHoleFillProperties_Statics::NewProp_bProjectDuringRemesh = { "bProjectDuringRemesh", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(USmoothHoleFillProperties), &Z_Construct_UClass_USmoothHoleFillProperties_Statics::NewProp_bProjectDuringRemesh_SetBit, METADATA_PARAMS(Z_Construct_UClass_USmoothHoleFillProperties_Statics::NewProp_bProjectDuringRemesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USmoothHoleFillProperties_Statics::NewProp_bProjectDuringRemesh_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USmoothHoleFillProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USmoothHoleFillProperties_Statics::NewProp_bConstrainToHoleInterior,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USmoothHoleFillProperties_Statics::NewProp_RemeshingExteriorRegionWidth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USmoothHoleFillProperties_Statics::NewProp_SmoothingExteriorRegionWidth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USmoothHoleFillProperties_Statics::NewProp_SmoothingInteriorRegionWidth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USmoothHoleFillProperties_Statics::NewProp_InteriorSmoothness,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USmoothHoleFillProperties_Statics::NewProp_FillDensityScalar,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USmoothHoleFillProperties_Statics::NewProp_bProjectDuringRemesh,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USmoothHoleFillProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USmoothHoleFillProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USmoothHoleFillProperties_Statics::ClassParams = {
		&USmoothHoleFillProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USmoothHoleFillProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USmoothHoleFillProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_USmoothHoleFillProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USmoothHoleFillProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USmoothHoleFillProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USmoothHoleFillProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USmoothHoleFillProperties, 1790494186);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<USmoothHoleFillProperties>()
	{
		return USmoothHoleFillProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USmoothHoleFillProperties(Z_Construct_UClass_USmoothHoleFillProperties, &USmoothHoleFillProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("USmoothHoleFillProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USmoothHoleFillProperties);
	void UHoleFillToolProperties::StaticRegisterNativesUHoleFillToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UHoleFillToolProperties_NoRegister()
	{
		return UHoleFillToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UHoleFillToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_FillType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FillType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_FillType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRemoveIsolatedTriangles_MetaData[];
#endif
		static void NewProp_bRemoveIsolatedTriangles_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRemoveIsolatedTriangles;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UHoleFillToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoleFillToolProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "HoleFillTool.h" },
		{ "ModuleRelativePath", "Public/HoleFillTool.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UHoleFillToolProperties_Statics::NewProp_FillType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoleFillToolProperties_Statics::NewProp_FillType_MetaData[] = {
		{ "Category", "Options" },
		{ "ModuleRelativePath", "Public/HoleFillTool.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UHoleFillToolProperties_Statics::NewProp_FillType = { "FillType", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UHoleFillToolProperties, FillType), Z_Construct_UEnum_ModelingOperators_EHoleFillOpFillType, METADATA_PARAMS(Z_Construct_UClass_UHoleFillToolProperties_Statics::NewProp_FillType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoleFillToolProperties_Statics::NewProp_FillType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoleFillToolProperties_Statics::NewProp_bRemoveIsolatedTriangles_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Clean up triangles that have no neighbors */" },
		{ "ModuleRelativePath", "Public/HoleFillTool.h" },
		{ "ToolTip", "Clean up triangles that have no neighbors" },
	};
#endif
	void Z_Construct_UClass_UHoleFillToolProperties_Statics::NewProp_bRemoveIsolatedTriangles_SetBit(void* Obj)
	{
		((UHoleFillToolProperties*)Obj)->bRemoveIsolatedTriangles = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UHoleFillToolProperties_Statics::NewProp_bRemoveIsolatedTriangles = { "bRemoveIsolatedTriangles", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UHoleFillToolProperties), &Z_Construct_UClass_UHoleFillToolProperties_Statics::NewProp_bRemoveIsolatedTriangles_SetBit, METADATA_PARAMS(Z_Construct_UClass_UHoleFillToolProperties_Statics::NewProp_bRemoveIsolatedTriangles_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoleFillToolProperties_Statics::NewProp_bRemoveIsolatedTriangles_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UHoleFillToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoleFillToolProperties_Statics::NewProp_FillType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoleFillToolProperties_Statics::NewProp_FillType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoleFillToolProperties_Statics::NewProp_bRemoveIsolatedTriangles,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UHoleFillToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UHoleFillToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UHoleFillToolProperties_Statics::ClassParams = {
		&UHoleFillToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UHoleFillToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UHoleFillToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UHoleFillToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UHoleFillToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UHoleFillToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UHoleFillToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UHoleFillToolProperties, 1259737846);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UHoleFillToolProperties>()
	{
		return UHoleFillToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UHoleFillToolProperties(Z_Construct_UClass_UHoleFillToolProperties, &UHoleFillToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UHoleFillToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UHoleFillToolProperties);
	DEFINE_FUNCTION(UHoleFillToolActions::execClear)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Clear();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UHoleFillToolActions::execSelectAll)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SelectAll();
		P_NATIVE_END;
	}
	void UHoleFillToolActions::StaticRegisterNativesUHoleFillToolActions()
	{
		UClass* Class = UHoleFillToolActions::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Clear", &UHoleFillToolActions::execClear },
			{ "SelectAll", &UHoleFillToolActions::execSelectAll },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UHoleFillToolActions_Clear_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UHoleFillToolActions_Clear_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "SelectionEdits" },
		{ "DisplayName", "Clear" },
		{ "DisplayPriority", "1" },
		{ "ModuleRelativePath", "Public/HoleFillTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UHoleFillToolActions_Clear_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UHoleFillToolActions, nullptr, "Clear", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UHoleFillToolActions_Clear_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UHoleFillToolActions_Clear_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UHoleFillToolActions_Clear()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UHoleFillToolActions_Clear_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UHoleFillToolActions_SelectAll_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UHoleFillToolActions_SelectAll_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "SelectionEdits" },
		{ "DisplayName", "Select All" },
		{ "DisplayPriority", "1" },
		{ "ModuleRelativePath", "Public/HoleFillTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UHoleFillToolActions_SelectAll_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UHoleFillToolActions, nullptr, "SelectAll", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UHoleFillToolActions_SelectAll_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UHoleFillToolActions_SelectAll_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UHoleFillToolActions_SelectAll()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UHoleFillToolActions_SelectAll_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UHoleFillToolActions_NoRegister()
	{
		return UHoleFillToolActions::StaticClass();
	}
	struct Z_Construct_UClass_UHoleFillToolActions_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UHoleFillToolActions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UHoleFillToolActions_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UHoleFillToolActions_Clear, "Clear" }, // 2054285620
		{ &Z_Construct_UFunction_UHoleFillToolActions_SelectAll, "SelectAll" }, // 4268462034
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoleFillToolActions_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "HoleFillTool.h" },
		{ "ModuleRelativePath", "Public/HoleFillTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UHoleFillToolActions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UHoleFillToolActions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UHoleFillToolActions_Statics::ClassParams = {
		&UHoleFillToolActions::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UHoleFillToolActions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UHoleFillToolActions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UHoleFillToolActions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UHoleFillToolActions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UHoleFillToolActions, 1983906808);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UHoleFillToolActions>()
	{
		return UHoleFillToolActions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UHoleFillToolActions(Z_Construct_UClass_UHoleFillToolActions, &UHoleFillToolActions::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UHoleFillToolActions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UHoleFillToolActions);
	void UHoleFillStatisticsProperties::StaticRegisterNativesUHoleFillStatisticsProperties()
	{
	}
	UClass* Z_Construct_UClass_UHoleFillStatisticsProperties_NoRegister()
	{
		return UHoleFillStatisticsProperties::StaticClass();
	}
	struct Z_Construct_UClass_UHoleFillStatisticsProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InitialHoles_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InitialHoles;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SelectedHoles_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SelectedHoles;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SuccessfulFills_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SuccessfulFills;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FailedFills_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FailedFills;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RemainingHoles_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_RemainingHoles;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UHoleFillStatisticsProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoleFillStatisticsProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "HoleFillTool.h" },
		{ "ModuleRelativePath", "Public/HoleFillTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoleFillStatisticsProperties_Statics::NewProp_InitialHoles_MetaData[] = {
		{ "Category", "HoleFillStatistics" },
		{ "ModuleRelativePath", "Public/HoleFillTool.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UHoleFillStatisticsProperties_Statics::NewProp_InitialHoles = { "InitialHoles", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UHoleFillStatisticsProperties, InitialHoles), METADATA_PARAMS(Z_Construct_UClass_UHoleFillStatisticsProperties_Statics::NewProp_InitialHoles_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoleFillStatisticsProperties_Statics::NewProp_InitialHoles_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoleFillStatisticsProperties_Statics::NewProp_SelectedHoles_MetaData[] = {
		{ "Category", "HoleFillStatistics" },
		{ "ModuleRelativePath", "Public/HoleFillTool.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UHoleFillStatisticsProperties_Statics::NewProp_SelectedHoles = { "SelectedHoles", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UHoleFillStatisticsProperties, SelectedHoles), METADATA_PARAMS(Z_Construct_UClass_UHoleFillStatisticsProperties_Statics::NewProp_SelectedHoles_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoleFillStatisticsProperties_Statics::NewProp_SelectedHoles_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoleFillStatisticsProperties_Statics::NewProp_SuccessfulFills_MetaData[] = {
		{ "Category", "HoleFillStatistics" },
		{ "ModuleRelativePath", "Public/HoleFillTool.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UHoleFillStatisticsProperties_Statics::NewProp_SuccessfulFills = { "SuccessfulFills", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UHoleFillStatisticsProperties, SuccessfulFills), METADATA_PARAMS(Z_Construct_UClass_UHoleFillStatisticsProperties_Statics::NewProp_SuccessfulFills_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoleFillStatisticsProperties_Statics::NewProp_SuccessfulFills_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoleFillStatisticsProperties_Statics::NewProp_FailedFills_MetaData[] = {
		{ "Category", "HoleFillStatistics" },
		{ "ModuleRelativePath", "Public/HoleFillTool.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UHoleFillStatisticsProperties_Statics::NewProp_FailedFills = { "FailedFills", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UHoleFillStatisticsProperties, FailedFills), METADATA_PARAMS(Z_Construct_UClass_UHoleFillStatisticsProperties_Statics::NewProp_FailedFills_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoleFillStatisticsProperties_Statics::NewProp_FailedFills_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoleFillStatisticsProperties_Statics::NewProp_RemainingHoles_MetaData[] = {
		{ "Category", "HoleFillStatistics" },
		{ "ModuleRelativePath", "Public/HoleFillTool.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UHoleFillStatisticsProperties_Statics::NewProp_RemainingHoles = { "RemainingHoles", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UHoleFillStatisticsProperties, RemainingHoles), METADATA_PARAMS(Z_Construct_UClass_UHoleFillStatisticsProperties_Statics::NewProp_RemainingHoles_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoleFillStatisticsProperties_Statics::NewProp_RemainingHoles_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UHoleFillStatisticsProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoleFillStatisticsProperties_Statics::NewProp_InitialHoles,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoleFillStatisticsProperties_Statics::NewProp_SelectedHoles,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoleFillStatisticsProperties_Statics::NewProp_SuccessfulFills,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoleFillStatisticsProperties_Statics::NewProp_FailedFills,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoleFillStatisticsProperties_Statics::NewProp_RemainingHoles,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UHoleFillStatisticsProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UHoleFillStatisticsProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UHoleFillStatisticsProperties_Statics::ClassParams = {
		&UHoleFillStatisticsProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UHoleFillStatisticsProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UHoleFillStatisticsProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UHoleFillStatisticsProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UHoleFillStatisticsProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UHoleFillStatisticsProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UHoleFillStatisticsProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UHoleFillStatisticsProperties, 1725898434);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UHoleFillStatisticsProperties>()
	{
		return UHoleFillStatisticsProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UHoleFillStatisticsProperties(Z_Construct_UClass_UHoleFillStatisticsProperties, &UHoleFillStatisticsProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UHoleFillStatisticsProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UHoleFillStatisticsProperties);
	void UHoleFillOperatorFactory::StaticRegisterNativesUHoleFillOperatorFactory()
	{
	}
	UClass* Z_Construct_UClass_UHoleFillOperatorFactory_NoRegister()
	{
		return UHoleFillOperatorFactory::StaticClass();
	}
	struct Z_Construct_UClass_UHoleFillOperatorFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FillTool_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FillTool;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UHoleFillOperatorFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoleFillOperatorFactory_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/*\n * Operator factory\n */" },
		{ "IncludePath", "HoleFillTool.h" },
		{ "ModuleRelativePath", "Public/HoleFillTool.h" },
		{ "ToolTip", "* Operator factory" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoleFillOperatorFactory_Statics::NewProp_FillTool_MetaData[] = {
		{ "ModuleRelativePath", "Public/HoleFillTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UHoleFillOperatorFactory_Statics::NewProp_FillTool = { "FillTool", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UHoleFillOperatorFactory, FillTool), Z_Construct_UClass_UHoleFillTool_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UHoleFillOperatorFactory_Statics::NewProp_FillTool_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoleFillOperatorFactory_Statics::NewProp_FillTool_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UHoleFillOperatorFactory_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoleFillOperatorFactory_Statics::NewProp_FillTool,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UHoleFillOperatorFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UHoleFillOperatorFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UHoleFillOperatorFactory_Statics::ClassParams = {
		&UHoleFillOperatorFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UHoleFillOperatorFactory_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UHoleFillOperatorFactory_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UHoleFillOperatorFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UHoleFillOperatorFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UHoleFillOperatorFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UHoleFillOperatorFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UHoleFillOperatorFactory, 4140671929);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UHoleFillOperatorFactory>()
	{
		return UHoleFillOperatorFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UHoleFillOperatorFactory(Z_Construct_UClass_UHoleFillOperatorFactory, &UHoleFillOperatorFactory::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UHoleFillOperatorFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UHoleFillOperatorFactory);
	void UHoleFillTool::StaticRegisterNativesUHoleFillTool()
	{
	}
	UClass* Z_Construct_UClass_UHoleFillTool_NoRegister()
	{
		return UHoleFillTool::StaticClass();
	}
	struct Z_Construct_UClass_UHoleFillTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SmoothHoleFillProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SmoothHoleFillProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Properties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Properties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Actions_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Actions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Statistics_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Statistics;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Preview_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Preview;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SelectionMechanic_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SelectionMechanic;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UHoleFillTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USingleSelectionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoleFillTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/*\n * Tool\n * Inherit from IClickBehaviorTarget so we can click on boundary loops.\n */" },
		{ "IncludePath", "HoleFillTool.h" },
		{ "ModuleRelativePath", "Public/HoleFillTool.h" },
		{ "ToolTip", "* Tool\n* Inherit from IClickBehaviorTarget so we can click on boundary loops." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoleFillTool_Statics::NewProp_SmoothHoleFillProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/HoleFillTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UHoleFillTool_Statics::NewProp_SmoothHoleFillProperties = { "SmoothHoleFillProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UHoleFillTool, SmoothHoleFillProperties), Z_Construct_UClass_USmoothHoleFillProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UHoleFillTool_Statics::NewProp_SmoothHoleFillProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoleFillTool_Statics::NewProp_SmoothHoleFillProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoleFillTool_Statics::NewProp_Properties_MetaData[] = {
		{ "ModuleRelativePath", "Public/HoleFillTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UHoleFillTool_Statics::NewProp_Properties = { "Properties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UHoleFillTool, Properties), Z_Construct_UClass_UHoleFillToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UHoleFillTool_Statics::NewProp_Properties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoleFillTool_Statics::NewProp_Properties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoleFillTool_Statics::NewProp_Actions_MetaData[] = {
		{ "ModuleRelativePath", "Public/HoleFillTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UHoleFillTool_Statics::NewProp_Actions = { "Actions", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UHoleFillTool, Actions), Z_Construct_UClass_UHoleFillToolActions_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UHoleFillTool_Statics::NewProp_Actions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoleFillTool_Statics::NewProp_Actions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoleFillTool_Statics::NewProp_Statistics_MetaData[] = {
		{ "ModuleRelativePath", "Public/HoleFillTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UHoleFillTool_Statics::NewProp_Statistics = { "Statistics", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UHoleFillTool, Statistics), Z_Construct_UClass_UHoleFillStatisticsProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UHoleFillTool_Statics::NewProp_Statistics_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoleFillTool_Statics::NewProp_Statistics_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoleFillTool_Statics::NewProp_Preview_MetaData[] = {
		{ "ModuleRelativePath", "Public/HoleFillTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UHoleFillTool_Statics::NewProp_Preview = { "Preview", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UHoleFillTool, Preview), Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UHoleFillTool_Statics::NewProp_Preview_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoleFillTool_Statics::NewProp_Preview_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoleFillTool_Statics::NewProp_SelectionMechanic_MetaData[] = {
		{ "ModuleRelativePath", "Public/HoleFillTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UHoleFillTool_Statics::NewProp_SelectionMechanic = { "SelectionMechanic", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UHoleFillTool, SelectionMechanic), Z_Construct_UClass_UPolygonSelectionMechanic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UHoleFillTool_Statics::NewProp_SelectionMechanic_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoleFillTool_Statics::NewProp_SelectionMechanic_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UHoleFillTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoleFillTool_Statics::NewProp_SmoothHoleFillProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoleFillTool_Statics::NewProp_Properties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoleFillTool_Statics::NewProp_Actions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoleFillTool_Statics::NewProp_Statistics,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoleFillTool_Statics::NewProp_Preview,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoleFillTool_Statics::NewProp_SelectionMechanic,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UHoleFillTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UHoleFillTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UHoleFillTool_Statics::ClassParams = {
		&UHoleFillTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UHoleFillTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UHoleFillTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UHoleFillTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UHoleFillTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UHoleFillTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UHoleFillTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UHoleFillTool, 2076767772);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UHoleFillTool>()
	{
		return UHoleFillTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UHoleFillTool(Z_Construct_UClass_UHoleFillTool, &UHoleFillTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UHoleFillTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UHoleFillTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
