// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ModelingOperatorsEditorOnly/Public/CleaningOps/SimplifyMeshOp.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSimplifyMeshOp() {}
// Cross Module References
	MODELINGOPERATORSEDITORONLY_API UEnum* Z_Construct_UEnum_ModelingOperatorsEditorOnly_ESimplifyType();
	UPackage* Z_Construct_UPackage__Script_ModelingOperatorsEditorOnly();
	MODELINGOPERATORSEDITORONLY_API UEnum* Z_Construct_UEnum_ModelingOperatorsEditorOnly_ESimplifyTargetType();
// End Cross Module References
	static UEnum* ESimplifyType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ModelingOperatorsEditorOnly_ESimplifyType, Z_Construct_UPackage__Script_ModelingOperatorsEditorOnly(), TEXT("ESimplifyType"));
		}
		return Singleton;
	}
	template<> MODELINGOPERATORSEDITORONLY_API UEnum* StaticEnum<ESimplifyType>()
	{
		return ESimplifyType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ESimplifyType(ESimplifyType_StaticEnum, TEXT("/Script/ModelingOperatorsEditorOnly"), TEXT("ESimplifyType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ModelingOperatorsEditorOnly_ESimplifyType_Hash() { return 1582279990U; }
	UEnum* Z_Construct_UEnum_ModelingOperatorsEditorOnly_ESimplifyType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ModelingOperatorsEditorOnly();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ESimplifyType"), 0, Get_Z_Construct_UEnum_ModelingOperatorsEditorOnly_ESimplifyType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ESimplifyType::QEM", (int64)ESimplifyType::QEM },
				{ "ESimplifyType::Attribute", (int64)ESimplifyType::Attribute },
				{ "ESimplifyType::UE4Standard", (int64)ESimplifyType::UE4Standard },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Attribute.Comment", "/** Potentially higher quality. Takes the normal into account. Will not simplify UV bounaries. */" },
				{ "Attribute.DisplayName", "Normal Aware" },
				{ "Attribute.Name", "ESimplifyType::Attribute" },
				{ "Attribute.ToolTip", "Potentially higher quality. Takes the normal into account. Will not simplify UV bounaries." },
				{ "ModuleRelativePath", "Public/CleaningOps/SimplifyMeshOp.h" },
				{ "QEM.Comment", "/** Fastest. Standard quadric error metric. Will not simplify UV boundaries.*/" },
				{ "QEM.DisplayName", "QEM" },
				{ "QEM.Name", "ESimplifyType::QEM" },
				{ "QEM.ToolTip", "Fastest. Standard quadric error metric. Will not simplify UV boundaries." },
				{ "UE4Standard.Comment", "/** Highest quality reduction.  Will simplify UV boundaries. */" },
				{ "UE4Standard.DisplayName", "UE4 Standard" },
				{ "UE4Standard.Name", "ESimplifyType::UE4Standard" },
				{ "UE4Standard.ToolTip", "Highest quality reduction.  Will simplify UV boundaries." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ModelingOperatorsEditorOnly,
				nullptr,
				"ESimplifyType",
				"ESimplifyType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ESimplifyTargetType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ModelingOperatorsEditorOnly_ESimplifyTargetType, Z_Construct_UPackage__Script_ModelingOperatorsEditorOnly(), TEXT("ESimplifyTargetType"));
		}
		return Singleton;
	}
	template<> MODELINGOPERATORSEDITORONLY_API UEnum* StaticEnum<ESimplifyTargetType>()
	{
		return ESimplifyTargetType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ESimplifyTargetType(ESimplifyTargetType_StaticEnum, TEXT("/Script/ModelingOperatorsEditorOnly"), TEXT("ESimplifyTargetType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ModelingOperatorsEditorOnly_ESimplifyTargetType_Hash() { return 1223517316U; }
	UEnum* Z_Construct_UEnum_ModelingOperatorsEditorOnly_ESimplifyTargetType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ModelingOperatorsEditorOnly();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ESimplifyTargetType"), 0, Get_Z_Construct_UEnum_ModelingOperatorsEditorOnly_ESimplifyTargetType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ESimplifyTargetType::Percentage", (int64)ESimplifyTargetType::Percentage },
				{ "ESimplifyTargetType::TriangleCount", (int64)ESimplifyTargetType::TriangleCount },
				{ "ESimplifyTargetType::VertexCount", (int64)ESimplifyTargetType::VertexCount },
				{ "ESimplifyTargetType::EdgeLength", (int64)ESimplifyTargetType::EdgeLength },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "EdgeLength.Comment", "/** Target edge length */" },
				{ "EdgeLength.DisplayName", "Edge Length" },
				{ "EdgeLength.Name", "ESimplifyTargetType::EdgeLength" },
				{ "EdgeLength.ToolTip", "Target edge length" },
				{ "ModuleRelativePath", "Public/CleaningOps/SimplifyMeshOp.h" },
				{ "Percentage.Comment", "/** Percentage of input triangles */" },
				{ "Percentage.DisplayName", "Percentage" },
				{ "Percentage.Name", "ESimplifyTargetType::Percentage" },
				{ "Percentage.ToolTip", "Percentage of input triangles" },
				{ "TriangleCount.Comment", "/** Target triangle count */" },
				{ "TriangleCount.DisplayName", "Triangle Count" },
				{ "TriangleCount.Name", "ESimplifyTargetType::TriangleCount" },
				{ "TriangleCount.ToolTip", "Target triangle count" },
				{ "VertexCount.Comment", "/** Target vertex count */" },
				{ "VertexCount.DisplayName", "Vertex Count" },
				{ "VertexCount.Name", "ESimplifyTargetType::VertexCount" },
				{ "VertexCount.ToolTip", "Target vertex count" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ModelingOperatorsEditorOnly,
				nullptr,
				"ESimplifyTargetType",
				"ESimplifyTargetType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
