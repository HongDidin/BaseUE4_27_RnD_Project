// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLS_PhysicsInspectorTool_generated_h
#error "PhysicsInspectorTool.generated.h already included, missing '#pragma once' in PhysicsInspectorTool.h"
#endif
#define MESHMODELINGTOOLS_PhysicsInspectorTool_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_16_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_16_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPhysicsInspectorToolBuilder(); \
	friend struct Z_Construct_UClass_UPhysicsInspectorToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UPhysicsInspectorToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UPhysicsInspectorToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUPhysicsInspectorToolBuilder(); \
	friend struct Z_Construct_UClass_UPhysicsInspectorToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UPhysicsInspectorToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UPhysicsInspectorToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPhysicsInspectorToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPhysicsInspectorToolBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPhysicsInspectorToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPhysicsInspectorToolBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPhysicsInspectorToolBuilder(UPhysicsInspectorToolBuilder&&); \
	NO_API UPhysicsInspectorToolBuilder(const UPhysicsInspectorToolBuilder&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPhysicsInspectorToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPhysicsInspectorToolBuilder(UPhysicsInspectorToolBuilder&&); \
	NO_API UPhysicsInspectorToolBuilder(const UPhysicsInspectorToolBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPhysicsInspectorToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPhysicsInspectorToolBuilder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPhysicsInspectorToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_13_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_16_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_16_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_16_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_16_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UPhysicsInspectorToolBuilder>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_32_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_32_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_32_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_32_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPhysicsInspectorTool(); \
	friend struct Z_Construct_UClass_UPhysicsInspectorTool_Statics; \
public: \
	DECLARE_CLASS(UPhysicsInspectorTool, UMultiSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UPhysicsInspectorTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_32_INCLASS \
private: \
	static void StaticRegisterNativesUPhysicsInspectorTool(); \
	friend struct Z_Construct_UClass_UPhysicsInspectorTool_Statics; \
public: \
	DECLARE_CLASS(UPhysicsInspectorTool, UMultiSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UPhysicsInspectorTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_32_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPhysicsInspectorTool(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPhysicsInspectorTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPhysicsInspectorTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPhysicsInspectorTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPhysicsInspectorTool(UPhysicsInspectorTool&&); \
	NO_API UPhysicsInspectorTool(const UPhysicsInspectorTool&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_32_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPhysicsInspectorTool() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPhysicsInspectorTool(UPhysicsInspectorTool&&); \
	NO_API UPhysicsInspectorTool(const UPhysicsInspectorTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPhysicsInspectorTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPhysicsInspectorTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UPhysicsInspectorTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_32_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__VizSettings() { return STRUCT_OFFSET(UPhysicsInspectorTool, VizSettings); } \
	FORCEINLINE static uint32 __PPO__ObjectData() { return STRUCT_OFFSET(UPhysicsInspectorTool, ObjectData); } \
	FORCEINLINE static uint32 __PPO__LineMaterial() { return STRUCT_OFFSET(UPhysicsInspectorTool, LineMaterial); } \
	FORCEINLINE static uint32 __PPO__PreviewElements() { return STRUCT_OFFSET(UPhysicsInspectorTool, PreviewElements); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_29_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_32_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_32_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_32_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_32_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_32_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_32_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_32_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_32_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_32_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_32_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_32_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h_32_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UPhysicsInspectorTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_PhysicsInspectorTool_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
