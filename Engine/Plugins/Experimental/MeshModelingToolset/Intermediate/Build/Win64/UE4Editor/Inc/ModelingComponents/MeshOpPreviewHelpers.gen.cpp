// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ModelingComponents/Public/MeshOpPreviewHelpers.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMeshOpPreviewHelpers() {}
// Cross Module References
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_ModelingComponents();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPreviewMesh_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
// End Cross Module References
	void UMeshOpPreviewWithBackgroundCompute::StaticRegisterNativesUMeshOpPreviewWithBackgroundCompute()
	{
	}
	UClass* Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_NoRegister()
	{
		return UMeshOpPreviewWithBackgroundCompute::StaticClass();
	}
	struct Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviewMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviewMesh;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StandardMaterials_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StandardMaterials_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_StandardMaterials;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverrideMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OverrideMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorkingMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorkingMaterial;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_ModelingComponents,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * UMeshOpPreviewWithBackgroundCompute is an infrastructure object that implements a common UI\n * pattern in interactive 3D tools, where we want to run an expensive computation on a mesh that\n * is based on user-specified parameters, and show a preview of the result. The expensive computation \n * (a MeshOperator) must run in a background thread so as to not block the UI. If the user\n * changes parameters while the Operator is running, it should be canceled and restarted. \n * When it completes, the Preview will be updated. When the user is happy, the current Mesh is\n * returned to the owner of this object.\n * \n * The MeshOperators are provided by the owner via a IDynamicMeshOperatorFactory implementation.\n * The owner must also Tick() this object regularly to allow the Preview to update when the\n * background computations complete.\n * \n * If an InProgress Material is set (via ConfigureMaterials) then when a background computation\n * is active, this material will be used to draw the previous Preview result, to give the \n * user a visual indication that work is happening.\n */" },
		{ "IncludePath", "MeshOpPreviewHelpers.h" },
		{ "ModuleRelativePath", "Public/MeshOpPreviewHelpers.h" },
		{ "ToolTip", "UMeshOpPreviewWithBackgroundCompute is an infrastructure object that implements a common UI\npattern in interactive 3D tools, where we want to run an expensive computation on a mesh that\nis based on user-specified parameters, and show a preview of the result. The expensive computation\n(a MeshOperator) must run in a background thread so as to not block the UI. If the user\nchanges parameters while the Operator is running, it should be canceled and restarted.\nWhen it completes, the Preview will be updated. When the user is happy, the current Mesh is\nreturned to the owner of this object.\n\nThe MeshOperators are provided by the owner via a IDynamicMeshOperatorFactory implementation.\nThe owner must also Tick() this object regularly to allow the Preview to update when the\nbackground computations complete.\n\nIf an InProgress Material is set (via ConfigureMaterials) then when a background computation\nis active, this material will be used to draw the previous Preview result, to give the\nuser a visual indication that work is happening." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_Statics::NewProp_PreviewMesh_MetaData[] = {
		{ "Comment", "// preview of MeshOperator result\n" },
		{ "ModuleRelativePath", "Public/MeshOpPreviewHelpers.h" },
		{ "ToolTip", "preview of MeshOperator result" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_Statics::NewProp_PreviewMesh = { "PreviewMesh", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshOpPreviewWithBackgroundCompute, PreviewMesh), Z_Construct_UClass_UPreviewMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_Statics::NewProp_PreviewMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_Statics::NewProp_PreviewMesh_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_Statics::NewProp_StandardMaterials_Inner = { "StandardMaterials", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_Statics::NewProp_StandardMaterials_MetaData[] = {
		{ "Comment", "// input set of materials to assign to PreviewMesh\n" },
		{ "ModuleRelativePath", "Public/MeshOpPreviewHelpers.h" },
		{ "ToolTip", "input set of materials to assign to PreviewMesh" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_Statics::NewProp_StandardMaterials = { "StandardMaterials", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshOpPreviewWithBackgroundCompute, StandardMaterials), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_Statics::NewProp_StandardMaterials_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_Statics::NewProp_StandardMaterials_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_Statics::NewProp_OverrideMaterial_MetaData[] = {
		{ "Comment", "// override material to forward to PreviewMesh if set\n" },
		{ "ModuleRelativePath", "Public/MeshOpPreviewHelpers.h" },
		{ "ToolTip", "override material to forward to PreviewMesh if set" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_Statics::NewProp_OverrideMaterial = { "OverrideMaterial", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshOpPreviewWithBackgroundCompute, OverrideMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_Statics::NewProp_OverrideMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_Statics::NewProp_OverrideMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_Statics::NewProp_WorkingMaterial_MetaData[] = {
		{ "Comment", "// if non-null, this material is swapped in when a background compute is active\n" },
		{ "ModuleRelativePath", "Public/MeshOpPreviewHelpers.h" },
		{ "ToolTip", "if non-null, this material is swapped in when a background compute is active" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_Statics::NewProp_WorkingMaterial = { "WorkingMaterial", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshOpPreviewWithBackgroundCompute, WorkingMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_Statics::NewProp_WorkingMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_Statics::NewProp_WorkingMaterial_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_Statics::NewProp_PreviewMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_Statics::NewProp_StandardMaterials_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_Statics::NewProp_StandardMaterials,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_Statics::NewProp_OverrideMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_Statics::NewProp_WorkingMaterial,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshOpPreviewWithBackgroundCompute>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_Statics::ClassParams = {
		&UMeshOpPreviewWithBackgroundCompute::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshOpPreviewWithBackgroundCompute, 82240531);
	template<> MODELINGCOMPONENTS_API UClass* StaticClass<UMeshOpPreviewWithBackgroundCompute>()
	{
		return UMeshOpPreviewWithBackgroundCompute::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshOpPreviewWithBackgroundCompute(Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute, &UMeshOpPreviewWithBackgroundCompute::StaticClass, TEXT("/Script/ModelingComponents"), TEXT("UMeshOpPreviewWithBackgroundCompute"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshOpPreviewWithBackgroundCompute);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
