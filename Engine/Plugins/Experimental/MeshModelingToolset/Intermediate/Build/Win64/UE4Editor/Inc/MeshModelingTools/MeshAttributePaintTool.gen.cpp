// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/MeshAttributePaintTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMeshAttributePaintTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EMeshAttributePaintToolActions();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshAttributePaintToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshAttributePaintToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UMeshSurfacePointToolBuilder();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshAttributePaintToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshAttributePaintToolProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshAttributePaintEditActions_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshAttributePaintEditActions();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshAttributePaintTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshAttributePaintTool();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDynamicMeshBrushTool();
// End Cross Module References
	static UEnum* EMeshAttributePaintToolActions_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EMeshAttributePaintToolActions, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EMeshAttributePaintToolActions"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EMeshAttributePaintToolActions>()
	{
		return EMeshAttributePaintToolActions_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMeshAttributePaintToolActions(EMeshAttributePaintToolActions_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EMeshAttributePaintToolActions"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EMeshAttributePaintToolActions_Hash() { return 2206541492U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EMeshAttributePaintToolActions()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMeshAttributePaintToolActions"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EMeshAttributePaintToolActions_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMeshAttributePaintToolActions::NoAction", (int64)EMeshAttributePaintToolActions::NoAction },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/MeshAttributePaintTool.h" },
				{ "NoAction.Name", "EMeshAttributePaintToolActions::NoAction" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EMeshAttributePaintToolActions",
				"EMeshAttributePaintToolActions",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UMeshAttributePaintToolBuilder::StaticRegisterNativesUMeshAttributePaintToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UMeshAttributePaintToolBuilder_NoRegister()
	{
		return UMeshAttributePaintToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UMeshAttributePaintToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshAttributePaintToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshSurfacePointToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshAttributePaintToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Tool Builder for Attribute Paint Tool\n */" },
		{ "IncludePath", "MeshAttributePaintTool.h" },
		{ "ModuleRelativePath", "Public/MeshAttributePaintTool.h" },
		{ "ToolTip", "Tool Builder for Attribute Paint Tool" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshAttributePaintToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshAttributePaintToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshAttributePaintToolBuilder_Statics::ClassParams = {
		&UMeshAttributePaintToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshAttributePaintToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshAttributePaintToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshAttributePaintToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshAttributePaintToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshAttributePaintToolBuilder, 3116594713);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UMeshAttributePaintToolBuilder>()
	{
		return UMeshAttributePaintToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshAttributePaintToolBuilder(Z_Construct_UClass_UMeshAttributePaintToolBuilder, &UMeshAttributePaintToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UMeshAttributePaintToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshAttributePaintToolBuilder);
	void UMeshAttributePaintToolProperties::StaticRegisterNativesUMeshAttributePaintToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UMeshAttributePaintToolProperties_NoRegister()
	{
		return UMeshAttributePaintToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UMeshAttributePaintToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Attributes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Attributes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Attributes;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SelectedAttribute_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_SelectedAttribute;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttributeName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AttributeName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshAttributePaintToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshAttributePaintToolProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Selected-Attribute settings Attribute Paint Tool\n */" },
		{ "IncludePath", "MeshAttributePaintTool.h" },
		{ "ModuleRelativePath", "Public/MeshAttributePaintTool.h" },
		{ "ToolTip", "Selected-Attribute settings Attribute Paint Tool" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UMeshAttributePaintToolProperties_Statics::NewProp_Attributes_Inner = { "Attributes", nullptr, (EPropertyFlags)0x0000000000020000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshAttributePaintToolProperties_Statics::NewProp_Attributes_MetaData[] = {
		{ "Category", "Attribute" },
		{ "ModuleRelativePath", "Public/MeshAttributePaintTool.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UMeshAttributePaintToolProperties_Statics::NewProp_Attributes = { "Attributes", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshAttributePaintToolProperties, Attributes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UMeshAttributePaintToolProperties_Statics::NewProp_Attributes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshAttributePaintToolProperties_Statics::NewProp_Attributes_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshAttributePaintToolProperties_Statics::NewProp_SelectedAttribute_MetaData[] = {
		{ "ArrayClamp", "Attributes" },
		{ "Category", "Attribute" },
		{ "ModuleRelativePath", "Public/MeshAttributePaintTool.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UMeshAttributePaintToolProperties_Statics::NewProp_SelectedAttribute = { "SelectedAttribute", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshAttributePaintToolProperties, SelectedAttribute), METADATA_PARAMS(Z_Construct_UClass_UMeshAttributePaintToolProperties_Statics::NewProp_SelectedAttribute_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshAttributePaintToolProperties_Statics::NewProp_SelectedAttribute_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshAttributePaintToolProperties_Statics::NewProp_AttributeName_MetaData[] = {
		{ "Category", "Attribute" },
		{ "ModuleRelativePath", "Public/MeshAttributePaintTool.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UMeshAttributePaintToolProperties_Statics::NewProp_AttributeName = { "AttributeName", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshAttributePaintToolProperties, AttributeName), METADATA_PARAMS(Z_Construct_UClass_UMeshAttributePaintToolProperties_Statics::NewProp_AttributeName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshAttributePaintToolProperties_Statics::NewProp_AttributeName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshAttributePaintToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshAttributePaintToolProperties_Statics::NewProp_Attributes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshAttributePaintToolProperties_Statics::NewProp_Attributes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshAttributePaintToolProperties_Statics::NewProp_SelectedAttribute,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshAttributePaintToolProperties_Statics::NewProp_AttributeName,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshAttributePaintToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshAttributePaintToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshAttributePaintToolProperties_Statics::ClassParams = {
		&UMeshAttributePaintToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshAttributePaintToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshAttributePaintToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshAttributePaintToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshAttributePaintToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshAttributePaintToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshAttributePaintToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshAttributePaintToolProperties, 1742154781);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UMeshAttributePaintToolProperties>()
	{
		return UMeshAttributePaintToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshAttributePaintToolProperties(Z_Construct_UClass_UMeshAttributePaintToolProperties, &UMeshAttributePaintToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UMeshAttributePaintToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshAttributePaintToolProperties);
	void UMeshAttributePaintEditActions::StaticRegisterNativesUMeshAttributePaintEditActions()
	{
	}
	UClass* Z_Construct_UClass_UMeshAttributePaintEditActions_NoRegister()
	{
		return UMeshAttributePaintEditActions::StaticClass();
	}
	struct Z_Construct_UClass_UMeshAttributePaintEditActions_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshAttributePaintEditActions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshAttributePaintEditActions_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshAttributePaintTool.h" },
		{ "ModuleRelativePath", "Public/MeshAttributePaintTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshAttributePaintEditActions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshAttributePaintEditActions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshAttributePaintEditActions_Statics::ClassParams = {
		&UMeshAttributePaintEditActions::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshAttributePaintEditActions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshAttributePaintEditActions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshAttributePaintEditActions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshAttributePaintEditActions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshAttributePaintEditActions, 4153167656);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UMeshAttributePaintEditActions>()
	{
		return UMeshAttributePaintEditActions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshAttributePaintEditActions(Z_Construct_UClass_UMeshAttributePaintEditActions, &UMeshAttributePaintEditActions::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UMeshAttributePaintEditActions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshAttributePaintEditActions);
	void UMeshAttributePaintTool::StaticRegisterNativesUMeshAttributePaintTool()
	{
	}
	UClass* Z_Construct_UClass_UMeshAttributePaintTool_NoRegister()
	{
		return UMeshAttributePaintTool::StaticClass();
	}
	struct Z_Construct_UClass_UMeshAttributePaintTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttribProps_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AttribProps;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshAttributePaintTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDynamicMeshBrushTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshAttributePaintTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * UMeshAttributePaintTool paints single-channel float attributes on a MeshDescription.\n * \n */" },
		{ "IncludePath", "MeshAttributePaintTool.h" },
		{ "ModuleRelativePath", "Public/MeshAttributePaintTool.h" },
		{ "ToolTip", "UMeshAttributePaintTool paints single-channel float attributes on a MeshDescription." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshAttributePaintTool_Statics::NewProp_AttribProps_MetaData[] = {
		{ "ModuleRelativePath", "Public/MeshAttributePaintTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshAttributePaintTool_Statics::NewProp_AttribProps = { "AttribProps", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshAttributePaintTool, AttribProps), Z_Construct_UClass_UMeshAttributePaintToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshAttributePaintTool_Statics::NewProp_AttribProps_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshAttributePaintTool_Statics::NewProp_AttribProps_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshAttributePaintTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshAttributePaintTool_Statics::NewProp_AttribProps,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshAttributePaintTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshAttributePaintTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshAttributePaintTool_Statics::ClassParams = {
		&UMeshAttributePaintTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshAttributePaintTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshAttributePaintTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshAttributePaintTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshAttributePaintTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshAttributePaintTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshAttributePaintTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshAttributePaintTool, 1033814532);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UMeshAttributePaintTool>()
	{
		return UMeshAttributePaintTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshAttributePaintTool(Z_Construct_UClass_UMeshAttributePaintTool, &UMeshAttributePaintTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UMeshAttributePaintTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshAttributePaintTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
