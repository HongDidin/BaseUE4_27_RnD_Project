// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLS_WeldMeshEdgesTool_generated_h
#error "WeldMeshEdgesTool.generated.h already included, missing '#pragma once' in WeldMeshEdgesTool.h"
#endif
#define MESHMODELINGTOOLS_WeldMeshEdgesTool_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_24_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_24_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_24_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUWeldMeshEdgesToolBuilder(); \
	friend struct Z_Construct_UClass_UWeldMeshEdgesToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UWeldMeshEdgesToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UWeldMeshEdgesToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_24_INCLASS \
private: \
	static void StaticRegisterNativesUWeldMeshEdgesToolBuilder(); \
	friend struct Z_Construct_UClass_UWeldMeshEdgesToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UWeldMeshEdgesToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UWeldMeshEdgesToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UWeldMeshEdgesToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UWeldMeshEdgesToolBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWeldMeshEdgesToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWeldMeshEdgesToolBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWeldMeshEdgesToolBuilder(UWeldMeshEdgesToolBuilder&&); \
	NO_API UWeldMeshEdgesToolBuilder(const UWeldMeshEdgesToolBuilder&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_24_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UWeldMeshEdgesToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWeldMeshEdgesToolBuilder(UWeldMeshEdgesToolBuilder&&); \
	NO_API UWeldMeshEdgesToolBuilder(const UWeldMeshEdgesToolBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWeldMeshEdgesToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWeldMeshEdgesToolBuilder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UWeldMeshEdgesToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_24_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_21_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_24_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_24_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_24_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_24_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_24_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_24_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UWeldMeshEdgesToolBuilder>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_37_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_37_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_37_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_37_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUWeldMeshEdgesTool(); \
	friend struct Z_Construct_UClass_UWeldMeshEdgesTool_Statics; \
public: \
	DECLARE_CLASS(UWeldMeshEdgesTool, USingleSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UWeldMeshEdgesTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_37_INCLASS \
private: \
	static void StaticRegisterNativesUWeldMeshEdgesTool(); \
	friend struct Z_Construct_UClass_UWeldMeshEdgesTool_Statics; \
public: \
	DECLARE_CLASS(UWeldMeshEdgesTool, USingleSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UWeldMeshEdgesTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_37_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UWeldMeshEdgesTool(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UWeldMeshEdgesTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWeldMeshEdgesTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWeldMeshEdgesTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWeldMeshEdgesTool(UWeldMeshEdgesTool&&); \
	NO_API UWeldMeshEdgesTool(const UWeldMeshEdgesTool&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_37_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWeldMeshEdgesTool(UWeldMeshEdgesTool&&); \
	NO_API UWeldMeshEdgesTool(const UWeldMeshEdgesTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWeldMeshEdgesTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWeldMeshEdgesTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UWeldMeshEdgesTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_37_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Tolerance() { return STRUCT_OFFSET(UWeldMeshEdgesTool, Tolerance); } \
	FORCEINLINE static uint32 __PPO__bOnlyUnique() { return STRUCT_OFFSET(UWeldMeshEdgesTool, bOnlyUnique); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_34_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_37_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_37_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_37_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_37_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_37_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_37_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_37_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_37_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_37_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_37_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_37_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h_37_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UWeldMeshEdgesTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_WeldMeshEdgesTool_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
