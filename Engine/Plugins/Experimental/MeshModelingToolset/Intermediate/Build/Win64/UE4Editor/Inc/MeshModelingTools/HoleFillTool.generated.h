// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLS_HoleFillTool_generated_h
#error "HoleFillTool.generated.h already included, missing '#pragma once' in HoleFillTool.h"
#endif
#define MESHMODELINGTOOLS_HoleFillTool_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_31_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_31_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_31_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_31_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUHoleFillToolBuilder(); \
	friend struct Z_Construct_UClass_UHoleFillToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UHoleFillToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UHoleFillToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_31_INCLASS \
private: \
	static void StaticRegisterNativesUHoleFillToolBuilder(); \
	friend struct Z_Construct_UClass_UHoleFillToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UHoleFillToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UHoleFillToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_31_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UHoleFillToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UHoleFillToolBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHoleFillToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHoleFillToolBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHoleFillToolBuilder(UHoleFillToolBuilder&&); \
	NO_API UHoleFillToolBuilder(const UHoleFillToolBuilder&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_31_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UHoleFillToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHoleFillToolBuilder(UHoleFillToolBuilder&&); \
	NO_API UHoleFillToolBuilder(const UHoleFillToolBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHoleFillToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHoleFillToolBuilder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UHoleFillToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_31_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_28_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_31_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_31_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_31_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_31_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_31_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_31_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_31_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_31_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_31_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_31_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_31_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_31_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UHoleFillToolBuilder>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_45_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_45_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_45_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_45_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSmoothHoleFillProperties(); \
	friend struct Z_Construct_UClass_USmoothHoleFillProperties_Statics; \
public: \
	DECLARE_CLASS(USmoothHoleFillProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(USmoothHoleFillProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_45_INCLASS \
private: \
	static void StaticRegisterNativesUSmoothHoleFillProperties(); \
	friend struct Z_Construct_UClass_USmoothHoleFillProperties_Statics; \
public: \
	DECLARE_CLASS(USmoothHoleFillProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(USmoothHoleFillProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_45_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USmoothHoleFillProperties(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USmoothHoleFillProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USmoothHoleFillProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USmoothHoleFillProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USmoothHoleFillProperties(USmoothHoleFillProperties&&); \
	NO_API USmoothHoleFillProperties(const USmoothHoleFillProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_45_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USmoothHoleFillProperties(USmoothHoleFillProperties&&); \
	NO_API USmoothHoleFillProperties(const USmoothHoleFillProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USmoothHoleFillProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USmoothHoleFillProperties); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(USmoothHoleFillProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_45_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_42_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_45_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_45_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_45_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_45_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_45_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_45_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_45_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_45_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_45_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_45_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_45_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_45_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class USmoothHoleFillProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_113_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_113_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_113_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_113_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUHoleFillToolProperties(); \
	friend struct Z_Construct_UClass_UHoleFillToolProperties_Statics; \
public: \
	DECLARE_CLASS(UHoleFillToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UHoleFillToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_113_INCLASS \
private: \
	static void StaticRegisterNativesUHoleFillToolProperties(); \
	friend struct Z_Construct_UClass_UHoleFillToolProperties_Statics; \
public: \
	DECLARE_CLASS(UHoleFillToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UHoleFillToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_113_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UHoleFillToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UHoleFillToolProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHoleFillToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHoleFillToolProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHoleFillToolProperties(UHoleFillToolProperties&&); \
	NO_API UHoleFillToolProperties(const UHoleFillToolProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_113_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UHoleFillToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHoleFillToolProperties(UHoleFillToolProperties&&); \
	NO_API UHoleFillToolProperties(const UHoleFillToolProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHoleFillToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHoleFillToolProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UHoleFillToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_113_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_110_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_113_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_113_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_113_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_113_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_113_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_113_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_113_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_113_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_113_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_113_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_113_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_113_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UHoleFillToolProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_137_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_137_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execClear); \
	DECLARE_FUNCTION(execSelectAll);


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_137_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execClear); \
	DECLARE_FUNCTION(execSelectAll);


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_137_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUHoleFillToolActions(); \
	friend struct Z_Construct_UClass_UHoleFillToolActions_Statics; \
public: \
	DECLARE_CLASS(UHoleFillToolActions, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UHoleFillToolActions)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_137_INCLASS \
private: \
	static void StaticRegisterNativesUHoleFillToolActions(); \
	friend struct Z_Construct_UClass_UHoleFillToolActions_Statics; \
public: \
	DECLARE_CLASS(UHoleFillToolActions, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UHoleFillToolActions)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_137_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UHoleFillToolActions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UHoleFillToolActions) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHoleFillToolActions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHoleFillToolActions); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHoleFillToolActions(UHoleFillToolActions&&); \
	NO_API UHoleFillToolActions(const UHoleFillToolActions&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_137_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UHoleFillToolActions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHoleFillToolActions(UHoleFillToolActions&&); \
	NO_API UHoleFillToolActions(const UHoleFillToolActions&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHoleFillToolActions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHoleFillToolActions); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UHoleFillToolActions)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_137_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_134_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_137_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_137_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_137_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_137_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_137_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_137_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_137_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_137_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_137_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_137_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_137_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_137_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UHoleFillToolActions>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_167_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_167_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_167_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_167_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUHoleFillStatisticsProperties(); \
	friend struct Z_Construct_UClass_UHoleFillStatisticsProperties_Statics; \
public: \
	DECLARE_CLASS(UHoleFillStatisticsProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UHoleFillStatisticsProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_167_INCLASS \
private: \
	static void StaticRegisterNativesUHoleFillStatisticsProperties(); \
	friend struct Z_Construct_UClass_UHoleFillStatisticsProperties_Statics; \
public: \
	DECLARE_CLASS(UHoleFillStatisticsProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UHoleFillStatisticsProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_167_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UHoleFillStatisticsProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UHoleFillStatisticsProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHoleFillStatisticsProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHoleFillStatisticsProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHoleFillStatisticsProperties(UHoleFillStatisticsProperties&&); \
	NO_API UHoleFillStatisticsProperties(const UHoleFillStatisticsProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_167_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UHoleFillStatisticsProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHoleFillStatisticsProperties(UHoleFillStatisticsProperties&&); \
	NO_API UHoleFillStatisticsProperties(const UHoleFillStatisticsProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHoleFillStatisticsProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHoleFillStatisticsProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UHoleFillStatisticsProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_167_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_164_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_167_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_167_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_167_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_167_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_167_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_167_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_167_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_167_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_167_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_167_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_167_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_167_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UHoleFillStatisticsProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_198_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_198_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_198_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_198_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUHoleFillOperatorFactory(); \
	friend struct Z_Construct_UClass_UHoleFillOperatorFactory_Statics; \
public: \
	DECLARE_CLASS(UHoleFillOperatorFactory, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UHoleFillOperatorFactory)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_198_INCLASS \
private: \
	static void StaticRegisterNativesUHoleFillOperatorFactory(); \
	friend struct Z_Construct_UClass_UHoleFillOperatorFactory_Statics; \
public: \
	DECLARE_CLASS(UHoleFillOperatorFactory, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UHoleFillOperatorFactory)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_198_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UHoleFillOperatorFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UHoleFillOperatorFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHoleFillOperatorFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHoleFillOperatorFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHoleFillOperatorFactory(UHoleFillOperatorFactory&&); \
	NO_API UHoleFillOperatorFactory(const UHoleFillOperatorFactory&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_198_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UHoleFillOperatorFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHoleFillOperatorFactory(UHoleFillOperatorFactory&&); \
	NO_API UHoleFillOperatorFactory(const UHoleFillOperatorFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHoleFillOperatorFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHoleFillOperatorFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UHoleFillOperatorFactory)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_198_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_195_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_198_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_198_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_198_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_198_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_198_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_198_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_198_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_198_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_198_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_198_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_198_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_198_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UHoleFillOperatorFactory>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_216_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_216_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_216_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_216_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUHoleFillTool(); \
	friend struct Z_Construct_UClass_UHoleFillTool_Statics; \
public: \
	DECLARE_CLASS(UHoleFillTool, USingleSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UHoleFillTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_216_INCLASS \
private: \
	static void StaticRegisterNativesUHoleFillTool(); \
	friend struct Z_Construct_UClass_UHoleFillTool_Statics; \
public: \
	DECLARE_CLASS(UHoleFillTool, USingleSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UHoleFillTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_216_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UHoleFillTool(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UHoleFillTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHoleFillTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHoleFillTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHoleFillTool(UHoleFillTool&&); \
	NO_API UHoleFillTool(const UHoleFillTool&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_216_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UHoleFillTool() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHoleFillTool(UHoleFillTool&&); \
	NO_API UHoleFillTool(const UHoleFillTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHoleFillTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHoleFillTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UHoleFillTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_216_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SmoothHoleFillProperties() { return STRUCT_OFFSET(UHoleFillTool, SmoothHoleFillProperties); } \
	FORCEINLINE static uint32 __PPO__Properties() { return STRUCT_OFFSET(UHoleFillTool, Properties); } \
	FORCEINLINE static uint32 __PPO__Actions() { return STRUCT_OFFSET(UHoleFillTool, Actions); } \
	FORCEINLINE static uint32 __PPO__Statistics() { return STRUCT_OFFSET(UHoleFillTool, Statistics); } \
	FORCEINLINE static uint32 __PPO__Preview() { return STRUCT_OFFSET(UHoleFillTool, Preview); } \
	FORCEINLINE static uint32 __PPO__SelectionMechanic() { return STRUCT_OFFSET(UHoleFillTool, SelectionMechanic); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_213_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_216_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_216_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_216_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_216_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_216_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_216_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_216_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_216_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_216_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_216_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_216_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h_216_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UHoleFillTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_HoleFillTool_h


#define FOREACH_ENUM_EHOLEFILLTOOLACTIONS(op) \
	op(EHoleFillToolActions::NoAction) \
	op(EHoleFillToolActions::SelectAll) \
	op(EHoleFillToolActions::ClearSelection) 

enum class EHoleFillToolActions;
template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EHoleFillToolActions>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
