// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ModelingComponents/Public/Drawing/UVLayoutPreview.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUVLayoutPreview() {}
// Cross Module References
	MODELINGCOMPONENTS_API UEnum* Z_Construct_UEnum_ModelingComponents_EUVLayoutPreviewSide();
	UPackage* Z_Construct_UPackage__Script_ModelingComponents();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UUVLayoutPreviewProperties_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UUVLayoutPreviewProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UUVLayoutPreview_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UUVLayoutPreview();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPreviewMesh_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UTriangleSetComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
// End Cross Module References
	static UEnum* EUVLayoutPreviewSide_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ModelingComponents_EUVLayoutPreviewSide, Z_Construct_UPackage__Script_ModelingComponents(), TEXT("EUVLayoutPreviewSide"));
		}
		return Singleton;
	}
	template<> MODELINGCOMPONENTS_API UEnum* StaticEnum<EUVLayoutPreviewSide>()
	{
		return EUVLayoutPreviewSide_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EUVLayoutPreviewSide(EUVLayoutPreviewSide_StaticEnum, TEXT("/Script/ModelingComponents"), TEXT("EUVLayoutPreviewSide"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ModelingComponents_EUVLayoutPreviewSide_Hash() { return 3558164379U; }
	UEnum* Z_Construct_UEnum_ModelingComponents_EUVLayoutPreviewSide()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ModelingComponents();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EUVLayoutPreviewSide"), 0, Get_Z_Construct_UEnum_ModelingComponents_EUVLayoutPreviewSide_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EUVLayoutPreviewSide::Left", (int64)EUVLayoutPreviewSide::Left },
				{ "EUVLayoutPreviewSide::Right", (int64)EUVLayoutPreviewSide::Right },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/**\n * Where should in-viewport UVLayoutPreview be shown, relative to target object\n */" },
				{ "Left.Name", "EUVLayoutPreviewSide::Left" },
				{ "ModuleRelativePath", "Public/Drawing/UVLayoutPreview.h" },
				{ "Right.Name", "EUVLayoutPreviewSide::Right" },
				{ "ToolTip", "Where should in-viewport UVLayoutPreview be shown, relative to target object" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ModelingComponents,
				nullptr,
				"EUVLayoutPreviewSide",
				"EUVLayoutPreviewSide",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UUVLayoutPreviewProperties::StaticRegisterNativesUUVLayoutPreviewProperties()
	{
	}
	UClass* Z_Construct_UClass_UUVLayoutPreviewProperties_NoRegister()
	{
		return UUVLayoutPreviewProperties::StaticClass();
	}
	struct Z_Construct_UClass_UUVLayoutPreviewProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bVisible_MetaData[];
#endif
		static void NewProp_bVisible_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bVisible;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScaleFactor_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScaleFactor;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_WhichSide_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WhichSide_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_WhichSide;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowWireframe_MetaData[];
#endif
		static void NewProp_bShowWireframe_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowWireframe;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Shift_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Shift;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUVLayoutPreviewProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_ModelingComponents,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVLayoutPreviewProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Visualization settings for UV layout preview\n */" },
		{ "IncludePath", "Drawing/UVLayoutPreview.h" },
		{ "ModuleRelativePath", "Public/Drawing/UVLayoutPreview.h" },
		{ "ToolTip", "Visualization settings for UV layout preview" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVLayoutPreviewProperties_Statics::NewProp_bVisible_MetaData[] = {
		{ "Category", "UVLayoutPreview" },
		{ "Comment", "/** Should be UV Layout be shown */" },
		{ "ModuleRelativePath", "Public/Drawing/UVLayoutPreview.h" },
		{ "ToolTip", "Should be UV Layout be shown" },
	};
#endif
	void Z_Construct_UClass_UUVLayoutPreviewProperties_Statics::NewProp_bVisible_SetBit(void* Obj)
	{
		((UUVLayoutPreviewProperties*)Obj)->bVisible = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UUVLayoutPreviewProperties_Statics::NewProp_bVisible = { "bVisible", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UUVLayoutPreviewProperties), &Z_Construct_UClass_UUVLayoutPreviewProperties_Statics::NewProp_bVisible_SetBit, METADATA_PARAMS(Z_Construct_UClass_UUVLayoutPreviewProperties_Statics::NewProp_bVisible_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUVLayoutPreviewProperties_Statics::NewProp_bVisible_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVLayoutPreviewProperties_Statics::NewProp_ScaleFactor_MetaData[] = {
		{ "Category", "UVLayoutPreview" },
		{ "ClampMax", "1000" },
		{ "ClampMin", "0.0001" },
		{ "Comment", "/** World-space scaling factor on the UV Layout */" },
		{ "ModuleRelativePath", "Public/Drawing/UVLayoutPreview.h" },
		{ "ToolTip", "World-space scaling factor on the UV Layout" },
		{ "UIMax", "10.0" },
		{ "UIMin", "0.1" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UUVLayoutPreviewProperties_Statics::NewProp_ScaleFactor = { "ScaleFactor", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUVLayoutPreviewProperties, ScaleFactor), METADATA_PARAMS(Z_Construct_UClass_UUVLayoutPreviewProperties_Statics::NewProp_ScaleFactor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUVLayoutPreviewProperties_Statics::NewProp_ScaleFactor_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UUVLayoutPreviewProperties_Statics::NewProp_WhichSide_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVLayoutPreviewProperties_Statics::NewProp_WhichSide_MetaData[] = {
		{ "Category", "UVLayoutPreview" },
		{ "Comment", "/** Where should the UV layout be positioned relative to the target object, relative to camera */" },
		{ "ModuleRelativePath", "Public/Drawing/UVLayoutPreview.h" },
		{ "ToolTip", "Where should the UV layout be positioned relative to the target object, relative to camera" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UUVLayoutPreviewProperties_Statics::NewProp_WhichSide = { "WhichSide", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUVLayoutPreviewProperties, WhichSide), Z_Construct_UEnum_ModelingComponents_EUVLayoutPreviewSide, METADATA_PARAMS(Z_Construct_UClass_UUVLayoutPreviewProperties_Statics::NewProp_WhichSide_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUVLayoutPreviewProperties_Statics::NewProp_WhichSide_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVLayoutPreviewProperties_Statics::NewProp_bShowWireframe_MetaData[] = {
		{ "Category", "UVLayoutPreview" },
		{ "Comment", "/** If true, wireframe is shown for the UV layout */" },
		{ "ModuleRelativePath", "Public/Drawing/UVLayoutPreview.h" },
		{ "ToolTip", "If true, wireframe is shown for the UV layout" },
	};
#endif
	void Z_Construct_UClass_UUVLayoutPreviewProperties_Statics::NewProp_bShowWireframe_SetBit(void* Obj)
	{
		((UUVLayoutPreviewProperties*)Obj)->bShowWireframe = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UUVLayoutPreviewProperties_Statics::NewProp_bShowWireframe = { "bShowWireframe", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UUVLayoutPreviewProperties), &Z_Construct_UClass_UUVLayoutPreviewProperties_Statics::NewProp_bShowWireframe_SetBit, METADATA_PARAMS(Z_Construct_UClass_UUVLayoutPreviewProperties_Statics::NewProp_bShowWireframe_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUVLayoutPreviewProperties_Statics::NewProp_bShowWireframe_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVLayoutPreviewProperties_Statics::NewProp_Shift_MetaData[] = {
		{ "Category", "UVLayoutPreview" },
		{ "ModuleRelativePath", "Public/Drawing/UVLayoutPreview.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UUVLayoutPreviewProperties_Statics::NewProp_Shift = { "Shift", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUVLayoutPreviewProperties, Shift), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UClass_UUVLayoutPreviewProperties_Statics::NewProp_Shift_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUVLayoutPreviewProperties_Statics::NewProp_Shift_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UUVLayoutPreviewProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVLayoutPreviewProperties_Statics::NewProp_bVisible,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVLayoutPreviewProperties_Statics::NewProp_ScaleFactor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVLayoutPreviewProperties_Statics::NewProp_WhichSide_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVLayoutPreviewProperties_Statics::NewProp_WhichSide,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVLayoutPreviewProperties_Statics::NewProp_bShowWireframe,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVLayoutPreviewProperties_Statics::NewProp_Shift,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUVLayoutPreviewProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUVLayoutPreviewProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUVLayoutPreviewProperties_Statics::ClassParams = {
		&UUVLayoutPreviewProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UUVLayoutPreviewProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UUVLayoutPreviewProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UUVLayoutPreviewProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUVLayoutPreviewProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUVLayoutPreviewProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUVLayoutPreviewProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUVLayoutPreviewProperties, 2871559928);
	template<> MODELINGCOMPONENTS_API UClass* StaticClass<UUVLayoutPreviewProperties>()
	{
		return UUVLayoutPreviewProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUVLayoutPreviewProperties(Z_Construct_UClass_UUVLayoutPreviewProperties, &UUVLayoutPreviewProperties::StaticClass, TEXT("/Script/ModelingComponents"), TEXT("UUVLayoutPreviewProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUVLayoutPreviewProperties);
	void UUVLayoutPreview::StaticRegisterNativesUUVLayoutPreview()
	{
	}
	UClass* Z_Construct_UClass_UUVLayoutPreview_NoRegister()
	{
		return UUVLayoutPreview::StaticClass();
	}
	struct Z_Construct_UClass_UUVLayoutPreview_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Settings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Settings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviewMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviewMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TriangleComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TriangleComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowBackingRectangle_MetaData[];
#endif
		static void NewProp_bShowBackingRectangle_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowBackingRectangle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BackingRectangleMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BackingRectangleMaterial;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUVLayoutPreview_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_ModelingComponents,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVLayoutPreview_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * UUVLayoutPreview is a utility object that creates and manages a 3D plane on which a UV layout\n * for a 3D mesh is rendered. The UV layout\n */" },
		{ "IncludePath", "Drawing/UVLayoutPreview.h" },
		{ "ModuleRelativePath", "Public/Drawing/UVLayoutPreview.h" },
		{ "ToolTip", "UUVLayoutPreview is a utility object that creates and manages a 3D plane on which a UV layout\nfor a 3D mesh is rendered. The UV layout" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVLayoutPreview_Statics::NewProp_Settings_MetaData[] = {
		{ "Comment", "/** Visualization settings */" },
		{ "ModuleRelativePath", "Public/Drawing/UVLayoutPreview.h" },
		{ "ToolTip", "Visualization settings" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUVLayoutPreview_Statics::NewProp_Settings = { "Settings", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUVLayoutPreview, Settings), Z_Construct_UClass_UUVLayoutPreviewProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUVLayoutPreview_Statics::NewProp_Settings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUVLayoutPreview_Statics::NewProp_Settings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVLayoutPreview_Statics::NewProp_PreviewMesh_MetaData[] = {
		{ "Comment", "/** PreviewMesh is initialized with a copy of an input mesh with UVs mapped to position, ie such that (X,Y,Z) = (U,V,0) */" },
		{ "ModuleRelativePath", "Public/Drawing/UVLayoutPreview.h" },
		{ "ToolTip", "PreviewMesh is initialized with a copy of an input mesh with UVs mapped to position, ie such that (X,Y,Z) = (U,V,0)" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUVLayoutPreview_Statics::NewProp_PreviewMesh = { "PreviewMesh", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUVLayoutPreview, PreviewMesh), Z_Construct_UClass_UPreviewMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUVLayoutPreview_Statics::NewProp_PreviewMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUVLayoutPreview_Statics::NewProp_PreviewMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVLayoutPreview_Statics::NewProp_TriangleComponent_MetaData[] = {
		{ "Comment", "/** Set of additional triangles to draw, eg for backing rectangle, etc */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Drawing/UVLayoutPreview.h" },
		{ "ToolTip", "Set of additional triangles to draw, eg for backing rectangle, etc" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUVLayoutPreview_Statics::NewProp_TriangleComponent = { "TriangleComponent", nullptr, (EPropertyFlags)0x0010000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUVLayoutPreview, TriangleComponent), Z_Construct_UClass_UTriangleSetComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUVLayoutPreview_Statics::NewProp_TriangleComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUVLayoutPreview_Statics::NewProp_TriangleComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVLayoutPreview_Statics::NewProp_bShowBackingRectangle_MetaData[] = {
		{ "Comment", "/** Configure whether the backing rectangle should be shown */" },
		{ "ModuleRelativePath", "Public/Drawing/UVLayoutPreview.h" },
		{ "ToolTip", "Configure whether the backing rectangle should be shown" },
	};
#endif
	void Z_Construct_UClass_UUVLayoutPreview_Statics::NewProp_bShowBackingRectangle_SetBit(void* Obj)
	{
		((UUVLayoutPreview*)Obj)->bShowBackingRectangle = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UUVLayoutPreview_Statics::NewProp_bShowBackingRectangle = { "bShowBackingRectangle", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UUVLayoutPreview), &Z_Construct_UClass_UUVLayoutPreview_Statics::NewProp_bShowBackingRectangle_SetBit, METADATA_PARAMS(Z_Construct_UClass_UUVLayoutPreview_Statics::NewProp_bShowBackingRectangle_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUVLayoutPreview_Statics::NewProp_bShowBackingRectangle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVLayoutPreview_Statics::NewProp_BackingRectangleMaterial_MetaData[] = {
		{ "Comment", "/** Configure the backing rectangle material */" },
		{ "ModuleRelativePath", "Public/Drawing/UVLayoutPreview.h" },
		{ "ToolTip", "Configure the backing rectangle material" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUVLayoutPreview_Statics::NewProp_BackingRectangleMaterial = { "BackingRectangleMaterial", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUVLayoutPreview, BackingRectangleMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUVLayoutPreview_Statics::NewProp_BackingRectangleMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUVLayoutPreview_Statics::NewProp_BackingRectangleMaterial_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UUVLayoutPreview_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVLayoutPreview_Statics::NewProp_Settings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVLayoutPreview_Statics::NewProp_PreviewMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVLayoutPreview_Statics::NewProp_TriangleComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVLayoutPreview_Statics::NewProp_bShowBackingRectangle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVLayoutPreview_Statics::NewProp_BackingRectangleMaterial,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUVLayoutPreview_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUVLayoutPreview>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUVLayoutPreview_Statics::ClassParams = {
		&UUVLayoutPreview::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UUVLayoutPreview_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UUVLayoutPreview_Statics::PropPointers),
		0,
		0x009000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UUVLayoutPreview_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUVLayoutPreview_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUVLayoutPreview()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUVLayoutPreview_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUVLayoutPreview, 1632744521);
	template<> MODELINGCOMPONENTS_API UClass* StaticClass<UUVLayoutPreview>()
	{
		return UUVLayoutPreview::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUVLayoutPreview(Z_Construct_UClass_UUVLayoutPreview, &UUVLayoutPreview::StaticClass, TEXT("/Script/ModelingComponents"), TEXT("UUVLayoutPreview"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUVLayoutPreview);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
