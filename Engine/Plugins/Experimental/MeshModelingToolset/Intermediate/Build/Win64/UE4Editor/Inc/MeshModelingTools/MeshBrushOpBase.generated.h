// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLS_MeshBrushOpBase_generated_h
#error "MeshBrushOpBase.generated.h already included, missing '#pragma once' in MeshBrushOpBase.h"
#endif
#define MESHMODELINGTOOLS_MeshBrushOpBase_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshBrushOpBase_h_80_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshBrushOpBase_h_80_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshBrushOpBase_h_80_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshBrushOpBase_h_80_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshSculptBrushOpProps(); \
	friend struct Z_Construct_UClass_UMeshSculptBrushOpProps_Statics; \
public: \
	DECLARE_CLASS(UMeshSculptBrushOpProps, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMeshSculptBrushOpProps)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshBrushOpBase_h_80_INCLASS \
private: \
	static void StaticRegisterNativesUMeshSculptBrushOpProps(); \
	friend struct Z_Construct_UClass_UMeshSculptBrushOpProps_Statics; \
public: \
	DECLARE_CLASS(UMeshSculptBrushOpProps, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMeshSculptBrushOpProps)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshBrushOpBase_h_80_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshSculptBrushOpProps(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshSculptBrushOpProps) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshSculptBrushOpProps); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshSculptBrushOpProps); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshSculptBrushOpProps(UMeshSculptBrushOpProps&&); \
	NO_API UMeshSculptBrushOpProps(const UMeshSculptBrushOpProps&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshBrushOpBase_h_80_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshSculptBrushOpProps(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshSculptBrushOpProps(UMeshSculptBrushOpProps&&); \
	NO_API UMeshSculptBrushOpProps(const UMeshSculptBrushOpProps&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshSculptBrushOpProps); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshSculptBrushOpProps); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshSculptBrushOpProps)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshBrushOpBase_h_80_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshBrushOpBase_h_77_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshBrushOpBase_h_80_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshBrushOpBase_h_80_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshBrushOpBase_h_80_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshBrushOpBase_h_80_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshBrushOpBase_h_80_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshBrushOpBase_h_80_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshBrushOpBase_h_80_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshBrushOpBase_h_80_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshBrushOpBase_h_80_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshBrushOpBase_h_80_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshBrushOpBase_h_80_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshBrushOpBase_h_80_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UMeshSculptBrushOpProps>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Sculpting_MeshBrushOpBase_h


#define FOREACH_ENUM_EPLANEBRUSHSIDEMODE(op) \
	op(EPlaneBrushSideMode::BothSides) \
	op(EPlaneBrushSideMode::PushDown) \
	op(EPlaneBrushSideMode::PullTowards) 

enum class EPlaneBrushSideMode : uint8;
template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EPlaneBrushSideMode>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
