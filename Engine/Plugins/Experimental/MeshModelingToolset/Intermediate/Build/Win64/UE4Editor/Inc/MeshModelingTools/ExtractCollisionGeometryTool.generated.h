// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLS_ExtractCollisionGeometryTool_generated_h
#error "ExtractCollisionGeometryTool.generated.h already included, missing '#pragma once' in ExtractCollisionGeometryTool.h"
#endif
#define MESHMODELINGTOOLS_ExtractCollisionGeometryTool_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_18_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_18_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUExtractCollisionGeometryToolBuilder(); \
	friend struct Z_Construct_UClass_UExtractCollisionGeometryToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UExtractCollisionGeometryToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UExtractCollisionGeometryToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUExtractCollisionGeometryToolBuilder(); \
	friend struct Z_Construct_UClass_UExtractCollisionGeometryToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UExtractCollisionGeometryToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UExtractCollisionGeometryToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UExtractCollisionGeometryToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UExtractCollisionGeometryToolBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UExtractCollisionGeometryToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UExtractCollisionGeometryToolBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UExtractCollisionGeometryToolBuilder(UExtractCollisionGeometryToolBuilder&&); \
	NO_API UExtractCollisionGeometryToolBuilder(const UExtractCollisionGeometryToolBuilder&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UExtractCollisionGeometryToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UExtractCollisionGeometryToolBuilder(UExtractCollisionGeometryToolBuilder&&); \
	NO_API UExtractCollisionGeometryToolBuilder(const UExtractCollisionGeometryToolBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UExtractCollisionGeometryToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UExtractCollisionGeometryToolBuilder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UExtractCollisionGeometryToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_15_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_18_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_18_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_18_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_18_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UExtractCollisionGeometryToolBuilder>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_36_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_36_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_36_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_36_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUExtractCollisionGeometryTool(); \
	friend struct Z_Construct_UClass_UExtractCollisionGeometryTool_Statics; \
public: \
	DECLARE_CLASS(UExtractCollisionGeometryTool, USingleSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UExtractCollisionGeometryTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_36_INCLASS \
private: \
	static void StaticRegisterNativesUExtractCollisionGeometryTool(); \
	friend struct Z_Construct_UClass_UExtractCollisionGeometryTool_Statics; \
public: \
	DECLARE_CLASS(UExtractCollisionGeometryTool, USingleSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UExtractCollisionGeometryTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_36_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UExtractCollisionGeometryTool(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UExtractCollisionGeometryTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UExtractCollisionGeometryTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UExtractCollisionGeometryTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UExtractCollisionGeometryTool(UExtractCollisionGeometryTool&&); \
	NO_API UExtractCollisionGeometryTool(const UExtractCollisionGeometryTool&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_36_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UExtractCollisionGeometryTool() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UExtractCollisionGeometryTool(UExtractCollisionGeometryTool&&); \
	NO_API UExtractCollisionGeometryTool(const UExtractCollisionGeometryTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UExtractCollisionGeometryTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UExtractCollisionGeometryTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UExtractCollisionGeometryTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_36_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__VizSettings() { return STRUCT_OFFSET(UExtractCollisionGeometryTool, VizSettings); } \
	FORCEINLINE static uint32 __PPO__ObjectProps() { return STRUCT_OFFSET(UExtractCollisionGeometryTool, ObjectProps); } \
	FORCEINLINE static uint32 __PPO__PreviewElements() { return STRUCT_OFFSET(UExtractCollisionGeometryTool, PreviewElements); } \
	FORCEINLINE static uint32 __PPO__PreviewMesh() { return STRUCT_OFFSET(UExtractCollisionGeometryTool, PreviewMesh); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_33_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_36_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_36_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_36_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_36_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_36_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_36_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_36_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_36_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_36_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_36_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_36_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h_36_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UExtractCollisionGeometryTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Physics_ExtractCollisionGeometryTool_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
