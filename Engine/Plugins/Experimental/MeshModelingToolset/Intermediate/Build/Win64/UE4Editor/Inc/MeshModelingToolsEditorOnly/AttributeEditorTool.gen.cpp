// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingToolsEditorOnly/Public/AttributeEditorTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAttributeEditorTool() {}
// Cross Module References
	MESHMODELINGTOOLSEDITORONLY_API UEnum* Z_Construct_UEnum_MeshModelingToolsEditorOnly_EAttributeEditorToolActions();
	UPackage* Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly();
	MESHMODELINGTOOLSEDITORONLY_API UEnum* Z_Construct_UEnum_MeshModelingToolsEditorOnly_EAttributeEditorAttribType();
	MESHMODELINGTOOLSEDITORONLY_API UEnum* Z_Construct_UEnum_MeshModelingToolsEditorOnly_EAttributeEditorElementType();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UAttributeEditorToolBuilder_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UAttributeEditorToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UAttributeEditorAttribProperties_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UAttributeEditorAttribProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UAttributeEditorActionPropertySet_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UAttributeEditorActionPropertySet();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UAttributeEditorNormalsActions_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UAttributeEditorNormalsActions();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UAttributeEditorUVActions_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UAttributeEditorUVActions();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UAttributeEditorNewAttributeActions_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UAttributeEditorNewAttributeActions();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UAttributeEditorModifyAttributeActions_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UAttributeEditorModifyAttributeActions();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UAttributeEditorCopyAttributeActions_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UAttributeEditorCopyAttributeActions();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UAttributeEditorTool_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UAttributeEditorTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UMultiSelectionTool();
// End Cross Module References
	static UEnum* EAttributeEditorToolActions_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingToolsEditorOnly_EAttributeEditorToolActions, Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly(), TEXT("EAttributeEditorToolActions"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLSEDITORONLY_API UEnum* StaticEnum<EAttributeEditorToolActions>()
	{
		return EAttributeEditorToolActions_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EAttributeEditorToolActions(EAttributeEditorToolActions_StaticEnum, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("EAttributeEditorToolActions"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingToolsEditorOnly_EAttributeEditorToolActions_Hash() { return 1683666230U; }
	UEnum* Z_Construct_UEnum_MeshModelingToolsEditorOnly_EAttributeEditorToolActions()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EAttributeEditorToolActions"), 0, Get_Z_Construct_UEnum_MeshModelingToolsEditorOnly_EAttributeEditorToolActions_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EAttributeEditorToolActions::NoAction", (int64)EAttributeEditorToolActions::NoAction },
				{ "EAttributeEditorToolActions::ClearNormals", (int64)EAttributeEditorToolActions::ClearNormals },
				{ "EAttributeEditorToolActions::ClearSelectedUVs", (int64)EAttributeEditorToolActions::ClearSelectedUVs },
				{ "EAttributeEditorToolActions::ClearAllUVs", (int64)EAttributeEditorToolActions::ClearAllUVs },
				{ "EAttributeEditorToolActions::AddAttribute", (int64)EAttributeEditorToolActions::AddAttribute },
				{ "EAttributeEditorToolActions::AddWeightMapLayer", (int64)EAttributeEditorToolActions::AddWeightMapLayer },
				{ "EAttributeEditorToolActions::AddPolyGroupLayer", (int64)EAttributeEditorToolActions::AddPolyGroupLayer },
				{ "EAttributeEditorToolActions::DeleteAttribute", (int64)EAttributeEditorToolActions::DeleteAttribute },
				{ "EAttributeEditorToolActions::ClearAttribute", (int64)EAttributeEditorToolActions::ClearAttribute },
				{ "EAttributeEditorToolActions::CopyAttributeFromTo", (int64)EAttributeEditorToolActions::CopyAttributeFromTo },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "AddAttribute.Name", "EAttributeEditorToolActions::AddAttribute" },
				{ "AddPolyGroupLayer.Name", "EAttributeEditorToolActions::AddPolyGroupLayer" },
				{ "AddWeightMapLayer.Name", "EAttributeEditorToolActions::AddWeightMapLayer" },
				{ "ClearAllUVs.Name", "EAttributeEditorToolActions::ClearAllUVs" },
				{ "ClearAttribute.Name", "EAttributeEditorToolActions::ClearAttribute" },
				{ "ClearNormals.Name", "EAttributeEditorToolActions::ClearNormals" },
				{ "ClearSelectedUVs.Name", "EAttributeEditorToolActions::ClearSelectedUVs" },
				{ "CopyAttributeFromTo.Name", "EAttributeEditorToolActions::CopyAttributeFromTo" },
				{ "DeleteAttribute.Name", "EAttributeEditorToolActions::DeleteAttribute" },
				{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
				{ "NoAction.Name", "EAttributeEditorToolActions::NoAction" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
				nullptr,
				"EAttributeEditorToolActions",
				"EAttributeEditorToolActions",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EAttributeEditorAttribType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingToolsEditorOnly_EAttributeEditorAttribType, Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly(), TEXT("EAttributeEditorAttribType"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLSEDITORONLY_API UEnum* StaticEnum<EAttributeEditorAttribType>()
	{
		return EAttributeEditorAttribType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EAttributeEditorAttribType(EAttributeEditorAttribType_StaticEnum, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("EAttributeEditorAttribType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingToolsEditorOnly_EAttributeEditorAttribType_Hash() { return 2822353136U; }
	UEnum* Z_Construct_UEnum_MeshModelingToolsEditorOnly_EAttributeEditorAttribType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EAttributeEditorAttribType"), 0, Get_Z_Construct_UEnum_MeshModelingToolsEditorOnly_EAttributeEditorAttribType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EAttributeEditorAttribType::Int32", (int64)EAttributeEditorAttribType::Int32 },
				{ "EAttributeEditorAttribType::Boolean", (int64)EAttributeEditorAttribType::Boolean },
				{ "EAttributeEditorAttribType::Float", (int64)EAttributeEditorAttribType::Float },
				{ "EAttributeEditorAttribType::Vector2", (int64)EAttributeEditorAttribType::Vector2 },
				{ "EAttributeEditorAttribType::Vector3", (int64)EAttributeEditorAttribType::Vector3 },
				{ "EAttributeEditorAttribType::Vector4", (int64)EAttributeEditorAttribType::Vector4 },
				{ "EAttributeEditorAttribType::String", (int64)EAttributeEditorAttribType::String },
				{ "EAttributeEditorAttribType::Unknown", (int64)EAttributeEditorAttribType::Unknown },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Boolean.Name", "EAttributeEditorAttribType::Boolean" },
				{ "Float.Name", "EAttributeEditorAttribType::Float" },
				{ "Int32.Name", "EAttributeEditorAttribType::Int32" },
				{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
				{ "String.Name", "EAttributeEditorAttribType::String" },
				{ "Unknown.Name", "EAttributeEditorAttribType::Unknown" },
				{ "Vector2.Name", "EAttributeEditorAttribType::Vector2" },
				{ "Vector3.Name", "EAttributeEditorAttribType::Vector3" },
				{ "Vector4.Name", "EAttributeEditorAttribType::Vector4" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
				nullptr,
				"EAttributeEditorAttribType",
				"EAttributeEditorAttribType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EAttributeEditorElementType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingToolsEditorOnly_EAttributeEditorElementType, Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly(), TEXT("EAttributeEditorElementType"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLSEDITORONLY_API UEnum* StaticEnum<EAttributeEditorElementType>()
	{
		return EAttributeEditorElementType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EAttributeEditorElementType(EAttributeEditorElementType_StaticEnum, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("EAttributeEditorElementType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingToolsEditorOnly_EAttributeEditorElementType_Hash() { return 56882219U; }
	UEnum* Z_Construct_UEnum_MeshModelingToolsEditorOnly_EAttributeEditorElementType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EAttributeEditorElementType"), 0, Get_Z_Construct_UEnum_MeshModelingToolsEditorOnly_EAttributeEditorElementType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EAttributeEditorElementType::Vertex", (int64)EAttributeEditorElementType::Vertex },
				{ "EAttributeEditorElementType::VertexInstance", (int64)EAttributeEditorElementType::VertexInstance },
				{ "EAttributeEditorElementType::Triangle", (int64)EAttributeEditorElementType::Triangle },
				{ "EAttributeEditorElementType::Polygon", (int64)EAttributeEditorElementType::Polygon },
				{ "EAttributeEditorElementType::Edge", (int64)EAttributeEditorElementType::Edge },
				{ "EAttributeEditorElementType::PolygonGroup", (int64)EAttributeEditorElementType::PolygonGroup },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Edge.Name", "EAttributeEditorElementType::Edge" },
				{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
				{ "Polygon.Name", "EAttributeEditorElementType::Polygon" },
				{ "PolygonGroup.Name", "EAttributeEditorElementType::PolygonGroup" },
				{ "Triangle.Name", "EAttributeEditorElementType::Triangle" },
				{ "Vertex.Name", "EAttributeEditorElementType::Vertex" },
				{ "VertexInstance.Name", "EAttributeEditorElementType::VertexInstance" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
				nullptr,
				"EAttributeEditorElementType",
				"EAttributeEditorElementType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UAttributeEditorToolBuilder::StaticRegisterNativesUAttributeEditorToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UAttributeEditorToolBuilder_NoRegister()
	{
		return UAttributeEditorToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UAttributeEditorToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAttributeEditorToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttributeEditorToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "AttributeEditorTool.h" },
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAttributeEditorToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAttributeEditorToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAttributeEditorToolBuilder_Statics::ClassParams = {
		&UAttributeEditorToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UAttributeEditorToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAttributeEditorToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAttributeEditorToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAttributeEditorToolBuilder, 263974082);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UAttributeEditorToolBuilder>()
	{
		return UAttributeEditorToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAttributeEditorToolBuilder(Z_Construct_UClass_UAttributeEditorToolBuilder, &UAttributeEditorToolBuilder::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UAttributeEditorToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAttributeEditorToolBuilder);
	void UAttributeEditorAttribProperties::StaticRegisterNativesUAttributeEditorAttribProperties()
	{
	}
	UClass* Z_Construct_UClass_UAttributeEditorAttribProperties_NoRegister()
	{
		return UAttributeEditorAttribProperties::StaticClass();
	}
	struct Z_Construct_UClass_UAttributeEditorAttribProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_VertexAttributes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VertexAttributes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_VertexAttributes;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InstanceAttributes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InstanceAttributes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_InstanceAttributes;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_TriangleAttributes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TriangleAttributes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_TriangleAttributes;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PolygonAttributes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PolygonAttributes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PolygonAttributes;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_EdgeAttributes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EdgeAttributes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_EdgeAttributes;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_GroupAttributes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GroupAttributes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_GroupAttributes;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AttributeEditorTool.h" },
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_VertexAttributes_Inner = { "VertexAttributes", nullptr, (EPropertyFlags)0x0000000000020000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_VertexAttributes_MetaData[] = {
		{ "Category", "AttributesInspector" },
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_VertexAttributes = { "VertexAttributes", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAttributeEditorAttribProperties, VertexAttributes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_VertexAttributes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_VertexAttributes_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_InstanceAttributes_Inner = { "InstanceAttributes", nullptr, (EPropertyFlags)0x0000000000020000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_InstanceAttributes_MetaData[] = {
		{ "Category", "AttributesInspector" },
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_InstanceAttributes = { "InstanceAttributes", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAttributeEditorAttribProperties, InstanceAttributes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_InstanceAttributes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_InstanceAttributes_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_TriangleAttributes_Inner = { "TriangleAttributes", nullptr, (EPropertyFlags)0x0000000000020000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_TriangleAttributes_MetaData[] = {
		{ "Category", "AttributesInspector" },
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_TriangleAttributes = { "TriangleAttributes", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAttributeEditorAttribProperties, TriangleAttributes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_TriangleAttributes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_TriangleAttributes_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_PolygonAttributes_Inner = { "PolygonAttributes", nullptr, (EPropertyFlags)0x0000000000020000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_PolygonAttributes_MetaData[] = {
		{ "Category", "AttributesInspector" },
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_PolygonAttributes = { "PolygonAttributes", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAttributeEditorAttribProperties, PolygonAttributes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_PolygonAttributes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_PolygonAttributes_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_EdgeAttributes_Inner = { "EdgeAttributes", nullptr, (EPropertyFlags)0x0000000000020000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_EdgeAttributes_MetaData[] = {
		{ "Category", "AttributesInspector" },
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_EdgeAttributes = { "EdgeAttributes", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAttributeEditorAttribProperties, EdgeAttributes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_EdgeAttributes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_EdgeAttributes_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_GroupAttributes_Inner = { "GroupAttributes", nullptr, (EPropertyFlags)0x0000000000020000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_GroupAttributes_MetaData[] = {
		{ "Category", "AttributesInspector" },
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_GroupAttributes = { "GroupAttributes", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAttributeEditorAttribProperties, GroupAttributes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_GroupAttributes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_GroupAttributes_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_VertexAttributes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_VertexAttributes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_InstanceAttributes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_InstanceAttributes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_TriangleAttributes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_TriangleAttributes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_PolygonAttributes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_PolygonAttributes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_EdgeAttributes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_EdgeAttributes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_GroupAttributes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::NewProp_GroupAttributes,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAttributeEditorAttribProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::ClassParams = {
		&UAttributeEditorAttribProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAttributeEditorAttribProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAttributeEditorAttribProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAttributeEditorAttribProperties, 69287216);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UAttributeEditorAttribProperties>()
	{
		return UAttributeEditorAttribProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAttributeEditorAttribProperties(Z_Construct_UClass_UAttributeEditorAttribProperties, &UAttributeEditorAttribProperties::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UAttributeEditorAttribProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAttributeEditorAttribProperties);
	void UAttributeEditorActionPropertySet::StaticRegisterNativesUAttributeEditorActionPropertySet()
	{
	}
	UClass* Z_Construct_UClass_UAttributeEditorActionPropertySet_NoRegister()
	{
		return UAttributeEditorActionPropertySet::StaticClass();
	}
	struct Z_Construct_UClass_UAttributeEditorActionPropertySet_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAttributeEditorActionPropertySet_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttributeEditorActionPropertySet_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AttributeEditorTool.h" },
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAttributeEditorActionPropertySet_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAttributeEditorActionPropertySet>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAttributeEditorActionPropertySet_Statics::ClassParams = {
		&UAttributeEditorActionPropertySet::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UAttributeEditorActionPropertySet_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorActionPropertySet_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAttributeEditorActionPropertySet()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAttributeEditorActionPropertySet_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAttributeEditorActionPropertySet, 669763724);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UAttributeEditorActionPropertySet>()
	{
		return UAttributeEditorActionPropertySet::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAttributeEditorActionPropertySet(Z_Construct_UClass_UAttributeEditorActionPropertySet, &UAttributeEditorActionPropertySet::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UAttributeEditorActionPropertySet"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAttributeEditorActionPropertySet);
	DEFINE_FUNCTION(UAttributeEditorNormalsActions::execResetHardNormals)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ResetHardNormals();
		P_NATIVE_END;
	}
	void UAttributeEditorNormalsActions::StaticRegisterNativesUAttributeEditorNormalsActions()
	{
		UClass* Class = UAttributeEditorNormalsActions::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ResetHardNormals", &UAttributeEditorNormalsActions::execResetHardNormals },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UAttributeEditorNormalsActions_ResetHardNormals_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAttributeEditorNormalsActions_ResetHardNormals_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "Normals" },
		{ "Comment", "/** Remove any hard edges / split normals, setting all normals to a single vertex normal */" },
		{ "DisplayPriority", "1" },
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
		{ "ToolTip", "Remove any hard edges / split normals, setting all normals to a single vertex normal" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAttributeEditorNormalsActions_ResetHardNormals_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAttributeEditorNormalsActions, nullptr, "ResetHardNormals", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAttributeEditorNormalsActions_ResetHardNormals_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttributeEditorNormalsActions_ResetHardNormals_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAttributeEditorNormalsActions_ResetHardNormals()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAttributeEditorNormalsActions_ResetHardNormals_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UAttributeEditorNormalsActions_NoRegister()
	{
		return UAttributeEditorNormalsActions::StaticClass();
	}
	struct Z_Construct_UClass_UAttributeEditorNormalsActions_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAttributeEditorNormalsActions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAttributeEditorActionPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UAttributeEditorNormalsActions_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UAttributeEditorNormalsActions_ResetHardNormals, "ResetHardNormals" }, // 2149669606
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttributeEditorNormalsActions_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AttributeEditorTool.h" },
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAttributeEditorNormalsActions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAttributeEditorNormalsActions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAttributeEditorNormalsActions_Statics::ClassParams = {
		&UAttributeEditorNormalsActions::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UAttributeEditorNormalsActions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorNormalsActions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAttributeEditorNormalsActions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAttributeEditorNormalsActions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAttributeEditorNormalsActions, 831218088);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UAttributeEditorNormalsActions>()
	{
		return UAttributeEditorNormalsActions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAttributeEditorNormalsActions(Z_Construct_UClass_UAttributeEditorNormalsActions, &UAttributeEditorNormalsActions::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UAttributeEditorNormalsActions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAttributeEditorNormalsActions);
	DEFINE_FUNCTION(UAttributeEditorUVActions::execClearAllUVSets)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ClearAllUVSets();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAttributeEditorUVActions::execClearSelectedUVSets)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ClearSelectedUVSets();
		P_NATIVE_END;
	}
	void UAttributeEditorUVActions::StaticRegisterNativesUAttributeEditorUVActions()
	{
		UClass* Class = UAttributeEditorUVActions::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ClearAllUVSets", &UAttributeEditorUVActions::execClearAllUVSets },
			{ "ClearSelectedUVSets", &UAttributeEditorUVActions::execClearSelectedUVSets },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UAttributeEditorUVActions_ClearAllUVSets_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAttributeEditorUVActions_ClearAllUVSets_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "UVs" },
		{ "Comment", "/** Clear all UV layers, setting all UV values to (0,0) */" },
		{ "DisplayPriority", "2" },
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
		{ "ToolTip", "Clear all UV layers, setting all UV values to (0,0)" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAttributeEditorUVActions_ClearAllUVSets_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAttributeEditorUVActions, nullptr, "ClearAllUVSets", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAttributeEditorUVActions_ClearAllUVSets_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttributeEditorUVActions_ClearAllUVSets_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAttributeEditorUVActions_ClearAllUVSets()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAttributeEditorUVActions_ClearAllUVSets_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAttributeEditorUVActions_ClearSelectedUVSets_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAttributeEditorUVActions_ClearSelectedUVSets_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "UVs" },
		{ "Comment", "/** Clear the selected UV layers, setting all UV values to (0,0) */" },
		{ "DisplayPriority", "1" },
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
		{ "ToolTip", "Clear the selected UV layers, setting all UV values to (0,0)" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAttributeEditorUVActions_ClearSelectedUVSets_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAttributeEditorUVActions, nullptr, "ClearSelectedUVSets", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAttributeEditorUVActions_ClearSelectedUVSets_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttributeEditorUVActions_ClearSelectedUVSets_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAttributeEditorUVActions_ClearSelectedUVSets()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAttributeEditorUVActions_ClearSelectedUVSets_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UAttributeEditorUVActions_NoRegister()
	{
		return UAttributeEditorUVActions::StaticClass();
	}
	struct Z_Construct_UClass_UAttributeEditorUVActions_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bClearUVLayer0_MetaData[];
#endif
		static void NewProp_bClearUVLayer0_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bClearUVLayer0;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bClearUVLayer1_MetaData[];
#endif
		static void NewProp_bClearUVLayer1_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bClearUVLayer1;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bClearUVLayer2_MetaData[];
#endif
		static void NewProp_bClearUVLayer2_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bClearUVLayer2;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bClearUVLayer3_MetaData[];
#endif
		static void NewProp_bClearUVLayer3_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bClearUVLayer3;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bClearUVLayer4_MetaData[];
#endif
		static void NewProp_bClearUVLayer4_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bClearUVLayer4;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bClearUVLayer5_MetaData[];
#endif
		static void NewProp_bClearUVLayer5_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bClearUVLayer5;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bClearUVLayer6_MetaData[];
#endif
		static void NewProp_bClearUVLayer6_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bClearUVLayer6;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bClearUVLayer7_MetaData[];
#endif
		static void NewProp_bClearUVLayer7_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bClearUVLayer7;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumUVLayers_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_NumUVLayers;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAttributeEditorUVActions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAttributeEditorActionPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UAttributeEditorUVActions_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UAttributeEditorUVActions_ClearAllUVSets, "ClearAllUVSets" }, // 2918091407
		{ &Z_Construct_UFunction_UAttributeEditorUVActions_ClearSelectedUVSets, "ClearSelectedUVSets" }, // 2258357620
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttributeEditorUVActions_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AttributeEditorTool.h" },
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer0_MetaData[] = {
		{ "Category", "UVs" },
		{ "Comment", "/** Clear data from UV layer */" },
		{ "DisplayName", "Layer 0" },
		{ "EditCondition", "NumUVLayers > 0" },
		{ "EditConditionHides", "" },
		{ "HideEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
		{ "ToolTip", "Clear data from UV layer" },
	};
#endif
	void Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer0_SetBit(void* Obj)
	{
		((UAttributeEditorUVActions*)Obj)->bClearUVLayer0 = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer0 = { "bClearUVLayer0", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UAttributeEditorUVActions), &Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer0_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer0_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer0_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer1_MetaData[] = {
		{ "Category", "UVs" },
		{ "Comment", "/** Clear data from UV layer */" },
		{ "DisplayName", "Layer 1" },
		{ "EditCondition", "NumUVLayers > 1" },
		{ "EditConditionHides", "" },
		{ "HideEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
		{ "ToolTip", "Clear data from UV layer" },
	};
#endif
	void Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer1_SetBit(void* Obj)
	{
		((UAttributeEditorUVActions*)Obj)->bClearUVLayer1 = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer1 = { "bClearUVLayer1", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UAttributeEditorUVActions), &Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer1_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer1_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer1_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer2_MetaData[] = {
		{ "Category", "UVs" },
		{ "Comment", "/** Clear data from UV layer */" },
		{ "DisplayName", "Layer 2" },
		{ "EditCondition", "NumUVLayers > 2" },
		{ "EditConditionHides", "" },
		{ "HideEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
		{ "ToolTip", "Clear data from UV layer" },
	};
#endif
	void Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer2_SetBit(void* Obj)
	{
		((UAttributeEditorUVActions*)Obj)->bClearUVLayer2 = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer2 = { "bClearUVLayer2", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UAttributeEditorUVActions), &Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer2_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer2_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer2_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer3_MetaData[] = {
		{ "Category", "UVs" },
		{ "Comment", "/** Clear data from UV layer */" },
		{ "DisplayName", "Layer 3" },
		{ "EditCondition", "NumUVLayers > 3" },
		{ "EditConditionHides", "" },
		{ "HideEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
		{ "ToolTip", "Clear data from UV layer" },
	};
#endif
	void Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer3_SetBit(void* Obj)
	{
		((UAttributeEditorUVActions*)Obj)->bClearUVLayer3 = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer3 = { "bClearUVLayer3", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UAttributeEditorUVActions), &Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer3_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer3_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer3_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer4_MetaData[] = {
		{ "Category", "UVs" },
		{ "Comment", "/** Clear data from UV layer */" },
		{ "DisplayName", "Layer 4" },
		{ "EditCondition", "NumUVLayers > 4" },
		{ "EditConditionHides", "" },
		{ "HideEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
		{ "ToolTip", "Clear data from UV layer" },
	};
#endif
	void Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer4_SetBit(void* Obj)
	{
		((UAttributeEditorUVActions*)Obj)->bClearUVLayer4 = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer4 = { "bClearUVLayer4", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UAttributeEditorUVActions), &Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer4_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer4_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer4_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer5_MetaData[] = {
		{ "Category", "UVs" },
		{ "Comment", "/** Clear data from UV layer */" },
		{ "DisplayName", "Layer 5" },
		{ "EditCondition", "NumUVLayers > 5" },
		{ "EditConditionHides", "" },
		{ "HideEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
		{ "ToolTip", "Clear data from UV layer" },
	};
#endif
	void Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer5_SetBit(void* Obj)
	{
		((UAttributeEditorUVActions*)Obj)->bClearUVLayer5 = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer5 = { "bClearUVLayer5", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UAttributeEditorUVActions), &Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer5_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer5_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer5_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer6_MetaData[] = {
		{ "Category", "UVs" },
		{ "Comment", "/** Clear data from UV layer */" },
		{ "DisplayName", "Layer 6" },
		{ "EditCondition", "NumUVLayers > 6" },
		{ "EditConditionHides", "" },
		{ "HideEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
		{ "ToolTip", "Clear data from UV layer" },
	};
#endif
	void Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer6_SetBit(void* Obj)
	{
		((UAttributeEditorUVActions*)Obj)->bClearUVLayer6 = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer6 = { "bClearUVLayer6", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UAttributeEditorUVActions), &Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer6_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer6_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer6_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer7_MetaData[] = {
		{ "Category", "UVs" },
		{ "Comment", "/** Clear data from UV layer */" },
		{ "DisplayName", "Layer 7" },
		{ "EditCondition", "NumUVLayers > 7" },
		{ "EditConditionHides", "" },
		{ "HideEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
		{ "ToolTip", "Clear data from UV layer" },
	};
#endif
	void Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer7_SetBit(void* Obj)
	{
		((UAttributeEditorUVActions*)Obj)->bClearUVLayer7 = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer7 = { "bClearUVLayer7", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UAttributeEditorUVActions), &Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer7_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer7_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer7_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_NumUVLayers_MetaData[] = {
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_NumUVLayers = { "NumUVLayers", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAttributeEditorUVActions, NumUVLayers), METADATA_PARAMS(Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_NumUVLayers_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_NumUVLayers_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAttributeEditorUVActions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer0,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer1,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer2,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer3,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer4,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer5,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer6,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_bClearUVLayer7,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttributeEditorUVActions_Statics::NewProp_NumUVLayers,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAttributeEditorUVActions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAttributeEditorUVActions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAttributeEditorUVActions_Statics::ClassParams = {
		&UAttributeEditorUVActions::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UAttributeEditorUVActions_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorUVActions_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UAttributeEditorUVActions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorUVActions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAttributeEditorUVActions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAttributeEditorUVActions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAttributeEditorUVActions, 324798997);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UAttributeEditorUVActions>()
	{
		return UAttributeEditorUVActions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAttributeEditorUVActions(Z_Construct_UClass_UAttributeEditorUVActions, &UAttributeEditorUVActions::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UAttributeEditorUVActions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAttributeEditorUVActions);
	DEFINE_FUNCTION(UAttributeEditorNewAttributeActions::execAddPolyGroupLayer)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AddPolyGroupLayer();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAttributeEditorNewAttributeActions::execAddWeightMapLayer)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AddWeightMapLayer();
		P_NATIVE_END;
	}
	void UAttributeEditorNewAttributeActions::StaticRegisterNativesUAttributeEditorNewAttributeActions()
	{
		UClass* Class = UAttributeEditorNewAttributeActions::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddPolyGroupLayer", &UAttributeEditorNewAttributeActions::execAddPolyGroupLayer },
			{ "AddWeightMapLayer", &UAttributeEditorNewAttributeActions::execAddWeightMapLayer },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UAttributeEditorNewAttributeActions_AddPolyGroupLayer_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAttributeEditorNewAttributeActions_AddPolyGroupLayer_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "NewAttribute" },
		{ "Comment", "/** Add a new PolyGroup layer with the given Name */" },
		{ "DisplayPriority", "3" },
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
		{ "ToolTip", "Add a new PolyGroup layer with the given Name" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAttributeEditorNewAttributeActions_AddPolyGroupLayer_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAttributeEditorNewAttributeActions, nullptr, "AddPolyGroupLayer", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAttributeEditorNewAttributeActions_AddPolyGroupLayer_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttributeEditorNewAttributeActions_AddPolyGroupLayer_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAttributeEditorNewAttributeActions_AddPolyGroupLayer()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAttributeEditorNewAttributeActions_AddPolyGroupLayer_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAttributeEditorNewAttributeActions_AddWeightMapLayer_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAttributeEditorNewAttributeActions_AddWeightMapLayer_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "NewAttribute" },
		{ "Comment", "/** Add a new Per-Vertex Weight Map layer with the given Name */" },
		{ "DisplayPriority", "2" },
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
		{ "ToolTip", "Add a new Per-Vertex Weight Map layer with the given Name" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAttributeEditorNewAttributeActions_AddWeightMapLayer_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAttributeEditorNewAttributeActions, nullptr, "AddWeightMapLayer", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAttributeEditorNewAttributeActions_AddWeightMapLayer_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttributeEditorNewAttributeActions_AddWeightMapLayer_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAttributeEditorNewAttributeActions_AddWeightMapLayer()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAttributeEditorNewAttributeActions_AddWeightMapLayer_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UAttributeEditorNewAttributeActions_NoRegister()
	{
		return UAttributeEditorNewAttributeActions::StaticClass();
	}
	struct Z_Construct_UClass_UAttributeEditorNewAttributeActions_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_NewName;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ElementType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ElementType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ElementType;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DataType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DataType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_DataType;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAttributeEditorNewAttributeActions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAttributeEditorActionPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UAttributeEditorNewAttributeActions_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UAttributeEditorNewAttributeActions_AddPolyGroupLayer, "AddPolyGroupLayer" }, // 865602636
		{ &Z_Construct_UFunction_UAttributeEditorNewAttributeActions_AddWeightMapLayer, "AddWeightMapLayer" }, // 3099534302
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttributeEditorNewAttributeActions_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AttributeEditorTool.h" },
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttributeEditorNewAttributeActions_Statics::NewProp_NewName_MetaData[] = {
		{ "Category", "NewAttribute" },
		{ "DisplayName", "New Attribute Name" },
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UAttributeEditorNewAttributeActions_Statics::NewProp_NewName = { "NewName", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAttributeEditorNewAttributeActions, NewName), METADATA_PARAMS(Z_Construct_UClass_UAttributeEditorNewAttributeActions_Statics::NewProp_NewName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorNewAttributeActions_Statics::NewProp_NewName_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UAttributeEditorNewAttributeActions_Statics::NewProp_ElementType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttributeEditorNewAttributeActions_Statics::NewProp_ElementType_MetaData[] = {
		{ "Comment", "//UPROPERTY(EditAnywhere, Category = NewAttribute)\n" },
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
		{ "ToolTip", "UPROPERTY(EditAnywhere, Category = NewAttribute)" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UAttributeEditorNewAttributeActions_Statics::NewProp_ElementType = { "ElementType", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAttributeEditorNewAttributeActions, ElementType), Z_Construct_UEnum_MeshModelingToolsEditorOnly_EAttributeEditorElementType, METADATA_PARAMS(Z_Construct_UClass_UAttributeEditorNewAttributeActions_Statics::NewProp_ElementType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorNewAttributeActions_Statics::NewProp_ElementType_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UAttributeEditorNewAttributeActions_Statics::NewProp_DataType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttributeEditorNewAttributeActions_Statics::NewProp_DataType_MetaData[] = {
		{ "Comment", "//UPROPERTY(EditAnywhere, Category = NewAttribute)\n" },
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
		{ "ToolTip", "UPROPERTY(EditAnywhere, Category = NewAttribute)" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UAttributeEditorNewAttributeActions_Statics::NewProp_DataType = { "DataType", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAttributeEditorNewAttributeActions, DataType), Z_Construct_UEnum_MeshModelingToolsEditorOnly_EAttributeEditorAttribType, METADATA_PARAMS(Z_Construct_UClass_UAttributeEditorNewAttributeActions_Statics::NewProp_DataType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorNewAttributeActions_Statics::NewProp_DataType_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAttributeEditorNewAttributeActions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttributeEditorNewAttributeActions_Statics::NewProp_NewName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttributeEditorNewAttributeActions_Statics::NewProp_ElementType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttributeEditorNewAttributeActions_Statics::NewProp_ElementType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttributeEditorNewAttributeActions_Statics::NewProp_DataType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttributeEditorNewAttributeActions_Statics::NewProp_DataType,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAttributeEditorNewAttributeActions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAttributeEditorNewAttributeActions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAttributeEditorNewAttributeActions_Statics::ClassParams = {
		&UAttributeEditorNewAttributeActions::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UAttributeEditorNewAttributeActions_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorNewAttributeActions_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UAttributeEditorNewAttributeActions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorNewAttributeActions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAttributeEditorNewAttributeActions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAttributeEditorNewAttributeActions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAttributeEditorNewAttributeActions, 2117016929);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UAttributeEditorNewAttributeActions>()
	{
		return UAttributeEditorNewAttributeActions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAttributeEditorNewAttributeActions(Z_Construct_UClass_UAttributeEditorNewAttributeActions, &UAttributeEditorNewAttributeActions::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UAttributeEditorNewAttributeActions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAttributeEditorNewAttributeActions);
	DEFINE_FUNCTION(UAttributeEditorModifyAttributeActions::execDeleteSelected)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->DeleteSelected();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAttributeEditorModifyAttributeActions::execGetAttributeNamesFunc)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<FString>*)Z_Param__Result=P_THIS->GetAttributeNamesFunc();
		P_NATIVE_END;
	}
	void UAttributeEditorModifyAttributeActions::StaticRegisterNativesUAttributeEditorModifyAttributeActions()
	{
		UClass* Class = UAttributeEditorModifyAttributeActions::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "DeleteSelected", &UAttributeEditorModifyAttributeActions::execDeleteSelected },
			{ "GetAttributeNamesFunc", &UAttributeEditorModifyAttributeActions::execGetAttributeNamesFunc },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UAttributeEditorModifyAttributeActions_DeleteSelected_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAttributeEditorModifyAttributeActions_DeleteSelected_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "ModifyAttribute" },
		{ "Comment", "/** Remove the selected Attribute Name from the mesh */" },
		{ "DisplayPriority", "1" },
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
		{ "ToolTip", "Remove the selected Attribute Name from the mesh" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAttributeEditorModifyAttributeActions_DeleteSelected_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAttributeEditorModifyAttributeActions, nullptr, "DeleteSelected", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAttributeEditorModifyAttributeActions_DeleteSelected_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttributeEditorModifyAttributeActions_DeleteSelected_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAttributeEditorModifyAttributeActions_DeleteSelected()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAttributeEditorModifyAttributeActions_DeleteSelected_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAttributeEditorModifyAttributeActions_GetAttributeNamesFunc_Statics
	{
		struct AttributeEditorModifyAttributeActions_eventGetAttributeNamesFunc_Parms
		{
			TArray<FString> ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UAttributeEditorModifyAttributeActions_GetAttributeNamesFunc_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UAttributeEditorModifyAttributeActions_GetAttributeNamesFunc_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AttributeEditorModifyAttributeActions_eventGetAttributeNamesFunc_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAttributeEditorModifyAttributeActions_GetAttributeNamesFunc_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAttributeEditorModifyAttributeActions_GetAttributeNamesFunc_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAttributeEditorModifyAttributeActions_GetAttributeNamesFunc_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAttributeEditorModifyAttributeActions_GetAttributeNamesFunc_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAttributeEditorModifyAttributeActions_GetAttributeNamesFunc_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAttributeEditorModifyAttributeActions, nullptr, "GetAttributeNamesFunc", nullptr, nullptr, sizeof(AttributeEditorModifyAttributeActions_eventGetAttributeNamesFunc_Parms), Z_Construct_UFunction_UAttributeEditorModifyAttributeActions_GetAttributeNamesFunc_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttributeEditorModifyAttributeActions_GetAttributeNamesFunc_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAttributeEditorModifyAttributeActions_GetAttributeNamesFunc_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttributeEditorModifyAttributeActions_GetAttributeNamesFunc_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAttributeEditorModifyAttributeActions_GetAttributeNamesFunc()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAttributeEditorModifyAttributeActions_GetAttributeNamesFunc_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UAttributeEditorModifyAttributeActions_NoRegister()
	{
		return UAttributeEditorModifyAttributeActions::StaticClass();
	}
	struct Z_Construct_UClass_UAttributeEditorModifyAttributeActions_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Attribute_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Attribute;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AttributeNamesList_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttributeNamesList_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_AttributeNamesList;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAttributeEditorModifyAttributeActions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAttributeEditorActionPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UAttributeEditorModifyAttributeActions_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UAttributeEditorModifyAttributeActions_DeleteSelected, "DeleteSelected" }, // 2711764176
		{ &Z_Construct_UFunction_UAttributeEditorModifyAttributeActions_GetAttributeNamesFunc, "GetAttributeNamesFunc" }, // 2464647879
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttributeEditorModifyAttributeActions_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AttributeEditorTool.h" },
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttributeEditorModifyAttributeActions_Statics::NewProp_Attribute_MetaData[] = {
		{ "Category", "ModifyAttribute" },
		{ "GetOptions", "GetAttributeNamesFunc" },
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UAttributeEditorModifyAttributeActions_Statics::NewProp_Attribute = { "Attribute", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAttributeEditorModifyAttributeActions, Attribute), METADATA_PARAMS(Z_Construct_UClass_UAttributeEditorModifyAttributeActions_Statics::NewProp_Attribute_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorModifyAttributeActions_Statics::NewProp_Attribute_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UAttributeEditorModifyAttributeActions_Statics::NewProp_AttributeNamesList_Inner = { "AttributeNamesList", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttributeEditorModifyAttributeActions_Statics::NewProp_AttributeNamesList_MetaData[] = {
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UAttributeEditorModifyAttributeActions_Statics::NewProp_AttributeNamesList = { "AttributeNamesList", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAttributeEditorModifyAttributeActions, AttributeNamesList), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UAttributeEditorModifyAttributeActions_Statics::NewProp_AttributeNamesList_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorModifyAttributeActions_Statics::NewProp_AttributeNamesList_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAttributeEditorModifyAttributeActions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttributeEditorModifyAttributeActions_Statics::NewProp_Attribute,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttributeEditorModifyAttributeActions_Statics::NewProp_AttributeNamesList_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttributeEditorModifyAttributeActions_Statics::NewProp_AttributeNamesList,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAttributeEditorModifyAttributeActions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAttributeEditorModifyAttributeActions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAttributeEditorModifyAttributeActions_Statics::ClassParams = {
		&UAttributeEditorModifyAttributeActions::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UAttributeEditorModifyAttributeActions_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorModifyAttributeActions_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UAttributeEditorModifyAttributeActions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorModifyAttributeActions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAttributeEditorModifyAttributeActions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAttributeEditorModifyAttributeActions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAttributeEditorModifyAttributeActions, 901780800);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UAttributeEditorModifyAttributeActions>()
	{
		return UAttributeEditorModifyAttributeActions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAttributeEditorModifyAttributeActions(Z_Construct_UClass_UAttributeEditorModifyAttributeActions, &UAttributeEditorModifyAttributeActions::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UAttributeEditorModifyAttributeActions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAttributeEditorModifyAttributeActions);
	DEFINE_FUNCTION(UAttributeEditorCopyAttributeActions::execCopyFromTo)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->CopyFromTo();
		P_NATIVE_END;
	}
	void UAttributeEditorCopyAttributeActions::StaticRegisterNativesUAttributeEditorCopyAttributeActions()
	{
		UClass* Class = UAttributeEditorCopyAttributeActions::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CopyFromTo", &UAttributeEditorCopyAttributeActions::execCopyFromTo },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UAttributeEditorCopyAttributeActions_CopyFromTo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAttributeEditorCopyAttributeActions_CopyFromTo_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "CopyAttribute" },
		{ "DisplayPriority", "1" },
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAttributeEditorCopyAttributeActions_CopyFromTo_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAttributeEditorCopyAttributeActions, nullptr, "CopyFromTo", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAttributeEditorCopyAttributeActions_CopyFromTo_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttributeEditorCopyAttributeActions_CopyFromTo_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAttributeEditorCopyAttributeActions_CopyFromTo()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAttributeEditorCopyAttributeActions_CopyFromTo_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UAttributeEditorCopyAttributeActions_NoRegister()
	{
		return UAttributeEditorCopyAttributeActions::StaticClass();
	}
	struct Z_Construct_UClass_UAttributeEditorCopyAttributeActions_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FromAttribute_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FromAttribute_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FromAttribute;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ToAttribute_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ToAttribute_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ToAttribute;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAttributeEditorCopyAttributeActions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAttributeEditorActionPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UAttributeEditorCopyAttributeActions_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UAttributeEditorCopyAttributeActions_CopyFromTo, "CopyFromTo" }, // 2592020872
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttributeEditorCopyAttributeActions_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AttributeEditorTool.h" },
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UAttributeEditorCopyAttributeActions_Statics::NewProp_FromAttribute_Inner = { "FromAttribute", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttributeEditorCopyAttributeActions_Statics::NewProp_FromAttribute_MetaData[] = {
		{ "Category", "CopyAttribute" },
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UAttributeEditorCopyAttributeActions_Statics::NewProp_FromAttribute = { "FromAttribute", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAttributeEditorCopyAttributeActions, FromAttribute), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UAttributeEditorCopyAttributeActions_Statics::NewProp_FromAttribute_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorCopyAttributeActions_Statics::NewProp_FromAttribute_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UAttributeEditorCopyAttributeActions_Statics::NewProp_ToAttribute_Inner = { "ToAttribute", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttributeEditorCopyAttributeActions_Statics::NewProp_ToAttribute_MetaData[] = {
		{ "Category", "CopyAttribute" },
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UAttributeEditorCopyAttributeActions_Statics::NewProp_ToAttribute = { "ToAttribute", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAttributeEditorCopyAttributeActions, ToAttribute), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UAttributeEditorCopyAttributeActions_Statics::NewProp_ToAttribute_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorCopyAttributeActions_Statics::NewProp_ToAttribute_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAttributeEditorCopyAttributeActions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttributeEditorCopyAttributeActions_Statics::NewProp_FromAttribute_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttributeEditorCopyAttributeActions_Statics::NewProp_FromAttribute,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttributeEditorCopyAttributeActions_Statics::NewProp_ToAttribute_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttributeEditorCopyAttributeActions_Statics::NewProp_ToAttribute,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAttributeEditorCopyAttributeActions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAttributeEditorCopyAttributeActions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAttributeEditorCopyAttributeActions_Statics::ClassParams = {
		&UAttributeEditorCopyAttributeActions::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UAttributeEditorCopyAttributeActions_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorCopyAttributeActions_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UAttributeEditorCopyAttributeActions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorCopyAttributeActions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAttributeEditorCopyAttributeActions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAttributeEditorCopyAttributeActions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAttributeEditorCopyAttributeActions, 2730105823);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UAttributeEditorCopyAttributeActions>()
	{
		return UAttributeEditorCopyAttributeActions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAttributeEditorCopyAttributeActions(Z_Construct_UClass_UAttributeEditorCopyAttributeActions, &UAttributeEditorCopyAttributeActions::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UAttributeEditorCopyAttributeActions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAttributeEditorCopyAttributeActions);
	void UAttributeEditorTool::StaticRegisterNativesUAttributeEditorTool()
	{
	}
	UClass* Z_Construct_UClass_UAttributeEditorTool_NoRegister()
	{
		return UAttributeEditorTool::StaticClass();
	}
	struct Z_Construct_UClass_UAttributeEditorTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NormalsActions_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_NormalsActions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UVActions_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_UVActions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttributeProps_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AttributeProps;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewAttributeProps_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_NewAttributeProps;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ModifyAttributeProps_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ModifyAttributeProps;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CopyAttributeProps_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CopyAttributeProps;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAttributeEditorTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMultiSelectionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttributeEditorTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Mesh Attribute Editor Tool\n */" },
		{ "IncludePath", "AttributeEditorTool.h" },
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
		{ "ToolTip", "Mesh Attribute Editor Tool" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttributeEditorTool_Statics::NewProp_NormalsActions_MetaData[] = {
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UAttributeEditorTool_Statics::NewProp_NormalsActions = { "NormalsActions", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAttributeEditorTool, NormalsActions), Z_Construct_UClass_UAttributeEditorNormalsActions_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UAttributeEditorTool_Statics::NewProp_NormalsActions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorTool_Statics::NewProp_NormalsActions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttributeEditorTool_Statics::NewProp_UVActions_MetaData[] = {
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UAttributeEditorTool_Statics::NewProp_UVActions = { "UVActions", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAttributeEditorTool, UVActions), Z_Construct_UClass_UAttributeEditorUVActions_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UAttributeEditorTool_Statics::NewProp_UVActions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorTool_Statics::NewProp_UVActions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttributeEditorTool_Statics::NewProp_AttributeProps_MetaData[] = {
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UAttributeEditorTool_Statics::NewProp_AttributeProps = { "AttributeProps", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAttributeEditorTool, AttributeProps), Z_Construct_UClass_UAttributeEditorAttribProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UAttributeEditorTool_Statics::NewProp_AttributeProps_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorTool_Statics::NewProp_AttributeProps_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttributeEditorTool_Statics::NewProp_NewAttributeProps_MetaData[] = {
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UAttributeEditorTool_Statics::NewProp_NewAttributeProps = { "NewAttributeProps", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAttributeEditorTool, NewAttributeProps), Z_Construct_UClass_UAttributeEditorNewAttributeActions_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UAttributeEditorTool_Statics::NewProp_NewAttributeProps_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorTool_Statics::NewProp_NewAttributeProps_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttributeEditorTool_Statics::NewProp_ModifyAttributeProps_MetaData[] = {
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UAttributeEditorTool_Statics::NewProp_ModifyAttributeProps = { "ModifyAttributeProps", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAttributeEditorTool, ModifyAttributeProps), Z_Construct_UClass_UAttributeEditorModifyAttributeActions_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UAttributeEditorTool_Statics::NewProp_ModifyAttributeProps_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorTool_Statics::NewProp_ModifyAttributeProps_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttributeEditorTool_Statics::NewProp_CopyAttributeProps_MetaData[] = {
		{ "ModuleRelativePath", "Public/AttributeEditorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UAttributeEditorTool_Statics::NewProp_CopyAttributeProps = { "CopyAttributeProps", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAttributeEditorTool, CopyAttributeProps), Z_Construct_UClass_UAttributeEditorCopyAttributeActions_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UAttributeEditorTool_Statics::NewProp_CopyAttributeProps_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorTool_Statics::NewProp_CopyAttributeProps_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAttributeEditorTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttributeEditorTool_Statics::NewProp_NormalsActions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttributeEditorTool_Statics::NewProp_UVActions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttributeEditorTool_Statics::NewProp_AttributeProps,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttributeEditorTool_Statics::NewProp_NewAttributeProps,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttributeEditorTool_Statics::NewProp_ModifyAttributeProps,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttributeEditorTool_Statics::NewProp_CopyAttributeProps,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAttributeEditorTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAttributeEditorTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAttributeEditorTool_Statics::ClassParams = {
		&UAttributeEditorTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UAttributeEditorTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UAttributeEditorTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAttributeEditorTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAttributeEditorTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAttributeEditorTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAttributeEditorTool, 1912269589);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UAttributeEditorTool>()
	{
		return UAttributeEditorTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAttributeEditorTool(Z_Construct_UClass_UAttributeEditorTool, &UAttributeEditorTool::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UAttributeEditorTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAttributeEditorTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
