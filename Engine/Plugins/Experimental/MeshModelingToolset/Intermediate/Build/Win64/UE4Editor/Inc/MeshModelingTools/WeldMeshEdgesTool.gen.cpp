// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/WeldMeshEdgesTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWeldMeshEdgesTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UWeldMeshEdgesToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UWeldMeshEdgesToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UWeldMeshEdgesTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UWeldMeshEdgesTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_USingleSelectionTool();
// End Cross Module References
	void UWeldMeshEdgesToolBuilder::StaticRegisterNativesUWeldMeshEdgesToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UWeldMeshEdgesToolBuilder_NoRegister()
	{
		return UWeldMeshEdgesToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UWeldMeshEdgesToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UWeldMeshEdgesToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWeldMeshEdgesToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "WeldMeshEdgesTool.h" },
		{ "ModuleRelativePath", "Public/WeldMeshEdgesTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UWeldMeshEdgesToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UWeldMeshEdgesToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UWeldMeshEdgesToolBuilder_Statics::ClassParams = {
		&UWeldMeshEdgesToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UWeldMeshEdgesToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UWeldMeshEdgesToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UWeldMeshEdgesToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UWeldMeshEdgesToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UWeldMeshEdgesToolBuilder, 343148397);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UWeldMeshEdgesToolBuilder>()
	{
		return UWeldMeshEdgesToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UWeldMeshEdgesToolBuilder(Z_Construct_UClass_UWeldMeshEdgesToolBuilder, &UWeldMeshEdgesToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UWeldMeshEdgesToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UWeldMeshEdgesToolBuilder);
	void UWeldMeshEdgesTool::StaticRegisterNativesUWeldMeshEdgesTool()
	{
	}
	UClass* Z_Construct_UClass_UWeldMeshEdgesTool_NoRegister()
	{
		return UWeldMeshEdgesTool::StaticClass();
	}
	struct Z_Construct_UClass_UWeldMeshEdgesTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Tolerance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Tolerance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOnlyUnique_MetaData[];
#endif
		static void NewProp_bOnlyUnique_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOnlyUnique;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UWeldMeshEdgesTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USingleSelectionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWeldMeshEdgesTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Mesh Weld Edges Tool\n */" },
		{ "IncludePath", "WeldMeshEdgesTool.h" },
		{ "ModuleRelativePath", "Public/WeldMeshEdgesTool.h" },
		{ "ToolTip", "Mesh Weld Edges Tool" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWeldMeshEdgesTool_Statics::NewProp_Tolerance_MetaData[] = {
		{ "Category", "Options" },
		{ "ClampMax", "1000.0" },
		{ "ClampMin", "0.00000001" },
		{ "Comment", "/** Target edge length */" },
		{ "ModuleRelativePath", "Public/WeldMeshEdgesTool.h" },
		{ "ToolTip", "Target edge length" },
		{ "UIMax", "0.01" },
		{ "UIMin", "0.000001" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UWeldMeshEdgesTool_Statics::NewProp_Tolerance = { "Tolerance", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWeldMeshEdgesTool, Tolerance), METADATA_PARAMS(Z_Construct_UClass_UWeldMeshEdgesTool_Statics::NewProp_Tolerance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWeldMeshEdgesTool_Statics::NewProp_Tolerance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWeldMeshEdgesTool_Statics::NewProp_bOnlyUnique_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Enable edge flips */" },
		{ "ModuleRelativePath", "Public/WeldMeshEdgesTool.h" },
		{ "ToolTip", "Enable edge flips" },
	};
#endif
	void Z_Construct_UClass_UWeldMeshEdgesTool_Statics::NewProp_bOnlyUnique_SetBit(void* Obj)
	{
		((UWeldMeshEdgesTool*)Obj)->bOnlyUnique = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UWeldMeshEdgesTool_Statics::NewProp_bOnlyUnique = { "bOnlyUnique", nullptr, (EPropertyFlags)0x00200c0000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UWeldMeshEdgesTool), &Z_Construct_UClass_UWeldMeshEdgesTool_Statics::NewProp_bOnlyUnique_SetBit, METADATA_PARAMS(Z_Construct_UClass_UWeldMeshEdgesTool_Statics::NewProp_bOnlyUnique_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWeldMeshEdgesTool_Statics::NewProp_bOnlyUnique_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UWeldMeshEdgesTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWeldMeshEdgesTool_Statics::NewProp_Tolerance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWeldMeshEdgesTool_Statics::NewProp_bOnlyUnique,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UWeldMeshEdgesTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UWeldMeshEdgesTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UWeldMeshEdgesTool_Statics::ClassParams = {
		&UWeldMeshEdgesTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UWeldMeshEdgesTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UWeldMeshEdgesTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UWeldMeshEdgesTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UWeldMeshEdgesTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UWeldMeshEdgesTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UWeldMeshEdgesTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UWeldMeshEdgesTool, 1205147469);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UWeldMeshEdgesTool>()
	{
		return UWeldMeshEdgesTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UWeldMeshEdgesTool(Z_Construct_UClass_UWeldMeshEdgesTool, &UWeldMeshEdgesTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UWeldMeshEdgesTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UWeldMeshEdgesTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
