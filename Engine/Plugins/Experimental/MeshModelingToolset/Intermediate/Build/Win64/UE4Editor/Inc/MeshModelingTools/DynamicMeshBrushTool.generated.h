// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLS_DynamicMeshBrushTool_generated_h
#error "DynamicMeshBrushTool.generated.h already included, missing '#pragma once' in DynamicMeshBrushTool.h"
#endif
#define MESHMODELINGTOOLS_DynamicMeshBrushTool_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DynamicMeshBrushTool_h_25_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DynamicMeshBrushTool_h_25_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DynamicMeshBrushTool_h_25_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DynamicMeshBrushTool_h_25_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDynamicMeshBrushTool(); \
	friend struct Z_Construct_UClass_UDynamicMeshBrushTool_Statics; \
public: \
	DECLARE_CLASS(UDynamicMeshBrushTool, UBaseBrushTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UDynamicMeshBrushTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DynamicMeshBrushTool_h_25_INCLASS \
private: \
	static void StaticRegisterNativesUDynamicMeshBrushTool(); \
	friend struct Z_Construct_UClass_UDynamicMeshBrushTool_Statics; \
public: \
	DECLARE_CLASS(UDynamicMeshBrushTool, UBaseBrushTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UDynamicMeshBrushTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DynamicMeshBrushTool_h_25_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDynamicMeshBrushTool(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDynamicMeshBrushTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDynamicMeshBrushTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDynamicMeshBrushTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDynamicMeshBrushTool(UDynamicMeshBrushTool&&); \
	NO_API UDynamicMeshBrushTool(const UDynamicMeshBrushTool&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DynamicMeshBrushTool_h_25_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDynamicMeshBrushTool(UDynamicMeshBrushTool&&); \
	NO_API UDynamicMeshBrushTool(const UDynamicMeshBrushTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDynamicMeshBrushTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDynamicMeshBrushTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDynamicMeshBrushTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DynamicMeshBrushTool_h_25_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__PreviewMesh() { return STRUCT_OFFSET(UDynamicMeshBrushTool, PreviewMesh); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DynamicMeshBrushTool_h_22_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DynamicMeshBrushTool_h_25_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DynamicMeshBrushTool_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DynamicMeshBrushTool_h_25_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DynamicMeshBrushTool_h_25_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DynamicMeshBrushTool_h_25_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DynamicMeshBrushTool_h_25_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DynamicMeshBrushTool_h_25_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DynamicMeshBrushTool_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DynamicMeshBrushTool_h_25_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DynamicMeshBrushTool_h_25_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DynamicMeshBrushTool_h_25_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DynamicMeshBrushTool_h_25_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UDynamicMeshBrushTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_DynamicMeshBrushTool_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
