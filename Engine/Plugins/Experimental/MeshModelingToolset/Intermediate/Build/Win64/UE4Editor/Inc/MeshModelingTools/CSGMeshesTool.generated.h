// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLS_CSGMeshesTool_generated_h
#error "CSGMeshesTool.generated.h already included, missing '#pragma once' in CSGMeshesTool.h"
#endif
#define MESHMODELINGTOOLS_CSGMeshesTool_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_25_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_25_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_25_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_25_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCSGMeshesToolProperties(); \
	friend struct Z_Construct_UClass_UCSGMeshesToolProperties_Statics; \
public: \
	DECLARE_CLASS(UCSGMeshesToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UCSGMeshesToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_25_INCLASS \
private: \
	static void StaticRegisterNativesUCSGMeshesToolProperties(); \
	friend struct Z_Construct_UClass_UCSGMeshesToolProperties_Statics; \
public: \
	DECLARE_CLASS(UCSGMeshesToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UCSGMeshesToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_25_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCSGMeshesToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCSGMeshesToolProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCSGMeshesToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCSGMeshesToolProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCSGMeshesToolProperties(UCSGMeshesToolProperties&&); \
	NO_API UCSGMeshesToolProperties(const UCSGMeshesToolProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_25_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCSGMeshesToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCSGMeshesToolProperties(UCSGMeshesToolProperties&&); \
	NO_API UCSGMeshesToolProperties(const UCSGMeshesToolProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCSGMeshesToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCSGMeshesToolProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCSGMeshesToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_25_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_22_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_25_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_25_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_25_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_25_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_25_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_25_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_25_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_25_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_25_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_25_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UCSGMeshesToolProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_52_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_52_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_52_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_52_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCSGMeshesTool(); \
	friend struct Z_Construct_UClass_UCSGMeshesTool_Statics; \
public: \
	DECLARE_CLASS(UCSGMeshesTool, UBaseCreateFromSelectedTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UCSGMeshesTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_52_INCLASS \
private: \
	static void StaticRegisterNativesUCSGMeshesTool(); \
	friend struct Z_Construct_UClass_UCSGMeshesTool_Statics; \
public: \
	DECLARE_CLASS(UCSGMeshesTool, UBaseCreateFromSelectedTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UCSGMeshesTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_52_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCSGMeshesTool(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCSGMeshesTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCSGMeshesTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCSGMeshesTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCSGMeshesTool(UCSGMeshesTool&&); \
	NO_API UCSGMeshesTool(const UCSGMeshesTool&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_52_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCSGMeshesTool(UCSGMeshesTool&&); \
	NO_API UCSGMeshesTool(const UCSGMeshesTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCSGMeshesTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCSGMeshesTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UCSGMeshesTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_52_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CSGProperties() { return STRUCT_OFFSET(UCSGMeshesTool, CSGProperties); } \
	FORCEINLINE static uint32 __PPO__DrawnLineSet() { return STRUCT_OFFSET(UCSGMeshesTool, DrawnLineSet); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_49_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_52_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_52_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_52_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_52_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_52_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_52_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_52_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_52_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_52_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_52_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_52_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_52_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UCSGMeshesTool>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_96_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_96_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_96_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_96_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCSGMeshesToolBuilder(); \
	friend struct Z_Construct_UClass_UCSGMeshesToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UCSGMeshesToolBuilder, UBaseCreateFromSelectedToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UCSGMeshesToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_96_INCLASS \
private: \
	static void StaticRegisterNativesUCSGMeshesToolBuilder(); \
	friend struct Z_Construct_UClass_UCSGMeshesToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UCSGMeshesToolBuilder, UBaseCreateFromSelectedToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UCSGMeshesToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_96_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCSGMeshesToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCSGMeshesToolBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCSGMeshesToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCSGMeshesToolBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCSGMeshesToolBuilder(UCSGMeshesToolBuilder&&); \
	NO_API UCSGMeshesToolBuilder(const UCSGMeshesToolBuilder&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_96_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCSGMeshesToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCSGMeshesToolBuilder(UCSGMeshesToolBuilder&&); \
	NO_API UCSGMeshesToolBuilder(const UCSGMeshesToolBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCSGMeshesToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCSGMeshesToolBuilder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCSGMeshesToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_96_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_93_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_96_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_96_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_96_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_96_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_96_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_96_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_96_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_96_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_96_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_96_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_96_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h_96_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UCSGMeshesToolBuilder>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_CSGMeshesTool_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
