// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLS_TransformMeshesTool_generated_h
#error "TransformMeshesTool.generated.h already included, missing '#pragma once' in TransformMeshesTool.h"
#endif
#define MESHMODELINGTOOLS_TransformMeshesTool_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_120_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FTransformMeshesTarget_Statics; \
	MESHMODELINGTOOLS_API static class UScriptStruct* StaticStruct();


template<> MESHMODELINGTOOLS_API UScriptStruct* StaticStruct<struct FTransformMeshesTarget>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_24_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_24_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_24_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTransformMeshesToolBuilder(); \
	friend struct Z_Construct_UClass_UTransformMeshesToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UTransformMeshesToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UTransformMeshesToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_24_INCLASS \
private: \
	static void StaticRegisterNativesUTransformMeshesToolBuilder(); \
	friend struct Z_Construct_UClass_UTransformMeshesToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UTransformMeshesToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UTransformMeshesToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTransformMeshesToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTransformMeshesToolBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTransformMeshesToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTransformMeshesToolBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTransformMeshesToolBuilder(UTransformMeshesToolBuilder&&); \
	NO_API UTransformMeshesToolBuilder(const UTransformMeshesToolBuilder&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_24_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTransformMeshesToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTransformMeshesToolBuilder(UTransformMeshesToolBuilder&&); \
	NO_API UTransformMeshesToolBuilder(const UTransformMeshesToolBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTransformMeshesToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTransformMeshesToolBuilder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTransformMeshesToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_24_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_21_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_24_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_24_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_24_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_24_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_24_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_24_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UTransformMeshesToolBuilder>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_92_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_92_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_92_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_92_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTransformMeshesToolProperties(); \
	friend struct Z_Construct_UClass_UTransformMeshesToolProperties_Statics; \
public: \
	DECLARE_CLASS(UTransformMeshesToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UTransformMeshesToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_92_INCLASS \
private: \
	static void StaticRegisterNativesUTransformMeshesToolProperties(); \
	friend struct Z_Construct_UClass_UTransformMeshesToolProperties_Statics; \
public: \
	DECLARE_CLASS(UTransformMeshesToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UTransformMeshesToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_92_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTransformMeshesToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTransformMeshesToolProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTransformMeshesToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTransformMeshesToolProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTransformMeshesToolProperties(UTransformMeshesToolProperties&&); \
	NO_API UTransformMeshesToolProperties(const UTransformMeshesToolProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_92_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTransformMeshesToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTransformMeshesToolProperties(UTransformMeshesToolProperties&&); \
	NO_API UTransformMeshesToolProperties(const UTransformMeshesToolProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTransformMeshesToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTransformMeshesToolProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTransformMeshesToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_92_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_89_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_92_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_92_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_92_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_92_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_92_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_92_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_92_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_92_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_92_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_92_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_92_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_92_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UTransformMeshesToolProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_136_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_136_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_136_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_136_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTransformMeshesTool(); \
	friend struct Z_Construct_UClass_UTransformMeshesTool_Statics; \
public: \
	DECLARE_CLASS(UTransformMeshesTool, UMultiSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UTransformMeshesTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_136_INCLASS \
private: \
	static void StaticRegisterNativesUTransformMeshesTool(); \
	friend struct Z_Construct_UClass_UTransformMeshesTool_Statics; \
public: \
	DECLARE_CLASS(UTransformMeshesTool, UMultiSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UTransformMeshesTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_136_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTransformMeshesTool(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTransformMeshesTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTransformMeshesTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTransformMeshesTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTransformMeshesTool(UTransformMeshesTool&&); \
	NO_API UTransformMeshesTool(const UTransformMeshesTool&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_136_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTransformMeshesTool(UTransformMeshesTool&&); \
	NO_API UTransformMeshesTool(const UTransformMeshesTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTransformMeshesTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTransformMeshesTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UTransformMeshesTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_136_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__TransformProps() { return STRUCT_OFFSET(UTransformMeshesTool, TransformProps); } \
	FORCEINLINE static uint32 __PPO__ActiveGizmos() { return STRUCT_OFFSET(UTransformMeshesTool, ActiveGizmos); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_133_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_136_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_136_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_136_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_136_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_136_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_136_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_136_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_136_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_136_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_136_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_136_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h_136_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UTransformMeshesTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_TransformMeshesTool_h


#define FOREACH_ENUM_ETRANSFORMMESHESSNAPDRAGROTATIONMODE(op) \
	op(ETransformMeshesSnapDragRotationMode::Ignore) \
	op(ETransformMeshesSnapDragRotationMode::Align) \
	op(ETransformMeshesSnapDragRotationMode::AlignFlipped) \
	op(ETransformMeshesSnapDragRotationMode::LastValue) 

enum class ETransformMeshesSnapDragRotationMode : uint8;
template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<ETransformMeshesSnapDragRotationMode>();

#define FOREACH_ENUM_ETRANSFORMMESHESSNAPDRAGSOURCE(op) \
	op(ETransformMeshesSnapDragSource::ClickPoint) \
	op(ETransformMeshesSnapDragSource::Pivot) \
	op(ETransformMeshesSnapDragSource::LastValue) 

enum class ETransformMeshesSnapDragSource : uint8;
template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<ETransformMeshesSnapDragSource>();

#define FOREACH_ENUM_ETRANSFORMMESHESTRANSFORMMODE(op) \
	op(ETransformMeshesTransformMode::SharedGizmo) \
	op(ETransformMeshesTransformMode::SharedGizmoLocal) \
	op(ETransformMeshesTransformMode::PerObjectGizmo) \
	op(ETransformMeshesTransformMode::LastValue) 

enum class ETransformMeshesTransformMode : uint8;
template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<ETransformMeshesTransformMode>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
