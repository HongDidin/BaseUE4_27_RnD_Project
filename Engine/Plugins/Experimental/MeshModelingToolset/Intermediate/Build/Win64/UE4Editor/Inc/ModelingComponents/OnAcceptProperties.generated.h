// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MODELINGCOMPONENTS_OnAcceptProperties_generated_h
#error "OnAcceptProperties.generated.h already included, missing '#pragma once' in OnAcceptProperties.h"
#endif
#define MODELINGCOMPONENTS_OnAcceptProperties_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_OnAcceptProperties_h_33_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_OnAcceptProperties_h_33_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_OnAcceptProperties_h_33_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_OnAcceptProperties_h_33_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOnAcceptHandleSourcesProperties(); \
	friend struct Z_Construct_UClass_UOnAcceptHandleSourcesProperties_Statics; \
public: \
	DECLARE_CLASS(UOnAcceptHandleSourcesProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UOnAcceptHandleSourcesProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_OnAcceptProperties_h_33_INCLASS \
private: \
	static void StaticRegisterNativesUOnAcceptHandleSourcesProperties(); \
	friend struct Z_Construct_UClass_UOnAcceptHandleSourcesProperties_Statics; \
public: \
	DECLARE_CLASS(UOnAcceptHandleSourcesProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UOnAcceptHandleSourcesProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_OnAcceptProperties_h_33_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOnAcceptHandleSourcesProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOnAcceptHandleSourcesProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOnAcceptHandleSourcesProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOnAcceptHandleSourcesProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOnAcceptHandleSourcesProperties(UOnAcceptHandleSourcesProperties&&); \
	NO_API UOnAcceptHandleSourcesProperties(const UOnAcceptHandleSourcesProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_OnAcceptProperties_h_33_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOnAcceptHandleSourcesProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOnAcceptHandleSourcesProperties(UOnAcceptHandleSourcesProperties&&); \
	NO_API UOnAcceptHandleSourcesProperties(const UOnAcceptHandleSourcesProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOnAcceptHandleSourcesProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOnAcceptHandleSourcesProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOnAcceptHandleSourcesProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_OnAcceptProperties_h_33_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_OnAcceptProperties_h_30_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_OnAcceptProperties_h_33_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_OnAcceptProperties_h_33_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_OnAcceptProperties_h_33_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_OnAcceptProperties_h_33_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_OnAcceptProperties_h_33_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_OnAcceptProperties_h_33_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_OnAcceptProperties_h_33_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_OnAcceptProperties_h_33_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_OnAcceptProperties_h_33_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_OnAcceptProperties_h_33_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_OnAcceptProperties_h_33_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_OnAcceptProperties_h_33_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MODELINGCOMPONENTS_API UClass* StaticClass<class UOnAcceptHandleSourcesProperties>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_PropertySets_OnAcceptProperties_h


#define FOREACH_ENUM_EHANDLESOURCESMETHOD(op) \
	op(EHandleSourcesMethod::DeleteSources) \
	op(EHandleSourcesMethod::HideSources) \
	op(EHandleSourcesMethod::KeepSources) 

enum class EHandleSourcesMethod : uint8;
template<> MODELINGCOMPONENTS_API UEnum* StaticEnum<EHandleSourcesMethod>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
