// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MODELINGOPERATORS_BooleanMeshesOp_generated_h
#error "BooleanMeshesOp.generated.h already included, missing '#pragma once' in BooleanMeshesOp.h"
#endif
#define MODELINGOPERATORS_BooleanMeshesOp_generated_h

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingOperators_Public_CompositionOps_BooleanMeshesOp_h


#define FOREACH_ENUM_ECSGOPERATION(op) \
	op(ECSGOperation::DifferenceAB) \
	op(ECSGOperation::DifferenceBA) \
	op(ECSGOperation::Intersect) \
	op(ECSGOperation::Union) \
	op(ECSGOperation::TrimA) \
	op(ECSGOperation::TrimB) 

enum class ECSGOperation : uint8;
template<> MODELINGOPERATORS_API UEnum* StaticEnum<ECSGOperation>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
