// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MODELINGCOMPONENTS_LineSetComponent_generated_h
#error "LineSetComponent.generated.h already included, missing '#pragma once' in LineSetComponent.h"
#endif
#define MODELINGCOMPONENTS_LineSetComponent_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_LineSetComponent_h_44_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_LineSetComponent_h_44_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_LineSetComponent_h_44_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_LineSetComponent_h_44_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULineSetComponent(); \
	friend struct Z_Construct_UClass_ULineSetComponent_Statics; \
public: \
	DECLARE_CLASS(ULineSetComponent, UMeshComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(ULineSetComponent)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_LineSetComponent_h_44_INCLASS \
private: \
	static void StaticRegisterNativesULineSetComponent(); \
	friend struct Z_Construct_UClass_ULineSetComponent_Statics; \
public: \
	DECLARE_CLASS(ULineSetComponent, UMeshComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(ULineSetComponent)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_LineSetComponent_h_44_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULineSetComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULineSetComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULineSetComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULineSetComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULineSetComponent(ULineSetComponent&&); \
	NO_API ULineSetComponent(const ULineSetComponent&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_LineSetComponent_h_44_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULineSetComponent(ULineSetComponent&&); \
	NO_API ULineSetComponent(const ULineSetComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULineSetComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULineSetComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ULineSetComponent)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_LineSetComponent_h_44_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__LineMaterial() { return STRUCT_OFFSET(ULineSetComponent, LineMaterial); } \
	FORCEINLINE static uint32 __PPO__Bounds() { return STRUCT_OFFSET(ULineSetComponent, Bounds); } \
	FORCEINLINE static uint32 __PPO__bBoundsDirty() { return STRUCT_OFFSET(ULineSetComponent, bBoundsDirty); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_LineSetComponent_h_41_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_LineSetComponent_h_44_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_LineSetComponent_h_44_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_LineSetComponent_h_44_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_LineSetComponent_h_44_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_LineSetComponent_h_44_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_LineSetComponent_h_44_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_LineSetComponent_h_44_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_LineSetComponent_h_44_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_LineSetComponent_h_44_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_LineSetComponent_h_44_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_LineSetComponent_h_44_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_LineSetComponent_h_44_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MODELINGCOMPONENTS_API UClass* StaticClass<class ULineSetComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_LineSetComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
