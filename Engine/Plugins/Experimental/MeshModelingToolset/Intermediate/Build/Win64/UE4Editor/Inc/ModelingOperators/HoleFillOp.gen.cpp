// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ModelingOperators/Public/CleaningOps/HoleFillOp.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeHoleFillOp() {}
// Cross Module References
	MODELINGOPERATORS_API UEnum* Z_Construct_UEnum_ModelingOperators_EHoleFillOpFillType();
	UPackage* Z_Construct_UPackage__Script_ModelingOperators();
// End Cross Module References
	static UEnum* EHoleFillOpFillType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ModelingOperators_EHoleFillOpFillType, Z_Construct_UPackage__Script_ModelingOperators(), TEXT("EHoleFillOpFillType"));
		}
		return Singleton;
	}
	template<> MODELINGOPERATORS_API UEnum* StaticEnum<EHoleFillOpFillType>()
	{
		return EHoleFillOpFillType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EHoleFillOpFillType(EHoleFillOpFillType_StaticEnum, TEXT("/Script/ModelingOperators"), TEXT("EHoleFillOpFillType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ModelingOperators_EHoleFillOpFillType_Hash() { return 3627210792U; }
	UEnum* Z_Construct_UEnum_ModelingOperators_EHoleFillOpFillType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ModelingOperators();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EHoleFillOpFillType"), 0, Get_Z_Construct_UEnum_ModelingOperators_EHoleFillOpFillType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EHoleFillOpFillType::TriangleFan", (int64)EHoleFillOpFillType::TriangleFan },
				{ "EHoleFillOpFillType::PolygonEarClipping", (int64)EHoleFillOpFillType::PolygonEarClipping },
				{ "EHoleFillOpFillType::Planar", (int64)EHoleFillOpFillType::Planar },
				{ "EHoleFillOpFillType::Minimal", (int64)EHoleFillOpFillType::Minimal },
				{ "EHoleFillOpFillType::Smooth", (int64)EHoleFillOpFillType::Smooth },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Minimal.Comment", "/** Fill with a triangulation which attempts to minimize Gaussian curvature and not introduce new interior vertices. */" },
				{ "Minimal.DisplayName", "Minimal" },
				{ "Minimal.Name", "EHoleFillOpFillType::Minimal" },
				{ "Minimal.ToolTip", "Fill with a triangulation which attempts to minimize Gaussian curvature and not introduce new interior vertices." },
				{ "ModuleRelativePath", "Public/CleaningOps/HoleFillOp.h" },
				{ "Planar.Comment", "/** Choose a best-fit plane, project the boundary vertices to the plane, and use 2D Delaunay triangulation. */" },
				{ "Planar.DisplayName", "Planar" },
				{ "Planar.Name", "EHoleFillOpFillType::Planar" },
				{ "Planar.ToolTip", "Choose a best-fit plane, project the boundary vertices to the plane, and use 2D Delaunay triangulation." },
				{ "PolygonEarClipping.Comment", "/** Incrementally triangulate the hole boundary without introducing new interior vertices. */" },
				{ "PolygonEarClipping.DisplayName", "PolygonEarClipping" },
				{ "PolygonEarClipping.Name", "EHoleFillOpFillType::PolygonEarClipping" },
				{ "PolygonEarClipping.ToolTip", "Incrementally triangulate the hole boundary without introducing new interior vertices." },
				{ "Smooth.Comment", "/** Fill hole with a simple triangulation, then alternate between smoothing and remeshing. Optionally include the\n\x09    triangles around the hole in the smoothing/remeshing. */" },
				{ "Smooth.DisplayName", "Smooth" },
				{ "Smooth.Name", "EHoleFillOpFillType::Smooth" },
				{ "Smooth.ToolTip", "Fill hole with a simple triangulation, then alternate between smoothing and remeshing. Optionally include the\n          triangles around the hole in the smoothing/remeshing." },
				{ "TriangleFan.Comment", "/** Fill with a fan of triangles connected to a new central vertex. */" },
				{ "TriangleFan.DisplayName", "TriangleFan" },
				{ "TriangleFan.Name", "EHoleFillOpFillType::TriangleFan" },
				{ "TriangleFan.ToolTip", "Fill with a fan of triangles connected to a new central vertex." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ModelingOperators,
				nullptr,
				"EHoleFillOpFillType",
				"EHoleFillOpFillType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
