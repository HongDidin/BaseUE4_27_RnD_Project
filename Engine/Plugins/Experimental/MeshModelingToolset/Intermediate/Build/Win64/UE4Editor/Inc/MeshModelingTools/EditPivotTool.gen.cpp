// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/EditPivotTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEditPivotTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EEditPivotToolActions();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EEditPivotSnapDragRotationMode();
	MESHMODELINGTOOLS_API UScriptStruct* Z_Construct_UScriptStruct_FEditPivotTarget();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UTransformProxy_NoRegister();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UTransformGizmo_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEditPivotToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEditPivotToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEditPivotToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEditPivotToolProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEditPivotToolActionPropertySet_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEditPivotToolActionPropertySet();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEditPivotTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UEditPivotTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UMultiSelectionTool();
// End Cross Module References
	static UEnum* EEditPivotToolActions_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EEditPivotToolActions, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EEditPivotToolActions"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EEditPivotToolActions>()
	{
		return EEditPivotToolActions_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EEditPivotToolActions(EEditPivotToolActions_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EEditPivotToolActions"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EEditPivotToolActions_Hash() { return 1101892483U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EEditPivotToolActions()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EEditPivotToolActions"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EEditPivotToolActions_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EEditPivotToolActions::NoAction", (int64)EEditPivotToolActions::NoAction },
				{ "EEditPivotToolActions::Center", (int64)EEditPivotToolActions::Center },
				{ "EEditPivotToolActions::Bottom", (int64)EEditPivotToolActions::Bottom },
				{ "EEditPivotToolActions::Top", (int64)EEditPivotToolActions::Top },
				{ "EEditPivotToolActions::Left", (int64)EEditPivotToolActions::Left },
				{ "EEditPivotToolActions::Right", (int64)EEditPivotToolActions::Right },
				{ "EEditPivotToolActions::Front", (int64)EEditPivotToolActions::Front },
				{ "EEditPivotToolActions::Back", (int64)EEditPivotToolActions::Back },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Back.Name", "EEditPivotToolActions::Back" },
				{ "Bottom.Name", "EEditPivotToolActions::Bottom" },
				{ "Center.Name", "EEditPivotToolActions::Center" },
				{ "Front.Name", "EEditPivotToolActions::Front" },
				{ "Left.Name", "EEditPivotToolActions::Left" },
				{ "ModuleRelativePath", "Public/EditPivotTool.h" },
				{ "NoAction.Name", "EEditPivotToolActions::NoAction" },
				{ "Right.Name", "EEditPivotToolActions::Right" },
				{ "Top.Name", "EEditPivotToolActions::Top" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EEditPivotToolActions",
				"EEditPivotToolActions",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EEditPivotSnapDragRotationMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EEditPivotSnapDragRotationMode, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EEditPivotSnapDragRotationMode"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EEditPivotSnapDragRotationMode>()
	{
		return EEditPivotSnapDragRotationMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EEditPivotSnapDragRotationMode(EEditPivotSnapDragRotationMode_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EEditPivotSnapDragRotationMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EEditPivotSnapDragRotationMode_Hash() { return 144746530U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EEditPivotSnapDragRotationMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EEditPivotSnapDragRotationMode"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EEditPivotSnapDragRotationMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EEditPivotSnapDragRotationMode::Ignore", (int64)EEditPivotSnapDragRotationMode::Ignore },
				{ "EEditPivotSnapDragRotationMode::Align", (int64)EEditPivotSnapDragRotationMode::Align },
				{ "EEditPivotSnapDragRotationMode::AlignFlipped", (int64)EEditPivotSnapDragRotationMode::AlignFlipped },
				{ "EEditPivotSnapDragRotationMode::LastValue", (int64)EEditPivotSnapDragRotationMode::LastValue },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Align.Comment", "/** Snap-Drag aligns the Source and Target Normals to point in the same direction */" },
				{ "Align.DisplayName", "Align" },
				{ "Align.Name", "EEditPivotSnapDragRotationMode::Align" },
				{ "Align.ToolTip", "Snap-Drag aligns the Source and Target Normals to point in the same direction" },
				{ "AlignFlipped.Comment", "/** Snap-Drag aligns the Source Normal to the opposite of the Target Normal direction */" },
				{ "AlignFlipped.DisplayName", "Align Flipped" },
				{ "AlignFlipped.Name", "EEditPivotSnapDragRotationMode::AlignFlipped" },
				{ "AlignFlipped.ToolTip", "Snap-Drag aligns the Source Normal to the opposite of the Target Normal direction" },
				{ "Comment", "/** Snap-Drag Rotation Mode */" },
				{ "Ignore.Comment", "/** Snap-Drag only translates, ignoring Normals */" },
				{ "Ignore.DisplayName", "Ignore" },
				{ "Ignore.Name", "EEditPivotSnapDragRotationMode::Ignore" },
				{ "Ignore.ToolTip", "Snap-Drag only translates, ignoring Normals" },
				{ "LastValue.Hidden", "" },
				{ "LastValue.Name", "EEditPivotSnapDragRotationMode::LastValue" },
				{ "ModuleRelativePath", "Public/EditPivotTool.h" },
				{ "ToolTip", "Snap-Drag Rotation Mode" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EEditPivotSnapDragRotationMode",
				"EEditPivotSnapDragRotationMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FEditPivotTarget::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MESHMODELINGTOOLS_API uint32 Get_Z_Construct_UScriptStruct_FEditPivotTarget_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FEditPivotTarget, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EditPivotTarget"), sizeof(FEditPivotTarget), Get_Z_Construct_UScriptStruct_FEditPivotTarget_Hash());
	}
	return Singleton;
}
template<> MESHMODELINGTOOLS_API UScriptStruct* StaticStruct<FEditPivotTarget>()
{
	return FEditPivotTarget::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FEditPivotTarget(FEditPivotTarget::StaticStruct, TEXT("/Script/MeshModelingTools"), TEXT("EditPivotTarget"), false, nullptr, nullptr);
static struct FScriptStruct_MeshModelingTools_StaticRegisterNativesFEditPivotTarget
{
	FScriptStruct_MeshModelingTools_StaticRegisterNativesFEditPivotTarget()
	{
		UScriptStruct::DeferCppStructOps<FEditPivotTarget>(FName(TEXT("EditPivotTarget")));
	}
} ScriptStruct_MeshModelingTools_StaticRegisterNativesFEditPivotTarget;
	struct Z_Construct_UScriptStruct_FEditPivotTarget_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransformProxy_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TransformProxy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransformGizmo_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TransformGizmo;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEditPivotTarget_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/EditPivotTool.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FEditPivotTarget_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FEditPivotTarget>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEditPivotTarget_Statics::NewProp_TransformProxy_MetaData[] = {
		{ "ModuleRelativePath", "Public/EditPivotTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FEditPivotTarget_Statics::NewProp_TransformProxy = { "TransformProxy", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FEditPivotTarget, TransformProxy), Z_Construct_UClass_UTransformProxy_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FEditPivotTarget_Statics::NewProp_TransformProxy_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEditPivotTarget_Statics::NewProp_TransformProxy_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEditPivotTarget_Statics::NewProp_TransformGizmo_MetaData[] = {
		{ "ModuleRelativePath", "Public/EditPivotTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FEditPivotTarget_Statics::NewProp_TransformGizmo = { "TransformGizmo", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FEditPivotTarget, TransformGizmo), Z_Construct_UClass_UTransformGizmo_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FEditPivotTarget_Statics::NewProp_TransformGizmo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEditPivotTarget_Statics::NewProp_TransformGizmo_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FEditPivotTarget_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEditPivotTarget_Statics::NewProp_TransformProxy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEditPivotTarget_Statics::NewProp_TransformGizmo,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FEditPivotTarget_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
		nullptr,
		&NewStructOps,
		"EditPivotTarget",
		sizeof(FEditPivotTarget),
		alignof(FEditPivotTarget),
		Z_Construct_UScriptStruct_FEditPivotTarget_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEditPivotTarget_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FEditPivotTarget_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEditPivotTarget_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FEditPivotTarget()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FEditPivotTarget_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("EditPivotTarget"), sizeof(FEditPivotTarget), Get_Z_Construct_UScriptStruct_FEditPivotTarget_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FEditPivotTarget_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FEditPivotTarget_Hash() { return 3525756774U; }
	void UEditPivotToolBuilder::StaticRegisterNativesUEditPivotToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UEditPivotToolBuilder_NoRegister()
	{
		return UEditPivotToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UEditPivotToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEditPivotToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditPivotToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "EditPivotTool.h" },
		{ "ModuleRelativePath", "Public/EditPivotTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEditPivotToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEditPivotToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEditPivotToolBuilder_Statics::ClassParams = {
		&UEditPivotToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UEditPivotToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEditPivotToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEditPivotToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEditPivotToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEditPivotToolBuilder, 130902834);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UEditPivotToolBuilder>()
	{
		return UEditPivotToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEditPivotToolBuilder(Z_Construct_UClass_UEditPivotToolBuilder, &UEditPivotToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UEditPivotToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEditPivotToolBuilder);
	void UEditPivotToolProperties::StaticRegisterNativesUEditPivotToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UEditPivotToolProperties_NoRegister()
	{
		return UEditPivotToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UEditPivotToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableSnapDragging_MetaData[];
#endif
		static void NewProp_bEnableSnapDragging_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableSnapDragging;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_RotationMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RotationMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_RotationMode;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEditPivotToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditPivotToolProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Standard properties of the Transform Meshes operation\n */" },
		{ "IncludePath", "EditPivotTool.h" },
		{ "ModuleRelativePath", "Public/EditPivotTool.h" },
		{ "ToolTip", "Standard properties of the Transform Meshes operation" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditPivotToolProperties_Statics::NewProp_bEnableSnapDragging_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** When enabled, click-drag to reposition the Pivot */" },
		{ "ModuleRelativePath", "Public/EditPivotTool.h" },
		{ "ToolTip", "When enabled, click-drag to reposition the Pivot" },
	};
#endif
	void Z_Construct_UClass_UEditPivotToolProperties_Statics::NewProp_bEnableSnapDragging_SetBit(void* Obj)
	{
		((UEditPivotToolProperties*)Obj)->bEnableSnapDragging = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UEditPivotToolProperties_Statics::NewProp_bEnableSnapDragging = { "bEnableSnapDragging", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UEditPivotToolProperties), &Z_Construct_UClass_UEditPivotToolProperties_Statics::NewProp_bEnableSnapDragging_SetBit, METADATA_PARAMS(Z_Construct_UClass_UEditPivotToolProperties_Statics::NewProp_bEnableSnapDragging_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditPivotToolProperties_Statics::NewProp_bEnableSnapDragging_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UEditPivotToolProperties_Statics::NewProp_RotationMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditPivotToolProperties_Statics::NewProp_RotationMode_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** When Snap-Dragging, align source and target normals */" },
		{ "EditCondition", "bEnableSnapDragging == true" },
		{ "ModuleRelativePath", "Public/EditPivotTool.h" },
		{ "ToolTip", "When Snap-Dragging, align source and target normals" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UEditPivotToolProperties_Statics::NewProp_RotationMode = { "RotationMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEditPivotToolProperties, RotationMode), Z_Construct_UEnum_MeshModelingTools_EEditPivotSnapDragRotationMode, METADATA_PARAMS(Z_Construct_UClass_UEditPivotToolProperties_Statics::NewProp_RotationMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditPivotToolProperties_Statics::NewProp_RotationMode_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UEditPivotToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditPivotToolProperties_Statics::NewProp_bEnableSnapDragging,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditPivotToolProperties_Statics::NewProp_RotationMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditPivotToolProperties_Statics::NewProp_RotationMode,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEditPivotToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEditPivotToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEditPivotToolProperties_Statics::ClassParams = {
		&UEditPivotToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UEditPivotToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UEditPivotToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UEditPivotToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEditPivotToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEditPivotToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEditPivotToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEditPivotToolProperties, 79524920);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UEditPivotToolProperties>()
	{
		return UEditPivotToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEditPivotToolProperties(Z_Construct_UClass_UEditPivotToolProperties, &UEditPivotToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UEditPivotToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEditPivotToolProperties);
	DEFINE_FUNCTION(UEditPivotToolActionPropertySet::execBack)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Back();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditPivotToolActionPropertySet::execFront)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Front();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditPivotToolActionPropertySet::execRight)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Right();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditPivotToolActionPropertySet::execLeft)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Left();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditPivotToolActionPropertySet::execTop)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Top();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditPivotToolActionPropertySet::execBottom)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Bottom();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditPivotToolActionPropertySet::execCenter)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Center();
		P_NATIVE_END;
	}
	void UEditPivotToolActionPropertySet::StaticRegisterNativesUEditPivotToolActionPropertySet()
	{
		UClass* Class = UEditPivotToolActionPropertySet::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Back", &UEditPivotToolActionPropertySet::execBack },
			{ "Bottom", &UEditPivotToolActionPropertySet::execBottom },
			{ "Center", &UEditPivotToolActionPropertySet::execCenter },
			{ "Front", &UEditPivotToolActionPropertySet::execFront },
			{ "Left", &UEditPivotToolActionPropertySet::execLeft },
			{ "Right", &UEditPivotToolActionPropertySet::execRight },
			{ "Top", &UEditPivotToolActionPropertySet::execTop },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UEditPivotToolActionPropertySet_Back_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditPivotToolActionPropertySet_Back_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "BoxPositions" },
		{ "DisplayPriority", "4" },
		{ "ModuleRelativePath", "Public/EditPivotTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditPivotToolActionPropertySet_Back_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditPivotToolActionPropertySet, nullptr, "Back", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditPivotToolActionPropertySet_Back_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditPivotToolActionPropertySet_Back_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditPivotToolActionPropertySet_Back()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditPivotToolActionPropertySet_Back_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditPivotToolActionPropertySet_Bottom_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditPivotToolActionPropertySet_Bottom_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "BoxPositions" },
		{ "DisplayPriority", "2" },
		{ "ModuleRelativePath", "Public/EditPivotTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditPivotToolActionPropertySet_Bottom_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditPivotToolActionPropertySet, nullptr, "Bottom", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditPivotToolActionPropertySet_Bottom_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditPivotToolActionPropertySet_Bottom_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditPivotToolActionPropertySet_Bottom()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditPivotToolActionPropertySet_Bottom_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditPivotToolActionPropertySet_Center_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditPivotToolActionPropertySet_Center_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "BoxPositions" },
		{ "DisplayPriority", "1" },
		{ "ModuleRelativePath", "Public/EditPivotTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditPivotToolActionPropertySet_Center_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditPivotToolActionPropertySet, nullptr, "Center", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditPivotToolActionPropertySet_Center_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditPivotToolActionPropertySet_Center_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditPivotToolActionPropertySet_Center()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditPivotToolActionPropertySet_Center_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditPivotToolActionPropertySet_Front_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditPivotToolActionPropertySet_Front_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "BoxPositions" },
		{ "DisplayPriority", "4" },
		{ "ModuleRelativePath", "Public/EditPivotTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditPivotToolActionPropertySet_Front_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditPivotToolActionPropertySet, nullptr, "Front", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditPivotToolActionPropertySet_Front_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditPivotToolActionPropertySet_Front_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditPivotToolActionPropertySet_Front()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditPivotToolActionPropertySet_Front_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditPivotToolActionPropertySet_Left_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditPivotToolActionPropertySet_Left_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "BoxPositions" },
		{ "DisplayPriority", "3" },
		{ "ModuleRelativePath", "Public/EditPivotTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditPivotToolActionPropertySet_Left_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditPivotToolActionPropertySet, nullptr, "Left", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditPivotToolActionPropertySet_Left_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditPivotToolActionPropertySet_Left_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditPivotToolActionPropertySet_Left()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditPivotToolActionPropertySet_Left_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditPivotToolActionPropertySet_Right_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditPivotToolActionPropertySet_Right_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "BoxPositions" },
		{ "DisplayPriority", "3" },
		{ "ModuleRelativePath", "Public/EditPivotTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditPivotToolActionPropertySet_Right_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditPivotToolActionPropertySet, nullptr, "Right", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditPivotToolActionPropertySet_Right_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditPivotToolActionPropertySet_Right_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditPivotToolActionPropertySet_Right()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditPivotToolActionPropertySet_Right_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditPivotToolActionPropertySet_Top_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditPivotToolActionPropertySet_Top_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "BoxPositions" },
		{ "DisplayPriority", "2" },
		{ "ModuleRelativePath", "Public/EditPivotTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditPivotToolActionPropertySet_Top_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditPivotToolActionPropertySet, nullptr, "Top", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditPivotToolActionPropertySet_Top_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditPivotToolActionPropertySet_Top_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditPivotToolActionPropertySet_Top()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditPivotToolActionPropertySet_Top_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UEditPivotToolActionPropertySet_NoRegister()
	{
		return UEditPivotToolActionPropertySet::StaticClass();
	}
	struct Z_Construct_UClass_UEditPivotToolActionPropertySet_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseWorldBox_MetaData[];
#endif
		static void NewProp_bUseWorldBox_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseWorldBox;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEditPivotToolActionPropertySet_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UEditPivotToolActionPropertySet_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UEditPivotToolActionPropertySet_Back, "Back" }, // 1891504819
		{ &Z_Construct_UFunction_UEditPivotToolActionPropertySet_Bottom, "Bottom" }, // 1896216951
		{ &Z_Construct_UFunction_UEditPivotToolActionPropertySet_Center, "Center" }, // 1152559106
		{ &Z_Construct_UFunction_UEditPivotToolActionPropertySet_Front, "Front" }, // 1977974025
		{ &Z_Construct_UFunction_UEditPivotToolActionPropertySet_Left, "Left" }, // 3611559623
		{ &Z_Construct_UFunction_UEditPivotToolActionPropertySet_Right, "Right" }, // 1424902161
		{ &Z_Construct_UFunction_UEditPivotToolActionPropertySet_Top, "Top" }, // 148700495
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditPivotToolActionPropertySet_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "EditPivotTool.h" },
		{ "ModuleRelativePath", "Public/EditPivotTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditPivotToolActionPropertySet_Statics::NewProp_bUseWorldBox_MetaData[] = {
		{ "Category", "BoxPositions" },
		{ "Comment", "/** Use the World-Space Bounding Box of the target object, instead of the Object-space Bounding Box */" },
		{ "ModuleRelativePath", "Public/EditPivotTool.h" },
		{ "ToolTip", "Use the World-Space Bounding Box of the target object, instead of the Object-space Bounding Box" },
	};
#endif
	void Z_Construct_UClass_UEditPivotToolActionPropertySet_Statics::NewProp_bUseWorldBox_SetBit(void* Obj)
	{
		((UEditPivotToolActionPropertySet*)Obj)->bUseWorldBox = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UEditPivotToolActionPropertySet_Statics::NewProp_bUseWorldBox = { "bUseWorldBox", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UEditPivotToolActionPropertySet), &Z_Construct_UClass_UEditPivotToolActionPropertySet_Statics::NewProp_bUseWorldBox_SetBit, METADATA_PARAMS(Z_Construct_UClass_UEditPivotToolActionPropertySet_Statics::NewProp_bUseWorldBox_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditPivotToolActionPropertySet_Statics::NewProp_bUseWorldBox_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UEditPivotToolActionPropertySet_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditPivotToolActionPropertySet_Statics::NewProp_bUseWorldBox,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEditPivotToolActionPropertySet_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEditPivotToolActionPropertySet>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEditPivotToolActionPropertySet_Statics::ClassParams = {
		&UEditPivotToolActionPropertySet::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UEditPivotToolActionPropertySet_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UEditPivotToolActionPropertySet_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UEditPivotToolActionPropertySet_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEditPivotToolActionPropertySet_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEditPivotToolActionPropertySet()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEditPivotToolActionPropertySet_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEditPivotToolActionPropertySet, 4221783425);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UEditPivotToolActionPropertySet>()
	{
		return UEditPivotToolActionPropertySet::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEditPivotToolActionPropertySet(Z_Construct_UClass_UEditPivotToolActionPropertySet, &UEditPivotToolActionPropertySet::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UEditPivotToolActionPropertySet"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEditPivotToolActionPropertySet);
	void UEditPivotTool::StaticRegisterNativesUEditPivotTool()
	{
	}
	UClass* Z_Construct_UClass_UEditPivotTool_NoRegister()
	{
		return UEditPivotTool::StaticClass();
	}
	struct Z_Construct_UClass_UEditPivotTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransformProps_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TransformProps;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EditPivotActions_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_EditPivotActions;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ActiveGizmos_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActiveGizmos_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ActiveGizmos;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEditPivotTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMultiSelectionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditPivotTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "EditPivotTool.h" },
		{ "ModuleRelativePath", "Public/EditPivotTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditPivotTool_Statics::NewProp_TransformProps_MetaData[] = {
		{ "ModuleRelativePath", "Public/EditPivotTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UEditPivotTool_Statics::NewProp_TransformProps = { "TransformProps", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEditPivotTool, TransformProps), Z_Construct_UClass_UEditPivotToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UEditPivotTool_Statics::NewProp_TransformProps_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditPivotTool_Statics::NewProp_TransformProps_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditPivotTool_Statics::NewProp_EditPivotActions_MetaData[] = {
		{ "ModuleRelativePath", "Public/EditPivotTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UEditPivotTool_Statics::NewProp_EditPivotActions = { "EditPivotActions", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEditPivotTool, EditPivotActions), Z_Construct_UClass_UEditPivotToolActionPropertySet_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UEditPivotTool_Statics::NewProp_EditPivotActions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditPivotTool_Statics::NewProp_EditPivotActions_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UEditPivotTool_Statics::NewProp_ActiveGizmos_Inner = { "ActiveGizmos", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FEditPivotTarget, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditPivotTool_Statics::NewProp_ActiveGizmos_MetaData[] = {
		{ "ModuleRelativePath", "Public/EditPivotTool.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UEditPivotTool_Statics::NewProp_ActiveGizmos = { "ActiveGizmos", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEditPivotTool, ActiveGizmos), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UEditPivotTool_Statics::NewProp_ActiveGizmos_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditPivotTool_Statics::NewProp_ActiveGizmos_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UEditPivotTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditPivotTool_Statics::NewProp_TransformProps,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditPivotTool_Statics::NewProp_EditPivotActions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditPivotTool_Statics::NewProp_ActiveGizmos_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditPivotTool_Statics::NewProp_ActiveGizmos,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEditPivotTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEditPivotTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEditPivotTool_Statics::ClassParams = {
		&UEditPivotTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UEditPivotTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UEditPivotTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UEditPivotTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEditPivotTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEditPivotTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEditPivotTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEditPivotTool, 1317638055);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UEditPivotTool>()
	{
		return UEditPivotTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEditPivotTool(Z_Construct_UClass_UEditPivotTool, &UEditPivotTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UEditPivotTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEditPivotTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
