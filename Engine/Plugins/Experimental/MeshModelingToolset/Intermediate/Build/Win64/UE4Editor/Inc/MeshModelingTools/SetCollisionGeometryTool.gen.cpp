// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/Physics/SetCollisionGeometryTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSetCollisionGeometryTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EProjectedHullAxis();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_ECollisionGeometryType();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_ESetCollisionGeometryInputMode();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_USetCollisionGeometryToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_USetCollisionGeometryToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_USetCollisionGeometryToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_USetCollisionGeometryToolProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_ECollisionGeometryMode();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_USetCollisionGeometryTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_USetCollisionGeometryTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UMultiSelectionTool();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UCollisionGeometryVisualizationProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPhysicsObjectToolPropertySet_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPreviewGeometry_NoRegister();
// End Cross Module References
	static UEnum* EProjectedHullAxis_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EProjectedHullAxis, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EProjectedHullAxis"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EProjectedHullAxis>()
	{
		return EProjectedHullAxis_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EProjectedHullAxis(EProjectedHullAxis_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EProjectedHullAxis"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EProjectedHullAxis_Hash() { return 4104155312U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EProjectedHullAxis()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EProjectedHullAxis"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EProjectedHullAxis_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EProjectedHullAxis::X", (int64)EProjectedHullAxis::X },
				{ "EProjectedHullAxis::Y", (int64)EProjectedHullAxis::Y },
				{ "EProjectedHullAxis::Z", (int64)EProjectedHullAxis::Z },
				{ "EProjectedHullAxis::SmallestBoxDimension", (int64)EProjectedHullAxis::SmallestBoxDimension },
				{ "EProjectedHullAxis::SmallestVolume", (int64)EProjectedHullAxis::SmallestVolume },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/Physics/SetCollisionGeometryTool.h" },
				{ "SmallestBoxDimension.Name", "EProjectedHullAxis::SmallestBoxDimension" },
				{ "SmallestVolume.Name", "EProjectedHullAxis::SmallestVolume" },
				{ "X.Name", "EProjectedHullAxis::X" },
				{ "Y.Name", "EProjectedHullAxis::Y" },
				{ "Z.Name", "EProjectedHullAxis::Z" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EProjectedHullAxis",
				"EProjectedHullAxis",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ECollisionGeometryType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_ECollisionGeometryType, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("ECollisionGeometryType"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<ECollisionGeometryType>()
	{
		return ECollisionGeometryType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ECollisionGeometryType(ECollisionGeometryType_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("ECollisionGeometryType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_ECollisionGeometryType_Hash() { return 917561079U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_ECollisionGeometryType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ECollisionGeometryType"), 0, Get_Z_Construct_UEnum_MeshModelingTools_ECollisionGeometryType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ECollisionGeometryType::KeepExisting", (int64)ECollisionGeometryType::KeepExisting },
				{ "ECollisionGeometryType::AlignedBoxes", (int64)ECollisionGeometryType::AlignedBoxes },
				{ "ECollisionGeometryType::OrientedBoxes", (int64)ECollisionGeometryType::OrientedBoxes },
				{ "ECollisionGeometryType::MinimalSpheres", (int64)ECollisionGeometryType::MinimalSpheres },
				{ "ECollisionGeometryType::Capsules", (int64)ECollisionGeometryType::Capsules },
				{ "ECollisionGeometryType::ConvexHulls", (int64)ECollisionGeometryType::ConvexHulls },
				{ "ECollisionGeometryType::SweptHulls", (int64)ECollisionGeometryType::SweptHulls },
				{ "ECollisionGeometryType::MinVolume", (int64)ECollisionGeometryType::MinVolume },
				{ "ECollisionGeometryType::None", (int64)ECollisionGeometryType::None },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "AlignedBoxes.Name", "ECollisionGeometryType::AlignedBoxes" },
				{ "Capsules.Name", "ECollisionGeometryType::Capsules" },
				{ "ConvexHulls.Name", "ECollisionGeometryType::ConvexHulls" },
				{ "KeepExisting.Name", "ECollisionGeometryType::KeepExisting" },
				{ "MinimalSpheres.Name", "ECollisionGeometryType::MinimalSpheres" },
				{ "MinVolume.Name", "ECollisionGeometryType::MinVolume" },
				{ "ModuleRelativePath", "Public/Physics/SetCollisionGeometryTool.h" },
				{ "None.Name", "ECollisionGeometryType::None" },
				{ "OrientedBoxes.Name", "ECollisionGeometryType::OrientedBoxes" },
				{ "SweptHulls.Name", "ECollisionGeometryType::SweptHulls" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"ECollisionGeometryType",
				"ECollisionGeometryType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ESetCollisionGeometryInputMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_ESetCollisionGeometryInputMode, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("ESetCollisionGeometryInputMode"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<ESetCollisionGeometryInputMode>()
	{
		return ESetCollisionGeometryInputMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ESetCollisionGeometryInputMode(ESetCollisionGeometryInputMode_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("ESetCollisionGeometryInputMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_ESetCollisionGeometryInputMode_Hash() { return 2553122230U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_ESetCollisionGeometryInputMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ESetCollisionGeometryInputMode"), 0, Get_Z_Construct_UEnum_MeshModelingTools_ESetCollisionGeometryInputMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ESetCollisionGeometryInputMode::CombineAll", (int64)ESetCollisionGeometryInputMode::CombineAll },
				{ "ESetCollisionGeometryInputMode::PerInputObject", (int64)ESetCollisionGeometryInputMode::PerInputObject },
				{ "ESetCollisionGeometryInputMode::PerMeshComponent", (int64)ESetCollisionGeometryInputMode::PerMeshComponent },
				{ "ESetCollisionGeometryInputMode::PerMeshGroup", (int64)ESetCollisionGeometryInputMode::PerMeshGroup },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "CombineAll.Name", "ESetCollisionGeometryInputMode::CombineAll" },
				{ "ModuleRelativePath", "Public/Physics/SetCollisionGeometryTool.h" },
				{ "PerInputObject.Name", "ESetCollisionGeometryInputMode::PerInputObject" },
				{ "PerMeshComponent.Name", "ESetCollisionGeometryInputMode::PerMeshComponent" },
				{ "PerMeshGroup.Name", "ESetCollisionGeometryInputMode::PerMeshGroup" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"ESetCollisionGeometryInputMode",
				"ESetCollisionGeometryInputMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void USetCollisionGeometryToolBuilder::StaticRegisterNativesUSetCollisionGeometryToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_USetCollisionGeometryToolBuilder_NoRegister()
	{
		return USetCollisionGeometryToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_USetCollisionGeometryToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USetCollisionGeometryToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USetCollisionGeometryToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Physics/SetCollisionGeometryTool.h" },
		{ "ModuleRelativePath", "Public/Physics/SetCollisionGeometryTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USetCollisionGeometryToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USetCollisionGeometryToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USetCollisionGeometryToolBuilder_Statics::ClassParams = {
		&USetCollisionGeometryToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_USetCollisionGeometryToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USetCollisionGeometryToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USetCollisionGeometryToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USetCollisionGeometryToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USetCollisionGeometryToolBuilder, 4284441108);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<USetCollisionGeometryToolBuilder>()
	{
		return USetCollisionGeometryToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USetCollisionGeometryToolBuilder(Z_Construct_UClass_USetCollisionGeometryToolBuilder, &USetCollisionGeometryToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("USetCollisionGeometryToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USetCollisionGeometryToolBuilder);
	void USetCollisionGeometryToolProperties::StaticRegisterNativesUSetCollisionGeometryToolProperties()
	{
	}
	UClass* Z_Construct_UClass_USetCollisionGeometryToolProperties_NoRegister()
	{
		return USetCollisionGeometryToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_GeometryType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GeometryType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_GeometryType;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_InputMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InputMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseWorldSpace_MetaData[];
#endif
		static void NewProp_bUseWorldSpace_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseWorldSpace;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRemoveContained_MetaData[];
#endif
		static void NewProp_bRemoveContained_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRemoveContained;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableMaxCount_MetaData[];
#endif
		static void NewProp_bEnableMaxCount_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableMaxCount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MaxCount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MinThickness_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MinThickness;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDetectBoxes_MetaData[];
#endif
		static void NewProp_bDetectBoxes_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDetectBoxes;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDetectSpheres_MetaData[];
#endif
		static void NewProp_bDetectSpheres_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDetectSpheres;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDetectCapsules_MetaData[];
#endif
		static void NewProp_bDetectCapsules_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDetectCapsules;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSimplifyHulls_MetaData[];
#endif
		static void NewProp_bSimplifyHulls_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSimplifyHulls;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HullTargetFaceCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_HullTargetFaceCount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSimplifyPolygons_MetaData[];
#endif
		static void NewProp_bSimplifyPolygons_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSimplifyPolygons;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HullTolerance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HullTolerance;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_SweepAxis_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SweepAxis_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_SweepAxis;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAppendToExisting_MetaData[];
#endif
		static void NewProp_bAppendToExisting_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAppendToExisting;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_SetCollisionType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SetCollisionType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_SetCollisionType;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Physics/SetCollisionGeometryTool.h" },
		{ "ModuleRelativePath", "Public/Physics/SetCollisionGeometryTool.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_GeometryType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_GeometryType_MetaData[] = {
		{ "Category", "Options" },
		{ "ModuleRelativePath", "Public/Physics/SetCollisionGeometryTool.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_GeometryType = { "GeometryType", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USetCollisionGeometryToolProperties, GeometryType), Z_Construct_UEnum_MeshModelingTools_ECollisionGeometryType, METADATA_PARAMS(Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_GeometryType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_GeometryType_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_InputMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_InputMode_MetaData[] = {
		{ "Category", "Options" },
		{ "ModuleRelativePath", "Public/Physics/SetCollisionGeometryTool.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_InputMode = { "InputMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USetCollisionGeometryToolProperties, InputMode), Z_Construct_UEnum_MeshModelingTools_ESetCollisionGeometryInputMode, METADATA_PARAMS(Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_InputMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_InputMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bUseWorldSpace_MetaData[] = {
		{ "Category", "Options" },
		{ "ModuleRelativePath", "Public/Physics/SetCollisionGeometryTool.h" },
	};
#endif
	void Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bUseWorldSpace_SetBit(void* Obj)
	{
		((USetCollisionGeometryToolProperties*)Obj)->bUseWorldSpace = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bUseWorldSpace = { "bUseWorldSpace", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(USetCollisionGeometryToolProperties), &Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bUseWorldSpace_SetBit, METADATA_PARAMS(Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bUseWorldSpace_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bUseWorldSpace_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bRemoveContained_MetaData[] = {
		{ "Category", "Options" },
		{ "ModuleRelativePath", "Public/Physics/SetCollisionGeometryTool.h" },
	};
#endif
	void Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bRemoveContained_SetBit(void* Obj)
	{
		((USetCollisionGeometryToolProperties*)Obj)->bRemoveContained = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bRemoveContained = { "bRemoveContained", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(USetCollisionGeometryToolProperties), &Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bRemoveContained_SetBit, METADATA_PARAMS(Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bRemoveContained_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bRemoveContained_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bEnableMaxCount_MetaData[] = {
		{ "Category", "Options" },
		{ "ModuleRelativePath", "Public/Physics/SetCollisionGeometryTool.h" },
	};
#endif
	void Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bEnableMaxCount_SetBit(void* Obj)
	{
		((USetCollisionGeometryToolProperties*)Obj)->bEnableMaxCount = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bEnableMaxCount = { "bEnableMaxCount", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(USetCollisionGeometryToolProperties), &Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bEnableMaxCount_SetBit, METADATA_PARAMS(Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bEnableMaxCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bEnableMaxCount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_MaxCount_MetaData[] = {
		{ "Category", "Options" },
		{ "ClampMax", "9999999" },
		{ "ClampMin", "4" },
		{ "EditCondition", "bEnableMaxCount" },
		{ "ModuleRelativePath", "Public/Physics/SetCollisionGeometryTool.h" },
		{ "UIMax", "100" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_MaxCount = { "MaxCount", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USetCollisionGeometryToolProperties, MaxCount), METADATA_PARAMS(Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_MaxCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_MaxCount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_MinThickness_MetaData[] = {
		{ "Category", "Options" },
		{ "ModuleRelativePath", "Public/Physics/SetCollisionGeometryTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_MinThickness = { "MinThickness", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USetCollisionGeometryToolProperties, MinThickness), METADATA_PARAMS(Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_MinThickness_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_MinThickness_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bDetectBoxes_MetaData[] = {
		{ "Category", "AutoDetect" },
		{ "ModuleRelativePath", "Public/Physics/SetCollisionGeometryTool.h" },
	};
#endif
	void Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bDetectBoxes_SetBit(void* Obj)
	{
		((USetCollisionGeometryToolProperties*)Obj)->bDetectBoxes = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bDetectBoxes = { "bDetectBoxes", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(USetCollisionGeometryToolProperties), &Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bDetectBoxes_SetBit, METADATA_PARAMS(Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bDetectBoxes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bDetectBoxes_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bDetectSpheres_MetaData[] = {
		{ "Category", "AutoDetect" },
		{ "ModuleRelativePath", "Public/Physics/SetCollisionGeometryTool.h" },
	};
#endif
	void Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bDetectSpheres_SetBit(void* Obj)
	{
		((USetCollisionGeometryToolProperties*)Obj)->bDetectSpheres = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bDetectSpheres = { "bDetectSpheres", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(USetCollisionGeometryToolProperties), &Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bDetectSpheres_SetBit, METADATA_PARAMS(Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bDetectSpheres_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bDetectSpheres_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bDetectCapsules_MetaData[] = {
		{ "Category", "AutoDetect" },
		{ "ModuleRelativePath", "Public/Physics/SetCollisionGeometryTool.h" },
	};
#endif
	void Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bDetectCapsules_SetBit(void* Obj)
	{
		((USetCollisionGeometryToolProperties*)Obj)->bDetectCapsules = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bDetectCapsules = { "bDetectCapsules", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(USetCollisionGeometryToolProperties), &Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bDetectCapsules_SetBit, METADATA_PARAMS(Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bDetectCapsules_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bDetectCapsules_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bSimplifyHulls_MetaData[] = {
		{ "Category", "ConvexHulls" },
		{ "EditCondition", "GeometryType == ECollisionGeometryType::ConvexHulls" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/Physics/SetCollisionGeometryTool.h" },
	};
#endif
	void Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bSimplifyHulls_SetBit(void* Obj)
	{
		((USetCollisionGeometryToolProperties*)Obj)->bSimplifyHulls = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bSimplifyHulls = { "bSimplifyHulls", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(USetCollisionGeometryToolProperties), &Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bSimplifyHulls_SetBit, METADATA_PARAMS(Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bSimplifyHulls_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bSimplifyHulls_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_HullTargetFaceCount_MetaData[] = {
		{ "Category", "ConvexHulls" },
		{ "ClampMax", "9999999" },
		{ "ClampMin", "4" },
		{ "EditCondition", "GeometryType == ECollisionGeometryType::ConvexHulls" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/Physics/SetCollisionGeometryTool.h" },
		{ "UIMax", "100" },
		{ "UIMin", "4" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_HullTargetFaceCount = { "HullTargetFaceCount", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USetCollisionGeometryToolProperties, HullTargetFaceCount), METADATA_PARAMS(Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_HullTargetFaceCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_HullTargetFaceCount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bSimplifyPolygons_MetaData[] = {
		{ "Category", "SweptHulls" },
		{ "EditCondition", "GeometryType == ECollisionGeometryType::SweptHulls" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/Physics/SetCollisionGeometryTool.h" },
	};
#endif
	void Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bSimplifyPolygons_SetBit(void* Obj)
	{
		((USetCollisionGeometryToolProperties*)Obj)->bSimplifyPolygons = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bSimplifyPolygons = { "bSimplifyPolygons", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(USetCollisionGeometryToolProperties), &Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bSimplifyPolygons_SetBit, METADATA_PARAMS(Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bSimplifyPolygons_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bSimplifyPolygons_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_HullTolerance_MetaData[] = {
		{ "Category", "SweptHulls" },
		{ "ClampMax", "100000" },
		{ "ClampMin", "0" },
		{ "EditCondition", "GeometryType == ECollisionGeometryType::SweptHulls" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/Physics/SetCollisionGeometryTool.h" },
		{ "UIMax", "10" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_HullTolerance = { "HullTolerance", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USetCollisionGeometryToolProperties, HullTolerance), METADATA_PARAMS(Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_HullTolerance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_HullTolerance_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_SweepAxis_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_SweepAxis_MetaData[] = {
		{ "Category", "SweptHulls" },
		{ "ClampMax", "100000" },
		{ "ClampMin", "0" },
		{ "EditCondition", "GeometryType == ECollisionGeometryType::SweptHulls" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/Physics/SetCollisionGeometryTool.h" },
		{ "UIMax", "10" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_SweepAxis = { "SweepAxis", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USetCollisionGeometryToolProperties, SweepAxis), Z_Construct_UEnum_MeshModelingTools_EProjectedHullAxis, METADATA_PARAMS(Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_SweepAxis_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_SweepAxis_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bAppendToExisting_MetaData[] = {
		{ "Category", "OutputOptions" },
		{ "ModuleRelativePath", "Public/Physics/SetCollisionGeometryTool.h" },
	};
#endif
	void Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bAppendToExisting_SetBit(void* Obj)
	{
		((USetCollisionGeometryToolProperties*)Obj)->bAppendToExisting = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bAppendToExisting = { "bAppendToExisting", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(USetCollisionGeometryToolProperties), &Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bAppendToExisting_SetBit, METADATA_PARAMS(Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bAppendToExisting_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bAppendToExisting_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_SetCollisionType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_SetCollisionType_MetaData[] = {
		{ "Category", "OutputOptions" },
		{ "ModuleRelativePath", "Public/Physics/SetCollisionGeometryTool.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_SetCollisionType = { "SetCollisionType", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USetCollisionGeometryToolProperties, SetCollisionType), Z_Construct_UEnum_MeshModelingTools_ECollisionGeometryMode, METADATA_PARAMS(Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_SetCollisionType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_SetCollisionType_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_GeometryType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_GeometryType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_InputMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_InputMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bUseWorldSpace,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bRemoveContained,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bEnableMaxCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_MaxCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_MinThickness,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bDetectBoxes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bDetectSpheres,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bDetectCapsules,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bSimplifyHulls,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_HullTargetFaceCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bSimplifyPolygons,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_HullTolerance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_SweepAxis_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_SweepAxis,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_bAppendToExisting,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_SetCollisionType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::NewProp_SetCollisionType,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USetCollisionGeometryToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::ClassParams = {
		&USetCollisionGeometryToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USetCollisionGeometryToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USetCollisionGeometryToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USetCollisionGeometryToolProperties, 2266742919);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<USetCollisionGeometryToolProperties>()
	{
		return USetCollisionGeometryToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USetCollisionGeometryToolProperties(Z_Construct_UClass_USetCollisionGeometryToolProperties, &USetCollisionGeometryToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("USetCollisionGeometryToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USetCollisionGeometryToolProperties);
	void USetCollisionGeometryTool::StaticRegisterNativesUSetCollisionGeometryTool()
	{
	}
	UClass* Z_Construct_UClass_USetCollisionGeometryTool_NoRegister()
	{
		return USetCollisionGeometryTool::StaticClass();
	}
	struct Z_Construct_UClass_USetCollisionGeometryTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Settings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Settings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VizSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_VizSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CollisionProps_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CollisionProps;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LineMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LineMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviewGeom_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviewGeom;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USetCollisionGeometryTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMultiSelectionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USetCollisionGeometryTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Mesh Inspector Tool for visualizing mesh information\n */" },
		{ "IncludePath", "Physics/SetCollisionGeometryTool.h" },
		{ "ModuleRelativePath", "Public/Physics/SetCollisionGeometryTool.h" },
		{ "ToolTip", "Mesh Inspector Tool for visualizing mesh information" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USetCollisionGeometryTool_Statics::NewProp_Settings_MetaData[] = {
		{ "ModuleRelativePath", "Public/Physics/SetCollisionGeometryTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USetCollisionGeometryTool_Statics::NewProp_Settings = { "Settings", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USetCollisionGeometryTool, Settings), Z_Construct_UClass_USetCollisionGeometryToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USetCollisionGeometryTool_Statics::NewProp_Settings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USetCollisionGeometryTool_Statics::NewProp_Settings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USetCollisionGeometryTool_Statics::NewProp_VizSettings_MetaData[] = {
		{ "ModuleRelativePath", "Public/Physics/SetCollisionGeometryTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USetCollisionGeometryTool_Statics::NewProp_VizSettings = { "VizSettings", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USetCollisionGeometryTool, VizSettings), Z_Construct_UClass_UCollisionGeometryVisualizationProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USetCollisionGeometryTool_Statics::NewProp_VizSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USetCollisionGeometryTool_Statics::NewProp_VizSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USetCollisionGeometryTool_Statics::NewProp_CollisionProps_MetaData[] = {
		{ "ModuleRelativePath", "Public/Physics/SetCollisionGeometryTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USetCollisionGeometryTool_Statics::NewProp_CollisionProps = { "CollisionProps", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USetCollisionGeometryTool, CollisionProps), Z_Construct_UClass_UPhysicsObjectToolPropertySet_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USetCollisionGeometryTool_Statics::NewProp_CollisionProps_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USetCollisionGeometryTool_Statics::NewProp_CollisionProps_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USetCollisionGeometryTool_Statics::NewProp_LineMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Public/Physics/SetCollisionGeometryTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USetCollisionGeometryTool_Statics::NewProp_LineMaterial = { "LineMaterial", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USetCollisionGeometryTool, LineMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USetCollisionGeometryTool_Statics::NewProp_LineMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USetCollisionGeometryTool_Statics::NewProp_LineMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USetCollisionGeometryTool_Statics::NewProp_PreviewGeom_MetaData[] = {
		{ "ModuleRelativePath", "Public/Physics/SetCollisionGeometryTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USetCollisionGeometryTool_Statics::NewProp_PreviewGeom = { "PreviewGeom", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USetCollisionGeometryTool, PreviewGeom), Z_Construct_UClass_UPreviewGeometry_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USetCollisionGeometryTool_Statics::NewProp_PreviewGeom_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USetCollisionGeometryTool_Statics::NewProp_PreviewGeom_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USetCollisionGeometryTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USetCollisionGeometryTool_Statics::NewProp_Settings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USetCollisionGeometryTool_Statics::NewProp_VizSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USetCollisionGeometryTool_Statics::NewProp_CollisionProps,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USetCollisionGeometryTool_Statics::NewProp_LineMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USetCollisionGeometryTool_Statics::NewProp_PreviewGeom,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USetCollisionGeometryTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USetCollisionGeometryTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USetCollisionGeometryTool_Statics::ClassParams = {
		&USetCollisionGeometryTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USetCollisionGeometryTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USetCollisionGeometryTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_USetCollisionGeometryTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USetCollisionGeometryTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USetCollisionGeometryTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USetCollisionGeometryTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USetCollisionGeometryTool, 499489054);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<USetCollisionGeometryTool>()
	{
		return USetCollisionGeometryTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USetCollisionGeometryTool(Z_Construct_UClass_USetCollisionGeometryTool, &USetCollisionGeometryTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("USetCollisionGeometryTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USetCollisionGeometryTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
