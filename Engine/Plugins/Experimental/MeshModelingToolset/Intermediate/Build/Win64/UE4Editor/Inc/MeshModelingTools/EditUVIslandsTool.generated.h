// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLS_EditUVIslandsTool_generated_h
#error "EditUVIslandsTool.generated.h already included, missing '#pragma once' in EditUVIslandsTool.h"
#endif
#define MESHMODELINGTOOLS_EditUVIslandsTool_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_28_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_28_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_28_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_28_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUEditUVIslandsToolBuilder(); \
	friend struct Z_Construct_UClass_UEditUVIslandsToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UEditUVIslandsToolBuilder, UMeshSurfacePointToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UEditUVIslandsToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_28_INCLASS \
private: \
	static void StaticRegisterNativesUEditUVIslandsToolBuilder(); \
	friend struct Z_Construct_UClass_UEditUVIslandsToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UEditUVIslandsToolBuilder, UMeshSurfacePointToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UEditUVIslandsToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_28_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEditUVIslandsToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEditUVIslandsToolBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEditUVIslandsToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEditUVIslandsToolBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEditUVIslandsToolBuilder(UEditUVIslandsToolBuilder&&); \
	NO_API UEditUVIslandsToolBuilder(const UEditUVIslandsToolBuilder&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_28_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEditUVIslandsToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEditUVIslandsToolBuilder(UEditUVIslandsToolBuilder&&); \
	NO_API UEditUVIslandsToolBuilder(const UEditUVIslandsToolBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEditUVIslandsToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEditUVIslandsToolBuilder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEditUVIslandsToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_28_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_25_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_28_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_28_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_28_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_28_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_28_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_28_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_28_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_28_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_28_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_28_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_28_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_28_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UEditUVIslandsToolBuilder>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_62_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_62_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_62_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_62_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUEditUVIslandsTool(); \
	friend struct Z_Construct_UClass_UEditUVIslandsTool_Statics; \
public: \
	DECLARE_CLASS(UEditUVIslandsTool, UMeshSurfacePointTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UEditUVIslandsTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_62_INCLASS \
private: \
	static void StaticRegisterNativesUEditUVIslandsTool(); \
	friend struct Z_Construct_UClass_UEditUVIslandsTool_Statics; \
public: \
	DECLARE_CLASS(UEditUVIslandsTool, UMeshSurfacePointTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UEditUVIslandsTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_62_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEditUVIslandsTool(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEditUVIslandsTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEditUVIslandsTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEditUVIslandsTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEditUVIslandsTool(UEditUVIslandsTool&&); \
	NO_API UEditUVIslandsTool(const UEditUVIslandsTool&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_62_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEditUVIslandsTool(UEditUVIslandsTool&&); \
	NO_API UEditUVIslandsTool(const UEditUVIslandsTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEditUVIslandsTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEditUVIslandsTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UEditUVIslandsTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_62_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__DynamicMeshComponent() { return STRUCT_OFFSET(UEditUVIslandsTool, DynamicMeshComponent); } \
	FORCEINLINE static uint32 __PPO__SelectionMechanic() { return STRUCT_OFFSET(UEditUVIslandsTool, SelectionMechanic); } \
	FORCEINLINE static uint32 __PPO__MultiTransformer() { return STRUCT_OFFSET(UEditUVIslandsTool, MultiTransformer); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_59_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_62_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_62_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_62_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_62_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_62_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_62_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_62_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_62_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_62_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_62_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_62_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h_62_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UEditUVIslandsTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_EditUVIslandsTool_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
