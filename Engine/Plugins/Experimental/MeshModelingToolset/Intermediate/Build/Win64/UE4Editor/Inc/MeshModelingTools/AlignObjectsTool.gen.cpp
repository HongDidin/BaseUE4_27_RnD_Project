// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/AlignObjectsTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAlignObjectsTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EAlignObjectsBoxPoint();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EAlignObjectsAlignToOptions();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EAlignObjectsAlignTypes();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UAlignObjectsToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UAlignObjectsToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UAlignObjectsToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UAlignObjectsToolProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UAlignObjectsTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UAlignObjectsTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UMultiSelectionTool();
// End Cross Module References
	static UEnum* EAlignObjectsBoxPoint_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EAlignObjectsBoxPoint, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EAlignObjectsBoxPoint"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EAlignObjectsBoxPoint>()
	{
		return EAlignObjectsBoxPoint_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EAlignObjectsBoxPoint(EAlignObjectsBoxPoint_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EAlignObjectsBoxPoint"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EAlignObjectsBoxPoint_Hash() { return 3624252011U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EAlignObjectsBoxPoint()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EAlignObjectsBoxPoint"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EAlignObjectsBoxPoint_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EAlignObjectsBoxPoint::Center", (int64)EAlignObjectsBoxPoint::Center },
				{ "EAlignObjectsBoxPoint::Bottom", (int64)EAlignObjectsBoxPoint::Bottom },
				{ "EAlignObjectsBoxPoint::Top", (int64)EAlignObjectsBoxPoint::Top },
				{ "EAlignObjectsBoxPoint::Left", (int64)EAlignObjectsBoxPoint::Left },
				{ "EAlignObjectsBoxPoint::Right", (int64)EAlignObjectsBoxPoint::Right },
				{ "EAlignObjectsBoxPoint::Front", (int64)EAlignObjectsBoxPoint::Front },
				{ "EAlignObjectsBoxPoint::Back", (int64)EAlignObjectsBoxPoint::Back },
				{ "EAlignObjectsBoxPoint::Min", (int64)EAlignObjectsBoxPoint::Min },
				{ "EAlignObjectsBoxPoint::Max", (int64)EAlignObjectsBoxPoint::Max },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Back.Name", "EAlignObjectsBoxPoint::Back" },
				{ "Bottom.Name", "EAlignObjectsBoxPoint::Bottom" },
				{ "Center.Name", "EAlignObjectsBoxPoint::Center" },
				{ "Front.Name", "EAlignObjectsBoxPoint::Front" },
				{ "Left.Name", "EAlignObjectsBoxPoint::Left" },
				{ "Max.Name", "EAlignObjectsBoxPoint::Max" },
				{ "Min.Name", "EAlignObjectsBoxPoint::Min" },
				{ "ModuleRelativePath", "Public/AlignObjectsTool.h" },
				{ "Right.Name", "EAlignObjectsBoxPoint::Right" },
				{ "Top.Name", "EAlignObjectsBoxPoint::Top" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EAlignObjectsBoxPoint",
				"EAlignObjectsBoxPoint",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EAlignObjectsAlignToOptions_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EAlignObjectsAlignToOptions, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EAlignObjectsAlignToOptions"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EAlignObjectsAlignToOptions>()
	{
		return EAlignObjectsAlignToOptions_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EAlignObjectsAlignToOptions(EAlignObjectsAlignToOptions_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EAlignObjectsAlignToOptions"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EAlignObjectsAlignToOptions_Hash() { return 3079754341U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EAlignObjectsAlignToOptions()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EAlignObjectsAlignToOptions"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EAlignObjectsAlignToOptions_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EAlignObjectsAlignToOptions::FirstSelected", (int64)EAlignObjectsAlignToOptions::FirstSelected },
				{ "EAlignObjectsAlignToOptions::LastSelected", (int64)EAlignObjectsAlignToOptions::LastSelected },
				{ "EAlignObjectsAlignToOptions::Combined", (int64)EAlignObjectsAlignToOptions::Combined },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Combined.Name", "EAlignObjectsAlignToOptions::Combined" },
				{ "FirstSelected.Name", "EAlignObjectsAlignToOptions::FirstSelected" },
				{ "LastSelected.Name", "EAlignObjectsAlignToOptions::LastSelected" },
				{ "ModuleRelativePath", "Public/AlignObjectsTool.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EAlignObjectsAlignToOptions",
				"EAlignObjectsAlignToOptions",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EAlignObjectsAlignTypes_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EAlignObjectsAlignTypes, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EAlignObjectsAlignTypes"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EAlignObjectsAlignTypes>()
	{
		return EAlignObjectsAlignTypes_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EAlignObjectsAlignTypes(EAlignObjectsAlignTypes_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EAlignObjectsAlignTypes"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EAlignObjectsAlignTypes_Hash() { return 3994612100U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EAlignObjectsAlignTypes()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EAlignObjectsAlignTypes"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EAlignObjectsAlignTypes_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EAlignObjectsAlignTypes::Pivots", (int64)EAlignObjectsAlignTypes::Pivots },
				{ "EAlignObjectsAlignTypes::BoundingBoxes", (int64)EAlignObjectsAlignTypes::BoundingBoxes },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BoundingBoxes.Name", "EAlignObjectsAlignTypes::BoundingBoxes" },
				{ "ModuleRelativePath", "Public/AlignObjectsTool.h" },
				{ "Pivots.Name", "EAlignObjectsAlignTypes::Pivots" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EAlignObjectsAlignTypes",
				"EAlignObjectsAlignTypes",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UAlignObjectsToolBuilder::StaticRegisterNativesUAlignObjectsToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UAlignObjectsToolBuilder_NoRegister()
	{
		return UAlignObjectsToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UAlignObjectsToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAlignObjectsToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAlignObjectsToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "AlignObjectsTool.h" },
		{ "ModuleRelativePath", "Public/AlignObjectsTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAlignObjectsToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAlignObjectsToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAlignObjectsToolBuilder_Statics::ClassParams = {
		&UAlignObjectsToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UAlignObjectsToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAlignObjectsToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAlignObjectsToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAlignObjectsToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAlignObjectsToolBuilder, 2391097205);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UAlignObjectsToolBuilder>()
	{
		return UAlignObjectsToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAlignObjectsToolBuilder(Z_Construct_UClass_UAlignObjectsToolBuilder, &UAlignObjectsToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UAlignObjectsToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAlignObjectsToolBuilder);
	void UAlignObjectsToolProperties::StaticRegisterNativesUAlignObjectsToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UAlignObjectsToolProperties_NoRegister()
	{
		return UAlignObjectsToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UAlignObjectsToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_AlignType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AlignType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_AlignType;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_AlignTo_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AlignTo_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_AlignTo;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_BoxPosition_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoxPosition_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_BoxPosition;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAlignX_MetaData[];
#endif
		static void NewProp_bAlignX_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAlignX;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAlignY_MetaData[];
#endif
		static void NewProp_bAlignY_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAlignY;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAlignZ_MetaData[];
#endif
		static void NewProp_bAlignZ_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAlignZ;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAlignObjectsToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAlignObjectsToolProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Standard properties of the Align Objects Operation\n */" },
		{ "IncludePath", "AlignObjectsTool.h" },
		{ "ModuleRelativePath", "Public/AlignObjectsTool.h" },
		{ "ToolTip", "Standard properties of the Align Objects Operation" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_AlignType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_AlignType_MetaData[] = {
		{ "Category", "Options" },
		{ "ModuleRelativePath", "Public/AlignObjectsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_AlignType = { "AlignType", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAlignObjectsToolProperties, AlignType), Z_Construct_UEnum_MeshModelingTools_EAlignObjectsAlignTypes, METADATA_PARAMS(Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_AlignType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_AlignType_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_AlignTo_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_AlignTo_MetaData[] = {
		{ "Category", "Options" },
		{ "ModuleRelativePath", "Public/AlignObjectsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_AlignTo = { "AlignTo", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAlignObjectsToolProperties, AlignTo), Z_Construct_UEnum_MeshModelingTools_EAlignObjectsAlignToOptions, METADATA_PARAMS(Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_AlignTo_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_AlignTo_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_BoxPosition_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_BoxPosition_MetaData[] = {
		{ "Category", "Options" },
		{ "EditCondition", "AlignType == EAlignObjectsAlignTypes::BoundingBoxes || AlignTo == EAlignObjectsAlignToOptions::Combined" },
		{ "ModuleRelativePath", "Public/AlignObjectsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_BoxPosition = { "BoxPosition", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAlignObjectsToolProperties, BoxPosition), Z_Construct_UEnum_MeshModelingTools_EAlignObjectsBoxPoint, METADATA_PARAMS(Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_BoxPosition_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_BoxPosition_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_bAlignX_MetaData[] = {
		{ "Category", "Axes" },
		{ "ModuleRelativePath", "Public/AlignObjectsTool.h" },
	};
#endif
	void Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_bAlignX_SetBit(void* Obj)
	{
		((UAlignObjectsToolProperties*)Obj)->bAlignX = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_bAlignX = { "bAlignX", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UAlignObjectsToolProperties), &Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_bAlignX_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_bAlignX_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_bAlignX_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_bAlignY_MetaData[] = {
		{ "Category", "Axes" },
		{ "ModuleRelativePath", "Public/AlignObjectsTool.h" },
	};
#endif
	void Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_bAlignY_SetBit(void* Obj)
	{
		((UAlignObjectsToolProperties*)Obj)->bAlignY = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_bAlignY = { "bAlignY", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UAlignObjectsToolProperties), &Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_bAlignY_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_bAlignY_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_bAlignY_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_bAlignZ_MetaData[] = {
		{ "Category", "Axes" },
		{ "ModuleRelativePath", "Public/AlignObjectsTool.h" },
	};
#endif
	void Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_bAlignZ_SetBit(void* Obj)
	{
		((UAlignObjectsToolProperties*)Obj)->bAlignZ = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_bAlignZ = { "bAlignZ", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UAlignObjectsToolProperties), &Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_bAlignZ_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_bAlignZ_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_bAlignZ_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAlignObjectsToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_AlignType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_AlignType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_AlignTo_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_AlignTo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_BoxPosition_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_BoxPosition,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_bAlignX,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_bAlignY,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAlignObjectsToolProperties_Statics::NewProp_bAlignZ,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAlignObjectsToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAlignObjectsToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAlignObjectsToolProperties_Statics::ClassParams = {
		&UAlignObjectsToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UAlignObjectsToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UAlignObjectsToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UAlignObjectsToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAlignObjectsToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAlignObjectsToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAlignObjectsToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAlignObjectsToolProperties, 2711523);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UAlignObjectsToolProperties>()
	{
		return UAlignObjectsToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAlignObjectsToolProperties(Z_Construct_UClass_UAlignObjectsToolProperties, &UAlignObjectsToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UAlignObjectsToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAlignObjectsToolProperties);
	void UAlignObjectsTool::StaticRegisterNativesUAlignObjectsTool()
	{
	}
	UClass* Z_Construct_UClass_UAlignObjectsTool_NoRegister()
	{
		return UAlignObjectsTool::StaticClass();
	}
	struct Z_Construct_UClass_UAlignObjectsTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AlignProps_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AlignProps;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAlignObjectsTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMultiSelectionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAlignObjectsTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * UAlignObjectsTool transforms the input Components so that they are aligned in various ways, depending on the current settings.\n * The object positions move after every change in the parameters. Currently those changes are not transacted.\n * On cancel the original positions are restored, and on accept the positions are updated with a transaction.\n */" },
		{ "IncludePath", "AlignObjectsTool.h" },
		{ "ModuleRelativePath", "Public/AlignObjectsTool.h" },
		{ "ToolTip", "UAlignObjectsTool transforms the input Components so that they are aligned in various ways, depending on the current settings.\nThe object positions move after every change in the parameters. Currently those changes are not transacted.\nOn cancel the original positions are restored, and on accept the positions are updated with a transaction." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAlignObjectsTool_Statics::NewProp_AlignProps_MetaData[] = {
		{ "ModuleRelativePath", "Public/AlignObjectsTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UAlignObjectsTool_Statics::NewProp_AlignProps = { "AlignProps", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAlignObjectsTool, AlignProps), Z_Construct_UClass_UAlignObjectsToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UAlignObjectsTool_Statics::NewProp_AlignProps_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAlignObjectsTool_Statics::NewProp_AlignProps_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAlignObjectsTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAlignObjectsTool_Statics::NewProp_AlignProps,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAlignObjectsTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAlignObjectsTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAlignObjectsTool_Statics::ClassParams = {
		&UAlignObjectsTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UAlignObjectsTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UAlignObjectsTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UAlignObjectsTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAlignObjectsTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAlignObjectsTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAlignObjectsTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAlignObjectsTool, 2156325624);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UAlignObjectsTool>()
	{
		return UAlignObjectsTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAlignObjectsTool(Z_Construct_UClass_UAlignObjectsTool, &UAlignObjectsTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UAlignObjectsTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAlignObjectsTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
