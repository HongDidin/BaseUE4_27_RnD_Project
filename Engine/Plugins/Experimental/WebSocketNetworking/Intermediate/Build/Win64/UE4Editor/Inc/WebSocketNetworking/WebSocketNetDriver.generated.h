// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef WEBSOCKETNETWORKING_WebSocketNetDriver_generated_h
#error "WebSocketNetDriver.generated.h already included, missing '#pragma once' in WebSocketNetDriver.h"
#endif
#define WEBSOCKETNETWORKING_WebSocketNetDriver_generated_h

#define Engine_Plugins_Experimental_WebSocketNetworking_Source_WebSocketNetworking_Public_WebSocketNetDriver_h_18_SPARSE_DATA
#define Engine_Plugins_Experimental_WebSocketNetworking_Source_WebSocketNetworking_Public_WebSocketNetDriver_h_18_RPC_WRAPPERS
#define Engine_Plugins_Experimental_WebSocketNetworking_Source_WebSocketNetworking_Public_WebSocketNetDriver_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_WebSocketNetworking_Source_WebSocketNetworking_Public_WebSocketNetDriver_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUWebSocketNetDriver(); \
	friend struct Z_Construct_UClass_UWebSocketNetDriver_Statics; \
public: \
	DECLARE_CLASS(UWebSocketNetDriver, UNetDriver, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/WebSocketNetworking"), NO_API) \
	DECLARE_SERIALIZER(UWebSocketNetDriver)


#define Engine_Plugins_Experimental_WebSocketNetworking_Source_WebSocketNetworking_Public_WebSocketNetDriver_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUWebSocketNetDriver(); \
	friend struct Z_Construct_UClass_UWebSocketNetDriver_Statics; \
public: \
	DECLARE_CLASS(UWebSocketNetDriver, UNetDriver, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/WebSocketNetworking"), NO_API) \
	DECLARE_SERIALIZER(UWebSocketNetDriver)


#define Engine_Plugins_Experimental_WebSocketNetworking_Source_WebSocketNetworking_Public_WebSocketNetDriver_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UWebSocketNetDriver(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UWebSocketNetDriver) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWebSocketNetDriver); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWebSocketNetDriver); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWebSocketNetDriver(UWebSocketNetDriver&&); \
	NO_API UWebSocketNetDriver(const UWebSocketNetDriver&); \
public:


#define Engine_Plugins_Experimental_WebSocketNetworking_Source_WebSocketNetworking_Public_WebSocketNetDriver_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UWebSocketNetDriver(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWebSocketNetDriver(UWebSocketNetDriver&&); \
	NO_API UWebSocketNetDriver(const UWebSocketNetDriver&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWebSocketNetDriver); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWebSocketNetDriver); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UWebSocketNetDriver)


#define Engine_Plugins_Experimental_WebSocketNetworking_Source_WebSocketNetworking_Public_WebSocketNetDriver_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_WebSocketNetworking_Source_WebSocketNetworking_Public_WebSocketNetDriver_h_15_PROLOG
#define Engine_Plugins_Experimental_WebSocketNetworking_Source_WebSocketNetworking_Public_WebSocketNetDriver_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_WebSocketNetworking_Source_WebSocketNetworking_Public_WebSocketNetDriver_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_WebSocketNetworking_Source_WebSocketNetworking_Public_WebSocketNetDriver_h_18_SPARSE_DATA \
	Engine_Plugins_Experimental_WebSocketNetworking_Source_WebSocketNetworking_Public_WebSocketNetDriver_h_18_RPC_WRAPPERS \
	Engine_Plugins_Experimental_WebSocketNetworking_Source_WebSocketNetworking_Public_WebSocketNetDriver_h_18_INCLASS \
	Engine_Plugins_Experimental_WebSocketNetworking_Source_WebSocketNetworking_Public_WebSocketNetDriver_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_WebSocketNetworking_Source_WebSocketNetworking_Public_WebSocketNetDriver_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_WebSocketNetworking_Source_WebSocketNetworking_Public_WebSocketNetDriver_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_WebSocketNetworking_Source_WebSocketNetworking_Public_WebSocketNetDriver_h_18_SPARSE_DATA \
	Engine_Plugins_Experimental_WebSocketNetworking_Source_WebSocketNetworking_Public_WebSocketNetDriver_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_WebSocketNetworking_Source_WebSocketNetworking_Public_WebSocketNetDriver_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_WebSocketNetworking_Source_WebSocketNetworking_Public_WebSocketNetDriver_h_18_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class WebSocketNetDriver."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> WEBSOCKETNETWORKING_API UClass* StaticClass<class UWebSocketNetDriver>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_WebSocketNetworking_Source_WebSocketNetworking_Public_WebSocketNetDriver_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
