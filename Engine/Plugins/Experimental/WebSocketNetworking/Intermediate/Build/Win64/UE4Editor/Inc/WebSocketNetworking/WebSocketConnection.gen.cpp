// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "WebSocketNetworking/Public/WebSocketConnection.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWebSocketConnection() {}
// Cross Module References
	WEBSOCKETNETWORKING_API UClass* Z_Construct_UClass_UWebSocketConnection_NoRegister();
	WEBSOCKETNETWORKING_API UClass* Z_Construct_UClass_UWebSocketConnection();
	ENGINE_API UClass* Z_Construct_UClass_UNetConnection();
	UPackage* Z_Construct_UPackage__Script_WebSocketNetworking();
// End Cross Module References
	void UWebSocketConnection::StaticRegisterNativesUWebSocketConnection()
	{
	}
	UClass* Z_Construct_UClass_UWebSocketConnection_NoRegister()
	{
		return UWebSocketConnection::StaticClass();
	}
	struct Z_Construct_UClass_UWebSocketConnection_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UWebSocketConnection_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNetConnection,
		(UObject* (*)())Z_Construct_UPackage__Script_WebSocketNetworking,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWebSocketConnection_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "WebSocketConnection.h" },
		{ "ModuleRelativePath", "Public/WebSocketConnection.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UWebSocketConnection_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UWebSocketConnection>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UWebSocketConnection_Statics::ClassParams = {
		&UWebSocketConnection::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000ACu,
		METADATA_PARAMS(Z_Construct_UClass_UWebSocketConnection_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UWebSocketConnection_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UWebSocketConnection()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UWebSocketConnection_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UWebSocketConnection, 2250214419);
	template<> WEBSOCKETNETWORKING_API UClass* StaticClass<UWebSocketConnection>()
	{
		return UWebSocketConnection::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UWebSocketConnection(Z_Construct_UClass_UWebSocketConnection, &UWebSocketConnection::StaticClass, TEXT("/Script/WebSocketNetworking"), TEXT("UWebSocketConnection"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UWebSocketConnection);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
