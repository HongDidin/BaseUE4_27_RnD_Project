// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LIVELINKCONTROLRIG_LiveLinkRigUnits_generated_h
#error "LiveLinkRigUnits.generated.h already included, missing '#pragma once' in LiveLinkRigUnits.h"
#endif
#define LIVELINKCONTROLRIG_LiveLinkRigUnits_generated_h


#define FRigUnit_LiveLinkEvaluteFrameTransform_Execute() \
	void FRigUnit_LiveLinkEvaluteFrameTransform::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& SubjectName, \
		const bool bDrawDebug, \
		const FLinearColor& DebugColor, \
		const FTransform& DebugDrawOffset, \
		FTransform& Transform, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_LiveLinkControlRig_Source_LiveLinkControlRig_Public_LiveLinkRigUnits_h_102_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Statics; \
	static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& SubjectName, \
		const bool bDrawDebug, \
		const FLinearColor& DebugColor, \
		const FTransform& DebugDrawOffset, \
		FTransform& Transform, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FName& SubjectName = *(FName*)RigVMMemoryHandles[0].GetData(); \
		const bool bDrawDebug = *(bool*)RigVMMemoryHandles[1].GetData(); \
		const FLinearColor& DebugColor = *(FLinearColor*)RigVMMemoryHandles[2].GetData(); \
		const FTransform& DebugDrawOffset = *(FTransform*)RigVMMemoryHandles[3].GetData(); \
		FTransform& Transform = *(FTransform*)RigVMMemoryHandles[4].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			SubjectName, \
			bDrawDebug, \
			DebugColor, \
			DebugDrawOffset, \
			Transform, \
			Context \
		); \
	} \
	typedef FRigUnit_LiveLinkBase Super;


template<> LIVELINKCONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_LiveLinkEvaluteFrameTransform>();


#define FRigUnit_LiveLinkGetParameterValueByName_Execute() \
	void FRigUnit_LiveLinkGetParameterValueByName::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FSubjectFrameHandle& SubjectFrame, \
		const FName& ParameterName, \
		float& Value, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_LiveLinkControlRig_Source_LiveLinkControlRig_Public_LiveLinkRigUnits_h_80_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetParameterValueByName_Statics; \
	static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FSubjectFrameHandle& SubjectFrame, \
		const FName& ParameterName, \
		float& Value, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FSubjectFrameHandle& SubjectFrame = *(FSubjectFrameHandle*)RigVMMemoryHandles[0].GetData(); \
		const FName& ParameterName = *(FName*)RigVMMemoryHandles[1].GetData(); \
		float& Value = *(float*)RigVMMemoryHandles[2].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			SubjectFrame, \
			ParameterName, \
			Value, \
			Context \
		); \
	} \
	typedef FRigUnit_LiveLinkBase Super;


template<> LIVELINKCONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_LiveLinkGetParameterValueByName>();


#define FRigUnit_LiveLinkGetTransformByName_Execute() \
	void FRigUnit_LiveLinkGetTransformByName::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FSubjectFrameHandle& SubjectFrame, \
		const FName& TransformName, \
		const EBoneGetterSetterMode Space, \
		FTransform& Transform, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_LiveLinkControlRig_Source_LiveLinkControlRig_Public_LiveLinkRigUnits_h_55_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName_Statics; \
	static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FSubjectFrameHandle& SubjectFrame, \
		const FName& TransformName, \
		const EBoneGetterSetterMode Space, \
		FTransform& Transform, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FSubjectFrameHandle& SubjectFrame = *(FSubjectFrameHandle*)RigVMMemoryHandles[0].GetData(); \
		const FName& TransformName = *(FName*)RigVMMemoryHandles[1].GetData(); \
		EBoneGetterSetterMode Space = (EBoneGetterSetterMode)*(uint8*)RigVMMemoryHandles[2].GetData(); \
		FTransform& Transform = *(FTransform*)RigVMMemoryHandles[3].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			SubjectFrame, \
			TransformName, \
			Space, \
			Transform, \
			Context \
		); \
	} \
	typedef FRigUnit_LiveLinkBase Super;


template<> LIVELINKCONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_LiveLinkGetTransformByName>();


#define FRigUnit_LiveLinkEvaluteFrameAnimation_Execute() \
	void FRigUnit_LiveLinkEvaluteFrameAnimation::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& SubjectName, \
		const bool bDrawDebug, \
		const FLinearColor& DebugColor, \
		const FTransform& DebugDrawOffset, \
		FSubjectFrameHandle& SubjectFrame, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_LiveLinkControlRig_Source_LiveLinkControlRig_Public_LiveLinkRigUnits_h_27_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Statics; \
	static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& SubjectName, \
		const bool bDrawDebug, \
		const FLinearColor& DebugColor, \
		const FTransform& DebugDrawOffset, \
		FSubjectFrameHandle& SubjectFrame, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FName& SubjectName = *(FName*)RigVMMemoryHandles[0].GetData(); \
		const bool bDrawDebug = *(bool*)RigVMMemoryHandles[1].GetData(); \
		const FLinearColor& DebugColor = *(FLinearColor*)RigVMMemoryHandles[2].GetData(); \
		const FTransform& DebugDrawOffset = *(FTransform*)RigVMMemoryHandles[3].GetData(); \
		FSubjectFrameHandle& SubjectFrame = *(FSubjectFrameHandle*)RigVMMemoryHandles[4].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			SubjectName, \
			bDrawDebug, \
			DebugColor, \
			DebugDrawOffset, \
			SubjectFrame, \
			Context \
		); \
	} \
	typedef FRigUnit_LiveLinkBase Super;


template<> LIVELINKCONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_LiveLinkEvaluteFrameAnimation>();

#define Engine_Plugins_Experimental_LiveLinkControlRig_Source_LiveLinkControlRig_Public_LiveLinkRigUnits_h_18_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_LiveLinkBase_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FRigUnit Super;


template<> LIVELINKCONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_LiveLinkBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_LiveLinkControlRig_Source_LiveLinkControlRig_Public_LiveLinkRigUnits_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
