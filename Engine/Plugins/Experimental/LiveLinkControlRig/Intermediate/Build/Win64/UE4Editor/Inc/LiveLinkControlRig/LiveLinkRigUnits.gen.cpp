// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LiveLinkControlRig/Public/LiveLinkRigUnits.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLiveLinkRigUnits() {}
// Cross Module References
	LIVELINKCONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform();
	UPackage* Z_Construct_UPackage__Script_LiveLinkControlRig();
	LIVELINKCONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_LiveLinkBase();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	LIVELINKCONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetParameterValueByName();
	LIVELINKINTERFACE_API UScriptStruct* Z_Construct_UScriptStruct_FSubjectFrameHandle();
	LIVELINKCONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName();
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_EBoneGetterSetterMode();
	LIVELINKCONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_LiveLinkEvaluteFrameTransform>() == std::is_polymorphic<FRigUnit_LiveLinkBase>(), "USTRUCT FRigUnit_LiveLinkEvaluteFrameTransform cannot be polymorphic unless super FRigUnit_LiveLinkBase is polymorphic");

class UScriptStruct* FRigUnit_LiveLinkEvaluteFrameTransform::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LIVELINKCONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform, Z_Construct_UPackage__Script_LiveLinkControlRig(), TEXT("RigUnit_LiveLinkEvaluteFrameTransform"), sizeof(FRigUnit_LiveLinkEvaluteFrameTransform), Get_Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_LiveLinkEvaluteFrameTransform::Execute"), &FRigUnit_LiveLinkEvaluteFrameTransform::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> LIVELINKCONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_LiveLinkEvaluteFrameTransform>()
{
	return FRigUnit_LiveLinkEvaluteFrameTransform::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform(FRigUnit_LiveLinkEvaluteFrameTransform::StaticStruct, TEXT("/Script/LiveLinkControlRig"), TEXT("RigUnit_LiveLinkEvaluteFrameTransform"), false, nullptr, nullptr);
static struct FScriptStruct_LiveLinkControlRig_StaticRegisterNativesFRigUnit_LiveLinkEvaluteFrameTransform
{
	FScriptStruct_LiveLinkControlRig_StaticRegisterNativesFRigUnit_LiveLinkEvaluteFrameTransform()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_LiveLinkEvaluteFrameTransform>(FName(TEXT("RigUnit_LiveLinkEvaluteFrameTransform")));
	}
} ScriptStruct_LiveLinkControlRig_StaticRegisterNativesFRigUnit_LiveLinkEvaluteFrameTransform;
	struct Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SubjectName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_SubjectName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDrawDebug_MetaData[];
#endif
		static void NewProp_bDrawDebug_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDrawDebug;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DebugColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DebugColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DebugDrawOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DebugDrawOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Transform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Transform;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Live Link" },
		{ "Comment", "/**\n * Evaluate current Live Link Transform associated with supplied subject\n */" },
		{ "DisplayName", "Evaluate Live Link Frame (Transform)" },
		{ "ModuleRelativePath", "Public/LiveLinkRigUnits.h" },
		{ "ToolTip", "Evaluate current Live Link Transform associated with supplied subject" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_LiveLinkEvaluteFrameTransform>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Statics::NewProp_SubjectName_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Public/LiveLinkRigUnits.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Statics::NewProp_SubjectName = { "SubjectName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_LiveLinkEvaluteFrameTransform, SubjectName), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Statics::NewProp_SubjectName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Statics::NewProp_SubjectName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Statics::NewProp_bDrawDebug_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Public/LiveLinkRigUnits.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Statics::NewProp_bDrawDebug_SetBit(void* Obj)
	{
		((FRigUnit_LiveLinkEvaluteFrameTransform*)Obj)->bDrawDebug = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Statics::NewProp_bDrawDebug = { "bDrawDebug", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_LiveLinkEvaluteFrameTransform), &Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Statics::NewProp_bDrawDebug_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Statics::NewProp_bDrawDebug_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Statics::NewProp_bDrawDebug_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Statics::NewProp_DebugColor_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Public/LiveLinkRigUnits.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Statics::NewProp_DebugColor = { "DebugColor", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_LiveLinkEvaluteFrameTransform, DebugColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Statics::NewProp_DebugColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Statics::NewProp_DebugColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Statics::NewProp_DebugDrawOffset_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Public/LiveLinkRigUnits.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Statics::NewProp_DebugDrawOffset = { "DebugDrawOffset", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_LiveLinkEvaluteFrameTransform, DebugDrawOffset), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Statics::NewProp_DebugDrawOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Statics::NewProp_DebugDrawOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Statics::NewProp_Transform_MetaData[] = {
		{ "ModuleRelativePath", "Public/LiveLinkRigUnits.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Statics::NewProp_Transform = { "Transform", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_LiveLinkEvaluteFrameTransform, Transform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Statics::NewProp_Transform_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Statics::NewProp_Transform_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Statics::NewProp_SubjectName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Statics::NewProp_bDrawDebug,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Statics::NewProp_DebugColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Statics::NewProp_DebugDrawOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Statics::NewProp_Transform,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkControlRig,
		Z_Construct_UScriptStruct_FRigUnit_LiveLinkBase,
		&NewStructOps,
		"RigUnit_LiveLinkEvaluteFrameTransform",
		sizeof(FRigUnit_LiveLinkEvaluteFrameTransform),
		alignof(FRigUnit_LiveLinkEvaluteFrameTransform),
		Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LiveLinkControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_LiveLinkEvaluteFrameTransform"), sizeof(FRigUnit_LiveLinkEvaluteFrameTransform), Get_Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameTransform_Hash() { return 2012456493U; }

void FRigUnit_LiveLinkEvaluteFrameTransform::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		SubjectName,
		bDrawDebug,
		DebugColor,
		DebugDrawOffset,
		Transform,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_LiveLinkGetParameterValueByName>() == std::is_polymorphic<FRigUnit_LiveLinkBase>(), "USTRUCT FRigUnit_LiveLinkGetParameterValueByName cannot be polymorphic unless super FRigUnit_LiveLinkBase is polymorphic");

class UScriptStruct* FRigUnit_LiveLinkGetParameterValueByName::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LIVELINKCONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetParameterValueByName_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetParameterValueByName, Z_Construct_UPackage__Script_LiveLinkControlRig(), TEXT("RigUnit_LiveLinkGetParameterValueByName"), sizeof(FRigUnit_LiveLinkGetParameterValueByName), Get_Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetParameterValueByName_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_LiveLinkGetParameterValueByName::Execute"), &FRigUnit_LiveLinkGetParameterValueByName::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> LIVELINKCONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_LiveLinkGetParameterValueByName>()
{
	return FRigUnit_LiveLinkGetParameterValueByName::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_LiveLinkGetParameterValueByName(FRigUnit_LiveLinkGetParameterValueByName::StaticStruct, TEXT("/Script/LiveLinkControlRig"), TEXT("RigUnit_LiveLinkGetParameterValueByName"), false, nullptr, nullptr);
static struct FScriptStruct_LiveLinkControlRig_StaticRegisterNativesFRigUnit_LiveLinkGetParameterValueByName
{
	FScriptStruct_LiveLinkControlRig_StaticRegisterNativesFRigUnit_LiveLinkGetParameterValueByName()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_LiveLinkGetParameterValueByName>(FName(TEXT("RigUnit_LiveLinkGetParameterValueByName")));
	}
} ScriptStruct_LiveLinkControlRig_StaticRegisterNativesFRigUnit_LiveLinkGetParameterValueByName;
	struct Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetParameterValueByName_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SubjectFrame_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SubjectFrame;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParameterName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ParameterName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetParameterValueByName_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Live Link" },
		{ "Comment", "/**\n * Get the parameter value with supplied subject frame \n */" },
		{ "DisplayName", "Get Parameter Value By Name" },
		{ "ModuleRelativePath", "Public/LiveLinkRigUnits.h" },
		{ "ToolTip", "Get the parameter value with supplied subject frame" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetParameterValueByName_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_LiveLinkGetParameterValueByName>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetParameterValueByName_Statics::NewProp_SubjectFrame_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Public/LiveLinkRigUnits.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetParameterValueByName_Statics::NewProp_SubjectFrame = { "SubjectFrame", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_LiveLinkGetParameterValueByName, SubjectFrame), Z_Construct_UScriptStruct_FSubjectFrameHandle, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetParameterValueByName_Statics::NewProp_SubjectFrame_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetParameterValueByName_Statics::NewProp_SubjectFrame_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetParameterValueByName_Statics::NewProp_ParameterName_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Public/LiveLinkRigUnits.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetParameterValueByName_Statics::NewProp_ParameterName = { "ParameterName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_LiveLinkGetParameterValueByName, ParameterName), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetParameterValueByName_Statics::NewProp_ParameterName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetParameterValueByName_Statics::NewProp_ParameterName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetParameterValueByName_Statics::NewProp_Value_MetaData[] = {
		{ "ModuleRelativePath", "Public/LiveLinkRigUnits.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetParameterValueByName_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_LiveLinkGetParameterValueByName, Value), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetParameterValueByName_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetParameterValueByName_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetParameterValueByName_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetParameterValueByName_Statics::NewProp_SubjectFrame,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetParameterValueByName_Statics::NewProp_ParameterName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetParameterValueByName_Statics::NewProp_Value,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetParameterValueByName_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkControlRig,
		Z_Construct_UScriptStruct_FRigUnit_LiveLinkBase,
		&NewStructOps,
		"RigUnit_LiveLinkGetParameterValueByName",
		sizeof(FRigUnit_LiveLinkGetParameterValueByName),
		alignof(FRigUnit_LiveLinkGetParameterValueByName),
		Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetParameterValueByName_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetParameterValueByName_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetParameterValueByName_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetParameterValueByName_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetParameterValueByName()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetParameterValueByName_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LiveLinkControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_LiveLinkGetParameterValueByName"), sizeof(FRigUnit_LiveLinkGetParameterValueByName), Get_Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetParameterValueByName_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetParameterValueByName_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetParameterValueByName_Hash() { return 2852455061U; }

void FRigUnit_LiveLinkGetParameterValueByName::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		SubjectFrame,
		ParameterName,
		Value,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_LiveLinkGetTransformByName>() == std::is_polymorphic<FRigUnit_LiveLinkBase>(), "USTRUCT FRigUnit_LiveLinkGetTransformByName cannot be polymorphic unless super FRigUnit_LiveLinkBase is polymorphic");

class UScriptStruct* FRigUnit_LiveLinkGetTransformByName::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LIVELINKCONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName, Z_Construct_UPackage__Script_LiveLinkControlRig(), TEXT("RigUnit_LiveLinkGetTransformByName"), sizeof(FRigUnit_LiveLinkGetTransformByName), Get_Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_LiveLinkGetTransformByName::Execute"), &FRigUnit_LiveLinkGetTransformByName::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> LIVELINKCONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_LiveLinkGetTransformByName>()
{
	return FRigUnit_LiveLinkGetTransformByName::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName(FRigUnit_LiveLinkGetTransformByName::StaticStruct, TEXT("/Script/LiveLinkControlRig"), TEXT("RigUnit_LiveLinkGetTransformByName"), false, nullptr, nullptr);
static struct FScriptStruct_LiveLinkControlRig_StaticRegisterNativesFRigUnit_LiveLinkGetTransformByName
{
	FScriptStruct_LiveLinkControlRig_StaticRegisterNativesFRigUnit_LiveLinkGetTransformByName()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_LiveLinkGetTransformByName>(FName(TEXT("RigUnit_LiveLinkGetTransformByName")));
	}
} ScriptStruct_LiveLinkControlRig_StaticRegisterNativesFRigUnit_LiveLinkGetTransformByName;
	struct Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SubjectFrame_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SubjectFrame;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransformName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_TransformName;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Space_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Space_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Space;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Transform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Transform;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Live Link" },
		{ "Comment", "/**\n * Get the transform value with supplied subject frame\n */" },
		{ "DisplayName", "Get Transform By Name" },
		{ "ModuleRelativePath", "Public/LiveLinkRigUnits.h" },
		{ "ToolTip", "Get the transform value with supplied subject frame" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_LiveLinkGetTransformByName>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName_Statics::NewProp_SubjectFrame_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Public/LiveLinkRigUnits.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName_Statics::NewProp_SubjectFrame = { "SubjectFrame", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_LiveLinkGetTransformByName, SubjectFrame), Z_Construct_UScriptStruct_FSubjectFrameHandle, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName_Statics::NewProp_SubjectFrame_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName_Statics::NewProp_SubjectFrame_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName_Statics::NewProp_TransformName_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Public/LiveLinkRigUnits.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName_Statics::NewProp_TransformName = { "TransformName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_LiveLinkGetTransformByName, TransformName), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName_Statics::NewProp_TransformName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName_Statics::NewProp_TransformName_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName_Statics::NewProp_Space_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName_Statics::NewProp_Space_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Public/LiveLinkRigUnits.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName_Statics::NewProp_Space = { "Space", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_LiveLinkGetTransformByName, Space), Z_Construct_UEnum_ControlRig_EBoneGetterSetterMode, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName_Statics::NewProp_Space_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName_Statics::NewProp_Space_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName_Statics::NewProp_Transform_MetaData[] = {
		{ "ModuleRelativePath", "Public/LiveLinkRigUnits.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName_Statics::NewProp_Transform = { "Transform", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_LiveLinkGetTransformByName, Transform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName_Statics::NewProp_Transform_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName_Statics::NewProp_Transform_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName_Statics::NewProp_SubjectFrame,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName_Statics::NewProp_TransformName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName_Statics::NewProp_Space_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName_Statics::NewProp_Space,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName_Statics::NewProp_Transform,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkControlRig,
		Z_Construct_UScriptStruct_FRigUnit_LiveLinkBase,
		&NewStructOps,
		"RigUnit_LiveLinkGetTransformByName",
		sizeof(FRigUnit_LiveLinkGetTransformByName),
		alignof(FRigUnit_LiveLinkGetTransformByName),
		Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LiveLinkControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_LiveLinkGetTransformByName"), sizeof(FRigUnit_LiveLinkGetTransformByName), Get_Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_LiveLinkGetTransformByName_Hash() { return 1744582167U; }

void FRigUnit_LiveLinkGetTransformByName::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		SubjectFrame,
		TransformName,
		Space,
		Transform,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_LiveLinkEvaluteFrameAnimation>() == std::is_polymorphic<FRigUnit_LiveLinkBase>(), "USTRUCT FRigUnit_LiveLinkEvaluteFrameAnimation cannot be polymorphic unless super FRigUnit_LiveLinkBase is polymorphic");

class UScriptStruct* FRigUnit_LiveLinkEvaluteFrameAnimation::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LIVELINKCONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation, Z_Construct_UPackage__Script_LiveLinkControlRig(), TEXT("RigUnit_LiveLinkEvaluteFrameAnimation"), sizeof(FRigUnit_LiveLinkEvaluteFrameAnimation), Get_Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_LiveLinkEvaluteFrameAnimation::Execute"), &FRigUnit_LiveLinkEvaluteFrameAnimation::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> LIVELINKCONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_LiveLinkEvaluteFrameAnimation>()
{
	return FRigUnit_LiveLinkEvaluteFrameAnimation::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation(FRigUnit_LiveLinkEvaluteFrameAnimation::StaticStruct, TEXT("/Script/LiveLinkControlRig"), TEXT("RigUnit_LiveLinkEvaluteFrameAnimation"), false, nullptr, nullptr);
static struct FScriptStruct_LiveLinkControlRig_StaticRegisterNativesFRigUnit_LiveLinkEvaluteFrameAnimation
{
	FScriptStruct_LiveLinkControlRig_StaticRegisterNativesFRigUnit_LiveLinkEvaluteFrameAnimation()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_LiveLinkEvaluteFrameAnimation>(FName(TEXT("RigUnit_LiveLinkEvaluteFrameAnimation")));
	}
} ScriptStruct_LiveLinkControlRig_StaticRegisterNativesFRigUnit_LiveLinkEvaluteFrameAnimation;
	struct Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SubjectName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_SubjectName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDrawDebug_MetaData[];
#endif
		static void NewProp_bDrawDebug_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDrawDebug;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DebugColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DebugColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DebugDrawOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DebugDrawOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SubjectFrame_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SubjectFrame;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Live Link" },
		{ "Comment", "/**\n * Evaluate current Live Link Animation associated with supplied subject\n */" },
		{ "DisplayName", "Evaluate Live Link Frame (Animation)" },
		{ "ModuleRelativePath", "Public/LiveLinkRigUnits.h" },
		{ "ToolTip", "Evaluate current Live Link Animation associated with supplied subject" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_LiveLinkEvaluteFrameAnimation>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Statics::NewProp_SubjectName_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Public/LiveLinkRigUnits.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Statics::NewProp_SubjectName = { "SubjectName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_LiveLinkEvaluteFrameAnimation, SubjectName), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Statics::NewProp_SubjectName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Statics::NewProp_SubjectName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Statics::NewProp_bDrawDebug_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Public/LiveLinkRigUnits.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Statics::NewProp_bDrawDebug_SetBit(void* Obj)
	{
		((FRigUnit_LiveLinkEvaluteFrameAnimation*)Obj)->bDrawDebug = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Statics::NewProp_bDrawDebug = { "bDrawDebug", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_LiveLinkEvaluteFrameAnimation), &Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Statics::NewProp_bDrawDebug_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Statics::NewProp_bDrawDebug_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Statics::NewProp_bDrawDebug_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Statics::NewProp_DebugColor_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Public/LiveLinkRigUnits.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Statics::NewProp_DebugColor = { "DebugColor", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_LiveLinkEvaluteFrameAnimation, DebugColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Statics::NewProp_DebugColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Statics::NewProp_DebugColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Statics::NewProp_DebugDrawOffset_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Public/LiveLinkRigUnits.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Statics::NewProp_DebugDrawOffset = { "DebugDrawOffset", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_LiveLinkEvaluteFrameAnimation, DebugDrawOffset), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Statics::NewProp_DebugDrawOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Statics::NewProp_DebugDrawOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Statics::NewProp_SubjectFrame_MetaData[] = {
		{ "ModuleRelativePath", "Public/LiveLinkRigUnits.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Statics::NewProp_SubjectFrame = { "SubjectFrame", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_LiveLinkEvaluteFrameAnimation, SubjectFrame), Z_Construct_UScriptStruct_FSubjectFrameHandle, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Statics::NewProp_SubjectFrame_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Statics::NewProp_SubjectFrame_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Statics::NewProp_SubjectName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Statics::NewProp_bDrawDebug,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Statics::NewProp_DebugColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Statics::NewProp_DebugDrawOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Statics::NewProp_SubjectFrame,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkControlRig,
		Z_Construct_UScriptStruct_FRigUnit_LiveLinkBase,
		&NewStructOps,
		"RigUnit_LiveLinkEvaluteFrameAnimation",
		sizeof(FRigUnit_LiveLinkEvaluteFrameAnimation),
		alignof(FRigUnit_LiveLinkEvaluteFrameAnimation),
		Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LiveLinkControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_LiveLinkEvaluteFrameAnimation"), sizeof(FRigUnit_LiveLinkEvaluteFrameAnimation), Get_Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_LiveLinkEvaluteFrameAnimation_Hash() { return 3890552903U; }

void FRigUnit_LiveLinkEvaluteFrameAnimation::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		SubjectName,
		bDrawDebug,
		DebugColor,
		DebugDrawOffset,
		SubjectFrame,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_LiveLinkBase>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_LiveLinkBase cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_LiveLinkBase::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LIVELINKCONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_LiveLinkBase_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_LiveLinkBase, Z_Construct_UPackage__Script_LiveLinkControlRig(), TEXT("RigUnit_LiveLinkBase"), sizeof(FRigUnit_LiveLinkBase), Get_Z_Construct_UScriptStruct_FRigUnit_LiveLinkBase_Hash());
	}
	return Singleton;
}
template<> LIVELINKCONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_LiveLinkBase>()
{
	return FRigUnit_LiveLinkBase::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_LiveLinkBase(FRigUnit_LiveLinkBase::StaticStruct, TEXT("/Script/LiveLinkControlRig"), TEXT("RigUnit_LiveLinkBase"), false, nullptr, nullptr);
static struct FScriptStruct_LiveLinkControlRig_StaticRegisterNativesFRigUnit_LiveLinkBase
{
	FScriptStruct_LiveLinkControlRig_StaticRegisterNativesFRigUnit_LiveLinkBase()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_LiveLinkBase>(FName(TEXT("RigUnit_LiveLinkBase")));
	}
} ScriptStruct_LiveLinkControlRig_StaticRegisterNativesFRigUnit_LiveLinkBase;
	struct Z_Construct_UScriptStruct_FRigUnit_LiveLinkBase_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_LiveLinkBase_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "ModuleRelativePath", "Public/LiveLinkRigUnits.h" },
		{ "NodeColor", "0.3 0.1 0.1" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_LiveLinkBase_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_LiveLinkBase>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_LiveLinkBase_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_LiveLinkBase",
		sizeof(FRigUnit_LiveLinkBase),
		alignof(FRigUnit_LiveLinkBase),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_LiveLinkBase_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_LiveLinkBase_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_LiveLinkBase()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_LiveLinkBase_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LiveLinkControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_LiveLinkBase"), sizeof(FRigUnit_LiveLinkBase), Get_Z_Construct_UScriptStruct_FRigUnit_LiveLinkBase_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_LiveLinkBase_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_LiveLinkBase_Hash() { return 1981376191U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
