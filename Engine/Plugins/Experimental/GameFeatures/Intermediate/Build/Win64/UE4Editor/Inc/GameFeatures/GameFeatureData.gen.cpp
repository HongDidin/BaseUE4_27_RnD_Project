// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GameFeatures/Public/GameFeatureData.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGameFeatureData() {}
// Cross Module References
	GAMEFEATURES_API UClass* Z_Construct_UClass_UGameFeatureData_NoRegister();
	GAMEFEATURES_API UClass* Z_Construct_UClass_UGameFeatureData();
	ENGINE_API UClass* Z_Construct_UClass_UPrimaryDataAsset();
	UPackage* Z_Construct_UPackage__Script_GameFeatures();
	GAMEFEATURES_API UClass* Z_Construct_UClass_UGameFeatureAction_NoRegister();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FPrimaryAssetTypeInfo();
// End Cross Module References
	void UGameFeatureData::StaticRegisterNativesUGameFeatureData()
	{
	}
	UClass* Z_Construct_UClass_UGameFeatureData_NoRegister()
	{
		return UGameFeatureData::StaticClass();
	}
	struct Z_Construct_UClass_UGameFeatureData_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Actions_Inner_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Actions_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Actions_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Actions;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PrimaryAssetTypesToScan_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PrimaryAssetTypesToScan_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PrimaryAssetTypesToScan;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGameFeatureData_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UPrimaryDataAsset,
		(UObject* (*)())Z_Construct_UPackage__Script_GameFeatures,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameFeatureData_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Data related to a game feature, a collection of code and content that adds a separable discrete feature to the game */" },
		{ "IncludePath", "GameFeatureData.h" },
		{ "ModuleRelativePath", "Public/GameFeatureData.h" },
		{ "ToolTip", "Data related to a game feature, a collection of code and content that adds a separable discrete feature to the game" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameFeatureData_Statics::NewProp_Actions_Inner_MetaData[] = {
		{ "Category", "Actions" },
		{ "Comment", "/** List of actions to perform as this game feature is loaded/activated/deactivated/unloaded */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/GameFeatureData.h" },
		{ "ToolTip", "List of actions to perform as this game feature is loaded/activated/deactivated/unloaded" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGameFeatureData_Statics::NewProp_Actions_Inner = { "Actions", nullptr, (EPropertyFlags)0x0002000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UGameFeatureAction_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGameFeatureData_Statics::NewProp_Actions_Inner_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGameFeatureData_Statics::NewProp_Actions_Inner_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameFeatureData_Statics::NewProp_Actions_MetaData[] = {
		{ "Category", "Actions" },
		{ "Comment", "/** List of actions to perform as this game feature is loaded/activated/deactivated/unloaded */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/GameFeatureData.h" },
		{ "ToolTip", "List of actions to perform as this game feature is loaded/activated/deactivated/unloaded" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UGameFeatureData_Statics::NewProp_Actions = { "Actions", nullptr, (EPropertyFlags)0x0020088000010009, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGameFeatureData, Actions), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UGameFeatureData_Statics::NewProp_Actions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGameFeatureData_Statics::NewProp_Actions_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGameFeatureData_Statics::NewProp_PrimaryAssetTypesToScan_Inner = { "PrimaryAssetTypesToScan", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FPrimaryAssetTypeInfo, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameFeatureData_Statics::NewProp_PrimaryAssetTypesToScan_MetaData[] = {
		{ "Category", "Game Feature | Asset Manager" },
		{ "Comment", "/** List of asset types to scan at startup */" },
		{ "ModuleRelativePath", "Public/GameFeatureData.h" },
		{ "TitleProperty", "PrimaryAssetType" },
		{ "ToolTip", "List of asset types to scan at startup" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UGameFeatureData_Statics::NewProp_PrimaryAssetTypesToScan = { "PrimaryAssetTypesToScan", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGameFeatureData, PrimaryAssetTypesToScan), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UGameFeatureData_Statics::NewProp_PrimaryAssetTypesToScan_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGameFeatureData_Statics::NewProp_PrimaryAssetTypesToScan_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGameFeatureData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGameFeatureData_Statics::NewProp_Actions_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGameFeatureData_Statics::NewProp_Actions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGameFeatureData_Statics::NewProp_PrimaryAssetTypesToScan_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGameFeatureData_Statics::NewProp_PrimaryAssetTypesToScan,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGameFeatureData_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGameFeatureData>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGameFeatureData_Statics::ClassParams = {
		&UGameFeatureData::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UGameFeatureData_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UGameFeatureData_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGameFeatureData_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGameFeatureData_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGameFeatureData()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGameFeatureData_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGameFeatureData, 2892785284);
	template<> GAMEFEATURES_API UClass* StaticClass<UGameFeatureData>()
	{
		return UGameFeatureData::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGameFeatureData(Z_Construct_UClass_UGameFeatureData, &UGameFeatureData::StaticClass, TEXT("/Script/GameFeatures"), TEXT("UGameFeatureData"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGameFeatureData);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
