// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMEFEATURES_GameFeaturesProjectPolicies_generated_h
#error "GameFeaturesProjectPolicies.generated.h already included, missing '#pragma once' in GameFeaturesProjectPolicies.h"
#endif
#define GAMEFEATURES_GameFeaturesProjectPolicies_generated_h

#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_17_SPARSE_DATA
#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_17_RPC_WRAPPERS
#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGameFeaturesProjectPolicies(); \
	friend struct Z_Construct_UClass_UGameFeaturesProjectPolicies_Statics; \
public: \
	DECLARE_CLASS(UGameFeaturesProjectPolicies, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GameFeatures"), NO_API) \
	DECLARE_SERIALIZER(UGameFeaturesProjectPolicies)


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUGameFeaturesProjectPolicies(); \
	friend struct Z_Construct_UClass_UGameFeaturesProjectPolicies_Statics; \
public: \
	DECLARE_CLASS(UGameFeaturesProjectPolicies, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GameFeatures"), NO_API) \
	DECLARE_SERIALIZER(UGameFeaturesProjectPolicies)


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGameFeaturesProjectPolicies(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGameFeaturesProjectPolicies) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGameFeaturesProjectPolicies); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGameFeaturesProjectPolicies); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGameFeaturesProjectPolicies(UGameFeaturesProjectPolicies&&); \
	NO_API UGameFeaturesProjectPolicies(const UGameFeaturesProjectPolicies&); \
public:


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGameFeaturesProjectPolicies(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGameFeaturesProjectPolicies(UGameFeaturesProjectPolicies&&); \
	NO_API UGameFeaturesProjectPolicies(const UGameFeaturesProjectPolicies&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGameFeaturesProjectPolicies); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGameFeaturesProjectPolicies); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGameFeaturesProjectPolicies)


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_14_PROLOG
#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_17_RPC_WRAPPERS \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_17_INCLASS \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMEFEATURES_API UClass* StaticClass<class UGameFeaturesProjectPolicies>();

#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_49_SPARSE_DATA
#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_49_RPC_WRAPPERS
#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_49_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_49_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDefaultGameFeaturesProjectPolicies(); \
	friend struct Z_Construct_UClass_UDefaultGameFeaturesProjectPolicies_Statics; \
public: \
	DECLARE_CLASS(UDefaultGameFeaturesProjectPolicies, UGameFeaturesProjectPolicies, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GameFeatures"), NO_API) \
	DECLARE_SERIALIZER(UDefaultGameFeaturesProjectPolicies)


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_49_INCLASS \
private: \
	static void StaticRegisterNativesUDefaultGameFeaturesProjectPolicies(); \
	friend struct Z_Construct_UClass_UDefaultGameFeaturesProjectPolicies_Statics; \
public: \
	DECLARE_CLASS(UDefaultGameFeaturesProjectPolicies, UGameFeaturesProjectPolicies, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GameFeatures"), NO_API) \
	DECLARE_SERIALIZER(UDefaultGameFeaturesProjectPolicies)


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_49_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDefaultGameFeaturesProjectPolicies(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDefaultGameFeaturesProjectPolicies) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDefaultGameFeaturesProjectPolicies); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDefaultGameFeaturesProjectPolicies); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDefaultGameFeaturesProjectPolicies(UDefaultGameFeaturesProjectPolicies&&); \
	NO_API UDefaultGameFeaturesProjectPolicies(const UDefaultGameFeaturesProjectPolicies&); \
public:


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_49_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDefaultGameFeaturesProjectPolicies(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDefaultGameFeaturesProjectPolicies(UDefaultGameFeaturesProjectPolicies&&); \
	NO_API UDefaultGameFeaturesProjectPolicies(const UDefaultGameFeaturesProjectPolicies&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDefaultGameFeaturesProjectPolicies); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDefaultGameFeaturesProjectPolicies); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDefaultGameFeaturesProjectPolicies)


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_49_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_46_PROLOG
#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_49_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_49_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_49_SPARSE_DATA \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_49_RPC_WRAPPERS \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_49_INCLASS \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_49_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_49_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_49_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_49_SPARSE_DATA \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_49_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_49_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h_49_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMEFEATURES_API UClass* StaticClass<class UDefaultGameFeaturesProjectPolicies>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesProjectPolicies_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
