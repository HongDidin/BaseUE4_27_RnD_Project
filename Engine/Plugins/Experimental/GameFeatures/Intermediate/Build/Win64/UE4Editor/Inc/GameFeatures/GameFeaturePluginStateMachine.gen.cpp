// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GameFeatures/Private/GameFeaturePluginStateMachine.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGameFeaturePluginStateMachine() {}
// Cross Module References
	GAMEFEATURES_API UScriptStruct* Z_Construct_UScriptStruct_FGameFeaturePluginStateMachineProperties();
	UPackage* Z_Construct_UPackage__Script_GameFeatures();
	GAMEFEATURES_API UClass* Z_Construct_UClass_UGameFeatureData_NoRegister();
	GAMEFEATURES_API UClass* Z_Construct_UClass_UGameFeaturePluginStateMachine_NoRegister();
	GAMEFEATURES_API UClass* Z_Construct_UClass_UGameFeaturePluginStateMachine();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
// End Cross Module References
class UScriptStruct* FGameFeaturePluginStateMachineProperties::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern GAMEFEATURES_API uint32 Get_Z_Construct_UScriptStruct_FGameFeaturePluginStateMachineProperties_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FGameFeaturePluginStateMachineProperties, Z_Construct_UPackage__Script_GameFeatures(), TEXT("GameFeaturePluginStateMachineProperties"), sizeof(FGameFeaturePluginStateMachineProperties), Get_Z_Construct_UScriptStruct_FGameFeaturePluginStateMachineProperties_Hash());
	}
	return Singleton;
}
template<> GAMEFEATURES_API UScriptStruct* StaticStruct<FGameFeaturePluginStateMachineProperties>()
{
	return FGameFeaturePluginStateMachineProperties::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FGameFeaturePluginStateMachineProperties(FGameFeaturePluginStateMachineProperties::StaticStruct, TEXT("/Script/GameFeatures"), TEXT("GameFeaturePluginStateMachineProperties"), false, nullptr, nullptr);
static struct FScriptStruct_GameFeatures_StaticRegisterNativesFGameFeaturePluginStateMachineProperties
{
	FScriptStruct_GameFeatures_StaticRegisterNativesFGameFeaturePluginStateMachineProperties()
	{
		UScriptStruct::DeferCppStructOps<FGameFeaturePluginStateMachineProperties>(FName(TEXT("GameFeaturePluginStateMachineProperties")));
	}
} ScriptStruct_GameFeatures_StaticRegisterNativesFGameFeaturePluginStateMachineProperties;
	struct Z_Construct_UScriptStruct_FGameFeaturePluginStateMachineProperties_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GameFeatureData_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_GameFeatureData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGameFeaturePluginStateMachineProperties_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** The common properties that can be accessed by the states of the state machine */" },
		{ "ModuleRelativePath", "Private/GameFeaturePluginStateMachine.h" },
		{ "ToolTip", "The common properties that can be accessed by the states of the state machine" },
	};
#endif
	void* Z_Construct_UScriptStruct_FGameFeaturePluginStateMachineProperties_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FGameFeaturePluginStateMachineProperties>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGameFeaturePluginStateMachineProperties_Statics::NewProp_GameFeatureData_MetaData[] = {
		{ "Comment", "/** The data asset describing this game feature */" },
		{ "ModuleRelativePath", "Private/GameFeaturePluginStateMachine.h" },
		{ "ToolTip", "The data asset describing this game feature" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FGameFeaturePluginStateMachineProperties_Statics::NewProp_GameFeatureData = { "GameFeatureData", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGameFeaturePluginStateMachineProperties, GameFeatureData), Z_Construct_UClass_UGameFeatureData_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FGameFeaturePluginStateMachineProperties_Statics::NewProp_GameFeatureData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGameFeaturePluginStateMachineProperties_Statics::NewProp_GameFeatureData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FGameFeaturePluginStateMachineProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGameFeaturePluginStateMachineProperties_Statics::NewProp_GameFeatureData,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FGameFeaturePluginStateMachineProperties_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_GameFeatures,
		nullptr,
		&NewStructOps,
		"GameFeaturePluginStateMachineProperties",
		sizeof(FGameFeaturePluginStateMachineProperties),
		alignof(FGameFeaturePluginStateMachineProperties),
		Z_Construct_UScriptStruct_FGameFeaturePluginStateMachineProperties_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGameFeaturePluginStateMachineProperties_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FGameFeaturePluginStateMachineProperties_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGameFeaturePluginStateMachineProperties_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FGameFeaturePluginStateMachineProperties()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FGameFeaturePluginStateMachineProperties_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_GameFeatures();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("GameFeaturePluginStateMachineProperties"), sizeof(FGameFeaturePluginStateMachineProperties), Get_Z_Construct_UScriptStruct_FGameFeaturePluginStateMachineProperties_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FGameFeaturePluginStateMachineProperties_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FGameFeaturePluginStateMachineProperties_Hash() { return 3392849174U; }
	void UGameFeaturePluginStateMachine::StaticRegisterNativesUGameFeaturePluginStateMachine()
	{
	}
	UClass* Z_Construct_UClass_UGameFeaturePluginStateMachine_NoRegister()
	{
		return UGameFeaturePluginStateMachine::StaticClass();
	}
	struct Z_Construct_UClass_UGameFeaturePluginStateMachine_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StateProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_StateProperties;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGameFeaturePluginStateMachine_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_GameFeatures,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameFeaturePluginStateMachine_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** A state machine to manage transitioning a game feature plugin from just a URL into a fully loaded and active plugin, including registering its contents with other game systems */" },
		{ "IncludePath", "GameFeaturePluginStateMachine.h" },
		{ "ModuleRelativePath", "Private/GameFeaturePluginStateMachine.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "A state machine to manage transitioning a game feature plugin from just a URL into a fully loaded and active plugin, including registering its contents with other game systems" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameFeaturePluginStateMachine_Statics::NewProp_StateProperties_MetaData[] = {
		{ "Comment", "/** The common properties that can be accessed by the states of the state machine */" },
		{ "ModuleRelativePath", "Private/GameFeaturePluginStateMachine.h" },
		{ "ToolTip", "The common properties that can be accessed by the states of the state machine" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGameFeaturePluginStateMachine_Statics::NewProp_StateProperties = { "StateProperties", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGameFeaturePluginStateMachine, StateProperties), Z_Construct_UScriptStruct_FGameFeaturePluginStateMachineProperties, METADATA_PARAMS(Z_Construct_UClass_UGameFeaturePluginStateMachine_Statics::NewProp_StateProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGameFeaturePluginStateMachine_Statics::NewProp_StateProperties_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGameFeaturePluginStateMachine_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGameFeaturePluginStateMachine_Statics::NewProp_StateProperties,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGameFeaturePluginStateMachine_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGameFeaturePluginStateMachine>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGameFeaturePluginStateMachine_Statics::ClassParams = {
		&UGameFeaturePluginStateMachine::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UGameFeaturePluginStateMachine_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UGameFeaturePluginStateMachine_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGameFeaturePluginStateMachine_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGameFeaturePluginStateMachine_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGameFeaturePluginStateMachine()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGameFeaturePluginStateMachine_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGameFeaturePluginStateMachine, 2323288492);
	template<> GAMEFEATURES_API UClass* StaticClass<UGameFeaturePluginStateMachine>()
	{
		return UGameFeaturePluginStateMachine::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGameFeaturePluginStateMachine(Z_Construct_UClass_UGameFeaturePluginStateMachine, &UGameFeaturePluginStateMachine::StaticClass, TEXT("/Script/GameFeatures"), TEXT("UGameFeaturePluginStateMachine"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGameFeaturePluginStateMachine);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
