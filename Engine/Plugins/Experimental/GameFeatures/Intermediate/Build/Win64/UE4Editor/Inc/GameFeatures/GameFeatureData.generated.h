// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMEFEATURES_GameFeatureData_generated_h
#error "GameFeatureData.generated.h already included, missing '#pragma once' in GameFeatureData.h"
#endif
#define GAMEFEATURES_GameFeatureData_generated_h

#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureData_h_16_SPARSE_DATA
#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureData_h_16_RPC_WRAPPERS
#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureData_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureData_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGameFeatureData(); \
	friend struct Z_Construct_UClass_UGameFeatureData_Statics; \
public: \
	DECLARE_CLASS(UGameFeatureData, UPrimaryDataAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GameFeatures"), NO_API) \
	DECLARE_SERIALIZER(UGameFeatureData)


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureData_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUGameFeatureData(); \
	friend struct Z_Construct_UClass_UGameFeatureData_Statics; \
public: \
	DECLARE_CLASS(UGameFeatureData, UPrimaryDataAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GameFeatures"), NO_API) \
	DECLARE_SERIALIZER(UGameFeatureData)


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureData_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGameFeatureData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGameFeatureData) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGameFeatureData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGameFeatureData); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGameFeatureData(UGameFeatureData&&); \
	NO_API UGameFeatureData(const UGameFeatureData&); \
public:


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureData_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGameFeatureData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGameFeatureData(UGameFeatureData&&); \
	NO_API UGameFeatureData(const UGameFeatureData&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGameFeatureData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGameFeatureData); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGameFeatureData)


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureData_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Actions() { return STRUCT_OFFSET(UGameFeatureData, Actions); } \
	FORCEINLINE static uint32 __PPO__PrimaryAssetTypesToScan() { return STRUCT_OFFSET(UGameFeatureData, PrimaryAssetTypesToScan); }


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureData_h_13_PROLOG
#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureData_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureData_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureData_h_16_SPARSE_DATA \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureData_h_16_RPC_WRAPPERS \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureData_h_16_INCLASS \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureData_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureData_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureData_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureData_h_16_SPARSE_DATA \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureData_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureData_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureData_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMEFEATURES_API UClass* StaticClass<class UGameFeatureData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureData_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
