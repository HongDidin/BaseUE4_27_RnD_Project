// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMEFEATURES_GameFeatureAction_AddComponents_generated_h
#error "GameFeatureAction_AddComponents.generated.h already included, missing '#pragma once' in GameFeatureAction_AddComponents.h"
#endif
#define GAMEFEATURES_GameFeatureAction_AddComponents_generated_h

#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_AddComponents_h_24_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FGameFeatureComponentEntry_Statics; \
	static class UScriptStruct* StaticStruct();


template<> GAMEFEATURES_API UScriptStruct* StaticStruct<struct FGameFeatureComponentEntry>();

#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_AddComponents_h_60_SPARSE_DATA
#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_AddComponents_h_60_RPC_WRAPPERS
#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_AddComponents_h_60_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_AddComponents_h_60_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGameFeatureAction_AddComponents(); \
	friend struct Z_Construct_UClass_UGameFeatureAction_AddComponents_Statics; \
public: \
	DECLARE_CLASS(UGameFeatureAction_AddComponents, UGameFeatureAction, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GameFeatures"), GAMEFEATURES_API) \
	DECLARE_SERIALIZER(UGameFeatureAction_AddComponents)


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_AddComponents_h_60_INCLASS \
private: \
	static void StaticRegisterNativesUGameFeatureAction_AddComponents(); \
	friend struct Z_Construct_UClass_UGameFeatureAction_AddComponents_Statics; \
public: \
	DECLARE_CLASS(UGameFeatureAction_AddComponents, UGameFeatureAction, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GameFeatures"), GAMEFEATURES_API) \
	DECLARE_SERIALIZER(UGameFeatureAction_AddComponents)


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_AddComponents_h_60_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	GAMEFEATURES_API UGameFeatureAction_AddComponents(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGameFeatureAction_AddComponents) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(GAMEFEATURES_API, UGameFeatureAction_AddComponents); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGameFeatureAction_AddComponents); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	GAMEFEATURES_API UGameFeatureAction_AddComponents(UGameFeatureAction_AddComponents&&); \
	GAMEFEATURES_API UGameFeatureAction_AddComponents(const UGameFeatureAction_AddComponents&); \
public:


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_AddComponents_h_60_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	GAMEFEATURES_API UGameFeatureAction_AddComponents(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	GAMEFEATURES_API UGameFeatureAction_AddComponents(UGameFeatureAction_AddComponents&&); \
	GAMEFEATURES_API UGameFeatureAction_AddComponents(const UGameFeatureAction_AddComponents&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(GAMEFEATURES_API, UGameFeatureAction_AddComponents); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGameFeatureAction_AddComponents); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGameFeatureAction_AddComponents)


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_AddComponents_h_60_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_AddComponents_h_57_PROLOG
#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_AddComponents_h_60_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_AddComponents_h_60_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_AddComponents_h_60_SPARSE_DATA \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_AddComponents_h_60_RPC_WRAPPERS \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_AddComponents_h_60_INCLASS \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_AddComponents_h_60_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_AddComponents_h_60_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_AddComponents_h_60_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_AddComponents_h_60_SPARSE_DATA \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_AddComponents_h_60_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_AddComponents_h_60_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_AddComponents_h_60_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMEFEATURES_API UClass* StaticClass<class UGameFeatureAction_AddComponents>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_AddComponents_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
