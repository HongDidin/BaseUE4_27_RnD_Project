// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GameFeatures/Public/GameFeatureAction_AddCheats.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGameFeatureAction_AddCheats() {}
// Cross Module References
	GAMEFEATURES_API UClass* Z_Construct_UClass_UGameFeatureAction_AddCheats_NoRegister();
	GAMEFEATURES_API UClass* Z_Construct_UClass_UGameFeatureAction_AddCheats();
	GAMEFEATURES_API UClass* Z_Construct_UClass_UGameFeatureAction();
	UPackage* Z_Construct_UPackage__Script_GameFeatures();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	ENGINE_API UClass* Z_Construct_UClass_UCheatManagerExtension_NoRegister();
// End Cross Module References
	void UGameFeatureAction_AddCheats::StaticRegisterNativesUGameFeatureAction_AddCheats()
	{
	}
	UClass* Z_Construct_UClass_UGameFeatureAction_AddCheats_NoRegister()
	{
		return UGameFeatureAction_AddCheats::StaticClass();
	}
	struct Z_Construct_UClass_UGameFeatureAction_AddCheats_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_CheatManagers_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CheatManagers_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_CheatManagers;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SpawnedCheatManagers_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpawnedCheatManagers_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SpawnedCheatManagers;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGameFeatureAction_AddCheats_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGameFeatureAction,
		(UObject* (*)())Z_Construct_UPackage__Script_GameFeatures,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameFeatureAction_AddCheats_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Adds cheat manager extensions to the cheat manager for each player\n */" },
		{ "DisplayName", "Add Cheats" },
		{ "IncludePath", "GameFeatureAction_AddCheats.h" },
		{ "ModuleRelativePath", "Public/GameFeatureAction_AddCheats.h" },
		{ "ToolTip", "Adds cheat manager extensions to the cheat manager for each player" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UGameFeatureAction_AddCheats_Statics::NewProp_CheatManagers_Inner = { "CheatManagers", nullptr, (EPropertyFlags)0x0004000000000000, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UCheatManagerExtension_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameFeatureAction_AddCheats_Statics::NewProp_CheatManagers_MetaData[] = {
		{ "Category", "Cheats" },
		{ "Comment", "/** Cheat managers to setup for the game feature plugin */" },
		{ "ModuleRelativePath", "Public/GameFeatureAction_AddCheats.h" },
		{ "ToolTip", "Cheat managers to setup for the game feature plugin" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UGameFeatureAction_AddCheats_Statics::NewProp_CheatManagers = { "CheatManagers", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGameFeatureAction_AddCheats, CheatManagers), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UGameFeatureAction_AddCheats_Statics::NewProp_CheatManagers_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGameFeatureAction_AddCheats_Statics::NewProp_CheatManagers_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGameFeatureAction_AddCheats_Statics::NewProp_SpawnedCheatManagers_Inner = { "SpawnedCheatManagers", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UCheatManagerExtension_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameFeatureAction_AddCheats_Statics::NewProp_SpawnedCheatManagers_MetaData[] = {
		{ "ModuleRelativePath", "Public/GameFeatureAction_AddCheats.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UGameFeatureAction_AddCheats_Statics::NewProp_SpawnedCheatManagers = { "SpawnedCheatManagers", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGameFeatureAction_AddCheats, SpawnedCheatManagers), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UGameFeatureAction_AddCheats_Statics::NewProp_SpawnedCheatManagers_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGameFeatureAction_AddCheats_Statics::NewProp_SpawnedCheatManagers_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGameFeatureAction_AddCheats_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGameFeatureAction_AddCheats_Statics::NewProp_CheatManagers_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGameFeatureAction_AddCheats_Statics::NewProp_CheatManagers,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGameFeatureAction_AddCheats_Statics::NewProp_SpawnedCheatManagers_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGameFeatureAction_AddCheats_Statics::NewProp_SpawnedCheatManagers,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGameFeatureAction_AddCheats_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGameFeatureAction_AddCheats>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGameFeatureAction_AddCheats_Statics::ClassParams = {
		&UGameFeatureAction_AddCheats::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UGameFeatureAction_AddCheats_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UGameFeatureAction_AddCheats_Statics::PropPointers),
		0,
		0x002810A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGameFeatureAction_AddCheats_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGameFeatureAction_AddCheats_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGameFeatureAction_AddCheats()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGameFeatureAction_AddCheats_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGameFeatureAction_AddCheats, 3374029671);
	template<> GAMEFEATURES_API UClass* StaticClass<UGameFeatureAction_AddCheats>()
	{
		return UGameFeatureAction_AddCheats::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGameFeatureAction_AddCheats(Z_Construct_UClass_UGameFeatureAction_AddCheats, &UGameFeatureAction_AddCheats::StaticClass, TEXT("/Script/GameFeatures"), TEXT("UGameFeatureAction_AddCheats"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGameFeatureAction_AddCheats);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
