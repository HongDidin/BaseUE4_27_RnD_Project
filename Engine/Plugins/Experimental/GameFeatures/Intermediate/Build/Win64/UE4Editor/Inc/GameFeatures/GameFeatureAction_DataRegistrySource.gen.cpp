// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GameFeatures/Public/GameFeatureAction_DataRegistrySource.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGameFeatureAction_DataRegistrySource() {}
// Cross Module References
	GAMEFEATURES_API UScriptStruct* Z_Construct_UScriptStruct_FDataRegistrySourceToAdd();
	UPackage* Z_Construct_UPackage__Script_GameFeatures();
	ENGINE_API UClass* Z_Construct_UClass_UDataTable_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UCurveTable_NoRegister();
	GAMEFEATURES_API UClass* Z_Construct_UClass_UGameFeatureAction_DataRegistrySource_NoRegister();
	GAMEFEATURES_API UClass* Z_Construct_UClass_UGameFeatureAction_DataRegistrySource();
	GAMEFEATURES_API UClass* Z_Construct_UClass_UGameFeatureAction();
// End Cross Module References
class UScriptStruct* FDataRegistrySourceToAdd::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern GAMEFEATURES_API uint32 Get_Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDataRegistrySourceToAdd, Z_Construct_UPackage__Script_GameFeatures(), TEXT("DataRegistrySourceToAdd"), sizeof(FDataRegistrySourceToAdd), Get_Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Hash());
	}
	return Singleton;
}
template<> GAMEFEATURES_API UScriptStruct* StaticStruct<FDataRegistrySourceToAdd>()
{
	return FDataRegistrySourceToAdd::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDataRegistrySourceToAdd(FDataRegistrySourceToAdd::StaticStruct, TEXT("/Script/GameFeatures"), TEXT("DataRegistrySourceToAdd"), false, nullptr, nullptr);
static struct FScriptStruct_GameFeatures_StaticRegisterNativesFDataRegistrySourceToAdd
{
	FScriptStruct_GameFeatures_StaticRegisterNativesFDataRegistrySourceToAdd()
	{
		UScriptStruct::DeferCppStructOps<FDataRegistrySourceToAdd>(FName(TEXT("DataRegistrySourceToAdd")));
	}
} ScriptStruct_GameFeatures_StaticRegisterNativesFDataRegistrySourceToAdd;
	struct Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RegistryToAddTo_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_RegistryToAddTo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AssetPriority_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_AssetPriority;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bClientSource_MetaData[];
#endif
		static void NewProp_bClientSource_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bClientSource;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bServerSource_MetaData[];
#endif
		static void NewProp_bServerSource_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bServerSource;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DataTableToAdd_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_DataTableToAdd;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurveTableToAdd_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_CurveTableToAdd;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Defines which source assets to add and conditions for adding */" },
		{ "ModuleRelativePath", "Public/GameFeatureAction_DataRegistrySource.h" },
		{ "ToolTip", "Defines which source assets to add and conditions for adding" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDataRegistrySourceToAdd>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::NewProp_RegistryToAddTo_MetaData[] = {
		{ "Category", "Registry Data" },
		{ "Comment", "/** Name of the registry to add to */" },
		{ "ModuleRelativePath", "Public/GameFeatureAction_DataRegistrySource.h" },
		{ "ToolTip", "Name of the registry to add to" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::NewProp_RegistryToAddTo = { "RegistryToAddTo", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDataRegistrySourceToAdd, RegistryToAddTo), METADATA_PARAMS(Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::NewProp_RegistryToAddTo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::NewProp_RegistryToAddTo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::NewProp_AssetPriority_MetaData[] = {
		{ "Category", "Registry Data" },
		{ "Comment", "/** Priority to use when adding to the registry.  Higher priorities are searched first */" },
		{ "ModuleRelativePath", "Public/GameFeatureAction_DataRegistrySource.h" },
		{ "ToolTip", "Priority to use when adding to the registry.  Higher priorities are searched first" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::NewProp_AssetPriority = { "AssetPriority", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDataRegistrySourceToAdd, AssetPriority), METADATA_PARAMS(Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::NewProp_AssetPriority_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::NewProp_AssetPriority_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::NewProp_bClientSource_MetaData[] = {
		{ "Category", "Registry Data" },
		{ "Comment", "/** Should this component be added for clients */" },
		{ "ModuleRelativePath", "Public/GameFeatureAction_DataRegistrySource.h" },
		{ "ToolTip", "Should this component be added for clients" },
	};
#endif
	void Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::NewProp_bClientSource_SetBit(void* Obj)
	{
		((FDataRegistrySourceToAdd*)Obj)->bClientSource = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::NewProp_bClientSource = { "bClientSource", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool , RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(uint8), sizeof(FDataRegistrySourceToAdd), &Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::NewProp_bClientSource_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::NewProp_bClientSource_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::NewProp_bClientSource_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::NewProp_bServerSource_MetaData[] = {
		{ "Category", "Registry Data" },
		{ "Comment", "/** Should this component be added on servers */" },
		{ "ModuleRelativePath", "Public/GameFeatureAction_DataRegistrySource.h" },
		{ "ToolTip", "Should this component be added on servers" },
	};
#endif
	void Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::NewProp_bServerSource_SetBit(void* Obj)
	{
		((FDataRegistrySourceToAdd*)Obj)->bServerSource = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::NewProp_bServerSource = { "bServerSource", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool , RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(uint8), sizeof(FDataRegistrySourceToAdd), &Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::NewProp_bServerSource_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::NewProp_bServerSource_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::NewProp_bServerSource_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::NewProp_DataTableToAdd_MetaData[] = {
		{ "Category", "Registry Data" },
		{ "Comment", "/** Link to the data table to add to the registry */" },
		{ "ModuleRelativePath", "Public/GameFeatureAction_DataRegistrySource.h" },
		{ "ToolTip", "Link to the data table to add to the registry" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::NewProp_DataTableToAdd = { "DataTableToAdd", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDataRegistrySourceToAdd, DataTableToAdd), Z_Construct_UClass_UDataTable_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::NewProp_DataTableToAdd_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::NewProp_DataTableToAdd_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::NewProp_CurveTableToAdd_MetaData[] = {
		{ "Category", "Registry Data" },
		{ "Comment", "/** Link to the curve table to add to the registry */" },
		{ "ModuleRelativePath", "Public/GameFeatureAction_DataRegistrySource.h" },
		{ "ToolTip", "Link to the curve table to add to the registry" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::NewProp_CurveTableToAdd = { "CurveTableToAdd", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDataRegistrySourceToAdd, CurveTableToAdd), Z_Construct_UClass_UCurveTable_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::NewProp_CurveTableToAdd_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::NewProp_CurveTableToAdd_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::NewProp_RegistryToAddTo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::NewProp_AssetPriority,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::NewProp_bClientSource,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::NewProp_bServerSource,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::NewProp_DataTableToAdd,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::NewProp_CurveTableToAdd,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_GameFeatures,
		nullptr,
		&NewStructOps,
		"DataRegistrySourceToAdd",
		sizeof(FDataRegistrySourceToAdd),
		alignof(FDataRegistrySourceToAdd),
		Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDataRegistrySourceToAdd()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_GameFeatures();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DataRegistrySourceToAdd"), sizeof(FDataRegistrySourceToAdd), Get_Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDataRegistrySourceToAdd_Hash() { return 778260675U; }
	void UGameFeatureAction_DataRegistrySource::StaticRegisterNativesUGameFeatureAction_DataRegistrySource()
	{
	}
	UClass* Z_Construct_UClass_UGameFeatureAction_DataRegistrySource_NoRegister()
	{
		return UGameFeatureAction_DataRegistrySource::StaticClass();
	}
	struct Z_Construct_UClass_UGameFeatureAction_DataRegistrySource_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SourcesToAdd_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourcesToAdd_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SourcesToAdd;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGameFeatureAction_DataRegistrySource_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGameFeatureAction,
		(UObject* (*)())Z_Construct_UPackage__Script_GameFeatures,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameFeatureAction_DataRegistrySource_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Specifies a list of source assets to add to Data Registries when this feature is activated */" },
		{ "DisplayName", "Add Data Registry Source" },
		{ "IncludePath", "GameFeatureAction_DataRegistrySource.h" },
		{ "ModuleRelativePath", "Public/GameFeatureAction_DataRegistrySource.h" },
		{ "ToolTip", "Specifies a list of source assets to add to Data Registries when this feature is activated" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGameFeatureAction_DataRegistrySource_Statics::NewProp_SourcesToAdd_Inner = { "SourcesToAdd", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDataRegistrySourceToAdd, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameFeatureAction_DataRegistrySource_Statics::NewProp_SourcesToAdd_MetaData[] = {
		{ "Category", "Registry Data" },
		{ "ModuleRelativePath", "Public/GameFeatureAction_DataRegistrySource.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UGameFeatureAction_DataRegistrySource_Statics::NewProp_SourcesToAdd = { "SourcesToAdd", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGameFeatureAction_DataRegistrySource, SourcesToAdd), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UGameFeatureAction_DataRegistrySource_Statics::NewProp_SourcesToAdd_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGameFeatureAction_DataRegistrySource_Statics::NewProp_SourcesToAdd_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGameFeatureAction_DataRegistrySource_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGameFeatureAction_DataRegistrySource_Statics::NewProp_SourcesToAdd_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGameFeatureAction_DataRegistrySource_Statics::NewProp_SourcesToAdd,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGameFeatureAction_DataRegistrySource_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGameFeatureAction_DataRegistrySource>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGameFeatureAction_DataRegistrySource_Statics::ClassParams = {
		&UGameFeatureAction_DataRegistrySource::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UGameFeatureAction_DataRegistrySource_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UGameFeatureAction_DataRegistrySource_Statics::PropPointers),
		0,
		0x002810A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGameFeatureAction_DataRegistrySource_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGameFeatureAction_DataRegistrySource_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGameFeatureAction_DataRegistrySource()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGameFeatureAction_DataRegistrySource_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGameFeatureAction_DataRegistrySource, 2033320597);
	template<> GAMEFEATURES_API UClass* StaticClass<UGameFeatureAction_DataRegistrySource>()
	{
		return UGameFeatureAction_DataRegistrySource::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGameFeatureAction_DataRegistrySource(Z_Construct_UClass_UGameFeatureAction_DataRegistrySource, &UGameFeatureAction_DataRegistrySource::StaticClass, TEXT("/Script/GameFeatures"), TEXT("UGameFeatureAction_DataRegistrySource"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGameFeatureAction_DataRegistrySource);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
