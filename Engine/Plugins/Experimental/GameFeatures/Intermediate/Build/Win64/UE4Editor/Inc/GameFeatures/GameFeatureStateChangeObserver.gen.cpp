// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GameFeatures/Public/GameFeatureStateChangeObserver.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGameFeatureStateChangeObserver() {}
// Cross Module References
	GAMEFEATURES_API UClass* Z_Construct_UClass_UGameFeatureStateChangeObserver_NoRegister();
	GAMEFEATURES_API UClass* Z_Construct_UClass_UGameFeatureStateChangeObserver();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_GameFeatures();
// End Cross Module References
	void UGameFeatureStateChangeObserver::StaticRegisterNativesUGameFeatureStateChangeObserver()
	{
	}
	UClass* Z_Construct_UClass_UGameFeatureStateChangeObserver_NoRegister()
	{
		return UGameFeatureStateChangeObserver::StaticClass();
	}
	struct Z_Construct_UClass_UGameFeatureStateChangeObserver_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGameFeatureStateChangeObserver_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_GameFeatures,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameFeatureStateChangeObserver_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * This class is meant to be overridden in your game to handle game-specific reactions to game feature plugins\n * being mounted or unmounted\n *\n * Generally you should prefer to use UGameFeatureAction instances on your game feature data asset instead of\n * this, especially if any data is involved\n *\n * If you do use these, create them in your UGameFeaturesProjectPolicies subclass and register them via\n * AddObserver / RemoveObserver on UGameFeaturesSubsystem\n */" },
		{ "IncludePath", "GameFeatureStateChangeObserver.h" },
		{ "ModuleRelativePath", "Public/GameFeatureStateChangeObserver.h" },
		{ "ToolTip", "This class is meant to be overridden in your game to handle game-specific reactions to game feature plugins\nbeing mounted or unmounted\n\nGenerally you should prefer to use UGameFeatureAction instances on your game feature data asset instead of\nthis, especially if any data is involved\n\nIf you do use these, create them in your UGameFeaturesProjectPolicies subclass and register them via\nAddObserver / RemoveObserver on UGameFeaturesSubsystem" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGameFeatureStateChangeObserver_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGameFeatureStateChangeObserver>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGameFeatureStateChangeObserver_Statics::ClassParams = {
		&UGameFeatureStateChangeObserver::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGameFeatureStateChangeObserver_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGameFeatureStateChangeObserver_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGameFeatureStateChangeObserver()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGameFeatureStateChangeObserver_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGameFeatureStateChangeObserver, 3576750598);
	template<> GAMEFEATURES_API UClass* StaticClass<UGameFeatureStateChangeObserver>()
	{
		return UGameFeatureStateChangeObserver::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGameFeatureStateChangeObserver(Z_Construct_UClass_UGameFeatureStateChangeObserver, &UGameFeatureStateChangeObserver::StaticClass, TEXT("/Script/GameFeatures"), TEXT("UGameFeatureStateChangeObserver"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGameFeatureStateChangeObserver);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
