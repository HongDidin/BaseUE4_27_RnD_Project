// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMEFEATURES_GameFeaturePluginStateMachine_generated_h
#error "GameFeaturePluginStateMachine.generated.h already included, missing '#pragma once' in GameFeaturePluginStateMachine.h"
#endif
#define GAMEFEATURES_GameFeaturePluginStateMachine_generated_h

#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Private_GameFeaturePluginStateMachine_h_158_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FGameFeaturePluginStateMachineProperties_Statics; \
	GAMEFEATURES_API static class UScriptStruct* StaticStruct();


template<> GAMEFEATURES_API UScriptStruct* StaticStruct<struct FGameFeaturePluginStateMachineProperties>();

#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Private_GameFeaturePluginStateMachine_h_255_SPARSE_DATA
#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Private_GameFeaturePluginStateMachine_h_255_RPC_WRAPPERS
#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Private_GameFeaturePluginStateMachine_h_255_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Private_GameFeaturePluginStateMachine_h_255_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGameFeaturePluginStateMachine(); \
	friend struct Z_Construct_UClass_UGameFeaturePluginStateMachine_Statics; \
public: \
	DECLARE_CLASS(UGameFeaturePluginStateMachine, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GameFeatures"), NO_API) \
	DECLARE_SERIALIZER(UGameFeaturePluginStateMachine)


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Private_GameFeaturePluginStateMachine_h_255_INCLASS \
private: \
	static void StaticRegisterNativesUGameFeaturePluginStateMachine(); \
	friend struct Z_Construct_UClass_UGameFeaturePluginStateMachine_Statics; \
public: \
	DECLARE_CLASS(UGameFeaturePluginStateMachine, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GameFeatures"), NO_API) \
	DECLARE_SERIALIZER(UGameFeaturePluginStateMachine)


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Private_GameFeaturePluginStateMachine_h_255_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGameFeaturePluginStateMachine(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGameFeaturePluginStateMachine) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGameFeaturePluginStateMachine); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGameFeaturePluginStateMachine); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGameFeaturePluginStateMachine(UGameFeaturePluginStateMachine&&); \
	NO_API UGameFeaturePluginStateMachine(const UGameFeaturePluginStateMachine&); \
public:


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Private_GameFeaturePluginStateMachine_h_255_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGameFeaturePluginStateMachine(UGameFeaturePluginStateMachine&&); \
	NO_API UGameFeaturePluginStateMachine(const UGameFeaturePluginStateMachine&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGameFeaturePluginStateMachine); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGameFeaturePluginStateMachine); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGameFeaturePluginStateMachine)


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Private_GameFeaturePluginStateMachine_h_255_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__StateProperties() { return STRUCT_OFFSET(UGameFeaturePluginStateMachine, StateProperties); }


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Private_GameFeaturePluginStateMachine_h_252_PROLOG
#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Private_GameFeaturePluginStateMachine_h_255_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Private_GameFeaturePluginStateMachine_h_255_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Private_GameFeaturePluginStateMachine_h_255_SPARSE_DATA \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Private_GameFeaturePluginStateMachine_h_255_RPC_WRAPPERS \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Private_GameFeaturePluginStateMachine_h_255_INCLASS \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Private_GameFeaturePluginStateMachine_h_255_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Private_GameFeaturePluginStateMachine_h_255_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Private_GameFeaturePluginStateMachine_h_255_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Private_GameFeaturePluginStateMachine_h_255_SPARSE_DATA \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Private_GameFeaturePluginStateMachine_h_255_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Private_GameFeaturePluginStateMachine_h_255_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Private_GameFeaturePluginStateMachine_h_255_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMEFEATURES_API UClass* StaticClass<class UGameFeaturePluginStateMachine>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Private_GameFeaturePluginStateMachine_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
