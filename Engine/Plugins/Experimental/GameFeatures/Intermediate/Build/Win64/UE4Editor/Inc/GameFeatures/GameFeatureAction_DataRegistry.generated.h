// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMEFEATURES_GameFeatureAction_DataRegistry_generated_h
#error "GameFeatureAction_DataRegistry.generated.h already included, missing '#pragma once' in GameFeatureAction_DataRegistry.h"
#endif
#define GAMEFEATURES_GameFeatureAction_DataRegistry_generated_h

#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_DataRegistry_h_15_SPARSE_DATA
#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_DataRegistry_h_15_RPC_WRAPPERS
#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_DataRegistry_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_DataRegistry_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGameFeatureAction_DataRegistry(); \
	friend struct Z_Construct_UClass_UGameFeatureAction_DataRegistry_Statics; \
public: \
	DECLARE_CLASS(UGameFeatureAction_DataRegistry, UGameFeatureAction, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GameFeatures"), GAMEFEATURES_API) \
	DECLARE_SERIALIZER(UGameFeatureAction_DataRegistry)


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_DataRegistry_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUGameFeatureAction_DataRegistry(); \
	friend struct Z_Construct_UClass_UGameFeatureAction_DataRegistry_Statics; \
public: \
	DECLARE_CLASS(UGameFeatureAction_DataRegistry, UGameFeatureAction, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GameFeatures"), GAMEFEATURES_API) \
	DECLARE_SERIALIZER(UGameFeatureAction_DataRegistry)


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_DataRegistry_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	GAMEFEATURES_API UGameFeatureAction_DataRegistry(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGameFeatureAction_DataRegistry) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(GAMEFEATURES_API, UGameFeatureAction_DataRegistry); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGameFeatureAction_DataRegistry); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	GAMEFEATURES_API UGameFeatureAction_DataRegistry(UGameFeatureAction_DataRegistry&&); \
	GAMEFEATURES_API UGameFeatureAction_DataRegistry(const UGameFeatureAction_DataRegistry&); \
public:


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_DataRegistry_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	GAMEFEATURES_API UGameFeatureAction_DataRegistry(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	GAMEFEATURES_API UGameFeatureAction_DataRegistry(UGameFeatureAction_DataRegistry&&); \
	GAMEFEATURES_API UGameFeatureAction_DataRegistry(const UGameFeatureAction_DataRegistry&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(GAMEFEATURES_API, UGameFeatureAction_DataRegistry); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGameFeatureAction_DataRegistry); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGameFeatureAction_DataRegistry)


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_DataRegistry_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__RegistriesToAdd() { return STRUCT_OFFSET(UGameFeatureAction_DataRegistry, RegistriesToAdd); }


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_DataRegistry_h_12_PROLOG
#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_DataRegistry_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_DataRegistry_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_DataRegistry_h_15_SPARSE_DATA \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_DataRegistry_h_15_RPC_WRAPPERS \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_DataRegistry_h_15_INCLASS \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_DataRegistry_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_DataRegistry_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_DataRegistry_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_DataRegistry_h_15_SPARSE_DATA \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_DataRegistry_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_DataRegistry_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_DataRegistry_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMEFEATURES_API UClass* StaticClass<class UGameFeatureAction_DataRegistry>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureAction_DataRegistry_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
