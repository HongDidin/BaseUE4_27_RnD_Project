// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GameFeatures/Public/GameFeaturesSubsystemSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGameFeaturesSubsystemSettings() {}
// Cross Module References
	GAMEFEATURES_API UClass* Z_Construct_UClass_UGameFeaturesSubsystemSettings_NoRegister();
	GAMEFEATURES_API UClass* Z_Construct_UClass_UGameFeaturesSubsystemSettings();
	DEVELOPERSETTINGS_API UClass* Z_Construct_UClass_UDeveloperSettings();
	UPackage* Z_Construct_UPackage__Script_GameFeatures();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FSoftClassPath();
// End Cross Module References
	void UGameFeaturesSubsystemSettings::StaticRegisterNativesUGameFeaturesSubsystemSettings()
	{
	}
	UClass* Z_Construct_UClass_UGameFeaturesSubsystemSettings_NoRegister()
	{
		return UGameFeaturesSubsystemSettings::StaticClass();
	}
	struct Z_Construct_UClass_UGameFeaturesSubsystemSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GameFeaturesManagerClassName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GameFeaturesManagerClassName;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DisabledPlugins_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisabledPlugins_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DisabledPlugins;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AdditionalPluginMetadataKeys_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AdditionalPluginMetadataKeys_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_AdditionalPluginMetadataKeys;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BuiltInGameFeaturePluginsFolder_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_BuiltInGameFeaturePluginsFolder;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGameFeaturesSubsystemSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDeveloperSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_GameFeatures,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameFeaturesSubsystemSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Settings for the Game Features framework */" },
		{ "DisplayName", "Game Features" },
		{ "IncludePath", "GameFeaturesSubsystemSettings.h" },
		{ "ModuleRelativePath", "Public/GameFeaturesSubsystemSettings.h" },
		{ "ToolTip", "Settings for the Game Features framework" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameFeaturesSubsystemSettings_Statics::NewProp_GameFeaturesManagerClassName_MetaData[] = {
		{ "Category", "DefaultClasses" },
		{ "Comment", "/** Name of a singleton class to spawn as the AssetManager, configurable per game. If empty, it will spawn the default one (UDefaultGameFeaturesProjectPolicies) */" },
		{ "ConfigRestartRequired", "TRUE" },
		{ "DisplayName", "Game Feature Manager Class" },
		{ "MetaClass", "GameFeaturesProjectPolicies" },
		{ "ModuleRelativePath", "Public/GameFeaturesSubsystemSettings.h" },
		{ "ToolTip", "Name of a singleton class to spawn as the AssetManager, configurable per game. If empty, it will spawn the default one (UDefaultGameFeaturesProjectPolicies)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGameFeaturesSubsystemSettings_Statics::NewProp_GameFeaturesManagerClassName = { "GameFeaturesManagerClassName", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGameFeaturesSubsystemSettings, GameFeaturesManagerClassName), Z_Construct_UScriptStruct_FSoftClassPath, METADATA_PARAMS(Z_Construct_UClass_UGameFeaturesSubsystemSettings_Statics::NewProp_GameFeaturesManagerClassName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGameFeaturesSubsystemSettings_Statics::NewProp_GameFeaturesManagerClassName_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UGameFeaturesSubsystemSettings_Statics::NewProp_DisabledPlugins_Inner = { "DisabledPlugins", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameFeaturesSubsystemSettings_Statics::NewProp_DisabledPlugins_MetaData[] = {
		{ "Category", "GameFeatures" },
		{ "Comment", "/** List of plugins that are forcibly disabled (e.g., via a hotfix) */" },
		{ "ModuleRelativePath", "Public/GameFeaturesSubsystemSettings.h" },
		{ "ToolTip", "List of plugins that are forcibly disabled (e.g., via a hotfix)" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UGameFeaturesSubsystemSettings_Statics::NewProp_DisabledPlugins = { "DisabledPlugins", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGameFeaturesSubsystemSettings, DisabledPlugins), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UGameFeaturesSubsystemSettings_Statics::NewProp_DisabledPlugins_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGameFeaturesSubsystemSettings_Statics::NewProp_DisabledPlugins_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UGameFeaturesSubsystemSettings_Statics::NewProp_AdditionalPluginMetadataKeys_Inner = { "AdditionalPluginMetadataKeys", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameFeaturesSubsystemSettings_Statics::NewProp_AdditionalPluginMetadataKeys_MetaData[] = {
		{ "Category", "GameFeatures" },
		{ "Comment", "/** List of metadata (additional keys) to try parsing from the .uplugin to provide to FGameFeaturePluginDetails */" },
		{ "ModuleRelativePath", "Public/GameFeaturesSubsystemSettings.h" },
		{ "ToolTip", "List of metadata (additional keys) to try parsing from the .uplugin to provide to FGameFeaturePluginDetails" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UGameFeaturesSubsystemSettings_Statics::NewProp_AdditionalPluginMetadataKeys = { "AdditionalPluginMetadataKeys", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGameFeaturesSubsystemSettings, AdditionalPluginMetadataKeys), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UGameFeaturesSubsystemSettings_Statics::NewProp_AdditionalPluginMetadataKeys_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGameFeaturesSubsystemSettings_Statics::NewProp_AdditionalPluginMetadataKeys_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameFeaturesSubsystemSettings_Statics::NewProp_BuiltInGameFeaturePluginsFolder_MetaData[] = {
		{ "Comment", "/** The folder in which all discovered plugins are automatically considered game feature plugins. Plugins outside this folder may also be game features, but need to have bGameFeature: true in the plugin file *///@TODO: GameFeaturePluginEnginePush: Make this configurable\n//@TODO: GameFeaturePluginEnginePush: This comment doesn't jive with some of the code in the subsystem which is only paying attention to plugins in this folder\n" },
		{ "ModuleRelativePath", "Public/GameFeaturesSubsystemSettings.h" },
		{ "ToolTip", "The folder in which all discovered plugins are automatically considered game feature plugins. Plugins outside this folder may also be game features, but need to have bGameFeature: true in the plugin file //@TODO: GameFeaturePluginEnginePush: Make this configurable\n//@TODO: GameFeaturePluginEnginePush: This comment doesn't jive with some of the code in the subsystem which is only paying attention to plugins in this folder" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UGameFeaturesSubsystemSettings_Statics::NewProp_BuiltInGameFeaturePluginsFolder = { "BuiltInGameFeaturePluginsFolder", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGameFeaturesSubsystemSettings, BuiltInGameFeaturePluginsFolder), METADATA_PARAMS(Z_Construct_UClass_UGameFeaturesSubsystemSettings_Statics::NewProp_BuiltInGameFeaturePluginsFolder_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGameFeaturesSubsystemSettings_Statics::NewProp_BuiltInGameFeaturePluginsFolder_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGameFeaturesSubsystemSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGameFeaturesSubsystemSettings_Statics::NewProp_GameFeaturesManagerClassName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGameFeaturesSubsystemSettings_Statics::NewProp_DisabledPlugins_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGameFeaturesSubsystemSettings_Statics::NewProp_DisabledPlugins,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGameFeaturesSubsystemSettings_Statics::NewProp_AdditionalPluginMetadataKeys_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGameFeaturesSubsystemSettings_Statics::NewProp_AdditionalPluginMetadataKeys,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGameFeaturesSubsystemSettings_Statics::NewProp_BuiltInGameFeaturePluginsFolder,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGameFeaturesSubsystemSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGameFeaturesSubsystemSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGameFeaturesSubsystemSettings_Statics::ClassParams = {
		&UGameFeaturesSubsystemSettings::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UGameFeaturesSubsystemSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UGameFeaturesSubsystemSettings_Statics::PropPointers),
		0,
		0x001002A6u,
		METADATA_PARAMS(Z_Construct_UClass_UGameFeaturesSubsystemSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGameFeaturesSubsystemSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGameFeaturesSubsystemSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGameFeaturesSubsystemSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGameFeaturesSubsystemSettings, 1088768699);
	template<> GAMEFEATURES_API UClass* StaticClass<UGameFeaturesSubsystemSettings>()
	{
		return UGameFeaturesSubsystemSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGameFeaturesSubsystemSettings(Z_Construct_UClass_UGameFeaturesSubsystemSettings, &UGameFeaturesSubsystemSettings::StaticClass, TEXT("/Script/GameFeatures"), TEXT("UGameFeaturesSubsystemSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGameFeaturesSubsystemSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
