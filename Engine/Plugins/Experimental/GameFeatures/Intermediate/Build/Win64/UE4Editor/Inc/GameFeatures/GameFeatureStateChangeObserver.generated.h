// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMEFEATURES_GameFeatureStateChangeObserver_generated_h
#error "GameFeatureStateChangeObserver.generated.h already included, missing '#pragma once' in GameFeatureStateChangeObserver.h"
#endif
#define GAMEFEATURES_GameFeatureStateChangeObserver_generated_h

#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureStateChangeObserver_h_24_SPARSE_DATA
#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureStateChangeObserver_h_24_RPC_WRAPPERS
#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureStateChangeObserver_h_24_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureStateChangeObserver_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGameFeatureStateChangeObserver(); \
	friend struct Z_Construct_UClass_UGameFeatureStateChangeObserver_Statics; \
public: \
	DECLARE_CLASS(UGameFeatureStateChangeObserver, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GameFeatures"), NO_API) \
	DECLARE_SERIALIZER(UGameFeatureStateChangeObserver)


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureStateChangeObserver_h_24_INCLASS \
private: \
	static void StaticRegisterNativesUGameFeatureStateChangeObserver(); \
	friend struct Z_Construct_UClass_UGameFeatureStateChangeObserver_Statics; \
public: \
	DECLARE_CLASS(UGameFeatureStateChangeObserver, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GameFeatures"), NO_API) \
	DECLARE_SERIALIZER(UGameFeatureStateChangeObserver)


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureStateChangeObserver_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGameFeatureStateChangeObserver(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGameFeatureStateChangeObserver) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGameFeatureStateChangeObserver); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGameFeatureStateChangeObserver); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGameFeatureStateChangeObserver(UGameFeatureStateChangeObserver&&); \
	NO_API UGameFeatureStateChangeObserver(const UGameFeatureStateChangeObserver&); \
public:


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureStateChangeObserver_h_24_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGameFeatureStateChangeObserver(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGameFeatureStateChangeObserver(UGameFeatureStateChangeObserver&&); \
	NO_API UGameFeatureStateChangeObserver(const UGameFeatureStateChangeObserver&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGameFeatureStateChangeObserver); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGameFeatureStateChangeObserver); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGameFeatureStateChangeObserver)


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureStateChangeObserver_h_24_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureStateChangeObserver_h_21_PROLOG
#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureStateChangeObserver_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureStateChangeObserver_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureStateChangeObserver_h_24_SPARSE_DATA \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureStateChangeObserver_h_24_RPC_WRAPPERS \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureStateChangeObserver_h_24_INCLASS \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureStateChangeObserver_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureStateChangeObserver_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureStateChangeObserver_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureStateChangeObserver_h_24_SPARSE_DATA \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureStateChangeObserver_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureStateChangeObserver_h_24_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureStateChangeObserver_h_24_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMEFEATURES_API UClass* StaticClass<class UGameFeatureStateChangeObserver>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeatureStateChangeObserver_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
