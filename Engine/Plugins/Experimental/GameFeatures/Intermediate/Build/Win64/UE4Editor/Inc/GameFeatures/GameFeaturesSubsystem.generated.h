// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMEFEATURES_GameFeaturesSubsystem_generated_h
#error "GameFeaturesSubsystem.generated.h already included, missing '#pragma once' in GameFeaturesSubsystem.h"
#endif
#define GAMEFEATURES_GameFeaturesSubsystem_generated_h

#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesSubsystem_h_94_SPARSE_DATA
#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesSubsystem_h_94_RPC_WRAPPERS
#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesSubsystem_h_94_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesSubsystem_h_94_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGameFeaturesSubsystem(); \
	friend struct Z_Construct_UClass_UGameFeaturesSubsystem_Statics; \
public: \
	DECLARE_CLASS(UGameFeaturesSubsystem, UEngineSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GameFeatures"), NO_API) \
	DECLARE_SERIALIZER(UGameFeaturesSubsystem)


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesSubsystem_h_94_INCLASS \
private: \
	static void StaticRegisterNativesUGameFeaturesSubsystem(); \
	friend struct Z_Construct_UClass_UGameFeaturesSubsystem_Statics; \
public: \
	DECLARE_CLASS(UGameFeaturesSubsystem, UEngineSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GameFeatures"), NO_API) \
	DECLARE_SERIALIZER(UGameFeaturesSubsystem)


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesSubsystem_h_94_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGameFeaturesSubsystem(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGameFeaturesSubsystem) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGameFeaturesSubsystem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGameFeaturesSubsystem); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGameFeaturesSubsystem(UGameFeaturesSubsystem&&); \
	NO_API UGameFeaturesSubsystem(const UGameFeaturesSubsystem&); \
public:


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesSubsystem_h_94_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGameFeaturesSubsystem() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGameFeaturesSubsystem(UGameFeaturesSubsystem&&); \
	NO_API UGameFeaturesSubsystem(const UGameFeaturesSubsystem&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGameFeaturesSubsystem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGameFeaturesSubsystem); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UGameFeaturesSubsystem)


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesSubsystem_h_94_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__GameFeaturePluginStateMachines() { return STRUCT_OFFSET(UGameFeaturesSubsystem, GameFeaturePluginStateMachines); } \
	FORCEINLINE static uint32 __PPO__Observers() { return STRUCT_OFFSET(UGameFeaturesSubsystem, Observers); } \
	FORCEINLINE static uint32 __PPO__GameSpecificPolicies() { return STRUCT_OFFSET(UGameFeaturesSubsystem, GameSpecificPolicies); }


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesSubsystem_h_91_PROLOG
#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesSubsystem_h_94_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesSubsystem_h_94_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesSubsystem_h_94_SPARSE_DATA \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesSubsystem_h_94_RPC_WRAPPERS \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesSubsystem_h_94_INCLASS \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesSubsystem_h_94_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesSubsystem_h_94_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesSubsystem_h_94_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesSubsystem_h_94_SPARSE_DATA \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesSubsystem_h_94_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesSubsystem_h_94_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesSubsystem_h_94_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMEFEATURES_API UClass* StaticClass<class UGameFeaturesSubsystem>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_GameFeatures_Source_GameFeatures_Public_GameFeaturesSubsystem_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
