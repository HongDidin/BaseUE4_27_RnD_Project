// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GameFeatures/Public/GameFeaturesProjectPolicies.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGameFeaturesProjectPolicies() {}
// Cross Module References
	GAMEFEATURES_API UClass* Z_Construct_UClass_UGameFeaturesProjectPolicies_NoRegister();
	GAMEFEATURES_API UClass* Z_Construct_UClass_UGameFeaturesProjectPolicies();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_GameFeatures();
	GAMEFEATURES_API UClass* Z_Construct_UClass_UDefaultGameFeaturesProjectPolicies_NoRegister();
	GAMEFEATURES_API UClass* Z_Construct_UClass_UDefaultGameFeaturesProjectPolicies();
// End Cross Module References
	void UGameFeaturesProjectPolicies::StaticRegisterNativesUGameFeaturesProjectPolicies()
	{
	}
	UClass* Z_Construct_UClass_UGameFeaturesProjectPolicies_NoRegister()
	{
		return UGameFeaturesProjectPolicies::StaticClass();
	}
	struct Z_Construct_UClass_UGameFeaturesProjectPolicies_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGameFeaturesProjectPolicies_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_GameFeatures,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameFeaturesProjectPolicies_Statics::Class_MetaDataParams[] = {
		{ "Comment", "// This class allows project-specific rules to be implemented for game feature plugins.\n// Create a subclass and choose it in Project Settings .. Game Features\n" },
		{ "IncludePath", "GameFeaturesProjectPolicies.h" },
		{ "ModuleRelativePath", "Public/GameFeaturesProjectPolicies.h" },
		{ "ToolTip", "This class allows project-specific rules to be implemented for game feature plugins.\nCreate a subclass and choose it in Project Settings .. Game Features" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGameFeaturesProjectPolicies_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGameFeaturesProjectPolicies>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGameFeaturesProjectPolicies_Statics::ClassParams = {
		&UGameFeaturesProjectPolicies::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGameFeaturesProjectPolicies_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGameFeaturesProjectPolicies_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGameFeaturesProjectPolicies()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGameFeaturesProjectPolicies_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGameFeaturesProjectPolicies, 1269908083);
	template<> GAMEFEATURES_API UClass* StaticClass<UGameFeaturesProjectPolicies>()
	{
		return UGameFeaturesProjectPolicies::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGameFeaturesProjectPolicies(Z_Construct_UClass_UGameFeaturesProjectPolicies, &UGameFeaturesProjectPolicies::StaticClass, TEXT("/Script/GameFeatures"), TEXT("UGameFeaturesProjectPolicies"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGameFeaturesProjectPolicies);
	void UDefaultGameFeaturesProjectPolicies::StaticRegisterNativesUDefaultGameFeaturesProjectPolicies()
	{
	}
	UClass* Z_Construct_UClass_UDefaultGameFeaturesProjectPolicies_NoRegister()
	{
		return UDefaultGameFeaturesProjectPolicies::StaticClass();
	}
	struct Z_Construct_UClass_UDefaultGameFeaturesProjectPolicies_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDefaultGameFeaturesProjectPolicies_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGameFeaturesProjectPolicies,
		(UObject* (*)())Z_Construct_UPackage__Script_GameFeatures,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDefaultGameFeaturesProjectPolicies_Statics::Class_MetaDataParams[] = {
		{ "Comment", "// This is a default implementation that immediately processes all game feature plugins the based on\n// their BuiltInAutoRegister, BuiltInAutoLoad, and BuiltInAutoActivate settings.\n//\n// It will be used if no project-specific policy is set in Project Settings .. Game Features\n" },
		{ "IncludePath", "GameFeaturesProjectPolicies.h" },
		{ "ModuleRelativePath", "Public/GameFeaturesProjectPolicies.h" },
		{ "ToolTip", "This is a default implementation that immediately processes all game feature plugins the based on\ntheir BuiltInAutoRegister, BuiltInAutoLoad, and BuiltInAutoActivate settings.\n\nIt will be used if no project-specific policy is set in Project Settings .. Game Features" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDefaultGameFeaturesProjectPolicies_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDefaultGameFeaturesProjectPolicies>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDefaultGameFeaturesProjectPolicies_Statics::ClassParams = {
		&UDefaultGameFeaturesProjectPolicies::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDefaultGameFeaturesProjectPolicies_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDefaultGameFeaturesProjectPolicies_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDefaultGameFeaturesProjectPolicies()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDefaultGameFeaturesProjectPolicies_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDefaultGameFeaturesProjectPolicies, 2913836075);
	template<> GAMEFEATURES_API UClass* StaticClass<UDefaultGameFeaturesProjectPolicies>()
	{
		return UDefaultGameFeaturesProjectPolicies::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDefaultGameFeaturesProjectPolicies(Z_Construct_UClass_UDefaultGameFeaturesProjectPolicies, &UDefaultGameFeaturesProjectPolicies::StaticClass, TEXT("/Script/GameFeatures"), TEXT("UDefaultGameFeaturesProjectPolicies"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDefaultGameFeaturesProjectPolicies);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
