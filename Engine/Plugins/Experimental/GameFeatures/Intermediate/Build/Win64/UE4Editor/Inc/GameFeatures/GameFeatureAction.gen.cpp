// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GameFeatures/Public/GameFeatureAction.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGameFeatureAction() {}
// Cross Module References
	GAMEFEATURES_API UClass* Z_Construct_UClass_UGameFeatureAction_NoRegister();
	GAMEFEATURES_API UClass* Z_Construct_UClass_UGameFeatureAction();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_GameFeatures();
// End Cross Module References
	void UGameFeatureAction::StaticRegisterNativesUGameFeatureAction()
	{
	}
	UClass* Z_Construct_UClass_UGameFeatureAction_NoRegister()
	{
		return UGameFeatureAction::StaticClass();
	}
	struct Z_Construct_UClass_UGameFeatureAction_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGameFeatureAction_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_GameFeatures,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameFeatureAction_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Represents an action to be taken when a game feature is activated */" },
		{ "IncludePath", "GameFeatureAction.h" },
		{ "ModuleRelativePath", "Public/GameFeatureAction.h" },
		{ "ToolTip", "Represents an action to be taken when a game feature is activated" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGameFeatureAction_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGameFeatureAction>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGameFeatureAction_Statics::ClassParams = {
		&UGameFeatureAction::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x003010A1u,
		METADATA_PARAMS(Z_Construct_UClass_UGameFeatureAction_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGameFeatureAction_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGameFeatureAction()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGameFeatureAction_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGameFeatureAction, 1611433603);
	template<> GAMEFEATURES_API UClass* StaticClass<UGameFeatureAction>()
	{
		return UGameFeatureAction::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGameFeatureAction(Z_Construct_UClass_UGameFeatureAction, &UGameFeatureAction::StaticClass, TEXT("/Script/GameFeatures"), TEXT("UGameFeatureAction"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGameFeatureAction);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
