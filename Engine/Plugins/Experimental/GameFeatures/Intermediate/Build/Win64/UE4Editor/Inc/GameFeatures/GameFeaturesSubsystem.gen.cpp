// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GameFeatures/Public/GameFeaturesSubsystem.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGameFeaturesSubsystem() {}
// Cross Module References
	GAMEFEATURES_API UClass* Z_Construct_UClass_UGameFeaturesSubsystem_NoRegister();
	GAMEFEATURES_API UClass* Z_Construct_UClass_UGameFeaturesSubsystem();
	ENGINE_API UClass* Z_Construct_UClass_UEngineSubsystem();
	UPackage* Z_Construct_UPackage__Script_GameFeatures();
	GAMEFEATURES_API UClass* Z_Construct_UClass_UGameFeaturePluginStateMachine_NoRegister();
	GAMEFEATURES_API UClass* Z_Construct_UClass_UGameFeatureStateChangeObserver_NoRegister();
	GAMEFEATURES_API UClass* Z_Construct_UClass_UGameFeaturesProjectPolicies_NoRegister();
// End Cross Module References
	void UGameFeaturesSubsystem::StaticRegisterNativesUGameFeaturesSubsystem()
	{
	}
	UClass* Z_Construct_UClass_UGameFeaturesSubsystem_NoRegister()
	{
		return UGameFeaturesSubsystem::StaticClass();
	}
	struct Z_Construct_UClass_UGameFeaturesSubsystem_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_GameFeaturePluginStateMachines_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_GameFeaturePluginStateMachines_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GameFeaturePluginStateMachines_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_GameFeaturePluginStateMachines;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Observers_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Observers_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Observers;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GameSpecificPolicies_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_GameSpecificPolicies;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGameFeaturesSubsystem_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEngineSubsystem,
		(UObject* (*)())Z_Construct_UPackage__Script_GameFeatures,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameFeaturesSubsystem_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** The manager subsystem for game features */" },
		{ "IncludePath", "GameFeaturesSubsystem.h" },
		{ "ModuleRelativePath", "Public/GameFeaturesSubsystem.h" },
		{ "ToolTip", "The manager subsystem for game features" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGameFeaturesSubsystem_Statics::NewProp_GameFeaturePluginStateMachines_ValueProp = { "GameFeaturePluginStateMachines", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_UGameFeaturePluginStateMachine_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UGameFeaturesSubsystem_Statics::NewProp_GameFeaturePluginStateMachines_Key_KeyProp = { "GameFeaturePluginStateMachines_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameFeaturesSubsystem_Statics::NewProp_GameFeaturePluginStateMachines_MetaData[] = {
		{ "Comment", "/** The list of all game feature plugin state machine objects */" },
		{ "ModuleRelativePath", "Public/GameFeaturesSubsystem.h" },
		{ "ToolTip", "The list of all game feature plugin state machine objects" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UGameFeaturesSubsystem_Statics::NewProp_GameFeaturePluginStateMachines = { "GameFeaturePluginStateMachines", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGameFeaturesSubsystem, GameFeaturePluginStateMachines), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UGameFeaturesSubsystem_Statics::NewProp_GameFeaturePluginStateMachines_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGameFeaturesSubsystem_Statics::NewProp_GameFeaturePluginStateMachines_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGameFeaturesSubsystem_Statics::NewProp_Observers_Inner = { "Observers", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UGameFeatureStateChangeObserver_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameFeaturesSubsystem_Statics::NewProp_Observers_MetaData[] = {
		{ "ModuleRelativePath", "Public/GameFeaturesSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UGameFeaturesSubsystem_Statics::NewProp_Observers = { "Observers", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGameFeaturesSubsystem, Observers), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UGameFeaturesSubsystem_Statics::NewProp_Observers_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGameFeaturesSubsystem_Statics::NewProp_Observers_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameFeaturesSubsystem_Statics::NewProp_GameSpecificPolicies_MetaData[] = {
		{ "ModuleRelativePath", "Public/GameFeaturesSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGameFeaturesSubsystem_Statics::NewProp_GameSpecificPolicies = { "GameSpecificPolicies", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGameFeaturesSubsystem, GameSpecificPolicies), Z_Construct_UClass_UGameFeaturesProjectPolicies_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGameFeaturesSubsystem_Statics::NewProp_GameSpecificPolicies_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGameFeaturesSubsystem_Statics::NewProp_GameSpecificPolicies_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGameFeaturesSubsystem_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGameFeaturesSubsystem_Statics::NewProp_GameFeaturePluginStateMachines_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGameFeaturesSubsystem_Statics::NewProp_GameFeaturePluginStateMachines_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGameFeaturesSubsystem_Statics::NewProp_GameFeaturePluginStateMachines,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGameFeaturesSubsystem_Statics::NewProp_Observers_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGameFeaturesSubsystem_Statics::NewProp_Observers,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGameFeaturesSubsystem_Statics::NewProp_GameSpecificPolicies,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGameFeaturesSubsystem_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGameFeaturesSubsystem>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGameFeaturesSubsystem_Statics::ClassParams = {
		&UGameFeaturesSubsystem::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UGameFeaturesSubsystem_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UGameFeaturesSubsystem_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGameFeaturesSubsystem_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGameFeaturesSubsystem_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGameFeaturesSubsystem()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGameFeaturesSubsystem_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGameFeaturesSubsystem, 3358790152);
	template<> GAMEFEATURES_API UClass* StaticClass<UGameFeaturesSubsystem>()
	{
		return UGameFeaturesSubsystem::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGameFeaturesSubsystem(Z_Construct_UClass_UGameFeaturesSubsystem, &UGameFeaturesSubsystem::StaticClass, TEXT("/Script/GameFeatures"), TEXT("UGameFeaturesSubsystem"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGameFeaturesSubsystem);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
