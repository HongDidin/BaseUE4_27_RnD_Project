// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef REMOTESESSION_RemoteSessionMediaOutput_generated_h
#error "RemoteSessionMediaOutput.generated.h already included, missing '#pragma once' in RemoteSessionMediaOutput.h"
#endif
#define REMOTESESSION_RemoteSessionMediaOutput_generated_h

#define Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_13_SPARSE_DATA
#define Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_13_RPC_WRAPPERS
#define Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURemoteSessionMediaOutput(); \
	friend struct Z_Construct_UClass_URemoteSessionMediaOutput_Statics; \
public: \
	DECLARE_CLASS(URemoteSessionMediaOutput, UMediaOutput, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RemoteSession"), NO_API) \
	DECLARE_SERIALIZER(URemoteSessionMediaOutput)


#define Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_13_INCLASS \
private: \
	static void StaticRegisterNativesURemoteSessionMediaOutput(); \
	friend struct Z_Construct_UClass_URemoteSessionMediaOutput_Statics; \
public: \
	DECLARE_CLASS(URemoteSessionMediaOutput, UMediaOutput, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RemoteSession"), NO_API) \
	DECLARE_SERIALIZER(URemoteSessionMediaOutput)


#define Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URemoteSessionMediaOutput(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URemoteSessionMediaOutput) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URemoteSessionMediaOutput); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URemoteSessionMediaOutput); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URemoteSessionMediaOutput(URemoteSessionMediaOutput&&); \
	NO_API URemoteSessionMediaOutput(const URemoteSessionMediaOutput&); \
public:


#define Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URemoteSessionMediaOutput(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URemoteSessionMediaOutput(URemoteSessionMediaOutput&&); \
	NO_API URemoteSessionMediaOutput(const URemoteSessionMediaOutput&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URemoteSessionMediaOutput); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URemoteSessionMediaOutput); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URemoteSessionMediaOutput)


#define Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_10_PROLOG
#define Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_13_SPARSE_DATA \
	Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_13_RPC_WRAPPERS \
	Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_13_INCLASS \
	Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_13_SPARSE_DATA \
	Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> REMOTESESSION_API UClass* StaticClass<class URemoteSessionMediaOutput>();

#define Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_38_SPARSE_DATA
#define Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_38_RPC_WRAPPERS
#define Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_38_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_38_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURemoteSessionMediaCapture(); \
	friend struct Z_Construct_UClass_URemoteSessionMediaCapture_Statics; \
public: \
	DECLARE_CLASS(URemoteSessionMediaCapture, UMediaCapture, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RemoteSession"), NO_API) \
	DECLARE_SERIALIZER(URemoteSessionMediaCapture)


#define Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_38_INCLASS \
private: \
	static void StaticRegisterNativesURemoteSessionMediaCapture(); \
	friend struct Z_Construct_UClass_URemoteSessionMediaCapture_Statics; \
public: \
	DECLARE_CLASS(URemoteSessionMediaCapture, UMediaCapture, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RemoteSession"), NO_API) \
	DECLARE_SERIALIZER(URemoteSessionMediaCapture)


#define Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_38_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URemoteSessionMediaCapture(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URemoteSessionMediaCapture) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URemoteSessionMediaCapture); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URemoteSessionMediaCapture); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URemoteSessionMediaCapture(URemoteSessionMediaCapture&&); \
	NO_API URemoteSessionMediaCapture(const URemoteSessionMediaCapture&); \
public:


#define Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_38_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URemoteSessionMediaCapture(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URemoteSessionMediaCapture(URemoteSessionMediaCapture&&); \
	NO_API URemoteSessionMediaCapture(const URemoteSessionMediaCapture&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URemoteSessionMediaCapture); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URemoteSessionMediaCapture); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URemoteSessionMediaCapture)


#define Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_38_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_35_PROLOG
#define Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_38_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_38_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_38_SPARSE_DATA \
	Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_38_RPC_WRAPPERS \
	Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_38_INCLASS \
	Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_38_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_38_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_38_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_38_SPARSE_DATA \
	Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_38_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_38_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h_38_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> REMOTESESSION_API UClass* StaticClass<class URemoteSessionMediaCapture>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_RemoteSession_Source_RemoteSession_Public_ImageProviders_RemoteSessionMediaOutput_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
