// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RemoteSessionEditor/Private/Widgets/SRemoteSessionStream.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSRemoteSessionStream() {}
// Cross Module References
	REMOTESESSIONEDITOR_API UClass* Z_Construct_UClass_URemoteSessionStreamSettings_NoRegister();
	REMOTESESSIONEDITOR_API UClass* Z_Construct_UClass_URemoteSessionStreamSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_RemoteSessionEditor();
	REMOTESESSIONEDITOR_API UClass* Z_Construct_UClass_URemoteSessionStreamWidgetUserData_NoRegister();
	REMOTESESSIONEDITOR_API UClass* Z_Construct_UClass_URemoteSessionStreamWidgetUserData();
	ENGINE_API UClass* Z_Construct_UClass_UAssetUserData();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	UMG_API UClass* Z_Construct_UClass_UUserWidget_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	ENGINE_API UClass* Z_Construct_UClass_UTextureRenderTarget2D_NoRegister();
// End Cross Module References
	void URemoteSessionStreamSettings::StaticRegisterNativesURemoteSessionStreamSettings()
	{
	}
	UClass* Z_Construct_UClass_URemoteSessionStreamSettings_NoRegister()
	{
		return URemoteSessionStreamSettings::StaticClass();
	}
	struct Z_Construct_UClass_URemoteSessionStreamSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsVerticalSplitterOrientation_MetaData[];
#endif
		static void NewProp_bIsVerticalSplitterOrientation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsVerticalSplitterOrientation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowCheckered_MetaData[];
#endif
		static void NewProp_bShowCheckered_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowCheckered;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bScaleToFit_MetaData[];
#endif
		static void NewProp_bScaleToFit_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bScaleToFit;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URemoteSessionStreamSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteSessionEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteSessionStreamSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Settings for the remote session stream tab.\n */" },
		{ "IncludePath", "Widgets/SRemoteSessionStream.h" },
		{ "ModuleRelativePath", "Private/Widgets/SRemoteSessionStream.h" },
		{ "ToolTip", "Settings for the remote session stream tab." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteSessionStreamSettings_Statics::NewProp_bIsVerticalSplitterOrientation_MetaData[] = {
		{ "ModuleRelativePath", "Private/Widgets/SRemoteSessionStream.h" },
	};
#endif
	void Z_Construct_UClass_URemoteSessionStreamSettings_Statics::NewProp_bIsVerticalSplitterOrientation_SetBit(void* Obj)
	{
		((URemoteSessionStreamSettings*)Obj)->bIsVerticalSplitterOrientation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URemoteSessionStreamSettings_Statics::NewProp_bIsVerticalSplitterOrientation = { "bIsVerticalSplitterOrientation", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URemoteSessionStreamSettings), &Z_Construct_UClass_URemoteSessionStreamSettings_Statics::NewProp_bIsVerticalSplitterOrientation_SetBit, METADATA_PARAMS(Z_Construct_UClass_URemoteSessionStreamSettings_Statics::NewProp_bIsVerticalSplitterOrientation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteSessionStreamSettings_Statics::NewProp_bIsVerticalSplitterOrientation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteSessionStreamSettings_Statics::NewProp_bShowCheckered_MetaData[] = {
		{ "ModuleRelativePath", "Private/Widgets/SRemoteSessionStream.h" },
	};
#endif
	void Z_Construct_UClass_URemoteSessionStreamSettings_Statics::NewProp_bShowCheckered_SetBit(void* Obj)
	{
		((URemoteSessionStreamSettings*)Obj)->bShowCheckered = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URemoteSessionStreamSettings_Statics::NewProp_bShowCheckered = { "bShowCheckered", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URemoteSessionStreamSettings), &Z_Construct_UClass_URemoteSessionStreamSettings_Statics::NewProp_bShowCheckered_SetBit, METADATA_PARAMS(Z_Construct_UClass_URemoteSessionStreamSettings_Statics::NewProp_bShowCheckered_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteSessionStreamSettings_Statics::NewProp_bShowCheckered_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteSessionStreamSettings_Statics::NewProp_bScaleToFit_MetaData[] = {
		{ "ModuleRelativePath", "Private/Widgets/SRemoteSessionStream.h" },
	};
#endif
	void Z_Construct_UClass_URemoteSessionStreamSettings_Statics::NewProp_bScaleToFit_SetBit(void* Obj)
	{
		((URemoteSessionStreamSettings*)Obj)->bScaleToFit = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URemoteSessionStreamSettings_Statics::NewProp_bScaleToFit = { "bScaleToFit", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URemoteSessionStreamSettings), &Z_Construct_UClass_URemoteSessionStreamSettings_Statics::NewProp_bScaleToFit_SetBit, METADATA_PARAMS(Z_Construct_UClass_URemoteSessionStreamSettings_Statics::NewProp_bScaleToFit_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteSessionStreamSettings_Statics::NewProp_bScaleToFit_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URemoteSessionStreamSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteSessionStreamSettings_Statics::NewProp_bIsVerticalSplitterOrientation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteSessionStreamSettings_Statics::NewProp_bShowCheckered,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteSessionStreamSettings_Statics::NewProp_bScaleToFit,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URemoteSessionStreamSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URemoteSessionStreamSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URemoteSessionStreamSettings_Statics::ClassParams = {
		&URemoteSessionStreamSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URemoteSessionStreamSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URemoteSessionStreamSettings_Statics::PropPointers),
		0,
		0x000800A4u,
		METADATA_PARAMS(Z_Construct_UClass_URemoteSessionStreamSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteSessionStreamSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URemoteSessionStreamSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URemoteSessionStreamSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URemoteSessionStreamSettings, 922036908);
	template<> REMOTESESSIONEDITOR_API UClass* StaticClass<URemoteSessionStreamSettings>()
	{
		return URemoteSessionStreamSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URemoteSessionStreamSettings(Z_Construct_UClass_URemoteSessionStreamSettings, &URemoteSessionStreamSettings::StaticClass, TEXT("/Script/RemoteSessionEditor"), TEXT("URemoteSessionStreamSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URemoteSessionStreamSettings);
	void URemoteSessionStreamWidgetUserData::StaticRegisterNativesURemoteSessionStreamWidgetUserData()
	{
	}
	UClass* Z_Construct_UClass_URemoteSessionStreamWidgetUserData_NoRegister()
	{
		return URemoteSessionStreamWidgetUserData::StaticClass();
	}
	struct Z_Construct_UClass_URemoteSessionStreamWidgetUserData_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WidgetClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_WidgetClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Size_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Size;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RenderTarget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RenderTarget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Port_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt16PropertyParams NewProp_Port;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URemoteSessionStreamWidgetUserData_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAssetUserData,
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteSessionEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteSessionStreamWidgetUserData_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * URemoteSessionStreamWidgetUserData\n */" },
		{ "IncludePath", "Widgets/SRemoteSessionStream.h" },
		{ "ModuleRelativePath", "Private/Widgets/SRemoteSessionStream.h" },
		{ "ToolTip", "URemoteSessionStreamWidgetUserData" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteSessionStreamWidgetUserData_Statics::NewProp_WidgetClass_MetaData[] = {
		{ "Category", "Remote Session" },
		{ "ModuleRelativePath", "Private/Widgets/SRemoteSessionStream.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_URemoteSessionStreamWidgetUserData_Statics::NewProp_WidgetClass = { "WidgetClass", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteSessionStreamWidgetUserData, WidgetClass), Z_Construct_UClass_UUserWidget_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_URemoteSessionStreamWidgetUserData_Statics::NewProp_WidgetClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteSessionStreamWidgetUserData_Statics::NewProp_WidgetClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteSessionStreamWidgetUserData_Statics::NewProp_Size_MetaData[] = {
		{ "Category", "Remote Session" },
		{ "ModuleRelativePath", "Private/Widgets/SRemoteSessionStream.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URemoteSessionStreamWidgetUserData_Statics::NewProp_Size = { "Size", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteSessionStreamWidgetUserData, Size), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UClass_URemoteSessionStreamWidgetUserData_Statics::NewProp_Size_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteSessionStreamWidgetUserData_Statics::NewProp_Size_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteSessionStreamWidgetUserData_Statics::NewProp_RenderTarget_MetaData[] = {
		{ "Category", "Remote Session" },
		{ "ModuleRelativePath", "Private/Widgets/SRemoteSessionStream.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_URemoteSessionStreamWidgetUserData_Statics::NewProp_RenderTarget = { "RenderTarget", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteSessionStreamWidgetUserData, RenderTarget), Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_URemoteSessionStreamWidgetUserData_Statics::NewProp_RenderTarget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteSessionStreamWidgetUserData_Statics::NewProp_RenderTarget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteSessionStreamWidgetUserData_Statics::NewProp_Port_MetaData[] = {
		{ "Category", "Remote Session" },
		{ "ModuleRelativePath", "Private/Widgets/SRemoteSessionStream.h" },
	};
#endif
	const UE4CodeGen_Private::FInt16PropertyParams Z_Construct_UClass_URemoteSessionStreamWidgetUserData_Statics::NewProp_Port = { "Port", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Int16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteSessionStreamWidgetUserData, Port), METADATA_PARAMS(Z_Construct_UClass_URemoteSessionStreamWidgetUserData_Statics::NewProp_Port_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteSessionStreamWidgetUserData_Statics::NewProp_Port_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URemoteSessionStreamWidgetUserData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteSessionStreamWidgetUserData_Statics::NewProp_WidgetClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteSessionStreamWidgetUserData_Statics::NewProp_Size,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteSessionStreamWidgetUserData_Statics::NewProp_RenderTarget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteSessionStreamWidgetUserData_Statics::NewProp_Port,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URemoteSessionStreamWidgetUserData_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URemoteSessionStreamWidgetUserData>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URemoteSessionStreamWidgetUserData_Statics::ClassParams = {
		&URemoteSessionStreamWidgetUserData::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URemoteSessionStreamWidgetUserData_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URemoteSessionStreamWidgetUserData_Statics::PropPointers),
		0,
		0x002810A0u,
		METADATA_PARAMS(Z_Construct_UClass_URemoteSessionStreamWidgetUserData_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteSessionStreamWidgetUserData_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URemoteSessionStreamWidgetUserData()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URemoteSessionStreamWidgetUserData_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URemoteSessionStreamWidgetUserData, 1180788634);
	template<> REMOTESESSIONEDITOR_API UClass* StaticClass<URemoteSessionStreamWidgetUserData>()
	{
		return URemoteSessionStreamWidgetUserData::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URemoteSessionStreamWidgetUserData(Z_Construct_UClass_URemoteSessionStreamWidgetUserData, &URemoteSessionStreamWidgetUserData::StaticClass, TEXT("/Script/RemoteSessionEditor"), TEXT("URemoteSessionStreamWidgetUserData"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URemoteSessionStreamWidgetUserData);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
