// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RemoteSession/Public/RemoteSessionTypes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRemoteSessionTypes() {}
// Cross Module References
	REMOTESESSION_API UEnum* Z_Construct_UEnum_RemoteSession_ERemoteSessionConnectionChange();
	UPackage* Z_Construct_UPackage__Script_RemoteSession();
	REMOTESESSION_API UEnum* Z_Construct_UEnum_RemoteSession_ERemoteSessionChannelChange();
	REMOTESESSION_API UEnum* Z_Construct_UEnum_RemoteSession_ERemoteSessionChannelMode();
	REMOTESESSION_API UScriptStruct* Z_Construct_UScriptStruct_FRemoteSessionChannelInfo();
	REMOTESESSION_API UClass* Z_Construct_UClass_URemoteSessionSettings_NoRegister();
	REMOTESESSION_API UClass* Z_Construct_UClass_URemoteSessionSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
// End Cross Module References
	static UEnum* ERemoteSessionConnectionChange_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_RemoteSession_ERemoteSessionConnectionChange, Z_Construct_UPackage__Script_RemoteSession(), TEXT("ERemoteSessionConnectionChange"));
		}
		return Singleton;
	}
	template<> REMOTESESSION_API UEnum* StaticEnum<ERemoteSessionConnectionChange>()
	{
		return ERemoteSessionConnectionChange_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ERemoteSessionConnectionChange(ERemoteSessionConnectionChange_StaticEnum, TEXT("/Script/RemoteSession"), TEXT("ERemoteSessionConnectionChange"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_RemoteSession_ERemoteSessionConnectionChange_Hash() { return 94521085U; }
	UEnum* Z_Construct_UEnum_RemoteSession_ERemoteSessionConnectionChange()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteSession();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ERemoteSessionConnectionChange"), 0, Get_Z_Construct_UEnum_RemoteSession_ERemoteSessionConnectionChange_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ERemoteSessionConnectionChange::Connected", (int64)ERemoteSessionConnectionChange::Connected },
				{ "ERemoteSessionConnectionChange::Disconnected", (int64)ERemoteSessionConnectionChange::Disconnected },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Connected.Name", "ERemoteSessionConnectionChange::Connected" },
				{ "Disconnected.Name", "ERemoteSessionConnectionChange::Disconnected" },
				{ "ModuleRelativePath", "Public/RemoteSessionTypes.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_RemoteSession,
				nullptr,
				"ERemoteSessionConnectionChange",
				"ERemoteSessionConnectionChange",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ERemoteSessionChannelChange_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_RemoteSession_ERemoteSessionChannelChange, Z_Construct_UPackage__Script_RemoteSession(), TEXT("ERemoteSessionChannelChange"));
		}
		return Singleton;
	}
	template<> REMOTESESSION_API UEnum* StaticEnum<ERemoteSessionChannelChange>()
	{
		return ERemoteSessionChannelChange_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ERemoteSessionChannelChange(ERemoteSessionChannelChange_StaticEnum, TEXT("/Script/RemoteSession"), TEXT("ERemoteSessionChannelChange"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_RemoteSession_ERemoteSessionChannelChange_Hash() { return 1465781286U; }
	UEnum* Z_Construct_UEnum_RemoteSession_ERemoteSessionChannelChange()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteSession();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ERemoteSessionChannelChange"), 0, Get_Z_Construct_UEnum_RemoteSession_ERemoteSessionChannelChange_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ERemoteSessionChannelChange::Created", (int64)ERemoteSessionChannelChange::Created },
				{ "ERemoteSessionChannelChange::Destroyed", (int64)ERemoteSessionChannelChange::Destroyed },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Created.Name", "ERemoteSessionChannelChange::Created" },
				{ "Destroyed.Name", "ERemoteSessionChannelChange::Destroyed" },
				{ "ModuleRelativePath", "Public/RemoteSessionTypes.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_RemoteSession,
				nullptr,
				"ERemoteSessionChannelChange",
				"ERemoteSessionChannelChange",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ERemoteSessionChannelMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_RemoteSession_ERemoteSessionChannelMode, Z_Construct_UPackage__Script_RemoteSession(), TEXT("ERemoteSessionChannelMode"));
		}
		return Singleton;
	}
	template<> REMOTESESSION_API UEnum* StaticEnum<ERemoteSessionChannelMode>()
	{
		return ERemoteSessionChannelMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ERemoteSessionChannelMode(ERemoteSessionChannelMode_StaticEnum, TEXT("/Script/RemoteSession"), TEXT("ERemoteSessionChannelMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_RemoteSession_ERemoteSessionChannelMode_Hash() { return 57117642U; }
	UEnum* Z_Construct_UEnum_RemoteSession_ERemoteSessionChannelMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteSession();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ERemoteSessionChannelMode"), 0, Get_Z_Construct_UEnum_RemoteSession_ERemoteSessionChannelMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ERemoteSessionChannelMode::Unknown", (int64)ERemoteSessionChannelMode::Unknown },
				{ "ERemoteSessionChannelMode::Read", (int64)ERemoteSessionChannelMode::Read },
				{ "ERemoteSessionChannelMode::Write", (int64)ERemoteSessionChannelMode::Write },
				{ "ERemoteSessionChannelMode::MaxValue", (int64)ERemoteSessionChannelMode::MaxValue },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "MaxValue.Name", "ERemoteSessionChannelMode::MaxValue" },
				{ "ModuleRelativePath", "Public/RemoteSessionTypes.h" },
				{ "Read.Name", "ERemoteSessionChannelMode::Read" },
				{ "Unknown.Name", "ERemoteSessionChannelMode::Unknown" },
				{ "Write.Name", "ERemoteSessionChannelMode::Write" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_RemoteSession,
				nullptr,
				"ERemoteSessionChannelMode",
				"ERemoteSessionChannelMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FRemoteSessionChannelInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTESESSION_API uint32 Get_Z_Construct_UScriptStruct_FRemoteSessionChannelInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRemoteSessionChannelInfo, Z_Construct_UPackage__Script_RemoteSession(), TEXT("RemoteSessionChannelInfo"), sizeof(FRemoteSessionChannelInfo), Get_Z_Construct_UScriptStruct_FRemoteSessionChannelInfo_Hash());
	}
	return Singleton;
}
template<> REMOTESESSION_API UScriptStruct* StaticStruct<FRemoteSessionChannelInfo>()
{
	return FRemoteSessionChannelInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRemoteSessionChannelInfo(FRemoteSessionChannelInfo::StaticStruct, TEXT("/Script/RemoteSession"), TEXT("RemoteSessionChannelInfo"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteSession_StaticRegisterNativesFRemoteSessionChannelInfo
{
	FScriptStruct_RemoteSession_StaticRegisterNativesFRemoteSessionChannelInfo()
	{
		UScriptStruct::DeferCppStructOps<FRemoteSessionChannelInfo>(FName(TEXT("RemoteSessionChannelInfo")));
	}
} ScriptStruct_RemoteSession_StaticRegisterNativesFRemoteSessionChannelInfo;
	struct Z_Construct_UScriptStruct_FRemoteSessionChannelInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Type_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Type;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Mode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Mode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Mode;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteSessionChannelInfo_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/RemoteSessionTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRemoteSessionChannelInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRemoteSessionChannelInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteSessionChannelInfo_Statics::NewProp_Type_MetaData[] = {
		{ "ModuleRelativePath", "Public/RemoteSessionTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRemoteSessionChannelInfo_Statics::NewProp_Type = { "Type", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteSessionChannelInfo, Type), METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteSessionChannelInfo_Statics::NewProp_Type_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteSessionChannelInfo_Statics::NewProp_Type_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRemoteSessionChannelInfo_Statics::NewProp_Mode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteSessionChannelInfo_Statics::NewProp_Mode_MetaData[] = {
		{ "ModuleRelativePath", "Public/RemoteSessionTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FRemoteSessionChannelInfo_Statics::NewProp_Mode = { "Mode", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteSessionChannelInfo, Mode), Z_Construct_UEnum_RemoteSession_ERemoteSessionChannelMode, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteSessionChannelInfo_Statics::NewProp_Mode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteSessionChannelInfo_Statics::NewProp_Mode_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRemoteSessionChannelInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteSessionChannelInfo_Statics::NewProp_Type,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteSessionChannelInfo_Statics::NewProp_Mode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteSessionChannelInfo_Statics::NewProp_Mode,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRemoteSessionChannelInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteSession,
		nullptr,
		&NewStructOps,
		"RemoteSessionChannelInfo",
		sizeof(FRemoteSessionChannelInfo),
		alignof(FRemoteSessionChannelInfo),
		Z_Construct_UScriptStruct_FRemoteSessionChannelInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteSessionChannelInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteSessionChannelInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteSessionChannelInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRemoteSessionChannelInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRemoteSessionChannelInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteSession();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RemoteSessionChannelInfo"), sizeof(FRemoteSessionChannelInfo), Get_Z_Construct_UScriptStruct_FRemoteSessionChannelInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRemoteSessionChannelInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRemoteSessionChannelInfo_Hash() { return 2711425614U; }
	void URemoteSessionSettings::StaticRegisterNativesURemoteSessionSettings()
	{
	}
	UClass* Z_Construct_UClass_URemoteSessionSettings_NoRegister()
	{
		return URemoteSessionSettings::StaticClass();
	}
	struct Z_Construct_UClass_URemoteSessionSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HostPort_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_HostPort;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConnectionTimeout_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ConnectionTimeout;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConnectionTimeoutWhenDebugging_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ConnectionTimeoutWhenDebugging;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PingTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_PingTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAllowInShipping_MetaData[];
#endif
		static void NewProp_bAllowInShipping_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAllowInShipping;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoHostWithPIE_MetaData[];
#endif
		static void NewProp_bAutoHostWithPIE_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoHostWithPIE;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoHostWithGame_MetaData[];
#endif
		static void NewProp_bAutoHostWithGame_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoHostWithGame;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImageQuality_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ImageQuality;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FrameRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_FrameRate;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_WhitelistedChannels_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WhitelistedChannels_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_WhitelistedChannels;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_BlacklistedChannels_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BlacklistedChannels_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_BlacklistedChannels;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URemoteSessionSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteSession,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteSessionSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Settings for the Asset Management framework, which can be used to discover, load, and audit game-specific asset types */" },
		{ "IncludePath", "RemoteSessionTypes.h" },
		{ "ModuleRelativePath", "Public/RemoteSessionTypes.h" },
		{ "ToolTip", "Settings for the Asset Management framework, which can be used to discover, load, and audit game-specific asset types" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_HostPort_MetaData[] = {
		{ "Comment", "/* Port that the host will listen on */" },
		{ "ModuleRelativePath", "Public/RemoteSessionTypes.h" },
		{ "ToolTip", "Port that the host will listen on" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_HostPort = { "HostPort", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteSessionSettings, HostPort), METADATA_PARAMS(Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_HostPort_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_HostPort_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_ConnectionTimeout_MetaData[] = {
		{ "Comment", "/* Time until a connection will timeout  */" },
		{ "ModuleRelativePath", "Public/RemoteSessionTypes.h" },
		{ "ToolTip", "Time until a connection will timeout" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_ConnectionTimeout = { "ConnectionTimeout", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteSessionSettings, ConnectionTimeout), METADATA_PARAMS(Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_ConnectionTimeout_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_ConnectionTimeout_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_ConnectionTimeoutWhenDebugging_MetaData[] = {
		{ "Comment", "/* Time until a connection will timeout when a debugger is attached  */" },
		{ "ModuleRelativePath", "Public/RemoteSessionTypes.h" },
		{ "ToolTip", "Time until a connection will timeout when a debugger is attached" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_ConnectionTimeoutWhenDebugging = { "ConnectionTimeoutWhenDebugging", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteSessionSettings, ConnectionTimeoutWhenDebugging), METADATA_PARAMS(Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_ConnectionTimeoutWhenDebugging_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_ConnectionTimeoutWhenDebugging_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_PingTime_MetaData[] = {
		{ "Comment", "/* Time between pings  */" },
		{ "ModuleRelativePath", "Public/RemoteSessionTypes.h" },
		{ "ToolTip", "Time between pings" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_PingTime = { "PingTime", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteSessionSettings, PingTime), METADATA_PARAMS(Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_PingTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_PingTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_bAllowInShipping_MetaData[] = {
		{ "Comment", "/* Whether RemoteSession is functional in a shipping build */" },
		{ "ModuleRelativePath", "Public/RemoteSessionTypes.h" },
		{ "ToolTip", "Whether RemoteSession is functional in a shipping build" },
	};
#endif
	void Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_bAllowInShipping_SetBit(void* Obj)
	{
		((URemoteSessionSettings*)Obj)->bAllowInShipping = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_bAllowInShipping = { "bAllowInShipping", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URemoteSessionSettings), &Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_bAllowInShipping_SetBit, METADATA_PARAMS(Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_bAllowInShipping_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_bAllowInShipping_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_bAutoHostWithPIE_MetaData[] = {
		{ "Comment", "/* Does PIE automatically start hosting a session? */" },
		{ "ModuleRelativePath", "Public/RemoteSessionTypes.h" },
		{ "ToolTip", "Does PIE automatically start hosting a session?" },
	};
#endif
	void Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_bAutoHostWithPIE_SetBit(void* Obj)
	{
		((URemoteSessionSettings*)Obj)->bAutoHostWithPIE = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_bAutoHostWithPIE = { "bAutoHostWithPIE", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URemoteSessionSettings), &Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_bAutoHostWithPIE_SetBit, METADATA_PARAMS(Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_bAutoHostWithPIE_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_bAutoHostWithPIE_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_bAutoHostWithGame_MetaData[] = {
		{ "Comment", "/* Does launching a game automatically host a session? */" },
		{ "ModuleRelativePath", "Public/RemoteSessionTypes.h" },
		{ "ToolTip", "Does launching a game automatically host a session?" },
	};
#endif
	void Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_bAutoHostWithGame_SetBit(void* Obj)
	{
		((URemoteSessionSettings*)Obj)->bAutoHostWithGame = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_bAutoHostWithGame = { "bAutoHostWithGame", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URemoteSessionSettings), &Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_bAutoHostWithGame_SetBit, METADATA_PARAMS(Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_bAutoHostWithGame_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_bAutoHostWithGame_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_ImageQuality_MetaData[] = {
		{ "Comment", "/* Image quality (1-100) */" },
		{ "ModuleRelativePath", "Public/RemoteSessionTypes.h" },
		{ "ToolTip", "Image quality (1-100)" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_ImageQuality = { "ImageQuality", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteSessionSettings, ImageQuality), METADATA_PARAMS(Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_ImageQuality_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_ImageQuality_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_FrameRate_MetaData[] = {
		{ "Comment", "/* Framerate of images from the host (will be min of this value and the actual framerate of the game */" },
		{ "ModuleRelativePath", "Public/RemoteSessionTypes.h" },
		{ "ToolTip", "Framerate of images from the host (will be min of this value and the actual framerate of the game" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_FrameRate = { "FrameRate", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteSessionSettings, FrameRate), METADATA_PARAMS(Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_FrameRate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_FrameRate_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_WhitelistedChannels_Inner = { "WhitelistedChannels", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_WhitelistedChannels_MetaData[] = {
		{ "Comment", "/* Restrict remote session to these channels. If empty all registered channels are available */" },
		{ "ModuleRelativePath", "Public/RemoteSessionTypes.h" },
		{ "ToolTip", "Restrict remote session to these channels. If empty all registered channels are available" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_WhitelistedChannels = { "WhitelistedChannels", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteSessionSettings, WhitelistedChannels), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_WhitelistedChannels_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_WhitelistedChannels_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_BlacklistedChannels_Inner = { "BlacklistedChannels", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_BlacklistedChannels_MetaData[] = {
		{ "Comment", "/* Don't allow these channels to be used */" },
		{ "ModuleRelativePath", "Public/RemoteSessionTypes.h" },
		{ "ToolTip", "Don't allow these channels to be used" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_BlacklistedChannels = { "BlacklistedChannels", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteSessionSettings, BlacklistedChannels), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_BlacklistedChannels_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_BlacklistedChannels_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URemoteSessionSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_HostPort,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_ConnectionTimeout,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_ConnectionTimeoutWhenDebugging,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_PingTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_bAllowInShipping,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_bAutoHostWithPIE,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_bAutoHostWithGame,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_ImageQuality,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_FrameRate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_WhitelistedChannels_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_WhitelistedChannels,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_BlacklistedChannels_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteSessionSettings_Statics::NewProp_BlacklistedChannels,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URemoteSessionSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URemoteSessionSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URemoteSessionSettings_Statics::ClassParams = {
		&URemoteSessionSettings::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URemoteSessionSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URemoteSessionSettings_Statics::PropPointers),
		0,
		0x001000A4u,
		METADATA_PARAMS(Z_Construct_UClass_URemoteSessionSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteSessionSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URemoteSessionSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URemoteSessionSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URemoteSessionSettings, 2910118770);
	template<> REMOTESESSION_API UClass* StaticClass<URemoteSessionSettings>()
	{
		return URemoteSessionSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URemoteSessionSettings(Z_Construct_UClass_URemoteSessionSettings, &URemoteSessionSettings::StaticClass, TEXT("/Script/RemoteSession"), TEXT("URemoteSessionSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URemoteSessionSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
