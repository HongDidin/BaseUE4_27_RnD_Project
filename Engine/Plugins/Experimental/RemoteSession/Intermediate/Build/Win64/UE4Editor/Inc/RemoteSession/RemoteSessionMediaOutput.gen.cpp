// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RemoteSession/Public/ImageProviders/RemoteSessionMediaOutput.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRemoteSessionMediaOutput() {}
// Cross Module References
	REMOTESESSION_API UClass* Z_Construct_UClass_URemoteSessionMediaOutput_NoRegister();
	REMOTESESSION_API UClass* Z_Construct_UClass_URemoteSessionMediaOutput();
	MEDIAIOCORE_API UClass* Z_Construct_UClass_UMediaOutput();
	UPackage* Z_Construct_UPackage__Script_RemoteSession();
	REMOTESESSION_API UClass* Z_Construct_UClass_URemoteSessionMediaCapture_NoRegister();
	REMOTESESSION_API UClass* Z_Construct_UClass_URemoteSessionMediaCapture();
	MEDIAIOCORE_API UClass* Z_Construct_UClass_UMediaCapture();
// End Cross Module References
	void URemoteSessionMediaOutput::StaticRegisterNativesURemoteSessionMediaOutput()
	{
	}
	UClass* Z_Construct_UClass_URemoteSessionMediaOutput_NoRegister()
	{
		return URemoteSessionMediaOutput::StaticClass();
	}
	struct Z_Construct_UClass_URemoteSessionMediaOutput_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URemoteSessionMediaOutput_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMediaOutput,
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteSession,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteSessionMediaOutput_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "ImageProviders/RemoteSessionMediaOutput.h" },
		{ "ModuleRelativePath", "Public/ImageProviders/RemoteSessionMediaOutput.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_URemoteSessionMediaOutput_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URemoteSessionMediaOutput>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URemoteSessionMediaOutput_Statics::ClassParams = {
		&URemoteSessionMediaOutput::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_URemoteSessionMediaOutput_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteSessionMediaOutput_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URemoteSessionMediaOutput()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URemoteSessionMediaOutput_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URemoteSessionMediaOutput, 1779887585);
	template<> REMOTESESSION_API UClass* StaticClass<URemoteSessionMediaOutput>()
	{
		return URemoteSessionMediaOutput::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URemoteSessionMediaOutput(Z_Construct_UClass_URemoteSessionMediaOutput, &URemoteSessionMediaOutput::StaticClass, TEXT("/Script/RemoteSession"), TEXT("URemoteSessionMediaOutput"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URemoteSessionMediaOutput);
	void URemoteSessionMediaCapture::StaticRegisterNativesURemoteSessionMediaCapture()
	{
	}
	UClass* Z_Construct_UClass_URemoteSessionMediaCapture_NoRegister()
	{
		return URemoteSessionMediaCapture::StaticClass();
	}
	struct Z_Construct_UClass_URemoteSessionMediaCapture_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URemoteSessionMediaCapture_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMediaCapture,
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteSession,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteSessionMediaCapture_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "ImageProviders/RemoteSessionMediaOutput.h" },
		{ "ModuleRelativePath", "Public/ImageProviders/RemoteSessionMediaOutput.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_URemoteSessionMediaCapture_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URemoteSessionMediaCapture>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URemoteSessionMediaCapture_Statics::ClassParams = {
		&URemoteSessionMediaCapture::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009010A0u,
		METADATA_PARAMS(Z_Construct_UClass_URemoteSessionMediaCapture_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteSessionMediaCapture_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URemoteSessionMediaCapture()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URemoteSessionMediaCapture_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URemoteSessionMediaCapture, 4140698125);
	template<> REMOTESESSION_API UClass* StaticClass<URemoteSessionMediaCapture>()
	{
		return URemoteSessionMediaCapture::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URemoteSessionMediaCapture(Z_Construct_UClass_URemoteSessionMediaCapture, &URemoteSessionMediaCapture::StaticClass, TEXT("/Script/RemoteSession"), TEXT("URemoteSessionMediaCapture"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URemoteSessionMediaCapture);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
