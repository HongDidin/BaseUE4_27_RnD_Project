// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ChaosSolverEditor/Public/Chaos/ChaosSolverFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeChaosSolverFactory() {}
// Cross Module References
	CHAOSSOLVEREDITOR_API UClass* Z_Construct_UClass_UChaosSolverFactory_NoRegister();
	CHAOSSOLVEREDITOR_API UClass* Z_Construct_UClass_UChaosSolverFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_ChaosSolverEditor();
// End Cross Module References
	void UChaosSolverFactory::StaticRegisterNativesUChaosSolverFactory()
	{
	}
	UClass* Z_Construct_UClass_UChaosSolverFactory_NoRegister()
	{
		return UChaosSolverFactory::StaticClass();
	}
	struct Z_Construct_UClass_UChaosSolverFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UChaosSolverFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_ChaosSolverEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UChaosSolverFactory_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n* Factory for Simple Cube\n*/" },
		{ "IncludePath", "Chaos/ChaosSolverFactory.h" },
		{ "ModuleRelativePath", "Public/Chaos/ChaosSolverFactory.h" },
		{ "ToolTip", "Factory for Simple Cube" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UChaosSolverFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UChaosSolverFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UChaosSolverFactory_Statics::ClassParams = {
		&UChaosSolverFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UChaosSolverFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UChaosSolverFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UChaosSolverFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UChaosSolverFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UChaosSolverFactory, 1387875977);
	template<> CHAOSSOLVEREDITOR_API UClass* StaticClass<UChaosSolverFactory>()
	{
		return UChaosSolverFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UChaosSolverFactory(Z_Construct_UClass_UChaosSolverFactory, &UChaosSolverFactory::StaticClass, TEXT("/Script/ChaosSolverEditor"), TEXT("UChaosSolverFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UChaosSolverFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
