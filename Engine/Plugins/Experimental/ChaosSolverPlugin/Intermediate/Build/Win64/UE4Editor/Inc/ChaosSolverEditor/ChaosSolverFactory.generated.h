// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CHAOSSOLVEREDITOR_ChaosSolverFactory_generated_h
#error "ChaosSolverFactory.generated.h already included, missing '#pragma once' in ChaosSolverFactory.h"
#endif
#define CHAOSSOLVEREDITOR_ChaosSolverFactory_generated_h

#define Engine_Plugins_Experimental_ChaosSolverPlugin_Source_ChaosSolverEditor_Public_Chaos_ChaosSolverFactory_h_22_SPARSE_DATA
#define Engine_Plugins_Experimental_ChaosSolverPlugin_Source_ChaosSolverEditor_Public_Chaos_ChaosSolverFactory_h_22_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ChaosSolverPlugin_Source_ChaosSolverEditor_Public_Chaos_ChaosSolverFactory_h_22_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ChaosSolverPlugin_Source_ChaosSolverEditor_Public_Chaos_ChaosSolverFactory_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUChaosSolverFactory(); \
	friend struct Z_Construct_UClass_UChaosSolverFactory_Statics; \
public: \
	DECLARE_CLASS(UChaosSolverFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ChaosSolverEditor"), NO_API) \
	DECLARE_SERIALIZER(UChaosSolverFactory)


#define Engine_Plugins_Experimental_ChaosSolverPlugin_Source_ChaosSolverEditor_Public_Chaos_ChaosSolverFactory_h_22_INCLASS \
private: \
	static void StaticRegisterNativesUChaosSolverFactory(); \
	friend struct Z_Construct_UClass_UChaosSolverFactory_Statics; \
public: \
	DECLARE_CLASS(UChaosSolverFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ChaosSolverEditor"), NO_API) \
	DECLARE_SERIALIZER(UChaosSolverFactory)


#define Engine_Plugins_Experimental_ChaosSolverPlugin_Source_ChaosSolverEditor_Public_Chaos_ChaosSolverFactory_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UChaosSolverFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UChaosSolverFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UChaosSolverFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UChaosSolverFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UChaosSolverFactory(UChaosSolverFactory&&); \
	NO_API UChaosSolverFactory(const UChaosSolverFactory&); \
public:


#define Engine_Plugins_Experimental_ChaosSolverPlugin_Source_ChaosSolverEditor_Public_Chaos_ChaosSolverFactory_h_22_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UChaosSolverFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UChaosSolverFactory(UChaosSolverFactory&&); \
	NO_API UChaosSolverFactory(const UChaosSolverFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UChaosSolverFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UChaosSolverFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UChaosSolverFactory)


#define Engine_Plugins_Experimental_ChaosSolverPlugin_Source_ChaosSolverEditor_Public_Chaos_ChaosSolverFactory_h_22_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ChaosSolverPlugin_Source_ChaosSolverEditor_Public_Chaos_ChaosSolverFactory_h_19_PROLOG
#define Engine_Plugins_Experimental_ChaosSolverPlugin_Source_ChaosSolverEditor_Public_Chaos_ChaosSolverFactory_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosSolverPlugin_Source_ChaosSolverEditor_Public_Chaos_ChaosSolverFactory_h_22_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosSolverPlugin_Source_ChaosSolverEditor_Public_Chaos_ChaosSolverFactory_h_22_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosSolverPlugin_Source_ChaosSolverEditor_Public_Chaos_ChaosSolverFactory_h_22_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ChaosSolverPlugin_Source_ChaosSolverEditor_Public_Chaos_ChaosSolverFactory_h_22_INCLASS \
	Engine_Plugins_Experimental_ChaosSolverPlugin_Source_ChaosSolverEditor_Public_Chaos_ChaosSolverFactory_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ChaosSolverPlugin_Source_ChaosSolverEditor_Public_Chaos_ChaosSolverFactory_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosSolverPlugin_Source_ChaosSolverEditor_Public_Chaos_ChaosSolverFactory_h_22_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosSolverPlugin_Source_ChaosSolverEditor_Public_Chaos_ChaosSolverFactory_h_22_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosSolverPlugin_Source_ChaosSolverEditor_Public_Chaos_ChaosSolverFactory_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosSolverPlugin_Source_ChaosSolverEditor_Public_Chaos_ChaosSolverFactory_h_22_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosSolverPlugin_Source_ChaosSolverEditor_Public_Chaos_ChaosSolverFactory_h_22_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ChaosSolverFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CHAOSSOLVEREDITOR_API UClass* StaticClass<class UChaosSolverFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ChaosSolverPlugin_Source_ChaosSolverEditor_Public_Chaos_ChaosSolverFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
