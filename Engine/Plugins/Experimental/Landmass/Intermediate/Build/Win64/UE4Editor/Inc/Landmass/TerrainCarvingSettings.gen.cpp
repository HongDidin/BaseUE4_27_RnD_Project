// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Runtime/Public/TerrainCarvingSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTerrainCarvingSettings() {}
// Cross Module References
	LANDMASS_API UEnum* Z_Construct_UEnum_Landmass_EBrushBlendType();
	UPackage* Z_Construct_UPackage__Script_Landmass();
	LANDMASS_API UScriptStruct* Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings();
	LANDMASS_API UScriptStruct* Z_Construct_UScriptStruct_FLandmassFalloffSettings();
	LANDMASS_API UScriptStruct* Z_Construct_UScriptStruct_FLandmassBrushEffectsList();
// End Cross Module References
	static UEnum* EBrushBlendType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_Landmass_EBrushBlendType, Z_Construct_UPackage__Script_Landmass(), TEXT("EBrushBlendType"));
		}
		return Singleton;
	}
	template<> LANDMASS_API UEnum* StaticEnum<EBrushBlendType>()
	{
		return EBrushBlendType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EBrushBlendType(EBrushBlendType_StaticEnum, TEXT("/Script/Landmass"), TEXT("EBrushBlendType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_Landmass_EBrushBlendType_Hash() { return 893595434U; }
	UEnum* Z_Construct_UEnum_Landmass_EBrushBlendType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_Landmass();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EBrushBlendType"), 0, Get_Z_Construct_UEnum_Landmass_EBrushBlendType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EBrushBlendType::AlphaBlend", (int64)EBrushBlendType::AlphaBlend },
				{ "EBrushBlendType::Min", (int64)EBrushBlendType::Min },
				{ "EBrushBlendType::Max", (int64)EBrushBlendType::Max },
				{ "EBrushBlendType::Additive", (int64)EBrushBlendType::Additive },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Additive.Comment", "/** Performs an additive blend, using a flat Z=0 terrain as the input. Useful when you want to preserve underlying detail or ramps. */" },
				{ "Additive.Name", "EBrushBlendType::Additive" },
				{ "Additive.ToolTip", "Performs an additive blend, using a flat Z=0 terrain as the input. Useful when you want to preserve underlying detail or ramps." },
				{ "AlphaBlend.Comment", "/** Alpha Blend will affect the heightmap both upwards and downwards. */" },
				{ "AlphaBlend.Name", "EBrushBlendType::AlphaBlend" },
				{ "AlphaBlend.ToolTip", "Alpha Blend will affect the heightmap both upwards and downwards." },
				{ "BlueprintType", "true" },
				{ "Comment", "/** The blend mode changes how the brush material is applied to the terrain. */" },
				{ "Max.Comment", "/** Limits the brush to only raising the terrain. */" },
				{ "Max.Name", "EBrushBlendType::Max" },
				{ "Max.ToolTip", "Limits the brush to only raising the terrain." },
				{ "Min.Comment", "/** Limits the brush to only lowering the terrain. */" },
				{ "Min.Name", "EBrushBlendType::Min" },
				{ "Min.ToolTip", "Limits the brush to only lowering the terrain." },
				{ "ModuleRelativePath", "Public/TerrainCarvingSettings.h" },
				{ "ToolTip", "The blend mode changes how the brush material is applied to the terrain." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_Landmass,
				nullptr,
				"EBrushBlendType",
				"EBrushBlendType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FLandmassTerrainCarvingSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LANDMASS_API uint32 Get_Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings, Z_Construct_UPackage__Script_Landmass(), TEXT("LandmassTerrainCarvingSettings"), sizeof(FLandmassTerrainCarvingSettings), Get_Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Hash());
	}
	return Singleton;
}
template<> LANDMASS_API UScriptStruct* StaticStruct<FLandmassTerrainCarvingSettings>()
{
	return FLandmassTerrainCarvingSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FLandmassTerrainCarvingSettings(FLandmassTerrainCarvingSettings::StaticStruct, TEXT("/Script/Landmass"), TEXT("LandmassTerrainCarvingSettings"), false, nullptr, nullptr);
static struct FScriptStruct_Landmass_StaticRegisterNativesFLandmassTerrainCarvingSettings
{
	FScriptStruct_Landmass_StaticRegisterNativesFLandmassTerrainCarvingSettings()
	{
		UScriptStruct::DeferCppStructOps<FLandmassTerrainCarvingSettings>(FName(TEXT("LandmassTerrainCarvingSettings")));
	}
} ScriptStruct_Landmass_StaticRegisterNativesFLandmassTerrainCarvingSettings;
	struct Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_BlendMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BlendMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_BlendMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bInvertShape_MetaData[];
#endif
		static void NewProp_bInvertShape_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInvertShape;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FalloffSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FalloffSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Effects_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Effects;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Priority_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Priority;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/TerrainCarvingSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FLandmassTerrainCarvingSettings>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Statics::NewProp_BlendMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Statics::NewProp_BlendMode_MetaData[] = {
		{ "Category", "TerrainCarvingSettings" },
		{ "ModuleRelativePath", "Public/TerrainCarvingSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Statics::NewProp_BlendMode = { "BlendMode", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLandmassTerrainCarvingSettings, BlendMode), Z_Construct_UEnum_Landmass_EBrushBlendType, METADATA_PARAMS(Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Statics::NewProp_BlendMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Statics::NewProp_BlendMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Statics::NewProp_bInvertShape_MetaData[] = {
		{ "Category", "TerrainCarvingSettings" },
		{ "ModuleRelativePath", "Public/TerrainCarvingSettings.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Statics::NewProp_bInvertShape_SetBit(void* Obj)
	{
		((FLandmassTerrainCarvingSettings*)Obj)->bInvertShape = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Statics::NewProp_bInvertShape = { "bInvertShape", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FLandmassTerrainCarvingSettings), &Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Statics::NewProp_bInvertShape_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Statics::NewProp_bInvertShape_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Statics::NewProp_bInvertShape_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Statics::NewProp_FalloffSettings_MetaData[] = {
		{ "Category", "TerrainCarvingSettings" },
		{ "ModuleRelativePath", "Public/TerrainCarvingSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Statics::NewProp_FalloffSettings = { "FalloffSettings", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLandmassTerrainCarvingSettings, FalloffSettings), Z_Construct_UScriptStruct_FLandmassFalloffSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Statics::NewProp_FalloffSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Statics::NewProp_FalloffSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Statics::NewProp_Effects_MetaData[] = {
		{ "Category", "TerrainCarvingSettings" },
		{ "ModuleRelativePath", "Public/TerrainCarvingSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Statics::NewProp_Effects = { "Effects", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLandmassTerrainCarvingSettings, Effects), Z_Construct_UScriptStruct_FLandmassBrushEffectsList, METADATA_PARAMS(Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Statics::NewProp_Effects_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Statics::NewProp_Effects_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Statics::NewProp_Priority_MetaData[] = {
		{ "Category", "TerrainCarvingSettings" },
		{ "ModuleRelativePath", "Public/TerrainCarvingSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Statics::NewProp_Priority = { "Priority", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLandmassTerrainCarvingSettings, Priority), METADATA_PARAMS(Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Statics::NewProp_Priority_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Statics::NewProp_Priority_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Statics::NewProp_BlendMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Statics::NewProp_BlendMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Statics::NewProp_bInvertShape,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Statics::NewProp_FalloffSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Statics::NewProp_Effects,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Statics::NewProp_Priority,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Landmass,
		nullptr,
		&NewStructOps,
		"LandmassTerrainCarvingSettings",
		sizeof(FLandmassTerrainCarvingSettings),
		alignof(FLandmassTerrainCarvingSettings),
		Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Landmass();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("LandmassTerrainCarvingSettings"), sizeof(FLandmassTerrainCarvingSettings), Get_Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings_Hash() { return 3809579588U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
