// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Runtime/Public/BrushEffectsList.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBrushEffectsList() {}
// Cross Module References
	LANDMASS_API UScriptStruct* Z_Construct_UScriptStruct_FLandmassBrushEffectsList();
	UPackage* Z_Construct_UPackage__Script_Landmass();
	LANDMASS_API UScriptStruct* Z_Construct_UScriptStruct_FBrushEffectBlurring();
	LANDMASS_API UScriptStruct* Z_Construct_UScriptStruct_FBrushEffectCurlNoise();
	LANDMASS_API UScriptStruct* Z_Construct_UScriptStruct_FBrushEffectDisplacement();
	LANDMASS_API UScriptStruct* Z_Construct_UScriptStruct_FBrushEffectSmoothBlending();
	LANDMASS_API UScriptStruct* Z_Construct_UScriptStruct_FBrushEffectTerracing();
	ENGINE_API UClass* Z_Construct_UClass_UTexture2D_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
	LANDMASS_API UScriptStruct* Z_Construct_UScriptStruct_FBrushEffectCurves();
	ENGINE_API UClass* Z_Construct_UClass_UCurveFloat_NoRegister();
// End Cross Module References
class UScriptStruct* FLandmassBrushEffectsList::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LANDMASS_API uint32 Get_Z_Construct_UScriptStruct_FLandmassBrushEffectsList_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FLandmassBrushEffectsList, Z_Construct_UPackage__Script_Landmass(), TEXT("LandmassBrushEffectsList"), sizeof(FLandmassBrushEffectsList), Get_Z_Construct_UScriptStruct_FLandmassBrushEffectsList_Hash());
	}
	return Singleton;
}
template<> LANDMASS_API UScriptStruct* StaticStruct<FLandmassBrushEffectsList>()
{
	return FLandmassBrushEffectsList::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FLandmassBrushEffectsList(FLandmassBrushEffectsList::StaticStruct, TEXT("/Script/Landmass"), TEXT("LandmassBrushEffectsList"), false, nullptr, nullptr);
static struct FScriptStruct_Landmass_StaticRegisterNativesFLandmassBrushEffectsList
{
	FScriptStruct_Landmass_StaticRegisterNativesFLandmassBrushEffectsList()
	{
		UScriptStruct::DeferCppStructOps<FLandmassBrushEffectsList>(FName(TEXT("LandmassBrushEffectsList")));
	}
} ScriptStruct_Landmass_StaticRegisterNativesFLandmassBrushEffectsList;
	struct Z_Construct_UScriptStruct_FLandmassBrushEffectsList_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Blurring_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Blurring;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurlNoise_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CurlNoise;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Displacement_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Displacement;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SmoothBlending_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SmoothBlending;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Terracing_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Terracing;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLandmassBrushEffectsList_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/BrushEffectsList.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FLandmassBrushEffectsList_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FLandmassBrushEffectsList>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLandmassBrushEffectsList_Statics::NewProp_Blurring_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/BrushEffectsList.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FLandmassBrushEffectsList_Statics::NewProp_Blurring = { "Blurring", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLandmassBrushEffectsList, Blurring), Z_Construct_UScriptStruct_FBrushEffectBlurring, METADATA_PARAMS(Z_Construct_UScriptStruct_FLandmassBrushEffectsList_Statics::NewProp_Blurring_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLandmassBrushEffectsList_Statics::NewProp_Blurring_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLandmassBrushEffectsList_Statics::NewProp_CurlNoise_MetaData[] = {
		{ "Category", "FalloffSettings" },
		{ "ModuleRelativePath", "Public/BrushEffectsList.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FLandmassBrushEffectsList_Statics::NewProp_CurlNoise = { "CurlNoise", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLandmassBrushEffectsList, CurlNoise), Z_Construct_UScriptStruct_FBrushEffectCurlNoise, METADATA_PARAMS(Z_Construct_UScriptStruct_FLandmassBrushEffectsList_Statics::NewProp_CurlNoise_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLandmassBrushEffectsList_Statics::NewProp_CurlNoise_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLandmassBrushEffectsList_Statics::NewProp_Displacement_MetaData[] = {
		{ "Category", "FalloffSettings" },
		{ "ModuleRelativePath", "Public/BrushEffectsList.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FLandmassBrushEffectsList_Statics::NewProp_Displacement = { "Displacement", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLandmassBrushEffectsList, Displacement), Z_Construct_UScriptStruct_FBrushEffectDisplacement, METADATA_PARAMS(Z_Construct_UScriptStruct_FLandmassBrushEffectsList_Statics::NewProp_Displacement_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLandmassBrushEffectsList_Statics::NewProp_Displacement_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLandmassBrushEffectsList_Statics::NewProp_SmoothBlending_MetaData[] = {
		{ "Category", "FalloffSettings" },
		{ "ModuleRelativePath", "Public/BrushEffectsList.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FLandmassBrushEffectsList_Statics::NewProp_SmoothBlending = { "SmoothBlending", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLandmassBrushEffectsList, SmoothBlending), Z_Construct_UScriptStruct_FBrushEffectSmoothBlending, METADATA_PARAMS(Z_Construct_UScriptStruct_FLandmassBrushEffectsList_Statics::NewProp_SmoothBlending_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLandmassBrushEffectsList_Statics::NewProp_SmoothBlending_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLandmassBrushEffectsList_Statics::NewProp_Terracing_MetaData[] = {
		{ "Category", "FalloffSettings" },
		{ "ModuleRelativePath", "Public/BrushEffectsList.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FLandmassBrushEffectsList_Statics::NewProp_Terracing = { "Terracing", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLandmassBrushEffectsList, Terracing), Z_Construct_UScriptStruct_FBrushEffectTerracing, METADATA_PARAMS(Z_Construct_UScriptStruct_FLandmassBrushEffectsList_Statics::NewProp_Terracing_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLandmassBrushEffectsList_Statics::NewProp_Terracing_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FLandmassBrushEffectsList_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLandmassBrushEffectsList_Statics::NewProp_Blurring,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLandmassBrushEffectsList_Statics::NewProp_CurlNoise,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLandmassBrushEffectsList_Statics::NewProp_Displacement,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLandmassBrushEffectsList_Statics::NewProp_SmoothBlending,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLandmassBrushEffectsList_Statics::NewProp_Terracing,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FLandmassBrushEffectsList_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Landmass,
		nullptr,
		&NewStructOps,
		"LandmassBrushEffectsList",
		sizeof(FLandmassBrushEffectsList),
		alignof(FLandmassBrushEffectsList),
		Z_Construct_UScriptStruct_FLandmassBrushEffectsList_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLandmassBrushEffectsList_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FLandmassBrushEffectsList_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLandmassBrushEffectsList_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FLandmassBrushEffectsList()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FLandmassBrushEffectsList_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Landmass();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("LandmassBrushEffectsList"), sizeof(FLandmassBrushEffectsList), Get_Z_Construct_UScriptStruct_FLandmassBrushEffectsList_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FLandmassBrushEffectsList_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FLandmassBrushEffectsList_Hash() { return 3210040282U; }
class UScriptStruct* FBrushEffectTerracing::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LANDMASS_API uint32 Get_Z_Construct_UScriptStruct_FBrushEffectTerracing_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FBrushEffectTerracing, Z_Construct_UPackage__Script_Landmass(), TEXT("BrushEffectTerracing"), sizeof(FBrushEffectTerracing), Get_Z_Construct_UScriptStruct_FBrushEffectTerracing_Hash());
	}
	return Singleton;
}
template<> LANDMASS_API UScriptStruct* StaticStruct<FBrushEffectTerracing>()
{
	return FBrushEffectTerracing::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FBrushEffectTerracing(FBrushEffectTerracing::StaticStruct, TEXT("/Script/Landmass"), TEXT("BrushEffectTerracing"), false, nullptr, nullptr);
static struct FScriptStruct_Landmass_StaticRegisterNativesFBrushEffectTerracing
{
	FScriptStruct_Landmass_StaticRegisterNativesFBrushEffectTerracing()
	{
		UScriptStruct::DeferCppStructOps<FBrushEffectTerracing>(FName(TEXT("BrushEffectTerracing")));
	}
} ScriptStruct_Landmass_StaticRegisterNativesFBrushEffectTerracing;
	struct Z_Construct_UScriptStruct_FBrushEffectTerracing_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TerraceAlpha_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TerraceAlpha;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TerraceSpacing_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TerraceSpacing;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TerraceSmoothness_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TerraceSmoothness;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaskLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaskLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaskStartOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaskStartOffset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBrushEffectTerracing_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/BrushEffectsList.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FBrushEffectTerracing_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FBrushEffectTerracing>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBrushEffectTerracing_Statics::NewProp_TerraceAlpha_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/BrushEffectsList.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FBrushEffectTerracing_Statics::NewProp_TerraceAlpha = { "TerraceAlpha", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBrushEffectTerracing, TerraceAlpha), METADATA_PARAMS(Z_Construct_UScriptStruct_FBrushEffectTerracing_Statics::NewProp_TerraceAlpha_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBrushEffectTerracing_Statics::NewProp_TerraceAlpha_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBrushEffectTerracing_Statics::NewProp_TerraceSpacing_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/BrushEffectsList.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FBrushEffectTerracing_Statics::NewProp_TerraceSpacing = { "TerraceSpacing", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBrushEffectTerracing, TerraceSpacing), METADATA_PARAMS(Z_Construct_UScriptStruct_FBrushEffectTerracing_Statics::NewProp_TerraceSpacing_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBrushEffectTerracing_Statics::NewProp_TerraceSpacing_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBrushEffectTerracing_Statics::NewProp_TerraceSmoothness_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/BrushEffectsList.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FBrushEffectTerracing_Statics::NewProp_TerraceSmoothness = { "TerraceSmoothness", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBrushEffectTerracing, TerraceSmoothness), METADATA_PARAMS(Z_Construct_UScriptStruct_FBrushEffectTerracing_Statics::NewProp_TerraceSmoothness_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBrushEffectTerracing_Statics::NewProp_TerraceSmoothness_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBrushEffectTerracing_Statics::NewProp_MaskLength_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/BrushEffectsList.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FBrushEffectTerracing_Statics::NewProp_MaskLength = { "MaskLength", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBrushEffectTerracing, MaskLength), METADATA_PARAMS(Z_Construct_UScriptStruct_FBrushEffectTerracing_Statics::NewProp_MaskLength_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBrushEffectTerracing_Statics::NewProp_MaskLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBrushEffectTerracing_Statics::NewProp_MaskStartOffset_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/BrushEffectsList.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FBrushEffectTerracing_Statics::NewProp_MaskStartOffset = { "MaskStartOffset", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBrushEffectTerracing, MaskStartOffset), METADATA_PARAMS(Z_Construct_UScriptStruct_FBrushEffectTerracing_Statics::NewProp_MaskStartOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBrushEffectTerracing_Statics::NewProp_MaskStartOffset_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FBrushEffectTerracing_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBrushEffectTerracing_Statics::NewProp_TerraceAlpha,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBrushEffectTerracing_Statics::NewProp_TerraceSpacing,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBrushEffectTerracing_Statics::NewProp_TerraceSmoothness,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBrushEffectTerracing_Statics::NewProp_MaskLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBrushEffectTerracing_Statics::NewProp_MaskStartOffset,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FBrushEffectTerracing_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Landmass,
		nullptr,
		&NewStructOps,
		"BrushEffectTerracing",
		sizeof(FBrushEffectTerracing),
		alignof(FBrushEffectTerracing),
		Z_Construct_UScriptStruct_FBrushEffectTerracing_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBrushEffectTerracing_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FBrushEffectTerracing_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBrushEffectTerracing_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FBrushEffectTerracing()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FBrushEffectTerracing_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Landmass();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("BrushEffectTerracing"), sizeof(FBrushEffectTerracing), Get_Z_Construct_UScriptStruct_FBrushEffectTerracing_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FBrushEffectTerracing_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FBrushEffectTerracing_Hash() { return 328793632U; }
class UScriptStruct* FBrushEffectSmoothBlending::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LANDMASS_API uint32 Get_Z_Construct_UScriptStruct_FBrushEffectSmoothBlending_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FBrushEffectSmoothBlending, Z_Construct_UPackage__Script_Landmass(), TEXT("BrushEffectSmoothBlending"), sizeof(FBrushEffectSmoothBlending), Get_Z_Construct_UScriptStruct_FBrushEffectSmoothBlending_Hash());
	}
	return Singleton;
}
template<> LANDMASS_API UScriptStruct* StaticStruct<FBrushEffectSmoothBlending>()
{
	return FBrushEffectSmoothBlending::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FBrushEffectSmoothBlending(FBrushEffectSmoothBlending::StaticStruct, TEXT("/Script/Landmass"), TEXT("BrushEffectSmoothBlending"), false, nullptr, nullptr);
static struct FScriptStruct_Landmass_StaticRegisterNativesFBrushEffectSmoothBlending
{
	FScriptStruct_Landmass_StaticRegisterNativesFBrushEffectSmoothBlending()
	{
		UScriptStruct::DeferCppStructOps<FBrushEffectSmoothBlending>(FName(TEXT("BrushEffectSmoothBlending")));
	}
} ScriptStruct_Landmass_StaticRegisterNativesFBrushEffectSmoothBlending;
	struct Z_Construct_UScriptStruct_FBrushEffectSmoothBlending_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InnerSmoothDistance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InnerSmoothDistance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OuterSmoothDistance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_OuterSmoothDistance;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBrushEffectSmoothBlending_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/BrushEffectsList.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FBrushEffectSmoothBlending_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FBrushEffectSmoothBlending>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBrushEffectSmoothBlending_Statics::NewProp_InnerSmoothDistance_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/BrushEffectsList.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FBrushEffectSmoothBlending_Statics::NewProp_InnerSmoothDistance = { "InnerSmoothDistance", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBrushEffectSmoothBlending, InnerSmoothDistance), METADATA_PARAMS(Z_Construct_UScriptStruct_FBrushEffectSmoothBlending_Statics::NewProp_InnerSmoothDistance_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBrushEffectSmoothBlending_Statics::NewProp_InnerSmoothDistance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBrushEffectSmoothBlending_Statics::NewProp_OuterSmoothDistance_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/BrushEffectsList.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FBrushEffectSmoothBlending_Statics::NewProp_OuterSmoothDistance = { "OuterSmoothDistance", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBrushEffectSmoothBlending, OuterSmoothDistance), METADATA_PARAMS(Z_Construct_UScriptStruct_FBrushEffectSmoothBlending_Statics::NewProp_OuterSmoothDistance_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBrushEffectSmoothBlending_Statics::NewProp_OuterSmoothDistance_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FBrushEffectSmoothBlending_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBrushEffectSmoothBlending_Statics::NewProp_InnerSmoothDistance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBrushEffectSmoothBlending_Statics::NewProp_OuterSmoothDistance,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FBrushEffectSmoothBlending_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Landmass,
		nullptr,
		&NewStructOps,
		"BrushEffectSmoothBlending",
		sizeof(FBrushEffectSmoothBlending),
		alignof(FBrushEffectSmoothBlending),
		Z_Construct_UScriptStruct_FBrushEffectSmoothBlending_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBrushEffectSmoothBlending_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FBrushEffectSmoothBlending_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBrushEffectSmoothBlending_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FBrushEffectSmoothBlending()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FBrushEffectSmoothBlending_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Landmass();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("BrushEffectSmoothBlending"), sizeof(FBrushEffectSmoothBlending), Get_Z_Construct_UScriptStruct_FBrushEffectSmoothBlending_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FBrushEffectSmoothBlending_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FBrushEffectSmoothBlending_Hash() { return 813123246U; }
class UScriptStruct* FBrushEffectDisplacement::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LANDMASS_API uint32 Get_Z_Construct_UScriptStruct_FBrushEffectDisplacement_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FBrushEffectDisplacement, Z_Construct_UPackage__Script_Landmass(), TEXT("BrushEffectDisplacement"), sizeof(FBrushEffectDisplacement), Get_Z_Construct_UScriptStruct_FBrushEffectDisplacement_Hash());
	}
	return Singleton;
}
template<> LANDMASS_API UScriptStruct* StaticStruct<FBrushEffectDisplacement>()
{
	return FBrushEffectDisplacement::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FBrushEffectDisplacement(FBrushEffectDisplacement::StaticStruct, TEXT("/Script/Landmass"), TEXT("BrushEffectDisplacement"), false, nullptr, nullptr);
static struct FScriptStruct_Landmass_StaticRegisterNativesFBrushEffectDisplacement
{
	FScriptStruct_Landmass_StaticRegisterNativesFBrushEffectDisplacement()
	{
		UScriptStruct::DeferCppStructOps<FBrushEffectDisplacement>(FName(TEXT("BrushEffectDisplacement")));
	}
} ScriptStruct_Landmass_StaticRegisterNativesFBrushEffectDisplacement;
	struct Z_Construct_UScriptStruct_FBrushEffectDisplacement_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisplacementHeight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DisplacementHeight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisplacementTiling_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DisplacementTiling;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Texture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Texture;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Midpoint_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Midpoint;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Channel_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Channel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WeightmapInfluence_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_WeightmapInfluence;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBrushEffectDisplacement_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/BrushEffectsList.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FBrushEffectDisplacement_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FBrushEffectDisplacement>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBrushEffectDisplacement_Statics::NewProp_DisplacementHeight_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/BrushEffectsList.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FBrushEffectDisplacement_Statics::NewProp_DisplacementHeight = { "DisplacementHeight", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBrushEffectDisplacement, DisplacementHeight), METADATA_PARAMS(Z_Construct_UScriptStruct_FBrushEffectDisplacement_Statics::NewProp_DisplacementHeight_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBrushEffectDisplacement_Statics::NewProp_DisplacementHeight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBrushEffectDisplacement_Statics::NewProp_DisplacementTiling_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/BrushEffectsList.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FBrushEffectDisplacement_Statics::NewProp_DisplacementTiling = { "DisplacementTiling", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBrushEffectDisplacement, DisplacementTiling), METADATA_PARAMS(Z_Construct_UScriptStruct_FBrushEffectDisplacement_Statics::NewProp_DisplacementTiling_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBrushEffectDisplacement_Statics::NewProp_DisplacementTiling_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBrushEffectDisplacement_Statics::NewProp_Texture_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/BrushEffectsList.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FBrushEffectDisplacement_Statics::NewProp_Texture = { "Texture", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBrushEffectDisplacement, Texture), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FBrushEffectDisplacement_Statics::NewProp_Texture_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBrushEffectDisplacement_Statics::NewProp_Texture_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBrushEffectDisplacement_Statics::NewProp_Midpoint_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/BrushEffectsList.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FBrushEffectDisplacement_Statics::NewProp_Midpoint = { "Midpoint", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBrushEffectDisplacement, Midpoint), METADATA_PARAMS(Z_Construct_UScriptStruct_FBrushEffectDisplacement_Statics::NewProp_Midpoint_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBrushEffectDisplacement_Statics::NewProp_Midpoint_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBrushEffectDisplacement_Statics::NewProp_Channel_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/BrushEffectsList.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FBrushEffectDisplacement_Statics::NewProp_Channel = { "Channel", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBrushEffectDisplacement, Channel), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UScriptStruct_FBrushEffectDisplacement_Statics::NewProp_Channel_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBrushEffectDisplacement_Statics::NewProp_Channel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBrushEffectDisplacement_Statics::NewProp_WeightmapInfluence_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/BrushEffectsList.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FBrushEffectDisplacement_Statics::NewProp_WeightmapInfluence = { "WeightmapInfluence", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBrushEffectDisplacement, WeightmapInfluence), METADATA_PARAMS(Z_Construct_UScriptStruct_FBrushEffectDisplacement_Statics::NewProp_WeightmapInfluence_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBrushEffectDisplacement_Statics::NewProp_WeightmapInfluence_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FBrushEffectDisplacement_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBrushEffectDisplacement_Statics::NewProp_DisplacementHeight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBrushEffectDisplacement_Statics::NewProp_DisplacementTiling,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBrushEffectDisplacement_Statics::NewProp_Texture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBrushEffectDisplacement_Statics::NewProp_Midpoint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBrushEffectDisplacement_Statics::NewProp_Channel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBrushEffectDisplacement_Statics::NewProp_WeightmapInfluence,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FBrushEffectDisplacement_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Landmass,
		nullptr,
		&NewStructOps,
		"BrushEffectDisplacement",
		sizeof(FBrushEffectDisplacement),
		alignof(FBrushEffectDisplacement),
		Z_Construct_UScriptStruct_FBrushEffectDisplacement_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBrushEffectDisplacement_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FBrushEffectDisplacement_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBrushEffectDisplacement_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FBrushEffectDisplacement()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FBrushEffectDisplacement_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Landmass();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("BrushEffectDisplacement"), sizeof(FBrushEffectDisplacement), Get_Z_Construct_UScriptStruct_FBrushEffectDisplacement_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FBrushEffectDisplacement_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FBrushEffectDisplacement_Hash() { return 1322814680U; }
class UScriptStruct* FBrushEffectCurves::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LANDMASS_API uint32 Get_Z_Construct_UScriptStruct_FBrushEffectCurves_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FBrushEffectCurves, Z_Construct_UPackage__Script_Landmass(), TEXT("BrushEffectCurves"), sizeof(FBrushEffectCurves), Get_Z_Construct_UScriptStruct_FBrushEffectCurves_Hash());
	}
	return Singleton;
}
template<> LANDMASS_API UScriptStruct* StaticStruct<FBrushEffectCurves>()
{
	return FBrushEffectCurves::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FBrushEffectCurves(FBrushEffectCurves::StaticStruct, TEXT("/Script/Landmass"), TEXT("BrushEffectCurves"), false, nullptr, nullptr);
static struct FScriptStruct_Landmass_StaticRegisterNativesFBrushEffectCurves
{
	FScriptStruct_Landmass_StaticRegisterNativesFBrushEffectCurves()
	{
		UScriptStruct::DeferCppStructOps<FBrushEffectCurves>(FName(TEXT("BrushEffectCurves")));
	}
} ScriptStruct_Landmass_StaticRegisterNativesFBrushEffectCurves;
	struct Z_Construct_UScriptStruct_FBrushEffectCurves_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseCurveChannel_MetaData[];
#endif
		static void NewProp_bUseCurveChannel_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseCurveChannel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ElevationCurveAsset_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ElevationCurveAsset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChannelEdgeOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ChannelEdgeOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChannelDepth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ChannelDepth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurveRampWidth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CurveRampWidth;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBrushEffectCurves_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/BrushEffectsList.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FBrushEffectCurves_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FBrushEffectCurves>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBrushEffectCurves_Statics::NewProp_bUseCurveChannel_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/BrushEffectsList.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FBrushEffectCurves_Statics::NewProp_bUseCurveChannel_SetBit(void* Obj)
	{
		((FBrushEffectCurves*)Obj)->bUseCurveChannel = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FBrushEffectCurves_Statics::NewProp_bUseCurveChannel = { "bUseCurveChannel", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FBrushEffectCurves), &Z_Construct_UScriptStruct_FBrushEffectCurves_Statics::NewProp_bUseCurveChannel_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FBrushEffectCurves_Statics::NewProp_bUseCurveChannel_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBrushEffectCurves_Statics::NewProp_bUseCurveChannel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBrushEffectCurves_Statics::NewProp_ElevationCurveAsset_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/BrushEffectsList.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FBrushEffectCurves_Statics::NewProp_ElevationCurveAsset = { "ElevationCurveAsset", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBrushEffectCurves, ElevationCurveAsset), Z_Construct_UClass_UCurveFloat_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FBrushEffectCurves_Statics::NewProp_ElevationCurveAsset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBrushEffectCurves_Statics::NewProp_ElevationCurveAsset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBrushEffectCurves_Statics::NewProp_ChannelEdgeOffset_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/BrushEffectsList.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FBrushEffectCurves_Statics::NewProp_ChannelEdgeOffset = { "ChannelEdgeOffset", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBrushEffectCurves, ChannelEdgeOffset), METADATA_PARAMS(Z_Construct_UScriptStruct_FBrushEffectCurves_Statics::NewProp_ChannelEdgeOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBrushEffectCurves_Statics::NewProp_ChannelEdgeOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBrushEffectCurves_Statics::NewProp_ChannelDepth_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/BrushEffectsList.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FBrushEffectCurves_Statics::NewProp_ChannelDepth = { "ChannelDepth", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBrushEffectCurves, ChannelDepth), METADATA_PARAMS(Z_Construct_UScriptStruct_FBrushEffectCurves_Statics::NewProp_ChannelDepth_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBrushEffectCurves_Statics::NewProp_ChannelDepth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBrushEffectCurves_Statics::NewProp_CurveRampWidth_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/BrushEffectsList.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FBrushEffectCurves_Statics::NewProp_CurveRampWidth = { "CurveRampWidth", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBrushEffectCurves, CurveRampWidth), METADATA_PARAMS(Z_Construct_UScriptStruct_FBrushEffectCurves_Statics::NewProp_CurveRampWidth_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBrushEffectCurves_Statics::NewProp_CurveRampWidth_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FBrushEffectCurves_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBrushEffectCurves_Statics::NewProp_bUseCurveChannel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBrushEffectCurves_Statics::NewProp_ElevationCurveAsset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBrushEffectCurves_Statics::NewProp_ChannelEdgeOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBrushEffectCurves_Statics::NewProp_ChannelDepth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBrushEffectCurves_Statics::NewProp_CurveRampWidth,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FBrushEffectCurves_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Landmass,
		nullptr,
		&NewStructOps,
		"BrushEffectCurves",
		sizeof(FBrushEffectCurves),
		alignof(FBrushEffectCurves),
		Z_Construct_UScriptStruct_FBrushEffectCurves_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBrushEffectCurves_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FBrushEffectCurves_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBrushEffectCurves_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FBrushEffectCurves()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FBrushEffectCurves_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Landmass();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("BrushEffectCurves"), sizeof(FBrushEffectCurves), Get_Z_Construct_UScriptStruct_FBrushEffectCurves_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FBrushEffectCurves_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FBrushEffectCurves_Hash() { return 326370244U; }
class UScriptStruct* FBrushEffectCurlNoise::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LANDMASS_API uint32 Get_Z_Construct_UScriptStruct_FBrushEffectCurlNoise_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FBrushEffectCurlNoise, Z_Construct_UPackage__Script_Landmass(), TEXT("BrushEffectCurlNoise"), sizeof(FBrushEffectCurlNoise), Get_Z_Construct_UScriptStruct_FBrushEffectCurlNoise_Hash());
	}
	return Singleton;
}
template<> LANDMASS_API UScriptStruct* StaticStruct<FBrushEffectCurlNoise>()
{
	return FBrushEffectCurlNoise::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FBrushEffectCurlNoise(FBrushEffectCurlNoise::StaticStruct, TEXT("/Script/Landmass"), TEXT("BrushEffectCurlNoise"), false, nullptr, nullptr);
static struct FScriptStruct_Landmass_StaticRegisterNativesFBrushEffectCurlNoise
{
	FScriptStruct_Landmass_StaticRegisterNativesFBrushEffectCurlNoise()
	{
		UScriptStruct::DeferCppStructOps<FBrushEffectCurlNoise>(FName(TEXT("BrushEffectCurlNoise")));
	}
} ScriptStruct_Landmass_StaticRegisterNativesFBrushEffectCurlNoise;
	struct Z_Construct_UScriptStruct_FBrushEffectCurlNoise_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Curl1Amount_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Curl1Amount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Curl2Amount_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Curl2Amount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Curl1Tiling_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Curl1Tiling;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Curl2Tiling_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Curl2Tiling;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBrushEffectCurlNoise_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/BrushEffectsList.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FBrushEffectCurlNoise_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FBrushEffectCurlNoise>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBrushEffectCurlNoise_Statics::NewProp_Curl1Amount_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/BrushEffectsList.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FBrushEffectCurlNoise_Statics::NewProp_Curl1Amount = { "Curl1Amount", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBrushEffectCurlNoise, Curl1Amount), METADATA_PARAMS(Z_Construct_UScriptStruct_FBrushEffectCurlNoise_Statics::NewProp_Curl1Amount_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBrushEffectCurlNoise_Statics::NewProp_Curl1Amount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBrushEffectCurlNoise_Statics::NewProp_Curl2Amount_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/BrushEffectsList.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FBrushEffectCurlNoise_Statics::NewProp_Curl2Amount = { "Curl2Amount", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBrushEffectCurlNoise, Curl2Amount), METADATA_PARAMS(Z_Construct_UScriptStruct_FBrushEffectCurlNoise_Statics::NewProp_Curl2Amount_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBrushEffectCurlNoise_Statics::NewProp_Curl2Amount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBrushEffectCurlNoise_Statics::NewProp_Curl1Tiling_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/BrushEffectsList.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FBrushEffectCurlNoise_Statics::NewProp_Curl1Tiling = { "Curl1Tiling", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBrushEffectCurlNoise, Curl1Tiling), METADATA_PARAMS(Z_Construct_UScriptStruct_FBrushEffectCurlNoise_Statics::NewProp_Curl1Tiling_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBrushEffectCurlNoise_Statics::NewProp_Curl1Tiling_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBrushEffectCurlNoise_Statics::NewProp_Curl2Tiling_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/BrushEffectsList.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FBrushEffectCurlNoise_Statics::NewProp_Curl2Tiling = { "Curl2Tiling", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBrushEffectCurlNoise, Curl2Tiling), METADATA_PARAMS(Z_Construct_UScriptStruct_FBrushEffectCurlNoise_Statics::NewProp_Curl2Tiling_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBrushEffectCurlNoise_Statics::NewProp_Curl2Tiling_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FBrushEffectCurlNoise_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBrushEffectCurlNoise_Statics::NewProp_Curl1Amount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBrushEffectCurlNoise_Statics::NewProp_Curl2Amount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBrushEffectCurlNoise_Statics::NewProp_Curl1Tiling,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBrushEffectCurlNoise_Statics::NewProp_Curl2Tiling,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FBrushEffectCurlNoise_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Landmass,
		nullptr,
		&NewStructOps,
		"BrushEffectCurlNoise",
		sizeof(FBrushEffectCurlNoise),
		alignof(FBrushEffectCurlNoise),
		Z_Construct_UScriptStruct_FBrushEffectCurlNoise_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBrushEffectCurlNoise_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FBrushEffectCurlNoise_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBrushEffectCurlNoise_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FBrushEffectCurlNoise()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FBrushEffectCurlNoise_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Landmass();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("BrushEffectCurlNoise"), sizeof(FBrushEffectCurlNoise), Get_Z_Construct_UScriptStruct_FBrushEffectCurlNoise_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FBrushEffectCurlNoise_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FBrushEffectCurlNoise_Hash() { return 2974299344U; }
class UScriptStruct* FBrushEffectBlurring::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LANDMASS_API uint32 Get_Z_Construct_UScriptStruct_FBrushEffectBlurring_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FBrushEffectBlurring, Z_Construct_UPackage__Script_Landmass(), TEXT("BrushEffectBlurring"), sizeof(FBrushEffectBlurring), Get_Z_Construct_UScriptStruct_FBrushEffectBlurring_Hash());
	}
	return Singleton;
}
template<> LANDMASS_API UScriptStruct* StaticStruct<FBrushEffectBlurring>()
{
	return FBrushEffectBlurring::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FBrushEffectBlurring(FBrushEffectBlurring::StaticStruct, TEXT("/Script/Landmass"), TEXT("BrushEffectBlurring"), false, nullptr, nullptr);
static struct FScriptStruct_Landmass_StaticRegisterNativesFBrushEffectBlurring
{
	FScriptStruct_Landmass_StaticRegisterNativesFBrushEffectBlurring()
	{
		UScriptStruct::DeferCppStructOps<FBrushEffectBlurring>(FName(TEXT("BrushEffectBlurring")));
	}
} ScriptStruct_Landmass_StaticRegisterNativesFBrushEffectBlurring;
	struct Z_Construct_UScriptStruct_FBrushEffectBlurring_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bBlurShape_MetaData[];
#endif
		static void NewProp_bBlurShape_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bBlurShape;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Radius_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Radius;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBrushEffectBlurring_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/BrushEffectsList.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FBrushEffectBlurring_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FBrushEffectBlurring>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBrushEffectBlurring_Statics::NewProp_bBlurShape_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/BrushEffectsList.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FBrushEffectBlurring_Statics::NewProp_bBlurShape_SetBit(void* Obj)
	{
		((FBrushEffectBlurring*)Obj)->bBlurShape = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FBrushEffectBlurring_Statics::NewProp_bBlurShape = { "bBlurShape", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FBrushEffectBlurring), &Z_Construct_UScriptStruct_FBrushEffectBlurring_Statics::NewProp_bBlurShape_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FBrushEffectBlurring_Statics::NewProp_bBlurShape_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBrushEffectBlurring_Statics::NewProp_bBlurShape_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBrushEffectBlurring_Statics::NewProp_Radius_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/BrushEffectsList.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FBrushEffectBlurring_Statics::NewProp_Radius = { "Radius", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBrushEffectBlurring, Radius), METADATA_PARAMS(Z_Construct_UScriptStruct_FBrushEffectBlurring_Statics::NewProp_Radius_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBrushEffectBlurring_Statics::NewProp_Radius_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FBrushEffectBlurring_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBrushEffectBlurring_Statics::NewProp_bBlurShape,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBrushEffectBlurring_Statics::NewProp_Radius,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FBrushEffectBlurring_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Landmass,
		nullptr,
		&NewStructOps,
		"BrushEffectBlurring",
		sizeof(FBrushEffectBlurring),
		alignof(FBrushEffectBlurring),
		Z_Construct_UScriptStruct_FBrushEffectBlurring_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBrushEffectBlurring_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FBrushEffectBlurring_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBrushEffectBlurring_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FBrushEffectBlurring()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FBrushEffectBlurring_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Landmass();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("BrushEffectBlurring"), sizeof(FBrushEffectBlurring), Get_Z_Construct_UScriptStruct_FBrushEffectBlurring_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FBrushEffectBlurring_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FBrushEffectBlurring_Hash() { return 599355340U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
