// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Runtime/Public/FalloffSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFalloffSettings() {}
// Cross Module References
	LANDMASS_API UEnum* Z_Construct_UEnum_Landmass_EBrushFalloffMode();
	UPackage* Z_Construct_UPackage__Script_Landmass();
	LANDMASS_API UScriptStruct* Z_Construct_UScriptStruct_FLandmassFalloffSettings();
// End Cross Module References
	static UEnum* EBrushFalloffMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_Landmass_EBrushFalloffMode, Z_Construct_UPackage__Script_Landmass(), TEXT("EBrushFalloffMode"));
		}
		return Singleton;
	}
	template<> LANDMASS_API UEnum* StaticEnum<EBrushFalloffMode>()
	{
		return EBrushFalloffMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EBrushFalloffMode(EBrushFalloffMode_StaticEnum, TEXT("/Script/Landmass"), TEXT("EBrushFalloffMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_Landmass_EBrushFalloffMode_Hash() { return 2248063325U; }
	UEnum* Z_Construct_UEnum_Landmass_EBrushFalloffMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_Landmass();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EBrushFalloffMode"), 0, Get_Z_Construct_UEnum_Landmass_EBrushFalloffMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EBrushFalloffMode::Angle", (int64)EBrushFalloffMode::Angle },
				{ "EBrushFalloffMode::Width", (int64)EBrushFalloffMode::Width },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Angle.Name", "EBrushFalloffMode::Angle" },
				{ "BlueprintType", "true" },
				{ "ModuleRelativePath", "Public/FalloffSettings.h" },
				{ "Width.Name", "EBrushFalloffMode::Width" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_Landmass,
				nullptr,
				"EBrushFalloffMode",
				"EBrushFalloffMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FLandmassFalloffSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LANDMASS_API uint32 Get_Z_Construct_UScriptStruct_FLandmassFalloffSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FLandmassFalloffSettings, Z_Construct_UPackage__Script_Landmass(), TEXT("LandmassFalloffSettings"), sizeof(FLandmassFalloffSettings), Get_Z_Construct_UScriptStruct_FLandmassFalloffSettings_Hash());
	}
	return Singleton;
}
template<> LANDMASS_API UScriptStruct* StaticStruct<FLandmassFalloffSettings>()
{
	return FLandmassFalloffSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FLandmassFalloffSettings(FLandmassFalloffSettings::StaticStruct, TEXT("/Script/Landmass"), TEXT("LandmassFalloffSettings"), false, nullptr, nullptr);
static struct FScriptStruct_Landmass_StaticRegisterNativesFLandmassFalloffSettings
{
	FScriptStruct_Landmass_StaticRegisterNativesFLandmassFalloffSettings()
	{
		UScriptStruct::DeferCppStructOps<FLandmassFalloffSettings>(FName(TEXT("LandmassFalloffSettings")));
	}
} ScriptStruct_Landmass_StaticRegisterNativesFLandmassFalloffSettings;
	struct Z_Construct_UScriptStruct_FLandmassFalloffSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_FalloffMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FalloffMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_FalloffMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FalloffAngle_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FalloffAngle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FalloffWidth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FalloffWidth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EdgeOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_EdgeOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ZOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ZOffset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLandmassFalloffSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/FalloffSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FLandmassFalloffSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FLandmassFalloffSettings>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FLandmassFalloffSettings_Statics::NewProp_FalloffMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLandmassFalloffSettings_Statics::NewProp_FalloffMode_MetaData[] = {
		{ "Category", "FalloffSettings" },
		{ "ModuleRelativePath", "Public/FalloffSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FLandmassFalloffSettings_Statics::NewProp_FalloffMode = { "FalloffMode", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLandmassFalloffSettings, FalloffMode), Z_Construct_UEnum_Landmass_EBrushFalloffMode, METADATA_PARAMS(Z_Construct_UScriptStruct_FLandmassFalloffSettings_Statics::NewProp_FalloffMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLandmassFalloffSettings_Statics::NewProp_FalloffMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLandmassFalloffSettings_Statics::NewProp_FalloffAngle_MetaData[] = {
		{ "Category", "FalloffSettings" },
		{ "ModuleRelativePath", "Public/FalloffSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FLandmassFalloffSettings_Statics::NewProp_FalloffAngle = { "FalloffAngle", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLandmassFalloffSettings, FalloffAngle), METADATA_PARAMS(Z_Construct_UScriptStruct_FLandmassFalloffSettings_Statics::NewProp_FalloffAngle_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLandmassFalloffSettings_Statics::NewProp_FalloffAngle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLandmassFalloffSettings_Statics::NewProp_FalloffWidth_MetaData[] = {
		{ "Category", "FalloffSettings" },
		{ "ModuleRelativePath", "Public/FalloffSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FLandmassFalloffSettings_Statics::NewProp_FalloffWidth = { "FalloffWidth", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLandmassFalloffSettings, FalloffWidth), METADATA_PARAMS(Z_Construct_UScriptStruct_FLandmassFalloffSettings_Statics::NewProp_FalloffWidth_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLandmassFalloffSettings_Statics::NewProp_FalloffWidth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLandmassFalloffSettings_Statics::NewProp_EdgeOffset_MetaData[] = {
		{ "Category", "FalloffSettings" },
		{ "ModuleRelativePath", "Public/FalloffSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FLandmassFalloffSettings_Statics::NewProp_EdgeOffset = { "EdgeOffset", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLandmassFalloffSettings, EdgeOffset), METADATA_PARAMS(Z_Construct_UScriptStruct_FLandmassFalloffSettings_Statics::NewProp_EdgeOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLandmassFalloffSettings_Statics::NewProp_EdgeOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLandmassFalloffSettings_Statics::NewProp_ZOffset_MetaData[] = {
		{ "Category", "FalloffSettings" },
		{ "ModuleRelativePath", "Public/FalloffSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FLandmassFalloffSettings_Statics::NewProp_ZOffset = { "ZOffset", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLandmassFalloffSettings, ZOffset), METADATA_PARAMS(Z_Construct_UScriptStruct_FLandmassFalloffSettings_Statics::NewProp_ZOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLandmassFalloffSettings_Statics::NewProp_ZOffset_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FLandmassFalloffSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLandmassFalloffSettings_Statics::NewProp_FalloffMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLandmassFalloffSettings_Statics::NewProp_FalloffMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLandmassFalloffSettings_Statics::NewProp_FalloffAngle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLandmassFalloffSettings_Statics::NewProp_FalloffWidth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLandmassFalloffSettings_Statics::NewProp_EdgeOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLandmassFalloffSettings_Statics::NewProp_ZOffset,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FLandmassFalloffSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Landmass,
		nullptr,
		&NewStructOps,
		"LandmassFalloffSettings",
		sizeof(FLandmassFalloffSettings),
		alignof(FLandmassFalloffSettings),
		Z_Construct_UScriptStruct_FLandmassFalloffSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLandmassFalloffSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FLandmassFalloffSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLandmassFalloffSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FLandmassFalloffSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FLandmassFalloffSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Landmass();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("LandmassFalloffSettings"), sizeof(FLandmassFalloffSettings), Get_Z_Construct_UScriptStruct_FLandmassFalloffSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FLandmassFalloffSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FLandmassFalloffSettings_Hash() { return 4189170362U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
