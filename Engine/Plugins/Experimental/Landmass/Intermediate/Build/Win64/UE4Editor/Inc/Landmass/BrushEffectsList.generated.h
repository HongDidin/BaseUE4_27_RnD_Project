// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LANDMASS_BrushEffectsList_generated_h
#error "BrushEffectsList.generated.h already included, missing '#pragma once' in BrushEffectsList.h"
#endif
#define LANDMASS_BrushEffectsList_generated_h

#define Engine_Plugins_Experimental_Landmass_Source_Runtime_Public_BrushEffectsList_h_164_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FLandmassBrushEffectsList_Statics; \
	LANDMASS_API static class UScriptStruct* StaticStruct();


template<> LANDMASS_API UScriptStruct* StaticStruct<struct FLandmassBrushEffectsList>();

#define Engine_Plugins_Experimental_Landmass_Source_Runtime_Public_BrushEffectsList_h_134_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FBrushEffectTerracing_Statics; \
	LANDMASS_API static class UScriptStruct* StaticStruct();


template<> LANDMASS_API UScriptStruct* StaticStruct<struct FBrushEffectTerracing>();

#define Engine_Plugins_Experimental_Landmass_Source_Runtime_Public_BrushEffectsList_h_122_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FBrushEffectSmoothBlending_Statics; \
	LANDMASS_API static class UScriptStruct* StaticStruct();


template<> LANDMASS_API UScriptStruct* StaticStruct<struct FBrushEffectSmoothBlending>();

#define Engine_Plugins_Experimental_Landmass_Source_Runtime_Public_BrushEffectsList_h_88_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FBrushEffectDisplacement_Statics; \
	LANDMASS_API static class UScriptStruct* StaticStruct();


template<> LANDMASS_API UScriptStruct* StaticStruct<struct FBrushEffectDisplacement>();

#define Engine_Plugins_Experimental_Landmass_Source_Runtime_Public_BrushEffectsList_h_58_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FBrushEffectCurves_Statics; \
	LANDMASS_API static class UScriptStruct* StaticStruct();


template<> LANDMASS_API UScriptStruct* StaticStruct<struct FBrushEffectCurves>();

#define Engine_Plugins_Experimental_Landmass_Source_Runtime_Public_BrushEffectsList_h_33_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FBrushEffectCurlNoise_Statics; \
	LANDMASS_API static class UScriptStruct* StaticStruct();


template<> LANDMASS_API UScriptStruct* StaticStruct<struct FBrushEffectCurlNoise>();

#define Engine_Plugins_Experimental_Landmass_Source_Runtime_Public_BrushEffectsList_h_15_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FBrushEffectBlurring_Statics; \
	LANDMASS_API static class UScriptStruct* StaticStruct();


template<> LANDMASS_API UScriptStruct* StaticStruct<struct FBrushEffectBlurring>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_Landmass_Source_Runtime_Public_BrushEffectsList_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
