// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Editor/Public/LandmassBPEditorExtension.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLandmassBPEditorExtension() {}
// Cross Module References
	LANDMASSEDITOR_API UClass* Z_Construct_UClass_ULandmassBlueprintFunctionLibrary_NoRegister();
	LANDMASSEDITOR_API UClass* Z_Construct_UClass_ULandmassBlueprintFunctionLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_LandmassEditor();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
// End Cross Module References
	DEFINE_FUNCTION(ULandmassBlueprintFunctionLibrary::execGetCursorWorldRay)
	{
		P_GET_STRUCT_REF(FVector,Z_Param_Out_CameraLocation);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_RayOrigin);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_RayDirection);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=ULandmassBlueprintFunctionLibrary::GetCursorWorldRay(Z_Param_Out_CameraLocation,Z_Param_Out_RayOrigin,Z_Param_Out_RayDirection);
		P_NATIVE_END;
	}
	void ULandmassBlueprintFunctionLibrary::StaticRegisterNativesULandmassBlueprintFunctionLibrary()
	{
		UClass* Class = ULandmassBlueprintFunctionLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetCursorWorldRay", &ULandmassBlueprintFunctionLibrary::execGetCursorWorldRay },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ULandmassBlueprintFunctionLibrary_GetCursorWorldRay_Statics
	{
		struct LandmassBlueprintFunctionLibrary_eventGetCursorWorldRay_Parms
		{
			FVector CameraLocation;
			FVector RayOrigin;
			FVector RayDirection;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CameraLocation;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RayOrigin;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RayDirection;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULandmassBlueprintFunctionLibrary_GetCursorWorldRay_Statics::NewProp_CameraLocation = { "CameraLocation", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LandmassBlueprintFunctionLibrary_eventGetCursorWorldRay_Parms, CameraLocation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULandmassBlueprintFunctionLibrary_GetCursorWorldRay_Statics::NewProp_RayOrigin = { "RayOrigin", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LandmassBlueprintFunctionLibrary_eventGetCursorWorldRay_Parms, RayOrigin), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULandmassBlueprintFunctionLibrary_GetCursorWorldRay_Statics::NewProp_RayDirection = { "RayDirection", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LandmassBlueprintFunctionLibrary_eventGetCursorWorldRay_Parms, RayDirection), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_ULandmassBlueprintFunctionLibrary_GetCursorWorldRay_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((LandmassBlueprintFunctionLibrary_eventGetCursorWorldRay_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULandmassBlueprintFunctionLibrary_GetCursorWorldRay_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(LandmassBlueprintFunctionLibrary_eventGetCursorWorldRay_Parms), &Z_Construct_UFunction_ULandmassBlueprintFunctionLibrary_GetCursorWorldRay_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULandmassBlueprintFunctionLibrary_GetCursorWorldRay_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULandmassBlueprintFunctionLibrary_GetCursorWorldRay_Statics::NewProp_CameraLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULandmassBlueprintFunctionLibrary_GetCursorWorldRay_Statics::NewProp_RayOrigin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULandmassBlueprintFunctionLibrary_GetCursorWorldRay_Statics::NewProp_RayDirection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULandmassBlueprintFunctionLibrary_GetCursorWorldRay_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULandmassBlueprintFunctionLibrary_GetCursorWorldRay_Statics::Function_MetaDataParams[] = {
		{ "Category", "Rendering" },
		{ "DisplayName", "Get Cursor World Ray" },
		{ "Keywords", "Get Cursor World Ray" },
		{ "ModuleRelativePath", "Public/LandmassBPEditorExtension.h" },
		{ "UnsafeDuringActorConstruction", "true" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULandmassBlueprintFunctionLibrary_GetCursorWorldRay_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULandmassBlueprintFunctionLibrary, nullptr, "GetCursorWorldRay", nullptr, nullptr, sizeof(LandmassBlueprintFunctionLibrary_eventGetCursorWorldRay_Parms), Z_Construct_UFunction_ULandmassBlueprintFunctionLibrary_GetCursorWorldRay_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULandmassBlueprintFunctionLibrary_GetCursorWorldRay_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C22401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULandmassBlueprintFunctionLibrary_GetCursorWorldRay_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULandmassBlueprintFunctionLibrary_GetCursorWorldRay_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULandmassBlueprintFunctionLibrary_GetCursorWorldRay()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULandmassBlueprintFunctionLibrary_GetCursorWorldRay_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ULandmassBlueprintFunctionLibrary_NoRegister()
	{
		return ULandmassBlueprintFunctionLibrary::StaticClass();
	}
	struct Z_Construct_UClass_ULandmassBlueprintFunctionLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULandmassBlueprintFunctionLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_LandmassEditor,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ULandmassBlueprintFunctionLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ULandmassBlueprintFunctionLibrary_GetCursorWorldRay, "GetCursorWorldRay" }, // 1941524531
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULandmassBlueprintFunctionLibrary_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "LandmassBPEditorExtension.h" },
		{ "ModuleRelativePath", "Public/LandmassBPEditorExtension.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULandmassBlueprintFunctionLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULandmassBlueprintFunctionLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULandmassBlueprintFunctionLibrary_Statics::ClassParams = {
		&ULandmassBlueprintFunctionLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ULandmassBlueprintFunctionLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULandmassBlueprintFunctionLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULandmassBlueprintFunctionLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULandmassBlueprintFunctionLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULandmassBlueprintFunctionLibrary, 854742967);
	template<> LANDMASSEDITOR_API UClass* StaticClass<ULandmassBlueprintFunctionLibrary>()
	{
		return ULandmassBlueprintFunctionLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULandmassBlueprintFunctionLibrary(Z_Construct_UClass_ULandmassBlueprintFunctionLibrary, &ULandmassBlueprintFunctionLibrary::StaticClass, TEXT("/Script/LandmassEditor"), TEXT("ULandmassBlueprintFunctionLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULandmassBlueprintFunctionLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
