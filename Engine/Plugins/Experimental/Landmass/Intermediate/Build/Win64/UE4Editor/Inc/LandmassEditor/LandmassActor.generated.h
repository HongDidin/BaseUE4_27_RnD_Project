// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LANDMASSEDITOR_LandmassActor_generated_h
#error "LandmassActor.generated.h already included, missing '#pragma once' in LandmassActor.h"
#endif
#define LANDMASSEDITOR_LandmassActor_generated_h

#define Engine_Plugins_Experimental_Landmass_Source_Editor_Public_LandmassActor_h_17_SPARSE_DATA
#define Engine_Plugins_Experimental_Landmass_Source_Editor_Public_LandmassActor_h_17_RPC_WRAPPERS \
	virtual void ActorSelectionChanged_Implementation(bool bSelected); \
	virtual void CustomTick_Implementation(float DeltaSeconds); \
 \
	DECLARE_FUNCTION(execActorSelectionChanged); \
	DECLARE_FUNCTION(execSetEditorTickEnabled); \
	DECLARE_FUNCTION(execCustomTick);


#define Engine_Plugins_Experimental_Landmass_Source_Editor_Public_LandmassActor_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual void ActorSelectionChanged_Implementation(bool bSelected); \
	virtual void CustomTick_Implementation(float DeltaSeconds); \
 \
	DECLARE_FUNCTION(execActorSelectionChanged); \
	DECLARE_FUNCTION(execSetEditorTickEnabled); \
	DECLARE_FUNCTION(execCustomTick);


#define Engine_Plugins_Experimental_Landmass_Source_Editor_Public_LandmassActor_h_17_EVENT_PARMS \
	struct LandmassActor_eventActorSelectionChanged_Parms \
	{ \
		bool bSelected; \
	}; \
	struct LandmassActor_eventCustomTick_Parms \
	{ \
		float DeltaSeconds; \
	};


#define Engine_Plugins_Experimental_Landmass_Source_Editor_Public_LandmassActor_h_17_CALLBACK_WRAPPERS
#define Engine_Plugins_Experimental_Landmass_Source_Editor_Public_LandmassActor_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesALandmassActor(); \
	friend struct Z_Construct_UClass_ALandmassActor_Statics; \
public: \
	DECLARE_CLASS(ALandmassActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/LandmassEditor"), NO_API) \
	DECLARE_SERIALIZER(ALandmassActor)


#define Engine_Plugins_Experimental_Landmass_Source_Editor_Public_LandmassActor_h_17_INCLASS \
private: \
	static void StaticRegisterNativesALandmassActor(); \
	friend struct Z_Construct_UClass_ALandmassActor_Statics; \
public: \
	DECLARE_CLASS(ALandmassActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/LandmassEditor"), NO_API) \
	DECLARE_SERIALIZER(ALandmassActor)


#define Engine_Plugins_Experimental_Landmass_Source_Editor_Public_LandmassActor_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ALandmassActor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ALandmassActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALandmassActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALandmassActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALandmassActor(ALandmassActor&&); \
	NO_API ALandmassActor(const ALandmassActor&); \
public:


#define Engine_Plugins_Experimental_Landmass_Source_Editor_Public_LandmassActor_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ALandmassActor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALandmassActor(ALandmassActor&&); \
	NO_API ALandmassActor(const ALandmassActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALandmassActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALandmassActor); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ALandmassActor)


#define Engine_Plugins_Experimental_Landmass_Source_Editor_Public_LandmassActor_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_Landmass_Source_Editor_Public_LandmassActor_h_14_PROLOG \
	Engine_Plugins_Experimental_Landmass_Source_Editor_Public_LandmassActor_h_17_EVENT_PARMS


#define Engine_Plugins_Experimental_Landmass_Source_Editor_Public_LandmassActor_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_Landmass_Source_Editor_Public_LandmassActor_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_Landmass_Source_Editor_Public_LandmassActor_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_Landmass_Source_Editor_Public_LandmassActor_h_17_RPC_WRAPPERS \
	Engine_Plugins_Experimental_Landmass_Source_Editor_Public_LandmassActor_h_17_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_Landmass_Source_Editor_Public_LandmassActor_h_17_INCLASS \
	Engine_Plugins_Experimental_Landmass_Source_Editor_Public_LandmassActor_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_Landmass_Source_Editor_Public_LandmassActor_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_Landmass_Source_Editor_Public_LandmassActor_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_Landmass_Source_Editor_Public_LandmassActor_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_Landmass_Source_Editor_Public_LandmassActor_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_Landmass_Source_Editor_Public_LandmassActor_h_17_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_Landmass_Source_Editor_Public_LandmassActor_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_Landmass_Source_Editor_Public_LandmassActor_h_17_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class LandmassActor."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LANDMASSEDITOR_API UClass* StaticClass<class ALandmassActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_Landmass_Source_Editor_Public_LandmassActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
