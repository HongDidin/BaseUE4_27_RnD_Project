// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AWheeledVehiclePawn;
#ifdef CHAOSVEHICLES_VehicleAnimationInstance_generated_h
#error "VehicleAnimationInstance.generated.h already included, missing '#pragma once' in VehicleAnimationInstance.h"
#endif
#define CHAOSVEHICLES_VehicleAnimationInstance_generated_h

#define Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_VehicleAnimationInstance_h_24_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FVehicleAnimationInstanceProxy_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FAnimInstanceProxy Super;


template<> CHAOSVEHICLES_API UScriptStruct* StaticStruct<struct FVehicleAnimationInstanceProxy>();

#define Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_VehicleAnimationInstance_h_56_SPARSE_DATA
#define Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_VehicleAnimationInstance_h_56_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetVehicle);


#define Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_VehicleAnimationInstance_h_56_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetVehicle);


#define Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_VehicleAnimationInstance_h_56_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVehicleAnimationInstance(); \
	friend struct Z_Construct_UClass_UVehicleAnimationInstance_Statics; \
public: \
	DECLARE_CLASS(UVehicleAnimationInstance, UAnimInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ChaosVehicles"), NO_API) \
	DECLARE_SERIALIZER(UVehicleAnimationInstance)


#define Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_VehicleAnimationInstance_h_56_INCLASS \
private: \
	static void StaticRegisterNativesUVehicleAnimationInstance(); \
	friend struct Z_Construct_UClass_UVehicleAnimationInstance_Statics; \
public: \
	DECLARE_CLASS(UVehicleAnimationInstance, UAnimInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ChaosVehicles"), NO_API) \
	DECLARE_SERIALIZER(UVehicleAnimationInstance)


#define Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_VehicleAnimationInstance_h_56_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVehicleAnimationInstance(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVehicleAnimationInstance) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVehicleAnimationInstance); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVehicleAnimationInstance); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVehicleAnimationInstance(UVehicleAnimationInstance&&); \
	NO_API UVehicleAnimationInstance(const UVehicleAnimationInstance&); \
public:


#define Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_VehicleAnimationInstance_h_56_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVehicleAnimationInstance(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVehicleAnimationInstance(UVehicleAnimationInstance&&); \
	NO_API UVehicleAnimationInstance(const UVehicleAnimationInstance&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVehicleAnimationInstance); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVehicleAnimationInstance); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVehicleAnimationInstance)


#define Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_VehicleAnimationInstance_h_56_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__WheeledVehicleComponent() { return STRUCT_OFFSET(UVehicleAnimationInstance, WheeledVehicleComponent); }


#define Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_VehicleAnimationInstance_h_53_PROLOG
#define Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_VehicleAnimationInstance_h_56_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_VehicleAnimationInstance_h_56_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_VehicleAnimationInstance_h_56_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_VehicleAnimationInstance_h_56_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_VehicleAnimationInstance_h_56_INCLASS \
	Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_VehicleAnimationInstance_h_56_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_VehicleAnimationInstance_h_56_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_VehicleAnimationInstance_h_56_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_VehicleAnimationInstance_h_56_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_VehicleAnimationInstance_h_56_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_VehicleAnimationInstance_h_56_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_VehicleAnimationInstance_h_56_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class VehicleAnimationInstance."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CHAOSVEHICLES_API UClass* StaticClass<class UVehicleAnimationInstance>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_VehicleAnimationInstance_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
