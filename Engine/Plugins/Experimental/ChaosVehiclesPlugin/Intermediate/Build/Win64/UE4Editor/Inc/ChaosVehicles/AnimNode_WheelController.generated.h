// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CHAOSVEHICLES_AnimNode_WheelController_generated_h
#error "AnimNode_WheelController.generated.h already included, missing '#pragma once' in AnimNode_WheelController.h"
#endif
#define CHAOSVEHICLES_AnimNode_WheelController_generated_h

#define Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_AnimNode_WheelController_h_19_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAnimNode_WheelController_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FAnimNode_SkeletalControlBase Super;


template<> CHAOSVEHICLES_API UScriptStruct* StaticStruct<struct FAnimNode_WheelController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_AnimNode_WheelController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
