// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CHAOSVEHICLES_WheeledVehiclePawn_generated_h
#error "WheeledVehiclePawn.generated.h already included, missing '#pragma once' in WheeledVehiclePawn.h"
#endif
#define CHAOSVEHICLES_WheeledVehiclePawn_generated_h

#define Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_WheeledVehiclePawn_h_22_SPARSE_DATA
#define Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_WheeledVehiclePawn_h_22_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_WheeledVehiclePawn_h_22_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_WheeledVehiclePawn_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAWheeledVehiclePawn(); \
	friend struct Z_Construct_UClass_AWheeledVehiclePawn_Statics; \
public: \
	DECLARE_CLASS(AWheeledVehiclePawn, APawn, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Config), CASTCLASS_None, TEXT("/Script/ChaosVehicles"), NO_API) \
	DECLARE_SERIALIZER(AWheeledVehiclePawn)


#define Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_WheeledVehiclePawn_h_22_INCLASS \
private: \
	static void StaticRegisterNativesAWheeledVehiclePawn(); \
	friend struct Z_Construct_UClass_AWheeledVehiclePawn_Statics; \
public: \
	DECLARE_CLASS(AWheeledVehiclePawn, APawn, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Config), CASTCLASS_None, TEXT("/Script/ChaosVehicles"), NO_API) \
	DECLARE_SERIALIZER(AWheeledVehiclePawn)


#define Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_WheeledVehiclePawn_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AWheeledVehiclePawn(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AWheeledVehiclePawn) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWheeledVehiclePawn); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWheeledVehiclePawn); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWheeledVehiclePawn(AWheeledVehiclePawn&&); \
	NO_API AWheeledVehiclePawn(const AWheeledVehiclePawn&); \
public:


#define Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_WheeledVehiclePawn_h_22_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AWheeledVehiclePawn(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWheeledVehiclePawn(AWheeledVehiclePawn&&); \
	NO_API AWheeledVehiclePawn(const AWheeledVehiclePawn&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWheeledVehiclePawn); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWheeledVehiclePawn); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AWheeledVehiclePawn)


#define Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_WheeledVehiclePawn_h_22_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Mesh() { return STRUCT_OFFSET(AWheeledVehiclePawn, Mesh); } \
	FORCEINLINE static uint32 __PPO__VehicleMovementComponent() { return STRUCT_OFFSET(AWheeledVehiclePawn, VehicleMovementComponent); }


#define Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_WheeledVehiclePawn_h_19_PROLOG
#define Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_WheeledVehiclePawn_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_WheeledVehiclePawn_h_22_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_WheeledVehiclePawn_h_22_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_WheeledVehiclePawn_h_22_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_WheeledVehiclePawn_h_22_INCLASS \
	Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_WheeledVehiclePawn_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_WheeledVehiclePawn_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_WheeledVehiclePawn_h_22_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_WheeledVehiclePawn_h_22_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_WheeledVehiclePawn_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_WheeledVehiclePawn_h_22_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_WheeledVehiclePawn_h_22_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class WheeledVehiclePawn."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CHAOSVEHICLES_API UClass* StaticClass<class AWheeledVehiclePawn>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ChaosVehiclesPlugin_Source_ChaosVehicles_Public_WheeledVehiclePawn_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
