// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GIZMOEDMODE_GizmoEdMode_generated_h
#error "GizmoEdMode.generated.h already included, missing '#pragma once' in GizmoEdMode.h"
#endif
#define GIZMOEDMODE_GizmoEdMode_generated_h

#define Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_20_SPARSE_DATA
#define Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_20_RPC_WRAPPERS
#define Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_20_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGizmoEdModeSettings(); \
	friend struct Z_Construct_UClass_UGizmoEdModeSettings_Statics; \
public: \
	DECLARE_CLASS(UGizmoEdModeSettings, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GizmoEdMode"), NO_API) \
	DECLARE_SERIALIZER(UGizmoEdModeSettings)


#define Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_20_INCLASS \
private: \
	static void StaticRegisterNativesUGizmoEdModeSettings(); \
	friend struct Z_Construct_UClass_UGizmoEdModeSettings_Statics; \
public: \
	DECLARE_CLASS(UGizmoEdModeSettings, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GizmoEdMode"), NO_API) \
	DECLARE_SERIALIZER(UGizmoEdModeSettings)


#define Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGizmoEdModeSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGizmoEdModeSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGizmoEdModeSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGizmoEdModeSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGizmoEdModeSettings(UGizmoEdModeSettings&&); \
	NO_API UGizmoEdModeSettings(const UGizmoEdModeSettings&); \
public:


#define Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_20_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGizmoEdModeSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGizmoEdModeSettings(UGizmoEdModeSettings&&); \
	NO_API UGizmoEdModeSettings(const UGizmoEdModeSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGizmoEdModeSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGizmoEdModeSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGizmoEdModeSettings)


#define Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_20_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_17_PROLOG
#define Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_20_SPARSE_DATA \
	Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_20_RPC_WRAPPERS \
	Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_20_INCLASS \
	Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_20_SPARSE_DATA \
	Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_20_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GIZMOEDMODE_API UClass* StaticClass<class UGizmoEdModeSettings>();

#define Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_26_SPARSE_DATA
#define Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_26_RPC_WRAPPERS
#define Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_26_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_26_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGizmoEdMode(); \
	friend struct Z_Construct_UClass_UGizmoEdMode_Statics; \
public: \
	DECLARE_CLASS(UGizmoEdMode, UEdMode, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GizmoEdMode"), NO_API) \
	DECLARE_SERIALIZER(UGizmoEdMode)


#define Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_26_INCLASS \
private: \
	static void StaticRegisterNativesUGizmoEdMode(); \
	friend struct Z_Construct_UClass_UGizmoEdMode_Statics; \
public: \
	DECLARE_CLASS(UGizmoEdMode, UEdMode, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GizmoEdMode"), NO_API) \
	DECLARE_SERIALIZER(UGizmoEdMode)


#define Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_26_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGizmoEdMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGizmoEdMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGizmoEdMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGizmoEdMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGizmoEdMode(UGizmoEdMode&&); \
	NO_API UGizmoEdMode(const UGizmoEdMode&); \
public:


#define Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_26_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGizmoEdMode(UGizmoEdMode&&); \
	NO_API UGizmoEdMode(const UGizmoEdMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGizmoEdMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGizmoEdMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UGizmoEdMode)


#define Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_26_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__GizmoFactories() { return STRUCT_OFFSET(UGizmoEdMode, GizmoFactories); } \
	FORCEINLINE static uint32 __PPO__InteractiveGizmos() { return STRUCT_OFFSET(UGizmoEdMode, InteractiveGizmos); }


#define Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_23_PROLOG
#define Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_26_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_26_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_26_SPARSE_DATA \
	Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_26_RPC_WRAPPERS \
	Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_26_INCLASS \
	Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_26_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_26_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_26_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_26_SPARSE_DATA \
	Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_26_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_26_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h_26_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GIZMOEDMODE_API UClass* StaticClass<class UGizmoEdMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_GizmoEdMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
