// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GizmoEdMode/Public/AssetEditorGizmoFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAssetEditorGizmoFactory() {}
// Cross Module References
	GIZMOEDMODE_API UEnum* Z_Construct_UEnum_GizmoEdMode_EAssetEditorGizmoFactoryPriority();
	UPackage* Z_Construct_UPackage__Script_GizmoEdMode();
	GIZMOEDMODE_API UClass* Z_Construct_UClass_UAssetEditorGizmoFactory_NoRegister();
	GIZMOEDMODE_API UClass* Z_Construct_UClass_UAssetEditorGizmoFactory();
	COREUOBJECT_API UClass* Z_Construct_UClass_UInterface();
// End Cross Module References
	static UEnum* EAssetEditorGizmoFactoryPriority_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_GizmoEdMode_EAssetEditorGizmoFactoryPriority, Z_Construct_UPackage__Script_GizmoEdMode(), TEXT("EAssetEditorGizmoFactoryPriority"));
		}
		return Singleton;
	}
	template<> GIZMOEDMODE_API UEnum* StaticEnum<EAssetEditorGizmoFactoryPriority>()
	{
		return EAssetEditorGizmoFactoryPriority_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EAssetEditorGizmoFactoryPriority(EAssetEditorGizmoFactoryPriority_StaticEnum, TEXT("/Script/GizmoEdMode"), TEXT("EAssetEditorGizmoFactoryPriority"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_GizmoEdMode_EAssetEditorGizmoFactoryPriority_Hash() { return 543575401U; }
	UEnum* Z_Construct_UEnum_GizmoEdMode_EAssetEditorGizmoFactoryPriority()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_GizmoEdMode();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EAssetEditorGizmoFactoryPriority"), 0, Get_Z_Construct_UEnum_GizmoEdMode_EAssetEditorGizmoFactoryPriority_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EAssetEditorGizmoFactoryPriority::Default", (int64)EAssetEditorGizmoFactoryPriority::Default },
				{ "EAssetEditorGizmoFactoryPriority::Normal", (int64)EAssetEditorGizmoFactoryPriority::Normal },
				{ "EAssetEditorGizmoFactoryPriority::High", (int64)EAssetEditorGizmoFactoryPriority::High },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Default.Name", "EAssetEditorGizmoFactoryPriority::Default" },
				{ "High.Name", "EAssetEditorGizmoFactoryPriority::High" },
				{ "ModuleRelativePath", "Public/AssetEditorGizmoFactory.h" },
				{ "Normal.Name", "EAssetEditorGizmoFactoryPriority::Normal" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_GizmoEdMode,
				nullptr,
				"EAssetEditorGizmoFactoryPriority",
				"EAssetEditorGizmoFactoryPriority",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UAssetEditorGizmoFactory::StaticRegisterNativesUAssetEditorGizmoFactory()
	{
	}
	UClass* Z_Construct_UClass_UAssetEditorGizmoFactory_NoRegister()
	{
		return UAssetEditorGizmoFactory::StaticClass();
	}
	struct Z_Construct_UClass_UAssetEditorGizmoFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAssetEditorGizmoFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInterface,
		(UObject* (*)())Z_Construct_UPackage__Script_GizmoEdMode,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAssetEditorGizmoFactory_Statics::Class_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/AssetEditorGizmoFactory.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAssetEditorGizmoFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<IAssetEditorGizmoFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAssetEditorGizmoFactory_Statics::ClassParams = {
		&UAssetEditorGizmoFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001040A1u,
		METADATA_PARAMS(Z_Construct_UClass_UAssetEditorGizmoFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAssetEditorGizmoFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAssetEditorGizmoFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAssetEditorGizmoFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAssetEditorGizmoFactory, 332028530);
	template<> GIZMOEDMODE_API UClass* StaticClass<UAssetEditorGizmoFactory>()
	{
		return UAssetEditorGizmoFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAssetEditorGizmoFactory(Z_Construct_UClass_UAssetEditorGizmoFactory, &UAssetEditorGizmoFactory::StaticClass, TEXT("/Script/GizmoEdMode"), TEXT("UAssetEditorGizmoFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAssetEditorGizmoFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
