// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GizmoEdMode/Public/DefaultAssetEditorGizmoFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDefaultAssetEditorGizmoFactory() {}
// Cross Module References
	GIZMOEDMODE_API UClass* Z_Construct_UClass_UDefaultAssetEditorGizmoFactory_NoRegister();
	GIZMOEDMODE_API UClass* Z_Construct_UClass_UDefaultAssetEditorGizmoFactory();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_GizmoEdMode();
	GIZMOEDMODE_API UClass* Z_Construct_UClass_UAssetEditorGizmoFactory_NoRegister();
// End Cross Module References
	void UDefaultAssetEditorGizmoFactory::StaticRegisterNativesUDefaultAssetEditorGizmoFactory()
	{
	}
	UClass* Z_Construct_UClass_UDefaultAssetEditorGizmoFactory_NoRegister()
	{
		return UDefaultAssetEditorGizmoFactory::StaticClass();
	}
	struct Z_Construct_UClass_UDefaultAssetEditorGizmoFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDefaultAssetEditorGizmoFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_GizmoEdMode,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDefaultAssetEditorGizmoFactory_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "DefaultAssetEditorGizmoFactory.h" },
		{ "ModuleRelativePath", "Public/DefaultAssetEditorGizmoFactory.h" },
	};
#endif
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_UDefaultAssetEditorGizmoFactory_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UAssetEditorGizmoFactory_NoRegister, (int32)VTABLE_OFFSET(UDefaultAssetEditorGizmoFactory, IAssetEditorGizmoFactory), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDefaultAssetEditorGizmoFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDefaultAssetEditorGizmoFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDefaultAssetEditorGizmoFactory_Statics::ClassParams = {
		&UDefaultAssetEditorGizmoFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		UE_ARRAY_COUNT(InterfaceParams),
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDefaultAssetEditorGizmoFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDefaultAssetEditorGizmoFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDefaultAssetEditorGizmoFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDefaultAssetEditorGizmoFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDefaultAssetEditorGizmoFactory, 1576245801);
	template<> GIZMOEDMODE_API UClass* StaticClass<UDefaultAssetEditorGizmoFactory>()
	{
		return UDefaultAssetEditorGizmoFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDefaultAssetEditorGizmoFactory(Z_Construct_UClass_UDefaultAssetEditorGizmoFactory, &UDefaultAssetEditorGizmoFactory::StaticClass, TEXT("/Script/GizmoEdMode"), TEXT("UDefaultAssetEditorGizmoFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDefaultAssetEditorGizmoFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
