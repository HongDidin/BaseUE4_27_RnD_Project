// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GIZMOEDMODE_DefaultAssetEditorGizmoFactory_generated_h
#error "DefaultAssetEditorGizmoFactory.generated.h already included, missing '#pragma once' in DefaultAssetEditorGizmoFactory.h"
#endif
#define GIZMOEDMODE_DefaultAssetEditorGizmoFactory_generated_h

#define Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_DefaultAssetEditorGizmoFactory_h_12_SPARSE_DATA
#define Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_DefaultAssetEditorGizmoFactory_h_12_RPC_WRAPPERS
#define Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_DefaultAssetEditorGizmoFactory_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_DefaultAssetEditorGizmoFactory_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDefaultAssetEditorGizmoFactory(); \
	friend struct Z_Construct_UClass_UDefaultAssetEditorGizmoFactory_Statics; \
public: \
	DECLARE_CLASS(UDefaultAssetEditorGizmoFactory, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GizmoEdMode"), NO_API) \
	DECLARE_SERIALIZER(UDefaultAssetEditorGizmoFactory) \
	virtual UObject* _getUObject() const override { return const_cast<UDefaultAssetEditorGizmoFactory*>(this); }


#define Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_DefaultAssetEditorGizmoFactory_h_12_INCLASS \
private: \
	static void StaticRegisterNativesUDefaultAssetEditorGizmoFactory(); \
	friend struct Z_Construct_UClass_UDefaultAssetEditorGizmoFactory_Statics; \
public: \
	DECLARE_CLASS(UDefaultAssetEditorGizmoFactory, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GizmoEdMode"), NO_API) \
	DECLARE_SERIALIZER(UDefaultAssetEditorGizmoFactory) \
	virtual UObject* _getUObject() const override { return const_cast<UDefaultAssetEditorGizmoFactory*>(this); }


#define Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_DefaultAssetEditorGizmoFactory_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDefaultAssetEditorGizmoFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDefaultAssetEditorGizmoFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDefaultAssetEditorGizmoFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDefaultAssetEditorGizmoFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDefaultAssetEditorGizmoFactory(UDefaultAssetEditorGizmoFactory&&); \
	NO_API UDefaultAssetEditorGizmoFactory(const UDefaultAssetEditorGizmoFactory&); \
public:


#define Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_DefaultAssetEditorGizmoFactory_h_12_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDefaultAssetEditorGizmoFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDefaultAssetEditorGizmoFactory(UDefaultAssetEditorGizmoFactory&&); \
	NO_API UDefaultAssetEditorGizmoFactory(const UDefaultAssetEditorGizmoFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDefaultAssetEditorGizmoFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDefaultAssetEditorGizmoFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDefaultAssetEditorGizmoFactory)


#define Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_DefaultAssetEditorGizmoFactory_h_12_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_DefaultAssetEditorGizmoFactory_h_9_PROLOG
#define Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_DefaultAssetEditorGizmoFactory_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_DefaultAssetEditorGizmoFactory_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_DefaultAssetEditorGizmoFactory_h_12_SPARSE_DATA \
	Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_DefaultAssetEditorGizmoFactory_h_12_RPC_WRAPPERS \
	Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_DefaultAssetEditorGizmoFactory_h_12_INCLASS \
	Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_DefaultAssetEditorGizmoFactory_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_DefaultAssetEditorGizmoFactory_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_DefaultAssetEditorGizmoFactory_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_DefaultAssetEditorGizmoFactory_h_12_SPARSE_DATA \
	Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_DefaultAssetEditorGizmoFactory_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_DefaultAssetEditorGizmoFactory_h_12_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_DefaultAssetEditorGizmoFactory_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GIZMOEDMODE_API UClass* StaticClass<class UDefaultAssetEditorGizmoFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_GizmoEdMode_Source_GizmoEdMode_Public_DefaultAssetEditorGizmoFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
