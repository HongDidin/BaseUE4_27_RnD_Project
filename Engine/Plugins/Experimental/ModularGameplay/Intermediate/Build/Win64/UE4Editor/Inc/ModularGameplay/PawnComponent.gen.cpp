// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ModularGameplay/Public/Components/PawnComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePawnComponent() {}
// Cross Module References
	MODULARGAMEPLAY_API UClass* Z_Construct_UClass_UPawnComponent_NoRegister();
	MODULARGAMEPLAY_API UClass* Z_Construct_UClass_UPawnComponent();
	MODULARGAMEPLAY_API UClass* Z_Construct_UClass_UGameFrameworkComponent();
	UPackage* Z_Construct_UPackage__Script_ModularGameplay();
// End Cross Module References
	void UPawnComponent::StaticRegisterNativesUPawnComponent()
	{
	}
	UClass* Z_Construct_UClass_UPawnComponent_NoRegister()
	{
		return UPawnComponent::StaticClass();
	}
	struct Z_Construct_UClass_UPawnComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPawnComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGameFrameworkComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_ModularGameplay,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPawnComponent_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * PawnComponent is an actor component made for APawn and receives pawn events.\n */" },
		{ "HideCategories", "Trigger PhysicsVolume" },
		{ "IncludePath", "Components/PawnComponent.h" },
		{ "ModuleRelativePath", "Public/Components/PawnComponent.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "PawnComponent is an actor component made for APawn and receives pawn events." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPawnComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPawnComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPawnComponent_Statics::ClassParams = {
		&UPawnComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UPawnComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPawnComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPawnComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPawnComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPawnComponent, 1571973048);
	template<> MODULARGAMEPLAY_API UClass* StaticClass<UPawnComponent>()
	{
		return UPawnComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPawnComponent(Z_Construct_UClass_UPawnComponent, &UPawnComponent::StaticClass, TEXT("/Script/ModularGameplay"), TEXT("UPawnComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPawnComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
