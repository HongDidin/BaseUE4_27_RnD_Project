// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ModularGameplay/Public/Components/PlayerStateComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePlayerStateComponent() {}
// Cross Module References
	MODULARGAMEPLAY_API UClass* Z_Construct_UClass_UPlayerStateComponent_NoRegister();
	MODULARGAMEPLAY_API UClass* Z_Construct_UClass_UPlayerStateComponent();
	MODULARGAMEPLAY_API UClass* Z_Construct_UClass_UGameFrameworkComponent();
	UPackage* Z_Construct_UPackage__Script_ModularGameplay();
// End Cross Module References
	void UPlayerStateComponent::StaticRegisterNativesUPlayerStateComponent()
	{
	}
	UClass* Z_Construct_UClass_UPlayerStateComponent_NoRegister()
	{
		return UPlayerStateComponent::StaticClass();
	}
	struct Z_Construct_UClass_UPlayerStateComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPlayerStateComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGameFrameworkComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_ModularGameplay,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlayerStateComponent_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * PlayerStateComponent is an actor component made for APlayerState and receives PlayerState events.\n */" },
		{ "HideCategories", "Trigger PhysicsVolume" },
		{ "IncludePath", "Components/PlayerStateComponent.h" },
		{ "ModuleRelativePath", "Public/Components/PlayerStateComponent.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "PlayerStateComponent is an actor component made for APlayerState and receives PlayerState events." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPlayerStateComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPlayerStateComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPlayerStateComponent_Statics::ClassParams = {
		&UPlayerStateComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UPlayerStateComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPlayerStateComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPlayerStateComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPlayerStateComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPlayerStateComponent, 2840851463);
	template<> MODULARGAMEPLAY_API UClass* StaticClass<UPlayerStateComponent>()
	{
		return UPlayerStateComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPlayerStateComponent(Z_Construct_UClass_UPlayerStateComponent, &UPlayerStateComponent::StaticClass, TEXT("/Script/ModularGameplay"), TEXT("UPlayerStateComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPlayerStateComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
