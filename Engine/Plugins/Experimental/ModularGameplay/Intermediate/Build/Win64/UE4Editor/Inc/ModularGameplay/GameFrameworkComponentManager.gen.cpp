// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ModularGameplay/Public/Components/GameFrameworkComponentManager.h"
#include "Engine/Classes/Engine/GameInstance.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGameFrameworkComponentManager() {}
// Cross Module References
	MODULARGAMEPLAY_API UClass* Z_Construct_UClass_UGameFrameworkComponentManager_NoRegister();
	MODULARGAMEPLAY_API UClass* Z_Construct_UClass_UGameFrameworkComponentManager();
	ENGINE_API UClass* Z_Construct_UClass_UGameInstanceSubsystem();
	UPackage* Z_Construct_UPackage__Script_ModularGameplay();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UGameFrameworkComponentManager::execRemoveReceiver)
	{
		P_GET_OBJECT(AActor,Z_Param_Receiver);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->RemoveReceiver(Z_Param_Receiver);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGameFrameworkComponentManager::execAddReceiver)
	{
		P_GET_OBJECT(AActor,Z_Param_Receiver);
		P_GET_UBOOL(Z_Param_bAddOnlyInGameWorlds);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AddReceiver(Z_Param_Receiver,Z_Param_bAddOnlyInGameWorlds);
		P_NATIVE_END;
	}
	void UGameFrameworkComponentManager::StaticRegisterNativesUGameFrameworkComponentManager()
	{
		UClass* Class = UGameFrameworkComponentManager::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddReceiver", &UGameFrameworkComponentManager::execAddReceiver },
			{ "RemoveReceiver", &UGameFrameworkComponentManager::execRemoveReceiver },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UGameFrameworkComponentManager_AddReceiver_Statics
	{
		struct GameFrameworkComponentManager_eventAddReceiver_Parms
		{
			AActor* Receiver;
			bool bAddOnlyInGameWorlds;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Receiver;
		static void NewProp_bAddOnlyInGameWorlds_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAddOnlyInGameWorlds;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UGameFrameworkComponentManager_AddReceiver_Statics::NewProp_Receiver = { "Receiver", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GameFrameworkComponentManager_eventAddReceiver_Parms, Receiver), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UGameFrameworkComponentManager_AddReceiver_Statics::NewProp_bAddOnlyInGameWorlds_SetBit(void* Obj)
	{
		((GameFrameworkComponentManager_eventAddReceiver_Parms*)Obj)->bAddOnlyInGameWorlds = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGameFrameworkComponentManager_AddReceiver_Statics::NewProp_bAddOnlyInGameWorlds = { "bAddOnlyInGameWorlds", nullptr, (EPropertyFlags)0x0010040000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(GameFrameworkComponentManager_eventAddReceiver_Parms), &Z_Construct_UFunction_UGameFrameworkComponentManager_AddReceiver_Statics::NewProp_bAddOnlyInGameWorlds_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGameFrameworkComponentManager_AddReceiver_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGameFrameworkComponentManager_AddReceiver_Statics::NewProp_Receiver,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGameFrameworkComponentManager_AddReceiver_Statics::NewProp_bAddOnlyInGameWorlds,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGameFrameworkComponentManager_AddReceiver_Statics::Function_MetaDataParams[] = {
		{ "AdvancedDisplay", "1" },
		{ "Category", "Gameplay" },
		{ "Comment", "/** Adds an actor as a receiver for components. If it passes the actorclass filter on requests it will get the components. */" },
		{ "CPP_Default_bAddOnlyInGameWorlds", "true" },
		{ "DefaultToSelf", "Receiver" },
		{ "ModuleRelativePath", "Public/Components/GameFrameworkComponentManager.h" },
		{ "ToolTip", "Adds an actor as a receiver for components. If it passes the actorclass filter on requests it will get the components." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGameFrameworkComponentManager_AddReceiver_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGameFrameworkComponentManager, nullptr, "AddReceiver", nullptr, nullptr, sizeof(GameFrameworkComponentManager_eventAddReceiver_Parms), Z_Construct_UFunction_UGameFrameworkComponentManager_AddReceiver_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGameFrameworkComponentManager_AddReceiver_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020403, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGameFrameworkComponentManager_AddReceiver_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGameFrameworkComponentManager_AddReceiver_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGameFrameworkComponentManager_AddReceiver()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGameFrameworkComponentManager_AddReceiver_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGameFrameworkComponentManager_RemoveReceiver_Statics
	{
		struct GameFrameworkComponentManager_eventRemoveReceiver_Parms
		{
			AActor* Receiver;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Receiver;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UGameFrameworkComponentManager_RemoveReceiver_Statics::NewProp_Receiver = { "Receiver", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GameFrameworkComponentManager_eventRemoveReceiver_Parms, Receiver), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGameFrameworkComponentManager_RemoveReceiver_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGameFrameworkComponentManager_RemoveReceiver_Statics::NewProp_Receiver,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGameFrameworkComponentManager_RemoveReceiver_Statics::Function_MetaDataParams[] = {
		{ "Category", "Gameplay" },
		{ "Comment", "/** Removes an actor as a receiver for components. */" },
		{ "DefaultToSelf", "Receiver" },
		{ "ModuleRelativePath", "Public/Components/GameFrameworkComponentManager.h" },
		{ "ToolTip", "Removes an actor as a receiver for components." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGameFrameworkComponentManager_RemoveReceiver_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGameFrameworkComponentManager, nullptr, "RemoveReceiver", nullptr, nullptr, sizeof(GameFrameworkComponentManager_eventRemoveReceiver_Parms), Z_Construct_UFunction_UGameFrameworkComponentManager_RemoveReceiver_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGameFrameworkComponentManager_RemoveReceiver_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020403, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGameFrameworkComponentManager_RemoveReceiver_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGameFrameworkComponentManager_RemoveReceiver_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGameFrameworkComponentManager_RemoveReceiver()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGameFrameworkComponentManager_RemoveReceiver_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UGameFrameworkComponentManager_NoRegister()
	{
		return UGameFrameworkComponentManager::StaticClass();
	}
	struct Z_Construct_UClass_UGameFrameworkComponentManager_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AllReceivers_ElementProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AllReceivers_MetaData[];
#endif
		static const UE4CodeGen_Private::FSetPropertyParams NewProp_AllReceivers;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#endif // WITH_EDITORONLY_DATA
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGameFrameworkComponentManager_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGameInstanceSubsystem,
		(UObject* (*)())Z_Construct_UPackage__Script_ModularGameplay,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UGameFrameworkComponentManager_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UGameFrameworkComponentManager_AddReceiver, "AddReceiver" }, // 4172811604
		{ &Z_Construct_UFunction_UGameFrameworkComponentManager_RemoveReceiver, "RemoveReceiver" }, // 1316573186
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameFrameworkComponentManager_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** \n * GameFrameworkComponentManager\n *\n * A manager to handle putting components on actors as they come and go.\n * Put in a request to instantiate components of a given class on actors of a given class and they will automatically be made for them as the actors are spawned.\n * Actors must opt-in to this behavior by calling AddReceiver/RemoveReceiver for themselves when they are ready to receive the components and when they want to remove them.\n * Any actors that are in memory when a request is made will automatically get the components, and any in memory when a request is removed will lose the components immediately.\n * Requests are reference counted, so if multiple requests are made for the same actor class and component class, only one component will be added and that component wont be removed until all requests are removed.\n */" },
		{ "IncludePath", "Components/GameFrameworkComponentManager.h" },
		{ "ModuleRelativePath", "Public/Components/GameFrameworkComponentManager.h" },
		{ "ToolTip", "GameFrameworkComponentManager\n\nA manager to handle putting components on actors as they come and go.\nPut in a request to instantiate components of a given class on actors of a given class and they will automatically be made for them as the actors are spawned.\nActors must opt-in to this behavior by calling AddReceiver/RemoveReceiver for themselves when they are ready to receive the components and when they want to remove them.\nAny actors that are in memory when a request is made will automatically get the components, and any in memory when a request is removed will lose the components immediately.\nRequests are reference counted, so if multiple requests are made for the same actor class and component class, only one component will be added and that component wont be removed until all requests are removed." },
	};
#endif
#if WITH_EDITORONLY_DATA
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGameFrameworkComponentManager_Statics::NewProp_AllReceivers_ElementProp = { "AllReceivers", nullptr, (EPropertyFlags)0x0000000800000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameFrameworkComponentManager_Statics::NewProp_AllReceivers_MetaData[] = {
		{ "Comment", "/** Editor-only set to validate that component requests are only being added for actors that call AddReceiver and RemoveReceiver */" },
		{ "ModuleRelativePath", "Public/Components/GameFrameworkComponentManager.h" },
		{ "ToolTip", "Editor-only set to validate that component requests are only being added for actors that call AddReceiver and RemoveReceiver" },
	};
#endif
	const UE4CodeGen_Private::FSetPropertyParams Z_Construct_UClass_UGameFrameworkComponentManager_Statics::NewProp_AllReceivers = { "AllReceivers", nullptr, (EPropertyFlags)0x0040000800002000, UE4CodeGen_Private::EPropertyGenFlags::Set, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGameFrameworkComponentManager, AllReceivers), METADATA_PARAMS(Z_Construct_UClass_UGameFrameworkComponentManager_Statics::NewProp_AllReceivers_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGameFrameworkComponentManager_Statics::NewProp_AllReceivers_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGameFrameworkComponentManager_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGameFrameworkComponentManager_Statics::NewProp_AllReceivers_ElementProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGameFrameworkComponentManager_Statics::NewProp_AllReceivers,
	};
#endif // WITH_EDITORONLY_DATA
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGameFrameworkComponentManager_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGameFrameworkComponentManager>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGameFrameworkComponentManager_Statics::ClassParams = {
		&UGameFrameworkComponentManager::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		IF_WITH_EDITORONLY_DATA(Z_Construct_UClass_UGameFrameworkComponentManager_Statics::PropPointers, nullptr),
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		IF_WITH_EDITORONLY_DATA(UE_ARRAY_COUNT(Z_Construct_UClass_UGameFrameworkComponentManager_Statics::PropPointers), 0),
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGameFrameworkComponentManager_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGameFrameworkComponentManager_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGameFrameworkComponentManager()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGameFrameworkComponentManager_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGameFrameworkComponentManager, 1857470495);
	template<> MODULARGAMEPLAY_API UClass* StaticClass<UGameFrameworkComponentManager>()
	{
		return UGameFrameworkComponentManager::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGameFrameworkComponentManager(Z_Construct_UClass_UGameFrameworkComponentManager, &UGameFrameworkComponentManager::StaticClass, TEXT("/Script/ModularGameplay"), TEXT("UGameFrameworkComponentManager"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGameFrameworkComponentManager);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
