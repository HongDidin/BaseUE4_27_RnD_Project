// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MODULARGAMEPLAY_GameStateComponent_generated_h
#error "GameStateComponent.generated.h already included, missing '#pragma once' in GameStateComponent.h"
#endif
#define MODULARGAMEPLAY_GameStateComponent_generated_h

#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameStateComponent_h_16_SPARSE_DATA
#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameStateComponent_h_16_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameStateComponent_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameStateComponent_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGameStateComponent(); \
	friend struct Z_Construct_UClass_UGameStateComponent_Statics; \
public: \
	DECLARE_CLASS(UGameStateComponent, UGameFrameworkComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ModularGameplay"), NO_API) \
	DECLARE_SERIALIZER(UGameStateComponent)


#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameStateComponent_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUGameStateComponent(); \
	friend struct Z_Construct_UClass_UGameStateComponent_Statics; \
public: \
	DECLARE_CLASS(UGameStateComponent, UGameFrameworkComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ModularGameplay"), NO_API) \
	DECLARE_SERIALIZER(UGameStateComponent)


#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameStateComponent_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGameStateComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGameStateComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGameStateComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGameStateComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGameStateComponent(UGameStateComponent&&); \
	NO_API UGameStateComponent(const UGameStateComponent&); \
public:


#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameStateComponent_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGameStateComponent(UGameStateComponent&&); \
	NO_API UGameStateComponent(const UGameStateComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGameStateComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGameStateComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGameStateComponent)


#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameStateComponent_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameStateComponent_h_13_PROLOG
#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameStateComponent_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameStateComponent_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameStateComponent_h_16_SPARSE_DATA \
	Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameStateComponent_h_16_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameStateComponent_h_16_INCLASS \
	Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameStateComponent_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameStateComponent_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameStateComponent_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameStateComponent_h_16_SPARSE_DATA \
	Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameStateComponent_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameStateComponent_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameStateComponent_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MODULARGAMEPLAY_API UClass* StaticClass<class UGameStateComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameStateComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
