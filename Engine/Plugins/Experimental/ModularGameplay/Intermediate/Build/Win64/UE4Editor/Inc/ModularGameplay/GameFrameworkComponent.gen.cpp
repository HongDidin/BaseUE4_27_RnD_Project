// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ModularGameplay/Public/Components/GameFrameworkComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGameFrameworkComponent() {}
// Cross Module References
	MODULARGAMEPLAY_API UClass* Z_Construct_UClass_UGameFrameworkComponent_NoRegister();
	MODULARGAMEPLAY_API UClass* Z_Construct_UClass_UGameFrameworkComponent();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	UPackage* Z_Construct_UPackage__Script_ModularGameplay();
// End Cross Module References
	void UGameFrameworkComponent::StaticRegisterNativesUGameFrameworkComponent()
	{
	}
	UClass* Z_Construct_UClass_UGameFrameworkComponent_NoRegister()
	{
		return UGameFrameworkComponent::StaticClass();
	}
	struct Z_Construct_UClass_UGameFrameworkComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGameFrameworkComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_ModularGameplay,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameFrameworkComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * GameFrameworkComponent is a base class for a actor components made for the basic game framework classes.\n */" },
		{ "HideCategories", "Trigger PhysicsVolume" },
		{ "IncludePath", "Components/GameFrameworkComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Components/GameFrameworkComponent.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "GameFrameworkComponent is a base class for a actor components made for the basic game framework classes." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGameFrameworkComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGameFrameworkComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGameFrameworkComponent_Statics::ClassParams = {
		&UGameFrameworkComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UGameFrameworkComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGameFrameworkComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGameFrameworkComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGameFrameworkComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGameFrameworkComponent, 356993762);
	template<> MODULARGAMEPLAY_API UClass* StaticClass<UGameFrameworkComponent>()
	{
		return UGameFrameworkComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGameFrameworkComponent(Z_Construct_UClass_UGameFrameworkComponent, &UGameFrameworkComponent::StaticClass, TEXT("/Script/ModularGameplay"), TEXT("UGameFrameworkComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGameFrameworkComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
