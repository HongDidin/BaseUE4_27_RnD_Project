// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
#ifdef MODULARGAMEPLAY_GameFrameworkComponentManager_generated_h
#error "GameFrameworkComponentManager.generated.h already included, missing '#pragma once' in GameFrameworkComponentManager.h"
#endif
#define MODULARGAMEPLAY_GameFrameworkComponentManager_generated_h

#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameFrameworkComponentManager_h_56_SPARSE_DATA
#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameFrameworkComponentManager_h_56_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execRemoveReceiver); \
	DECLARE_FUNCTION(execAddReceiver);


#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameFrameworkComponentManager_h_56_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execRemoveReceiver); \
	DECLARE_FUNCTION(execAddReceiver);


#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameFrameworkComponentManager_h_56_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGameFrameworkComponentManager(); \
	friend struct Z_Construct_UClass_UGameFrameworkComponentManager_Statics; \
public: \
	DECLARE_CLASS(UGameFrameworkComponentManager, UGameInstanceSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ModularGameplay"), MODULARGAMEPLAY_API) \
	DECLARE_SERIALIZER(UGameFrameworkComponentManager)


#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameFrameworkComponentManager_h_56_INCLASS \
private: \
	static void StaticRegisterNativesUGameFrameworkComponentManager(); \
	friend struct Z_Construct_UClass_UGameFrameworkComponentManager_Statics; \
public: \
	DECLARE_CLASS(UGameFrameworkComponentManager, UGameInstanceSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ModularGameplay"), MODULARGAMEPLAY_API) \
	DECLARE_SERIALIZER(UGameFrameworkComponentManager)


#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameFrameworkComponentManager_h_56_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	MODULARGAMEPLAY_API UGameFrameworkComponentManager(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGameFrameworkComponentManager) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(MODULARGAMEPLAY_API, UGameFrameworkComponentManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGameFrameworkComponentManager); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	MODULARGAMEPLAY_API UGameFrameworkComponentManager(UGameFrameworkComponentManager&&); \
	MODULARGAMEPLAY_API UGameFrameworkComponentManager(const UGameFrameworkComponentManager&); \
public:


#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameFrameworkComponentManager_h_56_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	MODULARGAMEPLAY_API UGameFrameworkComponentManager() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	MODULARGAMEPLAY_API UGameFrameworkComponentManager(UGameFrameworkComponentManager&&); \
	MODULARGAMEPLAY_API UGameFrameworkComponentManager(const UGameFrameworkComponentManager&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(MODULARGAMEPLAY_API, UGameFrameworkComponentManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGameFrameworkComponentManager); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UGameFrameworkComponentManager)


#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameFrameworkComponentManager_h_56_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameFrameworkComponentManager_h_53_PROLOG
#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameFrameworkComponentManager_h_56_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameFrameworkComponentManager_h_56_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameFrameworkComponentManager_h_56_SPARSE_DATA \
	Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameFrameworkComponentManager_h_56_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameFrameworkComponentManager_h_56_INCLASS \
	Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameFrameworkComponentManager_h_56_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameFrameworkComponentManager_h_56_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameFrameworkComponentManager_h_56_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameFrameworkComponentManager_h_56_SPARSE_DATA \
	Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameFrameworkComponentManager_h_56_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameFrameworkComponentManager_h_56_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameFrameworkComponentManager_h_56_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MODULARGAMEPLAY_API UClass* StaticClass<class UGameFrameworkComponentManager>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_GameFrameworkComponentManager_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
