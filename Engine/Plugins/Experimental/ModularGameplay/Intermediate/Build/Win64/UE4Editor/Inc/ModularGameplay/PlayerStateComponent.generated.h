// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MODULARGAMEPLAY_PlayerStateComponent_generated_h
#error "PlayerStateComponent.generated.h already included, missing '#pragma once' in PlayerStateComponent.h"
#endif
#define MODULARGAMEPLAY_PlayerStateComponent_generated_h

#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PlayerStateComponent_h_15_SPARSE_DATA
#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PlayerStateComponent_h_15_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PlayerStateComponent_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PlayerStateComponent_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPlayerStateComponent(); \
	friend struct Z_Construct_UClass_UPlayerStateComponent_Statics; \
public: \
	DECLARE_CLASS(UPlayerStateComponent, UGameFrameworkComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ModularGameplay"), NO_API) \
	DECLARE_SERIALIZER(UPlayerStateComponent)


#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PlayerStateComponent_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUPlayerStateComponent(); \
	friend struct Z_Construct_UClass_UPlayerStateComponent_Statics; \
public: \
	DECLARE_CLASS(UPlayerStateComponent, UGameFrameworkComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ModularGameplay"), NO_API) \
	DECLARE_SERIALIZER(UPlayerStateComponent)


#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PlayerStateComponent_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPlayerStateComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPlayerStateComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPlayerStateComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPlayerStateComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPlayerStateComponent(UPlayerStateComponent&&); \
	NO_API UPlayerStateComponent(const UPlayerStateComponent&); \
public:


#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PlayerStateComponent_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPlayerStateComponent(UPlayerStateComponent&&); \
	NO_API UPlayerStateComponent(const UPlayerStateComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPlayerStateComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPlayerStateComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPlayerStateComponent)


#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PlayerStateComponent_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PlayerStateComponent_h_12_PROLOG
#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PlayerStateComponent_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PlayerStateComponent_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PlayerStateComponent_h_15_SPARSE_DATA \
	Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PlayerStateComponent_h_15_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PlayerStateComponent_h_15_INCLASS \
	Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PlayerStateComponent_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PlayerStateComponent_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PlayerStateComponent_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PlayerStateComponent_h_15_SPARSE_DATA \
	Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PlayerStateComponent_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PlayerStateComponent_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PlayerStateComponent_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MODULARGAMEPLAY_API UClass* StaticClass<class UPlayerStateComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PlayerStateComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
