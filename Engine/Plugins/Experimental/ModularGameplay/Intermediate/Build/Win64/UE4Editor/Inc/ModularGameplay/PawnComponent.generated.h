// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MODULARGAMEPLAY_PawnComponent_generated_h
#error "PawnComponent.generated.h already included, missing '#pragma once' in PawnComponent.h"
#endif
#define MODULARGAMEPLAY_PawnComponent_generated_h

#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PawnComponent_h_15_SPARSE_DATA
#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PawnComponent_h_15_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PawnComponent_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PawnComponent_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPawnComponent(); \
	friend struct Z_Construct_UClass_UPawnComponent_Statics; \
public: \
	DECLARE_CLASS(UPawnComponent, UGameFrameworkComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ModularGameplay"), NO_API) \
	DECLARE_SERIALIZER(UPawnComponent)


#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PawnComponent_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUPawnComponent(); \
	friend struct Z_Construct_UClass_UPawnComponent_Statics; \
public: \
	DECLARE_CLASS(UPawnComponent, UGameFrameworkComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ModularGameplay"), NO_API) \
	DECLARE_SERIALIZER(UPawnComponent)


#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PawnComponent_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPawnComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPawnComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPawnComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPawnComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPawnComponent(UPawnComponent&&); \
	NO_API UPawnComponent(const UPawnComponent&); \
public:


#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PawnComponent_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPawnComponent(UPawnComponent&&); \
	NO_API UPawnComponent(const UPawnComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPawnComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPawnComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPawnComponent)


#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PawnComponent_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PawnComponent_h_12_PROLOG
#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PawnComponent_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PawnComponent_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PawnComponent_h_15_SPARSE_DATA \
	Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PawnComponent_h_15_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PawnComponent_h_15_INCLASS \
	Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PawnComponent_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PawnComponent_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PawnComponent_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PawnComponent_h_15_SPARSE_DATA \
	Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PawnComponent_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PawnComponent_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PawnComponent_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MODULARGAMEPLAY_API UClass* StaticClass<class UPawnComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ModularGameplay_Source_ModularGameplay_Public_Components_PawnComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
