// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ModularGameplay/Public/Components/ControllerComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeControllerComponent() {}
// Cross Module References
	MODULARGAMEPLAY_API UClass* Z_Construct_UClass_UControllerComponent_NoRegister();
	MODULARGAMEPLAY_API UClass* Z_Construct_UClass_UControllerComponent();
	MODULARGAMEPLAY_API UClass* Z_Construct_UClass_UGameFrameworkComponent();
	UPackage* Z_Construct_UPackage__Script_ModularGameplay();
// End Cross Module References
	void UControllerComponent::StaticRegisterNativesUControllerComponent()
	{
	}
	UClass* Z_Construct_UClass_UControllerComponent_NoRegister()
	{
		return UControllerComponent::StaticClass();
	}
	struct Z_Construct_UClass_UControllerComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UControllerComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGameFrameworkComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_ModularGameplay,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControllerComponent_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * ControllerComponent is an actor component made for AController and receives controller events.\n */" },
		{ "HideCategories", "Trigger PhysicsVolume" },
		{ "IncludePath", "Components/ControllerComponent.h" },
		{ "ModuleRelativePath", "Public/Components/ControllerComponent.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "ControllerComponent is an actor component made for AController and receives controller events." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UControllerComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UControllerComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UControllerComponent_Statics::ClassParams = {
		&UControllerComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UControllerComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UControllerComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UControllerComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UControllerComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UControllerComponent, 3215036534);
	template<> MODULARGAMEPLAY_API UClass* StaticClass<UControllerComponent>()
	{
		return UControllerComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UControllerComponent(Z_Construct_UClass_UControllerComponent, &UControllerComponent::StaticClass, TEXT("/Script/ModularGameplay"), TEXT("UControllerComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UControllerComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
