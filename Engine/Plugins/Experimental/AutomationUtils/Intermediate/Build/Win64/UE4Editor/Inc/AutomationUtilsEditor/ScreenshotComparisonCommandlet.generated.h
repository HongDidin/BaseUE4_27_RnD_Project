// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef AUTOMATIONUTILSEDITOR_ScreenshotComparisonCommandlet_generated_h
#error "ScreenshotComparisonCommandlet.generated.h already included, missing '#pragma once' in ScreenshotComparisonCommandlet.h"
#endif
#define AUTOMATIONUTILSEDITOR_ScreenshotComparisonCommandlet_generated_h

#define Engine_Plugins_Experimental_AutomationUtils_Source_AutomationUtilsEditor_Public_ScreenshotComparisonCommandlet_h_14_SPARSE_DATA
#define Engine_Plugins_Experimental_AutomationUtils_Source_AutomationUtilsEditor_Public_ScreenshotComparisonCommandlet_h_14_RPC_WRAPPERS
#define Engine_Plugins_Experimental_AutomationUtils_Source_AutomationUtilsEditor_Public_ScreenshotComparisonCommandlet_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_AutomationUtils_Source_AutomationUtilsEditor_Public_ScreenshotComparisonCommandlet_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUScreenshotComparisonCommandlet(); \
	friend struct Z_Construct_UClass_UScreenshotComparisonCommandlet_Statics; \
public: \
	DECLARE_CLASS(UScreenshotComparisonCommandlet, UCommandlet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/AutomationUtilsEditor"), NO_API) \
	DECLARE_SERIALIZER(UScreenshotComparisonCommandlet) \
	static const TCHAR* StaticConfigName() {return TEXT("Editor");} \



#define Engine_Plugins_Experimental_AutomationUtils_Source_AutomationUtilsEditor_Public_ScreenshotComparisonCommandlet_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUScreenshotComparisonCommandlet(); \
	friend struct Z_Construct_UClass_UScreenshotComparisonCommandlet_Statics; \
public: \
	DECLARE_CLASS(UScreenshotComparisonCommandlet, UCommandlet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/AutomationUtilsEditor"), NO_API) \
	DECLARE_SERIALIZER(UScreenshotComparisonCommandlet) \
	static const TCHAR* StaticConfigName() {return TEXT("Editor");} \



#define Engine_Plugins_Experimental_AutomationUtils_Source_AutomationUtilsEditor_Public_ScreenshotComparisonCommandlet_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UScreenshotComparisonCommandlet(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UScreenshotComparisonCommandlet) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UScreenshotComparisonCommandlet); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UScreenshotComparisonCommandlet); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UScreenshotComparisonCommandlet(UScreenshotComparisonCommandlet&&); \
	NO_API UScreenshotComparisonCommandlet(const UScreenshotComparisonCommandlet&); \
public:


#define Engine_Plugins_Experimental_AutomationUtils_Source_AutomationUtilsEditor_Public_ScreenshotComparisonCommandlet_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UScreenshotComparisonCommandlet(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UScreenshotComparisonCommandlet(UScreenshotComparisonCommandlet&&); \
	NO_API UScreenshotComparisonCommandlet(const UScreenshotComparisonCommandlet&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UScreenshotComparisonCommandlet); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UScreenshotComparisonCommandlet); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UScreenshotComparisonCommandlet)


#define Engine_Plugins_Experimental_AutomationUtils_Source_AutomationUtilsEditor_Public_ScreenshotComparisonCommandlet_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_AutomationUtils_Source_AutomationUtilsEditor_Public_ScreenshotComparisonCommandlet_h_11_PROLOG
#define Engine_Plugins_Experimental_AutomationUtils_Source_AutomationUtilsEditor_Public_ScreenshotComparisonCommandlet_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_AutomationUtils_Source_AutomationUtilsEditor_Public_ScreenshotComparisonCommandlet_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_AutomationUtils_Source_AutomationUtilsEditor_Public_ScreenshotComparisonCommandlet_h_14_SPARSE_DATA \
	Engine_Plugins_Experimental_AutomationUtils_Source_AutomationUtilsEditor_Public_ScreenshotComparisonCommandlet_h_14_RPC_WRAPPERS \
	Engine_Plugins_Experimental_AutomationUtils_Source_AutomationUtilsEditor_Public_ScreenshotComparisonCommandlet_h_14_INCLASS \
	Engine_Plugins_Experimental_AutomationUtils_Source_AutomationUtilsEditor_Public_ScreenshotComparisonCommandlet_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_AutomationUtils_Source_AutomationUtilsEditor_Public_ScreenshotComparisonCommandlet_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_AutomationUtils_Source_AutomationUtilsEditor_Public_ScreenshotComparisonCommandlet_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_AutomationUtils_Source_AutomationUtilsEditor_Public_ScreenshotComparisonCommandlet_h_14_SPARSE_DATA \
	Engine_Plugins_Experimental_AutomationUtils_Source_AutomationUtilsEditor_Public_ScreenshotComparisonCommandlet_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_AutomationUtils_Source_AutomationUtilsEditor_Public_ScreenshotComparisonCommandlet_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_AutomationUtils_Source_AutomationUtilsEditor_Public_ScreenshotComparisonCommandlet_h_14_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ScreenshotComparisonCommandlet."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AUTOMATIONUTILSEDITOR_API UClass* StaticClass<class UScreenshotComparisonCommandlet>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_AutomationUtils_Source_AutomationUtilsEditor_Public_ScreenshotComparisonCommandlet_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
