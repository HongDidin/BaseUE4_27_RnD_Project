// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "InputEditor/Public/InputEditorModule.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeInputEditorModule() {}
// Cross Module References
	INPUTEDITOR_API UClass* Z_Construct_UClass_UInputMappingContext_Factory_NoRegister();
	INPUTEDITOR_API UClass* Z_Construct_UClass_UInputMappingContext_Factory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_InputEditor();
	INPUTEDITOR_API UClass* Z_Construct_UClass_UInputAction_Factory_NoRegister();
	INPUTEDITOR_API UClass* Z_Construct_UClass_UInputAction_Factory();
// End Cross Module References
	void UInputMappingContext_Factory::StaticRegisterNativesUInputMappingContext_Factory()
	{
	}
	UClass* Z_Construct_UClass_UInputMappingContext_Factory_NoRegister()
	{
		return UInputMappingContext_Factory::StaticClass();
	}
	struct Z_Construct_UClass_UInputMappingContext_Factory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UInputMappingContext_Factory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_InputEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputMappingContext_Factory_Statics::Class_MetaDataParams[] = {
		{ "Comment", "// Asset factories\n" },
		{ "IncludePath", "InputEditorModule.h" },
		{ "ModuleRelativePath", "Public/InputEditorModule.h" },
		{ "ToolTip", "Asset factories" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UInputMappingContext_Factory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UInputMappingContext_Factory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UInputMappingContext_Factory_Statics::ClassParams = {
		&UInputMappingContext_Factory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UInputMappingContext_Factory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UInputMappingContext_Factory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UInputMappingContext_Factory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UInputMappingContext_Factory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UInputMappingContext_Factory, 899406479);
	template<> INPUTEDITOR_API UClass* StaticClass<UInputMappingContext_Factory>()
	{
		return UInputMappingContext_Factory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UInputMappingContext_Factory(Z_Construct_UClass_UInputMappingContext_Factory, &UInputMappingContext_Factory::StaticClass, TEXT("/Script/InputEditor"), TEXT("UInputMappingContext_Factory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UInputMappingContext_Factory);
	void UInputAction_Factory::StaticRegisterNativesUInputAction_Factory()
	{
	}
	UClass* Z_Construct_UClass_UInputAction_Factory_NoRegister()
	{
		return UInputAction_Factory::StaticClass();
	}
	struct Z_Construct_UClass_UInputAction_Factory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UInputAction_Factory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_InputEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputAction_Factory_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "InputEditorModule.h" },
		{ "ModuleRelativePath", "Public/InputEditorModule.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UInputAction_Factory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UInputAction_Factory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UInputAction_Factory_Statics::ClassParams = {
		&UInputAction_Factory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UInputAction_Factory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UInputAction_Factory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UInputAction_Factory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UInputAction_Factory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UInputAction_Factory, 2635125839);
	template<> INPUTEDITOR_API UClass* StaticClass<UInputAction_Factory>()
	{
		return UInputAction_Factory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UInputAction_Factory(Z_Construct_UClass_UInputAction_Factory, &UInputAction_Factory::StaticClass, TEXT("/Script/InputEditor"), TEXT("UInputAction_Factory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UInputAction_Factory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
