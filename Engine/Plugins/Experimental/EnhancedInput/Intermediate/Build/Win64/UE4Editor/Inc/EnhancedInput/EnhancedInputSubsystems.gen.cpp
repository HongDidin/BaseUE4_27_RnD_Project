// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "EnhancedInput/Public/EnhancedInputSubsystems.h"
#include "Engine/Classes/Engine/LocalPlayer.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEnhancedInputSubsystems() {}
// Cross Module References
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UEnhancedInputLocalPlayerSubsystem_NoRegister();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UEnhancedInputLocalPlayerSubsystem();
	ENGINE_API UClass* Z_Construct_UClass_ULocalPlayerSubsystem();
	UPackage* Z_Construct_UPackage__Script_EnhancedInput();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UEnhancedInputSubsystemInterface_NoRegister();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UEnhancedInputEngineSubsystem_NoRegister();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UEnhancedInputEngineSubsystem();
	ENGINE_API UClass* Z_Construct_UClass_UEngineSubsystem();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UEnhancedPlayerInput_NoRegister();
// End Cross Module References
	void UEnhancedInputLocalPlayerSubsystem::StaticRegisterNativesUEnhancedInputLocalPlayerSubsystem()
	{
	}
	UClass* Z_Construct_UClass_UEnhancedInputLocalPlayerSubsystem_NoRegister()
	{
		return UEnhancedInputLocalPlayerSubsystem::StaticClass();
	}
	struct Z_Construct_UClass_UEnhancedInputLocalPlayerSubsystem_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEnhancedInputLocalPlayerSubsystem_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ULocalPlayerSubsystem,
		(UObject* (*)())Z_Construct_UPackage__Script_EnhancedInput,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEnhancedInputLocalPlayerSubsystem_Statics::Class_MetaDataParams[] = {
		{ "Comment", "// Per local player input subsystem\n" },
		{ "IncludePath", "EnhancedInputSubsystems.h" },
		{ "ModuleRelativePath", "Public/EnhancedInputSubsystems.h" },
		{ "ToolTip", "Per local player input subsystem" },
	};
#endif
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_UEnhancedInputLocalPlayerSubsystem_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UEnhancedInputSubsystemInterface_NoRegister, (int32)VTABLE_OFFSET(UEnhancedInputLocalPlayerSubsystem, IEnhancedInputSubsystemInterface), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEnhancedInputLocalPlayerSubsystem_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEnhancedInputLocalPlayerSubsystem>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEnhancedInputLocalPlayerSubsystem_Statics::ClassParams = {
		&UEnhancedInputLocalPlayerSubsystem::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		UE_ARRAY_COUNT(InterfaceParams),
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UEnhancedInputLocalPlayerSubsystem_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEnhancedInputLocalPlayerSubsystem_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEnhancedInputLocalPlayerSubsystem()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEnhancedInputLocalPlayerSubsystem_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEnhancedInputLocalPlayerSubsystem, 766846568);
	template<> ENHANCEDINPUT_API UClass* StaticClass<UEnhancedInputLocalPlayerSubsystem>()
	{
		return UEnhancedInputLocalPlayerSubsystem::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEnhancedInputLocalPlayerSubsystem(Z_Construct_UClass_UEnhancedInputLocalPlayerSubsystem, &UEnhancedInputLocalPlayerSubsystem::StaticClass, TEXT("/Script/EnhancedInput"), TEXT("UEnhancedInputLocalPlayerSubsystem"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEnhancedInputLocalPlayerSubsystem);
	void UEnhancedInputEngineSubsystem::StaticRegisterNativesUEnhancedInputEngineSubsystem()
	{
	}
	UClass* Z_Construct_UClass_UEnhancedInputEngineSubsystem_NoRegister()
	{
		return UEnhancedInputEngineSubsystem::StaticClass();
	}
	struct Z_Construct_UClass_UEnhancedInputEngineSubsystem_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlayerInput_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PlayerInput;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEnhancedInputEngineSubsystem_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEngineSubsystem,
		(UObject* (*)())Z_Construct_UPackage__Script_EnhancedInput,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEnhancedInputEngineSubsystem_Statics::Class_MetaDataParams[] = {
		{ "Comment", "// Global input handling subsystem\n// TODO: For now this is a non-functional placeholder.\n" },
		{ "IncludePath", "EnhancedInputSubsystems.h" },
		{ "ModuleRelativePath", "Public/EnhancedInputSubsystems.h" },
		{ "ToolTip", "Global input handling subsystem\nTODO: For now this is a non-functional placeholder." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEnhancedInputEngineSubsystem_Statics::NewProp_PlayerInput_MetaData[] = {
		{ "ModuleRelativePath", "Public/EnhancedInputSubsystems.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UEnhancedInputEngineSubsystem_Statics::NewProp_PlayerInput = { "PlayerInput", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEnhancedInputEngineSubsystem, PlayerInput), Z_Construct_UClass_UEnhancedPlayerInput_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UEnhancedInputEngineSubsystem_Statics::NewProp_PlayerInput_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEnhancedInputEngineSubsystem_Statics::NewProp_PlayerInput_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UEnhancedInputEngineSubsystem_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEnhancedInputEngineSubsystem_Statics::NewProp_PlayerInput,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_UEnhancedInputEngineSubsystem_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UEnhancedInputSubsystemInterface_NoRegister, (int32)VTABLE_OFFSET(UEnhancedInputEngineSubsystem, IEnhancedInputSubsystemInterface), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEnhancedInputEngineSubsystem_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEnhancedInputEngineSubsystem>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEnhancedInputEngineSubsystem_Statics::ClassParams = {
		&UEnhancedInputEngineSubsystem::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UEnhancedInputEngineSubsystem_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UEnhancedInputEngineSubsystem_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UEnhancedInputEngineSubsystem_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEnhancedInputEngineSubsystem_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEnhancedInputEngineSubsystem()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEnhancedInputEngineSubsystem_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEnhancedInputEngineSubsystem, 1206526175);
	template<> ENHANCEDINPUT_API UClass* StaticClass<UEnhancedInputEngineSubsystem>()
	{
		return UEnhancedInputEngineSubsystem::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEnhancedInputEngineSubsystem(Z_Construct_UClass_UEnhancedInputEngineSubsystem, &UEnhancedInputEngineSubsystem::StaticClass, TEXT("/Script/EnhancedInput"), TEXT("UEnhancedInputEngineSubsystem"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEnhancedInputEngineSubsystem);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
