// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef INPUTBLUEPRINTNODES_K2Node_InputDebugKey_generated_h
#error "K2Node_InputDebugKey.generated.h already included, missing '#pragma once' in K2Node_InputDebugKey.h"
#endif
#define INPUTBLUEPRINTNODES_K2Node_InputDebugKey_generated_h

#define Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKey_h_23_SPARSE_DATA
#define Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKey_h_23_RPC_WRAPPERS
#define Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKey_h_23_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKey_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUK2Node_InputDebugKey(); \
	friend struct Z_Construct_UClass_UK2Node_InputDebugKey_Statics; \
public: \
	DECLARE_CLASS(UK2Node_InputDebugKey, UK2Node, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/InputBlueprintNodes"), NO_API) \
	DECLARE_SERIALIZER(UK2Node_InputDebugKey) \
	virtual UObject* _getUObject() const override { return const_cast<UK2Node_InputDebugKey*>(this); }


#define Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKey_h_23_INCLASS \
private: \
	static void StaticRegisterNativesUK2Node_InputDebugKey(); \
	friend struct Z_Construct_UClass_UK2Node_InputDebugKey_Statics; \
public: \
	DECLARE_CLASS(UK2Node_InputDebugKey, UK2Node, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/InputBlueprintNodes"), NO_API) \
	DECLARE_SERIALIZER(UK2Node_InputDebugKey) \
	virtual UObject* _getUObject() const override { return const_cast<UK2Node_InputDebugKey*>(this); }


#define Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKey_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UK2Node_InputDebugKey(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UK2Node_InputDebugKey) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UK2Node_InputDebugKey); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UK2Node_InputDebugKey); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UK2Node_InputDebugKey(UK2Node_InputDebugKey&&); \
	NO_API UK2Node_InputDebugKey(const UK2Node_InputDebugKey&); \
public:


#define Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKey_h_23_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UK2Node_InputDebugKey(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UK2Node_InputDebugKey(UK2Node_InputDebugKey&&); \
	NO_API UK2Node_InputDebugKey(const UK2Node_InputDebugKey&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UK2Node_InputDebugKey); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UK2Node_InputDebugKey); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UK2Node_InputDebugKey)


#define Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKey_h_23_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKey_h_20_PROLOG
#define Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKey_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKey_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKey_h_23_SPARSE_DATA \
	Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKey_h_23_RPC_WRAPPERS \
	Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKey_h_23_INCLASS \
	Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKey_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKey_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKey_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKey_h_23_SPARSE_DATA \
	Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKey_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKey_h_23_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKey_h_23_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class K2Node_InputDebugKey."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> INPUTBLUEPRINTNODES_API UClass* StaticClass<class UK2Node_InputDebugKey>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKey_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
