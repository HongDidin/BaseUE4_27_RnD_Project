// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "EnhancedInput/Public/InputAction.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeInputAction() {}
// Cross Module References
	ENHANCEDINPUT_API UScriptStruct* Z_Construct_UScriptStruct_FInputActionInstance();
	UPackage* Z_Construct_UPackage__Script_EnhancedInput();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputAction_NoRegister();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputTrigger_NoRegister();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputModifier_NoRegister();
	ENHANCEDINPUT_API UEnum* Z_Construct_UEnum_EnhancedInput_ETriggerEvent();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputAction();
	ENGINE_API UClass* Z_Construct_UClass_UDataAsset();
	ENHANCEDINPUT_API UEnum* Z_Construct_UEnum_EnhancedInput_EInputActionValueType();
// End Cross Module References
class UScriptStruct* FInputActionInstance::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ENHANCEDINPUT_API uint32 Get_Z_Construct_UScriptStruct_FInputActionInstance_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FInputActionInstance, Z_Construct_UPackage__Script_EnhancedInput(), TEXT("InputActionInstance"), sizeof(FInputActionInstance), Get_Z_Construct_UScriptStruct_FInputActionInstance_Hash());
	}
	return Singleton;
}
template<> ENHANCEDINPUT_API UScriptStruct* StaticStruct<FInputActionInstance>()
{
	return FInputActionInstance::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FInputActionInstance(FInputActionInstance::StaticStruct, TEXT("/Script/EnhancedInput"), TEXT("InputActionInstance"), false, nullptr, nullptr);
static struct FScriptStruct_EnhancedInput_StaticRegisterNativesFInputActionInstance
{
	FScriptStruct_EnhancedInput_StaticRegisterNativesFInputActionInstance()
	{
		UScriptStruct::DeferCppStructOps<FInputActionInstance>(FName(TEXT("InputActionInstance")));
	}
} ScriptStruct_EnhancedInput_StaticRegisterNativesFInputActionInstance;
	struct Z_Construct_UScriptStruct_FInputActionInstance_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourceAction_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SourceAction;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Triggers_Inner_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Triggers_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Triggers_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Triggers;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Modifiers_Inner_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Modifiers_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Modifiers_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Modifiers;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PerInputModifiers_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PerInputModifiers_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PerInputModifiers;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FinalValueModifiers_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FinalValueModifiers_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FinalValueModifiers;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ElapsedProcessedTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ElapsedProcessedTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ElapsedTriggeredTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ElapsedTriggeredTime;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TriggerEvent_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TriggerEvent_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TriggerEvent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FInputActionInstance_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "// Run time queryable action instance\n// Generated from UInputAction templates above\n" },
		{ "ModuleRelativePath", "Public/InputAction.h" },
		{ "ToolTip", "Run time queryable action instance\nGenerated from UInputAction templates above" },
	};
#endif
	void* Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FInputActionInstance>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_SourceAction_MetaData[] = {
		{ "ModuleRelativePath", "Public/InputAction.h" },
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_SourceAction = { "SourceAction", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FInputActionInstance, SourceAction), Z_Construct_UClass_UInputAction_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_SourceAction_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_SourceAction_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_Triggers_Inner_MetaData[] = {
		{ "Category", "Config" },
		{ "Comment", "// TODO: Just hold a duplicate of the UInputAction in here?\n// TODO: Restrict blueprint access to triggers and modifiers?\n" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/InputAction.h" },
		{ "ToolTip", "TODO: Just hold a duplicate of the UInputAction in here?\nTODO: Restrict blueprint access to triggers and modifiers?" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_Triggers_Inner = { "Triggers", nullptr, (EPropertyFlags)0x0002000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UInputTrigger_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_Triggers_Inner_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_Triggers_Inner_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_Triggers_MetaData[] = {
		{ "Category", "Config" },
		{ "Comment", "// TODO: Just hold a duplicate of the UInputAction in here?\n// TODO: Restrict blueprint access to triggers and modifiers?\n" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/InputAction.h" },
		{ "ToolTip", "TODO: Just hold a duplicate of the UInputAction in here?\nTODO: Restrict blueprint access to triggers and modifiers?" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_Triggers = { "Triggers", nullptr, (EPropertyFlags)0x002008800000001c, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FInputActionInstance, Triggers), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_Triggers_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_Triggers_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_Modifiers_Inner_MetaData[] = {
		{ "Category", "Config" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/InputAction.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_Modifiers_Inner = { "Modifiers", nullptr, (EPropertyFlags)0x0002000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UInputModifier_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_Modifiers_Inner_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_Modifiers_Inner_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_Modifiers_MetaData[] = {
		{ "Category", "Config" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/InputAction.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_Modifiers = { "Modifiers", nullptr, (EPropertyFlags)0x002008800000001c, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FInputActionInstance, Modifiers), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_Modifiers_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_Modifiers_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_PerInputModifiers_Inner = { "PerInputModifiers", nullptr, (EPropertyFlags)0x0000000020000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UInputModifier_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_PerInputModifiers_MetaData[] = {
		{ "DeprecatedProperty", "" },
		{ "DeprecationMessage", "Moved to Modifiers." },
		{ "ModuleRelativePath", "Public/InputAction.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_PerInputModifiers = { "PerInputModifiers", nullptr, (EPropertyFlags)0x0020080020000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FInputActionInstance, PerInputModifiers_DEPRECATED), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_PerInputModifiers_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_PerInputModifiers_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_FinalValueModifiers_Inner = { "FinalValueModifiers", nullptr, (EPropertyFlags)0x0000000020000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UInputModifier_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_FinalValueModifiers_MetaData[] = {
		{ "DeprecatedProperty", "" },
		{ "DeprecationMessage", "Moved to Modifiers." },
		{ "ModuleRelativePath", "Public/InputAction.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_FinalValueModifiers = { "FinalValueModifiers", nullptr, (EPropertyFlags)0x0020080020000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FInputActionInstance, FinalValueModifiers_DEPRECATED), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_FinalValueModifiers_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_FinalValueModifiers_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_ElapsedProcessedTime_MetaData[] = {
		{ "Category", "Action" },
		{ "Comment", "// Total trigger processing/evaluation time (How long this action has been in event Started, Ongoing, or Triggered\n" },
		{ "ModuleRelativePath", "Public/InputAction.h" },
		{ "ToolTip", "Total trigger processing/evaluation time (How long this action has been in event Started, Ongoing, or Triggered" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_ElapsedProcessedTime = { "ElapsedProcessedTime", nullptr, (EPropertyFlags)0x0020080000000014, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FInputActionInstance, ElapsedProcessedTime), METADATA_PARAMS(Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_ElapsedProcessedTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_ElapsedProcessedTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_ElapsedTriggeredTime_MetaData[] = {
		{ "Category", "Action" },
		{ "Comment", "// Triggered time (How long this action has been in event Triggered only)\n" },
		{ "ModuleRelativePath", "Public/InputAction.h" },
		{ "ToolTip", "Triggered time (How long this action has been in event Triggered only)" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_ElapsedTriggeredTime = { "ElapsedTriggeredTime", nullptr, (EPropertyFlags)0x0020080000000014, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FInputActionInstance, ElapsedTriggeredTime), METADATA_PARAMS(Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_ElapsedTriggeredTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_ElapsedTriggeredTime_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_TriggerEvent_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_TriggerEvent_MetaData[] = {
		{ "Category", "Action" },
		{ "Comment", "// Trigger state\n" },
		{ "ModuleRelativePath", "Public/InputAction.h" },
		{ "ToolTip", "Trigger state" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_TriggerEvent = { "TriggerEvent", nullptr, (EPropertyFlags)0x0020080000000014, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FInputActionInstance, TriggerEvent), Z_Construct_UEnum_EnhancedInput_ETriggerEvent, METADATA_PARAMS(Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_TriggerEvent_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_TriggerEvent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FInputActionInstance_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_SourceAction,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_Triggers_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_Triggers,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_Modifiers_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_Modifiers,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_PerInputModifiers_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_PerInputModifiers,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_FinalValueModifiers_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_FinalValueModifiers,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_ElapsedProcessedTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_ElapsedTriggeredTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_TriggerEvent_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FInputActionInstance_Statics::NewProp_TriggerEvent,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FInputActionInstance_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_EnhancedInput,
		nullptr,
		&NewStructOps,
		"InputActionInstance",
		sizeof(FInputActionInstance),
		alignof(FInputActionInstance),
		Z_Construct_UScriptStruct_FInputActionInstance_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FInputActionInstance_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000205),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FInputActionInstance_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FInputActionInstance_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FInputActionInstance()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FInputActionInstance_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_EnhancedInput();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("InputActionInstance"), sizeof(FInputActionInstance), Get_Z_Construct_UScriptStruct_FInputActionInstance_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FInputActionInstance_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FInputActionInstance_Hash() { return 792230656U; }
	void UInputAction::StaticRegisterNativesUInputAction()
	{
	}
	UClass* Z_Construct_UClass_UInputAction_NoRegister()
	{
		return UInputAction::StaticClass();
	}
	struct Z_Construct_UClass_UInputAction_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bConsumeInput_MetaData[];
#endif
		static void NewProp_bConsumeInput_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bConsumeInput;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bTriggerWhenPaused_MetaData[];
#endif
		static void NewProp_bTriggerWhenPaused_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bTriggerWhenPaused;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bReserveAllMappings_MetaData[];
#endif
		static void NewProp_bReserveAllMappings_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bReserveAllMappings;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ValueType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ValueType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ValueType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Triggers_Inner_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Triggers_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Triggers_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Triggers;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Modifiers_Inner_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Modifiers_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Modifiers_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Modifiers;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UInputAction_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataAsset,
		(UObject* (*)())Z_Construct_UPackage__Script_EnhancedInput,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputAction_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "// Input action definition. These are instanced per player (via FInputActionInstance)\n" },
		{ "IncludePath", "InputAction.h" },
		{ "ModuleRelativePath", "Public/InputAction.h" },
		{ "ToolTip", "Input action definition. These are instanced per player (via FInputActionInstance)" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputAction_Statics::NewProp_bConsumeInput_MetaData[] = {
		{ "Category", "Action" },
		{ "Comment", "// Should this action swallow any inputs bound to it or allow them to pass through to affect lower priority bound actions?\n" },
		{ "ModuleRelativePath", "Public/InputAction.h" },
		{ "ToolTip", "Should this action swallow any inputs bound to it or allow them to pass through to affect lower priority bound actions?" },
	};
#endif
	void Z_Construct_UClass_UInputAction_Statics::NewProp_bConsumeInput_SetBit(void* Obj)
	{
		((UInputAction*)Obj)->bConsumeInput = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UInputAction_Statics::NewProp_bConsumeInput = { "bConsumeInput", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UInputAction), &Z_Construct_UClass_UInputAction_Statics::NewProp_bConsumeInput_SetBit, METADATA_PARAMS(Z_Construct_UClass_UInputAction_Statics::NewProp_bConsumeInput_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UInputAction_Statics::NewProp_bConsumeInput_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputAction_Statics::NewProp_bTriggerWhenPaused_MetaData[] = {
		{ "Category", "Action" },
		{ "Comment", "// Should this action be able to trigger whilst the game is paused - Replaces bExecuteWhenPaused\n" },
		{ "ModuleRelativePath", "Public/InputAction.h" },
		{ "ToolTip", "Should this action be able to trigger whilst the game is paused - Replaces bExecuteWhenPaused" },
	};
#endif
	void Z_Construct_UClass_UInputAction_Statics::NewProp_bTriggerWhenPaused_SetBit(void* Obj)
	{
		((UInputAction*)Obj)->bTriggerWhenPaused = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UInputAction_Statics::NewProp_bTriggerWhenPaused = { "bTriggerWhenPaused", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UInputAction), &Z_Construct_UClass_UInputAction_Statics::NewProp_bTriggerWhenPaused_SetBit, METADATA_PARAMS(Z_Construct_UClass_UInputAction_Statics::NewProp_bTriggerWhenPaused_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UInputAction_Statics::NewProp_bTriggerWhenPaused_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputAction_Statics::NewProp_bReserveAllMappings_MetaData[] = {
		{ "Category", "Action" },
		{ "Comment", "// This action's mappings are not intended to be automatically overridden by higher priority context mappings. Users must explicitly remove the mapping first. NOTE: It is the responsibility of the author of the mapping code to enforce this!\n" },
		{ "ModuleRelativePath", "Public/InputAction.h" },
		{ "ToolTip", "This action's mappings are not intended to be automatically overridden by higher priority context mappings. Users must explicitly remove the mapping first. NOTE: It is the responsibility of the author of the mapping code to enforce this!" },
	};
#endif
	void Z_Construct_UClass_UInputAction_Statics::NewProp_bReserveAllMappings_SetBit(void* Obj)
	{
		((UInputAction*)Obj)->bReserveAllMappings = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UInputAction_Statics::NewProp_bReserveAllMappings = { "bReserveAllMappings", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UInputAction), &Z_Construct_UClass_UInputAction_Statics::NewProp_bReserveAllMappings_SetBit, METADATA_PARAMS(Z_Construct_UClass_UInputAction_Statics::NewProp_bReserveAllMappings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UInputAction_Statics::NewProp_bReserveAllMappings_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UInputAction_Statics::NewProp_ValueType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputAction_Statics::NewProp_ValueType_MetaData[] = {
		{ "Category", "Action" },
		{ "Comment", "// The type that this action returns from a GetActionValue query or action event\n" },
		{ "ModuleRelativePath", "Public/InputAction.h" },
		{ "ToolTip", "The type that this action returns from a GetActionValue query or action event" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UInputAction_Statics::NewProp_ValueType = { "ValueType", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UInputAction, ValueType), Z_Construct_UEnum_EnhancedInput_EInputActionValueType, METADATA_PARAMS(Z_Construct_UClass_UInputAction_Statics::NewProp_ValueType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UInputAction_Statics::NewProp_ValueType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputAction_Statics::NewProp_Triggers_Inner_MetaData[] = {
		{ "Category", "Action" },
		{ "Comment", "/**\n\x09* Trigger qualifiers. If any trigger qualifiers exist the action will not trigger unless:\n\x09* At least one Explicit trigger in this list is be met.\n\x09* All Implicit triggers in this list are met.\n\x09*/" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/InputAction.h" },
		{ "ToolTip", "Trigger qualifiers. If any trigger qualifiers exist the action will not trigger unless:\nAt least one Explicit trigger in this list is be met.\nAll Implicit triggers in this list are met." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UInputAction_Statics::NewProp_Triggers_Inner = { "Triggers", nullptr, (EPropertyFlags)0x0002000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UInputTrigger_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UInputAction_Statics::NewProp_Triggers_Inner_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UInputAction_Statics::NewProp_Triggers_Inner_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputAction_Statics::NewProp_Triggers_MetaData[] = {
		{ "Category", "Action" },
		{ "Comment", "/**\n\x09* Trigger qualifiers. If any trigger qualifiers exist the action will not trigger unless:\n\x09* At least one Explicit trigger in this list is be met.\n\x09* All Implicit triggers in this list are met.\n\x09*/" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/InputAction.h" },
		{ "ToolTip", "Trigger qualifiers. If any trigger qualifiers exist the action will not trigger unless:\nAt least one Explicit trigger in this list is be met.\nAll Implicit triggers in this list are met." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UInputAction_Statics::NewProp_Triggers = { "Triggers", nullptr, (EPropertyFlags)0x001000800000000d, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UInputAction, Triggers), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UInputAction_Statics::NewProp_Triggers_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UInputAction_Statics::NewProp_Triggers_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputAction_Statics::NewProp_Modifiers_Inner_MetaData[] = {
		{ "Category", "Action" },
		{ "Comment", "/**\n\x09* Modifiers are applied to the final action value.\n\x09* These are applied sequentially in array order.\n\x09* They are applied on top of any FEnhancedActionKeyMapping modifiers that drove the initial input\n\x09*/" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/InputAction.h" },
		{ "ToolTip", "Modifiers are applied to the final action value.\nThese are applied sequentially in array order.\nThey are applied on top of any FEnhancedActionKeyMapping modifiers that drove the initial input" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UInputAction_Statics::NewProp_Modifiers_Inner = { "Modifiers", nullptr, (EPropertyFlags)0x0002000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UInputModifier_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UInputAction_Statics::NewProp_Modifiers_Inner_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UInputAction_Statics::NewProp_Modifiers_Inner_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputAction_Statics::NewProp_Modifiers_MetaData[] = {
		{ "Category", "Action" },
		{ "Comment", "/**\n\x09* Modifiers are applied to the final action value.\n\x09* These are applied sequentially in array order.\n\x09* They are applied on top of any FEnhancedActionKeyMapping modifiers that drove the initial input\n\x09*/" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/InputAction.h" },
		{ "ToolTip", "Modifiers are applied to the final action value.\nThese are applied sequentially in array order.\nThey are applied on top of any FEnhancedActionKeyMapping modifiers that drove the initial input" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UInputAction_Statics::NewProp_Modifiers = { "Modifiers", nullptr, (EPropertyFlags)0x001000800000000d, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UInputAction, Modifiers), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UInputAction_Statics::NewProp_Modifiers_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UInputAction_Statics::NewProp_Modifiers_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UInputAction_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputAction_Statics::NewProp_bConsumeInput,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputAction_Statics::NewProp_bTriggerWhenPaused,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputAction_Statics::NewProp_bReserveAllMappings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputAction_Statics::NewProp_ValueType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputAction_Statics::NewProp_ValueType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputAction_Statics::NewProp_Triggers_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputAction_Statics::NewProp_Triggers,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputAction_Statics::NewProp_Modifiers_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputAction_Statics::NewProp_Modifiers,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UInputAction_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UInputAction>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UInputAction_Statics::ClassParams = {
		&UInputAction::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UInputAction_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UInputAction_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UInputAction_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UInputAction_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UInputAction()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UInputAction_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UInputAction, 3448757217);
	template<> ENHANCEDINPUT_API UClass* StaticClass<UInputAction>()
	{
		return UInputAction::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UInputAction(Z_Construct_UClass_UInputAction, &UInputAction::StaticClass, TEXT("/Script/EnhancedInput"), TEXT("UInputAction"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UInputAction);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
