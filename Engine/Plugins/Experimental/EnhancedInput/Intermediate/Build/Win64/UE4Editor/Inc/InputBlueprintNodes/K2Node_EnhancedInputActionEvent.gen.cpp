// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "InputBlueprintNodes/Private/K2Node_EnhancedInputActionEvent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeK2Node_EnhancedInputActionEvent() {}
// Cross Module References
	INPUTBLUEPRINTNODES_API UClass* Z_Construct_UClass_UK2Node_EnhancedInputActionEvent_NoRegister();
	INPUTBLUEPRINTNODES_API UClass* Z_Construct_UClass_UK2Node_EnhancedInputActionEvent();
	BLUEPRINTGRAPH_API UClass* Z_Construct_UClass_UK2Node_Event();
	UPackage* Z_Construct_UPackage__Script_InputBlueprintNodes();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputAction_NoRegister();
	ENHANCEDINPUT_API UEnum* Z_Construct_UEnum_EnhancedInput_ETriggerEvent();
// End Cross Module References
	void UK2Node_EnhancedInputActionEvent::StaticRegisterNativesUK2Node_EnhancedInputActionEvent()
	{
	}
	UClass* Z_Construct_UClass_UK2Node_EnhancedInputActionEvent_NoRegister()
	{
		return UK2Node_EnhancedInputActionEvent::StaticClass();
	}
	struct Z_Construct_UClass_UK2Node_EnhancedInputActionEvent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputAction_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InputAction;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TriggerEvent_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TriggerEvent_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TriggerEvent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UK2Node_EnhancedInputActionEvent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UK2Node_Event,
		(UObject* (*)())Z_Construct_UPackage__Script_InputBlueprintNodes,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UK2Node_EnhancedInputActionEvent_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "K2Node_EnhancedInputActionEvent.h" },
		{ "ModuleRelativePath", "Private/K2Node_EnhancedInputActionEvent.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UK2Node_EnhancedInputActionEvent_Statics::NewProp_InputAction_MetaData[] = {
		{ "ModuleRelativePath", "Private/K2Node_EnhancedInputActionEvent.h" },
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UK2Node_EnhancedInputActionEvent_Statics::NewProp_InputAction = { "InputAction", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UK2Node_EnhancedInputActionEvent, InputAction), Z_Construct_UClass_UInputAction_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UK2Node_EnhancedInputActionEvent_Statics::NewProp_InputAction_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UK2Node_EnhancedInputActionEvent_Statics::NewProp_InputAction_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UK2Node_EnhancedInputActionEvent_Statics::NewProp_TriggerEvent_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UK2Node_EnhancedInputActionEvent_Statics::NewProp_TriggerEvent_MetaData[] = {
		{ "ModuleRelativePath", "Private/K2Node_EnhancedInputActionEvent.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UK2Node_EnhancedInputActionEvent_Statics::NewProp_TriggerEvent = { "TriggerEvent", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UK2Node_EnhancedInputActionEvent, TriggerEvent), Z_Construct_UEnum_EnhancedInput_ETriggerEvent, METADATA_PARAMS(Z_Construct_UClass_UK2Node_EnhancedInputActionEvent_Statics::NewProp_TriggerEvent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UK2Node_EnhancedInputActionEvent_Statics::NewProp_TriggerEvent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UK2Node_EnhancedInputActionEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UK2Node_EnhancedInputActionEvent_Statics::NewProp_InputAction,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UK2Node_EnhancedInputActionEvent_Statics::NewProp_TriggerEvent_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UK2Node_EnhancedInputActionEvent_Statics::NewProp_TriggerEvent,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UK2Node_EnhancedInputActionEvent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UK2Node_EnhancedInputActionEvent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UK2Node_EnhancedInputActionEvent_Statics::ClassParams = {
		&UK2Node_EnhancedInputActionEvent::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UK2Node_EnhancedInputActionEvent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UK2Node_EnhancedInputActionEvent_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UK2Node_EnhancedInputActionEvent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UK2Node_EnhancedInputActionEvent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UK2Node_EnhancedInputActionEvent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UK2Node_EnhancedInputActionEvent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UK2Node_EnhancedInputActionEvent, 3238973681);
	template<> INPUTBLUEPRINTNODES_API UClass* StaticClass<UK2Node_EnhancedInputActionEvent>()
	{
		return UK2Node_EnhancedInputActionEvent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UK2Node_EnhancedInputActionEvent(Z_Construct_UClass_UK2Node_EnhancedInputActionEvent, &UK2Node_EnhancedInputActionEvent::StaticClass, TEXT("/Script/InputBlueprintNodes"), TEXT("UK2Node_EnhancedInputActionEvent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UK2Node_EnhancedInputActionEvent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
