// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ENHANCEDINPUT_EnhancedPlayerInput_generated_h
#error "EnhancedPlayerInput.generated.h already included, missing '#pragma once' in EnhancedPlayerInput.h"
#endif
#define ENHANCEDINPUT_EnhancedPlayerInput_generated_h

#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedPlayerInput_h_28_SPARSE_DATA
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedPlayerInput_h_28_RPC_WRAPPERS
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedPlayerInput_h_28_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedPlayerInput_h_28_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUEnhancedPlayerInput(); \
	friend struct Z_Construct_UClass_UEnhancedPlayerInput_Statics; \
public: \
	DECLARE_CLASS(UEnhancedPlayerInput, UPlayerInput, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/EnhancedInput"), NO_API) \
	DECLARE_SERIALIZER(UEnhancedPlayerInput)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedPlayerInput_h_28_INCLASS \
private: \
	static void StaticRegisterNativesUEnhancedPlayerInput(); \
	friend struct Z_Construct_UClass_UEnhancedPlayerInput_Statics; \
public: \
	DECLARE_CLASS(UEnhancedPlayerInput, UPlayerInput, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/EnhancedInput"), NO_API) \
	DECLARE_SERIALIZER(UEnhancedPlayerInput)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedPlayerInput_h_28_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEnhancedPlayerInput(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEnhancedPlayerInput) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEnhancedPlayerInput); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEnhancedPlayerInput); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEnhancedPlayerInput(UEnhancedPlayerInput&&); \
	NO_API UEnhancedPlayerInput(const UEnhancedPlayerInput&); \
public:


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedPlayerInput_h_28_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEnhancedPlayerInput() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEnhancedPlayerInput(UEnhancedPlayerInput&&); \
	NO_API UEnhancedPlayerInput(const UEnhancedPlayerInput&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEnhancedPlayerInput); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEnhancedPlayerInput); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UEnhancedPlayerInput)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedPlayerInput_h_28_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__AppliedInputContexts() { return STRUCT_OFFSET(UEnhancedPlayerInput, AppliedInputContexts); } \
	FORCEINLINE static uint32 __PPO__EnhancedActionMappings() { return STRUCT_OFFSET(UEnhancedPlayerInput, EnhancedActionMappings); } \
	FORCEINLINE static uint32 __PPO__ActionInstanceData() { return STRUCT_OFFSET(UEnhancedPlayerInput, ActionInstanceData); }


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedPlayerInput_h_21_PROLOG
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedPlayerInput_h_28_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedPlayerInput_h_28_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedPlayerInput_h_28_SPARSE_DATA \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedPlayerInput_h_28_RPC_WRAPPERS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedPlayerInput_h_28_INCLASS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedPlayerInput_h_28_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedPlayerInput_h_28_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedPlayerInput_h_28_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedPlayerInput_h_28_SPARSE_DATA \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedPlayerInput_h_28_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedPlayerInput_h_28_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedPlayerInput_h_28_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ENHANCEDINPUT_API UClass* StaticClass<class UEnhancedPlayerInput>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedPlayerInput_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
