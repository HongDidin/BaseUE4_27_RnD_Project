// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef INPUTBLUEPRINTNODES_K2Node_InputDebugKeyEvent_generated_h
#error "K2Node_InputDebugKeyEvent.generated.h already included, missing '#pragma once' in K2Node_InputDebugKeyEvent.h"
#endif
#define INPUTBLUEPRINTNODES_K2Node_InputDebugKeyEvent_generated_h

#define Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKeyEvent_h_17_SPARSE_DATA
#define Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKeyEvent_h_17_RPC_WRAPPERS
#define Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKeyEvent_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKeyEvent_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUK2Node_InputDebugKeyEvent(); \
	friend struct Z_Construct_UClass_UK2Node_InputDebugKeyEvent_Statics; \
public: \
	DECLARE_CLASS(UK2Node_InputDebugKeyEvent, UK2Node_Event, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/InputBlueprintNodes"), NO_API) \
	DECLARE_SERIALIZER(UK2Node_InputDebugKeyEvent)


#define Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKeyEvent_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUK2Node_InputDebugKeyEvent(); \
	friend struct Z_Construct_UClass_UK2Node_InputDebugKeyEvent_Statics; \
public: \
	DECLARE_CLASS(UK2Node_InputDebugKeyEvent, UK2Node_Event, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/InputBlueprintNodes"), NO_API) \
	DECLARE_SERIALIZER(UK2Node_InputDebugKeyEvent)


#define Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKeyEvent_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UK2Node_InputDebugKeyEvent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UK2Node_InputDebugKeyEvent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UK2Node_InputDebugKeyEvent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UK2Node_InputDebugKeyEvent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UK2Node_InputDebugKeyEvent(UK2Node_InputDebugKeyEvent&&); \
	NO_API UK2Node_InputDebugKeyEvent(const UK2Node_InputDebugKeyEvent&); \
public:


#define Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKeyEvent_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UK2Node_InputDebugKeyEvent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UK2Node_InputDebugKeyEvent(UK2Node_InputDebugKeyEvent&&); \
	NO_API UK2Node_InputDebugKeyEvent(const UK2Node_InputDebugKeyEvent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UK2Node_InputDebugKeyEvent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UK2Node_InputDebugKeyEvent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UK2Node_InputDebugKeyEvent)


#define Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKeyEvent_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKeyEvent_h_14_PROLOG
#define Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKeyEvent_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKeyEvent_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKeyEvent_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKeyEvent_h_17_RPC_WRAPPERS \
	Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKeyEvent_h_17_INCLASS \
	Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKeyEvent_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKeyEvent_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKeyEvent_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKeyEvent_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKeyEvent_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKeyEvent_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKeyEvent_h_17_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class K2Node_InputDebugKeyEvent."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> INPUTBLUEPRINTNODES_API UClass* StaticClass<class UK2Node_InputDebugKeyEvent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_EnhancedInput_Source_InputBlueprintNodes_Private_K2Node_InputDebugKeyEvent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
