// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ENHANCEDINPUT_InputAction_generated_h
#error "InputAction.generated.h already included, missing '#pragma once' in InputAction.h"
#endif
#define ENHANCEDINPUT_InputAction_generated_h

#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputAction_h_71_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FInputActionInstance_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__SourceAction() { return STRUCT_OFFSET(FInputActionInstance, SourceAction); } \
	FORCEINLINE static uint32 __PPO__Triggers() { return STRUCT_OFFSET(FInputActionInstance, Triggers); } \
	FORCEINLINE static uint32 __PPO__Modifiers() { return STRUCT_OFFSET(FInputActionInstance, Modifiers); } \
	FORCEINLINE static uint32 __PPO__PerInputModifiers_DEPRECATED() { return STRUCT_OFFSET(FInputActionInstance, PerInputModifiers_DEPRECATED); } \
	FORCEINLINE static uint32 __PPO__FinalValueModifiers_DEPRECATED() { return STRUCT_OFFSET(FInputActionInstance, FinalValueModifiers_DEPRECATED); } \
	FORCEINLINE static uint32 __PPO__ElapsedProcessedTime() { return STRUCT_OFFSET(FInputActionInstance, ElapsedProcessedTime); } \
	FORCEINLINE static uint32 __PPO__ElapsedTriggeredTime() { return STRUCT_OFFSET(FInputActionInstance, ElapsedTriggeredTime); } \
	FORCEINLINE static uint32 __PPO__TriggerEvent() { return STRUCT_OFFSET(FInputActionInstance, TriggerEvent); }


template<> ENHANCEDINPUT_API UScriptStruct* StaticStruct<struct FInputActionInstance>();

#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputAction_h_20_SPARSE_DATA
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputAction_h_20_RPC_WRAPPERS
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputAction_h_20_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputAction_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUInputAction(); \
	friend struct Z_Construct_UClass_UInputAction_Statics; \
public: \
	DECLARE_CLASS(UInputAction, UDataAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/EnhancedInput"), NO_API) \
	DECLARE_SERIALIZER(UInputAction)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputAction_h_20_INCLASS \
private: \
	static void StaticRegisterNativesUInputAction(); \
	friend struct Z_Construct_UClass_UInputAction_Statics; \
public: \
	DECLARE_CLASS(UInputAction, UDataAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/EnhancedInput"), NO_API) \
	DECLARE_SERIALIZER(UInputAction)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputAction_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UInputAction(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInputAction) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UInputAction); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInputAction); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UInputAction(UInputAction&&); \
	NO_API UInputAction(const UInputAction&); \
public:


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputAction_h_20_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UInputAction(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UInputAction(UInputAction&&); \
	NO_API UInputAction(const UInputAction&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UInputAction); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInputAction); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInputAction)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputAction_h_20_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputAction_h_17_PROLOG
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputAction_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputAction_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputAction_h_20_SPARSE_DATA \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputAction_h_20_RPC_WRAPPERS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputAction_h_20_INCLASS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputAction_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputAction_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputAction_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputAction_h_20_SPARSE_DATA \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputAction_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputAction_h_20_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputAction_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ENHANCEDINPUT_API UClass* StaticClass<class UInputAction>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputAction_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
