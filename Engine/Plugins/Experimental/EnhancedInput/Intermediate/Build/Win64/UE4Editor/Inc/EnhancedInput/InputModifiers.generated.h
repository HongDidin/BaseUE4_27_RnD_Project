// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FLinearColor;
struct FInputActionValue;
enum class EModifierExecutionPhase : uint8;
class UEnhancedPlayerInput;
#ifdef ENHANCEDINPUT_InputModifiers_generated_h
#error "InputModifiers.generated.h already included, missing '#pragma once' in InputModifiers.h"
#endif
#define ENHANCEDINPUT_InputModifiers_generated_h

#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_29_SPARSE_DATA
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_29_RPC_WRAPPERS \
	virtual FLinearColor GetVisualizationColor_Implementation(FInputActionValue SampleValue, FInputActionValue FinalValue) const; \
	virtual EModifierExecutionPhase GetExecutionPhase_Implementation() const; \
	virtual FInputActionValue ModifyRaw_Implementation(const UEnhancedPlayerInput* PlayerInput, FInputActionValue CurrentValue, float DeltaTime) const; \
 \
	DECLARE_FUNCTION(execGetVisualizationColor); \
	DECLARE_FUNCTION(execGetExecutionPhase); \
	DECLARE_FUNCTION(execModifyRaw);


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_29_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetVisualizationColor); \
	DECLARE_FUNCTION(execGetExecutionPhase); \
	DECLARE_FUNCTION(execModifyRaw);


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_29_EVENT_PARMS \
	struct InputModifier_eventGetExecutionPhase_Parms \
	{ \
		EModifierExecutionPhase ReturnValue; \
 \
		/** Constructor, initializes return property only **/ \
		InputModifier_eventGetExecutionPhase_Parms() \
			: ReturnValue((EModifierExecutionPhase)0) \
		{ \
		} \
	}; \
	struct InputModifier_eventGetVisualizationColor_Parms \
	{ \
		FInputActionValue SampleValue; \
		FInputActionValue FinalValue; \
		FLinearColor ReturnValue; \
 \
		/** Constructor, initializes return property only **/ \
		InputModifier_eventGetVisualizationColor_Parms() \
			: ReturnValue(ForceInit) \
		{ \
		} \
	}; \
	struct InputModifier_eventModifyRaw_Parms \
	{ \
		const UEnhancedPlayerInput* PlayerInput; \
		FInputActionValue CurrentValue; \
		float DeltaTime; \
		FInputActionValue ReturnValue; \
	};


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_29_CALLBACK_WRAPPERS
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_29_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUInputModifier(); \
	friend struct Z_Construct_UClass_UInputModifier_Statics; \
public: \
	DECLARE_CLASS(UInputModifier, UObject, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_DefaultConfig), CASTCLASS_None, TEXT("/Script/EnhancedInput"), NO_API) \
	DECLARE_SERIALIZER(UInputModifier) \
	static const TCHAR* StaticConfigName() {return TEXT("Input");} \



#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_29_INCLASS \
private: \
	static void StaticRegisterNativesUInputModifier(); \
	friend struct Z_Construct_UClass_UInputModifier_Statics; \
public: \
	DECLARE_CLASS(UInputModifier, UObject, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_DefaultConfig), CASTCLASS_None, TEXT("/Script/EnhancedInput"), NO_API) \
	DECLARE_SERIALIZER(UInputModifier) \
	static const TCHAR* StaticConfigName() {return TEXT("Input");} \



#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_29_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UInputModifier(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInputModifier) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UInputModifier); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInputModifier); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UInputModifier(UInputModifier&&); \
	NO_API UInputModifier(const UInputModifier&); \
public:


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_29_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UInputModifier(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UInputModifier(UInputModifier&&); \
	NO_API UInputModifier(const UInputModifier&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UInputModifier); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInputModifier); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInputModifier)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_29_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_26_PROLOG \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_29_EVENT_PARMS


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_29_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_29_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_29_SPARSE_DATA \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_29_RPC_WRAPPERS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_29_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_29_INCLASS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_29_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_29_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_29_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_29_SPARSE_DATA \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_29_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_29_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_29_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_29_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ENHANCEDINPUT_API UClass* StaticClass<class UInputModifier>();

#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_94_SPARSE_DATA
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_94_RPC_WRAPPERS
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_94_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_94_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUInputModifierDeadZone(); \
	friend struct Z_Construct_UClass_UInputModifierDeadZone_Statics; \
public: \
	DECLARE_CLASS(UInputModifierDeadZone, UInputModifier, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/EnhancedInput"), ENHANCEDINPUT_API) \
	DECLARE_SERIALIZER(UInputModifierDeadZone)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_94_INCLASS \
private: \
	static void StaticRegisterNativesUInputModifierDeadZone(); \
	friend struct Z_Construct_UClass_UInputModifierDeadZone_Statics; \
public: \
	DECLARE_CLASS(UInputModifierDeadZone, UInputModifier, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/EnhancedInput"), ENHANCEDINPUT_API) \
	DECLARE_SERIALIZER(UInputModifierDeadZone)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_94_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ENHANCEDINPUT_API UInputModifierDeadZone(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInputModifierDeadZone) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ENHANCEDINPUT_API, UInputModifierDeadZone); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInputModifierDeadZone); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ENHANCEDINPUT_API UInputModifierDeadZone(UInputModifierDeadZone&&); \
	ENHANCEDINPUT_API UInputModifierDeadZone(const UInputModifierDeadZone&); \
public:


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_94_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ENHANCEDINPUT_API UInputModifierDeadZone(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ENHANCEDINPUT_API UInputModifierDeadZone(UInputModifierDeadZone&&); \
	ENHANCEDINPUT_API UInputModifierDeadZone(const UInputModifierDeadZone&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ENHANCEDINPUT_API, UInputModifierDeadZone); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInputModifierDeadZone); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInputModifierDeadZone)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_94_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_91_PROLOG
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_94_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_94_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_94_SPARSE_DATA \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_94_RPC_WRAPPERS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_94_INCLASS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_94_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_94_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_94_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_94_SPARSE_DATA \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_94_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_94_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_94_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ENHANCEDINPUT_API UClass* StaticClass<class UInputModifierDeadZone>();

#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_123_SPARSE_DATA
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_123_RPC_WRAPPERS
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_123_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_123_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUInputModifierScalar(); \
	friend struct Z_Construct_UClass_UInputModifierScalar_Statics; \
public: \
	DECLARE_CLASS(UInputModifierScalar, UInputModifier, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig), CASTCLASS_None, TEXT("/Script/EnhancedInput"), ENHANCEDINPUT_API) \
	DECLARE_SERIALIZER(UInputModifierScalar)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_123_INCLASS \
private: \
	static void StaticRegisterNativesUInputModifierScalar(); \
	friend struct Z_Construct_UClass_UInputModifierScalar_Statics; \
public: \
	DECLARE_CLASS(UInputModifierScalar, UInputModifier, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig), CASTCLASS_None, TEXT("/Script/EnhancedInput"), ENHANCEDINPUT_API) \
	DECLARE_SERIALIZER(UInputModifierScalar)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_123_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ENHANCEDINPUT_API UInputModifierScalar(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInputModifierScalar) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ENHANCEDINPUT_API, UInputModifierScalar); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInputModifierScalar); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ENHANCEDINPUT_API UInputModifierScalar(UInputModifierScalar&&); \
	ENHANCEDINPUT_API UInputModifierScalar(const UInputModifierScalar&); \
public:


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_123_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ENHANCEDINPUT_API UInputModifierScalar(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ENHANCEDINPUT_API UInputModifierScalar(UInputModifierScalar&&); \
	ENHANCEDINPUT_API UInputModifierScalar(const UInputModifierScalar&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ENHANCEDINPUT_API, UInputModifierScalar); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInputModifierScalar); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInputModifierScalar)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_123_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_120_PROLOG
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_123_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_123_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_123_SPARSE_DATA \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_123_RPC_WRAPPERS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_123_INCLASS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_123_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_123_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_123_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_123_SPARSE_DATA \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_123_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_123_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_123_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ENHANCEDINPUT_API UClass* StaticClass<class UInputModifierScalar>();

#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_141_SPARSE_DATA
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_141_RPC_WRAPPERS
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_141_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_141_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUInputModifierNegate(); \
	friend struct Z_Construct_UClass_UInputModifierNegate_Statics; \
public: \
	DECLARE_CLASS(UInputModifierNegate, UInputModifier, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig), CASTCLASS_None, TEXT("/Script/EnhancedInput"), ENHANCEDINPUT_API) \
	DECLARE_SERIALIZER(UInputModifierNegate)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_141_INCLASS \
private: \
	static void StaticRegisterNativesUInputModifierNegate(); \
	friend struct Z_Construct_UClass_UInputModifierNegate_Statics; \
public: \
	DECLARE_CLASS(UInputModifierNegate, UInputModifier, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig), CASTCLASS_None, TEXT("/Script/EnhancedInput"), ENHANCEDINPUT_API) \
	DECLARE_SERIALIZER(UInputModifierNegate)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_141_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ENHANCEDINPUT_API UInputModifierNegate(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInputModifierNegate) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ENHANCEDINPUT_API, UInputModifierNegate); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInputModifierNegate); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ENHANCEDINPUT_API UInputModifierNegate(UInputModifierNegate&&); \
	ENHANCEDINPUT_API UInputModifierNegate(const UInputModifierNegate&); \
public:


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_141_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ENHANCEDINPUT_API UInputModifierNegate(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ENHANCEDINPUT_API UInputModifierNegate(UInputModifierNegate&&); \
	ENHANCEDINPUT_API UInputModifierNegate(const UInputModifierNegate&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ENHANCEDINPUT_API, UInputModifierNegate); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInputModifierNegate); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInputModifierNegate)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_141_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_138_PROLOG
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_141_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_141_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_141_SPARSE_DATA \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_141_RPC_WRAPPERS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_141_INCLASS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_141_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_141_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_141_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_141_SPARSE_DATA \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_141_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_141_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_141_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ENHANCEDINPUT_API UClass* StaticClass<class UInputModifierNegate>();

#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_163_SPARSE_DATA
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_163_RPC_WRAPPERS
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_163_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_163_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUInputModifierSmooth(); \
	friend struct Z_Construct_UClass_UInputModifierSmooth_Statics; \
public: \
	DECLARE_CLASS(UInputModifierSmooth, UInputModifier, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig), CASTCLASS_None, TEXT("/Script/EnhancedInput"), ENHANCEDINPUT_API) \
	DECLARE_SERIALIZER(UInputModifierSmooth)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_163_INCLASS \
private: \
	static void StaticRegisterNativesUInputModifierSmooth(); \
	friend struct Z_Construct_UClass_UInputModifierSmooth_Statics; \
public: \
	DECLARE_CLASS(UInputModifierSmooth, UInputModifier, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig), CASTCLASS_None, TEXT("/Script/EnhancedInput"), ENHANCEDINPUT_API) \
	DECLARE_SERIALIZER(UInputModifierSmooth)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_163_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ENHANCEDINPUT_API UInputModifierSmooth(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInputModifierSmooth) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ENHANCEDINPUT_API, UInputModifierSmooth); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInputModifierSmooth); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ENHANCEDINPUT_API UInputModifierSmooth(UInputModifierSmooth&&); \
	ENHANCEDINPUT_API UInputModifierSmooth(const UInputModifierSmooth&); \
public:


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_163_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ENHANCEDINPUT_API UInputModifierSmooth(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ENHANCEDINPUT_API UInputModifierSmooth(UInputModifierSmooth&&); \
	ENHANCEDINPUT_API UInputModifierSmooth(const UInputModifierSmooth&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ENHANCEDINPUT_API, UInputModifierSmooth); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInputModifierSmooth); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInputModifierSmooth)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_163_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_160_PROLOG
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_163_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_163_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_163_SPARSE_DATA \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_163_RPC_WRAPPERS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_163_INCLASS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_163_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_163_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_163_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_163_SPARSE_DATA \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_163_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_163_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_163_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ENHANCEDINPUT_API UClass* StaticClass<class UInputModifierSmooth>();

#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_189_SPARSE_DATA
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_189_RPC_WRAPPERS
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_189_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_189_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUInputModifierResponseCurveExponential(); \
	friend struct Z_Construct_UClass_UInputModifierResponseCurveExponential_Statics; \
public: \
	DECLARE_CLASS(UInputModifierResponseCurveExponential, UInputModifier, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/EnhancedInput"), ENHANCEDINPUT_API) \
	DECLARE_SERIALIZER(UInputModifierResponseCurveExponential)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_189_INCLASS \
private: \
	static void StaticRegisterNativesUInputModifierResponseCurveExponential(); \
	friend struct Z_Construct_UClass_UInputModifierResponseCurveExponential_Statics; \
public: \
	DECLARE_CLASS(UInputModifierResponseCurveExponential, UInputModifier, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/EnhancedInput"), ENHANCEDINPUT_API) \
	DECLARE_SERIALIZER(UInputModifierResponseCurveExponential)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_189_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ENHANCEDINPUT_API UInputModifierResponseCurveExponential(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInputModifierResponseCurveExponential) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ENHANCEDINPUT_API, UInputModifierResponseCurveExponential); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInputModifierResponseCurveExponential); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ENHANCEDINPUT_API UInputModifierResponseCurveExponential(UInputModifierResponseCurveExponential&&); \
	ENHANCEDINPUT_API UInputModifierResponseCurveExponential(const UInputModifierResponseCurveExponential&); \
public:


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_189_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ENHANCEDINPUT_API UInputModifierResponseCurveExponential(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ENHANCEDINPUT_API UInputModifierResponseCurveExponential(UInputModifierResponseCurveExponential&&); \
	ENHANCEDINPUT_API UInputModifierResponseCurveExponential(const UInputModifierResponseCurveExponential&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ENHANCEDINPUT_API, UInputModifierResponseCurveExponential); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInputModifierResponseCurveExponential); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInputModifierResponseCurveExponential)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_189_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_186_PROLOG
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_189_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_189_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_189_SPARSE_DATA \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_189_RPC_WRAPPERS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_189_INCLASS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_189_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_189_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_189_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_189_SPARSE_DATA \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_189_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_189_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_189_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ENHANCEDINPUT_API UClass* StaticClass<class UInputModifierResponseCurveExponential>();

#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_207_SPARSE_DATA
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_207_RPC_WRAPPERS
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_207_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_207_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUInputModifierResponseCurveUser(); \
	friend struct Z_Construct_UClass_UInputModifierResponseCurveUser_Statics; \
public: \
	DECLARE_CLASS(UInputModifierResponseCurveUser, UInputModifier, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig), CASTCLASS_None, TEXT("/Script/EnhancedInput"), ENHANCEDINPUT_API) \
	DECLARE_SERIALIZER(UInputModifierResponseCurveUser)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_207_INCLASS \
private: \
	static void StaticRegisterNativesUInputModifierResponseCurveUser(); \
	friend struct Z_Construct_UClass_UInputModifierResponseCurveUser_Statics; \
public: \
	DECLARE_CLASS(UInputModifierResponseCurveUser, UInputModifier, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig), CASTCLASS_None, TEXT("/Script/EnhancedInput"), ENHANCEDINPUT_API) \
	DECLARE_SERIALIZER(UInputModifierResponseCurveUser)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_207_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ENHANCEDINPUT_API UInputModifierResponseCurveUser(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInputModifierResponseCurveUser) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ENHANCEDINPUT_API, UInputModifierResponseCurveUser); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInputModifierResponseCurveUser); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ENHANCEDINPUT_API UInputModifierResponseCurveUser(UInputModifierResponseCurveUser&&); \
	ENHANCEDINPUT_API UInputModifierResponseCurveUser(const UInputModifierResponseCurveUser&); \
public:


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_207_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ENHANCEDINPUT_API UInputModifierResponseCurveUser(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ENHANCEDINPUT_API UInputModifierResponseCurveUser(UInputModifierResponseCurveUser&&); \
	ENHANCEDINPUT_API UInputModifierResponseCurveUser(const UInputModifierResponseCurveUser&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ENHANCEDINPUT_API, UInputModifierResponseCurveUser); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInputModifierResponseCurveUser); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInputModifierResponseCurveUser)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_207_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_204_PROLOG
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_207_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_207_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_207_SPARSE_DATA \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_207_RPC_WRAPPERS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_207_INCLASS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_207_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_207_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_207_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_207_SPARSE_DATA \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_207_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_207_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_207_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ENHANCEDINPUT_API UClass* StaticClass<class UInputModifierResponseCurveUser>();

#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_238_SPARSE_DATA
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_238_RPC_WRAPPERS
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_238_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_238_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUInputModifierFOVScaling(); \
	friend struct Z_Construct_UClass_UInputModifierFOVScaling_Statics; \
public: \
	DECLARE_CLASS(UInputModifierFOVScaling, UInputModifier, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/EnhancedInput"), ENHANCEDINPUT_API) \
	DECLARE_SERIALIZER(UInputModifierFOVScaling)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_238_INCLASS \
private: \
	static void StaticRegisterNativesUInputModifierFOVScaling(); \
	friend struct Z_Construct_UClass_UInputModifierFOVScaling_Statics; \
public: \
	DECLARE_CLASS(UInputModifierFOVScaling, UInputModifier, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/EnhancedInput"), ENHANCEDINPUT_API) \
	DECLARE_SERIALIZER(UInputModifierFOVScaling)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_238_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ENHANCEDINPUT_API UInputModifierFOVScaling(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInputModifierFOVScaling) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ENHANCEDINPUT_API, UInputModifierFOVScaling); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInputModifierFOVScaling); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ENHANCEDINPUT_API UInputModifierFOVScaling(UInputModifierFOVScaling&&); \
	ENHANCEDINPUT_API UInputModifierFOVScaling(const UInputModifierFOVScaling&); \
public:


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_238_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ENHANCEDINPUT_API UInputModifierFOVScaling(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ENHANCEDINPUT_API UInputModifierFOVScaling(UInputModifierFOVScaling&&); \
	ENHANCEDINPUT_API UInputModifierFOVScaling(const UInputModifierFOVScaling&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ENHANCEDINPUT_API, UInputModifierFOVScaling); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInputModifierFOVScaling); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInputModifierFOVScaling)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_238_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_235_PROLOG
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_238_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_238_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_238_SPARSE_DATA \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_238_RPC_WRAPPERS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_238_INCLASS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_238_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_238_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_238_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_238_SPARSE_DATA \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_238_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_238_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_238_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ENHANCEDINPUT_API UClass* StaticClass<class UInputModifierFOVScaling>();

#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_261_SPARSE_DATA
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_261_RPC_WRAPPERS
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_261_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_261_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUInputModifierToWorldSpace(); \
	friend struct Z_Construct_UClass_UInputModifierToWorldSpace_Statics; \
public: \
	DECLARE_CLASS(UInputModifierToWorldSpace, UInputModifier, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig), CASTCLASS_None, TEXT("/Script/EnhancedInput"), ENHANCEDINPUT_API) \
	DECLARE_SERIALIZER(UInputModifierToWorldSpace)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_261_INCLASS \
private: \
	static void StaticRegisterNativesUInputModifierToWorldSpace(); \
	friend struct Z_Construct_UClass_UInputModifierToWorldSpace_Statics; \
public: \
	DECLARE_CLASS(UInputModifierToWorldSpace, UInputModifier, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig), CASTCLASS_None, TEXT("/Script/EnhancedInput"), ENHANCEDINPUT_API) \
	DECLARE_SERIALIZER(UInputModifierToWorldSpace)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_261_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ENHANCEDINPUT_API UInputModifierToWorldSpace(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInputModifierToWorldSpace) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ENHANCEDINPUT_API, UInputModifierToWorldSpace); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInputModifierToWorldSpace); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ENHANCEDINPUT_API UInputModifierToWorldSpace(UInputModifierToWorldSpace&&); \
	ENHANCEDINPUT_API UInputModifierToWorldSpace(const UInputModifierToWorldSpace&); \
public:


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_261_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ENHANCEDINPUT_API UInputModifierToWorldSpace(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ENHANCEDINPUT_API UInputModifierToWorldSpace(UInputModifierToWorldSpace&&); \
	ENHANCEDINPUT_API UInputModifierToWorldSpace(const UInputModifierToWorldSpace&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ENHANCEDINPUT_API, UInputModifierToWorldSpace); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInputModifierToWorldSpace); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInputModifierToWorldSpace)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_261_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_258_PROLOG
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_261_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_261_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_261_SPARSE_DATA \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_261_RPC_WRAPPERS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_261_INCLASS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_261_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_261_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_261_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_261_SPARSE_DATA \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_261_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_261_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_261_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ENHANCEDINPUT_API UClass* StaticClass<class UInputModifierToWorldSpace>();

#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_294_SPARSE_DATA
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_294_RPC_WRAPPERS
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_294_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_294_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUInputModifierSwizzleAxis(); \
	friend struct Z_Construct_UClass_UInputModifierSwizzleAxis_Statics; \
public: \
	DECLARE_CLASS(UInputModifierSwizzleAxis, UInputModifier, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig), CASTCLASS_None, TEXT("/Script/EnhancedInput"), ENHANCEDINPUT_API) \
	DECLARE_SERIALIZER(UInputModifierSwizzleAxis)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_294_INCLASS \
private: \
	static void StaticRegisterNativesUInputModifierSwizzleAxis(); \
	friend struct Z_Construct_UClass_UInputModifierSwizzleAxis_Statics; \
public: \
	DECLARE_CLASS(UInputModifierSwizzleAxis, UInputModifier, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig), CASTCLASS_None, TEXT("/Script/EnhancedInput"), ENHANCEDINPUT_API) \
	DECLARE_SERIALIZER(UInputModifierSwizzleAxis)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_294_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ENHANCEDINPUT_API UInputModifierSwizzleAxis(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInputModifierSwizzleAxis) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ENHANCEDINPUT_API, UInputModifierSwizzleAxis); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInputModifierSwizzleAxis); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ENHANCEDINPUT_API UInputModifierSwizzleAxis(UInputModifierSwizzleAxis&&); \
	ENHANCEDINPUT_API UInputModifierSwizzleAxis(const UInputModifierSwizzleAxis&); \
public:


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_294_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ENHANCEDINPUT_API UInputModifierSwizzleAxis(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ENHANCEDINPUT_API UInputModifierSwizzleAxis(UInputModifierSwizzleAxis&&); \
	ENHANCEDINPUT_API UInputModifierSwizzleAxis(const UInputModifierSwizzleAxis&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ENHANCEDINPUT_API, UInputModifierSwizzleAxis); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInputModifierSwizzleAxis); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInputModifierSwizzleAxis)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_294_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_291_PROLOG
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_294_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_294_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_294_SPARSE_DATA \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_294_RPC_WRAPPERS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_294_INCLASS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_294_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_294_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_294_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_294_SPARSE_DATA \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_294_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_294_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_294_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ENHANCEDINPUT_API UClass* StaticClass<class UInputModifierSwizzleAxis>();

#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_312_SPARSE_DATA
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_312_RPC_WRAPPERS
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_312_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_312_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUInputModifierCollection(); \
	friend struct Z_Construct_UClass_UInputModifierCollection_Statics; \
public: \
	DECLARE_CLASS(UInputModifierCollection, UInputModifier, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig), CASTCLASS_None, TEXT("/Script/EnhancedInput"), ENHANCEDINPUT_API) \
	DECLARE_SERIALIZER(UInputModifierCollection)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_312_INCLASS \
private: \
	static void StaticRegisterNativesUInputModifierCollection(); \
	friend struct Z_Construct_UClass_UInputModifierCollection_Statics; \
public: \
	DECLARE_CLASS(UInputModifierCollection, UInputModifier, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig), CASTCLASS_None, TEXT("/Script/EnhancedInput"), ENHANCEDINPUT_API) \
	DECLARE_SERIALIZER(UInputModifierCollection)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_312_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ENHANCEDINPUT_API UInputModifierCollection(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInputModifierCollection) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ENHANCEDINPUT_API, UInputModifierCollection); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInputModifierCollection); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ENHANCEDINPUT_API UInputModifierCollection(UInputModifierCollection&&); \
	ENHANCEDINPUT_API UInputModifierCollection(const UInputModifierCollection&); \
public:


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_312_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ENHANCEDINPUT_API UInputModifierCollection(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ENHANCEDINPUT_API UInputModifierCollection(UInputModifierCollection&&); \
	ENHANCEDINPUT_API UInputModifierCollection(const UInputModifierCollection&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ENHANCEDINPUT_API, UInputModifierCollection); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInputModifierCollection); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInputModifierCollection)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_312_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_309_PROLOG
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_312_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_312_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_312_SPARSE_DATA \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_312_RPC_WRAPPERS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_312_INCLASS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_312_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_312_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_312_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_312_SPARSE_DATA \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_312_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_312_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h_312_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ENHANCEDINPUT_API UClass* StaticClass<class UInputModifierCollection>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_InputModifiers_h


#define FOREACH_ENUM_EINPUTAXISSWIZZLE(op) \
	op(EInputAxisSwizzle::YXZ) \
	op(EInputAxisSwizzle::ZYX) \
	op(EInputAxisSwizzle::XZY) \
	op(EInputAxisSwizzle::YZX) \
	op(EInputAxisSwizzle::ZXY) 

enum class EInputAxisSwizzle : uint8;
template<> ENHANCEDINPUT_API UEnum* StaticEnum<EInputAxisSwizzle>();

#define FOREACH_ENUM_EFOVSCALINGTYPE(op) \
	op(EFOVScalingType::Standard) \
	op(EFOVScalingType::UE4_BackCompat) 

enum class EFOVScalingType : uint8;
template<> ENHANCEDINPUT_API UEnum* StaticEnum<EFOVScalingType>();

#define FOREACH_ENUM_EDEADZONETYPE(op) \
	op(EDeadZoneType::Axial) \
	op(EDeadZoneType::Radial) 

enum class EDeadZoneType : uint8;
template<> ENHANCEDINPUT_API UEnum* StaticEnum<EDeadZoneType>();

#define FOREACH_ENUM_EMODIFIEREXECUTIONPHASE(op) \
	op(EModifierExecutionPhase::PerInput) \
	op(EModifierExecutionPhase::FinalValue) \
	op(EModifierExecutionPhase::NumPhases) 

enum class EModifierExecutionPhase : uint8;
template<> ENHANCEDINPUT_API UEnum* StaticEnum<EModifierExecutionPhase>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
