// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "InputBlueprintNodes/Public/K2Node_GetInputActionValue.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeK2Node_GetInputActionValue() {}
// Cross Module References
	INPUTBLUEPRINTNODES_API UClass* Z_Construct_UClass_UK2Node_GetInputActionValue_NoRegister();
	INPUTBLUEPRINTNODES_API UClass* Z_Construct_UClass_UK2Node_GetInputActionValue();
	BLUEPRINTGRAPH_API UClass* Z_Construct_UClass_UK2Node();
	UPackage* Z_Construct_UPackage__Script_InputBlueprintNodes();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputAction_NoRegister();
// End Cross Module References
	void UK2Node_GetInputActionValue::StaticRegisterNativesUK2Node_GetInputActionValue()
	{
	}
	UClass* Z_Construct_UClass_UK2Node_GetInputActionValue_NoRegister()
	{
		return UK2Node_GetInputActionValue::StaticClass();
	}
	struct Z_Construct_UClass_UK2Node_GetInputActionValue_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputAction_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InputAction;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UK2Node_GetInputActionValue_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UK2Node,
		(UObject* (*)())Z_Construct_UPackage__Script_InputBlueprintNodes,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UK2Node_GetInputActionValue_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "K2Node_GetInputActionValue.h" },
		{ "Keywords", "Get, Input" },
		{ "ModuleRelativePath", "Public/K2Node_GetInputActionValue.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UK2Node_GetInputActionValue_Statics::NewProp_InputAction_MetaData[] = {
		{ "ModuleRelativePath", "Public/K2Node_GetInputActionValue.h" },
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UK2Node_GetInputActionValue_Statics::NewProp_InputAction = { "InputAction", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UK2Node_GetInputActionValue, InputAction), Z_Construct_UClass_UInputAction_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UK2Node_GetInputActionValue_Statics::NewProp_InputAction_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UK2Node_GetInputActionValue_Statics::NewProp_InputAction_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UK2Node_GetInputActionValue_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UK2Node_GetInputActionValue_Statics::NewProp_InputAction,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UK2Node_GetInputActionValue_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UK2Node_GetInputActionValue>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UK2Node_GetInputActionValue_Statics::ClassParams = {
		&UK2Node_GetInputActionValue::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UK2Node_GetInputActionValue_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UK2Node_GetInputActionValue_Statics::PropPointers),
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UK2Node_GetInputActionValue_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UK2Node_GetInputActionValue_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UK2Node_GetInputActionValue()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UK2Node_GetInputActionValue_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UK2Node_GetInputActionValue, 1492418984);
	template<> INPUTBLUEPRINTNODES_API UClass* StaticClass<UK2Node_GetInputActionValue>()
	{
		return UK2Node_GetInputActionValue::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UK2Node_GetInputActionValue(Z_Construct_UClass_UK2Node_GetInputActionValue, &UK2Node_GetInputActionValue::StaticClass, TEXT("/Script/InputBlueprintNodes"), TEXT("UK2Node_GetInputActionValue"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UK2Node_GetInputActionValue);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
