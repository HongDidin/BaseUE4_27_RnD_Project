// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "EnhancedInput/Public/EnhancedActionKeyMapping.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEnhancedActionKeyMapping() {}
// Cross Module References
	ENHANCEDINPUT_API UScriptStruct* Z_Construct_UScriptStruct_FEnhancedActionKeyMapping();
	UPackage* Z_Construct_UPackage__Script_EnhancedInput();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputAction_NoRegister();
	INPUTCORE_API UScriptStruct* Z_Construct_UScriptStruct_FKey();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputTrigger_NoRegister();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputModifier_NoRegister();
// End Cross Module References
class UScriptStruct* FEnhancedActionKeyMapping::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ENHANCEDINPUT_API uint32 Get_Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FEnhancedActionKeyMapping, Z_Construct_UPackage__Script_EnhancedInput(), TEXT("EnhancedActionKeyMapping"), sizeof(FEnhancedActionKeyMapping), Get_Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Hash());
	}
	return Singleton;
}
template<> ENHANCEDINPUT_API UScriptStruct* StaticStruct<FEnhancedActionKeyMapping>()
{
	return FEnhancedActionKeyMapping::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FEnhancedActionKeyMapping(FEnhancedActionKeyMapping::StaticStruct, TEXT("/Script/EnhancedInput"), TEXT("EnhancedActionKeyMapping"), false, nullptr, nullptr);
static struct FScriptStruct_EnhancedInput_StaticRegisterNativesFEnhancedActionKeyMapping
{
	FScriptStruct_EnhancedInput_StaticRegisterNativesFEnhancedActionKeyMapping()
	{
		UScriptStruct::DeferCppStructOps<FEnhancedActionKeyMapping>(FName(TEXT("EnhancedActionKeyMapping")));
	}
} ScriptStruct_EnhancedInput_StaticRegisterNativesFEnhancedActionKeyMapping;
	struct Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Action_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Action;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Key;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Triggers_Inner_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Triggers_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Triggers_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Triggers;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Modifiers_Inner_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Modifiers_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Modifiers_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Modifiers;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Defines a mapping between a key activation and the resulting enhanced action\n * An key could be a button press, joystick axis movement, etc.\n * An enhanced action could be MoveForward, Jump, Fire, etc.\n *\n**/" },
		{ "ModuleRelativePath", "Public/EnhancedActionKeyMapping.h" },
		{ "ToolTip", "Defines a mapping between a key activation and the resulting enhanced action\nAn key could be a button press, joystick axis movement, etc.\nAn enhanced action could be MoveForward, Jump, Fire, etc." },
	};
#endif
	void* Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FEnhancedActionKeyMapping>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Statics::NewProp_Action_MetaData[] = {
		{ "Category", "Input" },
		{ "Comment", "/** Action to be affected by the key  */" },
		{ "ModuleRelativePath", "Public/EnhancedActionKeyMapping.h" },
		{ "NativeConst", "" },
		{ "ToolTip", "Action to be affected by the key" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Statics::NewProp_Action = { "Action", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FEnhancedActionKeyMapping, Action), Z_Construct_UClass_UInputAction_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Statics::NewProp_Action_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Statics::NewProp_Action_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Statics::NewProp_Key_MetaData[] = {
		{ "Category", "Input" },
		{ "Comment", "/** Key that affect the action. */" },
		{ "ModuleRelativePath", "Public/EnhancedActionKeyMapping.h" },
		{ "ToolTip", "Key that affect the action." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Statics::NewProp_Key = { "Key", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FEnhancedActionKeyMapping, Key), Z_Construct_UScriptStruct_FKey, METADATA_PARAMS(Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Statics::NewProp_Key_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Statics::NewProp_Key_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Statics::NewProp_Triggers_Inner_MetaData[] = {
		{ "Category", "Input" },
		{ "Comment", "/**\n\x09* Trigger qualifiers. If any trigger qualifiers exist the mapping will not trigger unless:\n\x09* If there are any Explicit triggers in this list at least one of them must be met.\n\x09* All Implicit triggers in this list must be met.\n\x09*/" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/EnhancedActionKeyMapping.h" },
		{ "ToolTip", "Trigger qualifiers. If any trigger qualifiers exist the mapping will not trigger unless:\nIf there are any Explicit triggers in this list at least one of them must be met.\nAll Implicit triggers in this list must be met." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Statics::NewProp_Triggers_Inner = { "Triggers", nullptr, (EPropertyFlags)0x0002000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UInputTrigger_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Statics::NewProp_Triggers_Inner_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Statics::NewProp_Triggers_Inner_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Statics::NewProp_Triggers_MetaData[] = {
		{ "Category", "Input" },
		{ "Comment", "/**\n\x09* Trigger qualifiers. If any trigger qualifiers exist the mapping will not trigger unless:\n\x09* If there are any Explicit triggers in this list at least one of them must be met.\n\x09* All Implicit triggers in this list must be met.\n\x09*/" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/EnhancedActionKeyMapping.h" },
		{ "ToolTip", "Trigger qualifiers. If any trigger qualifiers exist the mapping will not trigger unless:\nIf there are any Explicit triggers in this list at least one of them must be met.\nAll Implicit triggers in this list must be met." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Statics::NewProp_Triggers = { "Triggers", nullptr, (EPropertyFlags)0x001000800000000d, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FEnhancedActionKeyMapping, Triggers), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Statics::NewProp_Triggers_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Statics::NewProp_Triggers_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Statics::NewProp_Modifiers_Inner_MetaData[] = {
		{ "Category", "Input" },
		{ "Comment", "/**\n\x09* Modifiers applied to the raw key value.\n\x09* These are applied sequentially in array order.\n\x09*/" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/EnhancedActionKeyMapping.h" },
		{ "ToolTip", "Modifiers applied to the raw key value.\nThese are applied sequentially in array order." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Statics::NewProp_Modifiers_Inner = { "Modifiers", nullptr, (EPropertyFlags)0x0002000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UInputModifier_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Statics::NewProp_Modifiers_Inner_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Statics::NewProp_Modifiers_Inner_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Statics::NewProp_Modifiers_MetaData[] = {
		{ "Category", "Input" },
		{ "Comment", "/**\n\x09* Modifiers applied to the raw key value.\n\x09* These are applied sequentially in array order.\n\x09*/" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/EnhancedActionKeyMapping.h" },
		{ "ToolTip", "Modifiers applied to the raw key value.\nThese are applied sequentially in array order." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Statics::NewProp_Modifiers = { "Modifiers", nullptr, (EPropertyFlags)0x001000800000000d, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FEnhancedActionKeyMapping, Modifiers), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Statics::NewProp_Modifiers_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Statics::NewProp_Modifiers_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Statics::NewProp_Action,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Statics::NewProp_Triggers_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Statics::NewProp_Triggers,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Statics::NewProp_Modifiers_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Statics::NewProp_Modifiers,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_EnhancedInput,
		nullptr,
		&NewStructOps,
		"EnhancedActionKeyMapping",
		sizeof(FEnhancedActionKeyMapping),
		alignof(FEnhancedActionKeyMapping),
		Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000005),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FEnhancedActionKeyMapping()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_EnhancedInput();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("EnhancedActionKeyMapping"), sizeof(FEnhancedActionKeyMapping), Get_Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FEnhancedActionKeyMapping_Hash() { return 4265914630U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
