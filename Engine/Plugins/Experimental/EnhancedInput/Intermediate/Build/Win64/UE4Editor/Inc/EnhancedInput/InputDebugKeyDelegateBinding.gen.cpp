// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "EnhancedInput/Public/InputDebugKeyDelegateBinding.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeInputDebugKeyDelegateBinding() {}
// Cross Module References
	ENHANCEDINPUT_API UScriptStruct* Z_Construct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding();
	UPackage* Z_Construct_UPackage__Script_EnhancedInput();
	SLATE_API UScriptStruct* Z_Construct_UScriptStruct_FInputChord();
	ENGINE_API UEnum* Z_Construct_UEnum_Engine_EInputEvent();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputDebugKeyDelegateBinding_NoRegister();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputDebugKeyDelegateBinding();
	ENGINE_API UClass* Z_Construct_UClass_UInputDelegateBinding();
// End Cross Module References
class UScriptStruct* FBlueprintInputDebugKeyDelegateBinding::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ENHANCEDINPUT_API uint32 Get_Z_Construct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding, Z_Construct_UPackage__Script_EnhancedInput(), TEXT("BlueprintInputDebugKeyDelegateBinding"), sizeof(FBlueprintInputDebugKeyDelegateBinding), Get_Z_Construct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding_Hash());
	}
	return Singleton;
}
template<> ENHANCEDINPUT_API UScriptStruct* StaticStruct<FBlueprintInputDebugKeyDelegateBinding>()
{
	return FBlueprintInputDebugKeyDelegateBinding::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding(FBlueprintInputDebugKeyDelegateBinding::StaticStruct, TEXT("/Script/EnhancedInput"), TEXT("BlueprintInputDebugKeyDelegateBinding"), false, nullptr, nullptr);
static struct FScriptStruct_EnhancedInput_StaticRegisterNativesFBlueprintInputDebugKeyDelegateBinding
{
	FScriptStruct_EnhancedInput_StaticRegisterNativesFBlueprintInputDebugKeyDelegateBinding()
	{
		UScriptStruct::DeferCppStructOps<FBlueprintInputDebugKeyDelegateBinding>(FName(TEXT("BlueprintInputDebugKeyDelegateBinding")));
	}
} ScriptStruct_EnhancedInput_StaticRegisterNativesFBlueprintInputDebugKeyDelegateBinding;
	struct Z_Construct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputChord_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InputChord;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputKeyEvent_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InputKeyEvent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FunctionNameToBind_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_FunctionNameToBind;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bExecuteWhenPaused_MetaData[];
#endif
		static void NewProp_bExecuteWhenPaused_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bExecuteWhenPaused;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/InputDebugKeyDelegateBinding.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FBlueprintInputDebugKeyDelegateBinding>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding_Statics::NewProp_InputChord_MetaData[] = {
		{ "ModuleRelativePath", "Public/InputDebugKeyDelegateBinding.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding_Statics::NewProp_InputChord = { "InputChord", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBlueprintInputDebugKeyDelegateBinding, InputChord), Z_Construct_UScriptStruct_FInputChord, METADATA_PARAMS(Z_Construct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding_Statics::NewProp_InputChord_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding_Statics::NewProp_InputChord_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding_Statics::NewProp_InputKeyEvent_MetaData[] = {
		{ "ModuleRelativePath", "Public/InputDebugKeyDelegateBinding.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding_Statics::NewProp_InputKeyEvent = { "InputKeyEvent", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBlueprintInputDebugKeyDelegateBinding, InputKeyEvent), Z_Construct_UEnum_Engine_EInputEvent, METADATA_PARAMS(Z_Construct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding_Statics::NewProp_InputKeyEvent_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding_Statics::NewProp_InputKeyEvent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding_Statics::NewProp_FunctionNameToBind_MetaData[] = {
		{ "ModuleRelativePath", "Public/InputDebugKeyDelegateBinding.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding_Statics::NewProp_FunctionNameToBind = { "FunctionNameToBind", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBlueprintInputDebugKeyDelegateBinding, FunctionNameToBind), METADATA_PARAMS(Z_Construct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding_Statics::NewProp_FunctionNameToBind_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding_Statics::NewProp_FunctionNameToBind_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding_Statics::NewProp_bExecuteWhenPaused_MetaData[] = {
		{ "ModuleRelativePath", "Public/InputDebugKeyDelegateBinding.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding_Statics::NewProp_bExecuteWhenPaused_SetBit(void* Obj)
	{
		((FBlueprintInputDebugKeyDelegateBinding*)Obj)->bExecuteWhenPaused = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding_Statics::NewProp_bExecuteWhenPaused = { "bExecuteWhenPaused", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FBlueprintInputDebugKeyDelegateBinding), &Z_Construct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding_Statics::NewProp_bExecuteWhenPaused_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding_Statics::NewProp_bExecuteWhenPaused_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding_Statics::NewProp_bExecuteWhenPaused_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding_Statics::NewProp_InputChord,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding_Statics::NewProp_InputKeyEvent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding_Statics::NewProp_FunctionNameToBind,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding_Statics::NewProp_bExecuteWhenPaused,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_EnhancedInput,
		nullptr,
		&NewStructOps,
		"BlueprintInputDebugKeyDelegateBinding",
		sizeof(FBlueprintInputDebugKeyDelegateBinding),
		alignof(FBlueprintInputDebugKeyDelegateBinding),
		Z_Construct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_EnhancedInput();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("BlueprintInputDebugKeyDelegateBinding"), sizeof(FBlueprintInputDebugKeyDelegateBinding), Get_Z_Construct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding_Hash() { return 3465064398U; }
	void UInputDebugKeyDelegateBinding::StaticRegisterNativesUInputDebugKeyDelegateBinding()
	{
	}
	UClass* Z_Construct_UClass_UInputDebugKeyDelegateBinding_NoRegister()
	{
		return UInputDebugKeyDelegateBinding::StaticClass();
	}
	struct Z_Construct_UClass_UInputDebugKeyDelegateBinding_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InputDebugKeyDelegateBindings_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputDebugKeyDelegateBindings_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_InputDebugKeyDelegateBindings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UInputDebugKeyDelegateBinding_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInputDelegateBinding,
		(UObject* (*)())Z_Construct_UPackage__Script_EnhancedInput,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputDebugKeyDelegateBinding_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "InputDebugKeyDelegateBinding.h" },
		{ "ModuleRelativePath", "Public/InputDebugKeyDelegateBinding.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UInputDebugKeyDelegateBinding_Statics::NewProp_InputDebugKeyDelegateBindings_Inner = { "InputDebugKeyDelegateBindings", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FBlueprintInputDebugKeyDelegateBinding, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputDebugKeyDelegateBinding_Statics::NewProp_InputDebugKeyDelegateBindings_MetaData[] = {
		{ "ModuleRelativePath", "Public/InputDebugKeyDelegateBinding.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UInputDebugKeyDelegateBinding_Statics::NewProp_InputDebugKeyDelegateBindings = { "InputDebugKeyDelegateBindings", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UInputDebugKeyDelegateBinding, InputDebugKeyDelegateBindings), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UInputDebugKeyDelegateBinding_Statics::NewProp_InputDebugKeyDelegateBindings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UInputDebugKeyDelegateBinding_Statics::NewProp_InputDebugKeyDelegateBindings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UInputDebugKeyDelegateBinding_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputDebugKeyDelegateBinding_Statics::NewProp_InputDebugKeyDelegateBindings_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputDebugKeyDelegateBinding_Statics::NewProp_InputDebugKeyDelegateBindings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UInputDebugKeyDelegateBinding_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UInputDebugKeyDelegateBinding>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UInputDebugKeyDelegateBinding_Statics::ClassParams = {
		&UInputDebugKeyDelegateBinding::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UInputDebugKeyDelegateBinding_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UInputDebugKeyDelegateBinding_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UInputDebugKeyDelegateBinding_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UInputDebugKeyDelegateBinding_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UInputDebugKeyDelegateBinding()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UInputDebugKeyDelegateBinding_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UInputDebugKeyDelegateBinding, 150013208);
	template<> ENHANCEDINPUT_API UClass* StaticClass<UInputDebugKeyDelegateBinding>()
	{
		return UInputDebugKeyDelegateBinding::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UInputDebugKeyDelegateBinding(Z_Construct_UClass_UInputDebugKeyDelegateBinding, &UInputDebugKeyDelegateBinding::StaticClass, TEXT("/Script/EnhancedInput"), TEXT("UInputDebugKeyDelegateBinding"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UInputDebugKeyDelegateBinding);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
