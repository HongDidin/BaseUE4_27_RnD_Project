// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef INPUTEDITOR_InputEditorModule_generated_h
#error "InputEditorModule.generated.h already included, missing '#pragma once' in InputEditorModule.h"
#endif
#define INPUTEDITOR_InputEditorModule_generated_h

#define Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_17_SPARSE_DATA
#define Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_17_RPC_WRAPPERS
#define Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUInputMappingContext_Factory(); \
	friend struct Z_Construct_UClass_UInputMappingContext_Factory_Statics; \
public: \
	DECLARE_CLASS(UInputMappingContext_Factory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/InputEditor"), NO_API) \
	DECLARE_SERIALIZER(UInputMappingContext_Factory)


#define Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUInputMappingContext_Factory(); \
	friend struct Z_Construct_UClass_UInputMappingContext_Factory_Statics; \
public: \
	DECLARE_CLASS(UInputMappingContext_Factory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/InputEditor"), NO_API) \
	DECLARE_SERIALIZER(UInputMappingContext_Factory)


#define Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UInputMappingContext_Factory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInputMappingContext_Factory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UInputMappingContext_Factory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInputMappingContext_Factory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UInputMappingContext_Factory(UInputMappingContext_Factory&&); \
	NO_API UInputMappingContext_Factory(const UInputMappingContext_Factory&); \
public:


#define Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UInputMappingContext_Factory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UInputMappingContext_Factory(UInputMappingContext_Factory&&); \
	NO_API UInputMappingContext_Factory(const UInputMappingContext_Factory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UInputMappingContext_Factory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInputMappingContext_Factory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInputMappingContext_Factory)


#define Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_14_PROLOG
#define Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_17_RPC_WRAPPERS \
	Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_17_INCLASS \
	Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_17_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class InputMappingContext_Factory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> INPUTEDITOR_API UClass* StaticClass<class UInputMappingContext_Factory>();

#define Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_25_SPARSE_DATA
#define Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_25_RPC_WRAPPERS
#define Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_25_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_25_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUInputAction_Factory(); \
	friend struct Z_Construct_UClass_UInputAction_Factory_Statics; \
public: \
	DECLARE_CLASS(UInputAction_Factory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/InputEditor"), NO_API) \
	DECLARE_SERIALIZER(UInputAction_Factory)


#define Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_25_INCLASS \
private: \
	static void StaticRegisterNativesUInputAction_Factory(); \
	friend struct Z_Construct_UClass_UInputAction_Factory_Statics; \
public: \
	DECLARE_CLASS(UInputAction_Factory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/InputEditor"), NO_API) \
	DECLARE_SERIALIZER(UInputAction_Factory)


#define Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_25_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UInputAction_Factory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInputAction_Factory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UInputAction_Factory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInputAction_Factory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UInputAction_Factory(UInputAction_Factory&&); \
	NO_API UInputAction_Factory(const UInputAction_Factory&); \
public:


#define Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_25_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UInputAction_Factory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UInputAction_Factory(UInputAction_Factory&&); \
	NO_API UInputAction_Factory(const UInputAction_Factory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UInputAction_Factory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInputAction_Factory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInputAction_Factory)


#define Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_25_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_22_PROLOG
#define Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_25_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_25_SPARSE_DATA \
	Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_25_RPC_WRAPPERS \
	Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_25_INCLASS \
	Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_25_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_25_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_25_SPARSE_DATA \
	Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_25_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_25_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h_25_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class InputAction_Factory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> INPUTEDITOR_API UClass* StaticClass<class UInputAction_Factory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_EnhancedInput_Source_InputEditor_Public_InputEditorModule_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
