// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UInputAction;
struct FKey;
class UInputMappingContext;
struct FMappingQueryIssue;
enum class EMappingQueryIssue : uint8;
enum class EMappingQueryResult : uint8;
#ifdef ENHANCEDINPUT_EnhancedInputSubsystemInterface_generated_h
#error "EnhancedInputSubsystemInterface.generated.h already included, missing '#pragma once' in EnhancedInputSubsystemInterface.h"
#endif
#define ENHANCEDINPUT_EnhancedInputSubsystemInterface_generated_h

#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputSubsystemInterface_h_21_SPARSE_DATA
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputSubsystemInterface_h_21_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execQueryKeysMappedToAction); \
	DECLARE_FUNCTION(execHasMappingContext); \
	DECLARE_FUNCTION(execQueryMapKeyInContextSet); \
	DECLARE_FUNCTION(execQueryMapKeyInActiveContextSet); \
	DECLARE_FUNCTION(execRequestRebuildControlMappings); \
	DECLARE_FUNCTION(execRemoveMappingContext); \
	DECLARE_FUNCTION(execAddMappingContext); \
	DECLARE_FUNCTION(execClearAllMappings);


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputSubsystemInterface_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execQueryKeysMappedToAction); \
	DECLARE_FUNCTION(execHasMappingContext); \
	DECLARE_FUNCTION(execQueryMapKeyInContextSet); \
	DECLARE_FUNCTION(execQueryMapKeyInActiveContextSet); \
	DECLARE_FUNCTION(execRequestRebuildControlMappings); \
	DECLARE_FUNCTION(execRemoveMappingContext); \
	DECLARE_FUNCTION(execAddMappingContext); \
	DECLARE_FUNCTION(execClearAllMappings);


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputSubsystemInterface_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ENHANCEDINPUT_API UEnhancedInputSubsystemInterface(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEnhancedInputSubsystemInterface) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ENHANCEDINPUT_API, UEnhancedInputSubsystemInterface); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEnhancedInputSubsystemInterface); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ENHANCEDINPUT_API UEnhancedInputSubsystemInterface(UEnhancedInputSubsystemInterface&&); \
	ENHANCEDINPUT_API UEnhancedInputSubsystemInterface(const UEnhancedInputSubsystemInterface&); \
public:


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputSubsystemInterface_h_21_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ENHANCEDINPUT_API UEnhancedInputSubsystemInterface(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ENHANCEDINPUT_API UEnhancedInputSubsystemInterface(UEnhancedInputSubsystemInterface&&); \
	ENHANCEDINPUT_API UEnhancedInputSubsystemInterface(const UEnhancedInputSubsystemInterface&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ENHANCEDINPUT_API, UEnhancedInputSubsystemInterface); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEnhancedInputSubsystemInterface); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEnhancedInputSubsystemInterface)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputSubsystemInterface_h_21_GENERATED_UINTERFACE_BODY() \
private: \
	static void StaticRegisterNativesUEnhancedInputSubsystemInterface(); \
	friend struct Z_Construct_UClass_UEnhancedInputSubsystemInterface_Statics; \
public: \
	DECLARE_CLASS(UEnhancedInputSubsystemInterface, UInterface, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), CASTCLASS_None, TEXT("/Script/EnhancedInput"), ENHANCEDINPUT_API) \
	DECLARE_SERIALIZER(UEnhancedInputSubsystemInterface)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputSubsystemInterface_h_21_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputSubsystemInterface_h_21_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputSubsystemInterface_h_21_STANDARD_CONSTRUCTORS \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputSubsystemInterface_h_21_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputSubsystemInterface_h_21_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputSubsystemInterface_h_21_ENHANCED_CONSTRUCTORS \
private: \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputSubsystemInterface_h_21_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~IEnhancedInputSubsystemInterface() {} \
public: \
	typedef UEnhancedInputSubsystemInterface UClassType; \
	typedef IEnhancedInputSubsystemInterface ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputSubsystemInterface_h_21_INCLASS_IINTERFACE \
protected: \
	virtual ~IEnhancedInputSubsystemInterface() {} \
public: \
	typedef UEnhancedInputSubsystemInterface UClassType; \
	typedef IEnhancedInputSubsystemInterface ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputSubsystemInterface_h_18_PROLOG
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputSubsystemInterface_h_29_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputSubsystemInterface_h_21_SPARSE_DATA \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputSubsystemInterface_h_21_RPC_WRAPPERS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputSubsystemInterface_h_21_INCLASS_IINTERFACE \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputSubsystemInterface_h_29_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputSubsystemInterface_h_21_SPARSE_DATA \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputSubsystemInterface_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputSubsystemInterface_h_21_INCLASS_IINTERFACE_NO_PURE_DECLS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ENHANCEDINPUT_API UClass* StaticClass<class UEnhancedInputSubsystemInterface>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputSubsystemInterface_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
