// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "EnhancedInput/Public/EnhancedInputActionDelegateBinding.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEnhancedInputActionDelegateBinding() {}
// Cross Module References
	ENHANCEDINPUT_API UScriptStruct* Z_Construct_UScriptStruct_FBlueprintEnhancedInputActionBinding();
	UPackage* Z_Construct_UPackage__Script_EnhancedInput();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputAction_NoRegister();
	ENHANCEDINPUT_API UEnum* Z_Construct_UEnum_EnhancedInput_ETriggerEvent();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UEnhancedInputActionDelegateBinding_NoRegister();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UEnhancedInputActionDelegateBinding();
	ENGINE_API UClass* Z_Construct_UClass_UInputDelegateBinding();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UEnhancedInputActionValueBinding_NoRegister();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UEnhancedInputActionValueBinding();
// End Cross Module References
class UScriptStruct* FBlueprintEnhancedInputActionBinding::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ENHANCEDINPUT_API uint32 Get_Z_Construct_UScriptStruct_FBlueprintEnhancedInputActionBinding_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FBlueprintEnhancedInputActionBinding, Z_Construct_UPackage__Script_EnhancedInput(), TEXT("BlueprintEnhancedInputActionBinding"), sizeof(FBlueprintEnhancedInputActionBinding), Get_Z_Construct_UScriptStruct_FBlueprintEnhancedInputActionBinding_Hash());
	}
	return Singleton;
}
template<> ENHANCEDINPUT_API UScriptStruct* StaticStruct<FBlueprintEnhancedInputActionBinding>()
{
	return FBlueprintEnhancedInputActionBinding::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FBlueprintEnhancedInputActionBinding(FBlueprintEnhancedInputActionBinding::StaticStruct, TEXT("/Script/EnhancedInput"), TEXT("BlueprintEnhancedInputActionBinding"), false, nullptr, nullptr);
static struct FScriptStruct_EnhancedInput_StaticRegisterNativesFBlueprintEnhancedInputActionBinding
{
	FScriptStruct_EnhancedInput_StaticRegisterNativesFBlueprintEnhancedInputActionBinding()
	{
		UScriptStruct::DeferCppStructOps<FBlueprintEnhancedInputActionBinding>(FName(TEXT("BlueprintEnhancedInputActionBinding")));
	}
} ScriptStruct_EnhancedInput_StaticRegisterNativesFBlueprintEnhancedInputActionBinding;
	struct Z_Construct_UScriptStruct_FBlueprintEnhancedInputActionBinding_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputAction_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InputAction;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TriggerEvent_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TriggerEvent_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TriggerEvent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FunctionNameToBind_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_FunctionNameToBind;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBlueprintEnhancedInputActionBinding_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/EnhancedInputActionDelegateBinding.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FBlueprintEnhancedInputActionBinding_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FBlueprintEnhancedInputActionBinding>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBlueprintEnhancedInputActionBinding_Statics::NewProp_InputAction_MetaData[] = {
		{ "ModuleRelativePath", "Public/EnhancedInputActionDelegateBinding.h" },
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FBlueprintEnhancedInputActionBinding_Statics::NewProp_InputAction = { "InputAction", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBlueprintEnhancedInputActionBinding, InputAction), Z_Construct_UClass_UInputAction_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FBlueprintEnhancedInputActionBinding_Statics::NewProp_InputAction_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBlueprintEnhancedInputActionBinding_Statics::NewProp_InputAction_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FBlueprintEnhancedInputActionBinding_Statics::NewProp_TriggerEvent_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBlueprintEnhancedInputActionBinding_Statics::NewProp_TriggerEvent_MetaData[] = {
		{ "ModuleRelativePath", "Public/EnhancedInputActionDelegateBinding.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FBlueprintEnhancedInputActionBinding_Statics::NewProp_TriggerEvent = { "TriggerEvent", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBlueprintEnhancedInputActionBinding, TriggerEvent), Z_Construct_UEnum_EnhancedInput_ETriggerEvent, METADATA_PARAMS(Z_Construct_UScriptStruct_FBlueprintEnhancedInputActionBinding_Statics::NewProp_TriggerEvent_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBlueprintEnhancedInputActionBinding_Statics::NewProp_TriggerEvent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBlueprintEnhancedInputActionBinding_Statics::NewProp_FunctionNameToBind_MetaData[] = {
		{ "ModuleRelativePath", "Public/EnhancedInputActionDelegateBinding.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FBlueprintEnhancedInputActionBinding_Statics::NewProp_FunctionNameToBind = { "FunctionNameToBind", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBlueprintEnhancedInputActionBinding, FunctionNameToBind), METADATA_PARAMS(Z_Construct_UScriptStruct_FBlueprintEnhancedInputActionBinding_Statics::NewProp_FunctionNameToBind_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBlueprintEnhancedInputActionBinding_Statics::NewProp_FunctionNameToBind_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FBlueprintEnhancedInputActionBinding_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBlueprintEnhancedInputActionBinding_Statics::NewProp_InputAction,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBlueprintEnhancedInputActionBinding_Statics::NewProp_TriggerEvent_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBlueprintEnhancedInputActionBinding_Statics::NewProp_TriggerEvent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBlueprintEnhancedInputActionBinding_Statics::NewProp_FunctionNameToBind,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FBlueprintEnhancedInputActionBinding_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_EnhancedInput,
		nullptr,
		&NewStructOps,
		"BlueprintEnhancedInputActionBinding",
		sizeof(FBlueprintEnhancedInputActionBinding),
		alignof(FBlueprintEnhancedInputActionBinding),
		Z_Construct_UScriptStruct_FBlueprintEnhancedInputActionBinding_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBlueprintEnhancedInputActionBinding_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FBlueprintEnhancedInputActionBinding_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBlueprintEnhancedInputActionBinding_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FBlueprintEnhancedInputActionBinding()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FBlueprintEnhancedInputActionBinding_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_EnhancedInput();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("BlueprintEnhancedInputActionBinding"), sizeof(FBlueprintEnhancedInputActionBinding), Get_Z_Construct_UScriptStruct_FBlueprintEnhancedInputActionBinding_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FBlueprintEnhancedInputActionBinding_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FBlueprintEnhancedInputActionBinding_Hash() { return 1402411517U; }
	void UEnhancedInputActionDelegateBinding::StaticRegisterNativesUEnhancedInputActionDelegateBinding()
	{
	}
	UClass* Z_Construct_UClass_UEnhancedInputActionDelegateBinding_NoRegister()
	{
		return UEnhancedInputActionDelegateBinding::StaticClass();
	}
	struct Z_Construct_UClass_UEnhancedInputActionDelegateBinding_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InputActionDelegateBindings_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputActionDelegateBindings_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_InputActionDelegateBindings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEnhancedInputActionDelegateBinding_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInputDelegateBinding,
		(UObject* (*)())Z_Construct_UPackage__Script_EnhancedInput,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEnhancedInputActionDelegateBinding_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "EnhancedInputActionDelegateBinding.h" },
		{ "ModuleRelativePath", "Public/EnhancedInputActionDelegateBinding.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UEnhancedInputActionDelegateBinding_Statics::NewProp_InputActionDelegateBindings_Inner = { "InputActionDelegateBindings", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FBlueprintEnhancedInputActionBinding, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEnhancedInputActionDelegateBinding_Statics::NewProp_InputActionDelegateBindings_MetaData[] = {
		{ "ModuleRelativePath", "Public/EnhancedInputActionDelegateBinding.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UEnhancedInputActionDelegateBinding_Statics::NewProp_InputActionDelegateBindings = { "InputActionDelegateBindings", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEnhancedInputActionDelegateBinding, InputActionDelegateBindings), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UEnhancedInputActionDelegateBinding_Statics::NewProp_InputActionDelegateBindings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEnhancedInputActionDelegateBinding_Statics::NewProp_InputActionDelegateBindings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UEnhancedInputActionDelegateBinding_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEnhancedInputActionDelegateBinding_Statics::NewProp_InputActionDelegateBindings_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEnhancedInputActionDelegateBinding_Statics::NewProp_InputActionDelegateBindings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEnhancedInputActionDelegateBinding_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEnhancedInputActionDelegateBinding>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEnhancedInputActionDelegateBinding_Statics::ClassParams = {
		&UEnhancedInputActionDelegateBinding::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UEnhancedInputActionDelegateBinding_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UEnhancedInputActionDelegateBinding_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UEnhancedInputActionDelegateBinding_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEnhancedInputActionDelegateBinding_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEnhancedInputActionDelegateBinding()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEnhancedInputActionDelegateBinding_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEnhancedInputActionDelegateBinding, 1890257034);
	template<> ENHANCEDINPUT_API UClass* StaticClass<UEnhancedInputActionDelegateBinding>()
	{
		return UEnhancedInputActionDelegateBinding::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEnhancedInputActionDelegateBinding(Z_Construct_UClass_UEnhancedInputActionDelegateBinding, &UEnhancedInputActionDelegateBinding::StaticClass, TEXT("/Script/EnhancedInput"), TEXT("UEnhancedInputActionDelegateBinding"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEnhancedInputActionDelegateBinding);
	void UEnhancedInputActionValueBinding::StaticRegisterNativesUEnhancedInputActionValueBinding()
	{
	}
	UClass* Z_Construct_UClass_UEnhancedInputActionValueBinding_NoRegister()
	{
		return UEnhancedInputActionValueBinding::StaticClass();
	}
	struct Z_Construct_UClass_UEnhancedInputActionValueBinding_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InputActionValueBindings_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputActionValueBindings_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_InputActionValueBindings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEnhancedInputActionValueBinding_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInputDelegateBinding,
		(UObject* (*)())Z_Construct_UPackage__Script_EnhancedInput,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEnhancedInputActionValueBinding_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "EnhancedInputActionDelegateBinding.h" },
		{ "ModuleRelativePath", "Public/EnhancedInputActionDelegateBinding.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UEnhancedInputActionValueBinding_Statics::NewProp_InputActionValueBindings_Inner = { "InputActionValueBindings", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FBlueprintEnhancedInputActionBinding, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEnhancedInputActionValueBinding_Statics::NewProp_InputActionValueBindings_MetaData[] = {
		{ "ModuleRelativePath", "Public/EnhancedInputActionDelegateBinding.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UEnhancedInputActionValueBinding_Statics::NewProp_InputActionValueBindings = { "InputActionValueBindings", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEnhancedInputActionValueBinding, InputActionValueBindings), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UEnhancedInputActionValueBinding_Statics::NewProp_InputActionValueBindings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEnhancedInputActionValueBinding_Statics::NewProp_InputActionValueBindings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UEnhancedInputActionValueBinding_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEnhancedInputActionValueBinding_Statics::NewProp_InputActionValueBindings_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEnhancedInputActionValueBinding_Statics::NewProp_InputActionValueBindings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEnhancedInputActionValueBinding_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEnhancedInputActionValueBinding>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEnhancedInputActionValueBinding_Statics::ClassParams = {
		&UEnhancedInputActionValueBinding::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UEnhancedInputActionValueBinding_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UEnhancedInputActionValueBinding_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UEnhancedInputActionValueBinding_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEnhancedInputActionValueBinding_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEnhancedInputActionValueBinding()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEnhancedInputActionValueBinding_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEnhancedInputActionValueBinding, 309570972);
	template<> ENHANCEDINPUT_API UClass* StaticClass<UEnhancedInputActionValueBinding>()
	{
		return UEnhancedInputActionValueBinding::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEnhancedInputActionValueBinding(Z_Construct_UClass_UEnhancedInputActionValueBinding, &UEnhancedInputActionValueBinding::StaticClass, TEXT("/Script/EnhancedInput"), TEXT("UEnhancedInputActionValueBinding"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEnhancedInputActionValueBinding);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
