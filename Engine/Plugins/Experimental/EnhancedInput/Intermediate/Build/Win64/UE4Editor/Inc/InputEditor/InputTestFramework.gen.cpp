// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "InputEditor/Private/Tests/InputTestFramework.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeInputTestFramework() {}
// Cross Module References
	INPUTEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FBindingTargets();
	UPackage* Z_Construct_UPackage__Script_InputEditor();
	INPUTEDITOR_API UClass* Z_Construct_UClass_UInputBindingTarget_NoRegister();
	INPUTEDITOR_API UClass* Z_Construct_UClass_UInputBindingTarget();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	INPUTEDITOR_API UClass* Z_Construct_UClass_UControllablePlayer_NoRegister();
	INPUTEDITOR_API UClass* Z_Construct_UClass_UControllablePlayer();
	ENGINE_API UClass* Z_Construct_UClass_APlayerController_NoRegister();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputMappingContext_NoRegister();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputAction_NoRegister();
// End Cross Module References
class UScriptStruct* FBindingTargets::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern INPUTEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FBindingTargets_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FBindingTargets, Z_Construct_UPackage__Script_InputEditor(), TEXT("BindingTargets"), sizeof(FBindingTargets), Get_Z_Construct_UScriptStruct_FBindingTargets_Hash());
	}
	return Singleton;
}
template<> INPUTEDITOR_API UScriptStruct* StaticStruct<FBindingTargets>()
{
	return FBindingTargets::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FBindingTargets(FBindingTargets::StaticStruct, TEXT("/Script/InputEditor"), TEXT("BindingTargets"), false, nullptr, nullptr);
static struct FScriptStruct_InputEditor_StaticRegisterNativesFBindingTargets
{
	FScriptStruct_InputEditor_StaticRegisterNativesFBindingTargets()
	{
		UScriptStruct::DeferCppStructOps<FBindingTargets>(FName(TEXT("BindingTargets")));
	}
} ScriptStruct_InputEditor_StaticRegisterNativesFBindingTargets;
	struct Z_Construct_UScriptStruct_FBindingTargets_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Started_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Started;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Ongoing_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Ongoing;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Canceled_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Canceled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Completed_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Completed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Triggered_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Triggered;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBindingTargets_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Tests/InputTestFramework.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FBindingTargets_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FBindingTargets>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBindingTargets_Statics::NewProp_Started_MetaData[] = {
		{ "ModuleRelativePath", "Private/Tests/InputTestFramework.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FBindingTargets_Statics::NewProp_Started = { "Started", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBindingTargets, Started), Z_Construct_UClass_UInputBindingTarget_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FBindingTargets_Statics::NewProp_Started_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBindingTargets_Statics::NewProp_Started_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBindingTargets_Statics::NewProp_Ongoing_MetaData[] = {
		{ "ModuleRelativePath", "Private/Tests/InputTestFramework.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FBindingTargets_Statics::NewProp_Ongoing = { "Ongoing", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBindingTargets, Ongoing), Z_Construct_UClass_UInputBindingTarget_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FBindingTargets_Statics::NewProp_Ongoing_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBindingTargets_Statics::NewProp_Ongoing_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBindingTargets_Statics::NewProp_Canceled_MetaData[] = {
		{ "ModuleRelativePath", "Private/Tests/InputTestFramework.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FBindingTargets_Statics::NewProp_Canceled = { "Canceled", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBindingTargets, Canceled), Z_Construct_UClass_UInputBindingTarget_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FBindingTargets_Statics::NewProp_Canceled_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBindingTargets_Statics::NewProp_Canceled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBindingTargets_Statics::NewProp_Completed_MetaData[] = {
		{ "ModuleRelativePath", "Private/Tests/InputTestFramework.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FBindingTargets_Statics::NewProp_Completed = { "Completed", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBindingTargets, Completed), Z_Construct_UClass_UInputBindingTarget_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FBindingTargets_Statics::NewProp_Completed_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBindingTargets_Statics::NewProp_Completed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBindingTargets_Statics::NewProp_Triggered_MetaData[] = {
		{ "ModuleRelativePath", "Private/Tests/InputTestFramework.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FBindingTargets_Statics::NewProp_Triggered = { "Triggered", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBindingTargets, Triggered), Z_Construct_UClass_UInputBindingTarget_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FBindingTargets_Statics::NewProp_Triggered_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBindingTargets_Statics::NewProp_Triggered_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FBindingTargets_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBindingTargets_Statics::NewProp_Started,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBindingTargets_Statics::NewProp_Ongoing,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBindingTargets_Statics::NewProp_Canceled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBindingTargets_Statics::NewProp_Completed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBindingTargets_Statics::NewProp_Triggered,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FBindingTargets_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_InputEditor,
		nullptr,
		&NewStructOps,
		"BindingTargets",
		sizeof(FBindingTargets),
		alignof(FBindingTargets),
		Z_Construct_UScriptStruct_FBindingTargets_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBindingTargets_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FBindingTargets_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBindingTargets_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FBindingTargets()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FBindingTargets_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_InputEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("BindingTargets"), sizeof(FBindingTargets), Get_Z_Construct_UScriptStruct_FBindingTargets_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FBindingTargets_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FBindingTargets_Hash() { return 3437162178U; }
	void UInputBindingTarget::StaticRegisterNativesUInputBindingTarget()
	{
	}
	UClass* Z_Construct_UClass_UInputBindingTarget_NoRegister()
	{
		return UInputBindingTarget::StaticClass();
	}
	struct Z_Construct_UClass_UInputBindingTarget_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UInputBindingTarget_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_InputEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputBindingTarget_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Tests/InputTestFramework.h" },
		{ "ModuleRelativePath", "Private/Tests/InputTestFramework.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UInputBindingTarget_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UInputBindingTarget>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UInputBindingTarget_Statics::ClassParams = {
		&UInputBindingTarget::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UInputBindingTarget_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UInputBindingTarget_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UInputBindingTarget()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UInputBindingTarget_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UInputBindingTarget, 1396696896);
	template<> INPUTEDITOR_API UClass* StaticClass<UInputBindingTarget>()
	{
		return UInputBindingTarget::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UInputBindingTarget(Z_Construct_UClass_UInputBindingTarget, &UInputBindingTarget::StaticClass, TEXT("/Script/InputEditor"), TEXT("UInputBindingTarget"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UInputBindingTarget);
	void UControllablePlayer::StaticRegisterNativesUControllablePlayer()
	{
	}
	UClass* Z_Construct_UClass_UControllablePlayer_NoRegister()
	{
		return UControllablePlayer::StaticClass();
	}
	struct Z_Construct_UClass_UControllablePlayer_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Player_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Player;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BindingTargets_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_BindingTargets_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BindingTargets_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_BindingTargets;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InputContext_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_InputContext_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputContext_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_InputContext;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InputAction_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_InputAction_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputAction_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_InputAction;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UControllablePlayer_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_InputEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControllablePlayer_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Tests/InputTestFramework.h" },
		{ "ModuleRelativePath", "Private/Tests/InputTestFramework.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControllablePlayer_Statics::NewProp_Player_MetaData[] = {
		{ "ModuleRelativePath", "Private/Tests/InputTestFramework.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UControllablePlayer_Statics::NewProp_Player = { "Player", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControllablePlayer, Player), Z_Construct_UClass_APlayerController_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UControllablePlayer_Statics::NewProp_Player_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControllablePlayer_Statics::NewProp_Player_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UControllablePlayer_Statics::NewProp_BindingTargets_ValueProp = { "BindingTargets", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FBindingTargets, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UControllablePlayer_Statics::NewProp_BindingTargets_Key_KeyProp = { "BindingTargets_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControllablePlayer_Statics::NewProp_BindingTargets_MetaData[] = {
		{ "ModuleRelativePath", "Private/Tests/InputTestFramework.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UControllablePlayer_Statics::NewProp_BindingTargets = { "BindingTargets", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControllablePlayer, BindingTargets), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UControllablePlayer_Statics::NewProp_BindingTargets_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControllablePlayer_Statics::NewProp_BindingTargets_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UControllablePlayer_Statics::NewProp_InputContext_ValueProp = { "InputContext", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_UInputMappingContext_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UControllablePlayer_Statics::NewProp_InputContext_Key_KeyProp = { "InputContext_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControllablePlayer_Statics::NewProp_InputContext_MetaData[] = {
		{ "Comment", "// Storage for input mapping contexts applied to the player\n" },
		{ "ModuleRelativePath", "Private/Tests/InputTestFramework.h" },
		{ "ToolTip", "Storage for input mapping contexts applied to the player" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UControllablePlayer_Statics::NewProp_InputContext = { "InputContext", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControllablePlayer, InputContext), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UControllablePlayer_Statics::NewProp_InputContext_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControllablePlayer_Statics::NewProp_InputContext_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UControllablePlayer_Statics::NewProp_InputAction_ValueProp = { "InputAction", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_UInputAction_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UControllablePlayer_Statics::NewProp_InputAction_Key_KeyProp = { "InputAction_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControllablePlayer_Statics::NewProp_InputAction_MetaData[] = {
		{ "Comment", "// Storage for input actions applied to the player\n" },
		{ "ModuleRelativePath", "Private/Tests/InputTestFramework.h" },
		{ "ToolTip", "Storage for input actions applied to the player" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UControllablePlayer_Statics::NewProp_InputAction = { "InputAction", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControllablePlayer, InputAction), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UControllablePlayer_Statics::NewProp_InputAction_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControllablePlayer_Statics::NewProp_InputAction_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UControllablePlayer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControllablePlayer_Statics::NewProp_Player,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControllablePlayer_Statics::NewProp_BindingTargets_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControllablePlayer_Statics::NewProp_BindingTargets_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControllablePlayer_Statics::NewProp_BindingTargets,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControllablePlayer_Statics::NewProp_InputContext_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControllablePlayer_Statics::NewProp_InputContext_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControllablePlayer_Statics::NewProp_InputContext,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControllablePlayer_Statics::NewProp_InputAction_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControllablePlayer_Statics::NewProp_InputAction_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControllablePlayer_Statics::NewProp_InputAction,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UControllablePlayer_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UControllablePlayer>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UControllablePlayer_Statics::ClassParams = {
		&UControllablePlayer::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UControllablePlayer_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UControllablePlayer_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UControllablePlayer_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UControllablePlayer_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UControllablePlayer()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UControllablePlayer_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UControllablePlayer, 1910707944);
	template<> INPUTEDITOR_API UClass* StaticClass<UControllablePlayer>()
	{
		return UControllablePlayer::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UControllablePlayer(Z_Construct_UClass_UControllablePlayer, &UControllablePlayer::StaticClass, TEXT("/Script/InputEditor"), TEXT("UControllablePlayer"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UControllablePlayer);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
