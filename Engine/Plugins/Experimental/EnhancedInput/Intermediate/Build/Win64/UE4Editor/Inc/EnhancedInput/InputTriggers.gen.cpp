// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "EnhancedInput/Public/InputTriggers.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeInputTriggers() {}
// Cross Module References
	ENHANCEDINPUT_API UEnum* Z_Construct_UEnum_EnhancedInput_ETriggerType();
	UPackage* Z_Construct_UPackage__Script_EnhancedInput();
	ENHANCEDINPUT_API UEnum* Z_Construct_UEnum_EnhancedInput_ETriggerEvent();
	ENHANCEDINPUT_API UEnum* Z_Construct_UEnum_EnhancedInput_ETriggerState();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputTrigger_NoRegister();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputTrigger();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	ENHANCEDINPUT_API UScriptStruct* Z_Construct_UScriptStruct_FInputActionValue();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UEnhancedPlayerInput_NoRegister();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputTriggerTimedBase_NoRegister();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputTriggerTimedBase();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputTriggerDown_NoRegister();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputTriggerDown();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputTriggerPressed_NoRegister();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputTriggerPressed();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputTriggerReleased_NoRegister();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputTriggerReleased();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputTriggerHold_NoRegister();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputTriggerHold();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputTriggerHoldAndRelease_NoRegister();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputTriggerHoldAndRelease();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputTriggerTap_NoRegister();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputTriggerTap();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputTriggerPulse_NoRegister();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputTriggerPulse();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputTriggerChordAction_NoRegister();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputTriggerChordAction();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputAction_NoRegister();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputTriggerChordBlocker_NoRegister();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputTriggerChordBlocker();
// End Cross Module References
	static UEnum* ETriggerType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_EnhancedInput_ETriggerType, Z_Construct_UPackage__Script_EnhancedInput(), TEXT("ETriggerType"));
		}
		return Singleton;
	}
	template<> ENHANCEDINPUT_API UEnum* StaticEnum<ETriggerType>()
	{
		return ETriggerType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETriggerType(ETriggerType_StaticEnum, TEXT("/Script/EnhancedInput"), TEXT("ETriggerType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_EnhancedInput_ETriggerType_Hash() { return 1015983199U; }
	UEnum* Z_Construct_UEnum_EnhancedInput_ETriggerType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_EnhancedInput();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETriggerType"), 0, Get_Z_Construct_UEnum_EnhancedInput_ETriggerType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETriggerType::Explicit", (int64)ETriggerType::Explicit },
				{ "ETriggerType::Implicit", (int64)ETriggerType::Implicit },
				{ "ETriggerType::Blocker", (int64)ETriggerType::Blocker },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Blocker.Comment", "// Inverted trigger that will block all other triggers if it is triggered.\n" },
				{ "Blocker.Name", "ETriggerType::Blocker" },
				{ "Blocker.ToolTip", "Inverted trigger that will block all other triggers if it is triggered." },
				{ "Comment", "/**\n* Trigger type determine how the trigger contributes to an action's overall trigger event the behavior of the trigger\n*/" },
				{ "Explicit.Comment", "// Input may trigger if any explicit trigger is triggered.\n" },
				{ "Explicit.Name", "ETriggerType::Explicit" },
				{ "Explicit.ToolTip", "Input may trigger if any explicit trigger is triggered." },
				{ "Implicit.Comment", "// Input may trigger only if all implicit triggers are triggered.\n" },
				{ "Implicit.Name", "ETriggerType::Implicit" },
				{ "Implicit.ToolTip", "Input may trigger only if all implicit triggers are triggered." },
				{ "ModuleRelativePath", "Public/InputTriggers.h" },
				{ "ToolTip", "Trigger type determine how the trigger contributes to an action's overall trigger event the behavior of the trigger" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_EnhancedInput,
				nullptr,
				"ETriggerType",
				"ETriggerType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ETriggerEvent_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_EnhancedInput_ETriggerEvent, Z_Construct_UPackage__Script_EnhancedInput(), TEXT("ETriggerEvent"));
		}
		return Singleton;
	}
	template<> ENHANCEDINPUT_API UEnum* StaticEnum<ETriggerEvent>()
	{
		return ETriggerEvent_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETriggerEvent(ETriggerEvent_StaticEnum, TEXT("/Script/EnhancedInput"), TEXT("ETriggerEvent"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_EnhancedInput_ETriggerEvent_Hash() { return 2170919621U; }
	UEnum* Z_Construct_UEnum_EnhancedInput_ETriggerEvent()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_EnhancedInput();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETriggerEvent"), 0, Get_Z_Construct_UEnum_EnhancedInput_ETriggerEvent_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETriggerEvent::None", (int64)ETriggerEvent::None },
				{ "ETriggerEvent::Started", (int64)ETriggerEvent::Started },
				{ "ETriggerEvent::Ongoing", (int64)ETriggerEvent::Ongoing },
				{ "ETriggerEvent::Canceled", (int64)ETriggerEvent::Canceled },
				{ "ETriggerEvent::Triggered", (int64)ETriggerEvent::Triggered },
				{ "ETriggerEvent::Completed", (int64)ETriggerEvent::Completed },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Canceled.Comment", "// Triggering has been canceled\n" },
				{ "Canceled.Name", "ETriggerEvent::Canceled" },
				{ "Canceled.ToolTip", "Triggering has been canceled" },
				{ "Comment", "/**\n* Trigger events are the Action's interpretation of all Trigger State transitions that occurred for the action in the last tick\n*/" },
				{ "Completed.Comment", "// The trigger state has transitioned from Triggered to None this frame, i.e. Triggering has finished.\n// NOTE: Using this event restricts you to one set of triggers for Started/Completed events. You may prefer two actions, each with its own trigger rules.\n// TODO: Completed will not fire if any trigger reports Ongoing on the same frame, but both should fire. e.g. Tick 2 of Hold (= Ongoing) + Pressed (= None) combo will raise Ongoing event only.\n" },
				{ "Completed.Name", "ETriggerEvent::Completed" },
				{ "Completed.ToolTip", "The trigger state has transitioned from Triggered to None this frame, i.e. Triggering has finished.\nNOTE: Using this event restricts you to one set of triggers for Started/Completed events. You may prefer two actions, each with its own trigger rules.\nTODO: Completed will not fire if any trigger reports Ongoing on the same frame, but both should fire. e.g. Tick 2 of Hold (= Ongoing) + Pressed (= None) combo will raise Ongoing event only." },
				{ "ModuleRelativePath", "Public/InputTriggers.h" },
				{ "None.Comment", "// No significant trigger state changes occurred and there are no active device inputs\n" },
				{ "None.Hidden", "" },
				{ "None.Name", "ETriggerEvent::None" },
				{ "None.ToolTip", "No significant trigger state changes occurred and there are no active device inputs" },
				{ "Ongoing.Comment", "// Triggering is still being processed\n" },
				{ "Ongoing.Name", "ETriggerEvent::Ongoing" },
				{ "Ongoing.ToolTip", "Triggering is still being processed" },
				{ "Started.Comment", "// An event has occurred that has begun Trigger evaluation. Note: Triggered may also occur this frame.\n" },
				{ "Started.Name", "ETriggerEvent::Started" },
				{ "Started.ToolTip", "An event has occurred that has begun Trigger evaluation. Note: Triggered may also occur this frame." },
				{ "ToolTip", "Trigger events are the Action's interpretation of all Trigger State transitions that occurred for the action in the last tick" },
				{ "Triggered.Comment", "// Triggering occurred after one or more processing ticks\n" },
				{ "Triggered.Name", "ETriggerEvent::Triggered" },
				{ "Triggered.ToolTip", "Triggering occurred after one or more processing ticks" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_EnhancedInput,
				nullptr,
				"ETriggerEvent",
				"ETriggerEvent",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ETriggerState_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_EnhancedInput_ETriggerState, Z_Construct_UPackage__Script_EnhancedInput(), TEXT("ETriggerState"));
		}
		return Singleton;
	}
	template<> ENHANCEDINPUT_API UEnum* StaticEnum<ETriggerState>()
	{
		return ETriggerState_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETriggerState(ETriggerState_StaticEnum, TEXT("/Script/EnhancedInput"), TEXT("ETriggerState"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_EnhancedInput_ETriggerState_Hash() { return 3686710773U; }
	UEnum* Z_Construct_UEnum_EnhancedInput_ETriggerState()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_EnhancedInput();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETriggerState"), 0, Get_Z_Construct_UEnum_EnhancedInput_ETriggerState_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETriggerState::None", (int64)ETriggerState::None },
				{ "ETriggerState::Ongoing", (int64)ETriggerState::Ongoing },
				{ "ETriggerState::Triggered", (int64)ETriggerState::Triggered },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/**\n* Trigger states are a light weight interpretation of the provided input values used in trigger UpdateState responses.\n*/" },
				{ "ModuleRelativePath", "Public/InputTriggers.h" },
				{ "None.Comment", "// No inputs\n" },
				{ "None.Name", "ETriggerState::None" },
				{ "None.ToolTip", "No inputs" },
				{ "Ongoing.Comment", "// Triggering is being monitored, but not yet been confirmed (e.g. a time based trigger that requires the trigger state to be maintained over several frames)\n" },
				{ "Ongoing.Name", "ETriggerState::Ongoing" },
				{ "Ongoing.ToolTip", "Triggering is being monitored, but not yet been confirmed (e.g. a time based trigger that requires the trigger state to be maintained over several frames)" },
				{ "ToolTip", "Trigger states are a light weight interpretation of the provided input values used in trigger UpdateState responses." },
				{ "Triggered.Comment", "// The trigger state has been met\n" },
				{ "Triggered.Name", "ETriggerState::Triggered" },
				{ "Triggered.ToolTip", "The trigger state has been met" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_EnhancedInput,
				nullptr,
				"ETriggerState",
				"ETriggerState",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	DEFINE_FUNCTION(UInputTrigger::execUpdateState)
	{
		P_GET_OBJECT(UEnhancedPlayerInput,Z_Param_PlayerInput);
		P_GET_STRUCT(FInputActionValue,Z_Param_ModifiedValue);
		P_GET_PROPERTY(FFloatProperty,Z_Param_DeltaTime);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(ETriggerState*)Z_Param__Result=P_THIS->UpdateState_Implementation(Z_Param_PlayerInput,Z_Param_ModifiedValue,Z_Param_DeltaTime);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UInputTrigger::execGetTriggerType)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(ETriggerType*)Z_Param__Result=P_THIS->GetTriggerType_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UInputTrigger::execIsActuated)
	{
		P_GET_STRUCT_REF(FInputActionValue,Z_Param_Out_ForValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsActuated(Z_Param_Out_ForValue);
		P_NATIVE_END;
	}
	static FName NAME_UInputTrigger_GetTriggerType = FName(TEXT("GetTriggerType"));
	ETriggerType UInputTrigger::GetTriggerType() const
	{
		InputTrigger_eventGetTriggerType_Parms Parms;
		const_cast<UInputTrigger*>(this)->ProcessEvent(FindFunctionChecked(NAME_UInputTrigger_GetTriggerType),&Parms);
		return Parms.ReturnValue;
	}
	static FName NAME_UInputTrigger_UpdateState = FName(TEXT("UpdateState"));
	ETriggerState UInputTrigger::UpdateState(const UEnhancedPlayerInput* PlayerInput, FInputActionValue ModifiedValue, float DeltaTime)
	{
		InputTrigger_eventUpdateState_Parms Parms;
		Parms.PlayerInput=PlayerInput;
		Parms.ModifiedValue=ModifiedValue;
		Parms.DeltaTime=DeltaTime;
		ProcessEvent(FindFunctionChecked(NAME_UInputTrigger_UpdateState),&Parms);
		return Parms.ReturnValue;
	}
	void UInputTrigger::StaticRegisterNativesUInputTrigger()
	{
		UClass* Class = UInputTrigger::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetTriggerType", &UInputTrigger::execGetTriggerType },
			{ "IsActuated", &UInputTrigger::execIsActuated },
			{ "UpdateState", &UInputTrigger::execUpdateState },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UInputTrigger_GetTriggerType_Statics
	{
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UInputTrigger_GetTriggerType_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UInputTrigger_GetTriggerType_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(InputTrigger_eventGetTriggerType_Parms, ReturnValue), Z_Construct_UEnum_EnhancedInput_ETriggerType, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UInputTrigger_GetTriggerType_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UInputTrigger_GetTriggerType_Statics::NewProp_ReturnValue_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UInputTrigger_GetTriggerType_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UInputTrigger_GetTriggerType_Statics::Function_MetaDataParams[] = {
		{ "Category", "Trigger" },
		{ "Comment", "/*\n\x09""Changes the way this trigger affects an action with multiple triggers:\n\x09\x09""All implicit triggers must be triggering to trigger the action.\n\x09\x09If there are any explicit triggers at least one must be triggering to trigger the action.\n\x09*/" },
		{ "ModuleRelativePath", "Public/InputTriggers.h" },
		{ "ToolTip", "Changes the way this trigger affects an action with multiple triggers:\n        All implicit triggers must be triggering to trigger the action.\n        If there are any explicit triggers at least one must be triggering to trigger the action." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UInputTrigger_GetTriggerType_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UInputTrigger, nullptr, "GetTriggerType", nullptr, nullptr, sizeof(InputTrigger_eventGetTriggerType_Parms), Z_Construct_UFunction_UInputTrigger_GetTriggerType_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UInputTrigger_GetTriggerType_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x5C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UInputTrigger_GetTriggerType_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UInputTrigger_GetTriggerType_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UInputTrigger_GetTriggerType()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UInputTrigger_GetTriggerType_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UInputTrigger_IsActuated_Statics
	{
		struct InputTrigger_eventIsActuated_Parms
		{
			FInputActionValue ForValue;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ForValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ForValue;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UInputTrigger_IsActuated_Statics::NewProp_ForValue_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UInputTrigger_IsActuated_Statics::NewProp_ForValue = { "ForValue", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(InputTrigger_eventIsActuated_Parms, ForValue), Z_Construct_UScriptStruct_FInputActionValue, METADATA_PARAMS(Z_Construct_UFunction_UInputTrigger_IsActuated_Statics::NewProp_ForValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UInputTrigger_IsActuated_Statics::NewProp_ForValue_MetaData)) };
	void Z_Construct_UFunction_UInputTrigger_IsActuated_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((InputTrigger_eventIsActuated_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UInputTrigger_IsActuated_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(InputTrigger_eventIsActuated_Parms), &Z_Construct_UFunction_UInputTrigger_IsActuated_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UInputTrigger_IsActuated_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UInputTrigger_IsActuated_Statics::NewProp_ForValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UInputTrigger_IsActuated_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UInputTrigger_IsActuated_Statics::Function_MetaDataParams[] = {
		{ "Category", "Trigger" },
		{ "Comment", "/*\n\x09* Is the value passed in sufficiently large to be of interest to the trigger.\n\x09* This is a helper function that implements the most obvious (>=) interpretation of the actuation threshold.\n\x09*/" },
		{ "ModuleRelativePath", "Public/InputTriggers.h" },
		{ "ToolTip", "* Is the value passed in sufficiently large to be of interest to the trigger.\n* This is a helper function that implements the most obvious (>=) interpretation of the actuation threshold." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UInputTrigger_IsActuated_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UInputTrigger, nullptr, "IsActuated", nullptr, nullptr, sizeof(InputTrigger_eventIsActuated_Parms), Z_Construct_UFunction_UInputTrigger_IsActuated_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UInputTrigger_IsActuated_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UInputTrigger_IsActuated_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UInputTrigger_IsActuated_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UInputTrigger_IsActuated()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UInputTrigger_IsActuated_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UInputTrigger_UpdateState_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlayerInput_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PlayerInput;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ModifiedValue;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DeltaTime;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UInputTrigger_UpdateState_Statics::NewProp_PlayerInput_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UInputTrigger_UpdateState_Statics::NewProp_PlayerInput = { "PlayerInput", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(InputTrigger_eventUpdateState_Parms, PlayerInput), Z_Construct_UClass_UEnhancedPlayerInput_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UInputTrigger_UpdateState_Statics::NewProp_PlayerInput_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UInputTrigger_UpdateState_Statics::NewProp_PlayerInput_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UInputTrigger_UpdateState_Statics::NewProp_ModifiedValue = { "ModifiedValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(InputTrigger_eventUpdateState_Parms, ModifiedValue), Z_Construct_UScriptStruct_FInputActionValue, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UInputTrigger_UpdateState_Statics::NewProp_DeltaTime = { "DeltaTime", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(InputTrigger_eventUpdateState_Parms, DeltaTime), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UInputTrigger_UpdateState_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UInputTrigger_UpdateState_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(InputTrigger_eventUpdateState_Parms, ReturnValue), Z_Construct_UEnum_EnhancedInput_ETriggerState, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UInputTrigger_UpdateState_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UInputTrigger_UpdateState_Statics::NewProp_PlayerInput,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UInputTrigger_UpdateState_Statics::NewProp_ModifiedValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UInputTrigger_UpdateState_Statics::NewProp_DeltaTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UInputTrigger_UpdateState_Statics::NewProp_ReturnValue_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UInputTrigger_UpdateState_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UInputTrigger_UpdateState_Statics::Function_MetaDataParams[] = {
		{ "Category", "Trigger" },
		{ "Comment", "/*\n\x09This function checks if the requisite conditions have been met for the trigger to fire.\n\x09 Returns Trigger State None\x09\x09 - No trigger conditions have been met. Trigger is inactive.\n\x09\x09\x09 Trigger State Ongoing\x09 - Some trigger conditions have been met. Trigger is processing but not yet active.\n\x09\x09\x09 Trigger State Triggered - All trigger conditions have been met to fire. Trigger is active.\n\x09*/" },
		{ "ModuleRelativePath", "Public/InputTriggers.h" },
		{ "ToolTip", "This function checks if the requisite conditions have been met for the trigger to fire.\n Returns Trigger State None              - No trigger conditions have been met. Trigger is inactive.\n                 Trigger State Ongoing   - Some trigger conditions have been met. Trigger is processing but not yet active.\n                 Trigger State Triggered - All trigger conditions have been met to fire. Trigger is active." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UInputTrigger_UpdateState_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UInputTrigger, nullptr, "UpdateState", nullptr, nullptr, sizeof(InputTrigger_eventUpdateState_Parms), Z_Construct_UFunction_UInputTrigger_UpdateState_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UInputTrigger_UpdateState_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UInputTrigger_UpdateState_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UInputTrigger_UpdateState_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UInputTrigger_UpdateState()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UInputTrigger_UpdateState_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UInputTrigger_NoRegister()
	{
		return UInputTrigger::StaticClass();
	}
	struct Z_Construct_UClass_UInputTrigger_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActuationThreshold_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ActuationThreshold;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LastValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UInputTrigger_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_EnhancedInput,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UInputTrigger_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UInputTrigger_GetTriggerType, "GetTriggerType" }, // 1109387509
		{ &Z_Construct_UFunction_UInputTrigger_IsActuated, "IsActuated" }, // 3166739501
		{ &Z_Construct_UFunction_UInputTrigger_UpdateState, "UpdateState" }, // 4274767037
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputTrigger_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\nBase class for building triggers.\nTransitions to Triggered state once the input meets or exceeds the actuation threshold.\n*/" },
		{ "IncludePath", "InputTriggers.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/InputTriggers.h" },
		{ "ToolTip", "Base class for building triggers.\nTransitions to Triggered state once the input meets or exceeds the actuation threshold." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputTrigger_Statics::NewProp_ActuationThreshold_MetaData[] = {
		{ "Category", "Trigger Settings" },
		{ "ModuleRelativePath", "Public/InputTriggers.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UInputTrigger_Statics::NewProp_ActuationThreshold = { "ActuationThreshold", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UInputTrigger, ActuationThreshold), METADATA_PARAMS(Z_Construct_UClass_UInputTrigger_Statics::NewProp_ActuationThreshold_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UInputTrigger_Statics::NewProp_ActuationThreshold_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputTrigger_Statics::NewProp_LastValue_MetaData[] = {
		{ "Category", "Trigger Settings" },
		{ "Comment", "// Value passed to UpdateState on the previous tick. This will be updated automatically after the trigger is updated.\n" },
		{ "ModuleRelativePath", "Public/InputTriggers.h" },
		{ "ToolTip", "Value passed to UpdateState on the previous tick. This will be updated automatically after the trigger is updated." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UInputTrigger_Statics::NewProp_LastValue = { "LastValue", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UInputTrigger, LastValue), Z_Construct_UScriptStruct_FInputActionValue, METADATA_PARAMS(Z_Construct_UClass_UInputTrigger_Statics::NewProp_LastValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UInputTrigger_Statics::NewProp_LastValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UInputTrigger_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputTrigger_Statics::NewProp_ActuationThreshold,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputTrigger_Statics::NewProp_LastValue,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UInputTrigger_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UInputTrigger>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UInputTrigger_Statics::ClassParams = {
		&UInputTrigger::StaticClass,
		"Input",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UInputTrigger_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UInputTrigger_Statics::PropPointers),
		0,
		0x401030A7u,
		METADATA_PARAMS(Z_Construct_UClass_UInputTrigger_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UInputTrigger_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UInputTrigger()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UInputTrigger_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UInputTrigger, 694012230);
	template<> ENHANCEDINPUT_API UClass* StaticClass<UInputTrigger>()
	{
		return UInputTrigger::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UInputTrigger(Z_Construct_UClass_UInputTrigger, &UInputTrigger::StaticClass, TEXT("/Script/EnhancedInput"), TEXT("UInputTrigger"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UInputTrigger);
	void UInputTriggerTimedBase::StaticRegisterNativesUInputTriggerTimedBase()
	{
	}
	UClass* Z_Construct_UClass_UInputTriggerTimedBase_NoRegister()
	{
		return UInputTriggerTimedBase::StaticClass();
	}
	struct Z_Construct_UClass_UInputTriggerTimedBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HeldDuration_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HeldDuration;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAffectedByTimeDilation_MetaData[];
#endif
		static void NewProp_bAffectedByTimeDilation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAffectedByTimeDilation;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UInputTriggerTimedBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInputTrigger,
		(UObject* (*)())Z_Construct_UPackage__Script_EnhancedInput,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputTriggerTimedBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\nBase class for building triggers that have firing conditions governed by elapsed time.\nThis class transitions state to Ongoing once input is actuated, and will track Ongoing input time until input is released.\nInheriting classes should provide the logic for Triggered transitions.\n*/" },
		{ "IncludePath", "InputTriggers.h" },
		{ "ModuleRelativePath", "Public/InputTriggers.h" },
		{ "ToolTip", "Base class for building triggers that have firing conditions governed by elapsed time.\nThis class transitions state to Ongoing once input is actuated, and will track Ongoing input time until input is released.\nInheriting classes should provide the logic for Triggered transitions." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputTriggerTimedBase_Statics::NewProp_HeldDuration_MetaData[] = {
		{ "Category", "Trigger Settings" },
		{ "ModuleRelativePath", "Public/InputTriggers.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UInputTriggerTimedBase_Statics::NewProp_HeldDuration = { "HeldDuration", nullptr, (EPropertyFlags)0x0020080000000004, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UInputTriggerTimedBase, HeldDuration), METADATA_PARAMS(Z_Construct_UClass_UInputTriggerTimedBase_Statics::NewProp_HeldDuration_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UInputTriggerTimedBase_Statics::NewProp_HeldDuration_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputTriggerTimedBase_Statics::NewProp_bAffectedByTimeDilation_MetaData[] = {
		{ "Category", "Trigger Settings" },
		{ "Comment", "// Should global time dilation be applied to the held duration?\n" },
		{ "ModuleRelativePath", "Public/InputTriggers.h" },
		{ "ToolTip", "Should global time dilation be applied to the held duration?" },
	};
#endif
	void Z_Construct_UClass_UInputTriggerTimedBase_Statics::NewProp_bAffectedByTimeDilation_SetBit(void* Obj)
	{
		((UInputTriggerTimedBase*)Obj)->bAffectedByTimeDilation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UInputTriggerTimedBase_Statics::NewProp_bAffectedByTimeDilation = { "bAffectedByTimeDilation", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UInputTriggerTimedBase), &Z_Construct_UClass_UInputTriggerTimedBase_Statics::NewProp_bAffectedByTimeDilation_SetBit, METADATA_PARAMS(Z_Construct_UClass_UInputTriggerTimedBase_Statics::NewProp_bAffectedByTimeDilation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UInputTriggerTimedBase_Statics::NewProp_bAffectedByTimeDilation_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UInputTriggerTimedBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputTriggerTimedBase_Statics::NewProp_HeldDuration,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputTriggerTimedBase_Statics::NewProp_bAffectedByTimeDilation,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UInputTriggerTimedBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UInputTriggerTimedBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UInputTriggerTimedBase_Statics::ClassParams = {
		&UInputTriggerTimedBase::StaticClass,
		"Input",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UInputTriggerTimedBase_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UInputTriggerTimedBase_Statics::PropPointers),
		0,
		0x400830A3u,
		METADATA_PARAMS(Z_Construct_UClass_UInputTriggerTimedBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UInputTriggerTimedBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UInputTriggerTimedBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UInputTriggerTimedBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UInputTriggerTimedBase, 761251100);
	template<> ENHANCEDINPUT_API UClass* StaticClass<UInputTriggerTimedBase>()
	{
		return UInputTriggerTimedBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UInputTriggerTimedBase(Z_Construct_UClass_UInputTriggerTimedBase, &UInputTriggerTimedBase::StaticClass, TEXT("/Script/EnhancedInput"), TEXT("UInputTriggerTimedBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UInputTriggerTimedBase);
	void UInputTriggerDown::StaticRegisterNativesUInputTriggerDown()
	{
	}
	UClass* Z_Construct_UClass_UInputTriggerDown_NoRegister()
	{
		return UInputTriggerDown::StaticClass();
	}
	struct Z_Construct_UClass_UInputTriggerDown_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UInputTriggerDown_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInputTrigger,
		(UObject* (*)())Z_Construct_UPackage__Script_EnhancedInput,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputTriggerDown_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** UInputTriggerDown\n\x09Trigger fires when the input exceeds the actuation threshold.\n\x09Note: When no triggers are bound Down (with an actuation threshold of > 0) is the default behavior.\n\x09*/" },
		{ "DisplayName", "Down" },
		{ "IncludePath", "InputTriggers.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Public/InputTriggers.h" },
		{ "ToolTip", "UInputTriggerDown\n      Trigger fires when the input exceeds the actuation threshold.\n      Note: When no triggers are bound Down (with an actuation threshold of > 0) is the default behavior." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UInputTriggerDown_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UInputTriggerDown>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UInputTriggerDown_Statics::ClassParams = {
		&UInputTriggerDown::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x400830A2u,
		METADATA_PARAMS(Z_Construct_UClass_UInputTriggerDown_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UInputTriggerDown_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UInputTriggerDown()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UInputTriggerDown_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UInputTriggerDown, 3301808126);
	template<> ENHANCEDINPUT_API UClass* StaticClass<UInputTriggerDown>()
	{
		return UInputTriggerDown::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UInputTriggerDown(Z_Construct_UClass_UInputTriggerDown, &UInputTriggerDown::StaticClass, TEXT("/Script/EnhancedInput"), TEXT("UInputTriggerDown"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UInputTriggerDown);
	void UInputTriggerPressed::StaticRegisterNativesUInputTriggerPressed()
	{
	}
	UClass* Z_Construct_UClass_UInputTriggerPressed_NoRegister()
	{
		return UInputTriggerPressed::StaticClass();
	}
	struct Z_Construct_UClass_UInputTriggerPressed_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UInputTriggerPressed_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInputTrigger,
		(UObject* (*)())Z_Construct_UPackage__Script_EnhancedInput,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputTriggerPressed_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** UInputTriggerPressed\n\x09Trigger fires once only when input exceeds the actuation threshold.\n\x09Holding the input will not cause further triggers.\n\x09*/" },
		{ "DisplayName", "Pressed" },
		{ "IncludePath", "InputTriggers.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Public/InputTriggers.h" },
		{ "ToolTip", "UInputTriggerPressed\n      Trigger fires once only when input exceeds the actuation threshold.\n      Holding the input will not cause further triggers." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UInputTriggerPressed_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UInputTriggerPressed>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UInputTriggerPressed_Statics::ClassParams = {
		&UInputTriggerPressed::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x400830A2u,
		METADATA_PARAMS(Z_Construct_UClass_UInputTriggerPressed_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UInputTriggerPressed_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UInputTriggerPressed()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UInputTriggerPressed_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UInputTriggerPressed, 329850134);
	template<> ENHANCEDINPUT_API UClass* StaticClass<UInputTriggerPressed>()
	{
		return UInputTriggerPressed::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UInputTriggerPressed(Z_Construct_UClass_UInputTriggerPressed, &UInputTriggerPressed::StaticClass, TEXT("/Script/EnhancedInput"), TEXT("UInputTriggerPressed"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UInputTriggerPressed);
	void UInputTriggerReleased::StaticRegisterNativesUInputTriggerReleased()
	{
	}
	UClass* Z_Construct_UClass_UInputTriggerReleased_NoRegister()
	{
		return UInputTriggerReleased::StaticClass();
	}
	struct Z_Construct_UClass_UInputTriggerReleased_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UInputTriggerReleased_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInputTrigger,
		(UObject* (*)())Z_Construct_UPackage__Script_EnhancedInput,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputTriggerReleased_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** UInputTriggerReleased\n\x09Trigger returns Ongoing whilst input exceeds the actuation threshold.\n\x09Trigger fires once only when input drops back below actuation threshold.\n\x09*/" },
		{ "DisplayName", "Released" },
		{ "IncludePath", "InputTriggers.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Public/InputTriggers.h" },
		{ "ToolTip", "UInputTriggerReleased\n      Trigger returns Ongoing whilst input exceeds the actuation threshold.\n      Trigger fires once only when input drops back below actuation threshold." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UInputTriggerReleased_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UInputTriggerReleased>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UInputTriggerReleased_Statics::ClassParams = {
		&UInputTriggerReleased::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x400830A2u,
		METADATA_PARAMS(Z_Construct_UClass_UInputTriggerReleased_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UInputTriggerReleased_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UInputTriggerReleased()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UInputTriggerReleased_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UInputTriggerReleased, 4249131203);
	template<> ENHANCEDINPUT_API UClass* StaticClass<UInputTriggerReleased>()
	{
		return UInputTriggerReleased::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UInputTriggerReleased(Z_Construct_UClass_UInputTriggerReleased, &UInputTriggerReleased::StaticClass, TEXT("/Script/EnhancedInput"), TEXT("UInputTriggerReleased"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UInputTriggerReleased);
	void UInputTriggerHold::StaticRegisterNativesUInputTriggerHold()
	{
	}
	UClass* Z_Construct_UClass_UInputTriggerHold_NoRegister()
	{
		return UInputTriggerHold::StaticClass();
	}
	struct Z_Construct_UClass_UInputTriggerHold_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HoldTimeThreshold_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HoldTimeThreshold;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsOneShot_MetaData[];
#endif
		static void NewProp_bIsOneShot_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsOneShot;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UInputTriggerHold_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInputTriggerTimedBase,
		(UObject* (*)())Z_Construct_UPackage__Script_EnhancedInput,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputTriggerHold_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** UInputTriggerHold\n\x09Trigger fires once input has remained actuated for HoldTimeThreshold seconds.\n\x09Trigger may optionally fire once, or repeatedly fire.\n*/" },
		{ "DisplayName", "Hold" },
		{ "IncludePath", "InputTriggers.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Public/InputTriggers.h" },
		{ "ToolTip", "UInputTriggerHold\n      Trigger fires once input has remained actuated for HoldTimeThreshold seconds.\n      Trigger may optionally fire once, or repeatedly fire." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputTriggerHold_Statics::NewProp_HoldTimeThreshold_MetaData[] = {
		{ "Category", "Trigger Settings" },
		{ "ClampMin", "0" },
		{ "Comment", "// How long does the input have to be held to cause trigger?\n" },
		{ "ModuleRelativePath", "Public/InputTriggers.h" },
		{ "ToolTip", "How long does the input have to be held to cause trigger?" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UInputTriggerHold_Statics::NewProp_HoldTimeThreshold = { "HoldTimeThreshold", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UInputTriggerHold, HoldTimeThreshold), METADATA_PARAMS(Z_Construct_UClass_UInputTriggerHold_Statics::NewProp_HoldTimeThreshold_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UInputTriggerHold_Statics::NewProp_HoldTimeThreshold_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputTriggerHold_Statics::NewProp_bIsOneShot_MetaData[] = {
		{ "Category", "Trigger Settings" },
		{ "Comment", "// Should this trigger fire only once, or fire every frame once the hold time threshold is met?\n" },
		{ "ModuleRelativePath", "Public/InputTriggers.h" },
		{ "ToolTip", "Should this trigger fire only once, or fire every frame once the hold time threshold is met?" },
	};
#endif
	void Z_Construct_UClass_UInputTriggerHold_Statics::NewProp_bIsOneShot_SetBit(void* Obj)
	{
		((UInputTriggerHold*)Obj)->bIsOneShot = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UInputTriggerHold_Statics::NewProp_bIsOneShot = { "bIsOneShot", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UInputTriggerHold), &Z_Construct_UClass_UInputTriggerHold_Statics::NewProp_bIsOneShot_SetBit, METADATA_PARAMS(Z_Construct_UClass_UInputTriggerHold_Statics::NewProp_bIsOneShot_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UInputTriggerHold_Statics::NewProp_bIsOneShot_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UInputTriggerHold_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputTriggerHold_Statics::NewProp_HoldTimeThreshold,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputTriggerHold_Statics::NewProp_bIsOneShot,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UInputTriggerHold_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UInputTriggerHold>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UInputTriggerHold_Statics::ClassParams = {
		&UInputTriggerHold::StaticClass,
		"Input",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UInputTriggerHold_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UInputTriggerHold_Statics::PropPointers),
		0,
		0x400830A6u,
		METADATA_PARAMS(Z_Construct_UClass_UInputTriggerHold_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UInputTriggerHold_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UInputTriggerHold()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UInputTriggerHold_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UInputTriggerHold, 3944737926);
	template<> ENHANCEDINPUT_API UClass* StaticClass<UInputTriggerHold>()
	{
		return UInputTriggerHold::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UInputTriggerHold(Z_Construct_UClass_UInputTriggerHold, &UInputTriggerHold::StaticClass, TEXT("/Script/EnhancedInput"), TEXT("UInputTriggerHold"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UInputTriggerHold);
	void UInputTriggerHoldAndRelease::StaticRegisterNativesUInputTriggerHoldAndRelease()
	{
	}
	UClass* Z_Construct_UClass_UInputTriggerHoldAndRelease_NoRegister()
	{
		return UInputTriggerHoldAndRelease::StaticClass();
	}
	struct Z_Construct_UClass_UInputTriggerHoldAndRelease_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HoldTimeThreshold_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HoldTimeThreshold;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UInputTriggerHoldAndRelease_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInputTriggerTimedBase,
		(UObject* (*)())Z_Construct_UPackage__Script_EnhancedInput,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputTriggerHoldAndRelease_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** UInputTriggerHoldAndRelease\n\x09Trigger fires when input is released after having been actuated for at least HoldTimeThreshold seconds.\n*/" },
		{ "DisplayName", "Hold And Release" },
		{ "IncludePath", "InputTriggers.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Public/InputTriggers.h" },
		{ "ToolTip", "UInputTriggerHoldAndRelease\n      Trigger fires when input is released after having been actuated for at least HoldTimeThreshold seconds." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputTriggerHoldAndRelease_Statics::NewProp_HoldTimeThreshold_MetaData[] = {
		{ "Category", "Trigger Settings" },
		{ "ClampMin", "0" },
		{ "Comment", "// How long does the input have to be held to cause trigger?\n" },
		{ "ModuleRelativePath", "Public/InputTriggers.h" },
		{ "ToolTip", "How long does the input have to be held to cause trigger?" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UInputTriggerHoldAndRelease_Statics::NewProp_HoldTimeThreshold = { "HoldTimeThreshold", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UInputTriggerHoldAndRelease, HoldTimeThreshold), METADATA_PARAMS(Z_Construct_UClass_UInputTriggerHoldAndRelease_Statics::NewProp_HoldTimeThreshold_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UInputTriggerHoldAndRelease_Statics::NewProp_HoldTimeThreshold_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UInputTriggerHoldAndRelease_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputTriggerHoldAndRelease_Statics::NewProp_HoldTimeThreshold,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UInputTriggerHoldAndRelease_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UInputTriggerHoldAndRelease>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UInputTriggerHoldAndRelease_Statics::ClassParams = {
		&UInputTriggerHoldAndRelease::StaticClass,
		"Input",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UInputTriggerHoldAndRelease_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UInputTriggerHoldAndRelease_Statics::PropPointers),
		0,
		0x400830A6u,
		METADATA_PARAMS(Z_Construct_UClass_UInputTriggerHoldAndRelease_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UInputTriggerHoldAndRelease_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UInputTriggerHoldAndRelease()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UInputTriggerHoldAndRelease_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UInputTriggerHoldAndRelease, 2444840022);
	template<> ENHANCEDINPUT_API UClass* StaticClass<UInputTriggerHoldAndRelease>()
	{
		return UInputTriggerHoldAndRelease::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UInputTriggerHoldAndRelease(Z_Construct_UClass_UInputTriggerHoldAndRelease, &UInputTriggerHoldAndRelease::StaticClass, TEXT("/Script/EnhancedInput"), TEXT("UInputTriggerHoldAndRelease"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UInputTriggerHoldAndRelease);
	void UInputTriggerTap::StaticRegisterNativesUInputTriggerTap()
	{
	}
	UClass* Z_Construct_UClass_UInputTriggerTap_NoRegister()
	{
		return UInputTriggerTap::StaticClass();
	}
	struct Z_Construct_UClass_UInputTriggerTap_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TapReleaseTimeThreshold_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TapReleaseTimeThreshold;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UInputTriggerTap_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInputTriggerTimedBase,
		(UObject* (*)())Z_Construct_UPackage__Script_EnhancedInput,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputTriggerTap_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** UInputTriggerTap\n\x09Input must be actuated then released within TapReleaseTimeThreshold seconds to trigger.\n*/" },
		{ "DisplayName", "Tap" },
		{ "IncludePath", "InputTriggers.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Public/InputTriggers.h" },
		{ "ToolTip", "UInputTriggerTap\n      Input must be actuated then released within TapReleaseTimeThreshold seconds to trigger." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputTriggerTap_Statics::NewProp_TapReleaseTimeThreshold_MetaData[] = {
		{ "Category", "Trigger Settings" },
		{ "ClampMin", "0" },
		{ "Comment", "// Release within this time-frame to trigger a tap\n" },
		{ "ModuleRelativePath", "Public/InputTriggers.h" },
		{ "ToolTip", "Release within this time-frame to trigger a tap" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UInputTriggerTap_Statics::NewProp_TapReleaseTimeThreshold = { "TapReleaseTimeThreshold", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UInputTriggerTap, TapReleaseTimeThreshold), METADATA_PARAMS(Z_Construct_UClass_UInputTriggerTap_Statics::NewProp_TapReleaseTimeThreshold_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UInputTriggerTap_Statics::NewProp_TapReleaseTimeThreshold_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UInputTriggerTap_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputTriggerTap_Statics::NewProp_TapReleaseTimeThreshold,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UInputTriggerTap_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UInputTriggerTap>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UInputTriggerTap_Statics::ClassParams = {
		&UInputTriggerTap::StaticClass,
		"Input",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UInputTriggerTap_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UInputTriggerTap_Statics::PropPointers),
		0,
		0x400830A6u,
		METADATA_PARAMS(Z_Construct_UClass_UInputTriggerTap_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UInputTriggerTap_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UInputTriggerTap()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UInputTriggerTap_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UInputTriggerTap, 1398860014);
	template<> ENHANCEDINPUT_API UClass* StaticClass<UInputTriggerTap>()
	{
		return UInputTriggerTap::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UInputTriggerTap(Z_Construct_UClass_UInputTriggerTap, &UInputTriggerTap::StaticClass, TEXT("/Script/EnhancedInput"), TEXT("UInputTriggerTap"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UInputTriggerTap);
	void UInputTriggerPulse::StaticRegisterNativesUInputTriggerPulse()
	{
	}
	UClass* Z_Construct_UClass_UInputTriggerPulse_NoRegister()
	{
		return UInputTriggerPulse::StaticClass();
	}
	struct Z_Construct_UClass_UInputTriggerPulse_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bTriggerOnStart_MetaData[];
#endif
		static void NewProp_bTriggerOnStart_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bTriggerOnStart;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Interval_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Interval;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TriggerLimit_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_TriggerLimit;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UInputTriggerPulse_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInputTriggerTimedBase,
		(UObject* (*)())Z_Construct_UPackage__Script_EnhancedInput,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputTriggerPulse_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** UInputTriggerPulse\n\x09Trigger that fires at an Interval, in seconds, while input is actuated. \n\x09Note:\x09""Completed only fires when the repeat limit is reached or when input is released immediately after being triggered.\n\x09\x09\x09Otherwise, Canceled is fired when input is released.\n\x09*/" },
		{ "DisplayName", "Pulse" },
		{ "IncludePath", "InputTriggers.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Public/InputTriggers.h" },
		{ "ToolTip", "UInputTriggerPulse\n      Trigger that fires at an Interval, in seconds, while input is actuated.\n      Note:   Completed only fires when the repeat limit is reached or when input is released immediately after being triggered.\n                      Otherwise, Canceled is fired when input is released." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputTriggerPulse_Statics::NewProp_bTriggerOnStart_MetaData[] = {
		{ "Category", "Trigger Settings" },
		{ "Comment", "// Whether to trigger when the input first exceeds the actuation threshold or wait for the first interval?\n" },
		{ "ModuleRelativePath", "Public/InputTriggers.h" },
		{ "ToolTip", "Whether to trigger when the input first exceeds the actuation threshold or wait for the first interval?" },
	};
#endif
	void Z_Construct_UClass_UInputTriggerPulse_Statics::NewProp_bTriggerOnStart_SetBit(void* Obj)
	{
		((UInputTriggerPulse*)Obj)->bTriggerOnStart = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UInputTriggerPulse_Statics::NewProp_bTriggerOnStart = { "bTriggerOnStart", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UInputTriggerPulse), &Z_Construct_UClass_UInputTriggerPulse_Statics::NewProp_bTriggerOnStart_SetBit, METADATA_PARAMS(Z_Construct_UClass_UInputTriggerPulse_Statics::NewProp_bTriggerOnStart_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UInputTriggerPulse_Statics::NewProp_bTriggerOnStart_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputTriggerPulse_Statics::NewProp_Interval_MetaData[] = {
		{ "Category", "Trigger Settings" },
		{ "ClampMin", "0" },
		{ "Comment", "// How long between each trigger fire while input is held, in seconds?\n" },
		{ "ModuleRelativePath", "Public/InputTriggers.h" },
		{ "ToolTip", "How long between each trigger fire while input is held, in seconds?" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UInputTriggerPulse_Statics::NewProp_Interval = { "Interval", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UInputTriggerPulse, Interval), METADATA_PARAMS(Z_Construct_UClass_UInputTriggerPulse_Statics::NewProp_Interval_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UInputTriggerPulse_Statics::NewProp_Interval_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputTriggerPulse_Statics::NewProp_TriggerLimit_MetaData[] = {
		{ "Category", "Trigger Settings" },
		{ "ClampMin", "0" },
		{ "Comment", "// How many times can the trigger fire while input is held? (0 = no limit)\n" },
		{ "ModuleRelativePath", "Public/InputTriggers.h" },
		{ "ToolTip", "How many times can the trigger fire while input is held? (0 = no limit)" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UInputTriggerPulse_Statics::NewProp_TriggerLimit = { "TriggerLimit", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UInputTriggerPulse, TriggerLimit), METADATA_PARAMS(Z_Construct_UClass_UInputTriggerPulse_Statics::NewProp_TriggerLimit_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UInputTriggerPulse_Statics::NewProp_TriggerLimit_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UInputTriggerPulse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputTriggerPulse_Statics::NewProp_bTriggerOnStart,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputTriggerPulse_Statics::NewProp_Interval,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputTriggerPulse_Statics::NewProp_TriggerLimit,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UInputTriggerPulse_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UInputTriggerPulse>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UInputTriggerPulse_Statics::ClassParams = {
		&UInputTriggerPulse::StaticClass,
		"Input",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UInputTriggerPulse_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UInputTriggerPulse_Statics::PropPointers),
		0,
		0x400830A6u,
		METADATA_PARAMS(Z_Construct_UClass_UInputTriggerPulse_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UInputTriggerPulse_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UInputTriggerPulse()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UInputTriggerPulse_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UInputTriggerPulse, 1173444278);
	template<> ENHANCEDINPUT_API UClass* StaticClass<UInputTriggerPulse>()
	{
		return UInputTriggerPulse::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UInputTriggerPulse(Z_Construct_UClass_UInputTriggerPulse, &UInputTriggerPulse::StaticClass, TEXT("/Script/EnhancedInput"), TEXT("UInputTriggerPulse"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UInputTriggerPulse);
	void UInputTriggerChordAction::StaticRegisterNativesUInputTriggerChordAction()
	{
	}
	UClass* Z_Construct_UClass_UInputTriggerChordAction_NoRegister()
	{
		return UInputTriggerChordAction::StaticClass();
	}
	struct Z_Construct_UClass_UInputTriggerChordAction_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChordAction_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ChordAction;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UInputTriggerChordAction_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInputTrigger,
		(UObject* (*)())Z_Construct_UPackage__Script_EnhancedInput,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputTriggerChordAction_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** UInputTriggerChordAction\n\x09""Applies a chord action that must be triggering for this trigger's action to trigger\n*/" },
		{ "DisplayName", "Chorded Action" },
		{ "IncludePath", "InputTriggers.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Public/InputTriggers.h" },
		{ "NotInputConfigurable", "true" },
		{ "ToolTip", "UInputTriggerChordAction\n      Applies a chord action that must be triggering for this trigger's action to trigger" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputTriggerChordAction_Statics::NewProp_ChordAction_MetaData[] = {
		{ "Category", "Trigger Settings" },
		{ "DisplayThumbnail", "false" },
		{ "ModuleRelativePath", "Public/InputTriggers.h" },
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UInputTriggerChordAction_Statics::NewProp_ChordAction = { "ChordAction", nullptr, (EPropertyFlags)0x0010000000000805, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UInputTriggerChordAction, ChordAction), Z_Construct_UClass_UInputAction_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UInputTriggerChordAction_Statics::NewProp_ChordAction_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UInputTriggerChordAction_Statics::NewProp_ChordAction_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UInputTriggerChordAction_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputTriggerChordAction_Statics::NewProp_ChordAction,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UInputTriggerChordAction_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UInputTriggerChordAction>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UInputTriggerChordAction_Statics::ClassParams = {
		&UInputTriggerChordAction::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UInputTriggerChordAction_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UInputTriggerChordAction_Statics::PropPointers),
		0,
		0x400830A2u,
		METADATA_PARAMS(Z_Construct_UClass_UInputTriggerChordAction_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UInputTriggerChordAction_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UInputTriggerChordAction()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UInputTriggerChordAction_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UInputTriggerChordAction, 1413149255);
	template<> ENHANCEDINPUT_API UClass* StaticClass<UInputTriggerChordAction>()
	{
		return UInputTriggerChordAction::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UInputTriggerChordAction(Z_Construct_UClass_UInputTriggerChordAction, &UInputTriggerChordAction::StaticClass, TEXT("/Script/EnhancedInput"), TEXT("UInputTriggerChordAction"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UInputTriggerChordAction);
	void UInputTriggerChordBlocker::StaticRegisterNativesUInputTriggerChordBlocker()
	{
	}
	UClass* Z_Construct_UClass_UInputTriggerChordBlocker_NoRegister()
	{
		return UInputTriggerChordBlocker::StaticClass();
	}
	struct Z_Construct_UClass_UInputTriggerChordBlocker_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UInputTriggerChordBlocker_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInputTriggerChordAction,
		(UObject* (*)())Z_Construct_UPackage__Script_EnhancedInput,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputTriggerChordBlocker_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** UInputTriggerChordBlocker\n\x09""Automatically instantiated  to block mappings that are masked by a UInputTriggerChordAction chord from firing whilst the chording key is active.\n\x09NOTE: Do not attempt to add these manually.\n*/" },
		{ "IncludePath", "InputTriggers.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Public/InputTriggers.h" },
		{ "ToolTip", "UInputTriggerChordBlocker\n      Automatically instantiated  to block mappings that are masked by a UInputTriggerChordAction chord from firing whilst the chording key is active.\n      NOTE: Do not attempt to add these manually." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UInputTriggerChordBlocker_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UInputTriggerChordBlocker>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UInputTriggerChordBlocker_Statics::ClassParams = {
		&UInputTriggerChordBlocker::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x440830A2u,
		METADATA_PARAMS(Z_Construct_UClass_UInputTriggerChordBlocker_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UInputTriggerChordBlocker_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UInputTriggerChordBlocker()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UInputTriggerChordBlocker_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UInputTriggerChordBlocker, 2883015376);
	template<> ENHANCEDINPUT_API UClass* StaticClass<UInputTriggerChordBlocker>()
	{
		return UInputTriggerChordBlocker::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UInputTriggerChordBlocker(Z_Construct_UClass_UInputTriggerChordBlocker, &UInputTriggerChordBlocker::StaticClass, TEXT("/Script/EnhancedInput"), TEXT("UInputTriggerChordBlocker"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UInputTriggerChordBlocker);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
