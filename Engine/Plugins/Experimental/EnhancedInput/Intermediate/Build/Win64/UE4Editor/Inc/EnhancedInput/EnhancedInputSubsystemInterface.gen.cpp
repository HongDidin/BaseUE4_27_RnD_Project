// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "EnhancedInput/Public/EnhancedInputSubsystemInterface.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEnhancedInputSubsystemInterface() {}
// Cross Module References
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UEnhancedInputSubsystemInterface_NoRegister();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UEnhancedInputSubsystemInterface();
	COREUOBJECT_API UClass* Z_Construct_UClass_UInterface();
	UPackage* Z_Construct_UPackage__Script_EnhancedInput();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputMappingContext_NoRegister();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputAction_NoRegister();
	INPUTCORE_API UScriptStruct* Z_Construct_UScriptStruct_FKey();
	ENHANCEDINPUT_API UScriptStruct* Z_Construct_UScriptStruct_FMappingQueryIssue();
	ENHANCEDINPUT_API UEnum* Z_Construct_UEnum_EnhancedInput_EMappingQueryIssue();
	ENHANCEDINPUT_API UEnum* Z_Construct_UEnum_EnhancedInput_EMappingQueryResult();
// End Cross Module References
	DEFINE_FUNCTION(IEnhancedInputSubsystemInterface::execQueryKeysMappedToAction)
	{
		P_GET_OBJECT(UInputAction,Z_Param_Action);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<FKey>*)Z_Param__Result=P_THIS->QueryKeysMappedToAction(Z_Param_Action);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IEnhancedInputSubsystemInterface::execHasMappingContext)
	{
		P_GET_OBJECT(UInputMappingContext,Z_Param_MappingContext);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->HasMappingContext(Z_Param_MappingContext);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IEnhancedInputSubsystemInterface::execQueryMapKeyInContextSet)
	{
		P_GET_TARRAY_REF(UInputMappingContext*,Z_Param_Out_PrioritizedActiveContexts);
		P_GET_OBJECT(UInputMappingContext,Z_Param_InputContext);
		P_GET_OBJECT(UInputAction,Z_Param_Action);
		P_GET_STRUCT(FKey,Z_Param_Key);
		P_GET_TARRAY_REF(FMappingQueryIssue,Z_Param_Out_OutIssues);
		P_GET_ENUM(EMappingQueryIssue,Z_Param_BlockingIssues);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(EMappingQueryResult*)Z_Param__Result=P_THIS->QueryMapKeyInContextSet(Z_Param_Out_PrioritizedActiveContexts,Z_Param_InputContext,Z_Param_Action,Z_Param_Key,Z_Param_Out_OutIssues,EMappingQueryIssue(Z_Param_BlockingIssues));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IEnhancedInputSubsystemInterface::execQueryMapKeyInActiveContextSet)
	{
		P_GET_OBJECT(UInputMappingContext,Z_Param_InputContext);
		P_GET_OBJECT(UInputAction,Z_Param_Action);
		P_GET_STRUCT(FKey,Z_Param_Key);
		P_GET_TARRAY_REF(FMappingQueryIssue,Z_Param_Out_OutIssues);
		P_GET_ENUM(EMappingQueryIssue,Z_Param_BlockingIssues);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(EMappingQueryResult*)Z_Param__Result=P_THIS->QueryMapKeyInActiveContextSet(Z_Param_InputContext,Z_Param_Action,Z_Param_Key,Z_Param_Out_OutIssues,EMappingQueryIssue(Z_Param_BlockingIssues));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IEnhancedInputSubsystemInterface::execRequestRebuildControlMappings)
	{
		P_GET_UBOOL(Z_Param_bForceImmediately);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->RequestRebuildControlMappings(Z_Param_bForceImmediately);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IEnhancedInputSubsystemInterface::execRemoveMappingContext)
	{
		P_GET_OBJECT(UInputMappingContext,Z_Param_MappingContext);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->RemoveMappingContext(Z_Param_MappingContext);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IEnhancedInputSubsystemInterface::execAddMappingContext)
	{
		P_GET_OBJECT(UInputMappingContext,Z_Param_MappingContext);
		P_GET_PROPERTY(FIntProperty,Z_Param_Priority);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AddMappingContext(Z_Param_MappingContext,Z_Param_Priority);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IEnhancedInputSubsystemInterface::execClearAllMappings)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ClearAllMappings();
		P_NATIVE_END;
	}
	void UEnhancedInputSubsystemInterface::StaticRegisterNativesUEnhancedInputSubsystemInterface()
	{
		UClass* Class = UEnhancedInputSubsystemInterface::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddMappingContext", &IEnhancedInputSubsystemInterface::execAddMappingContext },
			{ "ClearAllMappings", &IEnhancedInputSubsystemInterface::execClearAllMappings },
			{ "HasMappingContext", &IEnhancedInputSubsystemInterface::execHasMappingContext },
			{ "QueryKeysMappedToAction", &IEnhancedInputSubsystemInterface::execQueryKeysMappedToAction },
			{ "QueryMapKeyInActiveContextSet", &IEnhancedInputSubsystemInterface::execQueryMapKeyInActiveContextSet },
			{ "QueryMapKeyInContextSet", &IEnhancedInputSubsystemInterface::execQueryMapKeyInContextSet },
			{ "RemoveMappingContext", &IEnhancedInputSubsystemInterface::execRemoveMappingContext },
			{ "RequestRebuildControlMappings", &IEnhancedInputSubsystemInterface::execRequestRebuildControlMappings },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UEnhancedInputSubsystemInterface_AddMappingContext_Statics
	{
		struct EnhancedInputSubsystemInterface_eventAddMappingContext_Parms
		{
			const UInputMappingContext* MappingContext;
			int32 Priority;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MappingContext_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MappingContext;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Priority;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEnhancedInputSubsystemInterface_AddMappingContext_Statics::NewProp_MappingContext_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEnhancedInputSubsystemInterface_AddMappingContext_Statics::NewProp_MappingContext = { "MappingContext", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EnhancedInputSubsystemInterface_eventAddMappingContext_Parms, MappingContext), Z_Construct_UClass_UInputMappingContext_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UEnhancedInputSubsystemInterface_AddMappingContext_Statics::NewProp_MappingContext_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnhancedInputSubsystemInterface_AddMappingContext_Statics::NewProp_MappingContext_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEnhancedInputSubsystemInterface_AddMappingContext_Statics::NewProp_Priority = { "Priority", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EnhancedInputSubsystemInterface_eventAddMappingContext_Parms, Priority), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEnhancedInputSubsystemInterface_AddMappingContext_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnhancedInputSubsystemInterface_AddMappingContext_Statics::NewProp_MappingContext,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnhancedInputSubsystemInterface_AddMappingContext_Statics::NewProp_Priority,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEnhancedInputSubsystemInterface_AddMappingContext_Statics::Function_MetaDataParams[] = {
		{ "Category", "Input" },
		{ "Comment", "/**\n\x09 * Add a control mapping context.\n\x09 * @param MappingContext\x09""A set of key to action mappings to apply to this player\n\x09 * @param Priority\x09\x09\x09Higher priority mappings will be applied first and, if they consume input, will block lower priority mappings.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EnhancedInputSubsystemInterface.h" },
		{ "ToolTip", "Add a control mapping context.\n@param MappingContext        A set of key to action mappings to apply to this player\n@param Priority                      Higher priority mappings will be applied first and, if they consume input, will block lower priority mappings." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEnhancedInputSubsystemInterface_AddMappingContext_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEnhancedInputSubsystemInterface, nullptr, "AddMappingContext", nullptr, nullptr, sizeof(EnhancedInputSubsystemInterface_eventAddMappingContext_Parms), Z_Construct_UFunction_UEnhancedInputSubsystemInterface_AddMappingContext_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnhancedInputSubsystemInterface_AddMappingContext_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020408, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEnhancedInputSubsystemInterface_AddMappingContext_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnhancedInputSubsystemInterface_AddMappingContext_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEnhancedInputSubsystemInterface_AddMappingContext()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEnhancedInputSubsystemInterface_AddMappingContext_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEnhancedInputSubsystemInterface_ClearAllMappings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEnhancedInputSubsystemInterface_ClearAllMappings_Statics::Function_MetaDataParams[] = {
		{ "Category", "Input" },
		{ "Comment", "/**\n\x09 * Remove all applied mapping contexts.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EnhancedInputSubsystemInterface.h" },
		{ "ToolTip", "Remove all applied mapping contexts." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEnhancedInputSubsystemInterface_ClearAllMappings_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEnhancedInputSubsystemInterface, nullptr, "ClearAllMappings", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020408, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEnhancedInputSubsystemInterface_ClearAllMappings_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnhancedInputSubsystemInterface_ClearAllMappings_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEnhancedInputSubsystemInterface_ClearAllMappings()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEnhancedInputSubsystemInterface_ClearAllMappings_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEnhancedInputSubsystemInterface_HasMappingContext_Statics
	{
		struct EnhancedInputSubsystemInterface_eventHasMappingContext_Parms
		{
			const UInputMappingContext* MappingContext;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MappingContext_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MappingContext;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEnhancedInputSubsystemInterface_HasMappingContext_Statics::NewProp_MappingContext_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEnhancedInputSubsystemInterface_HasMappingContext_Statics::NewProp_MappingContext = { "MappingContext", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EnhancedInputSubsystemInterface_eventHasMappingContext_Parms, MappingContext), Z_Construct_UClass_UInputMappingContext_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UEnhancedInputSubsystemInterface_HasMappingContext_Statics::NewProp_MappingContext_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnhancedInputSubsystemInterface_HasMappingContext_Statics::NewProp_MappingContext_MetaData)) };
	void Z_Construct_UFunction_UEnhancedInputSubsystemInterface_HasMappingContext_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((EnhancedInputSubsystemInterface_eventHasMappingContext_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEnhancedInputSubsystemInterface_HasMappingContext_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EnhancedInputSubsystemInterface_eventHasMappingContext_Parms), &Z_Construct_UFunction_UEnhancedInputSubsystemInterface_HasMappingContext_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEnhancedInputSubsystemInterface_HasMappingContext_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnhancedInputSubsystemInterface_HasMappingContext_Statics::NewProp_MappingContext,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnhancedInputSubsystemInterface_HasMappingContext_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEnhancedInputSubsystemInterface_HasMappingContext_Statics::Function_MetaDataParams[] = {
		{ "Category", "Input|Mapping Queries" },
		{ "Comment", "/**\n\x09 * Check if a mapping context is applied to this subsystem's owner.\n\x09 */// TODO: BlueprintPure would be nicer. Move into library?\n" },
		{ "ModuleRelativePath", "Public/EnhancedInputSubsystemInterface.h" },
		{ "ToolTip", "Check if a mapping context is applied to this subsystem's owner.\n        // TODO: BlueprintPure would be nicer. Move into library?" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEnhancedInputSubsystemInterface_HasMappingContext_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEnhancedInputSubsystemInterface, nullptr, "HasMappingContext", nullptr, nullptr, sizeof(EnhancedInputSubsystemInterface_eventHasMappingContext_Parms), Z_Construct_UFunction_UEnhancedInputSubsystemInterface_HasMappingContext_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnhancedInputSubsystemInterface_HasMappingContext_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020408, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEnhancedInputSubsystemInterface_HasMappingContext_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnhancedInputSubsystemInterface_HasMappingContext_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEnhancedInputSubsystemInterface_HasMappingContext()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEnhancedInputSubsystemInterface_HasMappingContext_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryKeysMappedToAction_Statics
	{
		struct EnhancedInputSubsystemInterface_eventQueryKeysMappedToAction_Parms
		{
			const UInputAction* Action;
			TArray<FKey> ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Action_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Action;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryKeysMappedToAction_Statics::NewProp_Action_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryKeysMappedToAction_Statics::NewProp_Action = { "Action", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EnhancedInputSubsystemInterface_eventQueryKeysMappedToAction_Parms, Action), Z_Construct_UClass_UInputAction_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryKeysMappedToAction_Statics::NewProp_Action_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryKeysMappedToAction_Statics::NewProp_Action_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryKeysMappedToAction_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FKey, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryKeysMappedToAction_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EnhancedInputSubsystemInterface_eventQueryKeysMappedToAction_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryKeysMappedToAction_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryKeysMappedToAction_Statics::NewProp_Action,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryKeysMappedToAction_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryKeysMappedToAction_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryKeysMappedToAction_Statics::Function_MetaDataParams[] = {
		{ "Category", "Input|Mapping Queries" },
		{ "Comment", "/**\n\x09 * Returns the keys mapped to the given action in the active input mapping contexts.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EnhancedInputSubsystemInterface.h" },
		{ "ToolTip", "Returns the keys mapped to the given action in the active input mapping contexts." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryKeysMappedToAction_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEnhancedInputSubsystemInterface, nullptr, "QueryKeysMappedToAction", nullptr, nullptr, sizeof(EnhancedInputSubsystemInterface_eventQueryKeysMappedToAction_Parms), Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryKeysMappedToAction_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryKeysMappedToAction_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020408, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryKeysMappedToAction_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryKeysMappedToAction_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryKeysMappedToAction()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryKeysMappedToAction_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInActiveContextSet_Statics
	{
		struct EnhancedInputSubsystemInterface_eventQueryMapKeyInActiveContextSet_Parms
		{
			const UInputMappingContext* InputContext;
			const UInputAction* Action;
			FKey Key;
			TArray<FMappingQueryIssue> OutIssues;
			EMappingQueryIssue BlockingIssues;
			EMappingQueryResult ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputContext_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InputContext;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Action_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Action;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutIssues_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OutIssues;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_BlockingIssues_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_BlockingIssues;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInActiveContextSet_Statics::NewProp_InputContext_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInActiveContextSet_Statics::NewProp_InputContext = { "InputContext", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EnhancedInputSubsystemInterface_eventQueryMapKeyInActiveContextSet_Parms, InputContext), Z_Construct_UClass_UInputMappingContext_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInActiveContextSet_Statics::NewProp_InputContext_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInActiveContextSet_Statics::NewProp_InputContext_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInActiveContextSet_Statics::NewProp_Action_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInActiveContextSet_Statics::NewProp_Action = { "Action", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EnhancedInputSubsystemInterface_eventQueryMapKeyInActiveContextSet_Parms, Action), Z_Construct_UClass_UInputAction_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInActiveContextSet_Statics::NewProp_Action_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInActiveContextSet_Statics::NewProp_Action_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInActiveContextSet_Statics::NewProp_Key = { "Key", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EnhancedInputSubsystemInterface_eventQueryMapKeyInActiveContextSet_Parms, Key), Z_Construct_UScriptStruct_FKey, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInActiveContextSet_Statics::NewProp_OutIssues_Inner = { "OutIssues", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FMappingQueryIssue, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInActiveContextSet_Statics::NewProp_OutIssues = { "OutIssues", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EnhancedInputSubsystemInterface_eventQueryMapKeyInActiveContextSet_Parms, OutIssues), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInActiveContextSet_Statics::NewProp_BlockingIssues_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInActiveContextSet_Statics::NewProp_BlockingIssues = { "BlockingIssues", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EnhancedInputSubsystemInterface_eventQueryMapKeyInActiveContextSet_Parms, BlockingIssues), Z_Construct_UEnum_EnhancedInput_EMappingQueryIssue, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInActiveContextSet_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInActiveContextSet_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EnhancedInputSubsystemInterface_eventQueryMapKeyInActiveContextSet_Parms, ReturnValue), Z_Construct_UEnum_EnhancedInput_EMappingQueryResult, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInActiveContextSet_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInActiveContextSet_Statics::NewProp_InputContext,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInActiveContextSet_Statics::NewProp_Action,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInActiveContextSet_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInActiveContextSet_Statics::NewProp_OutIssues_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInActiveContextSet_Statics::NewProp_OutIssues,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInActiveContextSet_Statics::NewProp_BlockingIssues_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInActiveContextSet_Statics::NewProp_BlockingIssues,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInActiveContextSet_Statics::NewProp_ReturnValue_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInActiveContextSet_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInActiveContextSet_Statics::Function_MetaDataParams[] = {
		{ "Category", "Input|Mapping Queries" },
		{ "Comment", "/* = DefaultMappingIssues::StandardFatal*/" },
		{ "ModuleRelativePath", "Public/EnhancedInputSubsystemInterface.h" },
		{ "ToolTip", "= DefaultMappingIssues::StandardFatal" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInActiveContextSet_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEnhancedInputSubsystemInterface, nullptr, "QueryMapKeyInActiveContextSet", nullptr, nullptr, sizeof(EnhancedInputSubsystemInterface_eventQueryMapKeyInActiveContextSet_Parms), Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInActiveContextSet_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInActiveContextSet_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420408, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInActiveContextSet_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInActiveContextSet_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInActiveContextSet()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInActiveContextSet_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet_Statics
	{
		struct EnhancedInputSubsystemInterface_eventQueryMapKeyInContextSet_Parms
		{
			TArray<UInputMappingContext*> PrioritizedActiveContexts;
			const UInputMappingContext* InputContext;
			const UInputAction* Action;
			FKey Key;
			TArray<FMappingQueryIssue> OutIssues;
			EMappingQueryIssue BlockingIssues;
			EMappingQueryResult ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PrioritizedActiveContexts_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PrioritizedActiveContexts_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PrioritizedActiveContexts;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputContext_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InputContext;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Action_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Action;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutIssues_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OutIssues;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_BlockingIssues_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_BlockingIssues;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet_Statics::NewProp_PrioritizedActiveContexts_Inner = { "PrioritizedActiveContexts", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UInputMappingContext_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet_Statics::NewProp_PrioritizedActiveContexts_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet_Statics::NewProp_PrioritizedActiveContexts = { "PrioritizedActiveContexts", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EnhancedInputSubsystemInterface_eventQueryMapKeyInContextSet_Parms, PrioritizedActiveContexts), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet_Statics::NewProp_PrioritizedActiveContexts_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet_Statics::NewProp_PrioritizedActiveContexts_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet_Statics::NewProp_InputContext_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet_Statics::NewProp_InputContext = { "InputContext", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EnhancedInputSubsystemInterface_eventQueryMapKeyInContextSet_Parms, InputContext), Z_Construct_UClass_UInputMappingContext_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet_Statics::NewProp_InputContext_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet_Statics::NewProp_InputContext_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet_Statics::NewProp_Action_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet_Statics::NewProp_Action = { "Action", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EnhancedInputSubsystemInterface_eventQueryMapKeyInContextSet_Parms, Action), Z_Construct_UClass_UInputAction_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet_Statics::NewProp_Action_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet_Statics::NewProp_Action_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet_Statics::NewProp_Key = { "Key", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EnhancedInputSubsystemInterface_eventQueryMapKeyInContextSet_Parms, Key), Z_Construct_UScriptStruct_FKey, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet_Statics::NewProp_OutIssues_Inner = { "OutIssues", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FMappingQueryIssue, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet_Statics::NewProp_OutIssues = { "OutIssues", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EnhancedInputSubsystemInterface_eventQueryMapKeyInContextSet_Parms, OutIssues), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet_Statics::NewProp_BlockingIssues_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet_Statics::NewProp_BlockingIssues = { "BlockingIssues", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EnhancedInputSubsystemInterface_eventQueryMapKeyInContextSet_Parms, BlockingIssues), Z_Construct_UEnum_EnhancedInput_EMappingQueryIssue, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EnhancedInputSubsystemInterface_eventQueryMapKeyInContextSet_Parms, ReturnValue), Z_Construct_UEnum_EnhancedInput_EMappingQueryResult, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet_Statics::NewProp_PrioritizedActiveContexts_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet_Statics::NewProp_PrioritizedActiveContexts,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet_Statics::NewProp_InputContext,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet_Statics::NewProp_Action,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet_Statics::NewProp_OutIssues_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet_Statics::NewProp_OutIssues,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet_Statics::NewProp_BlockingIssues_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet_Statics::NewProp_BlockingIssues,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet_Statics::NewProp_ReturnValue_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet_Statics::Function_MetaDataParams[] = {
		{ "Category", "Input|Mapping Queries" },
		{ "Comment", "/* = DefaultMappingIssues::StandardFatal*/" },
		{ "ModuleRelativePath", "Public/EnhancedInputSubsystemInterface.h" },
		{ "ToolTip", "= DefaultMappingIssues::StandardFatal" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEnhancedInputSubsystemInterface, nullptr, "QueryMapKeyInContextSet", nullptr, nullptr, sizeof(EnhancedInputSubsystemInterface_eventQueryMapKeyInContextSet_Parms), Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420408, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEnhancedInputSubsystemInterface_RemoveMappingContext_Statics
	{
		struct EnhancedInputSubsystemInterface_eventRemoveMappingContext_Parms
		{
			const UInputMappingContext* MappingContext;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MappingContext_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MappingContext;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEnhancedInputSubsystemInterface_RemoveMappingContext_Statics::NewProp_MappingContext_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEnhancedInputSubsystemInterface_RemoveMappingContext_Statics::NewProp_MappingContext = { "MappingContext", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EnhancedInputSubsystemInterface_eventRemoveMappingContext_Parms, MappingContext), Z_Construct_UClass_UInputMappingContext_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UEnhancedInputSubsystemInterface_RemoveMappingContext_Statics::NewProp_MappingContext_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnhancedInputSubsystemInterface_RemoveMappingContext_Statics::NewProp_MappingContext_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEnhancedInputSubsystemInterface_RemoveMappingContext_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnhancedInputSubsystemInterface_RemoveMappingContext_Statics::NewProp_MappingContext,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEnhancedInputSubsystemInterface_RemoveMappingContext_Statics::Function_MetaDataParams[] = {
		{ "Category", "Input" },
		{ "Comment", "/**\n\x09 * Remove a specific control context. \n\x09 * This is safe to call even if the context is not applied.\n\x09 * @param MappingContext\x09\x09""Context to remove from the player\n\x09 */" },
		{ "ModuleRelativePath", "Public/EnhancedInputSubsystemInterface.h" },
		{ "ToolTip", "Remove a specific control context.\nThis is safe to call even if the context is not applied.\n@param MappingContext                Context to remove from the player" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEnhancedInputSubsystemInterface_RemoveMappingContext_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEnhancedInputSubsystemInterface, nullptr, "RemoveMappingContext", nullptr, nullptr, sizeof(EnhancedInputSubsystemInterface_eventRemoveMappingContext_Parms), Z_Construct_UFunction_UEnhancedInputSubsystemInterface_RemoveMappingContext_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnhancedInputSubsystemInterface_RemoveMappingContext_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020408, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEnhancedInputSubsystemInterface_RemoveMappingContext_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnhancedInputSubsystemInterface_RemoveMappingContext_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEnhancedInputSubsystemInterface_RemoveMappingContext()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEnhancedInputSubsystemInterface_RemoveMappingContext_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEnhancedInputSubsystemInterface_RequestRebuildControlMappings_Statics
	{
		struct EnhancedInputSubsystemInterface_eventRequestRebuildControlMappings_Parms
		{
			bool bForceImmediately;
		};
		static void NewProp_bForceImmediately_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bForceImmediately;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UEnhancedInputSubsystemInterface_RequestRebuildControlMappings_Statics::NewProp_bForceImmediately_SetBit(void* Obj)
	{
		((EnhancedInputSubsystemInterface_eventRequestRebuildControlMappings_Parms*)Obj)->bForceImmediately = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEnhancedInputSubsystemInterface_RequestRebuildControlMappings_Statics::NewProp_bForceImmediately = { "bForceImmediately", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EnhancedInputSubsystemInterface_eventRequestRebuildControlMappings_Parms), &Z_Construct_UFunction_UEnhancedInputSubsystemInterface_RequestRebuildControlMappings_Statics::NewProp_bForceImmediately_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEnhancedInputSubsystemInterface_RequestRebuildControlMappings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnhancedInputSubsystemInterface_RequestRebuildControlMappings_Statics::NewProp_bForceImmediately,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEnhancedInputSubsystemInterface_RequestRebuildControlMappings_Statics::Function_MetaDataParams[] = {
		{ "Category", "Input" },
		{ "Comment", "/**\n\x09 * Flag player for reapplication of all mapping contexts at the end of this frame.\n\x09 * This is called automatically when adding or removing mappings contexts.\n\x09 * @param bForceImmediately\x09\x09THe mapping changes will be applied synchronously, rather than at the end of the frame, making them available to the input system on the same frame.\n\x09 */" },
		{ "CPP_Default_bForceImmediately", "false" },
		{ "ModuleRelativePath", "Public/EnhancedInputSubsystemInterface.h" },
		{ "ToolTip", "Flag player for reapplication of all mapping contexts at the end of this frame.\nThis is called automatically when adding or removing mappings contexts.\n@param bForceImmediately             THe mapping changes will be applied synchronously, rather than at the end of the frame, making them available to the input system on the same frame." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEnhancedInputSubsystemInterface_RequestRebuildControlMappings_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEnhancedInputSubsystemInterface, nullptr, "RequestRebuildControlMappings", nullptr, nullptr, sizeof(EnhancedInputSubsystemInterface_eventRequestRebuildControlMappings_Parms), Z_Construct_UFunction_UEnhancedInputSubsystemInterface_RequestRebuildControlMappings_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnhancedInputSubsystemInterface_RequestRebuildControlMappings_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020408, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEnhancedInputSubsystemInterface_RequestRebuildControlMappings_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnhancedInputSubsystemInterface_RequestRebuildControlMappings_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEnhancedInputSubsystemInterface_RequestRebuildControlMappings()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEnhancedInputSubsystemInterface_RequestRebuildControlMappings_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UEnhancedInputSubsystemInterface_NoRegister()
	{
		return UEnhancedInputSubsystemInterface::StaticClass();
	}
	struct Z_Construct_UClass_UEnhancedInputSubsystemInterface_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEnhancedInputSubsystemInterface_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInterface,
		(UObject* (*)())Z_Construct_UPackage__Script_EnhancedInput,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UEnhancedInputSubsystemInterface_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UEnhancedInputSubsystemInterface_AddMappingContext, "AddMappingContext" }, // 3133656810
		{ &Z_Construct_UFunction_UEnhancedInputSubsystemInterface_ClearAllMappings, "ClearAllMappings" }, // 45875681
		{ &Z_Construct_UFunction_UEnhancedInputSubsystemInterface_HasMappingContext, "HasMappingContext" }, // 563909511
		{ &Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryKeysMappedToAction, "QueryKeysMappedToAction" }, // 3773718278
		{ &Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInActiveContextSet, "QueryMapKeyInActiveContextSet" }, // 4099152279
		{ &Z_Construct_UFunction_UEnhancedInputSubsystemInterface_QueryMapKeyInContextSet, "QueryMapKeyInContextSet" }, // 2924738876
		{ &Z_Construct_UFunction_UEnhancedInputSubsystemInterface_RemoveMappingContext, "RemoveMappingContext" }, // 2333652215
		{ &Z_Construct_UFunction_UEnhancedInputSubsystemInterface_RequestRebuildControlMappings, "RequestRebuildControlMappings" }, // 2444527976
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEnhancedInputSubsystemInterface_Statics::Class_MetaDataParams[] = {
		{ "CannotImplementInterfaceInBlueprint", "" },
		{ "ModuleRelativePath", "Public/EnhancedInputSubsystemInterface.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEnhancedInputSubsystemInterface_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<IEnhancedInputSubsystemInterface>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEnhancedInputSubsystemInterface_Statics::ClassParams = {
		&UEnhancedInputSubsystemInterface::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x000840A1u,
		METADATA_PARAMS(Z_Construct_UClass_UEnhancedInputSubsystemInterface_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEnhancedInputSubsystemInterface_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEnhancedInputSubsystemInterface()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEnhancedInputSubsystemInterface_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEnhancedInputSubsystemInterface, 980662290);
	template<> ENHANCEDINPUT_API UClass* StaticClass<UEnhancedInputSubsystemInterface>()
	{
		return UEnhancedInputSubsystemInterface::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEnhancedInputSubsystemInterface(Z_Construct_UClass_UEnhancedInputSubsystemInterface, &UEnhancedInputSubsystemInterface::StaticClass, TEXT("/Script/EnhancedInput"), TEXT("UEnhancedInputSubsystemInterface"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEnhancedInputSubsystemInterface);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
