// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "EnhancedInput/Public/InputActionValue.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeInputActionValue() {}
// Cross Module References
	ENHANCEDINPUT_API UEnum* Z_Construct_UEnum_EnhancedInput_EInputActionValueType();
	UPackage* Z_Construct_UPackage__Script_EnhancedInput();
	ENHANCEDINPUT_API UScriptStruct* Z_Construct_UScriptStruct_FInputActionValue();
// End Cross Module References
	static UEnum* EInputActionValueType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_EnhancedInput_EInputActionValueType, Z_Construct_UPackage__Script_EnhancedInput(), TEXT("EInputActionValueType"));
		}
		return Singleton;
	}
	template<> ENHANCEDINPUT_API UEnum* StaticEnum<EInputActionValueType>()
	{
		return EInputActionValueType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EInputActionValueType(EInputActionValueType_StaticEnum, TEXT("/Script/EnhancedInput"), TEXT("EInputActionValueType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_EnhancedInput_EInputActionValueType_Hash() { return 2092369164U; }
	UEnum* Z_Construct_UEnum_EnhancedInput_EInputActionValueType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_EnhancedInput();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EInputActionValueType"), 0, Get_Z_Construct_UEnum_EnhancedInput_EInputActionValueType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EInputActionValueType::Boolean", (int64)EInputActionValueType::Boolean },
				{ "EInputActionValueType::Axis1D", (int64)EInputActionValueType::Axis1D },
				{ "EInputActionValueType::Axis2D", (int64)EInputActionValueType::Axis2D },
				{ "EInputActionValueType::Axis3D", (int64)EInputActionValueType::Axis3D },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Axis1D.DisplayName", "Axis1D (float)" },
				{ "Axis1D.Name", "EInputActionValueType::Axis1D" },
				{ "Axis2D.DisplayName", "Axis2D (Vector2D)" },
				{ "Axis2D.Name", "EInputActionValueType::Axis2D" },
				{ "Axis3D.DisplayName", "Axis3D (Vector)" },
				{ "Axis3D.Name", "EInputActionValueType::Axis3D" },
				{ "BlueprintType", "true" },
				{ "Boolean.Comment", "// Value types in increasing size order (used for type promotion)\n// Name these Digital/Analog?\n" },
				{ "Boolean.DisplayName", "Digital (bool)" },
				{ "Boolean.Name", "EInputActionValueType::Boolean" },
				{ "Boolean.ToolTip", "Value types in increasing size order (used for type promotion)\nName these Digital/Analog?" },
				{ "ModuleRelativePath", "Public/InputActionValue.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_EnhancedInput,
				nullptr,
				"EInputActionValueType",
				"EInputActionValueType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FInputActionValue::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ENHANCEDINPUT_API uint32 Get_Z_Construct_UScriptStruct_FInputActionValue_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FInputActionValue, Z_Construct_UPackage__Script_EnhancedInput(), TEXT("InputActionValue"), sizeof(FInputActionValue), Get_Z_Construct_UScriptStruct_FInputActionValue_Hash());
	}
	return Singleton;
}
template<> ENHANCEDINPUT_API UScriptStruct* StaticStruct<FInputActionValue>()
{
	return FInputActionValue::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FInputActionValue(FInputActionValue::StaticStruct, TEXT("/Script/EnhancedInput"), TEXT("InputActionValue"), false, nullptr, nullptr);
static struct FScriptStruct_EnhancedInput_StaticRegisterNativesFInputActionValue
{
	FScriptStruct_EnhancedInput_StaticRegisterNativesFInputActionValue()
	{
		UScriptStruct::DeferCppStructOps<FInputActionValue>(FName(TEXT("InputActionValue")));
	}
} ScriptStruct_EnhancedInput_StaticRegisterNativesFInputActionValue;
	struct Z_Construct_UScriptStruct_FInputActionValue_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FInputActionValue_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "HasNativeBreak", "EnhancedInput.EnhancedInputLibrary.BreakInputActionValue" },
		{ "HasNativeMake", "EnhancedInput.EnhancedInputLibrary.MakeInputActionValue" },
		{ "ModuleRelativePath", "Public/InputActionValue.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FInputActionValue_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FInputActionValue>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FInputActionValue_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_EnhancedInput,
		nullptr,
		&NewStructOps,
		"InputActionValue",
		sizeof(FInputActionValue),
		alignof(FInputActionValue),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FInputActionValue_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FInputActionValue_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FInputActionValue()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FInputActionValue_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_EnhancedInput();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("InputActionValue"), sizeof(FInputActionValue), Get_Z_Construct_UScriptStruct_FInputActionValue_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FInputActionValue_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FInputActionValue_Hash() { return 3212591421U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
