// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "EnhancedInput/Public/EnhancedPlayerInput.h"
#include "Engine/Classes/GameFramework/PlayerController.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEnhancedPlayerInput() {}
// Cross Module References
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UEnhancedPlayerInput_NoRegister();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UEnhancedPlayerInput();
	ENGINE_API UClass* Z_Construct_UClass_UPlayerInput();
	UPackage* Z_Construct_UPackage__Script_EnhancedInput();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputMappingContext_NoRegister();
	ENHANCEDINPUT_API UScriptStruct* Z_Construct_UScriptStruct_FEnhancedActionKeyMapping();
	ENHANCEDINPUT_API UScriptStruct* Z_Construct_UScriptStruct_FInputActionInstance();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputAction_NoRegister();
// End Cross Module References
	void UEnhancedPlayerInput::StaticRegisterNativesUEnhancedPlayerInput()
	{
	}
	UClass* Z_Construct_UClass_UEnhancedPlayerInput_NoRegister()
	{
		return UEnhancedPlayerInput::StaticClass();
	}
	struct Z_Construct_UClass_UEnhancedPlayerInput_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_AppliedInputContexts_ValueProp;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AppliedInputContexts_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AppliedInputContexts_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_AppliedInputContexts;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EnhancedActionMappings_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EnhancedActionMappings_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_EnhancedActionMappings;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ActionInstanceData_ValueProp;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ActionInstanceData_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActionInstanceData_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_ActionInstanceData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEnhancedPlayerInput_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UPlayerInput,
		(UObject* (*)())Z_Construct_UPackage__Script_EnhancedInput,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEnhancedPlayerInput_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n* UEnhancedPlayerInput : UPlayerInput extensions for enhanced player input system\n*/" },
		{ "IncludePath", "EnhancedPlayerInput.h" },
		{ "ModuleRelativePath", "Public/EnhancedPlayerInput.h" },
		{ "ToolTip", "UEnhancedPlayerInput : UPlayerInput extensions for enhanced player input system" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UEnhancedPlayerInput_Statics::NewProp_AppliedInputContexts_ValueProp = { "AppliedInputContexts", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UEnhancedPlayerInput_Statics::NewProp_AppliedInputContexts_Key_KeyProp = { "AppliedInputContexts_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UInputMappingContext_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEnhancedPlayerInput_Statics::NewProp_AppliedInputContexts_MetaData[] = {
		{ "Comment", "/** Currently applied key mappings\n\x09 * Note: Source reference only. Use EnhancedActionMappings for the actual mappings (with properly instanced triggers/modifiers)\n\x09 */" },
		{ "ModuleRelativePath", "Public/EnhancedPlayerInput.h" },
		{ "ToolTip", "Currently applied key mappings\nNote: Source reference only. Use EnhancedActionMappings for the actual mappings (with properly instanced triggers/modifiers)" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UEnhancedPlayerInput_Statics::NewProp_AppliedInputContexts = { "AppliedInputContexts", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEnhancedPlayerInput, AppliedInputContexts), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UEnhancedPlayerInput_Statics::NewProp_AppliedInputContexts_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEnhancedPlayerInput_Statics::NewProp_AppliedInputContexts_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UEnhancedPlayerInput_Statics::NewProp_EnhancedActionMappings_Inner = { "EnhancedActionMappings", nullptr, (EPropertyFlags)0x0000008000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FEnhancedActionKeyMapping, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEnhancedPlayerInput_Statics::NewProp_EnhancedActionMappings_MetaData[] = {
		{ "Comment", "/** This player's version of the Action Mappings */" },
		{ "ModuleRelativePath", "Public/EnhancedPlayerInput.h" },
		{ "ToolTip", "This player's version of the Action Mappings" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UEnhancedPlayerInput_Statics::NewProp_EnhancedActionMappings = { "EnhancedActionMappings", nullptr, (EPropertyFlags)0x0040008000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEnhancedPlayerInput, EnhancedActionMappings), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UEnhancedPlayerInput_Statics::NewProp_EnhancedActionMappings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEnhancedPlayerInput_Statics::NewProp_EnhancedActionMappings_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UEnhancedPlayerInput_Statics::NewProp_ActionInstanceData_ValueProp = { "ActionInstanceData", nullptr, (EPropertyFlags)0x0000008000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FInputActionInstance, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UEnhancedPlayerInput_Statics::NewProp_ActionInstanceData_Key_KeyProp = { "ActionInstanceData_Key", nullptr, (EPropertyFlags)0x0000008000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UInputAction_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEnhancedPlayerInput_Statics::NewProp_ActionInstanceData_MetaData[] = {
		{ "Comment", "/** Tracked action values. Queryable. */" },
		{ "ModuleRelativePath", "Public/EnhancedPlayerInput.h" },
		{ "ToolTip", "Tracked action values. Queryable." },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UEnhancedPlayerInput_Statics::NewProp_ActionInstanceData = { "ActionInstanceData", nullptr, (EPropertyFlags)0x0040008000002000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEnhancedPlayerInput, ActionInstanceData), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UEnhancedPlayerInput_Statics::NewProp_ActionInstanceData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEnhancedPlayerInput_Statics::NewProp_ActionInstanceData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UEnhancedPlayerInput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEnhancedPlayerInput_Statics::NewProp_AppliedInputContexts_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEnhancedPlayerInput_Statics::NewProp_AppliedInputContexts_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEnhancedPlayerInput_Statics::NewProp_AppliedInputContexts,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEnhancedPlayerInput_Statics::NewProp_EnhancedActionMappings_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEnhancedPlayerInput_Statics::NewProp_EnhancedActionMappings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEnhancedPlayerInput_Statics::NewProp_ActionInstanceData_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEnhancedPlayerInput_Statics::NewProp_ActionInstanceData_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEnhancedPlayerInput_Statics::NewProp_ActionInstanceData,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEnhancedPlayerInput_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEnhancedPlayerInput>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEnhancedPlayerInput_Statics::ClassParams = {
		&UEnhancedPlayerInput::StaticClass,
		"Input",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UEnhancedPlayerInput_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UEnhancedPlayerInput_Statics::PropPointers),
		0,
		0x009000ACu,
		METADATA_PARAMS(Z_Construct_UClass_UEnhancedPlayerInput_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEnhancedPlayerInput_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEnhancedPlayerInput()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEnhancedPlayerInput_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEnhancedPlayerInput, 3537166899);
	template<> ENHANCEDINPUT_API UClass* StaticClass<UEnhancedPlayerInput>()
	{
		return UEnhancedPlayerInput::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEnhancedPlayerInput(Z_Construct_UClass_UEnhancedPlayerInput, &UEnhancedPlayerInput::StaticClass, TEXT("/Script/EnhancedInput"), TEXT("UEnhancedPlayerInput"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEnhancedPlayerInput);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
