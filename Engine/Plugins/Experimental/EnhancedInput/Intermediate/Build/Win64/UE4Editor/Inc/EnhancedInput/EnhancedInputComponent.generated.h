// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FInputActionValue;
struct FKey;
class UInputAction;
#ifdef ENHANCEDINPUT_EnhancedInputComponent_generated_h
#error "EnhancedInputComponent.generated.h already included, missing '#pragma once' in EnhancedInputComponent.h"
#endif
#define ENHANCEDINPUT_EnhancedInputComponent_generated_h

#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputComponent_h_27_DELEGATE \
struct _Script_EnhancedInput_eventEnhancedInputActionHandlerDynamicSignature_Parms \
{ \
	FInputActionValue ActionValue; \
	float ElapsedTime; \
	float TriggeredTime; \
}; \
static inline void FEnhancedInputActionHandlerDynamicSignature_DelegateWrapper(const FScriptDelegate& EnhancedInputActionHandlerDynamicSignature, FInputActionValue ActionValue, float ElapsedTime, float TriggeredTime) \
{ \
	_Script_EnhancedInput_eventEnhancedInputActionHandlerDynamicSignature_Parms Parms; \
	Parms.ActionValue=ActionValue; \
	Parms.ElapsedTime=ElapsedTime; \
	Parms.TriggeredTime=TriggeredTime; \
	EnhancedInputActionHandlerDynamicSignature.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputComponent_h_21_DELEGATE \
struct _Script_EnhancedInput_eventInputDebugKeyHandlerDynamicSignature_Parms \
{ \
	FKey Key; \
}; \
static inline void FInputDebugKeyHandlerDynamicSignature_DelegateWrapper(const FScriptDelegate& InputDebugKeyHandlerDynamicSignature, FKey Key) \
{ \
	_Script_EnhancedInput_eventInputDebugKeyHandlerDynamicSignature_Parms Parms; \
	Parms.Key=Key; \
	InputDebugKeyHandlerDynamicSignature.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputComponent_h_287_SPARSE_DATA
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputComponent_h_287_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetBoundActionValue);


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputComponent_h_287_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetBoundActionValue);


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputComponent_h_287_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUEnhancedInputComponent(); \
	friend struct Z_Construct_UClass_UEnhancedInputComponent_Statics; \
public: \
	DECLARE_CLASS(UEnhancedInputComponent, UInputComponent, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/EnhancedInput"), NO_API) \
	DECLARE_SERIALIZER(UEnhancedInputComponent)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputComponent_h_287_INCLASS \
private: \
	static void StaticRegisterNativesUEnhancedInputComponent(); \
	friend struct Z_Construct_UClass_UEnhancedInputComponent_Statics; \
public: \
	DECLARE_CLASS(UEnhancedInputComponent, UInputComponent, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/EnhancedInput"), NO_API) \
	DECLARE_SERIALIZER(UEnhancedInputComponent)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputComponent_h_287_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEnhancedInputComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEnhancedInputComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEnhancedInputComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEnhancedInputComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEnhancedInputComponent(UEnhancedInputComponent&&); \
	NO_API UEnhancedInputComponent(const UEnhancedInputComponent&); \
public:


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputComponent_h_287_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEnhancedInputComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEnhancedInputComponent(UEnhancedInputComponent&&); \
	NO_API UEnhancedInputComponent(const UEnhancedInputComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEnhancedInputComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEnhancedInputComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEnhancedInputComponent)


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputComponent_h_287_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputComponent_h_283_PROLOG
#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputComponent_h_287_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputComponent_h_287_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputComponent_h_287_SPARSE_DATA \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputComponent_h_287_RPC_WRAPPERS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputComponent_h_287_INCLASS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputComponent_h_287_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputComponent_h_287_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputComponent_h_287_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputComponent_h_287_SPARSE_DATA \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputComponent_h_287_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputComponent_h_287_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputComponent_h_287_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class EnhancedInputComponent."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ENHANCEDINPUT_API UClass* StaticClass<class UEnhancedInputComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_EnhancedInput_Source_EnhancedInput_Public_EnhancedInputComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
