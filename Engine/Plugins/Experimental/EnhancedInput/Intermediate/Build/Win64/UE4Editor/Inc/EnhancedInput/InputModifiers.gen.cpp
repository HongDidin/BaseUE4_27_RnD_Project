// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "EnhancedInput/Public/InputModifiers.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeInputModifiers() {}
// Cross Module References
	ENHANCEDINPUT_API UEnum* Z_Construct_UEnum_EnhancedInput_EInputAxisSwizzle();
	UPackage* Z_Construct_UPackage__Script_EnhancedInput();
	ENHANCEDINPUT_API UEnum* Z_Construct_UEnum_EnhancedInput_EFOVScalingType();
	ENHANCEDINPUT_API UEnum* Z_Construct_UEnum_EnhancedInput_EDeadZoneType();
	ENHANCEDINPUT_API UEnum* Z_Construct_UEnum_EnhancedInput_EModifierExecutionPhase();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputModifier_NoRegister();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputModifier();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	ENHANCEDINPUT_API UScriptStruct* Z_Construct_UScriptStruct_FInputActionValue();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UEnhancedPlayerInput_NoRegister();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputModifierDeadZone_NoRegister();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputModifierDeadZone();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputModifierScalar_NoRegister();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputModifierScalar();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputModifierNegate_NoRegister();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputModifierNegate();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputModifierSmooth_NoRegister();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputModifierSmooth();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputModifierResponseCurveExponential_NoRegister();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputModifierResponseCurveExponential();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputModifierResponseCurveUser_NoRegister();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputModifierResponseCurveUser();
	ENGINE_API UClass* Z_Construct_UClass_UCurveFloat_NoRegister();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputModifierFOVScaling_NoRegister();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputModifierFOVScaling();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputModifierToWorldSpace_NoRegister();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputModifierToWorldSpace();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputModifierSwizzleAxis_NoRegister();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputModifierSwizzleAxis();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputModifierCollection_NoRegister();
	ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputModifierCollection();
// End Cross Module References
	static UEnum* EInputAxisSwizzle_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_EnhancedInput_EInputAxisSwizzle, Z_Construct_UPackage__Script_EnhancedInput(), TEXT("EInputAxisSwizzle"));
		}
		return Singleton;
	}
	template<> ENHANCEDINPUT_API UEnum* StaticEnum<EInputAxisSwizzle>()
	{
		return EInputAxisSwizzle_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EInputAxisSwizzle(EInputAxisSwizzle_StaticEnum, TEXT("/Script/EnhancedInput"), TEXT("EInputAxisSwizzle"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_EnhancedInput_EInputAxisSwizzle_Hash() { return 2956798033U; }
	UEnum* Z_Construct_UEnum_EnhancedInput_EInputAxisSwizzle()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_EnhancedInput();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EInputAxisSwizzle"), 0, Get_Z_Construct_UEnum_EnhancedInput_EInputAxisSwizzle_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EInputAxisSwizzle::YXZ", (int64)EInputAxisSwizzle::YXZ },
				{ "EInputAxisSwizzle::ZYX", (int64)EInputAxisSwizzle::ZYX },
				{ "EInputAxisSwizzle::XZY", (int64)EInputAxisSwizzle::XZY },
				{ "EInputAxisSwizzle::YZX", (int64)EInputAxisSwizzle::YZX },
				{ "EInputAxisSwizzle::ZXY", (int64)EInputAxisSwizzle::ZXY },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/InputModifiers.h" },
				{ "XZY.Comment", "// Swap Y and Z axis\n" },
				{ "XZY.Name", "EInputAxisSwizzle::XZY" },
				{ "XZY.ToolTip", "Swap Y and Z axis" },
				{ "YXZ.Comment", "// Swap X and Y axis. Useful for binding 1D inputs to the Y axis for 2D actions.\n" },
				{ "YXZ.Name", "EInputAxisSwizzle::YXZ" },
				{ "YXZ.ToolTip", "Swap X and Y axis. Useful for binding 1D inputs to the Y axis for 2D actions." },
				{ "YZX.Comment", "// Reorder all axes, Y first\n" },
				{ "YZX.Name", "EInputAxisSwizzle::YZX" },
				{ "YZX.ToolTip", "Reorder all axes, Y first" },
				{ "ZXY.Comment", "// Reorder all axes, Z first\n" },
				{ "ZXY.Name", "EInputAxisSwizzle::ZXY" },
				{ "ZXY.ToolTip", "Reorder all axes, Z first" },
				{ "ZYX.Comment", "// Swap X and Z axis\n" },
				{ "ZYX.Name", "EInputAxisSwizzle::ZYX" },
				{ "ZYX.ToolTip", "Swap X and Z axis" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_EnhancedInput,
				nullptr,
				"EInputAxisSwizzle",
				"EInputAxisSwizzle",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EFOVScalingType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_EnhancedInput_EFOVScalingType, Z_Construct_UPackage__Script_EnhancedInput(), TEXT("EFOVScalingType"));
		}
		return Singleton;
	}
	template<> ENHANCEDINPUT_API UEnum* StaticEnum<EFOVScalingType>()
	{
		return EFOVScalingType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EFOVScalingType(EFOVScalingType_StaticEnum, TEXT("/Script/EnhancedInput"), TEXT("EFOVScalingType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_EnhancedInput_EFOVScalingType_Hash() { return 526922618U; }
	UEnum* Z_Construct_UEnum_EnhancedInput_EFOVScalingType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_EnhancedInput();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EFOVScalingType"), 0, Get_Z_Construct_UEnum_EnhancedInput_EFOVScalingType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EFOVScalingType::Standard", (int64)EFOVScalingType::Standard },
				{ "EFOVScalingType::UE4_BackCompat", (int64)EFOVScalingType::UE4_BackCompat },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/InputModifiers.h" },
				{ "Standard.Comment", "// FOV scaling to apply scaled movement deltas to inputs dependent upon the player's selected FOV\n" },
				{ "Standard.Name", "EFOVScalingType::Standard" },
				{ "Standard.ToolTip", "FOV scaling to apply scaled movement deltas to inputs dependent upon the player's selected FOV" },
				{ "UE4_BackCompat.Comment", "// FOV scaling was incorrectly calculated in UE4's UPlayerInput::MassageAxisInput. This implementation is intended to aid backwards compatibility, but should not be used by new projects.\n" },
				{ "UE4_BackCompat.Name", "EFOVScalingType::UE4_BackCompat" },
				{ "UE4_BackCompat.ToolTip", "FOV scaling was incorrectly calculated in UE4's UPlayerInput::MassageAxisInput. This implementation is intended to aid backwards compatibility, but should not be used by new projects." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_EnhancedInput,
				nullptr,
				"EFOVScalingType",
				"EFOVScalingType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EDeadZoneType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_EnhancedInput_EDeadZoneType, Z_Construct_UPackage__Script_EnhancedInput(), TEXT("EDeadZoneType"));
		}
		return Singleton;
	}
	template<> ENHANCEDINPUT_API UEnum* StaticEnum<EDeadZoneType>()
	{
		return EDeadZoneType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDeadZoneType(EDeadZoneType_StaticEnum, TEXT("/Script/EnhancedInput"), TEXT("EDeadZoneType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_EnhancedInput_EDeadZoneType_Hash() { return 4239386215U; }
	UEnum* Z_Construct_UEnum_EnhancedInput_EDeadZoneType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_EnhancedInput();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDeadZoneType"), 0, Get_Z_Construct_UEnum_EnhancedInput_EDeadZoneType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDeadZoneType::Axial", (int64)EDeadZoneType::Axial },
				{ "EDeadZoneType::Radial", (int64)EDeadZoneType::Radial },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Axial.Comment", "// Apply dead zone to axes individually. This will result in input being chamfered at the corners for 2d/3d axis inputs, and matches the original UE4 deadzone logic.\n" },
				{ "Axial.Name", "EDeadZoneType::Axial" },
				{ "Axial.ToolTip", "Apply dead zone to axes individually. This will result in input being chamfered at the corners for 2d/3d axis inputs, and matches the original UE4 deadzone logic." },
				{ "ModuleRelativePath", "Public/InputModifiers.h" },
				{ "Radial.Comment", "// Apply dead zone logic to all axes simultaneously. This gives smooth input (circular/spherical coverage). On a 1d axis input this works identically to Axial.\n" },
				{ "Radial.Name", "EDeadZoneType::Radial" },
				{ "Radial.ToolTip", "Apply dead zone logic to all axes simultaneously. This gives smooth input (circular/spherical coverage). On a 1d axis input this works identically to Axial." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_EnhancedInput,
				nullptr,
				"EDeadZoneType",
				"EDeadZoneType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EModifierExecutionPhase_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_EnhancedInput_EModifierExecutionPhase, Z_Construct_UPackage__Script_EnhancedInput(), TEXT("EModifierExecutionPhase"));
		}
		return Singleton;
	}
	template<> ENHANCEDINPUT_API UEnum* StaticEnum<EModifierExecutionPhase>()
	{
		return EModifierExecutionPhase_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EModifierExecutionPhase(EModifierExecutionPhase_StaticEnum, TEXT("/Script/EnhancedInput"), TEXT("EModifierExecutionPhase"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_EnhancedInput_EModifierExecutionPhase_Hash() { return 3127954181U; }
	UEnum* Z_Construct_UEnum_EnhancedInput_EModifierExecutionPhase()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_EnhancedInput();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EModifierExecutionPhase"), 0, Get_Z_Construct_UEnum_EnhancedInput_EModifierExecutionPhase_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EModifierExecutionPhase::PerInput", (int64)EModifierExecutionPhase::PerInput },
				{ "EModifierExecutionPhase::FinalValue", (int64)EModifierExecutionPhase::FinalValue },
				{ "EModifierExecutionPhase::NumPhases", (int64)EModifierExecutionPhase::NumPhases },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Comment", "// NOTE: Deprecated. Do not use.\n" },
				{ "FinalValue.Comment", "// Deprecated. Do not use.\n" },
				{ "FinalValue.Name", "EModifierExecutionPhase::FinalValue" },
				{ "FinalValue.ToolTip", "Deprecated. Do not use." },
				{ "ModuleRelativePath", "Public/InputModifiers.h" },
				{ "NumPhases.Hidden", "" },
				{ "NumPhases.Name", "EModifierExecutionPhase::NumPhases" },
				{ "PerInput.Comment", "// Deprecated. Do not use.\n" },
				{ "PerInput.Name", "EModifierExecutionPhase::PerInput" },
				{ "PerInput.ToolTip", "Deprecated. Do not use." },
				{ "ToolTip", "NOTE: Deprecated. Do not use." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_EnhancedInput,
				nullptr,
				"EModifierExecutionPhase",
				"EModifierExecutionPhase",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	DEFINE_FUNCTION(UInputModifier::execGetVisualizationColor)
	{
		P_GET_STRUCT(FInputActionValue,Z_Param_SampleValue);
		P_GET_STRUCT(FInputActionValue,Z_Param_FinalValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FLinearColor*)Z_Param__Result=P_THIS->GetVisualizationColor_Implementation(Z_Param_SampleValue,Z_Param_FinalValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UInputModifier::execGetExecutionPhase)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(EModifierExecutionPhase*)Z_Param__Result=P_THIS->GetExecutionPhase_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UInputModifier::execModifyRaw)
	{
		P_GET_OBJECT(UEnhancedPlayerInput,Z_Param_PlayerInput);
		P_GET_STRUCT(FInputActionValue,Z_Param_CurrentValue);
		P_GET_PROPERTY(FFloatProperty,Z_Param_DeltaTime);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FInputActionValue*)Z_Param__Result=P_THIS->ModifyRaw_Implementation(Z_Param_PlayerInput,Z_Param_CurrentValue,Z_Param_DeltaTime);
		P_NATIVE_END;
	}
	static FName NAME_UInputModifier_GetExecutionPhase = FName(TEXT("GetExecutionPhase"));
	EModifierExecutionPhase UInputModifier::GetExecutionPhase() const
	{
		InputModifier_eventGetExecutionPhase_Parms Parms;
		const_cast<UInputModifier*>(this)->ProcessEvent(FindFunctionChecked(NAME_UInputModifier_GetExecutionPhase),&Parms);
		return Parms.ReturnValue;
	}
	static FName NAME_UInputModifier_GetVisualizationColor = FName(TEXT("GetVisualizationColor"));
	FLinearColor UInputModifier::GetVisualizationColor(FInputActionValue SampleValue, FInputActionValue FinalValue) const
	{
		InputModifier_eventGetVisualizationColor_Parms Parms;
		Parms.SampleValue=SampleValue;
		Parms.FinalValue=FinalValue;
		const_cast<UInputModifier*>(this)->ProcessEvent(FindFunctionChecked(NAME_UInputModifier_GetVisualizationColor),&Parms);
		return Parms.ReturnValue;
	}
	static FName NAME_UInputModifier_ModifyRaw = FName(TEXT("ModifyRaw"));
	FInputActionValue UInputModifier::ModifyRaw(const UEnhancedPlayerInput* PlayerInput, FInputActionValue CurrentValue, float DeltaTime) const
	{
		InputModifier_eventModifyRaw_Parms Parms;
		Parms.PlayerInput=PlayerInput;
		Parms.CurrentValue=CurrentValue;
		Parms.DeltaTime=DeltaTime;
		const_cast<UInputModifier*>(this)->ProcessEvent(FindFunctionChecked(NAME_UInputModifier_ModifyRaw),&Parms);
		return Parms.ReturnValue;
	}
	void UInputModifier::StaticRegisterNativesUInputModifier()
	{
		UClass* Class = UInputModifier::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetExecutionPhase", &UInputModifier::execGetExecutionPhase },
			{ "GetVisualizationColor", &UInputModifier::execGetVisualizationColor },
			{ "ModifyRaw", &UInputModifier::execModifyRaw },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UInputModifier_GetExecutionPhase_Statics
	{
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UInputModifier_GetExecutionPhase_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UInputModifier_GetExecutionPhase_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(InputModifier_eventGetExecutionPhase_Parms, ReturnValue), Z_Construct_UEnum_EnhancedInput_EModifierExecutionPhase, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UInputModifier_GetExecutionPhase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UInputModifier_GetExecutionPhase_Statics::NewProp_ReturnValue_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UInputModifier_GetExecutionPhase_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UInputModifier_GetExecutionPhase_Statics::Function_MetaDataParams[] = {
		{ "Category", "Modifier" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Execution phase is deprecated." },
		{ "ModuleRelativePath", "Public/InputModifiers.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UInputModifier_GetExecutionPhase_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UInputModifier, nullptr, "GetExecutionPhase", nullptr, nullptr, sizeof(InputModifier_eventGetExecutionPhase_Parms), Z_Construct_UFunction_UInputModifier_GetExecutionPhase_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UInputModifier_GetExecutionPhase_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x48020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UInputModifier_GetExecutionPhase_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UInputModifier_GetExecutionPhase_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UInputModifier_GetExecutionPhase()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UInputModifier_GetExecutionPhase_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UInputModifier_GetVisualizationColor_Statics
	{
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SampleValue;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FinalValue;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UInputModifier_GetVisualizationColor_Statics::NewProp_SampleValue = { "SampleValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(InputModifier_eventGetVisualizationColor_Parms, SampleValue), Z_Construct_UScriptStruct_FInputActionValue, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UInputModifier_GetVisualizationColor_Statics::NewProp_FinalValue = { "FinalValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(InputModifier_eventGetVisualizationColor_Parms, FinalValue), Z_Construct_UScriptStruct_FInputActionValue, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UInputModifier_GetVisualizationColor_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(InputModifier_eventGetVisualizationColor_Parms, ReturnValue), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UInputModifier_GetVisualizationColor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UInputModifier_GetVisualizationColor_Statics::NewProp_SampleValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UInputModifier_GetVisualizationColor_Statics::NewProp_FinalValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UInputModifier_GetVisualizationColor_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UInputModifier_GetVisualizationColor_Statics::Function_MetaDataParams[] = {
		{ "Category", "Modifier" },
		{ "Comment", "/**\n\x09 * Helper to allow debug visualization of the modifier.\n\x09 * @param SampleValue - The base input action value pre-modification (ranging -1 -> 1 across all applicable axes).\n\x09 * @param FinalValue - The post-modification input action value for the provided SampleValue.\n\x09 */" },
		{ "ModuleRelativePath", "Public/InputModifiers.h" },
		{ "ToolTip", "Helper to allow debug visualization of the modifier.\n@param SampleValue - The base input action value pre-modification (ranging -1 -> 1 across all applicable axes).\n@param FinalValue - The post-modification input action value for the provided SampleValue." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UInputModifier_GetVisualizationColor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UInputModifier, nullptr, "GetVisualizationColor", nullptr, nullptr, sizeof(InputModifier_eventGetVisualizationColor_Parms), Z_Construct_UFunction_UInputModifier_GetVisualizationColor_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UInputModifier_GetVisualizationColor_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x48820C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UInputModifier_GetVisualizationColor_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UInputModifier_GetVisualizationColor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UInputModifier_GetVisualizationColor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UInputModifier_GetVisualizationColor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UInputModifier_ModifyRaw_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlayerInput_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PlayerInput;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CurrentValue;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DeltaTime;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UInputModifier_ModifyRaw_Statics::NewProp_PlayerInput_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UInputModifier_ModifyRaw_Statics::NewProp_PlayerInput = { "PlayerInput", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(InputModifier_eventModifyRaw_Parms, PlayerInput), Z_Construct_UClass_UEnhancedPlayerInput_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UInputModifier_ModifyRaw_Statics::NewProp_PlayerInput_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UInputModifier_ModifyRaw_Statics::NewProp_PlayerInput_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UInputModifier_ModifyRaw_Statics::NewProp_CurrentValue = { "CurrentValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(InputModifier_eventModifyRaw_Parms, CurrentValue), Z_Construct_UScriptStruct_FInputActionValue, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UInputModifier_ModifyRaw_Statics::NewProp_DeltaTime = { "DeltaTime", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(InputModifier_eventModifyRaw_Parms, DeltaTime), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UInputModifier_ModifyRaw_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(InputModifier_eventModifyRaw_Parms, ReturnValue), Z_Construct_UScriptStruct_FInputActionValue, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UInputModifier_ModifyRaw_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UInputModifier_ModifyRaw_Statics::NewProp_PlayerInput,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UInputModifier_ModifyRaw_Statics::NewProp_CurrentValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UInputModifier_ModifyRaw_Statics::NewProp_DeltaTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UInputModifier_ModifyRaw_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UInputModifier_ModifyRaw_Statics::Function_MetaDataParams[] = {
		{ "Category", "Modifier" },
		{ "Comment", "/**\n\x09 * ModifyRaw\n\x09 * Will be called by each modifier in the modifier chain\n\x09 * @param CurrentValue - The modified value returned by the previous modifier in the chain, or the base raw value if this is the first modifier in the chain.\n\x09 */" },
		{ "ModuleRelativePath", "Public/InputModifiers.h" },
		{ "ToolTip", "ModifyRaw\nWill be called by each modifier in the modifier chain\n@param CurrentValue - The modified value returned by the previous modifier in the chain, or the base raw value if this is the first modifier in the chain." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UInputModifier_ModifyRaw_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UInputModifier, nullptr, "ModifyRaw", nullptr, nullptr, sizeof(InputModifier_eventModifyRaw_Parms), Z_Construct_UFunction_UInputModifier_ModifyRaw_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UInputModifier_ModifyRaw_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x5C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UInputModifier_ModifyRaw_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UInputModifier_ModifyRaw_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UInputModifier_ModifyRaw()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UInputModifier_ModifyRaw_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UInputModifier_NoRegister()
	{
		return UInputModifier::StaticClass();
	}
	struct Z_Construct_UClass_UInputModifier_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UInputModifier_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_EnhancedInput,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UInputModifier_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UInputModifier_GetExecutionPhase, "GetExecutionPhase" }, // 228565295
		{ &Z_Construct_UFunction_UInputModifier_GetVisualizationColor, "GetVisualizationColor" }, // 3981297746
		{ &Z_Construct_UFunction_UInputModifier_ModifyRaw, "ModifyRaw" }, // 363797178
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputModifier_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\nBase class for building modifiers.\n*/" },
		{ "IncludePath", "InputModifiers.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/InputModifiers.h" },
		{ "ToolTip", "Base class for building modifiers." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UInputModifier_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UInputModifier>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UInputModifier_Statics::ClassParams = {
		&UInputModifier::StaticClass,
		"Input",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x401030A3u,
		METADATA_PARAMS(Z_Construct_UClass_UInputModifier_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UInputModifier_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UInputModifier()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UInputModifier_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UInputModifier, 4043932309);
	template<> ENHANCEDINPUT_API UClass* StaticClass<UInputModifier>()
	{
		return UInputModifier::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UInputModifier(Z_Construct_UClass_UInputModifier, &UInputModifier::StaticClass, TEXT("/Script/EnhancedInput"), TEXT("UInputModifier"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UInputModifier);
	void UInputModifierDeadZone::StaticRegisterNativesUInputModifierDeadZone()
	{
	}
	UClass* Z_Construct_UClass_UInputModifierDeadZone_NoRegister()
	{
		return UInputModifierDeadZone::StaticClass();
	}
	struct Z_Construct_UClass_UInputModifierDeadZone_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LowerThreshold_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_LowerThreshold;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UpperThreshold_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_UpperThreshold;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Type_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Type_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Type;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UInputModifierDeadZone_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInputModifier,
		(UObject* (*)())Z_Construct_UPackage__Script_EnhancedInput,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputModifierDeadZone_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Dead Zone\n    *  Input values within the range LowerThreshold -> UpperThreshold will be remapped from 0 -> 1.\n\x09*  Values outside this range will be clamped.\n\x09*/" },
		{ "DisplayName", "Dead Zone" },
		{ "IncludePath", "InputModifiers.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Public/InputModifiers.h" },
		{ "ToolTip", "Dead Zone\nInput values within the range LowerThreshold -> UpperThreshold will be remapped from 0 -> 1.\nValues outside this range will be clamped." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputModifierDeadZone_Statics::NewProp_LowerThreshold_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "// Threshold below which input is ignored\n" },
		{ "ModuleRelativePath", "Public/InputModifiers.h" },
		{ "ToolTip", "Threshold below which input is ignored" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UInputModifierDeadZone_Statics::NewProp_LowerThreshold = { "LowerThreshold", nullptr, (EPropertyFlags)0x0010000000004805, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UInputModifierDeadZone, LowerThreshold), METADATA_PARAMS(Z_Construct_UClass_UInputModifierDeadZone_Statics::NewProp_LowerThreshold_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UInputModifierDeadZone_Statics::NewProp_LowerThreshold_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputModifierDeadZone_Statics::NewProp_UpperThreshold_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "// Threshold above which input is clamped to 1\n" },
		{ "ModuleRelativePath", "Public/InputModifiers.h" },
		{ "ToolTip", "Threshold above which input is clamped to 1" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UInputModifierDeadZone_Statics::NewProp_UpperThreshold = { "UpperThreshold", nullptr, (EPropertyFlags)0x0010000000004805, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UInputModifierDeadZone, UpperThreshold), METADATA_PARAMS(Z_Construct_UClass_UInputModifierDeadZone_Statics::NewProp_UpperThreshold_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UInputModifierDeadZone_Statics::NewProp_UpperThreshold_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UInputModifierDeadZone_Statics::NewProp_Type_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputModifierDeadZone_Statics::NewProp_Type_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/InputModifiers.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UInputModifierDeadZone_Statics::NewProp_Type = { "Type", nullptr, (EPropertyFlags)0x0010000000004805, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UInputModifierDeadZone, Type), Z_Construct_UEnum_EnhancedInput_EDeadZoneType, METADATA_PARAMS(Z_Construct_UClass_UInputModifierDeadZone_Statics::NewProp_Type_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UInputModifierDeadZone_Statics::NewProp_Type_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UInputModifierDeadZone_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputModifierDeadZone_Statics::NewProp_LowerThreshold,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputModifierDeadZone_Statics::NewProp_UpperThreshold,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputModifierDeadZone_Statics::NewProp_Type_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputModifierDeadZone_Statics::NewProp_Type,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UInputModifierDeadZone_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UInputModifierDeadZone>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UInputModifierDeadZone_Statics::ClassParams = {
		&UInputModifierDeadZone::StaticClass,
		"Input",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UInputModifierDeadZone_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UInputModifierDeadZone_Statics::PropPointers),
		0,
		0x400830A6u,
		METADATA_PARAMS(Z_Construct_UClass_UInputModifierDeadZone_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UInputModifierDeadZone_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UInputModifierDeadZone()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UInputModifierDeadZone_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UInputModifierDeadZone, 3188813296);
	template<> ENHANCEDINPUT_API UClass* StaticClass<UInputModifierDeadZone>()
	{
		return UInputModifierDeadZone::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UInputModifierDeadZone(Z_Construct_UClass_UInputModifierDeadZone, &UInputModifierDeadZone::StaticClass, TEXT("/Script/EnhancedInput"), TEXT("UInputModifierDeadZone"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UInputModifierDeadZone);
	void UInputModifierScalar::StaticRegisterNativesUInputModifierScalar()
	{
	}
	UClass* Z_Construct_UClass_UInputModifierScalar_NoRegister()
	{
		return UInputModifierScalar::StaticClass();
	}
	struct Z_Construct_UClass_UInputModifierScalar_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Scalar_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Scalar;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UInputModifierScalar_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInputModifier,
		(UObject* (*)())Z_Construct_UPackage__Script_EnhancedInput,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputModifierScalar_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Scalar\n\x09*  Scales input by a set factor per axis\n\x09*/" },
		{ "DisplayName", "Scalar" },
		{ "IncludePath", "InputModifiers.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Public/InputModifiers.h" },
		{ "ToolTip", "Scalar\nScales input by a set factor per axis" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputModifierScalar_Statics::NewProp_Scalar_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "// TODO: Detail customization to only show modifiable axes for the relevant binding? This thing has no idea what it's bound to...\n" },
		{ "ModuleRelativePath", "Public/InputModifiers.h" },
		{ "ToolTip", "TODO: Detail customization to only show modifiable axes for the relevant binding? This thing has no idea what it's bound to..." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UInputModifierScalar_Statics::NewProp_Scalar = { "Scalar", nullptr, (EPropertyFlags)0x0010000000000805, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UInputModifierScalar, Scalar), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UInputModifierScalar_Statics::NewProp_Scalar_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UInputModifierScalar_Statics::NewProp_Scalar_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UInputModifierScalar_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputModifierScalar_Statics::NewProp_Scalar,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UInputModifierScalar_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UInputModifierScalar>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UInputModifierScalar_Statics::ClassParams = {
		&UInputModifierScalar::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UInputModifierScalar_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UInputModifierScalar_Statics::PropPointers),
		0,
		0x400830A2u,
		METADATA_PARAMS(Z_Construct_UClass_UInputModifierScalar_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UInputModifierScalar_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UInputModifierScalar()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UInputModifierScalar_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UInputModifierScalar, 2628704263);
	template<> ENHANCEDINPUT_API UClass* StaticClass<UInputModifierScalar>()
	{
		return UInputModifierScalar::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UInputModifierScalar(Z_Construct_UClass_UInputModifierScalar, &UInputModifierScalar::StaticClass, TEXT("/Script/EnhancedInput"), TEXT("UInputModifierScalar"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UInputModifierScalar);
	void UInputModifierNegate::StaticRegisterNativesUInputModifierNegate()
	{
	}
	UClass* Z_Construct_UClass_UInputModifierNegate_NoRegister()
	{
		return UInputModifierNegate::StaticClass();
	}
	struct Z_Construct_UClass_UInputModifierNegate_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bX_MetaData[];
#endif
		static void NewProp_bX_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bX;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bY_MetaData[];
#endif
		static void NewProp_bY_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bY;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bZ_MetaData[];
#endif
		static void NewProp_bZ_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bZ;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UInputModifierNegate_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInputModifier,
		(UObject* (*)())Z_Construct_UPackage__Script_EnhancedInput,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputModifierNegate_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Negate\n\x09*  Inverts input per axis\n\x09*/" },
		{ "DisplayName", "Negate" },
		{ "IncludePath", "InputModifiers.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Public/InputModifiers.h" },
		{ "ToolTip", "Negate\nInverts input per axis" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputModifierNegate_Statics::NewProp_bX_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/InputModifiers.h" },
	};
#endif
	void Z_Construct_UClass_UInputModifierNegate_Statics::NewProp_bX_SetBit(void* Obj)
	{
		((UInputModifierNegate*)Obj)->bX = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UInputModifierNegate_Statics::NewProp_bX = { "bX", nullptr, (EPropertyFlags)0x0010000000000805, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UInputModifierNegate), &Z_Construct_UClass_UInputModifierNegate_Statics::NewProp_bX_SetBit, METADATA_PARAMS(Z_Construct_UClass_UInputModifierNegate_Statics::NewProp_bX_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UInputModifierNegate_Statics::NewProp_bX_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputModifierNegate_Statics::NewProp_bY_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/InputModifiers.h" },
	};
#endif
	void Z_Construct_UClass_UInputModifierNegate_Statics::NewProp_bY_SetBit(void* Obj)
	{
		((UInputModifierNegate*)Obj)->bY = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UInputModifierNegate_Statics::NewProp_bY = { "bY", nullptr, (EPropertyFlags)0x0010000000000805, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UInputModifierNegate), &Z_Construct_UClass_UInputModifierNegate_Statics::NewProp_bY_SetBit, METADATA_PARAMS(Z_Construct_UClass_UInputModifierNegate_Statics::NewProp_bY_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UInputModifierNegate_Statics::NewProp_bY_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputModifierNegate_Statics::NewProp_bZ_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/InputModifiers.h" },
	};
#endif
	void Z_Construct_UClass_UInputModifierNegate_Statics::NewProp_bZ_SetBit(void* Obj)
	{
		((UInputModifierNegate*)Obj)->bZ = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UInputModifierNegate_Statics::NewProp_bZ = { "bZ", nullptr, (EPropertyFlags)0x0010000000000805, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UInputModifierNegate), &Z_Construct_UClass_UInputModifierNegate_Statics::NewProp_bZ_SetBit, METADATA_PARAMS(Z_Construct_UClass_UInputModifierNegate_Statics::NewProp_bZ_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UInputModifierNegate_Statics::NewProp_bZ_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UInputModifierNegate_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputModifierNegate_Statics::NewProp_bX,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputModifierNegate_Statics::NewProp_bY,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputModifierNegate_Statics::NewProp_bZ,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UInputModifierNegate_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UInputModifierNegate>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UInputModifierNegate_Statics::ClassParams = {
		&UInputModifierNegate::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UInputModifierNegate_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UInputModifierNegate_Statics::PropPointers),
		0,
		0x400830A2u,
		METADATA_PARAMS(Z_Construct_UClass_UInputModifierNegate_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UInputModifierNegate_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UInputModifierNegate()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UInputModifierNegate_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UInputModifierNegate, 1739725431);
	template<> ENHANCEDINPUT_API UClass* StaticClass<UInputModifierNegate>()
	{
		return UInputModifierNegate::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UInputModifierNegate(Z_Construct_UClass_UInputModifierNegate, &UInputModifierNegate::StaticClass, TEXT("/Script/EnhancedInput"), TEXT("UInputModifierNegate"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UInputModifierNegate);
	void UInputModifierSmooth::StaticRegisterNativesUInputModifierSmooth()
	{
	}
	UClass* Z_Construct_UClass_UInputModifierSmooth_NoRegister()
	{
		return UInputModifierSmooth::StaticClass();
	}
	struct Z_Construct_UClass_UInputModifierSmooth_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UInputModifierSmooth_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInputModifier,
		(UObject* (*)())Z_Construct_UPackage__Script_EnhancedInput,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputModifierSmooth_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Smooth\n\x09*  Smooth inputs out over multiple frames\n\x09*/" },
		{ "DisplayName", "Smooth" },
		{ "IncludePath", "InputModifiers.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Public/InputModifiers.h" },
		{ "ToolTip", "Smooth\nSmooth inputs out over multiple frames" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UInputModifierSmooth_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UInputModifierSmooth>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UInputModifierSmooth_Statics::ClassParams = {
		&UInputModifierSmooth::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x400830A2u,
		METADATA_PARAMS(Z_Construct_UClass_UInputModifierSmooth_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UInputModifierSmooth_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UInputModifierSmooth()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UInputModifierSmooth_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UInputModifierSmooth, 1310266948);
	template<> ENHANCEDINPUT_API UClass* StaticClass<UInputModifierSmooth>()
	{
		return UInputModifierSmooth::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UInputModifierSmooth(Z_Construct_UClass_UInputModifierSmooth, &UInputModifierSmooth::StaticClass, TEXT("/Script/EnhancedInput"), TEXT("UInputModifierSmooth"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UInputModifierSmooth);
	void UInputModifierResponseCurveExponential::StaticRegisterNativesUInputModifierResponseCurveExponential()
	{
	}
	UClass* Z_Construct_UClass_UInputModifierResponseCurveExponential_NoRegister()
	{
		return UInputModifierResponseCurveExponential::StaticClass();
	}
	struct Z_Construct_UClass_UInputModifierResponseCurveExponential_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurveExponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CurveExponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UInputModifierResponseCurveExponential_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInputModifier,
		(UObject* (*)())Z_Construct_UPackage__Script_EnhancedInput,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputModifierResponseCurveExponential_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Response Curve Exponential\n\x09*  Apply a simple exponential response curve to input values, per axis\n\x09*/" },
		{ "DisplayName", "Response Curve - Exponential" },
		{ "IncludePath", "InputModifiers.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Public/InputModifiers.h" },
		{ "ToolTip", "Response Curve Exponential\nApply a simple exponential response curve to input values, per axis" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputModifierResponseCurveExponential_Statics::NewProp_CurveExponent_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/InputModifiers.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UInputModifierResponseCurveExponential_Statics::NewProp_CurveExponent = { "CurveExponent", nullptr, (EPropertyFlags)0x0010000000004805, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UInputModifierResponseCurveExponential, CurveExponent), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UInputModifierResponseCurveExponential_Statics::NewProp_CurveExponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UInputModifierResponseCurveExponential_Statics::NewProp_CurveExponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UInputModifierResponseCurveExponential_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputModifierResponseCurveExponential_Statics::NewProp_CurveExponent,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UInputModifierResponseCurveExponential_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UInputModifierResponseCurveExponential>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UInputModifierResponseCurveExponential_Statics::ClassParams = {
		&UInputModifierResponseCurveExponential::StaticClass,
		"Input",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UInputModifierResponseCurveExponential_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UInputModifierResponseCurveExponential_Statics::PropPointers),
		0,
		0x400830A6u,
		METADATA_PARAMS(Z_Construct_UClass_UInputModifierResponseCurveExponential_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UInputModifierResponseCurveExponential_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UInputModifierResponseCurveExponential()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UInputModifierResponseCurveExponential_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UInputModifierResponseCurveExponential, 2524023175);
	template<> ENHANCEDINPUT_API UClass* StaticClass<UInputModifierResponseCurveExponential>()
	{
		return UInputModifierResponseCurveExponential::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UInputModifierResponseCurveExponential(Z_Construct_UClass_UInputModifierResponseCurveExponential, &UInputModifierResponseCurveExponential::StaticClass, TEXT("/Script/EnhancedInput"), TEXT("UInputModifierResponseCurveExponential"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UInputModifierResponseCurveExponential);
	void UInputModifierResponseCurveUser::StaticRegisterNativesUInputModifierResponseCurveUser()
	{
	}
	UClass* Z_Construct_UClass_UInputModifierResponseCurveUser_NoRegister()
	{
		return UInputModifierResponseCurveUser::StaticClass();
	}
	struct Z_Construct_UClass_UInputModifierResponseCurveUser_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ResponseX_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ResponseX;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ResponseY_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ResponseY;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ResponseZ_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ResponseZ;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UInputModifierResponseCurveUser_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInputModifier,
		(UObject* (*)())Z_Construct_UPackage__Script_EnhancedInput,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputModifierResponseCurveUser_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Response Curve User Defined\n\x09*  Apply a custom response curve to input values, per axis\n\x09*/" },
		{ "DisplayName", "Response Curve - User Defined" },
		{ "IncludePath", "InputModifiers.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Public/InputModifiers.h" },
		{ "ToolTip", "Response Curve User Defined\nApply a custom response curve to input values, per axis" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputModifierResponseCurveUser_Statics::NewProp_ResponseX_MetaData[] = {
		{ "Category", "Settings" },
		{ "DisplayThumbnail", "false" },
		{ "ModuleRelativePath", "Public/InputModifiers.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UInputModifierResponseCurveUser_Statics::NewProp_ResponseX = { "ResponseX", nullptr, (EPropertyFlags)0x0010000000000805, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UInputModifierResponseCurveUser, ResponseX), Z_Construct_UClass_UCurveFloat_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UInputModifierResponseCurveUser_Statics::NewProp_ResponseX_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UInputModifierResponseCurveUser_Statics::NewProp_ResponseX_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputModifierResponseCurveUser_Statics::NewProp_ResponseY_MetaData[] = {
		{ "Category", "Settings" },
		{ "DisplayThumbnail", "false" },
		{ "ModuleRelativePath", "Public/InputModifiers.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UInputModifierResponseCurveUser_Statics::NewProp_ResponseY = { "ResponseY", nullptr, (EPropertyFlags)0x0010000000000805, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UInputModifierResponseCurveUser, ResponseY), Z_Construct_UClass_UCurveFloat_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UInputModifierResponseCurveUser_Statics::NewProp_ResponseY_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UInputModifierResponseCurveUser_Statics::NewProp_ResponseY_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputModifierResponseCurveUser_Statics::NewProp_ResponseZ_MetaData[] = {
		{ "Category", "Settings" },
		{ "DisplayThumbnail", "false" },
		{ "ModuleRelativePath", "Public/InputModifiers.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UInputModifierResponseCurveUser_Statics::NewProp_ResponseZ = { "ResponseZ", nullptr, (EPropertyFlags)0x0010000000000805, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UInputModifierResponseCurveUser, ResponseZ), Z_Construct_UClass_UCurveFloat_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UInputModifierResponseCurveUser_Statics::NewProp_ResponseZ_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UInputModifierResponseCurveUser_Statics::NewProp_ResponseZ_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UInputModifierResponseCurveUser_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputModifierResponseCurveUser_Statics::NewProp_ResponseX,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputModifierResponseCurveUser_Statics::NewProp_ResponseY,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputModifierResponseCurveUser_Statics::NewProp_ResponseZ,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UInputModifierResponseCurveUser_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UInputModifierResponseCurveUser>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UInputModifierResponseCurveUser_Statics::ClassParams = {
		&UInputModifierResponseCurveUser::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UInputModifierResponseCurveUser_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UInputModifierResponseCurveUser_Statics::PropPointers),
		0,
		0x400830A2u,
		METADATA_PARAMS(Z_Construct_UClass_UInputModifierResponseCurveUser_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UInputModifierResponseCurveUser_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UInputModifierResponseCurveUser()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UInputModifierResponseCurveUser_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UInputModifierResponseCurveUser, 3402857205);
	template<> ENHANCEDINPUT_API UClass* StaticClass<UInputModifierResponseCurveUser>()
	{
		return UInputModifierResponseCurveUser::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UInputModifierResponseCurveUser(Z_Construct_UClass_UInputModifierResponseCurveUser, &UInputModifierResponseCurveUser::StaticClass, TEXT("/Script/EnhancedInput"), TEXT("UInputModifierResponseCurveUser"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UInputModifierResponseCurveUser);
	void UInputModifierFOVScaling::StaticRegisterNativesUInputModifierFOVScaling()
	{
	}
	UClass* Z_Construct_UClass_UInputModifierFOVScaling_NoRegister()
	{
		return UInputModifierFOVScaling::StaticClass();
	}
	struct Z_Construct_UClass_UInputModifierFOVScaling_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FOVScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FOVScale;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_FOVScalingType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FOVScalingType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_FOVScalingType;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UInputModifierFOVScaling_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInputModifier,
		(UObject* (*)())Z_Construct_UPackage__Script_EnhancedInput,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputModifierFOVScaling_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** FOV Scaling\n\x09* Apply FOV dependent scaling to input values, per axis\n\x09*/" },
		{ "DisplayName", "FOV Scaling" },
		{ "IncludePath", "InputModifiers.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Public/InputModifiers.h" },
		{ "ToolTip", "FOV Scaling\nApply FOV dependent scaling to input values, per axis" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputModifierFOVScaling_Statics::NewProp_FOVScale_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "// Extra scalar applied on top of basic FOV scaling.\n" },
		{ "ModuleRelativePath", "Public/InputModifiers.h" },
		{ "ToolTip", "Extra scalar applied on top of basic FOV scaling." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UInputModifierFOVScaling_Statics::NewProp_FOVScale = { "FOVScale", nullptr, (EPropertyFlags)0x0010000000004805, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UInputModifierFOVScaling, FOVScale), METADATA_PARAMS(Z_Construct_UClass_UInputModifierFOVScaling_Statics::NewProp_FOVScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UInputModifierFOVScaling_Statics::NewProp_FOVScale_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UInputModifierFOVScaling_Statics::NewProp_FOVScalingType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputModifierFOVScaling_Statics::NewProp_FOVScalingType_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/InputModifiers.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UInputModifierFOVScaling_Statics::NewProp_FOVScalingType = { "FOVScalingType", nullptr, (EPropertyFlags)0x0010000000004805, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UInputModifierFOVScaling, FOVScalingType), Z_Construct_UEnum_EnhancedInput_EFOVScalingType, METADATA_PARAMS(Z_Construct_UClass_UInputModifierFOVScaling_Statics::NewProp_FOVScalingType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UInputModifierFOVScaling_Statics::NewProp_FOVScalingType_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UInputModifierFOVScaling_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputModifierFOVScaling_Statics::NewProp_FOVScale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputModifierFOVScaling_Statics::NewProp_FOVScalingType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputModifierFOVScaling_Statics::NewProp_FOVScalingType,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UInputModifierFOVScaling_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UInputModifierFOVScaling>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UInputModifierFOVScaling_Statics::ClassParams = {
		&UInputModifierFOVScaling::StaticClass,
		"Input",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UInputModifierFOVScaling_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UInputModifierFOVScaling_Statics::PropPointers),
		0,
		0x400830A6u,
		METADATA_PARAMS(Z_Construct_UClass_UInputModifierFOVScaling_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UInputModifierFOVScaling_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UInputModifierFOVScaling()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UInputModifierFOVScaling_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UInputModifierFOVScaling, 734491563);
	template<> ENHANCEDINPUT_API UClass* StaticClass<UInputModifierFOVScaling>()
	{
		return UInputModifierFOVScaling::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UInputModifierFOVScaling(Z_Construct_UClass_UInputModifierFOVScaling, &UInputModifierFOVScaling::StaticClass, TEXT("/Script/EnhancedInput"), TEXT("UInputModifierFOVScaling"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UInputModifierFOVScaling);
	void UInputModifierToWorldSpace::StaticRegisterNativesUInputModifierToWorldSpace()
	{
	}
	UClass* Z_Construct_UClass_UInputModifierToWorldSpace_NoRegister()
	{
		return UInputModifierToWorldSpace::StaticClass();
	}
	struct Z_Construct_UClass_UInputModifierToWorldSpace_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UInputModifierToWorldSpace_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInputModifier,
		(UObject* (*)())Z_Construct_UPackage__Script_EnhancedInput,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputModifierToWorldSpace_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Input space to World space conversion\n\x09* Auto-converts axes within the Input Action Value into world space\x09""allowing the result to be directly plugged into functions that take world space values.\n\x09* E.g. For a 2D input axis up/down is mapped to world X (forward), whilst axis left/right is mapped to world Y (right).\n\x09*/" },
		{ "DisplayName", "To World Space" },
		{ "IncludePath", "InputModifiers.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Public/InputModifiers.h" },
		{ "ToolTip", "Input space to World space conversion\nAuto-converts axes within the Input Action Value into world space     allowing the result to be directly plugged into functions that take world space values.\nE.g. For a 2D input axis up/down is mapped to world X (forward), whilst axis left/right is mapped to world Y (right)." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UInputModifierToWorldSpace_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UInputModifierToWorldSpace>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UInputModifierToWorldSpace_Statics::ClassParams = {
		&UInputModifierToWorldSpace::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x400830A2u,
		METADATA_PARAMS(Z_Construct_UClass_UInputModifierToWorldSpace_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UInputModifierToWorldSpace_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UInputModifierToWorldSpace()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UInputModifierToWorldSpace_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UInputModifierToWorldSpace, 4273622909);
	template<> ENHANCEDINPUT_API UClass* StaticClass<UInputModifierToWorldSpace>()
	{
		return UInputModifierToWorldSpace::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UInputModifierToWorldSpace(Z_Construct_UClass_UInputModifierToWorldSpace, &UInputModifierToWorldSpace::StaticClass, TEXT("/Script/EnhancedInput"), TEXT("UInputModifierToWorldSpace"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UInputModifierToWorldSpace);
	void UInputModifierSwizzleAxis::StaticRegisterNativesUInputModifierSwizzleAxis()
	{
	}
	UClass* Z_Construct_UClass_UInputModifierSwizzleAxis_NoRegister()
	{
		return UInputModifierSwizzleAxis::StaticClass();
	}
	struct Z_Construct_UClass_UInputModifierSwizzleAxis_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Order_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Order_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Order;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UInputModifierSwizzleAxis_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInputModifier,
		(UObject* (*)())Z_Construct_UPackage__Script_EnhancedInput,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputModifierSwizzleAxis_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Swizzle axis components of an input value.\n\x09* Useful to map a 1D input onto the Y axis of a 2D action.\n\x09*/" },
		{ "DisplayName", "Swizzle Input Axis Values" },
		{ "IncludePath", "InputModifiers.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Public/InputModifiers.h" },
		{ "ToolTip", "Swizzle axis components of an input value.\nUseful to map a 1D input onto the Y axis of a 2D action." },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UInputModifierSwizzleAxis_Statics::NewProp_Order_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputModifierSwizzleAxis_Statics::NewProp_Order_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/InputModifiers.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UInputModifierSwizzleAxis_Statics::NewProp_Order = { "Order", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UInputModifierSwizzleAxis, Order), Z_Construct_UEnum_EnhancedInput_EInputAxisSwizzle, METADATA_PARAMS(Z_Construct_UClass_UInputModifierSwizzleAxis_Statics::NewProp_Order_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UInputModifierSwizzleAxis_Statics::NewProp_Order_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UInputModifierSwizzleAxis_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputModifierSwizzleAxis_Statics::NewProp_Order_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputModifierSwizzleAxis_Statics::NewProp_Order,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UInputModifierSwizzleAxis_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UInputModifierSwizzleAxis>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UInputModifierSwizzleAxis_Statics::ClassParams = {
		&UInputModifierSwizzleAxis::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UInputModifierSwizzleAxis_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UInputModifierSwizzleAxis_Statics::PropPointers),
		0,
		0x400830A2u,
		METADATA_PARAMS(Z_Construct_UClass_UInputModifierSwizzleAxis_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UInputModifierSwizzleAxis_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UInputModifierSwizzleAxis()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UInputModifierSwizzleAxis_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UInputModifierSwizzleAxis, 732230105);
	template<> ENHANCEDINPUT_API UClass* StaticClass<UInputModifierSwizzleAxis>()
	{
		return UInputModifierSwizzleAxis::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UInputModifierSwizzleAxis(Z_Construct_UClass_UInputModifierSwizzleAxis, &UInputModifierSwizzleAxis::StaticClass, TEXT("/Script/EnhancedInput"), TEXT("UInputModifierSwizzleAxis"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UInputModifierSwizzleAxis);
	void UInputModifierCollection::StaticRegisterNativesUInputModifierCollection()
	{
	}
	UClass* Z_Construct_UClass_UInputModifierCollection_NoRegister()
	{
		return UInputModifierCollection::StaticClass();
	}
	struct Z_Construct_UClass_UInputModifierCollection_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Modifiers_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Modifiers_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Modifiers;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPermitValueTypeModification_MetaData[];
#endif
		static void NewProp_bPermitValueTypeModification_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPermitValueTypeModification;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UInputModifierCollection_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInputModifier,
		(UObject* (*)())Z_Construct_UPackage__Script_EnhancedInput,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputModifierCollection_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Modifier collection\n\x09* A user definable group of modifiers that can be easily applied to multiple actions or mappings to save duplication work.\n\x09*/" },
		{ "DisplayName", "Modifier Collection" },
		{ "IncludePath", "InputModifiers.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Public/InputModifiers.h" },
		{ "ToolTip", "Modifier collection\nA user definable group of modifiers that can be easily applied to multiple actions or mappings to save duplication work." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UInputModifierCollection_Statics::NewProp_Modifiers_Inner = { "Modifiers", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UInputModifier_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputModifierCollection_Statics::NewProp_Modifiers_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/InputModifiers.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UInputModifierCollection_Statics::NewProp_Modifiers = { "Modifiers", nullptr, (EPropertyFlags)0x0010000000000805, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UInputModifierCollection, Modifiers), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UInputModifierCollection_Statics::NewProp_Modifiers_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UInputModifierCollection_Statics::NewProp_Modifiers_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInputModifierCollection_Statics::NewProp_bPermitValueTypeModification_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "/** If set each modifier will not have the modified value corrected to the base type before execution.\n\x09*\x09""After all modifiers are run the resulting value will be converted back to the action's value type as with any other modifier.\n\x09*\x09This allows for complex sets of conditional modifiers that can alter their behavior based on their predecessors value type.\n\x09*\x09Note that this is an advanced feature and may cause issues if used with the basic modifier implementations.\n\x09**/" },
		{ "ModuleRelativePath", "Public/InputModifiers.h" },
		{ "ToolTip", "If set each modifier will not have the modified value corrected to the base type before execution.\n     After all modifiers are run the resulting value will be converted back to the action's value type as with any other modifier.\n     This allows for complex sets of conditional modifiers that can alter their behavior based on their predecessors value type.\n     Note that this is an advanced feature and may cause issues if used with the basic modifier implementations." },
	};
#endif
	void Z_Construct_UClass_UInputModifierCollection_Statics::NewProp_bPermitValueTypeModification_SetBit(void* Obj)
	{
		((UInputModifierCollection*)Obj)->bPermitValueTypeModification = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UInputModifierCollection_Statics::NewProp_bPermitValueTypeModification = { "bPermitValueTypeModification", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UInputModifierCollection), &Z_Construct_UClass_UInputModifierCollection_Statics::NewProp_bPermitValueTypeModification_SetBit, METADATA_PARAMS(Z_Construct_UClass_UInputModifierCollection_Statics::NewProp_bPermitValueTypeModification_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UInputModifierCollection_Statics::NewProp_bPermitValueTypeModification_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UInputModifierCollection_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputModifierCollection_Statics::NewProp_Modifiers_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputModifierCollection_Statics::NewProp_Modifiers,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UInputModifierCollection_Statics::NewProp_bPermitValueTypeModification,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UInputModifierCollection_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UInputModifierCollection>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UInputModifierCollection_Statics::ClassParams = {
		&UInputModifierCollection::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UInputModifierCollection_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UInputModifierCollection_Statics::PropPointers),
		0,
		0x400830A2u,
		METADATA_PARAMS(Z_Construct_UClass_UInputModifierCollection_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UInputModifierCollection_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UInputModifierCollection()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UInputModifierCollection_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UInputModifierCollection, 2323944977);
	template<> ENHANCEDINPUT_API UClass* StaticClass<UInputModifierCollection>()
	{
		return UInputModifierCollection::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UInputModifierCollection(Z_Construct_UClass_UInputModifierCollection, &UInputModifierCollection::StaticClass, TEXT("/Script/EnhancedInput"), TEXT("UInputModifierCollection"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UInputModifierCollection);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
