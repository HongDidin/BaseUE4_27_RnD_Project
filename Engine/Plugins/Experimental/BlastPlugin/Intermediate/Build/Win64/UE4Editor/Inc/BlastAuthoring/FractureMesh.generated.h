// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef BLASTAUTHORING_FractureMesh_generated_h
#error "FractureMesh.generated.h already included, missing '#pragma once' in FractureMesh.h"
#endif
#define BLASTAUTHORING_FractureMesh_generated_h

#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_FractureMesh_h_45_SPARSE_DATA
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_FractureMesh_h_45_RPC_WRAPPERS
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_FractureMesh_h_45_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_FractureMesh_h_45_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFractureMesh(); \
	friend struct Z_Construct_UClass_UFractureMesh_Statics; \
public: \
	DECLARE_CLASS(UFractureMesh, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/BlastAuthoring"), NO_API) \
	DECLARE_SERIALIZER(UFractureMesh)


#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_FractureMesh_h_45_INCLASS \
private: \
	static void StaticRegisterNativesUFractureMesh(); \
	friend struct Z_Construct_UClass_UFractureMesh_Statics; \
public: \
	DECLARE_CLASS(UFractureMesh, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/BlastAuthoring"), NO_API) \
	DECLARE_SERIALIZER(UFractureMesh)


#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_FractureMesh_h_45_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFractureMesh(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureMesh) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureMesh); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureMesh); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureMesh(UFractureMesh&&); \
	NO_API UFractureMesh(const UFractureMesh&); \
public:


#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_FractureMesh_h_45_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFractureMesh(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureMesh(UFractureMesh&&); \
	NO_API UFractureMesh(const UFractureMesh&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureMesh); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureMesh); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureMesh)


#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_FractureMesh_h_45_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_FractureMesh_h_42_PROLOG
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_FractureMesh_h_45_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_FractureMesh_h_45_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_FractureMesh_h_45_SPARSE_DATA \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_FractureMesh_h_45_RPC_WRAPPERS \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_FractureMesh_h_45_INCLASS \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_FractureMesh_h_45_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_FractureMesh_h_45_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_FractureMesh_h_45_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_FractureMesh_h_45_SPARSE_DATA \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_FractureMesh_h_45_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_FractureMesh_h_45_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_FractureMesh_h_45_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> BLASTAUTHORING_API UClass* StaticClass<class UFractureMesh>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_FractureMesh_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
