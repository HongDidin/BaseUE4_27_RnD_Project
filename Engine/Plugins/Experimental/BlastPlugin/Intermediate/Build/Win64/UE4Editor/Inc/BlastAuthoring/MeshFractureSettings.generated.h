// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef BLASTAUTHORING_MeshFractureSettings_generated_h
#error "MeshFractureSettings.generated.h already included, missing '#pragma once' in MeshFractureSettings.h"
#endif
#define BLASTAUTHORING_MeshFractureSettings_generated_h

#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_141_SPARSE_DATA
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_141_RPC_WRAPPERS
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_141_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_141_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonFractureSettings(); \
	friend struct Z_Construct_UClass_UCommonFractureSettings_Statics; \
public: \
	DECLARE_CLASS(UCommonFractureSettings, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/BlastAuthoring"), NO_API) \
	DECLARE_SERIALIZER(UCommonFractureSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_141_INCLASS \
private: \
	static void StaticRegisterNativesUCommonFractureSettings(); \
	friend struct Z_Construct_UClass_UCommonFractureSettings_Statics; \
public: \
	DECLARE_CLASS(UCommonFractureSettings, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/BlastAuthoring"), NO_API) \
	DECLARE_SERIALIZER(UCommonFractureSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_141_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonFractureSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonFractureSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonFractureSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonFractureSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonFractureSettings(UCommonFractureSettings&&); \
	NO_API UCommonFractureSettings(const UCommonFractureSettings&); \
public:


#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_141_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonFractureSettings(UCommonFractureSettings&&); \
	NO_API UCommonFractureSettings(const UCommonFractureSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonFractureSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonFractureSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UCommonFractureSettings)


#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_141_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_137_PROLOG
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_141_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_141_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_141_SPARSE_DATA \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_141_RPC_WRAPPERS \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_141_INCLASS \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_141_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_141_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_141_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_141_SPARSE_DATA \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_141_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_141_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_141_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> BLASTAUTHORING_API UClass* StaticClass<class UCommonFractureSettings>();

#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_212_SPARSE_DATA
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_212_RPC_WRAPPERS
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_212_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_212_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUUniformFractureSettings(); \
	friend struct Z_Construct_UClass_UUniformFractureSettings_Statics; \
public: \
	DECLARE_CLASS(UUniformFractureSettings, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/BlastAuthoring"), NO_API) \
	DECLARE_SERIALIZER(UUniformFractureSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_212_INCLASS \
private: \
	static void StaticRegisterNativesUUniformFractureSettings(); \
	friend struct Z_Construct_UClass_UUniformFractureSettings_Statics; \
public: \
	DECLARE_CLASS(UUniformFractureSettings, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/BlastAuthoring"), NO_API) \
	DECLARE_SERIALIZER(UUniformFractureSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_212_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUniformFractureSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUniformFractureSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUniformFractureSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUniformFractureSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUniformFractureSettings(UUniformFractureSettings&&); \
	NO_API UUniformFractureSettings(const UUniformFractureSettings&); \
public:


#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_212_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUniformFractureSettings(UUniformFractureSettings&&); \
	NO_API UUniformFractureSettings(const UUniformFractureSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUniformFractureSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUniformFractureSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UUniformFractureSettings)


#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_212_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_208_PROLOG
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_212_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_212_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_212_SPARSE_DATA \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_212_RPC_WRAPPERS \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_212_INCLASS \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_212_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_212_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_212_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_212_SPARSE_DATA \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_212_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_212_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_212_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> BLASTAUTHORING_API UClass* StaticClass<class UUniformFractureSettings>();

#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_229_SPARSE_DATA
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_229_RPC_WRAPPERS
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_229_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_229_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUClusterFractureSettings(); \
	friend struct Z_Construct_UClass_UClusterFractureSettings_Statics; \
public: \
	DECLARE_CLASS(UClusterFractureSettings, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/BlastAuthoring"), NO_API) \
	DECLARE_SERIALIZER(UClusterFractureSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_229_INCLASS \
private: \
	static void StaticRegisterNativesUClusterFractureSettings(); \
	friend struct Z_Construct_UClass_UClusterFractureSettings_Statics; \
public: \
	DECLARE_CLASS(UClusterFractureSettings, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/BlastAuthoring"), NO_API) \
	DECLARE_SERIALIZER(UClusterFractureSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_229_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UClusterFractureSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UClusterFractureSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UClusterFractureSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UClusterFractureSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UClusterFractureSettings(UClusterFractureSettings&&); \
	NO_API UClusterFractureSettings(const UClusterFractureSettings&); \
public:


#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_229_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UClusterFractureSettings(UClusterFractureSettings&&); \
	NO_API UClusterFractureSettings(const UClusterFractureSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UClusterFractureSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UClusterFractureSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UClusterFractureSettings)


#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_229_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_225_PROLOG
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_229_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_229_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_229_SPARSE_DATA \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_229_RPC_WRAPPERS \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_229_INCLASS \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_229_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_229_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_229_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_229_SPARSE_DATA \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_229_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_229_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_229_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> BLASTAUTHORING_API UClass* StaticClass<class UClusterFractureSettings>();

#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_265_SPARSE_DATA
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_265_RPC_WRAPPERS
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_265_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_265_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURadialFractureSettings(); \
	friend struct Z_Construct_UClass_URadialFractureSettings_Statics; \
public: \
	DECLARE_CLASS(URadialFractureSettings, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/BlastAuthoring"), NO_API) \
	DECLARE_SERIALIZER(URadialFractureSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_265_INCLASS \
private: \
	static void StaticRegisterNativesURadialFractureSettings(); \
	friend struct Z_Construct_UClass_URadialFractureSettings_Statics; \
public: \
	DECLARE_CLASS(URadialFractureSettings, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/BlastAuthoring"), NO_API) \
	DECLARE_SERIALIZER(URadialFractureSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_265_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URadialFractureSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URadialFractureSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URadialFractureSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URadialFractureSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URadialFractureSettings(URadialFractureSettings&&); \
	NO_API URadialFractureSettings(const URadialFractureSettings&); \
public:


#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_265_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URadialFractureSettings(URadialFractureSettings&&); \
	NO_API URadialFractureSettings(const URadialFractureSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URadialFractureSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URadialFractureSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(URadialFractureSettings)


#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_265_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_261_PROLOG
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_265_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_265_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_265_SPARSE_DATA \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_265_RPC_WRAPPERS \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_265_INCLASS \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_265_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_265_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_265_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_265_SPARSE_DATA \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_265_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_265_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_265_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> BLASTAUTHORING_API UClass* StaticClass<class URadialFractureSettings>();

#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_304_SPARSE_DATA
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_304_RPC_WRAPPERS
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_304_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_304_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSlicingFractureSettings(); \
	friend struct Z_Construct_UClass_USlicingFractureSettings_Statics; \
public: \
	DECLARE_CLASS(USlicingFractureSettings, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/BlastAuthoring"), NO_API) \
	DECLARE_SERIALIZER(USlicingFractureSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_304_INCLASS \
private: \
	static void StaticRegisterNativesUSlicingFractureSettings(); \
	friend struct Z_Construct_UClass_USlicingFractureSettings_Statics; \
public: \
	DECLARE_CLASS(USlicingFractureSettings, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/BlastAuthoring"), NO_API) \
	DECLARE_SERIALIZER(USlicingFractureSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_304_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USlicingFractureSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USlicingFractureSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USlicingFractureSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USlicingFractureSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USlicingFractureSettings(USlicingFractureSettings&&); \
	NO_API USlicingFractureSettings(const USlicingFractureSettings&); \
public:


#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_304_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USlicingFractureSettings(USlicingFractureSettings&&); \
	NO_API USlicingFractureSettings(const USlicingFractureSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USlicingFractureSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USlicingFractureSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(USlicingFractureSettings)


#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_304_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_300_PROLOG
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_304_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_304_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_304_SPARSE_DATA \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_304_RPC_WRAPPERS \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_304_INCLASS \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_304_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_304_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_304_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_304_SPARSE_DATA \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_304_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_304_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_304_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> BLASTAUTHORING_API UClass* StaticClass<class USlicingFractureSettings>();

#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_358_SPARSE_DATA
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_358_RPC_WRAPPERS
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_358_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_358_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPlaneCutFractureSettings(); \
	friend struct Z_Construct_UClass_UPlaneCutFractureSettings_Statics; \
public: \
	DECLARE_CLASS(UPlaneCutFractureSettings, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/BlastAuthoring"), NO_API) \
	DECLARE_SERIALIZER(UPlaneCutFractureSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_358_INCLASS \
private: \
	static void StaticRegisterNativesUPlaneCutFractureSettings(); \
	friend struct Z_Construct_UClass_UPlaneCutFractureSettings_Statics; \
public: \
	DECLARE_CLASS(UPlaneCutFractureSettings, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/BlastAuthoring"), NO_API) \
	DECLARE_SERIALIZER(UPlaneCutFractureSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_358_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPlaneCutFractureSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPlaneCutFractureSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPlaneCutFractureSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPlaneCutFractureSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPlaneCutFractureSettings(UPlaneCutFractureSettings&&); \
	NO_API UPlaneCutFractureSettings(const UPlaneCutFractureSettings&); \
public:


#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_358_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPlaneCutFractureSettings(UPlaneCutFractureSettings&&); \
	NO_API UPlaneCutFractureSettings(const UPlaneCutFractureSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPlaneCutFractureSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPlaneCutFractureSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UPlaneCutFractureSettings)


#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_358_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_354_PROLOG
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_358_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_358_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_358_SPARSE_DATA \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_358_RPC_WRAPPERS \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_358_INCLASS \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_358_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_358_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_358_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_358_SPARSE_DATA \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_358_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_358_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_358_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> BLASTAUTHORING_API UClass* StaticClass<class UPlaneCutFractureSettings>();

#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_391_SPARSE_DATA
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_391_RPC_WRAPPERS
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_391_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_391_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCutoutFractureSettings(); \
	friend struct Z_Construct_UClass_UCutoutFractureSettings_Statics; \
public: \
	DECLARE_CLASS(UCutoutFractureSettings, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/BlastAuthoring"), NO_API) \
	DECLARE_SERIALIZER(UCutoutFractureSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_391_INCLASS \
private: \
	static void StaticRegisterNativesUCutoutFractureSettings(); \
	friend struct Z_Construct_UClass_UCutoutFractureSettings_Statics; \
public: \
	DECLARE_CLASS(UCutoutFractureSettings, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/BlastAuthoring"), NO_API) \
	DECLARE_SERIALIZER(UCutoutFractureSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_391_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCutoutFractureSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCutoutFractureSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCutoutFractureSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCutoutFractureSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCutoutFractureSettings(UCutoutFractureSettings&&); \
	NO_API UCutoutFractureSettings(const UCutoutFractureSettings&); \
public:


#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_391_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCutoutFractureSettings(UCutoutFractureSettings&&); \
	NO_API UCutoutFractureSettings(const UCutoutFractureSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCutoutFractureSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCutoutFractureSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UCutoutFractureSettings)


#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_391_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_387_PROLOG
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_391_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_391_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_391_SPARSE_DATA \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_391_RPC_WRAPPERS \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_391_INCLASS \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_391_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_391_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_391_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_391_SPARSE_DATA \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_391_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_391_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_391_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> BLASTAUTHORING_API UClass* StaticClass<class UCutoutFractureSettings>();

#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_442_SPARSE_DATA
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_442_RPC_WRAPPERS
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_442_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_442_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUBrickFractureSettings(); \
	friend struct Z_Construct_UClass_UBrickFractureSettings_Statics; \
public: \
	DECLARE_CLASS(UBrickFractureSettings, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/BlastAuthoring"), NO_API) \
	DECLARE_SERIALIZER(UBrickFractureSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_442_INCLASS \
private: \
	static void StaticRegisterNativesUBrickFractureSettings(); \
	friend struct Z_Construct_UClass_UBrickFractureSettings_Statics; \
public: \
	DECLARE_CLASS(UBrickFractureSettings, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/BlastAuthoring"), NO_API) \
	DECLARE_SERIALIZER(UBrickFractureSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_442_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBrickFractureSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBrickFractureSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBrickFractureSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBrickFractureSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBrickFractureSettings(UBrickFractureSettings&&); \
	NO_API UBrickFractureSettings(const UBrickFractureSettings&); \
public:


#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_442_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBrickFractureSettings(UBrickFractureSettings&&); \
	NO_API UBrickFractureSettings(const UBrickFractureSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBrickFractureSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBrickFractureSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UBrickFractureSettings)


#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_442_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_438_PROLOG
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_442_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_442_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_442_SPARSE_DATA \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_442_RPC_WRAPPERS \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_442_INCLASS \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_442_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_442_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_442_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_442_SPARSE_DATA \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_442_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_442_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_442_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> BLASTAUTHORING_API UClass* StaticClass<class UBrickFractureSettings>();

#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_485_SPARSE_DATA
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_485_RPC_WRAPPERS
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_485_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_485_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshFractureSettings(); \
	friend struct Z_Construct_UClass_UMeshFractureSettings_Statics; \
public: \
	DECLARE_CLASS(UMeshFractureSettings, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/BlastAuthoring"), NO_API) \
	DECLARE_SERIALIZER(UMeshFractureSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_485_INCLASS \
private: \
	static void StaticRegisterNativesUMeshFractureSettings(); \
	friend struct Z_Construct_UClass_UMeshFractureSettings_Statics; \
public: \
	DECLARE_CLASS(UMeshFractureSettings, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/BlastAuthoring"), NO_API) \
	DECLARE_SERIALIZER(UMeshFractureSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_485_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshFractureSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshFractureSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshFractureSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshFractureSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshFractureSettings(UMeshFractureSettings&&); \
	NO_API UMeshFractureSettings(const UMeshFractureSettings&); \
public:


#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_485_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshFractureSettings(UMeshFractureSettings&&); \
	NO_API UMeshFractureSettings(const UMeshFractureSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshFractureSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshFractureSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMeshFractureSettings)


#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_485_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_481_PROLOG
#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_485_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_485_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_485_SPARSE_DATA \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_485_RPC_WRAPPERS \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_485_INCLASS \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_485_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_485_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_485_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_485_SPARSE_DATA \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_485_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_485_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h_485_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> BLASTAUTHORING_API UClass* StaticClass<class UMeshFractureSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_BlastPlugin_Source_BlastAuthoring_Public_MeshFractureSettings_h


#define FOREACH_ENUM_EVIEWRESETTYPE(op) \
	op(EViewResetType::RESET_ALL) \
	op(EViewResetType::RESET_TRANSFORMS) 

enum class EViewResetType : uint8;
template<> BLASTAUTHORING_API UEnum* StaticEnum<EViewResetType>();

#define FOREACH_ENUM_EFRACTURESELECTIONMODE(op) \
	op(EFractureSelectionMode::ChunkSelect) \
	op(EFractureSelectionMode::ClusterSelect) \
	op(EFractureSelectionMode::LevelSelect) 

enum class EFractureSelectionMode : uint8;
template<> BLASTAUTHORING_API UEnum* StaticEnum<EFractureSelectionMode>();

#define FOREACH_ENUM_EFRACTURECOLORIZEMODE(op) \
	op(EFractureColorizeMode::ColorOff) \
	op(EFractureColorizeMode::ColorRandom) \
	op(EFractureColorizeMode::ColorLevels) 

enum class EFractureColorizeMode : uint8;
template<> BLASTAUTHORING_API UEnum* StaticEnum<EFractureColorizeMode>();

#define FOREACH_ENUM_EEXPLODEDVIEWMODE(op) \
	op(EExplodedViewMode::SplitLevels) \
	op(EExplodedViewMode::Linear) 

enum class EExplodedViewMode : uint8;
template<> BLASTAUTHORING_API UEnum* StaticEnum<EExplodedViewMode>();

#define FOREACH_ENUM_EMESHFRACTURELEVEL(op) \
	op(EMeshFractureLevel::AllLevels) \
	op(EMeshFractureLevel::Level0) \
	op(EMeshFractureLevel::Level1) \
	op(EMeshFractureLevel::Level2) \
	op(EMeshFractureLevel::Level3) \
	op(EMeshFractureLevel::Level4) \
	op(EMeshFractureLevel::Level5) \
	op(EMeshFractureLevel::Level6) 

enum class EMeshFractureLevel : uint8;
template<> BLASTAUTHORING_API UEnum* StaticEnum<EMeshFractureLevel>();

#define FOREACH_ENUM_EMESHFRACTUREBRICKPROJECTION(op) \
	op(EMeshFractureBrickProjection::X) \
	op(EMeshFractureBrickProjection::Y) \
	op(EMeshFractureBrickProjection::Z) 

enum class EMeshFractureBrickProjection : uint8;
template<> BLASTAUTHORING_API UEnum* StaticEnum<EMeshFractureBrickProjection>();

#define FOREACH_ENUM_EMESHAUTOCLUSTERMODE(op) \
	op(EMeshAutoClusterMode::BoundingBox) \
	op(EMeshAutoClusterMode::Proximity) \
	op(EMeshAutoClusterMode::Distance) 

enum class EMeshAutoClusterMode : uint8;
template<> BLASTAUTHORING_API UEnum* StaticEnum<EMeshAutoClusterMode>();

#define FOREACH_ENUM_EMESHFRACTUREMODE(op) \
	op(EMeshFractureMode::Uniform) \
	op(EMeshFractureMode::Clustered) \
	op(EMeshFractureMode::Radial) \
	op(EMeshFractureMode::Slicing) \
	op(EMeshFractureMode::PlaneCut) \
	op(EMeshFractureMode::Cutout) \
	op(EMeshFractureMode::Brick) 

enum class EMeshFractureMode : uint8;
template<> BLASTAUTHORING_API UEnum* StaticEnum<EMeshFractureMode>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
