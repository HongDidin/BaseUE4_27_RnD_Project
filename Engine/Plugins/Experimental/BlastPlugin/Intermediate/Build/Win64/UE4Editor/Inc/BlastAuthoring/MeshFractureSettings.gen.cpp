// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "BlastAuthoring/Public/MeshFractureSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMeshFractureSettings() {}
// Cross Module References
	BLASTAUTHORING_API UEnum* Z_Construct_UEnum_BlastAuthoring_EViewResetType();
	UPackage* Z_Construct_UPackage__Script_BlastAuthoring();
	BLASTAUTHORING_API UEnum* Z_Construct_UEnum_BlastAuthoring_EFractureSelectionMode();
	BLASTAUTHORING_API UEnum* Z_Construct_UEnum_BlastAuthoring_EFractureColorizeMode();
	BLASTAUTHORING_API UEnum* Z_Construct_UEnum_BlastAuthoring_EExplodedViewMode();
	BLASTAUTHORING_API UEnum* Z_Construct_UEnum_BlastAuthoring_EMeshFractureLevel();
	BLASTAUTHORING_API UEnum* Z_Construct_UEnum_BlastAuthoring_EMeshFractureBrickProjection();
	BLASTAUTHORING_API UEnum* Z_Construct_UEnum_BlastAuthoring_EMeshAutoClusterMode();
	BLASTAUTHORING_API UEnum* Z_Construct_UEnum_BlastAuthoring_EMeshFractureMode();
	BLASTAUTHORING_API UClass* Z_Construct_UClass_UCommonFractureSettings_NoRegister();
	BLASTAUTHORING_API UClass* Z_Construct_UClass_UCommonFractureSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	BLASTAUTHORING_API UClass* Z_Construct_UClass_UUniformFractureSettings_NoRegister();
	BLASTAUTHORING_API UClass* Z_Construct_UClass_UUniformFractureSettings();
	BLASTAUTHORING_API UClass* Z_Construct_UClass_UClusterFractureSettings_NoRegister();
	BLASTAUTHORING_API UClass* Z_Construct_UClass_UClusterFractureSettings();
	BLASTAUTHORING_API UClass* Z_Construct_UClass_URadialFractureSettings_NoRegister();
	BLASTAUTHORING_API UClass* Z_Construct_UClass_URadialFractureSettings();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	BLASTAUTHORING_API UClass* Z_Construct_UClass_USlicingFractureSettings_NoRegister();
	BLASTAUTHORING_API UClass* Z_Construct_UClass_USlicingFractureSettings();
	BLASTAUTHORING_API UClass* Z_Construct_UClass_UPlaneCutFractureSettings_NoRegister();
	BLASTAUTHORING_API UClass* Z_Construct_UClass_UPlaneCutFractureSettings();
	BLASTAUTHORING_API UClass* Z_Construct_UClass_UCutoutFractureSettings_NoRegister();
	BLASTAUTHORING_API UClass* Z_Construct_UClass_UCutoutFractureSettings();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	ENGINE_API UClass* Z_Construct_UClass_UTexture2D_NoRegister();
	BLASTAUTHORING_API UClass* Z_Construct_UClass_UBrickFractureSettings_NoRegister();
	BLASTAUTHORING_API UClass* Z_Construct_UClass_UBrickFractureSettings();
	BLASTAUTHORING_API UClass* Z_Construct_UClass_UMeshFractureSettings_NoRegister();
	BLASTAUTHORING_API UClass* Z_Construct_UClass_UMeshFractureSettings();
// End Cross Module References
	static UEnum* EViewResetType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_BlastAuthoring_EViewResetType, Z_Construct_UPackage__Script_BlastAuthoring(), TEXT("EViewResetType"));
		}
		return Singleton;
	}
	template<> BLASTAUTHORING_API UEnum* StaticEnum<EViewResetType>()
	{
		return EViewResetType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EViewResetType(EViewResetType_StaticEnum, TEXT("/Script/BlastAuthoring"), TEXT("EViewResetType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_BlastAuthoring_EViewResetType_Hash() { return 2782917916U; }
	UEnum* Z_Construct_UEnum_BlastAuthoring_EViewResetType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_BlastAuthoring();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EViewResetType"), 0, Get_Z_Construct_UEnum_BlastAuthoring_EViewResetType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EViewResetType::RESET_ALL", (int64)EViewResetType::RESET_ALL },
				{ "EViewResetType::RESET_TRANSFORMS", (int64)EViewResetType::RESET_TRANSFORMS },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
				{ "RESET_ALL.Name", "EViewResetType::RESET_ALL" },
				{ "RESET_TRANSFORMS.Name", "EViewResetType::RESET_TRANSFORMS" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_BlastAuthoring,
				nullptr,
				"EViewResetType",
				"EViewResetType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EFractureSelectionMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_BlastAuthoring_EFractureSelectionMode, Z_Construct_UPackage__Script_BlastAuthoring(), TEXT("EFractureSelectionMode"));
		}
		return Singleton;
	}
	template<> BLASTAUTHORING_API UEnum* StaticEnum<EFractureSelectionMode>()
	{
		return EFractureSelectionMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EFractureSelectionMode(EFractureSelectionMode_StaticEnum, TEXT("/Script/BlastAuthoring"), TEXT("EFractureSelectionMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_BlastAuthoring_EFractureSelectionMode_Hash() { return 72204553U; }
	UEnum* Z_Construct_UEnum_BlastAuthoring_EFractureSelectionMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_BlastAuthoring();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EFractureSelectionMode"), 0, Get_Z_Construct_UEnum_BlastAuthoring_EFractureSelectionMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EFractureSelectionMode::ChunkSelect", (int64)EFractureSelectionMode::ChunkSelect },
				{ "EFractureSelectionMode::ClusterSelect", (int64)EFractureSelectionMode::ClusterSelect },
				{ "EFractureSelectionMode::LevelSelect", (int64)EFractureSelectionMode::LevelSelect },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ChunkSelect.DisplayName", "Chunk Select" },
				{ "ChunkSelect.Name", "EFractureSelectionMode::ChunkSelect" },
				{ "ClusterSelect.DisplayName", "Cluster Select" },
				{ "ClusterSelect.Name", "EFractureSelectionMode::ClusterSelect" },
				{ "Comment", "/** Selection Mode */" },
				{ "LevelSelect.DisplayName", "Level Select" },
				{ "LevelSelect.Name", "EFractureSelectionMode::LevelSelect" },
				{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
				{ "ToolTip", "Selection Mode" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_BlastAuthoring,
				nullptr,
				"EFractureSelectionMode",
				"EFractureSelectionMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EFractureColorizeMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_BlastAuthoring_EFractureColorizeMode, Z_Construct_UPackage__Script_BlastAuthoring(), TEXT("EFractureColorizeMode"));
		}
		return Singleton;
	}
	template<> BLASTAUTHORING_API UEnum* StaticEnum<EFractureColorizeMode>()
	{
		return EFractureColorizeMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EFractureColorizeMode(EFractureColorizeMode_StaticEnum, TEXT("/Script/BlastAuthoring"), TEXT("EFractureColorizeMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_BlastAuthoring_EFractureColorizeMode_Hash() { return 840361463U; }
	UEnum* Z_Construct_UEnum_BlastAuthoring_EFractureColorizeMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_BlastAuthoring();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EFractureColorizeMode"), 0, Get_Z_Construct_UEnum_BlastAuthoring_EFractureColorizeMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EFractureColorizeMode::ColorOff", (int64)EFractureColorizeMode::ColorOff },
				{ "EFractureColorizeMode::ColorRandom", (int64)EFractureColorizeMode::ColorRandom },
				{ "EFractureColorizeMode::ColorLevels", (int64)EFractureColorizeMode::ColorLevels },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ColorLevels.Comment", "/** Colored based on bone hierarchy level */" },
				{ "ColorLevels.DisplayName", "Color Levels" },
				{ "ColorLevels.Name", "EFractureColorizeMode::ColorLevels" },
				{ "ColorLevels.ToolTip", "Colored based on bone hierarchy level" },
				{ "ColorOff.Comment", "/** Fracture colorization turned off */" },
				{ "ColorOff.DisplayName", "Colors off" },
				{ "ColorOff.Name", "EFractureColorizeMode::ColorOff" },
				{ "ColorOff.ToolTip", "Fracture colorization turned off" },
				{ "ColorRandom.Comment", "/** Random colored fracture pieces */" },
				{ "ColorRandom.DisplayName", "Random Color" },
				{ "ColorRandom.Name", "EFractureColorizeMode::ColorRandom" },
				{ "ColorRandom.ToolTip", "Random colored fracture pieces" },
				{ "Comment", "/** Colorize View Mode */" },
				{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
				{ "ToolTip", "Colorize View Mode" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_BlastAuthoring,
				nullptr,
				"EFractureColorizeMode",
				"EFractureColorizeMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EExplodedViewMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_BlastAuthoring_EExplodedViewMode, Z_Construct_UPackage__Script_BlastAuthoring(), TEXT("EExplodedViewMode"));
		}
		return Singleton;
	}
	template<> BLASTAUTHORING_API UEnum* StaticEnum<EExplodedViewMode>()
	{
		return EExplodedViewMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EExplodedViewMode(EExplodedViewMode_StaticEnum, TEXT("/Script/BlastAuthoring"), TEXT("EExplodedViewMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_BlastAuthoring_EExplodedViewMode_Hash() { return 890202957U; }
	UEnum* Z_Construct_UEnum_BlastAuthoring_EExplodedViewMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_BlastAuthoring();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EExplodedViewMode"), 0, Get_Z_Construct_UEnum_BlastAuthoring_EExplodedViewMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EExplodedViewMode::SplitLevels", (int64)EExplodedViewMode::SplitLevels },
				{ "EExplodedViewMode::Linear", (int64)EExplodedViewMode::Linear },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** Exploded View Mode */" },
				{ "Linear.Comment", "/** All levels split at the same time linearly */" },
				{ "Linear.DisplayName", "Linear" },
				{ "Linear.Name", "EExplodedViewMode::Linear" },
				{ "Linear.ToolTip", "All levels split at the same time linearly" },
				{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
				{ "SplitLevels.Comment", "/** Levels split at different times */" },
				{ "SplitLevels.DisplayName", "As Levels" },
				{ "SplitLevels.Name", "EExplodedViewMode::SplitLevels" },
				{ "SplitLevels.ToolTip", "Levels split at different times" },
				{ "ToolTip", "Exploded View Mode" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_BlastAuthoring,
				nullptr,
				"EExplodedViewMode",
				"EExplodedViewMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EMeshFractureLevel_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_BlastAuthoring_EMeshFractureLevel, Z_Construct_UPackage__Script_BlastAuthoring(), TEXT("EMeshFractureLevel"));
		}
		return Singleton;
	}
	template<> BLASTAUTHORING_API UEnum* StaticEnum<EMeshFractureLevel>()
	{
		return EMeshFractureLevel_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMeshFractureLevel(EMeshFractureLevel_StaticEnum, TEXT("/Script/BlastAuthoring"), TEXT("EMeshFractureLevel"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_BlastAuthoring_EMeshFractureLevel_Hash() { return 1963229684U; }
	UEnum* Z_Construct_UEnum_BlastAuthoring_EMeshFractureLevel()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_BlastAuthoring();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMeshFractureLevel"), 0, Get_Z_Construct_UEnum_BlastAuthoring_EMeshFractureLevel_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMeshFractureLevel::AllLevels", (int64)EMeshFractureLevel::AllLevels },
				{ "EMeshFractureLevel::Level0", (int64)EMeshFractureLevel::Level0 },
				{ "EMeshFractureLevel::Level1", (int64)EMeshFractureLevel::Level1 },
				{ "EMeshFractureLevel::Level2", (int64)EMeshFractureLevel::Level2 },
				{ "EMeshFractureLevel::Level3", (int64)EMeshFractureLevel::Level3 },
				{ "EMeshFractureLevel::Level4", (int64)EMeshFractureLevel::Level4 },
				{ "EMeshFractureLevel::Level5", (int64)EMeshFractureLevel::Level5 },
				{ "EMeshFractureLevel::Level6", (int64)EMeshFractureLevel::Level6 },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "AllLevels.DisplayName", "All Levels" },
				{ "AllLevels.Name", "EMeshFractureLevel::AllLevels" },
				{ "Comment", "/** Mesh fracture levels - lazy way to get a drop down list from UI */" },
				{ "Level0.DisplayName", "Level0" },
				{ "Level0.Name", "EMeshFractureLevel::Level0" },
				{ "Level1.DisplayName", "Level1" },
				{ "Level1.Name", "EMeshFractureLevel::Level1" },
				{ "Level2.DisplayName", "Level2" },
				{ "Level2.Name", "EMeshFractureLevel::Level2" },
				{ "Level3.DisplayName", "Level3" },
				{ "Level3.Name", "EMeshFractureLevel::Level3" },
				{ "Level4.DisplayName", "Level4" },
				{ "Level4.Name", "EMeshFractureLevel::Level4" },
				{ "Level5.DisplayName", "Level5" },
				{ "Level5.Name", "EMeshFractureLevel::Level5" },
				{ "Level6.DisplayName", "Level6" },
				{ "Level6.Name", "EMeshFractureLevel::Level6" },
				{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
				{ "ToolTip", "Mesh fracture levels - lazy way to get a drop down list from UI" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_BlastAuthoring,
				nullptr,
				"EMeshFractureLevel",
				"EMeshFractureLevel",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EMeshFractureBrickProjection_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_BlastAuthoring_EMeshFractureBrickProjection, Z_Construct_UPackage__Script_BlastAuthoring(), TEXT("EMeshFractureBrickProjection"));
		}
		return Singleton;
	}
	template<> BLASTAUTHORING_API UEnum* StaticEnum<EMeshFractureBrickProjection>()
	{
		return EMeshFractureBrickProjection_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMeshFractureBrickProjection(EMeshFractureBrickProjection_StaticEnum, TEXT("/Script/BlastAuthoring"), TEXT("EMeshFractureBrickProjection"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_BlastAuthoring_EMeshFractureBrickProjection_Hash() { return 1354783873U; }
	UEnum* Z_Construct_UEnum_BlastAuthoring_EMeshFractureBrickProjection()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_BlastAuthoring();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMeshFractureBrickProjection"), 0, Get_Z_Construct_UEnum_BlastAuthoring_EMeshFractureBrickProjection_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMeshFractureBrickProjection::X", (int64)EMeshFractureBrickProjection::X },
				{ "EMeshFractureBrickProjection::Y", (int64)EMeshFractureBrickProjection::Y },
				{ "EMeshFractureBrickProjection::Z", (int64)EMeshFractureBrickProjection::Z },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** Brick Projection Directions*/" },
				{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
				{ "ToolTip", "Brick Projection Directions" },
				{ "X.Comment", "/** Standard Voronoi */" },
				{ "X.DisplayName", "X" },
				{ "X.Name", "EMeshFractureBrickProjection::X" },
				{ "X.ToolTip", "Standard Voronoi" },
				{ "Y.Comment", "/** Clustered Voronoi */" },
				{ "Y.DisplayName", "Y" },
				{ "Y.Name", "EMeshFractureBrickProjection::Y" },
				{ "Y.ToolTip", "Clustered Voronoi" },
				{ "Z.Comment", "/** Radial Voronoi */" },
				{ "Z.DisplayName", "Z" },
				{ "Z.Name", "EMeshFractureBrickProjection::Z" },
				{ "Z.ToolTip", "Radial Voronoi" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_BlastAuthoring,
				nullptr,
				"EMeshFractureBrickProjection",
				"EMeshFractureBrickProjection",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EMeshAutoClusterMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_BlastAuthoring_EMeshAutoClusterMode, Z_Construct_UPackage__Script_BlastAuthoring(), TEXT("EMeshAutoClusterMode"));
		}
		return Singleton;
	}
	template<> BLASTAUTHORING_API UEnum* StaticEnum<EMeshAutoClusterMode>()
	{
		return EMeshAutoClusterMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMeshAutoClusterMode(EMeshAutoClusterMode_StaticEnum, TEXT("/Script/BlastAuthoring"), TEXT("EMeshAutoClusterMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_BlastAuthoring_EMeshAutoClusterMode_Hash() { return 4262015168U; }
	UEnum* Z_Construct_UEnum_BlastAuthoring_EMeshAutoClusterMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_BlastAuthoring();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMeshAutoClusterMode"), 0, Get_Z_Construct_UEnum_BlastAuthoring_EMeshAutoClusterMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMeshAutoClusterMode::BoundingBox", (int64)EMeshAutoClusterMode::BoundingBox },
				{ "EMeshAutoClusterMode::Proximity", (int64)EMeshAutoClusterMode::Proximity },
				{ "EMeshAutoClusterMode::Distance", (int64)EMeshAutoClusterMode::Distance },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BoundingBox.Comment", "/** Overlapping bounding box*/" },
				{ "BoundingBox.DisplayName", "Bounding Box" },
				{ "BoundingBox.Name", "EMeshAutoClusterMode::BoundingBox" },
				{ "BoundingBox.ToolTip", "Overlapping bounding box" },
				{ "Comment", "/** Mesh fracture pattern modes */" },
				{ "Distance.Comment", "/** Distance */" },
				{ "Distance.DisplayName", "Distance" },
				{ "Distance.Name", "EMeshAutoClusterMode::Distance" },
				{ "Distance.ToolTip", "Distance" },
				{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
				{ "Proximity.Comment", "/** GC connectivity */" },
				{ "Proximity.DisplayName", "Proximity" },
				{ "Proximity.Name", "EMeshAutoClusterMode::Proximity" },
				{ "Proximity.ToolTip", "GC connectivity" },
				{ "ToolTip", "Mesh fracture pattern modes" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_BlastAuthoring,
				nullptr,
				"EMeshAutoClusterMode",
				"EMeshAutoClusterMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EMeshFractureMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_BlastAuthoring_EMeshFractureMode, Z_Construct_UPackage__Script_BlastAuthoring(), TEXT("EMeshFractureMode"));
		}
		return Singleton;
	}
	template<> BLASTAUTHORING_API UEnum* StaticEnum<EMeshFractureMode>()
	{
		return EMeshFractureMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMeshFractureMode(EMeshFractureMode_StaticEnum, TEXT("/Script/BlastAuthoring"), TEXT("EMeshFractureMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_BlastAuthoring_EMeshFractureMode_Hash() { return 4179940910U; }
	UEnum* Z_Construct_UEnum_BlastAuthoring_EMeshFractureMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_BlastAuthoring();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMeshFractureMode"), 0, Get_Z_Construct_UEnum_BlastAuthoring_EMeshFractureMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMeshFractureMode::Uniform", (int64)EMeshFractureMode::Uniform },
				{ "EMeshFractureMode::Clustered", (int64)EMeshFractureMode::Clustered },
				{ "EMeshFractureMode::Radial", (int64)EMeshFractureMode::Radial },
				{ "EMeshFractureMode::Slicing", (int64)EMeshFractureMode::Slicing },
				{ "EMeshFractureMode::PlaneCut", (int64)EMeshFractureMode::PlaneCut },
				{ "EMeshFractureMode::Cutout", (int64)EMeshFractureMode::Cutout },
				{ "EMeshFractureMode::Brick", (int64)EMeshFractureMode::Brick },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Brick.Comment", "/** Special case Brick Cutout Slicing algorithm - non-voronoi */" },
				{ "Brick.DisplayName", "Brick" },
				{ "Brick.Name", "EMeshFractureMode::Brick" },
				{ "Brick.ToolTip", "Special case Brick Cutout Slicing algorithm - non-voronoi" },
				{ "Clustered.Comment", "/** Clustered Voronoi */" },
				{ "Clustered.DisplayName", "Clustered Voronoi" },
				{ "Clustered.Name", "EMeshFractureMode::Clustered" },
				{ "Clustered.ToolTip", "Clustered Voronoi" },
				{ "Comment", "/** Mesh fracture pattern modes */" },
				{ "Cutout.Comment", "/** Bitmap Cutout Slicing algorithm - non-voronoi */" },
				{ "Cutout.DisplayName", "Bitmap Cutout" },
				{ "Cutout.Name", "EMeshFractureMode::Cutout" },
				{ "Cutout.ToolTip", "Bitmap Cutout Slicing algorithm - non-voronoi" },
				{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
				{ "PlaneCut.Comment", "/** Simple Plane Slice - non-voronoi */" },
				{ "PlaneCut.DisplayName", "Plane Cut" },
				{ "PlaneCut.Name", "EMeshFractureMode::PlaneCut" },
				{ "PlaneCut.ToolTip", "Simple Plane Slice - non-voronoi" },
				{ "Radial.Comment", "/** Radial Voronoi */" },
				{ "Radial.DisplayName", "Radial Voronoi" },
				{ "Radial.Name", "EMeshFractureMode::Radial" },
				{ "Radial.ToolTip", "Radial Voronoi" },
				{ "Slicing.Comment", "/** Slicing algorithm - non-voronoi */" },
				{ "Slicing.DisplayName", "Slicing" },
				{ "Slicing.Name", "EMeshFractureMode::Slicing" },
				{ "Slicing.ToolTip", "Slicing algorithm - non-voronoi" },
				{ "ToolTip", "Mesh fracture pattern modes" },
				{ "Uniform.Comment", "/** Standard Voronoi */" },
				{ "Uniform.DisplayName", "Uniform Voronoi" },
				{ "Uniform.Name", "EMeshFractureMode::Uniform" },
				{ "Uniform.ToolTip", "Standard Voronoi" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_BlastAuthoring,
				nullptr,
				"EMeshFractureMode",
				"EMeshFractureMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UCommonFractureSettings::StaticRegisterNativesUCommonFractureSettings()
	{
	}
	UClass* Z_Construct_UClass_UCommonFractureSettings_NoRegister()
	{
		return UCommonFractureSettings::StaticClass();
	}
	struct Z_Construct_UClass_UCommonFractureSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ViewMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ViewMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShowBoneColors_MetaData[];
#endif
		static void NewProp_ShowBoneColors_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ShowBoneColors;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeleteSourceMesh_MetaData[];
#endif
		static void NewProp_DeleteSourceMesh_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_DeleteSourceMesh;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_AutoClusterGroupMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AutoClusterGroupMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_AutoClusterGroupMode;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_FractureMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FractureMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_FractureMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RemoveIslands_MetaData[];
#endif
		static void NewProp_RemoveIslands_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_RemoveIslands;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RandomSeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_RandomSeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChanceToFracture_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ChanceToFracture;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bGroupFracture_MetaData[];
#endif
		static void NewProp_bGroupFracture_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bGroupFracture;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCancelOnBadGeo_MetaData[];
#endif
		static void NewProp_bCancelOnBadGeo_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCancelOnBadGeo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bThreadedFracture_MetaData[];
#endif
		static void NewProp_bThreadedFracture_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bThreadedFracture;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bHealHoles_MetaData[];
#endif
		static void NewProp_bHealHoles_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bHealHoles;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReferenceActor_MetaData[];
#endif
		static const UE4CodeGen_Private::FLazyObjectPropertyParams NewProp_ReferenceActor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonFractureSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_BlastAuthoring,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonFractureSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshFractureSettings.h" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_ViewMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_ViewMode_MetaData[] = {
		{ "Category", "CommonFracture" },
		{ "Comment", "/** In Editor Fracture Viewing mode */" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "In Editor Fracture Viewing mode" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_ViewMode = { "ViewMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonFractureSettings, ViewMode), Z_Construct_UEnum_BlastAuthoring_EMeshFractureLevel, METADATA_PARAMS(Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_ViewMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_ViewMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_ShowBoneColors_MetaData[] = {
		{ "Category", "CommonFracture" },
		{ "Comment", "/** Enable bone color mode */" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Enable bone color mode" },
	};
#endif
	void Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_ShowBoneColors_SetBit(void* Obj)
	{
		((UCommonFractureSettings*)Obj)->ShowBoneColors = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_ShowBoneColors = { "ShowBoneColors", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCommonFractureSettings), &Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_ShowBoneColors_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_ShowBoneColors_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_ShowBoneColors_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_DeleteSourceMesh_MetaData[] = {
		{ "Category", "CommonFracture" },
		{ "Comment", "/** Delete Source mesh when fracturing & generating a Geometry Collection */" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Delete Source mesh when fracturing & generating a Geometry Collection" },
	};
#endif
	void Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_DeleteSourceMesh_SetBit(void* Obj)
	{
		((UCommonFractureSettings*)Obj)->DeleteSourceMesh = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_DeleteSourceMesh = { "DeleteSourceMesh", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCommonFractureSettings), &Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_DeleteSourceMesh_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_DeleteSourceMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_DeleteSourceMesh_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_AutoClusterGroupMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_AutoClusterGroupMode_MetaData[] = {
		{ "Category", "CommonFracture" },
		{ "Comment", "/** Group Detection Mode */" },
		{ "DisplayName", "Group Detection Mode" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Group Detection Mode" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_AutoClusterGroupMode = { "AutoClusterGroupMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonFractureSettings, AutoClusterGroupMode), Z_Construct_UEnum_BlastAuthoring_EMeshAutoClusterMode, METADATA_PARAMS(Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_AutoClusterGroupMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_AutoClusterGroupMode_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_FractureMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_FractureMode_MetaData[] = {
		{ "Category", "CommonFracture" },
		{ "Comment", "/** Fracture mode */" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Fracture mode" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_FractureMode = { "FractureMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonFractureSettings, FractureMode), Z_Construct_UEnum_BlastAuthoring_EMeshFractureMode, METADATA_PARAMS(Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_FractureMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_FractureMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_RemoveIslands_MetaData[] = {
		{ "Category", "CommonFracture" },
		{ "Comment", "/** Cleanup mesh option */" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Cleanup mesh option" },
	};
#endif
	void Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_RemoveIslands_SetBit(void* Obj)
	{
		((UCommonFractureSettings*)Obj)->RemoveIslands = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_RemoveIslands = { "RemoveIslands", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCommonFractureSettings), &Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_RemoveIslands_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_RemoveIslands_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_RemoveIslands_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_RandomSeed_MetaData[] = {
		{ "Category", "CommonFracture" },
		{ "ClampMax", "1000" },
		{ "ClampMin", "-1" },
		{ "Comment", "/** Random number generator seed for repeatability */" },
		{ "DisplayName", "Random Seed" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Random number generator seed for repeatability" },
		{ "UIMax", "1000" },
		{ "UIMin", "-1" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_RandomSeed = { "RandomSeed", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonFractureSettings, RandomSeed), METADATA_PARAMS(Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_RandomSeed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_RandomSeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_ChanceToFracture_MetaData[] = {
		{ "Category", "CommonFracture" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Chance to shatter each mesh.  Useful when shattering multiple selected meshes.  */" },
		{ "DisplayName", "Chance To Fracture Per Mesh" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Chance to shatter each mesh.  Useful when shattering multiple selected meshes." },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_ChanceToFracture = { "ChanceToFracture", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonFractureSettings, ChanceToFracture), METADATA_PARAMS(Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_ChanceToFracture_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_ChanceToFracture_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_bGroupFracture_MetaData[] = {
		{ "Category", "CommonFracture" },
		{ "Comment", "/** Generate a fracture pattern across all selected meshes.  */" },
		{ "DisplayName", "Group Fracture" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Generate a fracture pattern across all selected meshes." },
	};
#endif
	void Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_bGroupFracture_SetBit(void* Obj)
	{
		((UCommonFractureSettings*)Obj)->bGroupFracture = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_bGroupFracture = { "bGroupFracture", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCommonFractureSettings), &Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_bGroupFracture_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_bGroupFracture_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_bGroupFracture_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_bCancelOnBadGeo_MetaData[] = {
		{ "Category", "CommonFracture" },
		{ "Comment", "/** Reverts the fracture if a mesh is generated with <3 faces or verts  */" },
		{ "DisplayName", "Cancel On Bad Geo" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Reverts the fracture if a mesh is generated with <3 faces or verts" },
	};
#endif
	void Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_bCancelOnBadGeo_SetBit(void* Obj)
	{
		((UCommonFractureSettings*)Obj)->bCancelOnBadGeo = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_bCancelOnBadGeo = { "bCancelOnBadGeo", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCommonFractureSettings), &Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_bCancelOnBadGeo_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_bCancelOnBadGeo_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_bCancelOnBadGeo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_bThreadedFracture_MetaData[] = {
		{ "Category", "CommonFracture" },
		{ "Comment", "/** Launches a thread per selected object.  */" },
		{ "DisplayName", "Threaded Fracture (experimental)" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Launches a thread per selected object." },
	};
#endif
	void Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_bThreadedFracture_SetBit(void* Obj)
	{
		((UCommonFractureSettings*)Obj)->bThreadedFracture = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_bThreadedFracture = { "bThreadedFracture", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCommonFractureSettings), &Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_bThreadedFracture_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_bThreadedFracture_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_bThreadedFracture_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_bHealHoles_MetaData[] = {
		{ "Category", "CommonFracture" },
		{ "Comment", "/** Does hole detection and attempts to fill them in.  This is applied to both input and generated meshes  */" },
		{ "DisplayName", "Heal Holes (experimental)" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Does hole detection and attempts to fill them in.  This is applied to both input and generated meshes" },
	};
#endif
	void Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_bHealHoles_SetBit(void* Obj)
	{
		((UCommonFractureSettings*)Obj)->bHealHoles = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_bHealHoles = { "bHealHoles", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCommonFractureSettings), &Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_bHealHoles_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_bHealHoles_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_bHealHoles_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_ReferenceActor_MetaData[] = {
		{ "Category", "CommonFracture" },
		{ "Comment", "/** Actor to be used for voronoi bounds or plane cutting  */" },
		{ "DisplayName", "Reference Actor" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Actor to be used for voronoi bounds or plane cutting" },
	};
#endif
	const UE4CodeGen_Private::FLazyObjectPropertyParams Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_ReferenceActor = { "ReferenceActor", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::LazyObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonFractureSettings, ReferenceActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_ReferenceActor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_ReferenceActor_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCommonFractureSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_ViewMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_ViewMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_ShowBoneColors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_DeleteSourceMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_AutoClusterGroupMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_AutoClusterGroupMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_FractureMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_FractureMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_RemoveIslands,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_RandomSeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_ChanceToFracture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_bGroupFracture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_bCancelOnBadGeo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_bThreadedFracture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_bHealHoles,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonFractureSettings_Statics::NewProp_ReferenceActor,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonFractureSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonFractureSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCommonFractureSettings_Statics::ClassParams = {
		&UCommonFractureSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UCommonFractureSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UCommonFractureSettings_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonFractureSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonFractureSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonFractureSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCommonFractureSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCommonFractureSettings, 783425106);
	template<> BLASTAUTHORING_API UClass* StaticClass<UCommonFractureSettings>()
	{
		return UCommonFractureSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCommonFractureSettings(Z_Construct_UClass_UCommonFractureSettings, &UCommonFractureSettings::StaticClass, TEXT("/Script/BlastAuthoring"), TEXT("UCommonFractureSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonFractureSettings);
	void UUniformFractureSettings::StaticRegisterNativesUUniformFractureSettings()
	{
	}
	UClass* Z_Construct_UClass_UUniformFractureSettings_NoRegister()
	{
		return UUniformFractureSettings::StaticClass();
	}
	struct Z_Construct_UClass_UUniformFractureSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumberVoronoiSitesMin_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_NumberVoronoiSitesMin;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumberVoronoiSitesMax_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_NumberVoronoiSitesMax;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUniformFractureSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_BlastAuthoring,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUniformFractureSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshFractureSettings.h" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUniformFractureSettings_Statics::NewProp_NumberVoronoiSitesMin_MetaData[] = {
		{ "Category", "UniformVoronoi" },
		{ "ClampMin", "2" },
		{ "Comment", "/** Number of Voronoi sites - Uniform Voronoi Method */" },
		{ "DisplayName", "Min Voronoi Sites" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Number of Voronoi sites - Uniform Voronoi Method" },
		{ "UIMax", "5000" },
		{ "UIMin", "2" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UUniformFractureSettings_Statics::NewProp_NumberVoronoiSitesMin = { "NumberVoronoiSitesMin", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUniformFractureSettings, NumberVoronoiSitesMin), METADATA_PARAMS(Z_Construct_UClass_UUniformFractureSettings_Statics::NewProp_NumberVoronoiSitesMin_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUniformFractureSettings_Statics::NewProp_NumberVoronoiSitesMin_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUniformFractureSettings_Statics::NewProp_NumberVoronoiSitesMax_MetaData[] = {
		{ "Category", "UniformVoronoi" },
		{ "ClampMin", "2" },
		{ "DisplayName", "Max Voronoi Sites" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "UIMax", "5000" },
		{ "UIMin", "2" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UUniformFractureSettings_Statics::NewProp_NumberVoronoiSitesMax = { "NumberVoronoiSitesMax", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUniformFractureSettings, NumberVoronoiSitesMax), METADATA_PARAMS(Z_Construct_UClass_UUniformFractureSettings_Statics::NewProp_NumberVoronoiSitesMax_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUniformFractureSettings_Statics::NewProp_NumberVoronoiSitesMax_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UUniformFractureSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUniformFractureSettings_Statics::NewProp_NumberVoronoiSitesMin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUniformFractureSettings_Statics::NewProp_NumberVoronoiSitesMax,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUniformFractureSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUniformFractureSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUniformFractureSettings_Statics::ClassParams = {
		&UUniformFractureSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UUniformFractureSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UUniformFractureSettings_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UUniformFractureSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUniformFractureSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUniformFractureSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUniformFractureSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUniformFractureSettings, 156223671);
	template<> BLASTAUTHORING_API UClass* StaticClass<UUniformFractureSettings>()
	{
		return UUniformFractureSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUniformFractureSettings(Z_Construct_UClass_UUniformFractureSettings, &UUniformFractureSettings::StaticClass, TEXT("/Script/BlastAuthoring"), TEXT("UUniformFractureSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUniformFractureSettings);
	void UClusterFractureSettings::StaticRegisterNativesUClusterFractureSettings()
	{
	}
	UClass* Z_Construct_UClass_UClusterFractureSettings_NoRegister()
	{
		return UClusterFractureSettings::StaticClass();
	}
	struct Z_Construct_UClass_UClusterFractureSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumberClustersMin_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumberClustersMin;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumberClustersMax_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumberClustersMax;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SitesPerClusterMin_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SitesPerClusterMin;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SitesPerClusterMax_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SitesPerClusterMax;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClusterRadiusPercentageMin_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ClusterRadiusPercentageMin;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClusterRadiusPercentageMax_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ClusterRadiusPercentageMax;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClusterRadius_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ClusterRadius;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UClusterFractureSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_BlastAuthoring,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UClusterFractureSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshFractureSettings.h" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UClusterFractureSettings_Statics::NewProp_NumberClustersMin_MetaData[] = {
		{ "Category", "ClusteredVoronoi" },
		{ "ClampMax", "5000" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Number of Clusters - Clustered Voronoi Method */" },
		{ "DisplayName", "Minimum Cluster Sites" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Number of Clusters - Clustered Voronoi Method" },
		{ "UIMax", "5000" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UClusterFractureSettings_Statics::NewProp_NumberClustersMin = { "NumberClustersMin", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UClusterFractureSettings, NumberClustersMin), METADATA_PARAMS(Z_Construct_UClass_UClusterFractureSettings_Statics::NewProp_NumberClustersMin_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UClusterFractureSettings_Statics::NewProp_NumberClustersMin_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UClusterFractureSettings_Statics::NewProp_NumberClustersMax_MetaData[] = {
		{ "Category", "ClusteredVoronoi" },
		{ "ClampMax", "5000" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Number of Clusters - Clustered Voronoi Method */" },
		{ "DisplayName", "Maximum Cluster Sites" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Number of Clusters - Clustered Voronoi Method" },
		{ "UIMax", "5000" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UClusterFractureSettings_Statics::NewProp_NumberClustersMax = { "NumberClustersMax", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UClusterFractureSettings, NumberClustersMax), METADATA_PARAMS(Z_Construct_UClass_UClusterFractureSettings_Statics::NewProp_NumberClustersMax_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UClusterFractureSettings_Statics::NewProp_NumberClustersMax_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UClusterFractureSettings_Statics::NewProp_SitesPerClusterMin_MetaData[] = {
		{ "Category", "ClusteredVoronoi" },
		{ "ClampMax", "5000" },
		{ "ClampMin", "2" },
		{ "Comment", "/** Sites per of Clusters - Clustered Voronoi Method */" },
		{ "DisplayName", "Minimum Sites Per Cluster" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Sites per of Clusters - Clustered Voronoi Method" },
		{ "UIMax", "5000" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UClusterFractureSettings_Statics::NewProp_SitesPerClusterMin = { "SitesPerClusterMin", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UClusterFractureSettings, SitesPerClusterMin), METADATA_PARAMS(Z_Construct_UClass_UClusterFractureSettings_Statics::NewProp_SitesPerClusterMin_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UClusterFractureSettings_Statics::NewProp_SitesPerClusterMin_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UClusterFractureSettings_Statics::NewProp_SitesPerClusterMax_MetaData[] = {
		{ "Category", "ClusteredVoronoi" },
		{ "ClampMax", "5000" },
		{ "ClampMin", "2" },
		{ "DisplayName", "Maximum Sites Per Cluster" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "UIMax", "5000" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UClusterFractureSettings_Statics::NewProp_SitesPerClusterMax = { "SitesPerClusterMax", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UClusterFractureSettings, SitesPerClusterMax), METADATA_PARAMS(Z_Construct_UClass_UClusterFractureSettings_Statics::NewProp_SitesPerClusterMax_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UClusterFractureSettings_Statics::NewProp_SitesPerClusterMax_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UClusterFractureSettings_Statics::NewProp_ClusterRadiusPercentageMin_MetaData[] = {
		{ "Category", "ClusteredVoronoi" },
		{ "Comment", "/** Clusters Radius - Clustered Voronoi Method */" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Clusters Radius - Clustered Voronoi Method" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UClusterFractureSettings_Statics::NewProp_ClusterRadiusPercentageMin = { "ClusterRadiusPercentageMin", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UClusterFractureSettings, ClusterRadiusPercentageMin), METADATA_PARAMS(Z_Construct_UClass_UClusterFractureSettings_Statics::NewProp_ClusterRadiusPercentageMin_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UClusterFractureSettings_Statics::NewProp_ClusterRadiusPercentageMin_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UClusterFractureSettings_Statics::NewProp_ClusterRadiusPercentageMax_MetaData[] = {
		{ "Category", "ClusteredVoronoi" },
		{ "Comment", "/** Clusters Radius - Clustered Voronoi Method */" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Clusters Radius - Clustered Voronoi Method" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UClusterFractureSettings_Statics::NewProp_ClusterRadiusPercentageMax = { "ClusterRadiusPercentageMax", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UClusterFractureSettings, ClusterRadiusPercentageMax), METADATA_PARAMS(Z_Construct_UClass_UClusterFractureSettings_Statics::NewProp_ClusterRadiusPercentageMax_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UClusterFractureSettings_Statics::NewProp_ClusterRadiusPercentageMax_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UClusterFractureSettings_Statics::NewProp_ClusterRadius_MetaData[] = {
		{ "Category", "ClusteredVoronoi" },
		{ "Comment", "/** Clusters Radius - Clustered Voronoi Method */" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Clusters Radius - Clustered Voronoi Method" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UClusterFractureSettings_Statics::NewProp_ClusterRadius = { "ClusterRadius", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UClusterFractureSettings, ClusterRadius), METADATA_PARAMS(Z_Construct_UClass_UClusterFractureSettings_Statics::NewProp_ClusterRadius_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UClusterFractureSettings_Statics::NewProp_ClusterRadius_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UClusterFractureSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UClusterFractureSettings_Statics::NewProp_NumberClustersMin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UClusterFractureSettings_Statics::NewProp_NumberClustersMax,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UClusterFractureSettings_Statics::NewProp_SitesPerClusterMin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UClusterFractureSettings_Statics::NewProp_SitesPerClusterMax,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UClusterFractureSettings_Statics::NewProp_ClusterRadiusPercentageMin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UClusterFractureSettings_Statics::NewProp_ClusterRadiusPercentageMax,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UClusterFractureSettings_Statics::NewProp_ClusterRadius,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UClusterFractureSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UClusterFractureSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UClusterFractureSettings_Statics::ClassParams = {
		&UClusterFractureSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UClusterFractureSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UClusterFractureSettings_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UClusterFractureSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UClusterFractureSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UClusterFractureSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UClusterFractureSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UClusterFractureSettings, 3141790502);
	template<> BLASTAUTHORING_API UClass* StaticClass<UClusterFractureSettings>()
	{
		return UClusterFractureSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UClusterFractureSettings(Z_Construct_UClass_UClusterFractureSettings, &UClusterFractureSettings::StaticClass, TEXT("/Script/BlastAuthoring"), TEXT("UClusterFractureSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UClusterFractureSettings);
	void URadialFractureSettings::StaticRegisterNativesURadialFractureSettings()
	{
	}
	UClass* Z_Construct_UClass_URadialFractureSettings_NoRegister()
	{
		return URadialFractureSettings::StaticClass();
	}
	struct Z_Construct_UClass_URadialFractureSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Center_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Center;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Normal_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Normal;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Radius_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Radius;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AngularSteps_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_AngularSteps;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RadialSteps_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_RadialSteps;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AngleOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AngleOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Variability_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Variability;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URadialFractureSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_BlastAuthoring,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URadialFractureSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshFractureSettings.h" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URadialFractureSettings_Statics::NewProp_Center_MetaData[] = {
		{ "Category", "RadialVoronoi" },
		{ "Comment", "/** Center of generated pattern */" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Center of generated pattern" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URadialFractureSettings_Statics::NewProp_Center = { "Center", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URadialFractureSettings, Center), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_URadialFractureSettings_Statics::NewProp_Center_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URadialFractureSettings_Statics::NewProp_Center_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URadialFractureSettings_Statics::NewProp_Normal_MetaData[] = {
		{ "Category", "RadialVoronoi" },
		{ "Comment", "/** Normal to plane in which sites are generated */" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Normal to plane in which sites are generated" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URadialFractureSettings_Statics::NewProp_Normal = { "Normal", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URadialFractureSettings, Normal), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_URadialFractureSettings_Statics::NewProp_Normal_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URadialFractureSettings_Statics::NewProp_Normal_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URadialFractureSettings_Statics::NewProp_Radius_MetaData[] = {
		{ "Category", "RadialVoronoi" },
		{ "Comment", "/** Pattern radius */" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Pattern radius" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_URadialFractureSettings_Statics::NewProp_Radius = { "Radius", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URadialFractureSettings, Radius), METADATA_PARAMS(Z_Construct_UClass_URadialFractureSettings_Statics::NewProp_Radius_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URadialFractureSettings_Statics::NewProp_Radius_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URadialFractureSettings_Statics::NewProp_AngularSteps_MetaData[] = {
		{ "Category", "RadialVoronoi" },
		{ "Comment", "/** Number of angular steps */" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Number of angular steps" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_URadialFractureSettings_Statics::NewProp_AngularSteps = { "AngularSteps", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URadialFractureSettings, AngularSteps), METADATA_PARAMS(Z_Construct_UClass_URadialFractureSettings_Statics::NewProp_AngularSteps_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URadialFractureSettings_Statics::NewProp_AngularSteps_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URadialFractureSettings_Statics::NewProp_RadialSteps_MetaData[] = {
		{ "Category", "RadialVoronoi" },
		{ "Comment", "/** Number of radial steps */" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Number of radial steps" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_URadialFractureSettings_Statics::NewProp_RadialSteps = { "RadialSteps", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URadialFractureSettings, RadialSteps), METADATA_PARAMS(Z_Construct_UClass_URadialFractureSettings_Statics::NewProp_RadialSteps_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URadialFractureSettings_Statics::NewProp_RadialSteps_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URadialFractureSettings_Statics::NewProp_AngleOffset_MetaData[] = {
		{ "Category", "RadialVoronoi" },
		{ "Comment", "/** Angle offset at each radial step */" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Angle offset at each radial step" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_URadialFractureSettings_Statics::NewProp_AngleOffset = { "AngleOffset", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URadialFractureSettings, AngleOffset), METADATA_PARAMS(Z_Construct_UClass_URadialFractureSettings_Statics::NewProp_AngleOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URadialFractureSettings_Statics::NewProp_AngleOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URadialFractureSettings_Statics::NewProp_Variability_MetaData[] = {
		{ "Category", "RadialVoronoi" },
		{ "Comment", "/** Randomness of sites distribution */" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Randomness of sites distribution" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_URadialFractureSettings_Statics::NewProp_Variability = { "Variability", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URadialFractureSettings, Variability), METADATA_PARAMS(Z_Construct_UClass_URadialFractureSettings_Statics::NewProp_Variability_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URadialFractureSettings_Statics::NewProp_Variability_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URadialFractureSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URadialFractureSettings_Statics::NewProp_Center,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URadialFractureSettings_Statics::NewProp_Normal,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URadialFractureSettings_Statics::NewProp_Radius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URadialFractureSettings_Statics::NewProp_AngularSteps,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URadialFractureSettings_Statics::NewProp_RadialSteps,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URadialFractureSettings_Statics::NewProp_AngleOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URadialFractureSettings_Statics::NewProp_Variability,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URadialFractureSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URadialFractureSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URadialFractureSettings_Statics::ClassParams = {
		&URadialFractureSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URadialFractureSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URadialFractureSettings_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_URadialFractureSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URadialFractureSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URadialFractureSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URadialFractureSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URadialFractureSettings, 3847085087);
	template<> BLASTAUTHORING_API UClass* StaticClass<URadialFractureSettings>()
	{
		return URadialFractureSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URadialFractureSettings(Z_Construct_UClass_URadialFractureSettings, &URadialFractureSettings::StaticClass, TEXT("/Script/BlastAuthoring"), TEXT("URadialFractureSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URadialFractureSettings);
	void USlicingFractureSettings::StaticRegisterNativesUSlicingFractureSettings()
	{
	}
	UClass* Z_Construct_UClass_USlicingFractureSettings_NoRegister()
	{
		return USlicingFractureSettings::StaticClass();
	}
	struct Z_Construct_UClass_USlicingFractureSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SlicesX_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_SlicesX;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SlicesY_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_SlicesY;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SlicesZ_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_SlicesZ;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SliceAngleVariation_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SliceAngleVariation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SliceOffsetVariation_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SliceOffsetVariation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Amplitude_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Amplitude;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Frequency_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Frequency;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OctaveNumber_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_OctaveNumber;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SurfaceResolution_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SurfaceResolution;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USlicingFractureSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_BlastAuthoring,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USlicingFractureSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshFractureSettings.h" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_SlicesX_MetaData[] = {
		{ "Category", "Slicing" },
		{ "Comment", "/** Num Slices X axis - Slicing Method */" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Num Slices X axis - Slicing Method" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_SlicesX = { "SlicesX", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USlicingFractureSettings, SlicesX), METADATA_PARAMS(Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_SlicesX_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_SlicesX_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_SlicesY_MetaData[] = {
		{ "Category", "Slicing" },
		{ "Comment", "/** Num Slices Y axis - Slicing Method */" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Num Slices Y axis - Slicing Method" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_SlicesY = { "SlicesY", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USlicingFractureSettings, SlicesY), METADATA_PARAMS(Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_SlicesY_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_SlicesY_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_SlicesZ_MetaData[] = {
		{ "Category", "Slicing" },
		{ "Comment", "/** Num Slices Z axis - Slicing Method */" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Num Slices Z axis - Slicing Method" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_SlicesZ = { "SlicesZ", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USlicingFractureSettings, SlicesZ), METADATA_PARAMS(Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_SlicesZ_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_SlicesZ_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_SliceAngleVariation_MetaData[] = {
		{ "Category", "Slicing" },
		{ "Comment", "/** Slicing Angle Variation - Slicing Method [0..1] */" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Slicing Angle Variation - Slicing Method [0..1]" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_SliceAngleVariation = { "SliceAngleVariation", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USlicingFractureSettings, SliceAngleVariation), METADATA_PARAMS(Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_SliceAngleVariation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_SliceAngleVariation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_SliceOffsetVariation_MetaData[] = {
		{ "Category", "Slicing" },
		{ "Comment", "/** Slicing Offset Variation - Slicing Method [0..1] */" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Slicing Offset Variation - Slicing Method [0..1]" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_SliceOffsetVariation = { "SliceOffsetVariation", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USlicingFractureSettings, SliceOffsetVariation), METADATA_PARAMS(Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_SliceOffsetVariation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_SliceOffsetVariation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_Amplitude_MetaData[] = {
		{ "Category", "Noise" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_Amplitude = { "Amplitude", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USlicingFractureSettings, Amplitude), METADATA_PARAMS(Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_Amplitude_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_Amplitude_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_Frequency_MetaData[] = {
		{ "Category", "Noise" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_Frequency = { "Frequency", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USlicingFractureSettings, Frequency), METADATA_PARAMS(Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_Frequency_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_Frequency_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_OctaveNumber_MetaData[] = {
		{ "Category", "Noise" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_OctaveNumber = { "OctaveNumber", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USlicingFractureSettings, OctaveNumber), METADATA_PARAMS(Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_OctaveNumber_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_OctaveNumber_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_SurfaceResolution_MetaData[] = {
		{ "Category", "Noise" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_SurfaceResolution = { "SurfaceResolution", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USlicingFractureSettings, SurfaceResolution), METADATA_PARAMS(Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_SurfaceResolution_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_SurfaceResolution_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USlicingFractureSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_SlicesX,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_SlicesY,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_SlicesZ,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_SliceAngleVariation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_SliceOffsetVariation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_Amplitude,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_Frequency,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_OctaveNumber,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USlicingFractureSettings_Statics::NewProp_SurfaceResolution,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USlicingFractureSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USlicingFractureSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USlicingFractureSettings_Statics::ClassParams = {
		&USlicingFractureSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USlicingFractureSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USlicingFractureSettings_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_USlicingFractureSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USlicingFractureSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USlicingFractureSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USlicingFractureSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USlicingFractureSettings, 3692336453);
	template<> BLASTAUTHORING_API UClass* StaticClass<USlicingFractureSettings>()
	{
		return USlicingFractureSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USlicingFractureSettings(Z_Construct_UClass_USlicingFractureSettings, &USlicingFractureSettings::StaticClass, TEXT("/Script/BlastAuthoring"), TEXT("USlicingFractureSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USlicingFractureSettings);
	void UPlaneCutFractureSettings::StaticRegisterNativesUPlaneCutFractureSettings()
	{
	}
	UClass* Z_Construct_UClass_UPlaneCutFractureSettings_NoRegister()
	{
		return UPlaneCutFractureSettings::StaticClass();
	}
	struct Z_Construct_UClass_UPlaneCutFractureSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumberOfCuts_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumberOfCuts;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CutChunkChance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CutChunkChance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Amplitude_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Amplitude;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Frequency_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Frequency;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OctaveNumber_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_OctaveNumber;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SurfaceResolution_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SurfaceResolution;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPlaneCutFractureSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_BlastAuthoring,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlaneCutFractureSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshFractureSettings.h" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlaneCutFractureSettings_Statics::NewProp_NumberOfCuts_MetaData[] = {
		{ "Category", "PlaneCut" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UPlaneCutFractureSettings_Statics::NewProp_NumberOfCuts = { "NumberOfCuts", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPlaneCutFractureSettings, NumberOfCuts), METADATA_PARAMS(Z_Construct_UClass_UPlaneCutFractureSettings_Statics::NewProp_NumberOfCuts_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlaneCutFractureSettings_Statics::NewProp_NumberOfCuts_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlaneCutFractureSettings_Statics::NewProp_CutChunkChance_MetaData[] = {
		{ "Category", "PlaneCut" },
		{ "Comment", "/** Chance for subsequent cutting plane to cut individual chunks */" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Chance for subsequent cutting plane to cut individual chunks" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UPlaneCutFractureSettings_Statics::NewProp_CutChunkChance = { "CutChunkChance", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPlaneCutFractureSettings, CutChunkChance), METADATA_PARAMS(Z_Construct_UClass_UPlaneCutFractureSettings_Statics::NewProp_CutChunkChance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlaneCutFractureSettings_Statics::NewProp_CutChunkChance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlaneCutFractureSettings_Statics::NewProp_Amplitude_MetaData[] = {
		{ "Category", "Noise" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UPlaneCutFractureSettings_Statics::NewProp_Amplitude = { "Amplitude", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPlaneCutFractureSettings, Amplitude), METADATA_PARAMS(Z_Construct_UClass_UPlaneCutFractureSettings_Statics::NewProp_Amplitude_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlaneCutFractureSettings_Statics::NewProp_Amplitude_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlaneCutFractureSettings_Statics::NewProp_Frequency_MetaData[] = {
		{ "Category", "Noise" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UPlaneCutFractureSettings_Statics::NewProp_Frequency = { "Frequency", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPlaneCutFractureSettings, Frequency), METADATA_PARAMS(Z_Construct_UClass_UPlaneCutFractureSettings_Statics::NewProp_Frequency_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlaneCutFractureSettings_Statics::NewProp_Frequency_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlaneCutFractureSettings_Statics::NewProp_OctaveNumber_MetaData[] = {
		{ "Category", "Noise" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UPlaneCutFractureSettings_Statics::NewProp_OctaveNumber = { "OctaveNumber", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPlaneCutFractureSettings, OctaveNumber), METADATA_PARAMS(Z_Construct_UClass_UPlaneCutFractureSettings_Statics::NewProp_OctaveNumber_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlaneCutFractureSettings_Statics::NewProp_OctaveNumber_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlaneCutFractureSettings_Statics::NewProp_SurfaceResolution_MetaData[] = {
		{ "Category", "Noise" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UPlaneCutFractureSettings_Statics::NewProp_SurfaceResolution = { "SurfaceResolution", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPlaneCutFractureSettings, SurfaceResolution), METADATA_PARAMS(Z_Construct_UClass_UPlaneCutFractureSettings_Statics::NewProp_SurfaceResolution_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlaneCutFractureSettings_Statics::NewProp_SurfaceResolution_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPlaneCutFractureSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlaneCutFractureSettings_Statics::NewProp_NumberOfCuts,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlaneCutFractureSettings_Statics::NewProp_CutChunkChance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlaneCutFractureSettings_Statics::NewProp_Amplitude,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlaneCutFractureSettings_Statics::NewProp_Frequency,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlaneCutFractureSettings_Statics::NewProp_OctaveNumber,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlaneCutFractureSettings_Statics::NewProp_SurfaceResolution,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPlaneCutFractureSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPlaneCutFractureSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPlaneCutFractureSettings_Statics::ClassParams = {
		&UPlaneCutFractureSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UPlaneCutFractureSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UPlaneCutFractureSettings_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UPlaneCutFractureSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPlaneCutFractureSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPlaneCutFractureSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPlaneCutFractureSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPlaneCutFractureSettings, 1608363021);
	template<> BLASTAUTHORING_API UClass* StaticClass<UPlaneCutFractureSettings>()
	{
		return UPlaneCutFractureSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPlaneCutFractureSettings(Z_Construct_UClass_UPlaneCutFractureSettings, &UPlaneCutFractureSettings::StaticClass, TEXT("/Script/BlastAuthoring"), TEXT("UPlaneCutFractureSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPlaneCutFractureSettings);
	void UCutoutFractureSettings::StaticRegisterNativesUCutoutFractureSettings()
	{
	}
	UClass* Z_Construct_UClass_UCutoutFractureSettings_NoRegister()
	{
		return UCutoutFractureSettings::StaticClass();
	}
	struct Z_Construct_UClass_UCutoutFractureSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Transform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Transform;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Scale_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Scale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IsRelativeTransform_MetaData[];
#endif
		static void NewProp_IsRelativeTransform_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsRelativeTransform;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SnapThreshold_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SnapThreshold;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SegmentationErrorThreshold_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SegmentationErrorThreshold;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CutoutTexture_MetaData[];
#endif
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_CutoutTexture;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCutoutFractureSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_BlastAuthoring,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCutoutFractureSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "// #todo: Noise configuration\n" },
		{ "IncludePath", "MeshFractureSettings.h" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "#todo: Noise configuration" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCutoutFractureSettings_Statics::NewProp_Transform_MetaData[] = {
		{ "Category", "Cutout" },
		{ "Comment", "/**\n\x09Transform for initial pattern position and orientation.\n\x09""By default 2d pattern lies in XY plane (Y is up) the center of pattern is (0, 0)\n\x09*/" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Transform for initial pattern position and orientation.\nBy default 2d pattern lies in XY plane (Y is up) the center of pattern is (0, 0)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCutoutFractureSettings_Statics::NewProp_Transform = { "Transform", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCutoutFractureSettings, Transform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UClass_UCutoutFractureSettings_Statics::NewProp_Transform_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCutoutFractureSettings_Statics::NewProp_Transform_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCutoutFractureSettings_Statics::NewProp_Scale_MetaData[] = {
		{ "Category", "Cutout" },
		{ "Comment", "/**\n\x09Scale for pattern. Unscaled pattern has size (1, 1).\n\x09""For negative scale pattern will be placed at the center of chunk and scaled with max distance between points of its AABB\n\x09*/" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Scale for pattern. Unscaled pattern has size (1, 1).\nFor negative scale pattern will be placed at the center of chunk and scaled with max distance between points of its AABB" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCutoutFractureSettings_Statics::NewProp_Scale = { "Scale", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCutoutFractureSettings, Scale), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UClass_UCutoutFractureSettings_Statics::NewProp_Scale_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCutoutFractureSettings_Statics::NewProp_Scale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCutoutFractureSettings_Statics::NewProp_IsRelativeTransform_MetaData[] = {
		{ "Category", "Cutout" },
		{ "Comment", "/**\n\x09If relative transform is set - position will be displacement vector from chunk's center. Otherwise from global origin.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "If relative transform is set - position will be displacement vector from chunk's center. Otherwise from global origin." },
	};
#endif
	void Z_Construct_UClass_UCutoutFractureSettings_Statics::NewProp_IsRelativeTransform_SetBit(void* Obj)
	{
		((UCutoutFractureSettings*)Obj)->IsRelativeTransform = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCutoutFractureSettings_Statics::NewProp_IsRelativeTransform = { "IsRelativeTransform", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCutoutFractureSettings), &Z_Construct_UClass_UCutoutFractureSettings_Statics::NewProp_IsRelativeTransform_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCutoutFractureSettings_Statics::NewProp_IsRelativeTransform_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCutoutFractureSettings_Statics::NewProp_IsRelativeTransform_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCutoutFractureSettings_Statics::NewProp_SnapThreshold_MetaData[] = {
		{ "Category", "Cutout" },
		{ "Comment", "/**\n\x09The pixel distance at which neighboring cutout vertices and segments may be snapped into alignment. By default set it to 1\n\x09*/" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "The pixel distance at which neighboring cutout vertices and segments may be snapped into alignment. By default set it to 1" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UCutoutFractureSettings_Statics::NewProp_SnapThreshold = { "SnapThreshold", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCutoutFractureSettings, SnapThreshold), METADATA_PARAMS(Z_Construct_UClass_UCutoutFractureSettings_Statics::NewProp_SnapThreshold_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCutoutFractureSettings_Statics::NewProp_SnapThreshold_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCutoutFractureSettings_Statics::NewProp_SegmentationErrorThreshold_MetaData[] = {
		{ "Category", "Cutout" },
		{ "Comment", "/**\n\x09Reduce the number of vertices on curve until segmentation error is smaller than this value. By default set it to 0.001\n\x09*/" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Reduce the number of vertices on curve until segmentation error is smaller than this value. By default set it to 0.001" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UCutoutFractureSettings_Statics::NewProp_SegmentationErrorThreshold = { "SegmentationErrorThreshold", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCutoutFractureSettings, SegmentationErrorThreshold), METADATA_PARAMS(Z_Construct_UClass_UCutoutFractureSettings_Statics::NewProp_SegmentationErrorThreshold_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCutoutFractureSettings_Statics::NewProp_SegmentationErrorThreshold_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCutoutFractureSettings_Statics::NewProp_CutoutTexture_MetaData[] = {
		{ "Category", "Cutout" },
		{ "Comment", "/** Cutout bitmap */" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Cutout bitmap" },
	};
#endif
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UClass_UCutoutFractureSettings_Statics::NewProp_CutoutTexture = { "CutoutTexture", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCutoutFractureSettings, CutoutTexture), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCutoutFractureSettings_Statics::NewProp_CutoutTexture_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCutoutFractureSettings_Statics::NewProp_CutoutTexture_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCutoutFractureSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCutoutFractureSettings_Statics::NewProp_Transform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCutoutFractureSettings_Statics::NewProp_Scale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCutoutFractureSettings_Statics::NewProp_IsRelativeTransform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCutoutFractureSettings_Statics::NewProp_SnapThreshold,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCutoutFractureSettings_Statics::NewProp_SegmentationErrorThreshold,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCutoutFractureSettings_Statics::NewProp_CutoutTexture,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCutoutFractureSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCutoutFractureSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCutoutFractureSettings_Statics::ClassParams = {
		&UCutoutFractureSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UCutoutFractureSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UCutoutFractureSettings_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCutoutFractureSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCutoutFractureSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCutoutFractureSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCutoutFractureSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCutoutFractureSettings, 748734611);
	template<> BLASTAUTHORING_API UClass* StaticClass<UCutoutFractureSettings>()
	{
		return UCutoutFractureSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCutoutFractureSettings(Z_Construct_UClass_UCutoutFractureSettings, &UCutoutFractureSettings::StaticClass, TEXT("/Script/BlastAuthoring"), TEXT("UCutoutFractureSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCutoutFractureSettings);
	void UBrickFractureSettings::StaticRegisterNativesUBrickFractureSettings()
	{
	}
	UClass* Z_Construct_UClass_UBrickFractureSettings_NoRegister()
	{
		return UBrickFractureSettings::StaticClass();
	}
	struct Z_Construct_UClass_UBrickFractureSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Forward_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Forward_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Forward;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Up_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Up_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Up;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BrickLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BrickLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BrickHeight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BrickHeight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Amplitude_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Amplitude;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Frequency_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Frequency;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OctaveNumber_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_OctaveNumber;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SurfaceResolution_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SurfaceResolution;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBrickFractureSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_BlastAuthoring,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBrickFractureSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "// #todo: custom brick gracture pattern\n" },
		{ "IncludePath", "MeshFractureSettings.h" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "#todo: custom brick gracture pattern" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_Forward_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_Forward_MetaData[] = {
		{ "Category", "Brick" },
		{ "Comment", "/** Forward Direction to project brick pattern. */" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Forward Direction to project brick pattern." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_Forward = { "Forward", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBrickFractureSettings, Forward), Z_Construct_UEnum_BlastAuthoring_EMeshFractureBrickProjection, METADATA_PARAMS(Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_Forward_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_Forward_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_Up_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_Up_MetaData[] = {
		{ "Category", "Brick" },
		{ "Comment", "/** Up Direction for vertical brick slices. */" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Up Direction for vertical brick slices." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_Up = { "Up", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBrickFractureSettings, Up), Z_Construct_UEnum_BlastAuthoring_EMeshFractureBrickProjection, METADATA_PARAMS(Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_Up_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_Up_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_BrickLength_MetaData[] = {
		{ "Category", "Brick" },
		{ "Comment", "/** Brick length */" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Brick length" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_BrickLength = { "BrickLength", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBrickFractureSettings, BrickLength), METADATA_PARAMS(Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_BrickLength_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_BrickLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_BrickHeight_MetaData[] = {
		{ "Category", "Brick" },
		{ "Comment", "/** Brick Height */" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Brick Height" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_BrickHeight = { "BrickHeight", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBrickFractureSettings, BrickHeight), METADATA_PARAMS(Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_BrickHeight_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_BrickHeight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_Amplitude_MetaData[] = {
		{ "Category", "Noise" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_Amplitude = { "Amplitude", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBrickFractureSettings, Amplitude), METADATA_PARAMS(Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_Amplitude_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_Amplitude_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_Frequency_MetaData[] = {
		{ "Category", "Noise" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_Frequency = { "Frequency", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBrickFractureSettings, Frequency), METADATA_PARAMS(Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_Frequency_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_Frequency_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_OctaveNumber_MetaData[] = {
		{ "Category", "Noise" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_OctaveNumber = { "OctaveNumber", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBrickFractureSettings, OctaveNumber), METADATA_PARAMS(Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_OctaveNumber_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_OctaveNumber_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_SurfaceResolution_MetaData[] = {
		{ "Category", "Noise" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_SurfaceResolution = { "SurfaceResolution", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBrickFractureSettings, SurfaceResolution), METADATA_PARAMS(Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_SurfaceResolution_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_SurfaceResolution_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UBrickFractureSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_Forward_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_Forward,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_Up_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_Up,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_BrickLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_BrickHeight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_Amplitude,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_Frequency,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_OctaveNumber,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBrickFractureSettings_Statics::NewProp_SurfaceResolution,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBrickFractureSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBrickFractureSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBrickFractureSettings_Statics::ClassParams = {
		&UBrickFractureSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UBrickFractureSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UBrickFractureSettings_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UBrickFractureSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBrickFractureSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBrickFractureSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBrickFractureSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBrickFractureSettings, 3410571879);
	template<> BLASTAUTHORING_API UClass* StaticClass<UBrickFractureSettings>()
	{
		return UBrickFractureSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBrickFractureSettings(Z_Construct_UClass_UBrickFractureSettings, &UBrickFractureSettings::StaticClass, TEXT("/Script/BlastAuthoring"), TEXT("UBrickFractureSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBrickFractureSettings);
	void UMeshFractureSettings::StaticRegisterNativesUMeshFractureSettings()
	{
	}
	UClass* Z_Construct_UClass_UMeshFractureSettings_NoRegister()
	{
		return UMeshFractureSettings::StaticClass();
	}
	struct Z_Construct_UClass_UMeshFractureSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CommonSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CommonSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UniformSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_UniformSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClusterSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ClusterSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RadialSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RadialSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SlicingSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SlicingSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlaneCutSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PlaneCutSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CutoutSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CutoutSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BrickSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BrickSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshFractureSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_BlastAuthoring,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshFractureSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshFractureSettings.h" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshFractureSettings_Statics::NewProp_CommonSettings_MetaData[] = {
		{ "Category", "FractureCommon" },
		{ "Comment", "// general\n" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "general" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshFractureSettings_Statics::NewProp_CommonSettings = { "CommonSettings", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshFractureSettings, CommonSettings), Z_Construct_UClass_UCommonFractureSettings_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshFractureSettings_Statics::NewProp_CommonSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshFractureSettings_Statics::NewProp_CommonSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshFractureSettings_Statics::NewProp_UniformSettings_MetaData[] = {
		{ "Category", "UniformVoronoi" },
		{ "Comment", "// Uniform Voronoi\n" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Uniform Voronoi" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshFractureSettings_Statics::NewProp_UniformSettings = { "UniformSettings", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshFractureSettings, UniformSettings), Z_Construct_UClass_UUniformFractureSettings_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshFractureSettings_Statics::NewProp_UniformSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshFractureSettings_Statics::NewProp_UniformSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshFractureSettings_Statics::NewProp_ClusterSettings_MetaData[] = {
		{ "Category", "ClusteredVoronoi" },
		{ "Comment", "// Clustered Voronoi\n" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Clustered Voronoi" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshFractureSettings_Statics::NewProp_ClusterSettings = { "ClusterSettings", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshFractureSettings, ClusterSettings), Z_Construct_UClass_UClusterFractureSettings_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshFractureSettings_Statics::NewProp_ClusterSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshFractureSettings_Statics::NewProp_ClusterSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshFractureSettings_Statics::NewProp_RadialSettings_MetaData[] = {
		{ "Category", "RadialVoronoi" },
		{ "Comment", "// Radial Voronoi\n" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Radial Voronoi" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshFractureSettings_Statics::NewProp_RadialSettings = { "RadialSettings", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshFractureSettings, RadialSettings), Z_Construct_UClass_URadialFractureSettings_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshFractureSettings_Statics::NewProp_RadialSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshFractureSettings_Statics::NewProp_RadialSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshFractureSettings_Statics::NewProp_SlicingSettings_MetaData[] = {
		{ "Category", "Slicing" },
		{ "Comment", "// Slicing\n" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Slicing" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshFractureSettings_Statics::NewProp_SlicingSettings = { "SlicingSettings", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshFractureSettings, SlicingSettings), Z_Construct_UClass_USlicingFractureSettings_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshFractureSettings_Statics::NewProp_SlicingSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshFractureSettings_Statics::NewProp_SlicingSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshFractureSettings_Statics::NewProp_PlaneCutSettings_MetaData[] = {
		{ "Category", "PlaneCut" },
		{ "Comment", "// Plane Cut\n" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Plane Cut" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshFractureSettings_Statics::NewProp_PlaneCutSettings = { "PlaneCutSettings", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshFractureSettings, PlaneCutSettings), Z_Construct_UClass_UPlaneCutFractureSettings_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshFractureSettings_Statics::NewProp_PlaneCutSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshFractureSettings_Statics::NewProp_PlaneCutSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshFractureSettings_Statics::NewProp_CutoutSettings_MetaData[] = {
		{ "Category", "Cutout" },
		{ "Comment", "// Cutout\n" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Cutout" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshFractureSettings_Statics::NewProp_CutoutSettings = { "CutoutSettings", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshFractureSettings, CutoutSettings), Z_Construct_UClass_UCutoutFractureSettings_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshFractureSettings_Statics::NewProp_CutoutSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshFractureSettings_Statics::NewProp_CutoutSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshFractureSettings_Statics::NewProp_BrickSettings_MetaData[] = {
		{ "Category", "Cutout" },
		{ "Comment", "// Brick\n" },
		{ "ModuleRelativePath", "Public/MeshFractureSettings.h" },
		{ "ToolTip", "Brick" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshFractureSettings_Statics::NewProp_BrickSettings = { "BrickSettings", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshFractureSettings, BrickSettings), Z_Construct_UClass_UBrickFractureSettings_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshFractureSettings_Statics::NewProp_BrickSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshFractureSettings_Statics::NewProp_BrickSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshFractureSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshFractureSettings_Statics::NewProp_CommonSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshFractureSettings_Statics::NewProp_UniformSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshFractureSettings_Statics::NewProp_ClusterSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshFractureSettings_Statics::NewProp_RadialSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshFractureSettings_Statics::NewProp_SlicingSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshFractureSettings_Statics::NewProp_PlaneCutSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshFractureSettings_Statics::NewProp_CutoutSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshFractureSettings_Statics::NewProp_BrickSettings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshFractureSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshFractureSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshFractureSettings_Statics::ClassParams = {
		&UMeshFractureSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshFractureSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshFractureSettings_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshFractureSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshFractureSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshFractureSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshFractureSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshFractureSettings, 2245373675);
	template<> BLASTAUTHORING_API UClass* StaticClass<UMeshFractureSettings>()
	{
		return UMeshFractureSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshFractureSettings(Z_Construct_UClass_UMeshFractureSettings, &UMeshFractureSettings::StaticClass, TEXT("/Script/BlastAuthoring"), TEXT("UMeshFractureSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshFractureSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
