// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "BlastAuthoring/Public/FractureMesh.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFractureMesh() {}
// Cross Module References
	BLASTAUTHORING_API UClass* Z_Construct_UClass_UFractureMesh_NoRegister();
	BLASTAUTHORING_API UClass* Z_Construct_UClass_UFractureMesh();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_BlastAuthoring();
// End Cross Module References
	void UFractureMesh::StaticRegisterNativesUFractureMesh()
	{
	}
	UClass* Z_Construct_UClass_UFractureMesh_NoRegister()
	{
		return UFractureMesh::StaticClass();
	}
	struct Z_Construct_UClass_UFractureMesh_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFractureMesh_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_BlastAuthoring,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureMesh_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Performs Voronoi or Slicing fracture of the currently selected mesh */" },
		{ "IncludePath", "FractureMesh.h" },
		{ "ModuleRelativePath", "Public/FractureMesh.h" },
		{ "ToolTip", "Performs Voronoi or Slicing fracture of the currently selected mesh" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFractureMesh_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFractureMesh>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFractureMesh_Statics::ClassParams = {
		&UFractureMesh::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFractureMesh_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureMesh_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFractureMesh()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFractureMesh_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFractureMesh, 3309488999);
	template<> BLASTAUTHORING_API UClass* StaticClass<UFractureMesh>()
	{
		return UFractureMesh::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFractureMesh(Z_Construct_UClass_UFractureMesh, &UFractureMesh::StaticClass, TEXT("/Script/BlastAuthoring"), TEXT("UFractureMesh"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFractureMesh);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
