// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MotoSynthEditor/Private/MotoSynthSourceFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMotoSynthSourceFactory() {}
// Cross Module References
	MOTOSYNTHEDITOR_API UClass* Z_Construct_UClass_UMotoSynthPresetFactory_NoRegister();
	MOTOSYNTHEDITOR_API UClass* Z_Construct_UClass_UMotoSynthPresetFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_MotoSynthEditor();
	MOTOSYNTHEDITOR_API UClass* Z_Construct_UClass_UMotoSynthSourceFactory_NoRegister();
	MOTOSYNTHEDITOR_API UClass* Z_Construct_UClass_UMotoSynthSourceFactory();
// End Cross Module References
	void UMotoSynthPresetFactory::StaticRegisterNativesUMotoSynthPresetFactory()
	{
	}
	UClass* Z_Construct_UClass_UMotoSynthPresetFactory_NoRegister()
	{
		return UMotoSynthPresetFactory::StaticClass();
	}
	struct Z_Construct_UClass_UMotoSynthPresetFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMotoSynthPresetFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_MotoSynthEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMotoSynthPresetFactory_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "MotoSynthSourceFactory.h" },
		{ "ModuleRelativePath", "Private/MotoSynthSourceFactory.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMotoSynthPresetFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMotoSynthPresetFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMotoSynthPresetFactory_Statics::ClassParams = {
		&UMotoSynthPresetFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMotoSynthPresetFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMotoSynthPresetFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMotoSynthPresetFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMotoSynthPresetFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMotoSynthPresetFactory, 4245240500);
	template<> MOTOSYNTHEDITOR_API UClass* StaticClass<UMotoSynthPresetFactory>()
	{
		return UMotoSynthPresetFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMotoSynthPresetFactory(Z_Construct_UClass_UMotoSynthPresetFactory, &UMotoSynthPresetFactory::StaticClass, TEXT("/Script/MotoSynthEditor"), TEXT("UMotoSynthPresetFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMotoSynthPresetFactory);
	void UMotoSynthSourceFactory::StaticRegisterNativesUMotoSynthSourceFactory()
	{
	}
	UClass* Z_Construct_UClass_UMotoSynthSourceFactory_NoRegister()
	{
		return UMotoSynthSourceFactory::StaticClass();
	}
	struct Z_Construct_UClass_UMotoSynthSourceFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMotoSynthSourceFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_MotoSynthEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMotoSynthSourceFactory_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "MotoSynthSourceFactory.h" },
		{ "ModuleRelativePath", "Private/MotoSynthSourceFactory.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMotoSynthSourceFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMotoSynthSourceFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMotoSynthSourceFactory_Statics::ClassParams = {
		&UMotoSynthSourceFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMotoSynthSourceFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMotoSynthSourceFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMotoSynthSourceFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMotoSynthSourceFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMotoSynthSourceFactory, 691629554);
	template<> MOTOSYNTHEDITOR_API UClass* StaticClass<UMotoSynthSourceFactory>()
	{
		return UMotoSynthSourceFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMotoSynthSourceFactory(Z_Construct_UClass_UMotoSynthSourceFactory, &UMotoSynthSourceFactory::StaticClass, TEXT("/Script/MotoSynthEditor"), TEXT("UMotoSynthSourceFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMotoSynthSourceFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
