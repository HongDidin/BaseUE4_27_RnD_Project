// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UVPSettings;
#ifdef VPUTILITIES_VPSettings_generated_h
#error "VPSettings.generated.h already included, missing '#pragma once' in VPSettings.h"
#endif
#define VPUTILITIES_VPSettings_generated_h

#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPSettings_h_17_SPARSE_DATA
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPSettings_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetVPSettings);


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPSettings_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetVPSettings);


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPSettings_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVPSettings(); \
	friend struct Z_Construct_UClass_UVPSettings_Statics; \
public: \
	DECLARE_CLASS(UVPSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/VPUtilities"), NO_API) \
	DECLARE_SERIALIZER(UVPSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPSettings_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUVPSettings(); \
	friend struct Z_Construct_UClass_UVPSettings_Statics; \
public: \
	DECLARE_CLASS(UVPSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/VPUtilities"), NO_API) \
	DECLARE_SERIALIZER(UVPSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPSettings_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVPSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVPSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVPSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVPSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVPSettings(UVPSettings&&); \
	NO_API UVPSettings(const UVPSettings&); \
public:


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPSettings_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVPSettings(UVPSettings&&); \
	NO_API UVPSettings(const UVPSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVPSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVPSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UVPSettings)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPSettings_h_17_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Roles() { return STRUCT_OFFSET(UVPSettings, Roles); } \
	FORCEINLINE static uint32 __PPO__CommandLineRoles() { return STRUCT_OFFSET(UVPSettings, CommandLineRoles); }


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPSettings_h_14_PROLOG
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPSettings_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPSettings_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPSettings_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPSettings_h_17_RPC_WRAPPERS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPSettings_h_17_INCLASS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPSettings_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPSettings_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPSettings_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPSettings_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPSettings_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPSettings_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPSettings_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VPUTILITIES_API UClass* StaticClass<class UVPSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
