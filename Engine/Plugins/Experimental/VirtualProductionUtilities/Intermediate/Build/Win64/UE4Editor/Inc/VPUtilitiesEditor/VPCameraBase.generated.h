// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef VPUTILITIESEDITOR_VPCameraBase_generated_h
#error "VPCameraBase.generated.h already included, missing '#pragma once' in VPCameraBase.h"
#endif
#define VPUTILITIESEDITOR_VPCameraBase_generated_h

#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCameraBase_h_16_SPARSE_DATA
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCameraBase_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execResetPreview);


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCameraBase_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execResetPreview);


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCameraBase_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAVPCameraBase(); \
	friend struct Z_Construct_UClass_AVPCameraBase_Statics; \
public: \
	DECLARE_CLASS(AVPCameraBase, ACineCameraActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/VPUtilitiesEditor"), NO_API) \
	DECLARE_SERIALIZER(AVPCameraBase)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCameraBase_h_16_INCLASS \
private: \
	static void StaticRegisterNativesAVPCameraBase(); \
	friend struct Z_Construct_UClass_AVPCameraBase_Statics; \
public: \
	DECLARE_CLASS(AVPCameraBase, ACineCameraActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/VPUtilitiesEditor"), NO_API) \
	DECLARE_SERIALIZER(AVPCameraBase)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCameraBase_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AVPCameraBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AVPCameraBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AVPCameraBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AVPCameraBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AVPCameraBase(AVPCameraBase&&); \
	NO_API AVPCameraBase(const AVPCameraBase&); \
public:


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCameraBase_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AVPCameraBase(AVPCameraBase&&); \
	NO_API AVPCameraBase(const AVPCameraBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AVPCameraBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AVPCameraBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AVPCameraBase)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCameraBase_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SelectedByUsers() { return STRUCT_OFFSET(AVPCameraBase, SelectedByUsers); }


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCameraBase_h_13_PROLOG
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCameraBase_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCameraBase_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCameraBase_h_16_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCameraBase_h_16_RPC_WRAPPERS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCameraBase_h_16_INCLASS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCameraBase_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCameraBase_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCameraBase_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCameraBase_h_16_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCameraBase_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCameraBase_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCameraBase_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VPUTILITIESEDITOR_API UClass* StaticClass<class AVPCameraBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCameraBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
