// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
class UObject;
class UVPBookmark;
#ifdef VPBOOKMARK_VPBookmarkBlueprintLibrary_generated_h
#error "VPBookmarkBlueprintLibrary.generated.h already included, missing '#pragma once' in VPBookmarkBlueprintLibrary.h"
#endif
#define VPBOOKMARK_VPBookmarkBlueprintLibrary_generated_h

#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmarkBlueprintLibrary_h_17_SPARSE_DATA
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmarkBlueprintLibrary_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execCreateVPBookmarkName); \
	DECLARE_FUNCTION(execGetAllVPBookmark); \
	DECLARE_FUNCTION(execGetAllVPBookmarkActors); \
	DECLARE_FUNCTION(execFindVPBookmark);


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmarkBlueprintLibrary_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execCreateVPBookmarkName); \
	DECLARE_FUNCTION(execGetAllVPBookmark); \
	DECLARE_FUNCTION(execGetAllVPBookmarkActors); \
	DECLARE_FUNCTION(execFindVPBookmark);


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmarkBlueprintLibrary_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVPBookmarkBlueprintLibrary(); \
	friend struct Z_Construct_UClass_UVPBookmarkBlueprintLibrary_Statics; \
public: \
	DECLARE_CLASS(UVPBookmarkBlueprintLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VPBookmark"), NO_API) \
	DECLARE_SERIALIZER(UVPBookmarkBlueprintLibrary)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmarkBlueprintLibrary_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUVPBookmarkBlueprintLibrary(); \
	friend struct Z_Construct_UClass_UVPBookmarkBlueprintLibrary_Statics; \
public: \
	DECLARE_CLASS(UVPBookmarkBlueprintLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VPBookmark"), NO_API) \
	DECLARE_SERIALIZER(UVPBookmarkBlueprintLibrary)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmarkBlueprintLibrary_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVPBookmarkBlueprintLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVPBookmarkBlueprintLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVPBookmarkBlueprintLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVPBookmarkBlueprintLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVPBookmarkBlueprintLibrary(UVPBookmarkBlueprintLibrary&&); \
	NO_API UVPBookmarkBlueprintLibrary(const UVPBookmarkBlueprintLibrary&); \
public:


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmarkBlueprintLibrary_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVPBookmarkBlueprintLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVPBookmarkBlueprintLibrary(UVPBookmarkBlueprintLibrary&&); \
	NO_API UVPBookmarkBlueprintLibrary(const UVPBookmarkBlueprintLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVPBookmarkBlueprintLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVPBookmarkBlueprintLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVPBookmarkBlueprintLibrary)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmarkBlueprintLibrary_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmarkBlueprintLibrary_h_14_PROLOG
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmarkBlueprintLibrary_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmarkBlueprintLibrary_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmarkBlueprintLibrary_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmarkBlueprintLibrary_h_17_RPC_WRAPPERS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmarkBlueprintLibrary_h_17_INCLASS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmarkBlueprintLibrary_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmarkBlueprintLibrary_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmarkBlueprintLibrary_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmarkBlueprintLibrary_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmarkBlueprintLibrary_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmarkBlueprintLibrary_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmarkBlueprintLibrary_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VPBOOKMARK_API UClass* StaticClass<class UVPBookmarkBlueprintLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmarkBlueprintLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
