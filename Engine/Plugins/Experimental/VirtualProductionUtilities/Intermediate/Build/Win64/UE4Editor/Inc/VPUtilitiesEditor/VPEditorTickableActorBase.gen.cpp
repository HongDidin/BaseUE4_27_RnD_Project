// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VPUtilitiesEditor/Public/VPEditorTickableActorBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVPEditorTickableActorBase() {}
// Cross Module References
	VPUTILITIESEDITOR_API UClass* Z_Construct_UClass_AVPEditorTickableActorBase_NoRegister();
	VPUTILITIESEDITOR_API UClass* Z_Construct_UClass_AVPEditorTickableActorBase();
	VPUTILITIES_API UClass* Z_Construct_UClass_AVPViewportTickableActorBase();
	UPackage* Z_Construct_UPackage__Script_VPUtilitiesEditor();
// End Cross Module References
	void AVPEditorTickableActorBase::StaticRegisterNativesAVPEditorTickableActorBase()
	{
	}
	UClass* Z_Construct_UClass_AVPEditorTickableActorBase_NoRegister()
	{
		return AVPEditorTickableActorBase::StaticClass();
	}
	struct Z_Construct_UClass_AVPEditorTickableActorBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AVPEditorTickableActorBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AVPViewportTickableActorBase,
		(UObject* (*)())Z_Construct_UPackage__Script_VPUtilitiesEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVPEditorTickableActorBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Actor that tick in the Editor viewport with the event EditorTick.\n */" },
		{ "IncludePath", "VPEditorTickableActorBase.h" },
		{ "ModuleRelativePath", "Public/VPEditorTickableActorBase.h" },
		{ "ToolTip", "Actor that tick in the Editor viewport with the event EditorTick." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AVPEditorTickableActorBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AVPEditorTickableActorBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AVPEditorTickableActorBase_Statics::ClassParams = {
		&AVPEditorTickableActorBase::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A5u,
		METADATA_PARAMS(Z_Construct_UClass_AVPEditorTickableActorBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AVPEditorTickableActorBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AVPEditorTickableActorBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AVPEditorTickableActorBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AVPEditorTickableActorBase, 2994912924);
	template<> VPUTILITIESEDITOR_API UClass* StaticClass<AVPEditorTickableActorBase>()
	{
		return AVPEditorTickableActorBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AVPEditorTickableActorBase(Z_Construct_UClass_AVPEditorTickableActorBase, &AVPEditorTickableActorBase::StaticClass, TEXT("/Script/VPUtilitiesEditor"), TEXT("AVPEditorTickableActorBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AVPEditorTickableActorBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
