// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef VPUTILITIES_VPFullScreenUserWidgetActor_generated_h
#error "VPFullScreenUserWidgetActor.generated.h already included, missing '#pragma once' in VPFullScreenUserWidgetActor.h"
#endif
#define VPUTILITIES_VPFullScreenUserWidgetActor_generated_h

#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidgetActor_h_18_SPARSE_DATA
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidgetActor_h_18_RPC_WRAPPERS
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidgetActor_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidgetActor_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFullScreenUserWidgetActor(); \
	friend struct Z_Construct_UClass_AFullScreenUserWidgetActor_Statics; \
public: \
	DECLARE_CLASS(AFullScreenUserWidgetActor, AInfo, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/VPUtilities"), NO_API) \
	DECLARE_SERIALIZER(AFullScreenUserWidgetActor)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidgetActor_h_18_INCLASS \
private: \
	static void StaticRegisterNativesAFullScreenUserWidgetActor(); \
	friend struct Z_Construct_UClass_AFullScreenUserWidgetActor_Statics; \
public: \
	DECLARE_CLASS(AFullScreenUserWidgetActor, AInfo, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/VPUtilities"), NO_API) \
	DECLARE_SERIALIZER(AFullScreenUserWidgetActor)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidgetActor_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AFullScreenUserWidgetActor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFullScreenUserWidgetActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFullScreenUserWidgetActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFullScreenUserWidgetActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFullScreenUserWidgetActor(AFullScreenUserWidgetActor&&); \
	NO_API AFullScreenUserWidgetActor(const AFullScreenUserWidgetActor&); \
public:


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidgetActor_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFullScreenUserWidgetActor(AFullScreenUserWidgetActor&&); \
	NO_API AFullScreenUserWidgetActor(const AFullScreenUserWidgetActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFullScreenUserWidgetActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFullScreenUserWidgetActor); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFullScreenUserWidgetActor)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidgetActor_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ScreenUserWidget() { return STRUCT_OFFSET(AFullScreenUserWidgetActor, ScreenUserWidget); }


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidgetActor_h_15_PROLOG
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidgetActor_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidgetActor_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidgetActor_h_18_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidgetActor_h_18_RPC_WRAPPERS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidgetActor_h_18_INCLASS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidgetActor_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidgetActor_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidgetActor_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidgetActor_h_18_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidgetActor_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidgetActor_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidgetActor_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VPUTILITIES_API UClass* StaticClass<class AFullScreenUserWidgetActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidgetActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
