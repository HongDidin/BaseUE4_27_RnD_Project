// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VPUtilitiesEditor/Public/VPScoutingSubsystem.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVPScoutingSubsystem() {}
// Cross Module References
	VPUTILITIESEDITOR_API UEnum* Z_Construct_UEnum_VPUtilitiesEditor_EVProdPanelIDs();
	UPackage* Z_Construct_UPackage__Script_VPUtilitiesEditor();
	VPUTILITIESEDITOR_API UClass* Z_Construct_UClass_UVPScoutingSubsystemHelpersBase_NoRegister();
	VPUTILITIESEDITOR_API UClass* Z_Construct_UClass_UVPScoutingSubsystemHelpersBase();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	VPUTILITIESEDITOR_API UClass* Z_Construct_UClass_UVPScoutingSubsystemGestureManagerBase_NoRegister();
	VPUTILITIESEDITOR_API UClass* Z_Construct_UClass_UVPScoutingSubsystemGestureManagerBase();
	VPUTILITIESEDITOR_API UClass* Z_Construct_UClass_UVPScoutingSubsystem_NoRegister();
	VPUTILITIESEDITOR_API UClass* Z_Construct_UClass_UVPScoutingSubsystem();
	EDITORSUBSYSTEM_API UClass* Z_Construct_UClass_UEditorSubsystem();
	VREDITOR_API UClass* Z_Construct_UClass_UVREditorInteractor_NoRegister();
	VREDITOR_API UClass* Z_Construct_UClass_AVREditorFloatingUI_NoRegister();
	UMG_API UClass* Z_Construct_UClass_UUserWidget_NoRegister();
	VREDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FVREditorFloatingUICreationContext();
// End Cross Module References
	static UEnum* EVProdPanelIDs_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_VPUtilitiesEditor_EVProdPanelIDs, Z_Construct_UPackage__Script_VPUtilitiesEditor(), TEXT("EVProdPanelIDs"));
		}
		return Singleton;
	}
	template<> VPUTILITIESEDITOR_API UEnum* StaticEnum<EVProdPanelIDs>()
	{
		return EVProdPanelIDs_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EVProdPanelIDs(EVProdPanelIDs_StaticEnum, TEXT("/Script/VPUtilitiesEditor"), TEXT("EVProdPanelIDs"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_VPUtilitiesEditor_EVProdPanelIDs_Hash() { return 2349172425U; }
	UEnum* Z_Construct_UEnum_VPUtilitiesEditor_EVProdPanelIDs()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_VPUtilitiesEditor();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EVProdPanelIDs"), 0, Get_Z_Construct_UEnum_VPUtilitiesEditor_EVProdPanelIDs_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EVProdPanelIDs::Main", (int64)EVProdPanelIDs::Main },
				{ "EVProdPanelIDs::Left", (int64)EVProdPanelIDs::Left },
				{ "EVProdPanelIDs::Right", (int64)EVProdPanelIDs::Right },
				{ "EVProdPanelIDs::Context", (int64)EVProdPanelIDs::Context },
				{ "EVProdPanelIDs::Timeline", (int64)EVProdPanelIDs::Timeline },
				{ "EVProdPanelIDs::Measure", (int64)EVProdPanelIDs::Measure },
				{ "EVProdPanelIDs::Gaffer", (int64)EVProdPanelIDs::Gaffer },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Context.Name", "EVProdPanelIDs::Context" },
				{ "Gaffer.Name", "EVProdPanelIDs::Gaffer" },
				{ "Left.Name", "EVProdPanelIDs::Left" },
				{ "Main.Name", "EVProdPanelIDs::Main" },
				{ "Measure.Name", "EVProdPanelIDs::Measure" },
				{ "ModuleRelativePath", "Public/VPScoutingSubsystem.h" },
				{ "Right.Name", "EVProdPanelIDs::Right" },
				{ "Timeline.Name", "EVProdPanelIDs::Timeline" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_VPUtilitiesEditor,
				nullptr,
				"EVProdPanelIDs",
				"EVProdPanelIDs",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UVPScoutingSubsystemHelpersBase::StaticRegisterNativesUVPScoutingSubsystemHelpersBase()
	{
	}
	UClass* Z_Construct_UClass_UVPScoutingSubsystemHelpersBase_NoRegister()
	{
		return UVPScoutingSubsystemHelpersBase::StaticClass();
	}
	struct Z_Construct_UClass_UVPScoutingSubsystemHelpersBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVPScoutingSubsystemHelpersBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_VPUtilitiesEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVPScoutingSubsystemHelpersBase_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/*\n * Base class of the helper class defined in BP\n */" },
		{ "IncludePath", "VPScoutingSubsystem.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/VPScoutingSubsystem.h" },
		{ "ShowWorldContextPin", "" },
		{ "ToolTip", "* Base class of the helper class defined in BP" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVPScoutingSubsystemHelpersBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVPScoutingSubsystemHelpersBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVPScoutingSubsystemHelpersBase_Statics::ClassParams = {
		&UVPScoutingSubsystemHelpersBase::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000800A1u,
		METADATA_PARAMS(Z_Construct_UClass_UVPScoutingSubsystemHelpersBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVPScoutingSubsystemHelpersBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVPScoutingSubsystemHelpersBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVPScoutingSubsystemHelpersBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVPScoutingSubsystemHelpersBase, 1007718440);
	template<> VPUTILITIESEDITOR_API UClass* StaticClass<UVPScoutingSubsystemHelpersBase>()
	{
		return UVPScoutingSubsystemHelpersBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVPScoutingSubsystemHelpersBase(Z_Construct_UClass_UVPScoutingSubsystemHelpersBase, &UVPScoutingSubsystemHelpersBase::StaticClass, TEXT("/Script/VPUtilitiesEditor"), TEXT("UVPScoutingSubsystemHelpersBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVPScoutingSubsystemHelpersBase);
	DEFINE_FUNCTION(UVPScoutingSubsystemGestureManagerBase::execOnVREditingModeExit)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnVREditingModeExit_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVPScoutingSubsystemGestureManagerBase::execOnVREditingModeEnter)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnVREditingModeEnter_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVPScoutingSubsystemGestureManagerBase::execEditorTick)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_DeltaSeconds);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->EditorTick_Implementation(Z_Param_DeltaSeconds);
		P_NATIVE_END;
	}
	static FName NAME_UVPScoutingSubsystemGestureManagerBase_EditorTick = FName(TEXT("EditorTick"));
	void UVPScoutingSubsystemGestureManagerBase::EditorTick(float DeltaSeconds)
	{
		VPScoutingSubsystemGestureManagerBase_eventEditorTick_Parms Parms;
		Parms.DeltaSeconds=DeltaSeconds;
		ProcessEvent(FindFunctionChecked(NAME_UVPScoutingSubsystemGestureManagerBase_EditorTick),&Parms);
	}
	static FName NAME_UVPScoutingSubsystemGestureManagerBase_OnVREditingModeEnter = FName(TEXT("OnVREditingModeEnter"));
	void UVPScoutingSubsystemGestureManagerBase::OnVREditingModeEnter()
	{
		ProcessEvent(FindFunctionChecked(NAME_UVPScoutingSubsystemGestureManagerBase_OnVREditingModeEnter),NULL);
	}
	static FName NAME_UVPScoutingSubsystemGestureManagerBase_OnVREditingModeExit = FName(TEXT("OnVREditingModeExit"));
	void UVPScoutingSubsystemGestureManagerBase::OnVREditingModeExit()
	{
		ProcessEvent(FindFunctionChecked(NAME_UVPScoutingSubsystemGestureManagerBase_OnVREditingModeExit),NULL);
	}
	void UVPScoutingSubsystemGestureManagerBase::StaticRegisterNativesUVPScoutingSubsystemGestureManagerBase()
	{
		UClass* Class = UVPScoutingSubsystemGestureManagerBase::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "EditorTick", &UVPScoutingSubsystemGestureManagerBase::execEditorTick },
			{ "OnVREditingModeEnter", &UVPScoutingSubsystemGestureManagerBase::execOnVREditingModeEnter },
			{ "OnVREditingModeExit", &UVPScoutingSubsystemGestureManagerBase::execOnVREditingModeExit },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UVPScoutingSubsystemGestureManagerBase_EditorTick_Statics
	{
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DeltaSeconds;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UVPScoutingSubsystemGestureManagerBase_EditorTick_Statics::NewProp_DeltaSeconds = { "DeltaSeconds", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VPScoutingSubsystemGestureManagerBase_eventEditorTick_Parms, DeltaSeconds), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVPScoutingSubsystemGestureManagerBase_EditorTick_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVPScoutingSubsystemGestureManagerBase_EditorTick_Statics::NewProp_DeltaSeconds,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystemGestureManagerBase_EditorTick_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "Tick" },
		{ "ModuleRelativePath", "Public/VPScoutingSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVPScoutingSubsystemGestureManagerBase_EditorTick_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVPScoutingSubsystemGestureManagerBase, nullptr, "EditorTick", nullptr, nullptr, sizeof(VPScoutingSubsystemGestureManagerBase_eventEditorTick_Parms), Z_Construct_UFunction_UVPScoutingSubsystemGestureManagerBase_EditorTick_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystemGestureManagerBase_EditorTick_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystemGestureManagerBase_EditorTick_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystemGestureManagerBase_EditorTick_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVPScoutingSubsystemGestureManagerBase_EditorTick()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVPScoutingSubsystemGestureManagerBase_EditorTick_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVPScoutingSubsystemGestureManagerBase_OnVREditingModeEnter_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystemGestureManagerBase_OnVREditingModeEnter_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "VR" },
		{ "ModuleRelativePath", "Public/VPScoutingSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVPScoutingSubsystemGestureManagerBase_OnVREditingModeEnter_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVPScoutingSubsystemGestureManagerBase, nullptr, "OnVREditingModeEnter", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystemGestureManagerBase_OnVREditingModeEnter_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystemGestureManagerBase_OnVREditingModeEnter_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVPScoutingSubsystemGestureManagerBase_OnVREditingModeEnter()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVPScoutingSubsystemGestureManagerBase_OnVREditingModeEnter_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVPScoutingSubsystemGestureManagerBase_OnVREditingModeExit_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystemGestureManagerBase_OnVREditingModeExit_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "VR" },
		{ "ModuleRelativePath", "Public/VPScoutingSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVPScoutingSubsystemGestureManagerBase_OnVREditingModeExit_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVPScoutingSubsystemGestureManagerBase, nullptr, "OnVREditingModeExit", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystemGestureManagerBase_OnVREditingModeExit_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystemGestureManagerBase_OnVREditingModeExit_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVPScoutingSubsystemGestureManagerBase_OnVREditingModeExit()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVPScoutingSubsystemGestureManagerBase_OnVREditingModeExit_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UVPScoutingSubsystemGestureManagerBase_NoRegister()
	{
		return UVPScoutingSubsystemGestureManagerBase::StaticClass();
	}
	struct Z_Construct_UClass_UVPScoutingSubsystemGestureManagerBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVPScoutingSubsystemGestureManagerBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_VPUtilitiesEditor,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UVPScoutingSubsystemGestureManagerBase_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UVPScoutingSubsystemGestureManagerBase_EditorTick, "EditorTick" }, // 3918218660
		{ &Z_Construct_UFunction_UVPScoutingSubsystemGestureManagerBase_OnVREditingModeEnter, "OnVREditingModeEnter" }, // 1552025852
		{ &Z_Construct_UFunction_UVPScoutingSubsystemGestureManagerBase_OnVREditingModeExit, "OnVREditingModeExit" }, // 2539469284
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVPScoutingSubsystemGestureManagerBase_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/*\n * Base class of the gesture manager defined in BP\n */" },
		{ "IncludePath", "VPScoutingSubsystem.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/VPScoutingSubsystem.h" },
		{ "ShowWorldContextPin", "" },
		{ "ToolTip", "* Base class of the gesture manager defined in BP" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVPScoutingSubsystemGestureManagerBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVPScoutingSubsystemGestureManagerBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVPScoutingSubsystemGestureManagerBase_Statics::ClassParams = {
		&UVPScoutingSubsystemGestureManagerBase::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x000800A1u,
		METADATA_PARAMS(Z_Construct_UClass_UVPScoutingSubsystemGestureManagerBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVPScoutingSubsystemGestureManagerBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVPScoutingSubsystemGestureManagerBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVPScoutingSubsystemGestureManagerBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVPScoutingSubsystemGestureManagerBase, 3410497572);
	template<> VPUTILITIESEDITOR_API UClass* StaticClass<UVPScoutingSubsystemGestureManagerBase>()
	{
		return UVPScoutingSubsystemGestureManagerBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVPScoutingSubsystemGestureManagerBase(Z_Construct_UClass_UVPScoutingSubsystemGestureManagerBase, &UVPScoutingSubsystemGestureManagerBase::StaticClass, TEXT("/Script/VPUtilitiesEditor"), TEXT("UVPScoutingSubsystemGestureManagerBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVPScoutingSubsystemGestureManagerBase);
	DEFINE_FUNCTION(UVPScoutingSubsystem::execToggleRotationGridSnapping)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		UVPScoutingSubsystem::ToggleRotationGridSnapping();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVPScoutingSubsystem::execIsRotationGridSnappingEnabled)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UVPScoutingSubsystem::IsRotationGridSnappingEnabled();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVPScoutingSubsystem::execToggleLocationGridSnapping)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		UVPScoutingSubsystem::ToggleLocationGridSnapping();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVPScoutingSubsystem::execIsLocationGridSnappingEnabled)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UVPScoutingSubsystem::IsLocationGridSnappingEnabled();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVPScoutingSubsystem::execExitVRMode)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		UVPScoutingSubsystem::ExitVRMode();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVPScoutingSubsystem::execSetIsHelperSystemEnabled)
	{
		P_GET_UBOOL(Z_Param_bInIsHelperSystemEnabled);
		P_FINISH;
		P_NATIVE_BEGIN;
		UVPScoutingSubsystem::SetIsHelperSystemEnabled(Z_Param_bInIsHelperSystemEnabled);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVPScoutingSubsystem::execIsHelperSystemEnabled)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UVPScoutingSubsystem::IsHelperSystemEnabled();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVPScoutingSubsystem::execSetInertiaDampingCVar)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_InInertiaDamping);
		P_FINISH;
		P_NATIVE_BEGIN;
		UVPScoutingSubsystem::SetInertiaDampingCVar(Z_Param_InInertiaDamping);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVPScoutingSubsystem::execSetIsUsingInertiaDamping)
	{
		P_GET_UBOOL(Z_Param_bInIsUsingInertiaDamping);
		P_FINISH;
		P_NATIVE_BEGIN;
		UVPScoutingSubsystem::SetIsUsingInertiaDamping(Z_Param_bInIsUsingInertiaDamping);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVPScoutingSubsystem::execIsUsingInertiaDamping)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UVPScoutingSubsystem::IsUsingInertiaDamping();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVPScoutingSubsystem::execSetGripNavSpeed)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_InGripNavSpeed);
		P_FINISH;
		P_NATIVE_BEGIN;
		UVPScoutingSubsystem::SetGripNavSpeed(Z_Param_InGripNavSpeed);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVPScoutingSubsystem::execGetGripNavSpeed)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=UVPScoutingSubsystem::GetGripNavSpeed();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVPScoutingSubsystem::execSetFlightSpeed)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_InFlightSpeed);
		P_FINISH;
		P_NATIVE_BEGIN;
		UVPScoutingSubsystem::SetFlightSpeed(Z_Param_InFlightSpeed);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVPScoutingSubsystem::execGetFlightSpeed)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=UVPScoutingSubsystem::GetFlightSpeed();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVPScoutingSubsystem::execSetShowTransformGizmoCVar)
	{
		P_GET_UBOOL(Z_Param_bInShowTransformGizmoCVar);
		P_FINISH;
		P_NATIVE_BEGIN;
		UVPScoutingSubsystem::SetShowTransformGizmoCVar(Z_Param_bInShowTransformGizmoCVar);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVPScoutingSubsystem::execSetIsUsingTransformGizmo)
	{
		P_GET_UBOOL(Z_Param_bInIsUsingTransformGizmo);
		P_FINISH;
		P_NATIVE_BEGIN;
		UVPScoutingSubsystem::SetIsUsingTransformGizmo(Z_Param_bInIsUsingTransformGizmo);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVPScoutingSubsystem::execIsUsingTransformGizmo)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UVPScoutingSubsystem::IsUsingTransformGizmo();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVPScoutingSubsystem::execSetIsUsingMetricSystem)
	{
		P_GET_UBOOL(Z_Param_bInUseMetricSystem);
		P_FINISH;
		P_NATIVE_BEGIN;
		UVPScoutingSubsystem::SetIsUsingMetricSystem(Z_Param_bInUseMetricSystem);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVPScoutingSubsystem::execIsUsingMetricSystem)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UVPScoutingSubsystem::IsUsingMetricSystem();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVPScoutingSubsystem::execGetShowName)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=UVPScoutingSubsystem::GetShowName();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVPScoutingSubsystem::execGetDirectorName)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=UVPScoutingSubsystem::GetDirectorName();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVPScoutingSubsystem::execGetVProdPanelID)
	{
		P_GET_ENUM(EVProdPanelIDs,Z_Param_Panel);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FName*)Z_Param__Result=UVPScoutingSubsystem::GetVProdPanelID(EVProdPanelIDs(Z_Param_Panel));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVPScoutingSubsystem::execGetActiveEditorVRControllers)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<UVREditorInteractor*>*)Z_Param__Result=UVPScoutingSubsystem::GetActiveEditorVRControllers();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVPScoutingSubsystem::execGetPanelWidget)
	{
		P_GET_PROPERTY_REF(FNameProperty,Z_Param_Out_PanelID);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UUserWidget**)Z_Param__Result=P_THIS->GetPanelWidget(Z_Param_Out_PanelID);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVPScoutingSubsystem::execGetPanelActor)
	{
		P_GET_PROPERTY_REF(FNameProperty,Z_Param_Out_PanelID);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(AVREditorFloatingUI**)Z_Param__Result=P_THIS->GetPanelActor(Z_Param_Out_PanelID);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVPScoutingSubsystem::execIsVRScoutingUIOpen)
	{
		P_GET_PROPERTY_REF(FNameProperty,Z_Param_Out_PanelID);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsVRScoutingUIOpen(Z_Param_Out_PanelID);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVPScoutingSubsystem::execHideInfoDisplayPanel)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->HideInfoDisplayPanel();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVPScoutingSubsystem::execToggleVRScoutingUI)
	{
		P_GET_STRUCT_REF(FVREditorFloatingUICreationContext,Z_Param_Out_CreationContext);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ToggleVRScoutingUI(Z_Param_Out_CreationContext);
		P_NATIVE_END;
	}
	void UVPScoutingSubsystem::StaticRegisterNativesUVPScoutingSubsystem()
	{
		UClass* Class = UVPScoutingSubsystem::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ExitVRMode", &UVPScoutingSubsystem::execExitVRMode },
			{ "GetActiveEditorVRControllers", &UVPScoutingSubsystem::execGetActiveEditorVRControllers },
			{ "GetDirectorName", &UVPScoutingSubsystem::execGetDirectorName },
			{ "GetFlightSpeed", &UVPScoutingSubsystem::execGetFlightSpeed },
			{ "GetGripNavSpeed", &UVPScoutingSubsystem::execGetGripNavSpeed },
			{ "GetPanelActor", &UVPScoutingSubsystem::execGetPanelActor },
			{ "GetPanelWidget", &UVPScoutingSubsystem::execGetPanelWidget },
			{ "GetShowName", &UVPScoutingSubsystem::execGetShowName },
			{ "GetVProdPanelID", &UVPScoutingSubsystem::execGetVProdPanelID },
			{ "HideInfoDisplayPanel", &UVPScoutingSubsystem::execHideInfoDisplayPanel },
			{ "IsHelperSystemEnabled", &UVPScoutingSubsystem::execIsHelperSystemEnabled },
			{ "IsLocationGridSnappingEnabled", &UVPScoutingSubsystem::execIsLocationGridSnappingEnabled },
			{ "IsRotationGridSnappingEnabled", &UVPScoutingSubsystem::execIsRotationGridSnappingEnabled },
			{ "IsUsingInertiaDamping", &UVPScoutingSubsystem::execIsUsingInertiaDamping },
			{ "IsUsingMetricSystem", &UVPScoutingSubsystem::execIsUsingMetricSystem },
			{ "IsUsingTransformGizmo", &UVPScoutingSubsystem::execIsUsingTransformGizmo },
			{ "IsVRScoutingUIOpen", &UVPScoutingSubsystem::execIsVRScoutingUIOpen },
			{ "SetFlightSpeed", &UVPScoutingSubsystem::execSetFlightSpeed },
			{ "SetGripNavSpeed", &UVPScoutingSubsystem::execSetGripNavSpeed },
			{ "SetInertiaDampingCVar", &UVPScoutingSubsystem::execSetInertiaDampingCVar },
			{ "SetIsHelperSystemEnabled", &UVPScoutingSubsystem::execSetIsHelperSystemEnabled },
			{ "SetIsUsingInertiaDamping", &UVPScoutingSubsystem::execSetIsUsingInertiaDamping },
			{ "SetIsUsingMetricSystem", &UVPScoutingSubsystem::execSetIsUsingMetricSystem },
			{ "SetIsUsingTransformGizmo", &UVPScoutingSubsystem::execSetIsUsingTransformGizmo },
			{ "SetShowTransformGizmoCVar", &UVPScoutingSubsystem::execSetShowTransformGizmoCVar },
			{ "ToggleLocationGridSnapping", &UVPScoutingSubsystem::execToggleLocationGridSnapping },
			{ "ToggleRotationGridSnapping", &UVPScoutingSubsystem::execToggleRotationGridSnapping },
			{ "ToggleVRScoutingUI", &UVPScoutingSubsystem::execToggleVRScoutingUI },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UVPScoutingSubsystem_ExitVRMode_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_ExitVRMode_Statics::Function_MetaDataParams[] = {
		{ "Category", "Virtual Production" },
		{ "Comment", "/** Exit VR Mode  */" },
		{ "ModuleRelativePath", "Public/VPScoutingSubsystem.h" },
		{ "ToolTip", "Exit VR Mode" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVPScoutingSubsystem_ExitVRMode_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVPScoutingSubsystem, nullptr, "ExitVRMode", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_ExitVRMode_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_ExitVRMode_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVPScoutingSubsystem_ExitVRMode()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVPScoutingSubsystem_ExitVRMode_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVPScoutingSubsystem_GetActiveEditorVRControllers_Statics
	{
		struct VPScoutingSubsystem_eventGetActiveEditorVRControllers_Parms
		{
			TArray<UVREditorInteractor*> ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVPScoutingSubsystem_GetActiveEditorVRControllers_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UVREditorInteractor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UVPScoutingSubsystem_GetActiveEditorVRControllers_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VPScoutingSubsystem_eventGetActiveEditorVRControllers_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVPScoutingSubsystem_GetActiveEditorVRControllers_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVPScoutingSubsystem_GetActiveEditorVRControllers_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVPScoutingSubsystem_GetActiveEditorVRControllers_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_GetActiveEditorVRControllers_Statics::Function_MetaDataParams[] = {
		{ "Category", "Virtual Production" },
		{ "ModuleRelativePath", "Public/VPScoutingSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVPScoutingSubsystem_GetActiveEditorVRControllers_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVPScoutingSubsystem, nullptr, "GetActiveEditorVRControllers", nullptr, nullptr, sizeof(VPScoutingSubsystem_eventGetActiveEditorVRControllers_Parms), Z_Construct_UFunction_UVPScoutingSubsystem_GetActiveEditorVRControllers_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_GetActiveEditorVRControllers_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_GetActiveEditorVRControllers_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_GetActiveEditorVRControllers_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVPScoutingSubsystem_GetActiveEditorVRControllers()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVPScoutingSubsystem_GetActiveEditorVRControllers_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVPScoutingSubsystem_GetDirectorName_Statics
	{
		struct VPScoutingSubsystem_eventGetDirectorName_Parms
		{
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UVPScoutingSubsystem_GetDirectorName_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VPScoutingSubsystem_eventGetDirectorName_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVPScoutingSubsystem_GetDirectorName_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVPScoutingSubsystem_GetDirectorName_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_GetDirectorName_Statics::Function_MetaDataParams[] = {
		{ "Category", "Virtual Production" },
		{ "ModuleRelativePath", "Public/VPScoutingSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVPScoutingSubsystem_GetDirectorName_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVPScoutingSubsystem, nullptr, "GetDirectorName", nullptr, nullptr, sizeof(VPScoutingSubsystem_eventGetDirectorName_Parms), Z_Construct_UFunction_UVPScoutingSubsystem_GetDirectorName_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_GetDirectorName_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_GetDirectorName_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_GetDirectorName_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVPScoutingSubsystem_GetDirectorName()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVPScoutingSubsystem_GetDirectorName_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVPScoutingSubsystem_GetFlightSpeed_Statics
	{
		struct VPScoutingSubsystem_eventGetFlightSpeed_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UVPScoutingSubsystem_GetFlightSpeed_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VPScoutingSubsystem_eventGetFlightSpeed_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVPScoutingSubsystem_GetFlightSpeed_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVPScoutingSubsystem_GetFlightSpeed_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_GetFlightSpeed_Statics::Function_MetaDataParams[] = {
		{ "Category", "Virtual Production" },
		{ "Comment", "/** Get flight speed for scouting in VR */" },
		{ "ModuleRelativePath", "Public/VPScoutingSubsystem.h" },
		{ "ToolTip", "Get flight speed for scouting in VR" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVPScoutingSubsystem_GetFlightSpeed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVPScoutingSubsystem, nullptr, "GetFlightSpeed", nullptr, nullptr, sizeof(VPScoutingSubsystem_eventGetFlightSpeed_Parms), Z_Construct_UFunction_UVPScoutingSubsystem_GetFlightSpeed_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_GetFlightSpeed_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_GetFlightSpeed_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_GetFlightSpeed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVPScoutingSubsystem_GetFlightSpeed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVPScoutingSubsystem_GetFlightSpeed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVPScoutingSubsystem_GetGripNavSpeed_Statics
	{
		struct VPScoutingSubsystem_eventGetGripNavSpeed_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UVPScoutingSubsystem_GetGripNavSpeed_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VPScoutingSubsystem_eventGetGripNavSpeed_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVPScoutingSubsystem_GetGripNavSpeed_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVPScoutingSubsystem_GetGripNavSpeed_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_GetGripNavSpeed_Statics::Function_MetaDataParams[] = {
		{ "Category", "Virtual Production" },
		{ "Comment", "/** Get grip nav speed for scouting in VR */" },
		{ "ModuleRelativePath", "Public/VPScoutingSubsystem.h" },
		{ "ToolTip", "Get grip nav speed for scouting in VR" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVPScoutingSubsystem_GetGripNavSpeed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVPScoutingSubsystem, nullptr, "GetGripNavSpeed", nullptr, nullptr, sizeof(VPScoutingSubsystem_eventGetGripNavSpeed_Parms), Z_Construct_UFunction_UVPScoutingSubsystem_GetGripNavSpeed_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_GetGripNavSpeed_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_GetGripNavSpeed_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_GetGripNavSpeed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVPScoutingSubsystem_GetGripNavSpeed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVPScoutingSubsystem_GetGripNavSpeed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVPScoutingSubsystem_GetPanelActor_Statics
	{
		struct VPScoutingSubsystem_eventGetPanelActor_Parms
		{
			FName PanelID;
			AVREditorFloatingUI* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PanelID_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_PanelID;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_GetPanelActor_Statics::NewProp_PanelID_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UVPScoutingSubsystem_GetPanelActor_Statics::NewProp_PanelID = { "PanelID", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VPScoutingSubsystem_eventGetPanelActor_Parms, PanelID), METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_GetPanelActor_Statics::NewProp_PanelID_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_GetPanelActor_Statics::NewProp_PanelID_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVPScoutingSubsystem_GetPanelActor_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VPScoutingSubsystem_eventGetPanelActor_Parms, ReturnValue), Z_Construct_UClass_AVREditorFloatingUI_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVPScoutingSubsystem_GetPanelActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVPScoutingSubsystem_GetPanelActor_Statics::NewProp_PanelID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVPScoutingSubsystem_GetPanelActor_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_GetPanelActor_Statics::Function_MetaDataParams[] = {
		{ "Category", "Virtual Production" },
		{ "Comment", "/** Get UI panel Actor from the passed ID */" },
		{ "ModuleRelativePath", "Public/VPScoutingSubsystem.h" },
		{ "ToolTip", "Get UI panel Actor from the passed ID" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVPScoutingSubsystem_GetPanelActor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVPScoutingSubsystem, nullptr, "GetPanelActor", nullptr, nullptr, sizeof(VPScoutingSubsystem_eventGetPanelActor_Parms), Z_Construct_UFunction_UVPScoutingSubsystem_GetPanelActor_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_GetPanelActor_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_GetPanelActor_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_GetPanelActor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVPScoutingSubsystem_GetPanelActor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVPScoutingSubsystem_GetPanelActor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVPScoutingSubsystem_GetPanelWidget_Statics
	{
		struct VPScoutingSubsystem_eventGetPanelWidget_Parms
		{
			FName PanelID;
			UUserWidget* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PanelID_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_PanelID;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_GetPanelWidget_Statics::NewProp_PanelID_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UVPScoutingSubsystem_GetPanelWidget_Statics::NewProp_PanelID = { "PanelID", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VPScoutingSubsystem_eventGetPanelWidget_Parms, PanelID), METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_GetPanelWidget_Statics::NewProp_PanelID_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_GetPanelWidget_Statics::NewProp_PanelID_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_GetPanelWidget_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVPScoutingSubsystem_GetPanelWidget_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VPScoutingSubsystem_eventGetPanelWidget_Parms, ReturnValue), Z_Construct_UClass_UUserWidget_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_GetPanelWidget_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_GetPanelWidget_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVPScoutingSubsystem_GetPanelWidget_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVPScoutingSubsystem_GetPanelWidget_Statics::NewProp_PanelID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVPScoutingSubsystem_GetPanelWidget_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_GetPanelWidget_Statics::Function_MetaDataParams[] = {
		{ "Category", "Virtual Production" },
		{ "Comment", "/** Get UI panel widget from the passed ID */" },
		{ "ModuleRelativePath", "Public/VPScoutingSubsystem.h" },
		{ "ToolTip", "Get UI panel widget from the passed ID" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVPScoutingSubsystem_GetPanelWidget_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVPScoutingSubsystem, nullptr, "GetPanelWidget", nullptr, nullptr, sizeof(VPScoutingSubsystem_eventGetPanelWidget_Parms), Z_Construct_UFunction_UVPScoutingSubsystem_GetPanelWidget_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_GetPanelWidget_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_GetPanelWidget_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_GetPanelWidget_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVPScoutingSubsystem_GetPanelWidget()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVPScoutingSubsystem_GetPanelWidget_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVPScoutingSubsystem_GetShowName_Statics
	{
		struct VPScoutingSubsystem_eventGetShowName_Parms
		{
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UVPScoutingSubsystem_GetShowName_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VPScoutingSubsystem_eventGetShowName_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVPScoutingSubsystem_GetShowName_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVPScoutingSubsystem_GetShowName_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_GetShowName_Statics::Function_MetaDataParams[] = {
		{ "Category", "Virtual Production" },
		{ "ModuleRelativePath", "Public/VPScoutingSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVPScoutingSubsystem_GetShowName_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVPScoutingSubsystem, nullptr, "GetShowName", nullptr, nullptr, sizeof(VPScoutingSubsystem_eventGetShowName_Parms), Z_Construct_UFunction_UVPScoutingSubsystem_GetShowName_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_GetShowName_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_GetShowName_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_GetShowName_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVPScoutingSubsystem_GetShowName()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVPScoutingSubsystem_GetShowName_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVPScoutingSubsystem_GetVProdPanelID_Statics
	{
		struct VPScoutingSubsystem_eventGetVProdPanelID_Parms
		{
			EVProdPanelIDs Panel;
			FName ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Panel_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Panel_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Panel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UVPScoutingSubsystem_GetVProdPanelID_Statics::NewProp_Panel_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_GetVProdPanelID_Statics::NewProp_Panel_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UVPScoutingSubsystem_GetVProdPanelID_Statics::NewProp_Panel = { "Panel", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VPScoutingSubsystem_eventGetVProdPanelID_Parms, Panel), Z_Construct_UEnum_VPUtilitiesEditor_EVProdPanelIDs, METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_GetVProdPanelID_Statics::NewProp_Panel_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_GetVProdPanelID_Statics::NewProp_Panel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_GetVProdPanelID_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UVPScoutingSubsystem_GetVProdPanelID_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000582, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VPScoutingSubsystem_eventGetVProdPanelID_Parms, ReturnValue), METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_GetVProdPanelID_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_GetVProdPanelID_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVPScoutingSubsystem_GetVProdPanelID_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVPScoutingSubsystem_GetVProdPanelID_Statics::NewProp_Panel_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVPScoutingSubsystem_GetVProdPanelID_Statics::NewProp_Panel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVPScoutingSubsystem_GetVProdPanelID_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_GetVProdPanelID_Statics::Function_MetaDataParams[] = {
		{ "Category", "Virtual Production" },
		{ "ModuleRelativePath", "Public/VPScoutingSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVPScoutingSubsystem_GetVProdPanelID_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVPScoutingSubsystem, nullptr, "GetVProdPanelID", nullptr, nullptr, sizeof(VPScoutingSubsystem_eventGetVProdPanelID_Parms), Z_Construct_UFunction_UVPScoutingSubsystem_GetVProdPanelID_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_GetVProdPanelID_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_GetVProdPanelID_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_GetVProdPanelID_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVPScoutingSubsystem_GetVProdPanelID()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVPScoutingSubsystem_GetVProdPanelID_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVPScoutingSubsystem_HideInfoDisplayPanel_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_HideInfoDisplayPanel_Statics::Function_MetaDataParams[] = {
		{ "Category", "Virtual Production" },
		{ "Comment", "/** Hide VR Sequencer Window*/" },
		{ "ModuleRelativePath", "Public/VPScoutingSubsystem.h" },
		{ "ToolTip", "Hide VR Sequencer Window" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVPScoutingSubsystem_HideInfoDisplayPanel_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVPScoutingSubsystem, nullptr, "HideInfoDisplayPanel", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_HideInfoDisplayPanel_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_HideInfoDisplayPanel_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVPScoutingSubsystem_HideInfoDisplayPanel()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVPScoutingSubsystem_HideInfoDisplayPanel_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVPScoutingSubsystem_IsHelperSystemEnabled_Statics
	{
		struct VPScoutingSubsystem_eventIsHelperSystemEnabled_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVPScoutingSubsystem_IsHelperSystemEnabled_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VPScoutingSubsystem_eventIsHelperSystemEnabled_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVPScoutingSubsystem_IsHelperSystemEnabled_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VPScoutingSubsystem_eventIsHelperSystemEnabled_Parms), &Z_Construct_UFunction_UVPScoutingSubsystem_IsHelperSystemEnabled_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVPScoutingSubsystem_IsHelperSystemEnabled_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVPScoutingSubsystem_IsHelperSystemEnabled_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_IsHelperSystemEnabled_Statics::Function_MetaDataParams[] = {
		{ "Category", "Virtual Production" },
		{ "Comment", "/** Whether the helper system on the controllers is enabled */" },
		{ "ModuleRelativePath", "Public/VPScoutingSubsystem.h" },
		{ "ToolTip", "Whether the helper system on the controllers is enabled" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVPScoutingSubsystem_IsHelperSystemEnabled_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVPScoutingSubsystem, nullptr, "IsHelperSystemEnabled", nullptr, nullptr, sizeof(VPScoutingSubsystem_eventIsHelperSystemEnabled_Parms), Z_Construct_UFunction_UVPScoutingSubsystem_IsHelperSystemEnabled_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_IsHelperSystemEnabled_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_IsHelperSystemEnabled_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_IsHelperSystemEnabled_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVPScoutingSubsystem_IsHelperSystemEnabled()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVPScoutingSubsystem_IsHelperSystemEnabled_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVPScoutingSubsystem_IsLocationGridSnappingEnabled_Statics
	{
		struct VPScoutingSubsystem_eventIsLocationGridSnappingEnabled_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVPScoutingSubsystem_IsLocationGridSnappingEnabled_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VPScoutingSubsystem_eventIsLocationGridSnappingEnabled_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVPScoutingSubsystem_IsLocationGridSnappingEnabled_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VPScoutingSubsystem_eventIsLocationGridSnappingEnabled_Parms), &Z_Construct_UFunction_UVPScoutingSubsystem_IsLocationGridSnappingEnabled_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVPScoutingSubsystem_IsLocationGridSnappingEnabled_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVPScoutingSubsystem_IsLocationGridSnappingEnabled_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_IsLocationGridSnappingEnabled_Statics::Function_MetaDataParams[] = {
		{ "Category", "Virtual Production" },
		{ "Comment", "/** Whether location grid snapping is enabled */" },
		{ "ModuleRelativePath", "Public/VPScoutingSubsystem.h" },
		{ "ToolTip", "Whether location grid snapping is enabled" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVPScoutingSubsystem_IsLocationGridSnappingEnabled_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVPScoutingSubsystem, nullptr, "IsLocationGridSnappingEnabled", nullptr, nullptr, sizeof(VPScoutingSubsystem_eventIsLocationGridSnappingEnabled_Parms), Z_Construct_UFunction_UVPScoutingSubsystem_IsLocationGridSnappingEnabled_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_IsLocationGridSnappingEnabled_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_IsLocationGridSnappingEnabled_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_IsLocationGridSnappingEnabled_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVPScoutingSubsystem_IsLocationGridSnappingEnabled()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVPScoutingSubsystem_IsLocationGridSnappingEnabled_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVPScoutingSubsystem_IsRotationGridSnappingEnabled_Statics
	{
		struct VPScoutingSubsystem_eventIsRotationGridSnappingEnabled_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVPScoutingSubsystem_IsRotationGridSnappingEnabled_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VPScoutingSubsystem_eventIsRotationGridSnappingEnabled_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVPScoutingSubsystem_IsRotationGridSnappingEnabled_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VPScoutingSubsystem_eventIsRotationGridSnappingEnabled_Parms), &Z_Construct_UFunction_UVPScoutingSubsystem_IsRotationGridSnappingEnabled_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVPScoutingSubsystem_IsRotationGridSnappingEnabled_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVPScoutingSubsystem_IsRotationGridSnappingEnabled_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_IsRotationGridSnappingEnabled_Statics::Function_MetaDataParams[] = {
		{ "Category", "Virtual Production" },
		{ "Comment", "/** Whether rotation grid snapping is enabled */" },
		{ "ModuleRelativePath", "Public/VPScoutingSubsystem.h" },
		{ "ToolTip", "Whether rotation grid snapping is enabled" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVPScoutingSubsystem_IsRotationGridSnappingEnabled_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVPScoutingSubsystem, nullptr, "IsRotationGridSnappingEnabled", nullptr, nullptr, sizeof(VPScoutingSubsystem_eventIsRotationGridSnappingEnabled_Parms), Z_Construct_UFunction_UVPScoutingSubsystem_IsRotationGridSnappingEnabled_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_IsRotationGridSnappingEnabled_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_IsRotationGridSnappingEnabled_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_IsRotationGridSnappingEnabled_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVPScoutingSubsystem_IsRotationGridSnappingEnabled()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVPScoutingSubsystem_IsRotationGridSnappingEnabled_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingInertiaDamping_Statics
	{
		struct VPScoutingSubsystem_eventIsUsingInertiaDamping_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingInertiaDamping_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VPScoutingSubsystem_eventIsUsingInertiaDamping_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingInertiaDamping_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VPScoutingSubsystem_eventIsUsingInertiaDamping_Parms), &Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingInertiaDamping_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingInertiaDamping_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingInertiaDamping_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingInertiaDamping_Statics::Function_MetaDataParams[] = {
		{ "Category", "Virtual Production" },
		{ "Comment", "/** Whether grip nav inertia is enabled when scouting in VR */" },
		{ "ModuleRelativePath", "Public/VPScoutingSubsystem.h" },
		{ "ToolTip", "Whether grip nav inertia is enabled when scouting in VR" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingInertiaDamping_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVPScoutingSubsystem, nullptr, "IsUsingInertiaDamping", nullptr, nullptr, sizeof(VPScoutingSubsystem_eventIsUsingInertiaDamping_Parms), Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingInertiaDamping_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingInertiaDamping_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingInertiaDamping_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingInertiaDamping_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingInertiaDamping()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingInertiaDamping_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingMetricSystem_Statics
	{
		struct VPScoutingSubsystem_eventIsUsingMetricSystem_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingMetricSystem_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VPScoutingSubsystem_eventIsUsingMetricSystem_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingMetricSystem_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VPScoutingSubsystem_eventIsUsingMetricSystem_Parms), &Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingMetricSystem_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingMetricSystem_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingMetricSystem_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingMetricSystem_Statics::Function_MetaDataParams[] = {
		{ "Category", "Virtual Production" },
		{ "Comment", "/** Whether the VR user wants to use the metric system instead of imperial */" },
		{ "ModuleRelativePath", "Public/VPScoutingSubsystem.h" },
		{ "ToolTip", "Whether the VR user wants to use the metric system instead of imperial" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingMetricSystem_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVPScoutingSubsystem, nullptr, "IsUsingMetricSystem", nullptr, nullptr, sizeof(VPScoutingSubsystem_eventIsUsingMetricSystem_Parms), Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingMetricSystem_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingMetricSystem_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingMetricSystem_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingMetricSystem_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingMetricSystem()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingMetricSystem_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingTransformGizmo_Statics
	{
		struct VPScoutingSubsystem_eventIsUsingTransformGizmo_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingTransformGizmo_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VPScoutingSubsystem_eventIsUsingTransformGizmo_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingTransformGizmo_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VPScoutingSubsystem_eventIsUsingTransformGizmo_Parms), &Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingTransformGizmo_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingTransformGizmo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingTransformGizmo_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingTransformGizmo_Statics::Function_MetaDataParams[] = {
		{ "Category", "Virtual Production" },
		{ "Comment", "/** Whether the VR user wants to have the transform gizmo enabled */" },
		{ "ModuleRelativePath", "Public/VPScoutingSubsystem.h" },
		{ "ToolTip", "Whether the VR user wants to have the transform gizmo enabled" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingTransformGizmo_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVPScoutingSubsystem, nullptr, "IsUsingTransformGizmo", nullptr, nullptr, sizeof(VPScoutingSubsystem_eventIsUsingTransformGizmo_Parms), Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingTransformGizmo_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingTransformGizmo_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingTransformGizmo_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingTransformGizmo_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingTransformGizmo()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingTransformGizmo_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVPScoutingSubsystem_IsVRScoutingUIOpen_Statics
	{
		struct VPScoutingSubsystem_eventIsVRScoutingUIOpen_Parms
		{
			FName PanelID;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PanelID_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_PanelID;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_IsVRScoutingUIOpen_Statics::NewProp_PanelID_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UVPScoutingSubsystem_IsVRScoutingUIOpen_Statics::NewProp_PanelID = { "PanelID", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VPScoutingSubsystem_eventIsVRScoutingUIOpen_Parms, PanelID), METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_IsVRScoutingUIOpen_Statics::NewProp_PanelID_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_IsVRScoutingUIOpen_Statics::NewProp_PanelID_MetaData)) };
	void Z_Construct_UFunction_UVPScoutingSubsystem_IsVRScoutingUIOpen_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VPScoutingSubsystem_eventIsVRScoutingUIOpen_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVPScoutingSubsystem_IsVRScoutingUIOpen_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VPScoutingSubsystem_eventIsVRScoutingUIOpen_Parms), &Z_Construct_UFunction_UVPScoutingSubsystem_IsVRScoutingUIOpen_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVPScoutingSubsystem_IsVRScoutingUIOpen_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVPScoutingSubsystem_IsVRScoutingUIOpen_Statics::NewProp_PanelID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVPScoutingSubsystem_IsVRScoutingUIOpen_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_IsVRScoutingUIOpen_Statics::Function_MetaDataParams[] = {
		{ "Category", "Virtual Production" },
		{ "Comment", "/** Check whether a widget UI is open*/" },
		{ "ModuleRelativePath", "Public/VPScoutingSubsystem.h" },
		{ "ToolTip", "Check whether a widget UI is open" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVPScoutingSubsystem_IsVRScoutingUIOpen_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVPScoutingSubsystem, nullptr, "IsVRScoutingUIOpen", nullptr, nullptr, sizeof(VPScoutingSubsystem_eventIsVRScoutingUIOpen_Parms), Z_Construct_UFunction_UVPScoutingSubsystem_IsVRScoutingUIOpen_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_IsVRScoutingUIOpen_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_IsVRScoutingUIOpen_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_IsVRScoutingUIOpen_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVPScoutingSubsystem_IsVRScoutingUIOpen()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVPScoutingSubsystem_IsVRScoutingUIOpen_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVPScoutingSubsystem_SetFlightSpeed_Statics
	{
		struct VPScoutingSubsystem_eventSetFlightSpeed_Parms
		{
			float InFlightSpeed;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InFlightSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InFlightSpeed;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_SetFlightSpeed_Statics::NewProp_InFlightSpeed_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UVPScoutingSubsystem_SetFlightSpeed_Statics::NewProp_InFlightSpeed = { "InFlightSpeed", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VPScoutingSubsystem_eventSetFlightSpeed_Parms, InFlightSpeed), METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_SetFlightSpeed_Statics::NewProp_InFlightSpeed_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_SetFlightSpeed_Statics::NewProp_InFlightSpeed_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVPScoutingSubsystem_SetFlightSpeed_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVPScoutingSubsystem_SetFlightSpeed_Statics::NewProp_InFlightSpeed,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_SetFlightSpeed_Statics::Function_MetaDataParams[] = {
		{ "Category", "Virtual Production" },
		{ "Comment", "/** Set flight speed for scouting in VR */" },
		{ "ModuleRelativePath", "Public/VPScoutingSubsystem.h" },
		{ "ToolTip", "Set flight speed for scouting in VR" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVPScoutingSubsystem_SetFlightSpeed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVPScoutingSubsystem, nullptr, "SetFlightSpeed", nullptr, nullptr, sizeof(VPScoutingSubsystem_eventSetFlightSpeed_Parms), Z_Construct_UFunction_UVPScoutingSubsystem_SetFlightSpeed_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_SetFlightSpeed_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_SetFlightSpeed_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_SetFlightSpeed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVPScoutingSubsystem_SetFlightSpeed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVPScoutingSubsystem_SetFlightSpeed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVPScoutingSubsystem_SetGripNavSpeed_Statics
	{
		struct VPScoutingSubsystem_eventSetGripNavSpeed_Parms
		{
			float InGripNavSpeed;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InGripNavSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InGripNavSpeed;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_SetGripNavSpeed_Statics::NewProp_InGripNavSpeed_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UVPScoutingSubsystem_SetGripNavSpeed_Statics::NewProp_InGripNavSpeed = { "InGripNavSpeed", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VPScoutingSubsystem_eventSetGripNavSpeed_Parms, InGripNavSpeed), METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_SetGripNavSpeed_Statics::NewProp_InGripNavSpeed_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_SetGripNavSpeed_Statics::NewProp_InGripNavSpeed_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVPScoutingSubsystem_SetGripNavSpeed_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVPScoutingSubsystem_SetGripNavSpeed_Statics::NewProp_InGripNavSpeed,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_SetGripNavSpeed_Statics::Function_MetaDataParams[] = {
		{ "Category", "Virtual Production" },
		{ "Comment", "/** Set grip nav speed for scouting in VR */" },
		{ "ModuleRelativePath", "Public/VPScoutingSubsystem.h" },
		{ "ToolTip", "Set grip nav speed for scouting in VR" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVPScoutingSubsystem_SetGripNavSpeed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVPScoutingSubsystem, nullptr, "SetGripNavSpeed", nullptr, nullptr, sizeof(VPScoutingSubsystem_eventSetGripNavSpeed_Parms), Z_Construct_UFunction_UVPScoutingSubsystem_SetGripNavSpeed_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_SetGripNavSpeed_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_SetGripNavSpeed_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_SetGripNavSpeed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVPScoutingSubsystem_SetGripNavSpeed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVPScoutingSubsystem_SetGripNavSpeed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVPScoutingSubsystem_SetInertiaDampingCVar_Statics
	{
		struct VPScoutingSubsystem_eventSetInertiaDampingCVar_Parms
		{
			float InInertiaDamping;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InInertiaDamping_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InInertiaDamping;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_SetInertiaDampingCVar_Statics::NewProp_InInertiaDamping_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UVPScoutingSubsystem_SetInertiaDampingCVar_Statics::NewProp_InInertiaDamping = { "InInertiaDamping", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VPScoutingSubsystem_eventSetInertiaDampingCVar_Parms, InInertiaDamping), METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_SetInertiaDampingCVar_Statics::NewProp_InInertiaDamping_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_SetInertiaDampingCVar_Statics::NewProp_InInertiaDamping_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVPScoutingSubsystem_SetInertiaDampingCVar_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVPScoutingSubsystem_SetInertiaDampingCVar_Statics::NewProp_InInertiaDamping,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_SetInertiaDampingCVar_Statics::Function_MetaDataParams[] = {
		{ "Category", "Virtual Production" },
		{ "Comment", "/** Set value of cvar \"VI.HighSpeedInertiaDamping\" */" },
		{ "ModuleRelativePath", "Public/VPScoutingSubsystem.h" },
		{ "ToolTip", "Set value of cvar \"VI.HighSpeedInertiaDamping\"" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVPScoutingSubsystem_SetInertiaDampingCVar_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVPScoutingSubsystem, nullptr, "SetInertiaDampingCVar", nullptr, nullptr, sizeof(VPScoutingSubsystem_eventSetInertiaDampingCVar_Parms), Z_Construct_UFunction_UVPScoutingSubsystem_SetInertiaDampingCVar_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_SetInertiaDampingCVar_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_SetInertiaDampingCVar_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_SetInertiaDampingCVar_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVPScoutingSubsystem_SetInertiaDampingCVar()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVPScoutingSubsystem_SetInertiaDampingCVar_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVPScoutingSubsystem_SetIsHelperSystemEnabled_Statics
	{
		struct VPScoutingSubsystem_eventSetIsHelperSystemEnabled_Parms
		{
			bool bInIsHelperSystemEnabled;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bInIsHelperSystemEnabled_MetaData[];
#endif
		static void NewProp_bInIsHelperSystemEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInIsHelperSystemEnabled;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_SetIsHelperSystemEnabled_Statics::NewProp_bInIsHelperSystemEnabled_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_UVPScoutingSubsystem_SetIsHelperSystemEnabled_Statics::NewProp_bInIsHelperSystemEnabled_SetBit(void* Obj)
	{
		((VPScoutingSubsystem_eventSetIsHelperSystemEnabled_Parms*)Obj)->bInIsHelperSystemEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVPScoutingSubsystem_SetIsHelperSystemEnabled_Statics::NewProp_bInIsHelperSystemEnabled = { "bInIsHelperSystemEnabled", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VPScoutingSubsystem_eventSetIsHelperSystemEnabled_Parms), &Z_Construct_UFunction_UVPScoutingSubsystem_SetIsHelperSystemEnabled_Statics::NewProp_bInIsHelperSystemEnabled_SetBit, METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_SetIsHelperSystemEnabled_Statics::NewProp_bInIsHelperSystemEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_SetIsHelperSystemEnabled_Statics::NewProp_bInIsHelperSystemEnabled_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVPScoutingSubsystem_SetIsHelperSystemEnabled_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVPScoutingSubsystem_SetIsHelperSystemEnabled_Statics::NewProp_bInIsHelperSystemEnabled,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_SetIsHelperSystemEnabled_Statics::Function_MetaDataParams[] = {
		{ "Category", "Virtual Production" },
		{ "Comment", "/** Set whether the helper system on the controllers is enabled   */" },
		{ "ModuleRelativePath", "Public/VPScoutingSubsystem.h" },
		{ "ToolTip", "Set whether the helper system on the controllers is enabled" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVPScoutingSubsystem_SetIsHelperSystemEnabled_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVPScoutingSubsystem, nullptr, "SetIsHelperSystemEnabled", nullptr, nullptr, sizeof(VPScoutingSubsystem_eventSetIsHelperSystemEnabled_Parms), Z_Construct_UFunction_UVPScoutingSubsystem_SetIsHelperSystemEnabled_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_SetIsHelperSystemEnabled_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_SetIsHelperSystemEnabled_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_SetIsHelperSystemEnabled_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVPScoutingSubsystem_SetIsHelperSystemEnabled()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVPScoutingSubsystem_SetIsHelperSystemEnabled_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingInertiaDamping_Statics
	{
		struct VPScoutingSubsystem_eventSetIsUsingInertiaDamping_Parms
		{
			bool bInIsUsingInertiaDamping;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bInIsUsingInertiaDamping_MetaData[];
#endif
		static void NewProp_bInIsUsingInertiaDamping_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInIsUsingInertiaDamping;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingInertiaDamping_Statics::NewProp_bInIsUsingInertiaDamping_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingInertiaDamping_Statics::NewProp_bInIsUsingInertiaDamping_SetBit(void* Obj)
	{
		((VPScoutingSubsystem_eventSetIsUsingInertiaDamping_Parms*)Obj)->bInIsUsingInertiaDamping = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingInertiaDamping_Statics::NewProp_bInIsUsingInertiaDamping = { "bInIsUsingInertiaDamping", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VPScoutingSubsystem_eventSetIsUsingInertiaDamping_Parms), &Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingInertiaDamping_Statics::NewProp_bInIsUsingInertiaDamping_SetBit, METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingInertiaDamping_Statics::NewProp_bInIsUsingInertiaDamping_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingInertiaDamping_Statics::NewProp_bInIsUsingInertiaDamping_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingInertiaDamping_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingInertiaDamping_Statics::NewProp_bInIsUsingInertiaDamping,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingInertiaDamping_Statics::Function_MetaDataParams[] = {
		{ "Category", "Virtual Production" },
		{ "Comment", "/** Set whether grip nav inertia is enabled when scouting in VR */" },
		{ "ModuleRelativePath", "Public/VPScoutingSubsystem.h" },
		{ "ToolTip", "Set whether grip nav inertia is enabled when scouting in VR" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingInertiaDamping_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVPScoutingSubsystem, nullptr, "SetIsUsingInertiaDamping", nullptr, nullptr, sizeof(VPScoutingSubsystem_eventSetIsUsingInertiaDamping_Parms), Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingInertiaDamping_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingInertiaDamping_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingInertiaDamping_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingInertiaDamping_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingInertiaDamping()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingInertiaDamping_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingMetricSystem_Statics
	{
		struct VPScoutingSubsystem_eventSetIsUsingMetricSystem_Parms
		{
			bool bInUseMetricSystem;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bInUseMetricSystem_MetaData[];
#endif
		static void NewProp_bInUseMetricSystem_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInUseMetricSystem;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingMetricSystem_Statics::NewProp_bInUseMetricSystem_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingMetricSystem_Statics::NewProp_bInUseMetricSystem_SetBit(void* Obj)
	{
		((VPScoutingSubsystem_eventSetIsUsingMetricSystem_Parms*)Obj)->bInUseMetricSystem = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingMetricSystem_Statics::NewProp_bInUseMetricSystem = { "bInUseMetricSystem", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VPScoutingSubsystem_eventSetIsUsingMetricSystem_Parms), &Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingMetricSystem_Statics::NewProp_bInUseMetricSystem_SetBit, METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingMetricSystem_Statics::NewProp_bInUseMetricSystem_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingMetricSystem_Statics::NewProp_bInUseMetricSystem_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingMetricSystem_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingMetricSystem_Statics::NewProp_bInUseMetricSystem,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingMetricSystem_Statics::Function_MetaDataParams[] = {
		{ "Category", "Virtual Production" },
		{ "Comment", "/** Set whether the VR user wants to use the metric system instead of imperial */" },
		{ "ModuleRelativePath", "Public/VPScoutingSubsystem.h" },
		{ "ToolTip", "Set whether the VR user wants to use the metric system instead of imperial" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingMetricSystem_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVPScoutingSubsystem, nullptr, "SetIsUsingMetricSystem", nullptr, nullptr, sizeof(VPScoutingSubsystem_eventSetIsUsingMetricSystem_Parms), Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingMetricSystem_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingMetricSystem_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingMetricSystem_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingMetricSystem_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingMetricSystem()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingMetricSystem_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingTransformGizmo_Statics
	{
		struct VPScoutingSubsystem_eventSetIsUsingTransformGizmo_Parms
		{
			bool bInIsUsingTransformGizmo;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bInIsUsingTransformGizmo_MetaData[];
#endif
		static void NewProp_bInIsUsingTransformGizmo_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInIsUsingTransformGizmo;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingTransformGizmo_Statics::NewProp_bInIsUsingTransformGizmo_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingTransformGizmo_Statics::NewProp_bInIsUsingTransformGizmo_SetBit(void* Obj)
	{
		((VPScoutingSubsystem_eventSetIsUsingTransformGizmo_Parms*)Obj)->bInIsUsingTransformGizmo = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingTransformGizmo_Statics::NewProp_bInIsUsingTransformGizmo = { "bInIsUsingTransformGizmo", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VPScoutingSubsystem_eventSetIsUsingTransformGizmo_Parms), &Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingTransformGizmo_Statics::NewProp_bInIsUsingTransformGizmo_SetBit, METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingTransformGizmo_Statics::NewProp_bInIsUsingTransformGizmo_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingTransformGizmo_Statics::NewProp_bInIsUsingTransformGizmo_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingTransformGizmo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingTransformGizmo_Statics::NewProp_bInIsUsingTransformGizmo,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingTransformGizmo_Statics::Function_MetaDataParams[] = {
		{ "Category", "Virtual Production" },
		{ "Comment", "/** Set whether the VR user wants to have the transform gizmo enabled */" },
		{ "ModuleRelativePath", "Public/VPScoutingSubsystem.h" },
		{ "ToolTip", "Set whether the VR user wants to have the transform gizmo enabled" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingTransformGizmo_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVPScoutingSubsystem, nullptr, "SetIsUsingTransformGizmo", nullptr, nullptr, sizeof(VPScoutingSubsystem_eventSetIsUsingTransformGizmo_Parms), Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingTransformGizmo_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingTransformGizmo_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingTransformGizmo_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingTransformGizmo_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingTransformGizmo()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingTransformGizmo_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVPScoutingSubsystem_SetShowTransformGizmoCVar_Statics
	{
		struct VPScoutingSubsystem_eventSetShowTransformGizmoCVar_Parms
		{
			bool bInShowTransformGizmoCVar;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bInShowTransformGizmoCVar_MetaData[];
#endif
		static void NewProp_bInShowTransformGizmoCVar_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInShowTransformGizmoCVar;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_SetShowTransformGizmoCVar_Statics::NewProp_bInShowTransformGizmoCVar_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_UVPScoutingSubsystem_SetShowTransformGizmoCVar_Statics::NewProp_bInShowTransformGizmoCVar_SetBit(void* Obj)
	{
		((VPScoutingSubsystem_eventSetShowTransformGizmoCVar_Parms*)Obj)->bInShowTransformGizmoCVar = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVPScoutingSubsystem_SetShowTransformGizmoCVar_Statics::NewProp_bInShowTransformGizmoCVar = { "bInShowTransformGizmoCVar", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VPScoutingSubsystem_eventSetShowTransformGizmoCVar_Parms), &Z_Construct_UFunction_UVPScoutingSubsystem_SetShowTransformGizmoCVar_Statics::NewProp_bInShowTransformGizmoCVar_SetBit, METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_SetShowTransformGizmoCVar_Statics::NewProp_bInShowTransformGizmoCVar_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_SetShowTransformGizmoCVar_Statics::NewProp_bInShowTransformGizmoCVar_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVPScoutingSubsystem_SetShowTransformGizmoCVar_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVPScoutingSubsystem_SetShowTransformGizmoCVar_Statics::NewProp_bInShowTransformGizmoCVar,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_SetShowTransformGizmoCVar_Statics::Function_MetaDataParams[] = {
		{ "Category", "Virtual Production" },
		{ "Comment", "/** Set value of cvar \"VI.ShowTransformGizmo\" */" },
		{ "ModuleRelativePath", "Public/VPScoutingSubsystem.h" },
		{ "ToolTip", "Set value of cvar \"VI.ShowTransformGizmo\"" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVPScoutingSubsystem_SetShowTransformGizmoCVar_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVPScoutingSubsystem, nullptr, "SetShowTransformGizmoCVar", nullptr, nullptr, sizeof(VPScoutingSubsystem_eventSetShowTransformGizmoCVar_Parms), Z_Construct_UFunction_UVPScoutingSubsystem_SetShowTransformGizmoCVar_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_SetShowTransformGizmoCVar_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_SetShowTransformGizmoCVar_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_SetShowTransformGizmoCVar_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVPScoutingSubsystem_SetShowTransformGizmoCVar()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVPScoutingSubsystem_SetShowTransformGizmoCVar_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVPScoutingSubsystem_ToggleLocationGridSnapping_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_ToggleLocationGridSnapping_Statics::Function_MetaDataParams[] = {
		{ "Category", "Virtual Production" },
		{ "Comment", "/** Toggle location grid snapping */" },
		{ "ModuleRelativePath", "Public/VPScoutingSubsystem.h" },
		{ "ToolTip", "Toggle location grid snapping" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVPScoutingSubsystem_ToggleLocationGridSnapping_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVPScoutingSubsystem, nullptr, "ToggleLocationGridSnapping", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_ToggleLocationGridSnapping_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_ToggleLocationGridSnapping_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVPScoutingSubsystem_ToggleLocationGridSnapping()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVPScoutingSubsystem_ToggleLocationGridSnapping_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVPScoutingSubsystem_ToggleRotationGridSnapping_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_ToggleRotationGridSnapping_Statics::Function_MetaDataParams[] = {
		{ "Category", "Virtual Production" },
		{ "Comment", "/** Toggle rotation grid snapping */" },
		{ "ModuleRelativePath", "Public/VPScoutingSubsystem.h" },
		{ "ToolTip", "Toggle rotation grid snapping" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVPScoutingSubsystem_ToggleRotationGridSnapping_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVPScoutingSubsystem, nullptr, "ToggleRotationGridSnapping", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_ToggleRotationGridSnapping_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_ToggleRotationGridSnapping_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVPScoutingSubsystem_ToggleRotationGridSnapping()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVPScoutingSubsystem_ToggleRotationGridSnapping_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVPScoutingSubsystem_ToggleVRScoutingUI_Statics
	{
		struct VPScoutingSubsystem_eventToggleVRScoutingUI_Parms
		{
			FVREditorFloatingUICreationContext CreationContext;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CreationContext;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVPScoutingSubsystem_ToggleVRScoutingUI_Statics::NewProp_CreationContext = { "CreationContext", nullptr, (EPropertyFlags)0x0010000008000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VPScoutingSubsystem_eventToggleVRScoutingUI_Parms, CreationContext), Z_Construct_UScriptStruct_FVREditorFloatingUICreationContext, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVPScoutingSubsystem_ToggleVRScoutingUI_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVPScoutingSubsystem_ToggleVRScoutingUI_Statics::NewProp_CreationContext,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPScoutingSubsystem_ToggleVRScoutingUI_Statics::Function_MetaDataParams[] = {
		{ "Category", "Virtual Production" },
		{ "Comment", "/** Open a widget UI in front of the user. Opens default VProd UI (defined via the 'Virtual Scouting User Interface' setting) if null. */" },
		{ "ModuleRelativePath", "Public/VPScoutingSubsystem.h" },
		{ "ToolTip", "Open a widget UI in front of the user. Opens default VProd UI (defined via the 'Virtual Scouting User Interface' setting) if null." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVPScoutingSubsystem_ToggleVRScoutingUI_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVPScoutingSubsystem, nullptr, "ToggleVRScoutingUI", nullptr, nullptr, sizeof(VPScoutingSubsystem_eventToggleVRScoutingUI_Parms), Z_Construct_UFunction_UVPScoutingSubsystem_ToggleVRScoutingUI_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_ToggleVRScoutingUI_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVPScoutingSubsystem_ToggleVRScoutingUI_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPScoutingSubsystem_ToggleVRScoutingUI_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVPScoutingSubsystem_ToggleVRScoutingUI()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVPScoutingSubsystem_ToggleVRScoutingUI_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UVPScoutingSubsystem_NoRegister()
	{
		return UVPScoutingSubsystem::StaticClass();
	}
	struct Z_Construct_UClass_UVPScoutingSubsystem_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VPSubsystemHelpers_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_VPSubsystemHelpers;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GestureManager_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_GestureManager;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IsSettingsMenuOpen_MetaData[];
#endif
		static void NewProp_IsSettingsMenuOpen_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsSettingsMenuOpen;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GripNavSpeedCoeff_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_GripNavSpeedCoeff;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVPScoutingSubsystem_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEditorSubsystem,
		(UObject* (*)())Z_Construct_UPackage__Script_VPUtilitiesEditor,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UVPScoutingSubsystem_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UVPScoutingSubsystem_ExitVRMode, "ExitVRMode" }, // 842964809
		{ &Z_Construct_UFunction_UVPScoutingSubsystem_GetActiveEditorVRControllers, "GetActiveEditorVRControllers" }, // 1942951711
		{ &Z_Construct_UFunction_UVPScoutingSubsystem_GetDirectorName, "GetDirectorName" }, // 2696048138
		{ &Z_Construct_UFunction_UVPScoutingSubsystem_GetFlightSpeed, "GetFlightSpeed" }, // 3700477274
		{ &Z_Construct_UFunction_UVPScoutingSubsystem_GetGripNavSpeed, "GetGripNavSpeed" }, // 3931375536
		{ &Z_Construct_UFunction_UVPScoutingSubsystem_GetPanelActor, "GetPanelActor" }, // 668931770
		{ &Z_Construct_UFunction_UVPScoutingSubsystem_GetPanelWidget, "GetPanelWidget" }, // 3090871195
		{ &Z_Construct_UFunction_UVPScoutingSubsystem_GetShowName, "GetShowName" }, // 2877035729
		{ &Z_Construct_UFunction_UVPScoutingSubsystem_GetVProdPanelID, "GetVProdPanelID" }, // 3644075721
		{ &Z_Construct_UFunction_UVPScoutingSubsystem_HideInfoDisplayPanel, "HideInfoDisplayPanel" }, // 2702167075
		{ &Z_Construct_UFunction_UVPScoutingSubsystem_IsHelperSystemEnabled, "IsHelperSystemEnabled" }, // 572821247
		{ &Z_Construct_UFunction_UVPScoutingSubsystem_IsLocationGridSnappingEnabled, "IsLocationGridSnappingEnabled" }, // 4147074709
		{ &Z_Construct_UFunction_UVPScoutingSubsystem_IsRotationGridSnappingEnabled, "IsRotationGridSnappingEnabled" }, // 2143700679
		{ &Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingInertiaDamping, "IsUsingInertiaDamping" }, // 3073928286
		{ &Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingMetricSystem, "IsUsingMetricSystem" }, // 868219984
		{ &Z_Construct_UFunction_UVPScoutingSubsystem_IsUsingTransformGizmo, "IsUsingTransformGizmo" }, // 494052163
		{ &Z_Construct_UFunction_UVPScoutingSubsystem_IsVRScoutingUIOpen, "IsVRScoutingUIOpen" }, // 270279311
		{ &Z_Construct_UFunction_UVPScoutingSubsystem_SetFlightSpeed, "SetFlightSpeed" }, // 3311244050
		{ &Z_Construct_UFunction_UVPScoutingSubsystem_SetGripNavSpeed, "SetGripNavSpeed" }, // 1994531020
		{ &Z_Construct_UFunction_UVPScoutingSubsystem_SetInertiaDampingCVar, "SetInertiaDampingCVar" }, // 1621004376
		{ &Z_Construct_UFunction_UVPScoutingSubsystem_SetIsHelperSystemEnabled, "SetIsHelperSystemEnabled" }, // 3727699931
		{ &Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingInertiaDamping, "SetIsUsingInertiaDamping" }, // 1200277154
		{ &Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingMetricSystem, "SetIsUsingMetricSystem" }, // 1225111505
		{ &Z_Construct_UFunction_UVPScoutingSubsystem_SetIsUsingTransformGizmo, "SetIsUsingTransformGizmo" }, // 2848340711
		{ &Z_Construct_UFunction_UVPScoutingSubsystem_SetShowTransformGizmoCVar, "SetShowTransformGizmoCVar" }, // 2021020936
		{ &Z_Construct_UFunction_UVPScoutingSubsystem_ToggleLocationGridSnapping, "ToggleLocationGridSnapping" }, // 2704119863
		{ &Z_Construct_UFunction_UVPScoutingSubsystem_ToggleRotationGridSnapping, "ToggleRotationGridSnapping" }, // 1920906569
		{ &Z_Construct_UFunction_UVPScoutingSubsystem_ToggleVRScoutingUI, "ToggleVRScoutingUI" }, // 1473328369
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVPScoutingSubsystem_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/*\n * Subsystem used for VR Scouting\n */" },
		{ "IncludePath", "VPScoutingSubsystem.h" },
		{ "ModuleRelativePath", "Public/VPScoutingSubsystem.h" },
		{ "ToolTip", "* Subsystem used for VR Scouting" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVPScoutingSubsystem_Statics::NewProp_VPSubsystemHelpers_MetaData[] = {
		{ "Category", "Virtual Production" },
		{ "Comment", "/** Subsystems can't have any Blueprint implementations, so we attach this class for any BP logic that we to provide. */" },
		{ "ModuleRelativePath", "Public/VPScoutingSubsystem.h" },
		{ "ToolTip", "Subsystems can't have any Blueprint implementations, so we attach this class for any BP logic that we to provide." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UVPScoutingSubsystem_Statics::NewProp_VPSubsystemHelpers = { "VPSubsystemHelpers", nullptr, (EPropertyFlags)0x0010000000002014, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVPScoutingSubsystem, VPSubsystemHelpers), Z_Construct_UClass_UVPScoutingSubsystemHelpersBase_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVPScoutingSubsystem_Statics::NewProp_VPSubsystemHelpers_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVPScoutingSubsystem_Statics::NewProp_VPSubsystemHelpers_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVPScoutingSubsystem_Statics::NewProp_GestureManager_MetaData[] = {
		{ "Category", "Virtual Production" },
		{ "Comment", "/** GestureManager that manage some user input in VR editor. */" },
		{ "ModuleRelativePath", "Public/VPScoutingSubsystem.h" },
		{ "ToolTip", "GestureManager that manage some user input in VR editor." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UVPScoutingSubsystem_Statics::NewProp_GestureManager = { "GestureManager", nullptr, (EPropertyFlags)0x0010000000002014, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVPScoutingSubsystem, GestureManager), Z_Construct_UClass_UVPScoutingSubsystemGestureManagerBase_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVPScoutingSubsystem_Statics::NewProp_GestureManager_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVPScoutingSubsystem_Statics::NewProp_GestureManager_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVPScoutingSubsystem_Statics::NewProp_IsSettingsMenuOpen_MetaData[] = {
		{ "Category", "Main Menu" },
		{ "Comment", "/** bool to keep track of whether the settings menu panel in the main menu is open*/" },
		{ "ModuleRelativePath", "Public/VPScoutingSubsystem.h" },
		{ "ToolTip", "bool to keep track of whether the settings menu panel in the main menu is open" },
	};
#endif
	void Z_Construct_UClass_UVPScoutingSubsystem_Statics::NewProp_IsSettingsMenuOpen_SetBit(void* Obj)
	{
		((UVPScoutingSubsystem*)Obj)->IsSettingsMenuOpen = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVPScoutingSubsystem_Statics::NewProp_IsSettingsMenuOpen = { "IsSettingsMenuOpen", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVPScoutingSubsystem), &Z_Construct_UClass_UVPScoutingSubsystem_Statics::NewProp_IsSettingsMenuOpen_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVPScoutingSubsystem_Statics::NewProp_IsSettingsMenuOpen_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVPScoutingSubsystem_Statics::NewProp_IsSettingsMenuOpen_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVPScoutingSubsystem_Statics::NewProp_GripNavSpeedCoeff_MetaData[] = {
		{ "Category", "Virtual Production" },
		{ "Comment", "/** This is a multiplier for grip nav speed so we can keep the grip nav value in the range 0-1 and increase this variable if we need a bigger range */" },
		{ "ModuleRelativePath", "Public/VPScoutingSubsystem.h" },
		{ "ToolTip", "This is a multiplier for grip nav speed so we can keep the grip nav value in the range 0-1 and increase this variable if we need a bigger range" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVPScoutingSubsystem_Statics::NewProp_GripNavSpeedCoeff = { "GripNavSpeedCoeff", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVPScoutingSubsystem, GripNavSpeedCoeff), METADATA_PARAMS(Z_Construct_UClass_UVPScoutingSubsystem_Statics::NewProp_GripNavSpeedCoeff_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVPScoutingSubsystem_Statics::NewProp_GripNavSpeedCoeff_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UVPScoutingSubsystem_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVPScoutingSubsystem_Statics::NewProp_VPSubsystemHelpers,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVPScoutingSubsystem_Statics::NewProp_GestureManager,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVPScoutingSubsystem_Statics::NewProp_IsSettingsMenuOpen,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVPScoutingSubsystem_Statics::NewProp_GripNavSpeedCoeff,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVPScoutingSubsystem_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVPScoutingSubsystem>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVPScoutingSubsystem_Statics::ClassParams = {
		&UVPScoutingSubsystem::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UVPScoutingSubsystem_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UVPScoutingSubsystem_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UVPScoutingSubsystem_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVPScoutingSubsystem_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVPScoutingSubsystem()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVPScoutingSubsystem_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVPScoutingSubsystem, 3512398819);
	template<> VPUTILITIESEDITOR_API UClass* StaticClass<UVPScoutingSubsystem>()
	{
		return UVPScoutingSubsystem::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVPScoutingSubsystem(Z_Construct_UClass_UVPScoutingSubsystem, &UVPScoutingSubsystem::StaticClass, TEXT("/Script/VPUtilitiesEditor"), TEXT("UVPScoutingSubsystem"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVPScoutingSubsystem);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
