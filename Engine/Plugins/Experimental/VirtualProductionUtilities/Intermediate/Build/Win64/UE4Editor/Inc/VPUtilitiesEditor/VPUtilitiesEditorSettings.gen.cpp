// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VPUtilitiesEditor/Public/VPUtilitiesEditorSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVPUtilitiesEditorSettings() {}
// Cross Module References
	VPUTILITIESEDITOR_API UClass* Z_Construct_UClass_UVPUtilitiesEditorSettings_NoRegister();
	VPUTILITIESEDITOR_API UClass* Z_Construct_UClass_UVPUtilitiesEditorSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_VPUtilitiesEditor();
	BLUTILITY_API UClass* Z_Construct_UClass_UEditorUtilityWidget_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FSoftObjectPath();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FSoftClassPath();
// End Cross Module References
	void UVPUtilitiesEditorSettings::StaticRegisterNativesUVPUtilitiesEditorSettings()
	{
	}
	UClass* Z_Construct_UClass_UVPUtilitiesEditorSettings_NoRegister()
	{
		return UVPUtilitiesEditorSettings::StaticClass();
	}
	struct Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VirtualScoutingUI_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftClassPropertyParams NewProp_VirtualScoutingUI;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FlightSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FlightSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GripNavSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_GripNavSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseMetric_MetaData[];
#endif
		static void NewProp_bUseMetric_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseMetric;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseTransformGizmo_MetaData[];
#endif
		static void NewProp_bUseTransformGizmo_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseTransformGizmo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseGripInertiaDamping_MetaData[];
#endif
		static void NewProp_bUseGripInertiaDamping_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseGripInertiaDamping;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InertiaDamping_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InertiaDamping;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsHelperSystemEnabled_MetaData[];
#endif
		static void NewProp_bIsHelperSystemEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsHelperSystemEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bStartOSCServerAtLaunch_MetaData[];
#endif
		static void NewProp_bStartOSCServerAtLaunch_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bStartOSCServerAtLaunch;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OSCServerAddress_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_OSCServerAddress;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OSCServerPort_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_OSCServerPort;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_StartupOSCListeners_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StartupOSCListeners_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_StartupOSCListeners;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScoutingSubsystemEditorUtilityClassPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ScoutingSubsystemEditorUtilityClassPath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GestureManagerEditorUtilityClassPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GestureManagerEditorUtilityClassPath;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AdditionnalClassToLoad_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AdditionnalClassToLoad_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_AdditionnalClassToLoad;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_VPUtilitiesEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Virtual Production utilities settings for editor\n */" },
		{ "IncludePath", "VPUtilitiesEditorSettings.h" },
		{ "ModuleRelativePath", "Public/VPUtilitiesEditorSettings.h" },
		{ "ToolTip", "Virtual Production utilities settings for editor" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_VirtualScoutingUI_MetaData[] = {
		{ "Category", "Virtual Production" },
		{ "Comment", "/** The default user interface that we'll use for virtual scouting */" },
		{ "DisplayName", "Virtual Scouting User Interface" },
		{ "ModuleRelativePath", "Public/VPUtilitiesEditorSettings.h" },
		{ "ToolTip", "The default user interface that we'll use for virtual scouting" },
	};
#endif
	const UE4CodeGen_Private::FSoftClassPropertyParams Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_VirtualScoutingUI = { "VirtualScoutingUI", nullptr, (EPropertyFlags)0x0014000000004001, UE4CodeGen_Private::EPropertyGenFlags::SoftClass, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVPUtilitiesEditorSettings, VirtualScoutingUI), Z_Construct_UClass_UEditorUtilityWidget_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_VirtualScoutingUI_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_VirtualScoutingUI_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_FlightSpeed_MetaData[] = {
		{ "Category", "Virtual Production" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Speed when flying in VR*/" },
		{ "DisplayName", "Virtual Scouting Flight Speed" },
		{ "ModuleRelativePath", "Public/VPUtilitiesEditorSettings.h" },
		{ "ToolTip", "Speed when flying in VR" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_FlightSpeed = { "FlightSpeed", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVPUtilitiesEditorSettings, FlightSpeed), METADATA_PARAMS(Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_FlightSpeed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_FlightSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_GripNavSpeed_MetaData[] = {
		{ "Category", "Virtual Production" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Speed when using grip nav in VR */" },
		{ "DisplayName", "Virtual Scouting Grip Nav Speed" },
		{ "ModuleRelativePath", "Public/VPUtilitiesEditorSettings.h" },
		{ "ToolTip", "Speed when using grip nav in VR" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_GripNavSpeed = { "GripNavSpeed", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVPUtilitiesEditorSettings, GripNavSpeed), METADATA_PARAMS(Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_GripNavSpeed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_GripNavSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_bUseMetric_MetaData[] = {
		{ "Category", "Virtual Production" },
		{ "Comment", "/** Whether to use the metric system or imperial for measurements */" },
		{ "DisplayName", "Show Measurements In Metric Units" },
		{ "ModuleRelativePath", "Public/VPUtilitiesEditorSettings.h" },
		{ "ToolTip", "Whether to use the metric system or imperial for measurements" },
	};
#endif
	void Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_bUseMetric_SetBit(void* Obj)
	{
		((UVPUtilitiesEditorSettings*)Obj)->bUseMetric = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_bUseMetric = { "bUseMetric", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVPUtilitiesEditorSettings), &Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_bUseMetric_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_bUseMetric_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_bUseMetric_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_bUseTransformGizmo_MetaData[] = {
		{ "Category", "Virtual Production" },
		{ "Comment", "/** Whether to enable or disable the transform gizmo */" },
		{ "DisplayName", "Enable Transform Gizmo In VR" },
		{ "ModuleRelativePath", "Public/VPUtilitiesEditorSettings.h" },
		{ "ToolTip", "Whether to enable or disable the transform gizmo" },
	};
#endif
	void Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_bUseTransformGizmo_SetBit(void* Obj)
	{
		((UVPUtilitiesEditorSettings*)Obj)->bUseTransformGizmo = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_bUseTransformGizmo = { "bUseTransformGizmo", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVPUtilitiesEditorSettings), &Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_bUseTransformGizmo_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_bUseTransformGizmo_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_bUseTransformGizmo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_bUseGripInertiaDamping_MetaData[] = {
		{ "Category", "Virtual Production" },
		{ "Comment", "/** If true, the user will use inertia damping to stop after grip nav. Otherwise the user will just stop immediately */" },
		{ "DisplayName", "Use Grip Inertia Damping" },
		{ "ModuleRelativePath", "Public/VPUtilitiesEditorSettings.h" },
		{ "ToolTip", "If true, the user will use inertia damping to stop after grip nav. Otherwise the user will just stop immediately" },
	};
#endif
	void Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_bUseGripInertiaDamping_SetBit(void* Obj)
	{
		((UVPUtilitiesEditorSettings*)Obj)->bUseGripInertiaDamping = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_bUseGripInertiaDamping = { "bUseGripInertiaDamping", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVPUtilitiesEditorSettings), &Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_bUseGripInertiaDamping_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_bUseGripInertiaDamping_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_bUseGripInertiaDamping_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_InertiaDamping_MetaData[] = {
		{ "Category", "Virtual Production" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Damping applied to inertia */" },
		{ "DisplayName", "Inertia Damping" },
		{ "ModuleRelativePath", "Public/VPUtilitiesEditorSettings.h" },
		{ "ToolTip", "Damping applied to inertia" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_InertiaDamping = { "InertiaDamping", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVPUtilitiesEditorSettings, InertiaDamping), METADATA_PARAMS(Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_InertiaDamping_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_InertiaDamping_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_bIsHelperSystemEnabled_MetaData[] = {
		{ "Category", "Virtual Production" },
		{ "Comment", "/** Whether the helper system on the controllers is enabled */" },
		{ "DisplayName", "Helper System Enabled" },
		{ "ModuleRelativePath", "Public/VPUtilitiesEditorSettings.h" },
		{ "ToolTip", "Whether the helper system on the controllers is enabled" },
	};
#endif
	void Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_bIsHelperSystemEnabled_SetBit(void* Obj)
	{
		((UVPUtilitiesEditorSettings*)Obj)->bIsHelperSystemEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_bIsHelperSystemEnabled = { "bIsHelperSystemEnabled", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVPUtilitiesEditorSettings), &Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_bIsHelperSystemEnabled_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_bIsHelperSystemEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_bIsHelperSystemEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_bStartOSCServerAtLaunch_MetaData[] = {
		{ "Category", "OSC" },
		{ "Comment", "/** When enabled, an OSC server will automatically start on launch. */" },
		{ "DisplayName", "Start an OSC Server when the editor launches" },
		{ "ModuleRelativePath", "Public/VPUtilitiesEditorSettings.h" },
		{ "ToolTip", "When enabled, an OSC server will automatically start on launch." },
	};
#endif
	void Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_bStartOSCServerAtLaunch_SetBit(void* Obj)
	{
		((UVPUtilitiesEditorSettings*)Obj)->bStartOSCServerAtLaunch = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_bStartOSCServerAtLaunch = { "bStartOSCServerAtLaunch", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVPUtilitiesEditorSettings), &Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_bStartOSCServerAtLaunch_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_bStartOSCServerAtLaunch_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_bStartOSCServerAtLaunch_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_OSCServerAddress_MetaData[] = {
		{ "Category", "OSC" },
		{ "Comment", "/** The OSC server's address. */" },
		{ "DisplayName", "OSC Server Address" },
		{ "ModuleRelativePath", "Public/VPUtilitiesEditorSettings.h" },
		{ "ToolTip", "The OSC server's address." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_OSCServerAddress = { "OSCServerAddress", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVPUtilitiesEditorSettings, OSCServerAddress), METADATA_PARAMS(Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_OSCServerAddress_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_OSCServerAddress_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_OSCServerPort_MetaData[] = {
		{ "Category", "OSC" },
		{ "Comment", "/** The OSC server's port. */" },
		{ "DisplayName", "OSC Server Port" },
		{ "ModuleRelativePath", "Public/VPUtilitiesEditorSettings.h" },
		{ "ToolTip", "The OSC server's port." },
	};
#endif
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_OSCServerPort = { "OSCServerPort", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVPUtilitiesEditorSettings, OSCServerPort), METADATA_PARAMS(Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_OSCServerPort_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_OSCServerPort_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_StartupOSCListeners_Inner = { "StartupOSCListeners", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_StartupOSCListeners_MetaData[] = {
		{ "AllowedClasses", "EditorUtilityBlueprint" },
		{ "Category", "OSC" },
		{ "Comment", "/** What EditorUtilityObject should be ran on editor launch. */" },
		{ "DisplayName", "OSC Listeners" },
		{ "ModuleRelativePath", "Public/VPUtilitiesEditorSettings.h" },
		{ "ToolTip", "What EditorUtilityObject should be ran on editor launch." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_StartupOSCListeners = { "StartupOSCListeners", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVPUtilitiesEditorSettings, StartupOSCListeners), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_StartupOSCListeners_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_StartupOSCListeners_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_ScoutingSubsystemEditorUtilityClassPath_MetaData[] = {
		{ "Comment", "/** ScoutingSubsystem class to use for Blueprint helpers */" },
		{ "MetaClass", "VPScoutingSubsystemHelpersBase" },
		{ "ModuleRelativePath", "Public/VPUtilitiesEditorSettings.h" },
		{ "ToolTip", "ScoutingSubsystem class to use for Blueprint helpers" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_ScoutingSubsystemEditorUtilityClassPath = { "ScoutingSubsystemEditorUtilityClassPath", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVPUtilitiesEditorSettings, ScoutingSubsystemEditorUtilityClassPath), Z_Construct_UScriptStruct_FSoftClassPath, METADATA_PARAMS(Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_ScoutingSubsystemEditorUtilityClassPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_ScoutingSubsystemEditorUtilityClassPath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_GestureManagerEditorUtilityClassPath_MetaData[] = {
		{ "Comment", "/** GestureManager class to use by the ScoutingSubsystem */" },
		{ "MetaClass", "VPScoutingSubsystemGestureManagerBase" },
		{ "ModuleRelativePath", "Public/VPUtilitiesEditorSettings.h" },
		{ "ToolTip", "GestureManager class to use by the ScoutingSubsystem" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_GestureManagerEditorUtilityClassPath = { "GestureManagerEditorUtilityClassPath", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVPUtilitiesEditorSettings, GestureManagerEditorUtilityClassPath), Z_Construct_UScriptStruct_FSoftClassPath, METADATA_PARAMS(Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_GestureManagerEditorUtilityClassPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_GestureManagerEditorUtilityClassPath_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_AdditionnalClassToLoad_Inner = { "AdditionnalClassToLoad", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSoftClassPath, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_AdditionnalClassToLoad_MetaData[] = {
		{ "Comment", "/** GestureManager class to use by the ScoutingSubsystem */" },
		{ "ModuleRelativePath", "Public/VPUtilitiesEditorSettings.h" },
		{ "ToolTip", "GestureManager class to use by the ScoutingSubsystem" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_AdditionnalClassToLoad = { "AdditionnalClassToLoad", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVPUtilitiesEditorSettings, AdditionnalClassToLoad), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_AdditionnalClassToLoad_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_AdditionnalClassToLoad_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_VirtualScoutingUI,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_FlightSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_GripNavSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_bUseMetric,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_bUseTransformGizmo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_bUseGripInertiaDamping,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_InertiaDamping,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_bIsHelperSystemEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_bStartOSCServerAtLaunch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_OSCServerAddress,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_OSCServerPort,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_StartupOSCListeners_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_StartupOSCListeners,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_ScoutingSubsystemEditorUtilityClassPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_GestureManagerEditorUtilityClassPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_AdditionnalClassToLoad_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::NewProp_AdditionnalClassToLoad,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVPUtilitiesEditorSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::ClassParams = {
		&UVPUtilitiesEditorSettings::StaticClass,
		"VirtualProductionUtilities",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::PropPointers),
		0,
		0x001000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVPUtilitiesEditorSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVPUtilitiesEditorSettings, 814365439);
	template<> VPUTILITIESEDITOR_API UClass* StaticClass<UVPUtilitiesEditorSettings>()
	{
		return UVPUtilitiesEditorSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVPUtilitiesEditorSettings(Z_Construct_UClass_UVPUtilitiesEditorSettings, &UVPUtilitiesEditorSettings::StaticClass, TEXT("/Script/VPUtilitiesEditor"), TEXT("UVPUtilitiesEditorSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVPUtilitiesEditorSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
