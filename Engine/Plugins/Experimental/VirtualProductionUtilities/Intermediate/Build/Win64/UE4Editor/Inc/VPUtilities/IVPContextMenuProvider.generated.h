// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef VPUTILITIES_IVPContextMenuProvider_generated_h
#error "IVPContextMenuProvider.generated.h already included, missing '#pragma once' in IVPContextMenuProvider.h"
#endif
#define VPUTILITIES_IVPContextMenuProvider_generated_h

#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_IVPContextMenuProvider_h_16_SPARSE_DATA
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_IVPContextMenuProvider_h_16_RPC_WRAPPERS \
	virtual void OnCreateContextMenu_Implementation() {}; \
 \
	DECLARE_FUNCTION(execOnCreateContextMenu);


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_IVPContextMenuProvider_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual void OnCreateContextMenu_Implementation() {}; \
 \
	DECLARE_FUNCTION(execOnCreateContextMenu);


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_IVPContextMenuProvider_h_16_EVENT_PARMS
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_IVPContextMenuProvider_h_16_CALLBACK_WRAPPERS
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_IVPContextMenuProvider_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVPContextMenuProvider(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVPContextMenuProvider) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVPContextMenuProvider); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVPContextMenuProvider); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVPContextMenuProvider(UVPContextMenuProvider&&); \
	NO_API UVPContextMenuProvider(const UVPContextMenuProvider&); \
public:


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_IVPContextMenuProvider_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVPContextMenuProvider(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVPContextMenuProvider(UVPContextMenuProvider&&); \
	NO_API UVPContextMenuProvider(const UVPContextMenuProvider&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVPContextMenuProvider); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVPContextMenuProvider); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVPContextMenuProvider)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_IVPContextMenuProvider_h_16_GENERATED_UINTERFACE_BODY() \
private: \
	static void StaticRegisterNativesUVPContextMenuProvider(); \
	friend struct Z_Construct_UClass_UVPContextMenuProvider_Statics; \
public: \
	DECLARE_CLASS(UVPContextMenuProvider, UInterface, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), CASTCLASS_None, TEXT("/Script/VPUtilities"), NO_API) \
	DECLARE_SERIALIZER(UVPContextMenuProvider)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_IVPContextMenuProvider_h_16_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_IVPContextMenuProvider_h_16_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_IVPContextMenuProvider_h_16_STANDARD_CONSTRUCTORS \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_IVPContextMenuProvider_h_16_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_IVPContextMenuProvider_h_16_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_IVPContextMenuProvider_h_16_ENHANCED_CONSTRUCTORS \
private: \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_IVPContextMenuProvider_h_16_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~IVPContextMenuProvider() {} \
public: \
	typedef UVPContextMenuProvider UClassType; \
	typedef IVPContextMenuProvider ThisClass; \
	static void Execute_OnCreateContextMenu(UObject* O); \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_IVPContextMenuProvider_h_16_INCLASS_IINTERFACE \
protected: \
	virtual ~IVPContextMenuProvider() {} \
public: \
	typedef UVPContextMenuProvider UClassType; \
	typedef IVPContextMenuProvider ThisClass; \
	static void Execute_OnCreateContextMenu(UObject* O); \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_IVPContextMenuProvider_h_13_PROLOG \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_IVPContextMenuProvider_h_16_EVENT_PARMS


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_IVPContextMenuProvider_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_IVPContextMenuProvider_h_16_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_IVPContextMenuProvider_h_16_RPC_WRAPPERS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_IVPContextMenuProvider_h_16_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_IVPContextMenuProvider_h_16_INCLASS_IINTERFACE \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_IVPContextMenuProvider_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_IVPContextMenuProvider_h_16_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_IVPContextMenuProvider_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_IVPContextMenuProvider_h_16_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_IVPContextMenuProvider_h_16_INCLASS_IINTERFACE_NO_PURE_DECLS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VPUTILITIES_API UClass* StaticClass<class UVPContextMenuProvider>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_IVPContextMenuProvider_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
