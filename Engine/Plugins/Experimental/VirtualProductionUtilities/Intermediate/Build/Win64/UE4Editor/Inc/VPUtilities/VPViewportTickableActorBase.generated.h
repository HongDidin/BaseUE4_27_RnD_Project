// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef VPUTILITIES_VPViewportTickableActorBase_generated_h
#error "VPViewportTickableActorBase.generated.h already included, missing '#pragma once' in VPViewportTickableActorBase.h"
#endif
#define VPUTILITIES_VPViewportTickableActorBase_generated_h

#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPViewportTickableActorBase_h_27_SPARSE_DATA
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPViewportTickableActorBase_h_27_RPC_WRAPPERS \
	virtual void EditorDestroyed_Implementation(); \
	virtual void EditorTick_Implementation(float DeltaSeconds); \
 \
	DECLARE_FUNCTION(execEditorLockLocation); \
	DECLARE_FUNCTION(execEditorDestroyed); \
	DECLARE_FUNCTION(execEditorTick);


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPViewportTickableActorBase_h_27_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual void EditorDestroyed_Implementation(); \
	virtual void EditorTick_Implementation(float DeltaSeconds); \
 \
	DECLARE_FUNCTION(execEditorLockLocation); \
	DECLARE_FUNCTION(execEditorDestroyed); \
	DECLARE_FUNCTION(execEditorTick);


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPViewportTickableActorBase_h_27_EVENT_PARMS \
	struct VPViewportTickableActorBase_eventEditorTick_Parms \
	{ \
		float DeltaSeconds; \
	};


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPViewportTickableActorBase_h_27_CALLBACK_WRAPPERS
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPViewportTickableActorBase_h_27_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAVPViewportTickableActorBase(); \
	friend struct Z_Construct_UClass_AVPViewportTickableActorBase_Statics; \
public: \
	DECLARE_CLASS(AVPViewportTickableActorBase, AActor, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Config), CASTCLASS_None, TEXT("/Script/VPUtilities"), NO_API) \
	DECLARE_SERIALIZER(AVPViewportTickableActorBase)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPViewportTickableActorBase_h_27_INCLASS \
private: \
	static void StaticRegisterNativesAVPViewportTickableActorBase(); \
	friend struct Z_Construct_UClass_AVPViewportTickableActorBase_Statics; \
public: \
	DECLARE_CLASS(AVPViewportTickableActorBase, AActor, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Config), CASTCLASS_None, TEXT("/Script/VPUtilities"), NO_API) \
	DECLARE_SERIALIZER(AVPViewportTickableActorBase)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPViewportTickableActorBase_h_27_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AVPViewportTickableActorBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AVPViewportTickableActorBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AVPViewportTickableActorBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AVPViewportTickableActorBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AVPViewportTickableActorBase(AVPViewportTickableActorBase&&); \
	NO_API AVPViewportTickableActorBase(const AVPViewportTickableActorBase&); \
public:


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPViewportTickableActorBase_h_27_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AVPViewportTickableActorBase(AVPViewportTickableActorBase&&); \
	NO_API AVPViewportTickableActorBase(const AVPViewportTickableActorBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AVPViewportTickableActorBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AVPViewportTickableActorBase); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AVPViewportTickableActorBase)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPViewportTickableActorBase_h_27_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPViewportTickableActorBase_h_24_PROLOG \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPViewportTickableActorBase_h_27_EVENT_PARMS


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPViewportTickableActorBase_h_27_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPViewportTickableActorBase_h_27_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPViewportTickableActorBase_h_27_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPViewportTickableActorBase_h_27_RPC_WRAPPERS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPViewportTickableActorBase_h_27_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPViewportTickableActorBase_h_27_INCLASS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPViewportTickableActorBase_h_27_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPViewportTickableActorBase_h_27_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPViewportTickableActorBase_h_27_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPViewportTickableActorBase_h_27_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPViewportTickableActorBase_h_27_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPViewportTickableActorBase_h_27_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPViewportTickableActorBase_h_27_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPViewportTickableActorBase_h_27_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VPUTILITIES_API UClass* StaticClass<class AVPViewportTickableActorBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPViewportTickableActorBase_h


#define FOREACH_ENUM_EVPVIEWPORTTICKABLEFLAGS(op) \
	op(EVPViewportTickableFlags::Editor) \
	op(EVPViewportTickableFlags::Game) \
	op(EVPViewportTickableFlags::EditorPreview) \
	op(EVPViewportTickableFlags::GamePreview) 

enum class EVPViewportTickableFlags : uint8;
template<> VPUTILITIES_API UEnum* StaticEnum<EVPViewportTickableFlags>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
