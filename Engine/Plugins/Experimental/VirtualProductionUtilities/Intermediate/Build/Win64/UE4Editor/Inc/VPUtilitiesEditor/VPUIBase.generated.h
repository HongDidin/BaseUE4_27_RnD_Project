// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UVPBookmark;
#ifdef VPUTILITIESEDITOR_VPUIBase_generated_h
#error "VPUIBase.generated.h already included, missing '#pragma once' in VPUIBase.h"
#endif
#define VPUTILITIESEDITOR_VPUIBase_generated_h

#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPUIBase_h_20_SPARSE_DATA
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPUIBase_h_20_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetLastVirtualProductionLogMessage); \
	DECLARE_FUNCTION(execAppendVirtualProductionLog);


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPUIBase_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetLastVirtualProductionLogMessage); \
	DECLARE_FUNCTION(execAppendVirtualProductionLog);


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPUIBase_h_20_EVENT_PARMS \
	struct VPUIBase_eventOnBookmarkCleared_Parms \
	{ \
		UVPBookmark* Cleared; \
	}; \
	struct VPUIBase_eventOnBookmarkCreated_Parms \
	{ \
		UVPBookmark* Created; \
	}; \
	struct VPUIBase_eventOnBookmarkDestroyed_Parms \
	{ \
		UVPBookmark* Destroyed; \
	}; \
	struct VPUIBase_eventOnFlightModeChanged_Parms \
	{ \
		bool WasEntered; \
	};


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPUIBase_h_20_CALLBACK_WRAPPERS
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPUIBase_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVPUIBase(); \
	friend struct Z_Construct_UClass_UVPUIBase_Statics; \
public: \
	DECLARE_CLASS(UVPUIBase, UEditorUtilityWidget, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/VPUtilitiesEditor"), NO_API) \
	DECLARE_SERIALIZER(UVPUIBase)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPUIBase_h_20_INCLASS \
private: \
	static void StaticRegisterNativesUVPUIBase(); \
	friend struct Z_Construct_UClass_UVPUIBase_Statics; \
public: \
	DECLARE_CLASS(UVPUIBase, UEditorUtilityWidget, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/VPUtilitiesEditor"), NO_API) \
	DECLARE_SERIALIZER(UVPUIBase)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPUIBase_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVPUIBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVPUIBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVPUIBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVPUIBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVPUIBase(UVPUIBase&&); \
	NO_API UVPUIBase(const UVPUIBase&); \
public:


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPUIBase_h_20_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVPUIBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVPUIBase(UVPUIBase&&); \
	NO_API UVPUIBase(const UVPUIBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVPUIBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVPUIBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVPUIBase)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPUIBase_h_20_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__VirtualProductionLog() { return STRUCT_OFFSET(UVPUIBase, VirtualProductionLog); }


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPUIBase_h_17_PROLOG \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPUIBase_h_20_EVENT_PARMS


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPUIBase_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPUIBase_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPUIBase_h_20_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPUIBase_h_20_RPC_WRAPPERS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPUIBase_h_20_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPUIBase_h_20_INCLASS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPUIBase_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPUIBase_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPUIBase_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPUIBase_h_20_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPUIBase_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPUIBase_h_20_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPUIBase_h_20_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPUIBase_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VPUTILITIESEDITOR_API UClass* StaticClass<class UVPUIBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPUIBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
