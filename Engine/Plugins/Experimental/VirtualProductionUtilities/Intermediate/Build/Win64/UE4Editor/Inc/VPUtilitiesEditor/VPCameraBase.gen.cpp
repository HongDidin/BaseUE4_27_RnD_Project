// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VPUtilitiesEditor/Private/VPCameraBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVPCameraBase() {}
// Cross Module References
	VPUTILITIESEDITOR_API UClass* Z_Construct_UClass_AVPCameraBase_NoRegister();
	VPUTILITIESEDITOR_API UClass* Z_Construct_UClass_AVPCameraBase();
	CINEMATICCAMERA_API UClass* Z_Construct_UClass_ACineCameraActor();
	UPackage* Z_Construct_UPackage__Script_VPUtilitiesEditor();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
// End Cross Module References
	DEFINE_FUNCTION(AVPCameraBase::execResetPreview)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ResetPreview();
		P_NATIVE_END;
	}
	void AVPCameraBase::StaticRegisterNativesAVPCameraBase()
	{
		UClass* Class = AVPCameraBase::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ResetPreview", &AVPCameraBase::execResetPreview },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AVPCameraBase_ResetPreview_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AVPCameraBase_ResetPreview_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "Editor Scripting | Camera" },
		{ "Comment", "/** Remove the preview and clear the list of selected user. Another user may have the camera selected and will re-add it later. */" },
		{ "ModuleRelativePath", "Private/VPCameraBase.h" },
		{ "ToolTip", "Remove the preview and clear the list of selected user. Another user may have the camera selected and will re-add it later." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AVPCameraBase_ResetPreview_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AVPCameraBase, nullptr, "ResetPreview", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AVPCameraBase_ResetPreview_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AVPCameraBase_ResetPreview_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AVPCameraBase_ResetPreview()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AVPCameraBase_ResetPreview_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AVPCameraBase_NoRegister()
	{
		return AVPCameraBase::StaticClass();
	}
	struct Z_Construct_UClass_AVPCameraBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SelectedByUsers_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SelectedByUsers_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SelectedByUsers;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AVPCameraBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ACineCameraActor,
		(UObject* (*)())Z_Construct_UPackage__Script_VPUtilitiesEditor,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AVPCameraBase_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AVPCameraBase_ResetPreview, "ResetPreview" }, // 514664601
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVPCameraBase_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Input Rendering AutoPlayerActivation Input Rendering" },
		{ "IncludePath", "VPCameraBase.h" },
		{ "ModuleRelativePath", "Private/VPCameraBase.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AVPCameraBase_Statics::NewProp_SelectedByUsers_Inner = { "SelectedByUsers", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVPCameraBase_Statics::NewProp_SelectedByUsers_MetaData[] = {
		{ "Comment", "/** Contains the name of users in an MU session that have selected this */" },
		{ "ModuleRelativePath", "Private/VPCameraBase.h" },
		{ "ToolTip", "Contains the name of users in an MU session that have selected this" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AVPCameraBase_Statics::NewProp_SelectedByUsers = { "SelectedByUsers", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVPCameraBase, SelectedByUsers), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AVPCameraBase_Statics::NewProp_SelectedByUsers_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVPCameraBase_Statics::NewProp_SelectedByUsers_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AVPCameraBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVPCameraBase_Statics::NewProp_SelectedByUsers_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVPCameraBase_Statics::NewProp_SelectedByUsers,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AVPCameraBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AVPCameraBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AVPCameraBase_Statics::ClassParams = {
		&AVPCameraBase::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AVPCameraBase_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AVPCameraBase_Statics::PropPointers),
		0,
		0x008000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AVPCameraBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AVPCameraBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AVPCameraBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AVPCameraBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AVPCameraBase, 2279467714);
	template<> VPUTILITIESEDITOR_API UClass* StaticClass<AVPCameraBase>()
	{
		return AVPCameraBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AVPCameraBase(Z_Construct_UClass_AVPCameraBase, &AVPCameraBase::StaticClass, TEXT("/Script/VPUtilitiesEditor"), TEXT("AVPCameraBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AVPCameraBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
