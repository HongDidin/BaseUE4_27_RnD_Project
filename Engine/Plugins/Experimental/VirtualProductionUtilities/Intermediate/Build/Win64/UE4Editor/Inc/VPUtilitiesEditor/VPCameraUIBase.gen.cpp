// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VPUtilitiesEditor/Private/VPCameraUIBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVPCameraUIBase() {}
// Cross Module References
	VPUTILITIESEDITOR_API UClass* Z_Construct_UClass_UVPCameraUIBase_NoRegister();
	VPUTILITIESEDITOR_API UClass* Z_Construct_UClass_UVPCameraUIBase();
	UMG_API UClass* Z_Construct_UClass_UUserWidget();
	UPackage* Z_Construct_UPackage__Script_VPUtilitiesEditor();
	ENGINE_API UClass* Z_Construct_UClass_ACameraActor_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
// End Cross Module References
	static FName NAME_UVPCameraUIBase_OnSelectedCameraChanged = FName(TEXT("OnSelectedCameraChanged"));
	void UVPCameraUIBase::OnSelectedCameraChanged()
	{
		ProcessEvent(FindFunctionChecked(NAME_UVPCameraUIBase_OnSelectedCameraChanged),NULL);
	}
	void UVPCameraUIBase::StaticRegisterNativesUVPCameraUIBase()
	{
	}
	struct Z_Construct_UFunction_UVPCameraUIBase_OnSelectedCameraChanged_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPCameraUIBase_OnSelectedCameraChanged_Statics::Function_MetaDataParams[] = {
		{ "Category", "Camera" },
		{ "ModuleRelativePath", "Private/VPCameraUIBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVPCameraUIBase_OnSelectedCameraChanged_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVPCameraUIBase, nullptr, "OnSelectedCameraChanged", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08080800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVPCameraUIBase_OnSelectedCameraChanged_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPCameraUIBase_OnSelectedCameraChanged_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVPCameraUIBase_OnSelectedCameraChanged()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVPCameraUIBase_OnSelectedCameraChanged_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UVPCameraUIBase_NoRegister()
	{
		return UVPCameraUIBase::StaticClass();
	}
	struct Z_Construct_UClass_UVPCameraUIBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SelectedCamera_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SelectedCamera;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SelectedCameraComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SelectedCameraComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVPCameraUIBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UUserWidget,
		(UObject* (*)())Z_Construct_UPackage__Script_VPUtilitiesEditor,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UVPCameraUIBase_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UVPCameraUIBase_OnSelectedCameraChanged, "OnSelectedCameraChanged" }, // 2958115090
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVPCameraUIBase_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "VPCameraUIBase.h" },
		{ "ModuleRelativePath", "Private/VPCameraUIBase.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVPCameraUIBase_Statics::NewProp_SelectedCamera_MetaData[] = {
		{ "ModuleRelativePath", "Private/VPCameraUIBase.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UVPCameraUIBase_Statics::NewProp_SelectedCamera = { "SelectedCamera", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVPCameraUIBase, SelectedCamera), Z_Construct_UClass_ACameraActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVPCameraUIBase_Statics::NewProp_SelectedCamera_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVPCameraUIBase_Statics::NewProp_SelectedCamera_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVPCameraUIBase_Statics::NewProp_SelectedCameraComponent_MetaData[] = {
		{ "Category", "Camera" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Private/VPCameraUIBase.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UVPCameraUIBase_Statics::NewProp_SelectedCameraComponent = { "SelectedCameraComponent", nullptr, (EPropertyFlags)0x002008000008201c, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVPCameraUIBase, SelectedCameraComponent), Z_Construct_UClass_UCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVPCameraUIBase_Statics::NewProp_SelectedCameraComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVPCameraUIBase_Statics::NewProp_SelectedCameraComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UVPCameraUIBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVPCameraUIBase_Statics::NewProp_SelectedCamera,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVPCameraUIBase_Statics::NewProp_SelectedCameraComponent,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVPCameraUIBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVPCameraUIBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVPCameraUIBase_Statics::ClassParams = {
		&UVPCameraUIBase::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UVPCameraUIBase_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UVPCameraUIBase_Statics::PropPointers),
		0,
		0x00A010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UVPCameraUIBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVPCameraUIBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVPCameraUIBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVPCameraUIBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVPCameraUIBase, 1294668763);
	template<> VPUTILITIESEDITOR_API UClass* StaticClass<UVPCameraUIBase>()
	{
		return UVPCameraUIBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVPCameraUIBase(Z_Construct_UClass_UVPCameraUIBase, &UVPCameraUIBase::StaticClass, TEXT("/Script/VPUtilitiesEditor"), TEXT("UVPCameraUIBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVPCameraUIBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
