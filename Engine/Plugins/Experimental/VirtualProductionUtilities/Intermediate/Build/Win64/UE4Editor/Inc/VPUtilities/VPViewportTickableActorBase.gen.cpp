// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VPUtilities/Public/VPViewportTickableActorBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVPViewportTickableActorBase() {}
// Cross Module References
	VPUTILITIES_API UEnum* Z_Construct_UEnum_VPUtilities_EVPViewportTickableFlags();
	UPackage* Z_Construct_UPackage__Script_VPUtilities();
	VPUTILITIES_API UClass* Z_Construct_UClass_AVPViewportTickableActorBase_NoRegister();
	VPUTILITIES_API UClass* Z_Construct_UClass_AVPViewportTickableActorBase();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
// End Cross Module References
	static UEnum* EVPViewportTickableFlags_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_VPUtilities_EVPViewportTickableFlags, Z_Construct_UPackage__Script_VPUtilities(), TEXT("EVPViewportTickableFlags"));
		}
		return Singleton;
	}
	template<> VPUTILITIES_API UEnum* StaticEnum<EVPViewportTickableFlags>()
	{
		return EVPViewportTickableFlags_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EVPViewportTickableFlags(EVPViewportTickableFlags_StaticEnum, TEXT("/Script/VPUtilities"), TEXT("EVPViewportTickableFlags"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_VPUtilities_EVPViewportTickableFlags_Hash() { return 1669664121U; }
	UEnum* Z_Construct_UEnum_VPUtilities_EVPViewportTickableFlags()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_VPUtilities();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EVPViewportTickableFlags"), 0, Get_Z_Construct_UEnum_VPUtilities_EVPViewportTickableFlags_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EVPViewportTickableFlags::Editor", (int64)EVPViewportTickableFlags::Editor },
				{ "EVPViewportTickableFlags::Game", (int64)EVPViewportTickableFlags::Game },
				{ "EVPViewportTickableFlags::EditorPreview", (int64)EVPViewportTickableFlags::EditorPreview },
				{ "EVPViewportTickableFlags::GamePreview", (int64)EVPViewportTickableFlags::GamePreview },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Bitflags", "" },
				{ "Editor.Name", "EVPViewportTickableFlags::Editor" },
				{ "EditorPreview.Name", "EVPViewportTickableFlags::EditorPreview" },
				{ "Game.Name", "EVPViewportTickableFlags::Game" },
				{ "GamePreview.Name", "EVPViewportTickableFlags::GamePreview" },
				{ "ModuleRelativePath", "Public/VPViewportTickableActorBase.h" },
				{ "UseEnumValuesAsMaskValuesInEditor", "true" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_VPUtilities,
				nullptr,
				"EVPViewportTickableFlags",
				"EVPViewportTickableFlags",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	DEFINE_FUNCTION(AVPViewportTickableActorBase::execEditorLockLocation)
	{
		P_GET_UBOOL(Z_Param_bSetLockLocation);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->EditorLockLocation(Z_Param_bSetLockLocation);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AVPViewportTickableActorBase::execEditorDestroyed)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->EditorDestroyed_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AVPViewportTickableActorBase::execEditorTick)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_DeltaSeconds);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->EditorTick_Implementation(Z_Param_DeltaSeconds);
		P_NATIVE_END;
	}
	static FName NAME_AVPViewportTickableActorBase_EditorDestroyed = FName(TEXT("EditorDestroyed"));
	void AVPViewportTickableActorBase::EditorDestroyed()
	{
		ProcessEvent(FindFunctionChecked(NAME_AVPViewportTickableActorBase_EditorDestroyed),NULL);
	}
	static FName NAME_AVPViewportTickableActorBase_EditorTick = FName(TEXT("EditorTick"));
	void AVPViewportTickableActorBase::EditorTick(float DeltaSeconds)
	{
		VPViewportTickableActorBase_eventEditorTick_Parms Parms;
		Parms.DeltaSeconds=DeltaSeconds;
		ProcessEvent(FindFunctionChecked(NAME_AVPViewportTickableActorBase_EditorTick),&Parms);
	}
	void AVPViewportTickableActorBase::StaticRegisterNativesAVPViewportTickableActorBase()
	{
		UClass* Class = AVPViewportTickableActorBase::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "EditorDestroyed", &AVPViewportTickableActorBase::execEditorDestroyed },
			{ "EditorLockLocation", &AVPViewportTickableActorBase::execEditorLockLocation },
			{ "EditorTick", &AVPViewportTickableActorBase::execEditorTick },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AVPViewportTickableActorBase_EditorDestroyed_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AVPViewportTickableActorBase_EditorDestroyed_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "Utilities" },
		{ "ModuleRelativePath", "Public/VPViewportTickableActorBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AVPViewportTickableActorBase_EditorDestroyed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AVPViewportTickableActorBase, nullptr, "EditorDestroyed", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AVPViewportTickableActorBase_EditorDestroyed_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AVPViewportTickableActorBase_EditorDestroyed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AVPViewportTickableActorBase_EditorDestroyed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AVPViewportTickableActorBase_EditorDestroyed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AVPViewportTickableActorBase_EditorLockLocation_Statics
	{
		struct VPViewportTickableActorBase_eventEditorLockLocation_Parms
		{
			bool bSetLockLocation;
		};
		static void NewProp_bSetLockLocation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSetLockLocation;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AVPViewportTickableActorBase_EditorLockLocation_Statics::NewProp_bSetLockLocation_SetBit(void* Obj)
	{
		((VPViewportTickableActorBase_eventEditorLockLocation_Parms*)Obj)->bSetLockLocation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AVPViewportTickableActorBase_EditorLockLocation_Statics::NewProp_bSetLockLocation = { "bSetLockLocation", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VPViewportTickableActorBase_eventEditorLockLocation_Parms), &Z_Construct_UFunction_AVPViewportTickableActorBase_EditorLockLocation_Statics::NewProp_bSetLockLocation_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AVPViewportTickableActorBase_EditorLockLocation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AVPViewportTickableActorBase_EditorLockLocation_Statics::NewProp_bSetLockLocation,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AVPViewportTickableActorBase_EditorLockLocation_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "Editor" },
		{ "Comment", "/** Sets the LockLocation variable to disable movement from the translation gizmo */" },
		{ "ModuleRelativePath", "Public/VPViewportTickableActorBase.h" },
		{ "ToolTip", "Sets the LockLocation variable to disable movement from the translation gizmo" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AVPViewportTickableActorBase_EditorLockLocation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AVPViewportTickableActorBase, nullptr, "EditorLockLocation", nullptr, nullptr, sizeof(VPViewportTickableActorBase_eventEditorLockLocation_Parms), Z_Construct_UFunction_AVPViewportTickableActorBase_EditorLockLocation_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AVPViewportTickableActorBase_EditorLockLocation_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AVPViewportTickableActorBase_EditorLockLocation_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AVPViewportTickableActorBase_EditorLockLocation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AVPViewportTickableActorBase_EditorLockLocation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AVPViewportTickableActorBase_EditorLockLocation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AVPViewportTickableActorBase_EditorTick_Statics
	{
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DeltaSeconds;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AVPViewportTickableActorBase_EditorTick_Statics::NewProp_DeltaSeconds = { "DeltaSeconds", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VPViewportTickableActorBase_eventEditorTick_Parms, DeltaSeconds), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AVPViewportTickableActorBase_EditorTick_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AVPViewportTickableActorBase_EditorTick_Statics::NewProp_DeltaSeconds,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AVPViewportTickableActorBase_EditorTick_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "Tick" },
		{ "ModuleRelativePath", "Public/VPViewportTickableActorBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AVPViewportTickableActorBase_EditorTick_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AVPViewportTickableActorBase, nullptr, "EditorTick", nullptr, nullptr, sizeof(VPViewportTickableActorBase_eventEditorTick_Parms), Z_Construct_UFunction_AVPViewportTickableActorBase_EditorTick_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AVPViewportTickableActorBase_EditorTick_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AVPViewportTickableActorBase_EditorTick_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AVPViewportTickableActorBase_EditorTick_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AVPViewportTickableActorBase_EditorTick()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AVPViewportTickableActorBase_EditorTick_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AVPViewportTickableActorBase_NoRegister()
	{
		return AVPViewportTickableActorBase::StaticClass();
	}
	struct Z_Construct_UClass_AVPViewportTickableActorBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ViewportTickType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewportTickType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ViewportTickType;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AVPViewportTickableActorBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_VPUtilities,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AVPViewportTickableActorBase_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AVPViewportTickableActorBase_EditorDestroyed, "EditorDestroyed" }, // 646619455
		{ &Z_Construct_UFunction_AVPViewportTickableActorBase_EditorLockLocation, "EditorLockLocation" }, // 266526499
		{ &Z_Construct_UFunction_AVPViewportTickableActorBase_EditorTick, "EditorTick" }, // 113056431
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVPViewportTickableActorBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Actor that tick in the Editor viewport with the event EditorTick.\n */" },
		{ "IncludePath", "VPViewportTickableActorBase.h" },
		{ "ModuleRelativePath", "Public/VPViewportTickableActorBase.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Actor that tick in the Editor viewport with the event EditorTick." },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_AVPViewportTickableActorBase_Statics::NewProp_ViewportTickType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVPViewportTickableActorBase_Statics::NewProp_ViewportTickType_MetaData[] = {
		{ "Bitmask", "" },
		{ "BitmaskEnum", "EVPViewportTickableType" },
		{ "Category", "Actor Tick" },
		{ "Comment", "/**\n\x09 * Where the actor should be ticked.\n\x09 * Editor = Tick in the editor viewport. Use the event EditorTick.\n\x09 * Game = Tick in game even if we are only ticking the viewport. Use the event Tick.\n\x09 * Preview = Tick if the actor is present in any editing tool like Blueprint or Material graph. Use EditorTick.\n\x09 */" },
		{ "ModuleRelativePath", "Public/VPViewportTickableActorBase.h" },
		{ "ToolTip", "Where the actor should be ticked.\nEditor = Tick in the editor viewport. Use the event EditorTick.\nGame = Tick in game even if we are only ticking the viewport. Use the event Tick.\nPreview = Tick if the actor is present in any editing tool like Blueprint or Material graph. Use EditorTick." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_AVPViewportTickableActorBase_Statics::NewProp_ViewportTickType = { "ViewportTickType", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVPViewportTickableActorBase, ViewportTickType), Z_Construct_UEnum_VPUtilities_EVPViewportTickableFlags, METADATA_PARAMS(Z_Construct_UClass_AVPViewportTickableActorBase_Statics::NewProp_ViewportTickType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVPViewportTickableActorBase_Statics::NewProp_ViewportTickType_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AVPViewportTickableActorBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVPViewportTickableActorBase_Statics::NewProp_ViewportTickType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVPViewportTickableActorBase_Statics::NewProp_ViewportTickType,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AVPViewportTickableActorBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AVPViewportTickableActorBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AVPViewportTickableActorBase_Statics::ClassParams = {
		&AVPViewportTickableActorBase::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AVPViewportTickableActorBase_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AVPViewportTickableActorBase_Statics::PropPointers),
		0,
		0x009000A5u,
		METADATA_PARAMS(Z_Construct_UClass_AVPViewportTickableActorBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AVPViewportTickableActorBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AVPViewportTickableActorBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AVPViewportTickableActorBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AVPViewportTickableActorBase, 2415718564);
	template<> VPUTILITIES_API UClass* StaticClass<AVPViewportTickableActorBase>()
	{
		return AVPViewportTickableActorBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AVPViewportTickableActorBase(Z_Construct_UClass_AVPViewportTickableActorBase, &AVPViewportTickableActorBase::StaticClass, TEXT("/Script/VPUtilities"), TEXT("AVPViewportTickableActorBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AVPViewportTickableActorBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
