// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef VPUTILITIESEDITOR_VPUtilitiesEditorSettings_generated_h
#error "VPUtilitiesEditorSettings.generated.h already included, missing '#pragma once' in VPUtilitiesEditorSettings.h"
#endif
#define VPUTILITIESEDITOR_VPUtilitiesEditorSettings_generated_h

#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorSettings_h_19_SPARSE_DATA
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorSettings_h_19_RPC_WRAPPERS
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorSettings_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorSettings_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVPUtilitiesEditorSettings(); \
	friend struct Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics; \
public: \
	DECLARE_CLASS(UVPUtilitiesEditorSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/VPUtilitiesEditor"), NO_API) \
	DECLARE_SERIALIZER(UVPUtilitiesEditorSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("VirtualProductionUtilities");} \



#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorSettings_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUVPUtilitiesEditorSettings(); \
	friend struct Z_Construct_UClass_UVPUtilitiesEditorSettings_Statics; \
public: \
	DECLARE_CLASS(UVPUtilitiesEditorSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/VPUtilitiesEditor"), NO_API) \
	DECLARE_SERIALIZER(UVPUtilitiesEditorSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("VirtualProductionUtilities");} \



#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorSettings_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVPUtilitiesEditorSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVPUtilitiesEditorSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVPUtilitiesEditorSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVPUtilitiesEditorSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVPUtilitiesEditorSettings(UVPUtilitiesEditorSettings&&); \
	NO_API UVPUtilitiesEditorSettings(const UVPUtilitiesEditorSettings&); \
public:


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorSettings_h_19_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVPUtilitiesEditorSettings(UVPUtilitiesEditorSettings&&); \
	NO_API UVPUtilitiesEditorSettings(const UVPUtilitiesEditorSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVPUtilitiesEditorSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVPUtilitiesEditorSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UVPUtilitiesEditorSettings)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorSettings_h_19_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorSettings_h_16_PROLOG
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorSettings_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorSettings_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorSettings_h_19_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorSettings_h_19_RPC_WRAPPERS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorSettings_h_19_INCLASS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorSettings_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorSettings_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorSettings_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorSettings_h_19_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorSettings_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorSettings_h_19_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorSettings_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VPUTILITIESEDITOR_API UClass* StaticClass<class UVPUtilitiesEditorSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
