// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VPUtilities/Public/VPFullScreenUserWidgetActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVPFullScreenUserWidgetActor() {}
// Cross Module References
	VPUTILITIES_API UClass* Z_Construct_UClass_AFullScreenUserWidgetActor_NoRegister();
	VPUTILITIES_API UClass* Z_Construct_UClass_AFullScreenUserWidgetActor();
	ENGINE_API UClass* Z_Construct_UClass_AInfo();
	UPackage* Z_Construct_UPackage__Script_VPUtilities();
	VPUTILITIES_API UClass* Z_Construct_UClass_UVPFullScreenUserWidget_NoRegister();
// End Cross Module References
	void AFullScreenUserWidgetActor::StaticRegisterNativesAFullScreenUserWidgetActor()
	{
	}
	UClass* Z_Construct_UClass_AFullScreenUserWidgetActor_NoRegister()
	{
		return AFullScreenUserWidgetActor::StaticClass();
	}
	struct Z_Construct_UClass_AFullScreenUserWidgetActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreenUserWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ScreenUserWidget;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AFullScreenUserWidgetActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AInfo,
		(UObject* (*)())Z_Construct_UPackage__Script_VPUtilities,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFullScreenUserWidgetActor_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "UserInterface" },
		{ "Comment", "/**\n * Widgets are first rendered to a render target, then that render target is displayed in the world.\n */" },
		{ "HideCategories", "Actor Input Movement Collision Rendering Utilities|Transformation LOD Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "VPFullScreenUserWidgetActor.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/VPFullScreenUserWidgetActor.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
		{ "ToolTip", "Widgets are first rendered to a render target, then that render target is displayed in the world." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFullScreenUserWidgetActor_Statics::NewProp_ScreenUserWidget_MetaData[] = {
		{ "Category", "User Interface" },
		{ "Comment", "/** */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/VPFullScreenUserWidgetActor.h" },
		{ "ShowOnlyInnerProperties", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AFullScreenUserWidgetActor_Statics::NewProp_ScreenUserWidget = { "ScreenUserWidget", nullptr, (EPropertyFlags)0x00220800020a0009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AFullScreenUserWidgetActor, ScreenUserWidget), Z_Construct_UClass_UVPFullScreenUserWidget_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AFullScreenUserWidgetActor_Statics::NewProp_ScreenUserWidget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AFullScreenUserWidgetActor_Statics::NewProp_ScreenUserWidget_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AFullScreenUserWidgetActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFullScreenUserWidgetActor_Statics::NewProp_ScreenUserWidget,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AFullScreenUserWidgetActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AFullScreenUserWidgetActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AFullScreenUserWidgetActor_Statics::ClassParams = {
		&AFullScreenUserWidgetActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AFullScreenUserWidgetActor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AFullScreenUserWidgetActor_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AFullScreenUserWidgetActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AFullScreenUserWidgetActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AFullScreenUserWidgetActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AFullScreenUserWidgetActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AFullScreenUserWidgetActor, 2275713838);
	template<> VPUTILITIES_API UClass* StaticClass<AFullScreenUserWidgetActor>()
	{
		return AFullScreenUserWidgetActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AFullScreenUserWidgetActor(Z_Construct_UClass_AFullScreenUserWidgetActor, &AFullScreenUserWidgetActor::StaticClass, TEXT("/Script/VPUtilities"), TEXT("AFullScreenUserWidgetActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AFullScreenUserWidgetActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
