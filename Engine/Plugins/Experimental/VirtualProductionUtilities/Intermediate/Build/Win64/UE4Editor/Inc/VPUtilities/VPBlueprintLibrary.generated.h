// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class USplineMeshComponent;
struct FVector;
struct FTransform;
struct FGameplayTagContainer;
class UVPBookmark;
class AActor;
struct FVPBookmarkCreationContext;
class UObject;
class AVPViewportTickableActorBase;
struct FRotator;
#ifdef VPUTILITIES_VPBlueprintLibrary_generated_h
#error "VPBlueprintLibrary.generated.h already included, missing '#pragma once' in VPBlueprintLibrary.h"
#endif
#define VPUTILITIES_VPBlueprintLibrary_generated_h

#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPBlueprintLibrary_h_21_SPARSE_DATA
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPBlueprintLibrary_h_21_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execVPBookmarkSplineMeshIndicatorDisable); \
	DECLARE_FUNCTION(execVPBookmarkSplineMeshIndicatorSetStartAndEnd); \
	DECLARE_FUNCTION(execEditorDeleteSelectedObjects); \
	DECLARE_FUNCTION(execEditorDuplicate); \
	DECLARE_FUNCTION(execEditorRedo); \
	DECLARE_FUNCTION(execEditorUndo); \
	DECLARE_FUNCTION(execGetVREditorLaserHoverLocation); \
	DECLARE_FUNCTION(execIsVREditorModeActive); \
	DECLARE_FUNCTION(execSetGrabSpeed); \
	DECLARE_FUNCTION(execGetEditorVRRoomTransform); \
	DECLARE_FUNCTION(execGetEditorVRHeadTransform); \
	DECLARE_FUNCTION(execGetEditorViewportTransform); \
	DECLARE_FUNCTION(execGetVirtualProductionRole); \
	DECLARE_FUNCTION(execJumpToBookmarkInLevelEditor); \
	DECLARE_FUNCTION(execSpawnBookmarkAtCurrentLevelEditorPosition); \
	DECLARE_FUNCTION(execSpawnVPTickableActor); \
	DECLARE_FUNCTION(execRefresh3DEditorViewport);


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPBlueprintLibrary_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execVPBookmarkSplineMeshIndicatorDisable); \
	DECLARE_FUNCTION(execVPBookmarkSplineMeshIndicatorSetStartAndEnd); \
	DECLARE_FUNCTION(execEditorDeleteSelectedObjects); \
	DECLARE_FUNCTION(execEditorDuplicate); \
	DECLARE_FUNCTION(execEditorRedo); \
	DECLARE_FUNCTION(execEditorUndo); \
	DECLARE_FUNCTION(execGetVREditorLaserHoverLocation); \
	DECLARE_FUNCTION(execIsVREditorModeActive); \
	DECLARE_FUNCTION(execSetGrabSpeed); \
	DECLARE_FUNCTION(execGetEditorVRRoomTransform); \
	DECLARE_FUNCTION(execGetEditorVRHeadTransform); \
	DECLARE_FUNCTION(execGetEditorViewportTransform); \
	DECLARE_FUNCTION(execGetVirtualProductionRole); \
	DECLARE_FUNCTION(execJumpToBookmarkInLevelEditor); \
	DECLARE_FUNCTION(execSpawnBookmarkAtCurrentLevelEditorPosition); \
	DECLARE_FUNCTION(execSpawnVPTickableActor); \
	DECLARE_FUNCTION(execRefresh3DEditorViewport);


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPBlueprintLibrary_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVPBlueprintLibrary(); \
	friend struct Z_Construct_UClass_UVPBlueprintLibrary_Statics; \
public: \
	DECLARE_CLASS(UVPBlueprintLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VPUtilities"), NO_API) \
	DECLARE_SERIALIZER(UVPBlueprintLibrary)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPBlueprintLibrary_h_21_INCLASS \
private: \
	static void StaticRegisterNativesUVPBlueprintLibrary(); \
	friend struct Z_Construct_UClass_UVPBlueprintLibrary_Statics; \
public: \
	DECLARE_CLASS(UVPBlueprintLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VPUtilities"), NO_API) \
	DECLARE_SERIALIZER(UVPBlueprintLibrary)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPBlueprintLibrary_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVPBlueprintLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVPBlueprintLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVPBlueprintLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVPBlueprintLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVPBlueprintLibrary(UVPBlueprintLibrary&&); \
	NO_API UVPBlueprintLibrary(const UVPBlueprintLibrary&); \
public:


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPBlueprintLibrary_h_21_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVPBlueprintLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVPBlueprintLibrary(UVPBlueprintLibrary&&); \
	NO_API UVPBlueprintLibrary(const UVPBlueprintLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVPBlueprintLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVPBlueprintLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVPBlueprintLibrary)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPBlueprintLibrary_h_21_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPBlueprintLibrary_h_18_PROLOG
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPBlueprintLibrary_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPBlueprintLibrary_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPBlueprintLibrary_h_21_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPBlueprintLibrary_h_21_RPC_WRAPPERS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPBlueprintLibrary_h_21_INCLASS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPBlueprintLibrary_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPBlueprintLibrary_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPBlueprintLibrary_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPBlueprintLibrary_h_21_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPBlueprintLibrary_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPBlueprintLibrary_h_21_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPBlueprintLibrary_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VPUTILITIES_API UClass* StaticClass<class UVPBlueprintLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPBlueprintLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
