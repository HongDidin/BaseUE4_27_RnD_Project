// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VPUtilitiesEditor/Public/VPTransientEditorTickableActorBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVPTransientEditorTickableActorBase() {}
// Cross Module References
	VPUTILITIESEDITOR_API UClass* Z_Construct_UClass_AVPTransientEditorTickableActorBase_NoRegister();
	VPUTILITIESEDITOR_API UClass* Z_Construct_UClass_AVPTransientEditorTickableActorBase();
	VPUTILITIESEDITOR_API UClass* Z_Construct_UClass_AVPEditorTickableActorBase();
	UPackage* Z_Construct_UPackage__Script_VPUtilitiesEditor();
// End Cross Module References
	void AVPTransientEditorTickableActorBase::StaticRegisterNativesAVPTransientEditorTickableActorBase()
	{
	}
	UClass* Z_Construct_UClass_AVPTransientEditorTickableActorBase_NoRegister()
	{
		return AVPTransientEditorTickableActorBase::StaticClass();
	}
	struct Z_Construct_UClass_AVPTransientEditorTickableActorBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AVPTransientEditorTickableActorBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AVPEditorTickableActorBase,
		(UObject* (*)())Z_Construct_UPackage__Script_VPUtilitiesEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVPTransientEditorTickableActorBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Specific VPEditorTickableActor explicitely marked as Transient\n */" },
		{ "IncludePath", "VPTransientEditorTickableActorBase.h" },
		{ "ModuleRelativePath", "Public/VPTransientEditorTickableActorBase.h" },
		{ "ToolTip", "Specific VPEditorTickableActor explicitely marked as Transient" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AVPTransientEditorTickableActorBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AVPTransientEditorTickableActorBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AVPTransientEditorTickableActorBase_Statics::ClassParams = {
		&AVPTransientEditorTickableActorBase::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008000ADu,
		METADATA_PARAMS(Z_Construct_UClass_AVPTransientEditorTickableActorBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AVPTransientEditorTickableActorBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AVPTransientEditorTickableActorBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AVPTransientEditorTickableActorBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AVPTransientEditorTickableActorBase, 1500718598);
	template<> VPUTILITIESEDITOR_API UClass* StaticClass<AVPTransientEditorTickableActorBase>()
	{
		return AVPTransientEditorTickableActorBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AVPTransientEditorTickableActorBase(Z_Construct_UClass_AVPTransientEditorTickableActorBase, &AVPTransientEditorTickableActorBase::StaticClass, TEXT("/Script/VPUtilitiesEditor"), TEXT("AVPTransientEditorTickableActorBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AVPTransientEditorTickableActorBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
