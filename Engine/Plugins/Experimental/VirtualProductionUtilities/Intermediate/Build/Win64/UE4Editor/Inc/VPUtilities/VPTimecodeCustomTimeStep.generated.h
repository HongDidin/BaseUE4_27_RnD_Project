// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef VPUTILITIES_VPTimecodeCustomTimeStep_generated_h
#error "VPTimecodeCustomTimeStep.generated.h already included, missing '#pragma once' in VPTimecodeCustomTimeStep.h"
#endif
#define VPUTILITIES_VPTimecodeCustomTimeStep_generated_h

#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPTimecodeCustomTimeStep_h_20_SPARSE_DATA
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPTimecodeCustomTimeStep_h_20_RPC_WRAPPERS
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPTimecodeCustomTimeStep_h_20_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPTimecodeCustomTimeStep_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVPTimecodeCustomTimeStep(); \
	friend struct Z_Construct_UClass_UVPTimecodeCustomTimeStep_Statics; \
public: \
	DECLARE_CLASS(UVPTimecodeCustomTimeStep, UFixedFrameRateCustomTimeStep, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VPUtilities"), NO_API) \
	DECLARE_SERIALIZER(UVPTimecodeCustomTimeStep)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPTimecodeCustomTimeStep_h_20_INCLASS \
private: \
	static void StaticRegisterNativesUVPTimecodeCustomTimeStep(); \
	friend struct Z_Construct_UClass_UVPTimecodeCustomTimeStep_Statics; \
public: \
	DECLARE_CLASS(UVPTimecodeCustomTimeStep, UFixedFrameRateCustomTimeStep, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VPUtilities"), NO_API) \
	DECLARE_SERIALIZER(UVPTimecodeCustomTimeStep)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPTimecodeCustomTimeStep_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVPTimecodeCustomTimeStep(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVPTimecodeCustomTimeStep) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVPTimecodeCustomTimeStep); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVPTimecodeCustomTimeStep); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVPTimecodeCustomTimeStep(UVPTimecodeCustomTimeStep&&); \
	NO_API UVPTimecodeCustomTimeStep(const UVPTimecodeCustomTimeStep&); \
public:


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPTimecodeCustomTimeStep_h_20_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVPTimecodeCustomTimeStep(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVPTimecodeCustomTimeStep(UVPTimecodeCustomTimeStep&&); \
	NO_API UVPTimecodeCustomTimeStep(const UVPTimecodeCustomTimeStep&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVPTimecodeCustomTimeStep); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVPTimecodeCustomTimeStep); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVPTimecodeCustomTimeStep)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPTimecodeCustomTimeStep_h_20_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPTimecodeCustomTimeStep_h_17_PROLOG
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPTimecodeCustomTimeStep_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPTimecodeCustomTimeStep_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPTimecodeCustomTimeStep_h_20_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPTimecodeCustomTimeStep_h_20_RPC_WRAPPERS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPTimecodeCustomTimeStep_h_20_INCLASS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPTimecodeCustomTimeStep_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPTimecodeCustomTimeStep_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPTimecodeCustomTimeStep_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPTimecodeCustomTimeStep_h_20_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPTimecodeCustomTimeStep_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPTimecodeCustomTimeStep_h_20_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPTimecodeCustomTimeStep_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VPUTILITIES_API UClass* StaticClass<class UVPTimecodeCustomTimeStep>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPTimecodeCustomTimeStep_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
