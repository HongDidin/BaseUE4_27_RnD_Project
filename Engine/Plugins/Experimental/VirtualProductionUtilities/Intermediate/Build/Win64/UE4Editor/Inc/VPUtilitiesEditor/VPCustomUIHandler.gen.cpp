// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VPUtilitiesEditor/Private/VPCustomUIHandler.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVPCustomUIHandler() {}
// Cross Module References
	VPUTILITIESEDITOR_API UClass* Z_Construct_UClass_UVPCustomUIHandler_NoRegister();
	VPUTILITIESEDITOR_API UClass* Z_Construct_UClass_UVPCustomUIHandler();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_VPUtilitiesEditor();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	UMG_API UClass* Z_Construct_UClass_UUserWidget_NoRegister();
// End Cross Module References
	void UVPCustomUIHandler::StaticRegisterNativesUVPCustomUIHandler()
	{
	}
	UClass* Z_Construct_UClass_UVPCustomUIHandler_NoRegister()
	{
		return UVPCustomUIHandler::StaticClass();
	}
	struct Z_Construct_UClass_UVPCustomUIHandler_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VirtualProductionWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_VirtualProductionWidget;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVPCustomUIHandler_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_VPUtilitiesEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVPCustomUIHandler_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "VPCustomUIHandler.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Private/VPCustomUIHandler.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVPCustomUIHandler_Statics::NewProp_VirtualProductionWidget_MetaData[] = {
		{ "Category", "UI" },
		{ "ModuleRelativePath", "Private/VPCustomUIHandler.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UVPCustomUIHandler_Statics::NewProp_VirtualProductionWidget = { "VirtualProductionWidget", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVPCustomUIHandler, VirtualProductionWidget), Z_Construct_UClass_UUserWidget_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_UVPCustomUIHandler_Statics::NewProp_VirtualProductionWidget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVPCustomUIHandler_Statics::NewProp_VirtualProductionWidget_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UVPCustomUIHandler_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVPCustomUIHandler_Statics::NewProp_VirtualProductionWidget,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVPCustomUIHandler_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVPCustomUIHandler>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVPCustomUIHandler_Statics::ClassParams = {
		&UVPCustomUIHandler::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UVPCustomUIHandler_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UVPCustomUIHandler_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UVPCustomUIHandler_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVPCustomUIHandler_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVPCustomUIHandler()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVPCustomUIHandler_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVPCustomUIHandler, 3060225857);
	template<> VPUTILITIESEDITOR_API UClass* StaticClass<UVPCustomUIHandler>()
	{
		return UVPCustomUIHandler::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVPCustomUIHandler(Z_Construct_UClass_UVPCustomUIHandler, &UVPCustomUIHandler::StaticClass, TEXT("/Script/VPUtilitiesEditor"), TEXT("UVPCustomUIHandler"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVPCustomUIHandler);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
