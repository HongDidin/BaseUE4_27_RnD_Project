// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef VPUTILITIESEDITOR_VPCustomUIHandler_generated_h
#error "VPCustomUIHandler.generated.h already included, missing '#pragma once' in VPCustomUIHandler.h"
#endif
#define VPUTILITIESEDITOR_VPCustomUIHandler_generated_h

#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCustomUIHandler_h_17_SPARSE_DATA
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCustomUIHandler_h_17_RPC_WRAPPERS
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCustomUIHandler_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCustomUIHandler_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVPCustomUIHandler(); \
	friend struct Z_Construct_UClass_UVPCustomUIHandler_Statics; \
public: \
	DECLARE_CLASS(UVPCustomUIHandler, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VPUtilitiesEditor"), NO_API) \
	DECLARE_SERIALIZER(UVPCustomUIHandler)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCustomUIHandler_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUVPCustomUIHandler(); \
	friend struct Z_Construct_UClass_UVPCustomUIHandler_Statics; \
public: \
	DECLARE_CLASS(UVPCustomUIHandler, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VPUtilitiesEditor"), NO_API) \
	DECLARE_SERIALIZER(UVPCustomUIHandler)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCustomUIHandler_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVPCustomUIHandler(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVPCustomUIHandler) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVPCustomUIHandler); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVPCustomUIHandler); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVPCustomUIHandler(UVPCustomUIHandler&&); \
	NO_API UVPCustomUIHandler(const UVPCustomUIHandler&); \
public:


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCustomUIHandler_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVPCustomUIHandler(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVPCustomUIHandler(UVPCustomUIHandler&&); \
	NO_API UVPCustomUIHandler(const UVPCustomUIHandler&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVPCustomUIHandler); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVPCustomUIHandler); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVPCustomUIHandler)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCustomUIHandler_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCustomUIHandler_h_14_PROLOG
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCustomUIHandler_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCustomUIHandler_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCustomUIHandler_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCustomUIHandler_h_17_RPC_WRAPPERS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCustomUIHandler_h_17_INCLASS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCustomUIHandler_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCustomUIHandler_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCustomUIHandler_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCustomUIHandler_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCustomUIHandler_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCustomUIHandler_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCustomUIHandler_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VPUTILITIESEDITOR_API UClass* StaticClass<class UVPCustomUIHandler>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Private_VPCustomUIHandler_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
