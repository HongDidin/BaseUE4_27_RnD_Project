// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class EVProdPanelIDs : uint8;
class UVREditorInteractor;
class UUserWidget;
class AVREditorFloatingUI;
struct FVREditorFloatingUICreationContext;
#ifdef VPUTILITIESEDITOR_VPScoutingSubsystem_generated_h
#error "VPScoutingSubsystem.generated.h already included, missing '#pragma once' in VPScoutingSubsystem.h"
#endif
#define VPUTILITIESEDITOR_VPScoutingSubsystem_generated_h

#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_33_SPARSE_DATA
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_33_RPC_WRAPPERS
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_33_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_33_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVPScoutingSubsystemHelpersBase(); \
	friend struct Z_Construct_UClass_UVPScoutingSubsystemHelpersBase_Statics; \
public: \
	DECLARE_CLASS(UVPScoutingSubsystemHelpersBase, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/VPUtilitiesEditor"), VPUTILITIESEDITOR_API) \
	DECLARE_SERIALIZER(UVPScoutingSubsystemHelpersBase)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_33_INCLASS \
private: \
	static void StaticRegisterNativesUVPScoutingSubsystemHelpersBase(); \
	friend struct Z_Construct_UClass_UVPScoutingSubsystemHelpersBase_Statics; \
public: \
	DECLARE_CLASS(UVPScoutingSubsystemHelpersBase, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/VPUtilitiesEditor"), VPUTILITIESEDITOR_API) \
	DECLARE_SERIALIZER(UVPScoutingSubsystemHelpersBase)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_33_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	VPUTILITIESEDITOR_API UVPScoutingSubsystemHelpersBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVPScoutingSubsystemHelpersBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(VPUTILITIESEDITOR_API, UVPScoutingSubsystemHelpersBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVPScoutingSubsystemHelpersBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	VPUTILITIESEDITOR_API UVPScoutingSubsystemHelpersBase(UVPScoutingSubsystemHelpersBase&&); \
	VPUTILITIESEDITOR_API UVPScoutingSubsystemHelpersBase(const UVPScoutingSubsystemHelpersBase&); \
public:


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_33_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	VPUTILITIESEDITOR_API UVPScoutingSubsystemHelpersBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	VPUTILITIESEDITOR_API UVPScoutingSubsystemHelpersBase(UVPScoutingSubsystemHelpersBase&&); \
	VPUTILITIESEDITOR_API UVPScoutingSubsystemHelpersBase(const UVPScoutingSubsystemHelpersBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(VPUTILITIESEDITOR_API, UVPScoutingSubsystemHelpersBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVPScoutingSubsystemHelpersBase); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVPScoutingSubsystemHelpersBase)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_33_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_30_PROLOG
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_33_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_33_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_33_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_33_RPC_WRAPPERS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_33_INCLASS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_33_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_33_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_33_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_33_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_33_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_33_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_33_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VPUTILITIESEDITOR_API UClass* StaticClass<class UVPScoutingSubsystemHelpersBase>();

#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_43_SPARSE_DATA
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_43_RPC_WRAPPERS \
	virtual void OnVREditingModeExit_Implementation(); \
	virtual void OnVREditingModeEnter_Implementation(); \
	virtual void EditorTick_Implementation(float DeltaSeconds); \
 \
	DECLARE_FUNCTION(execOnVREditingModeExit); \
	DECLARE_FUNCTION(execOnVREditingModeEnter); \
	DECLARE_FUNCTION(execEditorTick);


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_43_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual void OnVREditingModeExit_Implementation(); \
	virtual void OnVREditingModeEnter_Implementation(); \
	virtual void EditorTick_Implementation(float DeltaSeconds); \
 \
	DECLARE_FUNCTION(execOnVREditingModeExit); \
	DECLARE_FUNCTION(execOnVREditingModeEnter); \
	DECLARE_FUNCTION(execEditorTick);


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_43_EVENT_PARMS \
	struct VPScoutingSubsystemGestureManagerBase_eventEditorTick_Parms \
	{ \
		float DeltaSeconds; \
	};


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_43_CALLBACK_WRAPPERS
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_43_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVPScoutingSubsystemGestureManagerBase(); \
	friend struct Z_Construct_UClass_UVPScoutingSubsystemGestureManagerBase_Statics; \
public: \
	DECLARE_CLASS(UVPScoutingSubsystemGestureManagerBase, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/VPUtilitiesEditor"), VPUTILITIESEDITOR_API) \
	DECLARE_SERIALIZER(UVPScoutingSubsystemGestureManagerBase)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_43_INCLASS \
private: \
	static void StaticRegisterNativesUVPScoutingSubsystemGestureManagerBase(); \
	friend struct Z_Construct_UClass_UVPScoutingSubsystemGestureManagerBase_Statics; \
public: \
	DECLARE_CLASS(UVPScoutingSubsystemGestureManagerBase, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/VPUtilitiesEditor"), VPUTILITIESEDITOR_API) \
	DECLARE_SERIALIZER(UVPScoutingSubsystemGestureManagerBase)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_43_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	VPUTILITIESEDITOR_API UVPScoutingSubsystemGestureManagerBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVPScoutingSubsystemGestureManagerBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(VPUTILITIESEDITOR_API, UVPScoutingSubsystemGestureManagerBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVPScoutingSubsystemGestureManagerBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	VPUTILITIESEDITOR_API UVPScoutingSubsystemGestureManagerBase(UVPScoutingSubsystemGestureManagerBase&&); \
	VPUTILITIESEDITOR_API UVPScoutingSubsystemGestureManagerBase(const UVPScoutingSubsystemGestureManagerBase&); \
public:


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_43_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	VPUTILITIESEDITOR_API UVPScoutingSubsystemGestureManagerBase(UVPScoutingSubsystemGestureManagerBase&&); \
	VPUTILITIESEDITOR_API UVPScoutingSubsystemGestureManagerBase(const UVPScoutingSubsystemGestureManagerBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(VPUTILITIESEDITOR_API, UVPScoutingSubsystemGestureManagerBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVPScoutingSubsystemGestureManagerBase); \
	DEFINE_ABSTRACT_DEFAULT_CONSTRUCTOR_CALL(UVPScoutingSubsystemGestureManagerBase)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_43_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_40_PROLOG \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_43_EVENT_PARMS


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_43_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_43_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_43_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_43_RPC_WRAPPERS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_43_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_43_INCLASS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_43_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_43_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_43_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_43_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_43_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_43_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_43_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_43_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VPUTILITIESEDITOR_API UClass* StaticClass<class UVPScoutingSubsystemGestureManagerBase>();

#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_79_SPARSE_DATA
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_79_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execToggleRotationGridSnapping); \
	DECLARE_FUNCTION(execIsRotationGridSnappingEnabled); \
	DECLARE_FUNCTION(execToggleLocationGridSnapping); \
	DECLARE_FUNCTION(execIsLocationGridSnappingEnabled); \
	DECLARE_FUNCTION(execExitVRMode); \
	DECLARE_FUNCTION(execSetIsHelperSystemEnabled); \
	DECLARE_FUNCTION(execIsHelperSystemEnabled); \
	DECLARE_FUNCTION(execSetInertiaDampingCVar); \
	DECLARE_FUNCTION(execSetIsUsingInertiaDamping); \
	DECLARE_FUNCTION(execIsUsingInertiaDamping); \
	DECLARE_FUNCTION(execSetGripNavSpeed); \
	DECLARE_FUNCTION(execGetGripNavSpeed); \
	DECLARE_FUNCTION(execSetFlightSpeed); \
	DECLARE_FUNCTION(execGetFlightSpeed); \
	DECLARE_FUNCTION(execSetShowTransformGizmoCVar); \
	DECLARE_FUNCTION(execSetIsUsingTransformGizmo); \
	DECLARE_FUNCTION(execIsUsingTransformGizmo); \
	DECLARE_FUNCTION(execSetIsUsingMetricSystem); \
	DECLARE_FUNCTION(execIsUsingMetricSystem); \
	DECLARE_FUNCTION(execGetShowName); \
	DECLARE_FUNCTION(execGetDirectorName); \
	DECLARE_FUNCTION(execGetVProdPanelID); \
	DECLARE_FUNCTION(execGetActiveEditorVRControllers); \
	DECLARE_FUNCTION(execGetPanelWidget); \
	DECLARE_FUNCTION(execGetPanelActor); \
	DECLARE_FUNCTION(execIsVRScoutingUIOpen); \
	DECLARE_FUNCTION(execHideInfoDisplayPanel); \
	DECLARE_FUNCTION(execToggleVRScoutingUI);


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_79_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execToggleRotationGridSnapping); \
	DECLARE_FUNCTION(execIsRotationGridSnappingEnabled); \
	DECLARE_FUNCTION(execToggleLocationGridSnapping); \
	DECLARE_FUNCTION(execIsLocationGridSnappingEnabled); \
	DECLARE_FUNCTION(execExitVRMode); \
	DECLARE_FUNCTION(execSetIsHelperSystemEnabled); \
	DECLARE_FUNCTION(execIsHelperSystemEnabled); \
	DECLARE_FUNCTION(execSetInertiaDampingCVar); \
	DECLARE_FUNCTION(execSetIsUsingInertiaDamping); \
	DECLARE_FUNCTION(execIsUsingInertiaDamping); \
	DECLARE_FUNCTION(execSetGripNavSpeed); \
	DECLARE_FUNCTION(execGetGripNavSpeed); \
	DECLARE_FUNCTION(execSetFlightSpeed); \
	DECLARE_FUNCTION(execGetFlightSpeed); \
	DECLARE_FUNCTION(execSetShowTransformGizmoCVar); \
	DECLARE_FUNCTION(execSetIsUsingTransformGizmo); \
	DECLARE_FUNCTION(execIsUsingTransformGizmo); \
	DECLARE_FUNCTION(execSetIsUsingMetricSystem); \
	DECLARE_FUNCTION(execIsUsingMetricSystem); \
	DECLARE_FUNCTION(execGetShowName); \
	DECLARE_FUNCTION(execGetDirectorName); \
	DECLARE_FUNCTION(execGetVProdPanelID); \
	DECLARE_FUNCTION(execGetActiveEditorVRControllers); \
	DECLARE_FUNCTION(execGetPanelWidget); \
	DECLARE_FUNCTION(execGetPanelActor); \
	DECLARE_FUNCTION(execIsVRScoutingUIOpen); \
	DECLARE_FUNCTION(execHideInfoDisplayPanel); \
	DECLARE_FUNCTION(execToggleVRScoutingUI);


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_79_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVPScoutingSubsystem(); \
	friend struct Z_Construct_UClass_UVPScoutingSubsystem_Statics; \
public: \
	DECLARE_CLASS(UVPScoutingSubsystem, UEditorSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VPUtilitiesEditor"), NO_API) \
	DECLARE_SERIALIZER(UVPScoutingSubsystem)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_79_INCLASS \
private: \
	static void StaticRegisterNativesUVPScoutingSubsystem(); \
	friend struct Z_Construct_UClass_UVPScoutingSubsystem_Statics; \
public: \
	DECLARE_CLASS(UVPScoutingSubsystem, UEditorSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VPUtilitiesEditor"), NO_API) \
	DECLARE_SERIALIZER(UVPScoutingSubsystem)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_79_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVPScoutingSubsystem(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVPScoutingSubsystem) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVPScoutingSubsystem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVPScoutingSubsystem); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVPScoutingSubsystem(UVPScoutingSubsystem&&); \
	NO_API UVPScoutingSubsystem(const UVPScoutingSubsystem&); \
public:


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_79_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVPScoutingSubsystem(UVPScoutingSubsystem&&); \
	NO_API UVPScoutingSubsystem(const UVPScoutingSubsystem&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVPScoutingSubsystem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVPScoutingSubsystem); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UVPScoutingSubsystem)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_79_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_76_PROLOG
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_79_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_79_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_79_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_79_RPC_WRAPPERS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_79_INCLASS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_79_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_79_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_79_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_79_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_79_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_79_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h_79_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VPUTILITIESEDITOR_API UClass* StaticClass<class UVPScoutingSubsystem>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPScoutingSubsystem_h


#define FOREACH_ENUM_EVPRODPANELIDS(op) \
	op(EVProdPanelIDs::Main) \
	op(EVProdPanelIDs::Left) \
	op(EVProdPanelIDs::Right) \
	op(EVProdPanelIDs::Context) \
	op(EVProdPanelIDs::Timeline) \
	op(EVProdPanelIDs::Measure) \
	op(EVProdPanelIDs::Gaffer) 

enum class EVProdPanelIDs : uint8;
template<> VPUTILITIESEDITOR_API UEnum* StaticEnum<EVProdPanelIDs>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
