// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef VPUTILITIES_VPFullScreenUserWidget_generated_h
#error "VPFullScreenUserWidget.generated.h already included, missing '#pragma once' in VPFullScreenUserWidget.h"
#endif
#define VPUTILITIES_VPFullScreenUserWidget_generated_h

#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidget_h_74_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FVPFullScreenUserWidget_PostProcess_Statics; \
	VPUTILITIES_API static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__PostProcessComponent() { return STRUCT_OFFSET(FVPFullScreenUserWidget_PostProcess, PostProcessComponent); } \
	FORCEINLINE static uint32 __PPO__PostProcessMaterialInstance() { return STRUCT_OFFSET(FVPFullScreenUserWidget_PostProcess, PostProcessMaterialInstance); }


template<> VPUTILITIES_API UScriptStruct* StaticStruct<struct FVPFullScreenUserWidget_PostProcess>();

#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidget_h_46_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FVPFullScreenUserWidget_Viewport_Statics; \
	VPUTILITIES_API static class UScriptStruct* StaticStruct();


template<> VPUTILITIES_API UScriptStruct* StaticStruct<struct FVPFullScreenUserWidget_Viewport>();

#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidget_h_190_SPARSE_DATA
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidget_h_190_RPC_WRAPPERS
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidget_h_190_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidget_h_190_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVPFullScreenUserWidget(); \
	friend struct Z_Construct_UClass_UVPFullScreenUserWidget_Statics; \
public: \
	DECLARE_CLASS(UVPFullScreenUserWidget, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VPUtilities"), NO_API) \
	DECLARE_SERIALIZER(UVPFullScreenUserWidget)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidget_h_190_INCLASS \
private: \
	static void StaticRegisterNativesUVPFullScreenUserWidget(); \
	friend struct Z_Construct_UClass_UVPFullScreenUserWidget_Statics; \
public: \
	DECLARE_CLASS(UVPFullScreenUserWidget, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VPUtilities"), NO_API) \
	DECLARE_SERIALIZER(UVPFullScreenUserWidget)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidget_h_190_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVPFullScreenUserWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVPFullScreenUserWidget) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVPFullScreenUserWidget); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVPFullScreenUserWidget); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVPFullScreenUserWidget(UVPFullScreenUserWidget&&); \
	NO_API UVPFullScreenUserWidget(const UVPFullScreenUserWidget&); \
public:


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidget_h_190_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVPFullScreenUserWidget(UVPFullScreenUserWidget&&); \
	NO_API UVPFullScreenUserWidget(const UVPFullScreenUserWidget&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVPFullScreenUserWidget); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVPFullScreenUserWidget); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVPFullScreenUserWidget)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidget_h_190_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__EditorDisplayType() { return STRUCT_OFFSET(UVPFullScreenUserWidget, EditorDisplayType); } \
	FORCEINLINE static uint32 __PPO__GameDisplayType() { return STRUCT_OFFSET(UVPFullScreenUserWidget, GameDisplayType); } \
	FORCEINLINE static uint32 __PPO__PIEDisplayType() { return STRUCT_OFFSET(UVPFullScreenUserWidget, PIEDisplayType); } \
	FORCEINLINE static uint32 __PPO__ViewportDisplayType() { return STRUCT_OFFSET(UVPFullScreenUserWidget, ViewportDisplayType); } \
	FORCEINLINE static uint32 __PPO__Widget() { return STRUCT_OFFSET(UVPFullScreenUserWidget, Widget); }


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidget_h_187_PROLOG
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidget_h_190_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidget_h_190_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidget_h_190_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidget_h_190_RPC_WRAPPERS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidget_h_190_INCLASS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidget_h_190_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidget_h_190_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidget_h_190_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidget_h_190_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidget_h_190_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidget_h_190_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidget_h_190_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VPUTILITIES_API UClass* StaticClass<class UVPFullScreenUserWidget>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilities_Public_VPFullScreenUserWidget_h


#define FOREACH_ENUM_EVPWIDGETDISPLAYTYPE(op) \
	op(EVPWidgetDisplayType::Inactive) \
	op(EVPWidgetDisplayType::Viewport) \
	op(EVPWidgetDisplayType::PostProcess) \
	op(EVPWidgetDisplayType::Composure) 

enum class EVPWidgetDisplayType : uint8;
template<> VPUTILITIES_API UEnum* StaticEnum<EVPWidgetDisplayType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
