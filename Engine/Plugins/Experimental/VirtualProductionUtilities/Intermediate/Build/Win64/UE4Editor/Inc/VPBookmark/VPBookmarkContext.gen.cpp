// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VPBookmark/Public/VPBookmarkContext.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVPBookmarkContext() {}
// Cross Module References
	VPBOOKMARK_API UScriptStruct* Z_Construct_UScriptStruct_FVPBookmarkCreationContext();
	UPackage* Z_Construct_UPackage__Script_VPBookmark();
// End Cross Module References
class UScriptStruct* FVPBookmarkCreationContext::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern VPBOOKMARK_API uint32 Get_Z_Construct_UScriptStruct_FVPBookmarkCreationContext_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FVPBookmarkCreationContext, Z_Construct_UPackage__Script_VPBookmark(), TEXT("VPBookmarkCreationContext"), sizeof(FVPBookmarkCreationContext), Get_Z_Construct_UScriptStruct_FVPBookmarkCreationContext_Hash());
	}
	return Singleton;
}
template<> VPBOOKMARK_API UScriptStruct* StaticStruct<FVPBookmarkCreationContext>()
{
	return FVPBookmarkCreationContext::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FVPBookmarkCreationContext(FVPBookmarkCreationContext::StaticStruct, TEXT("/Script/VPBookmark"), TEXT("VPBookmarkCreationContext"), false, nullptr, nullptr);
static struct FScriptStruct_VPBookmark_StaticRegisterNativesFVPBookmarkCreationContext
{
	FScriptStruct_VPBookmark_StaticRegisterNativesFVPBookmarkCreationContext()
	{
		UScriptStruct::DeferCppStructOps<FVPBookmarkCreationContext>(FName(TEXT("VPBookmarkCreationContext")));
	}
} ScriptStruct_VPBookmark_StaticRegisterNativesFVPBookmarkCreationContext;
	struct Z_Construct_UScriptStruct_FVPBookmarkCreationContext_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConcertCreator_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ConcertCreator;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CategoryName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_CategoryName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisplayName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DisplayName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVPBookmarkCreationContext_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/VPBookmarkContext.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FVPBookmarkCreationContext_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FVPBookmarkCreationContext>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVPBookmarkCreationContext_Statics::NewProp_ConcertCreator_MetaData[] = {
		{ "Category", "Bookmarks" },
		{ "Comment", "/** Username of the Concert client who is creating this bookmark. */" },
		{ "ModuleRelativePath", "Public/VPBookmarkContext.h" },
		{ "ToolTip", "Username of the Concert client who is creating this bookmark." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FVPBookmarkCreationContext_Statics::NewProp_ConcertCreator = { "ConcertCreator", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVPBookmarkCreationContext, ConcertCreator), METADATA_PARAMS(Z_Construct_UScriptStruct_FVPBookmarkCreationContext_Statics::NewProp_ConcertCreator_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVPBookmarkCreationContext_Statics::NewProp_ConcertCreator_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVPBookmarkCreationContext_Statics::NewProp_CategoryName_MetaData[] = {
		{ "Category", "Bookmarks" },
		{ "Comment", "/** Category of the Bookmark */" },
		{ "ModuleRelativePath", "Public/VPBookmarkContext.h" },
		{ "ToolTip", "Category of the Bookmark" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FVPBookmarkCreationContext_Statics::NewProp_CategoryName = { "CategoryName", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVPBookmarkCreationContext, CategoryName), METADATA_PARAMS(Z_Construct_UScriptStruct_FVPBookmarkCreationContext_Statics::NewProp_CategoryName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVPBookmarkCreationContext_Statics::NewProp_CategoryName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVPBookmarkCreationContext_Statics::NewProp_DisplayName_MetaData[] = {
		{ "Category", "Bookmarks" },
		{ "Comment", "/** Name of the Bookmark */" },
		{ "ModuleRelativePath", "Public/VPBookmarkContext.h" },
		{ "ToolTip", "Name of the Bookmark" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FVPBookmarkCreationContext_Statics::NewProp_DisplayName = { "DisplayName", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVPBookmarkCreationContext, DisplayName), METADATA_PARAMS(Z_Construct_UScriptStruct_FVPBookmarkCreationContext_Statics::NewProp_DisplayName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVPBookmarkCreationContext_Statics::NewProp_DisplayName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FVPBookmarkCreationContext_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVPBookmarkCreationContext_Statics::NewProp_ConcertCreator,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVPBookmarkCreationContext_Statics::NewProp_CategoryName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVPBookmarkCreationContext_Statics::NewProp_DisplayName,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FVPBookmarkCreationContext_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_VPBookmark,
		nullptr,
		&NewStructOps,
		"VPBookmarkCreationContext",
		sizeof(FVPBookmarkCreationContext),
		alignof(FVPBookmarkCreationContext),
		Z_Construct_UScriptStruct_FVPBookmarkCreationContext_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVPBookmarkCreationContext_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FVPBookmarkCreationContext_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVPBookmarkCreationContext_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FVPBookmarkCreationContext()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FVPBookmarkCreationContext_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_VPBookmark();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("VPBookmarkCreationContext"), sizeof(FVPBookmarkCreationContext), Get_Z_Construct_UScriptStruct_FVPBookmarkCreationContext_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FVPBookmarkCreationContext_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FVPBookmarkCreationContext_Hash() { return 2381671359U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
