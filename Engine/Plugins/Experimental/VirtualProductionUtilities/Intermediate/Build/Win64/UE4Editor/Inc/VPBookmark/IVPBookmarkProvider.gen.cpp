// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VPBookmark/Public/IVPBookmarkProvider.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeIVPBookmarkProvider() {}
// Cross Module References
	VPBOOKMARK_API UClass* Z_Construct_UClass_UVPBookmarkProvider_NoRegister();
	VPBOOKMARK_API UClass* Z_Construct_UClass_UVPBookmarkProvider();
	COREUOBJECT_API UClass* Z_Construct_UClass_UInterface();
	UPackage* Z_Construct_UPackage__Script_VPBookmark();
	VPBOOKMARK_API UClass* Z_Construct_UClass_UVPBookmark_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(IVPBookmarkProvider::execGenerateBookmarkName)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GenerateBookmarkName_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IVPBookmarkProvider::execHideBookmarkSplineMeshIndicator)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->HideBookmarkSplineMeshIndicator_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IVPBookmarkProvider::execUpdateBookmarkSplineMeshIndicator)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->UpdateBookmarkSplineMeshIndicator_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IVPBookmarkProvider::execOnBookmarkChanged)
	{
		P_GET_OBJECT(UVPBookmark,Z_Param_Bookmark);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnBookmarkChanged_Implementation(Z_Param_Bookmark);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IVPBookmarkProvider::execOnBookmarkActivation)
	{
		P_GET_OBJECT(UVPBookmark,Z_Param_Bookmark);
		P_GET_UBOOL(Z_Param_bActivate);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnBookmarkActivation_Implementation(Z_Param_Bookmark,Z_Param_bActivate);
		P_NATIVE_END;
	}
	void IVPBookmarkProvider::GenerateBookmarkName()
	{
		check(0 && "Do not directly call Event functions in Interfaces. Call Execute_GenerateBookmarkName instead.");
	}
	void IVPBookmarkProvider::HideBookmarkSplineMeshIndicator()
	{
		check(0 && "Do not directly call Event functions in Interfaces. Call Execute_HideBookmarkSplineMeshIndicator instead.");
	}
	void IVPBookmarkProvider::OnBookmarkActivation(UVPBookmark* Bookmark, bool bActivate)
	{
		check(0 && "Do not directly call Event functions in Interfaces. Call Execute_OnBookmarkActivation instead.");
	}
	void IVPBookmarkProvider::OnBookmarkChanged(UVPBookmark* Bookmark)
	{
		check(0 && "Do not directly call Event functions in Interfaces. Call Execute_OnBookmarkChanged instead.");
	}
	void IVPBookmarkProvider::UpdateBookmarkSplineMeshIndicator()
	{
		check(0 && "Do not directly call Event functions in Interfaces. Call Execute_UpdateBookmarkSplineMeshIndicator instead.");
	}
	void UVPBookmarkProvider::StaticRegisterNativesUVPBookmarkProvider()
	{
		UClass* Class = UVPBookmarkProvider::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GenerateBookmarkName", &IVPBookmarkProvider::execGenerateBookmarkName },
			{ "HideBookmarkSplineMeshIndicator", &IVPBookmarkProvider::execHideBookmarkSplineMeshIndicator },
			{ "OnBookmarkActivation", &IVPBookmarkProvider::execOnBookmarkActivation },
			{ "OnBookmarkChanged", &IVPBookmarkProvider::execOnBookmarkChanged },
			{ "UpdateBookmarkSplineMeshIndicator", &IVPBookmarkProvider::execUpdateBookmarkSplineMeshIndicator },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UVPBookmarkProvider_GenerateBookmarkName_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPBookmarkProvider_GenerateBookmarkName_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "Bookmarks" },
		{ "ModuleRelativePath", "Public/IVPBookmarkProvider.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVPBookmarkProvider_GenerateBookmarkName_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVPBookmarkProvider, nullptr, "GenerateBookmarkName", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVPBookmarkProvider_GenerateBookmarkName_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPBookmarkProvider_GenerateBookmarkName_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVPBookmarkProvider_GenerateBookmarkName()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVPBookmarkProvider_GenerateBookmarkName_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVPBookmarkProvider_HideBookmarkSplineMeshIndicator_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPBookmarkProvider_HideBookmarkSplineMeshIndicator_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "Bookmarks" },
		{ "ModuleRelativePath", "Public/IVPBookmarkProvider.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVPBookmarkProvider_HideBookmarkSplineMeshIndicator_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVPBookmarkProvider, nullptr, "HideBookmarkSplineMeshIndicator", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVPBookmarkProvider_HideBookmarkSplineMeshIndicator_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPBookmarkProvider_HideBookmarkSplineMeshIndicator_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVPBookmarkProvider_HideBookmarkSplineMeshIndicator()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVPBookmarkProvider_HideBookmarkSplineMeshIndicator_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVPBookmarkProvider_OnBookmarkActivation_Statics
	{
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Bookmark;
		static void NewProp_bActivate_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bActivate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVPBookmarkProvider_OnBookmarkActivation_Statics::NewProp_Bookmark = { "Bookmark", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VPBookmarkProvider_eventOnBookmarkActivation_Parms, Bookmark), Z_Construct_UClass_UVPBookmark_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UVPBookmarkProvider_OnBookmarkActivation_Statics::NewProp_bActivate_SetBit(void* Obj)
	{
		((VPBookmarkProvider_eventOnBookmarkActivation_Parms*)Obj)->bActivate = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVPBookmarkProvider_OnBookmarkActivation_Statics::NewProp_bActivate = { "bActivate", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VPBookmarkProvider_eventOnBookmarkActivation_Parms), &Z_Construct_UFunction_UVPBookmarkProvider_OnBookmarkActivation_Statics::NewProp_bActivate_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVPBookmarkProvider_OnBookmarkActivation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVPBookmarkProvider_OnBookmarkActivation_Statics::NewProp_Bookmark,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVPBookmarkProvider_OnBookmarkActivation_Statics::NewProp_bActivate,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPBookmarkProvider_OnBookmarkActivation_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "Bookmarks" },
		{ "ModuleRelativePath", "Public/IVPBookmarkProvider.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVPBookmarkProvider_OnBookmarkActivation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVPBookmarkProvider, nullptr, "OnBookmarkActivation", nullptr, nullptr, sizeof(VPBookmarkProvider_eventOnBookmarkActivation_Parms), Z_Construct_UFunction_UVPBookmarkProvider_OnBookmarkActivation_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPBookmarkProvider_OnBookmarkActivation_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVPBookmarkProvider_OnBookmarkActivation_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPBookmarkProvider_OnBookmarkActivation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVPBookmarkProvider_OnBookmarkActivation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVPBookmarkProvider_OnBookmarkActivation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVPBookmarkProvider_OnBookmarkChanged_Statics
	{
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Bookmark;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVPBookmarkProvider_OnBookmarkChanged_Statics::NewProp_Bookmark = { "Bookmark", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VPBookmarkProvider_eventOnBookmarkChanged_Parms, Bookmark), Z_Construct_UClass_UVPBookmark_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVPBookmarkProvider_OnBookmarkChanged_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVPBookmarkProvider_OnBookmarkChanged_Statics::NewProp_Bookmark,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPBookmarkProvider_OnBookmarkChanged_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "Bookmarks" },
		{ "ModuleRelativePath", "Public/IVPBookmarkProvider.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVPBookmarkProvider_OnBookmarkChanged_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVPBookmarkProvider, nullptr, "OnBookmarkChanged", nullptr, nullptr, sizeof(VPBookmarkProvider_eventOnBookmarkChanged_Parms), Z_Construct_UFunction_UVPBookmarkProvider_OnBookmarkChanged_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPBookmarkProvider_OnBookmarkChanged_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVPBookmarkProvider_OnBookmarkChanged_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPBookmarkProvider_OnBookmarkChanged_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVPBookmarkProvider_OnBookmarkChanged()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVPBookmarkProvider_OnBookmarkChanged_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVPBookmarkProvider_UpdateBookmarkSplineMeshIndicator_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPBookmarkProvider_UpdateBookmarkSplineMeshIndicator_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "Bookmarks" },
		{ "ModuleRelativePath", "Public/IVPBookmarkProvider.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVPBookmarkProvider_UpdateBookmarkSplineMeshIndicator_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVPBookmarkProvider, nullptr, "UpdateBookmarkSplineMeshIndicator", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVPBookmarkProvider_UpdateBookmarkSplineMeshIndicator_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPBookmarkProvider_UpdateBookmarkSplineMeshIndicator_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVPBookmarkProvider_UpdateBookmarkSplineMeshIndicator()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVPBookmarkProvider_UpdateBookmarkSplineMeshIndicator_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UVPBookmarkProvider_NoRegister()
	{
		return UVPBookmarkProvider::StaticClass();
	}
	struct Z_Construct_UClass_UVPBookmarkProvider_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVPBookmarkProvider_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInterface,
		(UObject* (*)())Z_Construct_UPackage__Script_VPBookmark,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UVPBookmarkProvider_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UVPBookmarkProvider_GenerateBookmarkName, "GenerateBookmarkName" }, // 157066233
		{ &Z_Construct_UFunction_UVPBookmarkProvider_HideBookmarkSplineMeshIndicator, "HideBookmarkSplineMeshIndicator" }, // 29298201
		{ &Z_Construct_UFunction_UVPBookmarkProvider_OnBookmarkActivation, "OnBookmarkActivation" }, // 2289328094
		{ &Z_Construct_UFunction_UVPBookmarkProvider_OnBookmarkChanged, "OnBookmarkChanged" }, // 3222165384
		{ &Z_Construct_UFunction_UVPBookmarkProvider_UpdateBookmarkSplineMeshIndicator, "UpdateBookmarkSplineMeshIndicator" }, // 792985762
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVPBookmarkProvider_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/IVPBookmarkProvider.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVPBookmarkProvider_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<IVPBookmarkProvider>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVPBookmarkProvider_Statics::ClassParams = {
		&UVPBookmarkProvider::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001040A1u,
		METADATA_PARAMS(Z_Construct_UClass_UVPBookmarkProvider_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVPBookmarkProvider_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVPBookmarkProvider()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVPBookmarkProvider_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVPBookmarkProvider, 3749019773);
	template<> VPBOOKMARK_API UClass* StaticClass<UVPBookmarkProvider>()
	{
		return UVPBookmarkProvider::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVPBookmarkProvider(Z_Construct_UClass_UVPBookmarkProvider, &UVPBookmarkProvider::StaticClass, TEXT("/Script/VPBookmark"), TEXT("UVPBookmarkProvider"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVPBookmarkProvider);
	static FName NAME_UVPBookmarkProvider_GenerateBookmarkName = FName(TEXT("GenerateBookmarkName"));
	void IVPBookmarkProvider::Execute_GenerateBookmarkName(UObject* O)
	{
		check(O != NULL);
		check(O->GetClass()->ImplementsInterface(UVPBookmarkProvider::StaticClass()));
		UFunction* const Func = O->FindFunction(NAME_UVPBookmarkProvider_GenerateBookmarkName);
		if (Func)
		{
			O->ProcessEvent(Func, NULL);
		}
		else if (auto I = (IVPBookmarkProvider*)(O->GetNativeInterfaceAddress(UVPBookmarkProvider::StaticClass())))
		{
			I->GenerateBookmarkName_Implementation();
		}
	}
	static FName NAME_UVPBookmarkProvider_HideBookmarkSplineMeshIndicator = FName(TEXT("HideBookmarkSplineMeshIndicator"));
	void IVPBookmarkProvider::Execute_HideBookmarkSplineMeshIndicator(UObject* O)
	{
		check(O != NULL);
		check(O->GetClass()->ImplementsInterface(UVPBookmarkProvider::StaticClass()));
		UFunction* const Func = O->FindFunction(NAME_UVPBookmarkProvider_HideBookmarkSplineMeshIndicator);
		if (Func)
		{
			O->ProcessEvent(Func, NULL);
		}
		else if (auto I = (IVPBookmarkProvider*)(O->GetNativeInterfaceAddress(UVPBookmarkProvider::StaticClass())))
		{
			I->HideBookmarkSplineMeshIndicator_Implementation();
		}
	}
	static FName NAME_UVPBookmarkProvider_OnBookmarkActivation = FName(TEXT("OnBookmarkActivation"));
	void IVPBookmarkProvider::Execute_OnBookmarkActivation(UObject* O, UVPBookmark* Bookmark, bool bActivate)
	{
		check(O != NULL);
		check(O->GetClass()->ImplementsInterface(UVPBookmarkProvider::StaticClass()));
		VPBookmarkProvider_eventOnBookmarkActivation_Parms Parms;
		UFunction* const Func = O->FindFunction(NAME_UVPBookmarkProvider_OnBookmarkActivation);
		if (Func)
		{
			Parms.Bookmark=Bookmark;
			Parms.bActivate=bActivate;
			O->ProcessEvent(Func, &Parms);
		}
		else if (auto I = (IVPBookmarkProvider*)(O->GetNativeInterfaceAddress(UVPBookmarkProvider::StaticClass())))
		{
			I->OnBookmarkActivation_Implementation(Bookmark,bActivate);
		}
	}
	static FName NAME_UVPBookmarkProvider_OnBookmarkChanged = FName(TEXT("OnBookmarkChanged"));
	void IVPBookmarkProvider::Execute_OnBookmarkChanged(UObject* O, UVPBookmark* Bookmark)
	{
		check(O != NULL);
		check(O->GetClass()->ImplementsInterface(UVPBookmarkProvider::StaticClass()));
		VPBookmarkProvider_eventOnBookmarkChanged_Parms Parms;
		UFunction* const Func = O->FindFunction(NAME_UVPBookmarkProvider_OnBookmarkChanged);
		if (Func)
		{
			Parms.Bookmark=Bookmark;
			O->ProcessEvent(Func, &Parms);
		}
		else if (auto I = (IVPBookmarkProvider*)(O->GetNativeInterfaceAddress(UVPBookmarkProvider::StaticClass())))
		{
			I->OnBookmarkChanged_Implementation(Bookmark);
		}
	}
	static FName NAME_UVPBookmarkProvider_UpdateBookmarkSplineMeshIndicator = FName(TEXT("UpdateBookmarkSplineMeshIndicator"));
	void IVPBookmarkProvider::Execute_UpdateBookmarkSplineMeshIndicator(UObject* O)
	{
		check(O != NULL);
		check(O->GetClass()->ImplementsInterface(UVPBookmarkProvider::StaticClass()));
		UFunction* const Func = O->FindFunction(NAME_UVPBookmarkProvider_UpdateBookmarkSplineMeshIndicator);
		if (Func)
		{
			O->ProcessEvent(Func, NULL);
		}
		else if (auto I = (IVPBookmarkProvider*)(O->GetNativeInterfaceAddress(UVPBookmarkProvider::StaticClass())))
		{
			I->UpdateBookmarkSplineMeshIndicator_Implementation();
		}
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
