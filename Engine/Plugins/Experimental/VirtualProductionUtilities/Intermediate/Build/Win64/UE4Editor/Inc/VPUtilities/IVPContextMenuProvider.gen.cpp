// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VPUtilities/Public/IVPContextMenuProvider.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeIVPContextMenuProvider() {}
// Cross Module References
	VPUTILITIES_API UClass* Z_Construct_UClass_UVPContextMenuProvider_NoRegister();
	VPUTILITIES_API UClass* Z_Construct_UClass_UVPContextMenuProvider();
	COREUOBJECT_API UClass* Z_Construct_UClass_UInterface();
	UPackage* Z_Construct_UPackage__Script_VPUtilities();
// End Cross Module References
	DEFINE_FUNCTION(IVPContextMenuProvider::execOnCreateContextMenu)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnCreateContextMenu_Implementation();
		P_NATIVE_END;
	}
	void IVPContextMenuProvider::OnCreateContextMenu()
	{
		check(0 && "Do not directly call Event functions in Interfaces. Call Execute_OnCreateContextMenu instead.");
	}
	void UVPContextMenuProvider::StaticRegisterNativesUVPContextMenuProvider()
	{
		UClass* Class = UVPContextMenuProvider::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "OnCreateContextMenu", &IVPContextMenuProvider::execOnCreateContextMenu },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UVPContextMenuProvider_OnCreateContextMenu_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVPContextMenuProvider_OnCreateContextMenu_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "Virtual Production" },
		{ "ModuleRelativePath", "Public/IVPContextMenuProvider.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVPContextMenuProvider_OnCreateContextMenu_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVPContextMenuProvider, nullptr, "OnCreateContextMenu", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVPContextMenuProvider_OnCreateContextMenu_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVPContextMenuProvider_OnCreateContextMenu_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVPContextMenuProvider_OnCreateContextMenu()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVPContextMenuProvider_OnCreateContextMenu_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UVPContextMenuProvider_NoRegister()
	{
		return UVPContextMenuProvider::StaticClass();
	}
	struct Z_Construct_UClass_UVPContextMenuProvider_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVPContextMenuProvider_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInterface,
		(UObject* (*)())Z_Construct_UPackage__Script_VPUtilities,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UVPContextMenuProvider_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UVPContextMenuProvider_OnCreateContextMenu, "OnCreateContextMenu" }, // 2114547273
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVPContextMenuProvider_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/IVPContextMenuProvider.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVPContextMenuProvider_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<IVPContextMenuProvider>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVPContextMenuProvider_Statics::ClassParams = {
		&UVPContextMenuProvider::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x000040A1u,
		METADATA_PARAMS(Z_Construct_UClass_UVPContextMenuProvider_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVPContextMenuProvider_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVPContextMenuProvider()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVPContextMenuProvider_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVPContextMenuProvider, 1449124578);
	template<> VPUTILITIES_API UClass* StaticClass<UVPContextMenuProvider>()
	{
		return UVPContextMenuProvider::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVPContextMenuProvider(Z_Construct_UClass_UVPContextMenuProvider, &UVPContextMenuProvider::StaticClass, TEXT("/Script/VPUtilities"), TEXT("UVPContextMenuProvider"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVPContextMenuProvider);
	static FName NAME_UVPContextMenuProvider_OnCreateContextMenu = FName(TEXT("OnCreateContextMenu"));
	void IVPContextMenuProvider::Execute_OnCreateContextMenu(UObject* O)
	{
		check(O != NULL);
		check(O->GetClass()->ImplementsInterface(UVPContextMenuProvider::StaticClass()));
		UFunction* const Func = O->FindFunction(NAME_UVPContextMenuProvider_OnCreateContextMenu);
		if (Func)
		{
			O->ProcessEvent(Func, NULL);
		}
		else if (auto I = (IVPContextMenuProvider*)(O->GetNativeInterfaceAddress(UVPContextMenuProvider::StaticClass())))
		{
			I->OnCreateContextMenu_Implementation();
		}
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
