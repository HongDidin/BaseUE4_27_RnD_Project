// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UOSCServer;
class UTexture;
class UObject;
class AVPTransientEditorTickableActorBase;
struct FVector;
struct FRotator;
class AVPEditorTickableActorBase;
#ifdef VPUTILITIESEDITOR_VPUtilitiesEditorBlueprintLibrary_generated_h
#error "VPUtilitiesEditorBlueprintLibrary.generated.h already included, missing '#pragma once' in VPUtilitiesEditorBlueprintLibrary.h"
#endif
#define VPUTILITIESEDITOR_VPUtilitiesEditorBlueprintLibrary_generated_h

#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorBlueprintLibrary_h_15_SPARSE_DATA
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorBlueprintLibrary_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetDefaultOSCServer); \
	DECLARE_FUNCTION(execImportSnapshotTexture); \
	DECLARE_FUNCTION(execSpawnVPTransientEditorTickableActor); \
	DECLARE_FUNCTION(execSpawnVPEditorTickableActor);


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorBlueprintLibrary_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetDefaultOSCServer); \
	DECLARE_FUNCTION(execImportSnapshotTexture); \
	DECLARE_FUNCTION(execSpawnVPTransientEditorTickableActor); \
	DECLARE_FUNCTION(execSpawnVPEditorTickableActor);


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorBlueprintLibrary_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVPUtilitiesEditorBlueprintLibrary(); \
	friend struct Z_Construct_UClass_UVPUtilitiesEditorBlueprintLibrary_Statics; \
public: \
	DECLARE_CLASS(UVPUtilitiesEditorBlueprintLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VPUtilitiesEditor"), NO_API) \
	DECLARE_SERIALIZER(UVPUtilitiesEditorBlueprintLibrary)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorBlueprintLibrary_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUVPUtilitiesEditorBlueprintLibrary(); \
	friend struct Z_Construct_UClass_UVPUtilitiesEditorBlueprintLibrary_Statics; \
public: \
	DECLARE_CLASS(UVPUtilitiesEditorBlueprintLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VPUtilitiesEditor"), NO_API) \
	DECLARE_SERIALIZER(UVPUtilitiesEditorBlueprintLibrary)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorBlueprintLibrary_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVPUtilitiesEditorBlueprintLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVPUtilitiesEditorBlueprintLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVPUtilitiesEditorBlueprintLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVPUtilitiesEditorBlueprintLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVPUtilitiesEditorBlueprintLibrary(UVPUtilitiesEditorBlueprintLibrary&&); \
	NO_API UVPUtilitiesEditorBlueprintLibrary(const UVPUtilitiesEditorBlueprintLibrary&); \
public:


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorBlueprintLibrary_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVPUtilitiesEditorBlueprintLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVPUtilitiesEditorBlueprintLibrary(UVPUtilitiesEditorBlueprintLibrary&&); \
	NO_API UVPUtilitiesEditorBlueprintLibrary(const UVPUtilitiesEditorBlueprintLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVPUtilitiesEditorBlueprintLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVPUtilitiesEditorBlueprintLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVPUtilitiesEditorBlueprintLibrary)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorBlueprintLibrary_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorBlueprintLibrary_h_12_PROLOG
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorBlueprintLibrary_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorBlueprintLibrary_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorBlueprintLibrary_h_15_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorBlueprintLibrary_h_15_RPC_WRAPPERS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorBlueprintLibrary_h_15_INCLASS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorBlueprintLibrary_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorBlueprintLibrary_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorBlueprintLibrary_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorBlueprintLibrary_h_15_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorBlueprintLibrary_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorBlueprintLibrary_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorBlueprintLibrary_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VPUTILITIESEDITOR_API UClass* StaticClass<class UVPUtilitiesEditorBlueprintLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPUtilitiesEditor_Public_VPUtilitiesEditorBlueprintLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
