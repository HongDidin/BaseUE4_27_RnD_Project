// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
#ifdef VPBOOKMARK_VPBookmark_generated_h
#error "VPBookmark.generated.h already included, missing '#pragma once' in VPBookmark.h"
#endif
#define VPBOOKMARK_VPBookmark_generated_h

#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmark_h_40_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FVPBookmarkJumpToSettings_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FBookmarkBaseJumpToSettings Super;


template<> VPBOOKMARK_API UScriptStruct* StaticStruct<struct FVPBookmarkJumpToSettings>();

#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmark_h_17_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FVPBookmarkViewportData_Statics; \
	static class UScriptStruct* StaticStruct();


template<> VPBOOKMARK_API UScriptStruct* StaticStruct<struct FVPBookmarkViewportData>();

#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmark_h_47_SPARSE_DATA
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmark_h_47_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetDisplayName); \
	DECLARE_FUNCTION(execGetAssociatedBookmarkActor); \
	DECLARE_FUNCTION(execGetBookmarkIndex); \
	DECLARE_FUNCTION(execIsActive);


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmark_h_47_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetDisplayName); \
	DECLARE_FUNCTION(execGetAssociatedBookmarkActor); \
	DECLARE_FUNCTION(execGetBookmarkIndex); \
	DECLARE_FUNCTION(execIsActive);


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmark_h_47_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVPBookmark(); \
	friend struct Z_Construct_UClass_UVPBookmark_Statics; \
public: \
	DECLARE_CLASS(UVPBookmark, UBookmarkBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VPBookmark"), NO_API) \
	DECLARE_SERIALIZER(UVPBookmark)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmark_h_47_INCLASS \
private: \
	static void StaticRegisterNativesUVPBookmark(); \
	friend struct Z_Construct_UClass_UVPBookmark_Statics; \
public: \
	DECLARE_CLASS(UVPBookmark, UBookmarkBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VPBookmark"), NO_API) \
	DECLARE_SERIALIZER(UVPBookmark)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmark_h_47_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVPBookmark(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVPBookmark) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVPBookmark); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVPBookmark); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVPBookmark(UVPBookmark&&); \
	NO_API UVPBookmark(const UVPBookmark&); \
public:


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmark_h_47_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVPBookmark(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVPBookmark(UVPBookmark&&); \
	NO_API UVPBookmark(const UVPBookmark&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVPBookmark); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVPBookmark); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVPBookmark)


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmark_h_47_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bIsActive() { return STRUCT_OFFSET(UVPBookmark, bIsActive); }


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmark_h_44_PROLOG
#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmark_h_47_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmark_h_47_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmark_h_47_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmark_h_47_RPC_WRAPPERS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmark_h_47_INCLASS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmark_h_47_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmark_h_47_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmark_h_47_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmark_h_47_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmark_h_47_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmark_h_47_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmark_h_47_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VPBOOKMARK_API UClass* StaticClass<class UVPBookmark>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualProductionUtilities_Source_VPBookmark_Public_VPBookmark_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
