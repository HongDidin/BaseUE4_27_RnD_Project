// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_AnimNode_ControlRigBase_generated_h
#error "AnimNode_ControlRigBase.generated.h already included, missing '#pragma once' in AnimNode_ControlRigBase.h"
#endif
#define CONTROLRIG_AnimNode_ControlRigBase_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_AnimNode_ControlRigBase_h_47_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__Source() { return STRUCT_OFFSET(FAnimNode_ControlRigBase, Source); } \
	FORCEINLINE static uint32 __PPO__ControlRigBoneMapping() { return STRUCT_OFFSET(FAnimNode_ControlRigBase, ControlRigBoneMapping); } \
	FORCEINLINE static uint32 __PPO__ControlRigCurveMapping() { return STRUCT_OFFSET(FAnimNode_ControlRigBase, ControlRigCurveMapping); } \
	FORCEINLINE static uint32 __PPO__InputToCurveMappingUIDs() { return STRUCT_OFFSET(FAnimNode_ControlRigBase, InputToCurveMappingUIDs); } \
	FORCEINLINE static uint32 __PPO__NodeMappingContainer() { return STRUCT_OFFSET(FAnimNode_ControlRigBase, NodeMappingContainer); } \
	FORCEINLINE static uint32 __PPO__InputSettings() { return STRUCT_OFFSET(FAnimNode_ControlRigBase, InputSettings); } \
	FORCEINLINE static uint32 __PPO__OutputSettings() { return STRUCT_OFFSET(FAnimNode_ControlRigBase, OutputSettings); } \
	FORCEINLINE static uint32 __PPO__bExecute() { return STRUCT_OFFSET(FAnimNode_ControlRigBase, bExecute); } \
	typedef FAnimNode_CustomProperty Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FAnimNode_ControlRigBase>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_AnimNode_ControlRigBase_h_15_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FControlRigIOSettings_Statics; \
	static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FControlRigIOSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_AnimNode_ControlRigBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
