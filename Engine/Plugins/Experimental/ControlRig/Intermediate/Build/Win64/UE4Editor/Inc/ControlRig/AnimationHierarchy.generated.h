// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_AnimationHierarchy_generated_h
#error "AnimationHierarchy.generated.h already included, missing '#pragma once' in AnimationHierarchy.h"
#endif
#define CONTROLRIG_AnimationHierarchy_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_AnimationHierarchy_h_38_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAnimationHierarchy_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	typedef FNodeHierarchyWithUserData Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FAnimationHierarchy>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_AnimationHierarchy_h_11_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConstraintNodeData_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__Constraints() { return STRUCT_OFFSET(FConstraintNodeData, Constraints); }


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FConstraintNodeData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_AnimationHierarchy_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
