// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigUnit_DebugLineStrip_generated_h
#error "RigUnit_DebugLineStrip.generated.h already included, missing '#pragma once' in RigUnit_DebugLineStrip.h"
#endif
#define CONTROLRIG_RigUnit_DebugLineStrip_generated_h


#define FRigUnit_DebugLineStripItemSpace_Execute() \
	void FRigUnit_DebugLineStripItemSpace::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FVector>& Points, \
		const FLinearColor& Color, \
		const float Thickness, \
		const FRigElementKey& Space, \
		const FTransform& WorldOffset, \
		const bool bEnabled, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Debug_RigUnit_DebugLineStrip_h_49_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_DebugLineStripItemSpace_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FVector>& Points, \
		const FLinearColor& Color, \
		const float Thickness, \
		const FRigElementKey& Space, \
		const FTransform& WorldOffset, \
		const bool bEnabled, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		FRigVMFixedArray<FVector> Points((FVector*)RigVMMemoryHandles[0].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[1].GetData())); \
		const FLinearColor& Color = *(FLinearColor*)RigVMMemoryHandles[2].GetData(); \
		const float Thickness = *(float*)RigVMMemoryHandles[3].GetData(); \
		const FRigElementKey& Space = *(FRigElementKey*)RigVMMemoryHandles[4].GetData(); \
		const FTransform& WorldOffset = *(FTransform*)RigVMMemoryHandles[5].GetData(); \
		const bool bEnabled = *(bool*)RigVMMemoryHandles[6].GetData(); \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[7].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Points, \
			Color, \
			Thickness, \
			Space, \
			WorldOffset, \
			bEnabled, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnit_DebugBaseMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_DebugLineStripItemSpace>();


#define FRigUnit_DebugLineStrip_Execute() \
	void FRigUnit_DebugLineStrip::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FVector>& Points, \
		const FLinearColor& Color, \
		const float Thickness, \
		const FName& Space, \
		const FTransform& WorldOffset, \
		const bool bEnabled, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Debug_RigUnit_DebugLineStrip_h_11_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_DebugLineStrip_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FVector>& Points, \
		const FLinearColor& Color, \
		const float Thickness, \
		const FName& Space, \
		const FTransform& WorldOffset, \
		const bool bEnabled, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		FRigVMFixedArray<FVector> Points((FVector*)RigVMMemoryHandles[0].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[1].GetData())); \
		const FLinearColor& Color = *(FLinearColor*)RigVMMemoryHandles[2].GetData(); \
		const float Thickness = *(float*)RigVMMemoryHandles[3].GetData(); \
		const FName& Space = *(FName*)RigVMMemoryHandles[4].GetData(); \
		const FTransform& WorldOffset = *(FTransform*)RigVMMemoryHandles[5].GetData(); \
		const bool bEnabled = *(bool*)RigVMMemoryHandles[6].GetData(); \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[7].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Points, \
			Color, \
			Thickness, \
			Space, \
			WorldOffset, \
			bEnabled, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnit_DebugBaseMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_DebugLineStrip>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Debug_RigUnit_DebugLineStrip_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
