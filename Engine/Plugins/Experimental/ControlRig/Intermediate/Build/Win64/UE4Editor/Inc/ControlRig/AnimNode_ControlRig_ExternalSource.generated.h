// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_AnimNode_ControlRig_ExternalSource_generated_h
#error "AnimNode_ControlRig_ExternalSource.generated.h already included, missing '#pragma once' in AnimNode_ControlRig_ExternalSource.h"
#endif
#define CONTROLRIG_AnimNode_ControlRig_ExternalSource_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_AnimNode_ControlRig_ExternalSource_h_14_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAnimNode_ControlRig_ExternalSource_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__ControlRig() { return STRUCT_OFFSET(FAnimNode_ControlRig_ExternalSource, ControlRig); } \
	typedef FAnimNode_ControlRigBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FAnimNode_ControlRig_ExternalSource>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_AnimNode_ControlRig_ExternalSource_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
