// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Animation/RigUnit_TimeConversion.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_TimeConversion() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_SecondsToFrames();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_AnimBase();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_FramesToSeconds();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_SecondsToFrames>() == std::is_polymorphic<FRigUnit_AnimBase>(), "USTRUCT FRigUnit_SecondsToFrames cannot be polymorphic unless super FRigUnit_AnimBase is polymorphic");

class UScriptStruct* FRigUnit_SecondsToFrames::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_SecondsToFrames_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_SecondsToFrames, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_SecondsToFrames"), sizeof(FRigUnit_SecondsToFrames), Get_Z_Construct_UScriptStruct_FRigUnit_SecondsToFrames_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_SecondsToFrames::Execute"), &FRigUnit_SecondsToFrames::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_SecondsToFrames>()
{
	return FRigUnit_SecondsToFrames::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_SecondsToFrames(FRigUnit_SecondsToFrames::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_SecondsToFrames"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_SecondsToFrames
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_SecondsToFrames()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_SecondsToFrames>(FName(TEXT("RigUnit_SecondsToFrames")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_SecondsToFrames;
	struct Z_Construct_UScriptStruct_FRigUnit_SecondsToFrames_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Seconds_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Seconds;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Frames_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Frames;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_SecondsToFrames_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Converts seconds to frames based on the current frame rate\n */" },
		{ "DisplayName", "Seconds to Frames" },
		{ "ModuleRelativePath", "Private/Units/Animation/RigUnit_TimeConversion.h" },
		{ "ToolTip", "Converts seconds to frames based on the current frame rate" },
		{ "Varying", "" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_SecondsToFrames_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_SecondsToFrames>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_SecondsToFrames_Statics::NewProp_Seconds_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Animation/RigUnit_TimeConversion.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_SecondsToFrames_Statics::NewProp_Seconds = { "Seconds", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_SecondsToFrames, Seconds), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_SecondsToFrames_Statics::NewProp_Seconds_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_SecondsToFrames_Statics::NewProp_Seconds_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_SecondsToFrames_Statics::NewProp_Frames_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Animation/RigUnit_TimeConversion.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_SecondsToFrames_Statics::NewProp_Frames = { "Frames", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_SecondsToFrames, Frames), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_SecondsToFrames_Statics::NewProp_Frames_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_SecondsToFrames_Statics::NewProp_Frames_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_SecondsToFrames_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_SecondsToFrames_Statics::NewProp_Seconds,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_SecondsToFrames_Statics::NewProp_Frames,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_SecondsToFrames_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_AnimBase,
		&NewStructOps,
		"RigUnit_SecondsToFrames",
		sizeof(FRigUnit_SecondsToFrames),
		alignof(FRigUnit_SecondsToFrames),
		Z_Construct_UScriptStruct_FRigUnit_SecondsToFrames_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_SecondsToFrames_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_SecondsToFrames_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_SecondsToFrames_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_SecondsToFrames()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_SecondsToFrames_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_SecondsToFrames"), sizeof(FRigUnit_SecondsToFrames), Get_Z_Construct_UScriptStruct_FRigUnit_SecondsToFrames_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_SecondsToFrames_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_SecondsToFrames_Hash() { return 922856878U; }

void FRigUnit_SecondsToFrames::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Seconds,
		Frames,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_FramesToSeconds>() == std::is_polymorphic<FRigUnit_AnimBase>(), "USTRUCT FRigUnit_FramesToSeconds cannot be polymorphic unless super FRigUnit_AnimBase is polymorphic");

class UScriptStruct* FRigUnit_FramesToSeconds::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_FramesToSeconds_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_FramesToSeconds, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_FramesToSeconds"), sizeof(FRigUnit_FramesToSeconds), Get_Z_Construct_UScriptStruct_FRigUnit_FramesToSeconds_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_FramesToSeconds::Execute"), &FRigUnit_FramesToSeconds::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_FramesToSeconds>()
{
	return FRigUnit_FramesToSeconds::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_FramesToSeconds(FRigUnit_FramesToSeconds::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_FramesToSeconds"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_FramesToSeconds
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_FramesToSeconds()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_FramesToSeconds>(FName(TEXT("RigUnit_FramesToSeconds")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_FramesToSeconds;
	struct Z_Construct_UScriptStruct_FRigUnit_FramesToSeconds_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Frames_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Frames;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Seconds_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Seconds;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_FramesToSeconds_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Converts frames to seconds based on the current frame rate\n */" },
		{ "DisplayName", "Frames to Seconds" },
		{ "ModuleRelativePath", "Private/Units/Animation/RigUnit_TimeConversion.h" },
		{ "ToolTip", "Converts frames to seconds based on the current frame rate" },
		{ "Varying", "" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_FramesToSeconds_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_FramesToSeconds>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_FramesToSeconds_Statics::NewProp_Frames_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Animation/RigUnit_TimeConversion.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_FramesToSeconds_Statics::NewProp_Frames = { "Frames", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_FramesToSeconds, Frames), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_FramesToSeconds_Statics::NewProp_Frames_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_FramesToSeconds_Statics::NewProp_Frames_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_FramesToSeconds_Statics::NewProp_Seconds_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Animation/RigUnit_TimeConversion.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_FramesToSeconds_Statics::NewProp_Seconds = { "Seconds", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_FramesToSeconds, Seconds), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_FramesToSeconds_Statics::NewProp_Seconds_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_FramesToSeconds_Statics::NewProp_Seconds_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_FramesToSeconds_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_FramesToSeconds_Statics::NewProp_Frames,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_FramesToSeconds_Statics::NewProp_Seconds,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_FramesToSeconds_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_AnimBase,
		&NewStructOps,
		"RigUnit_FramesToSeconds",
		sizeof(FRigUnit_FramesToSeconds),
		alignof(FRigUnit_FramesToSeconds),
		Z_Construct_UScriptStruct_FRigUnit_FramesToSeconds_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_FramesToSeconds_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_FramesToSeconds_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_FramesToSeconds_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_FramesToSeconds()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_FramesToSeconds_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_FramesToSeconds"), sizeof(FRigUnit_FramesToSeconds), Get_Z_Construct_UScriptStruct_FRigUnit_FramesToSeconds_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_FramesToSeconds_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_FramesToSeconds_Hash() { return 335561518U; }

void FRigUnit_FramesToSeconds::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Frames,
		Seconds,
		Context
	);
}

PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
