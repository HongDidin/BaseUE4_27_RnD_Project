// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Public/Rigs/RigHierarchyDefines.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigHierarchyDefines() {}
// Cross Module References
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_EControlRigSetKey();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_ERigEvent();
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_ERigElementType();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigEventContext();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigElement();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigElementKeyCollection();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigElementKey();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigControlModifiedContext();
// End Cross Module References
	static UEnum* EControlRigSetKey_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ControlRig_EControlRigSetKey, Z_Construct_UPackage__Script_ControlRig(), TEXT("EControlRigSetKey"));
		}
		return Singleton;
	}
	template<> CONTROLRIG_API UEnum* StaticEnum<EControlRigSetKey>()
	{
		return EControlRigSetKey_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EControlRigSetKey(EControlRigSetKey_StaticEnum, TEXT("/Script/ControlRig"), TEXT("EControlRigSetKey"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ControlRig_EControlRigSetKey_Hash() { return 3730919110U; }
	UEnum* Z_Construct_UEnum_ControlRig_EControlRigSetKey()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EControlRigSetKey"), 0, Get_Z_Construct_UEnum_ControlRig_EControlRigSetKey_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EControlRigSetKey::DoNotCare", (int64)EControlRigSetKey::DoNotCare },
				{ "EControlRigSetKey::Always", (int64)EControlRigSetKey::Always },
				{ "EControlRigSetKey::Never", (int64)EControlRigSetKey::Never },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Always.Comment", "//Don't care if a key is set or not, may get set, say if auto key is on somewhere.\n" },
				{ "Always.Name", "EControlRigSetKey::Always" },
				{ "Always.ToolTip", "Don't care if a key is set or not, may get set, say if auto key is on somewhere." },
				{ "Comment", "/** When setting control values what to do with regards to setting key.*/" },
				{ "DoNotCare.Name", "EControlRigSetKey::DoNotCare" },
				{ "ModuleRelativePath", "Public/Rigs/RigHierarchyDefines.h" },
				{ "Never.Comment", "//Always set a key here\n" },
				{ "Never.Name", "EControlRigSetKey::Never" },
				{ "Never.ToolTip", "Always set a key here" },
				{ "ToolTip", "When setting control values what to do with regards to setting key." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ControlRig,
				nullptr,
				"EControlRigSetKey",
				"EControlRigSetKey",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ERigEvent_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ControlRig_ERigEvent, Z_Construct_UPackage__Script_ControlRig(), TEXT("ERigEvent"));
		}
		return Singleton;
	}
	template<> CONTROLRIG_API UEnum* StaticEnum<ERigEvent>()
	{
		return ERigEvent_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ERigEvent(ERigEvent_StaticEnum, TEXT("/Script/ControlRig"), TEXT("ERigEvent"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ControlRig_ERigEvent_Hash() { return 1472470626U; }
	UEnum* Z_Construct_UEnum_ControlRig_ERigEvent()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ERigEvent"), 0, Get_Z_Construct_UEnum_ControlRig_ERigEvent_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ERigEvent::None", (int64)ERigEvent::None },
				{ "ERigEvent::RequestAutoKey", (int64)ERigEvent::RequestAutoKey },
				{ "ERigEvent::Max", (int64)ERigEvent::Max },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Max.Comment", "/** MAX - invalid */" },
				{ "Max.Hidden", "" },
				{ "Max.Name", "ERigEvent::Max" },
				{ "Max.ToolTip", "MAX - invalid" },
				{ "ModuleRelativePath", "Public/Rigs/RigHierarchyDefines.h" },
				{ "None.Comment", "/** Invalid event */" },
				{ "None.Name", "ERigEvent::None" },
				{ "None.ToolTip", "Invalid event" },
				{ "RequestAutoKey.Comment", "/** Request to Auto-Key the Control in Sequencer */" },
				{ "RequestAutoKey.Name", "ERigEvent::RequestAutoKey" },
				{ "RequestAutoKey.ToolTip", "Request to Auto-Key the Control in Sequencer" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ControlRig,
				nullptr,
				"ERigEvent",
				"ERigEvent",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ERigElementType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ControlRig_ERigElementType, Z_Construct_UPackage__Script_ControlRig(), TEXT("ERigElementType"));
		}
		return Singleton;
	}
	template<> CONTROLRIG_API UEnum* StaticEnum<ERigElementType>()
	{
		return ERigElementType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ERigElementType(ERigElementType_StaticEnum, TEXT("/Script/ControlRig"), TEXT("ERigElementType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ControlRig_ERigElementType_Hash() { return 3309475767U; }
	UEnum* Z_Construct_UEnum_ControlRig_ERigElementType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ERigElementType"), 0, Get_Z_Construct_UEnum_ControlRig_ERigElementType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ERigElementType::None", (int64)ERigElementType::None },
				{ "ERigElementType::Bone", (int64)ERigElementType::Bone },
				{ "ERigElementType::Space", (int64)ERigElementType::Space },
				{ "ERigElementType::Control", (int64)ERigElementType::Control },
				{ "ERigElementType::Curve", (int64)ERigElementType::Curve },
				{ "ERigElementType::All", (int64)ERigElementType::All },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "All.Name", "ERigElementType::All" },
				{ "BlueprintType", "true" },
				{ "Bone.Name", "ERigElementType::Bone" },
				{ "Comment", "/* \n * This is rig element types that we support\n * This can be used as a mask so supported as a bitfield\n */" },
				{ "Control.Name", "ERigElementType::Control" },
				{ "Curve.Name", "ERigElementType::Curve" },
				{ "ModuleRelativePath", "Public/Rigs/RigHierarchyDefines.h" },
				{ "None.Name", "ERigElementType::None" },
				{ "Space.Name", "ERigElementType::Space" },
				{ "ToolTip", "* This is rig element types that we support\n* This can be used as a mask so supported as a bitfield" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ControlRig,
				nullptr,
				"ERigElementType",
				"ERigElementType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FRigEventContext::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigEventContext_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigEventContext, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigEventContext"), sizeof(FRigEventContext), Get_Z_Construct_UScriptStruct_FRigEventContext_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigEventContext>()
{
	return FRigEventContext::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigEventContext(FRigEventContext::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigEventContext"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigEventContext
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigEventContext()
	{
		UScriptStruct::DeferCppStructOps<FRigEventContext>(FName(TEXT("RigEventContext")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigEventContext;
	struct Z_Construct_UScriptStruct_FRigEventContext_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigEventContext_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Rigs/RigHierarchyDefines.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigEventContext_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigEventContext>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigEventContext_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"RigEventContext",
		sizeof(FRigEventContext),
		alignof(FRigEventContext),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigEventContext_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigEventContext_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigEventContext()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigEventContext_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigEventContext"), sizeof(FRigEventContext), Get_Z_Construct_UScriptStruct_FRigEventContext_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigEventContext_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigEventContext_Hash() { return 4047896023U; }
class UScriptStruct* FRigElement::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigElement_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigElement, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigElement"), sizeof(FRigElement), Get_Z_Construct_UScriptStruct_FRigElement_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigElement>()
{
	return FRigElement::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigElement(FRigElement::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigElement"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigElement
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigElement()
	{
		UScriptStruct::DeferCppStructOps<FRigElement>(FName(TEXT("RigElement")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigElement;
	struct Z_Construct_UScriptStruct_FRigElement_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigElement_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Rigs/RigHierarchyDefines.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigElement_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigElement>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigElement_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "FRigElement" },
		{ "ModuleRelativePath", "Public/Rigs/RigHierarchyDefines.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigElement_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigElement, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigElement_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigElement_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigElement_Statics::NewProp_Index_MetaData[] = {
		{ "Category", "FRigElement" },
		{ "ModuleRelativePath", "Public/Rigs/RigHierarchyDefines.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigElement_Statics::NewProp_Index = { "Index", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigElement, Index), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigElement_Statics::NewProp_Index_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigElement_Statics::NewProp_Index_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigElement_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigElement_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigElement_Statics::NewProp_Index,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigElement_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"RigElement",
		sizeof(FRigElement),
		alignof(FRigElement),
		Z_Construct_UScriptStruct_FRigElement_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigElement_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigElement_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigElement_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigElement()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigElement_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigElement"), sizeof(FRigElement), Get_Z_Construct_UScriptStruct_FRigElement_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigElement_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigElement_Hash() { return 2036232166U; }
class UScriptStruct* FRigElementKeyCollection::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigElementKeyCollection_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigElementKeyCollection, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigElementKeyCollection"), sizeof(FRigElementKeyCollection), Get_Z_Construct_UScriptStruct_FRigElementKeyCollection_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigElementKeyCollection>()
{
	return FRigElementKeyCollection::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigElementKeyCollection(FRigElementKeyCollection::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigElementKeyCollection"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigElementKeyCollection
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigElementKeyCollection()
	{
		UScriptStruct::DeferCppStructOps<FRigElementKeyCollection>(FName(TEXT("RigElementKeyCollection")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigElementKeyCollection;
	struct Z_Construct_UScriptStruct_FRigElementKeyCollection_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigElementKeyCollection_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Rigs/RigHierarchyDefines.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigElementKeyCollection_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigElementKeyCollection>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigElementKeyCollection_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"RigElementKeyCollection",
		sizeof(FRigElementKeyCollection),
		alignof(FRigElementKeyCollection),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigElementKeyCollection_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigElementKeyCollection_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigElementKeyCollection()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigElementKeyCollection_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigElementKeyCollection"), sizeof(FRigElementKeyCollection), Get_Z_Construct_UScriptStruct_FRigElementKeyCollection_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigElementKeyCollection_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigElementKeyCollection_Hash() { return 2827972560U; }
class UScriptStruct* FRigElementKey::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigElementKey_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigElementKey, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigElementKey"), sizeof(FRigElementKey), Get_Z_Construct_UScriptStruct_FRigElementKey_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigElementKey>()
{
	return FRigElementKey::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigElementKey(FRigElementKey::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigElementKey"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigElementKey
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigElementKey()
	{
		UScriptStruct::DeferCppStructOps<FRigElementKey>(FName(TEXT("RigElementKey")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigElementKey;
	struct Z_Construct_UScriptStruct_FRigElementKey_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Type_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Type_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Type;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigElementKey_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Rigs/RigHierarchyDefines.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigElementKey_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigElementKey>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRigElementKey_Statics::NewProp_Type_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigElementKey_Statics::NewProp_Type_MetaData[] = {
		{ "Category", "Hierarchy" },
		{ "ModuleRelativePath", "Public/Rigs/RigHierarchyDefines.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FRigElementKey_Statics::NewProp_Type = { "Type", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigElementKey, Type), Z_Construct_UEnum_ControlRig_ERigElementType, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigElementKey_Statics::NewProp_Type_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigElementKey_Statics::NewProp_Type_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigElementKey_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "Hierarchy" },
		{ "CustomWidget", "ElementName" },
		{ "ModuleRelativePath", "Public/Rigs/RigHierarchyDefines.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigElementKey_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigElementKey, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigElementKey_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigElementKey_Statics::NewProp_Name_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigElementKey_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigElementKey_Statics::NewProp_Type_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigElementKey_Statics::NewProp_Type,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigElementKey_Statics::NewProp_Name,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigElementKey_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"RigElementKey",
		sizeof(FRigElementKey),
		alignof(FRigElementKey),
		Z_Construct_UScriptStruct_FRigElementKey_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigElementKey_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigElementKey_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigElementKey_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigElementKey()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigElementKey_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigElementKey"), sizeof(FRigElementKey), Get_Z_Construct_UScriptStruct_FRigElementKey_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigElementKey_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigElementKey_Hash() { return 1905148179U; }
class UScriptStruct* FRigControlModifiedContext::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigControlModifiedContext_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigControlModifiedContext, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigControlModifiedContext"), sizeof(FRigControlModifiedContext), Get_Z_Construct_UScriptStruct_FRigControlModifiedContext_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigControlModifiedContext>()
{
	return FRigControlModifiedContext::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigControlModifiedContext(FRigControlModifiedContext::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigControlModifiedContext"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigControlModifiedContext
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigControlModifiedContext()
	{
		UScriptStruct::DeferCppStructOps<FRigControlModifiedContext>(FName(TEXT("RigControlModifiedContext")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigControlModifiedContext;
	struct Z_Construct_UScriptStruct_FRigControlModifiedContext_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControlModifiedContext_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Rigs/RigHierarchyDefines.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigControlModifiedContext_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigControlModifiedContext>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigControlModifiedContext_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"RigControlModifiedContext",
		sizeof(FRigControlModifiedContext),
		alignof(FRigControlModifiedContext),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControlModifiedContext_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControlModifiedContext_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigControlModifiedContext()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigControlModifiedContext_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigControlModifiedContext"), sizeof(FRigControlModifiedContext), Get_Z_Construct_UScriptStruct_FRigControlModifiedContext_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigControlModifiedContext_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigControlModifiedContext_Hash() { return 2761969211U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
