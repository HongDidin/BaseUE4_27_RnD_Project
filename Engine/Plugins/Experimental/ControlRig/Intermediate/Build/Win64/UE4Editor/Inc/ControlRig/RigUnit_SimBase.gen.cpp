// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Simulation/RigUnit_SimBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_SimBase() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_SimBaseMutable();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnitMutable();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_SimBase();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_SimBaseMutable>() == std::is_polymorphic<FRigUnitMutable>(), "USTRUCT FRigUnit_SimBaseMutable cannot be polymorphic unless super FRigUnitMutable is polymorphic");

class UScriptStruct* FRigUnit_SimBaseMutable::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_SimBaseMutable_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_SimBaseMutable, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_SimBaseMutable"), sizeof(FRigUnit_SimBaseMutable), Get_Z_Construct_UScriptStruct_FRigUnit_SimBaseMutable_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_SimBaseMutable>()
{
	return FRigUnit_SimBaseMutable::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_SimBaseMutable(FRigUnit_SimBaseMutable::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_SimBaseMutable"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_SimBaseMutable
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_SimBaseMutable()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_SimBaseMutable>(FName(TEXT("RigUnit_SimBaseMutable")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_SimBaseMutable;
	struct Z_Construct_UScriptStruct_FRigUnit_SimBaseMutable_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_SimBaseMutable_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "Category", "Simulation" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_SimBase.h" },
		{ "NodeColor", "0.25 0.05 0.05" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_SimBaseMutable_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_SimBaseMutable>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_SimBaseMutable_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnitMutable,
		&NewStructOps,
		"RigUnit_SimBaseMutable",
		sizeof(FRigUnit_SimBaseMutable),
		alignof(FRigUnit_SimBaseMutable),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_SimBaseMutable_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_SimBaseMutable_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_SimBaseMutable()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_SimBaseMutable_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_SimBaseMutable"), sizeof(FRigUnit_SimBaseMutable), Get_Z_Construct_UScriptStruct_FRigUnit_SimBaseMutable_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_SimBaseMutable_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_SimBaseMutable_Hash() { return 1063333341U; }

static_assert(std::is_polymorphic<FRigUnit_SimBase>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_SimBase cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_SimBase::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_SimBase_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_SimBase, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_SimBase"), sizeof(FRigUnit_SimBase), Get_Z_Construct_UScriptStruct_FRigUnit_SimBase_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_SimBase>()
{
	return FRigUnit_SimBase::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_SimBase(FRigUnit_SimBase::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_SimBase"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_SimBase
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_SimBase()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_SimBase>(FName(TEXT("RigUnit_SimBase")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_SimBase;
	struct Z_Construct_UScriptStruct_FRigUnit_SimBase_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_SimBase_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "Category", "Simulation" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_SimBase.h" },
		{ "NodeColor", "0.25 0.05 0.05" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_SimBase_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_SimBase>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_SimBase_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_SimBase",
		sizeof(FRigUnit_SimBase),
		alignof(FRigUnit_SimBase),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_SimBase_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_SimBase_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_SimBase()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_SimBase_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_SimBase"), sizeof(FRigUnit_SimBase), Get_Z_Construct_UScriptStruct_FRigUnit_SimBase_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_SimBase_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_SimBase_Hash() { return 882962390U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
