// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Public/Sequencer/MovieSceneControlRigParameterSection.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMovieSceneControlRigParameterSection() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FChannelMapInfo();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FIntegerParameterNameAndCurve();
	MOVIESCENE_API UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneIntegerChannel();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FEnumParameterNameAndCurve();
	MOVIESCENE_API UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneByteChannel();
	CONTROLRIG_API UClass* Z_Construct_UClass_UMovieSceneControlRigParameterSection_NoRegister();
	CONTROLRIG_API UClass* Z_Construct_UClass_UMovieSceneControlRigParameterSection();
	MOVIESCENETRACKS_API UClass* Z_Construct_UClass_UMovieSceneParameterSection();
	CONTROLRIG_API UClass* Z_Construct_UClass_UControlRig_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	MOVIESCENETRACKS_API UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneTransformMask();
	MOVIESCENE_API UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneFloatChannel();
// End Cross Module References
class UScriptStruct* FChannelMapInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FChannelMapInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FChannelMapInfo, Z_Construct_UPackage__Script_ControlRig(), TEXT("ChannelMapInfo"), sizeof(FChannelMapInfo), Get_Z_Construct_UScriptStruct_FChannelMapInfo_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FChannelMapInfo>()
{
	return FChannelMapInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FChannelMapInfo(FChannelMapInfo::StaticStruct, TEXT("/Script/ControlRig"), TEXT("ChannelMapInfo"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFChannelMapInfo
{
	FScriptStruct_ControlRig_StaticRegisterNativesFChannelMapInfo()
	{
		UScriptStruct::DeferCppStructOps<FChannelMapInfo>(FName(TEXT("ChannelMapInfo")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFChannelMapInfo;
	struct Z_Construct_UScriptStruct_FChannelMapInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ControlIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ControlIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TotalChannelIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_TotalChannelIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChannelIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ChannelIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParentControlIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ParentControlIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChannelTypeName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ChannelTypeName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FChannelMapInfo_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneControlRigParameterSection.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FChannelMapInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FChannelMapInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FChannelMapInfo_Statics::NewProp_ControlIndex_MetaData[] = {
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneControlRigParameterSection.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FChannelMapInfo_Statics::NewProp_ControlIndex = { "ControlIndex", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FChannelMapInfo, ControlIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FChannelMapInfo_Statics::NewProp_ControlIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FChannelMapInfo_Statics::NewProp_ControlIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FChannelMapInfo_Statics::NewProp_TotalChannelIndex_MetaData[] = {
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneControlRigParameterSection.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FChannelMapInfo_Statics::NewProp_TotalChannelIndex = { "TotalChannelIndex", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FChannelMapInfo, TotalChannelIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FChannelMapInfo_Statics::NewProp_TotalChannelIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FChannelMapInfo_Statics::NewProp_TotalChannelIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FChannelMapInfo_Statics::NewProp_ChannelIndex_MetaData[] = {
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneControlRigParameterSection.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FChannelMapInfo_Statics::NewProp_ChannelIndex = { "ChannelIndex", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FChannelMapInfo, ChannelIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FChannelMapInfo_Statics::NewProp_ChannelIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FChannelMapInfo_Statics::NewProp_ChannelIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FChannelMapInfo_Statics::NewProp_ParentControlIndex_MetaData[] = {
		{ "Comment", "//channel index for it's type.. (e.g  float, int, bool).\n" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneControlRigParameterSection.h" },
		{ "ToolTip", "channel index for it's type.. (e.g  float, int, bool)." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FChannelMapInfo_Statics::NewProp_ParentControlIndex = { "ParentControlIndex", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FChannelMapInfo, ParentControlIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FChannelMapInfo_Statics::NewProp_ParentControlIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FChannelMapInfo_Statics::NewProp_ParentControlIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FChannelMapInfo_Statics::NewProp_ChannelTypeName_MetaData[] = {
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneControlRigParameterSection.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FChannelMapInfo_Statics::NewProp_ChannelTypeName = { "ChannelTypeName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FChannelMapInfo, ChannelTypeName), METADATA_PARAMS(Z_Construct_UScriptStruct_FChannelMapInfo_Statics::NewProp_ChannelTypeName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FChannelMapInfo_Statics::NewProp_ChannelTypeName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FChannelMapInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FChannelMapInfo_Statics::NewProp_ControlIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FChannelMapInfo_Statics::NewProp_TotalChannelIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FChannelMapInfo_Statics::NewProp_ChannelIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FChannelMapInfo_Statics::NewProp_ParentControlIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FChannelMapInfo_Statics::NewProp_ChannelTypeName,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FChannelMapInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"ChannelMapInfo",
		sizeof(FChannelMapInfo),
		alignof(FChannelMapInfo),
		Z_Construct_UScriptStruct_FChannelMapInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FChannelMapInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FChannelMapInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FChannelMapInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FChannelMapInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FChannelMapInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ChannelMapInfo"), sizeof(FChannelMapInfo), Get_Z_Construct_UScriptStruct_FChannelMapInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FChannelMapInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FChannelMapInfo_Hash() { return 2975129255U; }
class UScriptStruct* FIntegerParameterNameAndCurve::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FIntegerParameterNameAndCurve_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FIntegerParameterNameAndCurve, Z_Construct_UPackage__Script_ControlRig(), TEXT("IntegerParameterNameAndCurve"), sizeof(FIntegerParameterNameAndCurve), Get_Z_Construct_UScriptStruct_FIntegerParameterNameAndCurve_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FIntegerParameterNameAndCurve>()
{
	return FIntegerParameterNameAndCurve::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FIntegerParameterNameAndCurve(FIntegerParameterNameAndCurve::StaticStruct, TEXT("/Script/ControlRig"), TEXT("IntegerParameterNameAndCurve"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFIntegerParameterNameAndCurve
{
	FScriptStruct_ControlRig_StaticRegisterNativesFIntegerParameterNameAndCurve()
	{
		UScriptStruct::DeferCppStructOps<FIntegerParameterNameAndCurve>(FName(TEXT("IntegerParameterNameAndCurve")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFIntegerParameterNameAndCurve;
	struct Z_Construct_UScriptStruct_FIntegerParameterNameAndCurve_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParameterName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ParameterName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParameterCurve_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ParameterCurve;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIntegerParameterNameAndCurve_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneControlRigParameterSection.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FIntegerParameterNameAndCurve_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FIntegerParameterNameAndCurve>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIntegerParameterNameAndCurve_Statics::NewProp_ParameterName_MetaData[] = {
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneControlRigParameterSection.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FIntegerParameterNameAndCurve_Statics::NewProp_ParameterName = { "ParameterName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FIntegerParameterNameAndCurve, ParameterName), METADATA_PARAMS(Z_Construct_UScriptStruct_FIntegerParameterNameAndCurve_Statics::NewProp_ParameterName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FIntegerParameterNameAndCurve_Statics::NewProp_ParameterName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIntegerParameterNameAndCurve_Statics::NewProp_ParameterCurve_MetaData[] = {
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneControlRigParameterSection.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FIntegerParameterNameAndCurve_Statics::NewProp_ParameterCurve = { "ParameterCurve", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FIntegerParameterNameAndCurve, ParameterCurve), Z_Construct_UScriptStruct_FMovieSceneIntegerChannel, METADATA_PARAMS(Z_Construct_UScriptStruct_FIntegerParameterNameAndCurve_Statics::NewProp_ParameterCurve_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FIntegerParameterNameAndCurve_Statics::NewProp_ParameterCurve_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FIntegerParameterNameAndCurve_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIntegerParameterNameAndCurve_Statics::NewProp_ParameterName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIntegerParameterNameAndCurve_Statics::NewProp_ParameterCurve,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FIntegerParameterNameAndCurve_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"IntegerParameterNameAndCurve",
		sizeof(FIntegerParameterNameAndCurve),
		alignof(FIntegerParameterNameAndCurve),
		Z_Construct_UScriptStruct_FIntegerParameterNameAndCurve_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FIntegerParameterNameAndCurve_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FIntegerParameterNameAndCurve_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FIntegerParameterNameAndCurve_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FIntegerParameterNameAndCurve()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FIntegerParameterNameAndCurve_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("IntegerParameterNameAndCurve"), sizeof(FIntegerParameterNameAndCurve), Get_Z_Construct_UScriptStruct_FIntegerParameterNameAndCurve_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FIntegerParameterNameAndCurve_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FIntegerParameterNameAndCurve_Hash() { return 2844378607U; }
class UScriptStruct* FEnumParameterNameAndCurve::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FEnumParameterNameAndCurve_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FEnumParameterNameAndCurve, Z_Construct_UPackage__Script_ControlRig(), TEXT("EnumParameterNameAndCurve"), sizeof(FEnumParameterNameAndCurve), Get_Z_Construct_UScriptStruct_FEnumParameterNameAndCurve_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FEnumParameterNameAndCurve>()
{
	return FEnumParameterNameAndCurve::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FEnumParameterNameAndCurve(FEnumParameterNameAndCurve::StaticStruct, TEXT("/Script/ControlRig"), TEXT("EnumParameterNameAndCurve"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFEnumParameterNameAndCurve
{
	FScriptStruct_ControlRig_StaticRegisterNativesFEnumParameterNameAndCurve()
	{
		UScriptStruct::DeferCppStructOps<FEnumParameterNameAndCurve>(FName(TEXT("EnumParameterNameAndCurve")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFEnumParameterNameAndCurve;
	struct Z_Construct_UScriptStruct_FEnumParameterNameAndCurve_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParameterName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ParameterName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParameterCurve_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ParameterCurve;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEnumParameterNameAndCurve_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneControlRigParameterSection.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FEnumParameterNameAndCurve_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FEnumParameterNameAndCurve>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEnumParameterNameAndCurve_Statics::NewProp_ParameterName_MetaData[] = {
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneControlRigParameterSection.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FEnumParameterNameAndCurve_Statics::NewProp_ParameterName = { "ParameterName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FEnumParameterNameAndCurve, ParameterName), METADATA_PARAMS(Z_Construct_UScriptStruct_FEnumParameterNameAndCurve_Statics::NewProp_ParameterName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEnumParameterNameAndCurve_Statics::NewProp_ParameterName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEnumParameterNameAndCurve_Statics::NewProp_ParameterCurve_MetaData[] = {
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneControlRigParameterSection.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FEnumParameterNameAndCurve_Statics::NewProp_ParameterCurve = { "ParameterCurve", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FEnumParameterNameAndCurve, ParameterCurve), Z_Construct_UScriptStruct_FMovieSceneByteChannel, METADATA_PARAMS(Z_Construct_UScriptStruct_FEnumParameterNameAndCurve_Statics::NewProp_ParameterCurve_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEnumParameterNameAndCurve_Statics::NewProp_ParameterCurve_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FEnumParameterNameAndCurve_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEnumParameterNameAndCurve_Statics::NewProp_ParameterName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEnumParameterNameAndCurve_Statics::NewProp_ParameterCurve,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FEnumParameterNameAndCurve_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"EnumParameterNameAndCurve",
		sizeof(FEnumParameterNameAndCurve),
		alignof(FEnumParameterNameAndCurve),
		Z_Construct_UScriptStruct_FEnumParameterNameAndCurve_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEnumParameterNameAndCurve_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FEnumParameterNameAndCurve_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEnumParameterNameAndCurve_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FEnumParameterNameAndCurve()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FEnumParameterNameAndCurve_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("EnumParameterNameAndCurve"), sizeof(FEnumParameterNameAndCurve), Get_Z_Construct_UScriptStruct_FEnumParameterNameAndCurve_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FEnumParameterNameAndCurve_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FEnumParameterNameAndCurve_Hash() { return 1873087075U; }
	void UMovieSceneControlRigParameterSection::StaticRegisterNativesUMovieSceneControlRigParameterSection()
	{
	}
	UClass* Z_Construct_UClass_UMovieSceneControlRigParameterSection_NoRegister()
	{
		return UMovieSceneControlRigParameterSection::StaticClass();
	}
	struct Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ControlRig_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ControlRig;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ControlRigClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ControlRigClass;
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ControlsMask_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ControlsMask_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ControlsMask;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransformMask_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TransformMask;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Weight_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Weight;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ControlChannelMap_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ControlChannelMap_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ControlChannelMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_ControlChannelMap;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EnumParameterNamesAndCurves_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EnumParameterNamesAndCurves_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_EnumParameterNamesAndCurves;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_IntegerParameterNamesAndCurves_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IntegerParameterNamesAndCurves_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_IntegerParameterNamesAndCurves;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMovieSceneParameterSection,
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Movie scene section that controls animation controller animation\n */" },
		{ "IncludePath", "Sequencer/MovieSceneControlRigParameterSection.h" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneControlRigParameterSection.h" },
		{ "ToolTip", "Movie scene section that controls animation controller animation" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_ControlRig_MetaData[] = {
		{ "Comment", "/** Control Rig that controls us*/" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneControlRigParameterSection.h" },
		{ "ToolTip", "Control Rig that controls us" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_ControlRig = { "ControlRig", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMovieSceneControlRigParameterSection, ControlRig), Z_Construct_UClass_UControlRig_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_ControlRig_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_ControlRig_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_ControlRigClass_MetaData[] = {
		{ "Category", "Animation" },
		{ "Comment", "/** The class of control rig to instantiate */" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneControlRigParameterSection.h" },
		{ "ToolTip", "The class of control rig to instantiate" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_ControlRigClass = { "ControlRigClass", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMovieSceneControlRigParameterSection, ControlRigClass), Z_Construct_UClass_UControlRig_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_ControlRigClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_ControlRigClass_MetaData)) };
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_ControlsMask_Inner = { "ControlsMask", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_ControlsMask_MetaData[] = {
		{ "Comment", "/** Mask for controls themselves*/" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneControlRigParameterSection.h" },
		{ "ToolTip", "Mask for controls themselves" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_ControlsMask = { "ControlsMask", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMovieSceneControlRigParameterSection, ControlsMask), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_ControlsMask_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_ControlsMask_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_TransformMask_MetaData[] = {
		{ "Comment", "/** Mask for Transform Mask*/" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneControlRigParameterSection.h" },
		{ "ToolTip", "Mask for Transform Mask" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_TransformMask = { "TransformMask", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMovieSceneControlRigParameterSection, TransformMask), Z_Construct_UScriptStruct_FMovieSceneTransformMask, METADATA_PARAMS(Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_TransformMask_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_TransformMask_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_Weight_MetaData[] = {
		{ "Comment", "/** The weight curve for this animation controller section */" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneControlRigParameterSection.h" },
		{ "ToolTip", "The weight curve for this animation controller section" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_Weight = { "Weight", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMovieSceneControlRigParameterSection, Weight), Z_Construct_UScriptStruct_FMovieSceneFloatChannel, METADATA_PARAMS(Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_Weight_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_Weight_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_ControlChannelMap_ValueProp = { "ControlChannelMap", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FChannelMapInfo, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_ControlChannelMap_Key_KeyProp = { "ControlChannelMap_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_ControlChannelMap_MetaData[] = {
		{ "Comment", "/** Map from the control name to where it starts as a channel*/" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneControlRigParameterSection.h" },
		{ "ToolTip", "Map from the control name to where it starts as a channel" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_ControlChannelMap = { "ControlChannelMap", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMovieSceneControlRigParameterSection, ControlChannelMap), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_ControlChannelMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_ControlChannelMap_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_EnumParameterNamesAndCurves_Inner = { "EnumParameterNamesAndCurves", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FEnumParameterNameAndCurve, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_EnumParameterNamesAndCurves_MetaData[] = {
		{ "Comment", "/** Enum Curves*/" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneControlRigParameterSection.h" },
		{ "ToolTip", "Enum Curves" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_EnumParameterNamesAndCurves = { "EnumParameterNamesAndCurves", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMovieSceneControlRigParameterSection, EnumParameterNamesAndCurves), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_EnumParameterNamesAndCurves_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_EnumParameterNamesAndCurves_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_IntegerParameterNamesAndCurves_Inner = { "IntegerParameterNamesAndCurves", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FIntegerParameterNameAndCurve, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_IntegerParameterNamesAndCurves_MetaData[] = {
		{ "Comment", "/*Integer Curves*/" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneControlRigParameterSection.h" },
		{ "ToolTip", "Integer Curves" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_IntegerParameterNamesAndCurves = { "IntegerParameterNamesAndCurves", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMovieSceneControlRigParameterSection, IntegerParameterNamesAndCurves), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_IntegerParameterNamesAndCurves_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_IntegerParameterNamesAndCurves_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_ControlRig,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_ControlRigClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_ControlsMask_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_ControlsMask,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_TransformMask,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_Weight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_ControlChannelMap_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_ControlChannelMap_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_ControlChannelMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_EnumParameterNamesAndCurves_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_EnumParameterNamesAndCurves,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_IntegerParameterNamesAndCurves_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::NewProp_IntegerParameterNamesAndCurves,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMovieSceneControlRigParameterSection>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::ClassParams = {
		&UMovieSceneControlRigParameterSection::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::PropPointers),
		0,
		0x003000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMovieSceneControlRigParameterSection()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMovieSceneControlRigParameterSection, 3959383506);
	template<> CONTROLRIG_API UClass* StaticClass<UMovieSceneControlRigParameterSection>()
	{
		return UMovieSceneControlRigParameterSection::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMovieSceneControlRigParameterSection(Z_Construct_UClass_UMovieSceneControlRigParameterSection, &UMovieSceneControlRigParameterSection::StaticClass, TEXT("/Script/ControlRig"), TEXT("UMovieSceneControlRigParameterSection"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMovieSceneControlRigParameterSection);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(UMovieSceneControlRigParameterSection)
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
