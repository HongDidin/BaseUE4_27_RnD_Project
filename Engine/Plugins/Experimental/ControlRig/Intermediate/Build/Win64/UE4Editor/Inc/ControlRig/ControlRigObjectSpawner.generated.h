// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_ControlRigObjectSpawner_generated_h
#error "ControlRigObjectSpawner.generated.h already included, missing '#pragma once' in ControlRigObjectSpawner.h"
#endif
#define CONTROLRIG_ControlRigObjectSpawner_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigObjectSpawner_h_11_SPARSE_DATA
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigObjectSpawner_h_11_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigObjectSpawner_h_11_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigObjectSpawner_h_11_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUControlRigObjectHolder(); \
	friend struct Z_Construct_UClass_UControlRigObjectHolder_Statics; \
public: \
	DECLARE_CLASS(UControlRigObjectHolder, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRig"), NO_API) \
	DECLARE_SERIALIZER(UControlRigObjectHolder)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigObjectSpawner_h_11_INCLASS \
private: \
	static void StaticRegisterNativesUControlRigObjectHolder(); \
	friend struct Z_Construct_UClass_UControlRigObjectHolder_Statics; \
public: \
	DECLARE_CLASS(UControlRigObjectHolder, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRig"), NO_API) \
	DECLARE_SERIALIZER(UControlRigObjectHolder)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigObjectSpawner_h_11_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UControlRigObjectHolder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigObjectHolder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigObjectHolder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigObjectHolder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigObjectHolder(UControlRigObjectHolder&&); \
	NO_API UControlRigObjectHolder(const UControlRigObjectHolder&); \
public:


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigObjectSpawner_h_11_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UControlRigObjectHolder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigObjectHolder(UControlRigObjectHolder&&); \
	NO_API UControlRigObjectHolder(const UControlRigObjectHolder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigObjectHolder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigObjectHolder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigObjectHolder)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigObjectSpawner_h_11_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigObjectSpawner_h_8_PROLOG
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigObjectSpawner_h_11_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigObjectSpawner_h_11_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigObjectSpawner_h_11_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigObjectSpawner_h_11_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigObjectSpawner_h_11_INCLASS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigObjectSpawner_h_11_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigObjectSpawner_h_11_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigObjectSpawner_h_11_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigObjectSpawner_h_11_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigObjectSpawner_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigObjectSpawner_h_11_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigObjectSpawner_h_11_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONTROLRIG_API UClass* StaticClass<class UControlRigObjectHolder>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigObjectSpawner_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
