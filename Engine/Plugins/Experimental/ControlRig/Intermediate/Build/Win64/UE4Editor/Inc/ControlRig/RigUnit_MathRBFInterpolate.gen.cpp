// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Math/RigUnit_MathRBFInterpolate.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_MathRBFInterpolate() {}
// Cross Module References
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_ERBFVectorDistanceType();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_ERBFQuatDistanceType();
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_ERBFKernelType();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorXform();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FMathRBFInterpolateVectorXform_Target();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorQuat();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FMathRBFInterpolateVectorQuat_Target();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FQuat();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorColor();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FMathRBFInterpolateVectorColor_Target();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorVector();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FMathRBFInterpolateVectorVector_Target();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorFloat();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FMathRBFInterpolateVectorFloat_Target();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatXform();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FMathRBFInterpolateQuatXform_Target();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatQuat();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FMathRBFInterpolateQuatQuat_Target();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatColor();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FMathRBFInterpolateQuatColor_Target();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatVector();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FMathRBFInterpolateQuatVector_Target();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatFloat();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FMathRBFInterpolateQuatFloat_Target();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateBase();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorWorkData();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatWorkData();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathBase();
// End Cross Module References
	static UEnum* ERBFVectorDistanceType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ControlRig_ERBFVectorDistanceType, Z_Construct_UPackage__Script_ControlRig(), TEXT("ERBFVectorDistanceType"));
		}
		return Singleton;
	}
	template<> CONTROLRIG_API UEnum* StaticEnum<ERBFVectorDistanceType>()
	{
		return ERBFVectorDistanceType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ERBFVectorDistanceType(ERBFVectorDistanceType_StaticEnum, TEXT("/Script/ControlRig"), TEXT("ERBFVectorDistanceType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ControlRig_ERBFVectorDistanceType_Hash() { return 4158404030U; }
	UEnum* Z_Construct_UEnum_ControlRig_ERBFVectorDistanceType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ERBFVectorDistanceType"), 0, Get_Z_Construct_UEnum_ControlRig_ERBFVectorDistanceType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ERBFVectorDistanceType::Euclidean", (int64)ERBFVectorDistanceType::Euclidean },
				{ "ERBFVectorDistanceType::Manhattan", (int64)ERBFVectorDistanceType::Manhattan },
				{ "ERBFVectorDistanceType::ArcLength", (int64)ERBFVectorDistanceType::ArcLength },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ArcLength.Name", "ERBFVectorDistanceType::ArcLength" },
				{ "Comment", "/** Function to use for computing distance between the input and target \n\x09quaternions. */" },
				{ "Euclidean.Name", "ERBFVectorDistanceType::Euclidean" },
				{ "Manhattan.Name", "ERBFVectorDistanceType::Manhattan" },
				{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
				{ "ToolTip", "Function to use for computing distance between the input and target\n      quaternions." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ControlRig,
				nullptr,
				"ERBFVectorDistanceType",
				"ERBFVectorDistanceType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ERBFQuatDistanceType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ControlRig_ERBFQuatDistanceType, Z_Construct_UPackage__Script_ControlRig(), TEXT("ERBFQuatDistanceType"));
		}
		return Singleton;
	}
	template<> CONTROLRIG_API UEnum* StaticEnum<ERBFQuatDistanceType>()
	{
		return ERBFQuatDistanceType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ERBFQuatDistanceType(ERBFQuatDistanceType_StaticEnum, TEXT("/Script/ControlRig"), TEXT("ERBFQuatDistanceType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ControlRig_ERBFQuatDistanceType_Hash() { return 1917351046U; }
	UEnum* Z_Construct_UEnum_ControlRig_ERBFQuatDistanceType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ERBFQuatDistanceType"), 0, Get_Z_Construct_UEnum_ControlRig_ERBFQuatDistanceType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ERBFQuatDistanceType::Euclidean", (int64)ERBFQuatDistanceType::Euclidean },
				{ "ERBFQuatDistanceType::ArcLength", (int64)ERBFQuatDistanceType::ArcLength },
				{ "ERBFQuatDistanceType::SwingAngle", (int64)ERBFQuatDistanceType::SwingAngle },
				{ "ERBFQuatDistanceType::TwistAngle", (int64)ERBFQuatDistanceType::TwistAngle },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ArcLength.Name", "ERBFQuatDistanceType::ArcLength" },
				{ "Comment", "/** Function to use for computing distance between the input and target \n\x09quaternions. */" },
				{ "Euclidean.Name", "ERBFQuatDistanceType::Euclidean" },
				{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
				{ "SwingAngle.Name", "ERBFQuatDistanceType::SwingAngle" },
				{ "ToolTip", "Function to use for computing distance between the input and target\n      quaternions." },
				{ "TwistAngle.Name", "ERBFQuatDistanceType::TwistAngle" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ControlRig,
				nullptr,
				"ERBFQuatDistanceType",
				"ERBFQuatDistanceType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ERBFKernelType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ControlRig_ERBFKernelType, Z_Construct_UPackage__Script_ControlRig(), TEXT("ERBFKernelType"));
		}
		return Singleton;
	}
	template<> CONTROLRIG_API UEnum* StaticEnum<ERBFKernelType>()
	{
		return ERBFKernelType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ERBFKernelType(ERBFKernelType_StaticEnum, TEXT("/Script/ControlRig"), TEXT("ERBFKernelType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ControlRig_ERBFKernelType_Hash() { return 1413677850U; }
	UEnum* Z_Construct_UEnum_ControlRig_ERBFKernelType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ERBFKernelType"), 0, Get_Z_Construct_UEnum_ControlRig_ERBFKernelType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ERBFKernelType::Gaussian", (int64)ERBFKernelType::Gaussian },
				{ "ERBFKernelType::Exponential", (int64)ERBFKernelType::Exponential },
				{ "ERBFKernelType::Linear", (int64)ERBFKernelType::Linear },
				{ "ERBFKernelType::Cubic", (int64)ERBFKernelType::Cubic },
				{ "ERBFKernelType::Quintic", (int64)ERBFKernelType::Quintic },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** Function to use for each target falloff */" },
				{ "Cubic.Name", "ERBFKernelType::Cubic" },
				{ "Exponential.Name", "ERBFKernelType::Exponential" },
				{ "Gaussian.Name", "ERBFKernelType::Gaussian" },
				{ "Linear.Name", "ERBFKernelType::Linear" },
				{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
				{ "Quintic.Name", "ERBFKernelType::Quintic" },
				{ "ToolTip", "Function to use for each target falloff" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ControlRig,
				nullptr,
				"ERBFKernelType",
				"ERBFKernelType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}

static_assert(std::is_polymorphic<FRigUnit_MathRBFInterpolateVectorXform>() == std::is_polymorphic<FRigUnit_MathRBFInterpolateVectorBase>(), "USTRUCT FRigUnit_MathRBFInterpolateVectorXform cannot be polymorphic unless super FRigUnit_MathRBFInterpolateVectorBase is polymorphic");

class UScriptStruct* FRigUnit_MathRBFInterpolateVectorXform::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorXform_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorXform, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathRBFInterpolateVectorXform"), sizeof(FRigUnit_MathRBFInterpolateVectorXform), Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorXform_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathRBFInterpolateVectorXform::Execute"), &FRigUnit_MathRBFInterpolateVectorXform::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathRBFInterpolateVectorXform>()
{
	return FRigUnit_MathRBFInterpolateVectorXform::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorXform(FRigUnit_MathRBFInterpolateVectorXform::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathRBFInterpolateVectorXform"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateVectorXform
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateVectorXform()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathRBFInterpolateVectorXform>(FName(TEXT("RigUnit_MathRBFInterpolateVectorXform")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateVectorXform;
	struct Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorXform_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Targets_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Targets_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Targets;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Output_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Output;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorXform_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "DisplayName", "Vector to Transform" },
		{ "Keywords", "RBF,Interpolate,Vector" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
		{ "PrototypeName", "RBFInterpolateVectorToXform" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorXform_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathRBFInterpolateVectorXform>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorXform_Statics::NewProp_Targets_Inner = { "Targets", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FMathRBFInterpolateVectorXform_Target, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorXform_Statics::NewProp_Targets_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorXform_Statics::NewProp_Targets = { "Targets", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathRBFInterpolateVectorXform, Targets), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorXform_Statics::NewProp_Targets_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorXform_Statics::NewProp_Targets_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorXform_Statics::NewProp_Output_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorXform_Statics::NewProp_Output = { "Output", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathRBFInterpolateVectorXform, Output), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorXform_Statics::NewProp_Output_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorXform_Statics::NewProp_Output_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorXform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorXform_Statics::NewProp_Targets_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorXform_Statics::NewProp_Targets,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorXform_Statics::NewProp_Output,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorXform_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase,
		&NewStructOps,
		"RigUnit_MathRBFInterpolateVectorXform",
		sizeof(FRigUnit_MathRBFInterpolateVectorXform),
		alignof(FRigUnit_MathRBFInterpolateVectorXform),
		Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorXform_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorXform_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorXform_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorXform_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorXform()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorXform_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathRBFInterpolateVectorXform"), sizeof(FRigUnit_MathRBFInterpolateVectorXform), Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorXform_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorXform_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorXform_Hash() { return 109830874U; }

void FRigUnit_MathRBFInterpolateVectorXform::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
	FRigVMFixedArray<FMathRBFInterpolateVectorXform_Target> Targets_0_Array(Targets);
	
    StaticExecute(
		RigVMExecuteContext,
		Targets_0_Array,
		Output,
		Input,
		DistanceFunction,
		SmoothingFunction,
		SmoothingRadius,
		bNormalizeOutput,
		WorkData,
		Context
	);
}

class UScriptStruct* FMathRBFInterpolateVectorXform_Target::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FMathRBFInterpolateVectorXform_Target_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMathRBFInterpolateVectorXform_Target, Z_Construct_UPackage__Script_ControlRig(), TEXT("MathRBFInterpolateVectorXform_Target"), sizeof(FMathRBFInterpolateVectorXform_Target), Get_Z_Construct_UScriptStruct_FMathRBFInterpolateVectorXform_Target_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FMathRBFInterpolateVectorXform_Target>()
{
	return FMathRBFInterpolateVectorXform_Target::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMathRBFInterpolateVectorXform_Target(FMathRBFInterpolateVectorXform_Target::StaticStruct, TEXT("/Script/ControlRig"), TEXT("MathRBFInterpolateVectorXform_Target"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFMathRBFInterpolateVectorXform_Target
{
	FScriptStruct_ControlRig_StaticRegisterNativesFMathRBFInterpolateVectorXform_Target()
	{
		UScriptStruct::DeferCppStructOps<FMathRBFInterpolateVectorXform_Target>(FName(TEXT("MathRBFInterpolateVectorXform_Target")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFMathRBFInterpolateVectorXform_Target;
	struct Z_Construct_UScriptStruct_FMathRBFInterpolateVectorXform_Target_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Target_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Target;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMathRBFInterpolateVectorXform_Target_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMathRBFInterpolateVectorXform_Target_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMathRBFInterpolateVectorXform_Target>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMathRBFInterpolateVectorXform_Target_Statics::NewProp_Target_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMathRBFInterpolateVectorXform_Target_Statics::NewProp_Target = { "Target", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMathRBFInterpolateVectorXform_Target, Target), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FMathRBFInterpolateVectorXform_Target_Statics::NewProp_Target_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMathRBFInterpolateVectorXform_Target_Statics::NewProp_Target_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMathRBFInterpolateVectorXform_Target_Statics::NewProp_Value_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMathRBFInterpolateVectorXform_Target_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMathRBFInterpolateVectorXform_Target, Value), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FMathRBFInterpolateVectorXform_Target_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMathRBFInterpolateVectorXform_Target_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMathRBFInterpolateVectorXform_Target_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMathRBFInterpolateVectorXform_Target_Statics::NewProp_Target,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMathRBFInterpolateVectorXform_Target_Statics::NewProp_Value,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMathRBFInterpolateVectorXform_Target_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"MathRBFInterpolateVectorXform_Target",
		sizeof(FMathRBFInterpolateVectorXform_Target),
		alignof(FMathRBFInterpolateVectorXform_Target),
		Z_Construct_UScriptStruct_FMathRBFInterpolateVectorXform_Target_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMathRBFInterpolateVectorXform_Target_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMathRBFInterpolateVectorXform_Target_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMathRBFInterpolateVectorXform_Target_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMathRBFInterpolateVectorXform_Target()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMathRBFInterpolateVectorXform_Target_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MathRBFInterpolateVectorXform_Target"), sizeof(FMathRBFInterpolateVectorXform_Target), Get_Z_Construct_UScriptStruct_FMathRBFInterpolateVectorXform_Target_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMathRBFInterpolateVectorXform_Target_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMathRBFInterpolateVectorXform_Target_Hash() { return 4109572721U; }

static_assert(std::is_polymorphic<FRigUnit_MathRBFInterpolateVectorQuat>() == std::is_polymorphic<FRigUnit_MathRBFInterpolateVectorBase>(), "USTRUCT FRigUnit_MathRBFInterpolateVectorQuat cannot be polymorphic unless super FRigUnit_MathRBFInterpolateVectorBase is polymorphic");

class UScriptStruct* FRigUnit_MathRBFInterpolateVectorQuat::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorQuat_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorQuat, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathRBFInterpolateVectorQuat"), sizeof(FRigUnit_MathRBFInterpolateVectorQuat), Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorQuat_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathRBFInterpolateVectorQuat::Execute"), &FRigUnit_MathRBFInterpolateVectorQuat::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathRBFInterpolateVectorQuat>()
{
	return FRigUnit_MathRBFInterpolateVectorQuat::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorQuat(FRigUnit_MathRBFInterpolateVectorQuat::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathRBFInterpolateVectorQuat"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateVectorQuat
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateVectorQuat()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathRBFInterpolateVectorQuat>(FName(TEXT("RigUnit_MathRBFInterpolateVectorQuat")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateVectorQuat;
	struct Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorQuat_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Targets_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Targets_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Targets;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Output_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Output;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorQuat_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "DisplayName", "Vector to Quat" },
		{ "Keywords", "RBF,Interpolate,Vector" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
		{ "PrototypeName", "RBFInterpolateVectorToQuat" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorQuat_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathRBFInterpolateVectorQuat>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorQuat_Statics::NewProp_Targets_Inner = { "Targets", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FMathRBFInterpolateVectorQuat_Target, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorQuat_Statics::NewProp_Targets_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorQuat_Statics::NewProp_Targets = { "Targets", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathRBFInterpolateVectorQuat, Targets), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorQuat_Statics::NewProp_Targets_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorQuat_Statics::NewProp_Targets_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorQuat_Statics::NewProp_Output_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorQuat_Statics::NewProp_Output = { "Output", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathRBFInterpolateVectorQuat, Output), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorQuat_Statics::NewProp_Output_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorQuat_Statics::NewProp_Output_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorQuat_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorQuat_Statics::NewProp_Targets_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorQuat_Statics::NewProp_Targets,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorQuat_Statics::NewProp_Output,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorQuat_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase,
		&NewStructOps,
		"RigUnit_MathRBFInterpolateVectorQuat",
		sizeof(FRigUnit_MathRBFInterpolateVectorQuat),
		alignof(FRigUnit_MathRBFInterpolateVectorQuat),
		Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorQuat_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorQuat_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorQuat_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorQuat_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorQuat()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorQuat_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathRBFInterpolateVectorQuat"), sizeof(FRigUnit_MathRBFInterpolateVectorQuat), Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorQuat_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorQuat_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorQuat_Hash() { return 1654937296U; }

void FRigUnit_MathRBFInterpolateVectorQuat::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
	FRigVMFixedArray<FMathRBFInterpolateVectorQuat_Target> Targets_0_Array(Targets);
	
    StaticExecute(
		RigVMExecuteContext,
		Targets_0_Array,
		Output,
		Input,
		DistanceFunction,
		SmoothingFunction,
		SmoothingRadius,
		bNormalizeOutput,
		WorkData,
		Context
	);
}

class UScriptStruct* FMathRBFInterpolateVectorQuat_Target::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FMathRBFInterpolateVectorQuat_Target_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMathRBFInterpolateVectorQuat_Target, Z_Construct_UPackage__Script_ControlRig(), TEXT("MathRBFInterpolateVectorQuat_Target"), sizeof(FMathRBFInterpolateVectorQuat_Target), Get_Z_Construct_UScriptStruct_FMathRBFInterpolateVectorQuat_Target_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FMathRBFInterpolateVectorQuat_Target>()
{
	return FMathRBFInterpolateVectorQuat_Target::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMathRBFInterpolateVectorQuat_Target(FMathRBFInterpolateVectorQuat_Target::StaticStruct, TEXT("/Script/ControlRig"), TEXT("MathRBFInterpolateVectorQuat_Target"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFMathRBFInterpolateVectorQuat_Target
{
	FScriptStruct_ControlRig_StaticRegisterNativesFMathRBFInterpolateVectorQuat_Target()
	{
		UScriptStruct::DeferCppStructOps<FMathRBFInterpolateVectorQuat_Target>(FName(TEXT("MathRBFInterpolateVectorQuat_Target")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFMathRBFInterpolateVectorQuat_Target;
	struct Z_Construct_UScriptStruct_FMathRBFInterpolateVectorQuat_Target_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Target_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Target;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMathRBFInterpolateVectorQuat_Target_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMathRBFInterpolateVectorQuat_Target_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMathRBFInterpolateVectorQuat_Target>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMathRBFInterpolateVectorQuat_Target_Statics::NewProp_Target_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMathRBFInterpolateVectorQuat_Target_Statics::NewProp_Target = { "Target", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMathRBFInterpolateVectorQuat_Target, Target), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FMathRBFInterpolateVectorQuat_Target_Statics::NewProp_Target_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMathRBFInterpolateVectorQuat_Target_Statics::NewProp_Target_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMathRBFInterpolateVectorQuat_Target_Statics::NewProp_Value_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMathRBFInterpolateVectorQuat_Target_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMathRBFInterpolateVectorQuat_Target, Value), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FMathRBFInterpolateVectorQuat_Target_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMathRBFInterpolateVectorQuat_Target_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMathRBFInterpolateVectorQuat_Target_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMathRBFInterpolateVectorQuat_Target_Statics::NewProp_Target,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMathRBFInterpolateVectorQuat_Target_Statics::NewProp_Value,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMathRBFInterpolateVectorQuat_Target_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"MathRBFInterpolateVectorQuat_Target",
		sizeof(FMathRBFInterpolateVectorQuat_Target),
		alignof(FMathRBFInterpolateVectorQuat_Target),
		Z_Construct_UScriptStruct_FMathRBFInterpolateVectorQuat_Target_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMathRBFInterpolateVectorQuat_Target_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMathRBFInterpolateVectorQuat_Target_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMathRBFInterpolateVectorQuat_Target_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMathRBFInterpolateVectorQuat_Target()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMathRBFInterpolateVectorQuat_Target_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MathRBFInterpolateVectorQuat_Target"), sizeof(FMathRBFInterpolateVectorQuat_Target), Get_Z_Construct_UScriptStruct_FMathRBFInterpolateVectorQuat_Target_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMathRBFInterpolateVectorQuat_Target_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMathRBFInterpolateVectorQuat_Target_Hash() { return 371129956U; }

static_assert(std::is_polymorphic<FRigUnit_MathRBFInterpolateVectorColor>() == std::is_polymorphic<FRigUnit_MathRBFInterpolateVectorBase>(), "USTRUCT FRigUnit_MathRBFInterpolateVectorColor cannot be polymorphic unless super FRigUnit_MathRBFInterpolateVectorBase is polymorphic");

class UScriptStruct* FRigUnit_MathRBFInterpolateVectorColor::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorColor_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorColor, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathRBFInterpolateVectorColor"), sizeof(FRigUnit_MathRBFInterpolateVectorColor), Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorColor_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathRBFInterpolateVectorColor::Execute"), &FRigUnit_MathRBFInterpolateVectorColor::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathRBFInterpolateVectorColor>()
{
	return FRigUnit_MathRBFInterpolateVectorColor::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorColor(FRigUnit_MathRBFInterpolateVectorColor::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathRBFInterpolateVectorColor"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateVectorColor
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateVectorColor()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathRBFInterpolateVectorColor>(FName(TEXT("RigUnit_MathRBFInterpolateVectorColor")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateVectorColor;
	struct Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorColor_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Targets_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Targets_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Targets;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Output_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Output;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorColor_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "DisplayName", "Vector to Color" },
		{ "Keywords", "RBF,Interpolate,Vector" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
		{ "PrototypeName", "RBFInterpolateVectorToColor" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorColor_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathRBFInterpolateVectorColor>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorColor_Statics::NewProp_Targets_Inner = { "Targets", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FMathRBFInterpolateVectorColor_Target, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorColor_Statics::NewProp_Targets_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorColor_Statics::NewProp_Targets = { "Targets", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathRBFInterpolateVectorColor, Targets), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorColor_Statics::NewProp_Targets_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorColor_Statics::NewProp_Targets_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorColor_Statics::NewProp_Output_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorColor_Statics::NewProp_Output = { "Output", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathRBFInterpolateVectorColor, Output), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorColor_Statics::NewProp_Output_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorColor_Statics::NewProp_Output_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorColor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorColor_Statics::NewProp_Targets_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorColor_Statics::NewProp_Targets,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorColor_Statics::NewProp_Output,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorColor_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase,
		&NewStructOps,
		"RigUnit_MathRBFInterpolateVectorColor",
		sizeof(FRigUnit_MathRBFInterpolateVectorColor),
		alignof(FRigUnit_MathRBFInterpolateVectorColor),
		Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorColor_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorColor_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorColor_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorColor_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorColor()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorColor_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathRBFInterpolateVectorColor"), sizeof(FRigUnit_MathRBFInterpolateVectorColor), Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorColor_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorColor_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorColor_Hash() { return 1861186850U; }

void FRigUnit_MathRBFInterpolateVectorColor::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
	FRigVMFixedArray<FMathRBFInterpolateVectorColor_Target> Targets_0_Array(Targets);
	
    StaticExecute(
		RigVMExecuteContext,
		Targets_0_Array,
		Output,
		Input,
		DistanceFunction,
		SmoothingFunction,
		SmoothingRadius,
		bNormalizeOutput,
		WorkData,
		Context
	);
}

class UScriptStruct* FMathRBFInterpolateVectorColor_Target::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FMathRBFInterpolateVectorColor_Target_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMathRBFInterpolateVectorColor_Target, Z_Construct_UPackage__Script_ControlRig(), TEXT("MathRBFInterpolateVectorColor_Target"), sizeof(FMathRBFInterpolateVectorColor_Target), Get_Z_Construct_UScriptStruct_FMathRBFInterpolateVectorColor_Target_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FMathRBFInterpolateVectorColor_Target>()
{
	return FMathRBFInterpolateVectorColor_Target::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMathRBFInterpolateVectorColor_Target(FMathRBFInterpolateVectorColor_Target::StaticStruct, TEXT("/Script/ControlRig"), TEXT("MathRBFInterpolateVectorColor_Target"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFMathRBFInterpolateVectorColor_Target
{
	FScriptStruct_ControlRig_StaticRegisterNativesFMathRBFInterpolateVectorColor_Target()
	{
		UScriptStruct::DeferCppStructOps<FMathRBFInterpolateVectorColor_Target>(FName(TEXT("MathRBFInterpolateVectorColor_Target")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFMathRBFInterpolateVectorColor_Target;
	struct Z_Construct_UScriptStruct_FMathRBFInterpolateVectorColor_Target_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Target_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Target;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMathRBFInterpolateVectorColor_Target_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMathRBFInterpolateVectorColor_Target_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMathRBFInterpolateVectorColor_Target>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMathRBFInterpolateVectorColor_Target_Statics::NewProp_Target_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMathRBFInterpolateVectorColor_Target_Statics::NewProp_Target = { "Target", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMathRBFInterpolateVectorColor_Target, Target), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FMathRBFInterpolateVectorColor_Target_Statics::NewProp_Target_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMathRBFInterpolateVectorColor_Target_Statics::NewProp_Target_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMathRBFInterpolateVectorColor_Target_Statics::NewProp_Value_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMathRBFInterpolateVectorColor_Target_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMathRBFInterpolateVectorColor_Target, Value), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UScriptStruct_FMathRBFInterpolateVectorColor_Target_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMathRBFInterpolateVectorColor_Target_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMathRBFInterpolateVectorColor_Target_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMathRBFInterpolateVectorColor_Target_Statics::NewProp_Target,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMathRBFInterpolateVectorColor_Target_Statics::NewProp_Value,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMathRBFInterpolateVectorColor_Target_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"MathRBFInterpolateVectorColor_Target",
		sizeof(FMathRBFInterpolateVectorColor_Target),
		alignof(FMathRBFInterpolateVectorColor_Target),
		Z_Construct_UScriptStruct_FMathRBFInterpolateVectorColor_Target_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMathRBFInterpolateVectorColor_Target_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMathRBFInterpolateVectorColor_Target_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMathRBFInterpolateVectorColor_Target_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMathRBFInterpolateVectorColor_Target()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMathRBFInterpolateVectorColor_Target_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MathRBFInterpolateVectorColor_Target"), sizeof(FMathRBFInterpolateVectorColor_Target), Get_Z_Construct_UScriptStruct_FMathRBFInterpolateVectorColor_Target_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMathRBFInterpolateVectorColor_Target_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMathRBFInterpolateVectorColor_Target_Hash() { return 2463960403U; }

static_assert(std::is_polymorphic<FRigUnit_MathRBFInterpolateVectorVector>() == std::is_polymorphic<FRigUnit_MathRBFInterpolateVectorBase>(), "USTRUCT FRigUnit_MathRBFInterpolateVectorVector cannot be polymorphic unless super FRigUnit_MathRBFInterpolateVectorBase is polymorphic");

class UScriptStruct* FRigUnit_MathRBFInterpolateVectorVector::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorVector_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorVector, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathRBFInterpolateVectorVector"), sizeof(FRigUnit_MathRBFInterpolateVectorVector), Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorVector_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathRBFInterpolateVectorVector::Execute"), &FRigUnit_MathRBFInterpolateVectorVector::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathRBFInterpolateVectorVector>()
{
	return FRigUnit_MathRBFInterpolateVectorVector::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorVector(FRigUnit_MathRBFInterpolateVectorVector::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathRBFInterpolateVectorVector"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateVectorVector
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateVectorVector()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathRBFInterpolateVectorVector>(FName(TEXT("RigUnit_MathRBFInterpolateVectorVector")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateVectorVector;
	struct Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorVector_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Targets_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Targets_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Targets;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Output_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Output;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorVector_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "DisplayName", "Vector to Vector" },
		{ "Keywords", "RBF,Interpolate,Vector" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
		{ "PrototypeName", "RBFInterpolateVectorToVector" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorVector_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathRBFInterpolateVectorVector>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorVector_Statics::NewProp_Targets_Inner = { "Targets", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FMathRBFInterpolateVectorVector_Target, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorVector_Statics::NewProp_Targets_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorVector_Statics::NewProp_Targets = { "Targets", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathRBFInterpolateVectorVector, Targets), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorVector_Statics::NewProp_Targets_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorVector_Statics::NewProp_Targets_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorVector_Statics::NewProp_Output_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorVector_Statics::NewProp_Output = { "Output", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathRBFInterpolateVectorVector, Output), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorVector_Statics::NewProp_Output_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorVector_Statics::NewProp_Output_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorVector_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorVector_Statics::NewProp_Targets_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorVector_Statics::NewProp_Targets,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorVector_Statics::NewProp_Output,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorVector_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase,
		&NewStructOps,
		"RigUnit_MathRBFInterpolateVectorVector",
		sizeof(FRigUnit_MathRBFInterpolateVectorVector),
		alignof(FRigUnit_MathRBFInterpolateVectorVector),
		Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorVector_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorVector_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorVector_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorVector_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorVector()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorVector_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathRBFInterpolateVectorVector"), sizeof(FRigUnit_MathRBFInterpolateVectorVector), Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorVector_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorVector_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorVector_Hash() { return 4234944135U; }

void FRigUnit_MathRBFInterpolateVectorVector::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
	FRigVMFixedArray<FMathRBFInterpolateVectorVector_Target> Targets_0_Array(Targets);
	
    StaticExecute(
		RigVMExecuteContext,
		Targets_0_Array,
		Output,
		Input,
		DistanceFunction,
		SmoothingFunction,
		SmoothingRadius,
		bNormalizeOutput,
		WorkData,
		Context
	);
}

class UScriptStruct* FMathRBFInterpolateVectorVector_Target::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FMathRBFInterpolateVectorVector_Target_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMathRBFInterpolateVectorVector_Target, Z_Construct_UPackage__Script_ControlRig(), TEXT("MathRBFInterpolateVectorVector_Target"), sizeof(FMathRBFInterpolateVectorVector_Target), Get_Z_Construct_UScriptStruct_FMathRBFInterpolateVectorVector_Target_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FMathRBFInterpolateVectorVector_Target>()
{
	return FMathRBFInterpolateVectorVector_Target::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMathRBFInterpolateVectorVector_Target(FMathRBFInterpolateVectorVector_Target::StaticStruct, TEXT("/Script/ControlRig"), TEXT("MathRBFInterpolateVectorVector_Target"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFMathRBFInterpolateVectorVector_Target
{
	FScriptStruct_ControlRig_StaticRegisterNativesFMathRBFInterpolateVectorVector_Target()
	{
		UScriptStruct::DeferCppStructOps<FMathRBFInterpolateVectorVector_Target>(FName(TEXT("MathRBFInterpolateVectorVector_Target")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFMathRBFInterpolateVectorVector_Target;
	struct Z_Construct_UScriptStruct_FMathRBFInterpolateVectorVector_Target_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Target_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Target;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMathRBFInterpolateVectorVector_Target_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMathRBFInterpolateVectorVector_Target_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMathRBFInterpolateVectorVector_Target>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMathRBFInterpolateVectorVector_Target_Statics::NewProp_Target_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMathRBFInterpolateVectorVector_Target_Statics::NewProp_Target = { "Target", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMathRBFInterpolateVectorVector_Target, Target), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FMathRBFInterpolateVectorVector_Target_Statics::NewProp_Target_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMathRBFInterpolateVectorVector_Target_Statics::NewProp_Target_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMathRBFInterpolateVectorVector_Target_Statics::NewProp_Value_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMathRBFInterpolateVectorVector_Target_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMathRBFInterpolateVectorVector_Target, Value), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FMathRBFInterpolateVectorVector_Target_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMathRBFInterpolateVectorVector_Target_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMathRBFInterpolateVectorVector_Target_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMathRBFInterpolateVectorVector_Target_Statics::NewProp_Target,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMathRBFInterpolateVectorVector_Target_Statics::NewProp_Value,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMathRBFInterpolateVectorVector_Target_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"MathRBFInterpolateVectorVector_Target",
		sizeof(FMathRBFInterpolateVectorVector_Target),
		alignof(FMathRBFInterpolateVectorVector_Target),
		Z_Construct_UScriptStruct_FMathRBFInterpolateVectorVector_Target_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMathRBFInterpolateVectorVector_Target_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMathRBFInterpolateVectorVector_Target_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMathRBFInterpolateVectorVector_Target_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMathRBFInterpolateVectorVector_Target()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMathRBFInterpolateVectorVector_Target_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MathRBFInterpolateVectorVector_Target"), sizeof(FMathRBFInterpolateVectorVector_Target), Get_Z_Construct_UScriptStruct_FMathRBFInterpolateVectorVector_Target_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMathRBFInterpolateVectorVector_Target_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMathRBFInterpolateVectorVector_Target_Hash() { return 1384026908U; }

static_assert(std::is_polymorphic<FRigUnit_MathRBFInterpolateVectorFloat>() == std::is_polymorphic<FRigUnit_MathRBFInterpolateVectorBase>(), "USTRUCT FRigUnit_MathRBFInterpolateVectorFloat cannot be polymorphic unless super FRigUnit_MathRBFInterpolateVectorBase is polymorphic");

class UScriptStruct* FRigUnit_MathRBFInterpolateVectorFloat::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorFloat_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorFloat, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathRBFInterpolateVectorFloat"), sizeof(FRigUnit_MathRBFInterpolateVectorFloat), Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorFloat_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathRBFInterpolateVectorFloat::Execute"), &FRigUnit_MathRBFInterpolateVectorFloat::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathRBFInterpolateVectorFloat>()
{
	return FRigUnit_MathRBFInterpolateVectorFloat::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorFloat(FRigUnit_MathRBFInterpolateVectorFloat::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathRBFInterpolateVectorFloat"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateVectorFloat
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateVectorFloat()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathRBFInterpolateVectorFloat>(FName(TEXT("RigUnit_MathRBFInterpolateVectorFloat")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateVectorFloat;
	struct Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorFloat_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Targets_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Targets_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Targets;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Output_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Output;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorFloat_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "DisplayName", "Vector to Float" },
		{ "Keywords", "RBF,Interpolate,Vector" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
		{ "PrototypeName", "RBFInterpolateVectorToFloat" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorFloat_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathRBFInterpolateVectorFloat>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorFloat_Statics::NewProp_Targets_Inner = { "Targets", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FMathRBFInterpolateVectorFloat_Target, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorFloat_Statics::NewProp_Targets_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorFloat_Statics::NewProp_Targets = { "Targets", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathRBFInterpolateVectorFloat, Targets), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorFloat_Statics::NewProp_Targets_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorFloat_Statics::NewProp_Targets_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorFloat_Statics::NewProp_Output_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorFloat_Statics::NewProp_Output = { "Output", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathRBFInterpolateVectorFloat, Output), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorFloat_Statics::NewProp_Output_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorFloat_Statics::NewProp_Output_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorFloat_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorFloat_Statics::NewProp_Targets_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorFloat_Statics::NewProp_Targets,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorFloat_Statics::NewProp_Output,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorFloat_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase,
		&NewStructOps,
		"RigUnit_MathRBFInterpolateVectorFloat",
		sizeof(FRigUnit_MathRBFInterpolateVectorFloat),
		alignof(FRigUnit_MathRBFInterpolateVectorFloat),
		Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorFloat_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorFloat_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorFloat_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorFloat_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorFloat()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorFloat_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathRBFInterpolateVectorFloat"), sizeof(FRigUnit_MathRBFInterpolateVectorFloat), Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorFloat_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorFloat_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorFloat_Hash() { return 3787123642U; }

void FRigUnit_MathRBFInterpolateVectorFloat::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
	FRigVMFixedArray<FMathRBFInterpolateVectorFloat_Target> Targets_0_Array(Targets);
	
    StaticExecute(
		RigVMExecuteContext,
		Targets_0_Array,
		Output,
		Input,
		DistanceFunction,
		SmoothingFunction,
		SmoothingRadius,
		bNormalizeOutput,
		WorkData,
		Context
	);
}

class UScriptStruct* FMathRBFInterpolateVectorFloat_Target::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FMathRBFInterpolateVectorFloat_Target_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMathRBFInterpolateVectorFloat_Target, Z_Construct_UPackage__Script_ControlRig(), TEXT("MathRBFInterpolateVectorFloat_Target"), sizeof(FMathRBFInterpolateVectorFloat_Target), Get_Z_Construct_UScriptStruct_FMathRBFInterpolateVectorFloat_Target_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FMathRBFInterpolateVectorFloat_Target>()
{
	return FMathRBFInterpolateVectorFloat_Target::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMathRBFInterpolateVectorFloat_Target(FMathRBFInterpolateVectorFloat_Target::StaticStruct, TEXT("/Script/ControlRig"), TEXT("MathRBFInterpolateVectorFloat_Target"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFMathRBFInterpolateVectorFloat_Target
{
	FScriptStruct_ControlRig_StaticRegisterNativesFMathRBFInterpolateVectorFloat_Target()
	{
		UScriptStruct::DeferCppStructOps<FMathRBFInterpolateVectorFloat_Target>(FName(TEXT("MathRBFInterpolateVectorFloat_Target")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFMathRBFInterpolateVectorFloat_Target;
	struct Z_Construct_UScriptStruct_FMathRBFInterpolateVectorFloat_Target_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Target_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Target;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMathRBFInterpolateVectorFloat_Target_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/// Vector->T\n" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
		{ "ToolTip", "Vector->T" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMathRBFInterpolateVectorFloat_Target_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMathRBFInterpolateVectorFloat_Target>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMathRBFInterpolateVectorFloat_Target_Statics::NewProp_Target_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMathRBFInterpolateVectorFloat_Target_Statics::NewProp_Target = { "Target", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMathRBFInterpolateVectorFloat_Target, Target), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FMathRBFInterpolateVectorFloat_Target_Statics::NewProp_Target_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMathRBFInterpolateVectorFloat_Target_Statics::NewProp_Target_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMathRBFInterpolateVectorFloat_Target_Statics::NewProp_Value_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FMathRBFInterpolateVectorFloat_Target_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMathRBFInterpolateVectorFloat_Target, Value), METADATA_PARAMS(Z_Construct_UScriptStruct_FMathRBFInterpolateVectorFloat_Target_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMathRBFInterpolateVectorFloat_Target_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMathRBFInterpolateVectorFloat_Target_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMathRBFInterpolateVectorFloat_Target_Statics::NewProp_Target,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMathRBFInterpolateVectorFloat_Target_Statics::NewProp_Value,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMathRBFInterpolateVectorFloat_Target_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"MathRBFInterpolateVectorFloat_Target",
		sizeof(FMathRBFInterpolateVectorFloat_Target),
		alignof(FMathRBFInterpolateVectorFloat_Target),
		Z_Construct_UScriptStruct_FMathRBFInterpolateVectorFloat_Target_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMathRBFInterpolateVectorFloat_Target_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMathRBFInterpolateVectorFloat_Target_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMathRBFInterpolateVectorFloat_Target_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMathRBFInterpolateVectorFloat_Target()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMathRBFInterpolateVectorFloat_Target_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MathRBFInterpolateVectorFloat_Target"), sizeof(FMathRBFInterpolateVectorFloat_Target), Get_Z_Construct_UScriptStruct_FMathRBFInterpolateVectorFloat_Target_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMathRBFInterpolateVectorFloat_Target_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMathRBFInterpolateVectorFloat_Target_Hash() { return 2895386436U; }

static_assert(std::is_polymorphic<FRigUnit_MathRBFInterpolateQuatXform>() == std::is_polymorphic<FRigUnit_MathRBFInterpolateQuatBase>(), "USTRUCT FRigUnit_MathRBFInterpolateQuatXform cannot be polymorphic unless super FRigUnit_MathRBFInterpolateQuatBase is polymorphic");

class UScriptStruct* FRigUnit_MathRBFInterpolateQuatXform::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatXform_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatXform, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathRBFInterpolateQuatXform"), sizeof(FRigUnit_MathRBFInterpolateQuatXform), Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatXform_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathRBFInterpolateQuatXform::Execute"), &FRigUnit_MathRBFInterpolateQuatXform::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathRBFInterpolateQuatXform>()
{
	return FRigUnit_MathRBFInterpolateQuatXform::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatXform(FRigUnit_MathRBFInterpolateQuatXform::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathRBFInterpolateQuatXform"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateQuatXform
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateQuatXform()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathRBFInterpolateQuatXform>(FName(TEXT("RigUnit_MathRBFInterpolateQuatXform")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateQuatXform;
	struct Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatXform_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Targets_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Targets_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Targets;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Output_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Output;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatXform_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "DisplayName", "Quaternion to Transform" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
		{ "PrototypeName", "RBFInterpolateQuatToXform" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatXform_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathRBFInterpolateQuatXform>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatXform_Statics::NewProp_Targets_Inner = { "Targets", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FMathRBFInterpolateQuatXform_Target, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatXform_Statics::NewProp_Targets_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatXform_Statics::NewProp_Targets = { "Targets", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathRBFInterpolateQuatXform, Targets), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatXform_Statics::NewProp_Targets_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatXform_Statics::NewProp_Targets_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatXform_Statics::NewProp_Output_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatXform_Statics::NewProp_Output = { "Output", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathRBFInterpolateQuatXform, Output), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatXform_Statics::NewProp_Output_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatXform_Statics::NewProp_Output_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatXform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatXform_Statics::NewProp_Targets_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatXform_Statics::NewProp_Targets,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatXform_Statics::NewProp_Output,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatXform_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase,
		&NewStructOps,
		"RigUnit_MathRBFInterpolateQuatXform",
		sizeof(FRigUnit_MathRBFInterpolateQuatXform),
		alignof(FRigUnit_MathRBFInterpolateQuatXform),
		Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatXform_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatXform_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatXform_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatXform_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatXform()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatXform_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathRBFInterpolateQuatXform"), sizeof(FRigUnit_MathRBFInterpolateQuatXform), Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatXform_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatXform_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatXform_Hash() { return 220133743U; }

void FRigUnit_MathRBFInterpolateQuatXform::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
	FRigVMFixedArray<FMathRBFInterpolateQuatXform_Target> Targets_0_Array(Targets);
	
    StaticExecute(
		RigVMExecuteContext,
		Targets_0_Array,
		Output,
		Input,
		DistanceFunction,
		SmoothingFunction,
		SmoothingAngle,
		bNormalizeOutput,
		TwistAxis,
		WorkData,
		Context
	);
}

class UScriptStruct* FMathRBFInterpolateQuatXform_Target::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FMathRBFInterpolateQuatXform_Target_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMathRBFInterpolateQuatXform_Target, Z_Construct_UPackage__Script_ControlRig(), TEXT("MathRBFInterpolateQuatXform_Target"), sizeof(FMathRBFInterpolateQuatXform_Target), Get_Z_Construct_UScriptStruct_FMathRBFInterpolateQuatXform_Target_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FMathRBFInterpolateQuatXform_Target>()
{
	return FMathRBFInterpolateQuatXform_Target::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMathRBFInterpolateQuatXform_Target(FMathRBFInterpolateQuatXform_Target::StaticStruct, TEXT("/Script/ControlRig"), TEXT("MathRBFInterpolateQuatXform_Target"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFMathRBFInterpolateQuatXform_Target
{
	FScriptStruct_ControlRig_StaticRegisterNativesFMathRBFInterpolateQuatXform_Target()
	{
		UScriptStruct::DeferCppStructOps<FMathRBFInterpolateQuatXform_Target>(FName(TEXT("MathRBFInterpolateQuatXform_Target")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFMathRBFInterpolateQuatXform_Target;
	struct Z_Construct_UScriptStruct_FMathRBFInterpolateQuatXform_Target_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Target_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Target;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMathRBFInterpolateQuatXform_Target_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMathRBFInterpolateQuatXform_Target_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMathRBFInterpolateQuatXform_Target>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMathRBFInterpolateQuatXform_Target_Statics::NewProp_Target_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMathRBFInterpolateQuatXform_Target_Statics::NewProp_Target = { "Target", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMathRBFInterpolateQuatXform_Target, Target), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FMathRBFInterpolateQuatXform_Target_Statics::NewProp_Target_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMathRBFInterpolateQuatXform_Target_Statics::NewProp_Target_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMathRBFInterpolateQuatXform_Target_Statics::NewProp_Value_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMathRBFInterpolateQuatXform_Target_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMathRBFInterpolateQuatXform_Target, Value), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FMathRBFInterpolateQuatXform_Target_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMathRBFInterpolateQuatXform_Target_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMathRBFInterpolateQuatXform_Target_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMathRBFInterpolateQuatXform_Target_Statics::NewProp_Target,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMathRBFInterpolateQuatXform_Target_Statics::NewProp_Value,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMathRBFInterpolateQuatXform_Target_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"MathRBFInterpolateQuatXform_Target",
		sizeof(FMathRBFInterpolateQuatXform_Target),
		alignof(FMathRBFInterpolateQuatXform_Target),
		Z_Construct_UScriptStruct_FMathRBFInterpolateQuatXform_Target_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMathRBFInterpolateQuatXform_Target_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMathRBFInterpolateQuatXform_Target_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMathRBFInterpolateQuatXform_Target_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMathRBFInterpolateQuatXform_Target()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMathRBFInterpolateQuatXform_Target_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MathRBFInterpolateQuatXform_Target"), sizeof(FMathRBFInterpolateQuatXform_Target), Get_Z_Construct_UScriptStruct_FMathRBFInterpolateQuatXform_Target_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMathRBFInterpolateQuatXform_Target_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMathRBFInterpolateQuatXform_Target_Hash() { return 688449514U; }

static_assert(std::is_polymorphic<FRigUnit_MathRBFInterpolateQuatQuat>() == std::is_polymorphic<FRigUnit_MathRBFInterpolateQuatBase>(), "USTRUCT FRigUnit_MathRBFInterpolateQuatQuat cannot be polymorphic unless super FRigUnit_MathRBFInterpolateQuatBase is polymorphic");

class UScriptStruct* FRigUnit_MathRBFInterpolateQuatQuat::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatQuat_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatQuat, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathRBFInterpolateQuatQuat"), sizeof(FRigUnit_MathRBFInterpolateQuatQuat), Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatQuat_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathRBFInterpolateQuatQuat::Execute"), &FRigUnit_MathRBFInterpolateQuatQuat::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathRBFInterpolateQuatQuat>()
{
	return FRigUnit_MathRBFInterpolateQuatQuat::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatQuat(FRigUnit_MathRBFInterpolateQuatQuat::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathRBFInterpolateQuatQuat"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateQuatQuat
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateQuatQuat()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathRBFInterpolateQuatQuat>(FName(TEXT("RigUnit_MathRBFInterpolateQuatQuat")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateQuatQuat;
	struct Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatQuat_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Targets_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Targets_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Targets;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Output_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Output;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatQuat_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "DisplayName", "Quaternion to Quaternion" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
		{ "PrototypeName", "RBFInterpolateQuatToQuat" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatQuat_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathRBFInterpolateQuatQuat>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatQuat_Statics::NewProp_Targets_Inner = { "Targets", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FMathRBFInterpolateQuatQuat_Target, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatQuat_Statics::NewProp_Targets_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatQuat_Statics::NewProp_Targets = { "Targets", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathRBFInterpolateQuatQuat, Targets), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatQuat_Statics::NewProp_Targets_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatQuat_Statics::NewProp_Targets_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatQuat_Statics::NewProp_Output_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatQuat_Statics::NewProp_Output = { "Output", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathRBFInterpolateQuatQuat, Output), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatQuat_Statics::NewProp_Output_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatQuat_Statics::NewProp_Output_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatQuat_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatQuat_Statics::NewProp_Targets_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatQuat_Statics::NewProp_Targets,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatQuat_Statics::NewProp_Output,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatQuat_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase,
		&NewStructOps,
		"RigUnit_MathRBFInterpolateQuatQuat",
		sizeof(FRigUnit_MathRBFInterpolateQuatQuat),
		alignof(FRigUnit_MathRBFInterpolateQuatQuat),
		Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatQuat_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatQuat_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatQuat_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatQuat_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatQuat()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatQuat_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathRBFInterpolateQuatQuat"), sizeof(FRigUnit_MathRBFInterpolateQuatQuat), Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatQuat_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatQuat_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatQuat_Hash() { return 631942866U; }

void FRigUnit_MathRBFInterpolateQuatQuat::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
	FRigVMFixedArray<FMathRBFInterpolateQuatQuat_Target> Targets_0_Array(Targets);
	
    StaticExecute(
		RigVMExecuteContext,
		Targets_0_Array,
		Output,
		Input,
		DistanceFunction,
		SmoothingFunction,
		SmoothingAngle,
		bNormalizeOutput,
		TwistAxis,
		WorkData,
		Context
	);
}

class UScriptStruct* FMathRBFInterpolateQuatQuat_Target::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FMathRBFInterpolateQuatQuat_Target_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMathRBFInterpolateQuatQuat_Target, Z_Construct_UPackage__Script_ControlRig(), TEXT("MathRBFInterpolateQuatQuat_Target"), sizeof(FMathRBFInterpolateQuatQuat_Target), Get_Z_Construct_UScriptStruct_FMathRBFInterpolateQuatQuat_Target_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FMathRBFInterpolateQuatQuat_Target>()
{
	return FMathRBFInterpolateQuatQuat_Target::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMathRBFInterpolateQuatQuat_Target(FMathRBFInterpolateQuatQuat_Target::StaticStruct, TEXT("/Script/ControlRig"), TEXT("MathRBFInterpolateQuatQuat_Target"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFMathRBFInterpolateQuatQuat_Target
{
	FScriptStruct_ControlRig_StaticRegisterNativesFMathRBFInterpolateQuatQuat_Target()
	{
		UScriptStruct::DeferCppStructOps<FMathRBFInterpolateQuatQuat_Target>(FName(TEXT("MathRBFInterpolateQuatQuat_Target")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFMathRBFInterpolateQuatQuat_Target;
	struct Z_Construct_UScriptStruct_FMathRBFInterpolateQuatQuat_Target_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Target_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Target;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMathRBFInterpolateQuatQuat_Target_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMathRBFInterpolateQuatQuat_Target_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMathRBFInterpolateQuatQuat_Target>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMathRBFInterpolateQuatQuat_Target_Statics::NewProp_Target_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMathRBFInterpolateQuatQuat_Target_Statics::NewProp_Target = { "Target", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMathRBFInterpolateQuatQuat_Target, Target), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FMathRBFInterpolateQuatQuat_Target_Statics::NewProp_Target_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMathRBFInterpolateQuatQuat_Target_Statics::NewProp_Target_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMathRBFInterpolateQuatQuat_Target_Statics::NewProp_Value_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMathRBFInterpolateQuatQuat_Target_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMathRBFInterpolateQuatQuat_Target, Value), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FMathRBFInterpolateQuatQuat_Target_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMathRBFInterpolateQuatQuat_Target_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMathRBFInterpolateQuatQuat_Target_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMathRBFInterpolateQuatQuat_Target_Statics::NewProp_Target,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMathRBFInterpolateQuatQuat_Target_Statics::NewProp_Value,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMathRBFInterpolateQuatQuat_Target_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"MathRBFInterpolateQuatQuat_Target",
		sizeof(FMathRBFInterpolateQuatQuat_Target),
		alignof(FMathRBFInterpolateQuatQuat_Target),
		Z_Construct_UScriptStruct_FMathRBFInterpolateQuatQuat_Target_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMathRBFInterpolateQuatQuat_Target_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMathRBFInterpolateQuatQuat_Target_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMathRBFInterpolateQuatQuat_Target_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMathRBFInterpolateQuatQuat_Target()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMathRBFInterpolateQuatQuat_Target_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MathRBFInterpolateQuatQuat_Target"), sizeof(FMathRBFInterpolateQuatQuat_Target), Get_Z_Construct_UScriptStruct_FMathRBFInterpolateQuatQuat_Target_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMathRBFInterpolateQuatQuat_Target_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMathRBFInterpolateQuatQuat_Target_Hash() { return 3534831027U; }

static_assert(std::is_polymorphic<FRigUnit_MathRBFInterpolateQuatColor>() == std::is_polymorphic<FRigUnit_MathRBFInterpolateQuatBase>(), "USTRUCT FRigUnit_MathRBFInterpolateQuatColor cannot be polymorphic unless super FRigUnit_MathRBFInterpolateQuatBase is polymorphic");

class UScriptStruct* FRigUnit_MathRBFInterpolateQuatColor::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatColor_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatColor, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathRBFInterpolateQuatColor"), sizeof(FRigUnit_MathRBFInterpolateQuatColor), Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatColor_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathRBFInterpolateQuatColor::Execute"), &FRigUnit_MathRBFInterpolateQuatColor::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathRBFInterpolateQuatColor>()
{
	return FRigUnit_MathRBFInterpolateQuatColor::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatColor(FRigUnit_MathRBFInterpolateQuatColor::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathRBFInterpolateQuatColor"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateQuatColor
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateQuatColor()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathRBFInterpolateQuatColor>(FName(TEXT("RigUnit_MathRBFInterpolateQuatColor")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateQuatColor;
	struct Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatColor_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Targets_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Targets_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Targets;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Output_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Output;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatColor_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "DisplayName", "Quaternion to Color" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
		{ "PrototypeName", "RBFInterpolateQuatToColor" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatColor_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathRBFInterpolateQuatColor>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatColor_Statics::NewProp_Targets_Inner = { "Targets", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FMathRBFInterpolateQuatColor_Target, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatColor_Statics::NewProp_Targets_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatColor_Statics::NewProp_Targets = { "Targets", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathRBFInterpolateQuatColor, Targets), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatColor_Statics::NewProp_Targets_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatColor_Statics::NewProp_Targets_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatColor_Statics::NewProp_Output_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatColor_Statics::NewProp_Output = { "Output", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathRBFInterpolateQuatColor, Output), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatColor_Statics::NewProp_Output_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatColor_Statics::NewProp_Output_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatColor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatColor_Statics::NewProp_Targets_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatColor_Statics::NewProp_Targets,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatColor_Statics::NewProp_Output,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatColor_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase,
		&NewStructOps,
		"RigUnit_MathRBFInterpolateQuatColor",
		sizeof(FRigUnit_MathRBFInterpolateQuatColor),
		alignof(FRigUnit_MathRBFInterpolateQuatColor),
		Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatColor_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatColor_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatColor_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatColor_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatColor()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatColor_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathRBFInterpolateQuatColor"), sizeof(FRigUnit_MathRBFInterpolateQuatColor), Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatColor_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatColor_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatColor_Hash() { return 2561918059U; }

void FRigUnit_MathRBFInterpolateQuatColor::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
	FRigVMFixedArray<FMathRBFInterpolateQuatColor_Target> Targets_0_Array(Targets);
	
    StaticExecute(
		RigVMExecuteContext,
		Targets_0_Array,
		Output,
		Input,
		DistanceFunction,
		SmoothingFunction,
		SmoothingAngle,
		bNormalizeOutput,
		TwistAxis,
		WorkData,
		Context
	);
}

class UScriptStruct* FMathRBFInterpolateQuatColor_Target::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FMathRBFInterpolateQuatColor_Target_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMathRBFInterpolateQuatColor_Target, Z_Construct_UPackage__Script_ControlRig(), TEXT("MathRBFInterpolateQuatColor_Target"), sizeof(FMathRBFInterpolateQuatColor_Target), Get_Z_Construct_UScriptStruct_FMathRBFInterpolateQuatColor_Target_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FMathRBFInterpolateQuatColor_Target>()
{
	return FMathRBFInterpolateQuatColor_Target::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMathRBFInterpolateQuatColor_Target(FMathRBFInterpolateQuatColor_Target::StaticStruct, TEXT("/Script/ControlRig"), TEXT("MathRBFInterpolateQuatColor_Target"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFMathRBFInterpolateQuatColor_Target
{
	FScriptStruct_ControlRig_StaticRegisterNativesFMathRBFInterpolateQuatColor_Target()
	{
		UScriptStruct::DeferCppStructOps<FMathRBFInterpolateQuatColor_Target>(FName(TEXT("MathRBFInterpolateQuatColor_Target")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFMathRBFInterpolateQuatColor_Target;
	struct Z_Construct_UScriptStruct_FMathRBFInterpolateQuatColor_Target_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Target_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Target;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMathRBFInterpolateQuatColor_Target_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMathRBFInterpolateQuatColor_Target_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMathRBFInterpolateQuatColor_Target>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMathRBFInterpolateQuatColor_Target_Statics::NewProp_Target_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMathRBFInterpolateQuatColor_Target_Statics::NewProp_Target = { "Target", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMathRBFInterpolateQuatColor_Target, Target), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FMathRBFInterpolateQuatColor_Target_Statics::NewProp_Target_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMathRBFInterpolateQuatColor_Target_Statics::NewProp_Target_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMathRBFInterpolateQuatColor_Target_Statics::NewProp_Value_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMathRBFInterpolateQuatColor_Target_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMathRBFInterpolateQuatColor_Target, Value), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UScriptStruct_FMathRBFInterpolateQuatColor_Target_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMathRBFInterpolateQuatColor_Target_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMathRBFInterpolateQuatColor_Target_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMathRBFInterpolateQuatColor_Target_Statics::NewProp_Target,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMathRBFInterpolateQuatColor_Target_Statics::NewProp_Value,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMathRBFInterpolateQuatColor_Target_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"MathRBFInterpolateQuatColor_Target",
		sizeof(FMathRBFInterpolateQuatColor_Target),
		alignof(FMathRBFInterpolateQuatColor_Target),
		Z_Construct_UScriptStruct_FMathRBFInterpolateQuatColor_Target_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMathRBFInterpolateQuatColor_Target_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMathRBFInterpolateQuatColor_Target_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMathRBFInterpolateQuatColor_Target_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMathRBFInterpolateQuatColor_Target()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMathRBFInterpolateQuatColor_Target_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MathRBFInterpolateQuatColor_Target"), sizeof(FMathRBFInterpolateQuatColor_Target), Get_Z_Construct_UScriptStruct_FMathRBFInterpolateQuatColor_Target_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMathRBFInterpolateQuatColor_Target_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMathRBFInterpolateQuatColor_Target_Hash() { return 3977696993U; }

static_assert(std::is_polymorphic<FRigUnit_MathRBFInterpolateQuatVector>() == std::is_polymorphic<FRigUnit_MathRBFInterpolateQuatBase>(), "USTRUCT FRigUnit_MathRBFInterpolateQuatVector cannot be polymorphic unless super FRigUnit_MathRBFInterpolateQuatBase is polymorphic");

class UScriptStruct* FRigUnit_MathRBFInterpolateQuatVector::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatVector_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatVector, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathRBFInterpolateQuatVector"), sizeof(FRigUnit_MathRBFInterpolateQuatVector), Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatVector_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathRBFInterpolateQuatVector::Execute"), &FRigUnit_MathRBFInterpolateQuatVector::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathRBFInterpolateQuatVector>()
{
	return FRigUnit_MathRBFInterpolateQuatVector::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatVector(FRigUnit_MathRBFInterpolateQuatVector::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathRBFInterpolateQuatVector"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateQuatVector
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateQuatVector()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathRBFInterpolateQuatVector>(FName(TEXT("RigUnit_MathRBFInterpolateQuatVector")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateQuatVector;
	struct Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatVector_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Targets_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Targets_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Targets;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Output_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Output;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatVector_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "DisplayName", "Quaternion to Vector" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
		{ "PrototypeName", "RBFInterpolateQuatToVector" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatVector_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathRBFInterpolateQuatVector>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatVector_Statics::NewProp_Targets_Inner = { "Targets", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FMathRBFInterpolateQuatVector_Target, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatVector_Statics::NewProp_Targets_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatVector_Statics::NewProp_Targets = { "Targets", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathRBFInterpolateQuatVector, Targets), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatVector_Statics::NewProp_Targets_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatVector_Statics::NewProp_Targets_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatVector_Statics::NewProp_Output_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatVector_Statics::NewProp_Output = { "Output", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathRBFInterpolateQuatVector, Output), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatVector_Statics::NewProp_Output_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatVector_Statics::NewProp_Output_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatVector_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatVector_Statics::NewProp_Targets_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatVector_Statics::NewProp_Targets,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatVector_Statics::NewProp_Output,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatVector_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase,
		&NewStructOps,
		"RigUnit_MathRBFInterpolateQuatVector",
		sizeof(FRigUnit_MathRBFInterpolateQuatVector),
		alignof(FRigUnit_MathRBFInterpolateQuatVector),
		Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatVector_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatVector_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatVector_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatVector_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatVector()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatVector_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathRBFInterpolateQuatVector"), sizeof(FRigUnit_MathRBFInterpolateQuatVector), Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatVector_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatVector_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatVector_Hash() { return 3170678896U; }

void FRigUnit_MathRBFInterpolateQuatVector::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
	FRigVMFixedArray<FMathRBFInterpolateQuatVector_Target> Targets_0_Array(Targets);
	
    StaticExecute(
		RigVMExecuteContext,
		Targets_0_Array,
		Output,
		Input,
		DistanceFunction,
		SmoothingFunction,
		SmoothingAngle,
		bNormalizeOutput,
		TwistAxis,
		WorkData,
		Context
	);
}

class UScriptStruct* FMathRBFInterpolateQuatVector_Target::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FMathRBFInterpolateQuatVector_Target_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMathRBFInterpolateQuatVector_Target, Z_Construct_UPackage__Script_ControlRig(), TEXT("MathRBFInterpolateQuatVector_Target"), sizeof(FMathRBFInterpolateQuatVector_Target), Get_Z_Construct_UScriptStruct_FMathRBFInterpolateQuatVector_Target_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FMathRBFInterpolateQuatVector_Target>()
{
	return FMathRBFInterpolateQuatVector_Target::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMathRBFInterpolateQuatVector_Target(FMathRBFInterpolateQuatVector_Target::StaticStruct, TEXT("/Script/ControlRig"), TEXT("MathRBFInterpolateQuatVector_Target"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFMathRBFInterpolateQuatVector_Target
{
	FScriptStruct_ControlRig_StaticRegisterNativesFMathRBFInterpolateQuatVector_Target()
	{
		UScriptStruct::DeferCppStructOps<FMathRBFInterpolateQuatVector_Target>(FName(TEXT("MathRBFInterpolateQuatVector_Target")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFMathRBFInterpolateQuatVector_Target;
	struct Z_Construct_UScriptStruct_FMathRBFInterpolateQuatVector_Target_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Target_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Target;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMathRBFInterpolateQuatVector_Target_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMathRBFInterpolateQuatVector_Target_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMathRBFInterpolateQuatVector_Target>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMathRBFInterpolateQuatVector_Target_Statics::NewProp_Target_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMathRBFInterpolateQuatVector_Target_Statics::NewProp_Target = { "Target", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMathRBFInterpolateQuatVector_Target, Target), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FMathRBFInterpolateQuatVector_Target_Statics::NewProp_Target_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMathRBFInterpolateQuatVector_Target_Statics::NewProp_Target_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMathRBFInterpolateQuatVector_Target_Statics::NewProp_Value_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMathRBFInterpolateQuatVector_Target_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMathRBFInterpolateQuatVector_Target, Value), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FMathRBFInterpolateQuatVector_Target_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMathRBFInterpolateQuatVector_Target_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMathRBFInterpolateQuatVector_Target_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMathRBFInterpolateQuatVector_Target_Statics::NewProp_Target,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMathRBFInterpolateQuatVector_Target_Statics::NewProp_Value,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMathRBFInterpolateQuatVector_Target_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"MathRBFInterpolateQuatVector_Target",
		sizeof(FMathRBFInterpolateQuatVector_Target),
		alignof(FMathRBFInterpolateQuatVector_Target),
		Z_Construct_UScriptStruct_FMathRBFInterpolateQuatVector_Target_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMathRBFInterpolateQuatVector_Target_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMathRBFInterpolateQuatVector_Target_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMathRBFInterpolateQuatVector_Target_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMathRBFInterpolateQuatVector_Target()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMathRBFInterpolateQuatVector_Target_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MathRBFInterpolateQuatVector_Target"), sizeof(FMathRBFInterpolateQuatVector_Target), Get_Z_Construct_UScriptStruct_FMathRBFInterpolateQuatVector_Target_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMathRBFInterpolateQuatVector_Target_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMathRBFInterpolateQuatVector_Target_Hash() { return 2536883318U; }

static_assert(std::is_polymorphic<FRigUnit_MathRBFInterpolateQuatFloat>() == std::is_polymorphic<FRigUnit_MathRBFInterpolateQuatBase>(), "USTRUCT FRigUnit_MathRBFInterpolateQuatFloat cannot be polymorphic unless super FRigUnit_MathRBFInterpolateQuatBase is polymorphic");

class UScriptStruct* FRigUnit_MathRBFInterpolateQuatFloat::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatFloat_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatFloat, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathRBFInterpolateQuatFloat"), sizeof(FRigUnit_MathRBFInterpolateQuatFloat), Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatFloat_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathRBFInterpolateQuatFloat::Execute"), &FRigUnit_MathRBFInterpolateQuatFloat::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathRBFInterpolateQuatFloat>()
{
	return FRigUnit_MathRBFInterpolateQuatFloat::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatFloat(FRigUnit_MathRBFInterpolateQuatFloat::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathRBFInterpolateQuatFloat"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateQuatFloat
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateQuatFloat()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathRBFInterpolateQuatFloat>(FName(TEXT("RigUnit_MathRBFInterpolateQuatFloat")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateQuatFloat;
	struct Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatFloat_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Targets_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Targets_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Targets;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Output_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Output;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatFloat_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "DisplayName", "Quaternion to Float" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
		{ "PrototypeName", "RBFInterpolateQuatToFloat" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatFloat_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathRBFInterpolateQuatFloat>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatFloat_Statics::NewProp_Targets_Inner = { "Targets", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FMathRBFInterpolateQuatFloat_Target, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatFloat_Statics::NewProp_Targets_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatFloat_Statics::NewProp_Targets = { "Targets", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathRBFInterpolateQuatFloat, Targets), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatFloat_Statics::NewProp_Targets_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatFloat_Statics::NewProp_Targets_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatFloat_Statics::NewProp_Output_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatFloat_Statics::NewProp_Output = { "Output", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathRBFInterpolateQuatFloat, Output), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatFloat_Statics::NewProp_Output_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatFloat_Statics::NewProp_Output_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatFloat_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatFloat_Statics::NewProp_Targets_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatFloat_Statics::NewProp_Targets,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatFloat_Statics::NewProp_Output,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatFloat_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase,
		&NewStructOps,
		"RigUnit_MathRBFInterpolateQuatFloat",
		sizeof(FRigUnit_MathRBFInterpolateQuatFloat),
		alignof(FRigUnit_MathRBFInterpolateQuatFloat),
		Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatFloat_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatFloat_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatFloat_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatFloat_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatFloat()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatFloat_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathRBFInterpolateQuatFloat"), sizeof(FRigUnit_MathRBFInterpolateQuatFloat), Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatFloat_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatFloat_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatFloat_Hash() { return 1318536384U; }

void FRigUnit_MathRBFInterpolateQuatFloat::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
	FRigVMFixedArray<FMathRBFInterpolateQuatFloat_Target> Targets_0_Array(Targets);
	
    StaticExecute(
		RigVMExecuteContext,
		Targets_0_Array,
		Output,
		Input,
		DistanceFunction,
		SmoothingFunction,
		SmoothingAngle,
		bNormalizeOutput,
		TwistAxis,
		WorkData,
		Context
	);
}

class UScriptStruct* FMathRBFInterpolateQuatFloat_Target::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FMathRBFInterpolateQuatFloat_Target_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMathRBFInterpolateQuatFloat_Target, Z_Construct_UPackage__Script_ControlRig(), TEXT("MathRBFInterpolateQuatFloat_Target"), sizeof(FMathRBFInterpolateQuatFloat_Target), Get_Z_Construct_UScriptStruct_FMathRBFInterpolateQuatFloat_Target_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FMathRBFInterpolateQuatFloat_Target>()
{
	return FMathRBFInterpolateQuatFloat_Target::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMathRBFInterpolateQuatFloat_Target(FMathRBFInterpolateQuatFloat_Target::StaticStruct, TEXT("/Script/ControlRig"), TEXT("MathRBFInterpolateQuatFloat_Target"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFMathRBFInterpolateQuatFloat_Target
{
	FScriptStruct_ControlRig_StaticRegisterNativesFMathRBFInterpolateQuatFloat_Target()
	{
		UScriptStruct::DeferCppStructOps<FMathRBFInterpolateQuatFloat_Target>(FName(TEXT("MathRBFInterpolateQuatFloat_Target")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFMathRBFInterpolateQuatFloat_Target;
	struct Z_Construct_UScriptStruct_FMathRBFInterpolateQuatFloat_Target_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Target_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Target;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMathRBFInterpolateQuatFloat_Target_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "// Quat -> T\n" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
		{ "ToolTip", "Quat -> T" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMathRBFInterpolateQuatFloat_Target_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMathRBFInterpolateQuatFloat_Target>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMathRBFInterpolateQuatFloat_Target_Statics::NewProp_Target_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMathRBFInterpolateQuatFloat_Target_Statics::NewProp_Target = { "Target", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMathRBFInterpolateQuatFloat_Target, Target), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FMathRBFInterpolateQuatFloat_Target_Statics::NewProp_Target_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMathRBFInterpolateQuatFloat_Target_Statics::NewProp_Target_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMathRBFInterpolateQuatFloat_Target_Statics::NewProp_Value_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FMathRBFInterpolateQuatFloat_Target_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMathRBFInterpolateQuatFloat_Target, Value), METADATA_PARAMS(Z_Construct_UScriptStruct_FMathRBFInterpolateQuatFloat_Target_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMathRBFInterpolateQuatFloat_Target_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMathRBFInterpolateQuatFloat_Target_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMathRBFInterpolateQuatFloat_Target_Statics::NewProp_Target,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMathRBFInterpolateQuatFloat_Target_Statics::NewProp_Value,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMathRBFInterpolateQuatFloat_Target_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"MathRBFInterpolateQuatFloat_Target",
		sizeof(FMathRBFInterpolateQuatFloat_Target),
		alignof(FMathRBFInterpolateQuatFloat_Target),
		Z_Construct_UScriptStruct_FMathRBFInterpolateQuatFloat_Target_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMathRBFInterpolateQuatFloat_Target_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMathRBFInterpolateQuatFloat_Target_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMathRBFInterpolateQuatFloat_Target_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMathRBFInterpolateQuatFloat_Target()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMathRBFInterpolateQuatFloat_Target_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MathRBFInterpolateQuatFloat_Target"), sizeof(FMathRBFInterpolateQuatFloat_Target), Get_Z_Construct_UScriptStruct_FMathRBFInterpolateQuatFloat_Target_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMathRBFInterpolateQuatFloat_Target_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMathRBFInterpolateQuatFloat_Target_Hash() { return 3494220828U; }

static_assert(std::is_polymorphic<FRigUnit_MathRBFInterpolateVectorBase>() == std::is_polymorphic<FRigUnit_MathRBFInterpolateBase>(), "USTRUCT FRigUnit_MathRBFInterpolateVectorBase cannot be polymorphic unless super FRigUnit_MathRBFInterpolateBase is polymorphic");

class UScriptStruct* FRigUnit_MathRBFInterpolateVectorBase::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathRBFInterpolateVectorBase"), sizeof(FRigUnit_MathRBFInterpolateVectorBase), Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathRBFInterpolateVectorBase>()
{
	return FRigUnit_MathRBFInterpolateVectorBase::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase(FRigUnit_MathRBFInterpolateVectorBase::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathRBFInterpolateVectorBase"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateVectorBase
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateVectorBase()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathRBFInterpolateVectorBase>(FName(TEXT("RigUnit_MathRBFInterpolateVectorBase")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateVectorBase;
	struct Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Input_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Input;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DistanceFunction_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DistanceFunction_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_DistanceFunction;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_SmoothingFunction_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SmoothingFunction_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_SmoothingFunction;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SmoothingRadius_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SmoothingRadius;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bNormalizeOutput_MetaData[];
#endif
		static void NewProp_bNormalizeOutput_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bNormalizeOutput;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorkData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WorkData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "Keywords", "RBF,Interpolate,Vector" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathRBFInterpolateVectorBase>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::NewProp_Input_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::NewProp_Input = { "Input", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathRBFInterpolateVectorBase, Input), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::NewProp_Input_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::NewProp_Input_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::NewProp_DistanceFunction_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::NewProp_DistanceFunction_MetaData[] = {
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::NewProp_DistanceFunction = { "DistanceFunction", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathRBFInterpolateVectorBase, DistanceFunction), Z_Construct_UEnum_ControlRig_ERBFVectorDistanceType, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::NewProp_DistanceFunction_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::NewProp_DistanceFunction_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::NewProp_SmoothingFunction_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::NewProp_SmoothingFunction_MetaData[] = {
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::NewProp_SmoothingFunction = { "SmoothingFunction", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathRBFInterpolateVectorBase, SmoothingFunction), Z_Construct_UEnum_ControlRig_ERBFKernelType, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::NewProp_SmoothingFunction_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::NewProp_SmoothingFunction_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::NewProp_SmoothingRadius_MetaData[] = {
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::NewProp_SmoothingRadius = { "SmoothingRadius", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathRBFInterpolateVectorBase, SmoothingRadius), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::NewProp_SmoothingRadius_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::NewProp_SmoothingRadius_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::NewProp_bNormalizeOutput_MetaData[] = {
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::NewProp_bNormalizeOutput_SetBit(void* Obj)
	{
		((FRigUnit_MathRBFInterpolateVectorBase*)Obj)->bNormalizeOutput = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::NewProp_bNormalizeOutput = { "bNormalizeOutput", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_MathRBFInterpolateVectorBase), &Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::NewProp_bNormalizeOutput_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::NewProp_bNormalizeOutput_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::NewProp_bNormalizeOutput_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::NewProp_WorkData_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::NewProp_WorkData = { "WorkData", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathRBFInterpolateVectorBase, WorkData), Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorWorkData, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::NewProp_WorkData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::NewProp_WorkData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::NewProp_Input,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::NewProp_DistanceFunction_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::NewProp_DistanceFunction,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::NewProp_SmoothingFunction_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::NewProp_SmoothingFunction,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::NewProp_SmoothingRadius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::NewProp_bNormalizeOutput,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::NewProp_WorkData,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateBase,
		&NewStructOps,
		"RigUnit_MathRBFInterpolateVectorBase",
		sizeof(FRigUnit_MathRBFInterpolateVectorBase),
		alignof(FRigUnit_MathRBFInterpolateVectorBase),
		Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathRBFInterpolateVectorBase"), sizeof(FRigUnit_MathRBFInterpolateVectorBase), Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Hash() { return 361307225U; }

static_assert(std::is_polymorphic<FRigUnit_MathRBFInterpolateQuatBase>() == std::is_polymorphic<FRigUnit_MathRBFInterpolateBase>(), "USTRUCT FRigUnit_MathRBFInterpolateQuatBase cannot be polymorphic unless super FRigUnit_MathRBFInterpolateBase is polymorphic");

class UScriptStruct* FRigUnit_MathRBFInterpolateQuatBase::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathRBFInterpolateQuatBase"), sizeof(FRigUnit_MathRBFInterpolateQuatBase), Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathRBFInterpolateQuatBase>()
{
	return FRigUnit_MathRBFInterpolateQuatBase::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase(FRigUnit_MathRBFInterpolateQuatBase::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathRBFInterpolateQuatBase"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateQuatBase
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateQuatBase()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathRBFInterpolateQuatBase>(FName(TEXT("RigUnit_MathRBFInterpolateQuatBase")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateQuatBase;
	struct Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Input_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Input;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DistanceFunction_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DistanceFunction_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_DistanceFunction;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_SmoothingFunction_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SmoothingFunction_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_SmoothingFunction;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SmoothingAngle_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SmoothingAngle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bNormalizeOutput_MetaData[];
#endif
		static void NewProp_bNormalizeOutput_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bNormalizeOutput;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TwistAxis_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TwistAxis;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorkData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WorkData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "Keywords", "RBF,Interpolate,Quaternion" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathRBFInterpolateQuatBase>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewProp_Input_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewProp_Input = { "Input", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathRBFInterpolateQuatBase, Input), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewProp_Input_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewProp_Input_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewProp_DistanceFunction_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewProp_DistanceFunction_MetaData[] = {
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewProp_DistanceFunction = { "DistanceFunction", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathRBFInterpolateQuatBase, DistanceFunction), Z_Construct_UEnum_ControlRig_ERBFQuatDistanceType, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewProp_DistanceFunction_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewProp_DistanceFunction_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewProp_SmoothingFunction_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewProp_SmoothingFunction_MetaData[] = {
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewProp_SmoothingFunction = { "SmoothingFunction", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathRBFInterpolateQuatBase, SmoothingFunction), Z_Construct_UEnum_ControlRig_ERBFKernelType, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewProp_SmoothingFunction_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewProp_SmoothingFunction_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewProp_SmoothingAngle_MetaData[] = {
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewProp_SmoothingAngle = { "SmoothingAngle", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathRBFInterpolateQuatBase, SmoothingAngle), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewProp_SmoothingAngle_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewProp_SmoothingAngle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewProp_bNormalizeOutput_MetaData[] = {
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewProp_bNormalizeOutput_SetBit(void* Obj)
	{
		((FRigUnit_MathRBFInterpolateQuatBase*)Obj)->bNormalizeOutput = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewProp_bNormalizeOutput = { "bNormalizeOutput", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_MathRBFInterpolateQuatBase), &Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewProp_bNormalizeOutput_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewProp_bNormalizeOutput_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewProp_bNormalizeOutput_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewProp_TwistAxis_MetaData[] = {
		{ "EditCondition", "DistanceFunction == ERBFQuatDistanceType::SwingAngle || DistanceFunction == ERBFQuatDistanceType::TwistAngle" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewProp_TwistAxis = { "TwistAxis", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathRBFInterpolateQuatBase, TwistAxis), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewProp_TwistAxis_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewProp_TwistAxis_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewProp_WorkData_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewProp_WorkData = { "WorkData", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathRBFInterpolateQuatBase, WorkData), Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatWorkData, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewProp_WorkData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewProp_WorkData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewProp_Input,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewProp_DistanceFunction_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewProp_DistanceFunction,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewProp_SmoothingFunction_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewProp_SmoothingFunction,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewProp_SmoothingAngle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewProp_bNormalizeOutput,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewProp_TwistAxis,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::NewProp_WorkData,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateBase,
		&NewStructOps,
		"RigUnit_MathRBFInterpolateQuatBase",
		sizeof(FRigUnit_MathRBFInterpolateQuatBase),
		alignof(FRigUnit_MathRBFInterpolateQuatBase),
		Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathRBFInterpolateQuatBase"), sizeof(FRigUnit_MathRBFInterpolateQuatBase), Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Hash() { return 2638789256U; }

static_assert(std::is_polymorphic<FRigUnit_MathRBFInterpolateBase>() == std::is_polymorphic<FRigUnit_MathBase>(), "USTRUCT FRigUnit_MathRBFInterpolateBase cannot be polymorphic unless super FRigUnit_MathBase is polymorphic");

class UScriptStruct* FRigUnit_MathRBFInterpolateBase::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateBase_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateBase, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathRBFInterpolateBase"), sizeof(FRigUnit_MathRBFInterpolateBase), Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateBase_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathRBFInterpolateBase>()
{
	return FRigUnit_MathRBFInterpolateBase::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathRBFInterpolateBase(FRigUnit_MathRBFInterpolateBase::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathRBFInterpolateBase"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateBase
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateBase()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathRBFInterpolateBase>(FName(TEXT("RigUnit_MathRBFInterpolateBase")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateBase;
	struct Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateBase_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateBase_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "Category", "Math|RBF Interpolation" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateBase_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathRBFInterpolateBase>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateBase_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathBase,
		&NewStructOps,
		"RigUnit_MathRBFInterpolateBase",
		sizeof(FRigUnit_MathRBFInterpolateBase),
		alignof(FRigUnit_MathRBFInterpolateBase),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateBase_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateBase_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateBase()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateBase_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathRBFInterpolateBase"), sizeof(FRigUnit_MathRBFInterpolateBase), Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateBase_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateBase_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateBase_Hash() { return 2586451950U; }
class UScriptStruct* FRigUnit_MathRBFInterpolateVectorWorkData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorWorkData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorWorkData, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathRBFInterpolateVectorWorkData"), sizeof(FRigUnit_MathRBFInterpolateVectorWorkData), Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorWorkData_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathRBFInterpolateVectorWorkData>()
{
	return FRigUnit_MathRBFInterpolateVectorWorkData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorWorkData(FRigUnit_MathRBFInterpolateVectorWorkData::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathRBFInterpolateVectorWorkData"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateVectorWorkData
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateVectorWorkData()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathRBFInterpolateVectorWorkData>(FName(TEXT("RigUnit_MathRBFInterpolateVectorWorkData")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateVectorWorkData;
	struct Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorWorkData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorWorkData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorWorkData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathRBFInterpolateVectorWorkData>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorWorkData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"RigUnit_MathRBFInterpolateVectorWorkData",
		sizeof(FRigUnit_MathRBFInterpolateVectorWorkData),
		alignof(FRigUnit_MathRBFInterpolateVectorWorkData),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorWorkData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorWorkData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorWorkData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorWorkData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathRBFInterpolateVectorWorkData"), sizeof(FRigUnit_MathRBFInterpolateVectorWorkData), Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorWorkData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorWorkData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorWorkData_Hash() { return 263967268U; }
class UScriptStruct* FRigUnit_MathRBFInterpolateQuatWorkData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatWorkData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatWorkData, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathRBFInterpolateQuatWorkData"), sizeof(FRigUnit_MathRBFInterpolateQuatWorkData), Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatWorkData_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathRBFInterpolateQuatWorkData>()
{
	return FRigUnit_MathRBFInterpolateQuatWorkData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatWorkData(FRigUnit_MathRBFInterpolateQuatWorkData::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathRBFInterpolateQuatWorkData"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateQuatWorkData
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateQuatWorkData()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathRBFInterpolateQuatWorkData>(FName(TEXT("RigUnit_MathRBFInterpolateQuatWorkData")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathRBFInterpolateQuatWorkData;
	struct Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatWorkData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatWorkData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathRBFInterpolate.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatWorkData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathRBFInterpolateQuatWorkData>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatWorkData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"RigUnit_MathRBFInterpolateQuatWorkData",
		sizeof(FRigUnit_MathRBFInterpolateQuatWorkData),
		alignof(FRigUnit_MathRBFInterpolateQuatWorkData),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatWorkData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatWorkData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatWorkData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatWorkData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathRBFInterpolateQuatWorkData"), sizeof(FRigUnit_MathRBFInterpolateQuatWorkData), Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatWorkData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatWorkData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatWorkData_Hash() { return 500711640U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
