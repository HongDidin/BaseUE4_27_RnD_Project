// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_ControlRigControlActor_generated_h
#error "ControlRigControlActor.generated.h already included, missing '#pragma once' in ControlRigControlActor.h"
#endif
#define CONTROLRIG_ControlRigControlActor_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigControlActor_h_20_SPARSE_DATA
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigControlActor_h_20_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execRefresh); \
	DECLARE_FUNCTION(execClear);


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigControlActor_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execRefresh); \
	DECLARE_FUNCTION(execClear);


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigControlActor_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAControlRigControlActor(); \
	friend struct Z_Construct_UClass_AControlRigControlActor_Statics; \
public: \
	DECLARE_CLASS(AControlRigControlActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ControlRig"), NO_API) \
	DECLARE_SERIALIZER(AControlRigControlActor)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigControlActor_h_20_INCLASS \
private: \
	static void StaticRegisterNativesAControlRigControlActor(); \
	friend struct Z_Construct_UClass_AControlRigControlActor_Statics; \
public: \
	DECLARE_CLASS(AControlRigControlActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ControlRig"), NO_API) \
	DECLARE_SERIALIZER(AControlRigControlActor)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigControlActor_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AControlRigControlActor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AControlRigControlActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AControlRigControlActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AControlRigControlActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AControlRigControlActor(AControlRigControlActor&&); \
	NO_API AControlRigControlActor(const AControlRigControlActor&); \
public:


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigControlActor_h_20_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AControlRigControlActor(AControlRigControlActor&&); \
	NO_API AControlRigControlActor(const AControlRigControlActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AControlRigControlActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AControlRigControlActor); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AControlRigControlActor)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigControlActor_h_20_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ActorRootComponent() { return STRUCT_OFFSET(AControlRigControlActor, ActorRootComponent); } \
	FORCEINLINE static uint32 __PPO__ControlRig() { return STRUCT_OFFSET(AControlRigControlActor, ControlRig); } \
	FORCEINLINE static uint32 __PPO__ControlNames() { return STRUCT_OFFSET(AControlRigControlActor, ControlNames); } \
	FORCEINLINE static uint32 __PPO__GizmoTransforms() { return STRUCT_OFFSET(AControlRigControlActor, GizmoTransforms); } \
	FORCEINLINE static uint32 __PPO__Components() { return STRUCT_OFFSET(AControlRigControlActor, Components); } \
	FORCEINLINE static uint32 __PPO__Materials() { return STRUCT_OFFSET(AControlRigControlActor, Materials); } \
	FORCEINLINE static uint32 __PPO__ColorParameterName() { return STRUCT_OFFSET(AControlRigControlActor, ColorParameterName); }


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigControlActor_h_17_PROLOG
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigControlActor_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigControlActor_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigControlActor_h_20_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigControlActor_h_20_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigControlActor_h_20_INCLASS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigControlActor_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigControlActor_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigControlActor_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigControlActor_h_20_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigControlActor_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigControlActor_h_20_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigControlActor_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONTROLRIG_API UClass* StaticClass<class AControlRigControlActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigControlActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
