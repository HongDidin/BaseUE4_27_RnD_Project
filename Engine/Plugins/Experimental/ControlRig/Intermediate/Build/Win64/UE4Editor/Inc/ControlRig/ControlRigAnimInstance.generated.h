// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_ControlRigAnimInstance_generated_h
#error "ControlRigAnimInstance.generated.h already included, missing '#pragma once' in ControlRigAnimInstance.h"
#endif
#define CONTROLRIG_ControlRigAnimInstance_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigAnimInstance_h_14_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FControlRigAnimInstanceProxy_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FAnimInstanceProxy Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FControlRigAnimInstanceProxy>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigAnimInstance_h_40_SPARSE_DATA
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigAnimInstance_h_40_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigAnimInstance_h_40_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigAnimInstance_h_40_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUControlRigAnimInstance(); \
	friend struct Z_Construct_UClass_UControlRigAnimInstance_Statics; \
public: \
	DECLARE_CLASS(UControlRigAnimInstance, UAnimInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ControlRig"), NO_API) \
	DECLARE_SERIALIZER(UControlRigAnimInstance)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigAnimInstance_h_40_INCLASS \
private: \
	static void StaticRegisterNativesUControlRigAnimInstance(); \
	friend struct Z_Construct_UClass_UControlRigAnimInstance_Statics; \
public: \
	DECLARE_CLASS(UControlRigAnimInstance, UAnimInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ControlRig"), NO_API) \
	DECLARE_SERIALIZER(UControlRigAnimInstance)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigAnimInstance_h_40_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UControlRigAnimInstance(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigAnimInstance) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigAnimInstance); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigAnimInstance); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigAnimInstance(UControlRigAnimInstance&&); \
	NO_API UControlRigAnimInstance(const UControlRigAnimInstance&); \
public:


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigAnimInstance_h_40_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UControlRigAnimInstance(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigAnimInstance(UControlRigAnimInstance&&); \
	NO_API UControlRigAnimInstance(const UControlRigAnimInstance&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigAnimInstance); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigAnimInstance); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigAnimInstance)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigAnimInstance_h_40_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigAnimInstance_h_37_PROLOG
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigAnimInstance_h_40_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigAnimInstance_h_40_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigAnimInstance_h_40_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigAnimInstance_h_40_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigAnimInstance_h_40_INCLASS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigAnimInstance_h_40_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigAnimInstance_h_40_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigAnimInstance_h_40_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigAnimInstance_h_40_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigAnimInstance_h_40_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigAnimInstance_h_40_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigAnimInstance_h_40_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ControlRigAnimInstance."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONTROLRIG_API UClass* StaticClass<class UControlRigAnimInstance>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigAnimInstance_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
