// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigUnit_ProjectTransformToNewParent_generated_h
#error "RigUnit_ProjectTransformToNewParent.generated.h already included, missing '#pragma once' in RigUnit_ProjectTransformToNewParent.h"
#endif
#define CONTROLRIG_RigUnit_ProjectTransformToNewParent_generated_h


#define FRigUnit_ProjectTransformToNewParent_Execute() \
	void FRigUnit_ProjectTransformToNewParent::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKey& Child, \
		const bool bChildInitial, \
		const FRigElementKey& OldParent, \
		const bool bOldParentInitial, \
		const FRigElementKey& NewParent, \
		const bool bNewParentInitial, \
		FTransform& Transform, \
		FCachedRigElement& CachedChild, \
		FCachedRigElement& CachedOldParent, \
		FCachedRigElement& CachedNewParent, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Hierarchy_RigUnit_ProjectTransformToNewParent_h_19_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKey& Child, \
		const bool bChildInitial, \
		const FRigElementKey& OldParent, \
		const bool bOldParentInitial, \
		const FRigElementKey& NewParent, \
		const bool bNewParentInitial, \
		FTransform& Transform, \
		FCachedRigElement& CachedChild, \
		FCachedRigElement& CachedOldParent, \
		FCachedRigElement& CachedNewParent, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FRigElementKey& Child = *(FRigElementKey*)RigVMMemoryHandles[0].GetData(); \
		const bool bChildInitial = *(bool*)RigVMMemoryHandles[1].GetData(); \
		const FRigElementKey& OldParent = *(FRigElementKey*)RigVMMemoryHandles[2].GetData(); \
		const bool bOldParentInitial = *(bool*)RigVMMemoryHandles[3].GetData(); \
		const FRigElementKey& NewParent = *(FRigElementKey*)RigVMMemoryHandles[4].GetData(); \
		const bool bNewParentInitial = *(bool*)RigVMMemoryHandles[5].GetData(); \
		FTransform& Transform = *(FTransform*)RigVMMemoryHandles[6].GetData(); \
		FRigVMDynamicArray<FCachedRigElement> CachedChild_7_Array(*((FRigVMByteArray*)RigVMMemoryHandles[7].GetData(0, false))); \
		CachedChild_7_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedChild = CachedChild_7_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<FCachedRigElement> CachedOldParent_8_Array(*((FRigVMByteArray*)RigVMMemoryHandles[8].GetData(0, false))); \
		CachedOldParent_8_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedOldParent = CachedOldParent_8_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<FCachedRigElement> CachedNewParent_9_Array(*((FRigVMByteArray*)RigVMMemoryHandles[9].GetData(0, false))); \
		CachedNewParent_9_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedNewParent = CachedNewParent_9_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Child, \
			bChildInitial, \
			OldParent, \
			bOldParentInitial, \
			NewParent, \
			bNewParentInitial, \
			Transform, \
			CachedChild, \
			CachedOldParent, \
			CachedNewParent, \
			Context \
		); \
	} \
	typedef FRigUnit Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_ProjectTransformToNewParent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Hierarchy_RigUnit_ProjectTransformToNewParent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
