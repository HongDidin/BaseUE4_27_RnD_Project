// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class ERigHierarchyImportMode : uint8;
struct FRigElementKey;
struct FTransform;
struct FRigCurve;
struct FRigSpace;
enum class ERigSpaceType : uint8;
enum class ERigControlValueType : uint8;
struct FRotator;
struct FVector;
struct FVector2D;
struct FRigControl;
enum class ERigControlType : uint8;
struct FLinearColor;
struct FRigBone;
enum class ERigBoneType : uint8;
#ifdef CONTROLRIGDEVELOPER_ControlRigHierarchyModifier_generated_h
#error "ControlRigHierarchyModifier.generated.h already included, missing '#pragma once' in ControlRigHierarchyModifier.h"
#endif
#define CONTROLRIGDEVELOPER_ControlRigHierarchyModifier_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigHierarchyModifier_h_14_SPARSE_DATA
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigHierarchyModifier_h_14_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigHierarchyModifier_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#if WITH_EDITOR
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigHierarchyModifier_h_14_EDITOR_ONLY_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execImportFromText); \
	DECLARE_FUNCTION(execExportToText); \
	DECLARE_FUNCTION(execSetGlobalTransform); \
	DECLARE_FUNCTION(execGetGlobalTransform); \
	DECLARE_FUNCTION(execSetLocalTransform); \
	DECLARE_FUNCTION(execGetLocalTransform); \
	DECLARE_FUNCTION(execSetInitialGlobalTransform); \
	DECLARE_FUNCTION(execGetInitialGlobalTransform); \
	DECLARE_FUNCTION(execSetInitialTransform); \
	DECLARE_FUNCTION(execGetInitialTransform); \
	DECLARE_FUNCTION(execResetTransforms); \
	DECLARE_FUNCTION(execReset); \
	DECLARE_FUNCTION(execInitialize); \
	DECLARE_FUNCTION(execIsSelected); \
	DECLARE_FUNCTION(execClearSelection); \
	DECLARE_FUNCTION(execSelect); \
	DECLARE_FUNCTION(execGetSelection); \
	DECLARE_FUNCTION(execReparentElement); \
	DECLARE_FUNCTION(execRenameElement); \
	DECLARE_FUNCTION(execRemoveElement); \
	DECLARE_FUNCTION(execSetCurve); \
	DECLARE_FUNCTION(execGetCurve); \
	DECLARE_FUNCTION(execAddCurve); \
	DECLARE_FUNCTION(execSetSpace); \
	DECLARE_FUNCTION(execGetSpace); \
	DECLARE_FUNCTION(execAddSpace); \
	DECLARE_FUNCTION(execSetControlValueTransform); \
	DECLARE_FUNCTION(execSetControlValueRotator); \
	DECLARE_FUNCTION(execSetControlValueVector); \
	DECLARE_FUNCTION(execSetControlValueVector2D); \
	DECLARE_FUNCTION(execSetControlValueFloat); \
	DECLARE_FUNCTION(execSetControlValueInt); \
	DECLARE_FUNCTION(execSetControlValueBool); \
	DECLARE_FUNCTION(execGetControlValueTransform); \
	DECLARE_FUNCTION(execGetControlValueRotator); \
	DECLARE_FUNCTION(execGetControlValueVector); \
	DECLARE_FUNCTION(execGetControlValueVector2D); \
	DECLARE_FUNCTION(execGetControlValueFloat); \
	DECLARE_FUNCTION(execGetControlValueInt); \
	DECLARE_FUNCTION(execGetControlValueBool); \
	DECLARE_FUNCTION(execSetControl); \
	DECLARE_FUNCTION(execGetControl); \
	DECLARE_FUNCTION(execAddControl); \
	DECLARE_FUNCTION(execSetBone); \
	DECLARE_FUNCTION(execGetBone); \
	DECLARE_FUNCTION(execAddBone); \
	DECLARE_FUNCTION(execGetElements);


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigHierarchyModifier_h_14_EDITOR_ONLY_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execImportFromText); \
	DECLARE_FUNCTION(execExportToText); \
	DECLARE_FUNCTION(execSetGlobalTransform); \
	DECLARE_FUNCTION(execGetGlobalTransform); \
	DECLARE_FUNCTION(execSetLocalTransform); \
	DECLARE_FUNCTION(execGetLocalTransform); \
	DECLARE_FUNCTION(execSetInitialGlobalTransform); \
	DECLARE_FUNCTION(execGetInitialGlobalTransform); \
	DECLARE_FUNCTION(execSetInitialTransform); \
	DECLARE_FUNCTION(execGetInitialTransform); \
	DECLARE_FUNCTION(execResetTransforms); \
	DECLARE_FUNCTION(execReset); \
	DECLARE_FUNCTION(execInitialize); \
	DECLARE_FUNCTION(execIsSelected); \
	DECLARE_FUNCTION(execClearSelection); \
	DECLARE_FUNCTION(execSelect); \
	DECLARE_FUNCTION(execGetSelection); \
	DECLARE_FUNCTION(execReparentElement); \
	DECLARE_FUNCTION(execRenameElement); \
	DECLARE_FUNCTION(execRemoveElement); \
	DECLARE_FUNCTION(execSetCurve); \
	DECLARE_FUNCTION(execGetCurve); \
	DECLARE_FUNCTION(execAddCurve); \
	DECLARE_FUNCTION(execSetSpace); \
	DECLARE_FUNCTION(execGetSpace); \
	DECLARE_FUNCTION(execAddSpace); \
	DECLARE_FUNCTION(execSetControlValueTransform); \
	DECLARE_FUNCTION(execSetControlValueRotator); \
	DECLARE_FUNCTION(execSetControlValueVector); \
	DECLARE_FUNCTION(execSetControlValueVector2D); \
	DECLARE_FUNCTION(execSetControlValueFloat); \
	DECLARE_FUNCTION(execSetControlValueInt); \
	DECLARE_FUNCTION(execSetControlValueBool); \
	DECLARE_FUNCTION(execGetControlValueTransform); \
	DECLARE_FUNCTION(execGetControlValueRotator); \
	DECLARE_FUNCTION(execGetControlValueVector); \
	DECLARE_FUNCTION(execGetControlValueVector2D); \
	DECLARE_FUNCTION(execGetControlValueFloat); \
	DECLARE_FUNCTION(execGetControlValueInt); \
	DECLARE_FUNCTION(execGetControlValueBool); \
	DECLARE_FUNCTION(execSetControl); \
	DECLARE_FUNCTION(execGetControl); \
	DECLARE_FUNCTION(execAddControl); \
	DECLARE_FUNCTION(execSetBone); \
	DECLARE_FUNCTION(execGetBone); \
	DECLARE_FUNCTION(execAddBone); \
	DECLARE_FUNCTION(execGetElements);


#else
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigHierarchyModifier_h_14_EDITOR_ONLY_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigHierarchyModifier_h_14_EDITOR_ONLY_RPC_WRAPPERS_NO_PURE_DECLS
#endif //WITH_EDITOR
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigHierarchyModifier_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUControlRigHierarchyModifier(); \
	friend struct Z_Construct_UClass_UControlRigHierarchyModifier_Statics; \
public: \
	DECLARE_CLASS(UControlRigHierarchyModifier, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRigDeveloper"), NO_API) \
	DECLARE_SERIALIZER(UControlRigHierarchyModifier)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigHierarchyModifier_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUControlRigHierarchyModifier(); \
	friend struct Z_Construct_UClass_UControlRigHierarchyModifier_Statics; \
public: \
	DECLARE_CLASS(UControlRigHierarchyModifier, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRigDeveloper"), NO_API) \
	DECLARE_SERIALIZER(UControlRigHierarchyModifier)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigHierarchyModifier_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UControlRigHierarchyModifier(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigHierarchyModifier) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigHierarchyModifier); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigHierarchyModifier); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigHierarchyModifier(UControlRigHierarchyModifier&&); \
	NO_API UControlRigHierarchyModifier(const UControlRigHierarchyModifier&); \
public:


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigHierarchyModifier_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigHierarchyModifier(UControlRigHierarchyModifier&&); \
	NO_API UControlRigHierarchyModifier(const UControlRigHierarchyModifier&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigHierarchyModifier); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigHierarchyModifier); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UControlRigHierarchyModifier)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigHierarchyModifier_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigHierarchyModifier_h_11_PROLOG
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigHierarchyModifier_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigHierarchyModifier_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigHierarchyModifier_h_14_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigHierarchyModifier_h_14_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigHierarchyModifier_h_14_EDITOR_ONLY_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigHierarchyModifier_h_14_INCLASS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigHierarchyModifier_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigHierarchyModifier_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigHierarchyModifier_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigHierarchyModifier_h_14_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigHierarchyModifier_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigHierarchyModifier_h_14_EDITOR_ONLY_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigHierarchyModifier_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigHierarchyModifier_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONTROLRIGDEVELOPER_API UClass* StaticClass<class UControlRigHierarchyModifier>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigHierarchyModifier_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
