// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIGDEVELOPER_ControlRigGraphNode_generated_h
#error "ControlRigGraphNode.generated.h already included, missing '#pragma once' in ControlRigGraphNode.h"
#endif
#define CONTROLRIGDEVELOPER_ControlRigGraphNode_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphNode_h_24_SPARSE_DATA
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphNode_h_24_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphNode_h_24_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphNode_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUControlRigGraphNode(); \
	friend struct Z_Construct_UClass_UControlRigGraphNode_Statics; \
public: \
	DECLARE_CLASS(UControlRigGraphNode, UEdGraphNode, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRigDeveloper"), NO_API) \
	DECLARE_SERIALIZER(UControlRigGraphNode)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphNode_h_24_INCLASS \
private: \
	static void StaticRegisterNativesUControlRigGraphNode(); \
	friend struct Z_Construct_UClass_UControlRigGraphNode_Statics; \
public: \
	DECLARE_CLASS(UControlRigGraphNode, UEdGraphNode, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRigDeveloper"), NO_API) \
	DECLARE_SERIALIZER(UControlRigGraphNode)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphNode_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UControlRigGraphNode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigGraphNode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigGraphNode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigGraphNode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigGraphNode(UControlRigGraphNode&&); \
	NO_API UControlRigGraphNode(const UControlRigGraphNode&); \
public:


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphNode_h_24_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigGraphNode(UControlRigGraphNode&&); \
	NO_API UControlRigGraphNode(const UControlRigGraphNode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigGraphNode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigGraphNode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UControlRigGraphNode)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphNode_h_24_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ModelNodePath() { return STRUCT_OFFSET(UControlRigGraphNode, ModelNodePath); } \
	FORCEINLINE static uint32 __PPO__CachedModelNode() { return STRUCT_OFFSET(UControlRigGraphNode, CachedModelNode); } \
	FORCEINLINE static uint32 __PPO__CachedModelPins() { return STRUCT_OFFSET(UControlRigGraphNode, CachedModelPins); } \
	FORCEINLINE static uint32 __PPO__PropertyName_DEPRECATED() { return STRUCT_OFFSET(UControlRigGraphNode, PropertyName_DEPRECATED); } \
	FORCEINLINE static uint32 __PPO__StructPath_DEPRECATED() { return STRUCT_OFFSET(UControlRigGraphNode, StructPath_DEPRECATED); } \
	FORCEINLINE static uint32 __PPO__PinType_DEPRECATED() { return STRUCT_OFFSET(UControlRigGraphNode, PinType_DEPRECATED); } \
	FORCEINLINE static uint32 __PPO__ParameterType_DEPRECATED() { return STRUCT_OFFSET(UControlRigGraphNode, ParameterType_DEPRECATED); } \
	FORCEINLINE static uint32 __PPO__ExpandedPins_DEPRECATED() { return STRUCT_OFFSET(UControlRigGraphNode, ExpandedPins_DEPRECATED); }


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphNode_h_21_PROLOG
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphNode_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphNode_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphNode_h_24_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphNode_h_24_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphNode_h_24_INCLASS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphNode_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphNode_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphNode_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphNode_h_24_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphNode_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphNode_h_24_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphNode_h_24_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONTROLRIGDEVELOPER_API UClass* StaticClass<class UControlRigGraphNode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphNode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
