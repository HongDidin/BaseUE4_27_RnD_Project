// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Highlevel/Hierarchy/RigUnit_TransformConstraint.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_TransformConstraint() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_HighlevelBaseMutable();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigElementKey();
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_ETransformSpaceMode();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FConstraintTarget();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_WorkData();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_TransformConstraint();
	ANIMATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConstraintData();
	ANIMATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FTransformFilter();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_TransformConstraintPerItem>() == std::is_polymorphic<FRigUnit_HighlevelBaseMutable>(), "USTRUCT FRigUnit_TransformConstraintPerItem cannot be polymorphic unless super FRigUnit_HighlevelBaseMutable is polymorphic");

class UScriptStruct* FRigUnit_TransformConstraintPerItem::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_TransformConstraintPerItem"), sizeof(FRigUnit_TransformConstraintPerItem), Get_Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_TransformConstraintPerItem::Execute"), &FRigUnit_TransformConstraintPerItem::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_TransformConstraintPerItem>()
{
	return FRigUnit_TransformConstraintPerItem::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_TransformConstraintPerItem(FRigUnit_TransformConstraintPerItem::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_TransformConstraintPerItem"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_TransformConstraintPerItem
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_TransformConstraintPerItem()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_TransformConstraintPerItem>(FName(TEXT("RigUnit_TransformConstraintPerItem")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_TransformConstraintPerItem;
	struct Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Item_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Item;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_BaseTransformSpace_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseTransformSpace_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_BaseTransformSpace;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseTransform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BaseTransform;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseItem_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BaseItem;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Targets_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Targets_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Targets;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseInitialTransforms_MetaData[];
#endif
		static void NewProp_bUseInitialTransforms_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseInitialTransforms;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorkData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WorkData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Transforms" },
		{ "Comment", "/**\n * Constrains an item's transform to multiple items' transforms\n */" },
		{ "DisplayName", "Transform Constraint" },
		{ "Keywords", "Parent,Orient,Scale" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TransformConstraint.h" },
		{ "ToolTip", "Constrains an item's transform to multiple items' transforms" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_TransformConstraintPerItem>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewProp_Item_MetaData[] = {
		{ "Category", "Constraint" },
		{ "ExpandByDefault", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TransformConstraint.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewProp_Item = { "Item", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TransformConstraintPerItem, Item), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewProp_Item_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewProp_Item_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewProp_BaseTransformSpace_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewProp_BaseTransformSpace_MetaData[] = {
		{ "Category", "Constraint" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TransformConstraint.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewProp_BaseTransformSpace = { "BaseTransformSpace", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TransformConstraintPerItem, BaseTransformSpace), Z_Construct_UEnum_ControlRig_ETransformSpaceMode, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewProp_BaseTransformSpace_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewProp_BaseTransformSpace_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewProp_BaseTransform_MetaData[] = {
		{ "Category", "Constraint" },
		{ "Comment", "// Transform op option. Use if ETransformSpace is BaseTransform\n" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TransformConstraint.h" },
		{ "ToolTip", "Transform op option. Use if ETransformSpace is BaseTransform" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewProp_BaseTransform = { "BaseTransform", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TransformConstraintPerItem, BaseTransform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewProp_BaseTransform_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewProp_BaseTransform_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewProp_BaseItem_MetaData[] = {
		{ "Category", "Constraint" },
		{ "Comment", "// Transform op option. Use if ETransformSpace is BaseJoint\n" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TransformConstraint.h" },
		{ "ToolTip", "Transform op option. Use if ETransformSpace is BaseJoint" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewProp_BaseItem = { "BaseItem", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TransformConstraintPerItem, BaseItem), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewProp_BaseItem_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewProp_BaseItem_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewProp_Targets_Inner = { "Targets", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FConstraintTarget, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewProp_Targets_MetaData[] = {
		{ "Category", "Constraint" },
		{ "DefaultArraySize", "1" },
		{ "ExpandByDefault", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TransformConstraint.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewProp_Targets = { "Targets", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TransformConstraintPerItem, Targets), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewProp_Targets_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewProp_Targets_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewProp_bUseInitialTransforms_MetaData[] = {
		{ "Category", "Constraint" },
		{ "Comment", "// If checked the initial transform will be used for the constraint data\n" },
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TransformConstraint.h" },
		{ "ToolTip", "If checked the initial transform will be used for the constraint data" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewProp_bUseInitialTransforms_SetBit(void* Obj)
	{
		((FRigUnit_TransformConstraintPerItem*)Obj)->bUseInitialTransforms = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewProp_bUseInitialTransforms = { "bUseInitialTransforms", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_TransformConstraintPerItem), &Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewProp_bUseInitialTransforms_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewProp_bUseInitialTransforms_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewProp_bUseInitialTransforms_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewProp_WorkData_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TransformConstraint.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewProp_WorkData = { "WorkData", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TransformConstraintPerItem, WorkData), Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_WorkData, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewProp_WorkData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewProp_WorkData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewProp_Item,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewProp_BaseTransformSpace_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewProp_BaseTransformSpace,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewProp_BaseTransform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewProp_BaseItem,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewProp_Targets_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewProp_Targets,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewProp_bUseInitialTransforms,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::NewProp_WorkData,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_HighlevelBaseMutable,
		&NewStructOps,
		"RigUnit_TransformConstraintPerItem",
		sizeof(FRigUnit_TransformConstraintPerItem),
		alignof(FRigUnit_TransformConstraintPerItem),
		Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_TransformConstraintPerItem"), sizeof(FRigUnit_TransformConstraintPerItem), Get_Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Hash() { return 2290409524U; }

void FRigUnit_TransformConstraintPerItem::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
	FRigVMFixedArray<FConstraintTarget> Targets_4_Array(Targets);
	
    StaticExecute(
		RigVMExecuteContext,
		Item,
		BaseTransformSpace,
		BaseTransform,
		BaseItem,
		Targets_4_Array,
		bUseInitialTransforms,
		WorkData,
		ExecuteContext,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_TransformConstraint>() == std::is_polymorphic<FRigUnit_HighlevelBaseMutable>(), "USTRUCT FRigUnit_TransformConstraint cannot be polymorphic unless super FRigUnit_HighlevelBaseMutable is polymorphic");

class UScriptStruct* FRigUnit_TransformConstraint::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_TransformConstraint, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_TransformConstraint"), sizeof(FRigUnit_TransformConstraint), Get_Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_TransformConstraint::Execute"), &FRigUnit_TransformConstraint::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_TransformConstraint>()
{
	return FRigUnit_TransformConstraint::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_TransformConstraint(FRigUnit_TransformConstraint::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_TransformConstraint"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_TransformConstraint
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_TransformConstraint()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_TransformConstraint>(FName(TEXT("RigUnit_TransformConstraint")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_TransformConstraint;
	struct Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Bone_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Bone;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_BaseTransformSpace_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseTransformSpace_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_BaseTransformSpace;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseTransform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BaseTransform;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseBone_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_BaseBone;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Targets_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Targets_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Targets;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseInitialTransforms_MetaData[];
#endif
		static void NewProp_bUseInitialTransforms_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseInitialTransforms;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorkData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WorkData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Transforms" },
		{ "Deprecated", "4.25" },
		{ "DisplayName", "Transform Constraint" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TransformConstraint.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_TransformConstraint>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewProp_Bone_MetaData[] = {
		{ "Category", "Constraint" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TransformConstraint.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewProp_Bone = { "Bone", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TransformConstraint, Bone), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewProp_Bone_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewProp_Bone_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewProp_BaseTransformSpace_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewProp_BaseTransformSpace_MetaData[] = {
		{ "Category", "Constraint" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TransformConstraint.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewProp_BaseTransformSpace = { "BaseTransformSpace", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TransformConstraint, BaseTransformSpace), Z_Construct_UEnum_ControlRig_ETransformSpaceMode, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewProp_BaseTransformSpace_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewProp_BaseTransformSpace_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewProp_BaseTransform_MetaData[] = {
		{ "Category", "Constraint" },
		{ "Comment", "// Transform op option. Use if ETransformSpace is BaseTransform\n" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TransformConstraint.h" },
		{ "ToolTip", "Transform op option. Use if ETransformSpace is BaseTransform" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewProp_BaseTransform = { "BaseTransform", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TransformConstraint, BaseTransform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewProp_BaseTransform_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewProp_BaseTransform_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewProp_BaseBone_MetaData[] = {
		{ "Category", "Constraint" },
		{ "Comment", "// Transform op option. Use if ETransformSpace is BaseJoint\n" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TransformConstraint.h" },
		{ "ToolTip", "Transform op option. Use if ETransformSpace is BaseJoint" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewProp_BaseBone = { "BaseBone", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TransformConstraint, BaseBone), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewProp_BaseBone_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewProp_BaseBone_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewProp_Targets_Inner = { "Targets", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FConstraintTarget, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewProp_Targets_MetaData[] = {
		{ "Category", "Constraint" },
		{ "DefaultArraySize", "1" },
		{ "ExpandByDefault", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TransformConstraint.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewProp_Targets = { "Targets", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TransformConstraint, Targets), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewProp_Targets_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewProp_Targets_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewProp_bUseInitialTransforms_MetaData[] = {
		{ "Category", "Constraint" },
		{ "Comment", "// If checked the initial transform will be used for the constraint data\n" },
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TransformConstraint.h" },
		{ "ToolTip", "If checked the initial transform will be used for the constraint data" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewProp_bUseInitialTransforms_SetBit(void* Obj)
	{
		((FRigUnit_TransformConstraint*)Obj)->bUseInitialTransforms = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewProp_bUseInitialTransforms = { "bUseInitialTransforms", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_TransformConstraint), &Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewProp_bUseInitialTransforms_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewProp_bUseInitialTransforms_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewProp_bUseInitialTransforms_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewProp_WorkData_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TransformConstraint.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewProp_WorkData = { "WorkData", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TransformConstraint, WorkData), Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_WorkData, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewProp_WorkData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewProp_WorkData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewProp_Bone,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewProp_BaseTransformSpace_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewProp_BaseTransformSpace,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewProp_BaseTransform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewProp_BaseBone,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewProp_Targets_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewProp_Targets,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewProp_bUseInitialTransforms,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::NewProp_WorkData,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_HighlevelBaseMutable,
		&NewStructOps,
		"RigUnit_TransformConstraint",
		sizeof(FRigUnit_TransformConstraint),
		alignof(FRigUnit_TransformConstraint),
		Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_TransformConstraint()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_TransformConstraint"), sizeof(FRigUnit_TransformConstraint), Get_Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Hash() { return 4236201667U; }

void FRigUnit_TransformConstraint::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
	FRigVMFixedArray<FConstraintTarget> Targets_4_Array(Targets);
	
    StaticExecute(
		RigVMExecuteContext,
		Bone,
		BaseTransformSpace,
		BaseTransform,
		BaseBone,
		Targets_4_Array,
		bUseInitialTransforms,
		WorkData,
		ExecuteContext,
		Context
	);
}

class UScriptStruct* FRigUnit_TransformConstraint_WorkData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_WorkData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_WorkData, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_TransformConstraint_WorkData"), sizeof(FRigUnit_TransformConstraint_WorkData), Get_Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_WorkData_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_TransformConstraint_WorkData>()
{
	return FRigUnit_TransformConstraint_WorkData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_TransformConstraint_WorkData(FRigUnit_TransformConstraint_WorkData::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_TransformConstraint_WorkData"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_TransformConstraint_WorkData
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_TransformConstraint_WorkData()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_TransformConstraint_WorkData>(FName(TEXT("RigUnit_TransformConstraint_WorkData")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_TransformConstraint_WorkData;
	struct Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_WorkData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ConstraintData_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConstraintData_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ConstraintData;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ConstraintDataToTargets_ValueProp;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ConstraintDataToTargets_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConstraintDataToTargets_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_ConstraintDataToTargets;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_WorkData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TransformConstraint.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_WorkData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_TransformConstraint_WorkData>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_WorkData_Statics::NewProp_ConstraintData_Inner = { "ConstraintData", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FConstraintData, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_WorkData_Statics::NewProp_ConstraintData_MetaData[] = {
		{ "Comment", "// note that Targets.Num () != ConstraintData.Num()\n" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TransformConstraint.h" },
		{ "ToolTip", "note that Targets.Num () != ConstraintData.Num()" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_WorkData_Statics::NewProp_ConstraintData = { "ConstraintData", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TransformConstraint_WorkData, ConstraintData), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_WorkData_Statics::NewProp_ConstraintData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_WorkData_Statics::NewProp_ConstraintData_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_WorkData_Statics::NewProp_ConstraintDataToTargets_ValueProp = { "ConstraintDataToTargets", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_WorkData_Statics::NewProp_ConstraintDataToTargets_Key_KeyProp = { "ConstraintDataToTargets_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_WorkData_Statics::NewProp_ConstraintDataToTargets_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TransformConstraint.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_WorkData_Statics::NewProp_ConstraintDataToTargets = { "ConstraintDataToTargets", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TransformConstraint_WorkData, ConstraintDataToTargets), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_WorkData_Statics::NewProp_ConstraintDataToTargets_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_WorkData_Statics::NewProp_ConstraintDataToTargets_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_WorkData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_WorkData_Statics::NewProp_ConstraintData_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_WorkData_Statics::NewProp_ConstraintData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_WorkData_Statics::NewProp_ConstraintDataToTargets_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_WorkData_Statics::NewProp_ConstraintDataToTargets_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_WorkData_Statics::NewProp_ConstraintDataToTargets,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_WorkData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"RigUnit_TransformConstraint_WorkData",
		sizeof(FRigUnit_TransformConstraint_WorkData),
		alignof(FRigUnit_TransformConstraint_WorkData),
		Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_WorkData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_WorkData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_WorkData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_WorkData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_WorkData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_WorkData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_TransformConstraint_WorkData"), sizeof(FRigUnit_TransformConstraint_WorkData), Get_Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_WorkData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_WorkData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_WorkData_Hash() { return 3267032091U; }
class UScriptStruct* FConstraintTarget::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FConstraintTarget_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConstraintTarget, Z_Construct_UPackage__Script_ControlRig(), TEXT("ConstraintTarget"), sizeof(FConstraintTarget), Get_Z_Construct_UScriptStruct_FConstraintTarget_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FConstraintTarget>()
{
	return FConstraintTarget::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConstraintTarget(FConstraintTarget::StaticStruct, TEXT("/Script/ControlRig"), TEXT("ConstraintTarget"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFConstraintTarget
{
	FScriptStruct_ControlRig_StaticRegisterNativesFConstraintTarget()
	{
		UScriptStruct::DeferCppStructOps<FConstraintTarget>(FName(TEXT("ConstraintTarget")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFConstraintTarget;
	struct Z_Construct_UScriptStruct_FConstraintTarget_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Transform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Transform;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Weight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Weight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bMaintainOffset_MetaData[];
#endif
		static void NewProp_bMaintainOffset_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bMaintainOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Filter_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Filter;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConstraintTarget_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** \n * Spec define: https://wiki.it.epicgames.net/display/TeamOnline/Transform+Constraint\n */" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TransformConstraint.h" },
		{ "ToolTip", "Spec define: https://wiki.it.epicgames.net/display/TeamOnline/Transform+Constraint" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConstraintTarget_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConstraintTarget>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConstraintTarget_Statics::NewProp_Transform_MetaData[] = {
		{ "Category", "FConstraintTarget" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TransformConstraint.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConstraintTarget_Statics::NewProp_Transform = { "Transform", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConstraintTarget, Transform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FConstraintTarget_Statics::NewProp_Transform_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConstraintTarget_Statics::NewProp_Transform_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConstraintTarget_Statics::NewProp_Weight_MetaData[] = {
		{ "Category", "FConstraintTarget" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TransformConstraint.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FConstraintTarget_Statics::NewProp_Weight = { "Weight", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConstraintTarget, Weight), METADATA_PARAMS(Z_Construct_UScriptStruct_FConstraintTarget_Statics::NewProp_Weight_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConstraintTarget_Statics::NewProp_Weight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConstraintTarget_Statics::NewProp_bMaintainOffset_MetaData[] = {
		{ "Category", "FConstraintTarget" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TransformConstraint.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FConstraintTarget_Statics::NewProp_bMaintainOffset_SetBit(void* Obj)
	{
		((FConstraintTarget*)Obj)->bMaintainOffset = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FConstraintTarget_Statics::NewProp_bMaintainOffset = { "bMaintainOffset", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FConstraintTarget), &Z_Construct_UScriptStruct_FConstraintTarget_Statics::NewProp_bMaintainOffset_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FConstraintTarget_Statics::NewProp_bMaintainOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConstraintTarget_Statics::NewProp_bMaintainOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConstraintTarget_Statics::NewProp_Filter_MetaData[] = {
		{ "Category", "FConstraintTarget" },
		{ "Constant", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TransformConstraint.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConstraintTarget_Statics::NewProp_Filter = { "Filter", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConstraintTarget, Filter), Z_Construct_UScriptStruct_FTransformFilter, METADATA_PARAMS(Z_Construct_UScriptStruct_FConstraintTarget_Statics::NewProp_Filter_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConstraintTarget_Statics::NewProp_Filter_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConstraintTarget_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConstraintTarget_Statics::NewProp_Transform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConstraintTarget_Statics::NewProp_Weight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConstraintTarget_Statics::NewProp_bMaintainOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConstraintTarget_Statics::NewProp_Filter,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConstraintTarget_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"ConstraintTarget",
		sizeof(FConstraintTarget),
		alignof(FConstraintTarget),
		Z_Construct_UScriptStruct_FConstraintTarget_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConstraintTarget_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConstraintTarget_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConstraintTarget_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConstraintTarget()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConstraintTarget_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConstraintTarget"), sizeof(FConstraintTarget), Get_Z_Construct_UScriptStruct_FConstraintTarget_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConstraintTarget_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConstraintTarget_Hash() { return 3478115532U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
