// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigUnit_Collection_generated_h
#error "RigUnit_Collection.generated.h already included, missing '#pragma once' in RigUnit_Collection.h"
#endif
#define CONTROLRIG_RigUnit_Collection_generated_h


#define FRigUnit_CollectionLoop_Execute() \
	void FRigUnit_CollectionLoop::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKeyCollection& Collection, \
		FRigElementKey& Item, \
		int32& Index, \
		int32& Count, \
		float& Ratio, \
		bool& Continue, \
		FControlRigExecuteContext& Completed, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Execution_RigUnit_Collection_h_358_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKeyCollection& Collection, \
		FRigElementKey& Item, \
		int32& Index, \
		int32& Count, \
		float& Ratio, \
		bool& Continue, \
		FControlRigExecuteContext& Completed, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FRigElementKeyCollection& Collection = *(FRigElementKeyCollection*)RigVMMemoryHandles[0].GetData(); \
		FRigElementKey& Item = *(FRigElementKey*)RigVMMemoryHandles[1].GetData(); \
		int32& Index = *(int32*)RigVMMemoryHandles[2].GetData(); \
		int32& Count = *(int32*)RigVMMemoryHandles[3].GetData(); \
		float& Ratio = *(float*)RigVMMemoryHandles[4].GetData(); \
		bool& Continue = *(bool*)RigVMMemoryHandles[5].GetData(); \
		FControlRigExecuteContext& Completed = *(FControlRigExecuteContext*)RigVMMemoryHandles[6].GetData(); \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[7].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Collection, \
			Item, \
			Index, \
			Count, \
			Ratio, \
			Continue, \
			Completed, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnit_CollectionBaseMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_CollectionLoop>();


#define FRigUnit_CollectionItemAtIndex_Execute() \
	void FRigUnit_CollectionItemAtIndex::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKeyCollection& Collection, \
		const int32 Index, \
		FRigElementKey& Item, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Execution_RigUnit_Collection_h_330_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_CollectionItemAtIndex_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKeyCollection& Collection, \
		const int32 Index, \
		FRigElementKey& Item, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FRigElementKeyCollection& Collection = *(FRigElementKeyCollection*)RigVMMemoryHandles[0].GetData(); \
		const int32 Index = *(int32*)RigVMMemoryHandles[1].GetData(); \
		FRigElementKey& Item = *(FRigElementKey*)RigVMMemoryHandles[2].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Collection, \
			Index, \
			Item, \
			Context \
		); \
	} \
	typedef FRigUnit_CollectionBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_CollectionItemAtIndex>();


#define FRigUnit_CollectionCount_Execute() \
	void FRigUnit_CollectionCount::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKeyCollection& Collection, \
		int32& Count, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Execution_RigUnit_Collection_h_306_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_CollectionCount_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKeyCollection& Collection, \
		int32& Count, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FRigElementKeyCollection& Collection = *(FRigElementKeyCollection*)RigVMMemoryHandles[0].GetData(); \
		int32& Count = *(int32*)RigVMMemoryHandles[1].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Collection, \
			Count, \
			Context \
		); \
	} \
	typedef FRigUnit_CollectionBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_CollectionCount>();


#define FRigUnit_CollectionReverse_Execute() \
	void FRigUnit_CollectionReverse::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKeyCollection& Collection, \
		FRigElementKeyCollection& Reversed, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Execution_RigUnit_Collection_h_284_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_CollectionReverse_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKeyCollection& Collection, \
		FRigElementKeyCollection& Reversed, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FRigElementKeyCollection& Collection = *(FRigElementKeyCollection*)RigVMMemoryHandles[0].GetData(); \
		FRigElementKeyCollection& Reversed = *(FRigElementKeyCollection*)RigVMMemoryHandles[1].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Collection, \
			Reversed, \
			Context \
		); \
	} \
	typedef FRigUnit_CollectionBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_CollectionReverse>();


#define FRigUnit_CollectionDifference_Execute() \
	void FRigUnit_CollectionDifference::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKeyCollection& A, \
		const FRigElementKeyCollection& B, \
		FRigElementKeyCollection& Collection, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Execution_RigUnit_Collection_h_259_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_CollectionDifference_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKeyCollection& A, \
		const FRigElementKeyCollection& B, \
		FRigElementKeyCollection& Collection, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FRigElementKeyCollection& A = *(FRigElementKeyCollection*)RigVMMemoryHandles[0].GetData(); \
		const FRigElementKeyCollection& B = *(FRigElementKeyCollection*)RigVMMemoryHandles[1].GetData(); \
		FRigElementKeyCollection& Collection = *(FRigElementKeyCollection*)RigVMMemoryHandles[2].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			A, \
			B, \
			Collection, \
			Context \
		); \
	} \
	typedef FRigUnit_CollectionBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_CollectionDifference>();


#define FRigUnit_CollectionIntersection_Execute() \
	void FRigUnit_CollectionIntersection::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKeyCollection& A, \
		const FRigElementKeyCollection& B, \
		FRigElementKeyCollection& Collection, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Execution_RigUnit_Collection_h_233_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_CollectionIntersection_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKeyCollection& A, \
		const FRigElementKeyCollection& B, \
		FRigElementKeyCollection& Collection, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FRigElementKeyCollection& A = *(FRigElementKeyCollection*)RigVMMemoryHandles[0].GetData(); \
		const FRigElementKeyCollection& B = *(FRigElementKeyCollection*)RigVMMemoryHandles[1].GetData(); \
		FRigElementKeyCollection& Collection = *(FRigElementKeyCollection*)RigVMMemoryHandles[2].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			A, \
			B, \
			Collection, \
			Context \
		); \
	} \
	typedef FRigUnit_CollectionBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_CollectionIntersection>();


#define FRigUnit_CollectionUnion_Execute() \
	void FRigUnit_CollectionUnion::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKeyCollection& A, \
		const FRigElementKeyCollection& B, \
		FRigElementKeyCollection& Collection, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Execution_RigUnit_Collection_h_207_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_CollectionUnion_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKeyCollection& A, \
		const FRigElementKeyCollection& B, \
		FRigElementKeyCollection& Collection, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FRigElementKeyCollection& A = *(FRigElementKeyCollection*)RigVMMemoryHandles[0].GetData(); \
		const FRigElementKeyCollection& B = *(FRigElementKeyCollection*)RigVMMemoryHandles[1].GetData(); \
		FRigElementKeyCollection& Collection = *(FRigElementKeyCollection*)RigVMMemoryHandles[2].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			A, \
			B, \
			Collection, \
			Context \
		); \
	} \
	typedef FRigUnit_CollectionBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_CollectionUnion>();


#define FRigUnit_CollectionItems_Execute() \
	void FRigUnit_CollectionItems::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FRigElementKey>& Items, \
		FRigElementKeyCollection& Collection, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Execution_RigUnit_Collection_h_183_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_CollectionItems_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FRigElementKey>& Items, \
		FRigElementKeyCollection& Collection, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		FRigVMFixedArray<FRigElementKey> Items((FRigElementKey*)RigVMMemoryHandles[0].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[1].GetData())); \
		FRigElementKeyCollection& Collection = *(FRigElementKeyCollection*)RigVMMemoryHandles[2].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Items, \
			Collection, \
			Context \
		); \
	} \
	typedef FRigUnit_CollectionBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_CollectionItems>();


#define FRigUnit_CollectionReplaceItems_Execute() \
	void FRigUnit_CollectionReplaceItems::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKeyCollection& Items, \
		const FName& Old, \
		const FName& New, \
		const bool RemoveInvalidItems, \
		FRigElementKeyCollection& Collection, \
		FRigElementKeyCollection& CachedCollection, \
		int32& CachedHierarchyHash, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Execution_RigUnit_Collection_h_143_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKeyCollection& Items, \
		const FName& Old, \
		const FName& New, \
		const bool RemoveInvalidItems, \
		FRigElementKeyCollection& Collection, \
		FRigElementKeyCollection& CachedCollection, \
		int32& CachedHierarchyHash, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FRigElementKeyCollection& Items = *(FRigElementKeyCollection*)RigVMMemoryHandles[0].GetData(); \
		const FName& Old = *(FName*)RigVMMemoryHandles[1].GetData(); \
		const FName& New = *(FName*)RigVMMemoryHandles[2].GetData(); \
		const bool RemoveInvalidItems = *(bool*)RigVMMemoryHandles[3].GetData(); \
		FRigElementKeyCollection& Collection = *(FRigElementKeyCollection*)RigVMMemoryHandles[4].GetData(); \
		FRigVMDynamicArray<FRigElementKeyCollection> CachedCollection_5_Array(*((FRigVMByteArray*)RigVMMemoryHandles[5].GetData(0, false))); \
		CachedCollection_5_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FRigElementKeyCollection& CachedCollection = CachedCollection_5_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<int32> CachedHierarchyHash_6_Array(*((FRigVMByteArray*)RigVMMemoryHandles[6].GetData(0, false))); \
		CachedHierarchyHash_6_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		int32& CachedHierarchyHash = CachedHierarchyHash_6_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Items, \
			Old, \
			New, \
			RemoveInvalidItems, \
			Collection, \
			CachedCollection, \
			CachedHierarchyHash, \
			Context \
		); \
	} \
	typedef FRigUnit_CollectionBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_CollectionReplaceItems>();


#define FRigUnit_CollectionChildren_Execute() \
	void FRigUnit_CollectionChildren::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKey& Parent, \
		const bool bIncludeParent, \
		const bool bRecursive, \
		const ERigElementType TypeToSearch, \
		FRigElementKeyCollection& Collection, \
		FRigElementKeyCollection& CachedCollection, \
		int32& CachedHierarchyHash, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Execution_RigUnit_Collection_h_101_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKey& Parent, \
		const bool bIncludeParent, \
		const bool bRecursive, \
		const ERigElementType TypeToSearch, \
		FRigElementKeyCollection& Collection, \
		FRigElementKeyCollection& CachedCollection, \
		int32& CachedHierarchyHash, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FRigElementKey& Parent = *(FRigElementKey*)RigVMMemoryHandles[0].GetData(); \
		const bool bIncludeParent = *(bool*)RigVMMemoryHandles[1].GetData(); \
		const bool bRecursive = *(bool*)RigVMMemoryHandles[2].GetData(); \
		ERigElementType TypeToSearch = (ERigElementType)*(uint8*)RigVMMemoryHandles[3].GetData(); \
		FRigElementKeyCollection& Collection = *(FRigElementKeyCollection*)RigVMMemoryHandles[4].GetData(); \
		FRigVMDynamicArray<FRigElementKeyCollection> CachedCollection_5_Array(*((FRigVMByteArray*)RigVMMemoryHandles[5].GetData(0, false))); \
		CachedCollection_5_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FRigElementKeyCollection& CachedCollection = CachedCollection_5_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<int32> CachedHierarchyHash_6_Array(*((FRigVMByteArray*)RigVMMemoryHandles[6].GetData(0, false))); \
		CachedHierarchyHash_6_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		int32& CachedHierarchyHash = CachedHierarchyHash_6_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Parent, \
			bIncludeParent, \
			bRecursive, \
			TypeToSearch, \
			Collection, \
			CachedCollection, \
			CachedHierarchyHash, \
			Context \
		); \
	} \
	typedef FRigUnit_CollectionBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_CollectionChildren>();


#define FRigUnit_CollectionNameSearch_Execute() \
	void FRigUnit_CollectionNameSearch::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& PartialName, \
		const ERigElementType TypeToSearch, \
		FRigElementKeyCollection& Collection, \
		FRigElementKeyCollection& CachedCollection, \
		int32& CachedHierarchyHash, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Execution_RigUnit_Collection_h_66_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& PartialName, \
		const ERigElementType TypeToSearch, \
		FRigElementKeyCollection& Collection, \
		FRigElementKeyCollection& CachedCollection, \
		int32& CachedHierarchyHash, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FName& PartialName = *(FName*)RigVMMemoryHandles[0].GetData(); \
		ERigElementType TypeToSearch = (ERigElementType)*(uint8*)RigVMMemoryHandles[1].GetData(); \
		FRigElementKeyCollection& Collection = *(FRigElementKeyCollection*)RigVMMemoryHandles[2].GetData(); \
		FRigVMDynamicArray<FRigElementKeyCollection> CachedCollection_3_Array(*((FRigVMByteArray*)RigVMMemoryHandles[3].GetData(0, false))); \
		CachedCollection_3_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FRigElementKeyCollection& CachedCollection = CachedCollection_3_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<int32> CachedHierarchyHash_4_Array(*((FRigVMByteArray*)RigVMMemoryHandles[4].GetData(0, false))); \
		CachedHierarchyHash_4_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		int32& CachedHierarchyHash = CachedHierarchyHash_4_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			PartialName, \
			TypeToSearch, \
			Collection, \
			CachedCollection, \
			CachedHierarchyHash, \
			Context \
		); \
	} \
	typedef FRigUnit_CollectionBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_CollectionNameSearch>();


#define FRigUnit_CollectionChain_Execute() \
	void FRigUnit_CollectionChain::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKey& FirstItem, \
		const FRigElementKey& LastItem, \
		const bool Reverse, \
		FRigElementKeyCollection& Collection, \
		FRigElementKeyCollection& CachedCollection, \
		int32& CachedHierarchyHash, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Execution_RigUnit_Collection_h_28_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKey& FirstItem, \
		const FRigElementKey& LastItem, \
		const bool Reverse, \
		FRigElementKeyCollection& Collection, \
		FRigElementKeyCollection& CachedCollection, \
		int32& CachedHierarchyHash, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FRigElementKey& FirstItem = *(FRigElementKey*)RigVMMemoryHandles[0].GetData(); \
		const FRigElementKey& LastItem = *(FRigElementKey*)RigVMMemoryHandles[1].GetData(); \
		const bool Reverse = *(bool*)RigVMMemoryHandles[2].GetData(); \
		FRigElementKeyCollection& Collection = *(FRigElementKeyCollection*)RigVMMemoryHandles[3].GetData(); \
		FRigVMDynamicArray<FRigElementKeyCollection> CachedCollection_4_Array(*((FRigVMByteArray*)RigVMMemoryHandles[4].GetData(0, false))); \
		CachedCollection_4_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FRigElementKeyCollection& CachedCollection = CachedCollection_4_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<int32> CachedHierarchyHash_5_Array(*((FRigVMByteArray*)RigVMMemoryHandles[5].GetData(0, false))); \
		CachedHierarchyHash_5_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		int32& CachedHierarchyHash = CachedHierarchyHash_5_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			FirstItem, \
			LastItem, \
			Reverse, \
			Collection, \
			CachedCollection, \
			CachedHierarchyHash, \
			Context \
		); \
	} \
	typedef FRigUnit_CollectionBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_CollectionChain>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Execution_RigUnit_Collection_h_18_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_CollectionBaseMutable_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	typedef FRigUnitMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_CollectionBaseMutable>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Execution_RigUnit_Collection_h_12_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_CollectionBase_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	typedef FRigUnit Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_CollectionBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Execution_RigUnit_Collection_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
