// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRigEditor/Private/BakeToControlRigSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBakeToControlRigSettings() {}
// Cross Module References
	CONTROLRIGEDITOR_API UClass* Z_Construct_UClass_UBakeToControlRigSettings_NoRegister();
	CONTROLRIGEDITOR_API UClass* Z_Construct_UClass_UBakeToControlRigSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_ControlRigEditor();
// End Cross Module References
	void UBakeToControlRigSettings::StaticRegisterNativesUBakeToControlRigSettings()
	{
	}
	UClass* Z_Construct_UClass_UBakeToControlRigSettings_NoRegister()
	{
		return UBakeToControlRigSettings::StaticClass();
	}
	struct Z_Construct_UClass_UBakeToControlRigSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bReduceKeys_MetaData[];
#endif
		static void NewProp_bReduceKeys_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bReduceKeys;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Tolerance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Tolerance;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBakeToControlRigSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRigEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakeToControlRigSettings_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "BakeToControlRigSettings.h" },
		{ "ModuleRelativePath", "Private/BakeToControlRigSettings.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakeToControlRigSettings_Statics::NewProp_bReduceKeys_MetaData[] = {
		{ "Category", "Reduce Keys" },
		{ "Comment", "/** Reduce Keys */" },
		{ "ModuleRelativePath", "Private/BakeToControlRigSettings.h" },
		{ "ToolTip", "Reduce Keys" },
	};
#endif
	void Z_Construct_UClass_UBakeToControlRigSettings_Statics::NewProp_bReduceKeys_SetBit(void* Obj)
	{
		((UBakeToControlRigSettings*)Obj)->bReduceKeys = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UBakeToControlRigSettings_Statics::NewProp_bReduceKeys = { "bReduceKeys", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UBakeToControlRigSettings), &Z_Construct_UClass_UBakeToControlRigSettings_Statics::NewProp_bReduceKeys_SetBit, METADATA_PARAMS(Z_Construct_UClass_UBakeToControlRigSettings_Statics::NewProp_bReduceKeys_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakeToControlRigSettings_Statics::NewProp_bReduceKeys_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakeToControlRigSettings_Statics::NewProp_Tolerance_MetaData[] = {
		{ "Category", "Reduce Keys" },
		{ "Comment", "/** Reduce Keys Tolerance*/" },
		{ "ModuleRelativePath", "Private/BakeToControlRigSettings.h" },
		{ "ToolTip", "Reduce Keys Tolerance" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UBakeToControlRigSettings_Statics::NewProp_Tolerance = { "Tolerance", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBakeToControlRigSettings, Tolerance), METADATA_PARAMS(Z_Construct_UClass_UBakeToControlRigSettings_Statics::NewProp_Tolerance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBakeToControlRigSettings_Statics::NewProp_Tolerance_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UBakeToControlRigSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakeToControlRigSettings_Statics::NewProp_bReduceKeys,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBakeToControlRigSettings_Statics::NewProp_Tolerance,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBakeToControlRigSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBakeToControlRigSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBakeToControlRigSettings_Statics::ClassParams = {
		&UBakeToControlRigSettings::StaticClass,
		"EditorSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UBakeToControlRigSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UBakeToControlRigSettings_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UBakeToControlRigSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBakeToControlRigSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBakeToControlRigSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBakeToControlRigSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBakeToControlRigSettings, 1960716252);
	template<> CONTROLRIGEDITOR_API UClass* StaticClass<UBakeToControlRigSettings>()
	{
		return UBakeToControlRigSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBakeToControlRigSettings(Z_Construct_UClass_UBakeToControlRigSettings, &UBakeToControlRigSettings::StaticClass, TEXT("/Script/ControlRigEditor"), TEXT("UBakeToControlRigSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBakeToControlRigSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
