// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Math/RigUnit_MathInt.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_MathInt() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntLessEqual();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntBase();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntGreaterEqual();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntLess();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntGreater();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntNotEquals();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntEquals();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntClamp();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntSign();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntUnaryOp();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntToFloat();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntAbs();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntNegate();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntPow();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntBinaryOp();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntMax();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntMin();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntMod();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntDiv();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntMul();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntSub();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntAdd();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathBase();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_MathIntLessEqual>() == std::is_polymorphic<FRigUnit_MathIntBase>(), "USTRUCT FRigUnit_MathIntLessEqual cannot be polymorphic unless super FRigUnit_MathIntBase is polymorphic");

class UScriptStruct* FRigUnit_MathIntLessEqual::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntLessEqual_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathIntLessEqual, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathIntLessEqual"), sizeof(FRigUnit_MathIntLessEqual), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntLessEqual_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathIntLessEqual::Execute"), &FRigUnit_MathIntLessEqual::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathIntLessEqual>()
{
	return FRigUnit_MathIntLessEqual::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathIntLessEqual(FRigUnit_MathIntLessEqual::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathIntLessEqual"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntLessEqual
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntLessEqual()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathIntLessEqual>(FName(TEXT("RigUnit_MathIntLessEqual")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntLessEqual;
	struct Z_Construct_UScriptStruct_FRigUnit_MathIntLessEqual_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_A_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_A;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_B_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_B;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static void NewProp_Result_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntLessEqual_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns true if the value A is less than or equal to B\n */" },
		{ "DisplayName", "Less Equal" },
		{ "Keywords", "Smaller,<=" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
		{ "PrototypeName", "LessEqual" },
		{ "ToolTip", "Returns true if the value A is less than or equal to B" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathIntLessEqual_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathIntLessEqual>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntLessEqual_Statics::NewProp_A_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathIntLessEqual_Statics::NewProp_A = { "A", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathIntLessEqual, A), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntLessEqual_Statics::NewProp_A_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntLessEqual_Statics::NewProp_A_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntLessEqual_Statics::NewProp_B_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathIntLessEqual_Statics::NewProp_B = { "B", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathIntLessEqual, B), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntLessEqual_Statics::NewProp_B_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntLessEqual_Statics::NewProp_B_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntLessEqual_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
		{ "Output", "" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_MathIntLessEqual_Statics::NewProp_Result_SetBit(void* Obj)
	{
		((FRigUnit_MathIntLessEqual*)Obj)->Result = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathIntLessEqual_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_MathIntLessEqual), &Z_Construct_UScriptStruct_FRigUnit_MathIntLessEqual_Statics::NewProp_Result_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntLessEqual_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntLessEqual_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathIntLessEqual_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathIntLessEqual_Statics::NewProp_A,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathIntLessEqual_Statics::NewProp_B,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathIntLessEqual_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathIntLessEqual_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathIntBase,
		&NewStructOps,
		"RigUnit_MathIntLessEqual",
		sizeof(FRigUnit_MathIntLessEqual),
		alignof(FRigUnit_MathIntLessEqual),
		Z_Construct_UScriptStruct_FRigUnit_MathIntLessEqual_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntLessEqual_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntLessEqual_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntLessEqual_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntLessEqual()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntLessEqual_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathIntLessEqual"), sizeof(FRigUnit_MathIntLessEqual), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntLessEqual_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathIntLessEqual_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntLessEqual_Hash() { return 1141861018U; }

void FRigUnit_MathIntLessEqual::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathIntGreaterEqual>() == std::is_polymorphic<FRigUnit_MathIntBase>(), "USTRUCT FRigUnit_MathIntGreaterEqual cannot be polymorphic unless super FRigUnit_MathIntBase is polymorphic");

class UScriptStruct* FRigUnit_MathIntGreaterEqual::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntGreaterEqual_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathIntGreaterEqual, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathIntGreaterEqual"), sizeof(FRigUnit_MathIntGreaterEqual), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntGreaterEqual_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathIntGreaterEqual::Execute"), &FRigUnit_MathIntGreaterEqual::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathIntGreaterEqual>()
{
	return FRigUnit_MathIntGreaterEqual::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathIntGreaterEqual(FRigUnit_MathIntGreaterEqual::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathIntGreaterEqual"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntGreaterEqual
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntGreaterEqual()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathIntGreaterEqual>(FName(TEXT("RigUnit_MathIntGreaterEqual")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntGreaterEqual;
	struct Z_Construct_UScriptStruct_FRigUnit_MathIntGreaterEqual_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_A_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_A;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_B_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_B;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static void NewProp_Result_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntGreaterEqual_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns true if the value A is greater than or equal to B\n */" },
		{ "DisplayName", "Greater Equal" },
		{ "Keywords", "Larger,Bigger,>=" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
		{ "PrototypeName", "GreaterEqual" },
		{ "ToolTip", "Returns true if the value A is greater than or equal to B" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathIntGreaterEqual_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathIntGreaterEqual>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntGreaterEqual_Statics::NewProp_A_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathIntGreaterEqual_Statics::NewProp_A = { "A", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathIntGreaterEqual, A), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntGreaterEqual_Statics::NewProp_A_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntGreaterEqual_Statics::NewProp_A_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntGreaterEqual_Statics::NewProp_B_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathIntGreaterEqual_Statics::NewProp_B = { "B", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathIntGreaterEqual, B), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntGreaterEqual_Statics::NewProp_B_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntGreaterEqual_Statics::NewProp_B_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntGreaterEqual_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
		{ "Output", "" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_MathIntGreaterEqual_Statics::NewProp_Result_SetBit(void* Obj)
	{
		((FRigUnit_MathIntGreaterEqual*)Obj)->Result = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathIntGreaterEqual_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_MathIntGreaterEqual), &Z_Construct_UScriptStruct_FRigUnit_MathIntGreaterEqual_Statics::NewProp_Result_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntGreaterEqual_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntGreaterEqual_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathIntGreaterEqual_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathIntGreaterEqual_Statics::NewProp_A,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathIntGreaterEqual_Statics::NewProp_B,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathIntGreaterEqual_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathIntGreaterEqual_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathIntBase,
		&NewStructOps,
		"RigUnit_MathIntGreaterEqual",
		sizeof(FRigUnit_MathIntGreaterEqual),
		alignof(FRigUnit_MathIntGreaterEqual),
		Z_Construct_UScriptStruct_FRigUnit_MathIntGreaterEqual_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntGreaterEqual_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntGreaterEqual_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntGreaterEqual_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntGreaterEqual()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntGreaterEqual_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathIntGreaterEqual"), sizeof(FRigUnit_MathIntGreaterEqual), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntGreaterEqual_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathIntGreaterEqual_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntGreaterEqual_Hash() { return 3755581569U; }

void FRigUnit_MathIntGreaterEqual::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathIntLess>() == std::is_polymorphic<FRigUnit_MathIntBase>(), "USTRUCT FRigUnit_MathIntLess cannot be polymorphic unless super FRigUnit_MathIntBase is polymorphic");

class UScriptStruct* FRigUnit_MathIntLess::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntLess_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathIntLess, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathIntLess"), sizeof(FRigUnit_MathIntLess), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntLess_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathIntLess::Execute"), &FRigUnit_MathIntLess::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathIntLess>()
{
	return FRigUnit_MathIntLess::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathIntLess(FRigUnit_MathIntLess::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathIntLess"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntLess
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntLess()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathIntLess>(FName(TEXT("RigUnit_MathIntLess")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntLess;
	struct Z_Construct_UScriptStruct_FRigUnit_MathIntLess_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_A_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_A;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_B_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_B;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static void NewProp_Result_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntLess_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns true if the value A is less than B\n */" },
		{ "DisplayName", "Less" },
		{ "Keywords", "Smaller,<" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
		{ "PrototypeName", "Less" },
		{ "ToolTip", "Returns true if the value A is less than B" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathIntLess_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathIntLess>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntLess_Statics::NewProp_A_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathIntLess_Statics::NewProp_A = { "A", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathIntLess, A), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntLess_Statics::NewProp_A_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntLess_Statics::NewProp_A_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntLess_Statics::NewProp_B_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathIntLess_Statics::NewProp_B = { "B", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathIntLess, B), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntLess_Statics::NewProp_B_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntLess_Statics::NewProp_B_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntLess_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
		{ "Output", "" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_MathIntLess_Statics::NewProp_Result_SetBit(void* Obj)
	{
		((FRigUnit_MathIntLess*)Obj)->Result = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathIntLess_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_MathIntLess), &Z_Construct_UScriptStruct_FRigUnit_MathIntLess_Statics::NewProp_Result_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntLess_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntLess_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathIntLess_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathIntLess_Statics::NewProp_A,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathIntLess_Statics::NewProp_B,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathIntLess_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathIntLess_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathIntBase,
		&NewStructOps,
		"RigUnit_MathIntLess",
		sizeof(FRigUnit_MathIntLess),
		alignof(FRigUnit_MathIntLess),
		Z_Construct_UScriptStruct_FRigUnit_MathIntLess_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntLess_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntLess_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntLess_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntLess()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntLess_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathIntLess"), sizeof(FRigUnit_MathIntLess), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntLess_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathIntLess_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntLess_Hash() { return 2084036789U; }

void FRigUnit_MathIntLess::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathIntGreater>() == std::is_polymorphic<FRigUnit_MathIntBase>(), "USTRUCT FRigUnit_MathIntGreater cannot be polymorphic unless super FRigUnit_MathIntBase is polymorphic");

class UScriptStruct* FRigUnit_MathIntGreater::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntGreater_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathIntGreater, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathIntGreater"), sizeof(FRigUnit_MathIntGreater), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntGreater_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathIntGreater::Execute"), &FRigUnit_MathIntGreater::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathIntGreater>()
{
	return FRigUnit_MathIntGreater::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathIntGreater(FRigUnit_MathIntGreater::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathIntGreater"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntGreater
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntGreater()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathIntGreater>(FName(TEXT("RigUnit_MathIntGreater")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntGreater;
	struct Z_Construct_UScriptStruct_FRigUnit_MathIntGreater_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_A_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_A;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_B_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_B;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static void NewProp_Result_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntGreater_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns true if the value A is greater than B\n */" },
		{ "DisplayName", "Greater" },
		{ "Keywords", "Larger,Bigger,>" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
		{ "PrototypeName", "Greater" },
		{ "ToolTip", "Returns true if the value A is greater than B" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathIntGreater_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathIntGreater>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntGreater_Statics::NewProp_A_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathIntGreater_Statics::NewProp_A = { "A", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathIntGreater, A), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntGreater_Statics::NewProp_A_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntGreater_Statics::NewProp_A_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntGreater_Statics::NewProp_B_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathIntGreater_Statics::NewProp_B = { "B", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathIntGreater, B), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntGreater_Statics::NewProp_B_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntGreater_Statics::NewProp_B_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntGreater_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
		{ "Output", "" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_MathIntGreater_Statics::NewProp_Result_SetBit(void* Obj)
	{
		((FRigUnit_MathIntGreater*)Obj)->Result = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathIntGreater_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_MathIntGreater), &Z_Construct_UScriptStruct_FRigUnit_MathIntGreater_Statics::NewProp_Result_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntGreater_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntGreater_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathIntGreater_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathIntGreater_Statics::NewProp_A,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathIntGreater_Statics::NewProp_B,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathIntGreater_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathIntGreater_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathIntBase,
		&NewStructOps,
		"RigUnit_MathIntGreater",
		sizeof(FRigUnit_MathIntGreater),
		alignof(FRigUnit_MathIntGreater),
		Z_Construct_UScriptStruct_FRigUnit_MathIntGreater_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntGreater_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntGreater_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntGreater_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntGreater()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntGreater_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathIntGreater"), sizeof(FRigUnit_MathIntGreater), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntGreater_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathIntGreater_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntGreater_Hash() { return 3836221125U; }

void FRigUnit_MathIntGreater::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathIntNotEquals>() == std::is_polymorphic<FRigUnit_MathIntBase>(), "USTRUCT FRigUnit_MathIntNotEquals cannot be polymorphic unless super FRigUnit_MathIntBase is polymorphic");

class UScriptStruct* FRigUnit_MathIntNotEquals::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntNotEquals_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathIntNotEquals, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathIntNotEquals"), sizeof(FRigUnit_MathIntNotEquals), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntNotEquals_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathIntNotEquals::Execute"), &FRigUnit_MathIntNotEquals::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathIntNotEquals>()
{
	return FRigUnit_MathIntNotEquals::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathIntNotEquals(FRigUnit_MathIntNotEquals::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathIntNotEquals"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntNotEquals
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntNotEquals()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathIntNotEquals>(FName(TEXT("RigUnit_MathIntNotEquals")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntNotEquals;
	struct Z_Construct_UScriptStruct_FRigUnit_MathIntNotEquals_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_A_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_A;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_B_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_B;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static void NewProp_Result_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntNotEquals_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns true if the value A does not equal B\n */" },
		{ "DisplayName", "Not Equals" },
		{ "Keywords", "Different,!=" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
		{ "PrototypeName", "NotEquals" },
		{ "ToolTip", "Returns true if the value A does not equal B" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathIntNotEquals_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathIntNotEquals>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntNotEquals_Statics::NewProp_A_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathIntNotEquals_Statics::NewProp_A = { "A", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathIntNotEquals, A), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntNotEquals_Statics::NewProp_A_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntNotEquals_Statics::NewProp_A_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntNotEquals_Statics::NewProp_B_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathIntNotEquals_Statics::NewProp_B = { "B", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathIntNotEquals, B), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntNotEquals_Statics::NewProp_B_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntNotEquals_Statics::NewProp_B_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntNotEquals_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
		{ "Output", "" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_MathIntNotEquals_Statics::NewProp_Result_SetBit(void* Obj)
	{
		((FRigUnit_MathIntNotEquals*)Obj)->Result = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathIntNotEquals_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_MathIntNotEquals), &Z_Construct_UScriptStruct_FRigUnit_MathIntNotEquals_Statics::NewProp_Result_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntNotEquals_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntNotEquals_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathIntNotEquals_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathIntNotEquals_Statics::NewProp_A,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathIntNotEquals_Statics::NewProp_B,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathIntNotEquals_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathIntNotEquals_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathIntBase,
		&NewStructOps,
		"RigUnit_MathIntNotEquals",
		sizeof(FRigUnit_MathIntNotEquals),
		alignof(FRigUnit_MathIntNotEquals),
		Z_Construct_UScriptStruct_FRigUnit_MathIntNotEquals_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntNotEquals_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntNotEquals_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntNotEquals_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntNotEquals()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntNotEquals_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathIntNotEquals"), sizeof(FRigUnit_MathIntNotEquals), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntNotEquals_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathIntNotEquals_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntNotEquals_Hash() { return 656093231U; }

void FRigUnit_MathIntNotEquals::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathIntEquals>() == std::is_polymorphic<FRigUnit_MathIntBase>(), "USTRUCT FRigUnit_MathIntEquals cannot be polymorphic unless super FRigUnit_MathIntBase is polymorphic");

class UScriptStruct* FRigUnit_MathIntEquals::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntEquals_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathIntEquals, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathIntEquals"), sizeof(FRigUnit_MathIntEquals), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntEquals_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathIntEquals::Execute"), &FRigUnit_MathIntEquals::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathIntEquals>()
{
	return FRigUnit_MathIntEquals::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathIntEquals(FRigUnit_MathIntEquals::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathIntEquals"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntEquals
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntEquals()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathIntEquals>(FName(TEXT("RigUnit_MathIntEquals")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntEquals;
	struct Z_Construct_UScriptStruct_FRigUnit_MathIntEquals_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_A_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_A;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_B_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_B;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static void NewProp_Result_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntEquals_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns true if the value A equals B\n */" },
		{ "DisplayName", "Equals" },
		{ "Keywords", "Same,==" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
		{ "PrototypeName", "Equals" },
		{ "ToolTip", "Returns true if the value A equals B" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathIntEquals_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathIntEquals>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntEquals_Statics::NewProp_A_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathIntEquals_Statics::NewProp_A = { "A", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathIntEquals, A), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntEquals_Statics::NewProp_A_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntEquals_Statics::NewProp_A_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntEquals_Statics::NewProp_B_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathIntEquals_Statics::NewProp_B = { "B", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathIntEquals, B), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntEquals_Statics::NewProp_B_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntEquals_Statics::NewProp_B_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntEquals_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
		{ "Output", "" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_MathIntEquals_Statics::NewProp_Result_SetBit(void* Obj)
	{
		((FRigUnit_MathIntEquals*)Obj)->Result = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathIntEquals_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_MathIntEquals), &Z_Construct_UScriptStruct_FRigUnit_MathIntEquals_Statics::NewProp_Result_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntEquals_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntEquals_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathIntEquals_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathIntEquals_Statics::NewProp_A,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathIntEquals_Statics::NewProp_B,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathIntEquals_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathIntEquals_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathIntBase,
		&NewStructOps,
		"RigUnit_MathIntEquals",
		sizeof(FRigUnit_MathIntEquals),
		alignof(FRigUnit_MathIntEquals),
		Z_Construct_UScriptStruct_FRigUnit_MathIntEquals_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntEquals_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntEquals_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntEquals_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntEquals()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntEquals_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathIntEquals"), sizeof(FRigUnit_MathIntEquals), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntEquals_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathIntEquals_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntEquals_Hash() { return 1589730978U; }

void FRigUnit_MathIntEquals::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathIntClamp>() == std::is_polymorphic<FRigUnit_MathIntBase>(), "USTRUCT FRigUnit_MathIntClamp cannot be polymorphic unless super FRigUnit_MathIntBase is polymorphic");

class UScriptStruct* FRigUnit_MathIntClamp::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntClamp_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathIntClamp, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathIntClamp"), sizeof(FRigUnit_MathIntClamp), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntClamp_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathIntClamp::Execute"), &FRigUnit_MathIntClamp::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathIntClamp>()
{
	return FRigUnit_MathIntClamp::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathIntClamp(FRigUnit_MathIntClamp::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathIntClamp"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntClamp
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntClamp()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathIntClamp>(FName(TEXT("RigUnit_MathIntClamp")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntClamp;
	struct Z_Construct_UScriptStruct_FRigUnit_MathIntClamp_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Minimum_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Minimum;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Maximum_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Maximum;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntClamp_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Clamps the given value within the range provided by minimum and maximum\n */" },
		{ "DisplayName", "Clamp" },
		{ "Keywords", "Range,Remap" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
		{ "PrototypeName", "Clamp" },
		{ "ToolTip", "Clamps the given value within the range provided by minimum and maximum" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathIntClamp_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathIntClamp>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntClamp_Statics::NewProp_Value_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathIntClamp_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathIntClamp, Value), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntClamp_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntClamp_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntClamp_Statics::NewProp_Minimum_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathIntClamp_Statics::NewProp_Minimum = { "Minimum", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathIntClamp, Minimum), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntClamp_Statics::NewProp_Minimum_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntClamp_Statics::NewProp_Minimum_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntClamp_Statics::NewProp_Maximum_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathIntClamp_Statics::NewProp_Maximum = { "Maximum", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathIntClamp, Maximum), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntClamp_Statics::NewProp_Maximum_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntClamp_Statics::NewProp_Maximum_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntClamp_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathIntClamp_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathIntClamp, Result), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntClamp_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntClamp_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathIntClamp_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathIntClamp_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathIntClamp_Statics::NewProp_Minimum,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathIntClamp_Statics::NewProp_Maximum,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathIntClamp_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathIntClamp_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathIntBase,
		&NewStructOps,
		"RigUnit_MathIntClamp",
		sizeof(FRigUnit_MathIntClamp),
		alignof(FRigUnit_MathIntClamp),
		Z_Construct_UScriptStruct_FRigUnit_MathIntClamp_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntClamp_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntClamp_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntClamp_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntClamp()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntClamp_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathIntClamp"), sizeof(FRigUnit_MathIntClamp), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntClamp_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathIntClamp_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntClamp_Hash() { return 3748312110U; }

void FRigUnit_MathIntClamp::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Minimum,
		Maximum,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathIntSign>() == std::is_polymorphic<FRigUnit_MathIntUnaryOp>(), "USTRUCT FRigUnit_MathIntSign cannot be polymorphic unless super FRigUnit_MathIntUnaryOp is polymorphic");

class UScriptStruct* FRigUnit_MathIntSign::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntSign_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathIntSign, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathIntSign"), sizeof(FRigUnit_MathIntSign), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntSign_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathIntSign::Execute"), &FRigUnit_MathIntSign::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathIntSign>()
{
	return FRigUnit_MathIntSign::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathIntSign(FRigUnit_MathIntSign::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathIntSign"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntSign
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntSign()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathIntSign>(FName(TEXT("RigUnit_MathIntSign")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntSign;
	struct Z_Construct_UScriptStruct_FRigUnit_MathIntSign_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntSign_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the sign of the value (+1 for >= 0, -1 for < 0)\n */" },
		{ "DisplayName", "Sign" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
		{ "PrototypeName", "Sign" },
		{ "ToolTip", "Returns the sign of the value (+1 for >= 0, -1 for < 0)" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathIntSign_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathIntSign>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathIntSign_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathIntUnaryOp,
		&NewStructOps,
		"RigUnit_MathIntSign",
		sizeof(FRigUnit_MathIntSign),
		alignof(FRigUnit_MathIntSign),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntSign_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntSign_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntSign()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntSign_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathIntSign"), sizeof(FRigUnit_MathIntSign), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntSign_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathIntSign_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntSign_Hash() { return 1515171467U; }

void FRigUnit_MathIntSign::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathIntToFloat>() == std::is_polymorphic<FRigUnit_MathIntBase>(), "USTRUCT FRigUnit_MathIntToFloat cannot be polymorphic unless super FRigUnit_MathIntBase is polymorphic");

class UScriptStruct* FRigUnit_MathIntToFloat::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntToFloat_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathIntToFloat, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathIntToFloat"), sizeof(FRigUnit_MathIntToFloat), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntToFloat_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathIntToFloat::Execute"), &FRigUnit_MathIntToFloat::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathIntToFloat>()
{
	return FRigUnit_MathIntToFloat::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathIntToFloat(FRigUnit_MathIntToFloat::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathIntToFloat"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntToFloat
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntToFloat()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathIntToFloat>(FName(TEXT("RigUnit_MathIntToFloat")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntToFloat;
	struct Z_Construct_UScriptStruct_FRigUnit_MathIntToFloat_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntToFloat_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the int cast to a float\n */" },
		{ "DisplayName", "To Float" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
		{ "PrototypeName", "Convert" },
		{ "ToolTip", "Returns the int cast to a float" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathIntToFloat_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathIntToFloat>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntToFloat_Statics::NewProp_Value_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathIntToFloat_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathIntToFloat, Value), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntToFloat_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntToFloat_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntToFloat_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathIntToFloat_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathIntToFloat, Result), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntToFloat_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntToFloat_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathIntToFloat_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathIntToFloat_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathIntToFloat_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathIntToFloat_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathIntBase,
		&NewStructOps,
		"RigUnit_MathIntToFloat",
		sizeof(FRigUnit_MathIntToFloat),
		alignof(FRigUnit_MathIntToFloat),
		Z_Construct_UScriptStruct_FRigUnit_MathIntToFloat_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntToFloat_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntToFloat_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntToFloat_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntToFloat()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntToFloat_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathIntToFloat"), sizeof(FRigUnit_MathIntToFloat), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntToFloat_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathIntToFloat_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntToFloat_Hash() { return 2665893222U; }

void FRigUnit_MathIntToFloat::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathIntAbs>() == std::is_polymorphic<FRigUnit_MathIntUnaryOp>(), "USTRUCT FRigUnit_MathIntAbs cannot be polymorphic unless super FRigUnit_MathIntUnaryOp is polymorphic");

class UScriptStruct* FRigUnit_MathIntAbs::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntAbs_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathIntAbs, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathIntAbs"), sizeof(FRigUnit_MathIntAbs), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntAbs_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathIntAbs::Execute"), &FRigUnit_MathIntAbs::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathIntAbs>()
{
	return FRigUnit_MathIntAbs::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathIntAbs(FRigUnit_MathIntAbs::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathIntAbs"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntAbs
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntAbs()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathIntAbs>(FName(TEXT("RigUnit_MathIntAbs")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntAbs;
	struct Z_Construct_UScriptStruct_FRigUnit_MathIntAbs_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntAbs_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the absolute (positive) value\n */" },
		{ "DisplayName", "Absolute" },
		{ "Keywords", "Abs,Neg" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
		{ "PrototypeName", "Absolute" },
		{ "ToolTip", "Returns the absolute (positive) value" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathIntAbs_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathIntAbs>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathIntAbs_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathIntUnaryOp,
		&NewStructOps,
		"RigUnit_MathIntAbs",
		sizeof(FRigUnit_MathIntAbs),
		alignof(FRigUnit_MathIntAbs),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntAbs_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntAbs_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntAbs()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntAbs_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathIntAbs"), sizeof(FRigUnit_MathIntAbs), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntAbs_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathIntAbs_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntAbs_Hash() { return 1681552058U; }

void FRigUnit_MathIntAbs::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathIntNegate>() == std::is_polymorphic<FRigUnit_MathIntUnaryOp>(), "USTRUCT FRigUnit_MathIntNegate cannot be polymorphic unless super FRigUnit_MathIntUnaryOp is polymorphic");

class UScriptStruct* FRigUnit_MathIntNegate::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntNegate_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathIntNegate, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathIntNegate"), sizeof(FRigUnit_MathIntNegate), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntNegate_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathIntNegate::Execute"), &FRigUnit_MathIntNegate::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathIntNegate>()
{
	return FRigUnit_MathIntNegate::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathIntNegate(FRigUnit_MathIntNegate::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathIntNegate"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntNegate
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntNegate()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathIntNegate>(FName(TEXT("RigUnit_MathIntNegate")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntNegate;
	struct Z_Construct_UScriptStruct_FRigUnit_MathIntNegate_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntNegate_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the negative value\n */" },
		{ "DisplayName", "Negate" },
		{ "Keywords", "-,Abs" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
		{ "PrototypeName", "Negate" },
		{ "ToolTip", "Returns the negative value" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathIntNegate_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathIntNegate>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathIntNegate_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathIntUnaryOp,
		&NewStructOps,
		"RigUnit_MathIntNegate",
		sizeof(FRigUnit_MathIntNegate),
		alignof(FRigUnit_MathIntNegate),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntNegate_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntNegate_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntNegate()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntNegate_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathIntNegate"), sizeof(FRigUnit_MathIntNegate), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntNegate_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathIntNegate_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntNegate_Hash() { return 3106267025U; }

void FRigUnit_MathIntNegate::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathIntPow>() == std::is_polymorphic<FRigUnit_MathIntBinaryOp>(), "USTRUCT FRigUnit_MathIntPow cannot be polymorphic unless super FRigUnit_MathIntBinaryOp is polymorphic");

class UScriptStruct* FRigUnit_MathIntPow::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntPow_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathIntPow, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathIntPow"), sizeof(FRigUnit_MathIntPow), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntPow_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathIntPow::Execute"), &FRigUnit_MathIntPow::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathIntPow>()
{
	return FRigUnit_MathIntPow::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathIntPow(FRigUnit_MathIntPow::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathIntPow"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntPow
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntPow()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathIntPow>(FName(TEXT("RigUnit_MathIntPow")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntPow;
	struct Z_Construct_UScriptStruct_FRigUnit_MathIntPow_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntPow_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the value of A raised to the power of B.\n */" },
		{ "DisplayName", "Power" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
		{ "PrototypeName", "Power" },
		{ "ToolTip", "Returns the value of A raised to the power of B." },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathIntPow_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathIntPow>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathIntPow_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathIntBinaryOp,
		&NewStructOps,
		"RigUnit_MathIntPow",
		sizeof(FRigUnit_MathIntPow),
		alignof(FRigUnit_MathIntPow),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntPow_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntPow_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntPow()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntPow_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathIntPow"), sizeof(FRigUnit_MathIntPow), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntPow_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathIntPow_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntPow_Hash() { return 21415678U; }

void FRigUnit_MathIntPow::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathIntMax>() == std::is_polymorphic<FRigUnit_MathIntBinaryOp>(), "USTRUCT FRigUnit_MathIntMax cannot be polymorphic unless super FRigUnit_MathIntBinaryOp is polymorphic");

class UScriptStruct* FRigUnit_MathIntMax::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntMax_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathIntMax, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathIntMax"), sizeof(FRigUnit_MathIntMax), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntMax_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathIntMax::Execute"), &FRigUnit_MathIntMax::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathIntMax>()
{
	return FRigUnit_MathIntMax::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathIntMax(FRigUnit_MathIntMax::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathIntMax"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntMax
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntMax()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathIntMax>(FName(TEXT("RigUnit_MathIntMax")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntMax;
	struct Z_Construct_UScriptStruct_FRigUnit_MathIntMax_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntMax_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the larger of the two values\n */" },
		{ "DisplayName", "Maximum" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
		{ "PrototypeName", "Maximum" },
		{ "ToolTip", "Returns the larger of the two values" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathIntMax_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathIntMax>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathIntMax_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathIntBinaryOp,
		&NewStructOps,
		"RigUnit_MathIntMax",
		sizeof(FRigUnit_MathIntMax),
		alignof(FRigUnit_MathIntMax),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntMax_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntMax_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntMax()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntMax_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathIntMax"), sizeof(FRigUnit_MathIntMax), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntMax_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathIntMax_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntMax_Hash() { return 766889460U; }

void FRigUnit_MathIntMax::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathIntMin>() == std::is_polymorphic<FRigUnit_MathIntBinaryOp>(), "USTRUCT FRigUnit_MathIntMin cannot be polymorphic unless super FRigUnit_MathIntBinaryOp is polymorphic");

class UScriptStruct* FRigUnit_MathIntMin::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntMin_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathIntMin, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathIntMin"), sizeof(FRigUnit_MathIntMin), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntMin_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathIntMin::Execute"), &FRigUnit_MathIntMin::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathIntMin>()
{
	return FRigUnit_MathIntMin::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathIntMin(FRigUnit_MathIntMin::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathIntMin"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntMin
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntMin()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathIntMin>(FName(TEXT("RigUnit_MathIntMin")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntMin;
	struct Z_Construct_UScriptStruct_FRigUnit_MathIntMin_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntMin_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the smaller of the two values\n */" },
		{ "DisplayName", "Minimum" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
		{ "PrototypeName", "Minimum" },
		{ "ToolTip", "Returns the smaller of the two values" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathIntMin_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathIntMin>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathIntMin_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathIntBinaryOp,
		&NewStructOps,
		"RigUnit_MathIntMin",
		sizeof(FRigUnit_MathIntMin),
		alignof(FRigUnit_MathIntMin),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntMin_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntMin_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntMin()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntMin_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathIntMin"), sizeof(FRigUnit_MathIntMin), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntMin_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathIntMin_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntMin_Hash() { return 3104892755U; }

void FRigUnit_MathIntMin::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathIntMod>() == std::is_polymorphic<FRigUnit_MathIntBinaryOp>(), "USTRUCT FRigUnit_MathIntMod cannot be polymorphic unless super FRigUnit_MathIntBinaryOp is polymorphic");

class UScriptStruct* FRigUnit_MathIntMod::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntMod_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathIntMod, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathIntMod"), sizeof(FRigUnit_MathIntMod), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntMod_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathIntMod::Execute"), &FRigUnit_MathIntMod::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathIntMod>()
{
	return FRigUnit_MathIntMod::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathIntMod(FRigUnit_MathIntMod::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathIntMod"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntMod
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntMod()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathIntMod>(FName(TEXT("RigUnit_MathIntMod")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntMod;
	struct Z_Construct_UScriptStruct_FRigUnit_MathIntMod_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntMod_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the modulo of the two values\n */" },
		{ "DisplayName", "Modulo" },
		{ "Keywords", "%,fmod" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
		{ "PrototypeName", "Modulo" },
		{ "ToolTip", "Returns the modulo of the two values" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathIntMod_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathIntMod>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathIntMod_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathIntBinaryOp,
		&NewStructOps,
		"RigUnit_MathIntMod",
		sizeof(FRigUnit_MathIntMod),
		alignof(FRigUnit_MathIntMod),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntMod_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntMod_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntMod()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntMod_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathIntMod"), sizeof(FRigUnit_MathIntMod), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntMod_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathIntMod_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntMod_Hash() { return 2588605799U; }

void FRigUnit_MathIntMod::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathIntDiv>() == std::is_polymorphic<FRigUnit_MathIntBinaryOp>(), "USTRUCT FRigUnit_MathIntDiv cannot be polymorphic unless super FRigUnit_MathIntBinaryOp is polymorphic");

class UScriptStruct* FRigUnit_MathIntDiv::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntDiv_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathIntDiv, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathIntDiv"), sizeof(FRigUnit_MathIntDiv), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntDiv_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathIntDiv::Execute"), &FRigUnit_MathIntDiv::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathIntDiv>()
{
	return FRigUnit_MathIntDiv::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathIntDiv(FRigUnit_MathIntDiv::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathIntDiv"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntDiv
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntDiv()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathIntDiv>(FName(TEXT("RigUnit_MathIntDiv")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntDiv;
	struct Z_Construct_UScriptStruct_FRigUnit_MathIntDiv_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntDiv_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the division of the two values\n */" },
		{ "DisplayName", "Divide" },
		{ "Keywords", "Division,Divisor,/" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
		{ "PrototypeName", "Divide" },
		{ "ToolTip", "Returns the division of the two values" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathIntDiv_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathIntDiv>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathIntDiv_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathIntBinaryOp,
		&NewStructOps,
		"RigUnit_MathIntDiv",
		sizeof(FRigUnit_MathIntDiv),
		alignof(FRigUnit_MathIntDiv),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntDiv_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntDiv_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntDiv()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntDiv_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathIntDiv"), sizeof(FRigUnit_MathIntDiv), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntDiv_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathIntDiv_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntDiv_Hash() { return 4074132326U; }

void FRigUnit_MathIntDiv::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathIntMul>() == std::is_polymorphic<FRigUnit_MathIntBinaryOp>(), "USTRUCT FRigUnit_MathIntMul cannot be polymorphic unless super FRigUnit_MathIntBinaryOp is polymorphic");

class UScriptStruct* FRigUnit_MathIntMul::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntMul_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathIntMul, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathIntMul"), sizeof(FRigUnit_MathIntMul), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntMul_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathIntMul::Execute"), &FRigUnit_MathIntMul::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathIntMul>()
{
	return FRigUnit_MathIntMul::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathIntMul(FRigUnit_MathIntMul::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathIntMul"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntMul
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntMul()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathIntMul>(FName(TEXT("RigUnit_MathIntMul")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntMul;
	struct Z_Construct_UScriptStruct_FRigUnit_MathIntMul_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntMul_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the product of the two values\n */" },
		{ "DisplayName", "Multiply" },
		{ "Keywords", "Product,*" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
		{ "PrototypeName", "Multiply" },
		{ "ToolTip", "Returns the product of the two values" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathIntMul_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathIntMul>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathIntMul_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathIntBinaryOp,
		&NewStructOps,
		"RigUnit_MathIntMul",
		sizeof(FRigUnit_MathIntMul),
		alignof(FRigUnit_MathIntMul),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntMul_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntMul_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntMul()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntMul_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathIntMul"), sizeof(FRigUnit_MathIntMul), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntMul_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathIntMul_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntMul_Hash() { return 646472290U; }

void FRigUnit_MathIntMul::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathIntSub>() == std::is_polymorphic<FRigUnit_MathIntBinaryOp>(), "USTRUCT FRigUnit_MathIntSub cannot be polymorphic unless super FRigUnit_MathIntBinaryOp is polymorphic");

class UScriptStruct* FRigUnit_MathIntSub::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntSub_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathIntSub, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathIntSub"), sizeof(FRigUnit_MathIntSub), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntSub_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathIntSub::Execute"), &FRigUnit_MathIntSub::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathIntSub>()
{
	return FRigUnit_MathIntSub::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathIntSub(FRigUnit_MathIntSub::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathIntSub"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntSub
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntSub()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathIntSub>(FName(TEXT("RigUnit_MathIntSub")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntSub;
	struct Z_Construct_UScriptStruct_FRigUnit_MathIntSub_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntSub_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the difference of the two values\n */" },
		{ "DisplayName", "Subtract" },
		{ "Keywords", "-" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
		{ "PrototypeName", "Subtract" },
		{ "ToolTip", "Returns the difference of the two values" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathIntSub_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathIntSub>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathIntSub_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathIntBinaryOp,
		&NewStructOps,
		"RigUnit_MathIntSub",
		sizeof(FRigUnit_MathIntSub),
		alignof(FRigUnit_MathIntSub),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntSub_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntSub_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntSub()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntSub_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathIntSub"), sizeof(FRigUnit_MathIntSub), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntSub_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathIntSub_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntSub_Hash() { return 101278217U; }

void FRigUnit_MathIntSub::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathIntAdd>() == std::is_polymorphic<FRigUnit_MathIntBinaryOp>(), "USTRUCT FRigUnit_MathIntAdd cannot be polymorphic unless super FRigUnit_MathIntBinaryOp is polymorphic");

class UScriptStruct* FRigUnit_MathIntAdd::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntAdd_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathIntAdd, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathIntAdd"), sizeof(FRigUnit_MathIntAdd), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntAdd_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathIntAdd::Execute"), &FRigUnit_MathIntAdd::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathIntAdd>()
{
	return FRigUnit_MathIntAdd::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathIntAdd(FRigUnit_MathIntAdd::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathIntAdd"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntAdd
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntAdd()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathIntAdd>(FName(TEXT("RigUnit_MathIntAdd")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntAdd;
	struct Z_Construct_UScriptStruct_FRigUnit_MathIntAdd_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntAdd_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the sum of the two values\n */" },
		{ "DisplayName", "Add" },
		{ "Keywords", "Sum,+" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
		{ "PrototypeName", "Add" },
		{ "ToolTip", "Returns the sum of the two values" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathIntAdd_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathIntAdd>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathIntAdd_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathIntBinaryOp,
		&NewStructOps,
		"RigUnit_MathIntAdd",
		sizeof(FRigUnit_MathIntAdd),
		alignof(FRigUnit_MathIntAdd),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntAdd_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntAdd_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntAdd()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntAdd_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathIntAdd"), sizeof(FRigUnit_MathIntAdd), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntAdd_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathIntAdd_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntAdd_Hash() { return 2094934497U; }

void FRigUnit_MathIntAdd::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathIntBinaryOp>() == std::is_polymorphic<FRigUnit_MathIntBase>(), "USTRUCT FRigUnit_MathIntBinaryOp cannot be polymorphic unless super FRigUnit_MathIntBase is polymorphic");

class UScriptStruct* FRigUnit_MathIntBinaryOp::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntBinaryOp_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathIntBinaryOp, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathIntBinaryOp"), sizeof(FRigUnit_MathIntBinaryOp), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntBinaryOp_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathIntBinaryOp>()
{
	return FRigUnit_MathIntBinaryOp::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathIntBinaryOp(FRigUnit_MathIntBinaryOp::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathIntBinaryOp"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntBinaryOp
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntBinaryOp()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathIntBinaryOp>(FName(TEXT("RigUnit_MathIntBinaryOp")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntBinaryOp;
	struct Z_Construct_UScriptStruct_FRigUnit_MathIntBinaryOp_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_A_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_A;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_B_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_B;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntBinaryOp_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathIntBinaryOp_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathIntBinaryOp>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntBinaryOp_Statics::NewProp_A_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathIntBinaryOp_Statics::NewProp_A = { "A", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathIntBinaryOp, A), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntBinaryOp_Statics::NewProp_A_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntBinaryOp_Statics::NewProp_A_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntBinaryOp_Statics::NewProp_B_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathIntBinaryOp_Statics::NewProp_B = { "B", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathIntBinaryOp, B), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntBinaryOp_Statics::NewProp_B_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntBinaryOp_Statics::NewProp_B_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntBinaryOp_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathIntBinaryOp_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathIntBinaryOp, Result), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntBinaryOp_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntBinaryOp_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathIntBinaryOp_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathIntBinaryOp_Statics::NewProp_A,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathIntBinaryOp_Statics::NewProp_B,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathIntBinaryOp_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathIntBinaryOp_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathIntBase,
		&NewStructOps,
		"RigUnit_MathIntBinaryOp",
		sizeof(FRigUnit_MathIntBinaryOp),
		alignof(FRigUnit_MathIntBinaryOp),
		Z_Construct_UScriptStruct_FRigUnit_MathIntBinaryOp_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntBinaryOp_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntBinaryOp_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntBinaryOp_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntBinaryOp()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntBinaryOp_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathIntBinaryOp"), sizeof(FRigUnit_MathIntBinaryOp), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntBinaryOp_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathIntBinaryOp_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntBinaryOp_Hash() { return 4222204052U; }

static_assert(std::is_polymorphic<FRigUnit_MathIntUnaryOp>() == std::is_polymorphic<FRigUnit_MathIntBase>(), "USTRUCT FRigUnit_MathIntUnaryOp cannot be polymorphic unless super FRigUnit_MathIntBase is polymorphic");

class UScriptStruct* FRigUnit_MathIntUnaryOp::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntUnaryOp_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathIntUnaryOp, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathIntUnaryOp"), sizeof(FRigUnit_MathIntUnaryOp), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntUnaryOp_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathIntUnaryOp>()
{
	return FRigUnit_MathIntUnaryOp::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathIntUnaryOp(FRigUnit_MathIntUnaryOp::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathIntUnaryOp"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntUnaryOp
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntUnaryOp()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathIntUnaryOp>(FName(TEXT("RigUnit_MathIntUnaryOp")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntUnaryOp;
	struct Z_Construct_UScriptStruct_FRigUnit_MathIntUnaryOp_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntUnaryOp_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathIntUnaryOp_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathIntUnaryOp>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntUnaryOp_Statics::NewProp_Value_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathIntUnaryOp_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathIntUnaryOp, Value), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntUnaryOp_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntUnaryOp_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntUnaryOp_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathIntUnaryOp_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathIntUnaryOp, Result), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntUnaryOp_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntUnaryOp_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathIntUnaryOp_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathIntUnaryOp_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathIntUnaryOp_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathIntUnaryOp_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathIntBase,
		&NewStructOps,
		"RigUnit_MathIntUnaryOp",
		sizeof(FRigUnit_MathIntUnaryOp),
		alignof(FRigUnit_MathIntUnaryOp),
		Z_Construct_UScriptStruct_FRigUnit_MathIntUnaryOp_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntUnaryOp_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntUnaryOp_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntUnaryOp_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntUnaryOp()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntUnaryOp_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathIntUnaryOp"), sizeof(FRigUnit_MathIntUnaryOp), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntUnaryOp_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathIntUnaryOp_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntUnaryOp_Hash() { return 1320987903U; }

static_assert(std::is_polymorphic<FRigUnit_MathIntBase>() == std::is_polymorphic<FRigUnit_MathBase>(), "USTRUCT FRigUnit_MathIntBase cannot be polymorphic unless super FRigUnit_MathBase is polymorphic");

class UScriptStruct* FRigUnit_MathIntBase::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntBase_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathIntBase, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathIntBase"), sizeof(FRigUnit_MathIntBase), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntBase_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathIntBase>()
{
	return FRigUnit_MathIntBase::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathIntBase(FRigUnit_MathIntBase::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathIntBase"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntBase
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntBase()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathIntBase>(FName(TEXT("RigUnit_MathIntBase")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathIntBase;
	struct Z_Construct_UScriptStruct_FRigUnit_MathIntBase_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathIntBase_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "Category", "Math|Int" },
		{ "MenuDescSuffix", "(Int)" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathInt.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathIntBase_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathIntBase>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathIntBase_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathBase,
		&NewStructOps,
		"RigUnit_MathIntBase",
		sizeof(FRigUnit_MathIntBase),
		alignof(FRigUnit_MathIntBase),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathIntBase_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathIntBase_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathIntBase()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntBase_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathIntBase"), sizeof(FRigUnit_MathIntBase), Get_Z_Construct_UScriptStruct_FRigUnit_MathIntBase_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathIntBase_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathIntBase_Hash() { return 2405325278U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
