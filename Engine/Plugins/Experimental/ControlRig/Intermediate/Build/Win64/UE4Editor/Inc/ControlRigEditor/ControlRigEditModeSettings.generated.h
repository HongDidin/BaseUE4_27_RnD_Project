// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIGEDITOR_ControlRigEditModeSettings_generated_h
#error "ControlRigEditModeSettings.generated.h already included, missing '#pragma once' in ControlRigEditModeSettings.h"
#endif
#define CONTROLRIGEDITOR_ControlRigEditModeSettings_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditModeSettings_h_14_SPARSE_DATA
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditModeSettings_h_14_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditModeSettings_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditModeSettings_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUControlRigEditModeSettings(); \
	friend struct Z_Construct_UClass_UControlRigEditModeSettings_Statics; \
public: \
	DECLARE_CLASS(UControlRigEditModeSettings, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRigEditor"), NO_API) \
	DECLARE_SERIALIZER(UControlRigEditModeSettings)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditModeSettings_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUControlRigEditModeSettings(); \
	friend struct Z_Construct_UClass_UControlRigEditModeSettings_Statics; \
public: \
	DECLARE_CLASS(UControlRigEditModeSettings, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRigEditor"), NO_API) \
	DECLARE_SERIALIZER(UControlRigEditModeSettings)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditModeSettings_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UControlRigEditModeSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigEditModeSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigEditModeSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigEditModeSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigEditModeSettings(UControlRigEditModeSettings&&); \
	NO_API UControlRigEditModeSettings(const UControlRigEditModeSettings&); \
public:


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditModeSettings_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigEditModeSettings(UControlRigEditModeSettings&&); \
	NO_API UControlRigEditModeSettings(const UControlRigEditModeSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigEditModeSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigEditModeSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UControlRigEditModeSettings)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditModeSettings_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditModeSettings_h_11_PROLOG
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditModeSettings_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditModeSettings_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditModeSettings_h_14_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditModeSettings_h_14_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditModeSettings_h_14_INCLASS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditModeSettings_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditModeSettings_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditModeSettings_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditModeSettings_h_14_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditModeSettings_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditModeSettings_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditModeSettings_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONTROLRIGEDITOR_API UClass* StaticClass<class UControlRigEditModeSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditModeSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
