// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigUnit_MathRBFInterpolate_generated_h
#error "RigUnit_MathRBFInterpolate.generated.h already included, missing '#pragma once' in RigUnit_MathRBFInterpolate.h"
#endif
#define CONTROLRIG_RigUnit_MathRBFInterpolate_generated_h


#define FRigUnit_MathRBFInterpolateVectorXform_Execute() \
	void FRigUnit_MathRBFInterpolateVectorXform::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FMathRBFInterpolateVectorXform_Target>& Targets, \
		FTransform& Output, \
		const FVector& Input, \
		const ERBFVectorDistanceType DistanceFunction, \
		const ERBFKernelType SmoothingFunction, \
		const float SmoothingRadius, \
		const bool bNormalizeOutput, \
		FRigUnit_MathRBFInterpolateVectorWorkData& WorkData, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_MathRBFInterpolate_h_451_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorXform_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FMathRBFInterpolateVectorXform_Target>& Targets, \
		FTransform& Output, \
		const FVector& Input, \
		const ERBFVectorDistanceType DistanceFunction, \
		const ERBFKernelType SmoothingFunction, \
		const float SmoothingRadius, \
		const bool bNormalizeOutput, \
		FRigUnit_MathRBFInterpolateVectorWorkData& WorkData, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		FRigVMFixedArray<FMathRBFInterpolateVectorXform_Target> Targets((FMathRBFInterpolateVectorXform_Target*)RigVMMemoryHandles[0].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[1].GetData())); \
		FTransform& Output = *(FTransform*)RigVMMemoryHandles[2].GetData(); \
		const FVector& Input = *(FVector*)RigVMMemoryHandles[3].GetData(); \
		ERBFVectorDistanceType DistanceFunction = (ERBFVectorDistanceType)*(uint8*)RigVMMemoryHandles[4].GetData(); \
		ERBFKernelType SmoothingFunction = (ERBFKernelType)*(uint8*)RigVMMemoryHandles[5].GetData(); \
		const float SmoothingRadius = *(float*)RigVMMemoryHandles[6].GetData(); \
		const bool bNormalizeOutput = *(bool*)RigVMMemoryHandles[7].GetData(); \
		FRigVMDynamicArray<FRigUnit_MathRBFInterpolateVectorWorkData> WorkData_8_Array(*((FRigVMByteArray*)RigVMMemoryHandles[8].GetData(0, false))); \
		WorkData_8_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FRigUnit_MathRBFInterpolateVectorWorkData& WorkData = WorkData_8_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Targets, \
			Output, \
			Input, \
			DistanceFunction, \
			SmoothingFunction, \
			SmoothingRadius, \
			bNormalizeOutput, \
			WorkData, \
			Context \
		); \
	} \
	typedef FRigUnit_MathRBFInterpolateVectorBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_MathRBFInterpolateVectorXform>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_MathRBFInterpolate_h_438_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMathRBFInterpolateVectorXform_Target_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FMathRBFInterpolateVectorXform_Target>();


#define FRigUnit_MathRBFInterpolateVectorQuat_Execute() \
	void FRigUnit_MathRBFInterpolateVectorQuat::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FMathRBFInterpolateVectorQuat_Target>& Targets, \
		FQuat& Output, \
		const FVector& Input, \
		const ERBFVectorDistanceType DistanceFunction, \
		const ERBFKernelType SmoothingFunction, \
		const float SmoothingRadius, \
		const bool bNormalizeOutput, \
		FRigUnit_MathRBFInterpolateVectorWorkData& WorkData, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_MathRBFInterpolate_h_423_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorQuat_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FMathRBFInterpolateVectorQuat_Target>& Targets, \
		FQuat& Output, \
		const FVector& Input, \
		const ERBFVectorDistanceType DistanceFunction, \
		const ERBFKernelType SmoothingFunction, \
		const float SmoothingRadius, \
		const bool bNormalizeOutput, \
		FRigUnit_MathRBFInterpolateVectorWorkData& WorkData, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		FRigVMFixedArray<FMathRBFInterpolateVectorQuat_Target> Targets((FMathRBFInterpolateVectorQuat_Target*)RigVMMemoryHandles[0].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[1].GetData())); \
		FQuat& Output = *(FQuat*)RigVMMemoryHandles[2].GetData(); \
		const FVector& Input = *(FVector*)RigVMMemoryHandles[3].GetData(); \
		ERBFVectorDistanceType DistanceFunction = (ERBFVectorDistanceType)*(uint8*)RigVMMemoryHandles[4].GetData(); \
		ERBFKernelType SmoothingFunction = (ERBFKernelType)*(uint8*)RigVMMemoryHandles[5].GetData(); \
		const float SmoothingRadius = *(float*)RigVMMemoryHandles[6].GetData(); \
		const bool bNormalizeOutput = *(bool*)RigVMMemoryHandles[7].GetData(); \
		FRigVMDynamicArray<FRigUnit_MathRBFInterpolateVectorWorkData> WorkData_8_Array(*((FRigVMByteArray*)RigVMMemoryHandles[8].GetData(0, false))); \
		WorkData_8_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FRigUnit_MathRBFInterpolateVectorWorkData& WorkData = WorkData_8_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Targets, \
			Output, \
			Input, \
			DistanceFunction, \
			SmoothingFunction, \
			SmoothingRadius, \
			bNormalizeOutput, \
			WorkData, \
			Context \
		); \
	} \
	typedef FRigUnit_MathRBFInterpolateVectorBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_MathRBFInterpolateVectorQuat>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_MathRBFInterpolate_h_410_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMathRBFInterpolateVectorQuat_Target_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FMathRBFInterpolateVectorQuat_Target>();


#define FRigUnit_MathRBFInterpolateVectorColor_Execute() \
	void FRigUnit_MathRBFInterpolateVectorColor::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FMathRBFInterpolateVectorColor_Target>& Targets, \
		FLinearColor& Output, \
		const FVector& Input, \
		const ERBFVectorDistanceType DistanceFunction, \
		const ERBFKernelType SmoothingFunction, \
		const float SmoothingRadius, \
		const bool bNormalizeOutput, \
		FRigUnit_MathRBFInterpolateVectorWorkData& WorkData, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_MathRBFInterpolate_h_393_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorColor_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FMathRBFInterpolateVectorColor_Target>& Targets, \
		FLinearColor& Output, \
		const FVector& Input, \
		const ERBFVectorDistanceType DistanceFunction, \
		const ERBFKernelType SmoothingFunction, \
		const float SmoothingRadius, \
		const bool bNormalizeOutput, \
		FRigUnit_MathRBFInterpolateVectorWorkData& WorkData, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		FRigVMFixedArray<FMathRBFInterpolateVectorColor_Target> Targets((FMathRBFInterpolateVectorColor_Target*)RigVMMemoryHandles[0].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[1].GetData())); \
		FLinearColor& Output = *(FLinearColor*)RigVMMemoryHandles[2].GetData(); \
		const FVector& Input = *(FVector*)RigVMMemoryHandles[3].GetData(); \
		ERBFVectorDistanceType DistanceFunction = (ERBFVectorDistanceType)*(uint8*)RigVMMemoryHandles[4].GetData(); \
		ERBFKernelType SmoothingFunction = (ERBFKernelType)*(uint8*)RigVMMemoryHandles[5].GetData(); \
		const float SmoothingRadius = *(float*)RigVMMemoryHandles[6].GetData(); \
		const bool bNormalizeOutput = *(bool*)RigVMMemoryHandles[7].GetData(); \
		FRigVMDynamicArray<FRigUnit_MathRBFInterpolateVectorWorkData> WorkData_8_Array(*((FRigVMByteArray*)RigVMMemoryHandles[8].GetData(0, false))); \
		WorkData_8_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FRigUnit_MathRBFInterpolateVectorWorkData& WorkData = WorkData_8_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Targets, \
			Output, \
			Input, \
			DistanceFunction, \
			SmoothingFunction, \
			SmoothingRadius, \
			bNormalizeOutput, \
			WorkData, \
			Context \
		); \
	} \
	typedef FRigUnit_MathRBFInterpolateVectorBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_MathRBFInterpolateVectorColor>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_MathRBFInterpolate_h_380_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMathRBFInterpolateVectorColor_Target_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FMathRBFInterpolateVectorColor_Target>();


#define FRigUnit_MathRBFInterpolateVectorVector_Execute() \
	void FRigUnit_MathRBFInterpolateVectorVector::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FMathRBFInterpolateVectorVector_Target>& Targets, \
		FVector& Output, \
		const FVector& Input, \
		const ERBFVectorDistanceType DistanceFunction, \
		const ERBFKernelType SmoothingFunction, \
		const float SmoothingRadius, \
		const bool bNormalizeOutput, \
		FRigUnit_MathRBFInterpolateVectorWorkData& WorkData, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_MathRBFInterpolate_h_364_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorVector_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FMathRBFInterpolateVectorVector_Target>& Targets, \
		FVector& Output, \
		const FVector& Input, \
		const ERBFVectorDistanceType DistanceFunction, \
		const ERBFKernelType SmoothingFunction, \
		const float SmoothingRadius, \
		const bool bNormalizeOutput, \
		FRigUnit_MathRBFInterpolateVectorWorkData& WorkData, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		FRigVMFixedArray<FMathRBFInterpolateVectorVector_Target> Targets((FMathRBFInterpolateVectorVector_Target*)RigVMMemoryHandles[0].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[1].GetData())); \
		FVector& Output = *(FVector*)RigVMMemoryHandles[2].GetData(); \
		const FVector& Input = *(FVector*)RigVMMemoryHandles[3].GetData(); \
		ERBFVectorDistanceType DistanceFunction = (ERBFVectorDistanceType)*(uint8*)RigVMMemoryHandles[4].GetData(); \
		ERBFKernelType SmoothingFunction = (ERBFKernelType)*(uint8*)RigVMMemoryHandles[5].GetData(); \
		const float SmoothingRadius = *(float*)RigVMMemoryHandles[6].GetData(); \
		const bool bNormalizeOutput = *(bool*)RigVMMemoryHandles[7].GetData(); \
		FRigVMDynamicArray<FRigUnit_MathRBFInterpolateVectorWorkData> WorkData_8_Array(*((FRigVMByteArray*)RigVMMemoryHandles[8].GetData(0, false))); \
		WorkData_8_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FRigUnit_MathRBFInterpolateVectorWorkData& WorkData = WorkData_8_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Targets, \
			Output, \
			Input, \
			DistanceFunction, \
			SmoothingFunction, \
			SmoothingRadius, \
			bNormalizeOutput, \
			WorkData, \
			Context \
		); \
	} \
	typedef FRigUnit_MathRBFInterpolateVectorBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_MathRBFInterpolateVectorVector>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_MathRBFInterpolate_h_351_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMathRBFInterpolateVectorVector_Target_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FMathRBFInterpolateVectorVector_Target>();


#define FRigUnit_MathRBFInterpolateVectorFloat_Execute() \
	void FRigUnit_MathRBFInterpolateVectorFloat::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FMathRBFInterpolateVectorFloat_Target>& Targets, \
		float& Output, \
		const FVector& Input, \
		const ERBFVectorDistanceType DistanceFunction, \
		const ERBFKernelType SmoothingFunction, \
		const float SmoothingRadius, \
		const bool bNormalizeOutput, \
		FRigUnit_MathRBFInterpolateVectorWorkData& WorkData, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_MathRBFInterpolate_h_335_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorFloat_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FMathRBFInterpolateVectorFloat_Target>& Targets, \
		float& Output, \
		const FVector& Input, \
		const ERBFVectorDistanceType DistanceFunction, \
		const ERBFKernelType SmoothingFunction, \
		const float SmoothingRadius, \
		const bool bNormalizeOutput, \
		FRigUnit_MathRBFInterpolateVectorWorkData& WorkData, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		FRigVMFixedArray<FMathRBFInterpolateVectorFloat_Target> Targets((FMathRBFInterpolateVectorFloat_Target*)RigVMMemoryHandles[0].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[1].GetData())); \
		float& Output = *(float*)RigVMMemoryHandles[2].GetData(); \
		const FVector& Input = *(FVector*)RigVMMemoryHandles[3].GetData(); \
		ERBFVectorDistanceType DistanceFunction = (ERBFVectorDistanceType)*(uint8*)RigVMMemoryHandles[4].GetData(); \
		ERBFKernelType SmoothingFunction = (ERBFKernelType)*(uint8*)RigVMMemoryHandles[5].GetData(); \
		const float SmoothingRadius = *(float*)RigVMMemoryHandles[6].GetData(); \
		const bool bNormalizeOutput = *(bool*)RigVMMemoryHandles[7].GetData(); \
		FRigVMDynamicArray<FRigUnit_MathRBFInterpolateVectorWorkData> WorkData_8_Array(*((FRigVMByteArray*)RigVMMemoryHandles[8].GetData(0, false))); \
		WorkData_8_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FRigUnit_MathRBFInterpolateVectorWorkData& WorkData = WorkData_8_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Targets, \
			Output, \
			Input, \
			DistanceFunction, \
			SmoothingFunction, \
			SmoothingRadius, \
			bNormalizeOutput, \
			WorkData, \
			Context \
		); \
	} \
	typedef FRigUnit_MathRBFInterpolateVectorBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_MathRBFInterpolateVectorFloat>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_MathRBFInterpolate_h_322_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMathRBFInterpolateVectorFloat_Target_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FMathRBFInterpolateVectorFloat_Target>();


#define FRigUnit_MathRBFInterpolateQuatXform_Execute() \
	void FRigUnit_MathRBFInterpolateQuatXform::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FMathRBFInterpolateQuatXform_Target>& Targets, \
		FTransform& Output, \
		const FQuat& Input, \
		const ERBFQuatDistanceType DistanceFunction, \
		const ERBFKernelType SmoothingFunction, \
		const float SmoothingAngle, \
		const bool bNormalizeOutput, \
		const FVector& TwistAxis, \
		FRigUnit_MathRBFInterpolateQuatWorkData& WorkData, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_MathRBFInterpolate_h_304_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatXform_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FMathRBFInterpolateQuatXform_Target>& Targets, \
		FTransform& Output, \
		const FQuat& Input, \
		const ERBFQuatDistanceType DistanceFunction, \
		const ERBFKernelType SmoothingFunction, \
		const float SmoothingAngle, \
		const bool bNormalizeOutput, \
		const FVector& TwistAxis, \
		FRigUnit_MathRBFInterpolateQuatWorkData& WorkData, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		FRigVMFixedArray<FMathRBFInterpolateQuatXform_Target> Targets((FMathRBFInterpolateQuatXform_Target*)RigVMMemoryHandles[0].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[1].GetData())); \
		FTransform& Output = *(FTransform*)RigVMMemoryHandles[2].GetData(); \
		const FQuat& Input = *(FQuat*)RigVMMemoryHandles[3].GetData(); \
		ERBFQuatDistanceType DistanceFunction = (ERBFQuatDistanceType)*(uint8*)RigVMMemoryHandles[4].GetData(); \
		ERBFKernelType SmoothingFunction = (ERBFKernelType)*(uint8*)RigVMMemoryHandles[5].GetData(); \
		const float SmoothingAngle = *(float*)RigVMMemoryHandles[6].GetData(); \
		const bool bNormalizeOutput = *(bool*)RigVMMemoryHandles[7].GetData(); \
		const FVector& TwistAxis = *(FVector*)RigVMMemoryHandles[8].GetData(); \
		FRigVMDynamicArray<FRigUnit_MathRBFInterpolateQuatWorkData> WorkData_9_Array(*((FRigVMByteArray*)RigVMMemoryHandles[9].GetData(0, false))); \
		WorkData_9_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FRigUnit_MathRBFInterpolateQuatWorkData& WorkData = WorkData_9_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Targets, \
			Output, \
			Input, \
			DistanceFunction, \
			SmoothingFunction, \
			SmoothingAngle, \
			bNormalizeOutput, \
			TwistAxis, \
			WorkData, \
			Context \
		); \
	} \
	typedef FRigUnit_MathRBFInterpolateQuatBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_MathRBFInterpolateQuatXform>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_MathRBFInterpolate_h_291_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMathRBFInterpolateQuatXform_Target_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FMathRBFInterpolateQuatXform_Target>();


#define FRigUnit_MathRBFInterpolateQuatQuat_Execute() \
	void FRigUnit_MathRBFInterpolateQuatQuat::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FMathRBFInterpolateQuatQuat_Target>& Targets, \
		FQuat& Output, \
		const FQuat& Input, \
		const ERBFQuatDistanceType DistanceFunction, \
		const ERBFKernelType SmoothingFunction, \
		const float SmoothingAngle, \
		const bool bNormalizeOutput, \
		const FVector& TwistAxis, \
		FRigUnit_MathRBFInterpolateQuatWorkData& WorkData, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_MathRBFInterpolate_h_275_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatQuat_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FMathRBFInterpolateQuatQuat_Target>& Targets, \
		FQuat& Output, \
		const FQuat& Input, \
		const ERBFQuatDistanceType DistanceFunction, \
		const ERBFKernelType SmoothingFunction, \
		const float SmoothingAngle, \
		const bool bNormalizeOutput, \
		const FVector& TwistAxis, \
		FRigUnit_MathRBFInterpolateQuatWorkData& WorkData, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		FRigVMFixedArray<FMathRBFInterpolateQuatQuat_Target> Targets((FMathRBFInterpolateQuatQuat_Target*)RigVMMemoryHandles[0].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[1].GetData())); \
		FQuat& Output = *(FQuat*)RigVMMemoryHandles[2].GetData(); \
		const FQuat& Input = *(FQuat*)RigVMMemoryHandles[3].GetData(); \
		ERBFQuatDistanceType DistanceFunction = (ERBFQuatDistanceType)*(uint8*)RigVMMemoryHandles[4].GetData(); \
		ERBFKernelType SmoothingFunction = (ERBFKernelType)*(uint8*)RigVMMemoryHandles[5].GetData(); \
		const float SmoothingAngle = *(float*)RigVMMemoryHandles[6].GetData(); \
		const bool bNormalizeOutput = *(bool*)RigVMMemoryHandles[7].GetData(); \
		const FVector& TwistAxis = *(FVector*)RigVMMemoryHandles[8].GetData(); \
		FRigVMDynamicArray<FRigUnit_MathRBFInterpolateQuatWorkData> WorkData_9_Array(*((FRigVMByteArray*)RigVMMemoryHandles[9].GetData(0, false))); \
		WorkData_9_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FRigUnit_MathRBFInterpolateQuatWorkData& WorkData = WorkData_9_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Targets, \
			Output, \
			Input, \
			DistanceFunction, \
			SmoothingFunction, \
			SmoothingAngle, \
			bNormalizeOutput, \
			TwistAxis, \
			WorkData, \
			Context \
		); \
	} \
	typedef FRigUnit_MathRBFInterpolateQuatBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_MathRBFInterpolateQuatQuat>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_MathRBFInterpolate_h_262_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMathRBFInterpolateQuatQuat_Target_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FMathRBFInterpolateQuatQuat_Target>();


#define FRigUnit_MathRBFInterpolateQuatColor_Execute() \
	void FRigUnit_MathRBFInterpolateQuatColor::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FMathRBFInterpolateQuatColor_Target>& Targets, \
		FLinearColor& Output, \
		const FQuat& Input, \
		const ERBFQuatDistanceType DistanceFunction, \
		const ERBFKernelType SmoothingFunction, \
		const float SmoothingAngle, \
		const bool bNormalizeOutput, \
		const FVector& TwistAxis, \
		FRigUnit_MathRBFInterpolateQuatWorkData& WorkData, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_MathRBFInterpolate_h_246_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatColor_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FMathRBFInterpolateQuatColor_Target>& Targets, \
		FLinearColor& Output, \
		const FQuat& Input, \
		const ERBFQuatDistanceType DistanceFunction, \
		const ERBFKernelType SmoothingFunction, \
		const float SmoothingAngle, \
		const bool bNormalizeOutput, \
		const FVector& TwistAxis, \
		FRigUnit_MathRBFInterpolateQuatWorkData& WorkData, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		FRigVMFixedArray<FMathRBFInterpolateQuatColor_Target> Targets((FMathRBFInterpolateQuatColor_Target*)RigVMMemoryHandles[0].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[1].GetData())); \
		FLinearColor& Output = *(FLinearColor*)RigVMMemoryHandles[2].GetData(); \
		const FQuat& Input = *(FQuat*)RigVMMemoryHandles[3].GetData(); \
		ERBFQuatDistanceType DistanceFunction = (ERBFQuatDistanceType)*(uint8*)RigVMMemoryHandles[4].GetData(); \
		ERBFKernelType SmoothingFunction = (ERBFKernelType)*(uint8*)RigVMMemoryHandles[5].GetData(); \
		const float SmoothingAngle = *(float*)RigVMMemoryHandles[6].GetData(); \
		const bool bNormalizeOutput = *(bool*)RigVMMemoryHandles[7].GetData(); \
		const FVector& TwistAxis = *(FVector*)RigVMMemoryHandles[8].GetData(); \
		FRigVMDynamicArray<FRigUnit_MathRBFInterpolateQuatWorkData> WorkData_9_Array(*((FRigVMByteArray*)RigVMMemoryHandles[9].GetData(0, false))); \
		WorkData_9_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FRigUnit_MathRBFInterpolateQuatWorkData& WorkData = WorkData_9_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Targets, \
			Output, \
			Input, \
			DistanceFunction, \
			SmoothingFunction, \
			SmoothingAngle, \
			bNormalizeOutput, \
			TwistAxis, \
			WorkData, \
			Context \
		); \
	} \
	typedef FRigUnit_MathRBFInterpolateQuatBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_MathRBFInterpolateQuatColor>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_MathRBFInterpolate_h_233_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMathRBFInterpolateQuatColor_Target_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FMathRBFInterpolateQuatColor_Target>();


#define FRigUnit_MathRBFInterpolateQuatVector_Execute() \
	void FRigUnit_MathRBFInterpolateQuatVector::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FMathRBFInterpolateQuatVector_Target>& Targets, \
		FVector& Output, \
		const FQuat& Input, \
		const ERBFQuatDistanceType DistanceFunction, \
		const ERBFKernelType SmoothingFunction, \
		const float SmoothingAngle, \
		const bool bNormalizeOutput, \
		const FVector& TwistAxis, \
		FRigUnit_MathRBFInterpolateQuatWorkData& WorkData, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_MathRBFInterpolate_h_217_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatVector_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FMathRBFInterpolateQuatVector_Target>& Targets, \
		FVector& Output, \
		const FQuat& Input, \
		const ERBFQuatDistanceType DistanceFunction, \
		const ERBFKernelType SmoothingFunction, \
		const float SmoothingAngle, \
		const bool bNormalizeOutput, \
		const FVector& TwistAxis, \
		FRigUnit_MathRBFInterpolateQuatWorkData& WorkData, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		FRigVMFixedArray<FMathRBFInterpolateQuatVector_Target> Targets((FMathRBFInterpolateQuatVector_Target*)RigVMMemoryHandles[0].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[1].GetData())); \
		FVector& Output = *(FVector*)RigVMMemoryHandles[2].GetData(); \
		const FQuat& Input = *(FQuat*)RigVMMemoryHandles[3].GetData(); \
		ERBFQuatDistanceType DistanceFunction = (ERBFQuatDistanceType)*(uint8*)RigVMMemoryHandles[4].GetData(); \
		ERBFKernelType SmoothingFunction = (ERBFKernelType)*(uint8*)RigVMMemoryHandles[5].GetData(); \
		const float SmoothingAngle = *(float*)RigVMMemoryHandles[6].GetData(); \
		const bool bNormalizeOutput = *(bool*)RigVMMemoryHandles[7].GetData(); \
		const FVector& TwistAxis = *(FVector*)RigVMMemoryHandles[8].GetData(); \
		FRigVMDynamicArray<FRigUnit_MathRBFInterpolateQuatWorkData> WorkData_9_Array(*((FRigVMByteArray*)RigVMMemoryHandles[9].GetData(0, false))); \
		WorkData_9_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FRigUnit_MathRBFInterpolateQuatWorkData& WorkData = WorkData_9_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Targets, \
			Output, \
			Input, \
			DistanceFunction, \
			SmoothingFunction, \
			SmoothingAngle, \
			bNormalizeOutput, \
			TwistAxis, \
			WorkData, \
			Context \
		); \
	} \
	typedef FRigUnit_MathRBFInterpolateQuatBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_MathRBFInterpolateQuatVector>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_MathRBFInterpolate_h_204_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMathRBFInterpolateQuatVector_Target_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FMathRBFInterpolateQuatVector_Target>();


#define FRigUnit_MathRBFInterpolateQuatFloat_Execute() \
	void FRigUnit_MathRBFInterpolateQuatFloat::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FMathRBFInterpolateQuatFloat_Target>& Targets, \
		float& Output, \
		const FQuat& Input, \
		const ERBFQuatDistanceType DistanceFunction, \
		const ERBFKernelType SmoothingFunction, \
		const float SmoothingAngle, \
		const bool bNormalizeOutput, \
		const FVector& TwistAxis, \
		FRigUnit_MathRBFInterpolateQuatWorkData& WorkData, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_MathRBFInterpolate_h_188_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatFloat_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FMathRBFInterpolateQuatFloat_Target>& Targets, \
		float& Output, \
		const FQuat& Input, \
		const ERBFQuatDistanceType DistanceFunction, \
		const ERBFKernelType SmoothingFunction, \
		const float SmoothingAngle, \
		const bool bNormalizeOutput, \
		const FVector& TwistAxis, \
		FRigUnit_MathRBFInterpolateQuatWorkData& WorkData, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		FRigVMFixedArray<FMathRBFInterpolateQuatFloat_Target> Targets((FMathRBFInterpolateQuatFloat_Target*)RigVMMemoryHandles[0].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[1].GetData())); \
		float& Output = *(float*)RigVMMemoryHandles[2].GetData(); \
		const FQuat& Input = *(FQuat*)RigVMMemoryHandles[3].GetData(); \
		ERBFQuatDistanceType DistanceFunction = (ERBFQuatDistanceType)*(uint8*)RigVMMemoryHandles[4].GetData(); \
		ERBFKernelType SmoothingFunction = (ERBFKernelType)*(uint8*)RigVMMemoryHandles[5].GetData(); \
		const float SmoothingAngle = *(float*)RigVMMemoryHandles[6].GetData(); \
		const bool bNormalizeOutput = *(bool*)RigVMMemoryHandles[7].GetData(); \
		const FVector& TwistAxis = *(FVector*)RigVMMemoryHandles[8].GetData(); \
		FRigVMDynamicArray<FRigUnit_MathRBFInterpolateQuatWorkData> WorkData_9_Array(*((FRigVMByteArray*)RigVMMemoryHandles[9].GetData(0, false))); \
		WorkData_9_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FRigUnit_MathRBFInterpolateQuatWorkData& WorkData = WorkData_9_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Targets, \
			Output, \
			Input, \
			DistanceFunction, \
			SmoothingFunction, \
			SmoothingAngle, \
			bNormalizeOutput, \
			TwistAxis, \
			WorkData, \
			Context \
		); \
	} \
	typedef FRigUnit_MathRBFInterpolateQuatBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_MathRBFInterpolateQuatFloat>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_MathRBFInterpolate_h_175_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMathRBFInterpolateQuatFloat_Target_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FMathRBFInterpolateQuatFloat_Target>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_MathRBFInterpolate_h_131_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorBase_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	typedef FRigUnit_MathRBFInterpolateBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_MathRBFInterpolateVectorBase>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_MathRBFInterpolate_h_86_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatBase_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	typedef FRigUnit_MathRBFInterpolateBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_MathRBFInterpolateQuatBase>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_MathRBFInterpolate_h_79_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateBase_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	typedef FRigUnit_MathBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_MathRBFInterpolateBase>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_MathRBFInterpolate_h_66_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateVectorWorkData_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_MathRBFInterpolateVectorWorkData>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_MathRBFInterpolate_h_52_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_MathRBFInterpolateQuatWorkData_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_MathRBFInterpolateQuatWorkData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_MathRBFInterpolate_h


#define FOREACH_ENUM_ERBFVECTORDISTANCETYPE(op) \
	op(ERBFVectorDistanceType::Euclidean) \
	op(ERBFVectorDistanceType::Manhattan) \
	op(ERBFVectorDistanceType::ArcLength) 

enum class ERBFVectorDistanceType : uint8;
template<> CONTROLRIG_API UEnum* StaticEnum<ERBFVectorDistanceType>();

#define FOREACH_ENUM_ERBFQUATDISTANCETYPE(op) \
	op(ERBFQuatDistanceType::Euclidean) \
	op(ERBFQuatDistanceType::ArcLength) \
	op(ERBFQuatDistanceType::SwingAngle) \
	op(ERBFQuatDistanceType::TwistAngle) 

enum class ERBFQuatDistanceType : uint8;
template<> CONTROLRIG_API UEnum* StaticEnum<ERBFQuatDistanceType>();

#define FOREACH_ENUM_ERBFKERNELTYPE(op) \
	op(ERBFKernelType::Gaussian) \
	op(ERBFKernelType::Exponential) \
	op(ERBFKernelType::Linear) \
	op(ERBFKernelType::Cubic) \
	op(ERBFKernelType::Quintic) 

enum class ERBFKernelType : uint8;
template<> CONTROLRIG_API UEnum* StaticEnum<ERBFKernelType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
