// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRigEditor/Private/Graph/NodeSpawners/ControlRigUnitNodeSpawner.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeControlRigUnitNodeSpawner() {}
// Cross Module References
	CONTROLRIGEDITOR_API UClass* Z_Construct_UClass_UControlRigUnitNodeSpawner_NoRegister();
	CONTROLRIGEDITOR_API UClass* Z_Construct_UClass_UControlRigUnitNodeSpawner();
	BLUEPRINTGRAPH_API UClass* Z_Construct_UClass_UBlueprintNodeSpawner();
	UPackage* Z_Construct_UPackage__Script_ControlRigEditor();
	COREUOBJECT_API UClass* Z_Construct_UClass_UScriptStruct();
// End Cross Module References
	void UControlRigUnitNodeSpawner::StaticRegisterNativesUControlRigUnitNodeSpawner()
	{
	}
	UClass* Z_Construct_UClass_UControlRigUnitNodeSpawner_NoRegister()
	{
		return UControlRigUnitNodeSpawner::StaticClass();
	}
	struct Z_Construct_UClass_UControlRigUnitNodeSpawner_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StructTemplate_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StructTemplate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UControlRigUnitNodeSpawner_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintNodeSpawner,
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRigEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigUnitNodeSpawner_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Graph/NodeSpawners/ControlRigUnitNodeSpawner.h" },
		{ "ModuleRelativePath", "Private/Graph/NodeSpawners/ControlRigUnitNodeSpawner.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigUnitNodeSpawner_Statics::NewProp_StructTemplate_MetaData[] = {
		{ "Comment", "/** The unit type we will spawn */" },
		{ "ModuleRelativePath", "Private/Graph/NodeSpawners/ControlRigUnitNodeSpawner.h" },
		{ "ToolTip", "The unit type we will spawn" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UControlRigUnitNodeSpawner_Statics::NewProp_StructTemplate = { "StructTemplate", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigUnitNodeSpawner, StructTemplate), Z_Construct_UClass_UScriptStruct, METADATA_PARAMS(Z_Construct_UClass_UControlRigUnitNodeSpawner_Statics::NewProp_StructTemplate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigUnitNodeSpawner_Statics::NewProp_StructTemplate_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UControlRigUnitNodeSpawner_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigUnitNodeSpawner_Statics::NewProp_StructTemplate,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UControlRigUnitNodeSpawner_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UControlRigUnitNodeSpawner>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UControlRigUnitNodeSpawner_Statics::ClassParams = {
		&UControlRigUnitNodeSpawner::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UControlRigUnitNodeSpawner_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigUnitNodeSpawner_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UControlRigUnitNodeSpawner_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigUnitNodeSpawner_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UControlRigUnitNodeSpawner()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UControlRigUnitNodeSpawner_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UControlRigUnitNodeSpawner, 740778562);
	template<> CONTROLRIGEDITOR_API UClass* StaticClass<UControlRigUnitNodeSpawner>()
	{
		return UControlRigUnitNodeSpawner::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UControlRigUnitNodeSpawner(Z_Construct_UClass_UControlRigUnitNodeSpawner, &UControlRigUnitNodeSpawner::StaticClass, TEXT("/Script/ControlRigEditor"), TEXT("UControlRigUnitNodeSpawner"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UControlRigUnitNodeSpawner);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
