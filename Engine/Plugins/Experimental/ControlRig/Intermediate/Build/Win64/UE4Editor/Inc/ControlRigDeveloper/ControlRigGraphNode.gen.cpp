// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRigDeveloper/Public/Graph/ControlRigGraphNode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeControlRigGraphNode() {}
// Cross Module References
	CONTROLRIGDEVELOPER_API UClass* Z_Construct_UClass_UControlRigGraphNode_NoRegister();
	CONTROLRIGDEVELOPER_API UClass* Z_Construct_UClass_UControlRigGraphNode();
	ENGINE_API UClass* Z_Construct_UClass_UEdGraphNode();
	UPackage* Z_Construct_UPackage__Script_ControlRigDeveloper();
	RIGVMDEVELOPER_API UClass* Z_Construct_UClass_URigVMNode_NoRegister();
	RIGVMDEVELOPER_API UClass* Z_Construct_UClass_URigVMPin_NoRegister();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FEdGraphPinType();
// End Cross Module References
	void UControlRigGraphNode::StaticRegisterNativesUControlRigGraphNode()
	{
	}
	UClass* Z_Construct_UClass_UControlRigGraphNode_NoRegister()
	{
		return UControlRigGraphNode::StaticClass();
	}
	struct Z_Construct_UClass_UControlRigGraphNode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ModelNodePath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ModelNodePath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedModelNode_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CachedModelNode;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CachedModelPins_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_CachedModelPins_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedModelPins_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_CachedModelPins;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PropertyName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_PropertyName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StructPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_StructPath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PinType_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PinType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParameterType_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ParameterType;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ExpandedPins_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExpandedPins_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ExpandedPins;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UControlRigGraphNode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEdGraphNode,
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRigDeveloper,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigGraphNode_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Base class for animation ControlRig-related nodes */" },
		{ "IncludePath", "Graph/ControlRigGraphNode.h" },
		{ "ModuleRelativePath", "Public/Graph/ControlRigGraphNode.h" },
		{ "ToolTip", "Base class for animation ControlRig-related nodes" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_ModelNodePath_MetaData[] = {
		{ "ModuleRelativePath", "Public/Graph/ControlRigGraphNode.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_ModelNodePath = { "ModelNodePath", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigGraphNode, ModelNodePath), METADATA_PARAMS(Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_ModelNodePath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_ModelNodePath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_CachedModelNode_MetaData[] = {
		{ "ModuleRelativePath", "Public/Graph/ControlRigGraphNode.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_CachedModelNode = { "CachedModelNode", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigGraphNode, CachedModelNode), Z_Construct_UClass_URigVMNode_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_CachedModelNode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_CachedModelNode_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_CachedModelPins_ValueProp = { "CachedModelPins", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_URigVMPin_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_CachedModelPins_Key_KeyProp = { "CachedModelPins_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_CachedModelPins_MetaData[] = {
		{ "ModuleRelativePath", "Public/Graph/ControlRigGraphNode.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_CachedModelPins = { "CachedModelPins", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigGraphNode, CachedModelPins), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_CachedModelPins_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_CachedModelPins_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_PropertyName_MetaData[] = {
		{ "Comment", "/** The property we represent. For template nodes this represents the struct/property type name. */" },
		{ "ModuleRelativePath", "Public/Graph/ControlRigGraphNode.h" },
		{ "ToolTip", "The property we represent. For template nodes this represents the struct/property type name." },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_PropertyName = { "PropertyName", nullptr, (EPropertyFlags)0x0040000020000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigGraphNode, PropertyName_DEPRECATED), METADATA_PARAMS(Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_PropertyName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_PropertyName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_StructPath_MetaData[] = {
		{ "ModuleRelativePath", "Public/Graph/ControlRigGraphNode.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_StructPath = { "StructPath", nullptr, (EPropertyFlags)0x0040000020000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigGraphNode, StructPath_DEPRECATED), METADATA_PARAMS(Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_StructPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_StructPath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_PinType_MetaData[] = {
		{ "Comment", "/** Pin Type for property */" },
		{ "ModuleRelativePath", "Public/Graph/ControlRigGraphNode.h" },
		{ "ToolTip", "Pin Type for property" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_PinType = { "PinType", nullptr, (EPropertyFlags)0x0040000020000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigGraphNode, PinType_DEPRECATED), Z_Construct_UScriptStruct_FEdGraphPinType, METADATA_PARAMS(Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_PinType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_PinType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_ParameterType_MetaData[] = {
		{ "Comment", "/** The type of parameter */" },
		{ "ModuleRelativePath", "Public/Graph/ControlRigGraphNode.h" },
		{ "ToolTip", "The type of parameter" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_ParameterType = { "ParameterType", nullptr, (EPropertyFlags)0x0040000020000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigGraphNode, ParameterType_DEPRECATED), METADATA_PARAMS(Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_ParameterType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_ParameterType_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_ExpandedPins_Inner = { "ExpandedPins", nullptr, (EPropertyFlags)0x0000000020000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_ExpandedPins_MetaData[] = {
		{ "Comment", "/** Expanded pins */" },
		{ "ModuleRelativePath", "Public/Graph/ControlRigGraphNode.h" },
		{ "ToolTip", "Expanded pins" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_ExpandedPins = { "ExpandedPins", nullptr, (EPropertyFlags)0x0040000020000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigGraphNode, ExpandedPins_DEPRECATED), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_ExpandedPins_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_ExpandedPins_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UControlRigGraphNode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_ModelNodePath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_CachedModelNode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_CachedModelPins_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_CachedModelPins_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_CachedModelPins,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_PropertyName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_StructPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_PinType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_ParameterType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_ExpandedPins_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigGraphNode_Statics::NewProp_ExpandedPins,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UControlRigGraphNode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UControlRigGraphNode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UControlRigGraphNode_Statics::ClassParams = {
		&UControlRigGraphNode::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UControlRigGraphNode_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigGraphNode_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UControlRigGraphNode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigGraphNode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UControlRigGraphNode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UControlRigGraphNode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UControlRigGraphNode, 3654639754);
	template<> CONTROLRIGDEVELOPER_API UClass* StaticClass<UControlRigGraphNode>()
	{
		return UControlRigGraphNode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UControlRigGraphNode(Z_Construct_UClass_UControlRigGraphNode, &UControlRigGraphNode::StaticClass, TEXT("/Script/ControlRigDeveloper"), TEXT("UControlRigGraphNode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UControlRigGraphNode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
