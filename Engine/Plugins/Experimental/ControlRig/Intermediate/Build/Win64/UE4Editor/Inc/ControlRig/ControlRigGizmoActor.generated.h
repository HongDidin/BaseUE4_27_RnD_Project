// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FTransform;
#ifdef CONTROLRIG_ControlRigGizmoActor_generated_h
#error "ControlRigGizmoActor.generated.h already included, missing '#pragma once' in ControlRigGizmoActor.h"
#endif
#define CONTROLRIG_ControlRigGizmoActor_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoActor_h_17_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FGizmoActorCreationParam_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FGizmoActorCreationParam>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoActor_h_51_SPARSE_DATA
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoActor_h_51_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetGlobalTransform); \
	DECLARE_FUNCTION(execSetGlobalTransform); \
	DECLARE_FUNCTION(execIsHovered); \
	DECLARE_FUNCTION(execSetHovered); \
	DECLARE_FUNCTION(execSetSelectable); \
	DECLARE_FUNCTION(execIsSelectedInEditor); \
	DECLARE_FUNCTION(execSetSelected); \
	DECLARE_FUNCTION(execIsEnabled); \
	DECLARE_FUNCTION(execSetEnabled);


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoActor_h_51_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetGlobalTransform); \
	DECLARE_FUNCTION(execSetGlobalTransform); \
	DECLARE_FUNCTION(execIsHovered); \
	DECLARE_FUNCTION(execSetHovered); \
	DECLARE_FUNCTION(execSetSelectable); \
	DECLARE_FUNCTION(execIsSelectedInEditor); \
	DECLARE_FUNCTION(execSetSelected); \
	DECLARE_FUNCTION(execIsEnabled); \
	DECLARE_FUNCTION(execSetEnabled);


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoActor_h_51_EVENT_PARMS \
	struct ControlRigGizmoActor_eventOnEnabledChanged_Parms \
	{ \
		bool bIsEnabled; \
	}; \
	struct ControlRigGizmoActor_eventOnHoveredChanged_Parms \
	{ \
		bool bIsSelected; \
	}; \
	struct ControlRigGizmoActor_eventOnManipulatingChanged_Parms \
	{ \
		bool bIsManipulating; \
	}; \
	struct ControlRigGizmoActor_eventOnSelectionChanged_Parms \
	{ \
		bool bIsSelected; \
	}; \
	struct ControlRigGizmoActor_eventOnTransformChanged_Parms \
	{ \
		FTransform NewTransform; \
	};


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoActor_h_51_CALLBACK_WRAPPERS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoActor_h_51_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAControlRigGizmoActor(); \
	friend struct Z_Construct_UClass_AControlRigGizmoActor_Statics; \
public: \
	DECLARE_CLASS(AControlRigGizmoActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ControlRig"), NO_API) \
	DECLARE_SERIALIZER(AControlRigGizmoActor)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoActor_h_51_INCLASS \
private: \
	static void StaticRegisterNativesAControlRigGizmoActor(); \
	friend struct Z_Construct_UClass_AControlRigGizmoActor_Statics; \
public: \
	DECLARE_CLASS(AControlRigGizmoActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ControlRig"), NO_API) \
	DECLARE_SERIALIZER(AControlRigGizmoActor)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoActor_h_51_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AControlRigGizmoActor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AControlRigGizmoActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AControlRigGizmoActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AControlRigGizmoActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AControlRigGizmoActor(AControlRigGizmoActor&&); \
	NO_API AControlRigGizmoActor(const AControlRigGizmoActor&); \
public:


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoActor_h_51_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AControlRigGizmoActor(AControlRigGizmoActor&&); \
	NO_API AControlRigGizmoActor(const AControlRigGizmoActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AControlRigGizmoActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AControlRigGizmoActor); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AControlRigGizmoActor)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoActor_h_51_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoActor_h_48_PROLOG \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoActor_h_51_EVENT_PARMS


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoActor_h_51_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoActor_h_51_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoActor_h_51_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoActor_h_51_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoActor_h_51_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoActor_h_51_INCLASS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoActor_h_51_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoActor_h_51_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoActor_h_51_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoActor_h_51_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoActor_h_51_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoActor_h_51_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoActor_h_51_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoActor_h_51_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONTROLRIG_API UClass* StaticClass<class AControlRigGizmoActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
