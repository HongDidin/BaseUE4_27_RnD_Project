// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRigEditor/Private/Sequencer/ControlRigSequencerFilter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeControlRigSequencerFilter() {}
// Cross Module References
	CONTROLRIGEDITOR_API UClass* Z_Construct_UClass_UControlRigTrackFilter_NoRegister();
	CONTROLRIGEDITOR_API UClass* Z_Construct_UClass_UControlRigTrackFilter();
	SEQUENCER_API UClass* Z_Construct_UClass_USequencerTrackFilterExtension();
	UPackage* Z_Construct_UPackage__Script_ControlRigEditor();
// End Cross Module References
	void UControlRigTrackFilter::StaticRegisterNativesUControlRigTrackFilter()
	{
	}
	UClass* Z_Construct_UClass_UControlRigTrackFilter_NoRegister()
	{
		return UControlRigTrackFilter::StaticClass();
	}
	struct Z_Construct_UClass_UControlRigTrackFilter_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UControlRigTrackFilter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USequencerTrackFilterExtension,
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRigEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigTrackFilter_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Sequencer/ControlRigSequencerFilter.h" },
		{ "ModuleRelativePath", "Private/Sequencer/ControlRigSequencerFilter.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UControlRigTrackFilter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UControlRigTrackFilter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UControlRigTrackFilter_Statics::ClassParams = {
		&UControlRigTrackFilter::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UControlRigTrackFilter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigTrackFilter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UControlRigTrackFilter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UControlRigTrackFilter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UControlRigTrackFilter, 3784351777);
	template<> CONTROLRIGEDITOR_API UClass* StaticClass<UControlRigTrackFilter>()
	{
		return UControlRigTrackFilter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UControlRigTrackFilter(Z_Construct_UClass_UControlRigTrackFilter, &UControlRigTrackFilter::StaticClass, TEXT("/Script/ControlRigEditor"), TEXT("UControlRigTrackFilter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UControlRigTrackFilter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
