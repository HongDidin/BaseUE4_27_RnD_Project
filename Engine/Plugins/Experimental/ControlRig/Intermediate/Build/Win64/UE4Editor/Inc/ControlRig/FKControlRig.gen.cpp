// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Public/Rigs/FKControlRig.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFKControlRig() {}
// Cross Module References
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_EControlRigFKRigExecuteMode();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UClass* Z_Construct_UClass_UFKControlRig_NoRegister();
	CONTROLRIG_API UClass* Z_Construct_UClass_UFKControlRig();
	CONTROLRIG_API UClass* Z_Construct_UClass_UControlRig();
// End Cross Module References
	static UEnum* EControlRigFKRigExecuteMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ControlRig_EControlRigFKRigExecuteMode, Z_Construct_UPackage__Script_ControlRig(), TEXT("EControlRigFKRigExecuteMode"));
		}
		return Singleton;
	}
	template<> CONTROLRIG_API UEnum* StaticEnum<EControlRigFKRigExecuteMode>()
	{
		return EControlRigFKRigExecuteMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EControlRigFKRigExecuteMode(EControlRigFKRigExecuteMode_StaticEnum, TEXT("/Script/ControlRig"), TEXT("EControlRigFKRigExecuteMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ControlRig_EControlRigFKRigExecuteMode_Hash() { return 3209146268U; }
	UEnum* Z_Construct_UEnum_ControlRig_EControlRigFKRigExecuteMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EControlRigFKRigExecuteMode"), 0, Get_Z_Construct_UEnum_ControlRig_EControlRigFKRigExecuteMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EControlRigFKRigExecuteMode::Replace", (int64)EControlRigFKRigExecuteMode::Replace },
				{ "EControlRigFKRigExecuteMode::Additive", (int64)EControlRigFKRigExecuteMode::Additive },
				{ "EControlRigFKRigExecuteMode::Max", (int64)EControlRigFKRigExecuteMode::Max },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Additive.Comment", "/** Applies the authored pose as an additive layer */" },
				{ "Additive.Name", "EControlRigFKRigExecuteMode::Additive" },
				{ "Additive.ToolTip", "Applies the authored pose as an additive layer" },
				{ "Max.Comment", "/** MAX - invalid */" },
				{ "Max.Hidden", "" },
				{ "Max.Name", "EControlRigFKRigExecuteMode::Max" },
				{ "Max.ToolTip", "MAX - invalid" },
				{ "ModuleRelativePath", "Public/Rigs/FKControlRig.h" },
				{ "Replace.Comment", "/** Replaces the current pose */" },
				{ "Replace.Name", "EControlRigFKRigExecuteMode::Replace" },
				{ "Replace.ToolTip", "Replaces the current pose" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ControlRig,
				nullptr,
				"EControlRigFKRigExecuteMode",
				"EControlRigFKRigExecuteMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UFKControlRig::StaticRegisterNativesUFKControlRig()
	{
	}
	UClass* Z_Construct_UClass_UFKControlRig_NoRegister()
	{
		return UFKControlRig::StaticClass();
	}
	struct Z_Construct_UClass_UFKControlRig_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsControlActive_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IsControlActive_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_IsControlActive;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ApplyMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ApplyMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ApplyMode;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFKControlRig_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UControlRig,
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFKControlRig_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Rig that allows override editing per joint */" },
		{ "DisplayName", "FK Control Rig" },
		{ "IncludePath", "Rigs/FKControlRig.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Public/Rigs/FKControlRig.h" },
		{ "ToolTip", "Rig that allows override editing per joint" },
	};
#endif
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UFKControlRig_Statics::NewProp_IsControlActive_Inner = { "IsControlActive", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFKControlRig_Statics::NewProp_IsControlActive_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/FKControlRig.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UFKControlRig_Statics::NewProp_IsControlActive = { "IsControlActive", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFKControlRig, IsControlActive), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UFKControlRig_Statics::NewProp_IsControlActive_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFKControlRig_Statics::NewProp_IsControlActive_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UFKControlRig_Statics::NewProp_ApplyMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFKControlRig_Statics::NewProp_ApplyMode_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/FKControlRig.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UFKControlRig_Statics::NewProp_ApplyMode = { "ApplyMode", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFKControlRig, ApplyMode), Z_Construct_UEnum_ControlRig_EControlRigFKRigExecuteMode, METADATA_PARAMS(Z_Construct_UClass_UFKControlRig_Statics::NewProp_ApplyMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFKControlRig_Statics::NewProp_ApplyMode_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UFKControlRig_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFKControlRig_Statics::NewProp_IsControlActive_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFKControlRig_Statics::NewProp_IsControlActive,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFKControlRig_Statics::NewProp_ApplyMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFKControlRig_Statics::NewProp_ApplyMode,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFKControlRig_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFKControlRig>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFKControlRig_Statics::ClassParams = {
		&UFKControlRig::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UFKControlRig_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UFKControlRig_Statics::PropPointers),
		0,
		0x009010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFKControlRig_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFKControlRig_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFKControlRig()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFKControlRig_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFKControlRig, 3711723447);
	template<> CONTROLRIG_API UClass* StaticClass<UFKControlRig>()
	{
		return UFKControlRig::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFKControlRig(Z_Construct_UClass_UFKControlRig, &UFKControlRig::StaticClass, TEXT("/Script/ControlRig"), TEXT("UFKControlRig"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFKControlRig);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
