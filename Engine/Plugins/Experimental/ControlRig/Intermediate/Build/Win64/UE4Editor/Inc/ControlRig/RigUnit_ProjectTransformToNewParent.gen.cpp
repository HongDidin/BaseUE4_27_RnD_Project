// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Hierarchy/RigUnit_ProjectTransformToNewParent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_ProjectTransformToNewParent() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigElementKey();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FCachedRigElement();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_ProjectTransformToNewParent>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_ProjectTransformToNewParent cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_ProjectTransformToNewParent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_ProjectTransformToNewParent"), sizeof(FRigUnit_ProjectTransformToNewParent), Get_Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_ProjectTransformToNewParent::Execute"), &FRigUnit_ProjectTransformToNewParent::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_ProjectTransformToNewParent>()
{
	return FRigUnit_ProjectTransformToNewParent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_ProjectTransformToNewParent(FRigUnit_ProjectTransformToNewParent::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_ProjectTransformToNewParent"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ProjectTransformToNewParent
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ProjectTransformToNewParent()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_ProjectTransformToNewParent>(FName(TEXT("RigUnit_ProjectTransformToNewParent")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ProjectTransformToNewParent;
	struct Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Child_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Child;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bChildInitial_MetaData[];
#endif
		static void NewProp_bChildInitial_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bChildInitial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OldParent_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OldParent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOldParentInitial_MetaData[];
#endif
		static void NewProp_bOldParentInitial_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOldParentInitial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewParent_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NewParent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bNewParentInitial_MetaData[];
#endif
		static void NewProp_bNewParentInitial_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bNewParentInitial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Transform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Transform;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedChild_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CachedChild;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedOldParent_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CachedOldParent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedNewParent_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CachedNewParent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "/**\n * Projects a child from a previous to a new parent. The child doesn't\n * have to be parented that way in the hierarchy however.\n * For example: If you want the pelvis to move with the head (even though\n * the hierarchical relationship is the opposite) you can set the child\n * the pelvis (initial), set the old parent to be the head (initial)\n * and set the new parent to the head as well (not-initial == current).\n */" },
		{ "DisplayName", "Project to new Parent" },
		{ "DocumentationPolicy", "Strict" },
		{ "Keywords", "ProjectTransformToNewParent,Relative,Reparent,Offset" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_ProjectTransformToNewParent.h" },
		{ "ToolTip", "Projects a child from a previous to a new parent. The child doesn't\nhave to be parented that way in the hierarchy however.\nFor example: If you want the pelvis to move with the head (even though\nthe hierarchical relationship is the opposite) you can set the child\nthe pelvis (initial), set the old parent to be the head (initial)\nand set the new parent to the head as well (not-initial == current)." },
		{ "Varying", "" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_ProjectTransformToNewParent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_Child_MetaData[] = {
		{ "Comment", "/**\n\x09 * The element to project between parents\n\x09 */" },
		{ "ExpandByDefault", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_ProjectTransformToNewParent.h" },
		{ "ToolTip", "The element to project between parents" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_Child = { "Child", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ProjectTransformToNewParent, Child), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_Child_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_Child_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_bChildInitial_MetaData[] = {
		{ "Comment", "/**\n\x09 * If set to true the child will be retrieved in its initial transform\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_ProjectTransformToNewParent.h" },
		{ "ToolTip", "If set to true the child will be retrieved in its initial transform" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_bChildInitial_SetBit(void* Obj)
	{
		((FRigUnit_ProjectTransformToNewParent*)Obj)->bChildInitial = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_bChildInitial = { "bChildInitial", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_ProjectTransformToNewParent), &Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_bChildInitial_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_bChildInitial_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_bChildInitial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_OldParent_MetaData[] = {
		{ "Comment", "/**\n\x09 * The original parent of the child.\n\x09 * Can be an actual parent in the hierarchy or any other\n\x09 * item you want to use to compute to offset against.\n\x09 */" },
		{ "ExpandByDefault", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_ProjectTransformToNewParent.h" },
		{ "ToolTip", "The original parent of the child.\nCan be an actual parent in the hierarchy or any other\nitem you want to use to compute to offset against." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_OldParent = { "OldParent", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ProjectTransformToNewParent, OldParent), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_OldParent_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_OldParent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_bOldParentInitial_MetaData[] = {
		{ "Comment", "/**\n\x09 * If set to true the old parent will be retrieved in its initial transform\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_ProjectTransformToNewParent.h" },
		{ "ToolTip", "If set to true the old parent will be retrieved in its initial transform" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_bOldParentInitial_SetBit(void* Obj)
	{
		((FRigUnit_ProjectTransformToNewParent*)Obj)->bOldParentInitial = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_bOldParentInitial = { "bOldParentInitial", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_ProjectTransformToNewParent), &Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_bOldParentInitial_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_bOldParentInitial_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_bOldParentInitial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_NewParent_MetaData[] = {
		{ "Comment", "/**\n\x09 * The new parent of the child.\n\x09 */" },
		{ "ExpandByDefault", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_ProjectTransformToNewParent.h" },
		{ "ToolTip", "The new parent of the child." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_NewParent = { "NewParent", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ProjectTransformToNewParent, NewParent), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_NewParent_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_NewParent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_bNewParentInitial_MetaData[] = {
		{ "Comment", "/**\n\x09 * If set to true the new parent will be retrieved in its initial transform\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_ProjectTransformToNewParent.h" },
		{ "ToolTip", "If set to true the new parent will be retrieved in its initial transform" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_bNewParentInitial_SetBit(void* Obj)
	{
		((FRigUnit_ProjectTransformToNewParent*)Obj)->bNewParentInitial = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_bNewParentInitial = { "bNewParentInitial", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_ProjectTransformToNewParent), &Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_bNewParentInitial_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_bNewParentInitial_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_bNewParentInitial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_Transform_MetaData[] = {
		{ "Comment", "// The resulting transform\n" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_ProjectTransformToNewParent.h" },
		{ "Output", "" },
		{ "ToolTip", "The resulting transform" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_Transform = { "Transform", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ProjectTransformToNewParent, Transform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_Transform_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_Transform_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_CachedChild_MetaData[] = {
		{ "Comment", "// Used to cache the internally used child\n" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_ProjectTransformToNewParent.h" },
		{ "ToolTip", "Used to cache the internally used child" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_CachedChild = { "CachedChild", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ProjectTransformToNewParent, CachedChild), Z_Construct_UScriptStruct_FCachedRigElement, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_CachedChild_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_CachedChild_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_CachedOldParent_MetaData[] = {
		{ "Comment", "// Used to cache the internally used old parent\n" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_ProjectTransformToNewParent.h" },
		{ "ToolTip", "Used to cache the internally used old parent" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_CachedOldParent = { "CachedOldParent", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ProjectTransformToNewParent, CachedOldParent), Z_Construct_UScriptStruct_FCachedRigElement, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_CachedOldParent_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_CachedOldParent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_CachedNewParent_MetaData[] = {
		{ "Comment", "// Used to cache the internally used new parent\n" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_ProjectTransformToNewParent.h" },
		{ "ToolTip", "Used to cache the internally used new parent" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_CachedNewParent = { "CachedNewParent", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ProjectTransformToNewParent, CachedNewParent), Z_Construct_UScriptStruct_FCachedRigElement, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_CachedNewParent_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_CachedNewParent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_Child,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_bChildInitial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_OldParent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_bOldParentInitial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_NewParent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_bNewParentInitial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_Transform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_CachedChild,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_CachedOldParent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::NewProp_CachedNewParent,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_ProjectTransformToNewParent",
		sizeof(FRigUnit_ProjectTransformToNewParent),
		alignof(FRigUnit_ProjectTransformToNewParent),
		Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_ProjectTransformToNewParent"), sizeof(FRigUnit_ProjectTransformToNewParent), Get_Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ProjectTransformToNewParent_Hash() { return 3873420328U; }

void FRigUnit_ProjectTransformToNewParent::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Child,
		bChildInitial,
		OldParent,
		bOldParentInitial,
		NewParent,
		bNewParentInitial,
		Transform,
		CachedChild,
		CachedOldParent,
		CachedNewParent,
		Context
	);
}

PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
