// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Execution/RigUnit_ForLoop.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_ForLoop() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ForLoopCount();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnitMutable();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FControlRigExecuteContext();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_ForLoopCount>() == std::is_polymorphic<FRigUnitMutable>(), "USTRUCT FRigUnit_ForLoopCount cannot be polymorphic unless super FRigUnitMutable is polymorphic");

class UScriptStruct* FRigUnit_ForLoopCount::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_ForLoopCount, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_ForLoopCount"), sizeof(FRigUnit_ForLoopCount), Get_Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_ForLoopCount::Execute"), &FRigUnit_ForLoopCount::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_ForLoopCount>()
{
	return FRigUnit_ForLoopCount::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_ForLoopCount(FRigUnit_ForLoopCount::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_ForLoopCount"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ForLoopCount
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ForLoopCount()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_ForLoopCount>(FName(TEXT("RigUnit_ForLoopCount")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ForLoopCount;
	struct Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Count_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Count;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Ratio_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Ratio;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Continue_MetaData[];
#endif
		static void NewProp_Continue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Continue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Completed_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Completed;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Execution" },
		{ "Comment", "/**\n * Given a count, execute iteratively until the count is up\n */" },
		{ "DisplayName", "For Loop" },
		{ "Keywords", "Iterate" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_ForLoop.h" },
		{ "NodeColor", "0.1 0.1 0.1" },
		{ "TitleColor", "1 0 0" },
		{ "ToolTip", "Given a count, execute iteratively until the count is up" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_ForLoopCount>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Statics::NewProp_Count_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_ForLoop.h" },
		{ "Singleton", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Statics::NewProp_Count = { "Count", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ForLoopCount, Count), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Statics::NewProp_Count_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Statics::NewProp_Count_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Statics::NewProp_Index_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_ForLoop.h" },
		{ "Output", "" },
		{ "Singleton", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Statics::NewProp_Index = { "Index", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ForLoopCount, Index), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Statics::NewProp_Index_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Statics::NewProp_Index_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Statics::NewProp_Ratio_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_ForLoop.h" },
		{ "Output", "" },
		{ "Singleton", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Statics::NewProp_Ratio = { "Ratio", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ForLoopCount, Ratio), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Statics::NewProp_Ratio_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Statics::NewProp_Ratio_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Statics::NewProp_Continue_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_ForLoop.h" },
		{ "Singleton", "" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Statics::NewProp_Continue_SetBit(void* Obj)
	{
		((FRigUnit_ForLoopCount*)Obj)->Continue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Statics::NewProp_Continue = { "Continue", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_ForLoopCount), &Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Statics::NewProp_Continue_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Statics::NewProp_Continue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Statics::NewProp_Continue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Statics::NewProp_Completed_MetaData[] = {
		{ "Category", "ForLoop" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_ForLoop.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Statics::NewProp_Completed = { "Completed", nullptr, (EPropertyFlags)0x0010000000002001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ForLoopCount, Completed), Z_Construct_UScriptStruct_FControlRigExecuteContext, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Statics::NewProp_Completed_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Statics::NewProp_Completed_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Statics::NewProp_Count,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Statics::NewProp_Ratio,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Statics::NewProp_Continue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Statics::NewProp_Completed,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnitMutable,
		&NewStructOps,
		"RigUnit_ForLoopCount",
		sizeof(FRigUnit_ForLoopCount),
		alignof(FRigUnit_ForLoopCount),
		Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ForLoopCount()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_ForLoopCount"), sizeof(FRigUnit_ForLoopCount), Get_Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ForLoopCount_Hash() { return 506130756U; }

void FRigUnit_ForLoopCount::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Count,
		Index,
		Ratio,
		Continue,
		Completed,
		ExecuteContext,
		Context
	);
}

PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
