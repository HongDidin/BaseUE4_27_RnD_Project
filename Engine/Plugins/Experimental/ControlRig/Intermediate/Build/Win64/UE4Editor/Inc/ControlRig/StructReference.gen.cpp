// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Public/StructReference.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeStructReference() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FStructReference();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
// End Cross Module References
class UScriptStruct* FStructReference::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FStructReference_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FStructReference, Z_Construct_UPackage__Script_ControlRig(), TEXT("StructReference"), sizeof(FStructReference), Get_Z_Construct_UScriptStruct_FStructReference_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FStructReference>()
{
	return FStructReference::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FStructReference(FStructReference::StaticStruct, TEXT("/Script/ControlRig"), TEXT("StructReference"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFStructReference
{
	FScriptStruct_ControlRig_StaticRegisterNativesFStructReference()
	{
		UScriptStruct::DeferCppStructOps<FStructReference>(FName(TEXT("StructReference")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFStructReference;
	struct Z_Construct_UScriptStruct_FStructReference_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FStructReference_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** \n * Base class used to reference a struct in the graph. Don't use this directly, only derived classes.\n * Use the IMPLEMENT_STRUCT_REFERENCE macro to create new struct reference types easily.\n */" },
		{ "ModuleRelativePath", "Public/StructReference.h" },
		{ "ToolTip", "Base class used to reference a struct in the graph. Don't use this directly, only derived classes.\nUse the IMPLEMENT_STRUCT_REFERENCE macro to create new struct reference types easily." },
	};
#endif
	void* Z_Construct_UScriptStruct_FStructReference_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FStructReference>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FStructReference_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"StructReference",
		sizeof(FStructReference),
		alignof(FStructReference),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FStructReference_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStructReference_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FStructReference()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FStructReference_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("StructReference"), sizeof(FStructReference), Get_Z_Construct_UScriptStruct_FStructReference_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FStructReference_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FStructReference_Hash() { return 3372395793U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
