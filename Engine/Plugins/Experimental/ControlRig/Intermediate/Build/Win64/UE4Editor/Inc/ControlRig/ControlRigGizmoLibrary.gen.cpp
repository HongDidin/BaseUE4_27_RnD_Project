// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Public/ControlRigGizmoLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeControlRigGizmoLibrary() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FControlRigGizmoDefinition();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMesh_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	CONTROLRIG_API UClass* Z_Construct_UClass_UControlRigGizmoLibrary_NoRegister();
	CONTROLRIG_API UClass* Z_Construct_UClass_UControlRigGizmoLibrary();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	ENGINE_API UClass* Z_Construct_UClass_UMaterial_NoRegister();
// End Cross Module References
class UScriptStruct* FControlRigGizmoDefinition::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FControlRigGizmoDefinition_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FControlRigGizmoDefinition, Z_Construct_UPackage__Script_ControlRig(), TEXT("ControlRigGizmoDefinition"), sizeof(FControlRigGizmoDefinition), Get_Z_Construct_UScriptStruct_FControlRigGizmoDefinition_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FControlRigGizmoDefinition>()
{
	return FControlRigGizmoDefinition::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FControlRigGizmoDefinition(FControlRigGizmoDefinition::StaticStruct, TEXT("/Script/ControlRig"), TEXT("ControlRigGizmoDefinition"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFControlRigGizmoDefinition
{
	FScriptStruct_ControlRig_StaticRegisterNativesFControlRigGizmoDefinition()
	{
		UScriptStruct::DeferCppStructOps<FControlRigGizmoDefinition>(FName(TEXT("ControlRigGizmoDefinition")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFControlRigGizmoDefinition;
	struct Z_Construct_UScriptStruct_FControlRigGizmoDefinition_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GizmoName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_GizmoName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StaticMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_StaticMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Transform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Transform;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FControlRigGizmoDefinition_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "DisplayName", "Gizmo" },
		{ "ModuleRelativePath", "Public/ControlRigGizmoLibrary.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FControlRigGizmoDefinition_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FControlRigGizmoDefinition>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FControlRigGizmoDefinition_Statics::NewProp_GizmoName_MetaData[] = {
		{ "Category", "Gizmo" },
		{ "ModuleRelativePath", "Public/ControlRigGizmoLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FControlRigGizmoDefinition_Statics::NewProp_GizmoName = { "GizmoName", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FControlRigGizmoDefinition, GizmoName), METADATA_PARAMS(Z_Construct_UScriptStruct_FControlRigGizmoDefinition_Statics::NewProp_GizmoName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FControlRigGizmoDefinition_Statics::NewProp_GizmoName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FControlRigGizmoDefinition_Statics::NewProp_StaticMesh_MetaData[] = {
		{ "Category", "Gizmo" },
		{ "ModuleRelativePath", "Public/ControlRigGizmoLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UScriptStruct_FControlRigGizmoDefinition_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FControlRigGizmoDefinition, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FControlRigGizmoDefinition_Statics::NewProp_StaticMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FControlRigGizmoDefinition_Statics::NewProp_StaticMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FControlRigGizmoDefinition_Statics::NewProp_Transform_MetaData[] = {
		{ "Category", "Gizmo" },
		{ "ModuleRelativePath", "Public/ControlRigGizmoLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FControlRigGizmoDefinition_Statics::NewProp_Transform = { "Transform", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FControlRigGizmoDefinition, Transform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FControlRigGizmoDefinition_Statics::NewProp_Transform_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FControlRigGizmoDefinition_Statics::NewProp_Transform_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FControlRigGizmoDefinition_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FControlRigGizmoDefinition_Statics::NewProp_GizmoName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FControlRigGizmoDefinition_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FControlRigGizmoDefinition_Statics::NewProp_Transform,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FControlRigGizmoDefinition_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"ControlRigGizmoDefinition",
		sizeof(FControlRigGizmoDefinition),
		alignof(FControlRigGizmoDefinition),
		Z_Construct_UScriptStruct_FControlRigGizmoDefinition_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FControlRigGizmoDefinition_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FControlRigGizmoDefinition_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FControlRigGizmoDefinition_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FControlRigGizmoDefinition()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FControlRigGizmoDefinition_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ControlRigGizmoDefinition"), sizeof(FControlRigGizmoDefinition), Get_Z_Construct_UScriptStruct_FControlRigGizmoDefinition_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FControlRigGizmoDefinition_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FControlRigGizmoDefinition_Hash() { return 1081121708U; }
	void UControlRigGizmoLibrary::StaticRegisterNativesUControlRigGizmoLibrary()
	{
	}
	UClass* Z_Construct_UClass_UControlRigGizmoLibrary_NoRegister()
	{
		return UControlRigGizmoLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UControlRigGizmoLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultGizmo_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DefaultGizmo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_DefaultMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaterialColorParameter_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_MaterialColorParameter;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Gizmos_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Gizmos_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Gizmos;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UControlRigGizmoLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigGizmoLibrary_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "DisplayName", "GizmoLibrary" },
		{ "IncludePath", "ControlRigGizmoLibrary.h" },
		{ "ModuleRelativePath", "Public/ControlRigGizmoLibrary.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigGizmoLibrary_Statics::NewProp_DefaultGizmo_MetaData[] = {
		{ "Category", "GizmoLibrary" },
		{ "ModuleRelativePath", "Public/ControlRigGizmoLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UControlRigGizmoLibrary_Statics::NewProp_DefaultGizmo = { "DefaultGizmo", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigGizmoLibrary, DefaultGizmo), Z_Construct_UScriptStruct_FControlRigGizmoDefinition, METADATA_PARAMS(Z_Construct_UClass_UControlRigGizmoLibrary_Statics::NewProp_DefaultGizmo_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigGizmoLibrary_Statics::NewProp_DefaultGizmo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigGizmoLibrary_Statics::NewProp_DefaultMaterial_MetaData[] = {
		{ "Category", "GizmoLibrary" },
		{ "ModuleRelativePath", "Public/ControlRigGizmoLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UControlRigGizmoLibrary_Statics::NewProp_DefaultMaterial = { "DefaultMaterial", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigGizmoLibrary, DefaultMaterial), Z_Construct_UClass_UMaterial_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UControlRigGizmoLibrary_Statics::NewProp_DefaultMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigGizmoLibrary_Statics::NewProp_DefaultMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigGizmoLibrary_Statics::NewProp_MaterialColorParameter_MetaData[] = {
		{ "Category", "GizmoLibrary" },
		{ "ModuleRelativePath", "Public/ControlRigGizmoLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UControlRigGizmoLibrary_Statics::NewProp_MaterialColorParameter = { "MaterialColorParameter", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigGizmoLibrary, MaterialColorParameter), METADATA_PARAMS(Z_Construct_UClass_UControlRigGizmoLibrary_Statics::NewProp_MaterialColorParameter_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigGizmoLibrary_Statics::NewProp_MaterialColorParameter_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UControlRigGizmoLibrary_Statics::NewProp_Gizmos_Inner = { "Gizmos", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FControlRigGizmoDefinition, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigGizmoLibrary_Statics::NewProp_Gizmos_MetaData[] = {
		{ "Category", "GizmoLibrary" },
		{ "ModuleRelativePath", "Public/ControlRigGizmoLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UControlRigGizmoLibrary_Statics::NewProp_Gizmos = { "Gizmos", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigGizmoLibrary, Gizmos), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UControlRigGizmoLibrary_Statics::NewProp_Gizmos_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigGizmoLibrary_Statics::NewProp_Gizmos_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UControlRigGizmoLibrary_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigGizmoLibrary_Statics::NewProp_DefaultGizmo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigGizmoLibrary_Statics::NewProp_DefaultMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigGizmoLibrary_Statics::NewProp_MaterialColorParameter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigGizmoLibrary_Statics::NewProp_Gizmos_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigGizmoLibrary_Statics::NewProp_Gizmos,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UControlRigGizmoLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UControlRigGizmoLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UControlRigGizmoLibrary_Statics::ClassParams = {
		&UControlRigGizmoLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UControlRigGizmoLibrary_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigGizmoLibrary_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UControlRigGizmoLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigGizmoLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UControlRigGizmoLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UControlRigGizmoLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UControlRigGizmoLibrary, 1627126036);
	template<> CONTROLRIG_API UClass* StaticClass<UControlRigGizmoLibrary>()
	{
		return UControlRigGizmoLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UControlRigGizmoLibrary(Z_Construct_UClass_UControlRigGizmoLibrary, &UControlRigGizmoLibrary::StaticClass, TEXT("/Script/ControlRig"), TEXT("UControlRigGizmoLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UControlRigGizmoLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
