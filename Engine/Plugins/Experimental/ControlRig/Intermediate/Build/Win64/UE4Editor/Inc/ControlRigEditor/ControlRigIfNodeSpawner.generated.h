// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIGEDITOR_ControlRigIfNodeSpawner_generated_h
#error "ControlRigIfNodeSpawner.generated.h already included, missing '#pragma once' in ControlRigIfNodeSpawner.h"
#endif
#define CONTROLRIGEDITOR_ControlRigIfNodeSpawner_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigIfNodeSpawner_h_22_SPARSE_DATA
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigIfNodeSpawner_h_22_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigIfNodeSpawner_h_22_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigIfNodeSpawner_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUControlRigIfNodeSpawner(); \
	friend struct Z_Construct_UClass_UControlRigIfNodeSpawner_Statics; \
public: \
	DECLARE_CLASS(UControlRigIfNodeSpawner, UBlueprintNodeSpawner, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ControlRigEditor"), NO_API) \
	DECLARE_SERIALIZER(UControlRigIfNodeSpawner)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigIfNodeSpawner_h_22_INCLASS \
private: \
	static void StaticRegisterNativesUControlRigIfNodeSpawner(); \
	friend struct Z_Construct_UClass_UControlRigIfNodeSpawner_Statics; \
public: \
	DECLARE_CLASS(UControlRigIfNodeSpawner, UBlueprintNodeSpawner, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ControlRigEditor"), NO_API) \
	DECLARE_SERIALIZER(UControlRigIfNodeSpawner)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigIfNodeSpawner_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UControlRigIfNodeSpawner(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigIfNodeSpawner) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigIfNodeSpawner); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigIfNodeSpawner); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigIfNodeSpawner(UControlRigIfNodeSpawner&&); \
	NO_API UControlRigIfNodeSpawner(const UControlRigIfNodeSpawner&); \
public:


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigIfNodeSpawner_h_22_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UControlRigIfNodeSpawner(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigIfNodeSpawner(UControlRigIfNodeSpawner&&); \
	NO_API UControlRigIfNodeSpawner(const UControlRigIfNodeSpawner&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigIfNodeSpawner); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigIfNodeSpawner); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigIfNodeSpawner)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigIfNodeSpawner_h_22_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigIfNodeSpawner_h_19_PROLOG
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigIfNodeSpawner_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigIfNodeSpawner_h_22_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigIfNodeSpawner_h_22_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigIfNodeSpawner_h_22_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigIfNodeSpawner_h_22_INCLASS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigIfNodeSpawner_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigIfNodeSpawner_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigIfNodeSpawner_h_22_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigIfNodeSpawner_h_22_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigIfNodeSpawner_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigIfNodeSpawner_h_22_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigIfNodeSpawner_h_22_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONTROLRIGEDITOR_API UClass* StaticClass<class UControlRigIfNodeSpawner>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigIfNodeSpawner_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
