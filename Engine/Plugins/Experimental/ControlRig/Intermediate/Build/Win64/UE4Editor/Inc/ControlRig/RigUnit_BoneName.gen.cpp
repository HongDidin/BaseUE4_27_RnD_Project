// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Hierarchy/RigUnit_BoneName.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_BoneName() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ControlName();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_SpaceName();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_BoneName();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_Item();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigElementKey();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_ControlName>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_ControlName cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_ControlName::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ControlName_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_ControlName, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_ControlName"), sizeof(FRigUnit_ControlName), Get_Z_Construct_UScriptStruct_FRigUnit_ControlName_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_ControlName::Execute"), &FRigUnit_ControlName::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_ControlName>()
{
	return FRigUnit_ControlName::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_ControlName(FRigUnit_ControlName::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_ControlName"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ControlName
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ControlName()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_ControlName>(FName(TEXT("RigUnit_ControlName")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ControlName;
	struct Z_Construct_UScriptStruct_FRigUnit_ControlName_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Control_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Control;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ControlName_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "/**\n * ControlName is used to represent a Control name in the graph\n */" },
		{ "Deprecated", "4.25" },
		{ "DisplayName", "Control Name" },
		{ "DocumentationPolicy", "Strict" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_BoneName.h" },
		{ "ToolTip", "ControlName is used to represent a Control name in the graph" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_ControlName_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_ControlName>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ControlName_Statics::NewProp_Control_MetaData[] = {
		{ "Comment", "/**\n\x09 * The name of the Control\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_BoneName.h" },
		{ "Output", "" },
		{ "ToolTip", "The name of the Control" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_ControlName_Statics::NewProp_Control = { "Control", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ControlName, Control), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ControlName_Statics::NewProp_Control_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ControlName_Statics::NewProp_Control_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_ControlName_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ControlName_Statics::NewProp_Control,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_ControlName_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_ControlName",
		sizeof(FRigUnit_ControlName),
		alignof(FRigUnit_ControlName),
		Z_Construct_UScriptStruct_FRigUnit_ControlName_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ControlName_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ControlName_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ControlName_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ControlName()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ControlName_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_ControlName"), sizeof(FRigUnit_ControlName), Get_Z_Construct_UScriptStruct_FRigUnit_ControlName_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_ControlName_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ControlName_Hash() { return 2756011878U; }

void FRigUnit_ControlName::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Control,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_SpaceName>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_SpaceName cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_SpaceName::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_SpaceName_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_SpaceName, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_SpaceName"), sizeof(FRigUnit_SpaceName), Get_Z_Construct_UScriptStruct_FRigUnit_SpaceName_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_SpaceName::Execute"), &FRigUnit_SpaceName::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_SpaceName>()
{
	return FRigUnit_SpaceName::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_SpaceName(FRigUnit_SpaceName::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_SpaceName"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_SpaceName
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_SpaceName()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_SpaceName>(FName(TEXT("RigUnit_SpaceName")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_SpaceName;
	struct Z_Construct_UScriptStruct_FRigUnit_SpaceName_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Space_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Space;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_SpaceName_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "/**\n * SpaceName is used to represent a Space name in the graph\n */" },
		{ "Deprecated", "4.25" },
		{ "DisplayName", "Space Name" },
		{ "DocumentationPolicy", "Strict" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_BoneName.h" },
		{ "ToolTip", "SpaceName is used to represent a Space name in the graph" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_SpaceName_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_SpaceName>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_SpaceName_Statics::NewProp_Space_MetaData[] = {
		{ "Comment", "/**\n\x09 * The name of the Space\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_BoneName.h" },
		{ "Output", "" },
		{ "ToolTip", "The name of the Space" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_SpaceName_Statics::NewProp_Space = { "Space", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_SpaceName, Space), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_SpaceName_Statics::NewProp_Space_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_SpaceName_Statics::NewProp_Space_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_SpaceName_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_SpaceName_Statics::NewProp_Space,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_SpaceName_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_SpaceName",
		sizeof(FRigUnit_SpaceName),
		alignof(FRigUnit_SpaceName),
		Z_Construct_UScriptStruct_FRigUnit_SpaceName_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_SpaceName_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_SpaceName_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_SpaceName_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_SpaceName()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_SpaceName_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_SpaceName"), sizeof(FRigUnit_SpaceName), Get_Z_Construct_UScriptStruct_FRigUnit_SpaceName_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_SpaceName_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_SpaceName_Hash() { return 465658802U; }

void FRigUnit_SpaceName::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Space,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_BoneName>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_BoneName cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_BoneName::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_BoneName_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_BoneName, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_BoneName"), sizeof(FRigUnit_BoneName), Get_Z_Construct_UScriptStruct_FRigUnit_BoneName_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_BoneName::Execute"), &FRigUnit_BoneName::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_BoneName>()
{
	return FRigUnit_BoneName::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_BoneName(FRigUnit_BoneName::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_BoneName"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_BoneName
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_BoneName()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_BoneName>(FName(TEXT("RigUnit_BoneName")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_BoneName;
	struct Z_Construct_UScriptStruct_FRigUnit_BoneName_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Bone_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Bone;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_BoneName_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "/**\n * BoneName is used to represent a bone name in the graph\n */" },
		{ "Constant", "" },
		{ "Deprecated", "4.25" },
		{ "DisplayName", "Bone Name" },
		{ "DocumentationPolicy", "Strict" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_BoneName.h" },
		{ "ToolTip", "BoneName is used to represent a bone name in the graph" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_BoneName_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_BoneName>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_BoneName_Statics::NewProp_Bone_MetaData[] = {
		{ "Comment", "/**\n\x09 * The name of the Bone\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_BoneName.h" },
		{ "Output", "" },
		{ "ToolTip", "The name of the Bone" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_BoneName_Statics::NewProp_Bone = { "Bone", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_BoneName, Bone), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_BoneName_Statics::NewProp_Bone_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_BoneName_Statics::NewProp_Bone_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_BoneName_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_BoneName_Statics::NewProp_Bone,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_BoneName_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_BoneName",
		sizeof(FRigUnit_BoneName),
		alignof(FRigUnit_BoneName),
		Z_Construct_UScriptStruct_FRigUnit_BoneName_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_BoneName_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_BoneName_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_BoneName_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_BoneName()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_BoneName_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_BoneName"), sizeof(FRigUnit_BoneName), Get_Z_Construct_UScriptStruct_FRigUnit_BoneName_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_BoneName_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_BoneName_Hash() { return 2830434607U; }

void FRigUnit_BoneName::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Bone,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_Item>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_Item cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_Item::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Item_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_Item, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_Item"), sizeof(FRigUnit_Item), Get_Z_Construct_UScriptStruct_FRigUnit_Item_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_Item::Execute"), &FRigUnit_Item::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_Item>()
{
	return FRigUnit_Item::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_Item(FRigUnit_Item::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_Item"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_Item
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_Item()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_Item>(FName(TEXT("RigUnit_Item")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_Item;
	struct Z_Construct_UScriptStruct_FRigUnit_Item_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Item_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Item;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_Item_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "/**\n * The Item node is used to share a specific item across the graph\n */" },
		{ "Constant", "" },
		{ "DisplayName", "Item" },
		{ "DocumentationPolicy", "Strict" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_BoneName.h" },
		{ "ToolTip", "The Item node is used to share a specific item across the graph" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_Item_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_Item>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_Item_Statics::NewProp_Item_MetaData[] = {
		{ "Comment", "/**\n\x09 * The item\n\x09 */" },
		{ "ExpandByDefault", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_BoneName.h" },
		{ "Output", "" },
		{ "ToolTip", "The item" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_Item_Statics::NewProp_Item = { "Item", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_Item, Item), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_Item_Statics::NewProp_Item_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_Item_Statics::NewProp_Item_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_Item_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_Item_Statics::NewProp_Item,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_Item_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_Item",
		sizeof(FRigUnit_Item),
		alignof(FRigUnit_Item),
		Z_Construct_UScriptStruct_FRigUnit_Item_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_Item_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_Item_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_Item_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_Item()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Item_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_Item"), sizeof(FRigUnit_Item), Get_Z_Construct_UScriptStruct_FRigUnit_Item_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_Item_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Item_Hash() { return 362343221U; }

void FRigUnit_Item::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Item,
		Context
	);
}

PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
