// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigUnit_DeltaFromPrevious_generated_h
#error "RigUnit_DeltaFromPrevious.generated.h already included, missing '#pragma once' in RigUnit_DeltaFromPrevious.h"
#endif
#define CONTROLRIG_RigUnit_DeltaFromPrevious_generated_h


#define FRigUnit_DeltaFromPreviousTransform_Execute() \
	void FRigUnit_DeltaFromPreviousTransform::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FTransform& Value, \
		FTransform& Delta, \
		FTransform& PreviousValue, \
		FTransform& Cache, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Simulation_RigUnit_DeltaFromPrevious_h_101_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousTransform_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FTransform& Value, \
		FTransform& Delta, \
		FTransform& PreviousValue, \
		FTransform& Cache, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FTransform& Value = *(FTransform*)RigVMMemoryHandles[0].GetData(); \
		FTransform& Delta = *(FTransform*)RigVMMemoryHandles[1].GetData(); \
		FTransform& PreviousValue = *(FTransform*)RigVMMemoryHandles[2].GetData(); \
		FRigVMDynamicArray<FTransform> Cache_3_Array(*((FRigVMByteArray*)RigVMMemoryHandles[3].GetData(0, false))); \
		Cache_3_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FTransform& Cache = Cache_3_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Value, \
			Delta, \
			PreviousValue, \
			Cache, \
			Context \
		); \
	} \
	typedef FRigUnit_SimBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_DeltaFromPreviousTransform>();


#define FRigUnit_DeltaFromPreviousQuat_Execute() \
	void FRigUnit_DeltaFromPreviousQuat::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FQuat& Value, \
		FQuat& Delta, \
		FQuat& PreviousValue, \
		FQuat& Cache, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Simulation_RigUnit_DeltaFromPrevious_h_72_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousQuat_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FQuat& Value, \
		FQuat& Delta, \
		FQuat& PreviousValue, \
		FQuat& Cache, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FQuat& Value = *(FQuat*)RigVMMemoryHandles[0].GetData(); \
		FQuat& Delta = *(FQuat*)RigVMMemoryHandles[1].GetData(); \
		FQuat& PreviousValue = *(FQuat*)RigVMMemoryHandles[2].GetData(); \
		FRigVMDynamicArray<FQuat> Cache_3_Array(*((FRigVMByteArray*)RigVMMemoryHandles[3].GetData(0, false))); \
		Cache_3_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FQuat& Cache = Cache_3_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Value, \
			Delta, \
			PreviousValue, \
			Cache, \
			Context \
		); \
	} \
	typedef FRigUnit_SimBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_DeltaFromPreviousQuat>();


#define FRigUnit_DeltaFromPreviousVector_Execute() \
	void FRigUnit_DeltaFromPreviousVector::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FVector& Value, \
		FVector& Delta, \
		FVector& PreviousValue, \
		FVector& Cache, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Simulation_RigUnit_DeltaFromPrevious_h_43_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousVector_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FVector& Value, \
		FVector& Delta, \
		FVector& PreviousValue, \
		FVector& Cache, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FVector& Value = *(FVector*)RigVMMemoryHandles[0].GetData(); \
		FVector& Delta = *(FVector*)RigVMMemoryHandles[1].GetData(); \
		FVector& PreviousValue = *(FVector*)RigVMMemoryHandles[2].GetData(); \
		FRigVMDynamicArray<FVector> Cache_3_Array(*((FRigVMByteArray*)RigVMMemoryHandles[3].GetData(0, false))); \
		Cache_3_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FVector& Cache = Cache_3_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Value, \
			Delta, \
			PreviousValue, \
			Cache, \
			Context \
		); \
	} \
	typedef FRigUnit_SimBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_DeltaFromPreviousVector>();


#define FRigUnit_DeltaFromPreviousFloat_Execute() \
	void FRigUnit_DeltaFromPreviousFloat::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const float Value, \
		float& Delta, \
		float& PreviousValue, \
		float& Cache, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Simulation_RigUnit_DeltaFromPrevious_h_14_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousFloat_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const float Value, \
		float& Delta, \
		float& PreviousValue, \
		float& Cache, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const float Value = *(float*)RigVMMemoryHandles[0].GetData(); \
		float& Delta = *(float*)RigVMMemoryHandles[1].GetData(); \
		float& PreviousValue = *(float*)RigVMMemoryHandles[2].GetData(); \
		FRigVMDynamicArray<float> Cache_3_Array(*((FRigVMByteArray*)RigVMMemoryHandles[3].GetData(0, false))); \
		Cache_3_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		float& Cache = Cache_3_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Value, \
			Delta, \
			PreviousValue, \
			Cache, \
			Context \
		); \
	} \
	typedef FRigUnit_SimBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_DeltaFromPreviousFloat>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Simulation_RigUnit_DeltaFromPrevious_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
