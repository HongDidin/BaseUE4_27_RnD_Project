// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Debug/RigUnit_DebugBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_DebugBase() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_DebugBaseMutable();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnitMutable();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_DebugBase();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_DebugBaseMutable>() == std::is_polymorphic<FRigUnitMutable>(), "USTRUCT FRigUnit_DebugBaseMutable cannot be polymorphic unless super FRigUnitMutable is polymorphic");

class UScriptStruct* FRigUnit_DebugBaseMutable::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DebugBaseMutable_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_DebugBaseMutable, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_DebugBaseMutable"), sizeof(FRigUnit_DebugBaseMutable), Get_Z_Construct_UScriptStruct_FRigUnit_DebugBaseMutable_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_DebugBaseMutable>()
{
	return FRigUnit_DebugBaseMutable::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_DebugBaseMutable(FRigUnit_DebugBaseMutable::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_DebugBaseMutable"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DebugBaseMutable
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DebugBaseMutable()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_DebugBaseMutable>(FName(TEXT("RigUnit_DebugBaseMutable")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DebugBaseMutable;
	struct Z_Construct_UScriptStruct_FRigUnit_DebugBaseMutable_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DebugBaseMutable_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "Category", "Debug" },
		{ "ModuleRelativePath", "Private/Units/Debug/RigUnit_DebugBase.h" },
		{ "NodeColor", "0.25 0.25 0.05" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_DebugBaseMutable_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_DebugBaseMutable>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_DebugBaseMutable_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnitMutable,
		&NewStructOps,
		"RigUnit_DebugBaseMutable",
		sizeof(FRigUnit_DebugBaseMutable),
		alignof(FRigUnit_DebugBaseMutable),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DebugBaseMutable_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DebugBaseMutable_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_DebugBaseMutable()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DebugBaseMutable_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_DebugBaseMutable"), sizeof(FRigUnit_DebugBaseMutable), Get_Z_Construct_UScriptStruct_FRigUnit_DebugBaseMutable_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_DebugBaseMutable_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DebugBaseMutable_Hash() { return 10506425U; }

static_assert(std::is_polymorphic<FRigUnit_DebugBase>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_DebugBase cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_DebugBase::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DebugBase_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_DebugBase, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_DebugBase"), sizeof(FRigUnit_DebugBase), Get_Z_Construct_UScriptStruct_FRigUnit_DebugBase_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_DebugBase>()
{
	return FRigUnit_DebugBase::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_DebugBase(FRigUnit_DebugBase::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_DebugBase"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DebugBase
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DebugBase()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_DebugBase>(FName(TEXT("RigUnit_DebugBase")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DebugBase;
	struct Z_Construct_UScriptStruct_FRigUnit_DebugBase_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DebugBase_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "Category", "Debug" },
		{ "ModuleRelativePath", "Private/Units/Debug/RigUnit_DebugBase.h" },
		{ "NodeColor", "0.25 0.25 0.05" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_DebugBase_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_DebugBase>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_DebugBase_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_DebugBase",
		sizeof(FRigUnit_DebugBase),
		alignof(FRigUnit_DebugBase),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DebugBase_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DebugBase_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_DebugBase()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DebugBase_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_DebugBase"), sizeof(FRigUnit_DebugBase), Get_Z_Construct_UScriptStruct_FRigUnit_DebugBase_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_DebugBase_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DebugBase_Hash() { return 872314985U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
