// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Public/Rigs/AnimationHierarchy.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAnimationHierarchy() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FAnimationHierarchy();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	ANIMATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FNodeHierarchyWithUserData();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FConstraintNodeData();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	ANIMATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConstraintOffset();
	ANIMATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FTransformConstraint();
// End Cross Module References

static_assert(std::is_polymorphic<FAnimationHierarchy>() == std::is_polymorphic<FNodeHierarchyWithUserData>(), "USTRUCT FAnimationHierarchy cannot be polymorphic unless super FNodeHierarchyWithUserData is polymorphic");

class UScriptStruct* FAnimationHierarchy::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FAnimationHierarchy_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FAnimationHierarchy, Z_Construct_UPackage__Script_ControlRig(), TEXT("AnimationHierarchy"), sizeof(FAnimationHierarchy), Get_Z_Construct_UScriptStruct_FAnimationHierarchy_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FAnimationHierarchy>()
{
	return FAnimationHierarchy::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FAnimationHierarchy(FAnimationHierarchy::StaticStruct, TEXT("/Script/ControlRig"), TEXT("AnimationHierarchy"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFAnimationHierarchy
{
	FScriptStruct_ControlRig_StaticRegisterNativesFAnimationHierarchy()
	{
		UScriptStruct::DeferCppStructOps<FAnimationHierarchy>(FName(TEXT("AnimationHierarchy")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFAnimationHierarchy;
	struct Z_Construct_UScriptStruct_FAnimationHierarchy_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_UserData_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UserData_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_UserData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAnimationHierarchy_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Rigs/AnimationHierarchy.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FAnimationHierarchy_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FAnimationHierarchy>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FAnimationHierarchy_Statics::NewProp_UserData_Inner = { "UserData", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FConstraintNodeData, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAnimationHierarchy_Statics::NewProp_UserData_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/AnimationHierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FAnimationHierarchy_Statics::NewProp_UserData = { "UserData", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAnimationHierarchy, UserData), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FAnimationHierarchy_Statics::NewProp_UserData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimationHierarchy_Statics::NewProp_UserData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FAnimationHierarchy_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimationHierarchy_Statics::NewProp_UserData_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimationHierarchy_Statics::NewProp_UserData,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FAnimationHierarchy_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FNodeHierarchyWithUserData,
		&NewStructOps,
		"AnimationHierarchy",
		sizeof(FAnimationHierarchy),
		alignof(FAnimationHierarchy),
		Z_Construct_UScriptStruct_FAnimationHierarchy_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimationHierarchy_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FAnimationHierarchy_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimationHierarchy_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FAnimationHierarchy()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FAnimationHierarchy_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("AnimationHierarchy"), sizeof(FAnimationHierarchy), Get_Z_Construct_UScriptStruct_FAnimationHierarchy_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FAnimationHierarchy_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FAnimationHierarchy_Hash() { return 1397149712U; }
class UScriptStruct* FConstraintNodeData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FConstraintNodeData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConstraintNodeData, Z_Construct_UPackage__Script_ControlRig(), TEXT("ConstraintNodeData"), sizeof(FConstraintNodeData), Get_Z_Construct_UScriptStruct_FConstraintNodeData_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FConstraintNodeData>()
{
	return FConstraintNodeData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConstraintNodeData(FConstraintNodeData::StaticStruct, TEXT("/Script/ControlRig"), TEXT("ConstraintNodeData"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFConstraintNodeData
{
	FScriptStruct_ControlRig_StaticRegisterNativesFConstraintNodeData()
	{
		UScriptStruct::DeferCppStructOps<FConstraintNodeData>(FName(TEXT("ConstraintNodeData")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFConstraintNodeData;
	struct Z_Construct_UScriptStruct_FConstraintNodeData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RelativeParent_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RelativeParent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConstraintOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ConstraintOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LinkedNode_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_LinkedNode;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Constraints_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Constraints_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Constraints;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConstraintNodeData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Rigs/AnimationHierarchy.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConstraintNodeData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConstraintNodeData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConstraintNodeData_Statics::NewProp_RelativeParent_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/AnimationHierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConstraintNodeData_Statics::NewProp_RelativeParent = { "RelativeParent", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConstraintNodeData, RelativeParent), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FConstraintNodeData_Statics::NewProp_RelativeParent_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConstraintNodeData_Statics::NewProp_RelativeParent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConstraintNodeData_Statics::NewProp_ConstraintOffset_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/AnimationHierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConstraintNodeData_Statics::NewProp_ConstraintOffset = { "ConstraintOffset", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConstraintNodeData, ConstraintOffset), Z_Construct_UScriptStruct_FConstraintOffset, METADATA_PARAMS(Z_Construct_UScriptStruct_FConstraintNodeData_Statics::NewProp_ConstraintOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConstraintNodeData_Statics::NewProp_ConstraintOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConstraintNodeData_Statics::NewProp_LinkedNode_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/AnimationHierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FConstraintNodeData_Statics::NewProp_LinkedNode = { "LinkedNode", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConstraintNodeData, LinkedNode), METADATA_PARAMS(Z_Construct_UScriptStruct_FConstraintNodeData_Statics::NewProp_LinkedNode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConstraintNodeData_Statics::NewProp_LinkedNode_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConstraintNodeData_Statics::NewProp_Constraints_Inner = { "Constraints", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FTransformConstraint, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConstraintNodeData_Statics::NewProp_Constraints_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/AnimationHierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FConstraintNodeData_Statics::NewProp_Constraints = { "Constraints", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConstraintNodeData, Constraints), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FConstraintNodeData_Statics::NewProp_Constraints_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConstraintNodeData_Statics::NewProp_Constraints_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConstraintNodeData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConstraintNodeData_Statics::NewProp_RelativeParent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConstraintNodeData_Statics::NewProp_ConstraintOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConstraintNodeData_Statics::NewProp_LinkedNode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConstraintNodeData_Statics::NewProp_Constraints_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConstraintNodeData_Statics::NewProp_Constraints,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConstraintNodeData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"ConstraintNodeData",
		sizeof(FConstraintNodeData),
		alignof(FConstraintNodeData),
		Z_Construct_UScriptStruct_FConstraintNodeData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConstraintNodeData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConstraintNodeData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConstraintNodeData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConstraintNodeData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConstraintNodeData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConstraintNodeData"), sizeof(FConstraintNodeData), Get_Z_Construct_UScriptStruct_FConstraintNodeData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConstraintNodeData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConstraintNodeData_Hash() { return 1669415426U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
