// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Public/ControlRigDefines.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeControlRigDefines() {}
// Cross Module References
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_ERigExecutionType();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_EBoneGetterSetterMode();
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_ETransformGetterType();
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_EControlRigClampSpatialMode();
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_ETransformSpaceMode();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FControlRigExecuteContext();
	RIGVM_API UScriptStruct* Z_Construct_UScriptStruct_FRigVMExecuteContext();
// End Cross Module References
	static UEnum* ERigExecutionType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ControlRig_ERigExecutionType, Z_Construct_UPackage__Script_ControlRig(), TEXT("ERigExecutionType"));
		}
		return Singleton;
	}
	template<> CONTROLRIG_API UEnum* StaticEnum<ERigExecutionType>()
	{
		return ERigExecutionType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ERigExecutionType(ERigExecutionType_StaticEnum, TEXT("/Script/ControlRig"), TEXT("ERigExecutionType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ControlRig_ERigExecutionType_Hash() { return 3216799265U; }
	UEnum* Z_Construct_UEnum_ControlRig_ERigExecutionType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ERigExecutionType"), 0, Get_Z_Construct_UEnum_ControlRig_ERigExecutionType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ERigExecutionType::Runtime", (int64)ERigExecutionType::Runtime },
				{ "ERigExecutionType::Editing", (int64)ERigExecutionType::Editing },
				{ "ERigExecutionType::Max", (int64)ERigExecutionType::Max },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "// thought of mixing this with execution on\n// the problem is execution on is transient state, and \n// this execution type is something to be set per rig\n" },
				{ "Editing.Name", "ERigExecutionType::Editing" },
				{ "Max.Comment", "// editing time\n" },
				{ "Max.Hidden", "" },
				{ "Max.Name", "ERigExecutionType::Max" },
				{ "Max.ToolTip", "editing time" },
				{ "ModuleRelativePath", "Public/ControlRigDefines.h" },
				{ "Runtime.Name", "ERigExecutionType::Runtime" },
				{ "ToolTip", "thought of mixing this with execution on\nthe problem is execution on is transient state, and\nthis execution type is something to be set per rig" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ControlRig,
				nullptr,
				"ERigExecutionType",
				"ERigExecutionType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EBoneGetterSetterMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ControlRig_EBoneGetterSetterMode, Z_Construct_UPackage__Script_ControlRig(), TEXT("EBoneGetterSetterMode"));
		}
		return Singleton;
	}
	template<> CONTROLRIG_API UEnum* StaticEnum<EBoneGetterSetterMode>()
	{
		return EBoneGetterSetterMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EBoneGetterSetterMode(EBoneGetterSetterMode_StaticEnum, TEXT("/Script/ControlRig"), TEXT("EBoneGetterSetterMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ControlRig_EBoneGetterSetterMode_Hash() { return 1153146323U; }
	UEnum* Z_Construct_UEnum_ControlRig_EBoneGetterSetterMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EBoneGetterSetterMode"), 0, Get_Z_Construct_UEnum_ControlRig_EBoneGetterSetterMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EBoneGetterSetterMode::LocalSpace", (int64)EBoneGetterSetterMode::LocalSpace },
				{ "EBoneGetterSetterMode::GlobalSpace", (int64)EBoneGetterSetterMode::GlobalSpace },
				{ "EBoneGetterSetterMode::Max", (int64)EBoneGetterSetterMode::Max },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "GlobalSpace.Comment", "/** Apply in rig space*/" },
				{ "GlobalSpace.Name", "EBoneGetterSetterMode::GlobalSpace" },
				{ "GlobalSpace.ToolTip", "Apply in rig space" },
				{ "LocalSpace.Comment", "/** Apply in parent space */" },
				{ "LocalSpace.Name", "EBoneGetterSetterMode::LocalSpace" },
				{ "LocalSpace.ToolTip", "Apply in parent space" },
				{ "Max.Comment", "/** MAX - invalid */" },
				{ "Max.Hidden", "" },
				{ "Max.Name", "EBoneGetterSetterMode::Max" },
				{ "Max.ToolTip", "MAX - invalid" },
				{ "ModuleRelativePath", "Public/ControlRigDefines.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ControlRig,
				nullptr,
				"EBoneGetterSetterMode",
				"EBoneGetterSetterMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ETransformGetterType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ControlRig_ETransformGetterType, Z_Construct_UPackage__Script_ControlRig(), TEXT("ETransformGetterType"));
		}
		return Singleton;
	}
	template<> CONTROLRIG_API UEnum* StaticEnum<ETransformGetterType>()
	{
		return ETransformGetterType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETransformGetterType(ETransformGetterType_StaticEnum, TEXT("/Script/ControlRig"), TEXT("ETransformGetterType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ControlRig_ETransformGetterType_Hash() { return 476901980U; }
	UEnum* Z_Construct_UEnum_ControlRig_ETransformGetterType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETransformGetterType"), 0, Get_Z_Construct_UEnum_ControlRig_ETransformGetterType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETransformGetterType::Initial", (int64)ETransformGetterType::Initial },
				{ "ETransformGetterType::Current", (int64)ETransformGetterType::Current },
				{ "ETransformGetterType::Max", (int64)ETransformGetterType::Max },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Current.Name", "ETransformGetterType::Current" },
				{ "Initial.Name", "ETransformGetterType::Initial" },
				{ "Max.Hidden", "" },
				{ "Max.Name", "ETransformGetterType::Max" },
				{ "ModuleRelativePath", "Public/ControlRigDefines.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ControlRig,
				nullptr,
				"ETransformGetterType",
				"ETransformGetterType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EControlRigClampSpatialMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ControlRig_EControlRigClampSpatialMode, Z_Construct_UPackage__Script_ControlRig(), TEXT("EControlRigClampSpatialMode"));
		}
		return Singleton;
	}
	template<> CONTROLRIG_API UEnum* StaticEnum<EControlRigClampSpatialMode::Type>()
	{
		return EControlRigClampSpatialMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EControlRigClampSpatialMode(EControlRigClampSpatialMode_StaticEnum, TEXT("/Script/ControlRig"), TEXT("EControlRigClampSpatialMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ControlRig_EControlRigClampSpatialMode_Hash() { return 3774928207U; }
	UEnum* Z_Construct_UEnum_ControlRig_EControlRigClampSpatialMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EControlRigClampSpatialMode"), 0, Get_Z_Construct_UEnum_ControlRig_EControlRigClampSpatialMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EControlRigClampSpatialMode::Plane", (int64)EControlRigClampSpatialMode::Plane },
				{ "EControlRigClampSpatialMode::Cylinder", (int64)EControlRigClampSpatialMode::Cylinder },
				{ "EControlRigClampSpatialMode::Sphere", (int64)EControlRigClampSpatialMode::Sphere },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Cylinder.Name", "EControlRigClampSpatialMode::Cylinder" },
				{ "ModuleRelativePath", "Public/ControlRigDefines.h" },
				{ "Plane.Name", "EControlRigClampSpatialMode::Plane" },
				{ "Sphere.Name", "EControlRigClampSpatialMode::Sphere" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ControlRig,
				nullptr,
				"EControlRigClampSpatialMode",
				"EControlRigClampSpatialMode::Type",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::Namespaced,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ETransformSpaceMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ControlRig_ETransformSpaceMode, Z_Construct_UPackage__Script_ControlRig(), TEXT("ETransformSpaceMode"));
		}
		return Singleton;
	}
	template<> CONTROLRIG_API UEnum* StaticEnum<ETransformSpaceMode>()
	{
		return ETransformSpaceMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETransformSpaceMode(ETransformSpaceMode_StaticEnum, TEXT("/Script/ControlRig"), TEXT("ETransformSpaceMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ControlRig_ETransformSpaceMode_Hash() { return 3579310084U; }
	UEnum* Z_Construct_UEnum_ControlRig_ETransformSpaceMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETransformSpaceMode"), 0, Get_Z_Construct_UEnum_ControlRig_ETransformSpaceMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETransformSpaceMode::LocalSpace", (int64)ETransformSpaceMode::LocalSpace },
				{ "ETransformSpaceMode::GlobalSpace", (int64)ETransformSpaceMode::GlobalSpace },
				{ "ETransformSpaceMode::BaseSpace", (int64)ETransformSpaceMode::BaseSpace },
				{ "ETransformSpaceMode::BaseJoint", (int64)ETransformSpaceMode::BaseJoint },
				{ "ETransformSpaceMode::Max", (int64)ETransformSpaceMode::Max },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BaseJoint.Comment", "/** Apply in base bone */" },
				{ "BaseJoint.Name", "ETransformSpaceMode::BaseJoint" },
				{ "BaseJoint.ToolTip", "Apply in base bone" },
				{ "BaseSpace.Comment", "/** Apply in Base space */" },
				{ "BaseSpace.Name", "ETransformSpaceMode::BaseSpace" },
				{ "BaseSpace.ToolTip", "Apply in Base space" },
				{ "GlobalSpace.Comment", "/** Apply in rig space*/" },
				{ "GlobalSpace.Name", "ETransformSpaceMode::GlobalSpace" },
				{ "GlobalSpace.ToolTip", "Apply in rig space" },
				{ "LocalSpace.Comment", "/** Apply in parent space */" },
				{ "LocalSpace.Name", "ETransformSpaceMode::LocalSpace" },
				{ "LocalSpace.ToolTip", "Apply in parent space" },
				{ "Max.Comment", "/** MAX - invalid */" },
				{ "Max.Hidden", "" },
				{ "Max.Name", "ETransformSpaceMode::Max" },
				{ "Max.ToolTip", "MAX - invalid" },
				{ "ModuleRelativePath", "Public/ControlRigDefines.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ControlRig,
				nullptr,
				"ETransformSpaceMode",
				"ETransformSpaceMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}

static_assert(std::is_polymorphic<FControlRigExecuteContext>() == std::is_polymorphic<FRigVMExecuteContext>(), "USTRUCT FControlRigExecuteContext cannot be polymorphic unless super FRigVMExecuteContext is polymorphic");

class UScriptStruct* FControlRigExecuteContext::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FControlRigExecuteContext_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FControlRigExecuteContext, Z_Construct_UPackage__Script_ControlRig(), TEXT("ControlRigExecuteContext"), sizeof(FControlRigExecuteContext), Get_Z_Construct_UScriptStruct_FControlRigExecuteContext_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FControlRigExecuteContext>()
{
	return FControlRigExecuteContext::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FControlRigExecuteContext(FControlRigExecuteContext::StaticStruct, TEXT("/Script/ControlRig"), TEXT("ControlRigExecuteContext"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFControlRigExecuteContext
{
	FScriptStruct_ControlRig_StaticRegisterNativesFControlRigExecuteContext()
	{
		UScriptStruct::DeferCppStructOps<FControlRigExecuteContext>(FName(TEXT("ControlRigExecuteContext")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFControlRigExecuteContext;
	struct Z_Construct_UScriptStruct_FControlRigExecuteContext_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FControlRigExecuteContext_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ControlRigDefines.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FControlRigExecuteContext_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FControlRigExecuteContext>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FControlRigExecuteContext_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigVMExecuteContext,
		&NewStructOps,
		"ControlRigExecuteContext",
		sizeof(FControlRigExecuteContext),
		alignof(FControlRigExecuteContext),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FControlRigExecuteContext_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FControlRigExecuteContext_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FControlRigExecuteContext()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FControlRigExecuteContext_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ControlRigExecuteContext"), sizeof(FControlRigExecuteContext), Get_Z_Construct_UScriptStruct_FControlRigExecuteContext_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FControlRigExecuteContext_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FControlRigExecuteContext_Hash() { return 4279702553U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
