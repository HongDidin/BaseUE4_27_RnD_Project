// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_ControlRigSequencerAnimInstanceProxy_generated_h
#error "ControlRigSequencerAnimInstanceProxy.generated.h already included, missing '#pragma once' in ControlRigSequencerAnimInstanceProxy.h"
#endif
#define CONTROLRIG_ControlRigSequencerAnimInstanceProxy_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Sequencer_ControlRigSequencerAnimInstanceProxy_h_27_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FControlRigSequencerAnimInstanceProxy_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FAnimSequencerInstanceProxy Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FControlRigSequencerAnimInstanceProxy>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Sequencer_ControlRigSequencerAnimInstanceProxy_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
