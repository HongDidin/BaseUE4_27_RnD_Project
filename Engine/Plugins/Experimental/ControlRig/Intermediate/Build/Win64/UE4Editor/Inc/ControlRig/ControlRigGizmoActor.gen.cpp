// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Public/ControlRigGizmoActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeControlRigGizmoActor() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FGizmoActorCreationParam();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UClass* Z_Construct_UClass_AControlRigGizmoActor_NoRegister();
	CONTROLRIG_API UClass* Z_Construct_UClass_AControlRigGizmoActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
// End Cross Module References
class UScriptStruct* FGizmoActorCreationParam::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FGizmoActorCreationParam_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FGizmoActorCreationParam, Z_Construct_UPackage__Script_ControlRig(), TEXT("GizmoActorCreationParam"), sizeof(FGizmoActorCreationParam), Get_Z_Construct_UScriptStruct_FGizmoActorCreationParam_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FGizmoActorCreationParam>()
{
	return FGizmoActorCreationParam::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FGizmoActorCreationParam(FGizmoActorCreationParam::StaticStruct, TEXT("/Script/ControlRig"), TEXT("GizmoActorCreationParam"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFGizmoActorCreationParam
{
	FScriptStruct_ControlRig_StaticRegisterNativesFGizmoActorCreationParam()
	{
		UScriptStruct::DeferCppStructOps<FGizmoActorCreationParam>(FName(TEXT("GizmoActorCreationParam")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFGizmoActorCreationParam;
	struct Z_Construct_UScriptStruct_FGizmoActorCreationParam_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGizmoActorCreationParam_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ControlRigGizmoActor.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FGizmoActorCreationParam_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FGizmoActorCreationParam>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FGizmoActorCreationParam_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"GizmoActorCreationParam",
		sizeof(FGizmoActorCreationParam),
		alignof(FGizmoActorCreationParam),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FGizmoActorCreationParam_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGizmoActorCreationParam_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FGizmoActorCreationParam()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FGizmoActorCreationParam_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("GizmoActorCreationParam"), sizeof(FGizmoActorCreationParam), Get_Z_Construct_UScriptStruct_FGizmoActorCreationParam_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FGizmoActorCreationParam_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FGizmoActorCreationParam_Hash() { return 3080013486U; }
	DEFINE_FUNCTION(AControlRigGizmoActor::execGetGlobalTransform)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FTransform*)Z_Param__Result=P_THIS->GetGlobalTransform();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AControlRigGizmoActor::execSetGlobalTransform)
	{
		P_GET_STRUCT_REF(FTransform,Z_Param_Out_InTransform);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetGlobalTransform(Z_Param_Out_InTransform);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AControlRigGizmoActor::execIsHovered)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsHovered();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AControlRigGizmoActor::execSetHovered)
	{
		P_GET_UBOOL(Z_Param_bInHovered);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetHovered(Z_Param_bInHovered);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AControlRigGizmoActor::execSetSelectable)
	{
		P_GET_UBOOL(Z_Param_bInSelectable);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetSelectable(Z_Param_bInSelectable);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AControlRigGizmoActor::execIsSelectedInEditor)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsSelectedInEditor();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AControlRigGizmoActor::execSetSelected)
	{
		P_GET_UBOOL(Z_Param_bInSelected);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetSelected(Z_Param_bInSelected);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AControlRigGizmoActor::execIsEnabled)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsEnabled();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AControlRigGizmoActor::execSetEnabled)
	{
		P_GET_UBOOL(Z_Param_bInEnabled);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetEnabled(Z_Param_bInEnabled);
		P_NATIVE_END;
	}
	static FName NAME_AControlRigGizmoActor_OnEnabledChanged = FName(TEXT("OnEnabledChanged"));
	void AControlRigGizmoActor::OnEnabledChanged(bool bIsEnabled)
	{
		ControlRigGizmoActor_eventOnEnabledChanged_Parms Parms;
		Parms.bIsEnabled=bIsEnabled ? true : false;
		ProcessEvent(FindFunctionChecked(NAME_AControlRigGizmoActor_OnEnabledChanged),&Parms);
	}
	static FName NAME_AControlRigGizmoActor_OnHoveredChanged = FName(TEXT("OnHoveredChanged"));
	void AControlRigGizmoActor::OnHoveredChanged(bool bIsSelected)
	{
		ControlRigGizmoActor_eventOnHoveredChanged_Parms Parms;
		Parms.bIsSelected=bIsSelected ? true : false;
		ProcessEvent(FindFunctionChecked(NAME_AControlRigGizmoActor_OnHoveredChanged),&Parms);
	}
	static FName NAME_AControlRigGizmoActor_OnManipulatingChanged = FName(TEXT("OnManipulatingChanged"));
	void AControlRigGizmoActor::OnManipulatingChanged(bool bIsManipulating)
	{
		ControlRigGizmoActor_eventOnManipulatingChanged_Parms Parms;
		Parms.bIsManipulating=bIsManipulating ? true : false;
		ProcessEvent(FindFunctionChecked(NAME_AControlRigGizmoActor_OnManipulatingChanged),&Parms);
	}
	static FName NAME_AControlRigGizmoActor_OnSelectionChanged = FName(TEXT("OnSelectionChanged"));
	void AControlRigGizmoActor::OnSelectionChanged(bool bIsSelected)
	{
		ControlRigGizmoActor_eventOnSelectionChanged_Parms Parms;
		Parms.bIsSelected=bIsSelected ? true : false;
		ProcessEvent(FindFunctionChecked(NAME_AControlRigGizmoActor_OnSelectionChanged),&Parms);
	}
	static FName NAME_AControlRigGizmoActor_OnTransformChanged = FName(TEXT("OnTransformChanged"));
	void AControlRigGizmoActor::OnTransformChanged(FTransform const& NewTransform)
	{
		ControlRigGizmoActor_eventOnTransformChanged_Parms Parms;
		Parms.NewTransform=NewTransform;
		ProcessEvent(FindFunctionChecked(NAME_AControlRigGizmoActor_OnTransformChanged),&Parms);
	}
	void AControlRigGizmoActor::StaticRegisterNativesAControlRigGizmoActor()
	{
		UClass* Class = AControlRigGizmoActor::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetGlobalTransform", &AControlRigGizmoActor::execGetGlobalTransform },
			{ "IsEnabled", &AControlRigGizmoActor::execIsEnabled },
			{ "IsHovered", &AControlRigGizmoActor::execIsHovered },
			{ "IsSelectedInEditor", &AControlRigGizmoActor::execIsSelectedInEditor },
			{ "SetEnabled", &AControlRigGizmoActor::execSetEnabled },
			{ "SetGlobalTransform", &AControlRigGizmoActor::execSetGlobalTransform },
			{ "SetHovered", &AControlRigGizmoActor::execSetHovered },
			{ "SetSelectable", &AControlRigGizmoActor::execSetSelectable },
			{ "SetSelected", &AControlRigGizmoActor::execSetSelected },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AControlRigGizmoActor_GetGlobalTransform_Statics
	{
		struct ControlRigGizmoActor_eventGetGlobalTransform_Parms
		{
			FTransform ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AControlRigGizmoActor_GetGlobalTransform_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigGizmoActor_eventGetGlobalTransform_Parms, ReturnValue), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AControlRigGizmoActor_GetGlobalTransform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AControlRigGizmoActor_GetGlobalTransform_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AControlRigGizmoActor_GetGlobalTransform_Statics::Function_MetaDataParams[] = {
		{ "Category", "ControlRig|Gizmo" },
		{ "ModuleRelativePath", "Public/ControlRigGizmoActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AControlRigGizmoActor_GetGlobalTransform_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AControlRigGizmoActor, nullptr, "GetGlobalTransform", nullptr, nullptr, sizeof(ControlRigGizmoActor_eventGetGlobalTransform_Parms), Z_Construct_UFunction_AControlRigGizmoActor_GetGlobalTransform_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AControlRigGizmoActor_GetGlobalTransform_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AControlRigGizmoActor_GetGlobalTransform_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AControlRigGizmoActor_GetGlobalTransform_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AControlRigGizmoActor_GetGlobalTransform()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AControlRigGizmoActor_GetGlobalTransform_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AControlRigGizmoActor_IsEnabled_Statics
	{
		struct ControlRigGizmoActor_eventIsEnabled_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AControlRigGizmoActor_IsEnabled_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((ControlRigGizmoActor_eventIsEnabled_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AControlRigGizmoActor_IsEnabled_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ControlRigGizmoActor_eventIsEnabled_Parms), &Z_Construct_UFunction_AControlRigGizmoActor_IsEnabled_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AControlRigGizmoActor_IsEnabled_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AControlRigGizmoActor_IsEnabled_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AControlRigGizmoActor_IsEnabled_Statics::Function_MetaDataParams[] = {
		{ "BlueprintGetter", "" },
		{ "Comment", "/** Get whether the control is enabled/disabled */" },
		{ "ModuleRelativePath", "Public/ControlRigGizmoActor.h" },
		{ "ToolTip", "Get whether the control is enabled/disabled" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AControlRigGizmoActor_IsEnabled_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AControlRigGizmoActor, nullptr, "IsEnabled", nullptr, nullptr, sizeof(ControlRigGizmoActor_eventIsEnabled_Parms), Z_Construct_UFunction_AControlRigGizmoActor_IsEnabled_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AControlRigGizmoActor_IsEnabled_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AControlRigGizmoActor_IsEnabled_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AControlRigGizmoActor_IsEnabled_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AControlRigGizmoActor_IsEnabled()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AControlRigGizmoActor_IsEnabled_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AControlRigGizmoActor_IsHovered_Statics
	{
		struct ControlRigGizmoActor_eventIsHovered_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AControlRigGizmoActor_IsHovered_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((ControlRigGizmoActor_eventIsHovered_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AControlRigGizmoActor_IsHovered_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ControlRigGizmoActor_eventIsHovered_Parms), &Z_Construct_UFunction_AControlRigGizmoActor_IsHovered_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AControlRigGizmoActor_IsHovered_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AControlRigGizmoActor_IsHovered_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AControlRigGizmoActor_IsHovered_Statics::Function_MetaDataParams[] = {
		{ "BlueprintGetter", "" },
		{ "Comment", "/** Get whether the control is hovered */" },
		{ "ModuleRelativePath", "Public/ControlRigGizmoActor.h" },
		{ "ToolTip", "Get whether the control is hovered" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AControlRigGizmoActor_IsHovered_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AControlRigGizmoActor, nullptr, "IsHovered", nullptr, nullptr, sizeof(ControlRigGizmoActor_eventIsHovered_Parms), Z_Construct_UFunction_AControlRigGizmoActor_IsHovered_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AControlRigGizmoActor_IsHovered_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AControlRigGizmoActor_IsHovered_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AControlRigGizmoActor_IsHovered_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AControlRigGizmoActor_IsHovered()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AControlRigGizmoActor_IsHovered_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AControlRigGizmoActor_IsSelectedInEditor_Statics
	{
		struct ControlRigGizmoActor_eventIsSelectedInEditor_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AControlRigGizmoActor_IsSelectedInEditor_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((ControlRigGizmoActor_eventIsSelectedInEditor_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AControlRigGizmoActor_IsSelectedInEditor_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ControlRigGizmoActor_eventIsSelectedInEditor_Parms), &Z_Construct_UFunction_AControlRigGizmoActor_IsSelectedInEditor_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AControlRigGizmoActor_IsSelectedInEditor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AControlRigGizmoActor_IsSelectedInEditor_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AControlRigGizmoActor_IsSelectedInEditor_Statics::Function_MetaDataParams[] = {
		{ "BlueprintGetter", "" },
		{ "Comment", "/** Get whether the control is selected/unselected */" },
		{ "ModuleRelativePath", "Public/ControlRigGizmoActor.h" },
		{ "ToolTip", "Get whether the control is selected/unselected" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AControlRigGizmoActor_IsSelectedInEditor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AControlRigGizmoActor, nullptr, "IsSelectedInEditor", nullptr, nullptr, sizeof(ControlRigGizmoActor_eventIsSelectedInEditor_Parms), Z_Construct_UFunction_AControlRigGizmoActor_IsSelectedInEditor_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AControlRigGizmoActor_IsSelectedInEditor_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AControlRigGizmoActor_IsSelectedInEditor_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AControlRigGizmoActor_IsSelectedInEditor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AControlRigGizmoActor_IsSelectedInEditor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AControlRigGizmoActor_IsSelectedInEditor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AControlRigGizmoActor_OnEnabledChanged_Statics
	{
		static void NewProp_bIsEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsEnabled;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AControlRigGizmoActor_OnEnabledChanged_Statics::NewProp_bIsEnabled_SetBit(void* Obj)
	{
		((ControlRigGizmoActor_eventOnEnabledChanged_Parms*)Obj)->bIsEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AControlRigGizmoActor_OnEnabledChanged_Statics::NewProp_bIsEnabled = { "bIsEnabled", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ControlRigGizmoActor_eventOnEnabledChanged_Parms), &Z_Construct_UFunction_AControlRigGizmoActor_OnEnabledChanged_Statics::NewProp_bIsEnabled_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AControlRigGizmoActor_OnEnabledChanged_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AControlRigGizmoActor_OnEnabledChanged_Statics::NewProp_bIsEnabled,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AControlRigGizmoActor_OnEnabledChanged_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Event called when the enabled state of this control has changed */" },
		{ "ModuleRelativePath", "Public/ControlRigGizmoActor.h" },
		{ "ToolTip", "Event called when the enabled state of this control has changed" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AControlRigGizmoActor_OnEnabledChanged_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AControlRigGizmoActor, nullptr, "OnEnabledChanged", nullptr, nullptr, sizeof(ControlRigGizmoActor_eventOnEnabledChanged_Parms), Z_Construct_UFunction_AControlRigGizmoActor_OnEnabledChanged_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AControlRigGizmoActor_OnEnabledChanged_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AControlRigGizmoActor_OnEnabledChanged_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AControlRigGizmoActor_OnEnabledChanged_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AControlRigGizmoActor_OnEnabledChanged()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AControlRigGizmoActor_OnEnabledChanged_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AControlRigGizmoActor_OnHoveredChanged_Statics
	{
		static void NewProp_bIsSelected_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsSelected;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AControlRigGizmoActor_OnHoveredChanged_Statics::NewProp_bIsSelected_SetBit(void* Obj)
	{
		((ControlRigGizmoActor_eventOnHoveredChanged_Parms*)Obj)->bIsSelected = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AControlRigGizmoActor_OnHoveredChanged_Statics::NewProp_bIsSelected = { "bIsSelected", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ControlRigGizmoActor_eventOnHoveredChanged_Parms), &Z_Construct_UFunction_AControlRigGizmoActor_OnHoveredChanged_Statics::NewProp_bIsSelected_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AControlRigGizmoActor_OnHoveredChanged_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AControlRigGizmoActor_OnHoveredChanged_Statics::NewProp_bIsSelected,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AControlRigGizmoActor_OnHoveredChanged_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Event called when the hovered state of this control has changed */" },
		{ "ModuleRelativePath", "Public/ControlRigGizmoActor.h" },
		{ "ToolTip", "Event called when the hovered state of this control has changed" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AControlRigGizmoActor_OnHoveredChanged_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AControlRigGizmoActor, nullptr, "OnHoveredChanged", nullptr, nullptr, sizeof(ControlRigGizmoActor_eventOnHoveredChanged_Parms), Z_Construct_UFunction_AControlRigGizmoActor_OnHoveredChanged_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AControlRigGizmoActor_OnHoveredChanged_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AControlRigGizmoActor_OnHoveredChanged_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AControlRigGizmoActor_OnHoveredChanged_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AControlRigGizmoActor_OnHoveredChanged()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AControlRigGizmoActor_OnHoveredChanged_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AControlRigGizmoActor_OnManipulatingChanged_Statics
	{
		static void NewProp_bIsManipulating_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsManipulating;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AControlRigGizmoActor_OnManipulatingChanged_Statics::NewProp_bIsManipulating_SetBit(void* Obj)
	{
		((ControlRigGizmoActor_eventOnManipulatingChanged_Parms*)Obj)->bIsManipulating = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AControlRigGizmoActor_OnManipulatingChanged_Statics::NewProp_bIsManipulating = { "bIsManipulating", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ControlRigGizmoActor_eventOnManipulatingChanged_Parms), &Z_Construct_UFunction_AControlRigGizmoActor_OnManipulatingChanged_Statics::NewProp_bIsManipulating_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AControlRigGizmoActor_OnManipulatingChanged_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AControlRigGizmoActor_OnManipulatingChanged_Statics::NewProp_bIsManipulating,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AControlRigGizmoActor_OnManipulatingChanged_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Event called when the manipulating state of this control has changed */" },
		{ "ModuleRelativePath", "Public/ControlRigGizmoActor.h" },
		{ "ToolTip", "Event called when the manipulating state of this control has changed" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AControlRigGizmoActor_OnManipulatingChanged_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AControlRigGizmoActor, nullptr, "OnManipulatingChanged", nullptr, nullptr, sizeof(ControlRigGizmoActor_eventOnManipulatingChanged_Parms), Z_Construct_UFunction_AControlRigGizmoActor_OnManipulatingChanged_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AControlRigGizmoActor_OnManipulatingChanged_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AControlRigGizmoActor_OnManipulatingChanged_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AControlRigGizmoActor_OnManipulatingChanged_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AControlRigGizmoActor_OnManipulatingChanged()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AControlRigGizmoActor_OnManipulatingChanged_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AControlRigGizmoActor_OnSelectionChanged_Statics
	{
		static void NewProp_bIsSelected_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsSelected;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AControlRigGizmoActor_OnSelectionChanged_Statics::NewProp_bIsSelected_SetBit(void* Obj)
	{
		((ControlRigGizmoActor_eventOnSelectionChanged_Parms*)Obj)->bIsSelected = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AControlRigGizmoActor_OnSelectionChanged_Statics::NewProp_bIsSelected = { "bIsSelected", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ControlRigGizmoActor_eventOnSelectionChanged_Parms), &Z_Construct_UFunction_AControlRigGizmoActor_OnSelectionChanged_Statics::NewProp_bIsSelected_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AControlRigGizmoActor_OnSelectionChanged_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AControlRigGizmoActor_OnSelectionChanged_Statics::NewProp_bIsSelected,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AControlRigGizmoActor_OnSelectionChanged_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Event called when the selection state of this control has changed */" },
		{ "ModuleRelativePath", "Public/ControlRigGizmoActor.h" },
		{ "ToolTip", "Event called when the selection state of this control has changed" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AControlRigGizmoActor_OnSelectionChanged_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AControlRigGizmoActor, nullptr, "OnSelectionChanged", nullptr, nullptr, sizeof(ControlRigGizmoActor_eventOnSelectionChanged_Parms), Z_Construct_UFunction_AControlRigGizmoActor_OnSelectionChanged_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AControlRigGizmoActor_OnSelectionChanged_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AControlRigGizmoActor_OnSelectionChanged_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AControlRigGizmoActor_OnSelectionChanged_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AControlRigGizmoActor_OnSelectionChanged()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AControlRigGizmoActor_OnSelectionChanged_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AControlRigGizmoActor_OnTransformChanged_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewTransform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NewTransform;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AControlRigGizmoActor_OnTransformChanged_Statics::NewProp_NewTransform_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AControlRigGizmoActor_OnTransformChanged_Statics::NewProp_NewTransform = { "NewTransform", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigGizmoActor_eventOnTransformChanged_Parms, NewTransform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UFunction_AControlRigGizmoActor_OnTransformChanged_Statics::NewProp_NewTransform_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AControlRigGizmoActor_OnTransformChanged_Statics::NewProp_NewTransform_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AControlRigGizmoActor_OnTransformChanged_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AControlRigGizmoActor_OnTransformChanged_Statics::NewProp_NewTransform,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AControlRigGizmoActor_OnTransformChanged_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Event called when the transform of this control has changed */" },
		{ "ModuleRelativePath", "Public/ControlRigGizmoActor.h" },
		{ "ToolTip", "Event called when the transform of this control has changed" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AControlRigGizmoActor_OnTransformChanged_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AControlRigGizmoActor, nullptr, "OnTransformChanged", nullptr, nullptr, sizeof(ControlRigGizmoActor_eventOnTransformChanged_Parms), Z_Construct_UFunction_AControlRigGizmoActor_OnTransformChanged_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AControlRigGizmoActor_OnTransformChanged_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08C20800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AControlRigGizmoActor_OnTransformChanged_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AControlRigGizmoActor_OnTransformChanged_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AControlRigGizmoActor_OnTransformChanged()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AControlRigGizmoActor_OnTransformChanged_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AControlRigGizmoActor_SetEnabled_Statics
	{
		struct ControlRigGizmoActor_eventSetEnabled_Parms
		{
			bool bInEnabled;
		};
		static void NewProp_bInEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInEnabled;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AControlRigGizmoActor_SetEnabled_Statics::NewProp_bInEnabled_SetBit(void* Obj)
	{
		((ControlRigGizmoActor_eventSetEnabled_Parms*)Obj)->bInEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AControlRigGizmoActor_SetEnabled_Statics::NewProp_bInEnabled = { "bInEnabled", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ControlRigGizmoActor_eventSetEnabled_Parms), &Z_Construct_UFunction_AControlRigGizmoActor_SetEnabled_Statics::NewProp_bInEnabled_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AControlRigGizmoActor_SetEnabled_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AControlRigGizmoActor_SetEnabled_Statics::NewProp_bInEnabled,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AControlRigGizmoActor_SetEnabled_Statics::Function_MetaDataParams[] = {
		{ "BlueprintSetter", "" },
		{ "Comment", "/** Set the control to be enabled/disabled */" },
		{ "ModuleRelativePath", "Public/ControlRigGizmoActor.h" },
		{ "ToolTip", "Set the control to be enabled/disabled" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AControlRigGizmoActor_SetEnabled_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AControlRigGizmoActor, nullptr, "SetEnabled", nullptr, nullptr, sizeof(ControlRigGizmoActor_eventSetEnabled_Parms), Z_Construct_UFunction_AControlRigGizmoActor_SetEnabled_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AControlRigGizmoActor_SetEnabled_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AControlRigGizmoActor_SetEnabled_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AControlRigGizmoActor_SetEnabled_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AControlRigGizmoActor_SetEnabled()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AControlRigGizmoActor_SetEnabled_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AControlRigGizmoActor_SetGlobalTransform_Statics
	{
		struct ControlRigGizmoActor_eventSetGlobalTransform_Parms
		{
			FTransform InTransform;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InTransform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InTransform;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AControlRigGizmoActor_SetGlobalTransform_Statics::NewProp_InTransform_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AControlRigGizmoActor_SetGlobalTransform_Statics::NewProp_InTransform = { "InTransform", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigGizmoActor_eventSetGlobalTransform_Parms, InTransform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UFunction_AControlRigGizmoActor_SetGlobalTransform_Statics::NewProp_InTransform_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AControlRigGizmoActor_SetGlobalTransform_Statics::NewProp_InTransform_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AControlRigGizmoActor_SetGlobalTransform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AControlRigGizmoActor_SetGlobalTransform_Statics::NewProp_InTransform,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AControlRigGizmoActor_SetGlobalTransform_Statics::Function_MetaDataParams[] = {
		{ "Category", "ControlRig|Gizmo" },
		{ "Comment", "// this returns root component transform based on attach\n// when there is no attach, it is based on 0\n" },
		{ "ModuleRelativePath", "Public/ControlRigGizmoActor.h" },
		{ "ToolTip", "this returns root component transform based on attach\nwhen there is no attach, it is based on 0" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AControlRigGizmoActor_SetGlobalTransform_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AControlRigGizmoActor, nullptr, "SetGlobalTransform", nullptr, nullptr, sizeof(ControlRigGizmoActor_eventSetGlobalTransform_Parms), Z_Construct_UFunction_AControlRigGizmoActor_SetGlobalTransform_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AControlRigGizmoActor_SetGlobalTransform_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AControlRigGizmoActor_SetGlobalTransform_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AControlRigGizmoActor_SetGlobalTransform_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AControlRigGizmoActor_SetGlobalTransform()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AControlRigGizmoActor_SetGlobalTransform_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AControlRigGizmoActor_SetHovered_Statics
	{
		struct ControlRigGizmoActor_eventSetHovered_Parms
		{
			bool bInHovered;
		};
		static void NewProp_bInHovered_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInHovered;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AControlRigGizmoActor_SetHovered_Statics::NewProp_bInHovered_SetBit(void* Obj)
	{
		((ControlRigGizmoActor_eventSetHovered_Parms*)Obj)->bInHovered = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AControlRigGizmoActor_SetHovered_Statics::NewProp_bInHovered = { "bInHovered", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ControlRigGizmoActor_eventSetHovered_Parms), &Z_Construct_UFunction_AControlRigGizmoActor_SetHovered_Statics::NewProp_bInHovered_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AControlRigGizmoActor_SetHovered_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AControlRigGizmoActor_SetHovered_Statics::NewProp_bInHovered,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AControlRigGizmoActor_SetHovered_Statics::Function_MetaDataParams[] = {
		{ "BlueprintSetter", "" },
		{ "Comment", "/** Set the control to be hovered */" },
		{ "ModuleRelativePath", "Public/ControlRigGizmoActor.h" },
		{ "ToolTip", "Set the control to be hovered" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AControlRigGizmoActor_SetHovered_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AControlRigGizmoActor, nullptr, "SetHovered", nullptr, nullptr, sizeof(ControlRigGizmoActor_eventSetHovered_Parms), Z_Construct_UFunction_AControlRigGizmoActor_SetHovered_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AControlRigGizmoActor_SetHovered_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AControlRigGizmoActor_SetHovered_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AControlRigGizmoActor_SetHovered_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AControlRigGizmoActor_SetHovered()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AControlRigGizmoActor_SetHovered_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AControlRigGizmoActor_SetSelectable_Statics
	{
		struct ControlRigGizmoActor_eventSetSelectable_Parms
		{
			bool bInSelectable;
		};
		static void NewProp_bInSelectable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInSelectable;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AControlRigGizmoActor_SetSelectable_Statics::NewProp_bInSelectable_SetBit(void* Obj)
	{
		((ControlRigGizmoActor_eventSetSelectable_Parms*)Obj)->bInSelectable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AControlRigGizmoActor_SetSelectable_Statics::NewProp_bInSelectable = { "bInSelectable", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ControlRigGizmoActor_eventSetSelectable_Parms), &Z_Construct_UFunction_AControlRigGizmoActor_SetSelectable_Statics::NewProp_bInSelectable_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AControlRigGizmoActor_SetSelectable_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AControlRigGizmoActor_SetSelectable_Statics::NewProp_bInSelectable,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AControlRigGizmoActor_SetSelectable_Statics::Function_MetaDataParams[] = {
		{ "BlueprintSetter", "" },
		{ "Comment", "/** Set the control to be selected/unselected */" },
		{ "ModuleRelativePath", "Public/ControlRigGizmoActor.h" },
		{ "ToolTip", "Set the control to be selected/unselected" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AControlRigGizmoActor_SetSelectable_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AControlRigGizmoActor, nullptr, "SetSelectable", nullptr, nullptr, sizeof(ControlRigGizmoActor_eventSetSelectable_Parms), Z_Construct_UFunction_AControlRigGizmoActor_SetSelectable_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AControlRigGizmoActor_SetSelectable_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AControlRigGizmoActor_SetSelectable_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AControlRigGizmoActor_SetSelectable_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AControlRigGizmoActor_SetSelectable()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AControlRigGizmoActor_SetSelectable_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AControlRigGizmoActor_SetSelected_Statics
	{
		struct ControlRigGizmoActor_eventSetSelected_Parms
		{
			bool bInSelected;
		};
		static void NewProp_bInSelected_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInSelected;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AControlRigGizmoActor_SetSelected_Statics::NewProp_bInSelected_SetBit(void* Obj)
	{
		((ControlRigGizmoActor_eventSetSelected_Parms*)Obj)->bInSelected = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AControlRigGizmoActor_SetSelected_Statics::NewProp_bInSelected = { "bInSelected", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ControlRigGizmoActor_eventSetSelected_Parms), &Z_Construct_UFunction_AControlRigGizmoActor_SetSelected_Statics::NewProp_bInSelected_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AControlRigGizmoActor_SetSelected_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AControlRigGizmoActor_SetSelected_Statics::NewProp_bInSelected,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AControlRigGizmoActor_SetSelected_Statics::Function_MetaDataParams[] = {
		{ "BlueprintSetter", "" },
		{ "Comment", "/** Set the control to be selected/unselected */" },
		{ "ModuleRelativePath", "Public/ControlRigGizmoActor.h" },
		{ "ToolTip", "Set the control to be selected/unselected" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AControlRigGizmoActor_SetSelected_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AControlRigGizmoActor, nullptr, "SetSelected", nullptr, nullptr, sizeof(ControlRigGizmoActor_eventSetSelected_Parms), Z_Construct_UFunction_AControlRigGizmoActor_SetSelected_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AControlRigGizmoActor_SetSelected_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AControlRigGizmoActor_SetSelected_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AControlRigGizmoActor_SetSelected_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AControlRigGizmoActor_SetSelected()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AControlRigGizmoActor_SetSelected_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AControlRigGizmoActor_NoRegister()
	{
		return AControlRigGizmoActor::StaticClass();
	}
	struct Z_Construct_UClass_AControlRigGizmoActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActorRootComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ActorRootComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StaticMeshComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMeshComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ControlRigIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_ControlRigIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ControlName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ControlName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ColorParameterName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ColorParameterName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnabled_MetaData[];
#endif
		static void NewProp_bEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSelected_MetaData[];
#endif
		static void NewProp_bSelected_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSelected;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSelectable_MetaData[];
#endif
		static void NewProp_bSelectable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSelectable;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bHovered_MetaData[];
#endif
		static void NewProp_bHovered_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bHovered;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AControlRigGizmoActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AControlRigGizmoActor_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AControlRigGizmoActor_GetGlobalTransform, "GetGlobalTransform" }, // 3652641013
		{ &Z_Construct_UFunction_AControlRigGizmoActor_IsEnabled, "IsEnabled" }, // 2920011428
		{ &Z_Construct_UFunction_AControlRigGizmoActor_IsHovered, "IsHovered" }, // 1996040629
		{ &Z_Construct_UFunction_AControlRigGizmoActor_IsSelectedInEditor, "IsSelectedInEditor" }, // 719381888
		{ &Z_Construct_UFunction_AControlRigGizmoActor_OnEnabledChanged, "OnEnabledChanged" }, // 2633592124
		{ &Z_Construct_UFunction_AControlRigGizmoActor_OnHoveredChanged, "OnHoveredChanged" }, // 3747558601
		{ &Z_Construct_UFunction_AControlRigGizmoActor_OnManipulatingChanged, "OnManipulatingChanged" }, // 2226320412
		{ &Z_Construct_UFunction_AControlRigGizmoActor_OnSelectionChanged, "OnSelectionChanged" }, // 2487685187
		{ &Z_Construct_UFunction_AControlRigGizmoActor_OnTransformChanged, "OnTransformChanged" }, // 1480343726
		{ &Z_Construct_UFunction_AControlRigGizmoActor_SetEnabled, "SetEnabled" }, // 2209802463
		{ &Z_Construct_UFunction_AControlRigGizmoActor_SetGlobalTransform, "SetGlobalTransform" }, // 2399992628
		{ &Z_Construct_UFunction_AControlRigGizmoActor_SetHovered, "SetHovered" }, // 2994899682
		{ &Z_Construct_UFunction_AControlRigGizmoActor_SetSelectable, "SetSelectable" }, // 143264643
		{ &Z_Construct_UFunction_AControlRigGizmoActor_SetSelected, "SetSelected" }, // 2951960240
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AControlRigGizmoActor_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** An actor used to represent a rig control */" },
		{ "IncludePath", "ControlRigGizmoActor.h" },
		{ "ModuleRelativePath", "Public/ControlRigGizmoActor.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "An actor used to represent a rig control" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_ActorRootComponent_MetaData[] = {
		{ "Comment", "// this is the one holding transform for the controls\n" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/ControlRigGizmoActor.h" },
		{ "ToolTip", "this is the one holding transform for the controls" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_ActorRootComponent = { "ActorRootComponent", nullptr, (EPropertyFlags)0x0010000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AControlRigGizmoActor, ActorRootComponent), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_ActorRootComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_ActorRootComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_StaticMeshComponent_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "StaticMesh" },
		{ "Comment", "// this is visual representation of the transform\n" },
		{ "EditInline", "true" },
		{ "ExposeFunctionCategories", "Mesh,Rendering,Physics,Components|StaticMesh" },
		{ "ModuleRelativePath", "Public/ControlRigGizmoActor.h" },
		{ "ToolTip", "this is visual representation of the transform" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_StaticMeshComponent = { "StaticMeshComponent", nullptr, (EPropertyFlags)0x00100000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AControlRigGizmoActor, StaticMeshComponent), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_StaticMeshComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_StaticMeshComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_ControlRigIndex_MetaData[] = {
		{ "Comment", "// the name of the control this actor is referencing\n" },
		{ "ModuleRelativePath", "Public/ControlRigGizmoActor.h" },
		{ "ToolTip", "the name of the control this actor is referencing" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_ControlRigIndex = { "ControlRigIndex", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AControlRigGizmoActor, ControlRigIndex), METADATA_PARAMS(Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_ControlRigIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_ControlRigIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_ControlName_MetaData[] = {
		{ "Comment", "// the name of the control this actor is referencing\n" },
		{ "ModuleRelativePath", "Public/ControlRigGizmoActor.h" },
		{ "ToolTip", "the name of the control this actor is referencing" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_ControlName = { "ControlName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AControlRigGizmoActor, ControlName), METADATA_PARAMS(Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_ControlName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_ControlName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_ColorParameterName_MetaData[] = {
		{ "Comment", "// the name of the color parameter on the material\n" },
		{ "ModuleRelativePath", "Public/ControlRigGizmoActor.h" },
		{ "ToolTip", "the name of the color parameter on the material" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_ColorParameterName = { "ColorParameterName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AControlRigGizmoActor, ColorParameterName), METADATA_PARAMS(Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_ColorParameterName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_ColorParameterName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_bEnabled_MetaData[] = {
		{ "BlueprintGetter", "IsEnabled" },
		{ "BlueprintSetter", "SetEnabled" },
		{ "Category", "ControlRig|Gizmo" },
		{ "Comment", "/** Whether this control is enabled */" },
		{ "ModuleRelativePath", "Public/ControlRigGizmoActor.h" },
		{ "ToolTip", "Whether this control is enabled" },
	};
#endif
	void Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_bEnabled_SetBit(void* Obj)
	{
		((AControlRigGizmoActor*)Obj)->bEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_bEnabled = { "bEnabled", nullptr, (EPropertyFlags)0x0040000000000004, UE4CodeGen_Private::EPropertyGenFlags::Bool , RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(uint8), sizeof(AControlRigGizmoActor), &Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_bEnabled_SetBit, METADATA_PARAMS(Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_bEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_bEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_bSelected_MetaData[] = {
		{ "BlueprintGetter", "IsSelectedInEditor" },
		{ "BlueprintSetter", "SetSelected" },
		{ "Category", "ControlRig|Gizmo" },
		{ "Comment", "/** Whether this control is selected */" },
		{ "ModuleRelativePath", "Public/ControlRigGizmoActor.h" },
		{ "ToolTip", "Whether this control is selected" },
	};
#endif
	void Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_bSelected_SetBit(void* Obj)
	{
		((AControlRigGizmoActor*)Obj)->bSelected = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_bSelected = { "bSelected", nullptr, (EPropertyFlags)0x0040000000000004, UE4CodeGen_Private::EPropertyGenFlags::Bool , RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(uint8), sizeof(AControlRigGizmoActor), &Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_bSelected_SetBit, METADATA_PARAMS(Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_bSelected_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_bSelected_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_bSelectable_MetaData[] = {
		{ "BlueprintGetter", "IsSelectable" },
		{ "BlueprintSetter", "SetSelectable" },
		{ "Category", "ControlRig|Gizmo" },
		{ "Comment", "/** Whether this control can be selected */" },
		{ "ModuleRelativePath", "Public/ControlRigGizmoActor.h" },
		{ "ToolTip", "Whether this control can be selected" },
	};
#endif
	void Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_bSelectable_SetBit(void* Obj)
	{
		((AControlRigGizmoActor*)Obj)->bSelectable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_bSelectable = { "bSelectable", nullptr, (EPropertyFlags)0x0040000000000004, UE4CodeGen_Private::EPropertyGenFlags::Bool , RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(uint8), sizeof(AControlRigGizmoActor), &Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_bSelectable_SetBit, METADATA_PARAMS(Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_bSelectable_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_bSelectable_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_bHovered_MetaData[] = {
		{ "BlueprintGetter", "IsHovered" },
		{ "BlueprintSetter", "SetHovered" },
		{ "Category", "ControlRig|Gizmo" },
		{ "Comment", "/** Whether this control is hovered */" },
		{ "ModuleRelativePath", "Public/ControlRigGizmoActor.h" },
		{ "ToolTip", "Whether this control is hovered" },
	};
#endif
	void Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_bHovered_SetBit(void* Obj)
	{
		((AControlRigGizmoActor*)Obj)->bHovered = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_bHovered = { "bHovered", nullptr, (EPropertyFlags)0x0040000000000004, UE4CodeGen_Private::EPropertyGenFlags::Bool , RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(uint8), sizeof(AControlRigGizmoActor), &Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_bHovered_SetBit, METADATA_PARAMS(Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_bHovered_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_bHovered_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AControlRigGizmoActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_ActorRootComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_StaticMeshComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_ControlRigIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_ControlName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_ColorParameterName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_bEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_bSelected,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_bSelectable,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AControlRigGizmoActor_Statics::NewProp_bHovered,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AControlRigGizmoActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AControlRigGizmoActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AControlRigGizmoActor_Statics::ClassParams = {
		&AControlRigGizmoActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AControlRigGizmoActor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AControlRigGizmoActor_Statics::PropPointers),
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_AControlRigGizmoActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AControlRigGizmoActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AControlRigGizmoActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AControlRigGizmoActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AControlRigGizmoActor, 652846374);
	template<> CONTROLRIG_API UClass* StaticClass<AControlRigGizmoActor>()
	{
		return AControlRigGizmoActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AControlRigGizmoActor(Z_Construct_UClass_AControlRigGizmoActor, &AControlRigGizmoActor::StaticClass, TEXT("/Script/ControlRig"), TEXT("AControlRigGizmoActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AControlRigGizmoActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
