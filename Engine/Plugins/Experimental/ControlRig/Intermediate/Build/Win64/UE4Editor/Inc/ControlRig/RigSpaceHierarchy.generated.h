// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigSpaceHierarchy_generated_h
#error "RigSpaceHierarchy.generated.h already included, missing '#pragma once' in RigSpaceHierarchy.h"
#endif
#define CONTROLRIG_RigSpaceHierarchy_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_RigSpaceHierarchy_h_95_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigSpaceHierarchy_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__Spaces() { return STRUCT_OFFSET(FRigSpaceHierarchy, Spaces); } \
	FORCEINLINE static uint32 __PPO__NameToIndexMapping() { return STRUCT_OFFSET(FRigSpaceHierarchy, NameToIndexMapping); } \
	FORCEINLINE static uint32 __PPO__Selection() { return STRUCT_OFFSET(FRigSpaceHierarchy, Selection); }


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigSpaceHierarchy>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_RigSpaceHierarchy_h_31_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigSpace_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FRigElement Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigSpace>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_RigSpaceHierarchy_h


#define FOREACH_ENUM_ERIGSPACETYPE(op) \
	op(ERigSpaceType::Global) \
	op(ERigSpaceType::Bone) \
	op(ERigSpaceType::Control) \
	op(ERigSpaceType::Space) 

enum class ERigSpaceType : uint8;
template<> CONTROLRIG_API UEnum* StaticEnum<ERigSpaceType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
