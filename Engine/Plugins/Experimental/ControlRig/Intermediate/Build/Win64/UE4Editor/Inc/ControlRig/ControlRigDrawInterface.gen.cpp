// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Public/Drawing/ControlRigDrawInterface.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeControlRigDrawInterface() {}
// Cross Module References
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_EControlRigDrawHierarchyMode();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FControlRigDrawInterface();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FControlRigDrawContainer();
// End Cross Module References
	static UEnum* EControlRigDrawHierarchyMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ControlRig_EControlRigDrawHierarchyMode, Z_Construct_UPackage__Script_ControlRig(), TEXT("EControlRigDrawHierarchyMode"));
		}
		return Singleton;
	}
	template<> CONTROLRIG_API UEnum* StaticEnum<EControlRigDrawHierarchyMode::Type>()
	{
		return EControlRigDrawHierarchyMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EControlRigDrawHierarchyMode(EControlRigDrawHierarchyMode_StaticEnum, TEXT("/Script/ControlRig"), TEXT("EControlRigDrawHierarchyMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ControlRig_EControlRigDrawHierarchyMode_Hash() { return 1046628581U; }
	UEnum* Z_Construct_UEnum_ControlRig_EControlRigDrawHierarchyMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EControlRigDrawHierarchyMode"), 0, Get_Z_Construct_UEnum_ControlRig_EControlRigDrawHierarchyMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EControlRigDrawHierarchyMode::Axes", (int64)EControlRigDrawHierarchyMode::Axes },
				{ "EControlRigDrawHierarchyMode::Max", (int64)EControlRigDrawHierarchyMode::Max },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Axes.Comment", "/** Draw as axes */" },
				{ "Axes.Name", "EControlRigDrawHierarchyMode::Axes" },
				{ "Axes.ToolTip", "Draw as axes" },
				{ "Max.Comment", "/** MAX - invalid */" },
				{ "Max.Hidden", "" },
				{ "Max.Name", "EControlRigDrawHierarchyMode::Max" },
				{ "Max.ToolTip", "MAX - invalid" },
				{ "ModuleRelativePath", "Public/Drawing/ControlRigDrawInterface.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ControlRig,
				nullptr,
				"EControlRigDrawHierarchyMode",
				"EControlRigDrawHierarchyMode::Type",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::Namespaced,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}

static_assert(std::is_polymorphic<FControlRigDrawInterface>() == std::is_polymorphic<FControlRigDrawContainer>(), "USTRUCT FControlRigDrawInterface cannot be polymorphic unless super FControlRigDrawContainer is polymorphic");

class UScriptStruct* FControlRigDrawInterface::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FControlRigDrawInterface_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FControlRigDrawInterface, Z_Construct_UPackage__Script_ControlRig(), TEXT("ControlRigDrawInterface"), sizeof(FControlRigDrawInterface), Get_Z_Construct_UScriptStruct_FControlRigDrawInterface_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FControlRigDrawInterface>()
{
	return FControlRigDrawInterface::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FControlRigDrawInterface(FControlRigDrawInterface::StaticStruct, TEXT("/Script/ControlRig"), TEXT("ControlRigDrawInterface"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFControlRigDrawInterface
{
	FScriptStruct_ControlRig_StaticRegisterNativesFControlRigDrawInterface()
	{
		UScriptStruct::DeferCppStructOps<FControlRigDrawInterface>(FName(TEXT("ControlRigDrawInterface")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFControlRigDrawInterface;
	struct Z_Construct_UScriptStruct_FControlRigDrawInterface_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FControlRigDrawInterface_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Drawing/ControlRigDrawInterface.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FControlRigDrawInterface_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FControlRigDrawInterface>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FControlRigDrawInterface_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FControlRigDrawContainer,
		&NewStructOps,
		"ControlRigDrawInterface",
		sizeof(FControlRigDrawInterface),
		alignof(FControlRigDrawInterface),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FControlRigDrawInterface_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FControlRigDrawInterface_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FControlRigDrawInterface()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FControlRigDrawInterface_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ControlRigDrawInterface"), sizeof(FControlRigDrawInterface), Get_Z_Construct_UScriptStruct_FControlRigDrawInterface_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FControlRigDrawInterface_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FControlRigDrawInterface_Hash() { return 3881649261U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
