// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigUnit_MathBool_generated_h
#error "RigUnit_MathBool.generated.h already included, missing '#pragma once' in RigUnit_MathBool.h"
#endif
#define CONTROLRIG_RigUnit_MathBool_generated_h


#define FRigUnit_MathBoolNotEquals_Execute() \
	void FRigUnit_MathBoolNotEquals::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const bool A, \
		const bool B, \
		bool& Result, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_MathBool_h_179_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_MathBoolNotEquals_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const bool A, \
		const bool B, \
		bool& Result, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const bool A = *(bool*)RigVMMemoryHandles[0].GetData(); \
		const bool B = *(bool*)RigVMMemoryHandles[1].GetData(); \
		bool& Result = *(bool*)RigVMMemoryHandles[2].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			A, \
			B, \
			Result, \
			Context \
		); \
	} \
	typedef FRigUnit_MathBoolBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_MathBoolNotEquals>();


#define FRigUnit_MathBoolEquals_Execute() \
	void FRigUnit_MathBoolEquals::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const bool A, \
		const bool B, \
		bool& Result, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_MathBool_h_153_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_MathBoolEquals_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const bool A, \
		const bool B, \
		bool& Result, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const bool A = *(bool*)RigVMMemoryHandles[0].GetData(); \
		const bool B = *(bool*)RigVMMemoryHandles[1].GetData(); \
		bool& Result = *(bool*)RigVMMemoryHandles[2].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			A, \
			B, \
			Result, \
			Context \
		); \
	} \
	typedef FRigUnit_MathBoolBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_MathBoolEquals>();


#define FRigUnit_MathBoolOr_Execute() \
	void FRigUnit_MathBoolOr::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const bool A, \
		const bool B, \
		bool& Result, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_MathBool_h_141_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_MathBoolOr_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const bool A, \
		const bool B, \
		bool& Result, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const bool A = *(bool*)RigVMMemoryHandles[0].GetData(); \
		const bool B = *(bool*)RigVMMemoryHandles[1].GetData(); \
		bool& Result = *(bool*)RigVMMemoryHandles[2].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			A, \
			B, \
			Result, \
			Context \
		); \
	} \
	typedef FRigUnit_MathBoolBinaryOp Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_MathBoolOr>();


#define FRigUnit_MathBoolNand_Execute() \
	void FRigUnit_MathBoolNand::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const bool A, \
		const bool B, \
		bool& Result, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_MathBool_h_129_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_MathBoolNand_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const bool A, \
		const bool B, \
		bool& Result, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const bool A = *(bool*)RigVMMemoryHandles[0].GetData(); \
		const bool B = *(bool*)RigVMMemoryHandles[1].GetData(); \
		bool& Result = *(bool*)RigVMMemoryHandles[2].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			A, \
			B, \
			Result, \
			Context \
		); \
	} \
	typedef FRigUnit_MathBoolBinaryOp Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_MathBoolNand>();


#define FRigUnit_MathBoolAnd_Execute() \
	void FRigUnit_MathBoolAnd::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const bool A, \
		const bool B, \
		bool& Result, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_MathBool_h_117_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_MathBoolAnd_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const bool A, \
		const bool B, \
		bool& Result, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const bool A = *(bool*)RigVMMemoryHandles[0].GetData(); \
		const bool B = *(bool*)RigVMMemoryHandles[1].GetData(); \
		bool& Result = *(bool*)RigVMMemoryHandles[2].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			A, \
			B, \
			Result, \
			Context \
		); \
	} \
	typedef FRigUnit_MathBoolBinaryOp Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_MathBoolAnd>();


#define FRigUnit_MathBoolNot_Execute() \
	void FRigUnit_MathBoolNot::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const bool Value, \
		bool& Result, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_MathBool_h_105_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_MathBoolNot_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const bool Value, \
		bool& Result, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const bool Value = *(bool*)RigVMMemoryHandles[0].GetData(); \
		bool& Result = *(bool*)RigVMMemoryHandles[1].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Value, \
			Result, \
			Context \
		); \
	} \
	typedef FRigUnit_MathBoolUnaryOp Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_MathBoolNot>();


#define FRigUnit_MathBoolConstFalse_Execute() \
	void FRigUnit_MathBoolConstFalse::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		bool& Value, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_MathBool_h_88_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_MathBoolConstFalse_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		bool& Value, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		bool& Value = *(bool*)RigVMMemoryHandles[0].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Value, \
			Context \
		); \
	} \
	typedef FRigUnit_MathBoolConstant Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_MathBoolConstFalse>();


#define FRigUnit_MathBoolConstTrue_Execute() \
	void FRigUnit_MathBoolConstTrue::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		bool& Value, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_MathBool_h_71_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_MathBoolConstTrue_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		bool& Value, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		bool& Value = *(bool*)RigVMMemoryHandles[0].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Value, \
			Context \
		); \
	} \
	typedef FRigUnit_MathBoolConstant Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_MathBoolConstTrue>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_MathBool_h_48_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	typedef FRigUnit_MathBoolBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_MathBoolBinaryOp>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_MathBool_h_31_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_MathBoolUnaryOp_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	typedef FRigUnit_MathBoolBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_MathBoolUnaryOp>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_MathBool_h_17_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_MathBoolConstant_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	typedef FRigUnit_MathBoolBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_MathBoolConstant>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_MathBool_h_11_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_MathBoolBase_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	typedef FRigUnit_MathBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_MathBoolBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_MathBool_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
