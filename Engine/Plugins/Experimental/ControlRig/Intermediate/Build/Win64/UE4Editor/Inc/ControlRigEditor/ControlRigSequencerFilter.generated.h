// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIGEDITOR_ControlRigSequencerFilter_generated_h
#error "ControlRigSequencerFilter.generated.h already included, missing '#pragma once' in ControlRigSequencerFilter.h"
#endif
#define CONTROLRIGEDITOR_ControlRigSequencerFilter_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequencerFilter_h_15_SPARSE_DATA
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequencerFilter_h_15_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequencerFilter_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequencerFilter_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUControlRigTrackFilter(); \
	friend struct Z_Construct_UClass_UControlRigTrackFilter_Statics; \
public: \
	DECLARE_CLASS(UControlRigTrackFilter, USequencerTrackFilterExtension, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRigEditor"), NO_API) \
	DECLARE_SERIALIZER(UControlRigTrackFilter)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequencerFilter_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUControlRigTrackFilter(); \
	friend struct Z_Construct_UClass_UControlRigTrackFilter_Statics; \
public: \
	DECLARE_CLASS(UControlRigTrackFilter, USequencerTrackFilterExtension, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRigEditor"), NO_API) \
	DECLARE_SERIALIZER(UControlRigTrackFilter)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequencerFilter_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UControlRigTrackFilter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigTrackFilter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigTrackFilter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigTrackFilter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigTrackFilter(UControlRigTrackFilter&&); \
	NO_API UControlRigTrackFilter(const UControlRigTrackFilter&); \
public:


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequencerFilter_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UControlRigTrackFilter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigTrackFilter(UControlRigTrackFilter&&); \
	NO_API UControlRigTrackFilter(const UControlRigTrackFilter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigTrackFilter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigTrackFilter); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigTrackFilter)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequencerFilter_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequencerFilter_h_11_PROLOG
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequencerFilter_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequencerFilter_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequencerFilter_h_15_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequencerFilter_h_15_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequencerFilter_h_15_INCLASS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequencerFilter_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequencerFilter_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequencerFilter_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequencerFilter_h_15_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequencerFilter_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequencerFilter_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequencerFilter_h_15_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONTROLRIGEDITOR_API UClass* StaticClass<class UControlRigTrackFilter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequencerFilter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
