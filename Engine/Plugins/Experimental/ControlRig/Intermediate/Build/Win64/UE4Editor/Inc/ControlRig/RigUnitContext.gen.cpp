// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Public/Units/RigUnitContext.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnitContext() {}
// Cross Module References
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_EControlRigState();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
// End Cross Module References
	static UEnum* EControlRigState_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ControlRig_EControlRigState, Z_Construct_UPackage__Script_ControlRig(), TEXT("EControlRigState"));
		}
		return Singleton;
	}
	template<> CONTROLRIG_API UEnum* StaticEnum<EControlRigState>()
	{
		return EControlRigState_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EControlRigState(EControlRigState_StaticEnum, TEXT("/Script/ControlRig"), TEXT("EControlRigState"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ControlRig_EControlRigState_Hash() { return 2622840577U; }
	UEnum* Z_Construct_UEnum_ControlRig_EControlRigState()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EControlRigState"), 0, Get_Z_Construct_UEnum_ControlRig_EControlRigState_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EControlRigState::Init", (int64)EControlRigState::Init },
				{ "EControlRigState::Update", (int64)EControlRigState::Update },
				{ "EControlRigState::Invalid", (int64)EControlRigState::Invalid },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** Current state of rig\n*\x09What  state Control Rig currently is\n*/" },
				{ "Init.Name", "EControlRigState::Init" },
				{ "Invalid.Name", "EControlRigState::Invalid" },
				{ "ModuleRelativePath", "Public/Units/RigUnitContext.h" },
				{ "ToolTip", "Current state of rig\n    What  state Control Rig currently is" },
				{ "Update.Name", "EControlRigState::Update" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ControlRig,
				nullptr,
				"EControlRigState",
				"EControlRigState",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
