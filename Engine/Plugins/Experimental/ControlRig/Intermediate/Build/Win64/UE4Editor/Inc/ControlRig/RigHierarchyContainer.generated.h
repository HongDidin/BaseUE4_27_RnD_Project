// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigHierarchyContainer_generated_h
#error "RigHierarchyContainer.generated.h already included, missing '#pragma once' in RigHierarchyContainer.h"
#endif
#define CONTROLRIG_RigHierarchyContainer_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_RigHierarchyContainer_h_381_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigHierarchyRef_Statics; \
	static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigHierarchyRef>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_RigHierarchyContainer_h_88_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigHierarchyContainer_Statics; \
	static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigHierarchyContainer>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_RigHierarchyContainer_h_55_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigMirrorSettings_Statics; \
	static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigMirrorSettings>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_RigHierarchyContainer_h_37_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Statics; \
	static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigHierarchyCopyPasteContent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_RigHierarchyContainer_h


#define FOREACH_ENUM_ERIGHIERARCHYIMPORTMODE(op) \
	op(ERigHierarchyImportMode::Append) \
	op(ERigHierarchyImportMode::Replace) \
	op(ERigHierarchyImportMode::ReplaceLocalTransform) \
	op(ERigHierarchyImportMode::ReplaceGlobalTransform) \
	op(ERigHierarchyImportMode::Max) 

enum class ERigHierarchyImportMode : uint8;
template<> CONTROLRIG_API UEnum* StaticEnum<ERigHierarchyImportMode>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
