// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Public/Rigs/AdditiveControlRig.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAdditiveControlRig() {}
// Cross Module References
	CONTROLRIG_API UClass* Z_Construct_UClass_UAdditiveControlRig_NoRegister();
	CONTROLRIG_API UClass* Z_Construct_UClass_UAdditiveControlRig();
	CONTROLRIG_API UClass* Z_Construct_UClass_UControlRig();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
// End Cross Module References
	void UAdditiveControlRig::StaticRegisterNativesUAdditiveControlRig()
	{
	}
	UClass* Z_Construct_UClass_UAdditiveControlRig_NoRegister()
	{
		return UAdditiveControlRig::StaticClass();
	}
	struct Z_Construct_UClass_UAdditiveControlRig_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAdditiveControlRig_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UControlRig,
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAdditiveControlRig_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Rig that allows additive layer editing per joint */" },
		{ "IncludePath", "Rigs/AdditiveControlRig.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Public/Rigs/AdditiveControlRig.h" },
		{ "ToolTip", "Rig that allows additive layer editing per joint" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAdditiveControlRig_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAdditiveControlRig>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAdditiveControlRig_Statics::ClassParams = {
		&UAdditiveControlRig::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UAdditiveControlRig_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAdditiveControlRig_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAdditiveControlRig()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAdditiveControlRig_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAdditiveControlRig, 849340919);
	template<> CONTROLRIG_API UClass* StaticClass<UAdditiveControlRig>()
	{
		return UAdditiveControlRig::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAdditiveControlRig(Z_Construct_UClass_UAdditiveControlRig, &UAdditiveControlRig::StaticClass, TEXT("/Script/ControlRig"), TEXT("UAdditiveControlRig"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAdditiveControlRig);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
