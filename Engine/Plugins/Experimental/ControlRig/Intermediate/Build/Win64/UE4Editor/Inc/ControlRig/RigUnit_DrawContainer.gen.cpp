// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Drawing/RigUnit_DrawContainer.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_DrawContainer() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetTransform();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnitMutable();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetThickness();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetColor();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_DrawContainerGetInstruction();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_DrawContainerSetTransform>() == std::is_polymorphic<FRigUnitMutable>(), "USTRUCT FRigUnit_DrawContainerSetTransform cannot be polymorphic unless super FRigUnitMutable is polymorphic");

class UScriptStruct* FRigUnit_DrawContainerSetTransform::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetTransform_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetTransform, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_DrawContainerSetTransform"), sizeof(FRigUnit_DrawContainerSetTransform), Get_Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetTransform_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_DrawContainerSetTransform::Execute"), &FRigUnit_DrawContainerSetTransform::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_DrawContainerSetTransform>()
{
	return FRigUnit_DrawContainerSetTransform::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_DrawContainerSetTransform(FRigUnit_DrawContainerSetTransform::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_DrawContainerSetTransform"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DrawContainerSetTransform
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DrawContainerSetTransform()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_DrawContainerSetTransform>(FName(TEXT("RigUnit_DrawContainerSetTransform")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DrawContainerSetTransform;
	struct Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetTransform_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InstructionName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_InstructionName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Transform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Transform;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetTransform_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Drawing" },
		{ "Comment", "/**\n * Set Imported Draw Container curve transform\n */" },
		{ "DisplayName", "Set Draw Transform" },
		{ "Keywords", "Curve,Shape" },
		{ "ModuleRelativePath", "Private/Units/Drawing/RigUnit_DrawContainer.h" },
		{ "NodeColor", "0.25 0.25 0.05" },
		{ "ToolTip", "Set Imported Draw Container curve transform" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetTransform_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_DrawContainerSetTransform>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetTransform_Statics::NewProp_InstructionName_MetaData[] = {
		{ "Constant", "" },
		{ "CustomWidget", "DrawingName" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Drawing/RigUnit_DrawContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetTransform_Statics::NewProp_InstructionName = { "InstructionName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DrawContainerSetTransform, InstructionName), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetTransform_Statics::NewProp_InstructionName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetTransform_Statics::NewProp_InstructionName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetTransform_Statics::NewProp_Transform_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Drawing/RigUnit_DrawContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetTransform_Statics::NewProp_Transform = { "Transform", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DrawContainerSetTransform, Transform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetTransform_Statics::NewProp_Transform_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetTransform_Statics::NewProp_Transform_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetTransform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetTransform_Statics::NewProp_InstructionName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetTransform_Statics::NewProp_Transform,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetTransform_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnitMutable,
		&NewStructOps,
		"RigUnit_DrawContainerSetTransform",
		sizeof(FRigUnit_DrawContainerSetTransform),
		alignof(FRigUnit_DrawContainerSetTransform),
		Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetTransform_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetTransform_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetTransform_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetTransform_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetTransform()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetTransform_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_DrawContainerSetTransform"), sizeof(FRigUnit_DrawContainerSetTransform), Get_Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetTransform_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetTransform_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetTransform_Hash() { return 1694262000U; }

void FRigUnit_DrawContainerSetTransform::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		InstructionName,
		Transform,
		ExecuteContext,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_DrawContainerSetThickness>() == std::is_polymorphic<FRigUnitMutable>(), "USTRUCT FRigUnit_DrawContainerSetThickness cannot be polymorphic unless super FRigUnitMutable is polymorphic");

class UScriptStruct* FRigUnit_DrawContainerSetThickness::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetThickness_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetThickness, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_DrawContainerSetThickness"), sizeof(FRigUnit_DrawContainerSetThickness), Get_Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetThickness_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_DrawContainerSetThickness::Execute"), &FRigUnit_DrawContainerSetThickness::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_DrawContainerSetThickness>()
{
	return FRigUnit_DrawContainerSetThickness::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_DrawContainerSetThickness(FRigUnit_DrawContainerSetThickness::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_DrawContainerSetThickness"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DrawContainerSetThickness
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DrawContainerSetThickness()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_DrawContainerSetThickness>(FName(TEXT("RigUnit_DrawContainerSetThickness")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DrawContainerSetThickness;
	struct Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetThickness_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InstructionName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_InstructionName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Thickness_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Thickness;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetThickness_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Drawing" },
		{ "Comment", "/**\n * Set Imported Draw Container curve thickness\n */" },
		{ "DisplayName", "Set Draw Thickness" },
		{ "Keywords", "Curve,Shape" },
		{ "ModuleRelativePath", "Private/Units/Drawing/RigUnit_DrawContainer.h" },
		{ "NodeColor", "0.25 0.25 0.05" },
		{ "ToolTip", "Set Imported Draw Container curve thickness" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetThickness_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_DrawContainerSetThickness>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetThickness_Statics::NewProp_InstructionName_MetaData[] = {
		{ "Constant", "" },
		{ "CustomWidget", "DrawingName" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Drawing/RigUnit_DrawContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetThickness_Statics::NewProp_InstructionName = { "InstructionName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DrawContainerSetThickness, InstructionName), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetThickness_Statics::NewProp_InstructionName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetThickness_Statics::NewProp_InstructionName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetThickness_Statics::NewProp_Thickness_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Drawing/RigUnit_DrawContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetThickness_Statics::NewProp_Thickness = { "Thickness", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DrawContainerSetThickness, Thickness), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetThickness_Statics::NewProp_Thickness_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetThickness_Statics::NewProp_Thickness_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetThickness_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetThickness_Statics::NewProp_InstructionName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetThickness_Statics::NewProp_Thickness,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetThickness_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnitMutable,
		&NewStructOps,
		"RigUnit_DrawContainerSetThickness",
		sizeof(FRigUnit_DrawContainerSetThickness),
		alignof(FRigUnit_DrawContainerSetThickness),
		Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetThickness_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetThickness_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetThickness_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetThickness_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetThickness()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetThickness_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_DrawContainerSetThickness"), sizeof(FRigUnit_DrawContainerSetThickness), Get_Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetThickness_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetThickness_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetThickness_Hash() { return 2005858177U; }

void FRigUnit_DrawContainerSetThickness::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		InstructionName,
		Thickness,
		ExecuteContext,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_DrawContainerSetColor>() == std::is_polymorphic<FRigUnitMutable>(), "USTRUCT FRigUnit_DrawContainerSetColor cannot be polymorphic unless super FRigUnitMutable is polymorphic");

class UScriptStruct* FRigUnit_DrawContainerSetColor::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetColor_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetColor, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_DrawContainerSetColor"), sizeof(FRigUnit_DrawContainerSetColor), Get_Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetColor_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_DrawContainerSetColor::Execute"), &FRigUnit_DrawContainerSetColor::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_DrawContainerSetColor>()
{
	return FRigUnit_DrawContainerSetColor::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_DrawContainerSetColor(FRigUnit_DrawContainerSetColor::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_DrawContainerSetColor"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DrawContainerSetColor
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DrawContainerSetColor()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_DrawContainerSetColor>(FName(TEXT("RigUnit_DrawContainerSetColor")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DrawContainerSetColor;
	struct Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetColor_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InstructionName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_InstructionName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Color_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Color;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetColor_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Drawing" },
		{ "Comment", "/**\n * Set Imported Draw Container curve color\n */" },
		{ "DisplayName", "Set Draw Color" },
		{ "Keywords", "Curve,Shape" },
		{ "ModuleRelativePath", "Private/Units/Drawing/RigUnit_DrawContainer.h" },
		{ "NodeColor", "0.25 0.25 0.05" },
		{ "ToolTip", "Set Imported Draw Container curve color" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetColor_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_DrawContainerSetColor>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetColor_Statics::NewProp_InstructionName_MetaData[] = {
		{ "Constant", "" },
		{ "CustomWidget", "DrawingName" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Drawing/RigUnit_DrawContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetColor_Statics::NewProp_InstructionName = { "InstructionName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DrawContainerSetColor, InstructionName), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetColor_Statics::NewProp_InstructionName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetColor_Statics::NewProp_InstructionName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetColor_Statics::NewProp_Color_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Drawing/RigUnit_DrawContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetColor_Statics::NewProp_Color = { "Color", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DrawContainerSetColor, Color), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetColor_Statics::NewProp_Color_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetColor_Statics::NewProp_Color_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetColor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetColor_Statics::NewProp_InstructionName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetColor_Statics::NewProp_Color,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetColor_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnitMutable,
		&NewStructOps,
		"RigUnit_DrawContainerSetColor",
		sizeof(FRigUnit_DrawContainerSetColor),
		alignof(FRigUnit_DrawContainerSetColor),
		Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetColor_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetColor_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetColor_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetColor_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetColor()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetColor_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_DrawContainerSetColor"), sizeof(FRigUnit_DrawContainerSetColor), Get_Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetColor_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetColor_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DrawContainerSetColor_Hash() { return 1518127228U; }

void FRigUnit_DrawContainerSetColor::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		InstructionName,
		Color,
		ExecuteContext,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_DrawContainerGetInstruction>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_DrawContainerGetInstruction cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_DrawContainerGetInstruction::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DrawContainerGetInstruction_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_DrawContainerGetInstruction, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_DrawContainerGetInstruction"), sizeof(FRigUnit_DrawContainerGetInstruction), Get_Z_Construct_UScriptStruct_FRigUnit_DrawContainerGetInstruction_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_DrawContainerGetInstruction::Execute"), &FRigUnit_DrawContainerGetInstruction::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_DrawContainerGetInstruction>()
{
	return FRigUnit_DrawContainerGetInstruction::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_DrawContainerGetInstruction(FRigUnit_DrawContainerGetInstruction::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_DrawContainerGetInstruction"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DrawContainerGetInstruction
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DrawContainerGetInstruction()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_DrawContainerGetInstruction>(FName(TEXT("RigUnit_DrawContainerGetInstruction")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DrawContainerGetInstruction;
	struct Z_Construct_UScriptStruct_FRigUnit_DrawContainerGetInstruction_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InstructionName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_InstructionName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Color_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Color;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Transform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Transform;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DrawContainerGetInstruction_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Drawing" },
		{ "Comment", "/**\n * Get Imported Draw Container curve transform and color\n */" },
		{ "DisplayName", "Get Draw Instruction" },
		{ "Keywords", "Curve,Shape" },
		{ "ModuleRelativePath", "Private/Units/Drawing/RigUnit_DrawContainer.h" },
		{ "NodeColor", "0.25 0.25 0.05" },
		{ "ToolTip", "Get Imported Draw Container curve transform and color" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_DrawContainerGetInstruction_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_DrawContainerGetInstruction>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DrawContainerGetInstruction_Statics::NewProp_InstructionName_MetaData[] = {
		{ "Constant", "" },
		{ "CustomWidget", "DrawingName" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Drawing/RigUnit_DrawContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_DrawContainerGetInstruction_Statics::NewProp_InstructionName = { "InstructionName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DrawContainerGetInstruction, InstructionName), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DrawContainerGetInstruction_Statics::NewProp_InstructionName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DrawContainerGetInstruction_Statics::NewProp_InstructionName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DrawContainerGetInstruction_Statics::NewProp_Color_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Drawing/RigUnit_DrawContainer.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_DrawContainerGetInstruction_Statics::NewProp_Color = { "Color", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DrawContainerGetInstruction, Color), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DrawContainerGetInstruction_Statics::NewProp_Color_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DrawContainerGetInstruction_Statics::NewProp_Color_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DrawContainerGetInstruction_Statics::NewProp_Transform_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Drawing/RigUnit_DrawContainer.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_DrawContainerGetInstruction_Statics::NewProp_Transform = { "Transform", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DrawContainerGetInstruction, Transform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DrawContainerGetInstruction_Statics::NewProp_Transform_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DrawContainerGetInstruction_Statics::NewProp_Transform_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_DrawContainerGetInstruction_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DrawContainerGetInstruction_Statics::NewProp_InstructionName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DrawContainerGetInstruction_Statics::NewProp_Color,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DrawContainerGetInstruction_Statics::NewProp_Transform,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_DrawContainerGetInstruction_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_DrawContainerGetInstruction",
		sizeof(FRigUnit_DrawContainerGetInstruction),
		alignof(FRigUnit_DrawContainerGetInstruction),
		Z_Construct_UScriptStruct_FRigUnit_DrawContainerGetInstruction_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DrawContainerGetInstruction_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DrawContainerGetInstruction_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DrawContainerGetInstruction_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_DrawContainerGetInstruction()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DrawContainerGetInstruction_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_DrawContainerGetInstruction"), sizeof(FRigUnit_DrawContainerGetInstruction), Get_Z_Construct_UScriptStruct_FRigUnit_DrawContainerGetInstruction_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_DrawContainerGetInstruction_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DrawContainerGetInstruction_Hash() { return 3872034997U; }

void FRigUnit_DrawContainerGetInstruction::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		InstructionName,
		Color,
		Transform,
		Context
	);
}

PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
