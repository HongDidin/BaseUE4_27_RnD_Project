// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Execution/RigUnit_Hierarchy.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_Hierarchy() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_HierarchyBase();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigElementKey();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigElementKeyCollection();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FCachedRigElement();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParent();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_HierarchyGetSiblings>() == std::is_polymorphic<FRigUnit_HierarchyBase>(), "USTRUCT FRigUnit_HierarchyGetSiblings cannot be polymorphic unless super FRigUnit_HierarchyBase is polymorphic");

class UScriptStruct* FRigUnit_HierarchyGetSiblings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_HierarchyGetSiblings"), sizeof(FRigUnit_HierarchyGetSiblings), Get_Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_HierarchyGetSiblings::Execute"), &FRigUnit_HierarchyGetSiblings::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_HierarchyGetSiblings>()
{
	return FRigUnit_HierarchyGetSiblings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_HierarchyGetSiblings(FRigUnit_HierarchyGetSiblings::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_HierarchyGetSiblings"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_HierarchyGetSiblings
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_HierarchyGetSiblings()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_HierarchyGetSiblings>(FName(TEXT("RigUnit_HierarchyGetSiblings")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_HierarchyGetSiblings;
	struct Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Item_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Item;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIncludeItem_MetaData[];
#endif
		static void NewProp_bIncludeItem_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIncludeItem;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Siblings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Siblings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedItem_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CachedItem;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedSiblings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CachedSiblings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the item's siblings\n */" },
		{ "DisplayName", "Get Siblings" },
		{ "Keywords", "Chain,Siblings,Hierarchy" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Hierarchy.h" },
		{ "ToolTip", "Returns the item's siblings" },
		{ "Varying", "" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_HierarchyGetSiblings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Statics::NewProp_Item_MetaData[] = {
		{ "ExpandByDefault", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Hierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Statics::NewProp_Item = { "Item", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_HierarchyGetSiblings, Item), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Statics::NewProp_Item_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Statics::NewProp_Item_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Statics::NewProp_bIncludeItem_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Hierarchy.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Statics::NewProp_bIncludeItem_SetBit(void* Obj)
	{
		((FRigUnit_HierarchyGetSiblings*)Obj)->bIncludeItem = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Statics::NewProp_bIncludeItem = { "bIncludeItem", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_HierarchyGetSiblings), &Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Statics::NewProp_bIncludeItem_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Statics::NewProp_bIncludeItem_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Statics::NewProp_bIncludeItem_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Statics::NewProp_Siblings_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Hierarchy.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Statics::NewProp_Siblings = { "Siblings", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_HierarchyGetSiblings, Siblings), Z_Construct_UScriptStruct_FRigElementKeyCollection, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Statics::NewProp_Siblings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Statics::NewProp_Siblings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Statics::NewProp_CachedItem_MetaData[] = {
		{ "Comment", "// Used to cache the internally\n" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Hierarchy.h" },
		{ "ToolTip", "Used to cache the internally" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Statics::NewProp_CachedItem = { "CachedItem", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_HierarchyGetSiblings, CachedItem), Z_Construct_UScriptStruct_FCachedRigElement, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Statics::NewProp_CachedItem_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Statics::NewProp_CachedItem_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Statics::NewProp_CachedSiblings_MetaData[] = {
		{ "Comment", "// Used to cache the internally\n" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Hierarchy.h" },
		{ "ToolTip", "Used to cache the internally" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Statics::NewProp_CachedSiblings = { "CachedSiblings", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_HierarchyGetSiblings, CachedSiblings), Z_Construct_UScriptStruct_FRigElementKeyCollection, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Statics::NewProp_CachedSiblings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Statics::NewProp_CachedSiblings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Statics::NewProp_Item,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Statics::NewProp_bIncludeItem,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Statics::NewProp_Siblings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Statics::NewProp_CachedItem,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Statics::NewProp_CachedSiblings,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_HierarchyBase,
		&NewStructOps,
		"RigUnit_HierarchyGetSiblings",
		sizeof(FRigUnit_HierarchyGetSiblings),
		alignof(FRigUnit_HierarchyGetSiblings),
		Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_HierarchyGetSiblings"), sizeof(FRigUnit_HierarchyGetSiblings), Get_Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Hash() { return 3318186731U; }

void FRigUnit_HierarchyGetSiblings::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Item,
		bIncludeItem,
		Siblings,
		CachedItem,
		CachedSiblings,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_HierarchyGetChildren>() == std::is_polymorphic<FRigUnit_HierarchyBase>(), "USTRUCT FRigUnit_HierarchyGetChildren cannot be polymorphic unless super FRigUnit_HierarchyBase is polymorphic");

class UScriptStruct* FRigUnit_HierarchyGetChildren::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_HierarchyGetChildren"), sizeof(FRigUnit_HierarchyGetChildren), Get_Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_HierarchyGetChildren::Execute"), &FRigUnit_HierarchyGetChildren::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_HierarchyGetChildren>()
{
	return FRigUnit_HierarchyGetChildren::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_HierarchyGetChildren(FRigUnit_HierarchyGetChildren::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_HierarchyGetChildren"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_HierarchyGetChildren
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_HierarchyGetChildren()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_HierarchyGetChildren>(FName(TEXT("RigUnit_HierarchyGetChildren")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_HierarchyGetChildren;
	struct Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Parent_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Parent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIncludeParent_MetaData[];
#endif
		static void NewProp_bIncludeParent_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIncludeParent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRecursive_MetaData[];
#endif
		static void NewProp_bRecursive_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRecursive;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Children_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Children;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedParent_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CachedParent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedChildren_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CachedChildren;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the item's children\n */" },
		{ "Deprecated", "4.25.0" },
		{ "DisplayName", "Get Children" },
		{ "Keywords", "Chain,Children,Hierarchy" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Hierarchy.h" },
		{ "ToolTip", "Returns the item's children" },
		{ "Varying", "" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_HierarchyGetChildren>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::NewProp_Parent_MetaData[] = {
		{ "ExpandByDefault", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Hierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::NewProp_Parent = { "Parent", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_HierarchyGetChildren, Parent), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::NewProp_Parent_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::NewProp_Parent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::NewProp_bIncludeParent_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Hierarchy.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::NewProp_bIncludeParent_SetBit(void* Obj)
	{
		((FRigUnit_HierarchyGetChildren*)Obj)->bIncludeParent = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::NewProp_bIncludeParent = { "bIncludeParent", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_HierarchyGetChildren), &Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::NewProp_bIncludeParent_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::NewProp_bIncludeParent_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::NewProp_bIncludeParent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::NewProp_bRecursive_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Hierarchy.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::NewProp_bRecursive_SetBit(void* Obj)
	{
		((FRigUnit_HierarchyGetChildren*)Obj)->bRecursive = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::NewProp_bRecursive = { "bRecursive", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_HierarchyGetChildren), &Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::NewProp_bRecursive_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::NewProp_bRecursive_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::NewProp_bRecursive_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::NewProp_Children_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Hierarchy.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::NewProp_Children = { "Children", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_HierarchyGetChildren, Children), Z_Construct_UScriptStruct_FRigElementKeyCollection, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::NewProp_Children_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::NewProp_Children_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::NewProp_CachedParent_MetaData[] = {
		{ "Comment", "// Used to cache the internally\n" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Hierarchy.h" },
		{ "ToolTip", "Used to cache the internally" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::NewProp_CachedParent = { "CachedParent", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_HierarchyGetChildren, CachedParent), Z_Construct_UScriptStruct_FCachedRigElement, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::NewProp_CachedParent_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::NewProp_CachedParent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::NewProp_CachedChildren_MetaData[] = {
		{ "Comment", "// Used to cache the internally\n" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Hierarchy.h" },
		{ "ToolTip", "Used to cache the internally" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::NewProp_CachedChildren = { "CachedChildren", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_HierarchyGetChildren, CachedChildren), Z_Construct_UScriptStruct_FRigElementKeyCollection, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::NewProp_CachedChildren_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::NewProp_CachedChildren_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::NewProp_Parent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::NewProp_bIncludeParent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::NewProp_bRecursive,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::NewProp_Children,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::NewProp_CachedParent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::NewProp_CachedChildren,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_HierarchyBase,
		&NewStructOps,
		"RigUnit_HierarchyGetChildren",
		sizeof(FRigUnit_HierarchyGetChildren),
		alignof(FRigUnit_HierarchyGetChildren),
		Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_HierarchyGetChildren"), sizeof(FRigUnit_HierarchyGetChildren), Get_Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Hash() { return 829645703U; }

void FRigUnit_HierarchyGetChildren::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Parent,
		bIncludeParent,
		bRecursive,
		Children,
		CachedParent,
		CachedChildren,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_HierarchyGetParents>() == std::is_polymorphic<FRigUnit_HierarchyBase>(), "USTRUCT FRigUnit_HierarchyGetParents cannot be polymorphic unless super FRigUnit_HierarchyBase is polymorphic");

class UScriptStruct* FRigUnit_HierarchyGetParents::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_HierarchyGetParents"), sizeof(FRigUnit_HierarchyGetParents), Get_Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_HierarchyGetParents::Execute"), &FRigUnit_HierarchyGetParents::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_HierarchyGetParents>()
{
	return FRigUnit_HierarchyGetParents::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_HierarchyGetParents(FRigUnit_HierarchyGetParents::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_HierarchyGetParents"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_HierarchyGetParents
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_HierarchyGetParents()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_HierarchyGetParents>(FName(TEXT("RigUnit_HierarchyGetParents")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_HierarchyGetParents;
	struct Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Child_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Child;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIncludeChild_MetaData[];
#endif
		static void NewProp_bIncludeChild_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIncludeChild;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bReverse_MetaData[];
#endif
		static void NewProp_bReverse_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bReverse;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Parents_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Parents;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedChild_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CachedChild;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedParents_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CachedParents;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the item's parents\n */" },
		{ "DisplayName", "Get Parents" },
		{ "Keywords", "Chain,Parents,Hierarchy" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Hierarchy.h" },
		{ "ToolTip", "Returns the item's parents" },
		{ "Varying", "" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_HierarchyGetParents>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::NewProp_Child_MetaData[] = {
		{ "ExpandByDefault", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Hierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::NewProp_Child = { "Child", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_HierarchyGetParents, Child), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::NewProp_Child_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::NewProp_Child_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::NewProp_bIncludeChild_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Hierarchy.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::NewProp_bIncludeChild_SetBit(void* Obj)
	{
		((FRigUnit_HierarchyGetParents*)Obj)->bIncludeChild = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::NewProp_bIncludeChild = { "bIncludeChild", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_HierarchyGetParents), &Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::NewProp_bIncludeChild_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::NewProp_bIncludeChild_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::NewProp_bIncludeChild_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::NewProp_bReverse_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Hierarchy.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::NewProp_bReverse_SetBit(void* Obj)
	{
		((FRigUnit_HierarchyGetParents*)Obj)->bReverse = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::NewProp_bReverse = { "bReverse", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_HierarchyGetParents), &Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::NewProp_bReverse_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::NewProp_bReverse_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::NewProp_bReverse_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::NewProp_Parents_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Hierarchy.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::NewProp_Parents = { "Parents", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_HierarchyGetParents, Parents), Z_Construct_UScriptStruct_FRigElementKeyCollection, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::NewProp_Parents_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::NewProp_Parents_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::NewProp_CachedChild_MetaData[] = {
		{ "Comment", "// Used to cache the internally\n" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Hierarchy.h" },
		{ "ToolTip", "Used to cache the internally" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::NewProp_CachedChild = { "CachedChild", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_HierarchyGetParents, CachedChild), Z_Construct_UScriptStruct_FCachedRigElement, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::NewProp_CachedChild_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::NewProp_CachedChild_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::NewProp_CachedParents_MetaData[] = {
		{ "Comment", "// Used to cache the internally\n" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Hierarchy.h" },
		{ "ToolTip", "Used to cache the internally" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::NewProp_CachedParents = { "CachedParents", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_HierarchyGetParents, CachedParents), Z_Construct_UScriptStruct_FRigElementKeyCollection, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::NewProp_CachedParents_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::NewProp_CachedParents_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::NewProp_Child,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::NewProp_bIncludeChild,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::NewProp_bReverse,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::NewProp_Parents,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::NewProp_CachedChild,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::NewProp_CachedParents,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_HierarchyBase,
		&NewStructOps,
		"RigUnit_HierarchyGetParents",
		sizeof(FRigUnit_HierarchyGetParents),
		alignof(FRigUnit_HierarchyGetParents),
		Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_HierarchyGetParents"), sizeof(FRigUnit_HierarchyGetParents), Get_Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Hash() { return 1586407377U; }

void FRigUnit_HierarchyGetParents::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Child,
		bIncludeChild,
		bReverse,
		Parents,
		CachedChild,
		CachedParents,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_HierarchyGetParent>() == std::is_polymorphic<FRigUnit_HierarchyBase>(), "USTRUCT FRigUnit_HierarchyGetParent cannot be polymorphic unless super FRigUnit_HierarchyBase is polymorphic");

class UScriptStruct* FRigUnit_HierarchyGetParent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParent, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_HierarchyGetParent"), sizeof(FRigUnit_HierarchyGetParent), Get_Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParent_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_HierarchyGetParent::Execute"), &FRigUnit_HierarchyGetParent::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_HierarchyGetParent>()
{
	return FRigUnit_HierarchyGetParent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_HierarchyGetParent(FRigUnit_HierarchyGetParent::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_HierarchyGetParent"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_HierarchyGetParent
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_HierarchyGetParent()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_HierarchyGetParent>(FName(TEXT("RigUnit_HierarchyGetParent")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_HierarchyGetParent;
	struct Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Child_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Child;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Parent_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Parent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedChild_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CachedChild;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedParent_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CachedParent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParent_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the item's parent\n */" },
		{ "DisplayName", "Get Parent" },
		{ "Keywords", "Child,Parent,Root,Up,Top" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Hierarchy.h" },
		{ "ToolTip", "Returns the item's parent" },
		{ "Varying", "" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_HierarchyGetParent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParent_Statics::NewProp_Child_MetaData[] = {
		{ "ExpandByDefault", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Hierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParent_Statics::NewProp_Child = { "Child", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_HierarchyGetParent, Child), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParent_Statics::NewProp_Child_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParent_Statics::NewProp_Child_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParent_Statics::NewProp_Parent_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Hierarchy.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParent_Statics::NewProp_Parent = { "Parent", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_HierarchyGetParent, Parent), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParent_Statics::NewProp_Parent_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParent_Statics::NewProp_Parent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParent_Statics::NewProp_CachedChild_MetaData[] = {
		{ "Comment", "// Used to cache the internally\n" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Hierarchy.h" },
		{ "ToolTip", "Used to cache the internally" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParent_Statics::NewProp_CachedChild = { "CachedChild", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_HierarchyGetParent, CachedChild), Z_Construct_UScriptStruct_FCachedRigElement, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParent_Statics::NewProp_CachedChild_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParent_Statics::NewProp_CachedChild_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParent_Statics::NewProp_CachedParent_MetaData[] = {
		{ "Comment", "// Used to cache the internally\n" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Hierarchy.h" },
		{ "ToolTip", "Used to cache the internally" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParent_Statics::NewProp_CachedParent = { "CachedParent", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_HierarchyGetParent, CachedParent), Z_Construct_UScriptStruct_FCachedRigElement, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParent_Statics::NewProp_CachedParent_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParent_Statics::NewProp_CachedParent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParent_Statics::NewProp_Child,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParent_Statics::NewProp_Parent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParent_Statics::NewProp_CachedChild,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParent_Statics::NewProp_CachedParent,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_HierarchyBase,
		&NewStructOps,
		"RigUnit_HierarchyGetParent",
		sizeof(FRigUnit_HierarchyGetParent),
		alignof(FRigUnit_HierarchyGetParent),
		Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_HierarchyGetParent"), sizeof(FRigUnit_HierarchyGetParent), Get_Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParent_Hash() { return 930194329U; }

void FRigUnit_HierarchyGetParent::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Child,
		Parent,
		CachedChild,
		CachedParent,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_HierarchyBase>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_HierarchyBase cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_HierarchyBase::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_HierarchyBase_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_HierarchyBase, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_HierarchyBase"), sizeof(FRigUnit_HierarchyBase), Get_Z_Construct_UScriptStruct_FRigUnit_HierarchyBase_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_HierarchyBase>()
{
	return FRigUnit_HierarchyBase::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_HierarchyBase(FRigUnit_HierarchyBase::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_HierarchyBase"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_HierarchyBase
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_HierarchyBase()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_HierarchyBase>(FName(TEXT("RigUnit_HierarchyBase")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_HierarchyBase;
	struct Z_Construct_UScriptStruct_FRigUnit_HierarchyBase_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_HierarchyBase_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "Category", "Hierarchy" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Hierarchy.h" },
		{ "NodeColor", "0.7 0.05 0.5" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_HierarchyBase_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_HierarchyBase>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_HierarchyBase_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_HierarchyBase",
		sizeof(FRigUnit_HierarchyBase),
		alignof(FRigUnit_HierarchyBase),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_HierarchyBase_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_HierarchyBase_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_HierarchyBase()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_HierarchyBase_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_HierarchyBase"), sizeof(FRigUnit_HierarchyBase), Get_Z_Construct_UScriptStruct_FRigUnit_HierarchyBase_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_HierarchyBase_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_HierarchyBase_Hash() { return 601653884U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
