// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigUnit_PointSimulation_generated_h
#error "RigUnit_PointSimulation.generated.h already included, missing '#pragma once' in RigUnit_PointSimulation.h"
#endif
#define CONTROLRIG_RigUnit_PointSimulation_generated_h


#define FRigUnit_PointSimulation_Execute() \
	void FRigUnit_PointSimulation::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FCRSimPoint>& Points, \
		const FRigVMFixedArray<FCRSimLinearSpring>& Links, \
		const FRigVMFixedArray<FCRSimPointForce>& Forces, \
		const FRigVMFixedArray<FCRSimSoftCollision>& CollisionVolumes, \
		const float SimulatedStepsPerSecond, \
		const ECRSimPointIntegrateType IntegratorType, \
		const float VerletBlend, \
		const FRigVMFixedArray<FRigUnit_PointSimulation_BoneTarget>& BoneTargets, \
		const bool bLimitLocalPosition, \
		const bool bPropagateToChildren, \
		const FVector& PrimaryAimAxis, \
		const FVector& SecondaryAimAxis, \
		const FRigUnit_PointSimulation_DebugSettings& DebugSettings, \
		FCRFourPointBezier& Bezier, \
		FRigUnit_PointSimulation_WorkData& WorkData, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Simulation_RigUnit_PointSimulation_h_123_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FCRSimPoint>& Points, \
		const FRigVMFixedArray<FCRSimLinearSpring>& Links, \
		const FRigVMFixedArray<FCRSimPointForce>& Forces, \
		const FRigVMFixedArray<FCRSimSoftCollision>& CollisionVolumes, \
		const float SimulatedStepsPerSecond, \
		const ECRSimPointIntegrateType IntegratorType, \
		const float VerletBlend, \
		const FRigVMFixedArray<FRigUnit_PointSimulation_BoneTarget>& BoneTargets, \
		const bool bLimitLocalPosition, \
		const bool bPropagateToChildren, \
		const FVector& PrimaryAimAxis, \
		const FVector& SecondaryAimAxis, \
		const FRigUnit_PointSimulation_DebugSettings& DebugSettings, \
		FCRFourPointBezier& Bezier, \
		FRigUnit_PointSimulation_WorkData& WorkData, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		FRigVMFixedArray<FCRSimPoint> Points((FCRSimPoint*)RigVMMemoryHandles[0].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[1].GetData())); \
		FRigVMFixedArray<FCRSimLinearSpring> Links((FCRSimLinearSpring*)RigVMMemoryHandles[2].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[3].GetData())); \
		FRigVMFixedArray<FCRSimPointForce> Forces((FCRSimPointForce*)RigVMMemoryHandles[4].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[5].GetData())); \
		FRigVMFixedArray<FCRSimSoftCollision> CollisionVolumes((FCRSimSoftCollision*)RigVMMemoryHandles[6].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[7].GetData())); \
		const float SimulatedStepsPerSecond = *(float*)RigVMMemoryHandles[8].GetData(); \
		ECRSimPointIntegrateType IntegratorType = (ECRSimPointIntegrateType)*(uint8*)RigVMMemoryHandles[9].GetData(); \
		const float VerletBlend = *(float*)RigVMMemoryHandles[10].GetData(); \
		FRigVMFixedArray<FRigUnit_PointSimulation_BoneTarget> BoneTargets((FRigUnit_PointSimulation_BoneTarget*)RigVMMemoryHandles[11].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[12].GetData())); \
		const bool bLimitLocalPosition = *(bool*)RigVMMemoryHandles[13].GetData(); \
		const bool bPropagateToChildren = *(bool*)RigVMMemoryHandles[14].GetData(); \
		const FVector& PrimaryAimAxis = *(FVector*)RigVMMemoryHandles[15].GetData(); \
		const FVector& SecondaryAimAxis = *(FVector*)RigVMMemoryHandles[16].GetData(); \
		const FRigUnit_PointSimulation_DebugSettings& DebugSettings = *(FRigUnit_PointSimulation_DebugSettings*)RigVMMemoryHandles[17].GetData(); \
		FCRFourPointBezier& Bezier = *(FCRFourPointBezier*)RigVMMemoryHandles[18].GetData(); \
		FRigVMDynamicArray<FRigUnit_PointSimulation_WorkData> WorkData_19_Array(*((FRigVMByteArray*)RigVMMemoryHandles[19].GetData(0, false))); \
		WorkData_19_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FRigUnit_PointSimulation_WorkData& WorkData = WorkData_19_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[20].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Points, \
			Links, \
			Forces, \
			CollisionVolumes, \
			SimulatedStepsPerSecond, \
			IntegratorType, \
			VerletBlend, \
			BoneTargets, \
			bLimitLocalPosition, \
			bPropagateToChildren, \
			PrimaryAimAxis, \
			SecondaryAimAxis, \
			DebugSettings, \
			Bezier, \
			WorkData, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnit_SimBaseMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_PointSimulation>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Simulation_RigUnit_PointSimulation_h_107_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_PointSimulation_WorkData_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_PointSimulation_WorkData>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Simulation_RigUnit_PointSimulation_h_69_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_PointSimulation_BoneTarget_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_PointSimulation_BoneTarget>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Simulation_RigUnit_PointSimulation_h_17_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_PointSimulation_DebugSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Simulation_RigUnit_PointSimulation_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
