// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Public/ControlRig.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeControlRig() {}
// Cross Module References
	CONTROLRIG_API UClass* Z_Construct_UClass_UControlRig_NoRegister();
	CONTROLRIG_API UClass* Z_Construct_UClass_UControlRig();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_ERigExecutionType();
	RIGVM_API UClass* Z_Construct_UClass_URigVM_NoRegister();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigHierarchyContainer();
	CONTROLRIG_API UClass* Z_Construct_UClass_UControlRigGizmoLibrary_NoRegister();
	PROPERTYPATH_API UScriptStruct* Z_Construct_UScriptStruct_FCachedPropertyPath();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FControlRigDrawContainer();
	ANIMATIONCORE_API UClass* Z_Construct_UClass_UAnimationDataSourceRegistry_NoRegister();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigInfluenceMapPerEvent();
	ENGINE_API UClass* Z_Construct_UClass_UAssetUserData_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UNodeMappingProviderInterface_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UInterface_AssetUserData_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UControlRig::execSetInteractionRigClass)
	{
		P_GET_OBJECT(UClass,Z_Param_InInteractionRigClass);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetInteractionRigClass(Z_Param_InInteractionRigClass);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRig::execGetInteractionRigClass)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TSubclassOf<UControlRig> *)Z_Param__Result=P_THIS->GetInteractionRigClass();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRig::execSetInteractionRig)
	{
		P_GET_OBJECT(UControlRig,Z_Param_InInteractionRig);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetInteractionRig(Z_Param_InInteractionRig);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRig::execGetInteractionRig)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UControlRig**)Z_Param__Result=P_THIS->GetInteractionRig();
		P_NATIVE_END;
	}
	void UControlRig::StaticRegisterNativesUControlRig()
	{
		UClass* Class = UControlRig::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetInteractionRig", &UControlRig::execGetInteractionRig },
			{ "GetInteractionRigClass", &UControlRig::execGetInteractionRigClass },
			{ "SetInteractionRig", &UControlRig::execSetInteractionRig },
			{ "SetInteractionRigClass", &UControlRig::execSetInteractionRigClass },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UControlRig_GetInteractionRig_Statics
	{
		struct ControlRig_eventGetInteractionRig_Parms
		{
			UControlRig* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UControlRig_GetInteractionRig_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRig_eventGetInteractionRig_Parms, ReturnValue), Z_Construct_UClass_UControlRig_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRig_GetInteractionRig_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRig_GetInteractionRig_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRig_GetInteractionRig_Statics::Function_MetaDataParams[] = {
		{ "BlueprintGetter", "" },
		{ "ModuleRelativePath", "Public/ControlRig.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRig_GetInteractionRig_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRig, nullptr, "GetInteractionRig", nullptr, nullptr, sizeof(ControlRig_eventGetInteractionRig_Parms), Z_Construct_UFunction_UControlRig_GetInteractionRig_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRig_GetInteractionRig_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRig_GetInteractionRig_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRig_GetInteractionRig_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRig_GetInteractionRig()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRig_GetInteractionRig_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UControlRig_GetInteractionRigClass_Statics
	{
		struct ControlRig_eventGetInteractionRigClass_Parms
		{
			TSubclassOf<UControlRig>  ReturnValue;
		};
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UControlRig_GetInteractionRigClass_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0014000000000580, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRig_eventGetInteractionRigClass_Parms, ReturnValue), Z_Construct_UClass_UControlRig_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRig_GetInteractionRigClass_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRig_GetInteractionRigClass_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRig_GetInteractionRigClass_Statics::Function_MetaDataParams[] = {
		{ "BlueprintGetter", "" },
		{ "ModuleRelativePath", "Public/ControlRig.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRig_GetInteractionRigClass_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRig, nullptr, "GetInteractionRigClass", nullptr, nullptr, sizeof(ControlRig_eventGetInteractionRigClass_Parms), Z_Construct_UFunction_UControlRig_GetInteractionRigClass_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRig_GetInteractionRigClass_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRig_GetInteractionRigClass_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRig_GetInteractionRigClass_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRig_GetInteractionRigClass()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRig_GetInteractionRigClass_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UControlRig_SetInteractionRig_Statics
	{
		struct ControlRig_eventSetInteractionRig_Parms
		{
			UControlRig* InInteractionRig;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InInteractionRig;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UControlRig_SetInteractionRig_Statics::NewProp_InInteractionRig = { "InInteractionRig", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRig_eventSetInteractionRig_Parms, InInteractionRig), Z_Construct_UClass_UControlRig_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRig_SetInteractionRig_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRig_SetInteractionRig_Statics::NewProp_InInteractionRig,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRig_SetInteractionRig_Statics::Function_MetaDataParams[] = {
		{ "BlueprintSetter", "" },
		{ "ModuleRelativePath", "Public/ControlRig.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRig_SetInteractionRig_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRig, nullptr, "SetInteractionRig", nullptr, nullptr, sizeof(ControlRig_eventSetInteractionRig_Parms), Z_Construct_UFunction_UControlRig_SetInteractionRig_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRig_SetInteractionRig_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRig_SetInteractionRig_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRig_SetInteractionRig_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRig_SetInteractionRig()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRig_SetInteractionRig_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UControlRig_SetInteractionRigClass_Statics
	{
		struct ControlRig_eventSetInteractionRigClass_Parms
		{
			TSubclassOf<UControlRig>  InInteractionRigClass;
		};
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_InInteractionRigClass;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UControlRig_SetInteractionRigClass_Statics::NewProp_InInteractionRigClass = { "InInteractionRigClass", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRig_eventSetInteractionRigClass_Parms, InInteractionRigClass), Z_Construct_UClass_UControlRig_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRig_SetInteractionRigClass_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRig_SetInteractionRigClass_Statics::NewProp_InInteractionRigClass,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRig_SetInteractionRigClass_Statics::Function_MetaDataParams[] = {
		{ "BlueprintSetter", "" },
		{ "ModuleRelativePath", "Public/ControlRig.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRig_SetInteractionRigClass_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRig, nullptr, "SetInteractionRigClass", nullptr, nullptr, sizeof(ControlRig_eventSetInteractionRigClass_Parms), Z_Construct_UFunction_UControlRig_SetInteractionRigClass_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRig_SetInteractionRigClass_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRig_SetInteractionRigClass_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRig_SetInteractionRigClass_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRig_SetInteractionRigClass()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRig_SetInteractionRigClass_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UControlRig_NoRegister()
	{
		return UControlRig::StaticClass();
	}
	struct Z_Construct_UClass_UControlRig_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ExecutionType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExecutionType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ExecutionType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VM_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_VM;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Hierarchy_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Hierarchy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GizmoLibrary_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_GizmoLibrary;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InputProperties_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_InputProperties_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_InputProperties;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutputProperties_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_OutputProperties_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OutputProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_OutputProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DrawContainer_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DrawContainer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DataSourceRegistry_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DataSourceRegistry;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_EventQueue_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EventQueue_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_EventQueue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Influences_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Influences;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InteractionRig_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InteractionRig;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InteractionRigClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_InteractionRigClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AssetUserData_Inner_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AssetUserData_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AssetUserData_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_AssetUserData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UControlRig_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UControlRig_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UControlRig_GetInteractionRig, "GetInteractionRig" }, // 2145858241
		{ &Z_Construct_UFunction_UControlRig_GetInteractionRigClass, "GetInteractionRigClass" }, // 1829224393
		{ &Z_Construct_UFunction_UControlRig_SetInteractionRig, "SetInteractionRig" }, // 4114127537
		{ &Z_Construct_UFunction_UControlRig_SetInteractionRigClass, "SetInteractionRigClass" }, // 1281099971
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRig_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** Runs logic for mapping input data to transforms (the \"Rig\") */" },
		{ "IncludePath", "ControlRig.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/ControlRig.h" },
		{ "ToolTip", "Runs logic for mapping input data to transforms (the \"Rig\")" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UControlRig_Statics::NewProp_ExecutionType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRig_Statics::NewProp_ExecutionType_MetaData[] = {
		{ "Comment", "// END UObject interface\n" },
		{ "ModuleRelativePath", "Public/ControlRig.h" },
		{ "ToolTip", "END UObject interface" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UControlRig_Statics::NewProp_ExecutionType = { "ExecutionType", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRig, ExecutionType), Z_Construct_UEnum_ControlRig_ERigExecutionType, METADATA_PARAMS(Z_Construct_UClass_UControlRig_Statics::NewProp_ExecutionType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRig_Statics::NewProp_ExecutionType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRig_Statics::NewProp_VM_MetaData[] = {
		{ "ModuleRelativePath", "Public/ControlRig.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UControlRig_Statics::NewProp_VM = { "VM", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRig, VM), Z_Construct_UClass_URigVM_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UControlRig_Statics::NewProp_VM_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRig_Statics::NewProp_VM_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRig_Statics::NewProp_Hierarchy_MetaData[] = {
		{ "ModuleRelativePath", "Public/ControlRig.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UControlRig_Statics::NewProp_Hierarchy = { "Hierarchy", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRig, Hierarchy), Z_Construct_UScriptStruct_FRigHierarchyContainer, METADATA_PARAMS(Z_Construct_UClass_UControlRig_Statics::NewProp_Hierarchy_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRig_Statics::NewProp_Hierarchy_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRig_Statics::NewProp_GizmoLibrary_MetaData[] = {
		{ "ModuleRelativePath", "Public/ControlRig.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UControlRig_Statics::NewProp_GizmoLibrary = { "GizmoLibrary", nullptr, (EPropertyFlags)0x0044000000000000, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRig, GizmoLibrary), Z_Construct_UClass_UControlRigGizmoLibrary_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UControlRig_Statics::NewProp_GizmoLibrary_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRig_Statics::NewProp_GizmoLibrary_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UControlRig_Statics::NewProp_InputProperties_ValueProp = { "InputProperties", nullptr, (EPropertyFlags)0x0000000020000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FCachedPropertyPath, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UControlRig_Statics::NewProp_InputProperties_Key_KeyProp = { "InputProperties_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRig_Statics::NewProp_InputProperties_MetaData[] = {
		{ "Comment", "// you either go Input or Output, currently if you put it in both place, Output will override\n" },
		{ "ModuleRelativePath", "Public/ControlRig.h" },
		{ "ToolTip", "you either go Input or Output, currently if you put it in both place, Output will override" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UControlRig_Statics::NewProp_InputProperties = { "InputProperties", nullptr, (EPropertyFlags)0x0040000020000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRig, InputProperties_DEPRECATED), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UControlRig_Statics::NewProp_InputProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRig_Statics::NewProp_InputProperties_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UControlRig_Statics::NewProp_OutputProperties_ValueProp = { "OutputProperties", nullptr, (EPropertyFlags)0x0000000020000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FCachedPropertyPath, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UControlRig_Statics::NewProp_OutputProperties_Key_KeyProp = { "OutputProperties_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRig_Statics::NewProp_OutputProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/ControlRig.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UControlRig_Statics::NewProp_OutputProperties = { "OutputProperties", nullptr, (EPropertyFlags)0x0040000020000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRig, OutputProperties_DEPRECATED), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UControlRig_Statics::NewProp_OutputProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRig_Statics::NewProp_OutputProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRig_Statics::NewProp_DrawContainer_MetaData[] = {
		{ "ModuleRelativePath", "Public/ControlRig.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UControlRig_Statics::NewProp_DrawContainer = { "DrawContainer", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRig, DrawContainer), Z_Construct_UScriptStruct_FControlRigDrawContainer, METADATA_PARAMS(Z_Construct_UClass_UControlRig_Statics::NewProp_DrawContainer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRig_Statics::NewProp_DrawContainer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRig_Statics::NewProp_DataSourceRegistry_MetaData[] = {
		{ "Comment", "/** The registry to access data source */" },
		{ "ModuleRelativePath", "Public/ControlRig.h" },
		{ "ToolTip", "The registry to access data source" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UControlRig_Statics::NewProp_DataSourceRegistry = { "DataSourceRegistry", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRig, DataSourceRegistry), Z_Construct_UClass_UAnimationDataSourceRegistry_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UControlRig_Statics::NewProp_DataSourceRegistry_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRig_Statics::NewProp_DataSourceRegistry_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UControlRig_Statics::NewProp_EventQueue_Inner = { "EventQueue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRig_Statics::NewProp_EventQueue_MetaData[] = {
		{ "Comment", "/** The event name used during an update */" },
		{ "ModuleRelativePath", "Public/ControlRig.h" },
		{ "ToolTip", "The event name used during an update" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UControlRig_Statics::NewProp_EventQueue = { "EventQueue", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRig, EventQueue), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UControlRig_Statics::NewProp_EventQueue_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRig_Statics::NewProp_EventQueue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRig_Statics::NewProp_Influences_MetaData[] = {
		{ "ModuleRelativePath", "Public/ControlRig.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UControlRig_Statics::NewProp_Influences = { "Influences", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRig, Influences), Z_Construct_UScriptStruct_FRigInfluenceMapPerEvent, METADATA_PARAMS(Z_Construct_UClass_UControlRig_Statics::NewProp_Influences_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRig_Statics::NewProp_Influences_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRig_Statics::NewProp_InteractionRig_MetaData[] = {
		{ "BlueprintGetter", "GetInteractionRig" },
		{ "BlueprintSetter", "SetInteractionRig" },
		{ "Category", "Interaction" },
		{ "ModuleRelativePath", "Public/ControlRig.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UControlRig_Statics::NewProp_InteractionRig = { "InteractionRig", nullptr, (EPropertyFlags)0x0040000000002004, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRig, InteractionRig), Z_Construct_UClass_UControlRig_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UControlRig_Statics::NewProp_InteractionRig_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRig_Statics::NewProp_InteractionRig_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRig_Statics::NewProp_InteractionRigClass_MetaData[] = {
		{ "BlueprintGetter", "GetInteractionRigClass" },
		{ "BlueprintSetter", "SetInteractionRigClass" },
		{ "Category", "Interaction" },
		{ "DisplayName", "Interaction Rig" },
		{ "ModuleRelativePath", "Public/ControlRig.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UControlRig_Statics::NewProp_InteractionRigClass = { "InteractionRigClass", nullptr, (EPropertyFlags)0x0044000000002005, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRig, InteractionRigClass), Z_Construct_UClass_UControlRig_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_UControlRig_Statics::NewProp_InteractionRigClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRig_Statics::NewProp_InteractionRigClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRig_Statics::NewProp_AssetUserData_Inner_MetaData[] = {
		{ "Category", "Default" },
		{ "Comment", "/** Array of user data stored with the asset */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/ControlRig.h" },
		{ "ToolTip", "Array of user data stored with the asset" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UControlRig_Statics::NewProp_AssetUserData_Inner = { "AssetUserData", nullptr, (EPropertyFlags)0x0002000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UAssetUserData_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UControlRig_Statics::NewProp_AssetUserData_Inner_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRig_Statics::NewProp_AssetUserData_Inner_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRig_Statics::NewProp_AssetUserData_MetaData[] = {
		{ "Category", "Default" },
		{ "Comment", "/** Array of user data stored with the asset */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/ControlRig.h" },
		{ "ToolTip", "Array of user data stored with the asset" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UControlRig_Statics::NewProp_AssetUserData = { "AssetUserData", nullptr, (EPropertyFlags)0x00200c8000000009, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRig, AssetUserData), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UControlRig_Statics::NewProp_AssetUserData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRig_Statics::NewProp_AssetUserData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UControlRig_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRig_Statics::NewProp_ExecutionType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRig_Statics::NewProp_ExecutionType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRig_Statics::NewProp_VM,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRig_Statics::NewProp_Hierarchy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRig_Statics::NewProp_GizmoLibrary,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRig_Statics::NewProp_InputProperties_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRig_Statics::NewProp_InputProperties_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRig_Statics::NewProp_InputProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRig_Statics::NewProp_OutputProperties_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRig_Statics::NewProp_OutputProperties_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRig_Statics::NewProp_OutputProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRig_Statics::NewProp_DrawContainer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRig_Statics::NewProp_DataSourceRegistry,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRig_Statics::NewProp_EventQueue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRig_Statics::NewProp_EventQueue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRig_Statics::NewProp_Influences,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRig_Statics::NewProp_InteractionRig,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRig_Statics::NewProp_InteractionRigClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRig_Statics::NewProp_AssetUserData_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRig_Statics::NewProp_AssetUserData,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_UControlRig_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UNodeMappingProviderInterface_NoRegister, (int32)VTABLE_OFFSET(UControlRig, INodeMappingProviderInterface), false },
			{ Z_Construct_UClass_UInterface_AssetUserData_NoRegister, (int32)VTABLE_OFFSET(UControlRig, IInterface_AssetUserData), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UControlRig_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UControlRig>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UControlRig_Statics::ClassParams = {
		&UControlRig::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UControlRig_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UControlRig_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x009010A1u,
		METADATA_PARAMS(Z_Construct_UClass_UControlRig_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRig_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UControlRig()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UControlRig_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UControlRig, 2181511477);
	template<> CONTROLRIG_API UClass* StaticClass<UControlRig>()
	{
		return UControlRig::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UControlRig(Z_Construct_UClass_UControlRig, &UControlRig::StaticClass, TEXT("/Script/ControlRig"), TEXT("UControlRig"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UControlRig);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(UControlRig)
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
