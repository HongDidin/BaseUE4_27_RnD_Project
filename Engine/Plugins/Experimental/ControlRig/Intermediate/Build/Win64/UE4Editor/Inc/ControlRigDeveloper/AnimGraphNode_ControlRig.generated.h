// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIGDEVELOPER_AnimGraphNode_ControlRig_generated_h
#error "AnimGraphNode_ControlRig.generated.h already included, missing '#pragma once' in AnimGraphNode_ControlRig.h"
#endif
#define CONTROLRIGDEVELOPER_AnimGraphNode_ControlRig_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Private_AnimGraphNode_ControlRig_h_15_SPARSE_DATA
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Private_AnimGraphNode_ControlRig_h_15_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Private_AnimGraphNode_ControlRig_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Private_AnimGraphNode_ControlRig_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAnimGraphNode_ControlRig(); \
	friend struct Z_Construct_UClass_UAnimGraphNode_ControlRig_Statics; \
public: \
	DECLARE_CLASS(UAnimGraphNode_ControlRig, UAnimGraphNode_CustomProperty, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRigDeveloper"), CONTROLRIGDEVELOPER_API) \
	DECLARE_SERIALIZER(UAnimGraphNode_ControlRig)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Private_AnimGraphNode_ControlRig_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUAnimGraphNode_ControlRig(); \
	friend struct Z_Construct_UClass_UAnimGraphNode_ControlRig_Statics; \
public: \
	DECLARE_CLASS(UAnimGraphNode_ControlRig, UAnimGraphNode_CustomProperty, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRigDeveloper"), CONTROLRIGDEVELOPER_API) \
	DECLARE_SERIALIZER(UAnimGraphNode_ControlRig)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Private_AnimGraphNode_ControlRig_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	CONTROLRIGDEVELOPER_API UAnimGraphNode_ControlRig(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAnimGraphNode_ControlRig) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(CONTROLRIGDEVELOPER_API, UAnimGraphNode_ControlRig); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAnimGraphNode_ControlRig); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	CONTROLRIGDEVELOPER_API UAnimGraphNode_ControlRig(UAnimGraphNode_ControlRig&&); \
	CONTROLRIGDEVELOPER_API UAnimGraphNode_ControlRig(const UAnimGraphNode_ControlRig&); \
public:


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Private_AnimGraphNode_ControlRig_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	CONTROLRIGDEVELOPER_API UAnimGraphNode_ControlRig(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	CONTROLRIGDEVELOPER_API UAnimGraphNode_ControlRig(UAnimGraphNode_ControlRig&&); \
	CONTROLRIGDEVELOPER_API UAnimGraphNode_ControlRig(const UAnimGraphNode_ControlRig&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(CONTROLRIGDEVELOPER_API, UAnimGraphNode_ControlRig); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAnimGraphNode_ControlRig); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAnimGraphNode_ControlRig)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Private_AnimGraphNode_ControlRig_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Private_AnimGraphNode_ControlRig_h_12_PROLOG
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Private_AnimGraphNode_ControlRig_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Private_AnimGraphNode_ControlRig_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Private_AnimGraphNode_ControlRig_h_15_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Private_AnimGraphNode_ControlRig_h_15_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Private_AnimGraphNode_ControlRig_h_15_INCLASS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Private_AnimGraphNode_ControlRig_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Private_AnimGraphNode_ControlRig_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Private_AnimGraphNode_ControlRig_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Private_AnimGraphNode_ControlRig_h_15_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Private_AnimGraphNode_ControlRig_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Private_AnimGraphNode_ControlRig_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Private_AnimGraphNode_ControlRig_h_15_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class AnimGraphNode_ControlRig."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONTROLRIGDEVELOPER_API UClass* StaticClass<class UAnimGraphNode_ControlRig>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Private_AnimGraphNode_ControlRig_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
