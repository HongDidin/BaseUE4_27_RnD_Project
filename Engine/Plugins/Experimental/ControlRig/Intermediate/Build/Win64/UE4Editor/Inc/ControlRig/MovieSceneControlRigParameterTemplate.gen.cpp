// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Public/Sequencer/MovieSceneControlRigParameterTemplate.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMovieSceneControlRigParameterTemplate() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneControlRigParameterTemplate();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	MOVIESCENETRACKS_API UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneParameterSectionTemplate();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FEnumParameterNameAndCurve();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FIntegerParameterNameAndCurve();
// End Cross Module References

static_assert(std::is_polymorphic<FMovieSceneControlRigParameterTemplate>() == std::is_polymorphic<FMovieSceneParameterSectionTemplate>(), "USTRUCT FMovieSceneControlRigParameterTemplate cannot be polymorphic unless super FMovieSceneParameterSectionTemplate is polymorphic");

class UScriptStruct* FMovieSceneControlRigParameterTemplate::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FMovieSceneControlRigParameterTemplate_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMovieSceneControlRigParameterTemplate, Z_Construct_UPackage__Script_ControlRig(), TEXT("MovieSceneControlRigParameterTemplate"), sizeof(FMovieSceneControlRigParameterTemplate), Get_Z_Construct_UScriptStruct_FMovieSceneControlRigParameterTemplate_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FMovieSceneControlRigParameterTemplate>()
{
	return FMovieSceneControlRigParameterTemplate::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMovieSceneControlRigParameterTemplate(FMovieSceneControlRigParameterTemplate::StaticStruct, TEXT("/Script/ControlRig"), TEXT("MovieSceneControlRigParameterTemplate"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFMovieSceneControlRigParameterTemplate
{
	FScriptStruct_ControlRig_StaticRegisterNativesFMovieSceneControlRigParameterTemplate()
	{
		UScriptStruct::DeferCppStructOps<FMovieSceneControlRigParameterTemplate>(FName(TEXT("MovieSceneControlRigParameterTemplate")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFMovieSceneControlRigParameterTemplate;
	struct Z_Construct_UScriptStruct_FMovieSceneControlRigParameterTemplate_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Enums_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Enums_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Enums;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Integers_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Integers_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Integers;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneControlRigParameterTemplate_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneControlRigParameterTemplate.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMovieSceneControlRigParameterTemplate_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMovieSceneControlRigParameterTemplate>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMovieSceneControlRigParameterTemplate_Statics::NewProp_Enums_Inner = { "Enums", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FEnumParameterNameAndCurve, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneControlRigParameterTemplate_Statics::NewProp_Enums_MetaData[] = {
		{ "Comment", "/** The bool parameter names and their associated curves. */" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneControlRigParameterTemplate.h" },
		{ "ToolTip", "The bool parameter names and their associated curves." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FMovieSceneControlRigParameterTemplate_Statics::NewProp_Enums = { "Enums", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMovieSceneControlRigParameterTemplate, Enums), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneControlRigParameterTemplate_Statics::NewProp_Enums_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneControlRigParameterTemplate_Statics::NewProp_Enums_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMovieSceneControlRigParameterTemplate_Statics::NewProp_Integers_Inner = { "Integers", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FIntegerParameterNameAndCurve, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneControlRigParameterTemplate_Statics::NewProp_Integers_MetaData[] = {
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneControlRigParameterTemplate.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FMovieSceneControlRigParameterTemplate_Statics::NewProp_Integers = { "Integers", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMovieSceneControlRigParameterTemplate, Integers), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneControlRigParameterTemplate_Statics::NewProp_Integers_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneControlRigParameterTemplate_Statics::NewProp_Integers_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMovieSceneControlRigParameterTemplate_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneControlRigParameterTemplate_Statics::NewProp_Enums_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneControlRigParameterTemplate_Statics::NewProp_Enums,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneControlRigParameterTemplate_Statics::NewProp_Integers_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneControlRigParameterTemplate_Statics::NewProp_Integers,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMovieSceneControlRigParameterTemplate_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FMovieSceneParameterSectionTemplate,
		&NewStructOps,
		"MovieSceneControlRigParameterTemplate",
		sizeof(FMovieSceneControlRigParameterTemplate),
		alignof(FMovieSceneControlRigParameterTemplate),
		Z_Construct_UScriptStruct_FMovieSceneControlRigParameterTemplate_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneControlRigParameterTemplate_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000005),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneControlRigParameterTemplate_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneControlRigParameterTemplate_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneControlRigParameterTemplate()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMovieSceneControlRigParameterTemplate_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MovieSceneControlRigParameterTemplate"), sizeof(FMovieSceneControlRigParameterTemplate), Get_Z_Construct_UScriptStruct_FMovieSceneControlRigParameterTemplate_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMovieSceneControlRigParameterTemplate_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMovieSceneControlRigParameterTemplate_Hash() { return 2517406522U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
