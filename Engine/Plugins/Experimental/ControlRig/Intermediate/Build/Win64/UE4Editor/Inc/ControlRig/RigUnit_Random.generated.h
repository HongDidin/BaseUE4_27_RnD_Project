// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigUnit_Random_generated_h
#error "RigUnit_Random.generated.h already included, missing '#pragma once' in RigUnit_Random.h"
#endif
#define CONTROLRIG_RigUnit_Random_generated_h


#define FRigUnit_RandomVector_Execute() \
	void FRigUnit_RandomVector::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const int32 Seed, \
		const float Minimum, \
		const float Maximum, \
		const float Duration, \
		FVector& Result, \
		FVector& LastResult, \
		int32& LastSeed, \
		float& TimeLeft, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_Random_h_60_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_RandomVector_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const int32 Seed, \
		const float Minimum, \
		const float Maximum, \
		const float Duration, \
		FVector& Result, \
		FVector& LastResult, \
		int32& LastSeed, \
		float& TimeLeft, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const int32 Seed = *(int32*)RigVMMemoryHandles[0].GetData(); \
		const float Minimum = *(float*)RigVMMemoryHandles[1].GetData(); \
		const float Maximum = *(float*)RigVMMemoryHandles[2].GetData(); \
		const float Duration = *(float*)RigVMMemoryHandles[3].GetData(); \
		FVector& Result = *(FVector*)RigVMMemoryHandles[4].GetData(); \
		FRigVMDynamicArray<FVector> LastResult_5_Array(*((FRigVMByteArray*)RigVMMemoryHandles[5].GetData(0, false))); \
		LastResult_5_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FVector& LastResult = LastResult_5_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<int32> LastSeed_6_Array(*((FRigVMByteArray*)RigVMMemoryHandles[6].GetData(0, false))); \
		LastSeed_6_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		int32& LastSeed = LastSeed_6_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<float> TimeLeft_7_Array(*((FRigVMByteArray*)RigVMMemoryHandles[7].GetData(0, false))); \
		TimeLeft_7_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		float& TimeLeft = TimeLeft_7_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Seed, \
			Minimum, \
			Maximum, \
			Duration, \
			Result, \
			LastResult, \
			LastSeed, \
			TimeLeft, \
			Context \
		); \
	} \
	typedef FRigUnit_MathBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_RandomVector>();


#define FRigUnit_RandomFloat_Execute() \
	void FRigUnit_RandomFloat::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const int32 Seed, \
		const float Minimum, \
		const float Maximum, \
		const float Duration, \
		float& Result, \
		float& LastResult, \
		int32& LastSeed, \
		float& TimeLeft, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_Random_h_14_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_RandomFloat_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const int32 Seed, \
		const float Minimum, \
		const float Maximum, \
		const float Duration, \
		float& Result, \
		float& LastResult, \
		int32& LastSeed, \
		float& TimeLeft, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const int32 Seed = *(int32*)RigVMMemoryHandles[0].GetData(); \
		const float Minimum = *(float*)RigVMMemoryHandles[1].GetData(); \
		const float Maximum = *(float*)RigVMMemoryHandles[2].GetData(); \
		const float Duration = *(float*)RigVMMemoryHandles[3].GetData(); \
		float& Result = *(float*)RigVMMemoryHandles[4].GetData(); \
		FRigVMDynamicArray<float> LastResult_5_Array(*((FRigVMByteArray*)RigVMMemoryHandles[5].GetData(0, false))); \
		LastResult_5_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		float& LastResult = LastResult_5_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<int32> LastSeed_6_Array(*((FRigVMByteArray*)RigVMMemoryHandles[6].GetData(0, false))); \
		LastSeed_6_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		int32& LastSeed = LastSeed_6_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<float> TimeLeft_7_Array(*((FRigVMByteArray*)RigVMMemoryHandles[7].GetData(0, false))); \
		TimeLeft_7_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		float& TimeLeft = TimeLeft_7_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Seed, \
			Minimum, \
			Maximum, \
			Duration, \
			Result, \
			LastResult, \
			LastSeed, \
			TimeLeft, \
			Context \
		); \
	} \
	typedef FRigUnit_MathBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_RandomFloat>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_Random_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
