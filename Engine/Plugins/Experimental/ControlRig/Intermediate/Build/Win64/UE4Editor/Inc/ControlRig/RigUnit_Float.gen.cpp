// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Deprecated/Math/RigUnit_Float.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_Float() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MapRange_Float();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_Clamp_Float();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_Divide_FloatFloat();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_BinaryFloatOp();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_Subtract_FloatFloat();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_Add_FloatFloat();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_Multiply_FloatFloat();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_MapRange_Float>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_MapRange_Float cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_MapRange_Float::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MapRange_Float, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MapRange_Float"), sizeof(FRigUnit_MapRange_Float), Get_Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MapRange_Float::Execute"), &FRigUnit_MapRange_Float::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MapRange_Float>()
{
	return FRigUnit_MapRange_Float::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MapRange_Float(FRigUnit_MapRange_Float::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MapRange_Float"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MapRange_Float
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MapRange_Float()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MapRange_Float>(FName(TEXT("RigUnit_MapRange_Float")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MapRange_Float;
	struct Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MinIn_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MinIn;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxIn_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaxIn;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MinOut_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MinOut;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxOut_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaxOut;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Math|Float" },
		{ "Comment", "/** Two args and a result of float type */" },
		{ "Deprecated", "4.23.0" },
		{ "DisplayName", "MapRange" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Float.h" },
		{ "ToolTip", "Two args and a result of float type" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MapRange_Float>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Statics::NewProp_Value_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Float.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MapRange_Float, Value), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Statics::NewProp_MinIn_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Float.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Statics::NewProp_MinIn = { "MinIn", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MapRange_Float, MinIn), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Statics::NewProp_MinIn_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Statics::NewProp_MinIn_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Statics::NewProp_MaxIn_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Float.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Statics::NewProp_MaxIn = { "MaxIn", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MapRange_Float, MaxIn), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Statics::NewProp_MaxIn_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Statics::NewProp_MaxIn_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Statics::NewProp_MinOut_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Float.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Statics::NewProp_MinOut = { "MinOut", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MapRange_Float, MinOut), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Statics::NewProp_MinOut_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Statics::NewProp_MinOut_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Statics::NewProp_MaxOut_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Float.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Statics::NewProp_MaxOut = { "MaxOut", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MapRange_Float, MaxOut), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Statics::NewProp_MaxOut_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Statics::NewProp_MaxOut_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Float.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MapRange_Float, Result), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Statics::NewProp_MinIn,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Statics::NewProp_MaxIn,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Statics::NewProp_MinOut,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Statics::NewProp_MaxOut,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_MapRange_Float",
		sizeof(FRigUnit_MapRange_Float),
		alignof(FRigUnit_MapRange_Float),
		Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MapRange_Float()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MapRange_Float"), sizeof(FRigUnit_MapRange_Float), Get_Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MapRange_Float_Hash() { return 2407966905U; }

void FRigUnit_MapRange_Float::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		MinIn,
		MaxIn,
		MinOut,
		MaxOut,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_Clamp_Float>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_Clamp_Float cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_Clamp_Float::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Clamp_Float_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_Clamp_Float, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_Clamp_Float"), sizeof(FRigUnit_Clamp_Float), Get_Z_Construct_UScriptStruct_FRigUnit_Clamp_Float_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_Clamp_Float::Execute"), &FRigUnit_Clamp_Float::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_Clamp_Float>()
{
	return FRigUnit_Clamp_Float::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_Clamp_Float(FRigUnit_Clamp_Float::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_Clamp_Float"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_Clamp_Float
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_Clamp_Float()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_Clamp_Float>(FName(TEXT("RigUnit_Clamp_Float")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_Clamp_Float;
	struct Z_Construct_UScriptStruct_FRigUnit_Clamp_Float_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Min_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Min;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Max_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Max;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_Clamp_Float_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Math|Float" },
		{ "Comment", "/** Two args and a result of float type */" },
		{ "Deprecated", "4.23.0" },
		{ "DisplayName", "Clamp" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Float.h" },
		{ "NodeColor", "0.1 0.7 0.1" },
		{ "ToolTip", "Two args and a result of float type" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_Clamp_Float_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_Clamp_Float>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_Clamp_Float_Statics::NewProp_Value_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Float.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_Clamp_Float_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_Clamp_Float, Value), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_Clamp_Float_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_Clamp_Float_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_Clamp_Float_Statics::NewProp_Min_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Float.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_Clamp_Float_Statics::NewProp_Min = { "Min", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_Clamp_Float, Min), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_Clamp_Float_Statics::NewProp_Min_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_Clamp_Float_Statics::NewProp_Min_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_Clamp_Float_Statics::NewProp_Max_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Float.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_Clamp_Float_Statics::NewProp_Max = { "Max", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_Clamp_Float, Max), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_Clamp_Float_Statics::NewProp_Max_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_Clamp_Float_Statics::NewProp_Max_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_Clamp_Float_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Float.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_Clamp_Float_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_Clamp_Float, Result), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_Clamp_Float_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_Clamp_Float_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_Clamp_Float_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_Clamp_Float_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_Clamp_Float_Statics::NewProp_Min,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_Clamp_Float_Statics::NewProp_Max,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_Clamp_Float_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_Clamp_Float_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_Clamp_Float",
		sizeof(FRigUnit_Clamp_Float),
		alignof(FRigUnit_Clamp_Float),
		Z_Construct_UScriptStruct_FRigUnit_Clamp_Float_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_Clamp_Float_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_Clamp_Float_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_Clamp_Float_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_Clamp_Float()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Clamp_Float_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_Clamp_Float"), sizeof(FRigUnit_Clamp_Float), Get_Z_Construct_UScriptStruct_FRigUnit_Clamp_Float_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_Clamp_Float_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Clamp_Float_Hash() { return 1416065820U; }

void FRigUnit_Clamp_Float::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Min,
		Max,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_Divide_FloatFloat>() == std::is_polymorphic<FRigUnit_BinaryFloatOp>(), "USTRUCT FRigUnit_Divide_FloatFloat cannot be polymorphic unless super FRigUnit_BinaryFloatOp is polymorphic");

class UScriptStruct* FRigUnit_Divide_FloatFloat::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Divide_FloatFloat_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_Divide_FloatFloat, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_Divide_FloatFloat"), sizeof(FRigUnit_Divide_FloatFloat), Get_Z_Construct_UScriptStruct_FRigUnit_Divide_FloatFloat_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_Divide_FloatFloat::Execute"), &FRigUnit_Divide_FloatFloat::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_Divide_FloatFloat>()
{
	return FRigUnit_Divide_FloatFloat::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_Divide_FloatFloat(FRigUnit_Divide_FloatFloat::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_Divide_FloatFloat"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_Divide_FloatFloat
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_Divide_FloatFloat()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_Divide_FloatFloat>(FName(TEXT("RigUnit_Divide_FloatFloat")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_Divide_FloatFloat;
	struct Z_Construct_UScriptStruct_FRigUnit_Divide_FloatFloat_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_Divide_FloatFloat_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Math|Float" },
		{ "Deprecated", "4.23.0" },
		{ "DisplayName", "Divide" },
		{ "Keywords", "/" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Float.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_Divide_FloatFloat_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_Divide_FloatFloat>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_Divide_FloatFloat_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_BinaryFloatOp,
		&NewStructOps,
		"RigUnit_Divide_FloatFloat",
		sizeof(FRigUnit_Divide_FloatFloat),
		alignof(FRigUnit_Divide_FloatFloat),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_Divide_FloatFloat_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_Divide_FloatFloat_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_Divide_FloatFloat()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Divide_FloatFloat_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_Divide_FloatFloat"), sizeof(FRigUnit_Divide_FloatFloat), Get_Z_Construct_UScriptStruct_FRigUnit_Divide_FloatFloat_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_Divide_FloatFloat_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Divide_FloatFloat_Hash() { return 275854730U; }

void FRigUnit_Divide_FloatFloat::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Argument0,
		Argument1,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_Subtract_FloatFloat>() == std::is_polymorphic<FRigUnit_BinaryFloatOp>(), "USTRUCT FRigUnit_Subtract_FloatFloat cannot be polymorphic unless super FRigUnit_BinaryFloatOp is polymorphic");

class UScriptStruct* FRigUnit_Subtract_FloatFloat::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Subtract_FloatFloat_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_Subtract_FloatFloat, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_Subtract_FloatFloat"), sizeof(FRigUnit_Subtract_FloatFloat), Get_Z_Construct_UScriptStruct_FRigUnit_Subtract_FloatFloat_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_Subtract_FloatFloat::Execute"), &FRigUnit_Subtract_FloatFloat::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_Subtract_FloatFloat>()
{
	return FRigUnit_Subtract_FloatFloat::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_Subtract_FloatFloat(FRigUnit_Subtract_FloatFloat::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_Subtract_FloatFloat"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_Subtract_FloatFloat
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_Subtract_FloatFloat()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_Subtract_FloatFloat>(FName(TEXT("RigUnit_Subtract_FloatFloat")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_Subtract_FloatFloat;
	struct Z_Construct_UScriptStruct_FRigUnit_Subtract_FloatFloat_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_Subtract_FloatFloat_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Math|Float" },
		{ "Deprecated", "4.23.0" },
		{ "DisplayName", "Subtract" },
		{ "Keywords", "-" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Float.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_Subtract_FloatFloat_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_Subtract_FloatFloat>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_Subtract_FloatFloat_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_BinaryFloatOp,
		&NewStructOps,
		"RigUnit_Subtract_FloatFloat",
		sizeof(FRigUnit_Subtract_FloatFloat),
		alignof(FRigUnit_Subtract_FloatFloat),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_Subtract_FloatFloat_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_Subtract_FloatFloat_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_Subtract_FloatFloat()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Subtract_FloatFloat_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_Subtract_FloatFloat"), sizeof(FRigUnit_Subtract_FloatFloat), Get_Z_Construct_UScriptStruct_FRigUnit_Subtract_FloatFloat_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_Subtract_FloatFloat_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Subtract_FloatFloat_Hash() { return 1950256190U; }

void FRigUnit_Subtract_FloatFloat::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Argument0,
		Argument1,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_Add_FloatFloat>() == std::is_polymorphic<FRigUnit_BinaryFloatOp>(), "USTRUCT FRigUnit_Add_FloatFloat cannot be polymorphic unless super FRigUnit_BinaryFloatOp is polymorphic");

class UScriptStruct* FRigUnit_Add_FloatFloat::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Add_FloatFloat_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_Add_FloatFloat, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_Add_FloatFloat"), sizeof(FRigUnit_Add_FloatFloat), Get_Z_Construct_UScriptStruct_FRigUnit_Add_FloatFloat_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_Add_FloatFloat::Execute"), &FRigUnit_Add_FloatFloat::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_Add_FloatFloat>()
{
	return FRigUnit_Add_FloatFloat::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_Add_FloatFloat(FRigUnit_Add_FloatFloat::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_Add_FloatFloat"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_Add_FloatFloat
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_Add_FloatFloat()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_Add_FloatFloat>(FName(TEXT("RigUnit_Add_FloatFloat")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_Add_FloatFloat;
	struct Z_Construct_UScriptStruct_FRigUnit_Add_FloatFloat_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_Add_FloatFloat_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Math|Float" },
		{ "Deprecated", "4.23.0" },
		{ "DisplayName", "Add" },
		{ "Keywords", "+,Sum" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Float.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_Add_FloatFloat_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_Add_FloatFloat>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_Add_FloatFloat_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_BinaryFloatOp,
		&NewStructOps,
		"RigUnit_Add_FloatFloat",
		sizeof(FRigUnit_Add_FloatFloat),
		alignof(FRigUnit_Add_FloatFloat),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_Add_FloatFloat_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_Add_FloatFloat_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_Add_FloatFloat()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Add_FloatFloat_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_Add_FloatFloat"), sizeof(FRigUnit_Add_FloatFloat), Get_Z_Construct_UScriptStruct_FRigUnit_Add_FloatFloat_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_Add_FloatFloat_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Add_FloatFloat_Hash() { return 3235717072U; }

void FRigUnit_Add_FloatFloat::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Argument0,
		Argument1,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_Multiply_FloatFloat>() == std::is_polymorphic<FRigUnit_BinaryFloatOp>(), "USTRUCT FRigUnit_Multiply_FloatFloat cannot be polymorphic unless super FRigUnit_BinaryFloatOp is polymorphic");

class UScriptStruct* FRigUnit_Multiply_FloatFloat::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Multiply_FloatFloat_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_Multiply_FloatFloat, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_Multiply_FloatFloat"), sizeof(FRigUnit_Multiply_FloatFloat), Get_Z_Construct_UScriptStruct_FRigUnit_Multiply_FloatFloat_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_Multiply_FloatFloat::Execute"), &FRigUnit_Multiply_FloatFloat::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_Multiply_FloatFloat>()
{
	return FRigUnit_Multiply_FloatFloat::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_Multiply_FloatFloat(FRigUnit_Multiply_FloatFloat::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_Multiply_FloatFloat"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_Multiply_FloatFloat
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_Multiply_FloatFloat()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_Multiply_FloatFloat>(FName(TEXT("RigUnit_Multiply_FloatFloat")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_Multiply_FloatFloat;
	struct Z_Construct_UScriptStruct_FRigUnit_Multiply_FloatFloat_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_Multiply_FloatFloat_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Math|Float" },
		{ "Deprecated", "4.23.0" },
		{ "DisplayName", "Multiply" },
		{ "Keywords", "*" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Float.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_Multiply_FloatFloat_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_Multiply_FloatFloat>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_Multiply_FloatFloat_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_BinaryFloatOp,
		&NewStructOps,
		"RigUnit_Multiply_FloatFloat",
		sizeof(FRigUnit_Multiply_FloatFloat),
		alignof(FRigUnit_Multiply_FloatFloat),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_Multiply_FloatFloat_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_Multiply_FloatFloat_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_Multiply_FloatFloat()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Multiply_FloatFloat_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_Multiply_FloatFloat"), sizeof(FRigUnit_Multiply_FloatFloat), Get_Z_Construct_UScriptStruct_FRigUnit_Multiply_FloatFloat_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_Multiply_FloatFloat_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Multiply_FloatFloat_Hash() { return 392673161U; }

void FRigUnit_Multiply_FloatFloat::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Argument0,
		Argument1,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_BinaryFloatOp>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_BinaryFloatOp cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_BinaryFloatOp::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_BinaryFloatOp_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_BinaryFloatOp, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_BinaryFloatOp"), sizeof(FRigUnit_BinaryFloatOp), Get_Z_Construct_UScriptStruct_FRigUnit_BinaryFloatOp_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_BinaryFloatOp>()
{
	return FRigUnit_BinaryFloatOp::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_BinaryFloatOp(FRigUnit_BinaryFloatOp::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_BinaryFloatOp"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_BinaryFloatOp
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_BinaryFloatOp()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_BinaryFloatOp>(FName(TEXT("RigUnit_BinaryFloatOp")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_BinaryFloatOp;
	struct Z_Construct_UScriptStruct_FRigUnit_BinaryFloatOp_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Argument0_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Argument0;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Argument1_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Argument1;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_BinaryFloatOp_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "Comment", "/** Two args and a result of float type */" },
		{ "Deprecated", "4.23.0" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Float.h" },
		{ "NodeColor", "0.1 0.7 0.1" },
		{ "ToolTip", "Two args and a result of float type" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_BinaryFloatOp_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_BinaryFloatOp>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_BinaryFloatOp_Statics::NewProp_Argument0_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Float.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_BinaryFloatOp_Statics::NewProp_Argument0 = { "Argument0", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_BinaryFloatOp, Argument0), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_BinaryFloatOp_Statics::NewProp_Argument0_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_BinaryFloatOp_Statics::NewProp_Argument0_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_BinaryFloatOp_Statics::NewProp_Argument1_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Float.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_BinaryFloatOp_Statics::NewProp_Argument1 = { "Argument1", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_BinaryFloatOp, Argument1), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_BinaryFloatOp_Statics::NewProp_Argument1_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_BinaryFloatOp_Statics::NewProp_Argument1_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_BinaryFloatOp_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Float.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_BinaryFloatOp_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_BinaryFloatOp, Result), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_BinaryFloatOp_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_BinaryFloatOp_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_BinaryFloatOp_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_BinaryFloatOp_Statics::NewProp_Argument0,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_BinaryFloatOp_Statics::NewProp_Argument1,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_BinaryFloatOp_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_BinaryFloatOp_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_BinaryFloatOp",
		sizeof(FRigUnit_BinaryFloatOp),
		alignof(FRigUnit_BinaryFloatOp),
		Z_Construct_UScriptStruct_FRigUnit_BinaryFloatOp_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_BinaryFloatOp_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_BinaryFloatOp_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_BinaryFloatOp_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_BinaryFloatOp()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_BinaryFloatOp_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_BinaryFloatOp"), sizeof(FRigUnit_BinaryFloatOp), Get_Z_Construct_UScriptStruct_FRigUnit_BinaryFloatOp_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_BinaryFloatOp_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_BinaryFloatOp_Hash() { return 4153008686U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
