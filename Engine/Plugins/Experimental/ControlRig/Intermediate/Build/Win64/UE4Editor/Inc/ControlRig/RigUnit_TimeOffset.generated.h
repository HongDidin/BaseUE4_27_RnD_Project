// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigUnit_TimeOffset_generated_h
#error "RigUnit_TimeOffset.generated.h already included, missing '#pragma once' in RigUnit_TimeOffset.h"
#endif
#define CONTROLRIG_RigUnit_TimeOffset_generated_h


#define FRigUnit_TimeOffsetTransform_Execute() \
	void FRigUnit_TimeOffsetTransform::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FTransform& Value, \
		const float SecondsAgo, \
		const int32 BufferSize, \
		const float TimeRange, \
		FTransform& Result, \
		FRigVMFixedArray<FTransform>& Buffer, \
		FRigVMFixedArray<float>& DeltaTimes, \
		int32& LastInsertIndex, \
		int32& UpperBound, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Simulation_RigUnit_TimeOffset_h_120_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FTransform& Value, \
		const float SecondsAgo, \
		const int32 BufferSize, \
		const float TimeRange, \
		FTransform& Result, \
		FRigVMFixedArray<FTransform>& Buffer, \
		FRigVMFixedArray<float>& DeltaTimes, \
		int32& LastInsertIndex, \
		int32& UpperBound, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FTransform& Value = *(FTransform*)RigVMMemoryHandles[0].GetData(); \
		const float SecondsAgo = *(float*)RigVMMemoryHandles[1].GetData(); \
		const int32 BufferSize = *(int32*)RigVMMemoryHandles[2].GetData(); \
		const float TimeRange = *(float*)RigVMMemoryHandles[3].GetData(); \
		FTransform& Result = *(FTransform*)RigVMMemoryHandles[4].GetData(); \
		FRigVMFixedArray<FTransform> Buffer((FTransform*)RigVMMemoryHandles[5].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[6].GetData())); \
		FRigVMFixedArray<float> DeltaTimes((float*)RigVMMemoryHandles[7].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[8].GetData())); \
		FRigVMDynamicArray<int32> LastInsertIndex_9_Array(*((FRigVMByteArray*)RigVMMemoryHandles[9].GetData(0, false))); \
		LastInsertIndex_9_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		int32& LastInsertIndex = LastInsertIndex_9_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<int32> UpperBound_10_Array(*((FRigVMByteArray*)RigVMMemoryHandles[10].GetData(0, false))); \
		UpperBound_10_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		int32& UpperBound = UpperBound_10_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Value, \
			SecondsAgo, \
			BufferSize, \
			TimeRange, \
			Result, \
			Buffer, \
			DeltaTimes, \
			LastInsertIndex, \
			UpperBound, \
			Context \
		); \
	} \
	virtual int32 GetArraySize(const FName& InMemberName, const FRigVMUserDataArray& Context) override; \
	typedef FRigUnit_SimBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_TimeOffsetTransform>();


#define FRigUnit_TimeOffsetVector_Execute() \
	void FRigUnit_TimeOffsetVector::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FVector& Value, \
		const float SecondsAgo, \
		const int32 BufferSize, \
		const float TimeRange, \
		FVector& Result, \
		FRigVMFixedArray<FVector>& Buffer, \
		FRigVMFixedArray<float>& DeltaTimes, \
		int32& LastInsertIndex, \
		int32& UpperBound, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Simulation_RigUnit_TimeOffset_h_67_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FVector& Value, \
		const float SecondsAgo, \
		const int32 BufferSize, \
		const float TimeRange, \
		FVector& Result, \
		FRigVMFixedArray<FVector>& Buffer, \
		FRigVMFixedArray<float>& DeltaTimes, \
		int32& LastInsertIndex, \
		int32& UpperBound, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FVector& Value = *(FVector*)RigVMMemoryHandles[0].GetData(); \
		const float SecondsAgo = *(float*)RigVMMemoryHandles[1].GetData(); \
		const int32 BufferSize = *(int32*)RigVMMemoryHandles[2].GetData(); \
		const float TimeRange = *(float*)RigVMMemoryHandles[3].GetData(); \
		FVector& Result = *(FVector*)RigVMMemoryHandles[4].GetData(); \
		FRigVMFixedArray<FVector> Buffer((FVector*)RigVMMemoryHandles[5].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[6].GetData())); \
		FRigVMFixedArray<float> DeltaTimes((float*)RigVMMemoryHandles[7].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[8].GetData())); \
		FRigVMDynamicArray<int32> LastInsertIndex_9_Array(*((FRigVMByteArray*)RigVMMemoryHandles[9].GetData(0, false))); \
		LastInsertIndex_9_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		int32& LastInsertIndex = LastInsertIndex_9_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<int32> UpperBound_10_Array(*((FRigVMByteArray*)RigVMMemoryHandles[10].GetData(0, false))); \
		UpperBound_10_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		int32& UpperBound = UpperBound_10_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Value, \
			SecondsAgo, \
			BufferSize, \
			TimeRange, \
			Result, \
			Buffer, \
			DeltaTimes, \
			LastInsertIndex, \
			UpperBound, \
			Context \
		); \
	} \
	virtual int32 GetArraySize(const FName& InMemberName, const FRigVMUserDataArray& Context) override; \
	typedef FRigUnit_SimBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_TimeOffsetVector>();


#define FRigUnit_TimeOffsetFloat_Execute() \
	void FRigUnit_TimeOffsetFloat::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const float Value, \
		const float SecondsAgo, \
		const int32 BufferSize, \
		const float TimeRange, \
		float& Result, \
		FRigVMFixedArray<float>& Buffer, \
		FRigVMFixedArray<float>& DeltaTimes, \
		int32& LastInsertIndex, \
		int32& UpperBound, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Simulation_RigUnit_TimeOffset_h_14_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const float Value, \
		const float SecondsAgo, \
		const int32 BufferSize, \
		const float TimeRange, \
		float& Result, \
		FRigVMFixedArray<float>& Buffer, \
		FRigVMFixedArray<float>& DeltaTimes, \
		int32& LastInsertIndex, \
		int32& UpperBound, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const float Value = *(float*)RigVMMemoryHandles[0].GetData(); \
		const float SecondsAgo = *(float*)RigVMMemoryHandles[1].GetData(); \
		const int32 BufferSize = *(int32*)RigVMMemoryHandles[2].GetData(); \
		const float TimeRange = *(float*)RigVMMemoryHandles[3].GetData(); \
		float& Result = *(float*)RigVMMemoryHandles[4].GetData(); \
		FRigVMFixedArray<float> Buffer((float*)RigVMMemoryHandles[5].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[6].GetData())); \
		FRigVMFixedArray<float> DeltaTimes((float*)RigVMMemoryHandles[7].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[8].GetData())); \
		FRigVMDynamicArray<int32> LastInsertIndex_9_Array(*((FRigVMByteArray*)RigVMMemoryHandles[9].GetData(0, false))); \
		LastInsertIndex_9_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		int32& LastInsertIndex = LastInsertIndex_9_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<int32> UpperBound_10_Array(*((FRigVMByteArray*)RigVMMemoryHandles[10].GetData(0, false))); \
		UpperBound_10_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		int32& UpperBound = UpperBound_10_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Value, \
			SecondsAgo, \
			BufferSize, \
			TimeRange, \
			Result, \
			Buffer, \
			DeltaTimes, \
			LastInsertIndex, \
			UpperBound, \
			Context \
		); \
	} \
	virtual int32 GetArraySize(const FName& InMemberName, const FRigVMUserDataArray& Context) override; \
	typedef FRigUnit_SimBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_TimeOffsetFloat>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Simulation_RigUnit_TimeOffset_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
