// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Public/Math/Simulation/CRSimLinearSpring.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCRSimLinearSpring() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FCRSimLinearSpring();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
// End Cross Module References
class UScriptStruct* FCRSimLinearSpring::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FCRSimLinearSpring_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCRSimLinearSpring, Z_Construct_UPackage__Script_ControlRig(), TEXT("CRSimLinearSpring"), sizeof(FCRSimLinearSpring), Get_Z_Construct_UScriptStruct_FCRSimLinearSpring_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FCRSimLinearSpring>()
{
	return FCRSimLinearSpring::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCRSimLinearSpring(FCRSimLinearSpring::StaticStruct, TEXT("/Script/ControlRig"), TEXT("CRSimLinearSpring"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFCRSimLinearSpring
{
	FScriptStruct_ControlRig_StaticRegisterNativesFCRSimLinearSpring()
	{
		UScriptStruct::DeferCppStructOps<FCRSimLinearSpring>(FName(TEXT("CRSimLinearSpring")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFCRSimLinearSpring;
	struct Z_Construct_UScriptStruct_FCRSimLinearSpring_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SubjectA_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SubjectA;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SubjectB_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SubjectB;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Coefficient_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Coefficient;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Equilibrium_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Equilibrium;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimLinearSpring_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimLinearSpring.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FCRSimLinearSpring_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FCRSimLinearSpring>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimLinearSpring_Statics::NewProp_SubjectA_MetaData[] = {
		{ "Comment", "/**\n\x09 * The first point affected by this spring\n\x09 */" },
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimLinearSpring.h" },
		{ "ToolTip", "The first point affected by this spring" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FCRSimLinearSpring_Statics::NewProp_SubjectA = { "SubjectA", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCRSimLinearSpring, SubjectA), METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimLinearSpring_Statics::NewProp_SubjectA_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimLinearSpring_Statics::NewProp_SubjectA_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimLinearSpring_Statics::NewProp_SubjectB_MetaData[] = {
		{ "Comment", "/**\n\x09 * The second point affected by this spring\n\x09 */" },
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimLinearSpring.h" },
		{ "ToolTip", "The second point affected by this spring" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FCRSimLinearSpring_Statics::NewProp_SubjectB = { "SubjectB", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCRSimLinearSpring, SubjectB), METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimLinearSpring_Statics::NewProp_SubjectB_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimLinearSpring_Statics::NewProp_SubjectB_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimLinearSpring_Statics::NewProp_Coefficient_MetaData[] = {
		{ "Comment", "/**\n\x09 * The power of this spring\n\x09 */" },
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimLinearSpring.h" },
		{ "ToolTip", "The power of this spring" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCRSimLinearSpring_Statics::NewProp_Coefficient = { "Coefficient", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCRSimLinearSpring, Coefficient), METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimLinearSpring_Statics::NewProp_Coefficient_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimLinearSpring_Statics::NewProp_Coefficient_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimLinearSpring_Statics::NewProp_Equilibrium_MetaData[] = {
		{ "Comment", "/**\n\x09 * The rest length of this spring.\n\x09 * A value of lower than zero indicates that the equilibrium\n\x09 * should be based on the current distance of the two subjects.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimLinearSpring.h" },
		{ "ToolTip", "The rest length of this spring.\nA value of lower than zero indicates that the equilibrium\nshould be based on the current distance of the two subjects." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCRSimLinearSpring_Statics::NewProp_Equilibrium = { "Equilibrium", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCRSimLinearSpring, Equilibrium), METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimLinearSpring_Statics::NewProp_Equilibrium_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimLinearSpring_Statics::NewProp_Equilibrium_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FCRSimLinearSpring_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimLinearSpring_Statics::NewProp_SubjectA,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimLinearSpring_Statics::NewProp_SubjectB,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimLinearSpring_Statics::NewProp_Coefficient,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimLinearSpring_Statics::NewProp_Equilibrium,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FCRSimLinearSpring_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"CRSimLinearSpring",
		sizeof(FCRSimLinearSpring),
		alignof(FCRSimLinearSpring),
		Z_Construct_UScriptStruct_FCRSimLinearSpring_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimLinearSpring_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimLinearSpring_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimLinearSpring_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FCRSimLinearSpring()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FCRSimLinearSpring_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CRSimLinearSpring"), sizeof(FCRSimLinearSpring), Get_Z_Construct_UScriptStruct_FCRSimLinearSpring_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FCRSimLinearSpring_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCRSimLinearSpring_Hash() { return 1590015951U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
