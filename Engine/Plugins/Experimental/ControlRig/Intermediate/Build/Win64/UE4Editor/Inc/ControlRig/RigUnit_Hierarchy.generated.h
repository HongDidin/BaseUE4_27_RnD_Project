// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigUnit_Hierarchy_generated_h
#error "RigUnit_Hierarchy.generated.h already included, missing '#pragma once' in RigUnit_Hierarchy.h"
#endif
#define CONTROLRIG_RigUnit_Hierarchy_generated_h


#define FRigUnit_HierarchyGetSiblings_Execute() \
	void FRigUnit_HierarchyGetSiblings::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKey& Item, \
		const bool bIncludeItem, \
		FRigElementKeyCollection& Siblings, \
		FCachedRigElement& CachedItem, \
		FRigElementKeyCollection& CachedSiblings, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Execution_RigUnit_Hierarchy_h_135_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_HierarchyGetSiblings_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKey& Item, \
		const bool bIncludeItem, \
		FRigElementKeyCollection& Siblings, \
		FCachedRigElement& CachedItem, \
		FRigElementKeyCollection& CachedSiblings, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FRigElementKey& Item = *(FRigElementKey*)RigVMMemoryHandles[0].GetData(); \
		const bool bIncludeItem = *(bool*)RigVMMemoryHandles[1].GetData(); \
		FRigElementKeyCollection& Siblings = *(FRigElementKeyCollection*)RigVMMemoryHandles[2].GetData(); \
		FRigVMDynamicArray<FCachedRigElement> CachedItem_3_Array(*((FRigVMByteArray*)RigVMMemoryHandles[3].GetData(0, false))); \
		CachedItem_3_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedItem = CachedItem_3_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<FRigElementKeyCollection> CachedSiblings_4_Array(*((FRigVMByteArray*)RigVMMemoryHandles[4].GetData(0, false))); \
		CachedSiblings_4_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FRigElementKeyCollection& CachedSiblings = CachedSiblings_4_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Item, \
			bIncludeItem, \
			Siblings, \
			CachedItem, \
			CachedSiblings, \
			Context \
		); \
	} \
	typedef FRigUnit_HierarchyBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_HierarchyGetSiblings>();


#define FRigUnit_HierarchyGetChildren_Execute() \
	void FRigUnit_HierarchyGetChildren::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKey& Parent, \
		const bool bIncludeParent, \
		const bool bRecursive, \
		FRigElementKeyCollection& Children, \
		FCachedRigElement& CachedParent, \
		FRigElementKeyCollection& CachedChildren, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Execution_RigUnit_Hierarchy_h_94_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_HierarchyGetChildren_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKey& Parent, \
		const bool bIncludeParent, \
		const bool bRecursive, \
		FRigElementKeyCollection& Children, \
		FCachedRigElement& CachedParent, \
		FRigElementKeyCollection& CachedChildren, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FRigElementKey& Parent = *(FRigElementKey*)RigVMMemoryHandles[0].GetData(); \
		const bool bIncludeParent = *(bool*)RigVMMemoryHandles[1].GetData(); \
		const bool bRecursive = *(bool*)RigVMMemoryHandles[2].GetData(); \
		FRigElementKeyCollection& Children = *(FRigElementKeyCollection*)RigVMMemoryHandles[3].GetData(); \
		FRigVMDynamicArray<FCachedRigElement> CachedParent_4_Array(*((FRigVMByteArray*)RigVMMemoryHandles[4].GetData(0, false))); \
		CachedParent_4_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedParent = CachedParent_4_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<FRigElementKeyCollection> CachedChildren_5_Array(*((FRigVMByteArray*)RigVMMemoryHandles[5].GetData(0, false))); \
		CachedChildren_5_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FRigElementKeyCollection& CachedChildren = CachedChildren_5_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Parent, \
			bIncludeParent, \
			bRecursive, \
			Children, \
			CachedParent, \
			CachedChildren, \
			Context \
		); \
	} \
	typedef FRigUnit_HierarchyBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_HierarchyGetChildren>();


#define FRigUnit_HierarchyGetParents_Execute() \
	void FRigUnit_HierarchyGetParents::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKey& Child, \
		const bool bIncludeChild, \
		const bool bReverse, \
		FRigElementKeyCollection& Parents, \
		FCachedRigElement& CachedChild, \
		FRigElementKeyCollection& CachedParents, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Execution_RigUnit_Hierarchy_h_53_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParents_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKey& Child, \
		const bool bIncludeChild, \
		const bool bReverse, \
		FRigElementKeyCollection& Parents, \
		FCachedRigElement& CachedChild, \
		FRigElementKeyCollection& CachedParents, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FRigElementKey& Child = *(FRigElementKey*)RigVMMemoryHandles[0].GetData(); \
		const bool bIncludeChild = *(bool*)RigVMMemoryHandles[1].GetData(); \
		const bool bReverse = *(bool*)RigVMMemoryHandles[2].GetData(); \
		FRigElementKeyCollection& Parents = *(FRigElementKeyCollection*)RigVMMemoryHandles[3].GetData(); \
		FRigVMDynamicArray<FCachedRigElement> CachedChild_4_Array(*((FRigVMByteArray*)RigVMMemoryHandles[4].GetData(0, false))); \
		CachedChild_4_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedChild = CachedChild_4_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<FRigElementKeyCollection> CachedParents_5_Array(*((FRigVMByteArray*)RigVMMemoryHandles[5].GetData(0, false))); \
		CachedParents_5_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FRigElementKeyCollection& CachedParents = CachedParents_5_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Child, \
			bIncludeChild, \
			bReverse, \
			Parents, \
			CachedChild, \
			CachedParents, \
			Context \
		); \
	} \
	typedef FRigUnit_HierarchyBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_HierarchyGetParents>();


#define FRigUnit_HierarchyGetParent_Execute() \
	void FRigUnit_HierarchyGetParent::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKey& Child, \
		FRigElementKey& Parent, \
		FCachedRigElement& CachedChild, \
		FCachedRigElement& CachedParent, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Execution_RigUnit_Hierarchy_h_21_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_HierarchyGetParent_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKey& Child, \
		FRigElementKey& Parent, \
		FCachedRigElement& CachedChild, \
		FCachedRigElement& CachedParent, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FRigElementKey& Child = *(FRigElementKey*)RigVMMemoryHandles[0].GetData(); \
		FRigElementKey& Parent = *(FRigElementKey*)RigVMMemoryHandles[1].GetData(); \
		FRigVMDynamicArray<FCachedRigElement> CachedChild_2_Array(*((FRigVMByteArray*)RigVMMemoryHandles[2].GetData(0, false))); \
		CachedChild_2_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedChild = CachedChild_2_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<FCachedRigElement> CachedParent_3_Array(*((FRigVMByteArray*)RigVMMemoryHandles[3].GetData(0, false))); \
		CachedParent_3_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedParent = CachedParent_3_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Child, \
			Parent, \
			CachedChild, \
			CachedParent, \
			Context \
		); \
	} \
	typedef FRigUnit_HierarchyBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_HierarchyGetParent>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Execution_RigUnit_Hierarchy_h_12_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_HierarchyBase_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	typedef FRigUnit Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_HierarchyBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Execution_RigUnit_Hierarchy_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
