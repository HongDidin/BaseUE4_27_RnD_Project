// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Public/AnimNode_ControlRigBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAnimNode_ControlRigBase() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FAnimNode_ControlRigBase();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FAnimNode_CustomProperty();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FPoseLink();
	ENGINE_API UClass* Z_Construct_UClass_UNodeMappingContainer_NoRegister();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FControlRigIOSettings();
// End Cross Module References

static_assert(std::is_polymorphic<FAnimNode_ControlRigBase>() == std::is_polymorphic<FAnimNode_CustomProperty>(), "USTRUCT FAnimNode_ControlRigBase cannot be polymorphic unless super FAnimNode_CustomProperty is polymorphic");

class UScriptStruct* FAnimNode_ControlRigBase::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FAnimNode_ControlRigBase, Z_Construct_UPackage__Script_ControlRig(), TEXT("AnimNode_ControlRigBase"), sizeof(FAnimNode_ControlRigBase), Get_Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FAnimNode_ControlRigBase>()
{
	return FAnimNode_ControlRigBase::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FAnimNode_ControlRigBase(FAnimNode_ControlRigBase::StaticStruct, TEXT("/Script/ControlRig"), TEXT("AnimNode_ControlRigBase"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFAnimNode_ControlRigBase
{
	FScriptStruct_ControlRig_StaticRegisterNativesFAnimNode_ControlRigBase()
	{
		UScriptStruct::DeferCppStructOps<FAnimNode_ControlRigBase>(FName(TEXT("AnimNode_ControlRigBase")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFAnimNode_ControlRigBase;
	struct Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Source_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Source;
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_ControlRigBoneMapping_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ControlRigBoneMapping_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ControlRigBoneMapping_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_ControlRigBoneMapping;
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_ControlRigCurveMapping_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ControlRigCurveMapping_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ControlRigCurveMapping_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_ControlRigCurveMapping;
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_InputToCurveMappingUIDs_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_InputToCurveMappingUIDs_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputToCurveMappingUIDs_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_InputToCurveMappingUIDs;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NodeMappingContainer_MetaData[];
#endif
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_NodeMappingContainer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InputSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OutputSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutputSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bExecute_MetaData[];
#endif
		static void NewProp_bExecute_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bExecute;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Animation node that allows animation ControlRig output to be used in an animation graph\n */" },
		{ "ModuleRelativePath", "Public/AnimNode_ControlRigBase.h" },
		{ "ToolTip", "Animation node that allows animation ControlRig output to be used in an animation graph" },
	};
#endif
	void* Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FAnimNode_ControlRigBase>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_Source_MetaData[] = {
		{ "Category", "Links" },
		{ "ModuleRelativePath", "Public/AnimNode_ControlRigBase.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_Source = { "Source", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAnimNode_ControlRigBase, Source), Z_Construct_UScriptStruct_FPoseLink, METADATA_PARAMS(Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_Source_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_Source_MetaData)) };
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_ControlRigBoneMapping_ValueProp = { "ControlRigBoneMapping", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_ControlRigBoneMapping_Key_KeyProp = { "ControlRigBoneMapping_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_ControlRigBoneMapping_MetaData[] = {
		{ "Comment", "/** Rig Hierarchy bone name to required array index mapping */" },
		{ "ModuleRelativePath", "Public/AnimNode_ControlRigBase.h" },
		{ "ToolTip", "Rig Hierarchy bone name to required array index mapping" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_ControlRigBoneMapping = { "ControlRigBoneMapping", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAnimNode_ControlRigBase, ControlRigBoneMapping), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_ControlRigBoneMapping_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_ControlRigBoneMapping_MetaData)) };
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_ControlRigCurveMapping_ValueProp = { "ControlRigCurveMapping", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_ControlRigCurveMapping_Key_KeyProp = { "ControlRigCurveMapping_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_ControlRigCurveMapping_MetaData[] = {
		{ "Comment", "/** Rig Curve name to Curve LUI mapping */" },
		{ "ModuleRelativePath", "Public/AnimNode_ControlRigBase.h" },
		{ "ToolTip", "Rig Curve name to Curve LUI mapping" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_ControlRigCurveMapping = { "ControlRigCurveMapping", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAnimNode_ControlRigBase, ControlRigCurveMapping), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_ControlRigCurveMapping_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_ControlRigCurveMapping_MetaData)) };
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_InputToCurveMappingUIDs_ValueProp = { "InputToCurveMappingUIDs", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_InputToCurveMappingUIDs_Key_KeyProp = { "InputToCurveMappingUIDs_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_InputToCurveMappingUIDs_MetaData[] = {
		{ "ModuleRelativePath", "Public/AnimNode_ControlRigBase.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_InputToCurveMappingUIDs = { "InputToCurveMappingUIDs", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAnimNode_ControlRigBase, InputToCurveMappingUIDs), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_InputToCurveMappingUIDs_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_InputToCurveMappingUIDs_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_NodeMappingContainer_MetaData[] = {
		{ "Comment", "/** Node Mapping Container */" },
		{ "ModuleRelativePath", "Public/AnimNode_ControlRigBase.h" },
		{ "ToolTip", "Node Mapping Container" },
	};
#endif
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_NodeMappingContainer = { "NodeMappingContainer", nullptr, (EPropertyFlags)0x0024080000002000, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAnimNode_ControlRigBase, NodeMappingContainer), Z_Construct_UClass_UNodeMappingContainer_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_NodeMappingContainer_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_NodeMappingContainer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_InputSettings_MetaData[] = {
		{ "ModuleRelativePath", "Public/AnimNode_ControlRigBase.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_InputSettings = { "InputSettings", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAnimNode_ControlRigBase, InputSettings), Z_Construct_UScriptStruct_FControlRigIOSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_InputSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_InputSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_OutputSettings_MetaData[] = {
		{ "ModuleRelativePath", "Public/AnimNode_ControlRigBase.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_OutputSettings = { "OutputSettings", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAnimNode_ControlRigBase, OutputSettings), Z_Construct_UScriptStruct_FControlRigIOSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_OutputSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_OutputSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_bExecute_MetaData[] = {
		{ "ModuleRelativePath", "Public/AnimNode_ControlRigBase.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_bExecute_SetBit(void* Obj)
	{
		((FAnimNode_ControlRigBase*)Obj)->bExecute = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_bExecute = { "bExecute", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FAnimNode_ControlRigBase), &Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_bExecute_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_bExecute_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_bExecute_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_Source,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_ControlRigBoneMapping_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_ControlRigBoneMapping_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_ControlRigBoneMapping,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_ControlRigCurveMapping_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_ControlRigCurveMapping_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_ControlRigCurveMapping,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_InputToCurveMappingUIDs_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_InputToCurveMappingUIDs_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_InputToCurveMappingUIDs,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_NodeMappingContainer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_InputSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_OutputSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::NewProp_bExecute,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FAnimNode_CustomProperty,
		&NewStructOps,
		"AnimNode_ControlRigBase",
		sizeof(FAnimNode_ControlRigBase),
		alignof(FAnimNode_ControlRigBase),
		Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FAnimNode_ControlRigBase()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("AnimNode_ControlRigBase"), sizeof(FAnimNode_ControlRigBase), Get_Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FAnimNode_ControlRigBase_Hash() { return 1298359382U; }
class UScriptStruct* FControlRigIOSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FControlRigIOSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FControlRigIOSettings, Z_Construct_UPackage__Script_ControlRig(), TEXT("ControlRigIOSettings"), sizeof(FControlRigIOSettings), Get_Z_Construct_UScriptStruct_FControlRigIOSettings_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FControlRigIOSettings>()
{
	return FControlRigIOSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FControlRigIOSettings(FControlRigIOSettings::StaticStruct, TEXT("/Script/ControlRig"), TEXT("ControlRigIOSettings"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFControlRigIOSettings
{
	FScriptStruct_ControlRig_StaticRegisterNativesFControlRigIOSettings()
	{
		UScriptStruct::DeferCppStructOps<FControlRigIOSettings>(FName(TEXT("ControlRigIOSettings")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFControlRigIOSettings;
	struct Z_Construct_UScriptStruct_FControlRigIOSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUpdatePose_MetaData[];
#endif
		static void NewProp_bUpdatePose_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUpdatePose;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUpdateCurves_MetaData[];
#endif
		static void NewProp_bUpdateCurves_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUpdateCurves;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FControlRigIOSettings_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Struct defining the settings to override when driving a control rig */" },
		{ "ModuleRelativePath", "Public/AnimNode_ControlRigBase.h" },
		{ "ToolTip", "Struct defining the settings to override when driving a control rig" },
	};
#endif
	void* Z_Construct_UScriptStruct_FControlRigIOSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FControlRigIOSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FControlRigIOSettings_Statics::NewProp_bUpdatePose_MetaData[] = {
		{ "ModuleRelativePath", "Public/AnimNode_ControlRigBase.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FControlRigIOSettings_Statics::NewProp_bUpdatePose_SetBit(void* Obj)
	{
		((FControlRigIOSettings*)Obj)->bUpdatePose = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FControlRigIOSettings_Statics::NewProp_bUpdatePose = { "bUpdatePose", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FControlRigIOSettings), &Z_Construct_UScriptStruct_FControlRigIOSettings_Statics::NewProp_bUpdatePose_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FControlRigIOSettings_Statics::NewProp_bUpdatePose_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FControlRigIOSettings_Statics::NewProp_bUpdatePose_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FControlRigIOSettings_Statics::NewProp_bUpdateCurves_MetaData[] = {
		{ "ModuleRelativePath", "Public/AnimNode_ControlRigBase.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FControlRigIOSettings_Statics::NewProp_bUpdateCurves_SetBit(void* Obj)
	{
		((FControlRigIOSettings*)Obj)->bUpdateCurves = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FControlRigIOSettings_Statics::NewProp_bUpdateCurves = { "bUpdateCurves", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FControlRigIOSettings), &Z_Construct_UScriptStruct_FControlRigIOSettings_Statics::NewProp_bUpdateCurves_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FControlRigIOSettings_Statics::NewProp_bUpdateCurves_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FControlRigIOSettings_Statics::NewProp_bUpdateCurves_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FControlRigIOSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FControlRigIOSettings_Statics::NewProp_bUpdatePose,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FControlRigIOSettings_Statics::NewProp_bUpdateCurves,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FControlRigIOSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"ControlRigIOSettings",
		sizeof(FControlRigIOSettings),
		alignof(FControlRigIOSettings),
		Z_Construct_UScriptStruct_FControlRigIOSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FControlRigIOSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FControlRigIOSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FControlRigIOSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FControlRigIOSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FControlRigIOSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ControlRigIOSettings"), sizeof(FControlRigIOSettings), Get_Z_Construct_UScriptStruct_FControlRigIOSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FControlRigIOSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FControlRigIOSettings_Hash() { return 539112804U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
