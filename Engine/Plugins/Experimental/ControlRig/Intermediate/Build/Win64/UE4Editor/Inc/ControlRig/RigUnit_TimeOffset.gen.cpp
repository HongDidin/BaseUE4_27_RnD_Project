// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Simulation/RigUnit_TimeOffset.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_TimeOffset() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_SimBase();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_TimeOffsetTransform>() == std::is_polymorphic<FRigUnit_SimBase>(), "USTRUCT FRigUnit_TimeOffsetTransform cannot be polymorphic unless super FRigUnit_SimBase is polymorphic");

class UScriptStruct* FRigUnit_TimeOffsetTransform::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_TimeOffsetTransform"), sizeof(FRigUnit_TimeOffsetTransform), Get_Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_TimeOffsetTransform::Execute"), &FRigUnit_TimeOffsetTransform::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_TimeOffsetTransform>()
{
	return FRigUnit_TimeOffsetTransform::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_TimeOffsetTransform(FRigUnit_TimeOffsetTransform::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_TimeOffsetTransform"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_TimeOffsetTransform
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_TimeOffsetTransform()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_TimeOffsetTransform>(FName(TEXT("RigUnit_TimeOffsetTransform")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_TimeOffsetTransform;
	struct Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SecondsAgo_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SecondsAgo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BufferSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_BufferSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimeRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TimeRange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Buffer_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Buffer_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Buffer;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DeltaTimes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeltaTimes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DeltaTimes;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastInsertIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LastInsertIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UpperBound_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_UpperBound;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Records a value over time and can access the value from the past\n */" },
		{ "DisplayName", "Value Over Time (Transform)" },
		{ "Keywords", "Buffer,Delta,History,Previous,TimeOffset,Delay" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_TimeOffset.h" },
		{ "PrototypeName", "TimeOffset" },
		{ "ToolTip", "Records a value over time and can access the value from the past" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_TimeOffsetTransform>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_Value_MetaData[] = {
		{ "Comment", "/** The value to record */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_TimeOffset.h" },
		{ "ToolTip", "The value to record" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TimeOffsetTransform, Value), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_SecondsAgo_MetaData[] = {
		{ "Comment", "/** Seconds of time in the past you want to query the value for */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_TimeOffset.h" },
		{ "ToolTip", "Seconds of time in the past you want to query the value for" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_SecondsAgo = { "SecondsAgo", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TimeOffsetTransform, SecondsAgo), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_SecondsAgo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_SecondsAgo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_BufferSize_MetaData[] = {
		{ "Comment", "/** The sampling precision of the buffer. The higher the more precise - the more memory usage. */" },
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_TimeOffset.h" },
		{ "ToolTip", "The sampling precision of the buffer. The higher the more precise - the more memory usage." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_BufferSize = { "BufferSize", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TimeOffsetTransform, BufferSize), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_BufferSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_BufferSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_TimeRange_MetaData[] = {
		{ "Comment", "/** The maximum time required for offsetting in seconds. */" },
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_TimeOffset.h" },
		{ "ToolTip", "The maximum time required for offsetting in seconds." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_TimeRange = { "TimeRange", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TimeOffsetTransform, TimeRange), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_TimeRange_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_TimeRange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_TimeOffset.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TimeOffsetTransform, Result), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_Buffer_Inner = { "Buffer", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_Buffer_MetaData[] = {
		{ "ArraySize", "FMath::Clamp<int32>(BufferSize, 2, 512)" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_TimeOffset.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_Buffer = { "Buffer", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TimeOffsetTransform, Buffer), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_Buffer_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_Buffer_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_DeltaTimes_Inner = { "DeltaTimes", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_DeltaTimes_MetaData[] = {
		{ "ArraySize", "FMath::Clamp<int32>(BufferSize, 2, 512)" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_TimeOffset.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_DeltaTimes = { "DeltaTimes", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TimeOffsetTransform, DeltaTimes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_DeltaTimes_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_DeltaTimes_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_LastInsertIndex_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_TimeOffset.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_LastInsertIndex = { "LastInsertIndex", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TimeOffsetTransform, LastInsertIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_LastInsertIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_LastInsertIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_UpperBound_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_TimeOffset.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_UpperBound = { "UpperBound", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TimeOffsetTransform, UpperBound), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_UpperBound_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_UpperBound_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_SecondsAgo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_BufferSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_TimeRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_Result,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_Buffer_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_Buffer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_DeltaTimes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_DeltaTimes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_LastInsertIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::NewProp_UpperBound,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_SimBase,
		&NewStructOps,
		"RigUnit_TimeOffsetTransform",
		sizeof(FRigUnit_TimeOffsetTransform),
		alignof(FRigUnit_TimeOffsetTransform),
		Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_TimeOffsetTransform"), sizeof(FRigUnit_TimeOffsetTransform), Get_Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_TimeOffsetTransform_Hash() { return 1632997499U; }

void FRigUnit_TimeOffsetTransform::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
	Buffer.SetNum( FMath::Clamp<int32>(BufferSize, 2, 512) );
	FRigVMFixedArray<FTransform> Buffer_5_Array(Buffer);
	DeltaTimes.SetNum( FMath::Clamp<int32>(BufferSize, 2, 512) );
	FRigVMFixedArray<float> DeltaTimes_6_Array(DeltaTimes);
	
    StaticExecute(
		RigVMExecuteContext,
		Value,
		SecondsAgo,
		BufferSize,
		TimeRange,
		Result,
		Buffer_5_Array,
		DeltaTimes_6_Array,
		LastInsertIndex,
		UpperBound,
		Context
	);
}

int32 FRigUnit_TimeOffsetTransform::GetArraySize(const FName& InMemberName, const FRigVMUserDataArray& Context)
{
	if(InMemberName == TEXT("Buffer"))
	{
		return FMath::Clamp<int32>(BufferSize, 2, 512);
	}
	if(InMemberName == TEXT("DeltaTimes"))
	{
		return FMath::Clamp<int32>(BufferSize, 2, 512);
	}
	return INDEX_NONE;
}


static_assert(std::is_polymorphic<FRigUnit_TimeOffsetVector>() == std::is_polymorphic<FRigUnit_SimBase>(), "USTRUCT FRigUnit_TimeOffsetVector cannot be polymorphic unless super FRigUnit_SimBase is polymorphic");

class UScriptStruct* FRigUnit_TimeOffsetVector::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_TimeOffsetVector"), sizeof(FRigUnit_TimeOffsetVector), Get_Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_TimeOffsetVector::Execute"), &FRigUnit_TimeOffsetVector::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_TimeOffsetVector>()
{
	return FRigUnit_TimeOffsetVector::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_TimeOffsetVector(FRigUnit_TimeOffsetVector::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_TimeOffsetVector"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_TimeOffsetVector
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_TimeOffsetVector()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_TimeOffsetVector>(FName(TEXT("RigUnit_TimeOffsetVector")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_TimeOffsetVector;
	struct Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SecondsAgo_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SecondsAgo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BufferSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_BufferSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimeRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TimeRange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Buffer_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Buffer_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Buffer;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DeltaTimes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeltaTimes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DeltaTimes;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastInsertIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LastInsertIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UpperBound_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_UpperBound;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Records a value over time and can access the value from the past\n */" },
		{ "DisplayName", "Value Over Time (Vector)" },
		{ "Keywords", "Buffer,Delta,History,Previous,TimeOffset,Delay" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_TimeOffset.h" },
		{ "PrototypeName", "TimeOffset" },
		{ "ToolTip", "Records a value over time and can access the value from the past" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_TimeOffsetVector>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_Value_MetaData[] = {
		{ "Comment", "/** The value to record */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_TimeOffset.h" },
		{ "ToolTip", "The value to record" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TimeOffsetVector, Value), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_SecondsAgo_MetaData[] = {
		{ "Comment", "/** Seconds of time in the past you want to query the value for */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_TimeOffset.h" },
		{ "ToolTip", "Seconds of time in the past you want to query the value for" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_SecondsAgo = { "SecondsAgo", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TimeOffsetVector, SecondsAgo), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_SecondsAgo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_SecondsAgo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_BufferSize_MetaData[] = {
		{ "Comment", "/** The sampling precision of the buffer. The higher the more precise - the more memory usage. */" },
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_TimeOffset.h" },
		{ "ToolTip", "The sampling precision of the buffer. The higher the more precise - the more memory usage." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_BufferSize = { "BufferSize", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TimeOffsetVector, BufferSize), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_BufferSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_BufferSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_TimeRange_MetaData[] = {
		{ "Comment", "/** The maximum time required for offsetting in seconds. */" },
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_TimeOffset.h" },
		{ "ToolTip", "The maximum time required for offsetting in seconds." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_TimeRange = { "TimeRange", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TimeOffsetVector, TimeRange), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_TimeRange_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_TimeRange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_TimeOffset.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TimeOffsetVector, Result), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_Buffer_Inner = { "Buffer", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_Buffer_MetaData[] = {
		{ "ArraySize", "FMath::Clamp<int32>(BufferSize, 2, 512)" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_TimeOffset.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_Buffer = { "Buffer", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TimeOffsetVector, Buffer), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_Buffer_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_Buffer_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_DeltaTimes_Inner = { "DeltaTimes", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_DeltaTimes_MetaData[] = {
		{ "ArraySize", "FMath::Clamp<int32>(BufferSize, 2, 512)" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_TimeOffset.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_DeltaTimes = { "DeltaTimes", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TimeOffsetVector, DeltaTimes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_DeltaTimes_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_DeltaTimes_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_LastInsertIndex_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_TimeOffset.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_LastInsertIndex = { "LastInsertIndex", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TimeOffsetVector, LastInsertIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_LastInsertIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_LastInsertIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_UpperBound_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_TimeOffset.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_UpperBound = { "UpperBound", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TimeOffsetVector, UpperBound), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_UpperBound_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_UpperBound_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_SecondsAgo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_BufferSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_TimeRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_Result,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_Buffer_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_Buffer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_DeltaTimes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_DeltaTimes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_LastInsertIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::NewProp_UpperBound,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_SimBase,
		&NewStructOps,
		"RigUnit_TimeOffsetVector",
		sizeof(FRigUnit_TimeOffsetVector),
		alignof(FRigUnit_TimeOffsetVector),
		Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_TimeOffsetVector"), sizeof(FRigUnit_TimeOffsetVector), Get_Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_TimeOffsetVector_Hash() { return 2253960791U; }

void FRigUnit_TimeOffsetVector::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
	Buffer.SetNum( FMath::Clamp<int32>(BufferSize, 2, 512) );
	FRigVMFixedArray<FVector> Buffer_5_Array(Buffer);
	DeltaTimes.SetNum( FMath::Clamp<int32>(BufferSize, 2, 512) );
	FRigVMFixedArray<float> DeltaTimes_6_Array(DeltaTimes);
	
    StaticExecute(
		RigVMExecuteContext,
		Value,
		SecondsAgo,
		BufferSize,
		TimeRange,
		Result,
		Buffer_5_Array,
		DeltaTimes_6_Array,
		LastInsertIndex,
		UpperBound,
		Context
	);
}

int32 FRigUnit_TimeOffsetVector::GetArraySize(const FName& InMemberName, const FRigVMUserDataArray& Context)
{
	if(InMemberName == TEXT("Buffer"))
	{
		return FMath::Clamp<int32>(BufferSize, 2, 512);
	}
	if(InMemberName == TEXT("DeltaTimes"))
	{
		return FMath::Clamp<int32>(BufferSize, 2, 512);
	}
	return INDEX_NONE;
}


static_assert(std::is_polymorphic<FRigUnit_TimeOffsetFloat>() == std::is_polymorphic<FRigUnit_SimBase>(), "USTRUCT FRigUnit_TimeOffsetFloat cannot be polymorphic unless super FRigUnit_SimBase is polymorphic");

class UScriptStruct* FRigUnit_TimeOffsetFloat::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_TimeOffsetFloat"), sizeof(FRigUnit_TimeOffsetFloat), Get_Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_TimeOffsetFloat::Execute"), &FRigUnit_TimeOffsetFloat::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_TimeOffsetFloat>()
{
	return FRigUnit_TimeOffsetFloat::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_TimeOffsetFloat(FRigUnit_TimeOffsetFloat::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_TimeOffsetFloat"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_TimeOffsetFloat
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_TimeOffsetFloat()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_TimeOffsetFloat>(FName(TEXT("RigUnit_TimeOffsetFloat")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_TimeOffsetFloat;
	struct Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SecondsAgo_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SecondsAgo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BufferSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_BufferSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimeRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TimeRange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Buffer_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Buffer_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Buffer;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DeltaTimes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeltaTimes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DeltaTimes;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastInsertIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LastInsertIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UpperBound_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_UpperBound;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Records a value over time and can access the value from the past\n */" },
		{ "DisplayName", "Value Over Time (Float)" },
		{ "Keywords", "Buffer,Delta,History,Previous,TimeOffset,Delay" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_TimeOffset.h" },
		{ "PrototypeName", "TimeOffset" },
		{ "ToolTip", "Records a value over time and can access the value from the past" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_TimeOffsetFloat>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_Value_MetaData[] = {
		{ "Comment", "/** The value to record */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_TimeOffset.h" },
		{ "ToolTip", "The value to record" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TimeOffsetFloat, Value), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_SecondsAgo_MetaData[] = {
		{ "Comment", "/** Seconds of time in the past you want to query the value for */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_TimeOffset.h" },
		{ "ToolTip", "Seconds of time in the past you want to query the value for" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_SecondsAgo = { "SecondsAgo", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TimeOffsetFloat, SecondsAgo), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_SecondsAgo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_SecondsAgo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_BufferSize_MetaData[] = {
		{ "Comment", "/** The sampling precision of the buffer. The higher the more precise - the more memory usage. */" },
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_TimeOffset.h" },
		{ "ToolTip", "The sampling precision of the buffer. The higher the more precise - the more memory usage." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_BufferSize = { "BufferSize", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TimeOffsetFloat, BufferSize), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_BufferSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_BufferSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_TimeRange_MetaData[] = {
		{ "Comment", "/** The maximum time required for offsetting in seconds. */" },
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_TimeOffset.h" },
		{ "ToolTip", "The maximum time required for offsetting in seconds." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_TimeRange = { "TimeRange", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TimeOffsetFloat, TimeRange), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_TimeRange_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_TimeRange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_TimeOffset.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TimeOffsetFloat, Result), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_Buffer_Inner = { "Buffer", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_Buffer_MetaData[] = {
		{ "ArraySize", "FMath::Clamp<int32>(BufferSize, 2, 512)" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_TimeOffset.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_Buffer = { "Buffer", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TimeOffsetFloat, Buffer), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_Buffer_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_Buffer_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_DeltaTimes_Inner = { "DeltaTimes", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_DeltaTimes_MetaData[] = {
		{ "ArraySize", "FMath::Clamp<int32>(BufferSize, 2, 512)" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_TimeOffset.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_DeltaTimes = { "DeltaTimes", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TimeOffsetFloat, DeltaTimes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_DeltaTimes_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_DeltaTimes_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_LastInsertIndex_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_TimeOffset.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_LastInsertIndex = { "LastInsertIndex", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TimeOffsetFloat, LastInsertIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_LastInsertIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_LastInsertIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_UpperBound_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_TimeOffset.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_UpperBound = { "UpperBound", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TimeOffsetFloat, UpperBound), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_UpperBound_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_UpperBound_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_SecondsAgo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_BufferSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_TimeRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_Result,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_Buffer_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_Buffer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_DeltaTimes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_DeltaTimes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_LastInsertIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::NewProp_UpperBound,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_SimBase,
		&NewStructOps,
		"RigUnit_TimeOffsetFloat",
		sizeof(FRigUnit_TimeOffsetFloat),
		alignof(FRigUnit_TimeOffsetFloat),
		Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_TimeOffsetFloat"), sizeof(FRigUnit_TimeOffsetFloat), Get_Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_TimeOffsetFloat_Hash() { return 2931219550U; }

void FRigUnit_TimeOffsetFloat::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
	Buffer.SetNum( FMath::Clamp<int32>(BufferSize, 2, 512) );
	FRigVMFixedArray<float> Buffer_5_Array(Buffer);
	DeltaTimes.SetNum( FMath::Clamp<int32>(BufferSize, 2, 512) );
	FRigVMFixedArray<float> DeltaTimes_6_Array(DeltaTimes);
	
    StaticExecute(
		RigVMExecuteContext,
		Value,
		SecondsAgo,
		BufferSize,
		TimeRange,
		Result,
		Buffer_5_Array,
		DeltaTimes_6_Array,
		LastInsertIndex,
		UpperBound,
		Context
	);
}

int32 FRigUnit_TimeOffsetFloat::GetArraySize(const FName& InMemberName, const FRigVMUserDataArray& Context)
{
	if(InMemberName == TEXT("Buffer"))
	{
		return FMath::Clamp<int32>(BufferSize, 2, 512);
	}
	if(InMemberName == TEXT("DeltaTimes"))
	{
		return FMath::Clamp<int32>(BufferSize, 2, 512);
	}
	return INDEX_NONE;
}

PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
