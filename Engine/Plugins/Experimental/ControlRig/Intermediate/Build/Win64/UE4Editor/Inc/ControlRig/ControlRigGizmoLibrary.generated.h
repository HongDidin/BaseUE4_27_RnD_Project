// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_ControlRigGizmoLibrary_generated_h
#error "ControlRigGizmoLibrary.generated.h already included, missing '#pragma once' in ControlRigGizmoLibrary.h"
#endif
#define CONTROLRIG_ControlRigGizmoLibrary_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoLibrary_h_16_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FControlRigGizmoDefinition_Statics; \
	static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FControlRigGizmoDefinition>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoLibrary_h_38_SPARSE_DATA
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoLibrary_h_38_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoLibrary_h_38_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoLibrary_h_38_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUControlRigGizmoLibrary(); \
	friend struct Z_Construct_UClass_UControlRigGizmoLibrary_Statics; \
public: \
	DECLARE_CLASS(UControlRigGizmoLibrary, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRig"), NO_API) \
	DECLARE_SERIALIZER(UControlRigGizmoLibrary)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoLibrary_h_38_INCLASS \
private: \
	static void StaticRegisterNativesUControlRigGizmoLibrary(); \
	friend struct Z_Construct_UClass_UControlRigGizmoLibrary_Statics; \
public: \
	DECLARE_CLASS(UControlRigGizmoLibrary, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRig"), NO_API) \
	DECLARE_SERIALIZER(UControlRigGizmoLibrary)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoLibrary_h_38_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UControlRigGizmoLibrary(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigGizmoLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigGizmoLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigGizmoLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigGizmoLibrary(UControlRigGizmoLibrary&&); \
	NO_API UControlRigGizmoLibrary(const UControlRigGizmoLibrary&); \
public:


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoLibrary_h_38_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigGizmoLibrary(UControlRigGizmoLibrary&&); \
	NO_API UControlRigGizmoLibrary(const UControlRigGizmoLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigGizmoLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigGizmoLibrary); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UControlRigGizmoLibrary)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoLibrary_h_38_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoLibrary_h_35_PROLOG
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoLibrary_h_38_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoLibrary_h_38_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoLibrary_h_38_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoLibrary_h_38_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoLibrary_h_38_INCLASS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoLibrary_h_38_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoLibrary_h_38_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoLibrary_h_38_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoLibrary_h_38_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoLibrary_h_38_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoLibrary_h_38_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoLibrary_h_38_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONTROLRIG_API UClass* StaticClass<class UControlRigGizmoLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigGizmoLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
