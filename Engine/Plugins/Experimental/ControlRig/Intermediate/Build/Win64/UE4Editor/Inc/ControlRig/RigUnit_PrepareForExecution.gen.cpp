// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Execution/RigUnit_PrepareForExecution.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_PrepareForExecution() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_PrepareForExecution();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FControlRigExecuteContext();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_PrepareForExecution>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_PrepareForExecution cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_PrepareForExecution::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_PrepareForExecution_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_PrepareForExecution, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_PrepareForExecution"), sizeof(FRigUnit_PrepareForExecution), Get_Z_Construct_UScriptStruct_FRigUnit_PrepareForExecution_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_PrepareForExecution::Execute"), &FRigUnit_PrepareForExecution::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_PrepareForExecution>()
{
	return FRigUnit_PrepareForExecution::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_PrepareForExecution(FRigUnit_PrepareForExecution::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_PrepareForExecution"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_PrepareForExecution
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_PrepareForExecution()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_PrepareForExecution>(FName(TEXT("RigUnit_PrepareForExecution")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_PrepareForExecution;
	struct Z_Construct_UScriptStruct_FRigUnit_PrepareForExecution_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExecuteContext_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ExecuteContext;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_PrepareForExecution_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Setup" },
		{ "Comment", "/**\n * Event to setup elements before all solves\n */" },
		{ "DisplayName", "Setup Event" },
		{ "Keywords", "Setup,Init,Fit" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_PrepareForExecution.h" },
		{ "NodeColor", "0.1 0.1 0.1" },
		{ "TitleColor", "1 0 0" },
		{ "ToolTip", "Event to setup elements before all solves" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_PrepareForExecution_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_PrepareForExecution>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_PrepareForExecution_Statics::NewProp_ExecuteContext_MetaData[] = {
		{ "Category", "PrepareForExecution" },
		{ "Comment", "// The execution result\n" },
		{ "DisplayName", "Execute" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_PrepareForExecution.h" },
		{ "Output", "" },
		{ "ToolTip", "The execution result" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_PrepareForExecution_Statics::NewProp_ExecuteContext = { "ExecuteContext", nullptr, (EPropertyFlags)0x0010000000002001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_PrepareForExecution, ExecuteContext), Z_Construct_UScriptStruct_FControlRigExecuteContext, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_PrepareForExecution_Statics::NewProp_ExecuteContext_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PrepareForExecution_Statics::NewProp_ExecuteContext_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_PrepareForExecution_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_PrepareForExecution_Statics::NewProp_ExecuteContext,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_PrepareForExecution_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_PrepareForExecution",
		sizeof(FRigUnit_PrepareForExecution),
		alignof(FRigUnit_PrepareForExecution),
		Z_Construct_UScriptStruct_FRigUnit_PrepareForExecution_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PrepareForExecution_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_PrepareForExecution_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PrepareForExecution_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_PrepareForExecution()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_PrepareForExecution_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_PrepareForExecution"), sizeof(FRigUnit_PrepareForExecution), Get_Z_Construct_UScriptStruct_FRigUnit_PrepareForExecution_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_PrepareForExecution_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_PrepareForExecution_Hash() { return 140743167U; }

void FRigUnit_PrepareForExecution::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		ExecuteContext,
		Context
	);
}

PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
