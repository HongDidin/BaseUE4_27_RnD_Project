// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Public/AnimNode_ControlRig_ExternalSource.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAnimNode_ControlRig_ExternalSource() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FAnimNode_ControlRig_ExternalSource();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FAnimNode_ControlRigBase();
	CONTROLRIG_API UClass* Z_Construct_UClass_UControlRig_NoRegister();
// End Cross Module References

static_assert(std::is_polymorphic<FAnimNode_ControlRig_ExternalSource>() == std::is_polymorphic<FAnimNode_ControlRigBase>(), "USTRUCT FAnimNode_ControlRig_ExternalSource cannot be polymorphic unless super FAnimNode_ControlRigBase is polymorphic");

class UScriptStruct* FAnimNode_ControlRig_ExternalSource::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FAnimNode_ControlRig_ExternalSource_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FAnimNode_ControlRig_ExternalSource, Z_Construct_UPackage__Script_ControlRig(), TEXT("AnimNode_ControlRig_ExternalSource"), sizeof(FAnimNode_ControlRig_ExternalSource), Get_Z_Construct_UScriptStruct_FAnimNode_ControlRig_ExternalSource_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FAnimNode_ControlRig_ExternalSource>()
{
	return FAnimNode_ControlRig_ExternalSource::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FAnimNode_ControlRig_ExternalSource(FAnimNode_ControlRig_ExternalSource::StaticStruct, TEXT("/Script/ControlRig"), TEXT("AnimNode_ControlRig_ExternalSource"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFAnimNode_ControlRig_ExternalSource
{
	FScriptStruct_ControlRig_StaticRegisterNativesFAnimNode_ControlRig_ExternalSource()
	{
		UScriptStruct::DeferCppStructOps<FAnimNode_ControlRig_ExternalSource>(FName(TEXT("AnimNode_ControlRig_ExternalSource")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFAnimNode_ControlRig_ExternalSource;
	struct Z_Construct_UScriptStruct_FAnimNode_ControlRig_ExternalSource_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ControlRig_MetaData[];
#endif
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_ControlRig;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAnimNode_ControlRig_ExternalSource_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Animation node that allows animation ControlRig output to be used in an animation graph\n */" },
		{ "ModuleRelativePath", "Public/AnimNode_ControlRig_ExternalSource.h" },
		{ "ToolTip", "Animation node that allows animation ControlRig output to be used in an animation graph" },
	};
#endif
	void* Z_Construct_UScriptStruct_FAnimNode_ControlRig_ExternalSource_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FAnimNode_ControlRig_ExternalSource>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAnimNode_ControlRig_ExternalSource_Statics::NewProp_ControlRig_MetaData[] = {
		{ "ModuleRelativePath", "Public/AnimNode_ControlRig_ExternalSource.h" },
	};
#endif
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UScriptStruct_FAnimNode_ControlRig_ExternalSource_Statics::NewProp_ControlRig = { "ControlRig", nullptr, (EPropertyFlags)0x0044000000002000, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAnimNode_ControlRig_ExternalSource, ControlRig), Z_Construct_UClass_UControlRig_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FAnimNode_ControlRig_ExternalSource_Statics::NewProp_ControlRig_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_ControlRig_ExternalSource_Statics::NewProp_ControlRig_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FAnimNode_ControlRig_ExternalSource_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_ControlRig_ExternalSource_Statics::NewProp_ControlRig,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FAnimNode_ControlRig_ExternalSource_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FAnimNode_ControlRigBase,
		&NewStructOps,
		"AnimNode_ControlRig_ExternalSource",
		sizeof(FAnimNode_ControlRig_ExternalSource),
		alignof(FAnimNode_ControlRig_ExternalSource),
		Z_Construct_UScriptStruct_FAnimNode_ControlRig_ExternalSource_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_ControlRig_ExternalSource_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FAnimNode_ControlRig_ExternalSource_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_ControlRig_ExternalSource_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FAnimNode_ControlRig_ExternalSource()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FAnimNode_ControlRig_ExternalSource_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("AnimNode_ControlRig_ExternalSource"), sizeof(FAnimNode_ControlRig_ExternalSource), Get_Z_Construct_UScriptStruct_FAnimNode_ControlRig_ExternalSource_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FAnimNode_ControlRig_ExternalSource_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FAnimNode_ControlRig_ExternalSource_Hash() { return 782888914U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
