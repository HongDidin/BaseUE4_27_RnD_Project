// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Simulation/RigUnit_DeltaFromPrevious.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_DeltaFromPrevious() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousTransform();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_SimBase();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousQuat();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FQuat();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousFloat();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_DeltaFromPreviousTransform>() == std::is_polymorphic<FRigUnit_SimBase>(), "USTRUCT FRigUnit_DeltaFromPreviousTransform cannot be polymorphic unless super FRigUnit_SimBase is polymorphic");

class UScriptStruct* FRigUnit_DeltaFromPreviousTransform::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousTransform_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousTransform, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_DeltaFromPreviousTransform"), sizeof(FRigUnit_DeltaFromPreviousTransform), Get_Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousTransform_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_DeltaFromPreviousTransform::Execute"), &FRigUnit_DeltaFromPreviousTransform::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_DeltaFromPreviousTransform>()
{
	return FRigUnit_DeltaFromPreviousTransform::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_DeltaFromPreviousTransform(FRigUnit_DeltaFromPreviousTransform::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_DeltaFromPreviousTransform"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DeltaFromPreviousTransform
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DeltaFromPreviousTransform()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_DeltaFromPreviousTransform>(FName(TEXT("RigUnit_DeltaFromPreviousTransform")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DeltaFromPreviousTransform;
	struct Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousTransform_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Delta_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Delta;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviousValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PreviousValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Cache_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Cache;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousTransform_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Computes the difference from the previous value going through the node\n */" },
		{ "DisplayName", "DeltaFromPrevious (Transform)" },
		{ "Keywords", "Difference,Velocity,Acceleration" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_DeltaFromPrevious.h" },
		{ "PrototypeName", "DeltaFromPrevious" },
		{ "ToolTip", "Computes the difference from the previous value going through the node" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousTransform_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_DeltaFromPreviousTransform>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousTransform_Statics::NewProp_Value_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_DeltaFromPrevious.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousTransform_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DeltaFromPreviousTransform, Value), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousTransform_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousTransform_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousTransform_Statics::NewProp_Delta_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_DeltaFromPrevious.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousTransform_Statics::NewProp_Delta = { "Delta", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DeltaFromPreviousTransform, Delta), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousTransform_Statics::NewProp_Delta_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousTransform_Statics::NewProp_Delta_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousTransform_Statics::NewProp_PreviousValue_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_DeltaFromPrevious.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousTransform_Statics::NewProp_PreviousValue = { "PreviousValue", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DeltaFromPreviousTransform, PreviousValue), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousTransform_Statics::NewProp_PreviousValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousTransform_Statics::NewProp_PreviousValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousTransform_Statics::NewProp_Cache_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_DeltaFromPrevious.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousTransform_Statics::NewProp_Cache = { "Cache", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DeltaFromPreviousTransform, Cache), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousTransform_Statics::NewProp_Cache_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousTransform_Statics::NewProp_Cache_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousTransform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousTransform_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousTransform_Statics::NewProp_Delta,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousTransform_Statics::NewProp_PreviousValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousTransform_Statics::NewProp_Cache,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousTransform_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_SimBase,
		&NewStructOps,
		"RigUnit_DeltaFromPreviousTransform",
		sizeof(FRigUnit_DeltaFromPreviousTransform),
		alignof(FRigUnit_DeltaFromPreviousTransform),
		Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousTransform_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousTransform_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousTransform_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousTransform_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousTransform()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousTransform_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_DeltaFromPreviousTransform"), sizeof(FRigUnit_DeltaFromPreviousTransform), Get_Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousTransform_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousTransform_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousTransform_Hash() { return 3147317010U; }

void FRigUnit_DeltaFromPreviousTransform::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Delta,
		PreviousValue,
		Cache,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_DeltaFromPreviousQuat>() == std::is_polymorphic<FRigUnit_SimBase>(), "USTRUCT FRigUnit_DeltaFromPreviousQuat cannot be polymorphic unless super FRigUnit_SimBase is polymorphic");

class UScriptStruct* FRigUnit_DeltaFromPreviousQuat::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousQuat_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousQuat, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_DeltaFromPreviousQuat"), sizeof(FRigUnit_DeltaFromPreviousQuat), Get_Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousQuat_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_DeltaFromPreviousQuat::Execute"), &FRigUnit_DeltaFromPreviousQuat::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_DeltaFromPreviousQuat>()
{
	return FRigUnit_DeltaFromPreviousQuat::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_DeltaFromPreviousQuat(FRigUnit_DeltaFromPreviousQuat::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_DeltaFromPreviousQuat"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DeltaFromPreviousQuat
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DeltaFromPreviousQuat()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_DeltaFromPreviousQuat>(FName(TEXT("RigUnit_DeltaFromPreviousQuat")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DeltaFromPreviousQuat;
	struct Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousQuat_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Delta_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Delta;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviousValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PreviousValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Cache_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Cache;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousQuat_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Computes the difference from the previous value going through the node\n */" },
		{ "DisplayName", "DeltaFromPrevious (Quaternion)" },
		{ "Keywords", "Difference,Velocity,Acceleration" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_DeltaFromPrevious.h" },
		{ "PrototypeName", "DeltaFromPrevious" },
		{ "ToolTip", "Computes the difference from the previous value going through the node" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousQuat_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_DeltaFromPreviousQuat>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousQuat_Statics::NewProp_Value_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_DeltaFromPrevious.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousQuat_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DeltaFromPreviousQuat, Value), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousQuat_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousQuat_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousQuat_Statics::NewProp_Delta_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_DeltaFromPrevious.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousQuat_Statics::NewProp_Delta = { "Delta", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DeltaFromPreviousQuat, Delta), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousQuat_Statics::NewProp_Delta_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousQuat_Statics::NewProp_Delta_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousQuat_Statics::NewProp_PreviousValue_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_DeltaFromPrevious.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousQuat_Statics::NewProp_PreviousValue = { "PreviousValue", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DeltaFromPreviousQuat, PreviousValue), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousQuat_Statics::NewProp_PreviousValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousQuat_Statics::NewProp_PreviousValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousQuat_Statics::NewProp_Cache_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_DeltaFromPrevious.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousQuat_Statics::NewProp_Cache = { "Cache", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DeltaFromPreviousQuat, Cache), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousQuat_Statics::NewProp_Cache_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousQuat_Statics::NewProp_Cache_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousQuat_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousQuat_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousQuat_Statics::NewProp_Delta,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousQuat_Statics::NewProp_PreviousValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousQuat_Statics::NewProp_Cache,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousQuat_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_SimBase,
		&NewStructOps,
		"RigUnit_DeltaFromPreviousQuat",
		sizeof(FRigUnit_DeltaFromPreviousQuat),
		alignof(FRigUnit_DeltaFromPreviousQuat),
		Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousQuat_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousQuat_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousQuat_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousQuat_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousQuat()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousQuat_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_DeltaFromPreviousQuat"), sizeof(FRigUnit_DeltaFromPreviousQuat), Get_Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousQuat_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousQuat_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousQuat_Hash() { return 44027430U; }

void FRigUnit_DeltaFromPreviousQuat::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Delta,
		PreviousValue,
		Cache,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_DeltaFromPreviousVector>() == std::is_polymorphic<FRigUnit_SimBase>(), "USTRUCT FRigUnit_DeltaFromPreviousVector cannot be polymorphic unless super FRigUnit_SimBase is polymorphic");

class UScriptStruct* FRigUnit_DeltaFromPreviousVector::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousVector_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousVector, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_DeltaFromPreviousVector"), sizeof(FRigUnit_DeltaFromPreviousVector), Get_Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousVector_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_DeltaFromPreviousVector::Execute"), &FRigUnit_DeltaFromPreviousVector::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_DeltaFromPreviousVector>()
{
	return FRigUnit_DeltaFromPreviousVector::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_DeltaFromPreviousVector(FRigUnit_DeltaFromPreviousVector::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_DeltaFromPreviousVector"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DeltaFromPreviousVector
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DeltaFromPreviousVector()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_DeltaFromPreviousVector>(FName(TEXT("RigUnit_DeltaFromPreviousVector")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DeltaFromPreviousVector;
	struct Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousVector_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Delta_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Delta;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviousValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PreviousValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Cache_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Cache;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousVector_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Computes the difference from the previous value going through the node\n */" },
		{ "DisplayName", "DeltaFromPrevious (Vector)" },
		{ "Keywords", "Difference,Velocity,Acceleration" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_DeltaFromPrevious.h" },
		{ "PrototypeName", "DeltaFromPrevious" },
		{ "ToolTip", "Computes the difference from the previous value going through the node" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousVector_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_DeltaFromPreviousVector>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousVector_Statics::NewProp_Value_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_DeltaFromPrevious.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousVector_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DeltaFromPreviousVector, Value), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousVector_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousVector_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousVector_Statics::NewProp_Delta_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_DeltaFromPrevious.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousVector_Statics::NewProp_Delta = { "Delta", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DeltaFromPreviousVector, Delta), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousVector_Statics::NewProp_Delta_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousVector_Statics::NewProp_Delta_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousVector_Statics::NewProp_PreviousValue_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_DeltaFromPrevious.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousVector_Statics::NewProp_PreviousValue = { "PreviousValue", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DeltaFromPreviousVector, PreviousValue), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousVector_Statics::NewProp_PreviousValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousVector_Statics::NewProp_PreviousValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousVector_Statics::NewProp_Cache_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_DeltaFromPrevious.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousVector_Statics::NewProp_Cache = { "Cache", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DeltaFromPreviousVector, Cache), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousVector_Statics::NewProp_Cache_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousVector_Statics::NewProp_Cache_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousVector_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousVector_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousVector_Statics::NewProp_Delta,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousVector_Statics::NewProp_PreviousValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousVector_Statics::NewProp_Cache,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousVector_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_SimBase,
		&NewStructOps,
		"RigUnit_DeltaFromPreviousVector",
		sizeof(FRigUnit_DeltaFromPreviousVector),
		alignof(FRigUnit_DeltaFromPreviousVector),
		Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousVector_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousVector_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousVector_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousVector_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousVector()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousVector_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_DeltaFromPreviousVector"), sizeof(FRigUnit_DeltaFromPreviousVector), Get_Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousVector_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousVector_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousVector_Hash() { return 2172191519U; }

void FRigUnit_DeltaFromPreviousVector::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Delta,
		PreviousValue,
		Cache,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_DeltaFromPreviousFloat>() == std::is_polymorphic<FRigUnit_SimBase>(), "USTRUCT FRigUnit_DeltaFromPreviousFloat cannot be polymorphic unless super FRigUnit_SimBase is polymorphic");

class UScriptStruct* FRigUnit_DeltaFromPreviousFloat::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousFloat_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousFloat, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_DeltaFromPreviousFloat"), sizeof(FRigUnit_DeltaFromPreviousFloat), Get_Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousFloat_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_DeltaFromPreviousFloat::Execute"), &FRigUnit_DeltaFromPreviousFloat::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_DeltaFromPreviousFloat>()
{
	return FRigUnit_DeltaFromPreviousFloat::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_DeltaFromPreviousFloat(FRigUnit_DeltaFromPreviousFloat::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_DeltaFromPreviousFloat"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DeltaFromPreviousFloat
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DeltaFromPreviousFloat()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_DeltaFromPreviousFloat>(FName(TEXT("RigUnit_DeltaFromPreviousFloat")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DeltaFromPreviousFloat;
	struct Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousFloat_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Delta_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Delta;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviousValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PreviousValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Cache_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Cache;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousFloat_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Computes the difference from the previous value going through the node\n */" },
		{ "DisplayName", "DeltaFromPrevious (Float)" },
		{ "Keywords", "Difference,Velocity,Acceleration" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_DeltaFromPrevious.h" },
		{ "PrototypeName", "DeltaFromPrevious" },
		{ "ToolTip", "Computes the difference from the previous value going through the node" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousFloat_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_DeltaFromPreviousFloat>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousFloat_Statics::NewProp_Value_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_DeltaFromPrevious.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousFloat_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DeltaFromPreviousFloat, Value), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousFloat_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousFloat_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousFloat_Statics::NewProp_Delta_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_DeltaFromPrevious.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousFloat_Statics::NewProp_Delta = { "Delta", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DeltaFromPreviousFloat, Delta), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousFloat_Statics::NewProp_Delta_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousFloat_Statics::NewProp_Delta_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousFloat_Statics::NewProp_PreviousValue_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_DeltaFromPrevious.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousFloat_Statics::NewProp_PreviousValue = { "PreviousValue", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DeltaFromPreviousFloat, PreviousValue), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousFloat_Statics::NewProp_PreviousValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousFloat_Statics::NewProp_PreviousValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousFloat_Statics::NewProp_Cache_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_DeltaFromPrevious.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousFloat_Statics::NewProp_Cache = { "Cache", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DeltaFromPreviousFloat, Cache), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousFloat_Statics::NewProp_Cache_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousFloat_Statics::NewProp_Cache_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousFloat_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousFloat_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousFloat_Statics::NewProp_Delta,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousFloat_Statics::NewProp_PreviousValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousFloat_Statics::NewProp_Cache,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousFloat_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_SimBase,
		&NewStructOps,
		"RigUnit_DeltaFromPreviousFloat",
		sizeof(FRigUnit_DeltaFromPreviousFloat),
		alignof(FRigUnit_DeltaFromPreviousFloat),
		Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousFloat_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousFloat_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousFloat_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousFloat_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousFloat()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousFloat_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_DeltaFromPreviousFloat"), sizeof(FRigUnit_DeltaFromPreviousFloat), Get_Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousFloat_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousFloat_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DeltaFromPreviousFloat_Hash() { return 954611034U; }

void FRigUnit_DeltaFromPreviousFloat::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Delta,
		PreviousValue,
		Cache,
		Context
	);
}

PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
