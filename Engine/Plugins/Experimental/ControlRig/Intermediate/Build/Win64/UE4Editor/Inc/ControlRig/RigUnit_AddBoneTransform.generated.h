// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigUnit_AddBoneTransform_generated_h
#error "RigUnit_AddBoneTransform.generated.h already included, missing '#pragma once' in RigUnit_AddBoneTransform.h"
#endif
#define CONTROLRIG_RigUnit_AddBoneTransform_generated_h


#define FRigUnit_AddBoneTransform_Execute() \
	void FRigUnit_AddBoneTransform::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Bone, \
		const FTransform& Transform, \
		const float Weight, \
		const bool bPostMultiply, \
		const bool bPropagateToChildren, \
		FCachedRigElement& CachedBone, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Units_Hierarchy_RigUnit_AddBoneTransform_h_15_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_AddBoneTransform_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Bone, \
		const FTransform& Transform, \
		const float Weight, \
		const bool bPostMultiply, \
		const bool bPropagateToChildren, \
		FCachedRigElement& CachedBone, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FName& Bone = *(FName*)RigVMMemoryHandles[0].GetData(); \
		const FTransform& Transform = *(FTransform*)RigVMMemoryHandles[1].GetData(); \
		const float Weight = *(float*)RigVMMemoryHandles[2].GetData(); \
		const bool bPostMultiply = *(bool*)RigVMMemoryHandles[3].GetData(); \
		const bool bPropagateToChildren = *(bool*)RigVMMemoryHandles[4].GetData(); \
		FRigVMDynamicArray<FCachedRigElement> CachedBone_5_Array(*((FRigVMByteArray*)RigVMMemoryHandles[5].GetData(0, false))); \
		CachedBone_5_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedBone = CachedBone_5_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[6].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Bone, \
			Transform, \
			Weight, \
			bPostMultiply, \
			bPropagateToChildren, \
			CachedBone, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnitMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_AddBoneTransform>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Units_Hierarchy_RigUnit_AddBoneTransform_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
