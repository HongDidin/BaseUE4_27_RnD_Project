// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_ControlRigValidationPass_generated_h
#error "ControlRigValidationPass.generated.h already included, missing '#pragma once' in ControlRigValidationPass.h"
#endif
#define CONTROLRIG_ControlRigValidationPass_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_18_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FControlRigValidationContext_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FControlRigValidationContext>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_50_SPARSE_DATA
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_50_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_50_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_50_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUControlRigValidator(); \
	friend struct Z_Construct_UClass_UControlRigValidator_Statics; \
public: \
	DECLARE_CLASS(UControlRigValidator, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRig"), NO_API) \
	DECLARE_SERIALIZER(UControlRigValidator)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_50_INCLASS \
private: \
	static void StaticRegisterNativesUControlRigValidator(); \
	friend struct Z_Construct_UClass_UControlRigValidator_Statics; \
public: \
	DECLARE_CLASS(UControlRigValidator, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRig"), NO_API) \
	DECLARE_SERIALIZER(UControlRigValidator)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_50_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UControlRigValidator(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigValidator) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigValidator); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigValidator); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigValidator(UControlRigValidator&&); \
	NO_API UControlRigValidator(const UControlRigValidator&); \
public:


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_50_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UControlRigValidator(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigValidator(UControlRigValidator&&); \
	NO_API UControlRigValidator(const UControlRigValidator&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigValidator); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigValidator); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigValidator)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_50_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Passes() { return STRUCT_OFFSET(UControlRigValidator, Passes); }


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_47_PROLOG
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_50_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_50_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_50_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_50_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_50_INCLASS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_50_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_50_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_50_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_50_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_50_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_50_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_50_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ControlRigValidator."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONTROLRIG_API UClass* StaticClass<class UControlRigValidator>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_81_SPARSE_DATA
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_81_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_81_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_81_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUControlRigValidationPass(); \
	friend struct Z_Construct_UClass_UControlRigValidationPass_Statics; \
public: \
	DECLARE_CLASS(UControlRigValidationPass, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/ControlRig"), NO_API) \
	DECLARE_SERIALIZER(UControlRigValidationPass)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_81_INCLASS \
private: \
	static void StaticRegisterNativesUControlRigValidationPass(); \
	friend struct Z_Construct_UClass_UControlRigValidationPass_Statics; \
public: \
	DECLARE_CLASS(UControlRigValidationPass, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/ControlRig"), NO_API) \
	DECLARE_SERIALIZER(UControlRigValidationPass)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_81_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UControlRigValidationPass(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigValidationPass) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigValidationPass); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigValidationPass); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigValidationPass(UControlRigValidationPass&&); \
	NO_API UControlRigValidationPass(const UControlRigValidationPass&); \
public:


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_81_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UControlRigValidationPass(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigValidationPass(UControlRigValidationPass&&); \
	NO_API UControlRigValidationPass(const UControlRigValidationPass&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigValidationPass); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigValidationPass); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigValidationPass)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_81_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_78_PROLOG
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_81_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_81_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_81_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_81_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_81_INCLASS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_81_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_81_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_81_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_81_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_81_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_81_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h_81_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ControlRigValidationPass."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONTROLRIG_API UClass* StaticClass<class UControlRigValidationPass>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigValidationPass_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
