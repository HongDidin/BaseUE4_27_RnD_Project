// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Public/Math/ControlRigMathLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeControlRigMathLibrary() {}
// Cross Module References
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_EControlRigAnimEasingType();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_EControlRigRotationOrder();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FCRFourPointBezier();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
// End Cross Module References
	static UEnum* EControlRigAnimEasingType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ControlRig_EControlRigAnimEasingType, Z_Construct_UPackage__Script_ControlRig(), TEXT("EControlRigAnimEasingType"));
		}
		return Singleton;
	}
	template<> CONTROLRIG_API UEnum* StaticEnum<EControlRigAnimEasingType>()
	{
		return EControlRigAnimEasingType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EControlRigAnimEasingType(EControlRigAnimEasingType_StaticEnum, TEXT("/Script/ControlRig"), TEXT("EControlRigAnimEasingType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ControlRig_EControlRigAnimEasingType_Hash() { return 3344084635U; }
	UEnum* Z_Construct_UEnum_ControlRig_EControlRigAnimEasingType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EControlRigAnimEasingType"), 0, Get_Z_Construct_UEnum_ControlRig_EControlRigAnimEasingType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EControlRigAnimEasingType::Linear", (int64)EControlRigAnimEasingType::Linear },
				{ "EControlRigAnimEasingType::QuadraticEaseIn", (int64)EControlRigAnimEasingType::QuadraticEaseIn },
				{ "EControlRigAnimEasingType::QuadraticEaseOut", (int64)EControlRigAnimEasingType::QuadraticEaseOut },
				{ "EControlRigAnimEasingType::QuadraticEaseInOut", (int64)EControlRigAnimEasingType::QuadraticEaseInOut },
				{ "EControlRigAnimEasingType::CubicEaseIn", (int64)EControlRigAnimEasingType::CubicEaseIn },
				{ "EControlRigAnimEasingType::CubicEaseOut", (int64)EControlRigAnimEasingType::CubicEaseOut },
				{ "EControlRigAnimEasingType::CubicEaseInOut", (int64)EControlRigAnimEasingType::CubicEaseInOut },
				{ "EControlRigAnimEasingType::QuarticEaseIn", (int64)EControlRigAnimEasingType::QuarticEaseIn },
				{ "EControlRigAnimEasingType::QuarticEaseOut", (int64)EControlRigAnimEasingType::QuarticEaseOut },
				{ "EControlRigAnimEasingType::QuarticEaseInOut", (int64)EControlRigAnimEasingType::QuarticEaseInOut },
				{ "EControlRigAnimEasingType::QuinticEaseIn", (int64)EControlRigAnimEasingType::QuinticEaseIn },
				{ "EControlRigAnimEasingType::QuinticEaseOut", (int64)EControlRigAnimEasingType::QuinticEaseOut },
				{ "EControlRigAnimEasingType::QuinticEaseInOut", (int64)EControlRigAnimEasingType::QuinticEaseInOut },
				{ "EControlRigAnimEasingType::SineEaseIn", (int64)EControlRigAnimEasingType::SineEaseIn },
				{ "EControlRigAnimEasingType::SineEaseOut", (int64)EControlRigAnimEasingType::SineEaseOut },
				{ "EControlRigAnimEasingType::SineEaseInOut", (int64)EControlRigAnimEasingType::SineEaseInOut },
				{ "EControlRigAnimEasingType::CircularEaseIn", (int64)EControlRigAnimEasingType::CircularEaseIn },
				{ "EControlRigAnimEasingType::CircularEaseOut", (int64)EControlRigAnimEasingType::CircularEaseOut },
				{ "EControlRigAnimEasingType::CircularEaseInOut", (int64)EControlRigAnimEasingType::CircularEaseInOut },
				{ "EControlRigAnimEasingType::ExponentialEaseIn", (int64)EControlRigAnimEasingType::ExponentialEaseIn },
				{ "EControlRigAnimEasingType::ExponentialEaseOut", (int64)EControlRigAnimEasingType::ExponentialEaseOut },
				{ "EControlRigAnimEasingType::ExponentialEaseInOut", (int64)EControlRigAnimEasingType::ExponentialEaseInOut },
				{ "EControlRigAnimEasingType::ElasticEaseIn", (int64)EControlRigAnimEasingType::ElasticEaseIn },
				{ "EControlRigAnimEasingType::ElasticEaseOut", (int64)EControlRigAnimEasingType::ElasticEaseOut },
				{ "EControlRigAnimEasingType::ElasticEaseInOut", (int64)EControlRigAnimEasingType::ElasticEaseInOut },
				{ "EControlRigAnimEasingType::BackEaseIn", (int64)EControlRigAnimEasingType::BackEaseIn },
				{ "EControlRigAnimEasingType::BackEaseOut", (int64)EControlRigAnimEasingType::BackEaseOut },
				{ "EControlRigAnimEasingType::BackEaseInOut", (int64)EControlRigAnimEasingType::BackEaseInOut },
				{ "EControlRigAnimEasingType::BounceEaseIn", (int64)EControlRigAnimEasingType::BounceEaseIn },
				{ "EControlRigAnimEasingType::BounceEaseOut", (int64)EControlRigAnimEasingType::BounceEaseOut },
				{ "EControlRigAnimEasingType::BounceEaseInOut", (int64)EControlRigAnimEasingType::BounceEaseInOut },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BackEaseIn.Name", "EControlRigAnimEasingType::BackEaseIn" },
				{ "BackEaseInOut.Name", "EControlRigAnimEasingType::BackEaseInOut" },
				{ "BackEaseOut.Name", "EControlRigAnimEasingType::BackEaseOut" },
				{ "BounceEaseIn.Name", "EControlRigAnimEasingType::BounceEaseIn" },
				{ "BounceEaseInOut.Name", "EControlRigAnimEasingType::BounceEaseInOut" },
				{ "BounceEaseOut.Name", "EControlRigAnimEasingType::BounceEaseOut" },
				{ "CircularEaseIn.Name", "EControlRigAnimEasingType::CircularEaseIn" },
				{ "CircularEaseInOut.Name", "EControlRigAnimEasingType::CircularEaseInOut" },
				{ "CircularEaseOut.Name", "EControlRigAnimEasingType::CircularEaseOut" },
				{ "CubicEaseIn.Name", "EControlRigAnimEasingType::CubicEaseIn" },
				{ "CubicEaseInOut.Name", "EControlRigAnimEasingType::CubicEaseInOut" },
				{ "CubicEaseOut.Name", "EControlRigAnimEasingType::CubicEaseOut" },
				{ "ElasticEaseIn.Name", "EControlRigAnimEasingType::ElasticEaseIn" },
				{ "ElasticEaseInOut.Name", "EControlRigAnimEasingType::ElasticEaseInOut" },
				{ "ElasticEaseOut.Name", "EControlRigAnimEasingType::ElasticEaseOut" },
				{ "ExponentialEaseIn.Name", "EControlRigAnimEasingType::ExponentialEaseIn" },
				{ "ExponentialEaseInOut.Name", "EControlRigAnimEasingType::ExponentialEaseInOut" },
				{ "ExponentialEaseOut.Name", "EControlRigAnimEasingType::ExponentialEaseOut" },
				{ "Linear.Name", "EControlRigAnimEasingType::Linear" },
				{ "ModuleRelativePath", "Public/Math/ControlRigMathLibrary.h" },
				{ "QuadraticEaseIn.Name", "EControlRigAnimEasingType::QuadraticEaseIn" },
				{ "QuadraticEaseInOut.Name", "EControlRigAnimEasingType::QuadraticEaseInOut" },
				{ "QuadraticEaseOut.Name", "EControlRigAnimEasingType::QuadraticEaseOut" },
				{ "QuarticEaseIn.Name", "EControlRigAnimEasingType::QuarticEaseIn" },
				{ "QuarticEaseInOut.Name", "EControlRigAnimEasingType::QuarticEaseInOut" },
				{ "QuarticEaseOut.Name", "EControlRigAnimEasingType::QuarticEaseOut" },
				{ "QuinticEaseIn.Name", "EControlRigAnimEasingType::QuinticEaseIn" },
				{ "QuinticEaseInOut.Name", "EControlRigAnimEasingType::QuinticEaseInOut" },
				{ "QuinticEaseOut.Name", "EControlRigAnimEasingType::QuinticEaseOut" },
				{ "SineEaseIn.Name", "EControlRigAnimEasingType::SineEaseIn" },
				{ "SineEaseInOut.Name", "EControlRigAnimEasingType::SineEaseInOut" },
				{ "SineEaseOut.Name", "EControlRigAnimEasingType::SineEaseOut" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ControlRig,
				nullptr,
				"EControlRigAnimEasingType",
				"EControlRigAnimEasingType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EControlRigRotationOrder_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ControlRig_EControlRigRotationOrder, Z_Construct_UPackage__Script_ControlRig(), TEXT("EControlRigRotationOrder"));
		}
		return Singleton;
	}
	template<> CONTROLRIG_API UEnum* StaticEnum<EControlRigRotationOrder>()
	{
		return EControlRigRotationOrder_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EControlRigRotationOrder(EControlRigRotationOrder_StaticEnum, TEXT("/Script/ControlRig"), TEXT("EControlRigRotationOrder"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ControlRig_EControlRigRotationOrder_Hash() { return 3185608347U; }
	UEnum* Z_Construct_UEnum_ControlRig_EControlRigRotationOrder()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EControlRigRotationOrder"), 0, Get_Z_Construct_UEnum_ControlRig_EControlRigRotationOrder_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EControlRigRotationOrder::XYZ", (int64)EControlRigRotationOrder::XYZ },
				{ "EControlRigRotationOrder::XZY", (int64)EControlRigRotationOrder::XZY },
				{ "EControlRigRotationOrder::YXZ", (int64)EControlRigRotationOrder::YXZ },
				{ "EControlRigRotationOrder::YZX", (int64)EControlRigRotationOrder::YZX },
				{ "EControlRigRotationOrder::ZXY", (int64)EControlRigRotationOrder::ZXY },
				{ "EControlRigRotationOrder::ZYX", (int64)EControlRigRotationOrder::ZYX },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/Math/ControlRigMathLibrary.h" },
				{ "XYZ.Name", "EControlRigRotationOrder::XYZ" },
				{ "XZY.Name", "EControlRigRotationOrder::XZY" },
				{ "YXZ.Name", "EControlRigRotationOrder::YXZ" },
				{ "YZX.Name", "EControlRigRotationOrder::YZX" },
				{ "ZXY.Name", "EControlRigRotationOrder::ZXY" },
				{ "ZYX.Name", "EControlRigRotationOrder::ZYX" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ControlRig,
				nullptr,
				"EControlRigRotationOrder",
				"EControlRigRotationOrder",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FCRFourPointBezier::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FCRFourPointBezier_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCRFourPointBezier, Z_Construct_UPackage__Script_ControlRig(), TEXT("CRFourPointBezier"), sizeof(FCRFourPointBezier), Get_Z_Construct_UScriptStruct_FCRFourPointBezier_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FCRFourPointBezier>()
{
	return FCRFourPointBezier::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCRFourPointBezier(FCRFourPointBezier::StaticStruct, TEXT("/Script/ControlRig"), TEXT("CRFourPointBezier"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFCRFourPointBezier
{
	FScriptStruct_ControlRig_StaticRegisterNativesFCRFourPointBezier()
	{
		UScriptStruct::DeferCppStructOps<FCRFourPointBezier>(FName(TEXT("CRFourPointBezier")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFCRFourPointBezier;
	struct Z_Construct_UScriptStruct_FCRFourPointBezier_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_A_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_A;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_B_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_B;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_C_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_C;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_D_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_D;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRFourPointBezier_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Math/ControlRigMathLibrary.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FCRFourPointBezier_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FCRFourPointBezier>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRFourPointBezier_Statics::NewProp_A_MetaData[] = {
		{ "ModuleRelativePath", "Public/Math/ControlRigMathLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCRFourPointBezier_Statics::NewProp_A = { "A", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCRFourPointBezier, A), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FCRFourPointBezier_Statics::NewProp_A_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRFourPointBezier_Statics::NewProp_A_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRFourPointBezier_Statics::NewProp_B_MetaData[] = {
		{ "ModuleRelativePath", "Public/Math/ControlRigMathLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCRFourPointBezier_Statics::NewProp_B = { "B", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCRFourPointBezier, B), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FCRFourPointBezier_Statics::NewProp_B_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRFourPointBezier_Statics::NewProp_B_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRFourPointBezier_Statics::NewProp_C_MetaData[] = {
		{ "ModuleRelativePath", "Public/Math/ControlRigMathLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCRFourPointBezier_Statics::NewProp_C = { "C", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCRFourPointBezier, C), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FCRFourPointBezier_Statics::NewProp_C_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRFourPointBezier_Statics::NewProp_C_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRFourPointBezier_Statics::NewProp_D_MetaData[] = {
		{ "ModuleRelativePath", "Public/Math/ControlRigMathLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCRFourPointBezier_Statics::NewProp_D = { "D", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCRFourPointBezier, D), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FCRFourPointBezier_Statics::NewProp_D_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRFourPointBezier_Statics::NewProp_D_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FCRFourPointBezier_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRFourPointBezier_Statics::NewProp_A,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRFourPointBezier_Statics::NewProp_B,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRFourPointBezier_Statics::NewProp_C,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRFourPointBezier_Statics::NewProp_D,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FCRFourPointBezier_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"CRFourPointBezier",
		sizeof(FCRFourPointBezier),
		alignof(FCRFourPointBezier),
		Z_Construct_UScriptStruct_FCRFourPointBezier_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRFourPointBezier_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FCRFourPointBezier_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRFourPointBezier_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FCRFourPointBezier()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FCRFourPointBezier_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CRFourPointBezier"), sizeof(FCRFourPointBezier), Get_Z_Construct_UScriptStruct_FCRFourPointBezier_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FCRFourPointBezier_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCRFourPointBezier_Hash() { return 4096671670U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
