// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Public/Math/Simulation/CRSimPointForce.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCRSimPointForce() {}
// Cross Module References
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_ECRSimPointForceType();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FCRSimPointForce();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
// End Cross Module References
	static UEnum* ECRSimPointForceType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ControlRig_ECRSimPointForceType, Z_Construct_UPackage__Script_ControlRig(), TEXT("ECRSimPointForceType"));
		}
		return Singleton;
	}
	template<> CONTROLRIG_API UEnum* StaticEnum<ECRSimPointForceType>()
	{
		return ECRSimPointForceType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ECRSimPointForceType(ECRSimPointForceType_StaticEnum, TEXT("/Script/ControlRig"), TEXT("ECRSimPointForceType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ControlRig_ECRSimPointForceType_Hash() { return 1161678254U; }
	UEnum* Z_Construct_UEnum_ControlRig_ECRSimPointForceType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ECRSimPointForceType"), 0, Get_Z_Construct_UEnum_ControlRig_ECRSimPointForceType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ECRSimPointForceType::Direction", (int64)ECRSimPointForceType::Direction },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Direction.Name", "ECRSimPointForceType::Direction" },
				{ "ModuleRelativePath", "Public/Math/Simulation/CRSimPointForce.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ControlRig,
				nullptr,
				"ECRSimPointForceType",
				"ECRSimPointForceType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FCRSimPointForce::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FCRSimPointForce_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCRSimPointForce, Z_Construct_UPackage__Script_ControlRig(), TEXT("CRSimPointForce"), sizeof(FCRSimPointForce), Get_Z_Construct_UScriptStruct_FCRSimPointForce_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FCRSimPointForce>()
{
	return FCRSimPointForce::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCRSimPointForce(FCRSimPointForce::StaticStruct, TEXT("/Script/ControlRig"), TEXT("CRSimPointForce"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFCRSimPointForce
{
	FScriptStruct_ControlRig_StaticRegisterNativesFCRSimPointForce()
	{
		UScriptStruct::DeferCppStructOps<FCRSimPointForce>(FName(TEXT("CRSimPointForce")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFCRSimPointForce;
	struct Z_Construct_UScriptStruct_FCRSimPointForce_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ForceType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ForceType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ForceType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Vector_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Vector;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Coefficient_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Coefficient;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bNormalize_MetaData[];
#endif
		static void NewProp_bNormalize_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bNormalize;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimPointForce_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimPointForce.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FCRSimPointForce_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FCRSimPointForce>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FCRSimPointForce_Statics::NewProp_ForceType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimPointForce_Statics::NewProp_ForceType_MetaData[] = {
		{ "Comment", "/**\n\x09 * The type of force.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimPointForce.h" },
		{ "ToolTip", "The type of force." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FCRSimPointForce_Statics::NewProp_ForceType = { "ForceType", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCRSimPointForce, ForceType), Z_Construct_UEnum_ControlRig_ECRSimPointForceType, METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimPointForce_Statics::NewProp_ForceType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimPointForce_Statics::NewProp_ForceType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimPointForce_Statics::NewProp_Vector_MetaData[] = {
		{ "Comment", "/**\n\x09 * The point / direction to use for the force.\n\x09 * This is a direction for direction based forces,\n\x09 * while this is a position for attractor / repel based forces.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimPointForce.h" },
		{ "ToolTip", "The point / direction to use for the force.\nThis is a direction for direction based forces,\nwhile this is a position for attractor / repel based forces." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCRSimPointForce_Statics::NewProp_Vector = { "Vector", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCRSimPointForce, Vector), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimPointForce_Statics::NewProp_Vector_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimPointForce_Statics::NewProp_Vector_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimPointForce_Statics::NewProp_Coefficient_MetaData[] = {
		{ "Comment", "/**\n\x09 * The strength of the force (a multiplier for direction based forces)\n\x09 */" },
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimPointForce.h" },
		{ "ToolTip", "The strength of the force (a multiplier for direction based forces)" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCRSimPointForce_Statics::NewProp_Coefficient = { "Coefficient", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCRSimPointForce, Coefficient), METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimPointForce_Statics::NewProp_Coefficient_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimPointForce_Statics::NewProp_Coefficient_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimPointForce_Statics::NewProp_bNormalize_MetaData[] = {
		{ "Comment", "/**\n\x09 * If set to true the input vector will be normalized.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimPointForce.h" },
		{ "ToolTip", "If set to true the input vector will be normalized." },
	};
#endif
	void Z_Construct_UScriptStruct_FCRSimPointForce_Statics::NewProp_bNormalize_SetBit(void* Obj)
	{
		((FCRSimPointForce*)Obj)->bNormalize = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FCRSimPointForce_Statics::NewProp_bNormalize = { "bNormalize", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FCRSimPointForce), &Z_Construct_UScriptStruct_FCRSimPointForce_Statics::NewProp_bNormalize_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimPointForce_Statics::NewProp_bNormalize_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimPointForce_Statics::NewProp_bNormalize_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FCRSimPointForce_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimPointForce_Statics::NewProp_ForceType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimPointForce_Statics::NewProp_ForceType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimPointForce_Statics::NewProp_Vector,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimPointForce_Statics::NewProp_Coefficient,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimPointForce_Statics::NewProp_bNormalize,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FCRSimPointForce_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"CRSimPointForce",
		sizeof(FCRSimPointForce),
		alignof(FCRSimPointForce),
		Z_Construct_UScriptStruct_FCRSimPointForce_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimPointForce_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimPointForce_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimPointForce_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FCRSimPointForce()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FCRSimPointForce_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CRSimPointForce"), sizeof(FCRSimPointForce), Get_Z_Construct_UScriptStruct_FCRSimPointForce_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FCRSimPointForce_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCRSimPointForce_Hash() { return 3663262521U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
