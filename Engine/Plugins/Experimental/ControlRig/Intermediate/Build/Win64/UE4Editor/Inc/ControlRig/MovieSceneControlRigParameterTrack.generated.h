// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_MovieSceneControlRigParameterTrack_generated_h
#error "MovieSceneControlRigParameterTrack.generated.h already included, missing '#pragma once' in MovieSceneControlRigParameterTrack.h"
#endif
#define CONTROLRIG_MovieSceneControlRigParameterTrack_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterTrack_h_24_SPARSE_DATA
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterTrack_h_24_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterTrack_h_24_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterTrack_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMovieSceneControlRigParameterTrack(); \
	friend struct Z_Construct_UClass_UMovieSceneControlRigParameterTrack_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneControlRigParameterTrack, UMovieSceneNameableTrack, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRig"), CONTROLRIG_API) \
	DECLARE_SERIALIZER(UMovieSceneControlRigParameterTrack) \
	virtual UObject* _getUObject() const override { return const_cast<UMovieSceneControlRigParameterTrack*>(this); }


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterTrack_h_24_INCLASS \
private: \
	static void StaticRegisterNativesUMovieSceneControlRigParameterTrack(); \
	friend struct Z_Construct_UClass_UMovieSceneControlRigParameterTrack_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneControlRigParameterTrack, UMovieSceneNameableTrack, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRig"), CONTROLRIG_API) \
	DECLARE_SERIALIZER(UMovieSceneControlRigParameterTrack) \
	virtual UObject* _getUObject() const override { return const_cast<UMovieSceneControlRigParameterTrack*>(this); }


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterTrack_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	CONTROLRIG_API UMovieSceneControlRigParameterTrack(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneControlRigParameterTrack) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(CONTROLRIG_API, UMovieSceneControlRigParameterTrack); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneControlRigParameterTrack); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	CONTROLRIG_API UMovieSceneControlRigParameterTrack(UMovieSceneControlRigParameterTrack&&); \
	CONTROLRIG_API UMovieSceneControlRigParameterTrack(const UMovieSceneControlRigParameterTrack&); \
public:


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterTrack_h_24_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	CONTROLRIG_API UMovieSceneControlRigParameterTrack(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	CONTROLRIG_API UMovieSceneControlRigParameterTrack(UMovieSceneControlRigParameterTrack&&); \
	CONTROLRIG_API UMovieSceneControlRigParameterTrack(const UMovieSceneControlRigParameterTrack&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(CONTROLRIG_API, UMovieSceneControlRigParameterTrack); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneControlRigParameterTrack); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneControlRigParameterTrack)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterTrack_h_24_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ControlRig() { return STRUCT_OFFSET(UMovieSceneControlRigParameterTrack, ControlRig); } \
	FORCEINLINE static uint32 __PPO__SectionToKey() { return STRUCT_OFFSET(UMovieSceneControlRigParameterTrack, SectionToKey); } \
	FORCEINLINE static uint32 __PPO__Sections() { return STRUCT_OFFSET(UMovieSceneControlRigParameterTrack, Sections); } \
	FORCEINLINE static uint32 __PPO__TrackName() { return STRUCT_OFFSET(UMovieSceneControlRigParameterTrack, TrackName); }


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterTrack_h_18_PROLOG
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterTrack_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterTrack_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterTrack_h_24_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterTrack_h_24_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterTrack_h_24_INCLASS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterTrack_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterTrack_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterTrack_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterTrack_h_24_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterTrack_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterTrack_h_24_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterTrack_h_24_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class MovieSceneControlRigParameterTrack."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONTROLRIG_API UClass* StaticClass<class UMovieSceneControlRigParameterTrack>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterTrack_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
