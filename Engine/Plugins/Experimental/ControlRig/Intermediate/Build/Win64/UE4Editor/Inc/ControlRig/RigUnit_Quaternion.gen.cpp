// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Deprecated/Math/RigUnit_Quaternion.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_Quaternion() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_QuaternionToAngle();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FQuat();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_QuaternionFromAxisAndAngle();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_QuaternionToAxisAndAngle();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_InverseQuaterion();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_UnaryQuaternionOp();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MultiplyQuaternion();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_BinaryQuaternionOp();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_QuaternionToAngle>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_QuaternionToAngle cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_QuaternionToAngle::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_QuaternionToAngle_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_QuaternionToAngle, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_QuaternionToAngle"), sizeof(FRigUnit_QuaternionToAngle), Get_Z_Construct_UScriptStruct_FRigUnit_QuaternionToAngle_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_QuaternionToAngle::Execute"), &FRigUnit_QuaternionToAngle::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_QuaternionToAngle>()
{
	return FRigUnit_QuaternionToAngle::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_QuaternionToAngle(FRigUnit_QuaternionToAngle::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_QuaternionToAngle"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_QuaternionToAngle
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_QuaternionToAngle()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_QuaternionToAngle>(FName(TEXT("RigUnit_QuaternionToAngle")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_QuaternionToAngle;
	struct Z_Construct_UScriptStruct_FRigUnit_QuaternionToAngle_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Axis_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Axis;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Argument_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Argument;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Angle_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Angle;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_QuaternionToAngle_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Math|Quaternion" },
		{ "Deprecated", "4.23.0" },
		{ "DisplayName", "Get Angle Around Axis" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Quaternion.h" },
		{ "NodeColor", "0.1 0.7 0.1" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_QuaternionToAngle_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_QuaternionToAngle>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_QuaternionToAngle_Statics::NewProp_Axis_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Quaternion.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_QuaternionToAngle_Statics::NewProp_Axis = { "Axis", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_QuaternionToAngle, Axis), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_QuaternionToAngle_Statics::NewProp_Axis_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_QuaternionToAngle_Statics::NewProp_Axis_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_QuaternionToAngle_Statics::NewProp_Argument_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Quaternion.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_QuaternionToAngle_Statics::NewProp_Argument = { "Argument", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_QuaternionToAngle, Argument), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_QuaternionToAngle_Statics::NewProp_Argument_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_QuaternionToAngle_Statics::NewProp_Argument_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_QuaternionToAngle_Statics::NewProp_Angle_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Quaternion.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_QuaternionToAngle_Statics::NewProp_Angle = { "Angle", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_QuaternionToAngle, Angle), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_QuaternionToAngle_Statics::NewProp_Angle_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_QuaternionToAngle_Statics::NewProp_Angle_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_QuaternionToAngle_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_QuaternionToAngle_Statics::NewProp_Axis,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_QuaternionToAngle_Statics::NewProp_Argument,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_QuaternionToAngle_Statics::NewProp_Angle,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_QuaternionToAngle_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_QuaternionToAngle",
		sizeof(FRigUnit_QuaternionToAngle),
		alignof(FRigUnit_QuaternionToAngle),
		Z_Construct_UScriptStruct_FRigUnit_QuaternionToAngle_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_QuaternionToAngle_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_QuaternionToAngle_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_QuaternionToAngle_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_QuaternionToAngle()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_QuaternionToAngle_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_QuaternionToAngle"), sizeof(FRigUnit_QuaternionToAngle), Get_Z_Construct_UScriptStruct_FRigUnit_QuaternionToAngle_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_QuaternionToAngle_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_QuaternionToAngle_Hash() { return 1329111267U; }

void FRigUnit_QuaternionToAngle::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Axis,
		Argument,
		Angle,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_QuaternionFromAxisAndAngle>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_QuaternionFromAxisAndAngle cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_QuaternionFromAxisAndAngle::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_QuaternionFromAxisAndAngle_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_QuaternionFromAxisAndAngle, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_QuaternionFromAxisAndAngle"), sizeof(FRigUnit_QuaternionFromAxisAndAngle), Get_Z_Construct_UScriptStruct_FRigUnit_QuaternionFromAxisAndAngle_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_QuaternionFromAxisAndAngle::Execute"), &FRigUnit_QuaternionFromAxisAndAngle::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_QuaternionFromAxisAndAngle>()
{
	return FRigUnit_QuaternionFromAxisAndAngle::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_QuaternionFromAxisAndAngle(FRigUnit_QuaternionFromAxisAndAngle::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_QuaternionFromAxisAndAngle"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_QuaternionFromAxisAndAngle
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_QuaternionFromAxisAndAngle()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_QuaternionFromAxisAndAngle>(FName(TEXT("RigUnit_QuaternionFromAxisAndAngle")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_QuaternionFromAxisAndAngle;
	struct Z_Construct_UScriptStruct_FRigUnit_QuaternionFromAxisAndAngle_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Axis_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Axis;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Angle_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Angle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_QuaternionFromAxisAndAngle_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Math|Quaternion" },
		{ "Deprecated", "4.23.0" },
		{ "DisplayName", "From Axis And Angle(Quaternion)" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Quaternion.h" },
		{ "NodeColor", "0.1 0.7 0.1" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_QuaternionFromAxisAndAngle_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_QuaternionFromAxisAndAngle>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_QuaternionFromAxisAndAngle_Statics::NewProp_Axis_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Quaternion.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_QuaternionFromAxisAndAngle_Statics::NewProp_Axis = { "Axis", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_QuaternionFromAxisAndAngle, Axis), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_QuaternionFromAxisAndAngle_Statics::NewProp_Axis_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_QuaternionFromAxisAndAngle_Statics::NewProp_Axis_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_QuaternionFromAxisAndAngle_Statics::NewProp_Angle_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Quaternion.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_QuaternionFromAxisAndAngle_Statics::NewProp_Angle = { "Angle", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_QuaternionFromAxisAndAngle, Angle), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_QuaternionFromAxisAndAngle_Statics::NewProp_Angle_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_QuaternionFromAxisAndAngle_Statics::NewProp_Angle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_QuaternionFromAxisAndAngle_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Quaternion.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_QuaternionFromAxisAndAngle_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_QuaternionFromAxisAndAngle, Result), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_QuaternionFromAxisAndAngle_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_QuaternionFromAxisAndAngle_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_QuaternionFromAxisAndAngle_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_QuaternionFromAxisAndAngle_Statics::NewProp_Axis,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_QuaternionFromAxisAndAngle_Statics::NewProp_Angle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_QuaternionFromAxisAndAngle_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_QuaternionFromAxisAndAngle_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_QuaternionFromAxisAndAngle",
		sizeof(FRigUnit_QuaternionFromAxisAndAngle),
		alignof(FRigUnit_QuaternionFromAxisAndAngle),
		Z_Construct_UScriptStruct_FRigUnit_QuaternionFromAxisAndAngle_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_QuaternionFromAxisAndAngle_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_QuaternionFromAxisAndAngle_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_QuaternionFromAxisAndAngle_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_QuaternionFromAxisAndAngle()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_QuaternionFromAxisAndAngle_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_QuaternionFromAxisAndAngle"), sizeof(FRigUnit_QuaternionFromAxisAndAngle), Get_Z_Construct_UScriptStruct_FRigUnit_QuaternionFromAxisAndAngle_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_QuaternionFromAxisAndAngle_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_QuaternionFromAxisAndAngle_Hash() { return 170365901U; }

void FRigUnit_QuaternionFromAxisAndAngle::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Axis,
		Angle,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_QuaternionToAxisAndAngle>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_QuaternionToAxisAndAngle cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_QuaternionToAxisAndAngle::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_QuaternionToAxisAndAngle_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_QuaternionToAxisAndAngle, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_QuaternionToAxisAndAngle"), sizeof(FRigUnit_QuaternionToAxisAndAngle), Get_Z_Construct_UScriptStruct_FRigUnit_QuaternionToAxisAndAngle_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_QuaternionToAxisAndAngle::Execute"), &FRigUnit_QuaternionToAxisAndAngle::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_QuaternionToAxisAndAngle>()
{
	return FRigUnit_QuaternionToAxisAndAngle::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_QuaternionToAxisAndAngle(FRigUnit_QuaternionToAxisAndAngle::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_QuaternionToAxisAndAngle"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_QuaternionToAxisAndAngle
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_QuaternionToAxisAndAngle()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_QuaternionToAxisAndAngle>(FName(TEXT("RigUnit_QuaternionToAxisAndAngle")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_QuaternionToAxisAndAngle;
	struct Z_Construct_UScriptStruct_FRigUnit_QuaternionToAxisAndAngle_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Argument_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Argument;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Axis_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Axis;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Angle_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Angle;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_QuaternionToAxisAndAngle_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Math|Quaternion" },
		{ "Deprecated", "4.23.0" },
		{ "DisplayName", "To Axis And Angle(Quaternion)" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Quaternion.h" },
		{ "NodeColor", "0.1 0.7 0.1" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_QuaternionToAxisAndAngle_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_QuaternionToAxisAndAngle>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_QuaternionToAxisAndAngle_Statics::NewProp_Argument_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Quaternion.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_QuaternionToAxisAndAngle_Statics::NewProp_Argument = { "Argument", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_QuaternionToAxisAndAngle, Argument), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_QuaternionToAxisAndAngle_Statics::NewProp_Argument_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_QuaternionToAxisAndAngle_Statics::NewProp_Argument_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_QuaternionToAxisAndAngle_Statics::NewProp_Axis_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Quaternion.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_QuaternionToAxisAndAngle_Statics::NewProp_Axis = { "Axis", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_QuaternionToAxisAndAngle, Axis), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_QuaternionToAxisAndAngle_Statics::NewProp_Axis_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_QuaternionToAxisAndAngle_Statics::NewProp_Axis_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_QuaternionToAxisAndAngle_Statics::NewProp_Angle_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Quaternion.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_QuaternionToAxisAndAngle_Statics::NewProp_Angle = { "Angle", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_QuaternionToAxisAndAngle, Angle), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_QuaternionToAxisAndAngle_Statics::NewProp_Angle_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_QuaternionToAxisAndAngle_Statics::NewProp_Angle_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_QuaternionToAxisAndAngle_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_QuaternionToAxisAndAngle_Statics::NewProp_Argument,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_QuaternionToAxisAndAngle_Statics::NewProp_Axis,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_QuaternionToAxisAndAngle_Statics::NewProp_Angle,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_QuaternionToAxisAndAngle_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_QuaternionToAxisAndAngle",
		sizeof(FRigUnit_QuaternionToAxisAndAngle),
		alignof(FRigUnit_QuaternionToAxisAndAngle),
		Z_Construct_UScriptStruct_FRigUnit_QuaternionToAxisAndAngle_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_QuaternionToAxisAndAngle_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_QuaternionToAxisAndAngle_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_QuaternionToAxisAndAngle_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_QuaternionToAxisAndAngle()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_QuaternionToAxisAndAngle_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_QuaternionToAxisAndAngle"), sizeof(FRigUnit_QuaternionToAxisAndAngle), Get_Z_Construct_UScriptStruct_FRigUnit_QuaternionToAxisAndAngle_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_QuaternionToAxisAndAngle_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_QuaternionToAxisAndAngle_Hash() { return 1311485541U; }

void FRigUnit_QuaternionToAxisAndAngle::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Argument,
		Axis,
		Angle,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_InverseQuaterion>() == std::is_polymorphic<FRigUnit_UnaryQuaternionOp>(), "USTRUCT FRigUnit_InverseQuaterion cannot be polymorphic unless super FRigUnit_UnaryQuaternionOp is polymorphic");

class UScriptStruct* FRigUnit_InverseQuaterion::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_InverseQuaterion_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_InverseQuaterion, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_InverseQuaterion"), sizeof(FRigUnit_InverseQuaterion), Get_Z_Construct_UScriptStruct_FRigUnit_InverseQuaterion_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_InverseQuaterion::Execute"), &FRigUnit_InverseQuaterion::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_InverseQuaterion>()
{
	return FRigUnit_InverseQuaterion::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_InverseQuaterion(FRigUnit_InverseQuaterion::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_InverseQuaterion"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_InverseQuaterion
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_InverseQuaterion()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_InverseQuaterion>(FName(TEXT("RigUnit_InverseQuaterion")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_InverseQuaterion;
	struct Z_Construct_UScriptStruct_FRigUnit_InverseQuaterion_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_InverseQuaterion_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Math|Quaternion" },
		{ "Deprecated", "4.23.0" },
		{ "DisplayName", "Inverse(Quaternion)" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Quaternion.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_InverseQuaterion_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_InverseQuaterion>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_InverseQuaterion_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_UnaryQuaternionOp,
		&NewStructOps,
		"RigUnit_InverseQuaterion",
		sizeof(FRigUnit_InverseQuaterion),
		alignof(FRigUnit_InverseQuaterion),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_InverseQuaterion_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_InverseQuaterion_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_InverseQuaterion()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_InverseQuaterion_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_InverseQuaterion"), sizeof(FRigUnit_InverseQuaterion), Get_Z_Construct_UScriptStruct_FRigUnit_InverseQuaterion_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_InverseQuaterion_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_InverseQuaterion_Hash() { return 2734577962U; }

void FRigUnit_InverseQuaterion::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Argument,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_UnaryQuaternionOp>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_UnaryQuaternionOp cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_UnaryQuaternionOp::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_UnaryQuaternionOp_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_UnaryQuaternionOp, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_UnaryQuaternionOp"), sizeof(FRigUnit_UnaryQuaternionOp), Get_Z_Construct_UScriptStruct_FRigUnit_UnaryQuaternionOp_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_UnaryQuaternionOp>()
{
	return FRigUnit_UnaryQuaternionOp::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_UnaryQuaternionOp(FRigUnit_UnaryQuaternionOp::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_UnaryQuaternionOp"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_UnaryQuaternionOp
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_UnaryQuaternionOp()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_UnaryQuaternionOp>(FName(TEXT("RigUnit_UnaryQuaternionOp")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_UnaryQuaternionOp;
	struct Z_Construct_UScriptStruct_FRigUnit_UnaryQuaternionOp_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Argument_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Argument;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_UnaryQuaternionOp_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "Comment", "/** Two args and a result of Quaternion type */" },
		{ "Deprecated", "4.23.0" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Quaternion.h" },
		{ "NodeColor", "0.1 0.7 0.1" },
		{ "ToolTip", "Two args and a result of Quaternion type" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_UnaryQuaternionOp_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_UnaryQuaternionOp>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_UnaryQuaternionOp_Statics::NewProp_Argument_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Quaternion.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_UnaryQuaternionOp_Statics::NewProp_Argument = { "Argument", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_UnaryQuaternionOp, Argument), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_UnaryQuaternionOp_Statics::NewProp_Argument_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_UnaryQuaternionOp_Statics::NewProp_Argument_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_UnaryQuaternionOp_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Quaternion.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_UnaryQuaternionOp_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_UnaryQuaternionOp, Result), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_UnaryQuaternionOp_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_UnaryQuaternionOp_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_UnaryQuaternionOp_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_UnaryQuaternionOp_Statics::NewProp_Argument,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_UnaryQuaternionOp_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_UnaryQuaternionOp_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_UnaryQuaternionOp",
		sizeof(FRigUnit_UnaryQuaternionOp),
		alignof(FRigUnit_UnaryQuaternionOp),
		Z_Construct_UScriptStruct_FRigUnit_UnaryQuaternionOp_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_UnaryQuaternionOp_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_UnaryQuaternionOp_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_UnaryQuaternionOp_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_UnaryQuaternionOp()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_UnaryQuaternionOp_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_UnaryQuaternionOp"), sizeof(FRigUnit_UnaryQuaternionOp), Get_Z_Construct_UScriptStruct_FRigUnit_UnaryQuaternionOp_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_UnaryQuaternionOp_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_UnaryQuaternionOp_Hash() { return 3367906171U; }

static_assert(std::is_polymorphic<FRigUnit_MultiplyQuaternion>() == std::is_polymorphic<FRigUnit_BinaryQuaternionOp>(), "USTRUCT FRigUnit_MultiplyQuaternion cannot be polymorphic unless super FRigUnit_BinaryQuaternionOp is polymorphic");

class UScriptStruct* FRigUnit_MultiplyQuaternion::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MultiplyQuaternion_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MultiplyQuaternion, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MultiplyQuaternion"), sizeof(FRigUnit_MultiplyQuaternion), Get_Z_Construct_UScriptStruct_FRigUnit_MultiplyQuaternion_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MultiplyQuaternion::Execute"), &FRigUnit_MultiplyQuaternion::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MultiplyQuaternion>()
{
	return FRigUnit_MultiplyQuaternion::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MultiplyQuaternion(FRigUnit_MultiplyQuaternion::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MultiplyQuaternion"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MultiplyQuaternion
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MultiplyQuaternion()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MultiplyQuaternion>(FName(TEXT("RigUnit_MultiplyQuaternion")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MultiplyQuaternion;
	struct Z_Construct_UScriptStruct_FRigUnit_MultiplyQuaternion_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MultiplyQuaternion_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Math|Quaternion" },
		{ "Deprecated", "4.23.0" },
		{ "DisplayName", "Multiply(Quaternion)" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Quaternion.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MultiplyQuaternion_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MultiplyQuaternion>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MultiplyQuaternion_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_BinaryQuaternionOp,
		&NewStructOps,
		"RigUnit_MultiplyQuaternion",
		sizeof(FRigUnit_MultiplyQuaternion),
		alignof(FRigUnit_MultiplyQuaternion),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MultiplyQuaternion_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MultiplyQuaternion_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MultiplyQuaternion()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MultiplyQuaternion_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MultiplyQuaternion"), sizeof(FRigUnit_MultiplyQuaternion), Get_Z_Construct_UScriptStruct_FRigUnit_MultiplyQuaternion_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MultiplyQuaternion_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MultiplyQuaternion_Hash() { return 597831101U; }

void FRigUnit_MultiplyQuaternion::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Argument0,
		Argument1,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_BinaryQuaternionOp>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_BinaryQuaternionOp cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_BinaryQuaternionOp::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_BinaryQuaternionOp_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_BinaryQuaternionOp, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_BinaryQuaternionOp"), sizeof(FRigUnit_BinaryQuaternionOp), Get_Z_Construct_UScriptStruct_FRigUnit_BinaryQuaternionOp_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_BinaryQuaternionOp>()
{
	return FRigUnit_BinaryQuaternionOp::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_BinaryQuaternionOp(FRigUnit_BinaryQuaternionOp::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_BinaryQuaternionOp"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_BinaryQuaternionOp
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_BinaryQuaternionOp()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_BinaryQuaternionOp>(FName(TEXT("RigUnit_BinaryQuaternionOp")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_BinaryQuaternionOp;
	struct Z_Construct_UScriptStruct_FRigUnit_BinaryQuaternionOp_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Argument0_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Argument0;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Argument1_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Argument1;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_BinaryQuaternionOp_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "Comment", "/** Two args and a result of Quaternion type */" },
		{ "Deprecated", "4.23.0" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Quaternion.h" },
		{ "NodeColor", "0.1 0.7 0.1" },
		{ "ToolTip", "Two args and a result of Quaternion type" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_BinaryQuaternionOp_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_BinaryQuaternionOp>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_BinaryQuaternionOp_Statics::NewProp_Argument0_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Quaternion.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_BinaryQuaternionOp_Statics::NewProp_Argument0 = { "Argument0", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_BinaryQuaternionOp, Argument0), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_BinaryQuaternionOp_Statics::NewProp_Argument0_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_BinaryQuaternionOp_Statics::NewProp_Argument0_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_BinaryQuaternionOp_Statics::NewProp_Argument1_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Quaternion.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_BinaryQuaternionOp_Statics::NewProp_Argument1 = { "Argument1", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_BinaryQuaternionOp, Argument1), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_BinaryQuaternionOp_Statics::NewProp_Argument1_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_BinaryQuaternionOp_Statics::NewProp_Argument1_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_BinaryQuaternionOp_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Quaternion.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_BinaryQuaternionOp_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_BinaryQuaternionOp, Result), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_BinaryQuaternionOp_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_BinaryQuaternionOp_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_BinaryQuaternionOp_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_BinaryQuaternionOp_Statics::NewProp_Argument0,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_BinaryQuaternionOp_Statics::NewProp_Argument1,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_BinaryQuaternionOp_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_BinaryQuaternionOp_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_BinaryQuaternionOp",
		sizeof(FRigUnit_BinaryQuaternionOp),
		alignof(FRigUnit_BinaryQuaternionOp),
		Z_Construct_UScriptStruct_FRigUnit_BinaryQuaternionOp_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_BinaryQuaternionOp_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_BinaryQuaternionOp_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_BinaryQuaternionOp_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_BinaryQuaternionOp()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_BinaryQuaternionOp_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_BinaryQuaternionOp"), sizeof(FRigUnit_BinaryQuaternionOp), Get_Z_Construct_UScriptStruct_FRigUnit_BinaryQuaternionOp_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_BinaryQuaternionOp_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_BinaryQuaternionOp_Hash() { return 63599228U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
