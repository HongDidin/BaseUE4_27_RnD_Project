// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_ChainHarmonics() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_HighlevelBaseMutable();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigElementKey();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FRuntimeFloatCurve();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FCachedRigElement();
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_EControlRigAnimEasingType();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_ChainHarmonicsPerItem>() == std::is_polymorphic<FRigUnit_HighlevelBaseMutable>(), "USTRUCT FRigUnit_ChainHarmonicsPerItem cannot be polymorphic unless super FRigUnit_HighlevelBaseMutable is polymorphic");

class UScriptStruct* FRigUnit_ChainHarmonicsPerItem::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_ChainHarmonicsPerItem"), sizeof(FRigUnit_ChainHarmonicsPerItem), Get_Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_ChainHarmonicsPerItem::Execute"), &FRigUnit_ChainHarmonicsPerItem::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_ChainHarmonicsPerItem>()
{
	return FRigUnit_ChainHarmonicsPerItem::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem(FRigUnit_ChainHarmonicsPerItem::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_ChainHarmonicsPerItem"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ChainHarmonicsPerItem
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ChainHarmonicsPerItem()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_ChainHarmonicsPerItem>(FName(TEXT("RigUnit_ChainHarmonicsPerItem")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ChainHarmonicsPerItem;
	struct Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChainRoot_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ChainRoot;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Speed_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Speed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Reach_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Reach;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Wave_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Wave;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaveCurve_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WaveCurve;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Pendulum_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Pendulum;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDrawDebug_MetaData[];
#endif
		static void NewProp_bDrawDebug_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDrawDebug;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DrawWorldOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DrawWorldOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorkData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WorkData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Performs chain based simulation\n */" },
		{ "DisplayName", "Chain Harmonics" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
		{ "ToolTip", "Performs chain based simulation" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_ChainHarmonicsPerItem>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_ChainRoot_MetaData[] = {
		{ "ExpandByDefault", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_ChainRoot = { "ChainRoot", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonicsPerItem, ChainRoot), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_ChainRoot_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_ChainRoot_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_Speed_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_Speed = { "Speed", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonicsPerItem, Speed), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_Speed_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_Speed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_Reach_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_Reach = { "Reach", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonicsPerItem, Reach), Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_Reach_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_Reach_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_Wave_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_Wave = { "Wave", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonicsPerItem, Wave), Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_Wave_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_Wave_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_WaveCurve_MetaData[] = {
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_WaveCurve = { "WaveCurve", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonicsPerItem, WaveCurve), Z_Construct_UScriptStruct_FRuntimeFloatCurve, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_WaveCurve_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_WaveCurve_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_Pendulum_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_Pendulum = { "Pendulum", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonicsPerItem, Pendulum), Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_Pendulum_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_Pendulum_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_bDrawDebug_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_bDrawDebug_SetBit(void* Obj)
	{
		((FRigUnit_ChainHarmonicsPerItem*)Obj)->bDrawDebug = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_bDrawDebug = { "bDrawDebug", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_ChainHarmonicsPerItem), &Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_bDrawDebug_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_bDrawDebug_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_bDrawDebug_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_DrawWorldOffset_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_DrawWorldOffset = { "DrawWorldOffset", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonicsPerItem, DrawWorldOffset), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_DrawWorldOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_DrawWorldOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_WorkData_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_WorkData = { "WorkData", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonicsPerItem, WorkData), Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_WorkData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_WorkData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_ChainRoot,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_Speed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_Reach,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_Wave,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_WaveCurve,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_Pendulum,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_bDrawDebug,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_DrawWorldOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::NewProp_WorkData,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_HighlevelBaseMutable,
		&NewStructOps,
		"RigUnit_ChainHarmonicsPerItem",
		sizeof(FRigUnit_ChainHarmonicsPerItem),
		alignof(FRigUnit_ChainHarmonicsPerItem),
		Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_ChainHarmonicsPerItem"), sizeof(FRigUnit_ChainHarmonicsPerItem), Get_Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ChainHarmonicsPerItem_Hash() { return 1640973758U; }

void FRigUnit_ChainHarmonicsPerItem::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		ChainRoot,
		Speed,
		Reach,
		Wave,
		WaveCurve,
		Pendulum,
		bDrawDebug,
		DrawWorldOffset,
		WorkData,
		ExecuteContext,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_ChainHarmonics>() == std::is_polymorphic<FRigUnit_HighlevelBaseMutable>(), "USTRUCT FRigUnit_ChainHarmonics cannot be polymorphic unless super FRigUnit_HighlevelBaseMutable is polymorphic");

class UScriptStruct* FRigUnit_ChainHarmonics::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_ChainHarmonics"), sizeof(FRigUnit_ChainHarmonics), Get_Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_ChainHarmonics::Execute"), &FRigUnit_ChainHarmonics::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_ChainHarmonics>()
{
	return FRigUnit_ChainHarmonics::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_ChainHarmonics(FRigUnit_ChainHarmonics::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_ChainHarmonics"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ChainHarmonics
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ChainHarmonics()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_ChainHarmonics>(FName(TEXT("RigUnit_ChainHarmonics")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ChainHarmonics;
	struct Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChainRoot_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ChainRoot;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Speed_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Speed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Reach_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Reach;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Wave_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Wave;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaveCurve_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WaveCurve;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Pendulum_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Pendulum;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDrawDebug_MetaData[];
#endif
		static void NewProp_bDrawDebug_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDrawDebug;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DrawWorldOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DrawWorldOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorkData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WorkData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::Struct_MetaDataParams[] = {
		{ "Deprecated", "4.25" },
		{ "DisplayName", "ChainHarmonics" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_ChainHarmonics>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_ChainRoot_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_ChainRoot = { "ChainRoot", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonics, ChainRoot), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_ChainRoot_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_ChainRoot_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_Speed_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_Speed = { "Speed", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonics, Speed), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_Speed_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_Speed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_Reach_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_Reach = { "Reach", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonics, Reach), Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_Reach_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_Reach_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_Wave_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_Wave = { "Wave", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonics, Wave), Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_Wave_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_Wave_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_WaveCurve_MetaData[] = {
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_WaveCurve = { "WaveCurve", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonics, WaveCurve), Z_Construct_UScriptStruct_FRuntimeFloatCurve, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_WaveCurve_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_WaveCurve_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_Pendulum_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_Pendulum = { "Pendulum", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonics, Pendulum), Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_Pendulum_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_Pendulum_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_bDrawDebug_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_bDrawDebug_SetBit(void* Obj)
	{
		((FRigUnit_ChainHarmonics*)Obj)->bDrawDebug = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_bDrawDebug = { "bDrawDebug", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_ChainHarmonics), &Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_bDrawDebug_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_bDrawDebug_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_bDrawDebug_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_DrawWorldOffset_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_DrawWorldOffset = { "DrawWorldOffset", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonics, DrawWorldOffset), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_DrawWorldOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_DrawWorldOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_WorkData_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_WorkData = { "WorkData", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonics, WorkData), Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_WorkData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_WorkData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_ChainRoot,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_Speed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_Reach,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_Wave,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_WaveCurve,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_Pendulum,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_bDrawDebug,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_DrawWorldOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::NewProp_WorkData,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_HighlevelBaseMutable,
		&NewStructOps,
		"RigUnit_ChainHarmonics",
		sizeof(FRigUnit_ChainHarmonics),
		alignof(FRigUnit_ChainHarmonics),
		Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_ChainHarmonics"), sizeof(FRigUnit_ChainHarmonics), Get_Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Hash() { return 3319210107U; }

void FRigUnit_ChainHarmonics::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		ChainRoot,
		Speed,
		Reach,
		Wave,
		WaveCurve,
		Pendulum,
		bDrawDebug,
		DrawWorldOffset,
		WorkData,
		ExecuteContext,
		Context
	);
}

class UScriptStruct* FRigUnit_ChainHarmonics_WorkData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_ChainHarmonics_WorkData"), sizeof(FRigUnit_ChainHarmonics_WorkData), Get_Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_ChainHarmonics_WorkData>()
{
	return FRigUnit_ChainHarmonics_WorkData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData(FRigUnit_ChainHarmonics_WorkData::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_ChainHarmonics_WorkData"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ChainHarmonics_WorkData
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ChainHarmonics_WorkData()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_ChainHarmonics_WorkData>(FName(TEXT("RigUnit_ChainHarmonics_WorkData")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ChainHarmonics_WorkData;
	struct Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Time_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Time;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Items_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Items_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Items;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Ratio_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Ratio_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Ratio;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LocalTip_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LocalTip_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_LocalTip;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PendulumTip_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PendulumTip_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PendulumTip;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PendulumPosition_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PendulumPosition_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PendulumPosition;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PendulumVelocity_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PendulumVelocity_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PendulumVelocity;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HierarchyLine_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HierarchyLine_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_HierarchyLine;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_VelocityLines_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VelocityLines_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_VelocityLines;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_ChainHarmonics_WorkData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_Time_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_Time = { "Time", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonics_WorkData, Time), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_Time_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_Time_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_Items_Inner = { "Items", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FCachedRigElement, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_Items_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_Items = { "Items", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonics_WorkData, Items), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_Items_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_Items_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_Ratio_Inner = { "Ratio", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_Ratio_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_Ratio = { "Ratio", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonics_WorkData, Ratio), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_Ratio_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_Ratio_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_LocalTip_Inner = { "LocalTip", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_LocalTip_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_LocalTip = { "LocalTip", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonics_WorkData, LocalTip), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_LocalTip_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_LocalTip_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_PendulumTip_Inner = { "PendulumTip", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_PendulumTip_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_PendulumTip = { "PendulumTip", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonics_WorkData, PendulumTip), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_PendulumTip_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_PendulumTip_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_PendulumPosition_Inner = { "PendulumPosition", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_PendulumPosition_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_PendulumPosition = { "PendulumPosition", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonics_WorkData, PendulumPosition), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_PendulumPosition_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_PendulumPosition_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_PendulumVelocity_Inner = { "PendulumVelocity", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_PendulumVelocity_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_PendulumVelocity = { "PendulumVelocity", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonics_WorkData, PendulumVelocity), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_PendulumVelocity_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_PendulumVelocity_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_HierarchyLine_Inner = { "HierarchyLine", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_HierarchyLine_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_HierarchyLine = { "HierarchyLine", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonics_WorkData, HierarchyLine), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_HierarchyLine_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_HierarchyLine_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_VelocityLines_Inner = { "VelocityLines", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_VelocityLines_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_VelocityLines = { "VelocityLines", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonics_WorkData, VelocityLines), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_VelocityLines_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_VelocityLines_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_Time,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_Items_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_Items,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_Ratio_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_Ratio,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_LocalTip_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_LocalTip,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_PendulumTip_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_PendulumTip,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_PendulumPosition_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_PendulumPosition,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_PendulumVelocity_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_PendulumVelocity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_HierarchyLine_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_HierarchyLine,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_VelocityLines_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::NewProp_VelocityLines,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"RigUnit_ChainHarmonics_WorkData",
		sizeof(FRigUnit_ChainHarmonics_WorkData),
		alignof(FRigUnit_ChainHarmonics_WorkData),
		Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_ChainHarmonics_WorkData"), sizeof(FRigUnit_ChainHarmonics_WorkData), Get_Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_WorkData_Hash() { return 2385834239U; }
class UScriptStruct* FRigUnit_ChainHarmonics_Pendulum::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_ChainHarmonics_Pendulum"), sizeof(FRigUnit_ChainHarmonics_Pendulum), Get_Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_ChainHarmonics_Pendulum>()
{
	return FRigUnit_ChainHarmonics_Pendulum::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum(FRigUnit_ChainHarmonics_Pendulum::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_ChainHarmonics_Pendulum"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ChainHarmonics_Pendulum
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ChainHarmonics_Pendulum()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_ChainHarmonics_Pendulum>(FName(TEXT("RigUnit_ChainHarmonics_Pendulum")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ChainHarmonics_Pendulum;
	struct Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnabled_MetaData[];
#endif
		static void NewProp_bEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PendulumStiffness_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PendulumStiffness;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PendulumGravity_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PendulumGravity;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PendulumBlend_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PendulumBlend;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PendulumDrag_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PendulumDrag;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PendulumMinimum_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PendulumMinimum;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PendulumMaximum_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PendulumMaximum;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_PendulumEase_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PendulumEase_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_PendulumEase;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UnwindAxis_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_UnwindAxis;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UnwindMinimum_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_UnwindMinimum;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UnwindMaximum_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_UnwindMaximum;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_ChainHarmonics_Pendulum>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_bEnabled_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_bEnabled_SetBit(void* Obj)
	{
		((FRigUnit_ChainHarmonics_Pendulum*)Obj)->bEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_bEnabled = { "bEnabled", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_ChainHarmonics_Pendulum), &Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_bEnabled_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_bEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_bEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_PendulumStiffness_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_PendulumStiffness = { "PendulumStiffness", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonics_Pendulum, PendulumStiffness), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_PendulumStiffness_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_PendulumStiffness_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_PendulumGravity_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_PendulumGravity = { "PendulumGravity", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonics_Pendulum, PendulumGravity), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_PendulumGravity_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_PendulumGravity_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_PendulumBlend_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_PendulumBlend = { "PendulumBlend", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonics_Pendulum, PendulumBlend), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_PendulumBlend_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_PendulumBlend_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_PendulumDrag_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_PendulumDrag = { "PendulumDrag", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonics_Pendulum, PendulumDrag), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_PendulumDrag_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_PendulumDrag_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_PendulumMinimum_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_PendulumMinimum = { "PendulumMinimum", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonics_Pendulum, PendulumMinimum), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_PendulumMinimum_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_PendulumMinimum_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_PendulumMaximum_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_PendulumMaximum = { "PendulumMaximum", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonics_Pendulum, PendulumMaximum), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_PendulumMaximum_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_PendulumMaximum_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_PendulumEase_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_PendulumEase_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_PendulumEase = { "PendulumEase", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonics_Pendulum, PendulumEase), Z_Construct_UEnum_ControlRig_EControlRigAnimEasingType, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_PendulumEase_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_PendulumEase_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_UnwindAxis_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_UnwindAxis = { "UnwindAxis", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonics_Pendulum, UnwindAxis), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_UnwindAxis_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_UnwindAxis_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_UnwindMinimum_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_UnwindMinimum = { "UnwindMinimum", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonics_Pendulum, UnwindMinimum), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_UnwindMinimum_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_UnwindMinimum_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_UnwindMaximum_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_UnwindMaximum = { "UnwindMaximum", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonics_Pendulum, UnwindMaximum), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_UnwindMaximum_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_UnwindMaximum_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_bEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_PendulumStiffness,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_PendulumGravity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_PendulumBlend,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_PendulumDrag,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_PendulumMinimum,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_PendulumMaximum,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_PendulumEase_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_PendulumEase,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_UnwindAxis,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_UnwindMinimum,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::NewProp_UnwindMaximum,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"RigUnit_ChainHarmonics_Pendulum",
		sizeof(FRigUnit_ChainHarmonics_Pendulum),
		alignof(FRigUnit_ChainHarmonics_Pendulum),
		Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_ChainHarmonics_Pendulum"), sizeof(FRigUnit_ChainHarmonics_Pendulum), Get_Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Pendulum_Hash() { return 4002481542U; }
class UScriptStruct* FRigUnit_ChainHarmonics_Wave::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_ChainHarmonics_Wave"), sizeof(FRigUnit_ChainHarmonics_Wave), Get_Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_ChainHarmonics_Wave>()
{
	return FRigUnit_ChainHarmonics_Wave::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_ChainHarmonics_Wave(FRigUnit_ChainHarmonics_Wave::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_ChainHarmonics_Wave"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ChainHarmonics_Wave
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ChainHarmonics_Wave()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_ChainHarmonics_Wave>(FName(TEXT("RigUnit_ChainHarmonics_Wave")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ChainHarmonics_Wave;
	struct Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnabled_MetaData[];
#endif
		static void NewProp_bEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaveFrequency_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WaveFrequency;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaveAmplitude_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WaveAmplitude;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaveOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WaveOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaveNoise_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WaveNoise;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaveMinimum_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_WaveMinimum;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaveMaximum_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_WaveMaximum;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_WaveEase_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaveEase_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_WaveEase;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_ChainHarmonics_Wave>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_bEnabled_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_bEnabled_SetBit(void* Obj)
	{
		((FRigUnit_ChainHarmonics_Wave*)Obj)->bEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_bEnabled = { "bEnabled", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_ChainHarmonics_Wave), &Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_bEnabled_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_bEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_bEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_WaveFrequency_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_WaveFrequency = { "WaveFrequency", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonics_Wave, WaveFrequency), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_WaveFrequency_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_WaveFrequency_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_WaveAmplitude_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_WaveAmplitude = { "WaveAmplitude", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonics_Wave, WaveAmplitude), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_WaveAmplitude_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_WaveAmplitude_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_WaveOffset_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_WaveOffset = { "WaveOffset", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonics_Wave, WaveOffset), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_WaveOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_WaveOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_WaveNoise_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_WaveNoise = { "WaveNoise", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonics_Wave, WaveNoise), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_WaveNoise_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_WaveNoise_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_WaveMinimum_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_WaveMinimum = { "WaveMinimum", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonics_Wave, WaveMinimum), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_WaveMinimum_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_WaveMinimum_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_WaveMaximum_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_WaveMaximum = { "WaveMaximum", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonics_Wave, WaveMaximum), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_WaveMaximum_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_WaveMaximum_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_WaveEase_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_WaveEase_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_WaveEase = { "WaveEase", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonics_Wave, WaveEase), Z_Construct_UEnum_ControlRig_EControlRigAnimEasingType, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_WaveEase_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_WaveEase_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_bEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_WaveFrequency,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_WaveAmplitude,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_WaveOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_WaveNoise,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_WaveMinimum,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_WaveMaximum,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_WaveEase_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::NewProp_WaveEase,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"RigUnit_ChainHarmonics_Wave",
		sizeof(FRigUnit_ChainHarmonics_Wave),
		alignof(FRigUnit_ChainHarmonics_Wave),
		Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_ChainHarmonics_Wave"), sizeof(FRigUnit_ChainHarmonics_Wave), Get_Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Wave_Hash() { return 3780767031U; }
class UScriptStruct* FRigUnit_ChainHarmonics_Reach::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_ChainHarmonics_Reach"), sizeof(FRigUnit_ChainHarmonics_Reach), Get_Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_ChainHarmonics_Reach>()
{
	return FRigUnit_ChainHarmonics_Reach::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_ChainHarmonics_Reach(FRigUnit_ChainHarmonics_Reach::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_ChainHarmonics_Reach"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ChainHarmonics_Reach
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ChainHarmonics_Reach()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_ChainHarmonics_Reach>(FName(TEXT("RigUnit_ChainHarmonics_Reach")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ChainHarmonics_Reach;
	struct Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnabled_MetaData[];
#endif
		static void NewProp_bEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReachTarget_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReachTarget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReachAxis_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReachAxis;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReachMinimum_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReachMinimum;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReachMaximum_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReachMaximum;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReachEase_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReachEase_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReachEase;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_ChainHarmonics_Reach>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::NewProp_bEnabled_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::NewProp_bEnabled_SetBit(void* Obj)
	{
		((FRigUnit_ChainHarmonics_Reach*)Obj)->bEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::NewProp_bEnabled = { "bEnabled", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_ChainHarmonics_Reach), &Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::NewProp_bEnabled_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::NewProp_bEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::NewProp_bEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::NewProp_ReachTarget_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::NewProp_ReachTarget = { "ReachTarget", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonics_Reach, ReachTarget), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::NewProp_ReachTarget_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::NewProp_ReachTarget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::NewProp_ReachAxis_MetaData[] = {
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::NewProp_ReachAxis = { "ReachAxis", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonics_Reach, ReachAxis), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::NewProp_ReachAxis_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::NewProp_ReachAxis_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::NewProp_ReachMinimum_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::NewProp_ReachMinimum = { "ReachMinimum", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonics_Reach, ReachMinimum), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::NewProp_ReachMinimum_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::NewProp_ReachMinimum_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::NewProp_ReachMaximum_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::NewProp_ReachMaximum = { "ReachMaximum", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonics_Reach, ReachMaximum), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::NewProp_ReachMaximum_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::NewProp_ReachMaximum_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::NewProp_ReachEase_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::NewProp_ReachEase_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Harmonics/RigUnit_ChainHarmonics.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::NewProp_ReachEase = { "ReachEase", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ChainHarmonics_Reach, ReachEase), Z_Construct_UEnum_ControlRig_EControlRigAnimEasingType, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::NewProp_ReachEase_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::NewProp_ReachEase_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::NewProp_bEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::NewProp_ReachTarget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::NewProp_ReachAxis,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::NewProp_ReachMinimum,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::NewProp_ReachMaximum,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::NewProp_ReachEase_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::NewProp_ReachEase,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"RigUnit_ChainHarmonics_Reach",
		sizeof(FRigUnit_ChainHarmonics_Reach),
		alignof(FRigUnit_ChainHarmonics_Reach),
		Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_ChainHarmonics_Reach"), sizeof(FRigUnit_ChainHarmonics_Reach), Get_Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ChainHarmonics_Reach_Hash() { return 2146543498U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
