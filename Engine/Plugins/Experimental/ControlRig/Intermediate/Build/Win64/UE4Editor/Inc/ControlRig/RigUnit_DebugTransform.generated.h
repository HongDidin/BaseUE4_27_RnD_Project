// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigUnit_DebugTransform_generated_h
#error "RigUnit_DebugTransform.generated.h already included, missing '#pragma once' in RigUnit_DebugTransform.h"
#endif
#define CONTROLRIG_RigUnit_DebugTransform_generated_h


#define FRigUnit_DebugTransformArrayMutable_Execute() \
	void FRigUnit_DebugTransformArrayMutable::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FTransform>& Transforms, \
		const ERigUnitDebugTransformMode Mode, \
		const FLinearColor& Color, \
		const float Thickness, \
		const float Scale, \
		const FName& Space, \
		const FTransform& WorldOffset, \
		const bool bEnabled, \
		FRigUnit_DebugTransformArrayMutable_WorkData& WorkData, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Debug_RigUnit_DebugTransform_h_169_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_DebugTransformArrayMutable_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FTransform>& Transforms, \
		const ERigUnitDebugTransformMode Mode, \
		const FLinearColor& Color, \
		const float Thickness, \
		const float Scale, \
		const FName& Space, \
		const FTransform& WorldOffset, \
		const bool bEnabled, \
		FRigUnit_DebugTransformArrayMutable_WorkData& WorkData, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		FRigVMFixedArray<FTransform> Transforms((FTransform*)RigVMMemoryHandles[0].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[1].GetData())); \
		ERigUnitDebugTransformMode Mode = (ERigUnitDebugTransformMode)*(uint8*)RigVMMemoryHandles[2].GetData(); \
		const FLinearColor& Color = *(FLinearColor*)RigVMMemoryHandles[3].GetData(); \
		const float Thickness = *(float*)RigVMMemoryHandles[4].GetData(); \
		const float Scale = *(float*)RigVMMemoryHandles[5].GetData(); \
		const FName& Space = *(FName*)RigVMMemoryHandles[6].GetData(); \
		const FTransform& WorldOffset = *(FTransform*)RigVMMemoryHandles[7].GetData(); \
		const bool bEnabled = *(bool*)RigVMMemoryHandles[8].GetData(); \
		FRigVMDynamicArray<FRigUnit_DebugTransformArrayMutable_WorkData> WorkData_9_Array(*((FRigVMByteArray*)RigVMMemoryHandles[9].GetData(0, false))); \
		WorkData_9_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FRigUnit_DebugTransformArrayMutable_WorkData& WorkData = WorkData_9_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[10].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Transforms, \
			Mode, \
			Color, \
			Thickness, \
			Scale, \
			Space, \
			WorldOffset, \
			bEnabled, \
			WorkData, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnit_DebugBaseMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_DebugTransformArrayMutable>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Debug_RigUnit_DebugTransform_h_160_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_DebugTransformArrayMutable_WorkData_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_DebugTransformArrayMutable_WorkData>();


#define FRigUnit_DebugTransformMutableItemSpace_Execute() \
	void FRigUnit_DebugTransformMutableItemSpace::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FTransform& Transform, \
		const ERigUnitDebugTransformMode Mode, \
		const FLinearColor& Color, \
		const float Thickness, \
		const float Scale, \
		const FRigElementKey& Space, \
		const FTransform& WorldOffset, \
		const bool bEnabled, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Debug_RigUnit_DebugTransform_h_116_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_DebugTransformMutableItemSpace_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FTransform& Transform, \
		const ERigUnitDebugTransformMode Mode, \
		const FLinearColor& Color, \
		const float Thickness, \
		const float Scale, \
		const FRigElementKey& Space, \
		const FTransform& WorldOffset, \
		const bool bEnabled, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FTransform& Transform = *(FTransform*)RigVMMemoryHandles[0].GetData(); \
		ERigUnitDebugTransformMode Mode = (ERigUnitDebugTransformMode)*(uint8*)RigVMMemoryHandles[1].GetData(); \
		const FLinearColor& Color = *(FLinearColor*)RigVMMemoryHandles[2].GetData(); \
		const float Thickness = *(float*)RigVMMemoryHandles[3].GetData(); \
		const float Scale = *(float*)RigVMMemoryHandles[4].GetData(); \
		const FRigElementKey& Space = *(FRigElementKey*)RigVMMemoryHandles[5].GetData(); \
		const FTransform& WorldOffset = *(FTransform*)RigVMMemoryHandles[6].GetData(); \
		const bool bEnabled = *(bool*)RigVMMemoryHandles[7].GetData(); \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[8].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Transform, \
			Mode, \
			Color, \
			Thickness, \
			Scale, \
			Space, \
			WorldOffset, \
			bEnabled, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnit_DebugBaseMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_DebugTransformMutableItemSpace>();


#define FRigUnit_DebugTransformMutable_Execute() \
	void FRigUnit_DebugTransformMutable::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FTransform& Transform, \
		const ERigUnitDebugTransformMode Mode, \
		const FLinearColor& Color, \
		const float Thickness, \
		const float Scale, \
		const FName& Space, \
		const FTransform& WorldOffset, \
		const bool bEnabled, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Debug_RigUnit_DebugTransform_h_70_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_DebugTransformMutable_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FTransform& Transform, \
		const ERigUnitDebugTransformMode Mode, \
		const FLinearColor& Color, \
		const float Thickness, \
		const float Scale, \
		const FName& Space, \
		const FTransform& WorldOffset, \
		const bool bEnabled, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FTransform& Transform = *(FTransform*)RigVMMemoryHandles[0].GetData(); \
		ERigUnitDebugTransformMode Mode = (ERigUnitDebugTransformMode)*(uint8*)RigVMMemoryHandles[1].GetData(); \
		const FLinearColor& Color = *(FLinearColor*)RigVMMemoryHandles[2].GetData(); \
		const float Thickness = *(float*)RigVMMemoryHandles[3].GetData(); \
		const float Scale = *(float*)RigVMMemoryHandles[4].GetData(); \
		const FName& Space = *(FName*)RigVMMemoryHandles[5].GetData(); \
		const FTransform& WorldOffset = *(FTransform*)RigVMMemoryHandles[6].GetData(); \
		const bool bEnabled = *(bool*)RigVMMemoryHandles[7].GetData(); \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[8].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Transform, \
			Mode, \
			Color, \
			Thickness, \
			Scale, \
			Space, \
			WorldOffset, \
			bEnabled, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnit_DebugBaseMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_DebugTransformMutable>();


#define FRigUnit_DebugTransform_Execute() \
	void FRigUnit_DebugTransform::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FTransform& Transform, \
		const ERigUnitDebugTransformMode Mode, \
		const FLinearColor& Color, \
		const float Thickness, \
		const float Scale, \
		const FName& Space, \
		const FTransform& WorldOffset, \
		const bool bEnabled, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Debug_RigUnit_DebugTransform_h_27_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_DebugTransform_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FTransform& Transform, \
		const ERigUnitDebugTransformMode Mode, \
		const FLinearColor& Color, \
		const float Thickness, \
		const float Scale, \
		const FName& Space, \
		const FTransform& WorldOffset, \
		const bool bEnabled, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		FTransform& Transform = *(FTransform*)RigVMMemoryHandles[0].GetData(); \
		ERigUnitDebugTransformMode Mode = (ERigUnitDebugTransformMode)*(uint8*)RigVMMemoryHandles[1].GetData(); \
		const FLinearColor& Color = *(FLinearColor*)RigVMMemoryHandles[2].GetData(); \
		const float Thickness = *(float*)RigVMMemoryHandles[3].GetData(); \
		const float Scale = *(float*)RigVMMemoryHandles[4].GetData(); \
		const FName& Space = *(FName*)RigVMMemoryHandles[5].GetData(); \
		const FTransform& WorldOffset = *(FTransform*)RigVMMemoryHandles[6].GetData(); \
		const bool bEnabled = *(bool*)RigVMMemoryHandles[7].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Transform, \
			Mode, \
			Color, \
			Thickness, \
			Scale, \
			Space, \
			WorldOffset, \
			bEnabled, \
			Context \
		); \
	} \
	typedef FRigUnit_DebugBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_DebugTransform>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Debug_RigUnit_DebugTransform_h


#define FOREACH_ENUM_ERIGUNITDEBUGTRANSFORMMODE(op) \
	op(ERigUnitDebugTransformMode::Point) \
	op(ERigUnitDebugTransformMode::Axes) \
	op(ERigUnitDebugTransformMode::Box) \
	op(ERigUnitDebugTransformMode::Max) 

enum class ERigUnitDebugTransformMode : uint8;
template<> CONTROLRIG_API UEnum* StaticEnum<ERigUnitDebugTransformMode>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
