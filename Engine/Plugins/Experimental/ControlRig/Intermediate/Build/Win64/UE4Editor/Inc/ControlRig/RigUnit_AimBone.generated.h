// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigUnit_AimBone_generated_h
#error "RigUnit_AimBone.generated.h already included, missing '#pragma once' in RigUnit_AimBone.h"
#endif
#define CONTROLRIG_RigUnit_AimBone_generated_h


#define FRigUnit_AimItem_Execute() \
	void FRigUnit_AimItem::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKey& Item, \
		const FRigUnit_AimItem_Target& Primary, \
		const FRigUnit_AimItem_Target& Secondary, \
		const float Weight, \
		const FRigUnit_AimBone_DebugSettings& DebugSettings, \
		FCachedRigElement& CachedItem, \
		FCachedRigElement& PrimaryCachedSpace, \
		FCachedRigElement& SecondaryCachedSpace, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_AimBone_h_288_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_AimItem_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKey& Item, \
		const FRigUnit_AimItem_Target& Primary, \
		const FRigUnit_AimItem_Target& Secondary, \
		const float Weight, \
		const FRigUnit_AimBone_DebugSettings& DebugSettings, \
		FCachedRigElement& CachedItem, \
		FCachedRigElement& PrimaryCachedSpace, \
		FCachedRigElement& SecondaryCachedSpace, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FRigElementKey& Item = *(FRigElementKey*)RigVMMemoryHandles[0].GetData(); \
		const FRigUnit_AimItem_Target& Primary = *(FRigUnit_AimItem_Target*)RigVMMemoryHandles[1].GetData(); \
		const FRigUnit_AimItem_Target& Secondary = *(FRigUnit_AimItem_Target*)RigVMMemoryHandles[2].GetData(); \
		const float Weight = *(float*)RigVMMemoryHandles[3].GetData(); \
		const FRigUnit_AimBone_DebugSettings& DebugSettings = *(FRigUnit_AimBone_DebugSettings*)RigVMMemoryHandles[4].GetData(); \
		FRigVMDynamicArray<FCachedRigElement> CachedItem_5_Array(*((FRigVMByteArray*)RigVMMemoryHandles[5].GetData(0, false))); \
		CachedItem_5_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedItem = CachedItem_5_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<FCachedRigElement> PrimaryCachedSpace_6_Array(*((FRigVMByteArray*)RigVMMemoryHandles[6].GetData(0, false))); \
		PrimaryCachedSpace_6_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& PrimaryCachedSpace = PrimaryCachedSpace_6_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<FCachedRigElement> SecondaryCachedSpace_7_Array(*((FRigVMByteArray*)RigVMMemoryHandles[7].GetData(0, false))); \
		SecondaryCachedSpace_7_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& SecondaryCachedSpace = SecondaryCachedSpace_7_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[8].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Item, \
			Primary, \
			Secondary, \
			Weight, \
			DebugSettings, \
			CachedItem, \
			PrimaryCachedSpace, \
			SecondaryCachedSpace, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnit_HighlevelBaseMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_AimItem>();


#define FRigUnit_AimBone_Execute() \
	void FRigUnit_AimBone::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Bone, \
		const FRigUnit_AimBone_Target& Primary, \
		const FRigUnit_AimBone_Target& Secondary, \
		const float Weight, \
		const bool bPropagateToChildren, \
		const FRigUnit_AimBone_DebugSettings& DebugSettings, \
		FCachedRigElement& CachedBoneIndex, \
		FCachedRigElement& PrimaryCachedSpace, \
		FCachedRigElement& SecondaryCachedSpace, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_AimBone_h_202_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_AimBone_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Bone, \
		const FRigUnit_AimBone_Target& Primary, \
		const FRigUnit_AimBone_Target& Secondary, \
		const float Weight, \
		const bool bPropagateToChildren, \
		const FRigUnit_AimBone_DebugSettings& DebugSettings, \
		FCachedRigElement& CachedBoneIndex, \
		FCachedRigElement& PrimaryCachedSpace, \
		FCachedRigElement& SecondaryCachedSpace, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FName& Bone = *(FName*)RigVMMemoryHandles[0].GetData(); \
		const FRigUnit_AimBone_Target& Primary = *(FRigUnit_AimBone_Target*)RigVMMemoryHandles[1].GetData(); \
		const FRigUnit_AimBone_Target& Secondary = *(FRigUnit_AimBone_Target*)RigVMMemoryHandles[2].GetData(); \
		const float Weight = *(float*)RigVMMemoryHandles[3].GetData(); \
		const bool bPropagateToChildren = *(bool*)RigVMMemoryHandles[4].GetData(); \
		const FRigUnit_AimBone_DebugSettings& DebugSettings = *(FRigUnit_AimBone_DebugSettings*)RigVMMemoryHandles[5].GetData(); \
		FRigVMDynamicArray<FCachedRigElement> CachedBoneIndex_6_Array(*((FRigVMByteArray*)RigVMMemoryHandles[6].GetData(0, false))); \
		CachedBoneIndex_6_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedBoneIndex = CachedBoneIndex_6_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<FCachedRigElement> PrimaryCachedSpace_7_Array(*((FRigVMByteArray*)RigVMMemoryHandles[7].GetData(0, false))); \
		PrimaryCachedSpace_7_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& PrimaryCachedSpace = PrimaryCachedSpace_7_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<FCachedRigElement> SecondaryCachedSpace_8_Array(*((FRigVMByteArray*)RigVMMemoryHandles[8].GetData(0, false))); \
		SecondaryCachedSpace_8_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& SecondaryCachedSpace = SecondaryCachedSpace_8_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[9].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Bone, \
			Primary, \
			Secondary, \
			Weight, \
			bPropagateToChildren, \
			DebugSettings, \
			CachedBoneIndex, \
			PrimaryCachedSpace, \
			SecondaryCachedSpace, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnit_HighlevelBaseMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_AimBone>();


#define FRigUnit_AimBoneMath_Execute() \
	void FRigUnit_AimBoneMath::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FTransform& InputTransform, \
		const FRigUnit_AimItem_Target& Primary, \
		const FRigUnit_AimItem_Target& Secondary, \
		const float Weight, \
		FTransform& Result, \
		const FRigUnit_AimBone_DebugSettings& DebugSettings, \
		FCachedRigElement& PrimaryCachedSpace, \
		FCachedRigElement& SecondaryCachedSpace, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_AimBone_h_136_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_AimBoneMath_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FTransform& InputTransform, \
		const FRigUnit_AimItem_Target& Primary, \
		const FRigUnit_AimItem_Target& Secondary, \
		const float Weight, \
		FTransform& Result, \
		const FRigUnit_AimBone_DebugSettings& DebugSettings, \
		FCachedRigElement& PrimaryCachedSpace, \
		FCachedRigElement& SecondaryCachedSpace, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FTransform& InputTransform = *(FTransform*)RigVMMemoryHandles[0].GetData(); \
		const FRigUnit_AimItem_Target& Primary = *(FRigUnit_AimItem_Target*)RigVMMemoryHandles[1].GetData(); \
		const FRigUnit_AimItem_Target& Secondary = *(FRigUnit_AimItem_Target*)RigVMMemoryHandles[2].GetData(); \
		const float Weight = *(float*)RigVMMemoryHandles[3].GetData(); \
		FTransform& Result = *(FTransform*)RigVMMemoryHandles[4].GetData(); \
		const FRigUnit_AimBone_DebugSettings& DebugSettings = *(FRigUnit_AimBone_DebugSettings*)RigVMMemoryHandles[5].GetData(); \
		FRigVMDynamicArray<FCachedRigElement> PrimaryCachedSpace_6_Array(*((FRigVMByteArray*)RigVMMemoryHandles[6].GetData(0, false))); \
		PrimaryCachedSpace_6_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& PrimaryCachedSpace = PrimaryCachedSpace_6_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<FCachedRigElement> SecondaryCachedSpace_7_Array(*((FRigVMByteArray*)RigVMMemoryHandles[7].GetData(0, false))); \
		SecondaryCachedSpace_7_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& SecondaryCachedSpace = SecondaryCachedSpace_7_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			InputTransform, \
			Primary, \
			Secondary, \
			Weight, \
			Result, \
			DebugSettings, \
			PrimaryCachedSpace, \
			SecondaryCachedSpace, \
			Context \
		); \
	} \
	typedef FRigUnit_HighlevelBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_AimBoneMath>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_AimBone_h_101_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_AimBone_DebugSettings_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_AimBone_DebugSettings>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_AimBone_h_56_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_AimItem_Target_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_AimItem_Target>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_AimBone_h_11_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_AimBone_Target_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_AimBone_Target>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_AimBone_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
