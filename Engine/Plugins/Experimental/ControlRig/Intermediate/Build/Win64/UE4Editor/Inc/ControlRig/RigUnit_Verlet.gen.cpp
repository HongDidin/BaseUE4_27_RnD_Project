// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Simulation/RigUnit_Verlet.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_Verlet() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_SimBase();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FCRSimPoint();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_VerletIntegrateVector>() == std::is_polymorphic<FRigUnit_SimBase>(), "USTRUCT FRigUnit_VerletIntegrateVector cannot be polymorphic unless super FRigUnit_SimBase is polymorphic");

class UScriptStruct* FRigUnit_VerletIntegrateVector::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_VerletIntegrateVector"), sizeof(FRigUnit_VerletIntegrateVector), Get_Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_VerletIntegrateVector::Execute"), &FRigUnit_VerletIntegrateVector::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_VerletIntegrateVector>()
{
	return FRigUnit_VerletIntegrateVector::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_VerletIntegrateVector(FRigUnit_VerletIntegrateVector::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_VerletIntegrateVector"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_VerletIntegrateVector
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_VerletIntegrateVector()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_VerletIntegrateVector>(FName(TEXT("RigUnit_VerletIntegrateVector")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_VerletIntegrateVector;
	struct Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Target_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Target;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Strength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Strength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Damp_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Damp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Blend_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Blend;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Position_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Position;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Velocity_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Velocity;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Acceleration_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Acceleration;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Point_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Point;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bInitialized_MetaData[];
#endif
		static void NewProp_bInitialized_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInitialized;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Simulates a single position over time using verlet integration\n */" },
		{ "DisplayName", "Verlet (Vector)" },
		{ "Keywords", "Simulate,Integrate" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Verlet.h" },
		{ "PrototypeName", "Verlet" },
		{ "ToolTip", "Simulates a single position over time using verlet integration" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_VerletIntegrateVector>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_Target_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Verlet.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_Target = { "Target", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_VerletIntegrateVector, Target), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_Target_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_Target_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_Strength_MetaData[] = {
		{ "Comment", "/** The strength of the verlet spring */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Verlet.h" },
		{ "ToolTip", "The strength of the verlet spring" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_Strength = { "Strength", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_VerletIntegrateVector, Strength), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_Strength_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_Strength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_Damp_MetaData[] = {
		{ "Comment", "/** The amount of damping to apply ( 0.0 to 1.0, but usually really low like 0.005 )*/" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Verlet.h" },
		{ "ToolTip", "The amount of damping to apply ( 0.0 to 1.0, but usually really low like 0.005 )" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_Damp = { "Damp", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_VerletIntegrateVector, Damp), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_Damp_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_Damp_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_Blend_MetaData[] = {
		{ "Comment", "/** The amount of blending to apply per second */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Verlet.h" },
		{ "ToolTip", "The amount of blending to apply per second" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_Blend = { "Blend", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_VerletIntegrateVector, Blend), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_Blend_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_Blend_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_Position_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Verlet.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_Position = { "Position", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_VerletIntegrateVector, Position), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_Position_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_Position_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_Velocity_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Verlet.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_Velocity = { "Velocity", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_VerletIntegrateVector, Velocity), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_Velocity_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_Velocity_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_Acceleration_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Verlet.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_Acceleration = { "Acceleration", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_VerletIntegrateVector, Acceleration), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_Acceleration_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_Acceleration_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_Point_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Verlet.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_Point = { "Point", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_VerletIntegrateVector, Point), Z_Construct_UScriptStruct_FCRSimPoint, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_Point_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_Point_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_bInitialized_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Verlet.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_bInitialized_SetBit(void* Obj)
	{
		((FRigUnit_VerletIntegrateVector*)Obj)->bInitialized = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_bInitialized = { "bInitialized", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_VerletIntegrateVector), &Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_bInitialized_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_bInitialized_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_bInitialized_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_Target,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_Strength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_Damp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_Blend,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_Position,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_Velocity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_Acceleration,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_Point,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::NewProp_bInitialized,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_SimBase,
		&NewStructOps,
		"RigUnit_VerletIntegrateVector",
		sizeof(FRigUnit_VerletIntegrateVector),
		alignof(FRigUnit_VerletIntegrateVector),
		Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_VerletIntegrateVector"), sizeof(FRigUnit_VerletIntegrateVector), Get_Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_VerletIntegrateVector_Hash() { return 1603788240U; }

void FRigUnit_VerletIntegrateVector::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Target,
		Strength,
		Damp,
		Blend,
		Position,
		Velocity,
		Acceleration,
		Point,
		bInitialized,
		Context
	);
}

PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
