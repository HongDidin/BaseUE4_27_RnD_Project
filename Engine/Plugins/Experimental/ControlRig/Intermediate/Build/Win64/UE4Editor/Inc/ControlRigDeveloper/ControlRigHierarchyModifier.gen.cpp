// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRigDeveloper/Public/ControlRigHierarchyModifier.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeControlRigHierarchyModifier() {}
// Cross Module References
	CONTROLRIGDEVELOPER_API UClass* Z_Construct_UClass_UControlRigHierarchyModifier_NoRegister();
	CONTROLRIGDEVELOPER_API UClass* Z_Construct_UClass_UControlRigHierarchyModifier();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_ControlRigDeveloper();
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_ERigBoneType();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigElementKey();
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_ERigControlType();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_ERigSpaceType();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigBone();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigControl();
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_ERigControlValueType();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigCurve();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigSpace();
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_ERigHierarchyImportMode();
// End Cross Module References
#if WITH_EDITOR
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execImportFromText)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_InContent);
		P_GET_ENUM(ERigHierarchyImportMode,Z_Param_InImportMode);
		P_GET_UBOOL(Z_Param_bSelectNewElements);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<FRigElementKey>*)Z_Param__Result=P_THIS->ImportFromText(Z_Param_InContent,ERigHierarchyImportMode(Z_Param_InImportMode),Z_Param_bSelectNewElements);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execExportToText)
	{
		P_GET_TARRAY_REF(FRigElementKey,Z_Param_Out_InElementsToExport);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=P_THIS->ExportToText(Z_Param_Out_InElementsToExport);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execSetGlobalTransform)
	{
		P_GET_STRUCT_REF(FRigElementKey,Z_Param_Out_InKey);
		P_GET_STRUCT_REF(FTransform,Z_Param_Out_InTransform);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetGlobalTransform(Z_Param_Out_InKey,Z_Param_Out_InTransform);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execGetGlobalTransform)
	{
		P_GET_STRUCT_REF(FRigElementKey,Z_Param_Out_InKey);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FTransform*)Z_Param__Result=P_THIS->GetGlobalTransform(Z_Param_Out_InKey);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execSetLocalTransform)
	{
		P_GET_STRUCT_REF(FRigElementKey,Z_Param_Out_InKey);
		P_GET_STRUCT_REF(FTransform,Z_Param_Out_InTransform);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetLocalTransform(Z_Param_Out_InKey,Z_Param_Out_InTransform);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execGetLocalTransform)
	{
		P_GET_STRUCT_REF(FRigElementKey,Z_Param_Out_InKey);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FTransform*)Z_Param__Result=P_THIS->GetLocalTransform(Z_Param_Out_InKey);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execSetInitialGlobalTransform)
	{
		P_GET_STRUCT_REF(FRigElementKey,Z_Param_Out_InKey);
		P_GET_STRUCT_REF(FTransform,Z_Param_Out_InTransform);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetInitialGlobalTransform(Z_Param_Out_InKey,Z_Param_Out_InTransform);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execGetInitialGlobalTransform)
	{
		P_GET_STRUCT_REF(FRigElementKey,Z_Param_Out_InKey);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FTransform*)Z_Param__Result=P_THIS->GetInitialGlobalTransform(Z_Param_Out_InKey);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execSetInitialTransform)
	{
		P_GET_STRUCT_REF(FRigElementKey,Z_Param_Out_InKey);
		P_GET_STRUCT_REF(FTransform,Z_Param_Out_InTransform);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetInitialTransform(Z_Param_Out_InKey,Z_Param_Out_InTransform);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execGetInitialTransform)
	{
		P_GET_STRUCT_REF(FRigElementKey,Z_Param_Out_InKey);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FTransform*)Z_Param__Result=P_THIS->GetInitialTransform(Z_Param_Out_InKey);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execResetTransforms)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ResetTransforms();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execReset)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Reset();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execInitialize)
	{
		P_GET_UBOOL(Z_Param_bResetTransforms);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Initialize(Z_Param_bResetTransforms);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execIsSelected)
	{
		P_GET_STRUCT_REF(FRigElementKey,Z_Param_Out_InKey);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsSelected(Z_Param_Out_InKey);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execClearSelection)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->ClearSelection();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execSelect)
	{
		P_GET_STRUCT_REF(FRigElementKey,Z_Param_Out_InKey);
		P_GET_UBOOL(Z_Param_bSelect);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->Select(Z_Param_Out_InKey,Z_Param_bSelect);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execGetSelection)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<FRigElementKey>*)Z_Param__Result=P_THIS->GetSelection();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execReparentElement)
	{
		P_GET_STRUCT_REF(FRigElementKey,Z_Param_Out_InElement);
		P_GET_STRUCT_REF(FRigElementKey,Z_Param_Out_InNewParent);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->ReparentElement(Z_Param_Out_InElement,Z_Param_Out_InNewParent);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execRenameElement)
	{
		P_GET_STRUCT_REF(FRigElementKey,Z_Param_Out_InElement);
		P_GET_PROPERTY_REF(FNameProperty,Z_Param_Out_InNewName);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FRigElementKey*)Z_Param__Result=P_THIS->RenameElement(Z_Param_Out_InElement,Z_Param_Out_InNewName);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execRemoveElement)
	{
		P_GET_STRUCT_REF(FRigElementKey,Z_Param_Out_InElement);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->RemoveElement(Z_Param_Out_InElement);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execSetCurve)
	{
		P_GET_STRUCT_REF(FRigCurve,Z_Param_Out_InElement);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetCurve(Z_Param_Out_InElement);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execGetCurve)
	{
		P_GET_STRUCT_REF(FRigElementKey,Z_Param_Out_InKey);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FRigCurve*)Z_Param__Result=P_THIS->GetCurve(Z_Param_Out_InKey);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execAddCurve)
	{
		P_GET_PROPERTY_REF(FNameProperty,Z_Param_Out_InNewName);
		P_GET_PROPERTY(FFloatProperty,Z_Param_InValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FRigElementKey*)Z_Param__Result=P_THIS->AddCurve(Z_Param_Out_InNewName,Z_Param_InValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execSetSpace)
	{
		P_GET_STRUCT_REF(FRigSpace,Z_Param_Out_InElement);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetSpace(Z_Param_Out_InElement);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execGetSpace)
	{
		P_GET_STRUCT_REF(FRigElementKey,Z_Param_Out_InKey);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FRigSpace*)Z_Param__Result=P_THIS->GetSpace(Z_Param_Out_InKey);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execAddSpace)
	{
		P_GET_PROPERTY_REF(FNameProperty,Z_Param_Out_InNewName);
		P_GET_ENUM(ERigSpaceType,Z_Param_InSpaceType);
		P_GET_PROPERTY_REF(FNameProperty,Z_Param_Out_InParentName);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FRigElementKey*)Z_Param__Result=P_THIS->AddSpace(Z_Param_Out_InNewName,ERigSpaceType(Z_Param_InSpaceType),Z_Param_Out_InParentName);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execSetControlValueTransform)
	{
		P_GET_STRUCT_REF(FRigElementKey,Z_Param_Out_InKey);
		P_GET_STRUCT(FTransform,Z_Param_InValue);
		P_GET_ENUM(ERigControlValueType,Z_Param_InValueType);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetControlValueTransform(Z_Param_Out_InKey,Z_Param_InValue,ERigControlValueType(Z_Param_InValueType));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execSetControlValueRotator)
	{
		P_GET_STRUCT_REF(FRigElementKey,Z_Param_Out_InKey);
		P_GET_STRUCT(FRotator,Z_Param_InValue);
		P_GET_ENUM(ERigControlValueType,Z_Param_InValueType);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetControlValueRotator(Z_Param_Out_InKey,Z_Param_InValue,ERigControlValueType(Z_Param_InValueType));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execSetControlValueVector)
	{
		P_GET_STRUCT_REF(FRigElementKey,Z_Param_Out_InKey);
		P_GET_STRUCT(FVector,Z_Param_InValue);
		P_GET_ENUM(ERigControlValueType,Z_Param_InValueType);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetControlValueVector(Z_Param_Out_InKey,Z_Param_InValue,ERigControlValueType(Z_Param_InValueType));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execSetControlValueVector2D)
	{
		P_GET_STRUCT_REF(FRigElementKey,Z_Param_Out_InKey);
		P_GET_STRUCT(FVector2D,Z_Param_InValue);
		P_GET_ENUM(ERigControlValueType,Z_Param_InValueType);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetControlValueVector2D(Z_Param_Out_InKey,Z_Param_InValue,ERigControlValueType(Z_Param_InValueType));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execSetControlValueFloat)
	{
		P_GET_STRUCT_REF(FRigElementKey,Z_Param_Out_InKey);
		P_GET_PROPERTY(FFloatProperty,Z_Param_InValue);
		P_GET_ENUM(ERigControlValueType,Z_Param_InValueType);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetControlValueFloat(Z_Param_Out_InKey,Z_Param_InValue,ERigControlValueType(Z_Param_InValueType));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execSetControlValueInt)
	{
		P_GET_STRUCT_REF(FRigElementKey,Z_Param_Out_InKey);
		P_GET_PROPERTY(FIntProperty,Z_Param_InValue);
		P_GET_ENUM(ERigControlValueType,Z_Param_InValueType);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetControlValueInt(Z_Param_Out_InKey,Z_Param_InValue,ERigControlValueType(Z_Param_InValueType));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execSetControlValueBool)
	{
		P_GET_STRUCT_REF(FRigElementKey,Z_Param_Out_InKey);
		P_GET_UBOOL(Z_Param_InValue);
		P_GET_ENUM(ERigControlValueType,Z_Param_InValueType);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetControlValueBool(Z_Param_Out_InKey,Z_Param_InValue,ERigControlValueType(Z_Param_InValueType));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execGetControlValueTransform)
	{
		P_GET_STRUCT_REF(FRigElementKey,Z_Param_Out_InKey);
		P_GET_ENUM(ERigControlValueType,Z_Param_InValueType);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FTransform*)Z_Param__Result=P_THIS->GetControlValueTransform(Z_Param_Out_InKey,ERigControlValueType(Z_Param_InValueType));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execGetControlValueRotator)
	{
		P_GET_STRUCT_REF(FRigElementKey,Z_Param_Out_InKey);
		P_GET_ENUM(ERigControlValueType,Z_Param_InValueType);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FRotator*)Z_Param__Result=P_THIS->GetControlValueRotator(Z_Param_Out_InKey,ERigControlValueType(Z_Param_InValueType));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execGetControlValueVector)
	{
		P_GET_STRUCT_REF(FRigElementKey,Z_Param_Out_InKey);
		P_GET_ENUM(ERigControlValueType,Z_Param_InValueType);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FVector*)Z_Param__Result=P_THIS->GetControlValueVector(Z_Param_Out_InKey,ERigControlValueType(Z_Param_InValueType));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execGetControlValueVector2D)
	{
		P_GET_STRUCT_REF(FRigElementKey,Z_Param_Out_InKey);
		P_GET_ENUM(ERigControlValueType,Z_Param_InValueType);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FVector2D*)Z_Param__Result=P_THIS->GetControlValueVector2D(Z_Param_Out_InKey,ERigControlValueType(Z_Param_InValueType));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execGetControlValueFloat)
	{
		P_GET_STRUCT_REF(FRigElementKey,Z_Param_Out_InKey);
		P_GET_ENUM(ERigControlValueType,Z_Param_InValueType);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=P_THIS->GetControlValueFloat(Z_Param_Out_InKey,ERigControlValueType(Z_Param_InValueType));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execGetControlValueInt)
	{
		P_GET_STRUCT_REF(FRigElementKey,Z_Param_Out_InKey);
		P_GET_ENUM(ERigControlValueType,Z_Param_InValueType);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetControlValueInt(Z_Param_Out_InKey,ERigControlValueType(Z_Param_InValueType));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execGetControlValueBool)
	{
		P_GET_STRUCT_REF(FRigElementKey,Z_Param_Out_InKey);
		P_GET_ENUM(ERigControlValueType,Z_Param_InValueType);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GetControlValueBool(Z_Param_Out_InKey,ERigControlValueType(Z_Param_InValueType));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execSetControl)
	{
		P_GET_STRUCT_REF(FRigControl,Z_Param_Out_InElement);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetControl(Z_Param_Out_InElement);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execGetControl)
	{
		P_GET_STRUCT_REF(FRigElementKey,Z_Param_Out_InKey);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FRigControl*)Z_Param__Result=P_THIS->GetControl(Z_Param_Out_InKey);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execAddControl)
	{
		P_GET_PROPERTY_REF(FNameProperty,Z_Param_Out_InNewName);
		P_GET_ENUM(ERigControlType,Z_Param_InControlType);
		P_GET_PROPERTY_REF(FNameProperty,Z_Param_Out_InParentName);
		P_GET_PROPERTY_REF(FNameProperty,Z_Param_Out_InSpaceName);
		P_GET_PROPERTY_REF(FNameProperty,Z_Param_Out_InGizmoName);
		P_GET_STRUCT_REF(FLinearColor,Z_Param_Out_InGizmoColor);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FRigElementKey*)Z_Param__Result=P_THIS->AddControl(Z_Param_Out_InNewName,ERigControlType(Z_Param_InControlType),Z_Param_Out_InParentName,Z_Param_Out_InSpaceName,Z_Param_Out_InGizmoName,Z_Param_Out_InGizmoColor);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execSetBone)
	{
		P_GET_STRUCT_REF(FRigBone,Z_Param_Out_InElement);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetBone(Z_Param_Out_InElement);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execGetBone)
	{
		P_GET_STRUCT_REF(FRigElementKey,Z_Param_Out_InKey);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FRigBone*)Z_Param__Result=P_THIS->GetBone(Z_Param_Out_InKey);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execAddBone)
	{
		P_GET_PROPERTY_REF(FNameProperty,Z_Param_Out_InNewName);
		P_GET_PROPERTY_REF(FNameProperty,Z_Param_Out_InParentName);
		P_GET_ENUM(ERigBoneType,Z_Param_InType);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FRigElementKey*)Z_Param__Result=P_THIS->AddBone(Z_Param_Out_InNewName,Z_Param_Out_InParentName,ERigBoneType(Z_Param_InType));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigHierarchyModifier::execGetElements)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<FRigElementKey>*)Z_Param__Result=P_THIS->GetElements();
		P_NATIVE_END;
	}
#endif //WITH_EDITOR
	void UControlRigHierarchyModifier::StaticRegisterNativesUControlRigHierarchyModifier()
	{
#if WITH_EDITOR
		UClass* Class = UControlRigHierarchyModifier::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddBone", &UControlRigHierarchyModifier::execAddBone },
			{ "AddControl", &UControlRigHierarchyModifier::execAddControl },
			{ "AddCurve", &UControlRigHierarchyModifier::execAddCurve },
			{ "AddSpace", &UControlRigHierarchyModifier::execAddSpace },
			{ "ClearSelection", &UControlRigHierarchyModifier::execClearSelection },
			{ "ExportToText", &UControlRigHierarchyModifier::execExportToText },
			{ "GetBone", &UControlRigHierarchyModifier::execGetBone },
			{ "GetControl", &UControlRigHierarchyModifier::execGetControl },
			{ "GetControlValueBool", &UControlRigHierarchyModifier::execGetControlValueBool },
			{ "GetControlValueFloat", &UControlRigHierarchyModifier::execGetControlValueFloat },
			{ "GetControlValueInt", &UControlRigHierarchyModifier::execGetControlValueInt },
			{ "GetControlValueRotator", &UControlRigHierarchyModifier::execGetControlValueRotator },
			{ "GetControlValueTransform", &UControlRigHierarchyModifier::execGetControlValueTransform },
			{ "GetControlValueVector", &UControlRigHierarchyModifier::execGetControlValueVector },
			{ "GetControlValueVector2D", &UControlRigHierarchyModifier::execGetControlValueVector2D },
			{ "GetCurve", &UControlRigHierarchyModifier::execGetCurve },
			{ "GetElements", &UControlRigHierarchyModifier::execGetElements },
			{ "GetGlobalTransform", &UControlRigHierarchyModifier::execGetGlobalTransform },
			{ "GetInitialGlobalTransform", &UControlRigHierarchyModifier::execGetInitialGlobalTransform },
			{ "GetInitialTransform", &UControlRigHierarchyModifier::execGetInitialTransform },
			{ "GetLocalTransform", &UControlRigHierarchyModifier::execGetLocalTransform },
			{ "GetSelection", &UControlRigHierarchyModifier::execGetSelection },
			{ "GetSpace", &UControlRigHierarchyModifier::execGetSpace },
			{ "ImportFromText", &UControlRigHierarchyModifier::execImportFromText },
			{ "Initialize", &UControlRigHierarchyModifier::execInitialize },
			{ "IsSelected", &UControlRigHierarchyModifier::execIsSelected },
			{ "RemoveElement", &UControlRigHierarchyModifier::execRemoveElement },
			{ "RenameElement", &UControlRigHierarchyModifier::execRenameElement },
			{ "ReparentElement", &UControlRigHierarchyModifier::execReparentElement },
			{ "Reset", &UControlRigHierarchyModifier::execReset },
			{ "ResetTransforms", &UControlRigHierarchyModifier::execResetTransforms },
			{ "Select", &UControlRigHierarchyModifier::execSelect },
			{ "SetBone", &UControlRigHierarchyModifier::execSetBone },
			{ "SetControl", &UControlRigHierarchyModifier::execSetControl },
			{ "SetControlValueBool", &UControlRigHierarchyModifier::execSetControlValueBool },
			{ "SetControlValueFloat", &UControlRigHierarchyModifier::execSetControlValueFloat },
			{ "SetControlValueInt", &UControlRigHierarchyModifier::execSetControlValueInt },
			{ "SetControlValueRotator", &UControlRigHierarchyModifier::execSetControlValueRotator },
			{ "SetControlValueTransform", &UControlRigHierarchyModifier::execSetControlValueTransform },
			{ "SetControlValueVector", &UControlRigHierarchyModifier::execSetControlValueVector },
			{ "SetControlValueVector2D", &UControlRigHierarchyModifier::execSetControlValueVector2D },
			{ "SetCurve", &UControlRigHierarchyModifier::execSetCurve },
			{ "SetGlobalTransform", &UControlRigHierarchyModifier::execSetGlobalTransform },
			{ "SetInitialGlobalTransform", &UControlRigHierarchyModifier::execSetInitialGlobalTransform },
			{ "SetInitialTransform", &UControlRigHierarchyModifier::execSetInitialTransform },
			{ "SetLocalTransform", &UControlRigHierarchyModifier::execSetLocalTransform },
			{ "SetSpace", &UControlRigHierarchyModifier::execSetSpace },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
#endif // WITH_EDITOR
	}
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_AddBone_Statics
	{
		struct ControlRigHierarchyModifier_eventAddBone_Parms
		{
			FName InNewName;
			FName InParentName;
			ERigBoneType InType;
			FRigElementKey ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InNewName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_InNewName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InParentName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_InParentName;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InType_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InType;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_AddBone_Statics::NewProp_InNewName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_AddBone_Statics::NewProp_InNewName = { "InNewName", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventAddBone_Parms, InNewName), METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_AddBone_Statics::NewProp_InNewName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_AddBone_Statics::NewProp_InNewName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_AddBone_Statics::NewProp_InParentName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_AddBone_Statics::NewProp_InParentName = { "InParentName", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventAddBone_Parms, InParentName), METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_AddBone_Statics::NewProp_InParentName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_AddBone_Statics::NewProp_InParentName_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_AddBone_Statics::NewProp_InType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_AddBone_Statics::NewProp_InType = { "InType", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventAddBone_Parms, InType), Z_Construct_UEnum_ControlRig_ERigBoneType, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_AddBone_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventAddBone_Parms, ReturnValue), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_AddBone_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_AddBone_Statics::NewProp_InNewName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_AddBone_Statics::NewProp_InParentName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_AddBone_Statics::NewProp_InType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_AddBone_Statics::NewProp_InType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_AddBone_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_AddBone_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Adds a new single bone\n" },
		{ "CPP_Default_InParentName", "None" },
		{ "CPP_Default_InType", "User" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Adds a new single bone" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_AddBone_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "AddBone", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventAddBone_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_AddBone_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_AddBone_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_AddBone_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_AddBone_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_AddBone()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_AddBone_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl_Statics
	{
		struct ControlRigHierarchyModifier_eventAddControl_Parms
		{
			FName InNewName;
			ERigControlType InControlType;
			FName InParentName;
			FName InSpaceName;
			FName InGizmoName;
			FLinearColor InGizmoColor;
			FRigElementKey ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InNewName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_InNewName;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InControlType_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InControlType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InParentName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_InParentName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InSpaceName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_InSpaceName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InGizmoName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_InGizmoName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InGizmoColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InGizmoColor;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl_Statics::NewProp_InNewName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl_Statics::NewProp_InNewName = { "InNewName", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventAddControl_Parms, InNewName), METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl_Statics::NewProp_InNewName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl_Statics::NewProp_InNewName_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl_Statics::NewProp_InControlType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl_Statics::NewProp_InControlType = { "InControlType", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventAddControl_Parms, InControlType), Z_Construct_UEnum_ControlRig_ERigControlType, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl_Statics::NewProp_InParentName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl_Statics::NewProp_InParentName = { "InParentName", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventAddControl_Parms, InParentName), METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl_Statics::NewProp_InParentName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl_Statics::NewProp_InParentName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl_Statics::NewProp_InSpaceName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl_Statics::NewProp_InSpaceName = { "InSpaceName", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventAddControl_Parms, InSpaceName), METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl_Statics::NewProp_InSpaceName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl_Statics::NewProp_InSpaceName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl_Statics::NewProp_InGizmoName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl_Statics::NewProp_InGizmoName = { "InGizmoName", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventAddControl_Parms, InGizmoName), METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl_Statics::NewProp_InGizmoName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl_Statics::NewProp_InGizmoName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl_Statics::NewProp_InGizmoColor_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl_Statics::NewProp_InGizmoColor = { "InGizmoColor", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventAddControl_Parms, InGizmoColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl_Statics::NewProp_InGizmoColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl_Statics::NewProp_InGizmoColor_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventAddControl_Parms, ReturnValue), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl_Statics::NewProp_InNewName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl_Statics::NewProp_InControlType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl_Statics::NewProp_InControlType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl_Statics::NewProp_InParentName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl_Statics::NewProp_InSpaceName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl_Statics::NewProp_InGizmoName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl_Statics::NewProp_InGizmoColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Adds a new single control\n" },
		{ "CPP_Default_InControlType", "Transform" },
		{ "CPP_Default_InGizmoColor", "(R=1.000000,G=0.000000,B=0.000000,A=1.000000)" },
		{ "CPP_Default_InGizmoName", "Gizmo" },
		{ "CPP_Default_InParentName", "None" },
		{ "CPP_Default_InSpaceName", "None" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Adds a new single control" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "AddControl", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventAddControl_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_AddCurve_Statics
	{
		struct ControlRigHierarchyModifier_eventAddCurve_Parms
		{
			FName InNewName;
			float InValue;
			FRigElementKey ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InNewName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_InNewName;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InValue;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_AddCurve_Statics::NewProp_InNewName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_AddCurve_Statics::NewProp_InNewName = { "InNewName", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventAddCurve_Parms, InNewName), METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_AddCurve_Statics::NewProp_InNewName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_AddCurve_Statics::NewProp_InNewName_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_AddCurve_Statics::NewProp_InValue = { "InValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventAddCurve_Parms, InValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_AddCurve_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventAddCurve_Parms, ReturnValue), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_AddCurve_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_AddCurve_Statics::NewProp_InNewName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_AddCurve_Statics::NewProp_InValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_AddCurve_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_AddCurve_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Adds a new single curve\n" },
		{ "CPP_Default_InValue", "0.000000" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Adds a new single curve" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_AddCurve_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "AddCurve", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventAddCurve_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_AddCurve_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_AddCurve_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_AddCurve_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_AddCurve_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_AddCurve()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_AddCurve_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_AddSpace_Statics
	{
		struct ControlRigHierarchyModifier_eventAddSpace_Parms
		{
			FName InNewName;
			ERigSpaceType InSpaceType;
			FName InParentName;
			FRigElementKey ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InNewName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_InNewName;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InSpaceType_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InSpaceType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InParentName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_InParentName;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_AddSpace_Statics::NewProp_InNewName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_AddSpace_Statics::NewProp_InNewName = { "InNewName", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventAddSpace_Parms, InNewName), METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_AddSpace_Statics::NewProp_InNewName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_AddSpace_Statics::NewProp_InNewName_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_AddSpace_Statics::NewProp_InSpaceType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_AddSpace_Statics::NewProp_InSpaceType = { "InSpaceType", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventAddSpace_Parms, InSpaceType), Z_Construct_UEnum_ControlRig_ERigSpaceType, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_AddSpace_Statics::NewProp_InParentName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_AddSpace_Statics::NewProp_InParentName = { "InParentName", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventAddSpace_Parms, InParentName), METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_AddSpace_Statics::NewProp_InParentName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_AddSpace_Statics::NewProp_InParentName_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_AddSpace_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventAddSpace_Parms, ReturnValue), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_AddSpace_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_AddSpace_Statics::NewProp_InNewName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_AddSpace_Statics::NewProp_InSpaceType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_AddSpace_Statics::NewProp_InSpaceType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_AddSpace_Statics::NewProp_InParentName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_AddSpace_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_AddSpace_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Adds a new single space\n" },
		{ "CPP_Default_InParentName", "None" },
		{ "CPP_Default_InSpaceType", "Global" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Adds a new single space" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_AddSpace_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "AddSpace", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventAddSpace_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_AddSpace_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_AddSpace_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_AddSpace_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_AddSpace_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_AddSpace()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_AddSpace_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_ClearSelection_Statics
	{
		struct ControlRigHierarchyModifier_eventClearSelection_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UControlRigHierarchyModifier_ClearSelection_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((ControlRigHierarchyModifier_eventClearSelection_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_ClearSelection_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ControlRigHierarchyModifier_eventClearSelection_Parms), &Z_Construct_UFunction_UControlRigHierarchyModifier_ClearSelection_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_ClearSelection_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_ClearSelection_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_ClearSelection_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Clears the selection\n" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Clears the selection" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_ClearSelection_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "ClearSelection", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventClearSelection_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_ClearSelection_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_ClearSelection_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_ClearSelection_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_ClearSelection_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_ClearSelection()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_ClearSelection_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_ExportToText_Statics
	{
		struct ControlRigHierarchyModifier_eventExportToText_Parms
		{
			TArray<FRigElementKey> InElementsToExport;
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InElementsToExport_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InElementsToExport_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_InElementsToExport;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_ExportToText_Statics::NewProp_InElementsToExport_Inner = { "InElementsToExport", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_ExportToText_Statics::NewProp_InElementsToExport_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_ExportToText_Statics::NewProp_InElementsToExport = { "InElementsToExport", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventExportToText_Parms, InElementsToExport), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_ExportToText_Statics::NewProp_InElementsToExport_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_ExportToText_Statics::NewProp_InElementsToExport_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_ExportToText_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventExportToText_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_ExportToText_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_ExportToText_Statics::NewProp_InElementsToExport_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_ExportToText_Statics::NewProp_InElementsToExport,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_ExportToText_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_ExportToText_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Exports the elements provided to text (for copy & paste, import / export)\n" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Exports the elements provided to text (for copy & paste, import / export)" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_ExportToText_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "ExportToText", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventExportToText_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_ExportToText_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_ExportToText_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x74420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_ExportToText_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_ExportToText_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_ExportToText()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_ExportToText_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_GetBone_Statics
	{
		struct ControlRigHierarchyModifier_eventGetBone_Parms
		{
			FRigElementKey InKey;
			FRigBone ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InKey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InKey;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_GetBone_Statics::NewProp_InKey_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetBone_Statics::NewProp_InKey = { "InKey", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventGetBone_Parms, InKey), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_GetBone_Statics::NewProp_InKey_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetBone_Statics::NewProp_InKey_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetBone_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventGetBone_Parms, ReturnValue), Z_Construct_UScriptStruct_FRigBone, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_GetBone_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetBone_Statics::NewProp_InKey,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetBone_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_GetBone_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Returns a single bone from provided key\n" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Returns a single bone from provided key" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetBone_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "GetBone", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventGetBone_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_GetBone_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetBone_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_GetBone_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetBone_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_GetBone()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_GetBone_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_GetControl_Statics
	{
		struct ControlRigHierarchyModifier_eventGetControl_Parms
		{
			FRigElementKey InKey;
			FRigControl ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InKey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InKey;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_GetControl_Statics::NewProp_InKey_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetControl_Statics::NewProp_InKey = { "InKey", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventGetControl_Parms, InKey), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_GetControl_Statics::NewProp_InKey_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetControl_Statics::NewProp_InKey_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetControl_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventGetControl_Parms, ReturnValue), Z_Construct_UScriptStruct_FRigControl, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_GetControl_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetControl_Statics::NewProp_InKey,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetControl_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_GetControl_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Returns a single control from provided key\n" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Returns a single control from provided key" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetControl_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "GetControl", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventGetControl_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_GetControl_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetControl_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_GetControl_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetControl_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_GetControl()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_GetControl_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueBool_Statics
	{
		struct ControlRigHierarchyModifier_eventGetControlValueBool_Parms
		{
			FRigElementKey InKey;
			ERigControlValueType InValueType;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InKey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InKey;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InValueType_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InValueType;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueBool_Statics::NewProp_InKey_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueBool_Statics::NewProp_InKey = { "InKey", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventGetControlValueBool_Parms, InKey), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueBool_Statics::NewProp_InKey_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueBool_Statics::NewProp_InKey_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueBool_Statics::NewProp_InValueType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueBool_Statics::NewProp_InValueType = { "InValueType", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventGetControlValueBool_Parms, InValueType), Z_Construct_UEnum_ControlRig_ERigControlValueType, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueBool_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((ControlRigHierarchyModifier_eventGetControlValueBool_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueBool_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ControlRigHierarchyModifier_eventGetControlValueBool_Parms), &Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueBool_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueBool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueBool_Statics::NewProp_InKey,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueBool_Statics::NewProp_InValueType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueBool_Statics::NewProp_InValueType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueBool_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueBool_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Sets a control value\n" },
		{ "CPP_Default_InValueType", "Initial" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Sets a control value" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueBool_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "GetControlValueBool", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventGetControlValueBool_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueBool_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueBool_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueBool_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueBool_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueBool()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueBool_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueFloat_Statics
	{
		struct ControlRigHierarchyModifier_eventGetControlValueFloat_Parms
		{
			FRigElementKey InKey;
			ERigControlValueType InValueType;
			float ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InKey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InKey;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InValueType_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InValueType;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueFloat_Statics::NewProp_InKey_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueFloat_Statics::NewProp_InKey = { "InKey", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventGetControlValueFloat_Parms, InKey), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueFloat_Statics::NewProp_InKey_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueFloat_Statics::NewProp_InKey_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueFloat_Statics::NewProp_InValueType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueFloat_Statics::NewProp_InValueType = { "InValueType", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventGetControlValueFloat_Parms, InValueType), Z_Construct_UEnum_ControlRig_ERigControlValueType, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueFloat_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventGetControlValueFloat_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueFloat_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueFloat_Statics::NewProp_InKey,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueFloat_Statics::NewProp_InValueType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueFloat_Statics::NewProp_InValueType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueFloat_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueFloat_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Sets a control value\n" },
		{ "CPP_Default_InValueType", "Initial" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Sets a control value" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueFloat_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "GetControlValueFloat", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventGetControlValueFloat_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueFloat_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueFloat_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueFloat_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueFloat_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueFloat()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueFloat_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueInt_Statics
	{
		struct ControlRigHierarchyModifier_eventGetControlValueInt_Parms
		{
			FRigElementKey InKey;
			ERigControlValueType InValueType;
			int32 ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InKey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InKey;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InValueType_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InValueType;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueInt_Statics::NewProp_InKey_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueInt_Statics::NewProp_InKey = { "InKey", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventGetControlValueInt_Parms, InKey), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueInt_Statics::NewProp_InKey_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueInt_Statics::NewProp_InKey_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueInt_Statics::NewProp_InValueType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueInt_Statics::NewProp_InValueType = { "InValueType", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventGetControlValueInt_Parms, InValueType), Z_Construct_UEnum_ControlRig_ERigControlValueType, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueInt_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventGetControlValueInt_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueInt_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueInt_Statics::NewProp_InKey,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueInt_Statics::NewProp_InValueType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueInt_Statics::NewProp_InValueType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueInt_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueInt_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Sets a control value\n" },
		{ "CPP_Default_InValueType", "Initial" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Sets a control value" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueInt_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "GetControlValueInt", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventGetControlValueInt_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueInt_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueInt_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueInt_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueInt_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueInt()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueInt_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueRotator_Statics
	{
		struct ControlRigHierarchyModifier_eventGetControlValueRotator_Parms
		{
			FRigElementKey InKey;
			ERigControlValueType InValueType;
			FRotator ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InKey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InKey;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InValueType_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InValueType;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueRotator_Statics::NewProp_InKey_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueRotator_Statics::NewProp_InKey = { "InKey", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventGetControlValueRotator_Parms, InKey), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueRotator_Statics::NewProp_InKey_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueRotator_Statics::NewProp_InKey_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueRotator_Statics::NewProp_InValueType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueRotator_Statics::NewProp_InValueType = { "InValueType", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventGetControlValueRotator_Parms, InValueType), Z_Construct_UEnum_ControlRig_ERigControlValueType, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueRotator_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventGetControlValueRotator_Parms, ReturnValue), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueRotator_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueRotator_Statics::NewProp_InKey,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueRotator_Statics::NewProp_InValueType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueRotator_Statics::NewProp_InValueType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueRotator_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueRotator_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Sets a control value\n" },
		{ "CPP_Default_InValueType", "Initial" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Sets a control value" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueRotator_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "GetControlValueRotator", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventGetControlValueRotator_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueRotator_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueRotator_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueRotator_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueRotator_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueRotator()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueRotator_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueTransform_Statics
	{
		struct ControlRigHierarchyModifier_eventGetControlValueTransform_Parms
		{
			FRigElementKey InKey;
			ERigControlValueType InValueType;
			FTransform ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InKey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InKey;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InValueType_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InValueType;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueTransform_Statics::NewProp_InKey_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueTransform_Statics::NewProp_InKey = { "InKey", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventGetControlValueTransform_Parms, InKey), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueTransform_Statics::NewProp_InKey_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueTransform_Statics::NewProp_InKey_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueTransform_Statics::NewProp_InValueType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueTransform_Statics::NewProp_InValueType = { "InValueType", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventGetControlValueTransform_Parms, InValueType), Z_Construct_UEnum_ControlRig_ERigControlValueType, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueTransform_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventGetControlValueTransform_Parms, ReturnValue), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueTransform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueTransform_Statics::NewProp_InKey,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueTransform_Statics::NewProp_InValueType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueTransform_Statics::NewProp_InValueType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueTransform_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueTransform_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Sets a control value\n" },
		{ "CPP_Default_InValueType", "Initial" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Sets a control value" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueTransform_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "GetControlValueTransform", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventGetControlValueTransform_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueTransform_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueTransform_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueTransform_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueTransform_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueTransform()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueTransform_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector_Statics
	{
		struct ControlRigHierarchyModifier_eventGetControlValueVector_Parms
		{
			FRigElementKey InKey;
			ERigControlValueType InValueType;
			FVector ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InKey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InKey;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InValueType_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InValueType;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector_Statics::NewProp_InKey_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector_Statics::NewProp_InKey = { "InKey", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventGetControlValueVector_Parms, InKey), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector_Statics::NewProp_InKey_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector_Statics::NewProp_InKey_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector_Statics::NewProp_InValueType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector_Statics::NewProp_InValueType = { "InValueType", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventGetControlValueVector_Parms, InValueType), Z_Construct_UEnum_ControlRig_ERigControlValueType, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventGetControlValueVector_Parms, ReturnValue), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector_Statics::NewProp_InKey,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector_Statics::NewProp_InValueType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector_Statics::NewProp_InValueType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Sets a control value\n" },
		{ "CPP_Default_InValueType", "Initial" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Sets a control value" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "GetControlValueVector", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventGetControlValueVector_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector2D_Statics
	{
		struct ControlRigHierarchyModifier_eventGetControlValueVector2D_Parms
		{
			FRigElementKey InKey;
			ERigControlValueType InValueType;
			FVector2D ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InKey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InKey;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InValueType_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InValueType;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector2D_Statics::NewProp_InKey_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector2D_Statics::NewProp_InKey = { "InKey", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventGetControlValueVector2D_Parms, InKey), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector2D_Statics::NewProp_InKey_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector2D_Statics::NewProp_InKey_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector2D_Statics::NewProp_InValueType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector2D_Statics::NewProp_InValueType = { "InValueType", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventGetControlValueVector2D_Parms, InValueType), Z_Construct_UEnum_ControlRig_ERigControlValueType, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector2D_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventGetControlValueVector2D_Parms, ReturnValue), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector2D_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector2D_Statics::NewProp_InKey,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector2D_Statics::NewProp_InValueType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector2D_Statics::NewProp_InValueType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector2D_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector2D_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Sets a control value\n" },
		{ "CPP_Default_InValueType", "Initial" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Sets a control value" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector2D_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "GetControlValueVector2D", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventGetControlValueVector2D_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector2D_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector2D_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector2D_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector2D_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector2D()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector2D_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_GetCurve_Statics
	{
		struct ControlRigHierarchyModifier_eventGetCurve_Parms
		{
			FRigElementKey InKey;
			FRigCurve ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InKey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InKey;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_GetCurve_Statics::NewProp_InKey_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetCurve_Statics::NewProp_InKey = { "InKey", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventGetCurve_Parms, InKey), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_GetCurve_Statics::NewProp_InKey_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetCurve_Statics::NewProp_InKey_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetCurve_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventGetCurve_Parms, ReturnValue), Z_Construct_UScriptStruct_FRigCurve, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_GetCurve_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetCurve_Statics::NewProp_InKey,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetCurve_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_GetCurve_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Returns a single curve from provided key\n" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Returns a single curve from provided key" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetCurve_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "GetCurve", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventGetCurve_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_GetCurve_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetCurve_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_GetCurve_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetCurve_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_GetCurve()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_GetCurve_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_GetElements_Statics
	{
		struct ControlRigHierarchyModifier_eventGetElements_Parms
		{
			TArray<FRigElementKey> ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetElements_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetElements_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventGetElements_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_GetElements_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetElements_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetElements_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_GetElements_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Returns the keys of all elements within the hierarchy\n" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Returns the keys of all elements within the hierarchy" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetElements_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "GetElements", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventGetElements_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_GetElements_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetElements_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x74020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_GetElements_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetElements_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_GetElements()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_GetElements_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_GetGlobalTransform_Statics
	{
		struct ControlRigHierarchyModifier_eventGetGlobalTransform_Parms
		{
			FRigElementKey InKey;
			FTransform ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InKey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InKey;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_GetGlobalTransform_Statics::NewProp_InKey_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetGlobalTransform_Statics::NewProp_InKey = { "InKey", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventGetGlobalTransform_Parms, InKey), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_GetGlobalTransform_Statics::NewProp_InKey_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetGlobalTransform_Statics::NewProp_InKey_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetGlobalTransform_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventGetGlobalTransform_Parms, ReturnValue), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_GetGlobalTransform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetGlobalTransform_Statics::NewProp_InKey,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetGlobalTransform_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_GetGlobalTransform_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Returns the current global transform of a given element key\n" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Returns the current global transform of a given element key" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetGlobalTransform_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "GetGlobalTransform", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventGetGlobalTransform_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_GetGlobalTransform_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetGlobalTransform_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x74C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_GetGlobalTransform_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetGlobalTransform_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_GetGlobalTransform()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_GetGlobalTransform_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_GetInitialGlobalTransform_Statics
	{
		struct ControlRigHierarchyModifier_eventGetInitialGlobalTransform_Parms
		{
			FRigElementKey InKey;
			FTransform ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InKey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InKey;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_GetInitialGlobalTransform_Statics::NewProp_InKey_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetInitialGlobalTransform_Statics::NewProp_InKey = { "InKey", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventGetInitialGlobalTransform_Parms, InKey), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_GetInitialGlobalTransform_Statics::NewProp_InKey_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetInitialGlobalTransform_Statics::NewProp_InKey_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetInitialGlobalTransform_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventGetInitialGlobalTransform_Parms, ReturnValue), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_GetInitialGlobalTransform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetInitialGlobalTransform_Statics::NewProp_InKey,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetInitialGlobalTransform_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_GetInitialGlobalTransform_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Returns the initial global transform for a given element key\n" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Returns the initial global transform for a given element key" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetInitialGlobalTransform_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "GetInitialGlobalTransform", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventGetInitialGlobalTransform_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_GetInitialGlobalTransform_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetInitialGlobalTransform_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x74C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_GetInitialGlobalTransform_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetInitialGlobalTransform_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_GetInitialGlobalTransform()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_GetInitialGlobalTransform_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_GetInitialTransform_Statics
	{
		struct ControlRigHierarchyModifier_eventGetInitialTransform_Parms
		{
			FRigElementKey InKey;
			FTransform ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InKey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InKey;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_GetInitialTransform_Statics::NewProp_InKey_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetInitialTransform_Statics::NewProp_InKey = { "InKey", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventGetInitialTransform_Parms, InKey), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_GetInitialTransform_Statics::NewProp_InKey_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetInitialTransform_Statics::NewProp_InKey_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetInitialTransform_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventGetInitialTransform_Parms, ReturnValue), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_GetInitialTransform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetInitialTransform_Statics::NewProp_InKey,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetInitialTransform_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_GetInitialTransform_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Returns the initial transform for a given element key\n" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Returns the initial transform for a given element key" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetInitialTransform_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "GetInitialTransform", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventGetInitialTransform_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_GetInitialTransform_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetInitialTransform_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x74C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_GetInitialTransform_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetInitialTransform_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_GetInitialTransform()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_GetInitialTransform_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_GetLocalTransform_Statics
	{
		struct ControlRigHierarchyModifier_eventGetLocalTransform_Parms
		{
			FRigElementKey InKey;
			FTransform ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InKey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InKey;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_GetLocalTransform_Statics::NewProp_InKey_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetLocalTransform_Statics::NewProp_InKey = { "InKey", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventGetLocalTransform_Parms, InKey), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_GetLocalTransform_Statics::NewProp_InKey_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetLocalTransform_Statics::NewProp_InKey_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetLocalTransform_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventGetLocalTransform_Parms, ReturnValue), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_GetLocalTransform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetLocalTransform_Statics::NewProp_InKey,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetLocalTransform_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_GetLocalTransform_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Returns the current local transform of a given element key\n" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Returns the current local transform of a given element key" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetLocalTransform_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "GetLocalTransform", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventGetLocalTransform_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_GetLocalTransform_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetLocalTransform_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x74C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_GetLocalTransform_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetLocalTransform_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_GetLocalTransform()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_GetLocalTransform_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_GetSelection_Statics
	{
		struct ControlRigHierarchyModifier_eventGetSelection_Parms
		{
			TArray<FRigElementKey> ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetSelection_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetSelection_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventGetSelection_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_GetSelection_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetSelection_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetSelection_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_GetSelection_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Returns the keys of all selected elements within the hierarchy\n" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Returns the keys of all selected elements within the hierarchy" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetSelection_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "GetSelection", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventGetSelection_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_GetSelection_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetSelection_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x74020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_GetSelection_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetSelection_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_GetSelection()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_GetSelection_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_GetSpace_Statics
	{
		struct ControlRigHierarchyModifier_eventGetSpace_Parms
		{
			FRigElementKey InKey;
			FRigSpace ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InKey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InKey;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_GetSpace_Statics::NewProp_InKey_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetSpace_Statics::NewProp_InKey = { "InKey", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventGetSpace_Parms, InKey), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_GetSpace_Statics::NewProp_InKey_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetSpace_Statics::NewProp_InKey_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetSpace_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventGetSpace_Parms, ReturnValue), Z_Construct_UScriptStruct_FRigSpace, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_GetSpace_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetSpace_Statics::NewProp_InKey,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_GetSpace_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_GetSpace_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Returns a single space from provided key\n" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Returns a single space from provided key" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_GetSpace_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "GetSpace", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventGetSpace_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_GetSpace_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetSpace_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_GetSpace_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_GetSpace_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_GetSpace()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_GetSpace_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_ImportFromText_Statics
	{
		struct ControlRigHierarchyModifier_eventImportFromText_Parms
		{
			FString InContent;
			ERigHierarchyImportMode InImportMode;
			bool bSelectNewElements;
			TArray<FRigElementKey> ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InContent_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InContent;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InImportMode_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InImportMode;
		static void NewProp_bSelectNewElements_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSelectNewElements;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_ImportFromText_Statics::NewProp_InContent_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_ImportFromText_Statics::NewProp_InContent = { "InContent", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventImportFromText_Parms, InContent), METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_ImportFromText_Statics::NewProp_InContent_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_ImportFromText_Statics::NewProp_InContent_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_ImportFromText_Statics::NewProp_InImportMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_ImportFromText_Statics::NewProp_InImportMode = { "InImportMode", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventImportFromText_Parms, InImportMode), Z_Construct_UEnum_ControlRig_ERigHierarchyImportMode, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UControlRigHierarchyModifier_ImportFromText_Statics::NewProp_bSelectNewElements_SetBit(void* Obj)
	{
		((ControlRigHierarchyModifier_eventImportFromText_Parms*)Obj)->bSelectNewElements = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_ImportFromText_Statics::NewProp_bSelectNewElements = { "bSelectNewElements", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ControlRigHierarchyModifier_eventImportFromText_Parms), &Z_Construct_UFunction_UControlRigHierarchyModifier_ImportFromText_Statics::NewProp_bSelectNewElements_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_ImportFromText_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_ImportFromText_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventImportFromText_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_ImportFromText_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_ImportFromText_Statics::NewProp_InContent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_ImportFromText_Statics::NewProp_InImportMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_ImportFromText_Statics::NewProp_InImportMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_ImportFromText_Statics::NewProp_bSelectNewElements,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_ImportFromText_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_ImportFromText_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_ImportFromText_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Imports the content of the provided text and returns the keys created\n" },
		{ "CPP_Default_bSelectNewElements", "true" },
		{ "CPP_Default_InImportMode", "Append" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Imports the content of the provided text and returns the keys created" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_ImportFromText_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "ImportFromText", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventImportFromText_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_ImportFromText_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_ImportFromText_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_ImportFromText_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_ImportFromText_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_ImportFromText()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_ImportFromText_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_Initialize_Statics
	{
		struct ControlRigHierarchyModifier_eventInitialize_Parms
		{
			bool bResetTransforms;
		};
		static void NewProp_bResetTransforms_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bResetTransforms;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UControlRigHierarchyModifier_Initialize_Statics::NewProp_bResetTransforms_SetBit(void* Obj)
	{
		((ControlRigHierarchyModifier_eventInitialize_Parms*)Obj)->bResetTransforms = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_Initialize_Statics::NewProp_bResetTransforms = { "bResetTransforms", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ControlRigHierarchyModifier_eventInitialize_Parms), &Z_Construct_UFunction_UControlRigHierarchyModifier_Initialize_Statics::NewProp_bResetTransforms_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_Initialize_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_Initialize_Statics::NewProp_bResetTransforms,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_Initialize_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Initializes the rig, but calling reset on all elements\n" },
		{ "CPP_Default_bResetTransforms", "true" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Initializes the rig, but calling reset on all elements" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_Initialize_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "Initialize", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventInitialize_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_Initialize_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_Initialize_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_Initialize_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_Initialize_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_Initialize()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_Initialize_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_IsSelected_Statics
	{
		struct ControlRigHierarchyModifier_eventIsSelected_Parms
		{
			FRigElementKey InKey;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InKey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InKey;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_IsSelected_Statics::NewProp_InKey_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_IsSelected_Statics::NewProp_InKey = { "InKey", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventIsSelected_Parms, InKey), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_IsSelected_Statics::NewProp_InKey_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_IsSelected_Statics::NewProp_InKey_MetaData)) };
	void Z_Construct_UFunction_UControlRigHierarchyModifier_IsSelected_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((ControlRigHierarchyModifier_eventIsSelected_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_IsSelected_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ControlRigHierarchyModifier_eventIsSelected_Parms), &Z_Construct_UFunction_UControlRigHierarchyModifier_IsSelected_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_IsSelected_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_IsSelected_Statics::NewProp_InKey,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_IsSelected_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_IsSelected_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Returns true if a given element is currently selected\n" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Returns true if a given element is currently selected" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_IsSelected_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "IsSelected", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventIsSelected_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_IsSelected_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_IsSelected_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x74420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_IsSelected_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_IsSelected_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_IsSelected()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_IsSelected_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_RemoveElement_Statics
	{
		struct ControlRigHierarchyModifier_eventRemoveElement_Parms
		{
			FRigElementKey InElement;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InElement_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InElement;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_RemoveElement_Statics::NewProp_InElement_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_RemoveElement_Statics::NewProp_InElement = { "InElement", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventRemoveElement_Parms, InElement), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_RemoveElement_Statics::NewProp_InElement_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_RemoveElement_Statics::NewProp_InElement_MetaData)) };
	void Z_Construct_UFunction_UControlRigHierarchyModifier_RemoveElement_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((ControlRigHierarchyModifier_eventRemoveElement_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_RemoveElement_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ControlRigHierarchyModifier_eventRemoveElement_Parms), &Z_Construct_UFunction_UControlRigHierarchyModifier_RemoveElement_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_RemoveElement_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_RemoveElement_Statics::NewProp_InElement,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_RemoveElement_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_RemoveElement_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Removes a single element, returns true if successful\n" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Removes a single element, returns true if successful" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_RemoveElement_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "RemoveElement", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventRemoveElement_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_RemoveElement_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_RemoveElement_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_RemoveElement_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_RemoveElement_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_RemoveElement()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_RemoveElement_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_RenameElement_Statics
	{
		struct ControlRigHierarchyModifier_eventRenameElement_Parms
		{
			FRigElementKey InElement;
			FName InNewName;
			FRigElementKey ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InElement_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InElement;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InNewName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_InNewName;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_RenameElement_Statics::NewProp_InElement_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_RenameElement_Statics::NewProp_InElement = { "InElement", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventRenameElement_Parms, InElement), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_RenameElement_Statics::NewProp_InElement_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_RenameElement_Statics::NewProp_InElement_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_RenameElement_Statics::NewProp_InNewName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_RenameElement_Statics::NewProp_InNewName = { "InNewName", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventRenameElement_Parms, InNewName), METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_RenameElement_Statics::NewProp_InNewName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_RenameElement_Statics::NewProp_InNewName_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_RenameElement_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventRenameElement_Parms, ReturnValue), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_RenameElement_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_RenameElement_Statics::NewProp_InElement,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_RenameElement_Statics::NewProp_InNewName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_RenameElement_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_RenameElement_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Renames an existing element and returns the new element key\n" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Renames an existing element and returns the new element key" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_RenameElement_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "RenameElement", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventRenameElement_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_RenameElement_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_RenameElement_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_RenameElement_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_RenameElement_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_RenameElement()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_RenameElement_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_ReparentElement_Statics
	{
		struct ControlRigHierarchyModifier_eventReparentElement_Parms
		{
			FRigElementKey InElement;
			FRigElementKey InNewParent;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InElement_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InElement;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InNewParent_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InNewParent;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_ReparentElement_Statics::NewProp_InElement_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_ReparentElement_Statics::NewProp_InElement = { "InElement", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventReparentElement_Parms, InElement), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_ReparentElement_Statics::NewProp_InElement_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_ReparentElement_Statics::NewProp_InElement_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_ReparentElement_Statics::NewProp_InNewParent_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_ReparentElement_Statics::NewProp_InNewParent = { "InNewParent", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventReparentElement_Parms, InNewParent), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_ReparentElement_Statics::NewProp_InNewParent_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_ReparentElement_Statics::NewProp_InNewParent_MetaData)) };
	void Z_Construct_UFunction_UControlRigHierarchyModifier_ReparentElement_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((ControlRigHierarchyModifier_eventReparentElement_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_ReparentElement_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ControlRigHierarchyModifier_eventReparentElement_Parms), &Z_Construct_UFunction_UControlRigHierarchyModifier_ReparentElement_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_ReparentElement_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_ReparentElement_Statics::NewProp_InElement,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_ReparentElement_Statics::NewProp_InNewParent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_ReparentElement_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_ReparentElement_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Reparents an element to another element, returns true if successful\n" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Reparents an element to another element, returns true if successful" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_ReparentElement_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "ReparentElement", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventReparentElement_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_ReparentElement_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_ReparentElement_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_ReparentElement_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_ReparentElement_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_ReparentElement()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_ReparentElement_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_Reset_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_Reset_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Removes all elements of the hierarchy\n" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Removes all elements of the hierarchy" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_Reset_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "Reset", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_Reset_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_Reset_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_Reset()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_Reset_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_ResetTransforms_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_ResetTransforms_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Resets the transforms on all elements of the hierarchy\n" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Resets the transforms on all elements of the hierarchy" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_ResetTransforms_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "ResetTransforms", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_ResetTransforms_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_ResetTransforms_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_ResetTransforms()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_ResetTransforms_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_Select_Statics
	{
		struct ControlRigHierarchyModifier_eventSelect_Parms
		{
			FRigElementKey InKey;
			bool bSelect;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InKey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InKey;
		static void NewProp_bSelect_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSelect;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_Select_Statics::NewProp_InKey_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_Select_Statics::NewProp_InKey = { "InKey", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventSelect_Parms, InKey), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_Select_Statics::NewProp_InKey_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_Select_Statics::NewProp_InKey_MetaData)) };
	void Z_Construct_UFunction_UControlRigHierarchyModifier_Select_Statics::NewProp_bSelect_SetBit(void* Obj)
	{
		((ControlRigHierarchyModifier_eventSelect_Parms*)Obj)->bSelect = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_Select_Statics::NewProp_bSelect = { "bSelect", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ControlRigHierarchyModifier_eventSelect_Parms), &Z_Construct_UFunction_UControlRigHierarchyModifier_Select_Statics::NewProp_bSelect_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UControlRigHierarchyModifier_Select_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((ControlRigHierarchyModifier_eventSelect_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_Select_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ControlRigHierarchyModifier_eventSelect_Parms), &Z_Construct_UFunction_UControlRigHierarchyModifier_Select_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_Select_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_Select_Statics::NewProp_InKey,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_Select_Statics::NewProp_bSelect,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_Select_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_Select_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Selects or deselects a given element\n" },
		{ "CPP_Default_bSelect", "true" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Selects or deselects a given element" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_Select_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "Select", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventSelect_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_Select_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_Select_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_Select_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_Select_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_Select()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_Select_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_SetBone_Statics
	{
		struct ControlRigHierarchyModifier_eventSetBone_Parms
		{
			FRigBone InElement;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InElement_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InElement;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_SetBone_Statics::NewProp_InElement_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetBone_Statics::NewProp_InElement = { "InElement", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventSetBone_Parms, InElement), Z_Construct_UScriptStruct_FRigBone, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_SetBone_Statics::NewProp_InElement_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetBone_Statics::NewProp_InElement_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_SetBone_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_SetBone_Statics::NewProp_InElement,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_SetBone_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Updates a single bone\n" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Updates a single bone" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetBone_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "SetBone", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventSetBone_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_SetBone_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetBone_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_SetBone_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetBone_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_SetBone()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_SetBone_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_SetControl_Statics
	{
		struct ControlRigHierarchyModifier_eventSetControl_Parms
		{
			FRigControl InElement;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InElement_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InElement;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_SetControl_Statics::NewProp_InElement_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetControl_Statics::NewProp_InElement = { "InElement", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventSetControl_Parms, InElement), Z_Construct_UScriptStruct_FRigControl, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_SetControl_Statics::NewProp_InElement_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetControl_Statics::NewProp_InElement_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_SetControl_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_SetControl_Statics::NewProp_InElement,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_SetControl_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Updates a single control\n" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Updates a single control" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetControl_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "SetControl", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventSetControl_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_SetControl_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetControl_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_SetControl_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetControl_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_SetControl()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_SetControl_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueBool_Statics
	{
		struct ControlRigHierarchyModifier_eventSetControlValueBool_Parms
		{
			FRigElementKey InKey;
			bool InValue;
			ERigControlValueType InValueType;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InKey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InKey;
		static void NewProp_InValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_InValue;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InValueType_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InValueType;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueBool_Statics::NewProp_InKey_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueBool_Statics::NewProp_InKey = { "InKey", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventSetControlValueBool_Parms, InKey), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueBool_Statics::NewProp_InKey_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueBool_Statics::NewProp_InKey_MetaData)) };
	void Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueBool_Statics::NewProp_InValue_SetBit(void* Obj)
	{
		((ControlRigHierarchyModifier_eventSetControlValueBool_Parms*)Obj)->InValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueBool_Statics::NewProp_InValue = { "InValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ControlRigHierarchyModifier_eventSetControlValueBool_Parms), &Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueBool_Statics::NewProp_InValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueBool_Statics::NewProp_InValueType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueBool_Statics::NewProp_InValueType = { "InValueType", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventSetControlValueBool_Parms, InValueType), Z_Construct_UEnum_ControlRig_ERigControlValueType, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueBool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueBool_Statics::NewProp_InKey,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueBool_Statics::NewProp_InValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueBool_Statics::NewProp_InValueType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueBool_Statics::NewProp_InValueType,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueBool_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Sets a control value\n" },
		{ "CPP_Default_InValueType", "Initial" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Sets a control value" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueBool_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "SetControlValueBool", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventSetControlValueBool_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueBool_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueBool_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueBool_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueBool_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueBool()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueBool_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueFloat_Statics
	{
		struct ControlRigHierarchyModifier_eventSetControlValueFloat_Parms
		{
			FRigElementKey InKey;
			float InValue;
			ERigControlValueType InValueType;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InKey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InKey;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InValue;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InValueType_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InValueType;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueFloat_Statics::NewProp_InKey_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueFloat_Statics::NewProp_InKey = { "InKey", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventSetControlValueFloat_Parms, InKey), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueFloat_Statics::NewProp_InKey_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueFloat_Statics::NewProp_InKey_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueFloat_Statics::NewProp_InValue = { "InValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventSetControlValueFloat_Parms, InValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueFloat_Statics::NewProp_InValueType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueFloat_Statics::NewProp_InValueType = { "InValueType", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventSetControlValueFloat_Parms, InValueType), Z_Construct_UEnum_ControlRig_ERigControlValueType, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueFloat_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueFloat_Statics::NewProp_InKey,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueFloat_Statics::NewProp_InValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueFloat_Statics::NewProp_InValueType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueFloat_Statics::NewProp_InValueType,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueFloat_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Sets a control value\n" },
		{ "CPP_Default_InValueType", "Initial" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Sets a control value" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueFloat_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "SetControlValueFloat", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventSetControlValueFloat_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueFloat_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueFloat_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueFloat_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueFloat_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueFloat()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueFloat_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueInt_Statics
	{
		struct ControlRigHierarchyModifier_eventSetControlValueInt_Parms
		{
			FRigElementKey InKey;
			int32 InValue;
			ERigControlValueType InValueType;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InKey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InKey;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_InValue;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InValueType_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InValueType;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueInt_Statics::NewProp_InKey_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueInt_Statics::NewProp_InKey = { "InKey", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventSetControlValueInt_Parms, InKey), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueInt_Statics::NewProp_InKey_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueInt_Statics::NewProp_InKey_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueInt_Statics::NewProp_InValue = { "InValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventSetControlValueInt_Parms, InValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueInt_Statics::NewProp_InValueType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueInt_Statics::NewProp_InValueType = { "InValueType", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventSetControlValueInt_Parms, InValueType), Z_Construct_UEnum_ControlRig_ERigControlValueType, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueInt_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueInt_Statics::NewProp_InKey,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueInt_Statics::NewProp_InValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueInt_Statics::NewProp_InValueType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueInt_Statics::NewProp_InValueType,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueInt_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Sets a control value\n" },
		{ "CPP_Default_InValueType", "Initial" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Sets a control value" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueInt_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "SetControlValueInt", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventSetControlValueInt_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueInt_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueInt_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueInt_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueInt_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueInt()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueInt_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueRotator_Statics
	{
		struct ControlRigHierarchyModifier_eventSetControlValueRotator_Parms
		{
			FRigElementKey InKey;
			FRotator InValue;
			ERigControlValueType InValueType;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InKey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InKey;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InValue;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InValueType_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InValueType;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueRotator_Statics::NewProp_InKey_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueRotator_Statics::NewProp_InKey = { "InKey", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventSetControlValueRotator_Parms, InKey), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueRotator_Statics::NewProp_InKey_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueRotator_Statics::NewProp_InKey_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueRotator_Statics::NewProp_InValue = { "InValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventSetControlValueRotator_Parms, InValue), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueRotator_Statics::NewProp_InValueType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueRotator_Statics::NewProp_InValueType = { "InValueType", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventSetControlValueRotator_Parms, InValueType), Z_Construct_UEnum_ControlRig_ERigControlValueType, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueRotator_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueRotator_Statics::NewProp_InKey,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueRotator_Statics::NewProp_InValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueRotator_Statics::NewProp_InValueType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueRotator_Statics::NewProp_InValueType,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueRotator_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Sets a control value\n" },
		{ "CPP_Default_InValueType", "Initial" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Sets a control value" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueRotator_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "SetControlValueRotator", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventSetControlValueRotator_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueRotator_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueRotator_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueRotator_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueRotator_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueRotator()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueRotator_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueTransform_Statics
	{
		struct ControlRigHierarchyModifier_eventSetControlValueTransform_Parms
		{
			FRigElementKey InKey;
			FTransform InValue;
			ERigControlValueType InValueType;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InKey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InKey;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InValue;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InValueType_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InValueType;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueTransform_Statics::NewProp_InKey_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueTransform_Statics::NewProp_InKey = { "InKey", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventSetControlValueTransform_Parms, InKey), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueTransform_Statics::NewProp_InKey_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueTransform_Statics::NewProp_InKey_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueTransform_Statics::NewProp_InValue = { "InValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventSetControlValueTransform_Parms, InValue), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueTransform_Statics::NewProp_InValueType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueTransform_Statics::NewProp_InValueType = { "InValueType", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventSetControlValueTransform_Parms, InValueType), Z_Construct_UEnum_ControlRig_ERigControlValueType, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueTransform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueTransform_Statics::NewProp_InKey,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueTransform_Statics::NewProp_InValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueTransform_Statics::NewProp_InValueType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueTransform_Statics::NewProp_InValueType,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueTransform_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Sets a control value\n" },
		{ "CPP_Default_InValueType", "Initial" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Sets a control value" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueTransform_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "SetControlValueTransform", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventSetControlValueTransform_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueTransform_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueTransform_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueTransform_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueTransform_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueTransform()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueTransform_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector_Statics
	{
		struct ControlRigHierarchyModifier_eventSetControlValueVector_Parms
		{
			FRigElementKey InKey;
			FVector InValue;
			ERigControlValueType InValueType;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InKey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InKey;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InValue;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InValueType_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InValueType;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector_Statics::NewProp_InKey_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector_Statics::NewProp_InKey = { "InKey", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventSetControlValueVector_Parms, InKey), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector_Statics::NewProp_InKey_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector_Statics::NewProp_InKey_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector_Statics::NewProp_InValue = { "InValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventSetControlValueVector_Parms, InValue), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector_Statics::NewProp_InValueType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector_Statics::NewProp_InValueType = { "InValueType", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventSetControlValueVector_Parms, InValueType), Z_Construct_UEnum_ControlRig_ERigControlValueType, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector_Statics::NewProp_InKey,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector_Statics::NewProp_InValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector_Statics::NewProp_InValueType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector_Statics::NewProp_InValueType,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Sets a control value\n" },
		{ "CPP_Default_InValueType", "Initial" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Sets a control value" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "SetControlValueVector", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventSetControlValueVector_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector2D_Statics
	{
		struct ControlRigHierarchyModifier_eventSetControlValueVector2D_Parms
		{
			FRigElementKey InKey;
			FVector2D InValue;
			ERigControlValueType InValueType;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InKey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InKey;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InValue;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InValueType_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InValueType;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector2D_Statics::NewProp_InKey_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector2D_Statics::NewProp_InKey = { "InKey", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventSetControlValueVector2D_Parms, InKey), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector2D_Statics::NewProp_InKey_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector2D_Statics::NewProp_InKey_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector2D_Statics::NewProp_InValue = { "InValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventSetControlValueVector2D_Parms, InValue), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector2D_Statics::NewProp_InValueType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector2D_Statics::NewProp_InValueType = { "InValueType", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventSetControlValueVector2D_Parms, InValueType), Z_Construct_UEnum_ControlRig_ERigControlValueType, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector2D_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector2D_Statics::NewProp_InKey,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector2D_Statics::NewProp_InValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector2D_Statics::NewProp_InValueType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector2D_Statics::NewProp_InValueType,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector2D_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Sets a control value\n" },
		{ "CPP_Default_InValueType", "Initial" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Sets a control value" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector2D_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "SetControlValueVector2D", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventSetControlValueVector2D_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector2D_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector2D_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector2D_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector2D_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector2D()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector2D_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_SetCurve_Statics
	{
		struct ControlRigHierarchyModifier_eventSetCurve_Parms
		{
			FRigCurve InElement;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InElement_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InElement;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_SetCurve_Statics::NewProp_InElement_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetCurve_Statics::NewProp_InElement = { "InElement", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventSetCurve_Parms, InElement), Z_Construct_UScriptStruct_FRigCurve, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_SetCurve_Statics::NewProp_InElement_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetCurve_Statics::NewProp_InElement_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_SetCurve_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_SetCurve_Statics::NewProp_InElement,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_SetCurve_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Updates a single curve\n" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Updates a single curve" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetCurve_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "SetCurve", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventSetCurve_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_SetCurve_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetCurve_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_SetCurve_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetCurve_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_SetCurve()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_SetCurve_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_SetGlobalTransform_Statics
	{
		struct ControlRigHierarchyModifier_eventSetGlobalTransform_Parms
		{
			FRigElementKey InKey;
			FTransform InTransform;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InKey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InKey;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InTransform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InTransform;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_SetGlobalTransform_Statics::NewProp_InKey_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetGlobalTransform_Statics::NewProp_InKey = { "InKey", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventSetGlobalTransform_Parms, InKey), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_SetGlobalTransform_Statics::NewProp_InKey_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetGlobalTransform_Statics::NewProp_InKey_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_SetGlobalTransform_Statics::NewProp_InTransform_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetGlobalTransform_Statics::NewProp_InTransform = { "InTransform", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventSetGlobalTransform_Parms, InTransform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_SetGlobalTransform_Statics::NewProp_InTransform_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetGlobalTransform_Statics::NewProp_InTransform_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_SetGlobalTransform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_SetGlobalTransform_Statics::NewProp_InKey,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_SetGlobalTransform_Statics::NewProp_InTransform,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_SetGlobalTransform_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Sets the current global transform of a given element key\n" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Sets the current global transform of a given element key" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetGlobalTransform_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "SetGlobalTransform", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventSetGlobalTransform_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_SetGlobalTransform_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetGlobalTransform_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_SetGlobalTransform_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetGlobalTransform_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_SetGlobalTransform()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_SetGlobalTransform_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialGlobalTransform_Statics
	{
		struct ControlRigHierarchyModifier_eventSetInitialGlobalTransform_Parms
		{
			FRigElementKey InKey;
			FTransform InTransform;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InKey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InKey;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InTransform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InTransform;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialGlobalTransform_Statics::NewProp_InKey_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialGlobalTransform_Statics::NewProp_InKey = { "InKey", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventSetInitialGlobalTransform_Parms, InKey), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialGlobalTransform_Statics::NewProp_InKey_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialGlobalTransform_Statics::NewProp_InKey_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialGlobalTransform_Statics::NewProp_InTransform_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialGlobalTransform_Statics::NewProp_InTransform = { "InTransform", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventSetInitialGlobalTransform_Parms, InTransform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialGlobalTransform_Statics::NewProp_InTransform_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialGlobalTransform_Statics::NewProp_InTransform_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialGlobalTransform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialGlobalTransform_Statics::NewProp_InKey,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialGlobalTransform_Statics::NewProp_InTransform,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialGlobalTransform_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Sets the initial global transform for a given element key\n" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Sets the initial global transform for a given element key" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialGlobalTransform_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "SetInitialGlobalTransform", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventSetInitialGlobalTransform_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialGlobalTransform_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialGlobalTransform_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialGlobalTransform_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialGlobalTransform_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialGlobalTransform()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialGlobalTransform_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialTransform_Statics
	{
		struct ControlRigHierarchyModifier_eventSetInitialTransform_Parms
		{
			FRigElementKey InKey;
			FTransform InTransform;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InKey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InKey;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InTransform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InTransform;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialTransform_Statics::NewProp_InKey_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialTransform_Statics::NewProp_InKey = { "InKey", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventSetInitialTransform_Parms, InKey), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialTransform_Statics::NewProp_InKey_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialTransform_Statics::NewProp_InKey_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialTransform_Statics::NewProp_InTransform_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialTransform_Statics::NewProp_InTransform = { "InTransform", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventSetInitialTransform_Parms, InTransform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialTransform_Statics::NewProp_InTransform_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialTransform_Statics::NewProp_InTransform_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialTransform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialTransform_Statics::NewProp_InKey,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialTransform_Statics::NewProp_InTransform,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialTransform_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Sets the initial transform for a given element key\n" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Sets the initial transform for a given element key" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialTransform_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "SetInitialTransform", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventSetInitialTransform_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialTransform_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialTransform_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialTransform_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialTransform_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialTransform()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialTransform_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_SetLocalTransform_Statics
	{
		struct ControlRigHierarchyModifier_eventSetLocalTransform_Parms
		{
			FRigElementKey InKey;
			FTransform InTransform;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InKey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InKey;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InTransform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InTransform;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_SetLocalTransform_Statics::NewProp_InKey_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetLocalTransform_Statics::NewProp_InKey = { "InKey", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventSetLocalTransform_Parms, InKey), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_SetLocalTransform_Statics::NewProp_InKey_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetLocalTransform_Statics::NewProp_InKey_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_SetLocalTransform_Statics::NewProp_InTransform_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetLocalTransform_Statics::NewProp_InTransform = { "InTransform", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventSetLocalTransform_Parms, InTransform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_SetLocalTransform_Statics::NewProp_InTransform_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetLocalTransform_Statics::NewProp_InTransform_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_SetLocalTransform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_SetLocalTransform_Statics::NewProp_InKey,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_SetLocalTransform_Statics::NewProp_InTransform,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_SetLocalTransform_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Sets the current local transform of a given element key\n" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Sets the current local transform of a given element key" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetLocalTransform_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "SetLocalTransform", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventSetLocalTransform_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_SetLocalTransform_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetLocalTransform_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_SetLocalTransform_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetLocalTransform_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_SetLocalTransform()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_SetLocalTransform_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigHierarchyModifier_SetSpace_Statics
	{
		struct ControlRigHierarchyModifier_eventSetSpace_Parms
		{
			FRigSpace InElement;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InElement_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InElement;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_SetSpace_Statics::NewProp_InElement_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetSpace_Statics::NewProp_InElement = { "InElement", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigHierarchyModifier_eventSetSpace_Parms, InElement), Z_Construct_UScriptStruct_FRigSpace, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_SetSpace_Statics::NewProp_InElement_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetSpace_Statics::NewProp_InElement_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigHierarchyModifier_SetSpace_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigHierarchyModifier_SetSpace_Statics::NewProp_InElement,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigHierarchyModifier_SetSpace_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "// Updates a single space\n" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
		{ "ToolTip", "Updates a single space" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigHierarchyModifier_SetSpace_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigHierarchyModifier, nullptr, "SetSpace", nullptr, nullptr, sizeof(ControlRigHierarchyModifier_eventSetSpace_Parms), Z_Construct_UFunction_UControlRigHierarchyModifier_SetSpace_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetSpace_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigHierarchyModifier_SetSpace_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigHierarchyModifier_SetSpace_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigHierarchyModifier_SetSpace()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigHierarchyModifier_SetSpace_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
	UClass* Z_Construct_UClass_UControlRigHierarchyModifier_NoRegister()
	{
		return UControlRigHierarchyModifier::StaticClass();
	}
	struct Z_Construct_UClass_UControlRigHierarchyModifier_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_EDITOR
		static const FClassFunctionLinkInfo FuncInfo[];
#endif //WITH_EDITOR
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UControlRigHierarchyModifier_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRigDeveloper,
	};
#if WITH_EDITOR
	const FClassFunctionLinkInfo Z_Construct_UClass_UControlRigHierarchyModifier_Statics::FuncInfo[] = {
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_AddBone, "AddBone" }, // 3835117279
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_AddControl, "AddControl" }, // 4144218688
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_AddCurve, "AddCurve" }, // 3680719379
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_AddSpace, "AddSpace" }, // 3579005393
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_ClearSelection, "ClearSelection" }, // 1022255835
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_ExportToText, "ExportToText" }, // 492001878
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_GetBone, "GetBone" }, // 900236316
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_GetControl, "GetControl" }, // 2076999607
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueBool, "GetControlValueBool" }, // 1494957657
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueFloat, "GetControlValueFloat" }, // 4158723074
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueInt, "GetControlValueInt" }, // 1960784572
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueRotator, "GetControlValueRotator" }, // 1857445039
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueTransform, "GetControlValueTransform" }, // 251147439
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector, "GetControlValueVector" }, // 2581240938
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_GetControlValueVector2D, "GetControlValueVector2D" }, // 4237451900
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_GetCurve, "GetCurve" }, // 3539279738
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_GetElements, "GetElements" }, // 1221795720
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_GetGlobalTransform, "GetGlobalTransform" }, // 1726919127
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_GetInitialGlobalTransform, "GetInitialGlobalTransform" }, // 574241602
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_GetInitialTransform, "GetInitialTransform" }, // 913624675
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_GetLocalTransform, "GetLocalTransform" }, // 612338699
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_GetSelection, "GetSelection" }, // 3954146446
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_GetSpace, "GetSpace" }, // 1892163146
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_ImportFromText, "ImportFromText" }, // 3119020423
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_Initialize, "Initialize" }, // 1039772055
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_IsSelected, "IsSelected" }, // 4115317856
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_RemoveElement, "RemoveElement" }, // 2506221241
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_RenameElement, "RenameElement" }, // 963362982
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_ReparentElement, "ReparentElement" }, // 1994312411
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_Reset, "Reset" }, // 965233896
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_ResetTransforms, "ResetTransforms" }, // 1237889986
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_Select, "Select" }, // 2393788134
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_SetBone, "SetBone" }, // 2181208680
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_SetControl, "SetControl" }, // 3128364924
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueBool, "SetControlValueBool" }, // 2636519452
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueFloat, "SetControlValueFloat" }, // 643797877
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueInt, "SetControlValueInt" }, // 2562992206
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueRotator, "SetControlValueRotator" }, // 1529225406
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueTransform, "SetControlValueTransform" }, // 3143753129
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector, "SetControlValueVector" }, // 3864568269
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_SetControlValueVector2D, "SetControlValueVector2D" }, // 2572696359
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_SetCurve, "SetCurve" }, // 2782642371
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_SetGlobalTransform, "SetGlobalTransform" }, // 2212211043
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialGlobalTransform, "SetInitialGlobalTransform" }, // 3326925071
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_SetInitialTransform, "SetInitialTransform" }, // 3809995282
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_SetLocalTransform, "SetLocalTransform" }, // 3075308765
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigHierarchyModifier_SetSpace, "SetSpace" }, // 1428455320
#endif //WITH_EDITOR
	};
#endif //WITH_EDITOR
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigHierarchyModifier_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "ControlRigHierarchyModifier.h" },
		{ "ModuleRelativePath", "Public/ControlRigHierarchyModifier.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UControlRigHierarchyModifier_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UControlRigHierarchyModifier>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UControlRigHierarchyModifier_Statics::ClassParams = {
		&UControlRigHierarchyModifier::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		IF_WITH_EDITOR(FuncInfo, nullptr),
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		IF_WITH_EDITOR(UE_ARRAY_COUNT(FuncInfo), 0),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UControlRigHierarchyModifier_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigHierarchyModifier_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UControlRigHierarchyModifier()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UControlRigHierarchyModifier_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UControlRigHierarchyModifier, 2448542525);
	template<> CONTROLRIGDEVELOPER_API UClass* StaticClass<UControlRigHierarchyModifier>()
	{
		return UControlRigHierarchyModifier::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UControlRigHierarchyModifier(Z_Construct_UClass_UControlRigHierarchyModifier, &UControlRigHierarchyModifier::StaticClass, TEXT("/Script/ControlRigDeveloper"), TEXT("UControlRigHierarchyModifier"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UControlRigHierarchyModifier);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
