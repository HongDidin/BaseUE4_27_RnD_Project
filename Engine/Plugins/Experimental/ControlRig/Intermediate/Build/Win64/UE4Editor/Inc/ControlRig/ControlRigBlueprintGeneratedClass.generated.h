// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_ControlRigBlueprintGeneratedClass_generated_h
#error "ControlRigBlueprintGeneratedClass.generated.h already included, missing '#pragma once' in ControlRigBlueprintGeneratedClass.h"
#endif
#define CONTROLRIG_ControlRigBlueprintGeneratedClass_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigBlueprintGeneratedClass_h_15_SPARSE_DATA
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigBlueprintGeneratedClass_h_15_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigBlueprintGeneratedClass_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigBlueprintGeneratedClass_h_15_ARCHIVESERIALIZER \
	DECLARE_FSTRUCTUREDARCHIVE_SERIALIZER(UControlRigBlueprintGeneratedClass, NO_API)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigBlueprintGeneratedClass_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUControlRigBlueprintGeneratedClass(); \
	friend struct Z_Construct_UClass_UControlRigBlueprintGeneratedClass_Statics; \
public: \
	DECLARE_CLASS(UControlRigBlueprintGeneratedClass, UBlueprintGeneratedClass, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRig"), NO_API) \
	DECLARE_SERIALIZER(UControlRigBlueprintGeneratedClass) \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigBlueprintGeneratedClass_h_15_ARCHIVESERIALIZER


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigBlueprintGeneratedClass_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUControlRigBlueprintGeneratedClass(); \
	friend struct Z_Construct_UClass_UControlRigBlueprintGeneratedClass_Statics; \
public: \
	DECLARE_CLASS(UControlRigBlueprintGeneratedClass, UBlueprintGeneratedClass, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRig"), NO_API) \
	DECLARE_SERIALIZER(UControlRigBlueprintGeneratedClass) \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigBlueprintGeneratedClass_h_15_ARCHIVESERIALIZER


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigBlueprintGeneratedClass_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UControlRigBlueprintGeneratedClass(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigBlueprintGeneratedClass) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigBlueprintGeneratedClass); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigBlueprintGeneratedClass); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigBlueprintGeneratedClass(UControlRigBlueprintGeneratedClass&&); \
	NO_API UControlRigBlueprintGeneratedClass(const UControlRigBlueprintGeneratedClass&); \
public:


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigBlueprintGeneratedClass_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UControlRigBlueprintGeneratedClass(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigBlueprintGeneratedClass(UControlRigBlueprintGeneratedClass&&); \
	NO_API UControlRigBlueprintGeneratedClass(const UControlRigBlueprintGeneratedClass&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigBlueprintGeneratedClass); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigBlueprintGeneratedClass); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigBlueprintGeneratedClass)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigBlueprintGeneratedClass_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigBlueprintGeneratedClass_h_12_PROLOG
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigBlueprintGeneratedClass_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigBlueprintGeneratedClass_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigBlueprintGeneratedClass_h_15_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigBlueprintGeneratedClass_h_15_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigBlueprintGeneratedClass_h_15_INCLASS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigBlueprintGeneratedClass_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigBlueprintGeneratedClass_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigBlueprintGeneratedClass_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigBlueprintGeneratedClass_h_15_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigBlueprintGeneratedClass_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigBlueprintGeneratedClass_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigBlueprintGeneratedClass_h_15_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ControlRigBlueprintGeneratedClass."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONTROLRIG_API UClass* StaticClass<class UControlRigBlueprintGeneratedClass>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRigBlueprintGeneratedClass_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
