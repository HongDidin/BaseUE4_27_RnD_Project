// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Public/Units/Control/RigUnit_Control_StaticMesh.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_Control_StaticMesh() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_Control_StaticMesh();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_Control();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_Control_StaticMesh>() == std::is_polymorphic<FRigUnit_Control>(), "USTRUCT FRigUnit_Control_StaticMesh cannot be polymorphic unless super FRigUnit_Control is polymorphic");

class UScriptStruct* FRigUnit_Control_StaticMesh::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Control_StaticMesh_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_Control_StaticMesh, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_Control_StaticMesh"), sizeof(FRigUnit_Control_StaticMesh), Get_Z_Construct_UScriptStruct_FRigUnit_Control_StaticMesh_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_Control_StaticMesh::Execute"), &FRigUnit_Control_StaticMesh::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_Control_StaticMesh>()
{
	return FRigUnit_Control_StaticMesh::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_Control_StaticMesh(FRigUnit_Control_StaticMesh::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_Control_StaticMesh"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_Control_StaticMesh
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_Control_StaticMesh()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_Control_StaticMesh>(FName(TEXT("RigUnit_Control_StaticMesh")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_Control_StaticMesh;
	struct Z_Construct_UScriptStruct_FRigUnit_Control_StaticMesh_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshTransform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MeshTransform;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_Control_StaticMesh_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Controls" },
		{ "Comment", "/** A control unit used to drive a transform from an external source */" },
		{ "DisplayName", "Static Mesh Control" },
		{ "ModuleRelativePath", "Public/Units/Control/RigUnit_Control_StaticMesh.h" },
		{ "ShowVariableNameInTitle", "" },
		{ "ToolTip", "A control unit used to drive a transform from an external source" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_Control_StaticMesh_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_Control_StaticMesh>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_Control_StaticMesh_Statics::NewProp_MeshTransform_MetaData[] = {
		{ "Comment", "/** The the transform the mesh will be rendered with (applied on top of the control's transform in the viewport) */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Public/Units/Control/RigUnit_Control_StaticMesh.h" },
		{ "ToolTip", "The the transform the mesh will be rendered with (applied on top of the control's transform in the viewport)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_Control_StaticMesh_Statics::NewProp_MeshTransform = { "MeshTransform", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_Control_StaticMesh, MeshTransform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_Control_StaticMesh_Statics::NewProp_MeshTransform_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_Control_StaticMesh_Statics::NewProp_MeshTransform_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_Control_StaticMesh_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_Control_StaticMesh_Statics::NewProp_MeshTransform,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_Control_StaticMesh_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_Control,
		&NewStructOps,
		"RigUnit_Control_StaticMesh",
		sizeof(FRigUnit_Control_StaticMesh),
		alignof(FRigUnit_Control_StaticMesh),
		Z_Construct_UScriptStruct_FRigUnit_Control_StaticMesh_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_Control_StaticMesh_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_Control_StaticMesh_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_Control_StaticMesh_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_Control_StaticMesh()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Control_StaticMesh_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_Control_StaticMesh"), sizeof(FRigUnit_Control_StaticMesh), Get_Z_Construct_UScriptStruct_FRigUnit_Control_StaticMesh_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_Control_StaticMesh_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Control_StaticMesh_Hash() { return 720454319U; }

void FRigUnit_Control_StaticMesh::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		MeshTransform,
		Transform,
		Base,
		InitTransform,
		Result,
		Filter,
		Context
	);
}

PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
