// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigUnit_CCDIK_generated_h
#error "RigUnit_CCDIK.generated.h already included, missing '#pragma once' in RigUnit_CCDIK.h"
#endif
#define CONTROLRIG_RigUnit_CCDIK_generated_h


#define FRigUnit_CCDIKPerItem_Execute() \
	void FRigUnit_CCDIKPerItem::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKeyCollection& Items, \
		const FTransform& EffectorTransform, \
		const float Precision, \
		const float Weight, \
		const int32 MaxIterations, \
		const bool bStartFromTail, \
		const float BaseRotationLimit, \
		const FRigVMFixedArray<FRigUnit_CCDIK_RotationLimitPerItem>& RotationLimits, \
		const bool bPropagateToChildren, \
		FRigUnit_CCDIK_WorkData& WorkData, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_CCDIK_h_181_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_CCDIKPerItem_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKeyCollection& Items, \
		const FTransform& EffectorTransform, \
		const float Precision, \
		const float Weight, \
		const int32 MaxIterations, \
		const bool bStartFromTail, \
		const float BaseRotationLimit, \
		const FRigVMFixedArray<FRigUnit_CCDIK_RotationLimitPerItem>& RotationLimits, \
		const bool bPropagateToChildren, \
		FRigUnit_CCDIK_WorkData& WorkData, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FRigElementKeyCollection& Items = *(FRigElementKeyCollection*)RigVMMemoryHandles[0].GetData(); \
		const FTransform& EffectorTransform = *(FTransform*)RigVMMemoryHandles[1].GetData(); \
		const float Precision = *(float*)RigVMMemoryHandles[2].GetData(); \
		const float Weight = *(float*)RigVMMemoryHandles[3].GetData(); \
		const int32 MaxIterations = *(int32*)RigVMMemoryHandles[4].GetData(); \
		const bool bStartFromTail = *(bool*)RigVMMemoryHandles[5].GetData(); \
		const float BaseRotationLimit = *(float*)RigVMMemoryHandles[6].GetData(); \
		FRigVMFixedArray<FRigUnit_CCDIK_RotationLimitPerItem> RotationLimits((FRigUnit_CCDIK_RotationLimitPerItem*)RigVMMemoryHandles[7].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[8].GetData())); \
		const bool bPropagateToChildren = *(bool*)RigVMMemoryHandles[9].GetData(); \
		FRigVMDynamicArray<FRigUnit_CCDIK_WorkData> WorkData_10_Array(*((FRigVMByteArray*)RigVMMemoryHandles[10].GetData(0, false))); \
		WorkData_10_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FRigUnit_CCDIK_WorkData& WorkData = WorkData_10_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[11].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Items, \
			EffectorTransform, \
			Precision, \
			Weight, \
			MaxIterations, \
			bStartFromTail, \
			BaseRotationLimit, \
			RotationLimits, \
			bPropagateToChildren, \
			WorkData, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnit_HighlevelBaseMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_CCDIKPerItem>();


#define FRigUnit_CCDIK_Execute() \
	void FRigUnit_CCDIK::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& StartBone, \
		const FName& EffectorBone, \
		const FTransform& EffectorTransform, \
		const float Precision, \
		const float Weight, \
		const int32 MaxIterations, \
		const bool bStartFromTail, \
		const float BaseRotationLimit, \
		const FRigVMFixedArray<FRigUnit_CCDIK_RotationLimit>& RotationLimits, \
		const bool bPropagateToChildren, \
		FRigUnit_CCDIK_WorkData& WorkData, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_CCDIK_h_91_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_CCDIK_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& StartBone, \
		const FName& EffectorBone, \
		const FTransform& EffectorTransform, \
		const float Precision, \
		const float Weight, \
		const int32 MaxIterations, \
		const bool bStartFromTail, \
		const float BaseRotationLimit, \
		const FRigVMFixedArray<FRigUnit_CCDIK_RotationLimit>& RotationLimits, \
		const bool bPropagateToChildren, \
		FRigUnit_CCDIK_WorkData& WorkData, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FName& StartBone = *(FName*)RigVMMemoryHandles[0].GetData(); \
		const FName& EffectorBone = *(FName*)RigVMMemoryHandles[1].GetData(); \
		const FTransform& EffectorTransform = *(FTransform*)RigVMMemoryHandles[2].GetData(); \
		const float Precision = *(float*)RigVMMemoryHandles[3].GetData(); \
		const float Weight = *(float*)RigVMMemoryHandles[4].GetData(); \
		const int32 MaxIterations = *(int32*)RigVMMemoryHandles[5].GetData(); \
		const bool bStartFromTail = *(bool*)RigVMMemoryHandles[6].GetData(); \
		const float BaseRotationLimit = *(float*)RigVMMemoryHandles[7].GetData(); \
		FRigVMFixedArray<FRigUnit_CCDIK_RotationLimit> RotationLimits((FRigUnit_CCDIK_RotationLimit*)RigVMMemoryHandles[8].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[9].GetData())); \
		const bool bPropagateToChildren = *(bool*)RigVMMemoryHandles[10].GetData(); \
		FRigVMDynamicArray<FRigUnit_CCDIK_WorkData> WorkData_11_Array(*((FRigVMByteArray*)RigVMMemoryHandles[11].GetData(0, false))); \
		WorkData_11_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FRigUnit_CCDIK_WorkData& WorkData = WorkData_11_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[12].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			StartBone, \
			EffectorBone, \
			EffectorTransform, \
			Precision, \
			Weight, \
			MaxIterations, \
			bStartFromTail, \
			BaseRotationLimit, \
			RotationLimits, \
			bPropagateToChildren, \
			WorkData, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnit_HighlevelBaseMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_CCDIK>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_CCDIK_h_60_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_CCDIK_WorkData_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_CCDIK_WorkData>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_CCDIK_h_36_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_CCDIK_RotationLimitPerItem_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_CCDIK_RotationLimitPerItem>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_CCDIK_h_12_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_CCDIK_RotationLimit_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_CCDIK_RotationLimit>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_CCDIK_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
