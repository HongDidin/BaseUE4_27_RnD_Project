// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_ControlRigSequencerAnimInstance_generated_h
#error "ControlRigSequencerAnimInstance.generated.h already included, missing '#pragma once' in ControlRigSequencerAnimInstance.h"
#endif
#define CONTROLRIG_ControlRigSequencerAnimInstance_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigSequencerAnimInstance_h_15_SPARSE_DATA
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigSequencerAnimInstance_h_15_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigSequencerAnimInstance_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigSequencerAnimInstance_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUControlRigSequencerAnimInstance(); \
	friend struct Z_Construct_UClass_UControlRigSequencerAnimInstance_Statics; \
public: \
	DECLARE_CLASS(UControlRigSequencerAnimInstance, UAnimSequencerInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ControlRig"), NO_API) \
	DECLARE_SERIALIZER(UControlRigSequencerAnimInstance)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigSequencerAnimInstance_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUControlRigSequencerAnimInstance(); \
	friend struct Z_Construct_UClass_UControlRigSequencerAnimInstance_Statics; \
public: \
	DECLARE_CLASS(UControlRigSequencerAnimInstance, UAnimSequencerInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ControlRig"), NO_API) \
	DECLARE_SERIALIZER(UControlRigSequencerAnimInstance)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigSequencerAnimInstance_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UControlRigSequencerAnimInstance(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigSequencerAnimInstance) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigSequencerAnimInstance); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigSequencerAnimInstance); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigSequencerAnimInstance(UControlRigSequencerAnimInstance&&); \
	NO_API UControlRigSequencerAnimInstance(const UControlRigSequencerAnimInstance&); \
public:


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigSequencerAnimInstance_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UControlRigSequencerAnimInstance(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigSequencerAnimInstance(UControlRigSequencerAnimInstance&&); \
	NO_API UControlRigSequencerAnimInstance(const UControlRigSequencerAnimInstance&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigSequencerAnimInstance); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigSequencerAnimInstance); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigSequencerAnimInstance)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigSequencerAnimInstance_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigSequencerAnimInstance_h_12_PROLOG
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigSequencerAnimInstance_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigSequencerAnimInstance_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigSequencerAnimInstance_h_15_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigSequencerAnimInstance_h_15_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigSequencerAnimInstance_h_15_INCLASS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigSequencerAnimInstance_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigSequencerAnimInstance_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigSequencerAnimInstance_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigSequencerAnimInstance_h_15_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigSequencerAnimInstance_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigSequencerAnimInstance_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigSequencerAnimInstance_h_15_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ControlRigSequencerAnimInstance."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONTROLRIG_API UClass* StaticClass<class UControlRigSequencerAnimInstance>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigSequencerAnimInstance_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
