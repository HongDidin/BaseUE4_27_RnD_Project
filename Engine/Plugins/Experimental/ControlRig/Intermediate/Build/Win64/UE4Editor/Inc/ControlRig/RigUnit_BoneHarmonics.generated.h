// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigUnit_BoneHarmonics_generated_h
#error "RigUnit_BoneHarmonics.generated.h already included, missing '#pragma once' in RigUnit_BoneHarmonics.h"
#endif
#define CONTROLRIG_RigUnit_BoneHarmonics_generated_h


#define FRigUnit_ItemHarmonics_Execute() \
	void FRigUnit_ItemHarmonics::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FRigUnit_Harmonics_TargetItem>& Targets, \
		const FVector& WaveSpeed, \
		const FVector& WaveFrequency, \
		const FVector& WaveAmplitude, \
		const FVector& WaveOffset, \
		const FVector& WaveNoise, \
		const EControlRigAnimEasingType WaveEase, \
		const float WaveMinimum, \
		const float WaveMaximum, \
		const EControlRigRotationOrder RotationOrder, \
		FRigUnit_BoneHarmonics_WorkData& WorkData, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Harmonics_RigUnit_BoneHarmonics_h_151_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_ItemHarmonics_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FRigUnit_Harmonics_TargetItem>& Targets, \
		const FVector& WaveSpeed, \
		const FVector& WaveFrequency, \
		const FVector& WaveAmplitude, \
		const FVector& WaveOffset, \
		const FVector& WaveNoise, \
		const EControlRigAnimEasingType WaveEase, \
		const float WaveMinimum, \
		const float WaveMaximum, \
		const EControlRigRotationOrder RotationOrder, \
		FRigUnit_BoneHarmonics_WorkData& WorkData, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		FRigVMFixedArray<FRigUnit_Harmonics_TargetItem> Targets((FRigUnit_Harmonics_TargetItem*)RigVMMemoryHandles[0].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[1].GetData())); \
		const FVector& WaveSpeed = *(FVector*)RigVMMemoryHandles[2].GetData(); \
		const FVector& WaveFrequency = *(FVector*)RigVMMemoryHandles[3].GetData(); \
		const FVector& WaveAmplitude = *(FVector*)RigVMMemoryHandles[4].GetData(); \
		const FVector& WaveOffset = *(FVector*)RigVMMemoryHandles[5].GetData(); \
		const FVector& WaveNoise = *(FVector*)RigVMMemoryHandles[6].GetData(); \
		EControlRigAnimEasingType WaveEase = (EControlRigAnimEasingType)*(uint8*)RigVMMemoryHandles[7].GetData(); \
		const float WaveMinimum = *(float*)RigVMMemoryHandles[8].GetData(); \
		const float WaveMaximum = *(float*)RigVMMemoryHandles[9].GetData(); \
		EControlRigRotationOrder RotationOrder = (EControlRigRotationOrder)*(uint8*)RigVMMemoryHandles[10].GetData(); \
		FRigVMDynamicArray<FRigUnit_BoneHarmonics_WorkData> WorkData_11_Array(*((FRigVMByteArray*)RigVMMemoryHandles[11].GetData(0, false))); \
		WorkData_11_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FRigUnit_BoneHarmonics_WorkData& WorkData = WorkData_11_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[12].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Targets, \
			WaveSpeed, \
			WaveFrequency, \
			WaveAmplitude, \
			WaveOffset, \
			WaveNoise, \
			WaveEase, \
			WaveMinimum, \
			WaveMaximum, \
			RotationOrder, \
			WorkData, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnit_HighlevelBaseMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_ItemHarmonics>();


#define FRigUnit_BoneHarmonics_Execute() \
	void FRigUnit_BoneHarmonics::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FRigUnit_BoneHarmonics_BoneTarget>& Bones, \
		const FVector& WaveSpeed, \
		const FVector& WaveFrequency, \
		const FVector& WaveAmplitude, \
		const FVector& WaveOffset, \
		const FVector& WaveNoise, \
		const EControlRigAnimEasingType WaveEase, \
		const float WaveMinimum, \
		const float WaveMaximum, \
		const EControlRigRotationOrder RotationOrder, \
		const bool bPropagateToChildren, \
		FRigUnit_BoneHarmonics_WorkData& WorkData, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Harmonics_RigUnit_BoneHarmonics_h_82_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_BoneHarmonics_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FRigUnit_BoneHarmonics_BoneTarget>& Bones, \
		const FVector& WaveSpeed, \
		const FVector& WaveFrequency, \
		const FVector& WaveAmplitude, \
		const FVector& WaveOffset, \
		const FVector& WaveNoise, \
		const EControlRigAnimEasingType WaveEase, \
		const float WaveMinimum, \
		const float WaveMaximum, \
		const EControlRigRotationOrder RotationOrder, \
		const bool bPropagateToChildren, \
		FRigUnit_BoneHarmonics_WorkData& WorkData, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		FRigVMFixedArray<FRigUnit_BoneHarmonics_BoneTarget> Bones((FRigUnit_BoneHarmonics_BoneTarget*)RigVMMemoryHandles[0].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[1].GetData())); \
		const FVector& WaveSpeed = *(FVector*)RigVMMemoryHandles[2].GetData(); \
		const FVector& WaveFrequency = *(FVector*)RigVMMemoryHandles[3].GetData(); \
		const FVector& WaveAmplitude = *(FVector*)RigVMMemoryHandles[4].GetData(); \
		const FVector& WaveOffset = *(FVector*)RigVMMemoryHandles[5].GetData(); \
		const FVector& WaveNoise = *(FVector*)RigVMMemoryHandles[6].GetData(); \
		EControlRigAnimEasingType WaveEase = (EControlRigAnimEasingType)*(uint8*)RigVMMemoryHandles[7].GetData(); \
		const float WaveMinimum = *(float*)RigVMMemoryHandles[8].GetData(); \
		const float WaveMaximum = *(float*)RigVMMemoryHandles[9].GetData(); \
		EControlRigRotationOrder RotationOrder = (EControlRigRotationOrder)*(uint8*)RigVMMemoryHandles[10].GetData(); \
		const bool bPropagateToChildren = *(bool*)RigVMMemoryHandles[11].GetData(); \
		FRigVMDynamicArray<FRigUnit_BoneHarmonics_WorkData> WorkData_12_Array(*((FRigVMByteArray*)RigVMMemoryHandles[12].GetData(0, false))); \
		WorkData_12_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FRigUnit_BoneHarmonics_WorkData& WorkData = WorkData_12_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[13].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Bones, \
			WaveSpeed, \
			WaveFrequency, \
			WaveAmplitude, \
			WaveOffset, \
			WaveNoise, \
			WaveEase, \
			WaveMinimum, \
			WaveMaximum, \
			RotationOrder, \
			bPropagateToChildren, \
			WorkData, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnit_HighlevelBaseMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_BoneHarmonics>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Harmonics_RigUnit_BoneHarmonics_h_62_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_BoneHarmonics_WorkData_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_BoneHarmonics_WorkData>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Harmonics_RigUnit_BoneHarmonics_h_37_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_Harmonics_TargetItem_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_Harmonics_TargetItem>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Harmonics_RigUnit_BoneHarmonics_h_12_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_BoneHarmonics_BoneTarget_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_BoneHarmonics_BoneTarget>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Harmonics_RigUnit_BoneHarmonics_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
