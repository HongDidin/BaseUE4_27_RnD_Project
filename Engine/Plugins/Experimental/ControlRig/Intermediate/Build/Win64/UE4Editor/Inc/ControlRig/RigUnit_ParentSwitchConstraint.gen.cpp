// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Hierarchy/RigUnit_ParentSwitchConstraint.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_ParentSwitchConstraint() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnitMutable();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigElementKey();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigElementKeyCollection();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FCachedRigElement();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_ParentSwitchConstraint>() == std::is_polymorphic<FRigUnitMutable>(), "USTRUCT FRigUnit_ParentSwitchConstraint cannot be polymorphic unless super FRigUnitMutable is polymorphic");

class UScriptStruct* FRigUnit_ParentSwitchConstraint::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_ParentSwitchConstraint"), sizeof(FRigUnit_ParentSwitchConstraint), Get_Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_ParentSwitchConstraint::Execute"), &FRigUnit_ParentSwitchConstraint::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_ParentSwitchConstraint>()
{
	return FRigUnit_ParentSwitchConstraint::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_ParentSwitchConstraint(FRigUnit_ParentSwitchConstraint::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_ParentSwitchConstraint"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ParentSwitchConstraint
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ParentSwitchConstraint()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_ParentSwitchConstraint>(FName(TEXT("RigUnit_ParentSwitchConstraint")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ParentSwitchConstraint;
	struct Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Subject_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Subject;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParentIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ParentIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Parents_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Parents;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InitialGlobalTransform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InitialGlobalTransform;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Weight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Weight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Transform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Transform;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Switched_MetaData[];
#endif
		static void NewProp_Switched_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Switched;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedSubject_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CachedSubject;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedParent_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CachedParent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RelativeOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RelativeOffset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "/**\n * The Parent Switch Constraint is used to have an item follow one of multiple parents,\n * and allowing to switch between the parent in question.\n */" },
		{ "DisplayName", "Parent Switch Constraint" },
		{ "DocumentationPolicy", "Strict" },
		{ "Keywords", "SpaceSwitch" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_ParentSwitchConstraint.h" },
		{ "ToolTip", "The Parent Switch Constraint is used to have an item follow one of multiple parents,\nand allowing to switch between the parent in question." },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_ParentSwitchConstraint>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_Subject_MetaData[] = {
		{ "Comment", "/**\n\x09 * The subject to constrain\n\x09 */" },
		{ "ExpandByDefault", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_ParentSwitchConstraint.h" },
		{ "ToolTip", "The subject to constrain" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_Subject = { "Subject", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ParentSwitchConstraint, Subject), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_Subject_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_Subject_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_ParentIndex_MetaData[] = {
		{ "Comment", "/**\n\x09 * The parent index to use for constraining the subject\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_ParentSwitchConstraint.h" },
		{ "ToolTip", "The parent index to use for constraining the subject" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_ParentIndex = { "ParentIndex", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ParentSwitchConstraint, ParentIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_ParentIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_ParentIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_Parents_MetaData[] = {
		{ "Comment", "/**\n\x09 * The list of parents to constrain to\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_ParentSwitchConstraint.h" },
		{ "ToolTip", "The list of parents to constrain to" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_Parents = { "Parents", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ParentSwitchConstraint, Parents), Z_Construct_UScriptStruct_FRigElementKeyCollection, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_Parents_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_Parents_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_InitialGlobalTransform_MetaData[] = {
		{ "Comment", "/**\n\x09 * The initial global transform for the subject\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_ParentSwitchConstraint.h" },
		{ "ToolTip", "The initial global transform for the subject" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_InitialGlobalTransform = { "InitialGlobalTransform", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ParentSwitchConstraint, InitialGlobalTransform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_InitialGlobalTransform_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_InitialGlobalTransform_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_Weight_MetaData[] = {
		{ "Comment", "/**\n\x09 * The weight of the change - how much the change should be applied\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_ParentSwitchConstraint.h" },
		{ "ToolTip", "The weight of the change - how much the change should be applied" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_Weight = { "Weight", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ParentSwitchConstraint, Weight), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_Weight_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_Weight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_Transform_MetaData[] = {
		{ "Comment", "/**\n\x09 * The transform result (full without weighting)\n\x09 */" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_ParentSwitchConstraint.h" },
		{ "Output", "" },
		{ "ToolTip", "The transform result (full without weighting)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_Transform = { "Transform", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ParentSwitchConstraint, Transform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_Transform_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_Transform_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_Switched_MetaData[] = {
		{ "Comment", "/**\n\x09 * Returns true if the parent has changed\n\x09 */" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_ParentSwitchConstraint.h" },
		{ "Output", "" },
		{ "ToolTip", "Returns true if the parent has changed" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_Switched_SetBit(void* Obj)
	{
		((FRigUnit_ParentSwitchConstraint*)Obj)->Switched = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_Switched = { "Switched", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_ParentSwitchConstraint), &Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_Switched_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_Switched_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_Switched_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_CachedSubject_MetaData[] = {
		{ "Comment", "// Used to cache the internally used subject\n" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_ParentSwitchConstraint.h" },
		{ "ToolTip", "Used to cache the internally used subject" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_CachedSubject = { "CachedSubject", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ParentSwitchConstraint, CachedSubject), Z_Construct_UScriptStruct_FCachedRigElement, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_CachedSubject_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_CachedSubject_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_CachedParent_MetaData[] = {
		{ "Comment", "// Used to cache the internally used parent\n" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_ParentSwitchConstraint.h" },
		{ "ToolTip", "Used to cache the internally used parent" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_CachedParent = { "CachedParent", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ParentSwitchConstraint, CachedParent), Z_Construct_UScriptStruct_FCachedRigElement, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_CachedParent_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_CachedParent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_RelativeOffset_MetaData[] = {
		{ "Comment", "// The cached relative offset between subject and parent\n" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_ParentSwitchConstraint.h" },
		{ "ToolTip", "The cached relative offset between subject and parent" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_RelativeOffset = { "RelativeOffset", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ParentSwitchConstraint, RelativeOffset), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_RelativeOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_RelativeOffset_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_Subject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_ParentIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_Parents,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_InitialGlobalTransform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_Weight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_Transform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_Switched,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_CachedSubject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_CachedParent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::NewProp_RelativeOffset,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnitMutable,
		&NewStructOps,
		"RigUnit_ParentSwitchConstraint",
		sizeof(FRigUnit_ParentSwitchConstraint),
		alignof(FRigUnit_ParentSwitchConstraint),
		Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_ParentSwitchConstraint"), sizeof(FRigUnit_ParentSwitchConstraint), Get_Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Hash() { return 452960214U; }

void FRigUnit_ParentSwitchConstraint::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Subject,
		ParentIndex,
		Parents,
		InitialGlobalTransform,
		Weight,
		Transform,
		Switched,
		CachedSubject,
		CachedParent,
		RelativeOffset,
		ExecuteContext,
		Context
	);
}

PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
