// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigUnit_DistributeRotation_generated_h
#error "RigUnit_DistributeRotation.generated.h already included, missing '#pragma once' in RigUnit_DistributeRotation.h"
#endif
#define CONTROLRIG_RigUnit_DistributeRotation_generated_h


#define FRigUnit_DistributeRotationForCollection_Execute() \
	void FRigUnit_DistributeRotationForCollection::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKeyCollection& Items, \
		const FRigVMFixedArray<FRigUnit_DistributeRotation_Rotation>& Rotations, \
		const EControlRigAnimEasingType RotationEaseType, \
		const float Weight, \
		FRigUnit_DistributeRotation_WorkData& WorkData, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_DistributeRotation_h_134_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKeyCollection& Items, \
		const FRigVMFixedArray<FRigUnit_DistributeRotation_Rotation>& Rotations, \
		const EControlRigAnimEasingType RotationEaseType, \
		const float Weight, \
		FRigUnit_DistributeRotation_WorkData& WorkData, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FRigElementKeyCollection& Items = *(FRigElementKeyCollection*)RigVMMemoryHandles[0].GetData(); \
		FRigVMFixedArray<FRigUnit_DistributeRotation_Rotation> Rotations((FRigUnit_DistributeRotation_Rotation*)RigVMMemoryHandles[1].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[2].GetData())); \
		EControlRigAnimEasingType RotationEaseType = (EControlRigAnimEasingType)*(uint8*)RigVMMemoryHandles[3].GetData(); \
		const float Weight = *(float*)RigVMMemoryHandles[4].GetData(); \
		FRigVMDynamicArray<FRigUnit_DistributeRotation_WorkData> WorkData_5_Array(*((FRigVMByteArray*)RigVMMemoryHandles[5].GetData(0, false))); \
		WorkData_5_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FRigUnit_DistributeRotation_WorkData& WorkData = WorkData_5_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[6].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Items, \
			Rotations, \
			RotationEaseType, \
			Weight, \
			WorkData, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnit_HighlevelBaseMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_DistributeRotationForCollection>();


#define FRigUnit_DistributeRotation_Execute() \
	void FRigUnit_DistributeRotation::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& StartBone, \
		const FName& EndBone, \
		const FRigVMFixedArray<FRigUnit_DistributeRotation_Rotation>& Rotations, \
		const EControlRigAnimEasingType RotationEaseType, \
		const float Weight, \
		const bool bPropagateToChildren, \
		FRigUnit_DistributeRotation_WorkData& WorkData, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_DistributeRotation_h_62_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& StartBone, \
		const FName& EndBone, \
		const FRigVMFixedArray<FRigUnit_DistributeRotation_Rotation>& Rotations, \
		const EControlRigAnimEasingType RotationEaseType, \
		const float Weight, \
		const bool bPropagateToChildren, \
		FRigUnit_DistributeRotation_WorkData& WorkData, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FName& StartBone = *(FName*)RigVMMemoryHandles[0].GetData(); \
		const FName& EndBone = *(FName*)RigVMMemoryHandles[1].GetData(); \
		FRigVMFixedArray<FRigUnit_DistributeRotation_Rotation> Rotations((FRigUnit_DistributeRotation_Rotation*)RigVMMemoryHandles[2].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[3].GetData())); \
		EControlRigAnimEasingType RotationEaseType = (EControlRigAnimEasingType)*(uint8*)RigVMMemoryHandles[4].GetData(); \
		const float Weight = *(float*)RigVMMemoryHandles[5].GetData(); \
		const bool bPropagateToChildren = *(bool*)RigVMMemoryHandles[6].GetData(); \
		FRigVMDynamicArray<FRigUnit_DistributeRotation_WorkData> WorkData_7_Array(*((FRigVMByteArray*)RigVMMemoryHandles[7].GetData(0, false))); \
		WorkData_7_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FRigUnit_DistributeRotation_WorkData& WorkData = WorkData_7_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[8].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			StartBone, \
			EndBone, \
			Rotations, \
			RotationEaseType, \
			Weight, \
			bPropagateToChildren, \
			WorkData, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnit_HighlevelBaseMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_DistributeRotation>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_DistributeRotation_h_36_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_DistributeRotation_WorkData>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_DistributeRotation_h_12_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Rotation_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_DistributeRotation_Rotation>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_DistributeRotation_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
