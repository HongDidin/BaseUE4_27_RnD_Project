// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Public/Rigs/RigInfluenceMap.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigInfluenceMap() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigInfluenceMapPerEvent();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigInfluenceMap();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigInfluenceEntry();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigElementKey();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigInfluenceEntryModifier();
// End Cross Module References
class UScriptStruct* FRigInfluenceMapPerEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigInfluenceMapPerEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigInfluenceMapPerEvent, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigInfluenceMapPerEvent"), sizeof(FRigInfluenceMapPerEvent), Get_Z_Construct_UScriptStruct_FRigInfluenceMapPerEvent_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigInfluenceMapPerEvent>()
{
	return FRigInfluenceMapPerEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigInfluenceMapPerEvent(FRigInfluenceMapPerEvent::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigInfluenceMapPerEvent"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigInfluenceMapPerEvent
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigInfluenceMapPerEvent()
	{
		UScriptStruct::DeferCppStructOps<FRigInfluenceMapPerEvent>(FName(TEXT("RigInfluenceMapPerEvent")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigInfluenceMapPerEvent;
	struct Z_Construct_UScriptStruct_FRigInfluenceMapPerEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Maps_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Maps_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Maps;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_EventToIndex_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_EventToIndex_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EventToIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_EventToIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigInfluenceMapPerEvent_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Rigs/RigInfluenceMap.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigInfluenceMapPerEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigInfluenceMapPerEvent>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigInfluenceMapPerEvent_Statics::NewProp_Maps_Inner = { "Maps", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRigInfluenceMap, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigInfluenceMapPerEvent_Statics::NewProp_Maps_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/RigInfluenceMap.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigInfluenceMapPerEvent_Statics::NewProp_Maps = { "Maps", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigInfluenceMapPerEvent, Maps), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigInfluenceMapPerEvent_Statics::NewProp_Maps_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigInfluenceMapPerEvent_Statics::NewProp_Maps_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigInfluenceMapPerEvent_Statics::NewProp_EventToIndex_ValueProp = { "EventToIndex", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigInfluenceMapPerEvent_Statics::NewProp_EventToIndex_Key_KeyProp = { "EventToIndex_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigInfluenceMapPerEvent_Statics::NewProp_EventToIndex_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/RigInfluenceMap.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FRigInfluenceMapPerEvent_Statics::NewProp_EventToIndex = { "EventToIndex", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigInfluenceMapPerEvent, EventToIndex), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigInfluenceMapPerEvent_Statics::NewProp_EventToIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigInfluenceMapPerEvent_Statics::NewProp_EventToIndex_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigInfluenceMapPerEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigInfluenceMapPerEvent_Statics::NewProp_Maps_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigInfluenceMapPerEvent_Statics::NewProp_Maps,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigInfluenceMapPerEvent_Statics::NewProp_EventToIndex_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigInfluenceMapPerEvent_Statics::NewProp_EventToIndex_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigInfluenceMapPerEvent_Statics::NewProp_EventToIndex,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigInfluenceMapPerEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"RigInfluenceMapPerEvent",
		sizeof(FRigInfluenceMapPerEvent),
		alignof(FRigInfluenceMapPerEvent),
		Z_Construct_UScriptStruct_FRigInfluenceMapPerEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigInfluenceMapPerEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigInfluenceMapPerEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigInfluenceMapPerEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigInfluenceMapPerEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigInfluenceMapPerEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigInfluenceMapPerEvent"), sizeof(FRigInfluenceMapPerEvent), Get_Z_Construct_UScriptStruct_FRigInfluenceMapPerEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigInfluenceMapPerEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigInfluenceMapPerEvent_Hash() { return 269010839U; }
class UScriptStruct* FRigInfluenceMap::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigInfluenceMap_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigInfluenceMap, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigInfluenceMap"), sizeof(FRigInfluenceMap), Get_Z_Construct_UScriptStruct_FRigInfluenceMap_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigInfluenceMap>()
{
	return FRigInfluenceMap::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigInfluenceMap(FRigInfluenceMap::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigInfluenceMap"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigInfluenceMap
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigInfluenceMap()
	{
		UScriptStruct::DeferCppStructOps<FRigInfluenceMap>(FName(TEXT("RigInfluenceMap")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigInfluenceMap;
	struct Z_Construct_UScriptStruct_FRigInfluenceMap_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EventName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_EventName;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Entries_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Entries_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Entries;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_KeyToIndex_ValueProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_KeyToIndex_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_KeyToIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_KeyToIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigInfluenceMap_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Rigs/RigInfluenceMap.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigInfluenceMap_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigInfluenceMap>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigInfluenceMap_Statics::NewProp_EventName_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/RigInfluenceMap.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigInfluenceMap_Statics::NewProp_EventName = { "EventName", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigInfluenceMap, EventName), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigInfluenceMap_Statics::NewProp_EventName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigInfluenceMap_Statics::NewProp_EventName_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigInfluenceMap_Statics::NewProp_Entries_Inner = { "Entries", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRigInfluenceEntry, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigInfluenceMap_Statics::NewProp_Entries_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/RigInfluenceMap.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigInfluenceMap_Statics::NewProp_Entries = { "Entries", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigInfluenceMap, Entries), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigInfluenceMap_Statics::NewProp_Entries_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigInfluenceMap_Statics::NewProp_Entries_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigInfluenceMap_Statics::NewProp_KeyToIndex_ValueProp = { "KeyToIndex", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigInfluenceMap_Statics::NewProp_KeyToIndex_Key_KeyProp = { "KeyToIndex_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigInfluenceMap_Statics::NewProp_KeyToIndex_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/RigInfluenceMap.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FRigInfluenceMap_Statics::NewProp_KeyToIndex = { "KeyToIndex", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigInfluenceMap, KeyToIndex), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigInfluenceMap_Statics::NewProp_KeyToIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigInfluenceMap_Statics::NewProp_KeyToIndex_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigInfluenceMap_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigInfluenceMap_Statics::NewProp_EventName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigInfluenceMap_Statics::NewProp_Entries_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigInfluenceMap_Statics::NewProp_Entries,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigInfluenceMap_Statics::NewProp_KeyToIndex_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigInfluenceMap_Statics::NewProp_KeyToIndex_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigInfluenceMap_Statics::NewProp_KeyToIndex,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigInfluenceMap_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"RigInfluenceMap",
		sizeof(FRigInfluenceMap),
		alignof(FRigInfluenceMap),
		Z_Construct_UScriptStruct_FRigInfluenceMap_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigInfluenceMap_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigInfluenceMap_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigInfluenceMap_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigInfluenceMap()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigInfluenceMap_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigInfluenceMap"), sizeof(FRigInfluenceMap), Get_Z_Construct_UScriptStruct_FRigInfluenceMap_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigInfluenceMap_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigInfluenceMap_Hash() { return 2804416060U; }
class UScriptStruct* FRigInfluenceEntryModifier::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigInfluenceEntryModifier_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigInfluenceEntryModifier, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigInfluenceEntryModifier"), sizeof(FRigInfluenceEntryModifier), Get_Z_Construct_UScriptStruct_FRigInfluenceEntryModifier_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigInfluenceEntryModifier>()
{
	return FRigInfluenceEntryModifier::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigInfluenceEntryModifier(FRigInfluenceEntryModifier::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigInfluenceEntryModifier"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigInfluenceEntryModifier
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigInfluenceEntryModifier()
	{
		UScriptStruct::DeferCppStructOps<FRigInfluenceEntryModifier>(FName(TEXT("RigInfluenceEntryModifier")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigInfluenceEntryModifier;
	struct Z_Construct_UScriptStruct_FRigInfluenceEntryModifier_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AffectedList_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AffectedList_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_AffectedList;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigInfluenceEntryModifier_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Rigs/RigInfluenceMap.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigInfluenceEntryModifier_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigInfluenceEntryModifier>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigInfluenceEntryModifier_Statics::NewProp_AffectedList_Inner = { "AffectedList", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigInfluenceEntryModifier_Statics::NewProp_AffectedList_MetaData[] = {
		{ "Category", "Inversion" },
		{ "ModuleRelativePath", "Public/Rigs/RigInfluenceMap.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigInfluenceEntryModifier_Statics::NewProp_AffectedList = { "AffectedList", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigInfluenceEntryModifier, AffectedList), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigInfluenceEntryModifier_Statics::NewProp_AffectedList_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigInfluenceEntryModifier_Statics::NewProp_AffectedList_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigInfluenceEntryModifier_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigInfluenceEntryModifier_Statics::NewProp_AffectedList_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigInfluenceEntryModifier_Statics::NewProp_AffectedList,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigInfluenceEntryModifier_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"RigInfluenceEntryModifier",
		sizeof(FRigInfluenceEntryModifier),
		alignof(FRigInfluenceEntryModifier),
		Z_Construct_UScriptStruct_FRigInfluenceEntryModifier_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigInfluenceEntryModifier_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigInfluenceEntryModifier_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigInfluenceEntryModifier_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigInfluenceEntryModifier()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigInfluenceEntryModifier_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigInfluenceEntryModifier"), sizeof(FRigInfluenceEntryModifier), Get_Z_Construct_UScriptStruct_FRigInfluenceEntryModifier_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigInfluenceEntryModifier_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigInfluenceEntryModifier_Hash() { return 1757924518U; }
class UScriptStruct* FRigInfluenceEntry::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigInfluenceEntry_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigInfluenceEntry, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigInfluenceEntry"), sizeof(FRigInfluenceEntry), Get_Z_Construct_UScriptStruct_FRigInfluenceEntry_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigInfluenceEntry>()
{
	return FRigInfluenceEntry::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigInfluenceEntry(FRigInfluenceEntry::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigInfluenceEntry"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigInfluenceEntry
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigInfluenceEntry()
	{
		UScriptStruct::DeferCppStructOps<FRigInfluenceEntry>(FName(TEXT("RigInfluenceEntry")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigInfluenceEntry;
	struct Z_Construct_UScriptStruct_FRigInfluenceEntry_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Source_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Source;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AffectedList_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AffectedList_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_AffectedList;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigInfluenceEntry_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Rigs/RigInfluenceMap.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigInfluenceEntry_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigInfluenceEntry>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigInfluenceEntry_Statics::NewProp_Source_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/RigInfluenceMap.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigInfluenceEntry_Statics::NewProp_Source = { "Source", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigInfluenceEntry, Source), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigInfluenceEntry_Statics::NewProp_Source_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigInfluenceEntry_Statics::NewProp_Source_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigInfluenceEntry_Statics::NewProp_AffectedList_Inner = { "AffectedList", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigInfluenceEntry_Statics::NewProp_AffectedList_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/RigInfluenceMap.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigInfluenceEntry_Statics::NewProp_AffectedList = { "AffectedList", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigInfluenceEntry, AffectedList), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigInfluenceEntry_Statics::NewProp_AffectedList_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigInfluenceEntry_Statics::NewProp_AffectedList_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigInfluenceEntry_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigInfluenceEntry_Statics::NewProp_Source,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigInfluenceEntry_Statics::NewProp_AffectedList_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigInfluenceEntry_Statics::NewProp_AffectedList,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigInfluenceEntry_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"RigInfluenceEntry",
		sizeof(FRigInfluenceEntry),
		alignof(FRigInfluenceEntry),
		Z_Construct_UScriptStruct_FRigInfluenceEntry_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigInfluenceEntry_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigInfluenceEntry_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigInfluenceEntry_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigInfluenceEntry()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigInfluenceEntry_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigInfluenceEntry"), sizeof(FRigInfluenceEntry), Get_Z_Construct_UScriptStruct_FRigInfluenceEntry_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigInfluenceEntry_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigInfluenceEntry_Hash() { return 2484173497U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
