// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIGEDITOR_ControlRigUnitNodeSpawner_generated_h
#error "ControlRigUnitNodeSpawner.generated.h already included, missing '#pragma once' in ControlRigUnitNodeSpawner.h"
#endif
#define CONTROLRIGEDITOR_ControlRigUnitNodeSpawner_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigUnitNodeSpawner_h_26_SPARSE_DATA
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigUnitNodeSpawner_h_26_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigUnitNodeSpawner_h_26_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigUnitNodeSpawner_h_26_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUControlRigUnitNodeSpawner(); \
	friend struct Z_Construct_UClass_UControlRigUnitNodeSpawner_Statics; \
public: \
	DECLARE_CLASS(UControlRigUnitNodeSpawner, UBlueprintNodeSpawner, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ControlRigEditor"), NO_API) \
	DECLARE_SERIALIZER(UControlRigUnitNodeSpawner)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigUnitNodeSpawner_h_26_INCLASS \
private: \
	static void StaticRegisterNativesUControlRigUnitNodeSpawner(); \
	friend struct Z_Construct_UClass_UControlRigUnitNodeSpawner_Statics; \
public: \
	DECLARE_CLASS(UControlRigUnitNodeSpawner, UBlueprintNodeSpawner, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ControlRigEditor"), NO_API) \
	DECLARE_SERIALIZER(UControlRigUnitNodeSpawner)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigUnitNodeSpawner_h_26_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UControlRigUnitNodeSpawner(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigUnitNodeSpawner) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigUnitNodeSpawner); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigUnitNodeSpawner); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigUnitNodeSpawner(UControlRigUnitNodeSpawner&&); \
	NO_API UControlRigUnitNodeSpawner(const UControlRigUnitNodeSpawner&); \
public:


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigUnitNodeSpawner_h_26_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UControlRigUnitNodeSpawner(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigUnitNodeSpawner(UControlRigUnitNodeSpawner&&); \
	NO_API UControlRigUnitNodeSpawner(const UControlRigUnitNodeSpawner&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigUnitNodeSpawner); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigUnitNodeSpawner); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigUnitNodeSpawner)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigUnitNodeSpawner_h_26_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__StructTemplate() { return STRUCT_OFFSET(UControlRigUnitNodeSpawner, StructTemplate); }


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigUnitNodeSpawner_h_23_PROLOG
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigUnitNodeSpawner_h_26_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigUnitNodeSpawner_h_26_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigUnitNodeSpawner_h_26_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigUnitNodeSpawner_h_26_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigUnitNodeSpawner_h_26_INCLASS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigUnitNodeSpawner_h_26_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigUnitNodeSpawner_h_26_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigUnitNodeSpawner_h_26_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigUnitNodeSpawner_h_26_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigUnitNodeSpawner_h_26_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigUnitNodeSpawner_h_26_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigUnitNodeSpawner_h_26_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONTROLRIGEDITOR_API UClass* StaticClass<class UControlRigUnitNodeSpawner>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigUnitNodeSpawner_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
