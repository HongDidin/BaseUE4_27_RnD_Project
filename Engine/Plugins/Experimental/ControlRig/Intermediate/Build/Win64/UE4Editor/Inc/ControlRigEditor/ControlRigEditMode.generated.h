// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIGEDITOR_ControlRigEditMode_generated_h
#error "ControlRigEditMode.generated.h already included, missing '#pragma once' in ControlRigEditMode.h"
#endif
#define CONTROLRIGEDITOR_ControlRigEditMode_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditMode_h_48_SPARSE_DATA
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditMode_h_48_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execPostPoseUpdate); \
	DECLARE_FUNCTION(execOnPoseInitialized);


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditMode_h_48_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execPostPoseUpdate); \
	DECLARE_FUNCTION(execOnPoseInitialized);


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditMode_h_48_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUControlRigEditModeDelegateHelper(); \
	friend struct Z_Construct_UClass_UControlRigEditModeDelegateHelper_Statics; \
public: \
	DECLARE_CLASS(UControlRigEditModeDelegateHelper, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRigEditor"), NO_API) \
	DECLARE_SERIALIZER(UControlRigEditModeDelegateHelper)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditMode_h_48_INCLASS \
private: \
	static void StaticRegisterNativesUControlRigEditModeDelegateHelper(); \
	friend struct Z_Construct_UClass_UControlRigEditModeDelegateHelper_Statics; \
public: \
	DECLARE_CLASS(UControlRigEditModeDelegateHelper, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRigEditor"), NO_API) \
	DECLARE_SERIALIZER(UControlRigEditModeDelegateHelper)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditMode_h_48_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UControlRigEditModeDelegateHelper(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigEditModeDelegateHelper) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigEditModeDelegateHelper); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigEditModeDelegateHelper); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigEditModeDelegateHelper(UControlRigEditModeDelegateHelper&&); \
	NO_API UControlRigEditModeDelegateHelper(const UControlRigEditModeDelegateHelper&); \
public:


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditMode_h_48_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UControlRigEditModeDelegateHelper(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigEditModeDelegateHelper(UControlRigEditModeDelegateHelper&&); \
	NO_API UControlRigEditModeDelegateHelper(const UControlRigEditModeDelegateHelper&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigEditModeDelegateHelper); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigEditModeDelegateHelper); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigEditModeDelegateHelper)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditMode_h_48_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditMode_h_45_PROLOG
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditMode_h_48_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditMode_h_48_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditMode_h_48_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditMode_h_48_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditMode_h_48_INCLASS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditMode_h_48_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditMode_h_48_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditMode_h_48_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditMode_h_48_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditMode_h_48_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditMode_h_48_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditMode_h_48_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONTROLRIGEDITOR_API UClass* StaticClass<class UControlRigEditModeDelegateHelper>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_EditMode_ControlRigEditMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
