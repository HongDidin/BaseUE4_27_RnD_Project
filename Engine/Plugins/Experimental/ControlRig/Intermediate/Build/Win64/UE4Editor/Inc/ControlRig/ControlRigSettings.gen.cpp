// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Public/Settings/ControlRigSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeControlRigSettings() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FControlRigSettingsPerPinBool();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UClass* Z_Construct_UClass_UControlRigSettings_NoRegister();
	CONTROLRIG_API UClass* Z_Construct_UClass_UControlRigSettings();
	DEVELOPERSETTINGS_API UClass* Z_Construct_UClass_UDeveloperSettings();
	CONTROLRIG_API UClass* Z_Construct_UClass_UControlRigGizmoLibrary_NoRegister();
// End Cross Module References
class UScriptStruct* FControlRigSettingsPerPinBool::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FControlRigSettingsPerPinBool_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FControlRigSettingsPerPinBool, Z_Construct_UPackage__Script_ControlRig(), TEXT("ControlRigSettingsPerPinBool"), sizeof(FControlRigSettingsPerPinBool), Get_Z_Construct_UScriptStruct_FControlRigSettingsPerPinBool_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FControlRigSettingsPerPinBool>()
{
	return FControlRigSettingsPerPinBool::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FControlRigSettingsPerPinBool(FControlRigSettingsPerPinBool::StaticStruct, TEXT("/Script/ControlRig"), TEXT("ControlRigSettingsPerPinBool"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFControlRigSettingsPerPinBool
{
	FScriptStruct_ControlRig_StaticRegisterNativesFControlRigSettingsPerPinBool()
	{
		UScriptStruct::DeferCppStructOps<FControlRigSettingsPerPinBool>(FName(TEXT("ControlRigSettingsPerPinBool")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFControlRigSettingsPerPinBool;
	struct Z_Construct_UScriptStruct_FControlRigSettingsPerPinBool_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Values_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Values_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Values_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_Values;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FControlRigSettingsPerPinBool_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Settings/ControlRigSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FControlRigSettingsPerPinBool_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FControlRigSettingsPerPinBool>();
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FControlRigSettingsPerPinBool_Statics::NewProp_Values_ValueProp = { "Values", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FControlRigSettingsPerPinBool_Statics::NewProp_Values_Key_KeyProp = { "Values_Key", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FControlRigSettingsPerPinBool_Statics::NewProp_Values_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/Settings/ControlRigSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FControlRigSettingsPerPinBool_Statics::NewProp_Values = { "Values", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FControlRigSettingsPerPinBool, Values), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FControlRigSettingsPerPinBool_Statics::NewProp_Values_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FControlRigSettingsPerPinBool_Statics::NewProp_Values_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FControlRigSettingsPerPinBool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FControlRigSettingsPerPinBool_Statics::NewProp_Values_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FControlRigSettingsPerPinBool_Statics::NewProp_Values_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FControlRigSettingsPerPinBool_Statics::NewProp_Values,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FControlRigSettingsPerPinBool_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"ControlRigSettingsPerPinBool",
		sizeof(FControlRigSettingsPerPinBool),
		alignof(FControlRigSettingsPerPinBool),
		Z_Construct_UScriptStruct_FControlRigSettingsPerPinBool_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FControlRigSettingsPerPinBool_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FControlRigSettingsPerPinBool_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FControlRigSettingsPerPinBool_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FControlRigSettingsPerPinBool()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FControlRigSettingsPerPinBool_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ControlRigSettingsPerPinBool"), sizeof(FControlRigSettingsPerPinBool), Get_Z_Construct_UScriptStruct_FControlRigSettingsPerPinBool_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FControlRigSettingsPerPinBool_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FControlRigSettingsPerPinBool_Hash() { return 1812648812U; }
	void UControlRigSettings::StaticRegisterNativesUControlRigSettings()
	{
	}
	UClass* Z_Construct_UClass_UControlRigSettings_NoRegister()
	{
		return UControlRigSettings::StaticClass();
	}
	struct Z_Construct_UClass_UControlRigSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultGizmoLibrary_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_DefaultGizmoLibrary;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bResetControlsOnCompile_MetaData[];
#endif
		static void NewProp_bResetControlsOnCompile_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bResetControlsOnCompile;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bResetControlsOnPinValueInteraction_MetaData[];
#endif
		static void NewProp_bResetControlsOnPinValueInteraction_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bResetControlsOnPinValueInteraction;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bResetControlTransformsOnCompile_MetaData[];
#endif
		static void NewProp_bResetControlTransformsOnCompile_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bResetControlTransformsOnCompile;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RigUnitPinExpansion_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_RigUnitPinExpansion_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RigUnitPinExpansion_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_RigUnitPinExpansion;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#endif // WITH_EDITORONLY_DATA
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UControlRigSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDeveloperSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Default ControlRig settings.\n */" },
		{ "DisplayName", "Control Rig" },
		{ "IncludePath", "Settings/ControlRigSettings.h" },
		{ "ModuleRelativePath", "Public/Settings/ControlRigSettings.h" },
		{ "ToolTip", "Default ControlRig settings." },
	};
#endif
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigSettings_Statics::NewProp_DefaultGizmoLibrary_MetaData[] = {
		{ "Category", "DefaultGizmo" },
		{ "ModuleRelativePath", "Public/Settings/ControlRigSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UControlRigSettings_Statics::NewProp_DefaultGizmoLibrary = { "DefaultGizmoLibrary", nullptr, (EPropertyFlags)0x0014000800004001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigSettings, DefaultGizmoLibrary), Z_Construct_UClass_UControlRigGizmoLibrary_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UControlRigSettings_Statics::NewProp_DefaultGizmoLibrary_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigSettings_Statics::NewProp_DefaultGizmoLibrary_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigSettings_Statics::NewProp_bResetControlsOnCompile_MetaData[] = {
		{ "Category", "Interaction" },
		{ "Comment", "// When this is checked all controls will return to their initial\n// value as the user hits the Compile button.\n" },
		{ "ModuleRelativePath", "Public/Settings/ControlRigSettings.h" },
		{ "ToolTip", "When this is checked all controls will return to their initial\nvalue as the user hits the Compile button." },
	};
#endif
	void Z_Construct_UClass_UControlRigSettings_Statics::NewProp_bResetControlsOnCompile_SetBit(void* Obj)
	{
		((UControlRigSettings*)Obj)->bResetControlsOnCompile = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UControlRigSettings_Statics::NewProp_bResetControlsOnCompile = { "bResetControlsOnCompile", nullptr, (EPropertyFlags)0x0010000800004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UControlRigSettings), &Z_Construct_UClass_UControlRigSettings_Statics::NewProp_bResetControlsOnCompile_SetBit, METADATA_PARAMS(Z_Construct_UClass_UControlRigSettings_Statics::NewProp_bResetControlsOnCompile_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigSettings_Statics::NewProp_bResetControlsOnCompile_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigSettings_Statics::NewProp_bResetControlsOnPinValueInteraction_MetaData[] = {
		{ "Category", "Interaction" },
		{ "Comment", "// When this is checked all controls will return to their initial\n// value as the user interacts with a pin value\n" },
		{ "ModuleRelativePath", "Public/Settings/ControlRigSettings.h" },
		{ "ToolTip", "When this is checked all controls will return to their initial\nvalue as the user interacts with a pin value" },
	};
#endif
	void Z_Construct_UClass_UControlRigSettings_Statics::NewProp_bResetControlsOnPinValueInteraction_SetBit(void* Obj)
	{
		((UControlRigSettings*)Obj)->bResetControlsOnPinValueInteraction = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UControlRigSettings_Statics::NewProp_bResetControlsOnPinValueInteraction = { "bResetControlsOnPinValueInteraction", nullptr, (EPropertyFlags)0x0010000800004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UControlRigSettings), &Z_Construct_UClass_UControlRigSettings_Statics::NewProp_bResetControlsOnPinValueInteraction_SetBit, METADATA_PARAMS(Z_Construct_UClass_UControlRigSettings_Statics::NewProp_bResetControlsOnPinValueInteraction_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigSettings_Statics::NewProp_bResetControlsOnPinValueInteraction_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigSettings_Statics::NewProp_bResetControlTransformsOnCompile_MetaData[] = {
		{ "Category", "Compilation" },
		{ "Comment", "/**\n\x09 * When checked controls will be reset during a manual compilation\n\x09 * (when pressing the Compile button)\n\x09 */" },
		{ "ModuleRelativePath", "Public/Settings/ControlRigSettings.h" },
		{ "ToolTip", "When checked controls will be reset during a manual compilation\n(when pressing the Compile button)" },
	};
#endif
	void Z_Construct_UClass_UControlRigSettings_Statics::NewProp_bResetControlTransformsOnCompile_SetBit(void* Obj)
	{
		((UControlRigSettings*)Obj)->bResetControlTransformsOnCompile = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UControlRigSettings_Statics::NewProp_bResetControlTransformsOnCompile = { "bResetControlTransformsOnCompile", nullptr, (EPropertyFlags)0x0010000800004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UControlRigSettings), &Z_Construct_UClass_UControlRigSettings_Statics::NewProp_bResetControlTransformsOnCompile_SetBit, METADATA_PARAMS(Z_Construct_UClass_UControlRigSettings_Statics::NewProp_bResetControlTransformsOnCompile_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigSettings_Statics::NewProp_bResetControlTransformsOnCompile_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UControlRigSettings_Statics::NewProp_RigUnitPinExpansion_ValueProp = { "RigUnitPinExpansion", nullptr, (EPropertyFlags)0x0000000800004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FControlRigSettingsPerPinBool, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UControlRigSettings_Statics::NewProp_RigUnitPinExpansion_Key_KeyProp = { "RigUnitPinExpansion_Key", nullptr, (EPropertyFlags)0x0000000800004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigSettings_Statics::NewProp_RigUnitPinExpansion_MetaData[] = {
		{ "Category", "NodeGraph" },
		{ "Comment", "/**\n\x09 * A map which remembers the expansion setting for each rig unit pin.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Settings/ControlRigSettings.h" },
		{ "ToolTip", "A map which remembers the expansion setting for each rig unit pin." },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UControlRigSettings_Statics::NewProp_RigUnitPinExpansion = { "RigUnitPinExpansion", nullptr, (EPropertyFlags)0x0010000800004001, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigSettings, RigUnitPinExpansion), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UControlRigSettings_Statics::NewProp_RigUnitPinExpansion_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigSettings_Statics::NewProp_RigUnitPinExpansion_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UControlRigSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigSettings_Statics::NewProp_DefaultGizmoLibrary,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigSettings_Statics::NewProp_bResetControlsOnCompile,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigSettings_Statics::NewProp_bResetControlsOnPinValueInteraction,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigSettings_Statics::NewProp_bResetControlTransformsOnCompile,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigSettings_Statics::NewProp_RigUnitPinExpansion_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigSettings_Statics::NewProp_RigUnitPinExpansion_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigSettings_Statics::NewProp_RigUnitPinExpansion,
	};
#endif // WITH_EDITORONLY_DATA
	const FCppClassTypeInfoStatic Z_Construct_UClass_UControlRigSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UControlRigSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UControlRigSettings_Statics::ClassParams = {
		&UControlRigSettings::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		IF_WITH_EDITORONLY_DATA(Z_Construct_UClass_UControlRigSettings_Statics::PropPointers, nullptr),
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		IF_WITH_EDITORONLY_DATA(UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigSettings_Statics::PropPointers), 0),
		0,
		0x001000A6u,
		METADATA_PARAMS(Z_Construct_UClass_UControlRigSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UControlRigSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UControlRigSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UControlRigSettings, 4293338892);
	template<> CONTROLRIG_API UClass* StaticClass<UControlRigSettings>()
	{
		return UControlRigSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UControlRigSettings(Z_Construct_UClass_UControlRigSettings, &UControlRigSettings::StaticClass, TEXT("/Script/ControlRig"), TEXT("UControlRigSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UControlRigSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
