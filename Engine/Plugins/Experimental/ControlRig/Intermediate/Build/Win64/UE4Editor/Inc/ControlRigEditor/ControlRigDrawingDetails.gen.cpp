// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRigEditor/Private/ControlRigDrawingDetails.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeControlRigDrawingDetails() {}
// Cross Module References
	CONTROLRIGEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FControlRigDrawContainerImportFbxSettings();
	UPackage* Z_Construct_UPackage__Script_ControlRigEditor();
// End Cross Module References
class UScriptStruct* FControlRigDrawContainerImportFbxSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIGEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FControlRigDrawContainerImportFbxSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FControlRigDrawContainerImportFbxSettings, Z_Construct_UPackage__Script_ControlRigEditor(), TEXT("ControlRigDrawContainerImportFbxSettings"), sizeof(FControlRigDrawContainerImportFbxSettings), Get_Z_Construct_UScriptStruct_FControlRigDrawContainerImportFbxSettings_Hash());
	}
	return Singleton;
}
template<> CONTROLRIGEDITOR_API UScriptStruct* StaticStruct<FControlRigDrawContainerImportFbxSettings>()
{
	return FControlRigDrawContainerImportFbxSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FControlRigDrawContainerImportFbxSettings(FControlRigDrawContainerImportFbxSettings::StaticStruct, TEXT("/Script/ControlRigEditor"), TEXT("ControlRigDrawContainerImportFbxSettings"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRigEditor_StaticRegisterNativesFControlRigDrawContainerImportFbxSettings
{
	FScriptStruct_ControlRigEditor_StaticRegisterNativesFControlRigDrawContainerImportFbxSettings()
	{
		UScriptStruct::DeferCppStructOps<FControlRigDrawContainerImportFbxSettings>(FName(TEXT("ControlRigDrawContainerImportFbxSettings")));
	}
} ScriptStruct_ControlRigEditor_StaticRegisterNativesFControlRigDrawContainerImportFbxSettings;
	struct Z_Construct_UScriptStruct_FControlRigDrawContainerImportFbxSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Scale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Scale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Detail_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Detail;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bMergeCurves_MetaData[];
#endif
		static void NewProp_bMergeCurves_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bMergeCurves;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FControlRigDrawContainerImportFbxSettings_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/ControlRigDrawingDetails.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FControlRigDrawContainerImportFbxSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FControlRigDrawContainerImportFbxSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FControlRigDrawContainerImportFbxSettings_Statics::NewProp_Scale_MetaData[] = {
		{ "Category", "Fbx Import" },
		{ "ModuleRelativePath", "Private/ControlRigDrawingDetails.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FControlRigDrawContainerImportFbxSettings_Statics::NewProp_Scale = { "Scale", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FControlRigDrawContainerImportFbxSettings, Scale), METADATA_PARAMS(Z_Construct_UScriptStruct_FControlRigDrawContainerImportFbxSettings_Statics::NewProp_Scale_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FControlRigDrawContainerImportFbxSettings_Statics::NewProp_Scale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FControlRigDrawContainerImportFbxSettings_Statics::NewProp_Detail_MetaData[] = {
		{ "Category", "Fbx Import" },
		{ "ModuleRelativePath", "Private/ControlRigDrawingDetails.h" },
		{ "UIMax", "8" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FControlRigDrawContainerImportFbxSettings_Statics::NewProp_Detail = { "Detail", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FControlRigDrawContainerImportFbxSettings, Detail), METADATA_PARAMS(Z_Construct_UScriptStruct_FControlRigDrawContainerImportFbxSettings_Statics::NewProp_Detail_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FControlRigDrawContainerImportFbxSettings_Statics::NewProp_Detail_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FControlRigDrawContainerImportFbxSettings_Statics::NewProp_bMergeCurves_MetaData[] = {
		{ "Category", "Fbx Import" },
		{ "ModuleRelativePath", "Private/ControlRigDrawingDetails.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FControlRigDrawContainerImportFbxSettings_Statics::NewProp_bMergeCurves_SetBit(void* Obj)
	{
		((FControlRigDrawContainerImportFbxSettings*)Obj)->bMergeCurves = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FControlRigDrawContainerImportFbxSettings_Statics::NewProp_bMergeCurves = { "bMergeCurves", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FControlRigDrawContainerImportFbxSettings), &Z_Construct_UScriptStruct_FControlRigDrawContainerImportFbxSettings_Statics::NewProp_bMergeCurves_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FControlRigDrawContainerImportFbxSettings_Statics::NewProp_bMergeCurves_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FControlRigDrawContainerImportFbxSettings_Statics::NewProp_bMergeCurves_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FControlRigDrawContainerImportFbxSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FControlRigDrawContainerImportFbxSettings_Statics::NewProp_Scale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FControlRigDrawContainerImportFbxSettings_Statics::NewProp_Detail,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FControlRigDrawContainerImportFbxSettings_Statics::NewProp_bMergeCurves,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FControlRigDrawContainerImportFbxSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRigEditor,
		nullptr,
		&NewStructOps,
		"ControlRigDrawContainerImportFbxSettings",
		sizeof(FControlRigDrawContainerImportFbxSettings),
		alignof(FControlRigDrawContainerImportFbxSettings),
		Z_Construct_UScriptStruct_FControlRigDrawContainerImportFbxSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FControlRigDrawContainerImportFbxSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FControlRigDrawContainerImportFbxSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FControlRigDrawContainerImportFbxSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FControlRigDrawContainerImportFbxSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FControlRigDrawContainerImportFbxSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRigEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ControlRigDrawContainerImportFbxSettings"), sizeof(FControlRigDrawContainerImportFbxSettings), Get_Z_Construct_UScriptStruct_FControlRigDrawContainerImportFbxSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FControlRigDrawContainerImportFbxSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FControlRigDrawContainerImportFbxSettings_Hash() { return 3998853763U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
