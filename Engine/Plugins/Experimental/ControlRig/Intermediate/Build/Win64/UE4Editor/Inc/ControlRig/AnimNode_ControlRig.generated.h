// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_AnimNode_ControlRig_generated_h
#error "AnimNode_ControlRig.generated.h already included, missing '#pragma once' in AnimNode_ControlRig.h"
#endif
#define CONTROLRIG_AnimNode_ControlRig_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_AnimNode_ControlRig_h_18_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAnimNode_ControlRig_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__ControlRigClass() { return STRUCT_OFFSET(FAnimNode_ControlRig, ControlRigClass); } \
	FORCEINLINE static uint32 __PPO__ControlRig() { return STRUCT_OFFSET(FAnimNode_ControlRig, ControlRig); } \
	FORCEINLINE static uint32 __PPO__Alpha() { return STRUCT_OFFSET(FAnimNode_ControlRig, Alpha); } \
	FORCEINLINE static uint32 __PPO__AlphaInputType() { return STRUCT_OFFSET(FAnimNode_ControlRig, AlphaInputType); } \
	FORCEINLINE static uint32 __PPO__AlphaScaleBias() { return STRUCT_OFFSET(FAnimNode_ControlRig, AlphaScaleBias); } \
	FORCEINLINE static uint32 __PPO__AlphaBoolBlend() { return STRUCT_OFFSET(FAnimNode_ControlRig, AlphaBoolBlend); } \
	FORCEINLINE static uint32 __PPO__AlphaCurveName() { return STRUCT_OFFSET(FAnimNode_ControlRig, AlphaCurveName); } \
	FORCEINLINE static uint32 __PPO__AlphaScaleBiasClamp() { return STRUCT_OFFSET(FAnimNode_ControlRig, AlphaScaleBiasClamp); } \
	FORCEINLINE static uint32 __PPO__InputMapping() { return STRUCT_OFFSET(FAnimNode_ControlRig, InputMapping); } \
	FORCEINLINE static uint32 __PPO__OutputMapping() { return STRUCT_OFFSET(FAnimNode_ControlRig, OutputMapping); } \
	FORCEINLINE static uint32 __PPO__LODThreshold() { return STRUCT_OFFSET(FAnimNode_ControlRig, LODThreshold); } \
	typedef FAnimNode_ControlRigBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FAnimNode_ControlRig>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_AnimNode_ControlRig_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
