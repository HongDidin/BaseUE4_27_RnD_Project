// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_MovieSceneControlRigParameterSection_generated_h
#error "MovieSceneControlRigParameterSection.generated.h already included, missing '#pragma once' in MovieSceneControlRigParameterSection.h"
#endif
#define CONTROLRIG_MovieSceneControlRigParameterSection_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterSection_h_119_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FChannelMapInfo_Statics; \
	static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FChannelMapInfo>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterSection_h_76_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FIntegerParameterNameAndCurve_Statics; \
	static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FIntegerParameterNameAndCurve>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterSection_h_58_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FEnumParameterNameAndCurve_Statics; \
	static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FEnumParameterNameAndCurve>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterSection_h_145_SPARSE_DATA
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterSection_h_145_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterSection_h_145_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterSection_h_145_ARCHIVESERIALIZER \
	DECLARE_FSTRUCTUREDARCHIVE_SERIALIZER(UMovieSceneControlRigParameterSection, NO_API)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterSection_h_145_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMovieSceneControlRigParameterSection(); \
	friend struct Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneControlRigParameterSection, UMovieSceneParameterSection, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRig"), NO_API) \
	DECLARE_SERIALIZER(UMovieSceneControlRigParameterSection) \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterSection_h_145_ARCHIVESERIALIZER


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterSection_h_145_INCLASS \
private: \
	static void StaticRegisterNativesUMovieSceneControlRigParameterSection(); \
	friend struct Z_Construct_UClass_UMovieSceneControlRigParameterSection_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneControlRigParameterSection, UMovieSceneParameterSection, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRig"), NO_API) \
	DECLARE_SERIALIZER(UMovieSceneControlRigParameterSection) \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterSection_h_145_ARCHIVESERIALIZER


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterSection_h_145_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMovieSceneControlRigParameterSection(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneControlRigParameterSection) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMovieSceneControlRigParameterSection); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneControlRigParameterSection); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMovieSceneControlRigParameterSection(UMovieSceneControlRigParameterSection&&); \
	NO_API UMovieSceneControlRigParameterSection(const UMovieSceneControlRigParameterSection&); \
public:


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterSection_h_145_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMovieSceneControlRigParameterSection(UMovieSceneControlRigParameterSection&&); \
	NO_API UMovieSceneControlRigParameterSection(const UMovieSceneControlRigParameterSection&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMovieSceneControlRigParameterSection); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneControlRigParameterSection); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMovieSceneControlRigParameterSection)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterSection_h_145_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ControlRig() { return STRUCT_OFFSET(UMovieSceneControlRigParameterSection, ControlRig); } \
	FORCEINLINE static uint32 __PPO__EnumParameterNamesAndCurves() { return STRUCT_OFFSET(UMovieSceneControlRigParameterSection, EnumParameterNamesAndCurves); } \
	FORCEINLINE static uint32 __PPO__IntegerParameterNamesAndCurves() { return STRUCT_OFFSET(UMovieSceneControlRigParameterSection, IntegerParameterNamesAndCurves); }


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterSection_h_142_PROLOG
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterSection_h_145_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterSection_h_145_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterSection_h_145_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterSection_h_145_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterSection_h_145_INCLASS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterSection_h_145_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterSection_h_145_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterSection_h_145_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterSection_h_145_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterSection_h_145_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterSection_h_145_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterSection_h_145_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONTROLRIG_API UClass* StaticClass<class UMovieSceneControlRigParameterSection>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterSection_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
