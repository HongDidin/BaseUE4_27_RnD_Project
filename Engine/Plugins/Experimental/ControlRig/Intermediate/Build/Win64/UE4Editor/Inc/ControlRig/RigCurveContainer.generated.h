// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigCurveContainer_generated_h
#error "RigCurveContainer.generated.h already included, missing '#pragma once' in RigCurveContainer.h"
#endif
#define CONTROLRIG_RigCurveContainer_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_RigCurveContainer_h_38_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigCurveContainer_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__Curves() { return STRUCT_OFFSET(FRigCurveContainer, Curves); } \
	FORCEINLINE static uint32 __PPO__NameToIndexMapping() { return STRUCT_OFFSET(FRigCurveContainer, NameToIndexMapping); } \
	FORCEINLINE static uint32 __PPO__Selection() { return STRUCT_OFFSET(FRigCurveContainer, Selection); }


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigCurveContainer>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_RigCurveContainer_h_17_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigCurve_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	typedef FRigElement Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigCurve>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_RigCurveContainer_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
