// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Public/Rigs/RigHierarchyContainer.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigHierarchyContainer() {}
// Cross Module References
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_ERigHierarchyImportMode();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigHierarchyRef();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigHierarchyContainer();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigBoneHierarchy();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigSpaceHierarchy();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigControlHierarchy();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigCurveContainer();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigMirrorSettings();
	COREUOBJECT_API UEnum* Z_Construct_UEnum_CoreUObject_EAxis();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent();
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_ERigElementType();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
// End Cross Module References
	static UEnum* ERigHierarchyImportMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ControlRig_ERigHierarchyImportMode, Z_Construct_UPackage__Script_ControlRig(), TEXT("ERigHierarchyImportMode"));
		}
		return Singleton;
	}
	template<> CONTROLRIG_API UEnum* StaticEnum<ERigHierarchyImportMode>()
	{
		return ERigHierarchyImportMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ERigHierarchyImportMode(ERigHierarchyImportMode_StaticEnum, TEXT("/Script/ControlRig"), TEXT("ERigHierarchyImportMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ControlRig_ERigHierarchyImportMode_Hash() { return 3866658265U; }
	UEnum* Z_Construct_UEnum_ControlRig_ERigHierarchyImportMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ERigHierarchyImportMode"), 0, Get_Z_Construct_UEnum_ControlRig_ERigHierarchyImportMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ERigHierarchyImportMode::Append", (int64)ERigHierarchyImportMode::Append },
				{ "ERigHierarchyImportMode::Replace", (int64)ERigHierarchyImportMode::Replace },
				{ "ERigHierarchyImportMode::ReplaceLocalTransform", (int64)ERigHierarchyImportMode::ReplaceLocalTransform },
				{ "ERigHierarchyImportMode::ReplaceGlobalTransform", (int64)ERigHierarchyImportMode::ReplaceGlobalTransform },
				{ "ERigHierarchyImportMode::Max", (int64)ERigHierarchyImportMode::Max },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Append.Name", "ERigHierarchyImportMode::Append" },
				{ "BlueprintType", "true" },
				{ "Max.Comment", "/** MAX - invalid */" },
				{ "Max.Hidden", "" },
				{ "Max.Name", "ERigHierarchyImportMode::Max" },
				{ "Max.ToolTip", "MAX - invalid" },
				{ "ModuleRelativePath", "Public/Rigs/RigHierarchyContainer.h" },
				{ "Replace.Name", "ERigHierarchyImportMode::Replace" },
				{ "ReplaceGlobalTransform.Name", "ERigHierarchyImportMode::ReplaceGlobalTransform" },
				{ "ReplaceLocalTransform.Name", "ERigHierarchyImportMode::ReplaceLocalTransform" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ControlRig,
				nullptr,
				"ERigHierarchyImportMode",
				"ERigHierarchyImportMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FRigHierarchyRef::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigHierarchyRef_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigHierarchyRef, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigHierarchyRef"), sizeof(FRigHierarchyRef), Get_Z_Construct_UScriptStruct_FRigHierarchyRef_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigHierarchyRef>()
{
	return FRigHierarchyRef::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigHierarchyRef(FRigHierarchyRef::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigHierarchyRef"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigHierarchyRef
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigHierarchyRef()
	{
		UScriptStruct::DeferCppStructOps<FRigHierarchyRef>(FName(TEXT("RigHierarchyRef")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigHierarchyRef;
	struct Z_Construct_UScriptStruct_FRigHierarchyRef_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigHierarchyRef_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "// this struct is still here for backwards compatibility - but not used anywhere\n" },
		{ "ModuleRelativePath", "Public/Rigs/RigHierarchyContainer.h" },
		{ "ToolTip", "this struct is still here for backwards compatibility - but not used anywhere" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigHierarchyRef_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigHierarchyRef>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigHierarchyRef_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"RigHierarchyRef",
		sizeof(FRigHierarchyRef),
		alignof(FRigHierarchyRef),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigHierarchyRef_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigHierarchyRef_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigHierarchyRef()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigHierarchyRef_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigHierarchyRef"), sizeof(FRigHierarchyRef), Get_Z_Construct_UScriptStruct_FRigHierarchyRef_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigHierarchyRef_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigHierarchyRef_Hash() { return 2383548839U; }
class UScriptStruct* FRigHierarchyContainer::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigHierarchyContainer_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigHierarchyContainer, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigHierarchyContainer"), sizeof(FRigHierarchyContainer), Get_Z_Construct_UScriptStruct_FRigHierarchyContainer_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigHierarchyContainer>()
{
	return FRigHierarchyContainer::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigHierarchyContainer(FRigHierarchyContainer::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigHierarchyContainer"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigHierarchyContainer
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigHierarchyContainer()
	{
		UScriptStruct::DeferCppStructOps<FRigHierarchyContainer>(FName(TEXT("RigHierarchyContainer")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigHierarchyContainer;
	struct Z_Construct_UScriptStruct_FRigHierarchyContainer_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoneHierarchy_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BoneHierarchy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpaceHierarchy_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SpaceHierarchy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ControlHierarchy_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ControlHierarchy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurveContainer_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CurveContainer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Version_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Version;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigHierarchyContainer_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Rigs/RigHierarchyContainer.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigHierarchyContainer_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigHierarchyContainer>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigHierarchyContainer_Statics::NewProp_BoneHierarchy_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/RigHierarchyContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigHierarchyContainer_Statics::NewProp_BoneHierarchy = { "BoneHierarchy", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigHierarchyContainer, BoneHierarchy), Z_Construct_UScriptStruct_FRigBoneHierarchy, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigHierarchyContainer_Statics::NewProp_BoneHierarchy_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigHierarchyContainer_Statics::NewProp_BoneHierarchy_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigHierarchyContainer_Statics::NewProp_SpaceHierarchy_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/RigHierarchyContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigHierarchyContainer_Statics::NewProp_SpaceHierarchy = { "SpaceHierarchy", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigHierarchyContainer, SpaceHierarchy), Z_Construct_UScriptStruct_FRigSpaceHierarchy, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigHierarchyContainer_Statics::NewProp_SpaceHierarchy_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigHierarchyContainer_Statics::NewProp_SpaceHierarchy_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigHierarchyContainer_Statics::NewProp_ControlHierarchy_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/RigHierarchyContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigHierarchyContainer_Statics::NewProp_ControlHierarchy = { "ControlHierarchy", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigHierarchyContainer, ControlHierarchy), Z_Construct_UScriptStruct_FRigControlHierarchy, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigHierarchyContainer_Statics::NewProp_ControlHierarchy_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigHierarchyContainer_Statics::NewProp_ControlHierarchy_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigHierarchyContainer_Statics::NewProp_CurveContainer_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/RigHierarchyContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigHierarchyContainer_Statics::NewProp_CurveContainer = { "CurveContainer", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigHierarchyContainer, CurveContainer), Z_Construct_UScriptStruct_FRigCurveContainer, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigHierarchyContainer_Statics::NewProp_CurveContainer_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigHierarchyContainer_Statics::NewProp_CurveContainer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigHierarchyContainer_Statics::NewProp_Version_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/RigHierarchyContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigHierarchyContainer_Statics::NewProp_Version = { "Version", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigHierarchyContainer, Version), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigHierarchyContainer_Statics::NewProp_Version_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigHierarchyContainer_Statics::NewProp_Version_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigHierarchyContainer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigHierarchyContainer_Statics::NewProp_BoneHierarchy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigHierarchyContainer_Statics::NewProp_SpaceHierarchy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigHierarchyContainer_Statics::NewProp_ControlHierarchy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigHierarchyContainer_Statics::NewProp_CurveContainer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigHierarchyContainer_Statics::NewProp_Version,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigHierarchyContainer_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"RigHierarchyContainer",
		sizeof(FRigHierarchyContainer),
		alignof(FRigHierarchyContainer),
		Z_Construct_UScriptStruct_FRigHierarchyContainer_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigHierarchyContainer_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigHierarchyContainer_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigHierarchyContainer_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigHierarchyContainer()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigHierarchyContainer_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigHierarchyContainer"), sizeof(FRigHierarchyContainer), Get_Z_Construct_UScriptStruct_FRigHierarchyContainer_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigHierarchyContainer_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigHierarchyContainer_Hash() { return 2362057104U; }
class UScriptStruct* FRigMirrorSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigMirrorSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigMirrorSettings, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigMirrorSettings"), sizeof(FRigMirrorSettings), Get_Z_Construct_UScriptStruct_FRigMirrorSettings_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigMirrorSettings>()
{
	return FRigMirrorSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigMirrorSettings(FRigMirrorSettings::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigMirrorSettings"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigMirrorSettings
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigMirrorSettings()
	{
		UScriptStruct::DeferCppStructOps<FRigMirrorSettings>(FName(TEXT("RigMirrorSettings")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigMirrorSettings;
	struct Z_Construct_UScriptStruct_FRigMirrorSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MirrorAxis_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_MirrorAxis;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AxisToFlip_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_AxisToFlip;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OldName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_OldName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_NewName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigMirrorSettings_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Rigs/RigHierarchyContainer.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigMirrorSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigMirrorSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigMirrorSettings_Statics::NewProp_MirrorAxis_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "// the axis to mirror against\n" },
		{ "ModuleRelativePath", "Public/Rigs/RigHierarchyContainer.h" },
		{ "ToolTip", "the axis to mirror against" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRigMirrorSettings_Statics::NewProp_MirrorAxis = { "MirrorAxis", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigMirrorSettings, MirrorAxis), Z_Construct_UEnum_CoreUObject_EAxis, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigMirrorSettings_Statics::NewProp_MirrorAxis_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigMirrorSettings_Statics::NewProp_MirrorAxis_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigMirrorSettings_Statics::NewProp_AxisToFlip_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "// the axis to flip for rotations\n" },
		{ "ModuleRelativePath", "Public/Rigs/RigHierarchyContainer.h" },
		{ "ToolTip", "the axis to flip for rotations" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRigMirrorSettings_Statics::NewProp_AxisToFlip = { "AxisToFlip", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigMirrorSettings, AxisToFlip), Z_Construct_UEnum_CoreUObject_EAxis, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigMirrorSettings_Statics::NewProp_AxisToFlip_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigMirrorSettings_Statics::NewProp_AxisToFlip_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigMirrorSettings_Statics::NewProp_OldName_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "// the string to replace all occurences of with New Name\n" },
		{ "ModuleRelativePath", "Public/Rigs/RigHierarchyContainer.h" },
		{ "ToolTip", "the string to replace all occurences of with New Name" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRigMirrorSettings_Statics::NewProp_OldName = { "OldName", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigMirrorSettings, OldName), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigMirrorSettings_Statics::NewProp_OldName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigMirrorSettings_Statics::NewProp_OldName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigMirrorSettings_Statics::NewProp_NewName_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "// the string to replace all occurences of Old Name with\n" },
		{ "ModuleRelativePath", "Public/Rigs/RigHierarchyContainer.h" },
		{ "ToolTip", "the string to replace all occurences of Old Name with" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRigMirrorSettings_Statics::NewProp_NewName = { "NewName", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigMirrorSettings, NewName), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigMirrorSettings_Statics::NewProp_NewName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigMirrorSettings_Statics::NewProp_NewName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigMirrorSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigMirrorSettings_Statics::NewProp_MirrorAxis,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigMirrorSettings_Statics::NewProp_AxisToFlip,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigMirrorSettings_Statics::NewProp_OldName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigMirrorSettings_Statics::NewProp_NewName,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigMirrorSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"RigMirrorSettings",
		sizeof(FRigMirrorSettings),
		alignof(FRigMirrorSettings),
		Z_Construct_UScriptStruct_FRigMirrorSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigMirrorSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigMirrorSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigMirrorSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigMirrorSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigMirrorSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigMirrorSettings"), sizeof(FRigMirrorSettings), Get_Z_Construct_UScriptStruct_FRigMirrorSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigMirrorSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigMirrorSettings_Hash() { return 1561262133U; }
class UScriptStruct* FRigHierarchyCopyPasteContent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigHierarchyCopyPasteContent"), sizeof(FRigHierarchyCopyPasteContent), Get_Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigHierarchyCopyPasteContent>()
{
	return FRigHierarchyCopyPasteContent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigHierarchyCopyPasteContent(FRigHierarchyCopyPasteContent::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigHierarchyCopyPasteContent"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigHierarchyCopyPasteContent
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigHierarchyCopyPasteContent()
	{
		UScriptStruct::DeferCppStructOps<FRigHierarchyCopyPasteContent>(FName(TEXT("RigHierarchyCopyPasteContent")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigHierarchyCopyPasteContent;
	struct Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Types_Inner_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Types_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Types_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Types;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Contents_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Contents_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Contents;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LocalTransforms_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LocalTransforms_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_LocalTransforms;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GlobalTransforms_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GlobalTransforms_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_GlobalTransforms;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Rigs/RigHierarchyContainer.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigHierarchyCopyPasteContent>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Statics::NewProp_Types_Inner_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Statics::NewProp_Types_Inner = { "Types", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UEnum_ControlRig_ERigElementType, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Statics::NewProp_Types_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/RigHierarchyContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Statics::NewProp_Types = { "Types", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigHierarchyCopyPasteContent, Types), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Statics::NewProp_Types_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Statics::NewProp_Types_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Statics::NewProp_Contents_Inner = { "Contents", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Statics::NewProp_Contents_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/RigHierarchyContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Statics::NewProp_Contents = { "Contents", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigHierarchyCopyPasteContent, Contents), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Statics::NewProp_Contents_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Statics::NewProp_Contents_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Statics::NewProp_LocalTransforms_Inner = { "LocalTransforms", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Statics::NewProp_LocalTransforms_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/RigHierarchyContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Statics::NewProp_LocalTransforms = { "LocalTransforms", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigHierarchyCopyPasteContent, LocalTransforms), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Statics::NewProp_LocalTransforms_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Statics::NewProp_LocalTransforms_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Statics::NewProp_GlobalTransforms_Inner = { "GlobalTransforms", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Statics::NewProp_GlobalTransforms_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/RigHierarchyContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Statics::NewProp_GlobalTransforms = { "GlobalTransforms", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigHierarchyCopyPasteContent, GlobalTransforms), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Statics::NewProp_GlobalTransforms_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Statics::NewProp_GlobalTransforms_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Statics::NewProp_Types_Inner_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Statics::NewProp_Types_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Statics::NewProp_Types,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Statics::NewProp_Contents_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Statics::NewProp_Contents,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Statics::NewProp_LocalTransforms_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Statics::NewProp_LocalTransforms,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Statics::NewProp_GlobalTransforms_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Statics::NewProp_GlobalTransforms,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"RigHierarchyCopyPasteContent",
		sizeof(FRigHierarchyCopyPasteContent),
		alignof(FRigHierarchyCopyPasteContent),
		Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigHierarchyCopyPasteContent"), sizeof(FRigHierarchyCopyPasteContent), Get_Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigHierarchyCopyPasteContent_Hash() { return 1286871520U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
