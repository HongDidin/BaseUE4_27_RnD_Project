// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigUnit_SpringIK_generated_h
#error "RigUnit_SpringIK.generated.h already included, missing '#pragma once' in RigUnit_SpringIK.h"
#endif
#define CONTROLRIG_RigUnit_SpringIK_generated_h


#define FRigUnit_SpringIK_Execute() \
	void FRigUnit_SpringIK::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& StartBone, \
		const FName& EndBone, \
		const float HierarchyStrength, \
		const float EffectorStrength, \
		const float EffectorRatio, \
		const float RootStrength, \
		const float RootRatio, \
		const float Damping, \
		const FVector& PoleVector, \
		const bool bFlipPolePlane, \
		const EControlRigVectorKind PoleVectorKind, \
		const FName& PoleVectorSpace, \
		const FVector& PrimaryAxis, \
		const FVector& SecondaryAxis, \
		const bool bLiveSimulation, \
		const int32 Iterations, \
		const bool bLimitLocalPosition, \
		const bool bPropagateToChildren, \
		const FRigUnit_SpringIK_DebugSettings& DebugSettings, \
		FRigUnit_SpringIK_WorkData& WorkData, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_SpringIK_h_78_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_SpringIK_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& StartBone, \
		const FName& EndBone, \
		const float HierarchyStrength, \
		const float EffectorStrength, \
		const float EffectorRatio, \
		const float RootStrength, \
		const float RootRatio, \
		const float Damping, \
		const FVector& PoleVector, \
		const bool bFlipPolePlane, \
		const EControlRigVectorKind PoleVectorKind, \
		const FName& PoleVectorSpace, \
		const FVector& PrimaryAxis, \
		const FVector& SecondaryAxis, \
		const bool bLiveSimulation, \
		const int32 Iterations, \
		const bool bLimitLocalPosition, \
		const bool bPropagateToChildren, \
		const FRigUnit_SpringIK_DebugSettings& DebugSettings, \
		FRigUnit_SpringIK_WorkData& WorkData, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FName& StartBone = *(FName*)RigVMMemoryHandles[0].GetData(); \
		const FName& EndBone = *(FName*)RigVMMemoryHandles[1].GetData(); \
		const float HierarchyStrength = *(float*)RigVMMemoryHandles[2].GetData(); \
		const float EffectorStrength = *(float*)RigVMMemoryHandles[3].GetData(); \
		const float EffectorRatio = *(float*)RigVMMemoryHandles[4].GetData(); \
		const float RootStrength = *(float*)RigVMMemoryHandles[5].GetData(); \
		const float RootRatio = *(float*)RigVMMemoryHandles[6].GetData(); \
		const float Damping = *(float*)RigVMMemoryHandles[7].GetData(); \
		const FVector& PoleVector = *(FVector*)RigVMMemoryHandles[8].GetData(); \
		const bool bFlipPolePlane = *(bool*)RigVMMemoryHandles[9].GetData(); \
		EControlRigVectorKind PoleVectorKind = (EControlRigVectorKind)*(uint8*)RigVMMemoryHandles[10].GetData(); \
		const FName& PoleVectorSpace = *(FName*)RigVMMemoryHandles[11].GetData(); \
		const FVector& PrimaryAxis = *(FVector*)RigVMMemoryHandles[12].GetData(); \
		const FVector& SecondaryAxis = *(FVector*)RigVMMemoryHandles[13].GetData(); \
		const bool bLiveSimulation = *(bool*)RigVMMemoryHandles[14].GetData(); \
		const int32 Iterations = *(int32*)RigVMMemoryHandles[15].GetData(); \
		const bool bLimitLocalPosition = *(bool*)RigVMMemoryHandles[16].GetData(); \
		const bool bPropagateToChildren = *(bool*)RigVMMemoryHandles[17].GetData(); \
		const FRigUnit_SpringIK_DebugSettings& DebugSettings = *(FRigUnit_SpringIK_DebugSettings*)RigVMMemoryHandles[18].GetData(); \
		FRigVMDynamicArray<FRigUnit_SpringIK_WorkData> WorkData_19_Array(*((FRigVMByteArray*)RigVMMemoryHandles[19].GetData(0, false))); \
		WorkData_19_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FRigUnit_SpringIK_WorkData& WorkData = WorkData_19_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[20].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			StartBone, \
			EndBone, \
			HierarchyStrength, \
			EffectorStrength, \
			EffectorRatio, \
			RootStrength, \
			RootRatio, \
			Damping, \
			PoleVector, \
			bFlipPolePlane, \
			PoleVectorKind, \
			PoleVectorSpace, \
			PrimaryAxis, \
			SecondaryAxis, \
			bLiveSimulation, \
			Iterations, \
			bLimitLocalPosition, \
			bPropagateToChildren, \
			DebugSettings, \
			WorkData, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnit_HighlevelBaseMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_SpringIK>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_SpringIK_h_50_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_SpringIK_WorkData_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_SpringIK_WorkData>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_SpringIK_h_12_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_SpringIK_DebugSettings_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_SpringIK_DebugSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_SpringIK_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
