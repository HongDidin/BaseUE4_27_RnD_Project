// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigUnit_ProfilingBracket_generated_h
#error "RigUnit_ProfilingBracket.generated.h already included, missing '#pragma once' in RigUnit_ProfilingBracket.h"
#endif
#define CONTROLRIG_RigUnit_ProfilingBracket_generated_h


#define FRigUnit_EndProfilingTimer_Execute() \
	void FRigUnit_EndProfilingTimer::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const int32 NumberOfMeasurements, \
		const FString& Prefix, \
		float& AccumulatedTime, \
		int32& MeasurementsLeft, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Debug_RigUnit_ProfilingBracket_h_30_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_EndProfilingTimer_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const int32 NumberOfMeasurements, \
		const FString& Prefix, \
		float& AccumulatedTime, \
		int32& MeasurementsLeft, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const int32 NumberOfMeasurements = *(int32*)RigVMMemoryHandles[0].GetData(); \
		const FString& Prefix = *(FString*)RigVMMemoryHandles[1].GetData(); \
		FRigVMDynamicArray<float> AccumulatedTime_2_Array(*((FRigVMByteArray*)RigVMMemoryHandles[2].GetData(0, false))); \
		AccumulatedTime_2_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		float& AccumulatedTime = AccumulatedTime_2_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<int32> MeasurementsLeft_3_Array(*((FRigVMByteArray*)RigVMMemoryHandles[3].GetData(0, false))); \
		MeasurementsLeft_3_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		int32& MeasurementsLeft = MeasurementsLeft_3_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[4].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			NumberOfMeasurements, \
			Prefix, \
			AccumulatedTime, \
			MeasurementsLeft, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnit_DebugBaseMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_EndProfilingTimer>();


#define FRigUnit_StartProfilingTimer_Execute() \
	void FRigUnit_StartProfilingTimer::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Debug_RigUnit_ProfilingBracket_h_14_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_StartProfilingTimer_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[0].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnit_DebugBaseMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_StartProfilingTimer>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Debug_RigUnit_ProfilingBracket_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
