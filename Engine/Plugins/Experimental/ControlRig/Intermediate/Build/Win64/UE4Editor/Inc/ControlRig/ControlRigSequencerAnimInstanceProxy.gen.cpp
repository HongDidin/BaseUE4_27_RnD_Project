// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Sequencer/ControlRigSequencerAnimInstanceProxy.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeControlRigSequencerAnimInstanceProxy() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FControlRigSequencerAnimInstanceProxy();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	ANIMGRAPHRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FAnimSequencerInstanceProxy();
// End Cross Module References

static_assert(std::is_polymorphic<FControlRigSequencerAnimInstanceProxy>() == std::is_polymorphic<FAnimSequencerInstanceProxy>(), "USTRUCT FControlRigSequencerAnimInstanceProxy cannot be polymorphic unless super FAnimSequencerInstanceProxy is polymorphic");

class UScriptStruct* FControlRigSequencerAnimInstanceProxy::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FControlRigSequencerAnimInstanceProxy_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FControlRigSequencerAnimInstanceProxy, Z_Construct_UPackage__Script_ControlRig(), TEXT("ControlRigSequencerAnimInstanceProxy"), sizeof(FControlRigSequencerAnimInstanceProxy), Get_Z_Construct_UScriptStruct_FControlRigSequencerAnimInstanceProxy_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FControlRigSequencerAnimInstanceProxy>()
{
	return FControlRigSequencerAnimInstanceProxy::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FControlRigSequencerAnimInstanceProxy(FControlRigSequencerAnimInstanceProxy::StaticStruct, TEXT("/Script/ControlRig"), TEXT("ControlRigSequencerAnimInstanceProxy"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFControlRigSequencerAnimInstanceProxy
{
	FScriptStruct_ControlRig_StaticRegisterNativesFControlRigSequencerAnimInstanceProxy()
	{
		UScriptStruct::DeferCppStructOps<FControlRigSequencerAnimInstanceProxy>(FName(TEXT("ControlRigSequencerAnimInstanceProxy")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFControlRigSequencerAnimInstanceProxy;
	struct Z_Construct_UScriptStruct_FControlRigSequencerAnimInstanceProxy_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FControlRigSequencerAnimInstanceProxy_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Proxy that manages adding animation ControlRig nodes as well as acting as a regular sequencer proxy */" },
		{ "ModuleRelativePath", "Private/Sequencer/ControlRigSequencerAnimInstanceProxy.h" },
		{ "ToolTip", "Proxy that manages adding animation ControlRig nodes as well as acting as a regular sequencer proxy" },
	};
#endif
	void* Z_Construct_UScriptStruct_FControlRigSequencerAnimInstanceProxy_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FControlRigSequencerAnimInstanceProxy>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FControlRigSequencerAnimInstanceProxy_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FAnimSequencerInstanceProxy,
		&NewStructOps,
		"ControlRigSequencerAnimInstanceProxy",
		sizeof(FControlRigSequencerAnimInstanceProxy),
		alignof(FControlRigSequencerAnimInstanceProxy),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FControlRigSequencerAnimInstanceProxy_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FControlRigSequencerAnimInstanceProxy_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FControlRigSequencerAnimInstanceProxy()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FControlRigSequencerAnimInstanceProxy_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ControlRigSequencerAnimInstanceProxy"), sizeof(FControlRigSequencerAnimInstanceProxy), Get_Z_Construct_UScriptStruct_FControlRigSequencerAnimInstanceProxy_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FControlRigSequencerAnimInstanceProxy_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FControlRigSequencerAnimInstanceProxy_Hash() { return 2260951292U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
