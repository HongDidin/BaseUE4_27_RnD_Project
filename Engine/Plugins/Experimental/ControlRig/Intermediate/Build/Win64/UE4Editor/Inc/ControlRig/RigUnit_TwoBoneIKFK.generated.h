// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigUnit_TwoBoneIKFK_generated_h
#error "RigUnit_TwoBoneIKFK.generated.h already included, missing '#pragma once' in RigUnit_TwoBoneIKFK.h"
#endif
#define CONTROLRIG_RigUnit_TwoBoneIKFK_generated_h


#define FRigUnit_TwoBoneIKFK_Execute() \
	void FRigUnit_TwoBoneIKFK::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& StartJoint, \
		const FName& EndJoint, \
		const FVector& PoleTarget, \
		const float Spin, \
		const FTransform& EndEffector, \
		const float IKBlend, \
		FTransform& StartJointFKTransform, \
		FTransform& MidJointFKTransform, \
		FTransform& EndJointFKTransform, \
		float& PreviousFKIKBlend, \
		FTransform& StartJointIKTransform, \
		FTransform& MidJointIKTransform, \
		FTransform& EndJointIKTransform, \
		int32& StartJointIndex, \
		int32& MidJointIndex, \
		int32& EndJointIndex, \
		float& UpperLimbLength, \
		float& LowerLimbLength, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Units_Deprecated_RigUnit_TwoBoneIKFK_h_17_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKFK_Statics; \
	static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& StartJoint, \
		const FName& EndJoint, \
		const FVector& PoleTarget, \
		const float Spin, \
		const FTransform& EndEffector, \
		const float IKBlend, \
		FTransform& StartJointFKTransform, \
		FTransform& MidJointFKTransform, \
		FTransform& EndJointFKTransform, \
		float& PreviousFKIKBlend, \
		FTransform& StartJointIKTransform, \
		FTransform& MidJointIKTransform, \
		FTransform& EndJointIKTransform, \
		int32& StartJointIndex, \
		int32& MidJointIndex, \
		int32& EndJointIndex, \
		float& UpperLimbLength, \
		float& LowerLimbLength, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FName& StartJoint = *(FName*)RigVMMemoryHandles[0].GetData(); \
		const FName& EndJoint = *(FName*)RigVMMemoryHandles[1].GetData(); \
		const FVector& PoleTarget = *(FVector*)RigVMMemoryHandles[2].GetData(); \
		const float Spin = *(float*)RigVMMemoryHandles[3].GetData(); \
		const FTransform& EndEffector = *(FTransform*)RigVMMemoryHandles[4].GetData(); \
		const float IKBlend = *(float*)RigVMMemoryHandles[5].GetData(); \
		FRigVMDynamicArray<FTransform> StartJointFKTransform_6_Array(*((FRigVMByteArray*)RigVMMemoryHandles[6].GetData(0, false))); \
		StartJointFKTransform_6_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FTransform& StartJointFKTransform = StartJointFKTransform_6_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<FTransform> MidJointFKTransform_7_Array(*((FRigVMByteArray*)RigVMMemoryHandles[7].GetData(0, false))); \
		MidJointFKTransform_7_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FTransform& MidJointFKTransform = MidJointFKTransform_7_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<FTransform> EndJointFKTransform_8_Array(*((FRigVMByteArray*)RigVMMemoryHandles[8].GetData(0, false))); \
		EndJointFKTransform_8_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FTransform& EndJointFKTransform = EndJointFKTransform_8_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<float> PreviousFKIKBlend_9_Array(*((FRigVMByteArray*)RigVMMemoryHandles[9].GetData(0, false))); \
		PreviousFKIKBlend_9_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		float& PreviousFKIKBlend = PreviousFKIKBlend_9_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<FTransform> StartJointIKTransform_10_Array(*((FRigVMByteArray*)RigVMMemoryHandles[10].GetData(0, false))); \
		StartJointIKTransform_10_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FTransform& StartJointIKTransform = StartJointIKTransform_10_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<FTransform> MidJointIKTransform_11_Array(*((FRigVMByteArray*)RigVMMemoryHandles[11].GetData(0, false))); \
		MidJointIKTransform_11_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FTransform& MidJointIKTransform = MidJointIKTransform_11_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<FTransform> EndJointIKTransform_12_Array(*((FRigVMByteArray*)RigVMMemoryHandles[12].GetData(0, false))); \
		EndJointIKTransform_12_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FTransform& EndJointIKTransform = EndJointIKTransform_12_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<int32> StartJointIndex_13_Array(*((FRigVMByteArray*)RigVMMemoryHandles[13].GetData(0, false))); \
		StartJointIndex_13_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		int32& StartJointIndex = StartJointIndex_13_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<int32> MidJointIndex_14_Array(*((FRigVMByteArray*)RigVMMemoryHandles[14].GetData(0, false))); \
		MidJointIndex_14_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		int32& MidJointIndex = MidJointIndex_14_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<int32> EndJointIndex_15_Array(*((FRigVMByteArray*)RigVMMemoryHandles[15].GetData(0, false))); \
		EndJointIndex_15_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		int32& EndJointIndex = EndJointIndex_15_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<float> UpperLimbLength_16_Array(*((FRigVMByteArray*)RigVMMemoryHandles[16].GetData(0, false))); \
		UpperLimbLength_16_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		float& UpperLimbLength = UpperLimbLength_16_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<float> LowerLimbLength_17_Array(*((FRigVMByteArray*)RigVMMemoryHandles[17].GetData(0, false))); \
		LowerLimbLength_17_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		float& LowerLimbLength = LowerLimbLength_17_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[18].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			StartJoint, \
			EndJoint, \
			PoleTarget, \
			Spin, \
			EndEffector, \
			IKBlend, \
			StartJointFKTransform, \
			MidJointFKTransform, \
			EndJointFKTransform, \
			PreviousFKIKBlend, \
			StartJointIKTransform, \
			MidJointIKTransform, \
			EndJointIKTransform, \
			StartJointIndex, \
			MidJointIndex, \
			EndJointIndex, \
			UpperLimbLength, \
			LowerLimbLength, \
			ExecuteContext, \
			Context \
		); \
	} \
	FORCEINLINE static uint32 __PPO__StartJointFKTransform() { return STRUCT_OFFSET(FRigUnit_TwoBoneIKFK, StartJointFKTransform); } \
	FORCEINLINE static uint32 __PPO__MidJointFKTransform() { return STRUCT_OFFSET(FRigUnit_TwoBoneIKFK, MidJointFKTransform); } \
	FORCEINLINE static uint32 __PPO__EndJointFKTransform() { return STRUCT_OFFSET(FRigUnit_TwoBoneIKFK, EndJointFKTransform); } \
	FORCEINLINE static uint32 __PPO__PreviousFKIKBlend() { return STRUCT_OFFSET(FRigUnit_TwoBoneIKFK, PreviousFKIKBlend); } \
	FORCEINLINE static uint32 __PPO__StartJointIKTransform() { return STRUCT_OFFSET(FRigUnit_TwoBoneIKFK, StartJointIKTransform); } \
	FORCEINLINE static uint32 __PPO__MidJointIKTransform() { return STRUCT_OFFSET(FRigUnit_TwoBoneIKFK, MidJointIKTransform); } \
	FORCEINLINE static uint32 __PPO__EndJointIKTransform() { return STRUCT_OFFSET(FRigUnit_TwoBoneIKFK, EndJointIKTransform); } \
	FORCEINLINE static uint32 __PPO__StartJointIndex() { return STRUCT_OFFSET(FRigUnit_TwoBoneIKFK, StartJointIndex); } \
	FORCEINLINE static uint32 __PPO__MidJointIndex() { return STRUCT_OFFSET(FRigUnit_TwoBoneIKFK, MidJointIndex); } \
	FORCEINLINE static uint32 __PPO__EndJointIndex() { return STRUCT_OFFSET(FRigUnit_TwoBoneIKFK, EndJointIndex); } \
	FORCEINLINE static uint32 __PPO__UpperLimbLength() { return STRUCT_OFFSET(FRigUnit_TwoBoneIKFK, UpperLimbLength); } \
	FORCEINLINE static uint32 __PPO__LowerLimbLength() { return STRUCT_OFFSET(FRigUnit_TwoBoneIKFK, LowerLimbLength); } \
	typedef FRigUnitMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_TwoBoneIKFK>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Units_Deprecated_RigUnit_TwoBoneIKFK_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
