// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_ControlRigLayerInstance_generated_h
#error "ControlRigLayerInstance.generated.h already included, missing '#pragma once' in ControlRigLayerInstance.h"
#endif
#define CONTROLRIG_ControlRigLayerInstance_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigLayerInstance_h_20_SPARSE_DATA
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigLayerInstance_h_20_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigLayerInstance_h_20_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigLayerInstance_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUControlRigLayerInstance(); \
	friend struct Z_Construct_UClass_UControlRigLayerInstance_Statics; \
public: \
	DECLARE_CLASS(UControlRigLayerInstance, UAnimInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ControlRig"), NO_API) \
	DECLARE_SERIALIZER(UControlRigLayerInstance) \
	virtual UObject* _getUObject() const override { return const_cast<UControlRigLayerInstance*>(this); }


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigLayerInstance_h_20_INCLASS \
private: \
	static void StaticRegisterNativesUControlRigLayerInstance(); \
	friend struct Z_Construct_UClass_UControlRigLayerInstance_Statics; \
public: \
	DECLARE_CLASS(UControlRigLayerInstance, UAnimInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ControlRig"), NO_API) \
	DECLARE_SERIALIZER(UControlRigLayerInstance) \
	virtual UObject* _getUObject() const override { return const_cast<UControlRigLayerInstance*>(this); }


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigLayerInstance_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UControlRigLayerInstance(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigLayerInstance) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigLayerInstance); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigLayerInstance); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigLayerInstance(UControlRigLayerInstance&&); \
	NO_API UControlRigLayerInstance(const UControlRigLayerInstance&); \
public:


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigLayerInstance_h_20_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UControlRigLayerInstance(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigLayerInstance(UControlRigLayerInstance&&); \
	NO_API UControlRigLayerInstance(const UControlRigLayerInstance&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigLayerInstance); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigLayerInstance); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigLayerInstance)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigLayerInstance_h_20_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigLayerInstance_h_17_PROLOG
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigLayerInstance_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigLayerInstance_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigLayerInstance_h_20_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigLayerInstance_h_20_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigLayerInstance_h_20_INCLASS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigLayerInstance_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigLayerInstance_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigLayerInstance_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigLayerInstance_h_20_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigLayerInstance_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigLayerInstance_h_20_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigLayerInstance_h_20_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ControlRigLayerInstance."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONTROLRIG_API UClass* StaticClass<class UControlRigLayerInstance>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_ControlRigLayerInstance_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
