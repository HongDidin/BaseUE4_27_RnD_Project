// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigUnit_TwistBones_generated_h
#error "RigUnit_TwistBones.generated.h already included, missing '#pragma once' in RigUnit_TwistBones.h"
#endif
#define CONTROLRIG_RigUnit_TwistBones_generated_h


#define FRigUnit_TwistBonesPerItem_Execute() \
	void FRigUnit_TwistBonesPerItem::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKeyCollection& Items, \
		const FVector& TwistAxis, \
		const FVector& PoleAxis, \
		const EControlRigAnimEasingType TwistEaseType, \
		const float Weight, \
		const bool bPropagateToChildren, \
		FRigUnit_TwistBones_WorkData& WorkData, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_TwistBones_h_105_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_TwistBonesPerItem_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKeyCollection& Items, \
		const FVector& TwistAxis, \
		const FVector& PoleAxis, \
		const EControlRigAnimEasingType TwistEaseType, \
		const float Weight, \
		const bool bPropagateToChildren, \
		FRigUnit_TwistBones_WorkData& WorkData, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FRigElementKeyCollection& Items = *(FRigElementKeyCollection*)RigVMMemoryHandles[0].GetData(); \
		const FVector& TwistAxis = *(FVector*)RigVMMemoryHandles[1].GetData(); \
		const FVector& PoleAxis = *(FVector*)RigVMMemoryHandles[2].GetData(); \
		EControlRigAnimEasingType TwistEaseType = (EControlRigAnimEasingType)*(uint8*)RigVMMemoryHandles[3].GetData(); \
		const float Weight = *(float*)RigVMMemoryHandles[4].GetData(); \
		const bool bPropagateToChildren = *(bool*)RigVMMemoryHandles[5].GetData(); \
		FRigVMDynamicArray<FRigUnit_TwistBones_WorkData> WorkData_6_Array(*((FRigVMByteArray*)RigVMMemoryHandles[6].GetData(0, false))); \
		WorkData_6_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FRigUnit_TwistBones_WorkData& WorkData = WorkData_6_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[7].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Items, \
			TwistAxis, \
			PoleAxis, \
			TwistEaseType, \
			Weight, \
			bPropagateToChildren, \
			WorkData, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnit_HighlevelBaseMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_TwistBonesPerItem>();


#define FRigUnit_TwistBones_Execute() \
	void FRigUnit_TwistBones::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& StartBone, \
		const FName& EndBone, \
		const FVector& TwistAxis, \
		const FVector& PoleAxis, \
		const EControlRigAnimEasingType TwistEaseType, \
		const float Weight, \
		const bool bPropagateToChildren, \
		FRigUnit_TwistBones_WorkData& WorkData, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_TwistBones_h_31_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_TwistBones_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& StartBone, \
		const FName& EndBone, \
		const FVector& TwistAxis, \
		const FVector& PoleAxis, \
		const EControlRigAnimEasingType TwistEaseType, \
		const float Weight, \
		const bool bPropagateToChildren, \
		FRigUnit_TwistBones_WorkData& WorkData, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FName& StartBone = *(FName*)RigVMMemoryHandles[0].GetData(); \
		const FName& EndBone = *(FName*)RigVMMemoryHandles[1].GetData(); \
		const FVector& TwistAxis = *(FVector*)RigVMMemoryHandles[2].GetData(); \
		const FVector& PoleAxis = *(FVector*)RigVMMemoryHandles[3].GetData(); \
		EControlRigAnimEasingType TwistEaseType = (EControlRigAnimEasingType)*(uint8*)RigVMMemoryHandles[4].GetData(); \
		const float Weight = *(float*)RigVMMemoryHandles[5].GetData(); \
		const bool bPropagateToChildren = *(bool*)RigVMMemoryHandles[6].GetData(); \
		FRigVMDynamicArray<FRigUnit_TwistBones_WorkData> WorkData_7_Array(*((FRigVMByteArray*)RigVMMemoryHandles[7].GetData(0, false))); \
		WorkData_7_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FRigUnit_TwistBones_WorkData& WorkData = WorkData_7_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[8].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			StartBone, \
			EndBone, \
			TwistAxis, \
			PoleAxis, \
			TwistEaseType, \
			Weight, \
			bPropagateToChildren, \
			WorkData, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnit_HighlevelBaseMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_TwistBones>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_TwistBones_h_12_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_TwistBones_WorkData_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_TwistBones_WorkData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_TwistBones_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
