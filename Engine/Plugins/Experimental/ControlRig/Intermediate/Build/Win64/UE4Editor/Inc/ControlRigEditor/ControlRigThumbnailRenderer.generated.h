// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIGEDITOR_ControlRigThumbnailRenderer_generated_h
#error "ControlRigThumbnailRenderer.generated.h already included, missing '#pragma once' in ControlRigThumbnailRenderer.h"
#endif
#define CONTROLRIGEDITOR_ControlRigThumbnailRenderer_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Public_ControlRigThumbnailRenderer_h_23_SPARSE_DATA
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Public_ControlRigThumbnailRenderer_h_23_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Public_ControlRigThumbnailRenderer_h_23_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Public_ControlRigThumbnailRenderer_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUControlRigThumbnailRenderer(); \
	friend struct Z_Construct_UClass_UControlRigThumbnailRenderer_Statics; \
public: \
	DECLARE_CLASS(UControlRigThumbnailRenderer, USkeletalMeshThumbnailRenderer, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ControlRigEditor"), CONTROLRIGEDITOR_API) \
	DECLARE_SERIALIZER(UControlRigThumbnailRenderer)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Public_ControlRigThumbnailRenderer_h_23_INCLASS \
private: \
	static void StaticRegisterNativesUControlRigThumbnailRenderer(); \
	friend struct Z_Construct_UClass_UControlRigThumbnailRenderer_Statics; \
public: \
	DECLARE_CLASS(UControlRigThumbnailRenderer, USkeletalMeshThumbnailRenderer, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ControlRigEditor"), CONTROLRIGEDITOR_API) \
	DECLARE_SERIALIZER(UControlRigThumbnailRenderer)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Public_ControlRigThumbnailRenderer_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	CONTROLRIGEDITOR_API UControlRigThumbnailRenderer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigThumbnailRenderer) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(CONTROLRIGEDITOR_API, UControlRigThumbnailRenderer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigThumbnailRenderer); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	CONTROLRIGEDITOR_API UControlRigThumbnailRenderer(UControlRigThumbnailRenderer&&); \
	CONTROLRIGEDITOR_API UControlRigThumbnailRenderer(const UControlRigThumbnailRenderer&); \
public:


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Public_ControlRigThumbnailRenderer_h_23_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	CONTROLRIGEDITOR_API UControlRigThumbnailRenderer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	CONTROLRIGEDITOR_API UControlRigThumbnailRenderer(UControlRigThumbnailRenderer&&); \
	CONTROLRIGEDITOR_API UControlRigThumbnailRenderer(const UControlRigThumbnailRenderer&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(CONTROLRIGEDITOR_API, UControlRigThumbnailRenderer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigThumbnailRenderer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigThumbnailRenderer)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Public_ControlRigThumbnailRenderer_h_23_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Public_ControlRigThumbnailRenderer_h_20_PROLOG
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Public_ControlRigThumbnailRenderer_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Public_ControlRigThumbnailRenderer_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Public_ControlRigThumbnailRenderer_h_23_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Public_ControlRigThumbnailRenderer_h_23_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Public_ControlRigThumbnailRenderer_h_23_INCLASS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Public_ControlRigThumbnailRenderer_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Public_ControlRigThumbnailRenderer_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Public_ControlRigThumbnailRenderer_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Public_ControlRigThumbnailRenderer_h_23_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Public_ControlRigThumbnailRenderer_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Public_ControlRigThumbnailRenderer_h_23_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Public_ControlRigThumbnailRenderer_h_23_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ControlRigThumbnailRenderer."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONTROLRIGEDITOR_API UClass* StaticClass<class UControlRigThumbnailRenderer>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Public_ControlRigThumbnailRenderer_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
