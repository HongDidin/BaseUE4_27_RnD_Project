// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Public/Math/Simulation/CRSimContainer.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCRSimContainer() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FCRSimContainer();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
// End Cross Module References
class UScriptStruct* FCRSimContainer::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FCRSimContainer_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCRSimContainer, Z_Construct_UPackage__Script_ControlRig(), TEXT("CRSimContainer"), sizeof(FCRSimContainer), Get_Z_Construct_UScriptStruct_FCRSimContainer_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FCRSimContainer>()
{
	return FCRSimContainer::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCRSimContainer(FCRSimContainer::StaticStruct, TEXT("/Script/ControlRig"), TEXT("CRSimContainer"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFCRSimContainer
{
	FScriptStruct_ControlRig_StaticRegisterNativesFCRSimContainer()
	{
		UScriptStruct::DeferCppStructOps<FCRSimContainer>(FName(TEXT("CRSimContainer")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFCRSimContainer;
	struct Z_Construct_UScriptStruct_FCRSimContainer_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimeStep_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TimeStep;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AccumulatedTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AccumulatedTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimeLeftForStep_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TimeLeftForStep;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimContainer_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimContainer.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FCRSimContainer_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FCRSimContainer>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimContainer_Statics::NewProp_TimeStep_MetaData[] = {
		{ "Comment", "/**\n\x09 * The time step used by this container\n\x09 */" },
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimContainer.h" },
		{ "ToolTip", "The time step used by this container" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCRSimContainer_Statics::NewProp_TimeStep = { "TimeStep", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCRSimContainer, TimeStep), METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimContainer_Statics::NewProp_TimeStep_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimContainer_Statics::NewProp_TimeStep_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimContainer_Statics::NewProp_AccumulatedTime_MetaData[] = {
		{ "Comment", "/**\n\x09 * The time step used by this container\n\x09 */" },
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimContainer.h" },
		{ "ToolTip", "The time step used by this container" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCRSimContainer_Statics::NewProp_AccumulatedTime = { "AccumulatedTime", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCRSimContainer, AccumulatedTime), METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimContainer_Statics::NewProp_AccumulatedTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimContainer_Statics::NewProp_AccumulatedTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimContainer_Statics::NewProp_TimeLeftForStep_MetaData[] = {
		{ "Comment", "/**\n\x09 * The time left until the next step\n\x09 */" },
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimContainer.h" },
		{ "ToolTip", "The time left until the next step" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCRSimContainer_Statics::NewProp_TimeLeftForStep = { "TimeLeftForStep", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCRSimContainer, TimeLeftForStep), METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimContainer_Statics::NewProp_TimeLeftForStep_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimContainer_Statics::NewProp_TimeLeftForStep_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FCRSimContainer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimContainer_Statics::NewProp_TimeStep,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimContainer_Statics::NewProp_AccumulatedTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimContainer_Statics::NewProp_TimeLeftForStep,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FCRSimContainer_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"CRSimContainer",
		sizeof(FCRSimContainer),
		alignof(FCRSimContainer),
		Z_Construct_UScriptStruct_FCRSimContainer_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimContainer_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimContainer_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimContainer_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FCRSimContainer()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FCRSimContainer_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CRSimContainer"), sizeof(FCRSimContainer), Get_Z_Construct_UScriptStruct_FCRSimContainer_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FCRSimContainer_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCRSimContainer_Hash() { return 589075402U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
