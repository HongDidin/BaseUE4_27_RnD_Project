// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Debug/RigUnit_ProfilingBracket.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_ProfilingBracket() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_EndProfilingTimer();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_DebugBaseMutable();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_StartProfilingTimer();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_EndProfilingTimer>() == std::is_polymorphic<FRigUnit_DebugBaseMutable>(), "USTRUCT FRigUnit_EndProfilingTimer cannot be polymorphic unless super FRigUnit_DebugBaseMutable is polymorphic");

class UScriptStruct* FRigUnit_EndProfilingTimer::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_EndProfilingTimer_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_EndProfilingTimer, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_EndProfilingTimer"), sizeof(FRigUnit_EndProfilingTimer), Get_Z_Construct_UScriptStruct_FRigUnit_EndProfilingTimer_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_EndProfilingTimer::Execute"), &FRigUnit_EndProfilingTimer::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_EndProfilingTimer>()
{
	return FRigUnit_EndProfilingTimer::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_EndProfilingTimer(FRigUnit_EndProfilingTimer::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_EndProfilingTimer"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_EndProfilingTimer
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_EndProfilingTimer()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_EndProfilingTimer>(FName(TEXT("RigUnit_EndProfilingTimer")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_EndProfilingTimer;
	struct Z_Construct_UScriptStruct_FRigUnit_EndProfilingTimer_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumberOfMeasurements_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumberOfMeasurements;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Prefix_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Prefix;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AccumulatedTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AccumulatedTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeasurementsLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MeasurementsLeft;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_EndProfilingTimer_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Ends an existing profiling timer for debugging, used in conjunction with Start Profiling Timer\n */" },
		{ "DisplayName", "End Profiling Timer" },
		{ "Keywords", "Measure,StopProfiling,Meter,Profile" },
		{ "ModuleRelativePath", "Private/Units/Debug/RigUnit_ProfilingBracket.h" },
		{ "ToolTip", "Ends an existing profiling timer for debugging, used in conjunction with Start Profiling Timer" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_EndProfilingTimer_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_EndProfilingTimer>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_EndProfilingTimer_Statics::NewProp_NumberOfMeasurements_MetaData[] = {
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Debug/RigUnit_ProfilingBracket.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_EndProfilingTimer_Statics::NewProp_NumberOfMeasurements = { "NumberOfMeasurements", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_EndProfilingTimer, NumberOfMeasurements), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_EndProfilingTimer_Statics::NewProp_NumberOfMeasurements_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_EndProfilingTimer_Statics::NewProp_NumberOfMeasurements_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_EndProfilingTimer_Statics::NewProp_Prefix_MetaData[] = {
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Debug/RigUnit_ProfilingBracket.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRigUnit_EndProfilingTimer_Statics::NewProp_Prefix = { "Prefix", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_EndProfilingTimer, Prefix), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_EndProfilingTimer_Statics::NewProp_Prefix_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_EndProfilingTimer_Statics::NewProp_Prefix_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_EndProfilingTimer_Statics::NewProp_AccumulatedTime_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Debug/RigUnit_ProfilingBracket.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_EndProfilingTimer_Statics::NewProp_AccumulatedTime = { "AccumulatedTime", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_EndProfilingTimer, AccumulatedTime), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_EndProfilingTimer_Statics::NewProp_AccumulatedTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_EndProfilingTimer_Statics::NewProp_AccumulatedTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_EndProfilingTimer_Statics::NewProp_MeasurementsLeft_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Debug/RigUnit_ProfilingBracket.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_EndProfilingTimer_Statics::NewProp_MeasurementsLeft = { "MeasurementsLeft", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_EndProfilingTimer, MeasurementsLeft), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_EndProfilingTimer_Statics::NewProp_MeasurementsLeft_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_EndProfilingTimer_Statics::NewProp_MeasurementsLeft_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_EndProfilingTimer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_EndProfilingTimer_Statics::NewProp_NumberOfMeasurements,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_EndProfilingTimer_Statics::NewProp_Prefix,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_EndProfilingTimer_Statics::NewProp_AccumulatedTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_EndProfilingTimer_Statics::NewProp_MeasurementsLeft,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_EndProfilingTimer_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_DebugBaseMutable,
		&NewStructOps,
		"RigUnit_EndProfilingTimer",
		sizeof(FRigUnit_EndProfilingTimer),
		alignof(FRigUnit_EndProfilingTimer),
		Z_Construct_UScriptStruct_FRigUnit_EndProfilingTimer_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_EndProfilingTimer_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_EndProfilingTimer_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_EndProfilingTimer_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_EndProfilingTimer()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_EndProfilingTimer_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_EndProfilingTimer"), sizeof(FRigUnit_EndProfilingTimer), Get_Z_Construct_UScriptStruct_FRigUnit_EndProfilingTimer_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_EndProfilingTimer_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_EndProfilingTimer_Hash() { return 2873684239U; }

void FRigUnit_EndProfilingTimer::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		NumberOfMeasurements,
		Prefix,
		AccumulatedTime,
		MeasurementsLeft,
		ExecuteContext,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_StartProfilingTimer>() == std::is_polymorphic<FRigUnit_DebugBaseMutable>(), "USTRUCT FRigUnit_StartProfilingTimer cannot be polymorphic unless super FRigUnit_DebugBaseMutable is polymorphic");

class UScriptStruct* FRigUnit_StartProfilingTimer::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_StartProfilingTimer_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_StartProfilingTimer, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_StartProfilingTimer"), sizeof(FRigUnit_StartProfilingTimer), Get_Z_Construct_UScriptStruct_FRigUnit_StartProfilingTimer_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_StartProfilingTimer::Execute"), &FRigUnit_StartProfilingTimer::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_StartProfilingTimer>()
{
	return FRigUnit_StartProfilingTimer::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_StartProfilingTimer(FRigUnit_StartProfilingTimer::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_StartProfilingTimer"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_StartProfilingTimer
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_StartProfilingTimer()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_StartProfilingTimer>(FName(TEXT("RigUnit_StartProfilingTimer")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_StartProfilingTimer;
	struct Z_Construct_UScriptStruct_FRigUnit_StartProfilingTimer_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_StartProfilingTimer_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Starts a profiling timer for debugging, used in conjunction with End Profiling Timer\n */" },
		{ "DisplayName", "Start Profiling Timer" },
		{ "Keywords", "Measure,BeginProfiling,Profile" },
		{ "ModuleRelativePath", "Private/Units/Debug/RigUnit_ProfilingBracket.h" },
		{ "ToolTip", "Starts a profiling timer for debugging, used in conjunction with End Profiling Timer" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_StartProfilingTimer_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_StartProfilingTimer>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_StartProfilingTimer_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_DebugBaseMutable,
		&NewStructOps,
		"RigUnit_StartProfilingTimer",
		sizeof(FRigUnit_StartProfilingTimer),
		alignof(FRigUnit_StartProfilingTimer),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_StartProfilingTimer_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_StartProfilingTimer_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_StartProfilingTimer()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_StartProfilingTimer_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_StartProfilingTimer"), sizeof(FRigUnit_StartProfilingTimer), Get_Z_Construct_UScriptStruct_FRigUnit_StartProfilingTimer_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_StartProfilingTimer_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_StartProfilingTimer_Hash() { return 1219451077U; }

void FRigUnit_StartProfilingTimer::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		ExecuteContext,
		Context
	);
}

PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
