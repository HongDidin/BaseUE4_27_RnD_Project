// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRigEditor/Private/Editor/ControlRigEditor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeControlRigEditor() {}
// Cross Module References
	CONTROLRIGEDITOR_API UEnum* Z_Construct_UEnum_ControlRigEditor_EControlRigEditorEventQueue();
	UPackage* Z_Construct_UPackage__Script_ControlRigEditor();
// End Cross Module References
	static UEnum* EControlRigEditorEventQueue_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ControlRigEditor_EControlRigEditorEventQueue, Z_Construct_UPackage__Script_ControlRigEditor(), TEXT("EControlRigEditorEventQueue"));
		}
		return Singleton;
	}
	template<> CONTROLRIGEDITOR_API UEnum* StaticEnum<EControlRigEditorEventQueue>()
	{
		return EControlRigEditorEventQueue_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EControlRigEditorEventQueue(EControlRigEditorEventQueue_StaticEnum, TEXT("/Script/ControlRigEditor"), TEXT("EControlRigEditorEventQueue"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ControlRigEditor_EControlRigEditorEventQueue_Hash() { return 2808904897U; }
	UEnum* Z_Construct_UEnum_ControlRigEditor_EControlRigEditorEventQueue()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRigEditor();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EControlRigEditorEventQueue"), 0, Get_Z_Construct_UEnum_ControlRigEditor_EControlRigEditorEventQueue_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EControlRigEditorEventQueue::Setup", (int64)EControlRigEditorEventQueue::Setup },
				{ "EControlRigEditorEventQueue::Update", (int64)EControlRigEditorEventQueue::Update },
				{ "EControlRigEditorEventQueue::Inverse", (int64)EControlRigEditorEventQueue::Inverse },
				{ "EControlRigEditorEventQueue::InverseAndUpdate", (int64)EControlRigEditorEventQueue::InverseAndUpdate },
				{ "EControlRigEditorEventQueue::Max", (int64)EControlRigEditorEventQueue::Max },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Inverse.Comment", "/** Inverse Event */" },
				{ "Inverse.Name", "EControlRigEditorEventQueue::Inverse" },
				{ "Inverse.ToolTip", "Inverse Event" },
				{ "InverseAndUpdate.Comment", "/** Inverse -> Update */" },
				{ "InverseAndUpdate.Name", "EControlRigEditorEventQueue::InverseAndUpdate" },
				{ "InverseAndUpdate.ToolTip", "Inverse -> Update" },
				{ "Max.Comment", "/** MAX - invalid */" },
				{ "Max.Hidden", "" },
				{ "Max.Name", "EControlRigEditorEventQueue::Max" },
				{ "Max.ToolTip", "MAX - invalid" },
				{ "ModuleRelativePath", "Private/Editor/ControlRigEditor.h" },
				{ "Setup.Comment", "/** Setup Event */" },
				{ "Setup.Name", "EControlRigEditorEventQueue::Setup" },
				{ "Setup.ToolTip", "Setup Event" },
				{ "Update.Comment", "/** Update Event */" },
				{ "Update.Name", "EControlRigEditorEventQueue::Update" },
				{ "Update.ToolTip", "Update Event" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ControlRigEditor,
				nullptr,
				"EControlRigEditorEventQueue",
				"EControlRigEditorEventQueue",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
