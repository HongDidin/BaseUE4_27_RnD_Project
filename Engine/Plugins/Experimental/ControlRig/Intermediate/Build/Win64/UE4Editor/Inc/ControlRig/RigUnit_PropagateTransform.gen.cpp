// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Hierarchy/RigUnit_PropagateTransform.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_PropagateTransform() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_PropagateTransform();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnitMutable();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigElementKey();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FCachedRigElement();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_PropagateTransform>() == std::is_polymorphic<FRigUnitMutable>(), "USTRUCT FRigUnit_PropagateTransform cannot be polymorphic unless super FRigUnitMutable is polymorphic");

class UScriptStruct* FRigUnit_PropagateTransform::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_PropagateTransform, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_PropagateTransform"), sizeof(FRigUnit_PropagateTransform), Get_Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_PropagateTransform::Execute"), &FRigUnit_PropagateTransform::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_PropagateTransform>()
{
	return FRigUnit_PropagateTransform::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_PropagateTransform(FRigUnit_PropagateTransform::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_PropagateTransform"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_PropagateTransform
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_PropagateTransform()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_PropagateTransform>(FName(TEXT("RigUnit_PropagateTransform")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_PropagateTransform;
	struct Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Item_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Item;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRecomputeGlobal_MetaData[];
#endif
		static void NewProp_bRecomputeGlobal_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRecomputeGlobal;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bApplyToChildren_MetaData[];
#endif
		static void NewProp_bApplyToChildren_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bApplyToChildren;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRecursive_MetaData[];
#endif
		static void NewProp_bRecursive_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRecursive;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CachedIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "/**\n * Propagate Transform can be used to force a recalculation of a bone's global transform\n * from its local - as well as propagating that change onto the children.\n */" },
		{ "DisplayName", "Propagate Transform" },
		{ "DocumentationPolicy", "Strict" },
		{ "Keywords", "PropagateToChildren,RecomputeGlobal,RecalculateGlobal" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_PropagateTransform.h" },
		{ "ToolTip", "Propagate Transform can be used to force a recalculation of a bone's global transform\nfrom its local - as well as propagating that change onto the children." },
		{ "Varying", "" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_PropagateTransform>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Statics::NewProp_Item_MetaData[] = {
		{ "Comment", "/**\n\x09 * The item to offset the transform for\n\x09 */" },
		{ "ExpandByDefault", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_PropagateTransform.h" },
		{ "ToolTip", "The item to offset the transform for" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Statics::NewProp_Item = { "Item", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_PropagateTransform, Item), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Statics::NewProp_Item_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Statics::NewProp_Item_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Statics::NewProp_bRecomputeGlobal_MetaData[] = {
		{ "Comment", "// If set to true the item's global transform will be recomputed from\n// its parent's transform and its local.\n" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_PropagateTransform.h" },
		{ "ToolTip", "If set to true the item's global transform will be recomputed from\nits parent's transform and its local." },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Statics::NewProp_bRecomputeGlobal_SetBit(void* Obj)
	{
		((FRigUnit_PropagateTransform*)Obj)->bRecomputeGlobal = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Statics::NewProp_bRecomputeGlobal = { "bRecomputeGlobal", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_PropagateTransform), &Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Statics::NewProp_bRecomputeGlobal_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Statics::NewProp_bRecomputeGlobal_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Statics::NewProp_bRecomputeGlobal_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Statics::NewProp_bApplyToChildren_MetaData[] = {
		{ "Comment", "// If set to true the direct children of this item will be recomputed as well.\n// Combined with bRecursive all children will be affected recursively.\n" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_PropagateTransform.h" },
		{ "ToolTip", "If set to true the direct children of this item will be recomputed as well.\nCombined with bRecursive all children will be affected recursively." },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Statics::NewProp_bApplyToChildren_SetBit(void* Obj)
	{
		((FRigUnit_PropagateTransform*)Obj)->bApplyToChildren = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Statics::NewProp_bApplyToChildren = { "bApplyToChildren", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_PropagateTransform), &Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Statics::NewProp_bApplyToChildren_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Statics::NewProp_bApplyToChildren_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Statics::NewProp_bApplyToChildren_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Statics::NewProp_bRecursive_MetaData[] = {
		{ "Comment", "// If set to true and with bApplyToChildren enabled\n// all children will be affected recursively.\n" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_PropagateTransform.h" },
		{ "ToolTip", "If set to true and with bApplyToChildren enabled\nall children will be affected recursively." },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Statics::NewProp_bRecursive_SetBit(void* Obj)
	{
		((FRigUnit_PropagateTransform*)Obj)->bRecursive = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Statics::NewProp_bRecursive = { "bRecursive", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_PropagateTransform), &Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Statics::NewProp_bRecursive_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Statics::NewProp_bRecursive_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Statics::NewProp_bRecursive_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Statics::NewProp_CachedIndex_MetaData[] = {
		{ "Comment", "// Used to cache the item internally\n" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_PropagateTransform.h" },
		{ "ToolTip", "Used to cache the item internally" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Statics::NewProp_CachedIndex = { "CachedIndex", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_PropagateTransform, CachedIndex), Z_Construct_UScriptStruct_FCachedRigElement, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Statics::NewProp_CachedIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Statics::NewProp_CachedIndex_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Statics::NewProp_Item,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Statics::NewProp_bRecomputeGlobal,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Statics::NewProp_bApplyToChildren,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Statics::NewProp_bRecursive,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Statics::NewProp_CachedIndex,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnitMutable,
		&NewStructOps,
		"RigUnit_PropagateTransform",
		sizeof(FRigUnit_PropagateTransform),
		alignof(FRigUnit_PropagateTransform),
		Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_PropagateTransform()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_PropagateTransform"), sizeof(FRigUnit_PropagateTransform), Get_Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_PropagateTransform_Hash() { return 3479880911U; }

void FRigUnit_PropagateTransform::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Item,
		bRecomputeGlobal,
		bApplyToChildren,
		bRecursive,
		CachedIndex,
		ExecuteContext,
		Context
	);
}

PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
