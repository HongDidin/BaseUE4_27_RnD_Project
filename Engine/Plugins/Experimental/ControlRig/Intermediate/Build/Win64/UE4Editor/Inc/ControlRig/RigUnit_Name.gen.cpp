// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Core/RigUnit_Name.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_Name() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_Contains();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_NameBase();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_StartsWith();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_EndsWith();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_NameReplace();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_NameTruncate();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_NameConcat();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_Contains>() == std::is_polymorphic<FRigUnit_NameBase>(), "USTRUCT FRigUnit_Contains cannot be polymorphic unless super FRigUnit_NameBase is polymorphic");

class UScriptStruct* FRigUnit_Contains::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Contains_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_Contains, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_Contains"), sizeof(FRigUnit_Contains), Get_Z_Construct_UScriptStruct_FRigUnit_Contains_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_Contains::Execute"), &FRigUnit_Contains::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_Contains>()
{
	return FRigUnit_Contains::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_Contains(FRigUnit_Contains::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_Contains"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_Contains
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_Contains()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_Contains>(FName(TEXT("RigUnit_Contains")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_Contains;
	struct Z_Construct_UScriptStruct_FRigUnit_Contains_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Search_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Search;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static void NewProp_Result_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_Contains_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns true or false if a given name exists in another given name\n */" },
		{ "DisplayName", "Contains" },
		{ "Keywords", "Contains,Find,Has,Search" },
		{ "ModuleRelativePath", "Private/Units/Core/RigUnit_Name.h" },
		{ "ToolTip", "Returns true or false if a given name exists in another given name" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_Contains_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_Contains>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_Contains_Statics::NewProp_Name_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Core/RigUnit_Name.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_Contains_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_Contains, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_Contains_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_Contains_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_Contains_Statics::NewProp_Search_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Core/RigUnit_Name.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_Contains_Statics::NewProp_Search = { "Search", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_Contains, Search), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_Contains_Statics::NewProp_Search_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_Contains_Statics::NewProp_Search_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_Contains_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Core/RigUnit_Name.h" },
		{ "Output", "" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_Contains_Statics::NewProp_Result_SetBit(void* Obj)
	{
		((FRigUnit_Contains*)Obj)->Result = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_Contains_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_Contains), &Z_Construct_UScriptStruct_FRigUnit_Contains_Statics::NewProp_Result_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_Contains_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_Contains_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_Contains_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_Contains_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_Contains_Statics::NewProp_Search,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_Contains_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_Contains_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_NameBase,
		&NewStructOps,
		"RigUnit_Contains",
		sizeof(FRigUnit_Contains),
		alignof(FRigUnit_Contains),
		Z_Construct_UScriptStruct_FRigUnit_Contains_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_Contains_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_Contains_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_Contains_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_Contains()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Contains_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_Contains"), sizeof(FRigUnit_Contains), Get_Z_Construct_UScriptStruct_FRigUnit_Contains_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_Contains_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Contains_Hash() { return 990913694U; }

void FRigUnit_Contains::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Name,
		Search,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_StartsWith>() == std::is_polymorphic<FRigUnit_NameBase>(), "USTRUCT FRigUnit_StartsWith cannot be polymorphic unless super FRigUnit_NameBase is polymorphic");

class UScriptStruct* FRigUnit_StartsWith::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_StartsWith_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_StartsWith, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_StartsWith"), sizeof(FRigUnit_StartsWith), Get_Z_Construct_UScriptStruct_FRigUnit_StartsWith_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_StartsWith::Execute"), &FRigUnit_StartsWith::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_StartsWith>()
{
	return FRigUnit_StartsWith::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_StartsWith(FRigUnit_StartsWith::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_StartsWith"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_StartsWith
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_StartsWith()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_StartsWith>(FName(TEXT("RigUnit_StartsWith")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_StartsWith;
	struct Z_Construct_UScriptStruct_FRigUnit_StartsWith_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Start_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Start;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static void NewProp_Result_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_StartsWith_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Tests whether this string starts with given string\n */" },
		{ "DisplayName", "Starts With" },
		{ "Keywords", "Left" },
		{ "ModuleRelativePath", "Private/Units/Core/RigUnit_Name.h" },
		{ "ToolTip", "Tests whether this string starts with given string" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_StartsWith_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_StartsWith>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_StartsWith_Statics::NewProp_Name_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Core/RigUnit_Name.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_StartsWith_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_StartsWith, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_StartsWith_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_StartsWith_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_StartsWith_Statics::NewProp_Start_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Core/RigUnit_Name.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_StartsWith_Statics::NewProp_Start = { "Start", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_StartsWith, Start), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_StartsWith_Statics::NewProp_Start_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_StartsWith_Statics::NewProp_Start_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_StartsWith_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Core/RigUnit_Name.h" },
		{ "Output", "" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_StartsWith_Statics::NewProp_Result_SetBit(void* Obj)
	{
		((FRigUnit_StartsWith*)Obj)->Result = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_StartsWith_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_StartsWith), &Z_Construct_UScriptStruct_FRigUnit_StartsWith_Statics::NewProp_Result_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_StartsWith_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_StartsWith_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_StartsWith_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_StartsWith_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_StartsWith_Statics::NewProp_Start,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_StartsWith_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_StartsWith_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_NameBase,
		&NewStructOps,
		"RigUnit_StartsWith",
		sizeof(FRigUnit_StartsWith),
		alignof(FRigUnit_StartsWith),
		Z_Construct_UScriptStruct_FRigUnit_StartsWith_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_StartsWith_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_StartsWith_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_StartsWith_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_StartsWith()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_StartsWith_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_StartsWith"), sizeof(FRigUnit_StartsWith), Get_Z_Construct_UScriptStruct_FRigUnit_StartsWith_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_StartsWith_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_StartsWith_Hash() { return 1488825775U; }

void FRigUnit_StartsWith::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Name,
		Start,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_EndsWith>() == std::is_polymorphic<FRigUnit_NameBase>(), "USTRUCT FRigUnit_EndsWith cannot be polymorphic unless super FRigUnit_NameBase is polymorphic");

class UScriptStruct* FRigUnit_EndsWith::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_EndsWith_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_EndsWith, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_EndsWith"), sizeof(FRigUnit_EndsWith), Get_Z_Construct_UScriptStruct_FRigUnit_EndsWith_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_EndsWith::Execute"), &FRigUnit_EndsWith::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_EndsWith>()
{
	return FRigUnit_EndsWith::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_EndsWith(FRigUnit_EndsWith::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_EndsWith"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_EndsWith
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_EndsWith()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_EndsWith>(FName(TEXT("RigUnit_EndsWith")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_EndsWith;
	struct Z_Construct_UScriptStruct_FRigUnit_EndsWith_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Ending_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Ending;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static void NewProp_Result_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_EndsWith_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Tests whether this string ends with given string\n */" },
		{ "DisplayName", "Ends With" },
		{ "Keywords", "Right" },
		{ "ModuleRelativePath", "Private/Units/Core/RigUnit_Name.h" },
		{ "ToolTip", "Tests whether this string ends with given string" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_EndsWith_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_EndsWith>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_EndsWith_Statics::NewProp_Name_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Core/RigUnit_Name.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_EndsWith_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_EndsWith, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_EndsWith_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_EndsWith_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_EndsWith_Statics::NewProp_Ending_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Core/RigUnit_Name.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_EndsWith_Statics::NewProp_Ending = { "Ending", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_EndsWith, Ending), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_EndsWith_Statics::NewProp_Ending_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_EndsWith_Statics::NewProp_Ending_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_EndsWith_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Core/RigUnit_Name.h" },
		{ "Output", "" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_EndsWith_Statics::NewProp_Result_SetBit(void* Obj)
	{
		((FRigUnit_EndsWith*)Obj)->Result = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_EndsWith_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_EndsWith), &Z_Construct_UScriptStruct_FRigUnit_EndsWith_Statics::NewProp_Result_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_EndsWith_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_EndsWith_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_EndsWith_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_EndsWith_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_EndsWith_Statics::NewProp_Ending,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_EndsWith_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_EndsWith_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_NameBase,
		&NewStructOps,
		"RigUnit_EndsWith",
		sizeof(FRigUnit_EndsWith),
		alignof(FRigUnit_EndsWith),
		Z_Construct_UScriptStruct_FRigUnit_EndsWith_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_EndsWith_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_EndsWith_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_EndsWith_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_EndsWith()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_EndsWith_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_EndsWith"), sizeof(FRigUnit_EndsWith), Get_Z_Construct_UScriptStruct_FRigUnit_EndsWith_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_EndsWith_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_EndsWith_Hash() { return 2513167418U; }

void FRigUnit_EndsWith::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Name,
		Ending,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_NameReplace>() == std::is_polymorphic<FRigUnit_NameBase>(), "USTRUCT FRigUnit_NameReplace cannot be polymorphic unless super FRigUnit_NameBase is polymorphic");

class UScriptStruct* FRigUnit_NameReplace::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_NameReplace_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_NameReplace, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_NameReplace"), sizeof(FRigUnit_NameReplace), Get_Z_Construct_UScriptStruct_FRigUnit_NameReplace_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_NameReplace::Execute"), &FRigUnit_NameReplace::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_NameReplace>()
{
	return FRigUnit_NameReplace::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_NameReplace(FRigUnit_NameReplace::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_NameReplace"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_NameReplace
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_NameReplace()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_NameReplace>(FName(TEXT("RigUnit_NameReplace")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_NameReplace;
	struct Z_Construct_UScriptStruct_FRigUnit_NameReplace_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Old_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Old;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_New_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_New;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_NameReplace_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Replace all occurrences of a substring in this string\n */" },
		{ "DisplayName", "Replace" },
		{ "Keywords", "Search,Emplace,Find" },
		{ "ModuleRelativePath", "Private/Units/Core/RigUnit_Name.h" },
		{ "ToolTip", "Replace all occurrences of a substring in this string" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_NameReplace_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_NameReplace>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_NameReplace_Statics::NewProp_Name_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Core/RigUnit_Name.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_NameReplace_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_NameReplace, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_NameReplace_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_NameReplace_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_NameReplace_Statics::NewProp_Old_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Core/RigUnit_Name.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_NameReplace_Statics::NewProp_Old = { "Old", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_NameReplace, Old), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_NameReplace_Statics::NewProp_Old_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_NameReplace_Statics::NewProp_Old_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_NameReplace_Statics::NewProp_New_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Core/RigUnit_Name.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_NameReplace_Statics::NewProp_New = { "New", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_NameReplace, New), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_NameReplace_Statics::NewProp_New_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_NameReplace_Statics::NewProp_New_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_NameReplace_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Core/RigUnit_Name.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_NameReplace_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_NameReplace, Result), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_NameReplace_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_NameReplace_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_NameReplace_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_NameReplace_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_NameReplace_Statics::NewProp_Old,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_NameReplace_Statics::NewProp_New,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_NameReplace_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_NameReplace_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_NameBase,
		&NewStructOps,
		"RigUnit_NameReplace",
		sizeof(FRigUnit_NameReplace),
		alignof(FRigUnit_NameReplace),
		Z_Construct_UScriptStruct_FRigUnit_NameReplace_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_NameReplace_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_NameReplace_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_NameReplace_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_NameReplace()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_NameReplace_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_NameReplace"), sizeof(FRigUnit_NameReplace), Get_Z_Construct_UScriptStruct_FRigUnit_NameReplace_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_NameReplace_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_NameReplace_Hash() { return 1848121800U; }

void FRigUnit_NameReplace::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Name,
		Old,
		New,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_NameTruncate>() == std::is_polymorphic<FRigUnit_NameBase>(), "USTRUCT FRigUnit_NameTruncate cannot be polymorphic unless super FRigUnit_NameBase is polymorphic");

class UScriptStruct* FRigUnit_NameTruncate::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_NameTruncate, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_NameTruncate"), sizeof(FRigUnit_NameTruncate), Get_Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_NameTruncate::Execute"), &FRigUnit_NameTruncate::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_NameTruncate>()
{
	return FRigUnit_NameTruncate::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_NameTruncate(FRigUnit_NameTruncate::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_NameTruncate"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_NameTruncate
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_NameTruncate()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_NameTruncate>(FName(TEXT("RigUnit_NameTruncate")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_NameTruncate;
	struct Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Count_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Count;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FromEnd_MetaData[];
#endif
		static void NewProp_FromEnd_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_FromEnd;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Remainder_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Remainder;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Chopped_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Chopped;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the left or right most characters from the string chopping the given number of characters from the start or the end\n */" },
		{ "DisplayName", "Chop" },
		{ "Keywords", "Truncate,-,Remove,Subtract,Split" },
		{ "ModuleRelativePath", "Private/Units/Core/RigUnit_Name.h" },
		{ "ToolTip", "Returns the left or right most characters from the string chopping the given number of characters from the start or the end" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_NameTruncate>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Statics::NewProp_Name_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Core/RigUnit_Name.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_NameTruncate, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Statics::NewProp_Count_MetaData[] = {
		{ "Comment", "// Number of characters to remove from left or right\n" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Core/RigUnit_Name.h" },
		{ "ToolTip", "Number of characters to remove from left or right" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Statics::NewProp_Count = { "Count", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_NameTruncate, Count), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Statics::NewProp_Count_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Statics::NewProp_Count_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Statics::NewProp_FromEnd_MetaData[] = {
		{ "Comment", "// if set to true the characters will be removed from the end\n" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Core/RigUnit_Name.h" },
		{ "ToolTip", "if set to true the characters will be removed from the end" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Statics::NewProp_FromEnd_SetBit(void* Obj)
	{
		((FRigUnit_NameTruncate*)Obj)->FromEnd = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Statics::NewProp_FromEnd = { "FromEnd", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_NameTruncate), &Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Statics::NewProp_FromEnd_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Statics::NewProp_FromEnd_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Statics::NewProp_FromEnd_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Statics::NewProp_Remainder_MetaData[] = {
		{ "Comment", "// the part of the string without the chopped characters\n" },
		{ "ModuleRelativePath", "Private/Units/Core/RigUnit_Name.h" },
		{ "Output", "" },
		{ "ToolTip", "the part of the string without the chopped characters" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Statics::NewProp_Remainder = { "Remainder", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_NameTruncate, Remainder), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Statics::NewProp_Remainder_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Statics::NewProp_Remainder_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Statics::NewProp_Chopped_MetaData[] = {
		{ "Comment", "// the part of the name that has been chopped off\n" },
		{ "ModuleRelativePath", "Private/Units/Core/RigUnit_Name.h" },
		{ "Output", "" },
		{ "ToolTip", "the part of the name that has been chopped off" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Statics::NewProp_Chopped = { "Chopped", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_NameTruncate, Chopped), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Statics::NewProp_Chopped_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Statics::NewProp_Chopped_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Statics::NewProp_Count,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Statics::NewProp_FromEnd,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Statics::NewProp_Remainder,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Statics::NewProp_Chopped,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_NameBase,
		&NewStructOps,
		"RigUnit_NameTruncate",
		sizeof(FRigUnit_NameTruncate),
		alignof(FRigUnit_NameTruncate),
		Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_NameTruncate()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_NameTruncate"), sizeof(FRigUnit_NameTruncate), Get_Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_NameTruncate_Hash() { return 2276858206U; }

void FRigUnit_NameTruncate::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Name,
		Count,
		FromEnd,
		Remainder,
		Chopped,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_NameConcat>() == std::is_polymorphic<FRigUnit_NameBase>(), "USTRUCT FRigUnit_NameConcat cannot be polymorphic unless super FRigUnit_NameBase is polymorphic");

class UScriptStruct* FRigUnit_NameConcat::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_NameConcat_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_NameConcat, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_NameConcat"), sizeof(FRigUnit_NameConcat), Get_Z_Construct_UScriptStruct_FRigUnit_NameConcat_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_NameConcat::Execute"), &FRigUnit_NameConcat::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_NameConcat>()
{
	return FRigUnit_NameConcat::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_NameConcat(FRigUnit_NameConcat::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_NameConcat"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_NameConcat
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_NameConcat()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_NameConcat>(FName(TEXT("RigUnit_NameConcat")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_NameConcat;
	struct Z_Construct_UScriptStruct_FRigUnit_NameConcat_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_A_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_A;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_B_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_B;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_NameConcat_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Concatenates two strings together to make a new string\n */" },
		{ "DisplayName", "Concat" },
		{ "Keywords", "Add,+,Combine,Merge,Append" },
		{ "ModuleRelativePath", "Private/Units/Core/RigUnit_Name.h" },
		{ "ToolTip", "Concatenates two strings together to make a new string" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_NameConcat_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_NameConcat>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_NameConcat_Statics::NewProp_A_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Core/RigUnit_Name.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_NameConcat_Statics::NewProp_A = { "A", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_NameConcat, A), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_NameConcat_Statics::NewProp_A_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_NameConcat_Statics::NewProp_A_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_NameConcat_Statics::NewProp_B_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Core/RigUnit_Name.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_NameConcat_Statics::NewProp_B = { "B", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_NameConcat, B), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_NameConcat_Statics::NewProp_B_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_NameConcat_Statics::NewProp_B_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_NameConcat_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Core/RigUnit_Name.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_NameConcat_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_NameConcat, Result), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_NameConcat_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_NameConcat_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_NameConcat_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_NameConcat_Statics::NewProp_A,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_NameConcat_Statics::NewProp_B,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_NameConcat_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_NameConcat_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_NameBase,
		&NewStructOps,
		"RigUnit_NameConcat",
		sizeof(FRigUnit_NameConcat),
		alignof(FRigUnit_NameConcat),
		Z_Construct_UScriptStruct_FRigUnit_NameConcat_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_NameConcat_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_NameConcat_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_NameConcat_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_NameConcat()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_NameConcat_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_NameConcat"), sizeof(FRigUnit_NameConcat), Get_Z_Construct_UScriptStruct_FRigUnit_NameConcat_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_NameConcat_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_NameConcat_Hash() { return 2238445584U; }

void FRigUnit_NameConcat::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_NameBase>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_NameBase cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_NameBase::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_NameBase_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_NameBase, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_NameBase"), sizeof(FRigUnit_NameBase), Get_Z_Construct_UScriptStruct_FRigUnit_NameBase_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_NameBase>()
{
	return FRigUnit_NameBase::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_NameBase(FRigUnit_NameBase::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_NameBase"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_NameBase
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_NameBase()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_NameBase>(FName(TEXT("RigUnit_NameBase")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_NameBase;
	struct Z_Construct_UScriptStruct_FRigUnit_NameBase_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_NameBase_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "Category", "Core|Name" },
		{ "ModuleRelativePath", "Private/Units/Core/RigUnit_Name.h" },
		{ "NodeColor", "0.7 0.05 0.5" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_NameBase_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_NameBase>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_NameBase_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_NameBase",
		sizeof(FRigUnit_NameBase),
		alignof(FRigUnit_NameBase),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_NameBase_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_NameBase_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_NameBase()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_NameBase_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_NameBase"), sizeof(FRigUnit_NameBase), Get_Z_Construct_UScriptStruct_FRigUnit_NameBase_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_NameBase_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_NameBase_Hash() { return 3109648137U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
