// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Deprecated/Math/RigUnit_Vector.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_Vector() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_Distance_VectorVector();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_Divide_VectorVector();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_BinaryVectorOp();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_Subtract_VectorVector();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_Add_VectorVector();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_Multiply_VectorVector();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_Distance_VectorVector>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_Distance_VectorVector cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_Distance_VectorVector::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Distance_VectorVector_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_Distance_VectorVector, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_Distance_VectorVector"), sizeof(FRigUnit_Distance_VectorVector), Get_Z_Construct_UScriptStruct_FRigUnit_Distance_VectorVector_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_Distance_VectorVector::Execute"), &FRigUnit_Distance_VectorVector::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_Distance_VectorVector>()
{
	return FRigUnit_Distance_VectorVector::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_Distance_VectorVector(FRigUnit_Distance_VectorVector::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_Distance_VectorVector"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_Distance_VectorVector
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_Distance_VectorVector()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_Distance_VectorVector>(FName(TEXT("RigUnit_Distance_VectorVector")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_Distance_VectorVector;
	struct Z_Construct_UScriptStruct_FRigUnit_Distance_VectorVector_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Argument0_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Argument0;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Argument1_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Argument1;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_Distance_VectorVector_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Math|Vector" },
		{ "Deprecated", "4.23.0" },
		{ "DisplayName", "Distance" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Vector.h" },
		{ "NodeColor", "0.1 0.7 0.1" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_Distance_VectorVector_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_Distance_VectorVector>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_Distance_VectorVector_Statics::NewProp_Argument0_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Vector.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_Distance_VectorVector_Statics::NewProp_Argument0 = { "Argument0", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_Distance_VectorVector, Argument0), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_Distance_VectorVector_Statics::NewProp_Argument0_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_Distance_VectorVector_Statics::NewProp_Argument0_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_Distance_VectorVector_Statics::NewProp_Argument1_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Vector.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_Distance_VectorVector_Statics::NewProp_Argument1 = { "Argument1", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_Distance_VectorVector, Argument1), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_Distance_VectorVector_Statics::NewProp_Argument1_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_Distance_VectorVector_Statics::NewProp_Argument1_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_Distance_VectorVector_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Vector.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_Distance_VectorVector_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_Distance_VectorVector, Result), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_Distance_VectorVector_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_Distance_VectorVector_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_Distance_VectorVector_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_Distance_VectorVector_Statics::NewProp_Argument0,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_Distance_VectorVector_Statics::NewProp_Argument1,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_Distance_VectorVector_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_Distance_VectorVector_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_Distance_VectorVector",
		sizeof(FRigUnit_Distance_VectorVector),
		alignof(FRigUnit_Distance_VectorVector),
		Z_Construct_UScriptStruct_FRigUnit_Distance_VectorVector_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_Distance_VectorVector_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_Distance_VectorVector_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_Distance_VectorVector_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_Distance_VectorVector()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Distance_VectorVector_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_Distance_VectorVector"), sizeof(FRigUnit_Distance_VectorVector), Get_Z_Construct_UScriptStruct_FRigUnit_Distance_VectorVector_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_Distance_VectorVector_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Distance_VectorVector_Hash() { return 4006858970U; }

void FRigUnit_Distance_VectorVector::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Argument0,
		Argument1,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_Divide_VectorVector>() == std::is_polymorphic<FRigUnit_BinaryVectorOp>(), "USTRUCT FRigUnit_Divide_VectorVector cannot be polymorphic unless super FRigUnit_BinaryVectorOp is polymorphic");

class UScriptStruct* FRigUnit_Divide_VectorVector::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Divide_VectorVector_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_Divide_VectorVector, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_Divide_VectorVector"), sizeof(FRigUnit_Divide_VectorVector), Get_Z_Construct_UScriptStruct_FRigUnit_Divide_VectorVector_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_Divide_VectorVector::Execute"), &FRigUnit_Divide_VectorVector::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_Divide_VectorVector>()
{
	return FRigUnit_Divide_VectorVector::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_Divide_VectorVector(FRigUnit_Divide_VectorVector::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_Divide_VectorVector"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_Divide_VectorVector
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_Divide_VectorVector()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_Divide_VectorVector>(FName(TEXT("RigUnit_Divide_VectorVector")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_Divide_VectorVector;
	struct Z_Construct_UScriptStruct_FRigUnit_Divide_VectorVector_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_Divide_VectorVector_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Math|Vector" },
		{ "Deprecated", "4.23.0" },
		{ "DisplayName", "Divide(Vector)" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Vector.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_Divide_VectorVector_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_Divide_VectorVector>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_Divide_VectorVector_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_BinaryVectorOp,
		&NewStructOps,
		"RigUnit_Divide_VectorVector",
		sizeof(FRigUnit_Divide_VectorVector),
		alignof(FRigUnit_Divide_VectorVector),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_Divide_VectorVector_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_Divide_VectorVector_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_Divide_VectorVector()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Divide_VectorVector_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_Divide_VectorVector"), sizeof(FRigUnit_Divide_VectorVector), Get_Z_Construct_UScriptStruct_FRigUnit_Divide_VectorVector_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_Divide_VectorVector_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Divide_VectorVector_Hash() { return 3076824865U; }

void FRigUnit_Divide_VectorVector::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Argument0,
		Argument1,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_Subtract_VectorVector>() == std::is_polymorphic<FRigUnit_BinaryVectorOp>(), "USTRUCT FRigUnit_Subtract_VectorVector cannot be polymorphic unless super FRigUnit_BinaryVectorOp is polymorphic");

class UScriptStruct* FRigUnit_Subtract_VectorVector::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Subtract_VectorVector_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_Subtract_VectorVector, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_Subtract_VectorVector"), sizeof(FRigUnit_Subtract_VectorVector), Get_Z_Construct_UScriptStruct_FRigUnit_Subtract_VectorVector_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_Subtract_VectorVector::Execute"), &FRigUnit_Subtract_VectorVector::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_Subtract_VectorVector>()
{
	return FRigUnit_Subtract_VectorVector::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_Subtract_VectorVector(FRigUnit_Subtract_VectorVector::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_Subtract_VectorVector"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_Subtract_VectorVector
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_Subtract_VectorVector()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_Subtract_VectorVector>(FName(TEXT("RigUnit_Subtract_VectorVector")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_Subtract_VectorVector;
	struct Z_Construct_UScriptStruct_FRigUnit_Subtract_VectorVector_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_Subtract_VectorVector_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Math|Vector" },
		{ "Deprecated", "4.23.0" },
		{ "DisplayName", "Subtract(Vector)" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Vector.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_Subtract_VectorVector_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_Subtract_VectorVector>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_Subtract_VectorVector_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_BinaryVectorOp,
		&NewStructOps,
		"RigUnit_Subtract_VectorVector",
		sizeof(FRigUnit_Subtract_VectorVector),
		alignof(FRigUnit_Subtract_VectorVector),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_Subtract_VectorVector_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_Subtract_VectorVector_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_Subtract_VectorVector()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Subtract_VectorVector_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_Subtract_VectorVector"), sizeof(FRigUnit_Subtract_VectorVector), Get_Z_Construct_UScriptStruct_FRigUnit_Subtract_VectorVector_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_Subtract_VectorVector_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Subtract_VectorVector_Hash() { return 986974459U; }

void FRigUnit_Subtract_VectorVector::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Argument0,
		Argument1,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_Add_VectorVector>() == std::is_polymorphic<FRigUnit_BinaryVectorOp>(), "USTRUCT FRigUnit_Add_VectorVector cannot be polymorphic unless super FRigUnit_BinaryVectorOp is polymorphic");

class UScriptStruct* FRigUnit_Add_VectorVector::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Add_VectorVector_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_Add_VectorVector, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_Add_VectorVector"), sizeof(FRigUnit_Add_VectorVector), Get_Z_Construct_UScriptStruct_FRigUnit_Add_VectorVector_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_Add_VectorVector::Execute"), &FRigUnit_Add_VectorVector::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_Add_VectorVector>()
{
	return FRigUnit_Add_VectorVector::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_Add_VectorVector(FRigUnit_Add_VectorVector::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_Add_VectorVector"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_Add_VectorVector
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_Add_VectorVector()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_Add_VectorVector>(FName(TEXT("RigUnit_Add_VectorVector")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_Add_VectorVector;
	struct Z_Construct_UScriptStruct_FRigUnit_Add_VectorVector_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_Add_VectorVector_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Math|Vector" },
		{ "Deprecated", "4.23.0" },
		{ "DisplayName", "Add(Vector)" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Vector.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_Add_VectorVector_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_Add_VectorVector>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_Add_VectorVector_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_BinaryVectorOp,
		&NewStructOps,
		"RigUnit_Add_VectorVector",
		sizeof(FRigUnit_Add_VectorVector),
		alignof(FRigUnit_Add_VectorVector),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_Add_VectorVector_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_Add_VectorVector_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_Add_VectorVector()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Add_VectorVector_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_Add_VectorVector"), sizeof(FRigUnit_Add_VectorVector), Get_Z_Construct_UScriptStruct_FRigUnit_Add_VectorVector_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_Add_VectorVector_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Add_VectorVector_Hash() { return 3051998632U; }

void FRigUnit_Add_VectorVector::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Argument0,
		Argument1,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_Multiply_VectorVector>() == std::is_polymorphic<FRigUnit_BinaryVectorOp>(), "USTRUCT FRigUnit_Multiply_VectorVector cannot be polymorphic unless super FRigUnit_BinaryVectorOp is polymorphic");

class UScriptStruct* FRigUnit_Multiply_VectorVector::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Multiply_VectorVector_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_Multiply_VectorVector, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_Multiply_VectorVector"), sizeof(FRigUnit_Multiply_VectorVector), Get_Z_Construct_UScriptStruct_FRigUnit_Multiply_VectorVector_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_Multiply_VectorVector::Execute"), &FRigUnit_Multiply_VectorVector::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_Multiply_VectorVector>()
{
	return FRigUnit_Multiply_VectorVector::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_Multiply_VectorVector(FRigUnit_Multiply_VectorVector::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_Multiply_VectorVector"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_Multiply_VectorVector
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_Multiply_VectorVector()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_Multiply_VectorVector>(FName(TEXT("RigUnit_Multiply_VectorVector")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_Multiply_VectorVector;
	struct Z_Construct_UScriptStruct_FRigUnit_Multiply_VectorVector_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_Multiply_VectorVector_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Math|Vector" },
		{ "Deprecated", "4.23.0" },
		{ "DisplayName", "Multiply(Vector)" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Vector.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_Multiply_VectorVector_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_Multiply_VectorVector>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_Multiply_VectorVector_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_BinaryVectorOp,
		&NewStructOps,
		"RigUnit_Multiply_VectorVector",
		sizeof(FRigUnit_Multiply_VectorVector),
		alignof(FRigUnit_Multiply_VectorVector),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_Multiply_VectorVector_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_Multiply_VectorVector_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_Multiply_VectorVector()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Multiply_VectorVector_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_Multiply_VectorVector"), sizeof(FRigUnit_Multiply_VectorVector), Get_Z_Construct_UScriptStruct_FRigUnit_Multiply_VectorVector_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_Multiply_VectorVector_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Multiply_VectorVector_Hash() { return 3406005034U; }

void FRigUnit_Multiply_VectorVector::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Argument0,
		Argument1,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_BinaryVectorOp>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_BinaryVectorOp cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_BinaryVectorOp::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_BinaryVectorOp_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_BinaryVectorOp, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_BinaryVectorOp"), sizeof(FRigUnit_BinaryVectorOp), Get_Z_Construct_UScriptStruct_FRigUnit_BinaryVectorOp_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_BinaryVectorOp>()
{
	return FRigUnit_BinaryVectorOp::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_BinaryVectorOp(FRigUnit_BinaryVectorOp::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_BinaryVectorOp"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_BinaryVectorOp
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_BinaryVectorOp()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_BinaryVectorOp>(FName(TEXT("RigUnit_BinaryVectorOp")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_BinaryVectorOp;
	struct Z_Construct_UScriptStruct_FRigUnit_BinaryVectorOp_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Argument0_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Argument0;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Argument1_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Argument1;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_BinaryVectorOp_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "Comment", "/** Two args and a result of Vector type */" },
		{ "Deprecated", "4.23.0" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Vector.h" },
		{ "NodeColor", "0.1 0.7 0.1" },
		{ "ToolTip", "Two args and a result of Vector type" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_BinaryVectorOp_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_BinaryVectorOp>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_BinaryVectorOp_Statics::NewProp_Argument0_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Vector.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_BinaryVectorOp_Statics::NewProp_Argument0 = { "Argument0", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_BinaryVectorOp, Argument0), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_BinaryVectorOp_Statics::NewProp_Argument0_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_BinaryVectorOp_Statics::NewProp_Argument0_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_BinaryVectorOp_Statics::NewProp_Argument1_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Vector.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_BinaryVectorOp_Statics::NewProp_Argument1 = { "Argument1", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_BinaryVectorOp, Argument1), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_BinaryVectorOp_Statics::NewProp_Argument1_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_BinaryVectorOp_Statics::NewProp_Argument1_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_BinaryVectorOp_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Vector.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_BinaryVectorOp_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_BinaryVectorOp, Result), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_BinaryVectorOp_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_BinaryVectorOp_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_BinaryVectorOp_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_BinaryVectorOp_Statics::NewProp_Argument0,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_BinaryVectorOp_Statics::NewProp_Argument1,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_BinaryVectorOp_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_BinaryVectorOp_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_BinaryVectorOp",
		sizeof(FRigUnit_BinaryVectorOp),
		alignof(FRigUnit_BinaryVectorOp),
		Z_Construct_UScriptStruct_FRigUnit_BinaryVectorOp_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_BinaryVectorOp_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_BinaryVectorOp_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_BinaryVectorOp_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_BinaryVectorOp()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_BinaryVectorOp_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_BinaryVectorOp"), sizeof(FRigUnit_BinaryVectorOp), Get_Z_Construct_UScriptStruct_FRigUnit_BinaryVectorOp_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_BinaryVectorOp_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_BinaryVectorOp_Hash() { return 529974425U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
