// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Public/Math/Simulation/CRSimPointConstraint.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCRSimPointConstraint() {}
// Cross Module References
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_ECRSimConstraintType();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FCRSimPointConstraint();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
// End Cross Module References
	static UEnum* ECRSimConstraintType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ControlRig_ECRSimConstraintType, Z_Construct_UPackage__Script_ControlRig(), TEXT("ECRSimConstraintType"));
		}
		return Singleton;
	}
	template<> CONTROLRIG_API UEnum* StaticEnum<ECRSimConstraintType>()
	{
		return ECRSimConstraintType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ECRSimConstraintType(ECRSimConstraintType_StaticEnum, TEXT("/Script/ControlRig"), TEXT("ECRSimConstraintType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ControlRig_ECRSimConstraintType_Hash() { return 3662567102U; }
	UEnum* Z_Construct_UEnum_ControlRig_ECRSimConstraintType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ECRSimConstraintType"), 0, Get_Z_Construct_UEnum_ControlRig_ECRSimConstraintType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ECRSimConstraintType::Distance", (int64)ECRSimConstraintType::Distance },
				{ "ECRSimConstraintType::DistanceFromA", (int64)ECRSimConstraintType::DistanceFromA },
				{ "ECRSimConstraintType::DistanceFromB", (int64)ECRSimConstraintType::DistanceFromB },
				{ "ECRSimConstraintType::Plane", (int64)ECRSimConstraintType::Plane },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Distance.Name", "ECRSimConstraintType::Distance" },
				{ "DistanceFromA.Name", "ECRSimConstraintType::DistanceFromA" },
				{ "DistanceFromB.Name", "ECRSimConstraintType::DistanceFromB" },
				{ "ModuleRelativePath", "Public/Math/Simulation/CRSimPointConstraint.h" },
				{ "Plane.Name", "ECRSimConstraintType::Plane" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ControlRig,
				nullptr,
				"ECRSimConstraintType",
				"ECRSimConstraintType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FCRSimPointConstraint::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FCRSimPointConstraint_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCRSimPointConstraint, Z_Construct_UPackage__Script_ControlRig(), TEXT("CRSimPointConstraint"), sizeof(FCRSimPointConstraint), Get_Z_Construct_UScriptStruct_FCRSimPointConstraint_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FCRSimPointConstraint>()
{
	return FCRSimPointConstraint::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCRSimPointConstraint(FCRSimPointConstraint::StaticStruct, TEXT("/Script/ControlRig"), TEXT("CRSimPointConstraint"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFCRSimPointConstraint
{
	FScriptStruct_ControlRig_StaticRegisterNativesFCRSimPointConstraint()
	{
		UScriptStruct::DeferCppStructOps<FCRSimPointConstraint>(FName(TEXT("CRSimPointConstraint")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFCRSimPointConstraint;
	struct Z_Construct_UScriptStruct_FCRSimPointConstraint_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Type_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Type_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Type;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SubjectA_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SubjectA;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SubjectB_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SubjectB;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DataA_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DataA;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DataB_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DataB;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimPointConstraint_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimPointConstraint.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FCRSimPointConstraint_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FCRSimPointConstraint>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FCRSimPointConstraint_Statics::NewProp_Type_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimPointConstraint_Statics::NewProp_Type_MetaData[] = {
		{ "Comment", "/**\n\x09 * The type of the constraint\n\x09 */" },
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimPointConstraint.h" },
		{ "ToolTip", "The type of the constraint" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FCRSimPointConstraint_Statics::NewProp_Type = { "Type", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCRSimPointConstraint, Type), Z_Construct_UEnum_ControlRig_ECRSimConstraintType, METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimPointConstraint_Statics::NewProp_Type_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimPointConstraint_Statics::NewProp_Type_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimPointConstraint_Statics::NewProp_SubjectA_MetaData[] = {
		{ "Comment", "/**\n\x09 * The first point affected by this constraint\n\x09 */" },
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimPointConstraint.h" },
		{ "ToolTip", "The first point affected by this constraint" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FCRSimPointConstraint_Statics::NewProp_SubjectA = { "SubjectA", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCRSimPointConstraint, SubjectA), METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimPointConstraint_Statics::NewProp_SubjectA_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimPointConstraint_Statics::NewProp_SubjectA_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimPointConstraint_Statics::NewProp_SubjectB_MetaData[] = {
		{ "Comment", "/**\n\x09 * The (optional) second point affected by this constraint\n\x09 * This is currently only used for the distance constraint\n\x09 */" },
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimPointConstraint.h" },
		{ "ToolTip", "The (optional) second point affected by this constraint\nThis is currently only used for the distance constraint" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FCRSimPointConstraint_Statics::NewProp_SubjectB = { "SubjectB", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCRSimPointConstraint, SubjectB), METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimPointConstraint_Statics::NewProp_SubjectB_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimPointConstraint_Statics::NewProp_SubjectB_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimPointConstraint_Statics::NewProp_DataA_MetaData[] = {
		{ "Comment", "/**\n\x09 * The first data member for the constraint.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimPointConstraint.h" },
		{ "ToolTip", "The first data member for the constraint." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCRSimPointConstraint_Statics::NewProp_DataA = { "DataA", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCRSimPointConstraint, DataA), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimPointConstraint_Statics::NewProp_DataA_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimPointConstraint_Statics::NewProp_DataA_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimPointConstraint_Statics::NewProp_DataB_MetaData[] = {
		{ "Comment", "/**\n\x09 * The second data member for the constraint.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimPointConstraint.h" },
		{ "ToolTip", "The second data member for the constraint." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCRSimPointConstraint_Statics::NewProp_DataB = { "DataB", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCRSimPointConstraint, DataB), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimPointConstraint_Statics::NewProp_DataB_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimPointConstraint_Statics::NewProp_DataB_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FCRSimPointConstraint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimPointConstraint_Statics::NewProp_Type_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimPointConstraint_Statics::NewProp_Type,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimPointConstraint_Statics::NewProp_SubjectA,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimPointConstraint_Statics::NewProp_SubjectB,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimPointConstraint_Statics::NewProp_DataA,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimPointConstraint_Statics::NewProp_DataB,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FCRSimPointConstraint_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"CRSimPointConstraint",
		sizeof(FCRSimPointConstraint),
		alignof(FCRSimPointConstraint),
		Z_Construct_UScriptStruct_FCRSimPointConstraint_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimPointConstraint_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimPointConstraint_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimPointConstraint_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FCRSimPointConstraint()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FCRSimPointConstraint_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CRSimPointConstraint"), sizeof(FCRSimPointConstraint), Get_Z_Construct_UScriptStruct_FCRSimPointConstraint_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FCRSimPointConstraint_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCRSimPointConstraint_Hash() { return 1784941872U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
