// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIGEDITOR_ControlRigGizmoLibraryFactory_generated_h
#error "ControlRigGizmoLibraryFactory.generated.h already included, missing '#pragma once' in ControlRigGizmoLibraryFactory.h"
#endif
#define CONTROLRIGEDITOR_ControlRigGizmoLibraryFactory_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigGizmoLibraryFactory_h_14_SPARSE_DATA
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigGizmoLibraryFactory_h_14_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigGizmoLibraryFactory_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigGizmoLibraryFactory_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUControlRigGizmoLibraryFactory(); \
	friend struct Z_Construct_UClass_UControlRigGizmoLibraryFactory_Statics; \
public: \
	DECLARE_CLASS(UControlRigGizmoLibraryFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRigEditor"), CONTROLRIGEDITOR_API) \
	DECLARE_SERIALIZER(UControlRigGizmoLibraryFactory)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigGizmoLibraryFactory_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUControlRigGizmoLibraryFactory(); \
	friend struct Z_Construct_UClass_UControlRigGizmoLibraryFactory_Statics; \
public: \
	DECLARE_CLASS(UControlRigGizmoLibraryFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRigEditor"), CONTROLRIGEDITOR_API) \
	DECLARE_SERIALIZER(UControlRigGizmoLibraryFactory)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigGizmoLibraryFactory_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	CONTROLRIGEDITOR_API UControlRigGizmoLibraryFactory(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigGizmoLibraryFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(CONTROLRIGEDITOR_API, UControlRigGizmoLibraryFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigGizmoLibraryFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	CONTROLRIGEDITOR_API UControlRigGizmoLibraryFactory(UControlRigGizmoLibraryFactory&&); \
	CONTROLRIGEDITOR_API UControlRigGizmoLibraryFactory(const UControlRigGizmoLibraryFactory&); \
public:


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigGizmoLibraryFactory_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	CONTROLRIGEDITOR_API UControlRigGizmoLibraryFactory(UControlRigGizmoLibraryFactory&&); \
	CONTROLRIGEDITOR_API UControlRigGizmoLibraryFactory(const UControlRigGizmoLibraryFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(CONTROLRIGEDITOR_API, UControlRigGizmoLibraryFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigGizmoLibraryFactory); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UControlRigGizmoLibraryFactory)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigGizmoLibraryFactory_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigGizmoLibraryFactory_h_11_PROLOG
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigGizmoLibraryFactory_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigGizmoLibraryFactory_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigGizmoLibraryFactory_h_14_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigGizmoLibraryFactory_h_14_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigGizmoLibraryFactory_h_14_INCLASS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigGizmoLibraryFactory_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigGizmoLibraryFactory_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigGizmoLibraryFactory_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigGizmoLibraryFactory_h_14_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigGizmoLibraryFactory_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigGizmoLibraryFactory_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigGizmoLibraryFactory_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONTROLRIGEDITOR_API UClass* StaticClass<class UControlRigGizmoLibraryFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigGizmoLibraryFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
