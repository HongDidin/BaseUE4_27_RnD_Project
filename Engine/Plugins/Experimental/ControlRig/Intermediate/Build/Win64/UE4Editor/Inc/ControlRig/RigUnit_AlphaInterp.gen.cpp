// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Simulation/RigUnit_AlphaInterp.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_AlphaInterp() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_SimBase();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FInputRange();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FInputScaleBiasClamp();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_AlphaInterp();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_AlphaInterpVector>() == std::is_polymorphic<FRigUnit_SimBase>(), "USTRUCT FRigUnit_AlphaInterpVector cannot be polymorphic unless super FRigUnit_SimBase is polymorphic");

class UScriptStruct* FRigUnit_AlphaInterpVector::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_AlphaInterpVector"), sizeof(FRigUnit_AlphaInterpVector), Get_Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_AlphaInterpVector::Execute"), &FRigUnit_AlphaInterpVector::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_AlphaInterpVector>()
{
	return FRigUnit_AlphaInterpVector::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_AlphaInterpVector(FRigUnit_AlphaInterpVector::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_AlphaInterpVector"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AlphaInterpVector
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AlphaInterpVector()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_AlphaInterpVector>(FName(TEXT("RigUnit_AlphaInterpVector")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AlphaInterpVector;
	struct Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Scale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Scale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Bias_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Bias;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bMapRange_MetaData[];
#endif
		static void NewProp_bMapRange_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bMapRange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InRange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OutRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutRange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bClampResult_MetaData[];
#endif
		static void NewProp_bClampResult_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bClampResult;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClampMin_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ClampMin;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClampMax_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ClampMax;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bInterpResult_MetaData[];
#endif
		static void NewProp_bInterpResult_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInterpResult;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InterpSpeedIncreasing_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InterpSpeedIncreasing;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InterpSpeedDecreasing_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InterpSpeedDecreasing;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Result;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScaleBiasClamp_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ScaleBiasClamp;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Adds a vector value over time over and over again\n */" },
		{ "DisplayName", "Interpolate" },
		{ "Keywords", "Alpha,Lerp,LinearInterpolate" },
		{ "MenuDescSuffix", "(Vector)" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_AlphaInterp.h" },
		{ "PrototypeName", "AlphaInterp" },
		{ "ToolTip", "Adds a vector value over time over and over again" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_AlphaInterpVector>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_Value_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_AlphaInterp.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AlphaInterpVector, Value), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_Scale_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_AlphaInterp.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_Scale = { "Scale", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AlphaInterpVector, Scale), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_Scale_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_Scale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_Bias_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_AlphaInterp.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_Bias = { "Bias", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AlphaInterpVector, Bias), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_Bias_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_Bias_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_bMapRange_MetaData[] = {
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_AlphaInterp.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_bMapRange_SetBit(void* Obj)
	{
		((FRigUnit_AlphaInterpVector*)Obj)->bMapRange = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_bMapRange = { "bMapRange", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_AlphaInterpVector), &Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_bMapRange_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_bMapRange_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_bMapRange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_InRange_MetaData[] = {
		{ "EditCondition", "bMapRange" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_AlphaInterp.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_InRange = { "InRange", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AlphaInterpVector, InRange), Z_Construct_UScriptStruct_FInputRange, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_InRange_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_InRange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_OutRange_MetaData[] = {
		{ "EditCondition", "bMapRange" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_AlphaInterp.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_OutRange = { "OutRange", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AlphaInterpVector, OutRange), Z_Construct_UScriptStruct_FInputRange, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_OutRange_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_OutRange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_bClampResult_MetaData[] = {
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_AlphaInterp.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_bClampResult_SetBit(void* Obj)
	{
		((FRigUnit_AlphaInterpVector*)Obj)->bClampResult = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_bClampResult = { "bClampResult", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_AlphaInterpVector), &Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_bClampResult_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_bClampResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_bClampResult_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_ClampMin_MetaData[] = {
		{ "EditCondition", "bClampResult" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_AlphaInterp.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_ClampMin = { "ClampMin", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AlphaInterpVector, ClampMin), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_ClampMin_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_ClampMin_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_ClampMax_MetaData[] = {
		{ "EditCondition", "bClampResult" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_AlphaInterp.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_ClampMax = { "ClampMax", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AlphaInterpVector, ClampMax), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_ClampMax_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_ClampMax_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_bInterpResult_MetaData[] = {
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_AlphaInterp.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_bInterpResult_SetBit(void* Obj)
	{
		((FRigUnit_AlphaInterpVector*)Obj)->bInterpResult = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_bInterpResult = { "bInterpResult", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_AlphaInterpVector), &Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_bInterpResult_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_bInterpResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_bInterpResult_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_InterpSpeedIncreasing_MetaData[] = {
		{ "EditCondition", "bInterpResult" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_AlphaInterp.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_InterpSpeedIncreasing = { "InterpSpeedIncreasing", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AlphaInterpVector, InterpSpeedIncreasing), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_InterpSpeedIncreasing_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_InterpSpeedIncreasing_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_InterpSpeedDecreasing_MetaData[] = {
		{ "EditCondition", "bInterpResult" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_AlphaInterp.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_InterpSpeedDecreasing = { "InterpSpeedDecreasing", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AlphaInterpVector, InterpSpeedDecreasing), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_InterpSpeedDecreasing_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_InterpSpeedDecreasing_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_AlphaInterp.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AlphaInterpVector, Result), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_Result_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_ScaleBiasClamp_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_AlphaInterp.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_ScaleBiasClamp = { "ScaleBiasClamp", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AlphaInterpVector, ScaleBiasClamp), Z_Construct_UScriptStruct_FInputScaleBiasClamp, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_ScaleBiasClamp_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_ScaleBiasClamp_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_Scale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_Bias,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_bMapRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_InRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_OutRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_bClampResult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_ClampMin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_ClampMax,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_bInterpResult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_InterpSpeedIncreasing,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_InterpSpeedDecreasing,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_Result,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::NewProp_ScaleBiasClamp,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_SimBase,
		&NewStructOps,
		"RigUnit_AlphaInterpVector",
		sizeof(FRigUnit_AlphaInterpVector),
		alignof(FRigUnit_AlphaInterpVector),
		Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_AlphaInterpVector"), sizeof(FRigUnit_AlphaInterpVector), Get_Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AlphaInterpVector_Hash() { return 2252001610U; }

void FRigUnit_AlphaInterpVector::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Scale,
		Bias,
		bMapRange,
		InRange,
		OutRange,
		bClampResult,
		ClampMin,
		ClampMax,
		bInterpResult,
		InterpSpeedIncreasing,
		InterpSpeedDecreasing,
		Result,
		ScaleBiasClamp,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_AlphaInterp>() == std::is_polymorphic<FRigUnit_SimBase>(), "USTRUCT FRigUnit_AlphaInterp cannot be polymorphic unless super FRigUnit_SimBase is polymorphic");

class UScriptStruct* FRigUnit_AlphaInterp::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_AlphaInterp, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_AlphaInterp"), sizeof(FRigUnit_AlphaInterp), Get_Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_AlphaInterp::Execute"), &FRigUnit_AlphaInterp::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_AlphaInterp>()
{
	return FRigUnit_AlphaInterp::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_AlphaInterp(FRigUnit_AlphaInterp::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_AlphaInterp"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AlphaInterp
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AlphaInterp()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_AlphaInterp>(FName(TEXT("RigUnit_AlphaInterp")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AlphaInterp;
	struct Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Scale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Scale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Bias_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Bias;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bMapRange_MetaData[];
#endif
		static void NewProp_bMapRange_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bMapRange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InRange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OutRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutRange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bClampResult_MetaData[];
#endif
		static void NewProp_bClampResult_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bClampResult;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClampMin_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ClampMin;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClampMax_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ClampMax;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bInterpResult_MetaData[];
#endif
		static void NewProp_bInterpResult_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInterpResult;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InterpSpeedIncreasing_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InterpSpeedIncreasing;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InterpSpeedDecreasing_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InterpSpeedDecreasing;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Result;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScaleBiasClamp_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ScaleBiasClamp;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Adds a float value over time over and over again\n */" },
		{ "DisplayName", "Interpolate" },
		{ "Keywords", "Alpha,Lerp,LinearInterpolate" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_AlphaInterp.h" },
		{ "PrototypeName", "AlphaInterp" },
		{ "ToolTip", "Adds a float value over time over and over again" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_AlphaInterp>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_Value_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_AlphaInterp.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AlphaInterp, Value), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_Scale_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_AlphaInterp.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_Scale = { "Scale", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AlphaInterp, Scale), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_Scale_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_Scale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_Bias_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_AlphaInterp.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_Bias = { "Bias", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AlphaInterp, Bias), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_Bias_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_Bias_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_bMapRange_MetaData[] = {
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_AlphaInterp.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_bMapRange_SetBit(void* Obj)
	{
		((FRigUnit_AlphaInterp*)Obj)->bMapRange = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_bMapRange = { "bMapRange", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_AlphaInterp), &Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_bMapRange_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_bMapRange_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_bMapRange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_InRange_MetaData[] = {
		{ "EditCondition", "bMapRange" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_AlphaInterp.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_InRange = { "InRange", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AlphaInterp, InRange), Z_Construct_UScriptStruct_FInputRange, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_InRange_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_InRange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_OutRange_MetaData[] = {
		{ "EditCondition", "bMapRange" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_AlphaInterp.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_OutRange = { "OutRange", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AlphaInterp, OutRange), Z_Construct_UScriptStruct_FInputRange, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_OutRange_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_OutRange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_bClampResult_MetaData[] = {
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_AlphaInterp.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_bClampResult_SetBit(void* Obj)
	{
		((FRigUnit_AlphaInterp*)Obj)->bClampResult = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_bClampResult = { "bClampResult", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_AlphaInterp), &Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_bClampResult_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_bClampResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_bClampResult_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_ClampMin_MetaData[] = {
		{ "EditCondition", "bClampResult" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_AlphaInterp.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_ClampMin = { "ClampMin", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AlphaInterp, ClampMin), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_ClampMin_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_ClampMin_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_ClampMax_MetaData[] = {
		{ "EditCondition", "bClampResult" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_AlphaInterp.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_ClampMax = { "ClampMax", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AlphaInterp, ClampMax), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_ClampMax_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_ClampMax_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_bInterpResult_MetaData[] = {
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_AlphaInterp.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_bInterpResult_SetBit(void* Obj)
	{
		((FRigUnit_AlphaInterp*)Obj)->bInterpResult = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_bInterpResult = { "bInterpResult", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_AlphaInterp), &Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_bInterpResult_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_bInterpResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_bInterpResult_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_InterpSpeedIncreasing_MetaData[] = {
		{ "EditCondition", "bInterpResult" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_AlphaInterp.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_InterpSpeedIncreasing = { "InterpSpeedIncreasing", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AlphaInterp, InterpSpeedIncreasing), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_InterpSpeedIncreasing_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_InterpSpeedIncreasing_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_InterpSpeedDecreasing_MetaData[] = {
		{ "EditCondition", "bInterpResult" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_AlphaInterp.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_InterpSpeedDecreasing = { "InterpSpeedDecreasing", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AlphaInterp, InterpSpeedDecreasing), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_InterpSpeedDecreasing_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_InterpSpeedDecreasing_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_AlphaInterp.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AlphaInterp, Result), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_Result_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_ScaleBiasClamp_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_AlphaInterp.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_ScaleBiasClamp = { "ScaleBiasClamp", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AlphaInterp, ScaleBiasClamp), Z_Construct_UScriptStruct_FInputScaleBiasClamp, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_ScaleBiasClamp_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_ScaleBiasClamp_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_Scale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_Bias,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_bMapRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_InRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_OutRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_bClampResult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_ClampMin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_ClampMax,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_bInterpResult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_InterpSpeedIncreasing,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_InterpSpeedDecreasing,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_Result,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::NewProp_ScaleBiasClamp,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_SimBase,
		&NewStructOps,
		"RigUnit_AlphaInterp",
		sizeof(FRigUnit_AlphaInterp),
		alignof(FRigUnit_AlphaInterp),
		Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_AlphaInterp()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_AlphaInterp"), sizeof(FRigUnit_AlphaInterp), Get_Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AlphaInterp_Hash() { return 1797533684U; }

void FRigUnit_AlphaInterp::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Scale,
		Bias,
		bMapRange,
		InRange,
		OutRange,
		bClampResult,
		ClampMin,
		ClampMax,
		bInterpResult,
		InterpSpeedIncreasing,
		InterpSpeedDecreasing,
		Result,
		ScaleBiasClamp,
		Context
	);
}

PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
