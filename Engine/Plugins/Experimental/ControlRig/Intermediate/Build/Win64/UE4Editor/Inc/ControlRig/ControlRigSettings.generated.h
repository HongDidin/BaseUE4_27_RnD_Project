// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_ControlRigSettings_generated_h
#error "ControlRigSettings.generated.h already included, missing '#pragma once' in ControlRigSettings.h"
#endif
#define CONTROLRIG_ControlRigSettings_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Settings_ControlRigSettings_h_19_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FControlRigSettingsPerPinBool_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FControlRigSettingsPerPinBool>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Settings_ControlRigSettings_h_31_SPARSE_DATA
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Settings_ControlRigSettings_h_31_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Settings_ControlRigSettings_h_31_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Settings_ControlRigSettings_h_31_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUControlRigSettings(); \
	friend struct Z_Construct_UClass_UControlRigSettings_Statics; \
public: \
	DECLARE_CLASS(UControlRigSettings, UDeveloperSettings, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/ControlRig"), NO_API) \
	DECLARE_SERIALIZER(UControlRigSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Settings_ControlRigSettings_h_31_INCLASS \
private: \
	static void StaticRegisterNativesUControlRigSettings(); \
	friend struct Z_Construct_UClass_UControlRigSettings_Statics; \
public: \
	DECLARE_CLASS(UControlRigSettings, UDeveloperSettings, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/ControlRig"), NO_API) \
	DECLARE_SERIALIZER(UControlRigSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Settings_ControlRigSettings_h_31_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UControlRigSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigSettings(UControlRigSettings&&); \
	NO_API UControlRigSettings(const UControlRigSettings&); \
public:


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Settings_ControlRigSettings_h_31_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UControlRigSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigSettings(UControlRigSettings&&); \
	NO_API UControlRigSettings(const UControlRigSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigSettings)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Settings_ControlRigSettings_h_31_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Settings_ControlRigSettings_h_28_PROLOG
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Settings_ControlRigSettings_h_31_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Settings_ControlRigSettings_h_31_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Settings_ControlRigSettings_h_31_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Settings_ControlRigSettings_h_31_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Settings_ControlRigSettings_h_31_INCLASS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Settings_ControlRigSettings_h_31_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Settings_ControlRigSettings_h_31_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Settings_ControlRigSettings_h_31_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Settings_ControlRigSettings_h_31_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Settings_ControlRigSettings_h_31_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Settings_ControlRigSettings_h_31_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Settings_ControlRigSettings_h_31_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ControlRigSettings."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONTROLRIG_API UClass* StaticClass<class UControlRigSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Settings_ControlRigSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
