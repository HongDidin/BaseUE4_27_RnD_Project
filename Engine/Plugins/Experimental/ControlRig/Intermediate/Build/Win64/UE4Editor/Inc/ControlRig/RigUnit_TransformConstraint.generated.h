// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigUnit_TransformConstraint_generated_h
#error "RigUnit_TransformConstraint.generated.h already included, missing '#pragma once' in RigUnit_TransformConstraint.h"
#endif
#define CONTROLRIG_RigUnit_TransformConstraint_generated_h


#define FRigUnit_TransformConstraintPerItem_Execute() \
	void FRigUnit_TransformConstraintPerItem::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKey& Item, \
		const ETransformSpaceMode BaseTransformSpace, \
		const FTransform& BaseTransform, \
		const FRigElementKey& BaseItem, \
		const FRigVMFixedArray<FConstraintTarget>& Targets, \
		const bool bUseInitialTransforms, \
		FRigUnit_TransformConstraint_WorkData& WorkData, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_TransformConstraint_h_122_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_TransformConstraintPerItem_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKey& Item, \
		const ETransformSpaceMode BaseTransformSpace, \
		const FTransform& BaseTransform, \
		const FRigElementKey& BaseItem, \
		const FRigVMFixedArray<FConstraintTarget>& Targets, \
		const bool bUseInitialTransforms, \
		FRigUnit_TransformConstraint_WorkData& WorkData, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FRigElementKey& Item = *(FRigElementKey*)RigVMMemoryHandles[0].GetData(); \
		ETransformSpaceMode BaseTransformSpace = (ETransformSpaceMode)*(uint8*)RigVMMemoryHandles[1].GetData(); \
		const FTransform& BaseTransform = *(FTransform*)RigVMMemoryHandles[2].GetData(); \
		const FRigElementKey& BaseItem = *(FRigElementKey*)RigVMMemoryHandles[3].GetData(); \
		FRigVMFixedArray<FConstraintTarget> Targets((FConstraintTarget*)RigVMMemoryHandles[4].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[5].GetData())); \
		const bool bUseInitialTransforms = *(bool*)RigVMMemoryHandles[6].GetData(); \
		FRigVMDynamicArray<FRigUnit_TransformConstraint_WorkData> WorkData_7_Array(*((FRigVMByteArray*)RigVMMemoryHandles[7].GetData(0, false))); \
		WorkData_7_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FRigUnit_TransformConstraint_WorkData& WorkData = WorkData_7_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[8].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Item, \
			BaseTransformSpace, \
			BaseTransform, \
			BaseItem, \
			Targets, \
			bUseInitialTransforms, \
			WorkData, \
			ExecuteContext, \
			Context \
		); \
	} \
	FORCEINLINE static uint32 __PPO__WorkData() { return STRUCT_OFFSET(FRigUnit_TransformConstraintPerItem, WorkData); } \
	typedef FRigUnit_HighlevelBaseMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_TransformConstraintPerItem>();


#define FRigUnit_TransformConstraint_Execute() \
	void FRigUnit_TransformConstraint::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Bone, \
		const ETransformSpaceMode BaseTransformSpace, \
		const FTransform& BaseTransform, \
		const FName& BaseBone, \
		const FRigVMFixedArray<FConstraintTarget>& Targets, \
		const bool bUseInitialTransforms, \
		FRigUnit_TransformConstraint_WorkData& WorkData, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_TransformConstraint_h_55_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Bone, \
		const ETransformSpaceMode BaseTransformSpace, \
		const FTransform& BaseTransform, \
		const FName& BaseBone, \
		const FRigVMFixedArray<FConstraintTarget>& Targets, \
		const bool bUseInitialTransforms, \
		FRigUnit_TransformConstraint_WorkData& WorkData, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FName& Bone = *(FName*)RigVMMemoryHandles[0].GetData(); \
		ETransformSpaceMode BaseTransformSpace = (ETransformSpaceMode)*(uint8*)RigVMMemoryHandles[1].GetData(); \
		const FTransform& BaseTransform = *(FTransform*)RigVMMemoryHandles[2].GetData(); \
		const FName& BaseBone = *(FName*)RigVMMemoryHandles[3].GetData(); \
		FRigVMFixedArray<FConstraintTarget> Targets((FConstraintTarget*)RigVMMemoryHandles[4].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[5].GetData())); \
		const bool bUseInitialTransforms = *(bool*)RigVMMemoryHandles[6].GetData(); \
		FRigVMDynamicArray<FRigUnit_TransformConstraint_WorkData> WorkData_7_Array(*((FRigVMByteArray*)RigVMMemoryHandles[7].GetData(0, false))); \
		WorkData_7_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FRigUnit_TransformConstraint_WorkData& WorkData = WorkData_7_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[8].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Bone, \
			BaseTransformSpace, \
			BaseTransform, \
			BaseBone, \
			Targets, \
			bUseInitialTransforms, \
			WorkData, \
			ExecuteContext, \
			Context \
		); \
	} \
	FORCEINLINE static uint32 __PPO__WorkData() { return STRUCT_OFFSET(FRigUnit_TransformConstraint, WorkData); } \
	typedef FRigUnit_HighlevelBaseMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_TransformConstraint>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_TransformConstraint_h_42_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_TransformConstraint_WorkData_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_TransformConstraint_WorkData>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_TransformConstraint_h_19_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConstraintTarget_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FConstraintTarget>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_TransformConstraint_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
