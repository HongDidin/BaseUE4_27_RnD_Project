// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Execution/RigUnit_Collection.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_Collection() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_CollectionLoop();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_CollectionBaseMutable();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigElementKeyCollection();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigElementKey();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FControlRigExecuteContext();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_CollectionItemAtIndex();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_CollectionBase();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_CollectionCount();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_CollectionReverse();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_CollectionDifference();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_CollectionIntersection();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_CollectionUnion();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_CollectionItems();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_CollectionChildren();
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_ERigElementType();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_CollectionChain();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnitMutable();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_CollectionLoop>() == std::is_polymorphic<FRigUnit_CollectionBaseMutable>(), "USTRUCT FRigUnit_CollectionLoop cannot be polymorphic unless super FRigUnit_CollectionBaseMutable is polymorphic");

class UScriptStruct* FRigUnit_CollectionLoop::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_CollectionLoop, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_CollectionLoop"), sizeof(FRigUnit_CollectionLoop), Get_Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_CollectionLoop::Execute"), &FRigUnit_CollectionLoop::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_CollectionLoop>()
{
	return FRigUnit_CollectionLoop::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_CollectionLoop(FRigUnit_CollectionLoop::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_CollectionLoop"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionLoop
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionLoop()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_CollectionLoop>(FName(TEXT("RigUnit_CollectionLoop")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionLoop;
	struct Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Collection_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Collection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Item_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Item;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Count_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Count;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Ratio_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Ratio;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Continue_MetaData[];
#endif
		static void NewProp_Continue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Continue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Completed_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Completed;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Given a collection of items, execute iteratively across all items in a given collection\n */" },
		{ "DisplayName", "For Each Item" },
		{ "Keywords", "Collection,Loop,Iterate" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
		{ "ToolTip", "Given a collection of items, execute iteratively across all items in a given collection" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_CollectionLoop>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::NewProp_Collection_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::NewProp_Collection = { "Collection", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionLoop, Collection), Z_Construct_UScriptStruct_FRigElementKeyCollection, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::NewProp_Collection_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::NewProp_Collection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::NewProp_Item_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::NewProp_Item = { "Item", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionLoop, Item), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::NewProp_Item_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::NewProp_Item_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::NewProp_Index_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
		{ "Output", "" },
		{ "Singleton", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::NewProp_Index = { "Index", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionLoop, Index), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::NewProp_Index_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::NewProp_Index_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::NewProp_Count_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
		{ "Output", "" },
		{ "Singleton", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::NewProp_Count = { "Count", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionLoop, Count), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::NewProp_Count_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::NewProp_Count_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::NewProp_Ratio_MetaData[] = {
		{ "Comment", "/**\n\x09 * Ranging from 0.0 (first item) and 1.0 (last item)\n\x09 * This is useful to drive a consecutive node with a \n\x09 * curve or an ease to distribute a value.\n\x09 */" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
		{ "Output", "" },
		{ "Singleton", "" },
		{ "ToolTip", "Ranging from 0.0 (first item) and 1.0 (last item)\nThis is useful to drive a consecutive node with a\ncurve or an ease to distribute a value." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::NewProp_Ratio = { "Ratio", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionLoop, Ratio), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::NewProp_Ratio_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::NewProp_Ratio_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::NewProp_Continue_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
		{ "Singleton", "" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::NewProp_Continue_SetBit(void* Obj)
	{
		((FRigUnit_CollectionLoop*)Obj)->Continue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::NewProp_Continue = { "Continue", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_CollectionLoop), &Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::NewProp_Continue_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::NewProp_Continue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::NewProp_Continue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::NewProp_Completed_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::NewProp_Completed = { "Completed", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionLoop, Completed), Z_Construct_UScriptStruct_FControlRigExecuteContext, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::NewProp_Completed_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::NewProp_Completed_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::NewProp_Collection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::NewProp_Item,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::NewProp_Count,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::NewProp_Ratio,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::NewProp_Continue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::NewProp_Completed,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_CollectionBaseMutable,
		&NewStructOps,
		"RigUnit_CollectionLoop",
		sizeof(FRigUnit_CollectionLoop),
		alignof(FRigUnit_CollectionLoop),
		Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_CollectionLoop()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_CollectionLoop"), sizeof(FRigUnit_CollectionLoop), Get_Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionLoop_Hash() { return 1100778487U; }

void FRigUnit_CollectionLoop::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Collection,
		Item,
		Index,
		Count,
		Ratio,
		Continue,
		Completed,
		ExecuteContext,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_CollectionItemAtIndex>() == std::is_polymorphic<FRigUnit_CollectionBase>(), "USTRUCT FRigUnit_CollectionItemAtIndex cannot be polymorphic unless super FRigUnit_CollectionBase is polymorphic");

class UScriptStruct* FRigUnit_CollectionItemAtIndex::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionItemAtIndex_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_CollectionItemAtIndex, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_CollectionItemAtIndex"), sizeof(FRigUnit_CollectionItemAtIndex), Get_Z_Construct_UScriptStruct_FRigUnit_CollectionItemAtIndex_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_CollectionItemAtIndex::Execute"), &FRigUnit_CollectionItemAtIndex::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_CollectionItemAtIndex>()
{
	return FRigUnit_CollectionItemAtIndex::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_CollectionItemAtIndex(FRigUnit_CollectionItemAtIndex::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_CollectionItemAtIndex"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionItemAtIndex
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionItemAtIndex()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_CollectionItemAtIndex>(FName(TEXT("RigUnit_CollectionItemAtIndex")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionItemAtIndex;
	struct Z_Construct_UScriptStruct_FRigUnit_CollectionItemAtIndex_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Collection_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Collection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Item_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Item;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionItemAtIndex_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns a single item within a collection by index\n */" },
		{ "DisplayName", "Item At Index" },
		{ "Keywords", "Item,GetIndex,AtIndex,At,ForIndex,[]" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
		{ "ToolTip", "Returns a single item within a collection by index" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_CollectionItemAtIndex_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_CollectionItemAtIndex>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionItemAtIndex_Statics::NewProp_Collection_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionItemAtIndex_Statics::NewProp_Collection = { "Collection", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionItemAtIndex, Collection), Z_Construct_UScriptStruct_FRigElementKeyCollection, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionItemAtIndex_Statics::NewProp_Collection_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionItemAtIndex_Statics::NewProp_Collection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionItemAtIndex_Statics::NewProp_Index_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionItemAtIndex_Statics::NewProp_Index = { "Index", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionItemAtIndex, Index), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionItemAtIndex_Statics::NewProp_Index_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionItemAtIndex_Statics::NewProp_Index_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionItemAtIndex_Statics::NewProp_Item_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionItemAtIndex_Statics::NewProp_Item = { "Item", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionItemAtIndex, Item), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionItemAtIndex_Statics::NewProp_Item_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionItemAtIndex_Statics::NewProp_Item_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_CollectionItemAtIndex_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionItemAtIndex_Statics::NewProp_Collection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionItemAtIndex_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionItemAtIndex_Statics::NewProp_Item,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_CollectionItemAtIndex_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_CollectionBase,
		&NewStructOps,
		"RigUnit_CollectionItemAtIndex",
		sizeof(FRigUnit_CollectionItemAtIndex),
		alignof(FRigUnit_CollectionItemAtIndex),
		Z_Construct_UScriptStruct_FRigUnit_CollectionItemAtIndex_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionItemAtIndex_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionItemAtIndex_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionItemAtIndex_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_CollectionItemAtIndex()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionItemAtIndex_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_CollectionItemAtIndex"), sizeof(FRigUnit_CollectionItemAtIndex), Get_Z_Construct_UScriptStruct_FRigUnit_CollectionItemAtIndex_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_CollectionItemAtIndex_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionItemAtIndex_Hash() { return 4225850051U; }

void FRigUnit_CollectionItemAtIndex::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Collection,
		Index,
		Item,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_CollectionCount>() == std::is_polymorphic<FRigUnit_CollectionBase>(), "USTRUCT FRigUnit_CollectionCount cannot be polymorphic unless super FRigUnit_CollectionBase is polymorphic");

class UScriptStruct* FRigUnit_CollectionCount::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionCount_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_CollectionCount, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_CollectionCount"), sizeof(FRigUnit_CollectionCount), Get_Z_Construct_UScriptStruct_FRigUnit_CollectionCount_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_CollectionCount::Execute"), &FRigUnit_CollectionCount::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_CollectionCount>()
{
	return FRigUnit_CollectionCount::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_CollectionCount(FRigUnit_CollectionCount::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_CollectionCount"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionCount
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionCount()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_CollectionCount>(FName(TEXT("RigUnit_CollectionCount")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionCount;
	struct Z_Construct_UScriptStruct_FRigUnit_CollectionCount_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Collection_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Collection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Count_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Count;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionCount_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the number of elements in a collection\n */" },
		{ "DisplayName", "Count" },
		{ "Keywords", "Collection,Array,Count,Num,Length,Size" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
		{ "ToolTip", "Returns the number of elements in a collection" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_CollectionCount_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_CollectionCount>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionCount_Statics::NewProp_Collection_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionCount_Statics::NewProp_Collection = { "Collection", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionCount, Collection), Z_Construct_UScriptStruct_FRigElementKeyCollection, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionCount_Statics::NewProp_Collection_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionCount_Statics::NewProp_Collection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionCount_Statics::NewProp_Count_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionCount_Statics::NewProp_Count = { "Count", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionCount, Count), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionCount_Statics::NewProp_Count_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionCount_Statics::NewProp_Count_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_CollectionCount_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionCount_Statics::NewProp_Collection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionCount_Statics::NewProp_Count,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_CollectionCount_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_CollectionBase,
		&NewStructOps,
		"RigUnit_CollectionCount",
		sizeof(FRigUnit_CollectionCount),
		alignof(FRigUnit_CollectionCount),
		Z_Construct_UScriptStruct_FRigUnit_CollectionCount_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionCount_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionCount_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionCount_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_CollectionCount()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionCount_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_CollectionCount"), sizeof(FRigUnit_CollectionCount), Get_Z_Construct_UScriptStruct_FRigUnit_CollectionCount_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_CollectionCount_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionCount_Hash() { return 4127341121U; }

void FRigUnit_CollectionCount::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Collection,
		Count,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_CollectionReverse>() == std::is_polymorphic<FRigUnit_CollectionBase>(), "USTRUCT FRigUnit_CollectionReverse cannot be polymorphic unless super FRigUnit_CollectionBase is polymorphic");

class UScriptStruct* FRigUnit_CollectionReverse::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionReverse_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_CollectionReverse, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_CollectionReverse"), sizeof(FRigUnit_CollectionReverse), Get_Z_Construct_UScriptStruct_FRigUnit_CollectionReverse_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_CollectionReverse::Execute"), &FRigUnit_CollectionReverse::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_CollectionReverse>()
{
	return FRigUnit_CollectionReverse::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_CollectionReverse(FRigUnit_CollectionReverse::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_CollectionReverse"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionReverse
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionReverse()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_CollectionReverse>(FName(TEXT("RigUnit_CollectionReverse")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionReverse;
	struct Z_Construct_UScriptStruct_FRigUnit_CollectionReverse_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Collection_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Collection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Reversed_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Reversed;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionReverse_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the collection in reverse order\n */" },
		{ "DisplayName", "Reverse" },
		{ "Keywords", "Direction,Order,Reverse" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
		{ "ToolTip", "Returns the collection in reverse order" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_CollectionReverse_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_CollectionReverse>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionReverse_Statics::NewProp_Collection_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionReverse_Statics::NewProp_Collection = { "Collection", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionReverse, Collection), Z_Construct_UScriptStruct_FRigElementKeyCollection, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionReverse_Statics::NewProp_Collection_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionReverse_Statics::NewProp_Collection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionReverse_Statics::NewProp_Reversed_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionReverse_Statics::NewProp_Reversed = { "Reversed", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionReverse, Reversed), Z_Construct_UScriptStruct_FRigElementKeyCollection, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionReverse_Statics::NewProp_Reversed_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionReverse_Statics::NewProp_Reversed_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_CollectionReverse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionReverse_Statics::NewProp_Collection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionReverse_Statics::NewProp_Reversed,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_CollectionReverse_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_CollectionBase,
		&NewStructOps,
		"RigUnit_CollectionReverse",
		sizeof(FRigUnit_CollectionReverse),
		alignof(FRigUnit_CollectionReverse),
		Z_Construct_UScriptStruct_FRigUnit_CollectionReverse_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionReverse_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionReverse_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionReverse_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_CollectionReverse()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionReverse_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_CollectionReverse"), sizeof(FRigUnit_CollectionReverse), Get_Z_Construct_UScriptStruct_FRigUnit_CollectionReverse_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_CollectionReverse_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionReverse_Hash() { return 22047672U; }

void FRigUnit_CollectionReverse::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Collection,
		Reversed,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_CollectionDifference>() == std::is_polymorphic<FRigUnit_CollectionBase>(), "USTRUCT FRigUnit_CollectionDifference cannot be polymorphic unless super FRigUnit_CollectionBase is polymorphic");

class UScriptStruct* FRigUnit_CollectionDifference::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionDifference_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_CollectionDifference, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_CollectionDifference"), sizeof(FRigUnit_CollectionDifference), Get_Z_Construct_UScriptStruct_FRigUnit_CollectionDifference_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_CollectionDifference::Execute"), &FRigUnit_CollectionDifference::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_CollectionDifference>()
{
	return FRigUnit_CollectionDifference::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_CollectionDifference(FRigUnit_CollectionDifference::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_CollectionDifference"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionDifference
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionDifference()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_CollectionDifference>(FName(TEXT("RigUnit_CollectionDifference")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionDifference;
	struct Z_Construct_UScriptStruct_FRigUnit_CollectionDifference_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_A_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_A;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_B_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_B;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Collection_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Collection;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionDifference_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the difference between two collections\n * (the items present in A but not in B).\n */" },
		{ "DisplayName", "Difference" },
		{ "Keywords", "Collection,Exclude,Subtract" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
		{ "ToolTip", "Returns the difference between two collections\n(the items present in A but not in B)." },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_CollectionDifference_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_CollectionDifference>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionDifference_Statics::NewProp_A_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionDifference_Statics::NewProp_A = { "A", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionDifference, A), Z_Construct_UScriptStruct_FRigElementKeyCollection, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionDifference_Statics::NewProp_A_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionDifference_Statics::NewProp_A_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionDifference_Statics::NewProp_B_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionDifference_Statics::NewProp_B = { "B", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionDifference, B), Z_Construct_UScriptStruct_FRigElementKeyCollection, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionDifference_Statics::NewProp_B_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionDifference_Statics::NewProp_B_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionDifference_Statics::NewProp_Collection_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionDifference_Statics::NewProp_Collection = { "Collection", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionDifference, Collection), Z_Construct_UScriptStruct_FRigElementKeyCollection, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionDifference_Statics::NewProp_Collection_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionDifference_Statics::NewProp_Collection_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_CollectionDifference_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionDifference_Statics::NewProp_A,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionDifference_Statics::NewProp_B,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionDifference_Statics::NewProp_Collection,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_CollectionDifference_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_CollectionBase,
		&NewStructOps,
		"RigUnit_CollectionDifference",
		sizeof(FRigUnit_CollectionDifference),
		alignof(FRigUnit_CollectionDifference),
		Z_Construct_UScriptStruct_FRigUnit_CollectionDifference_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionDifference_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionDifference_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionDifference_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_CollectionDifference()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionDifference_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_CollectionDifference"), sizeof(FRigUnit_CollectionDifference), Get_Z_Construct_UScriptStruct_FRigUnit_CollectionDifference_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_CollectionDifference_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionDifference_Hash() { return 875702477U; }

void FRigUnit_CollectionDifference::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Collection,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_CollectionIntersection>() == std::is_polymorphic<FRigUnit_CollectionBase>(), "USTRUCT FRigUnit_CollectionIntersection cannot be polymorphic unless super FRigUnit_CollectionBase is polymorphic");

class UScriptStruct* FRigUnit_CollectionIntersection::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionIntersection_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_CollectionIntersection, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_CollectionIntersection"), sizeof(FRigUnit_CollectionIntersection), Get_Z_Construct_UScriptStruct_FRigUnit_CollectionIntersection_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_CollectionIntersection::Execute"), &FRigUnit_CollectionIntersection::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_CollectionIntersection>()
{
	return FRigUnit_CollectionIntersection::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_CollectionIntersection(FRigUnit_CollectionIntersection::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_CollectionIntersection"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionIntersection
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionIntersection()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_CollectionIntersection>(FName(TEXT("RigUnit_CollectionIntersection")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionIntersection;
	struct Z_Construct_UScriptStruct_FRigUnit_CollectionIntersection_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_A_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_A;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_B_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_B;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Collection_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Collection;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionIntersection_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the intersection of two provided collections\n * (the items present in both A and B).\n */" },
		{ "DisplayName", "Intersection" },
		{ "Keywords", "Combine,Merge,Collection,Hierarchy" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
		{ "ToolTip", "Returns the intersection of two provided collections\n(the items present in both A and B)." },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_CollectionIntersection_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_CollectionIntersection>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionIntersection_Statics::NewProp_A_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionIntersection_Statics::NewProp_A = { "A", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionIntersection, A), Z_Construct_UScriptStruct_FRigElementKeyCollection, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionIntersection_Statics::NewProp_A_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionIntersection_Statics::NewProp_A_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionIntersection_Statics::NewProp_B_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionIntersection_Statics::NewProp_B = { "B", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionIntersection, B), Z_Construct_UScriptStruct_FRigElementKeyCollection, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionIntersection_Statics::NewProp_B_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionIntersection_Statics::NewProp_B_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionIntersection_Statics::NewProp_Collection_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionIntersection_Statics::NewProp_Collection = { "Collection", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionIntersection, Collection), Z_Construct_UScriptStruct_FRigElementKeyCollection, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionIntersection_Statics::NewProp_Collection_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionIntersection_Statics::NewProp_Collection_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_CollectionIntersection_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionIntersection_Statics::NewProp_A,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionIntersection_Statics::NewProp_B,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionIntersection_Statics::NewProp_Collection,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_CollectionIntersection_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_CollectionBase,
		&NewStructOps,
		"RigUnit_CollectionIntersection",
		sizeof(FRigUnit_CollectionIntersection),
		alignof(FRigUnit_CollectionIntersection),
		Z_Construct_UScriptStruct_FRigUnit_CollectionIntersection_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionIntersection_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionIntersection_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionIntersection_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_CollectionIntersection()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionIntersection_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_CollectionIntersection"), sizeof(FRigUnit_CollectionIntersection), Get_Z_Construct_UScriptStruct_FRigUnit_CollectionIntersection_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_CollectionIntersection_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionIntersection_Hash() { return 2748321989U; }

void FRigUnit_CollectionIntersection::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Collection,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_CollectionUnion>() == std::is_polymorphic<FRigUnit_CollectionBase>(), "USTRUCT FRigUnit_CollectionUnion cannot be polymorphic unless super FRigUnit_CollectionBase is polymorphic");

class UScriptStruct* FRigUnit_CollectionUnion::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionUnion_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_CollectionUnion, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_CollectionUnion"), sizeof(FRigUnit_CollectionUnion), Get_Z_Construct_UScriptStruct_FRigUnit_CollectionUnion_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_CollectionUnion::Execute"), &FRigUnit_CollectionUnion::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_CollectionUnion>()
{
	return FRigUnit_CollectionUnion::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_CollectionUnion(FRigUnit_CollectionUnion::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_CollectionUnion"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionUnion
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionUnion()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_CollectionUnion>(FName(TEXT("RigUnit_CollectionUnion")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionUnion;
	struct Z_Construct_UScriptStruct_FRigUnit_CollectionUnion_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_A_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_A;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_B_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_B;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Collection_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Collection;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionUnion_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the union of two provided collections\n * (the combination of all items from both A and B).\n */" },
		{ "DisplayName", "Union" },
		{ "Keywords", "Combine,Add,Merge,Collection,Hierarchy" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
		{ "ToolTip", "Returns the union of two provided collections\n(the combination of all items from both A and B)." },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_CollectionUnion_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_CollectionUnion>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionUnion_Statics::NewProp_A_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionUnion_Statics::NewProp_A = { "A", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionUnion, A), Z_Construct_UScriptStruct_FRigElementKeyCollection, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionUnion_Statics::NewProp_A_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionUnion_Statics::NewProp_A_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionUnion_Statics::NewProp_B_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionUnion_Statics::NewProp_B = { "B", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionUnion, B), Z_Construct_UScriptStruct_FRigElementKeyCollection, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionUnion_Statics::NewProp_B_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionUnion_Statics::NewProp_B_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionUnion_Statics::NewProp_Collection_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionUnion_Statics::NewProp_Collection = { "Collection", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionUnion, Collection), Z_Construct_UScriptStruct_FRigElementKeyCollection, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionUnion_Statics::NewProp_Collection_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionUnion_Statics::NewProp_Collection_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_CollectionUnion_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionUnion_Statics::NewProp_A,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionUnion_Statics::NewProp_B,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionUnion_Statics::NewProp_Collection,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_CollectionUnion_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_CollectionBase,
		&NewStructOps,
		"RigUnit_CollectionUnion",
		sizeof(FRigUnit_CollectionUnion),
		alignof(FRigUnit_CollectionUnion),
		Z_Construct_UScriptStruct_FRigUnit_CollectionUnion_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionUnion_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionUnion_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionUnion_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_CollectionUnion()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionUnion_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_CollectionUnion"), sizeof(FRigUnit_CollectionUnion), Get_Z_Construct_UScriptStruct_FRigUnit_CollectionUnion_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_CollectionUnion_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionUnion_Hash() { return 3763140379U; }

void FRigUnit_CollectionUnion::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Collection,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_CollectionItems>() == std::is_polymorphic<FRigUnit_CollectionBase>(), "USTRUCT FRigUnit_CollectionItems cannot be polymorphic unless super FRigUnit_CollectionBase is polymorphic");

class UScriptStruct* FRigUnit_CollectionItems::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionItems_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_CollectionItems, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_CollectionItems"), sizeof(FRigUnit_CollectionItems), Get_Z_Construct_UScriptStruct_FRigUnit_CollectionItems_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_CollectionItems::Execute"), &FRigUnit_CollectionItems::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_CollectionItems>()
{
	return FRigUnit_CollectionItems::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_CollectionItems(FRigUnit_CollectionItems::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_CollectionItems"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionItems
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionItems()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_CollectionItems>(FName(TEXT("RigUnit_CollectionItems")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionItems;
	struct Z_Construct_UScriptStruct_FRigUnit_CollectionItems_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Items_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Items_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Items;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Collection_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Collection;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionItems_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns a collection provided a specific list of items.\n */" },
		{ "DisplayName", "Items" },
		{ "Keywords", "Collection,Array" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
		{ "ToolTip", "Returns a collection provided a specific list of items." },
		{ "Varying", "" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_CollectionItems_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_CollectionItems>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionItems_Statics::NewProp_Items_Inner = { "Items", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionItems_Statics::NewProp_Items_MetaData[] = {
		{ "ExpandByDefault", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionItems_Statics::NewProp_Items = { "Items", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionItems, Items), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionItems_Statics::NewProp_Items_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionItems_Statics::NewProp_Items_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionItems_Statics::NewProp_Collection_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionItems_Statics::NewProp_Collection = { "Collection", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionItems, Collection), Z_Construct_UScriptStruct_FRigElementKeyCollection, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionItems_Statics::NewProp_Collection_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionItems_Statics::NewProp_Collection_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_CollectionItems_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionItems_Statics::NewProp_Items_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionItems_Statics::NewProp_Items,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionItems_Statics::NewProp_Collection,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_CollectionItems_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_CollectionBase,
		&NewStructOps,
		"RigUnit_CollectionItems",
		sizeof(FRigUnit_CollectionItems),
		alignof(FRigUnit_CollectionItems),
		Z_Construct_UScriptStruct_FRigUnit_CollectionItems_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionItems_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionItems_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionItems_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_CollectionItems()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionItems_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_CollectionItems"), sizeof(FRigUnit_CollectionItems), Get_Z_Construct_UScriptStruct_FRigUnit_CollectionItems_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_CollectionItems_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionItems_Hash() { return 1799075724U; }

void FRigUnit_CollectionItems::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
	FRigVMFixedArray<FRigElementKey> Items_0_Array(Items);
	
    StaticExecute(
		RigVMExecuteContext,
		Items_0_Array,
		Collection,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_CollectionReplaceItems>() == std::is_polymorphic<FRigUnit_CollectionBase>(), "USTRUCT FRigUnit_CollectionReplaceItems cannot be polymorphic unless super FRigUnit_CollectionBase is polymorphic");

class UScriptStruct* FRigUnit_CollectionReplaceItems::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_CollectionReplaceItems"), sizeof(FRigUnit_CollectionReplaceItems), Get_Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_CollectionReplaceItems::Execute"), &FRigUnit_CollectionReplaceItems::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_CollectionReplaceItems>()
{
	return FRigUnit_CollectionReplaceItems::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_CollectionReplaceItems(FRigUnit_CollectionReplaceItems::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_CollectionReplaceItems"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionReplaceItems
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionReplaceItems()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_CollectionReplaceItems>(FName(TEXT("RigUnit_CollectionReplaceItems")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionReplaceItems;
	struct Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Items_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Items;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Old_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Old;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_New_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_New;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RemoveInvalidItems_MetaData[];
#endif
		static void NewProp_RemoveInvalidItems_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_RemoveInvalidItems;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Collection_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Collection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedCollection_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CachedCollection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedHierarchyHash_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_CachedHierarchyHash;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Replaces all names within the collection\n */" },
		{ "DisplayName", "Replace Items" },
		{ "Keywords", "Replace,Find" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
		{ "ToolTip", "Replaces all names within the collection" },
		{ "Varying", "" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_CollectionReplaceItems>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::NewProp_Items_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::NewProp_Items = { "Items", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionReplaceItems, Items), Z_Construct_UScriptStruct_FRigElementKeyCollection, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::NewProp_Items_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::NewProp_Items_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::NewProp_Old_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::NewProp_Old = { "Old", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionReplaceItems, Old), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::NewProp_Old_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::NewProp_Old_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::NewProp_New_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::NewProp_New = { "New", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionReplaceItems, New), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::NewProp_New_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::NewProp_New_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::NewProp_RemoveInvalidItems_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::NewProp_RemoveInvalidItems_SetBit(void* Obj)
	{
		((FRigUnit_CollectionReplaceItems*)Obj)->RemoveInvalidItems = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::NewProp_RemoveInvalidItems = { "RemoveInvalidItems", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_CollectionReplaceItems), &Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::NewProp_RemoveInvalidItems_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::NewProp_RemoveInvalidItems_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::NewProp_RemoveInvalidItems_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::NewProp_Collection_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::NewProp_Collection = { "Collection", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionReplaceItems, Collection), Z_Construct_UScriptStruct_FRigElementKeyCollection, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::NewProp_Collection_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::NewProp_Collection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::NewProp_CachedCollection_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::NewProp_CachedCollection = { "CachedCollection", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionReplaceItems, CachedCollection), Z_Construct_UScriptStruct_FRigElementKeyCollection, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::NewProp_CachedCollection_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::NewProp_CachedCollection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::NewProp_CachedHierarchyHash_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::NewProp_CachedHierarchyHash = { "CachedHierarchyHash", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionReplaceItems, CachedHierarchyHash), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::NewProp_CachedHierarchyHash_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::NewProp_CachedHierarchyHash_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::NewProp_Items,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::NewProp_Old,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::NewProp_New,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::NewProp_RemoveInvalidItems,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::NewProp_Collection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::NewProp_CachedCollection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::NewProp_CachedHierarchyHash,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_CollectionBase,
		&NewStructOps,
		"RigUnit_CollectionReplaceItems",
		sizeof(FRigUnit_CollectionReplaceItems),
		alignof(FRigUnit_CollectionReplaceItems),
		Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_CollectionReplaceItems"), sizeof(FRigUnit_CollectionReplaceItems), Get_Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionReplaceItems_Hash() { return 4031963490U; }

void FRigUnit_CollectionReplaceItems::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Items,
		Old,
		New,
		RemoveInvalidItems,
		Collection,
		CachedCollection,
		CachedHierarchyHash,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_CollectionChildren>() == std::is_polymorphic<FRigUnit_CollectionBase>(), "USTRUCT FRigUnit_CollectionChildren cannot be polymorphic unless super FRigUnit_CollectionBase is polymorphic");

class UScriptStruct* FRigUnit_CollectionChildren::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_CollectionChildren, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_CollectionChildren"), sizeof(FRigUnit_CollectionChildren), Get_Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_CollectionChildren::Execute"), &FRigUnit_CollectionChildren::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_CollectionChildren>()
{
	return FRigUnit_CollectionChildren::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_CollectionChildren(FRigUnit_CollectionChildren::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_CollectionChildren"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionChildren
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionChildren()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_CollectionChildren>(FName(TEXT("RigUnit_CollectionChildren")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionChildren;
	struct Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Parent_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Parent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIncludeParent_MetaData[];
#endif
		static void NewProp_bIncludeParent_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIncludeParent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRecursive_MetaData[];
#endif
		static void NewProp_bRecursive_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRecursive;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TypeToSearch_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TypeToSearch_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TypeToSearch;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Collection_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Collection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedCollection_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CachedCollection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedHierarchyHash_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_CachedHierarchyHash;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Creates a collection based on the direct or recursive children\n * of a provided parent item. Returns an empty collection for an invalid parent item.\n */" },
		{ "DisplayName", "Children" },
		{ "Keywords", "Bone,Joint,Collection,Filter,Parent" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
		{ "ToolTip", "Creates a collection based on the direct or recursive children\nof a provided parent item. Returns an empty collection for an invalid parent item." },
		{ "Varying", "" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_CollectionChildren>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewProp_Parent_MetaData[] = {
		{ "ExpandByDefault", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewProp_Parent = { "Parent", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionChildren, Parent), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewProp_Parent_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewProp_Parent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewProp_bIncludeParent_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewProp_bIncludeParent_SetBit(void* Obj)
	{
		((FRigUnit_CollectionChildren*)Obj)->bIncludeParent = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewProp_bIncludeParent = { "bIncludeParent", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_CollectionChildren), &Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewProp_bIncludeParent_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewProp_bIncludeParent_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewProp_bIncludeParent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewProp_bRecursive_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewProp_bRecursive_SetBit(void* Obj)
	{
		((FRigUnit_CollectionChildren*)Obj)->bRecursive = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewProp_bRecursive = { "bRecursive", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_CollectionChildren), &Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewProp_bRecursive_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewProp_bRecursive_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewProp_bRecursive_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewProp_TypeToSearch_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewProp_TypeToSearch_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewProp_TypeToSearch = { "TypeToSearch", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionChildren, TypeToSearch), Z_Construct_UEnum_ControlRig_ERigElementType, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewProp_TypeToSearch_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewProp_TypeToSearch_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewProp_Collection_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewProp_Collection = { "Collection", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionChildren, Collection), Z_Construct_UScriptStruct_FRigElementKeyCollection, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewProp_Collection_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewProp_Collection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewProp_CachedCollection_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewProp_CachedCollection = { "CachedCollection", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionChildren, CachedCollection), Z_Construct_UScriptStruct_FRigElementKeyCollection, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewProp_CachedCollection_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewProp_CachedCollection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewProp_CachedHierarchyHash_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewProp_CachedHierarchyHash = { "CachedHierarchyHash", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionChildren, CachedHierarchyHash), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewProp_CachedHierarchyHash_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewProp_CachedHierarchyHash_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewProp_Parent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewProp_bIncludeParent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewProp_bRecursive,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewProp_TypeToSearch_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewProp_TypeToSearch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewProp_Collection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewProp_CachedCollection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::NewProp_CachedHierarchyHash,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_CollectionBase,
		&NewStructOps,
		"RigUnit_CollectionChildren",
		sizeof(FRigUnit_CollectionChildren),
		alignof(FRigUnit_CollectionChildren),
		Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_CollectionChildren()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_CollectionChildren"), sizeof(FRigUnit_CollectionChildren), Get_Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionChildren_Hash() { return 216885739U; }

void FRigUnit_CollectionChildren::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Parent,
		bIncludeParent,
		bRecursive,
		TypeToSearch,
		Collection,
		CachedCollection,
		CachedHierarchyHash,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_CollectionNameSearch>() == std::is_polymorphic<FRigUnit_CollectionBase>(), "USTRUCT FRigUnit_CollectionNameSearch cannot be polymorphic unless super FRigUnit_CollectionBase is polymorphic");

class UScriptStruct* FRigUnit_CollectionNameSearch::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_CollectionNameSearch"), sizeof(FRigUnit_CollectionNameSearch), Get_Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_CollectionNameSearch::Execute"), &FRigUnit_CollectionNameSearch::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_CollectionNameSearch>()
{
	return FRigUnit_CollectionNameSearch::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_CollectionNameSearch(FRigUnit_CollectionNameSearch::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_CollectionNameSearch"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionNameSearch
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionNameSearch()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_CollectionNameSearch>(FName(TEXT("RigUnit_CollectionNameSearch")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionNameSearch;
	struct Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PartialName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_PartialName;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TypeToSearch_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TypeToSearch_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TypeToSearch;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Collection_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Collection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedCollection_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CachedCollection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedHierarchyHash_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_CachedHierarchyHash;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Creates a collection based on a name search.\n * The name search is case sensitive.\n */" },
		{ "DisplayName", "Item Name Search" },
		{ "Keywords", "Bone,Joint,Collection,Filter" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
		{ "ToolTip", "Creates a collection based on a name search.\nThe name search is case sensitive." },
		{ "Varying", "" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_CollectionNameSearch>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Statics::NewProp_PartialName_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Statics::NewProp_PartialName = { "PartialName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionNameSearch, PartialName), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Statics::NewProp_PartialName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Statics::NewProp_PartialName_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Statics::NewProp_TypeToSearch_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Statics::NewProp_TypeToSearch_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Statics::NewProp_TypeToSearch = { "TypeToSearch", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionNameSearch, TypeToSearch), Z_Construct_UEnum_ControlRig_ERigElementType, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Statics::NewProp_TypeToSearch_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Statics::NewProp_TypeToSearch_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Statics::NewProp_Collection_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Statics::NewProp_Collection = { "Collection", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionNameSearch, Collection), Z_Construct_UScriptStruct_FRigElementKeyCollection, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Statics::NewProp_Collection_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Statics::NewProp_Collection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Statics::NewProp_CachedCollection_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Statics::NewProp_CachedCollection = { "CachedCollection", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionNameSearch, CachedCollection), Z_Construct_UScriptStruct_FRigElementKeyCollection, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Statics::NewProp_CachedCollection_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Statics::NewProp_CachedCollection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Statics::NewProp_CachedHierarchyHash_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Statics::NewProp_CachedHierarchyHash = { "CachedHierarchyHash", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionNameSearch, CachedHierarchyHash), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Statics::NewProp_CachedHierarchyHash_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Statics::NewProp_CachedHierarchyHash_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Statics::NewProp_PartialName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Statics::NewProp_TypeToSearch_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Statics::NewProp_TypeToSearch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Statics::NewProp_Collection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Statics::NewProp_CachedCollection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Statics::NewProp_CachedHierarchyHash,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_CollectionBase,
		&NewStructOps,
		"RigUnit_CollectionNameSearch",
		sizeof(FRigUnit_CollectionNameSearch),
		alignof(FRigUnit_CollectionNameSearch),
		Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_CollectionNameSearch"), sizeof(FRigUnit_CollectionNameSearch), Get_Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionNameSearch_Hash() { return 2187289868U; }

void FRigUnit_CollectionNameSearch::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		PartialName,
		TypeToSearch,
		Collection,
		CachedCollection,
		CachedHierarchyHash,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_CollectionChain>() == std::is_polymorphic<FRigUnit_CollectionBase>(), "USTRUCT FRigUnit_CollectionChain cannot be polymorphic unless super FRigUnit_CollectionBase is polymorphic");

class UScriptStruct* FRigUnit_CollectionChain::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_CollectionChain, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_CollectionChain"), sizeof(FRigUnit_CollectionChain), Get_Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_CollectionChain::Execute"), &FRigUnit_CollectionChain::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_CollectionChain>()
{
	return FRigUnit_CollectionChain::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_CollectionChain(FRigUnit_CollectionChain::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_CollectionChain"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionChain
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionChain()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_CollectionChain>(FName(TEXT("RigUnit_CollectionChain")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionChain;
	struct Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FirstItem_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FirstItem;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastItem_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LastItem;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Reverse_MetaData[];
#endif
		static void NewProp_Reverse_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Reverse;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Collection_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Collection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedCollection_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CachedCollection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedHierarchyHash_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_CachedHierarchyHash;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Creates a collection based on a first and last item within a chain.\n * Chains can refer to bone chains or chains within a control hierarchy.\n */" },
		{ "DisplayName", "Item Chain" },
		{ "Keywords", "Bone,Joint,Collection" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
		{ "ToolTip", "Creates a collection based on a first and last item within a chain.\nChains can refer to bone chains or chains within a control hierarchy." },
		{ "Varying", "" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_CollectionChain>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics::NewProp_FirstItem_MetaData[] = {
		{ "ExpandByDefault", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics::NewProp_FirstItem = { "FirstItem", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionChain, FirstItem), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics::NewProp_FirstItem_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics::NewProp_FirstItem_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics::NewProp_LastItem_MetaData[] = {
		{ "ExpandByDefault", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics::NewProp_LastItem = { "LastItem", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionChain, LastItem), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics::NewProp_LastItem_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics::NewProp_LastItem_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics::NewProp_Reverse_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics::NewProp_Reverse_SetBit(void* Obj)
	{
		((FRigUnit_CollectionChain*)Obj)->Reverse = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics::NewProp_Reverse = { "Reverse", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_CollectionChain), &Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics::NewProp_Reverse_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics::NewProp_Reverse_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics::NewProp_Reverse_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics::NewProp_Collection_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics::NewProp_Collection = { "Collection", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionChain, Collection), Z_Construct_UScriptStruct_FRigElementKeyCollection, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics::NewProp_Collection_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics::NewProp_Collection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics::NewProp_CachedCollection_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics::NewProp_CachedCollection = { "CachedCollection", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionChain, CachedCollection), Z_Construct_UScriptStruct_FRigElementKeyCollection, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics::NewProp_CachedCollection_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics::NewProp_CachedCollection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics::NewProp_CachedHierarchyHash_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics::NewProp_CachedHierarchyHash = { "CachedHierarchyHash", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_CollectionChain, CachedHierarchyHash), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics::NewProp_CachedHierarchyHash_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics::NewProp_CachedHierarchyHash_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics::NewProp_FirstItem,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics::NewProp_LastItem,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics::NewProp_Reverse,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics::NewProp_Collection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics::NewProp_CachedCollection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics::NewProp_CachedHierarchyHash,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_CollectionBase,
		&NewStructOps,
		"RigUnit_CollectionChain",
		sizeof(FRigUnit_CollectionChain),
		alignof(FRigUnit_CollectionChain),
		Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_CollectionChain()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_CollectionChain"), sizeof(FRigUnit_CollectionChain), Get_Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionChain_Hash() { return 23425905U; }

void FRigUnit_CollectionChain::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		FirstItem,
		LastItem,
		Reverse,
		Collection,
		CachedCollection,
		CachedHierarchyHash,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_CollectionBaseMutable>() == std::is_polymorphic<FRigUnitMutable>(), "USTRUCT FRigUnit_CollectionBaseMutable cannot be polymorphic unless super FRigUnitMutable is polymorphic");

class UScriptStruct* FRigUnit_CollectionBaseMutable::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionBaseMutable_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_CollectionBaseMutable, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_CollectionBaseMutable"), sizeof(FRigUnit_CollectionBaseMutable), Get_Z_Construct_UScriptStruct_FRigUnit_CollectionBaseMutable_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_CollectionBaseMutable>()
{
	return FRigUnit_CollectionBaseMutable::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_CollectionBaseMutable(FRigUnit_CollectionBaseMutable::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_CollectionBaseMutable"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionBaseMutable
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionBaseMutable()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_CollectionBaseMutable>(FName(TEXT("RigUnit_CollectionBaseMutable")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionBaseMutable;
	struct Z_Construct_UScriptStruct_FRigUnit_CollectionBaseMutable_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionBaseMutable_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "Category", "Collections" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
		{ "NodeColor", "0.7 0.05 0.5" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_CollectionBaseMutable_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_CollectionBaseMutable>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_CollectionBaseMutable_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnitMutable,
		&NewStructOps,
		"RigUnit_CollectionBaseMutable",
		sizeof(FRigUnit_CollectionBaseMutable),
		alignof(FRigUnit_CollectionBaseMutable),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionBaseMutable_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionBaseMutable_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_CollectionBaseMutable()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionBaseMutable_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_CollectionBaseMutable"), sizeof(FRigUnit_CollectionBaseMutable), Get_Z_Construct_UScriptStruct_FRigUnit_CollectionBaseMutable_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_CollectionBaseMutable_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionBaseMutable_Hash() { return 2841851596U; }

static_assert(std::is_polymorphic<FRigUnit_CollectionBase>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_CollectionBase cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_CollectionBase::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionBase_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_CollectionBase, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_CollectionBase"), sizeof(FRigUnit_CollectionBase), Get_Z_Construct_UScriptStruct_FRigUnit_CollectionBase_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_CollectionBase>()
{
	return FRigUnit_CollectionBase::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_CollectionBase(FRigUnit_CollectionBase::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_CollectionBase"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionBase
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionBase()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_CollectionBase>(FName(TEXT("RigUnit_CollectionBase")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_CollectionBase;
	struct Z_Construct_UScriptStruct_FRigUnit_CollectionBase_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_CollectionBase_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "Category", "Collections" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Collection.h" },
		{ "NodeColor", "0.7 0.05 0.5" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_CollectionBase_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_CollectionBase>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_CollectionBase_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_CollectionBase",
		sizeof(FRigUnit_CollectionBase),
		alignof(FRigUnit_CollectionBase),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_CollectionBase_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_CollectionBase_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_CollectionBase()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionBase_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_CollectionBase"), sizeof(FRigUnit_CollectionBase), Get_Z_Construct_UScriptStruct_FRigUnit_CollectionBase_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_CollectionBase_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_CollectionBase_Hash() { return 292159137U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
