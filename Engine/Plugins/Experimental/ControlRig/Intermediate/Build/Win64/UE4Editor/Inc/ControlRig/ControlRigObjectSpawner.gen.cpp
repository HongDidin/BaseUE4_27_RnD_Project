// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Public/Sequencer/ControlRigObjectSpawner.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeControlRigObjectSpawner() {}
// Cross Module References
	CONTROLRIG_API UClass* Z_Construct_UClass_UControlRigObjectHolder_NoRegister();
	CONTROLRIG_API UClass* Z_Construct_UClass_UControlRigObjectHolder();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
// End Cross Module References
	void UControlRigObjectHolder::StaticRegisterNativesUControlRigObjectHolder()
	{
	}
	UClass* Z_Construct_UClass_UControlRigObjectHolder_NoRegister()
	{
		return UControlRigObjectHolder::StaticClass();
	}
	struct Z_Construct_UClass_UControlRigObjectHolder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Objects_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Objects_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Objects;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UControlRigObjectHolder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigObjectHolder_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Sequencer/ControlRigObjectSpawner.h" },
		{ "ModuleRelativePath", "Public/Sequencer/ControlRigObjectSpawner.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UControlRigObjectHolder_Statics::NewProp_Objects_Inner = { "Objects", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigObjectHolder_Statics::NewProp_Objects_MetaData[] = {
		{ "ModuleRelativePath", "Public/Sequencer/ControlRigObjectSpawner.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UControlRigObjectHolder_Statics::NewProp_Objects = { "Objects", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigObjectHolder, Objects), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UControlRigObjectHolder_Statics::NewProp_Objects_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigObjectHolder_Statics::NewProp_Objects_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UControlRigObjectHolder_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigObjectHolder_Statics::NewProp_Objects_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigObjectHolder_Statics::NewProp_Objects,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UControlRigObjectHolder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UControlRigObjectHolder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UControlRigObjectHolder_Statics::ClassParams = {
		&UControlRigObjectHolder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UControlRigObjectHolder_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigObjectHolder_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UControlRigObjectHolder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigObjectHolder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UControlRigObjectHolder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UControlRigObjectHolder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UControlRigObjectHolder, 781881305);
	template<> CONTROLRIG_API UClass* StaticClass<UControlRigObjectHolder>()
	{
		return UControlRigObjectHolder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UControlRigObjectHolder(Z_Construct_UClass_UControlRigObjectHolder, &UControlRigObjectHolder::StaticClass, TEXT("/Script/ControlRig"), TEXT("UControlRigObjectHolder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UControlRigObjectHolder);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
