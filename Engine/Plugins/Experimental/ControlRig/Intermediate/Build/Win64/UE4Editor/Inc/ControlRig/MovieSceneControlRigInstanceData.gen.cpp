// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Public/Sequencer/MovieSceneControlRigInstanceData.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMovieSceneControlRigInstanceData() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	MOVIESCENE_API UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneSequenceInstanceData();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FInputBlendPose();
	MOVIESCENE_API UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneFloatChannel();
	MOVIESCENE_API UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneEvaluationOperand();
// End Cross Module References

static_assert(std::is_polymorphic<FMovieSceneControlRigInstanceData>() == std::is_polymorphic<FMovieSceneSequenceInstanceData>(), "USTRUCT FMovieSceneControlRigInstanceData cannot be polymorphic unless super FMovieSceneSequenceInstanceData is polymorphic");

class UScriptStruct* FMovieSceneControlRigInstanceData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData, Z_Construct_UPackage__Script_ControlRig(), TEXT("MovieSceneControlRigInstanceData"), sizeof(FMovieSceneControlRigInstanceData), Get_Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FMovieSceneControlRigInstanceData>()
{
	return FMovieSceneControlRigInstanceData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMovieSceneControlRigInstanceData(FMovieSceneControlRigInstanceData::StaticStruct, TEXT("/Script/ControlRig"), TEXT("MovieSceneControlRigInstanceData"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFMovieSceneControlRigInstanceData
{
	FScriptStruct_ControlRig_StaticRegisterNativesFMovieSceneControlRigInstanceData()
	{
		UScriptStruct::DeferCppStructOps<FMovieSceneControlRigInstanceData>(FName(TEXT("MovieSceneControlRigInstanceData")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFMovieSceneControlRigInstanceData;
	struct Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAdditive_MetaData[];
#endif
		static void NewProp_bAdditive_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAdditive;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bApplyBoneFilter_MetaData[];
#endif
		static void NewProp_bApplyBoneFilter_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bApplyBoneFilter;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoneFilter_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BoneFilter;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Weight_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Weight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Operand_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Operand;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneControlRigInstanceData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMovieSceneControlRigInstanceData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Statics::NewProp_bAdditive_MetaData[] = {
		{ "Comment", "/** Blend this track in additively (using the reference pose as a base) */" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneControlRigInstanceData.h" },
		{ "ToolTip", "Blend this track in additively (using the reference pose as a base)" },
	};
#endif
	void Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Statics::NewProp_bAdditive_SetBit(void* Obj)
	{
		((FMovieSceneControlRigInstanceData*)Obj)->bAdditive = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Statics::NewProp_bAdditive = { "bAdditive", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FMovieSceneControlRigInstanceData), &Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Statics::NewProp_bAdditive_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Statics::NewProp_bAdditive_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Statics::NewProp_bAdditive_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Statics::NewProp_bApplyBoneFilter_MetaData[] = {
		{ "Comment", "/** Only apply bones that are in the filter */" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneControlRigInstanceData.h" },
		{ "ToolTip", "Only apply bones that are in the filter" },
	};
#endif
	void Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Statics::NewProp_bApplyBoneFilter_SetBit(void* Obj)
	{
		((FMovieSceneControlRigInstanceData*)Obj)->bApplyBoneFilter = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Statics::NewProp_bApplyBoneFilter = { "bApplyBoneFilter", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FMovieSceneControlRigInstanceData), &Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Statics::NewProp_bApplyBoneFilter_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Statics::NewProp_bApplyBoneFilter_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Statics::NewProp_bApplyBoneFilter_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Statics::NewProp_BoneFilter_MetaData[] = {
		{ "Comment", "/** Per-bone filter to apply to our animation */" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneControlRigInstanceData.h" },
		{ "ToolTip", "Per-bone filter to apply to our animation" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Statics::NewProp_BoneFilter = { "BoneFilter", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMovieSceneControlRigInstanceData, BoneFilter), Z_Construct_UScriptStruct_FInputBlendPose, METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Statics::NewProp_BoneFilter_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Statics::NewProp_BoneFilter_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Statics::NewProp_Weight_MetaData[] = {
		{ "Comment", "/** The weight curve for this animation controller section */" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneControlRigInstanceData.h" },
		{ "ToolTip", "The weight curve for this animation controller section" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Statics::NewProp_Weight = { "Weight", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMovieSceneControlRigInstanceData, Weight), Z_Construct_UScriptStruct_FMovieSceneFloatChannel, METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Statics::NewProp_Weight_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Statics::NewProp_Weight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Statics::NewProp_Operand_MetaData[] = {
		{ "Comment", "/** The operand the control rig instance should operate on */" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneControlRigInstanceData.h" },
		{ "ToolTip", "The operand the control rig instance should operate on" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Statics::NewProp_Operand = { "Operand", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMovieSceneControlRigInstanceData, Operand), Z_Construct_UScriptStruct_FMovieSceneEvaluationOperand, METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Statics::NewProp_Operand_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Statics::NewProp_Operand_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Statics::NewProp_bAdditive,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Statics::NewProp_bApplyBoneFilter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Statics::NewProp_BoneFilter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Statics::NewProp_Weight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Statics::NewProp_Operand,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FMovieSceneSequenceInstanceData,
		&NewStructOps,
		"MovieSceneControlRigInstanceData",
		sizeof(FMovieSceneControlRigInstanceData),
		alignof(FMovieSceneControlRigInstanceData),
		Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MovieSceneControlRigInstanceData"), sizeof(FMovieSceneControlRigInstanceData), Get_Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMovieSceneControlRigInstanceData_Hash() { return 2495691522U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
