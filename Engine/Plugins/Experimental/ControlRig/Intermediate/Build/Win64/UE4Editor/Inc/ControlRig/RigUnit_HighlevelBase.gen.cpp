// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Public/Units/Highlevel/RigUnit_HighlevelBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_HighlevelBase() {}
// Cross Module References
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_EControlRigVectorKind();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_HighlevelBaseMutable();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnitMutable();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_HighlevelBase();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit();
// End Cross Module References
	static UEnum* EControlRigVectorKind_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ControlRig_EControlRigVectorKind, Z_Construct_UPackage__Script_ControlRig(), TEXT("EControlRigVectorKind"));
		}
		return Singleton;
	}
	template<> CONTROLRIG_API UEnum* StaticEnum<EControlRigVectorKind>()
	{
		return EControlRigVectorKind_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EControlRigVectorKind(EControlRigVectorKind_StaticEnum, TEXT("/Script/ControlRig"), TEXT("EControlRigVectorKind"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ControlRig_EControlRigVectorKind_Hash() { return 2549501569U; }
	UEnum* Z_Construct_UEnum_ControlRig_EControlRigVectorKind()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EControlRigVectorKind"), 0, Get_Z_Construct_UEnum_ControlRig_EControlRigVectorKind_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EControlRigVectorKind::Direction", (int64)EControlRigVectorKind::Direction },
				{ "EControlRigVectorKind::Location", (int64)EControlRigVectorKind::Location },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Direction.Name", "EControlRigVectorKind::Direction" },
				{ "Location.Name", "EControlRigVectorKind::Location" },
				{ "ModuleRelativePath", "Public/Units/Highlevel/RigUnit_HighlevelBase.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ControlRig,
				nullptr,
				"EControlRigVectorKind",
				"EControlRigVectorKind",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}

static_assert(std::is_polymorphic<FRigUnit_HighlevelBaseMutable>() == std::is_polymorphic<FRigUnitMutable>(), "USTRUCT FRigUnit_HighlevelBaseMutable cannot be polymorphic unless super FRigUnitMutable is polymorphic");

class UScriptStruct* FRigUnit_HighlevelBaseMutable::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_HighlevelBaseMutable_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_HighlevelBaseMutable, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_HighlevelBaseMutable"), sizeof(FRigUnit_HighlevelBaseMutable), Get_Z_Construct_UScriptStruct_FRigUnit_HighlevelBaseMutable_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_HighlevelBaseMutable>()
{
	return FRigUnit_HighlevelBaseMutable::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_HighlevelBaseMutable(FRigUnit_HighlevelBaseMutable::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_HighlevelBaseMutable"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_HighlevelBaseMutable
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_HighlevelBaseMutable()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_HighlevelBaseMutable>(FName(TEXT("RigUnit_HighlevelBaseMutable")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_HighlevelBaseMutable;
	struct Z_Construct_UScriptStruct_FRigUnit_HighlevelBaseMutable_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_HighlevelBaseMutable_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "Category", "Debug" },
		{ "ModuleRelativePath", "Public/Units/Highlevel/RigUnit_HighlevelBase.h" },
		{ "NodeColor", "0.4 0.05 0.4" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_HighlevelBaseMutable_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_HighlevelBaseMutable>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_HighlevelBaseMutable_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnitMutable,
		&NewStructOps,
		"RigUnit_HighlevelBaseMutable",
		sizeof(FRigUnit_HighlevelBaseMutable),
		alignof(FRigUnit_HighlevelBaseMutable),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_HighlevelBaseMutable_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_HighlevelBaseMutable_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_HighlevelBaseMutable()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_HighlevelBaseMutable_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_HighlevelBaseMutable"), sizeof(FRigUnit_HighlevelBaseMutable), Get_Z_Construct_UScriptStruct_FRigUnit_HighlevelBaseMutable_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_HighlevelBaseMutable_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_HighlevelBaseMutable_Hash() { return 4060797269U; }

static_assert(std::is_polymorphic<FRigUnit_HighlevelBase>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_HighlevelBase cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_HighlevelBase::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_HighlevelBase_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_HighlevelBase, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_HighlevelBase"), sizeof(FRigUnit_HighlevelBase), Get_Z_Construct_UScriptStruct_FRigUnit_HighlevelBase_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_HighlevelBase>()
{
	return FRigUnit_HighlevelBase::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_HighlevelBase(FRigUnit_HighlevelBase::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_HighlevelBase"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_HighlevelBase
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_HighlevelBase()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_HighlevelBase>(FName(TEXT("RigUnit_HighlevelBase")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_HighlevelBase;
	struct Z_Construct_UScriptStruct_FRigUnit_HighlevelBase_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_HighlevelBase_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "Category", "Debug" },
		{ "ModuleRelativePath", "Public/Units/Highlevel/RigUnit_HighlevelBase.h" },
		{ "NodeColor", "0.4 0.05 0.4" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_HighlevelBase_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_HighlevelBase>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_HighlevelBase_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_HighlevelBase",
		sizeof(FRigUnit_HighlevelBase),
		alignof(FRigUnit_HighlevelBase),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_HighlevelBase_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_HighlevelBase_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_HighlevelBase()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_HighlevelBase_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_HighlevelBase"), sizeof(FRigUnit_HighlevelBase), Get_Z_Construct_UScriptStruct_FRigUnit_HighlevelBase_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_HighlevelBase_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_HighlevelBase_Hash() { return 1474103701U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
