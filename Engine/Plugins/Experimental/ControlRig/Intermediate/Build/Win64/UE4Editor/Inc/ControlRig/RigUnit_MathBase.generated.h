// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigUnit_MathBase_generated_h
#error "RigUnit_MathBase.generated.h already included, missing '#pragma once' in RigUnit_MathBase.h"
#endif
#define CONTROLRIG_RigUnit_MathBase_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_MathBase_h_11_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_MathBase_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	typedef FRigUnit Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_MathBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Math_RigUnit_MathBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
