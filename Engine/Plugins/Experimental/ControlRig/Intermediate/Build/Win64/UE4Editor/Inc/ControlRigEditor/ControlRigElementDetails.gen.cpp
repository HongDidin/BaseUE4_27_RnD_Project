// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRigEditor/Private/ControlRigElementDetails.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeControlRigElementDetails() {}
// Cross Module References
	CONTROLRIGEDITOR_API UEnum* Z_Construct_UEnum_ControlRigEditor_ERigElementDetailsTransformComponent();
	UPackage* Z_Construct_UPackage__Script_ControlRigEditor();
// End Cross Module References
	static UEnum* ERigElementDetailsTransformComponent_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ControlRigEditor_ERigElementDetailsTransformComponent, Z_Construct_UPackage__Script_ControlRigEditor(), TEXT("ERigElementDetailsTransformComponent"));
		}
		return Singleton;
	}
	template<> CONTROLRIGEDITOR_API UEnum* StaticEnum<ERigElementDetailsTransformComponent>()
	{
		return ERigElementDetailsTransformComponent_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ERigElementDetailsTransformComponent(ERigElementDetailsTransformComponent_StaticEnum, TEXT("/Script/ControlRigEditor"), TEXT("ERigElementDetailsTransformComponent"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ControlRigEditor_ERigElementDetailsTransformComponent_Hash() { return 3347697752U; }
	UEnum* Z_Construct_UEnum_ControlRigEditor_ERigElementDetailsTransformComponent()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRigEditor();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ERigElementDetailsTransformComponent"), 0, Get_Z_Construct_UEnum_ControlRigEditor_ERigElementDetailsTransformComponent_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ERigElementDetailsTransformComponent::TranslationX", (int64)ERigElementDetailsTransformComponent::TranslationX },
				{ "ERigElementDetailsTransformComponent::TranslationY", (int64)ERigElementDetailsTransformComponent::TranslationY },
				{ "ERigElementDetailsTransformComponent::TranslationZ", (int64)ERigElementDetailsTransformComponent::TranslationZ },
				{ "ERigElementDetailsTransformComponent::RotationRoll", (int64)ERigElementDetailsTransformComponent::RotationRoll },
				{ "ERigElementDetailsTransformComponent::RotationPitch", (int64)ERigElementDetailsTransformComponent::RotationPitch },
				{ "ERigElementDetailsTransformComponent::RotationYaw", (int64)ERigElementDetailsTransformComponent::RotationYaw },
				{ "ERigElementDetailsTransformComponent::ScaleX", (int64)ERigElementDetailsTransformComponent::ScaleX },
				{ "ERigElementDetailsTransformComponent::ScaleY", (int64)ERigElementDetailsTransformComponent::ScaleY },
				{ "ERigElementDetailsTransformComponent::ScaleZ", (int64)ERigElementDetailsTransformComponent::ScaleZ },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ModuleRelativePath", "Private/ControlRigElementDetails.h" },
				{ "RotationPitch.Name", "ERigElementDetailsTransformComponent::RotationPitch" },
				{ "RotationRoll.Name", "ERigElementDetailsTransformComponent::RotationRoll" },
				{ "RotationYaw.Name", "ERigElementDetailsTransformComponent::RotationYaw" },
				{ "ScaleX.Name", "ERigElementDetailsTransformComponent::ScaleX" },
				{ "ScaleY.Name", "ERigElementDetailsTransformComponent::ScaleY" },
				{ "ScaleZ.Name", "ERigElementDetailsTransformComponent::ScaleZ" },
				{ "TranslationX.Name", "ERigElementDetailsTransformComponent::TranslationX" },
				{ "TranslationY.Name", "ERigElementDetailsTransformComponent::TranslationY" },
				{ "TranslationZ.Name", "ERigElementDetailsTransformComponent::TranslationZ" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ControlRigEditor,
				nullptr,
				"ERigElementDetailsTransformComponent",
				"ERigElementDetailsTransformComponent",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
