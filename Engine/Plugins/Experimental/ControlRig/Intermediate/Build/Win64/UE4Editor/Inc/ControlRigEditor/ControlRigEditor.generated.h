// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIGEDITOR_ControlRigEditor_generated_h
#error "ControlRigEditor.generated.h already included, missing '#pragma once' in ControlRigEditor.h"
#endif
#define CONTROLRIGEDITOR_ControlRigEditor_generated_h

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Editor_ControlRigEditor_h


#define FOREACH_ENUM_ECONTROLRIGEDITOREVENTQUEUE(op) \
	op(EControlRigEditorEventQueue::Setup) \
	op(EControlRigEditorEventQueue::Update) \
	op(EControlRigEditorEventQueue::Inverse) \
	op(EControlRigEditorEventQueue::InverseAndUpdate) \
	op(EControlRigEditorEventQueue::Max) 

enum class EControlRigEditorEventQueue : uint8;
template<> CONTROLRIGEDITOR_API UEnum* StaticEnum<EControlRigEditorEventQueue>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
