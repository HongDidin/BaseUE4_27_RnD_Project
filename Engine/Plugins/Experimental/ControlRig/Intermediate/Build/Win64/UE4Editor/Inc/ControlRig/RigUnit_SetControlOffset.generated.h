// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigUnit_SetControlOffset_generated_h
#error "RigUnit_SetControlOffset.generated.h already included, missing '#pragma once' in RigUnit_SetControlOffset.h"
#endif
#define CONTROLRIG_RigUnit_SetControlOffset_generated_h


#define FRigUnit_SetControlOffset_Execute() \
	void FRigUnit_SetControlOffset::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Control, \
		FTransform& Offset, \
		const EBoneGetterSetterMode Space, \
		FCachedRigElement& CachedControlIndex, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Hierarchy_RigUnit_SetControlOffset_h_14_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_SetControlOffset_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Control, \
		FTransform& Offset, \
		const EBoneGetterSetterMode Space, \
		FCachedRigElement& CachedControlIndex, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FName& Control = *(FName*)RigVMMemoryHandles[0].GetData(); \
		FTransform& Offset = *(FTransform*)RigVMMemoryHandles[1].GetData(); \
		EBoneGetterSetterMode Space = (EBoneGetterSetterMode)*(uint8*)RigVMMemoryHandles[2].GetData(); \
		FRigVMDynamicArray<FCachedRigElement> CachedControlIndex_3_Array(*((FRigVMByteArray*)RigVMMemoryHandles[3].GetData(0, false))); \
		CachedControlIndex_3_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedControlIndex = CachedControlIndex_3_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[4].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Control, \
			Offset, \
			Space, \
			CachedControlIndex, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnitMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_SetControlOffset>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Hierarchy_RigUnit_SetControlOffset_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
