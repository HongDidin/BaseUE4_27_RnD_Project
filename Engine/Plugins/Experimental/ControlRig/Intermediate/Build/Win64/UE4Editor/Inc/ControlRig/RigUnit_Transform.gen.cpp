// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Deprecated/Math/RigUnit_Transform.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_Transform() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_GetRelativeTransform();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_BinaryTransformOp();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MultiplyTransform();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_GetRelativeTransform>() == std::is_polymorphic<FRigUnit_BinaryTransformOp>(), "USTRUCT FRigUnit_GetRelativeTransform cannot be polymorphic unless super FRigUnit_BinaryTransformOp is polymorphic");

class UScriptStruct* FRigUnit_GetRelativeTransform::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_GetRelativeTransform_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_GetRelativeTransform, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_GetRelativeTransform"), sizeof(FRigUnit_GetRelativeTransform), Get_Z_Construct_UScriptStruct_FRigUnit_GetRelativeTransform_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_GetRelativeTransform::Execute"), &FRigUnit_GetRelativeTransform::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_GetRelativeTransform>()
{
	return FRigUnit_GetRelativeTransform::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_GetRelativeTransform(FRigUnit_GetRelativeTransform::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_GetRelativeTransform"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_GetRelativeTransform
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_GetRelativeTransform()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_GetRelativeTransform>(FName(TEXT("RigUnit_GetRelativeTransform")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_GetRelativeTransform;
	struct Z_Construct_UScriptStruct_FRigUnit_GetRelativeTransform_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_GetRelativeTransform_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Math|Transform" },
		{ "Deprecated", "4.23.0" },
		{ "DisplayName", "GetRelativeTransform" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Transform.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_GetRelativeTransform_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_GetRelativeTransform>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_GetRelativeTransform_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_BinaryTransformOp,
		&NewStructOps,
		"RigUnit_GetRelativeTransform",
		sizeof(FRigUnit_GetRelativeTransform),
		alignof(FRigUnit_GetRelativeTransform),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_GetRelativeTransform_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_GetRelativeTransform_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_GetRelativeTransform()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_GetRelativeTransform_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_GetRelativeTransform"), sizeof(FRigUnit_GetRelativeTransform), Get_Z_Construct_UScriptStruct_FRigUnit_GetRelativeTransform_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_GetRelativeTransform_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_GetRelativeTransform_Hash() { return 1122455067U; }

void FRigUnit_GetRelativeTransform::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Argument0,
		Argument1,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MultiplyTransform>() == std::is_polymorphic<FRigUnit_BinaryTransformOp>(), "USTRUCT FRigUnit_MultiplyTransform cannot be polymorphic unless super FRigUnit_BinaryTransformOp is polymorphic");

class UScriptStruct* FRigUnit_MultiplyTransform::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MultiplyTransform_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MultiplyTransform, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MultiplyTransform"), sizeof(FRigUnit_MultiplyTransform), Get_Z_Construct_UScriptStruct_FRigUnit_MultiplyTransform_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MultiplyTransform::Execute"), &FRigUnit_MultiplyTransform::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MultiplyTransform>()
{
	return FRigUnit_MultiplyTransform::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MultiplyTransform(FRigUnit_MultiplyTransform::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MultiplyTransform"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MultiplyTransform
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MultiplyTransform()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MultiplyTransform>(FName(TEXT("RigUnit_MultiplyTransform")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MultiplyTransform;
	struct Z_Construct_UScriptStruct_FRigUnit_MultiplyTransform_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MultiplyTransform_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Math|Transform" },
		{ "Deprecated", "4.23.0" },
		{ "DisplayName", "Multiply(Transform)" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Transform.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MultiplyTransform_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MultiplyTransform>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MultiplyTransform_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_BinaryTransformOp,
		&NewStructOps,
		"RigUnit_MultiplyTransform",
		sizeof(FRigUnit_MultiplyTransform),
		alignof(FRigUnit_MultiplyTransform),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MultiplyTransform_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MultiplyTransform_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MultiplyTransform()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MultiplyTransform_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MultiplyTransform"), sizeof(FRigUnit_MultiplyTransform), Get_Z_Construct_UScriptStruct_FRigUnit_MultiplyTransform_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MultiplyTransform_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MultiplyTransform_Hash() { return 1683489516U; }

void FRigUnit_MultiplyTransform::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Argument0,
		Argument1,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_BinaryTransformOp>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_BinaryTransformOp cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_BinaryTransformOp::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_BinaryTransformOp_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_BinaryTransformOp, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_BinaryTransformOp"), sizeof(FRigUnit_BinaryTransformOp), Get_Z_Construct_UScriptStruct_FRigUnit_BinaryTransformOp_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_BinaryTransformOp>()
{
	return FRigUnit_BinaryTransformOp::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_BinaryTransformOp(FRigUnit_BinaryTransformOp::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_BinaryTransformOp"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_BinaryTransformOp
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_BinaryTransformOp()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_BinaryTransformOp>(FName(TEXT("RigUnit_BinaryTransformOp")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_BinaryTransformOp;
	struct Z_Construct_UScriptStruct_FRigUnit_BinaryTransformOp_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Argument0_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Argument0;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Argument1_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Argument1;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_BinaryTransformOp_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "Comment", "/** Two args and a result of Transform type */" },
		{ "Deprecated", "4.23.0" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Transform.h" },
		{ "NodeColor", "0.1 0.7 0.1" },
		{ "ToolTip", "Two args and a result of Transform type" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_BinaryTransformOp_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_BinaryTransformOp>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_BinaryTransformOp_Statics::NewProp_Argument0_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Transform.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_BinaryTransformOp_Statics::NewProp_Argument0 = { "Argument0", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_BinaryTransformOp, Argument0), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_BinaryTransformOp_Statics::NewProp_Argument0_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_BinaryTransformOp_Statics::NewProp_Argument0_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_BinaryTransformOp_Statics::NewProp_Argument1_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Transform.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_BinaryTransformOp_Statics::NewProp_Argument1 = { "Argument1", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_BinaryTransformOp, Argument1), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_BinaryTransformOp_Statics::NewProp_Argument1_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_BinaryTransformOp_Statics::NewProp_Argument1_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_BinaryTransformOp_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Transform.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_BinaryTransformOp_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_BinaryTransformOp, Result), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_BinaryTransformOp_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_BinaryTransformOp_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_BinaryTransformOp_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_BinaryTransformOp_Statics::NewProp_Argument0,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_BinaryTransformOp_Statics::NewProp_Argument1,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_BinaryTransformOp_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_BinaryTransformOp_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_BinaryTransformOp",
		sizeof(FRigUnit_BinaryTransformOp),
		alignof(FRigUnit_BinaryTransformOp),
		Z_Construct_UScriptStruct_FRigUnit_BinaryTransformOp_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_BinaryTransformOp_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_BinaryTransformOp_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_BinaryTransformOp_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_BinaryTransformOp()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_BinaryTransformOp_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_BinaryTransformOp"), sizeof(FRigUnit_BinaryTransformOp), Get_Z_Construct_UScriptStruct_FRigUnit_BinaryTransformOp_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_BinaryTransformOp_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_BinaryTransformOp_Hash() { return 3072924118U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
