// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Math/RigUnit_MathBool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_MathBool() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathBoolNotEquals();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathBoolBase();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathBoolEquals();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathBoolOr();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathBoolNand();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathBoolAnd();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathBoolNot();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathBoolUnaryOp();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathBoolConstFalse();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathBoolConstant();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathBoolConstTrue();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathBase();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_MathBoolNotEquals>() == std::is_polymorphic<FRigUnit_MathBoolBase>(), "USTRUCT FRigUnit_MathBoolNotEquals cannot be polymorphic unless super FRigUnit_MathBoolBase is polymorphic");

class UScriptStruct* FRigUnit_MathBoolNotEquals::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolNotEquals_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathBoolNotEquals, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathBoolNotEquals"), sizeof(FRigUnit_MathBoolNotEquals), Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolNotEquals_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathBoolNotEquals::Execute"), &FRigUnit_MathBoolNotEquals::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathBoolNotEquals>()
{
	return FRigUnit_MathBoolNotEquals::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathBoolNotEquals(FRigUnit_MathBoolNotEquals::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathBoolNotEquals"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathBoolNotEquals
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathBoolNotEquals()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathBoolNotEquals>(FName(TEXT("RigUnit_MathBoolNotEquals")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathBoolNotEquals;
	struct Z_Construct_UScriptStruct_FRigUnit_MathBoolNotEquals_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_A_MetaData[];
#endif
		static void NewProp_A_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_A;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_B_MetaData[];
#endif
		static void NewProp_B_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_B;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static void NewProp_Result_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathBoolNotEquals_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns true if the value A does not equal B\n */" },
		{ "DisplayName", "Not Equals" },
		{ "Keywords", "Different,!=,Xor" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathBool.h" },
		{ "PrototypeName", "NotEquals" },
		{ "ToolTip", "Returns true if the value A does not equal B" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathBoolNotEquals_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathBoolNotEquals>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathBoolNotEquals_Statics::NewProp_A_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathBool.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_MathBoolNotEquals_Statics::NewProp_A_SetBit(void* Obj)
	{
		((FRigUnit_MathBoolNotEquals*)Obj)->A = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathBoolNotEquals_Statics::NewProp_A = { "A", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_MathBoolNotEquals), &Z_Construct_UScriptStruct_FRigUnit_MathBoolNotEquals_Statics::NewProp_A_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathBoolNotEquals_Statics::NewProp_A_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathBoolNotEquals_Statics::NewProp_A_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathBoolNotEquals_Statics::NewProp_B_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathBool.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_MathBoolNotEquals_Statics::NewProp_B_SetBit(void* Obj)
	{
		((FRigUnit_MathBoolNotEquals*)Obj)->B = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathBoolNotEquals_Statics::NewProp_B = { "B", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_MathBoolNotEquals), &Z_Construct_UScriptStruct_FRigUnit_MathBoolNotEquals_Statics::NewProp_B_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathBoolNotEquals_Statics::NewProp_B_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathBoolNotEquals_Statics::NewProp_B_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathBoolNotEquals_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathBool.h" },
		{ "Output", "" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_MathBoolNotEquals_Statics::NewProp_Result_SetBit(void* Obj)
	{
		((FRigUnit_MathBoolNotEquals*)Obj)->Result = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathBoolNotEquals_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_MathBoolNotEquals), &Z_Construct_UScriptStruct_FRigUnit_MathBoolNotEquals_Statics::NewProp_Result_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathBoolNotEquals_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathBoolNotEquals_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathBoolNotEquals_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathBoolNotEquals_Statics::NewProp_A,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathBoolNotEquals_Statics::NewProp_B,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathBoolNotEquals_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathBoolNotEquals_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathBoolBase,
		&NewStructOps,
		"RigUnit_MathBoolNotEquals",
		sizeof(FRigUnit_MathBoolNotEquals),
		alignof(FRigUnit_MathBoolNotEquals),
		Z_Construct_UScriptStruct_FRigUnit_MathBoolNotEquals_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathBoolNotEquals_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathBoolNotEquals_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathBoolNotEquals_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathBoolNotEquals()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolNotEquals_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathBoolNotEquals"), sizeof(FRigUnit_MathBoolNotEquals), Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolNotEquals_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathBoolNotEquals_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolNotEquals_Hash() { return 3430951292U; }

void FRigUnit_MathBoolNotEquals::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathBoolEquals>() == std::is_polymorphic<FRigUnit_MathBoolBase>(), "USTRUCT FRigUnit_MathBoolEquals cannot be polymorphic unless super FRigUnit_MathBoolBase is polymorphic");

class UScriptStruct* FRigUnit_MathBoolEquals::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolEquals_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathBoolEquals, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathBoolEquals"), sizeof(FRigUnit_MathBoolEquals), Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolEquals_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathBoolEquals::Execute"), &FRigUnit_MathBoolEquals::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathBoolEquals>()
{
	return FRigUnit_MathBoolEquals::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathBoolEquals(FRigUnit_MathBoolEquals::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathBoolEquals"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathBoolEquals
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathBoolEquals()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathBoolEquals>(FName(TEXT("RigUnit_MathBoolEquals")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathBoolEquals;
	struct Z_Construct_UScriptStruct_FRigUnit_MathBoolEquals_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_A_MetaData[];
#endif
		static void NewProp_A_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_A;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_B_MetaData[];
#endif
		static void NewProp_B_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_B;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static void NewProp_Result_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathBoolEquals_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns true if the value A equals B\n */" },
		{ "DisplayName", "Equals" },
		{ "Keywords", "Same,==" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathBool.h" },
		{ "PrototypeName", "Equals" },
		{ "ToolTip", "Returns true if the value A equals B" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathBoolEquals_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathBoolEquals>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathBoolEquals_Statics::NewProp_A_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathBool.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_MathBoolEquals_Statics::NewProp_A_SetBit(void* Obj)
	{
		((FRigUnit_MathBoolEquals*)Obj)->A = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathBoolEquals_Statics::NewProp_A = { "A", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_MathBoolEquals), &Z_Construct_UScriptStruct_FRigUnit_MathBoolEquals_Statics::NewProp_A_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathBoolEquals_Statics::NewProp_A_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathBoolEquals_Statics::NewProp_A_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathBoolEquals_Statics::NewProp_B_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathBool.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_MathBoolEquals_Statics::NewProp_B_SetBit(void* Obj)
	{
		((FRigUnit_MathBoolEquals*)Obj)->B = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathBoolEquals_Statics::NewProp_B = { "B", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_MathBoolEquals), &Z_Construct_UScriptStruct_FRigUnit_MathBoolEquals_Statics::NewProp_B_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathBoolEquals_Statics::NewProp_B_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathBoolEquals_Statics::NewProp_B_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathBoolEquals_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathBool.h" },
		{ "Output", "" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_MathBoolEquals_Statics::NewProp_Result_SetBit(void* Obj)
	{
		((FRigUnit_MathBoolEquals*)Obj)->Result = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathBoolEquals_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_MathBoolEquals), &Z_Construct_UScriptStruct_FRigUnit_MathBoolEquals_Statics::NewProp_Result_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathBoolEquals_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathBoolEquals_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathBoolEquals_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathBoolEquals_Statics::NewProp_A,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathBoolEquals_Statics::NewProp_B,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathBoolEquals_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathBoolEquals_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathBoolBase,
		&NewStructOps,
		"RigUnit_MathBoolEquals",
		sizeof(FRigUnit_MathBoolEquals),
		alignof(FRigUnit_MathBoolEquals),
		Z_Construct_UScriptStruct_FRigUnit_MathBoolEquals_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathBoolEquals_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathBoolEquals_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathBoolEquals_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathBoolEquals()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolEquals_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathBoolEquals"), sizeof(FRigUnit_MathBoolEquals), Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolEquals_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathBoolEquals_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolEquals_Hash() { return 2047523786U; }

void FRigUnit_MathBoolEquals::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathBoolOr>() == std::is_polymorphic<FRigUnit_MathBoolBinaryOp>(), "USTRUCT FRigUnit_MathBoolOr cannot be polymorphic unless super FRigUnit_MathBoolBinaryOp is polymorphic");

class UScriptStruct* FRigUnit_MathBoolOr::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolOr_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathBoolOr, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathBoolOr"), sizeof(FRigUnit_MathBoolOr), Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolOr_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathBoolOr::Execute"), &FRigUnit_MathBoolOr::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathBoolOr>()
{
	return FRigUnit_MathBoolOr::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathBoolOr(FRigUnit_MathBoolOr::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathBoolOr"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathBoolOr
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathBoolOr()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathBoolOr>(FName(TEXT("RigUnit_MathBoolOr")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathBoolOr;
	struct Z_Construct_UScriptStruct_FRigUnit_MathBoolOr_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathBoolOr_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns true if one of the conditions is true\n */" },
		{ "DisplayName", "Or" },
		{ "Keywords", "||" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathBool.h" },
		{ "PrototypeName", "Or" },
		{ "ToolTip", "Returns true if one of the conditions is true" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathBoolOr_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathBoolOr>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathBoolOr_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp,
		&NewStructOps,
		"RigUnit_MathBoolOr",
		sizeof(FRigUnit_MathBoolOr),
		alignof(FRigUnit_MathBoolOr),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathBoolOr_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathBoolOr_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathBoolOr()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolOr_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathBoolOr"), sizeof(FRigUnit_MathBoolOr), Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolOr_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathBoolOr_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolOr_Hash() { return 2878081910U; }

void FRigUnit_MathBoolOr::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathBoolNand>() == std::is_polymorphic<FRigUnit_MathBoolBinaryOp>(), "USTRUCT FRigUnit_MathBoolNand cannot be polymorphic unless super FRigUnit_MathBoolBinaryOp is polymorphic");

class UScriptStruct* FRigUnit_MathBoolNand::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolNand_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathBoolNand, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathBoolNand"), sizeof(FRigUnit_MathBoolNand), Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolNand_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathBoolNand::Execute"), &FRigUnit_MathBoolNand::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathBoolNand>()
{
	return FRigUnit_MathBoolNand::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathBoolNand(FRigUnit_MathBoolNand::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathBoolNand"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathBoolNand
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathBoolNand()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathBoolNand>(FName(TEXT("RigUnit_MathBoolNand")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathBoolNand;
	struct Z_Construct_UScriptStruct_FRigUnit_MathBoolNand_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathBoolNand_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns true if both conditions are false\n */" },
		{ "DisplayName", "Nand" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathBool.h" },
		{ "PrototypeName", "Nand" },
		{ "ToolTip", "Returns true if both conditions are false" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathBoolNand_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathBoolNand>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathBoolNand_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp,
		&NewStructOps,
		"RigUnit_MathBoolNand",
		sizeof(FRigUnit_MathBoolNand),
		alignof(FRigUnit_MathBoolNand),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathBoolNand_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathBoolNand_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathBoolNand()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolNand_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathBoolNand"), sizeof(FRigUnit_MathBoolNand), Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolNand_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathBoolNand_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolNand_Hash() { return 1870756247U; }

void FRigUnit_MathBoolNand::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathBoolAnd>() == std::is_polymorphic<FRigUnit_MathBoolBinaryOp>(), "USTRUCT FRigUnit_MathBoolAnd cannot be polymorphic unless super FRigUnit_MathBoolBinaryOp is polymorphic");

class UScriptStruct* FRigUnit_MathBoolAnd::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolAnd_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathBoolAnd, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathBoolAnd"), sizeof(FRigUnit_MathBoolAnd), Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolAnd_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathBoolAnd::Execute"), &FRigUnit_MathBoolAnd::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathBoolAnd>()
{
	return FRigUnit_MathBoolAnd::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathBoolAnd(FRigUnit_MathBoolAnd::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathBoolAnd"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathBoolAnd
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathBoolAnd()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathBoolAnd>(FName(TEXT("RigUnit_MathBoolAnd")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathBoolAnd;
	struct Z_Construct_UScriptStruct_FRigUnit_MathBoolAnd_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathBoolAnd_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns true if both conditions are true\n */" },
		{ "DisplayName", "And" },
		{ "Keywords", "&&" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathBool.h" },
		{ "PrototypeName", "And" },
		{ "ToolTip", "Returns true if both conditions are true" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathBoolAnd_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathBoolAnd>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathBoolAnd_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp,
		&NewStructOps,
		"RigUnit_MathBoolAnd",
		sizeof(FRigUnit_MathBoolAnd),
		alignof(FRigUnit_MathBoolAnd),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathBoolAnd_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathBoolAnd_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathBoolAnd()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolAnd_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathBoolAnd"), sizeof(FRigUnit_MathBoolAnd), Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolAnd_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathBoolAnd_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolAnd_Hash() { return 4284556235U; }

void FRigUnit_MathBoolAnd::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathBoolNot>() == std::is_polymorphic<FRigUnit_MathBoolUnaryOp>(), "USTRUCT FRigUnit_MathBoolNot cannot be polymorphic unless super FRigUnit_MathBoolUnaryOp is polymorphic");

class UScriptStruct* FRigUnit_MathBoolNot::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolNot_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathBoolNot, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathBoolNot"), sizeof(FRigUnit_MathBoolNot), Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolNot_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathBoolNot::Execute"), &FRigUnit_MathBoolNot::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathBoolNot>()
{
	return FRigUnit_MathBoolNot::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathBoolNot(FRigUnit_MathBoolNot::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathBoolNot"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathBoolNot
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathBoolNot()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathBoolNot>(FName(TEXT("RigUnit_MathBoolNot")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathBoolNot;
	struct Z_Construct_UScriptStruct_FRigUnit_MathBoolNot_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathBoolNot_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns true if the condition is false\n */" },
		{ "DisplayName", "Not" },
		{ "Keywords", "!" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathBool.h" },
		{ "PrototypeName", "Not" },
		{ "ToolTip", "Returns true if the condition is false" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathBoolNot_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathBoolNot>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathBoolNot_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathBoolUnaryOp,
		&NewStructOps,
		"RigUnit_MathBoolNot",
		sizeof(FRigUnit_MathBoolNot),
		alignof(FRigUnit_MathBoolNot),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathBoolNot_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathBoolNot_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathBoolNot()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolNot_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathBoolNot"), sizeof(FRigUnit_MathBoolNot), Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolNot_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathBoolNot_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolNot_Hash() { return 948309350U; }

void FRigUnit_MathBoolNot::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathBoolConstFalse>() == std::is_polymorphic<FRigUnit_MathBoolConstant>(), "USTRUCT FRigUnit_MathBoolConstFalse cannot be polymorphic unless super FRigUnit_MathBoolConstant is polymorphic");

class UScriptStruct* FRigUnit_MathBoolConstFalse::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolConstFalse_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathBoolConstFalse, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathBoolConstFalse"), sizeof(FRigUnit_MathBoolConstFalse), Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolConstFalse_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathBoolConstFalse::Execute"), &FRigUnit_MathBoolConstFalse::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathBoolConstFalse>()
{
	return FRigUnit_MathBoolConstFalse::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathBoolConstFalse(FRigUnit_MathBoolConstFalse::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathBoolConstFalse"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathBoolConstFalse
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathBoolConstFalse()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathBoolConstFalse>(FName(TEXT("RigUnit_MathBoolConstFalse")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathBoolConstFalse;
	struct Z_Construct_UScriptStruct_FRigUnit_MathBoolConstFalse_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathBoolConstFalse_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns false\n */" },
		{ "DisplayName", "False" },
		{ "Keywords", "No" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathBool.h" },
		{ "ToolTip", "Returns false" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathBoolConstFalse_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathBoolConstFalse>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathBoolConstFalse_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathBoolConstant,
		&NewStructOps,
		"RigUnit_MathBoolConstFalse",
		sizeof(FRigUnit_MathBoolConstFalse),
		alignof(FRigUnit_MathBoolConstFalse),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathBoolConstFalse_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathBoolConstFalse_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathBoolConstFalse()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolConstFalse_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathBoolConstFalse"), sizeof(FRigUnit_MathBoolConstFalse), Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolConstFalse_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathBoolConstFalse_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolConstFalse_Hash() { return 2478977415U; }

void FRigUnit_MathBoolConstFalse::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathBoolConstTrue>() == std::is_polymorphic<FRigUnit_MathBoolConstant>(), "USTRUCT FRigUnit_MathBoolConstTrue cannot be polymorphic unless super FRigUnit_MathBoolConstant is polymorphic");

class UScriptStruct* FRigUnit_MathBoolConstTrue::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolConstTrue_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathBoolConstTrue, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathBoolConstTrue"), sizeof(FRigUnit_MathBoolConstTrue), Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolConstTrue_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathBoolConstTrue::Execute"), &FRigUnit_MathBoolConstTrue::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathBoolConstTrue>()
{
	return FRigUnit_MathBoolConstTrue::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathBoolConstTrue(FRigUnit_MathBoolConstTrue::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathBoolConstTrue"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathBoolConstTrue
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathBoolConstTrue()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathBoolConstTrue>(FName(TEXT("RigUnit_MathBoolConstTrue")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathBoolConstTrue;
	struct Z_Construct_UScriptStruct_FRigUnit_MathBoolConstTrue_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathBoolConstTrue_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns true\n */" },
		{ "DisplayName", "True" },
		{ "Keywords", "Yes" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathBool.h" },
		{ "ToolTip", "Returns true" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathBoolConstTrue_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathBoolConstTrue>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathBoolConstTrue_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathBoolConstant,
		&NewStructOps,
		"RigUnit_MathBoolConstTrue",
		sizeof(FRigUnit_MathBoolConstTrue),
		alignof(FRigUnit_MathBoolConstTrue),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathBoolConstTrue_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathBoolConstTrue_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathBoolConstTrue()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolConstTrue_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathBoolConstTrue"), sizeof(FRigUnit_MathBoolConstTrue), Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolConstTrue_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathBoolConstTrue_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolConstTrue_Hash() { return 3650942954U; }

void FRigUnit_MathBoolConstTrue::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathBoolBinaryOp>() == std::is_polymorphic<FRigUnit_MathBoolBase>(), "USTRUCT FRigUnit_MathBoolBinaryOp cannot be polymorphic unless super FRigUnit_MathBoolBase is polymorphic");

class UScriptStruct* FRigUnit_MathBoolBinaryOp::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathBoolBinaryOp"), sizeof(FRigUnit_MathBoolBinaryOp), Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathBoolBinaryOp>()
{
	return FRigUnit_MathBoolBinaryOp::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathBoolBinaryOp(FRigUnit_MathBoolBinaryOp::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathBoolBinaryOp"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathBoolBinaryOp
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathBoolBinaryOp()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathBoolBinaryOp>(FName(TEXT("RigUnit_MathBoolBinaryOp")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathBoolBinaryOp;
	struct Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_A_MetaData[];
#endif
		static void NewProp_A_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_A;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_B_MetaData[];
#endif
		static void NewProp_B_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_B;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static void NewProp_Result_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathBool.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathBoolBinaryOp>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp_Statics::NewProp_A_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathBool.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp_Statics::NewProp_A_SetBit(void* Obj)
	{
		((FRigUnit_MathBoolBinaryOp*)Obj)->A = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp_Statics::NewProp_A = { "A", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_MathBoolBinaryOp), &Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp_Statics::NewProp_A_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp_Statics::NewProp_A_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp_Statics::NewProp_A_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp_Statics::NewProp_B_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathBool.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp_Statics::NewProp_B_SetBit(void* Obj)
	{
		((FRigUnit_MathBoolBinaryOp*)Obj)->B = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp_Statics::NewProp_B = { "B", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_MathBoolBinaryOp), &Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp_Statics::NewProp_B_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp_Statics::NewProp_B_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp_Statics::NewProp_B_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathBool.h" },
		{ "Output", "" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp_Statics::NewProp_Result_SetBit(void* Obj)
	{
		((FRigUnit_MathBoolBinaryOp*)Obj)->Result = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_MathBoolBinaryOp), &Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp_Statics::NewProp_Result_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp_Statics::NewProp_A,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp_Statics::NewProp_B,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathBoolBase,
		&NewStructOps,
		"RigUnit_MathBoolBinaryOp",
		sizeof(FRigUnit_MathBoolBinaryOp),
		alignof(FRigUnit_MathBoolBinaryOp),
		Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathBoolBinaryOp"), sizeof(FRigUnit_MathBoolBinaryOp), Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolBinaryOp_Hash() { return 107898830U; }

static_assert(std::is_polymorphic<FRigUnit_MathBoolUnaryOp>() == std::is_polymorphic<FRigUnit_MathBoolBase>(), "USTRUCT FRigUnit_MathBoolUnaryOp cannot be polymorphic unless super FRigUnit_MathBoolBase is polymorphic");

class UScriptStruct* FRigUnit_MathBoolUnaryOp::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolUnaryOp_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathBoolUnaryOp, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathBoolUnaryOp"), sizeof(FRigUnit_MathBoolUnaryOp), Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolUnaryOp_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathBoolUnaryOp>()
{
	return FRigUnit_MathBoolUnaryOp::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathBoolUnaryOp(FRigUnit_MathBoolUnaryOp::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathBoolUnaryOp"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathBoolUnaryOp
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathBoolUnaryOp()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathBoolUnaryOp>(FName(TEXT("RigUnit_MathBoolUnaryOp")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathBoolUnaryOp;
	struct Z_Construct_UScriptStruct_FRigUnit_MathBoolUnaryOp_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static void NewProp_Value_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static void NewProp_Result_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathBoolUnaryOp_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathBool.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathBoolUnaryOp_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathBoolUnaryOp>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathBoolUnaryOp_Statics::NewProp_Value_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathBool.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_MathBoolUnaryOp_Statics::NewProp_Value_SetBit(void* Obj)
	{
		((FRigUnit_MathBoolUnaryOp*)Obj)->Value = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathBoolUnaryOp_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_MathBoolUnaryOp), &Z_Construct_UScriptStruct_FRigUnit_MathBoolUnaryOp_Statics::NewProp_Value_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathBoolUnaryOp_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathBoolUnaryOp_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathBoolUnaryOp_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathBool.h" },
		{ "Output", "" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_MathBoolUnaryOp_Statics::NewProp_Result_SetBit(void* Obj)
	{
		((FRigUnit_MathBoolUnaryOp*)Obj)->Result = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathBoolUnaryOp_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_MathBoolUnaryOp), &Z_Construct_UScriptStruct_FRigUnit_MathBoolUnaryOp_Statics::NewProp_Result_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathBoolUnaryOp_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathBoolUnaryOp_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathBoolUnaryOp_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathBoolUnaryOp_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathBoolUnaryOp_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathBoolUnaryOp_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathBoolBase,
		&NewStructOps,
		"RigUnit_MathBoolUnaryOp",
		sizeof(FRigUnit_MathBoolUnaryOp),
		alignof(FRigUnit_MathBoolUnaryOp),
		Z_Construct_UScriptStruct_FRigUnit_MathBoolUnaryOp_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathBoolUnaryOp_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathBoolUnaryOp_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathBoolUnaryOp_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathBoolUnaryOp()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolUnaryOp_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathBoolUnaryOp"), sizeof(FRigUnit_MathBoolUnaryOp), Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolUnaryOp_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathBoolUnaryOp_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolUnaryOp_Hash() { return 561274454U; }

static_assert(std::is_polymorphic<FRigUnit_MathBoolConstant>() == std::is_polymorphic<FRigUnit_MathBoolBase>(), "USTRUCT FRigUnit_MathBoolConstant cannot be polymorphic unless super FRigUnit_MathBoolBase is polymorphic");

class UScriptStruct* FRigUnit_MathBoolConstant::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolConstant_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathBoolConstant, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathBoolConstant"), sizeof(FRigUnit_MathBoolConstant), Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolConstant_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathBoolConstant>()
{
	return FRigUnit_MathBoolConstant::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathBoolConstant(FRigUnit_MathBoolConstant::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathBoolConstant"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathBoolConstant
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathBoolConstant()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathBoolConstant>(FName(TEXT("RigUnit_MathBoolConstant")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathBoolConstant;
	struct Z_Construct_UScriptStruct_FRigUnit_MathBoolConstant_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static void NewProp_Value_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathBoolConstant_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathBool.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathBoolConstant_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathBoolConstant>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathBoolConstant_Statics::NewProp_Value_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathBool.h" },
		{ "Output", "" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_MathBoolConstant_Statics::NewProp_Value_SetBit(void* Obj)
	{
		((FRigUnit_MathBoolConstant*)Obj)->Value = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathBoolConstant_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_MathBoolConstant), &Z_Construct_UScriptStruct_FRigUnit_MathBoolConstant_Statics::NewProp_Value_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathBoolConstant_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathBoolConstant_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathBoolConstant_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathBoolConstant_Statics::NewProp_Value,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathBoolConstant_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathBoolBase,
		&NewStructOps,
		"RigUnit_MathBoolConstant",
		sizeof(FRigUnit_MathBoolConstant),
		alignof(FRigUnit_MathBoolConstant),
		Z_Construct_UScriptStruct_FRigUnit_MathBoolConstant_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathBoolConstant_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathBoolConstant_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathBoolConstant_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathBoolConstant()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolConstant_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathBoolConstant"), sizeof(FRigUnit_MathBoolConstant), Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolConstant_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathBoolConstant_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolConstant_Hash() { return 2500025745U; }

static_assert(std::is_polymorphic<FRigUnit_MathBoolBase>() == std::is_polymorphic<FRigUnit_MathBase>(), "USTRUCT FRigUnit_MathBoolBase cannot be polymorphic unless super FRigUnit_MathBase is polymorphic");

class UScriptStruct* FRigUnit_MathBoolBase::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolBase_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathBoolBase, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathBoolBase"), sizeof(FRigUnit_MathBoolBase), Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolBase_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathBoolBase>()
{
	return FRigUnit_MathBoolBase::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathBoolBase(FRigUnit_MathBoolBase::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathBoolBase"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathBoolBase
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathBoolBase()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathBoolBase>(FName(TEXT("RigUnit_MathBoolBase")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathBoolBase;
	struct Z_Construct_UScriptStruct_FRigUnit_MathBoolBase_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathBoolBase_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "Category", "Math|Boolean" },
		{ "MenuDescSuffix", "(Bool)" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathBool.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathBoolBase_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathBoolBase>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathBoolBase_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathBase,
		&NewStructOps,
		"RigUnit_MathBoolBase",
		sizeof(FRigUnit_MathBoolBase),
		alignof(FRigUnit_MathBoolBase),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathBoolBase_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathBoolBase_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathBoolBase()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolBase_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathBoolBase"), sizeof(FRigUnit_MathBoolBase), Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolBase_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathBoolBase_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathBoolBase_Hash() { return 3968143080U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
