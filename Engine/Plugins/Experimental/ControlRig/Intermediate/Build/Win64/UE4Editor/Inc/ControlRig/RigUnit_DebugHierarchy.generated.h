// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigUnit_DebugHierarchy_generated_h
#error "RigUnit_DebugHierarchy.generated.h already included, missing '#pragma once' in RigUnit_DebugHierarchy.h"
#endif
#define CONTROLRIG_RigUnit_DebugHierarchy_generated_h


#define FRigUnit_DebugHierarchy_Execute() \
	void FRigUnit_DebugHierarchy::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const float Scale, \
		const FLinearColor& Color, \
		const float Thickness, \
		const FTransform& WorldOffset, \
		const bool bEnabled, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Debug_RigUnit_DebugHierarchy_h_15_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_DebugHierarchy_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const float Scale, \
		const FLinearColor& Color, \
		const float Thickness, \
		const FTransform& WorldOffset, \
		const bool bEnabled, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const float Scale = *(float*)RigVMMemoryHandles[0].GetData(); \
		const FLinearColor& Color = *(FLinearColor*)RigVMMemoryHandles[1].GetData(); \
		const float Thickness = *(float*)RigVMMemoryHandles[2].GetData(); \
		const FTransform& WorldOffset = *(FTransform*)RigVMMemoryHandles[3].GetData(); \
		const bool bEnabled = *(bool*)RigVMMemoryHandles[4].GetData(); \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[5].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Scale, \
			Color, \
			Thickness, \
			WorldOffset, \
			bEnabled, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnit_DebugBaseMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_DebugHierarchy>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Debug_RigUnit_DebugHierarchy_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
