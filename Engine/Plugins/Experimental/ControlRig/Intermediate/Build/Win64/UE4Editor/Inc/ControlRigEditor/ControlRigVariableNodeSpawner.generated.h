// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIGEDITOR_ControlRigVariableNodeSpawner_generated_h
#error "ControlRigVariableNodeSpawner.generated.h already included, missing '#pragma once' in ControlRigVariableNodeSpawner.h"
#endif
#define CONTROLRIGEDITOR_ControlRigVariableNodeSpawner_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigVariableNodeSpawner_h_24_SPARSE_DATA
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigVariableNodeSpawner_h_24_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigVariableNodeSpawner_h_24_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigVariableNodeSpawner_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUControlRigVariableNodeSpawner(); \
	friend struct Z_Construct_UClass_UControlRigVariableNodeSpawner_Statics; \
public: \
	DECLARE_CLASS(UControlRigVariableNodeSpawner, UBlueprintNodeSpawner, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ControlRigEditor"), NO_API) \
	DECLARE_SERIALIZER(UControlRigVariableNodeSpawner)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigVariableNodeSpawner_h_24_INCLASS \
private: \
	static void StaticRegisterNativesUControlRigVariableNodeSpawner(); \
	friend struct Z_Construct_UClass_UControlRigVariableNodeSpawner_Statics; \
public: \
	DECLARE_CLASS(UControlRigVariableNodeSpawner, UBlueprintNodeSpawner, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ControlRigEditor"), NO_API) \
	DECLARE_SERIALIZER(UControlRigVariableNodeSpawner)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigVariableNodeSpawner_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UControlRigVariableNodeSpawner(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigVariableNodeSpawner) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigVariableNodeSpawner); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigVariableNodeSpawner); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigVariableNodeSpawner(UControlRigVariableNodeSpawner&&); \
	NO_API UControlRigVariableNodeSpawner(const UControlRigVariableNodeSpawner&); \
public:


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigVariableNodeSpawner_h_24_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UControlRigVariableNodeSpawner(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigVariableNodeSpawner(UControlRigVariableNodeSpawner&&); \
	NO_API UControlRigVariableNodeSpawner(const UControlRigVariableNodeSpawner&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigVariableNodeSpawner); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigVariableNodeSpawner); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigVariableNodeSpawner)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigVariableNodeSpawner_h_24_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigVariableNodeSpawner_h_21_PROLOG
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigVariableNodeSpawner_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigVariableNodeSpawner_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigVariableNodeSpawner_h_24_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigVariableNodeSpawner_h_24_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigVariableNodeSpawner_h_24_INCLASS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigVariableNodeSpawner_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigVariableNodeSpawner_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigVariableNodeSpawner_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigVariableNodeSpawner_h_24_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigVariableNodeSpawner_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigVariableNodeSpawner_h_24_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigVariableNodeSpawner_h_24_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONTROLRIGEDITOR_API UClass* StaticClass<class UControlRigVariableNodeSpawner>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigVariableNodeSpawner_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
