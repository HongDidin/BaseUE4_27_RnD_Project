// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIGEDITOR_ControlRigSkeletalMeshComponent_generated_h
#error "ControlRigSkeletalMeshComponent.generated.h already included, missing '#pragma once' in ControlRigSkeletalMeshComponent.h"
#endif
#define CONTROLRIGEDITOR_ControlRigSkeletalMeshComponent_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Editor_ControlRigSkeletalMeshComponent_h_11_SPARSE_DATA
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Editor_ControlRigSkeletalMeshComponent_h_11_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Editor_ControlRigSkeletalMeshComponent_h_11_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Editor_ControlRigSkeletalMeshComponent_h_11_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUControlRigSkeletalMeshComponent(); \
	friend struct Z_Construct_UClass_UControlRigSkeletalMeshComponent_Statics; \
public: \
	DECLARE_CLASS(UControlRigSkeletalMeshComponent, UDebugSkelMeshComponent, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ControlRigEditor"), CONTROLRIGEDITOR_API) \
	DECLARE_SERIALIZER(UControlRigSkeletalMeshComponent)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Editor_ControlRigSkeletalMeshComponent_h_11_INCLASS \
private: \
	static void StaticRegisterNativesUControlRigSkeletalMeshComponent(); \
	friend struct Z_Construct_UClass_UControlRigSkeletalMeshComponent_Statics; \
public: \
	DECLARE_CLASS(UControlRigSkeletalMeshComponent, UDebugSkelMeshComponent, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ControlRigEditor"), CONTROLRIGEDITOR_API) \
	DECLARE_SERIALIZER(UControlRigSkeletalMeshComponent)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Editor_ControlRigSkeletalMeshComponent_h_11_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	CONTROLRIGEDITOR_API UControlRigSkeletalMeshComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigSkeletalMeshComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(CONTROLRIGEDITOR_API, UControlRigSkeletalMeshComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigSkeletalMeshComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	CONTROLRIGEDITOR_API UControlRigSkeletalMeshComponent(UControlRigSkeletalMeshComponent&&); \
	CONTROLRIGEDITOR_API UControlRigSkeletalMeshComponent(const UControlRigSkeletalMeshComponent&); \
public:


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Editor_ControlRigSkeletalMeshComponent_h_11_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	CONTROLRIGEDITOR_API UControlRigSkeletalMeshComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	CONTROLRIGEDITOR_API UControlRigSkeletalMeshComponent(UControlRigSkeletalMeshComponent&&); \
	CONTROLRIGEDITOR_API UControlRigSkeletalMeshComponent(const UControlRigSkeletalMeshComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(CONTROLRIGEDITOR_API, UControlRigSkeletalMeshComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigSkeletalMeshComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigSkeletalMeshComponent)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Editor_ControlRigSkeletalMeshComponent_h_11_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Editor_ControlRigSkeletalMeshComponent_h_8_PROLOG
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Editor_ControlRigSkeletalMeshComponent_h_11_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Editor_ControlRigSkeletalMeshComponent_h_11_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Editor_ControlRigSkeletalMeshComponent_h_11_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Editor_ControlRigSkeletalMeshComponent_h_11_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Editor_ControlRigSkeletalMeshComponent_h_11_INCLASS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Editor_ControlRigSkeletalMeshComponent_h_11_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Editor_ControlRigSkeletalMeshComponent_h_11_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Editor_ControlRigSkeletalMeshComponent_h_11_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Editor_ControlRigSkeletalMeshComponent_h_11_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Editor_ControlRigSkeletalMeshComponent_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Editor_ControlRigSkeletalMeshComponent_h_11_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Editor_ControlRigSkeletalMeshComponent_h_11_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ControlRigSkeletalMeshComponent."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONTROLRIGEDITOR_API UClass* StaticClass<class UControlRigSkeletalMeshComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Editor_ControlRigSkeletalMeshComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
