// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Animation/RigUnit_GetDeltaTime.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_GetDeltaTime() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_GetDeltaTime();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_AnimBase();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_GetDeltaTime>() == std::is_polymorphic<FRigUnit_AnimBase>(), "USTRUCT FRigUnit_GetDeltaTime cannot be polymorphic unless super FRigUnit_AnimBase is polymorphic");

class UScriptStruct* FRigUnit_GetDeltaTime::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_GetDeltaTime_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_GetDeltaTime, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_GetDeltaTime"), sizeof(FRigUnit_GetDeltaTime), Get_Z_Construct_UScriptStruct_FRigUnit_GetDeltaTime_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_GetDeltaTime::Execute"), &FRigUnit_GetDeltaTime::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_GetDeltaTime>()
{
	return FRigUnit_GetDeltaTime::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_GetDeltaTime(FRigUnit_GetDeltaTime::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_GetDeltaTime"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_GetDeltaTime
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_GetDeltaTime()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_GetDeltaTime>(FName(TEXT("RigUnit_GetDeltaTime")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_GetDeltaTime;
	struct Z_Construct_UScriptStruct_FRigUnit_GetDeltaTime_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_GetDeltaTime_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the time gone by from the previous evaluation\n */" },
		{ "DisplayName", "Delta Time" },
		{ "ModuleRelativePath", "Private/Units/Animation/RigUnit_GetDeltaTime.h" },
		{ "ToolTip", "Returns the time gone by from the previous evaluation" },
		{ "Varying", "" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_GetDeltaTime_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_GetDeltaTime>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_GetDeltaTime_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Animation/RigUnit_GetDeltaTime.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_GetDeltaTime_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_GetDeltaTime, Result), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_GetDeltaTime_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_GetDeltaTime_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_GetDeltaTime_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_GetDeltaTime_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_GetDeltaTime_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_AnimBase,
		&NewStructOps,
		"RigUnit_GetDeltaTime",
		sizeof(FRigUnit_GetDeltaTime),
		alignof(FRigUnit_GetDeltaTime),
		Z_Construct_UScriptStruct_FRigUnit_GetDeltaTime_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_GetDeltaTime_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_GetDeltaTime_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_GetDeltaTime_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_GetDeltaTime()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_GetDeltaTime_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_GetDeltaTime"), sizeof(FRigUnit_GetDeltaTime), Get_Z_Construct_UScriptStruct_FRigUnit_GetDeltaTime_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_GetDeltaTime_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_GetDeltaTime_Hash() { return 3946392434U; }

void FRigUnit_GetDeltaTime::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Result,
		Context
	);
}

PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
