// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigHierarchyDefines_generated_h
#error "RigHierarchyDefines.generated.h already included, missing '#pragma once' in RigHierarchyDefines.h"
#endif
#define CONTROLRIG_RigHierarchyDefines_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_RigHierarchyDefines_h_377_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigEventContext_Statics; \
	static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigEventContext>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_RigHierarchyDefines_h_349_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigElement_Statics; \
	static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigElement>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_RigHierarchyDefines_h_199_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigElementKeyCollection_Statics; \
	static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigElementKeyCollection>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_RigHierarchyDefines_h_104_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigElementKey_Statics; \
	static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigElementKey>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_RigHierarchyDefines_h_50_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigControlModifiedContext_Statics; \
	static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigControlModifiedContext>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_RigHierarchyDefines_h


#define FOREACH_ENUM_ECONTROLRIGSETKEY(op) \
	op(EControlRigSetKey::DoNotCare) \
	op(EControlRigSetKey::Always) \
	op(EControlRigSetKey::Never) 

enum class EControlRigSetKey : uint8;
template<> CONTROLRIG_API UEnum* StaticEnum<EControlRigSetKey>();

#define FOREACH_ENUM_ERIGEVENT(op) \
	op(ERigEvent::None) \
	op(ERigEvent::RequestAutoKey) \
	op(ERigEvent::Max) 

enum class ERigEvent : uint8;
template<> CONTROLRIG_API UEnum* StaticEnum<ERigEvent>();

#define FOREACH_ENUM_ERIGELEMENTTYPE(op) \
	op(ERigElementType::None) \
	op(ERigElementType::Bone) \
	op(ERigElementType::Space) \
	op(ERigElementType::Control) \
	op(ERigElementType::Curve) \
	op(ERigElementType::All) 

enum class ERigElementType : uint8;
template<> CONTROLRIG_API UEnum* StaticEnum<ERigElementType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
