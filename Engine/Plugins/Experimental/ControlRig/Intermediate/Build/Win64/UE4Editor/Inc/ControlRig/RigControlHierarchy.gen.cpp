// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Public/Rigs/RigControlHierarchy.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigControlHierarchy() {}
// Cross Module References
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_ERigControlAxis();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_ERigControlValueType();
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_ERigControlType();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigControlHierarchy();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigControl();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigElement();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigControlValue();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
	COREUOBJECT_API UClass* Z_Construct_UClass_UEnum();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigControlValueStorage();
// End Cross Module References
	static UEnum* ERigControlAxis_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ControlRig_ERigControlAxis, Z_Construct_UPackage__Script_ControlRig(), TEXT("ERigControlAxis"));
		}
		return Singleton;
	}
	template<> CONTROLRIG_API UEnum* StaticEnum<ERigControlAxis>()
	{
		return ERigControlAxis_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ERigControlAxis(ERigControlAxis_StaticEnum, TEXT("/Script/ControlRig"), TEXT("ERigControlAxis"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ControlRig_ERigControlAxis_Hash() { return 2536318498U; }
	UEnum* Z_Construct_UEnum_ControlRig_ERigControlAxis()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ERigControlAxis"), 0, Get_Z_Construct_UEnum_ControlRig_ERigControlAxis_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ERigControlAxis::X", (int64)ERigControlAxis::X },
				{ "ERigControlAxis::Y", (int64)ERigControlAxis::Y },
				{ "ERigControlAxis::Z", (int64)ERigControlAxis::Z },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
				{ "X.Name", "ERigControlAxis::X" },
				{ "Y.Name", "ERigControlAxis::Y" },
				{ "Z.Name", "ERigControlAxis::Z" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ControlRig,
				nullptr,
				"ERigControlAxis",
				"ERigControlAxis",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ERigControlValueType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ControlRig_ERigControlValueType, Z_Construct_UPackage__Script_ControlRig(), TEXT("ERigControlValueType"));
		}
		return Singleton;
	}
	template<> CONTROLRIG_API UEnum* StaticEnum<ERigControlValueType>()
	{
		return ERigControlValueType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ERigControlValueType(ERigControlValueType_StaticEnum, TEXT("/Script/ControlRig"), TEXT("ERigControlValueType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ControlRig_ERigControlValueType_Hash() { return 484710886U; }
	UEnum* Z_Construct_UEnum_ControlRig_ERigControlValueType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ERigControlValueType"), 0, Get_Z_Construct_UEnum_ControlRig_ERigControlValueType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ERigControlValueType::Initial", (int64)ERigControlValueType::Initial },
				{ "ERigControlValueType::Current", (int64)ERigControlValueType::Current },
				{ "ERigControlValueType::Minimum", (int64)ERigControlValueType::Minimum },
				{ "ERigControlValueType::Maximum", (int64)ERigControlValueType::Maximum },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Current.Name", "ERigControlValueType::Current" },
				{ "Initial.Name", "ERigControlValueType::Initial" },
				{ "Maximum.Name", "ERigControlValueType::Maximum" },
				{ "Minimum.Name", "ERigControlValueType::Minimum" },
				{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ControlRig,
				nullptr,
				"ERigControlValueType",
				"ERigControlValueType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ERigControlType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ControlRig_ERigControlType, Z_Construct_UPackage__Script_ControlRig(), TEXT("ERigControlType"));
		}
		return Singleton;
	}
	template<> CONTROLRIG_API UEnum* StaticEnum<ERigControlType>()
	{
		return ERigControlType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ERigControlType(ERigControlType_StaticEnum, TEXT("/Script/ControlRig"), TEXT("ERigControlType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ControlRig_ERigControlType_Hash() { return 4265387854U; }
	UEnum* Z_Construct_UEnum_ControlRig_ERigControlType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ERigControlType"), 0, Get_Z_Construct_UEnum_ControlRig_ERigControlType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ERigControlType::Bool", (int64)ERigControlType::Bool },
				{ "ERigControlType::Float", (int64)ERigControlType::Float },
				{ "ERigControlType::Integer", (int64)ERigControlType::Integer },
				{ "ERigControlType::Vector2D", (int64)ERigControlType::Vector2D },
				{ "ERigControlType::Position", (int64)ERigControlType::Position },
				{ "ERigControlType::Scale", (int64)ERigControlType::Scale },
				{ "ERigControlType::Rotator", (int64)ERigControlType::Rotator },
				{ "ERigControlType::Transform", (int64)ERigControlType::Transform },
				{ "ERigControlType::TransformNoScale", (int64)ERigControlType::TransformNoScale },
				{ "ERigControlType::EulerTransform", (int64)ERigControlType::EulerTransform },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Bool.Name", "ERigControlType::Bool" },
				{ "EulerTransform.Name", "ERigControlType::EulerTransform" },
				{ "Float.Name", "ERigControlType::Float" },
				{ "Integer.Name", "ERigControlType::Integer" },
				{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
				{ "Position.Name", "ERigControlType::Position" },
				{ "Rotator.Name", "ERigControlType::Rotator" },
				{ "Scale.Name", "ERigControlType::Scale" },
				{ "Transform.Name", "ERigControlType::Transform" },
				{ "TransformNoScale.Name", "ERigControlType::TransformNoScale" },
				{ "Vector2D.Name", "ERigControlType::Vector2D" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ControlRig,
				nullptr,
				"ERigControlType",
				"ERigControlType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FRigControlHierarchy::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigControlHierarchy_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigControlHierarchy, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigControlHierarchy"), sizeof(FRigControlHierarchy), Get_Z_Construct_UScriptStruct_FRigControlHierarchy_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigControlHierarchy>()
{
	return FRigControlHierarchy::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigControlHierarchy(FRigControlHierarchy::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigControlHierarchy"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigControlHierarchy
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigControlHierarchy()
	{
		UScriptStruct::DeferCppStructOps<FRigControlHierarchy>(FName(TEXT("RigControlHierarchy")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigControlHierarchy;
	struct Z_Construct_UScriptStruct_FRigControlHierarchy_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Controls_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Controls_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Controls;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NameToIndexMapping_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_NameToIndexMapping_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NameToIndexMapping_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_NameToIndexMapping;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Selection_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Selection_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Selection;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControlHierarchy_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigControlHierarchy_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigControlHierarchy>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigControlHierarchy_Statics::NewProp_Controls_Inner = { "Controls", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRigControl, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControlHierarchy_Statics::NewProp_Controls_MetaData[] = {
		{ "Category", "FRigControlHierarchy" },
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigControlHierarchy_Statics::NewProp_Controls = { "Controls", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigControlHierarchy, Controls), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControlHierarchy_Statics::NewProp_Controls_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControlHierarchy_Statics::NewProp_Controls_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigControlHierarchy_Statics::NewProp_NameToIndexMapping_ValueProp = { "NameToIndexMapping", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigControlHierarchy_Statics::NewProp_NameToIndexMapping_Key_KeyProp = { "NameToIndexMapping_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControlHierarchy_Statics::NewProp_NameToIndexMapping_MetaData[] = {
		{ "Comment", "// can serialize fine? \n" },
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
		{ "ToolTip", "can serialize fine?" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FRigControlHierarchy_Statics::NewProp_NameToIndexMapping = { "NameToIndexMapping", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigControlHierarchy, NameToIndexMapping), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControlHierarchy_Statics::NewProp_NameToIndexMapping_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControlHierarchy_Statics::NewProp_NameToIndexMapping_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigControlHierarchy_Statics::NewProp_Selection_Inner = { "Selection", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControlHierarchy_Statics::NewProp_Selection_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigControlHierarchy_Statics::NewProp_Selection = { "Selection", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigControlHierarchy, Selection), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControlHierarchy_Statics::NewProp_Selection_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControlHierarchy_Statics::NewProp_Selection_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigControlHierarchy_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControlHierarchy_Statics::NewProp_Controls_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControlHierarchy_Statics::NewProp_Controls,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControlHierarchy_Statics::NewProp_NameToIndexMapping_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControlHierarchy_Statics::NewProp_NameToIndexMapping_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControlHierarchy_Statics::NewProp_NameToIndexMapping,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControlHierarchy_Statics::NewProp_Selection_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControlHierarchy_Statics::NewProp_Selection,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigControlHierarchy_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"RigControlHierarchy",
		sizeof(FRigControlHierarchy),
		alignof(FRigControlHierarchy),
		Z_Construct_UScriptStruct_FRigControlHierarchy_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControlHierarchy_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControlHierarchy_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControlHierarchy_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigControlHierarchy()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigControlHierarchy_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigControlHierarchy"), sizeof(FRigControlHierarchy), Get_Z_Construct_UScriptStruct_FRigControlHierarchy_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigControlHierarchy_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigControlHierarchy_Hash() { return 203103871U; }

static_assert(std::is_polymorphic<FRigControl>() == std::is_polymorphic<FRigElement>(), "USTRUCT FRigControl cannot be polymorphic unless super FRigElement is polymorphic");

class UScriptStruct* FRigControl::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigControl_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigControl, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigControl"), sizeof(FRigControl), Get_Z_Construct_UScriptStruct_FRigControl_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigControl>()
{
	return FRigControl::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigControl(FRigControl::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigControl"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigControl
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigControl()
	{
		UScriptStruct::DeferCppStructOps<FRigControl>(FName(TEXT("RigControl")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigControl;
	struct Z_Construct_UScriptStruct_FRigControl_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ControlType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ControlType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ControlType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisplayName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_DisplayName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParentName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ParentName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParentIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ParentIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpaceName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_SpaceName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpaceIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SpaceIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OffsetTransform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OffsetTransform;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InitialValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InitialValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_PrimaryAxis_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PrimaryAxis_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_PrimaryAxis;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsCurve_MetaData[];
#endif
		static void NewProp_bIsCurve_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsCurve;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAnimatable_MetaData[];
#endif
		static void NewProp_bAnimatable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAnimatable;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bLimitTranslation_MetaData[];
#endif
		static void NewProp_bLimitTranslation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bLimitTranslation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bLimitRotation_MetaData[];
#endif
		static void NewProp_bLimitRotation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bLimitRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bLimitScale_MetaData[];
#endif
		static void NewProp_bLimitScale_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bLimitScale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDrawLimits_MetaData[];
#endif
		static void NewProp_bDrawLimits_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDrawLimits;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MinimumValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MinimumValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaximumValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MaximumValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bGizmoEnabled_MetaData[];
#endif
		static void NewProp_bGizmoEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bGizmoEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bGizmoVisible_MetaData[];
#endif
		static void NewProp_bGizmoVisible_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bGizmoVisible;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GizmoName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_GizmoName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GizmoTransform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GizmoTransform;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GizmoColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GizmoColor;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Dependents_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Dependents_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Dependents;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsTransientControl_MetaData[];
#endif
		static void NewProp_bIsTransientControl_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsTransientControl;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ControlEnum_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ControlEnum;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControl_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigControl_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigControl>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_ControlType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_ControlType_MetaData[] = {
		{ "Category", "Control" },
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_ControlType = { "ControlType", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigControl, ControlType), Z_Construct_UEnum_ControlRig_ERigControlType, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_ControlType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_ControlType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_DisplayName_MetaData[] = {
		{ "Category", "Control" },
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_DisplayName = { "DisplayName", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigControl, DisplayName), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_DisplayName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_DisplayName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_ParentName_MetaData[] = {
		{ "Category", "Control" },
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_ParentName = { "ParentName", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigControl, ParentName), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_ParentName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_ParentName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_ParentIndex_MetaData[] = {
		{ "Category", "Control" },
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_ParentIndex = { "ParentIndex", nullptr, (EPropertyFlags)0x0010000000002014, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigControl, ParentIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_ParentIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_ParentIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_SpaceName_MetaData[] = {
		{ "Category", "Control" },
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_SpaceName = { "SpaceName", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigControl, SpaceName), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_SpaceName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_SpaceName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_SpaceIndex_MetaData[] = {
		{ "Category", "Control" },
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_SpaceIndex = { "SpaceIndex", nullptr, (EPropertyFlags)0x0010000000002014, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigControl, SpaceIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_SpaceIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_SpaceIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_OffsetTransform_MetaData[] = {
		{ "Category", "Control" },
		{ "Comment", "/**\n\x09 * Used to offset a control in global space. This can be useful\n\x09 * to offset a float control by rotating it or translating it.\n\x09 */" },
		{ "DisplayAfter", "ControlType" },
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
		{ "ToolTip", "Used to offset a control in global space. This can be useful\nto offset a float control by rotating it or translating it." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_OffsetTransform = { "OffsetTransform", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigControl, OffsetTransform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_OffsetTransform_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_OffsetTransform_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_InitialValue_MetaData[] = {
		{ "Category", "Control" },
		{ "Comment", "/**\n\x09 * The value that a control is reset to during begin play or when the\n\x09 * control rig is instantiated.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
		{ "ToolTip", "The value that a control is reset to during begin play or when the\ncontrol rig is instantiated." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_InitialValue = { "InitialValue", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigControl, InitialValue), Z_Construct_UScriptStruct_FRigControlValue, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_InitialValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_InitialValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_Value_MetaData[] = {
		{ "Category", "Control" },
		{ "Comment", "/**\n\x09 * The current value of the control.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
		{ "ToolTip", "The current value of the control." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000022015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigControl, Value), Z_Construct_UScriptStruct_FRigControlValue, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_PrimaryAxis_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_PrimaryAxis_MetaData[] = {
		{ "Category", "Control" },
		{ "Comment", "/** the primary axis to use for float controls */" },
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
		{ "ToolTip", "the primary axis to use for float controls" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_PrimaryAxis = { "PrimaryAxis", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigControl, PrimaryAxis), Z_Construct_UEnum_ControlRig_ERigControlAxis, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_PrimaryAxis_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_PrimaryAxis_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bIsCurve_MetaData[] = {
		{ "Comment", "/** If Created from a Curve  Container*/" },
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
		{ "ToolTip", "If Created from a Curve  Container" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bIsCurve_SetBit(void* Obj)
	{
		((FRigControl*)Obj)->bIsCurve = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bIsCurve = { "bIsCurve", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigControl), &Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bIsCurve_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bIsCurve_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bIsCurve_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bAnimatable_MetaData[] = {
		{ "Category", "Control" },
		{ "Comment", "/** If the control is animatable in sequencer */" },
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
		{ "ToolTip", "If the control is animatable in sequencer" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bAnimatable_SetBit(void* Obj)
	{
		((FRigControl*)Obj)->bAnimatable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bAnimatable = { "bAnimatable", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigControl), &Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bAnimatable_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bAnimatable_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bAnimatable_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bLimitTranslation_MetaData[] = {
		{ "Category", "Limits" },
		{ "Comment", "/** True if the control has to obey translation limits. */" },
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
		{ "ToolTip", "True if the control has to obey translation limits." },
	};
#endif
	void Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bLimitTranslation_SetBit(void* Obj)
	{
		((FRigControl*)Obj)->bLimitTranslation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bLimitTranslation = { "bLimitTranslation", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigControl), &Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bLimitTranslation_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bLimitTranslation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bLimitTranslation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bLimitRotation_MetaData[] = {
		{ "Category", "Limits" },
		{ "Comment", "/** True if the control has to obey rotation limits. */" },
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
		{ "ToolTip", "True if the control has to obey rotation limits." },
	};
#endif
	void Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bLimitRotation_SetBit(void* Obj)
	{
		((FRigControl*)Obj)->bLimitRotation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bLimitRotation = { "bLimitRotation", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigControl), &Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bLimitRotation_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bLimitRotation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bLimitRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bLimitScale_MetaData[] = {
		{ "Category", "Limits" },
		{ "Comment", "/** True if the control has to obey scale limits. */" },
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
		{ "ToolTip", "True if the control has to obey scale limits." },
	};
#endif
	void Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bLimitScale_SetBit(void* Obj)
	{
		((FRigControl*)Obj)->bLimitScale = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bLimitScale = { "bLimitScale", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigControl), &Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bLimitScale_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bLimitScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bLimitScale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bDrawLimits_MetaData[] = {
		{ "Category", "Limits" },
		{ "Comment", "/** True if the limits should be drawn in debug. */" },
		{ "EditCondition", "bLimitTranslation || bLimitRotation || bLimitScale" },
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
		{ "ToolTip", "True if the limits should be drawn in debug." },
	};
#endif
	void Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bDrawLimits_SetBit(void* Obj)
	{
		((FRigControl*)Obj)->bDrawLimits = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bDrawLimits = { "bDrawLimits", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigControl), &Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bDrawLimits_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bDrawLimits_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bDrawLimits_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_MinimumValue_MetaData[] = {
		{ "Category", "Limits" },
		{ "Comment", "/** The minimum limit of the control's value */" },
		{ "EditCondition", "bLimitTranslation || bLimitRotation || bLimitScale" },
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
		{ "ToolTip", "The minimum limit of the control's value" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_MinimumValue = { "MinimumValue", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigControl, MinimumValue), Z_Construct_UScriptStruct_FRigControlValue, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_MinimumValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_MinimumValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_MaximumValue_MetaData[] = {
		{ "Category", "Limits" },
		{ "Comment", "/** The maximum limit of the control's value */" },
		{ "EditCondition", "bLimitTranslation || bLimitRotation || bLimitScale" },
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
		{ "ToolTip", "The maximum limit of the control's value" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_MaximumValue = { "MaximumValue", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigControl, MaximumValue), Z_Construct_UScriptStruct_FRigControlValue, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_MaximumValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_MaximumValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bGizmoEnabled_MetaData[] = {
		{ "Category", "Gizmo" },
		{ "Comment", "/** Set to true if the gizmo is enabled in 3d */" },
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
		{ "ToolTip", "Set to true if the gizmo is enabled in 3d" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bGizmoEnabled_SetBit(void* Obj)
	{
		((FRigControl*)Obj)->bGizmoEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bGizmoEnabled = { "bGizmoEnabled", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigControl), &Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bGizmoEnabled_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bGizmoEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bGizmoEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bGizmoVisible_MetaData[] = {
		{ "Category", "Gizmo" },
		{ "Comment", "/** Set to true if the gizmo is currently visible in 3d */" },
		{ "EditCondition", "bGizmoEnabled" },
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
		{ "ToolTip", "Set to true if the gizmo is currently visible in 3d" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bGizmoVisible_SetBit(void* Obj)
	{
		((FRigControl*)Obj)->bGizmoVisible = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bGizmoVisible = { "bGizmoVisible", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigControl), &Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bGizmoVisible_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bGizmoVisible_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bGizmoVisible_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_GizmoName_MetaData[] = {
		{ "Category", "Gizmo" },
		{ "Comment", "/* This is optional UI setting - this doesn't mean this is always used, but it is optional for manipulation layer to use this*/" },
		{ "EditCondition", "bGizmoEnabled" },
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
		{ "ToolTip", "This is optional UI setting - this doesn't mean this is always used, but it is optional for manipulation layer to use this" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_GizmoName = { "GizmoName", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigControl, GizmoName), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_GizmoName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_GizmoName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_GizmoTransform_MetaData[] = {
		{ "Category", "Gizmo" },
		{ "EditCondition", "bGizmoEnabled" },
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_GizmoTransform = { "GizmoTransform", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigControl, GizmoTransform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_GizmoTransform_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_GizmoTransform_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_GizmoColor_MetaData[] = {
		{ "Category", "Gizmo" },
		{ "EditCondition", "bGizmoEnabled" },
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_GizmoColor = { "GizmoColor", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigControl, GizmoColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_GizmoColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_GizmoColor_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_Dependents_Inner = { "Dependents", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_Dependents_MetaData[] = {
		{ "Comment", "/** dependent list - direct dependent for child or anything that needs to update due to this */" },
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
		{ "ToolTip", "dependent list - direct dependent for child or anything that needs to update due to this" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_Dependents = { "Dependents", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigControl, Dependents), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_Dependents_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_Dependents_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bIsTransientControl_MetaData[] = {
		{ "Category", "Control" },
		{ "Comment", "/** If the control is transient and only visible in the control rig editor */" },
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
		{ "ToolTip", "If the control is transient and only visible in the control rig editor" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bIsTransientControl_SetBit(void* Obj)
	{
		((FRigControl*)Obj)->bIsTransientControl = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bIsTransientControl = { "bIsTransientControl", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigControl), &Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bIsTransientControl_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bIsTransientControl_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bIsTransientControl_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_ControlEnum_MetaData[] = {
		{ "Category", "Control" },
		{ "Comment", "/** If the control is transient and only visible in the control rig editor */" },
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
		{ "ToolTip", "If the control is transient and only visible in the control rig editor" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_ControlEnum = { "ControlEnum", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigControl, ControlEnum), Z_Construct_UClass_UEnum, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_ControlEnum_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_ControlEnum_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigControl_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_ControlType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_ControlType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_DisplayName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_ParentName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_ParentIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_SpaceName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_SpaceIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_OffsetTransform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_InitialValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_PrimaryAxis_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_PrimaryAxis,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bIsCurve,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bAnimatable,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bLimitTranslation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bLimitRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bLimitScale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bDrawLimits,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_MinimumValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_MaximumValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bGizmoEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bGizmoVisible,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_GizmoName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_GizmoTransform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_GizmoColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_Dependents_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_Dependents,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_bIsTransientControl,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControl_Statics::NewProp_ControlEnum,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigControl_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigElement,
		&NewStructOps,
		"RigControl",
		sizeof(FRigControl),
		alignof(FRigControl),
		Z_Construct_UScriptStruct_FRigControl_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControl_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControl_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControl_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigControl()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigControl_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigControl"), sizeof(FRigControl), Get_Z_Construct_UScriptStruct_FRigControl_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigControl_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigControl_Hash() { return 3965488565U; }
class UScriptStruct* FRigControlValue::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigControlValue_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigControlValue, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigControlValue"), sizeof(FRigControlValue), Get_Z_Construct_UScriptStruct_FRigControlValue_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigControlValue>()
{
	return FRigControlValue::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigControlValue(FRigControlValue::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigControlValue"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigControlValue
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigControlValue()
	{
		UScriptStruct::DeferCppStructOps<FRigControlValue>(FName(TEXT("RigControlValue")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigControlValue;
	struct Z_Construct_UScriptStruct_FRigControlValue_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FloatStorage_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FloatStorage;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Storage_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Storage;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControlValue_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigControlValue_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigControlValue>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControlValue_Statics::NewProp_FloatStorage_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigControlValue_Statics::NewProp_FloatStorage = { "FloatStorage", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigControlValue, FloatStorage), Z_Construct_UScriptStruct_FRigControlValueStorage, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControlValue_Statics::NewProp_FloatStorage_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControlValue_Statics::NewProp_FloatStorage_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControlValue_Statics::NewProp_Storage_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigControlValue_Statics::NewProp_Storage = { "Storage", nullptr, (EPropertyFlags)0x0040000020000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigControlValue, Storage_DEPRECATED), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControlValue_Statics::NewProp_Storage_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControlValue_Statics::NewProp_Storage_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigControlValue_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControlValue_Statics::NewProp_FloatStorage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControlValue_Statics::NewProp_Storage,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigControlValue_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"RigControlValue",
		sizeof(FRigControlValue),
		alignof(FRigControlValue),
		Z_Construct_UScriptStruct_FRigControlValue_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControlValue_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControlValue_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControlValue_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigControlValue()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigControlValue_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigControlValue"), sizeof(FRigControlValue), Get_Z_Construct_UScriptStruct_FRigControlValue_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigControlValue_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigControlValue_Hash() { return 1731485706U; }
class UScriptStruct* FRigControlValueStorage::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigControlValueStorage_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigControlValueStorage, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigControlValueStorage"), sizeof(FRigControlValueStorage), Get_Z_Construct_UScriptStruct_FRigControlValueStorage_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigControlValueStorage>()
{
	return FRigControlValueStorage::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigControlValueStorage(FRigControlValueStorage::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigControlValueStorage"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigControlValueStorage
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigControlValueStorage()
	{
		UScriptStruct::DeferCppStructOps<FRigControlValueStorage>(FName(TEXT("RigControlValueStorage")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigControlValueStorage;
	struct Z_Construct_UScriptStruct_FRigControlValueStorage_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Float00_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Float00;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Float01_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Float01;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Float02_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Float02;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Float03_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Float03;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Float10_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Float10;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Float11_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Float11;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Float12_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Float12;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Float13_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Float13;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Float20_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Float20;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Float21_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Float21;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Float22_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Float22;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Float23_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Float23;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Float30_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Float30;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Float31_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Float31;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Float32_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Float32;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Float33_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Float33;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bValid_MetaData[];
#endif
		static void NewProp_bValid_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bValid;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigControlValueStorage>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float00_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float00 = { "Float00", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigControlValueStorage, Float00), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float00_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float00_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float01_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float01 = { "Float01", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigControlValueStorage, Float01), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float01_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float01_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float02_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float02 = { "Float02", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigControlValueStorage, Float02), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float02_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float02_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float03_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float03 = { "Float03", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigControlValueStorage, Float03), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float03_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float03_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float10_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float10 = { "Float10", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigControlValueStorage, Float10), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float10_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float10_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float11_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float11 = { "Float11", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigControlValueStorage, Float11), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float11_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float11_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float12_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float12 = { "Float12", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigControlValueStorage, Float12), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float12_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float12_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float13_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float13 = { "Float13", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigControlValueStorage, Float13), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float13_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float13_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float20_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float20 = { "Float20", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigControlValueStorage, Float20), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float20_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float20_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float21_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float21 = { "Float21", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigControlValueStorage, Float21), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float21_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float21_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float22_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float22 = { "Float22", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigControlValueStorage, Float22), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float22_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float22_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float23_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float23 = { "Float23", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigControlValueStorage, Float23), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float23_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float23_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float30_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float30 = { "Float30", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigControlValueStorage, Float30), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float30_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float30_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float31_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float31 = { "Float31", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigControlValueStorage, Float31), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float31_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float31_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float32_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float32 = { "Float32", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigControlValueStorage, Float32), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float32_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float32_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float33_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float33 = { "Float33", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigControlValueStorage, Float33), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float33_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float33_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_bValid_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/RigControlHierarchy.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_bValid_SetBit(void* Obj)
	{
		((FRigControlValueStorage*)Obj)->bValid = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_bValid = { "bValid", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigControlValueStorage), &Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_bValid_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_bValid_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_bValid_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float00,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float01,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float02,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float03,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float10,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float11,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float12,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float13,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float20,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float21,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float22,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float23,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float30,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float31,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float32,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_Float33,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::NewProp_bValid,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"RigControlValueStorage",
		sizeof(FRigControlValueStorage),
		alignof(FRigControlValueStorage),
		Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigControlValueStorage()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigControlValueStorage_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigControlValueStorage"), sizeof(FRigControlValueStorage), Get_Z_Construct_UScriptStruct_FRigControlValueStorage_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigControlValueStorage_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigControlValueStorage_Hash() { return 3828351994U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
