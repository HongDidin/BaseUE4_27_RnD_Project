// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRigEditor/Private/Editor/SRigHierarchy.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSRigHierarchy() {}
// Cross Module References
	CONTROLRIGEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FRigHierarchyImportSettings();
	UPackage* Z_Construct_UPackage__Script_ControlRigEditor();
	ENGINE_API UClass* Z_Construct_UClass_USkeletalMesh_NoRegister();
// End Cross Module References
class UScriptStruct* FRigHierarchyImportSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIGEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FRigHierarchyImportSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigHierarchyImportSettings, Z_Construct_UPackage__Script_ControlRigEditor(), TEXT("RigHierarchyImportSettings"), sizeof(FRigHierarchyImportSettings), Get_Z_Construct_UScriptStruct_FRigHierarchyImportSettings_Hash());
	}
	return Singleton;
}
template<> CONTROLRIGEDITOR_API UScriptStruct* StaticStruct<FRigHierarchyImportSettings>()
{
	return FRigHierarchyImportSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigHierarchyImportSettings(FRigHierarchyImportSettings::StaticStruct, TEXT("/Script/ControlRigEditor"), TEXT("RigHierarchyImportSettings"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRigEditor_StaticRegisterNativesFRigHierarchyImportSettings
{
	FScriptStruct_ControlRigEditor_StaticRegisterNativesFRigHierarchyImportSettings()
	{
		UScriptStruct::DeferCppStructOps<FRigHierarchyImportSettings>(FName(TEXT("RigHierarchyImportSettings")));
	}
} ScriptStruct_ControlRigEditor_StaticRegisterNativesFRigHierarchyImportSettings;
	struct Z_Construct_UScriptStruct_FRigHierarchyImportSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Mesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Mesh;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigHierarchyImportSettings_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Editor/SRigHierarchy.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigHierarchyImportSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigHierarchyImportSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigHierarchyImportSettings_Statics::NewProp_Mesh_MetaData[] = {
		{ "Category", "Hierachy Import" },
		{ "ModuleRelativePath", "Private/Editor/SRigHierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FRigHierarchyImportSettings_Statics::NewProp_Mesh = { "Mesh", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigHierarchyImportSettings, Mesh), Z_Construct_UClass_USkeletalMesh_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigHierarchyImportSettings_Statics::NewProp_Mesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigHierarchyImportSettings_Statics::NewProp_Mesh_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigHierarchyImportSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigHierarchyImportSettings_Statics::NewProp_Mesh,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigHierarchyImportSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRigEditor,
		nullptr,
		&NewStructOps,
		"RigHierarchyImportSettings",
		sizeof(FRigHierarchyImportSettings),
		alignof(FRigHierarchyImportSettings),
		Z_Construct_UScriptStruct_FRigHierarchyImportSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigHierarchyImportSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigHierarchyImportSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigHierarchyImportSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigHierarchyImportSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigHierarchyImportSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRigEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigHierarchyImportSettings"), sizeof(FRigHierarchyImportSettings), Get_Z_Construct_UScriptStruct_FRigHierarchyImportSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigHierarchyImportSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigHierarchyImportSettings_Hash() { return 491690177U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
