// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Public/Math/Simulation/CRSimPointContainer.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCRSimPointContainer() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FCRSimPointContainer();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FCRSimContainer();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FCRSimPoint();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FCRSimLinearSpring();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FCRSimPointForce();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FCRSimSoftCollision();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FCRSimPointConstraint();
// End Cross Module References

static_assert(std::is_polymorphic<FCRSimPointContainer>() == std::is_polymorphic<FCRSimContainer>(), "USTRUCT FCRSimPointContainer cannot be polymorphic unless super FCRSimContainer is polymorphic");

class UScriptStruct* FCRSimPointContainer::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FCRSimPointContainer_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCRSimPointContainer, Z_Construct_UPackage__Script_ControlRig(), TEXT("CRSimPointContainer"), sizeof(FCRSimPointContainer), Get_Z_Construct_UScriptStruct_FCRSimPointContainer_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FCRSimPointContainer>()
{
	return FCRSimPointContainer::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCRSimPointContainer(FCRSimPointContainer::StaticStruct, TEXT("/Script/ControlRig"), TEXT("CRSimPointContainer"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFCRSimPointContainer
{
	FScriptStruct_ControlRig_StaticRegisterNativesFCRSimPointContainer()
	{
		UScriptStruct::DeferCppStructOps<FCRSimPointContainer>(FName(TEXT("CRSimPointContainer")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFCRSimPointContainer;
	struct Z_Construct_UScriptStruct_FCRSimPointContainer_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Points_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Points_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Points;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Springs_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Springs_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Springs;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Forces_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Forces_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Forces;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CollisionVolumes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CollisionVolumes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_CollisionVolumes;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Constraints_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Constraints_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Constraints;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PreviousStep_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviousStep_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PreviousStep;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimPointContainer.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FCRSimPointContainer>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_Points_Inner = { "Points", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FCRSimPoint, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_Points_MetaData[] = {
		{ "Comment", "/**\n\x09 * The points within the simulation\n\x09 */" },
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimPointContainer.h" },
		{ "ToolTip", "The points within the simulation" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_Points = { "Points", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCRSimPointContainer, Points), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_Points_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_Points_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_Springs_Inner = { "Springs", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FCRSimLinearSpring, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_Springs_MetaData[] = {
		{ "Comment", "/**\n\x09 * The springs within the simulation\n\x09 */" },
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimPointContainer.h" },
		{ "ToolTip", "The springs within the simulation" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_Springs = { "Springs", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCRSimPointContainer, Springs), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_Springs_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_Springs_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_Forces_Inner = { "Forces", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FCRSimPointForce, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_Forces_MetaData[] = {
		{ "Comment", "/**\n\x09 * The forces to apply to the points\n\x09 */" },
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimPointContainer.h" },
		{ "ToolTip", "The forces to apply to the points" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_Forces = { "Forces", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCRSimPointContainer, Forces), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_Forces_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_Forces_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_CollisionVolumes_Inner = { "CollisionVolumes", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FCRSimSoftCollision, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_CollisionVolumes_MetaData[] = {
		{ "Comment", "/**\n     * The collision volumes for the simulation\n     */" },
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimPointContainer.h" },
		{ "ToolTip", "The collision volumes for the simulation" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_CollisionVolumes = { "CollisionVolumes", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCRSimPointContainer, CollisionVolumes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_CollisionVolumes_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_CollisionVolumes_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_Constraints_Inner = { "Constraints", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FCRSimPointConstraint, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_Constraints_MetaData[] = {
		{ "Comment", "/**\n\x09 * The constraints within the simulation\n\x09 */" },
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimPointContainer.h" },
		{ "ToolTip", "The constraints within the simulation" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_Constraints = { "Constraints", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCRSimPointContainer, Constraints), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_Constraints_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_Constraints_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_PreviousStep_Inner = { "PreviousStep", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FCRSimPoint, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_PreviousStep_MetaData[] = {
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimPointContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_PreviousStep = { "PreviousStep", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCRSimPointContainer, PreviousStep), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_PreviousStep_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_PreviousStep_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_Points_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_Points,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_Springs_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_Springs,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_Forces_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_Forces,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_CollisionVolumes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_CollisionVolumes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_Constraints_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_Constraints,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_PreviousStep_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::NewProp_PreviousStep,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FCRSimContainer,
		&NewStructOps,
		"CRSimPointContainer",
		sizeof(FCRSimPointContainer),
		alignof(FCRSimPointContainer),
		Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FCRSimPointContainer()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FCRSimPointContainer_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CRSimPointContainer"), sizeof(FCRSimPointContainer), Get_Z_Construct_UScriptStruct_FCRSimPointContainer_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FCRSimPointContainer_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCRSimPointContainer_Hash() { return 3157549833U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
