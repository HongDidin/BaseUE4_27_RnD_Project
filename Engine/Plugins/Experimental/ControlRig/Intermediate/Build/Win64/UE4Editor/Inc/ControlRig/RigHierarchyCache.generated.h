// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigHierarchyCache_generated_h
#error "RigHierarchyCache.generated.h already included, missing '#pragma once' in RigHierarchyCache.h"
#endif
#define CONTROLRIG_RigHierarchyCache_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_RigHierarchyCache_h_18_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCachedRigElement_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__Key() { return STRUCT_OFFSET(FCachedRigElement, Key); } \
	FORCEINLINE static uint32 __PPO__Index() { return STRUCT_OFFSET(FCachedRigElement, Index); } \
	FORCEINLINE static uint32 __PPO__ContainerVersion() { return STRUCT_OFFSET(FCachedRigElement, ContainerVersion); }


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FCachedRigElement>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_RigHierarchyCache_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
