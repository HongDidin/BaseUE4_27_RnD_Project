// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigUnit_WorldCollision_generated_h
#error "RigUnit_WorldCollision.generated.h already included, missing '#pragma once' in RigUnit_WorldCollision.h"
#endif
#define CONTROLRIG_RigUnit_WorldCollision_generated_h


#define FRigUnit_SphereTraceWorld_Execute() \
	void FRigUnit_SphereTraceWorld::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FVector& Start, \
		const FVector& End, \
		const ECollisionChannel Channel, \
		const float Radius, \
		bool& bHit, \
		FVector& HitLocation, \
		FVector& HitNormal, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Collision_RigUnit_WorldCollision_h_15_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FVector& Start, \
		const FVector& End, \
		const ECollisionChannel Channel, \
		const float Radius, \
		bool& bHit, \
		FVector& HitLocation, \
		FVector& HitNormal, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FVector& Start = *(FVector*)RigVMMemoryHandles[0].GetData(); \
		const FVector& End = *(FVector*)RigVMMemoryHandles[1].GetData(); \
		ECollisionChannel Channel = (ECollisionChannel)*(uint8*)RigVMMemoryHandles[2].GetData(); \
		const float Radius = *(float*)RigVMMemoryHandles[3].GetData(); \
		bool& bHit = *(bool*)RigVMMemoryHandles[4].GetData(); \
		FVector& HitLocation = *(FVector*)RigVMMemoryHandles[5].GetData(); \
		FVector& HitNormal = *(FVector*)RigVMMemoryHandles[6].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Start, \
			End, \
			Channel, \
			Radius, \
			bHit, \
			HitLocation, \
			HitNormal, \
			Context \
		); \
	} \
	typedef FRigUnit Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_SphereTraceWorld>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Collision_RigUnit_WorldCollision_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
