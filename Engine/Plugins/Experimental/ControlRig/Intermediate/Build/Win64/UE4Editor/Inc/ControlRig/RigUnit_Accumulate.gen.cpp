// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Simulation/RigUnit_Accumulate.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_Accumulate() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_SimBase();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FQuat();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_AccumulateVectorRange>() == std::is_polymorphic<FRigUnit_SimBase>(), "USTRUCT FRigUnit_AccumulateVectorRange cannot be polymorphic unless super FRigUnit_SimBase is polymorphic");

class UScriptStruct* FRigUnit_AccumulateVectorRange::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_AccumulateVectorRange"), sizeof(FRigUnit_AccumulateVectorRange), Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_AccumulateVectorRange::Execute"), &FRigUnit_AccumulateVectorRange::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_AccumulateVectorRange>()
{
	return FRigUnit_AccumulateVectorRange::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_AccumulateVectorRange(FRigUnit_AccumulateVectorRange::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_AccumulateVectorRange"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AccumulateVectorRange
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AccumulateVectorRange()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_AccumulateVectorRange>(FName(TEXT("RigUnit_AccumulateVectorRange")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AccumulateVectorRange;
	struct Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Minimum_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Minimum;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Maximum_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Maximum;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AccumulatedMinimum_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AccumulatedMinimum;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AccumulatedMaximum_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AccumulatedMaximum;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Accumulates the min and max values over time\n */" },
		{ "DisplayName", "Accumulate Range (Vector)" },
		{ "Keywords", "Range" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
		{ "PrototypeName", "AccumulateRange" },
		{ "ToolTip", "Accumulates the min and max values over time" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_AccumulateVectorRange>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange_Statics::NewProp_Value_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateVectorRange, Value), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange_Statics::NewProp_Minimum_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange_Statics::NewProp_Minimum = { "Minimum", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateVectorRange, Minimum), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange_Statics::NewProp_Minimum_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange_Statics::NewProp_Minimum_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange_Statics::NewProp_Maximum_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange_Statics::NewProp_Maximum = { "Maximum", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateVectorRange, Maximum), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange_Statics::NewProp_Maximum_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange_Statics::NewProp_Maximum_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange_Statics::NewProp_AccumulatedMinimum_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange_Statics::NewProp_AccumulatedMinimum = { "AccumulatedMinimum", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateVectorRange, AccumulatedMinimum), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange_Statics::NewProp_AccumulatedMinimum_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange_Statics::NewProp_AccumulatedMinimum_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange_Statics::NewProp_AccumulatedMaximum_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange_Statics::NewProp_AccumulatedMaximum = { "AccumulatedMaximum", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateVectorRange, AccumulatedMaximum), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange_Statics::NewProp_AccumulatedMaximum_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange_Statics::NewProp_AccumulatedMaximum_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange_Statics::NewProp_Minimum,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange_Statics::NewProp_Maximum,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange_Statics::NewProp_AccumulatedMinimum,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange_Statics::NewProp_AccumulatedMaximum,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_SimBase,
		&NewStructOps,
		"RigUnit_AccumulateVectorRange",
		sizeof(FRigUnit_AccumulateVectorRange),
		alignof(FRigUnit_AccumulateVectorRange),
		Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_AccumulateVectorRange"), sizeof(FRigUnit_AccumulateVectorRange), Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange_Hash() { return 296259530U; }

void FRigUnit_AccumulateVectorRange::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Minimum,
		Maximum,
		AccumulatedMinimum,
		AccumulatedMaximum,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_AccumulateFloatRange>() == std::is_polymorphic<FRigUnit_SimBase>(), "USTRUCT FRigUnit_AccumulateFloatRange cannot be polymorphic unless super FRigUnit_SimBase is polymorphic");

class UScriptStruct* FRigUnit_AccumulateFloatRange::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_AccumulateFloatRange"), sizeof(FRigUnit_AccumulateFloatRange), Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_AccumulateFloatRange::Execute"), &FRigUnit_AccumulateFloatRange::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_AccumulateFloatRange>()
{
	return FRigUnit_AccumulateFloatRange::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_AccumulateFloatRange(FRigUnit_AccumulateFloatRange::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_AccumulateFloatRange"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AccumulateFloatRange
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AccumulateFloatRange()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_AccumulateFloatRange>(FName(TEXT("RigUnit_AccumulateFloatRange")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AccumulateFloatRange;
	struct Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Minimum_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Minimum;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Maximum_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Maximum;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AccumulatedMinimum_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AccumulatedMinimum;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AccumulatedMaximum_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AccumulatedMaximum;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Accumulates the min and max values over time\n */" },
		{ "DisplayName", "Accumulate Range (Float)" },
		{ "Keywords", "Range" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
		{ "PrototypeName", "AccumulateRange" },
		{ "ToolTip", "Accumulates the min and max values over time" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_AccumulateFloatRange>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange_Statics::NewProp_Value_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateFloatRange, Value), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange_Statics::NewProp_Minimum_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange_Statics::NewProp_Minimum = { "Minimum", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateFloatRange, Minimum), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange_Statics::NewProp_Minimum_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange_Statics::NewProp_Minimum_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange_Statics::NewProp_Maximum_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange_Statics::NewProp_Maximum = { "Maximum", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateFloatRange, Maximum), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange_Statics::NewProp_Maximum_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange_Statics::NewProp_Maximum_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange_Statics::NewProp_AccumulatedMinimum_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange_Statics::NewProp_AccumulatedMinimum = { "AccumulatedMinimum", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateFloatRange, AccumulatedMinimum), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange_Statics::NewProp_AccumulatedMinimum_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange_Statics::NewProp_AccumulatedMinimum_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange_Statics::NewProp_AccumulatedMaximum_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange_Statics::NewProp_AccumulatedMaximum = { "AccumulatedMaximum", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateFloatRange, AccumulatedMaximum), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange_Statics::NewProp_AccumulatedMaximum_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange_Statics::NewProp_AccumulatedMaximum_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange_Statics::NewProp_Minimum,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange_Statics::NewProp_Maximum,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange_Statics::NewProp_AccumulatedMinimum,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange_Statics::NewProp_AccumulatedMaximum,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_SimBase,
		&NewStructOps,
		"RigUnit_AccumulateFloatRange",
		sizeof(FRigUnit_AccumulateFloatRange),
		alignof(FRigUnit_AccumulateFloatRange),
		Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_AccumulateFloatRange"), sizeof(FRigUnit_AccumulateFloatRange), Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange_Hash() { return 3711987034U; }

void FRigUnit_AccumulateFloatRange::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Minimum,
		Maximum,
		AccumulatedMinimum,
		AccumulatedMaximum,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_AccumulateTransformLerp>() == std::is_polymorphic<FRigUnit_SimBase>(), "USTRUCT FRigUnit_AccumulateTransformLerp cannot be polymorphic unless super FRigUnit_SimBase is polymorphic");

class UScriptStruct* FRigUnit_AccumulateTransformLerp::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_AccumulateTransformLerp"), sizeof(FRigUnit_AccumulateTransformLerp), Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_AccumulateTransformLerp::Execute"), &FRigUnit_AccumulateTransformLerp::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_AccumulateTransformLerp>()
{
	return FRigUnit_AccumulateTransformLerp::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_AccumulateTransformLerp(FRigUnit_AccumulateTransformLerp::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_AccumulateTransformLerp"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AccumulateTransformLerp
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AccumulateTransformLerp()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_AccumulateTransformLerp>(FName(TEXT("RigUnit_AccumulateTransformLerp")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AccumulateTransformLerp;
	struct Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TargetValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InitialValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InitialValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Blend_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Blend;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIntegrateDeltaTime_MetaData[];
#endif
		static void NewProp_bIntegrateDeltaTime_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIntegrateDeltaTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Result;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AccumulatedValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AccumulatedValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Interpolates two transforms over time over and over again\n */" },
		{ "DisplayName", "Accumulate Lerp (Transform)" },
		{ "Keywords", "Simulate,Ramp" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
		{ "PrototypeName", "AccumulateLerp" },
		{ "ToolTip", "Interpolates two transforms over time over and over again" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_AccumulateTransformLerp>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics::NewProp_TargetValue_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics::NewProp_TargetValue = { "TargetValue", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateTransformLerp, TargetValue), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics::NewProp_TargetValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics::NewProp_TargetValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics::NewProp_InitialValue_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics::NewProp_InitialValue = { "InitialValue", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateTransformLerp, InitialValue), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics::NewProp_InitialValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics::NewProp_InitialValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics::NewProp_Blend_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics::NewProp_Blend = { "Blend", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateTransformLerp, Blend), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics::NewProp_Blend_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics::NewProp_Blend_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics::NewProp_bIntegrateDeltaTime_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics::NewProp_bIntegrateDeltaTime_SetBit(void* Obj)
	{
		((FRigUnit_AccumulateTransformLerp*)Obj)->bIntegrateDeltaTime = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics::NewProp_bIntegrateDeltaTime = { "bIntegrateDeltaTime", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_AccumulateTransformLerp), &Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics::NewProp_bIntegrateDeltaTime_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics::NewProp_bIntegrateDeltaTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics::NewProp_bIntegrateDeltaTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateTransformLerp, Result), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics::NewProp_Result_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics::NewProp_AccumulatedValue_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics::NewProp_AccumulatedValue = { "AccumulatedValue", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateTransformLerp, AccumulatedValue), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics::NewProp_AccumulatedValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics::NewProp_AccumulatedValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics::NewProp_TargetValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics::NewProp_InitialValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics::NewProp_Blend,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics::NewProp_bIntegrateDeltaTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics::NewProp_Result,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics::NewProp_AccumulatedValue,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_SimBase,
		&NewStructOps,
		"RigUnit_AccumulateTransformLerp",
		sizeof(FRigUnit_AccumulateTransformLerp),
		alignof(FRigUnit_AccumulateTransformLerp),
		Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_AccumulateTransformLerp"), sizeof(FRigUnit_AccumulateTransformLerp), Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Hash() { return 1738513463U; }

void FRigUnit_AccumulateTransformLerp::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		TargetValue,
		InitialValue,
		Blend,
		bIntegrateDeltaTime,
		Result,
		AccumulatedValue,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_AccumulateQuatLerp>() == std::is_polymorphic<FRigUnit_SimBase>(), "USTRUCT FRigUnit_AccumulateQuatLerp cannot be polymorphic unless super FRigUnit_SimBase is polymorphic");

class UScriptStruct* FRigUnit_AccumulateQuatLerp::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_AccumulateQuatLerp"), sizeof(FRigUnit_AccumulateQuatLerp), Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_AccumulateQuatLerp::Execute"), &FRigUnit_AccumulateQuatLerp::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_AccumulateQuatLerp>()
{
	return FRigUnit_AccumulateQuatLerp::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_AccumulateQuatLerp(FRigUnit_AccumulateQuatLerp::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_AccumulateQuatLerp"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AccumulateQuatLerp
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AccumulateQuatLerp()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_AccumulateQuatLerp>(FName(TEXT("RigUnit_AccumulateQuatLerp")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AccumulateQuatLerp;
	struct Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TargetValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InitialValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InitialValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Blend_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Blend;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIntegrateDeltaTime_MetaData[];
#endif
		static void NewProp_bIntegrateDeltaTime_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIntegrateDeltaTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Result;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AccumulatedValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AccumulatedValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Interpolates two quaternions over time over and over again\n */" },
		{ "DisplayName", "Accumulate Lerp (Quaternion)" },
		{ "Keywords", "Simulate,Ramp" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
		{ "PrototypeName", "AccumulateLerp" },
		{ "ToolTip", "Interpolates two quaternions over time over and over again" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_AccumulateQuatLerp>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics::NewProp_TargetValue_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics::NewProp_TargetValue = { "TargetValue", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateQuatLerp, TargetValue), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics::NewProp_TargetValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics::NewProp_TargetValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics::NewProp_InitialValue_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics::NewProp_InitialValue = { "InitialValue", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateQuatLerp, InitialValue), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics::NewProp_InitialValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics::NewProp_InitialValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics::NewProp_Blend_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics::NewProp_Blend = { "Blend", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateQuatLerp, Blend), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics::NewProp_Blend_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics::NewProp_Blend_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics::NewProp_bIntegrateDeltaTime_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics::NewProp_bIntegrateDeltaTime_SetBit(void* Obj)
	{
		((FRigUnit_AccumulateQuatLerp*)Obj)->bIntegrateDeltaTime = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics::NewProp_bIntegrateDeltaTime = { "bIntegrateDeltaTime", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_AccumulateQuatLerp), &Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics::NewProp_bIntegrateDeltaTime_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics::NewProp_bIntegrateDeltaTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics::NewProp_bIntegrateDeltaTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateQuatLerp, Result), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics::NewProp_Result_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics::NewProp_AccumulatedValue_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics::NewProp_AccumulatedValue = { "AccumulatedValue", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateQuatLerp, AccumulatedValue), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics::NewProp_AccumulatedValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics::NewProp_AccumulatedValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics::NewProp_TargetValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics::NewProp_InitialValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics::NewProp_Blend,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics::NewProp_bIntegrateDeltaTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics::NewProp_Result,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics::NewProp_AccumulatedValue,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_SimBase,
		&NewStructOps,
		"RigUnit_AccumulateQuatLerp",
		sizeof(FRigUnit_AccumulateQuatLerp),
		alignof(FRigUnit_AccumulateQuatLerp),
		Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_AccumulateQuatLerp"), sizeof(FRigUnit_AccumulateQuatLerp), Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Hash() { return 1267698625U; }

void FRigUnit_AccumulateQuatLerp::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		TargetValue,
		InitialValue,
		Blend,
		bIntegrateDeltaTime,
		Result,
		AccumulatedValue,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_AccumulateVectorLerp>() == std::is_polymorphic<FRigUnit_SimBase>(), "USTRUCT FRigUnit_AccumulateVectorLerp cannot be polymorphic unless super FRigUnit_SimBase is polymorphic");

class UScriptStruct* FRigUnit_AccumulateVectorLerp::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_AccumulateVectorLerp"), sizeof(FRigUnit_AccumulateVectorLerp), Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_AccumulateVectorLerp::Execute"), &FRigUnit_AccumulateVectorLerp::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_AccumulateVectorLerp>()
{
	return FRigUnit_AccumulateVectorLerp::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_AccumulateVectorLerp(FRigUnit_AccumulateVectorLerp::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_AccumulateVectorLerp"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AccumulateVectorLerp
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AccumulateVectorLerp()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_AccumulateVectorLerp>(FName(TEXT("RigUnit_AccumulateVectorLerp")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AccumulateVectorLerp;
	struct Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TargetValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InitialValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InitialValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Blend_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Blend;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIntegrateDeltaTime_MetaData[];
#endif
		static void NewProp_bIntegrateDeltaTime_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIntegrateDeltaTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Result;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AccumulatedValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AccumulatedValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Interpolates two vectors over time over and over again\n */" },
		{ "DisplayName", "Accumulate Lerp (Vector)" },
		{ "Keywords", "Simulate,Ramp" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
		{ "PrototypeName", "AccumulateLerp" },
		{ "ToolTip", "Interpolates two vectors over time over and over again" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_AccumulateVectorLerp>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics::NewProp_TargetValue_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics::NewProp_TargetValue = { "TargetValue", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateVectorLerp, TargetValue), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics::NewProp_TargetValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics::NewProp_TargetValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics::NewProp_InitialValue_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics::NewProp_InitialValue = { "InitialValue", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateVectorLerp, InitialValue), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics::NewProp_InitialValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics::NewProp_InitialValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics::NewProp_Blend_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics::NewProp_Blend = { "Blend", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateVectorLerp, Blend), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics::NewProp_Blend_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics::NewProp_Blend_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics::NewProp_bIntegrateDeltaTime_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics::NewProp_bIntegrateDeltaTime_SetBit(void* Obj)
	{
		((FRigUnit_AccumulateVectorLerp*)Obj)->bIntegrateDeltaTime = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics::NewProp_bIntegrateDeltaTime = { "bIntegrateDeltaTime", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_AccumulateVectorLerp), &Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics::NewProp_bIntegrateDeltaTime_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics::NewProp_bIntegrateDeltaTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics::NewProp_bIntegrateDeltaTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateVectorLerp, Result), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics::NewProp_Result_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics::NewProp_AccumulatedValue_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics::NewProp_AccumulatedValue = { "AccumulatedValue", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateVectorLerp, AccumulatedValue), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics::NewProp_AccumulatedValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics::NewProp_AccumulatedValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics::NewProp_TargetValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics::NewProp_InitialValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics::NewProp_Blend,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics::NewProp_bIntegrateDeltaTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics::NewProp_Result,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics::NewProp_AccumulatedValue,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_SimBase,
		&NewStructOps,
		"RigUnit_AccumulateVectorLerp",
		sizeof(FRigUnit_AccumulateVectorLerp),
		alignof(FRigUnit_AccumulateVectorLerp),
		Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_AccumulateVectorLerp"), sizeof(FRigUnit_AccumulateVectorLerp), Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Hash() { return 671712286U; }

void FRigUnit_AccumulateVectorLerp::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		TargetValue,
		InitialValue,
		Blend,
		bIntegrateDeltaTime,
		Result,
		AccumulatedValue,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_AccumulateFloatLerp>() == std::is_polymorphic<FRigUnit_SimBase>(), "USTRUCT FRigUnit_AccumulateFloatLerp cannot be polymorphic unless super FRigUnit_SimBase is polymorphic");

class UScriptStruct* FRigUnit_AccumulateFloatLerp::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_AccumulateFloatLerp"), sizeof(FRigUnit_AccumulateFloatLerp), Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_AccumulateFloatLerp::Execute"), &FRigUnit_AccumulateFloatLerp::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_AccumulateFloatLerp>()
{
	return FRigUnit_AccumulateFloatLerp::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_AccumulateFloatLerp(FRigUnit_AccumulateFloatLerp::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_AccumulateFloatLerp"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AccumulateFloatLerp
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AccumulateFloatLerp()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_AccumulateFloatLerp>(FName(TEXT("RigUnit_AccumulateFloatLerp")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AccumulateFloatLerp;
	struct Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TargetValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InitialValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InitialValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Blend_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Blend;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIntegrateDeltaTime_MetaData[];
#endif
		static void NewProp_bIntegrateDeltaTime_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIntegrateDeltaTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Result;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AccumulatedValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AccumulatedValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Interpolates two values over time over and over again\n */" },
		{ "DisplayName", "Accumulate Lerp (Float)" },
		{ "Keywords", "Simulate,Ramp" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
		{ "PrototypeName", "AccumulateLerp" },
		{ "ToolTip", "Interpolates two values over time over and over again" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_AccumulateFloatLerp>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics::NewProp_TargetValue_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics::NewProp_TargetValue = { "TargetValue", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateFloatLerp, TargetValue), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics::NewProp_TargetValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics::NewProp_TargetValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics::NewProp_InitialValue_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics::NewProp_InitialValue = { "InitialValue", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateFloatLerp, InitialValue), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics::NewProp_InitialValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics::NewProp_InitialValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics::NewProp_Blend_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics::NewProp_Blend = { "Blend", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateFloatLerp, Blend), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics::NewProp_Blend_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics::NewProp_Blend_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics::NewProp_bIntegrateDeltaTime_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics::NewProp_bIntegrateDeltaTime_SetBit(void* Obj)
	{
		((FRigUnit_AccumulateFloatLerp*)Obj)->bIntegrateDeltaTime = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics::NewProp_bIntegrateDeltaTime = { "bIntegrateDeltaTime", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_AccumulateFloatLerp), &Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics::NewProp_bIntegrateDeltaTime_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics::NewProp_bIntegrateDeltaTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics::NewProp_bIntegrateDeltaTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateFloatLerp, Result), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics::NewProp_Result_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics::NewProp_AccumulatedValue_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics::NewProp_AccumulatedValue = { "AccumulatedValue", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateFloatLerp, AccumulatedValue), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics::NewProp_AccumulatedValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics::NewProp_AccumulatedValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics::NewProp_TargetValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics::NewProp_InitialValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics::NewProp_Blend,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics::NewProp_bIntegrateDeltaTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics::NewProp_Result,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics::NewProp_AccumulatedValue,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_SimBase,
		&NewStructOps,
		"RigUnit_AccumulateFloatLerp",
		sizeof(FRigUnit_AccumulateFloatLerp),
		alignof(FRigUnit_AccumulateFloatLerp),
		Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_AccumulateFloatLerp"), sizeof(FRigUnit_AccumulateFloatLerp), Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Hash() { return 3084835137U; }

void FRigUnit_AccumulateFloatLerp::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		TargetValue,
		InitialValue,
		Blend,
		bIntegrateDeltaTime,
		Result,
		AccumulatedValue,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_AccumulateTransformMul>() == std::is_polymorphic<FRigUnit_SimBase>(), "USTRUCT FRigUnit_AccumulateTransformMul cannot be polymorphic unless super FRigUnit_SimBase is polymorphic");

class UScriptStruct* FRigUnit_AccumulateTransformMul::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_AccumulateTransformMul"), sizeof(FRigUnit_AccumulateTransformMul), Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_AccumulateTransformMul::Execute"), &FRigUnit_AccumulateTransformMul::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_AccumulateTransformMul>()
{
	return FRigUnit_AccumulateTransformMul::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_AccumulateTransformMul(FRigUnit_AccumulateTransformMul::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_AccumulateTransformMul"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AccumulateTransformMul
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AccumulateTransformMul()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_AccumulateTransformMul>(FName(TEXT("RigUnit_AccumulateTransformMul")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AccumulateTransformMul;
	struct Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Multiplier_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Multiplier;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InitialValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InitialValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bFlipOrder_MetaData[];
#endif
		static void NewProp_bFlipOrder_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFlipOrder;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIntegrateDeltaTime_MetaData[];
#endif
		static void NewProp_bIntegrateDeltaTime_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIntegrateDeltaTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Result;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AccumulatedValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AccumulatedValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Multiplies a transform over time over and over again\n */" },
		{ "DisplayName", "Accumulate Mul (Transform)" },
		{ "Keywords", "Simulate,**" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
		{ "PrototypeName", "AccumulateMul" },
		{ "ToolTip", "Multiplies a transform over time over and over again" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_AccumulateTransformMul>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::NewProp_Multiplier_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::NewProp_Multiplier = { "Multiplier", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateTransformMul, Multiplier), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::NewProp_Multiplier_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::NewProp_Multiplier_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::NewProp_InitialValue_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::NewProp_InitialValue = { "InitialValue", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateTransformMul, InitialValue), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::NewProp_InitialValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::NewProp_InitialValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::NewProp_bFlipOrder_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::NewProp_bFlipOrder_SetBit(void* Obj)
	{
		((FRigUnit_AccumulateTransformMul*)Obj)->bFlipOrder = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::NewProp_bFlipOrder = { "bFlipOrder", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_AccumulateTransformMul), &Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::NewProp_bFlipOrder_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::NewProp_bFlipOrder_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::NewProp_bFlipOrder_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::NewProp_bIntegrateDeltaTime_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::NewProp_bIntegrateDeltaTime_SetBit(void* Obj)
	{
		((FRigUnit_AccumulateTransformMul*)Obj)->bIntegrateDeltaTime = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::NewProp_bIntegrateDeltaTime = { "bIntegrateDeltaTime", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_AccumulateTransformMul), &Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::NewProp_bIntegrateDeltaTime_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::NewProp_bIntegrateDeltaTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::NewProp_bIntegrateDeltaTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateTransformMul, Result), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::NewProp_Result_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::NewProp_AccumulatedValue_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::NewProp_AccumulatedValue = { "AccumulatedValue", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateTransformMul, AccumulatedValue), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::NewProp_AccumulatedValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::NewProp_AccumulatedValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::NewProp_Multiplier,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::NewProp_InitialValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::NewProp_bFlipOrder,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::NewProp_bIntegrateDeltaTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::NewProp_Result,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::NewProp_AccumulatedValue,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_SimBase,
		&NewStructOps,
		"RigUnit_AccumulateTransformMul",
		sizeof(FRigUnit_AccumulateTransformMul),
		alignof(FRigUnit_AccumulateTransformMul),
		Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_AccumulateTransformMul"), sizeof(FRigUnit_AccumulateTransformMul), Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Hash() { return 1466523504U; }

void FRigUnit_AccumulateTransformMul::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Multiplier,
		InitialValue,
		bFlipOrder,
		bIntegrateDeltaTime,
		Result,
		AccumulatedValue,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_AccumulateQuatMul>() == std::is_polymorphic<FRigUnit_SimBase>(), "USTRUCT FRigUnit_AccumulateQuatMul cannot be polymorphic unless super FRigUnit_SimBase is polymorphic");

class UScriptStruct* FRigUnit_AccumulateQuatMul::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_AccumulateQuatMul"), sizeof(FRigUnit_AccumulateQuatMul), Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_AccumulateQuatMul::Execute"), &FRigUnit_AccumulateQuatMul::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_AccumulateQuatMul>()
{
	return FRigUnit_AccumulateQuatMul::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_AccumulateQuatMul(FRigUnit_AccumulateQuatMul::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_AccumulateQuatMul"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AccumulateQuatMul
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AccumulateQuatMul()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_AccumulateQuatMul>(FName(TEXT("RigUnit_AccumulateQuatMul")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AccumulateQuatMul;
	struct Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Multiplier_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Multiplier;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InitialValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InitialValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bFlipOrder_MetaData[];
#endif
		static void NewProp_bFlipOrder_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFlipOrder;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIntegrateDeltaTime_MetaData[];
#endif
		static void NewProp_bIntegrateDeltaTime_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIntegrateDeltaTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Result;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AccumulatedValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AccumulatedValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Multiplies a quaternion over time over and over again\n */" },
		{ "DisplayName", "Accumulate Mul (Quaternion)" },
		{ "Keywords", "Simulate,**" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
		{ "PrototypeName", "AccumulateMul" },
		{ "ToolTip", "Multiplies a quaternion over time over and over again" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_AccumulateQuatMul>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::NewProp_Multiplier_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::NewProp_Multiplier = { "Multiplier", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateQuatMul, Multiplier), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::NewProp_Multiplier_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::NewProp_Multiplier_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::NewProp_InitialValue_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::NewProp_InitialValue = { "InitialValue", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateQuatMul, InitialValue), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::NewProp_InitialValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::NewProp_InitialValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::NewProp_bFlipOrder_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::NewProp_bFlipOrder_SetBit(void* Obj)
	{
		((FRigUnit_AccumulateQuatMul*)Obj)->bFlipOrder = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::NewProp_bFlipOrder = { "bFlipOrder", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_AccumulateQuatMul), &Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::NewProp_bFlipOrder_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::NewProp_bFlipOrder_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::NewProp_bFlipOrder_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::NewProp_bIntegrateDeltaTime_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::NewProp_bIntegrateDeltaTime_SetBit(void* Obj)
	{
		((FRigUnit_AccumulateQuatMul*)Obj)->bIntegrateDeltaTime = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::NewProp_bIntegrateDeltaTime = { "bIntegrateDeltaTime", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_AccumulateQuatMul), &Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::NewProp_bIntegrateDeltaTime_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::NewProp_bIntegrateDeltaTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::NewProp_bIntegrateDeltaTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateQuatMul, Result), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::NewProp_Result_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::NewProp_AccumulatedValue_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::NewProp_AccumulatedValue = { "AccumulatedValue", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateQuatMul, AccumulatedValue), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::NewProp_AccumulatedValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::NewProp_AccumulatedValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::NewProp_Multiplier,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::NewProp_InitialValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::NewProp_bFlipOrder,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::NewProp_bIntegrateDeltaTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::NewProp_Result,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::NewProp_AccumulatedValue,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_SimBase,
		&NewStructOps,
		"RigUnit_AccumulateQuatMul",
		sizeof(FRigUnit_AccumulateQuatMul),
		alignof(FRigUnit_AccumulateQuatMul),
		Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_AccumulateQuatMul"), sizeof(FRigUnit_AccumulateQuatMul), Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Hash() { return 3601147261U; }

void FRigUnit_AccumulateQuatMul::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Multiplier,
		InitialValue,
		bFlipOrder,
		bIntegrateDeltaTime,
		Result,
		AccumulatedValue,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_AccumulateVectorMul>() == std::is_polymorphic<FRigUnit_SimBase>(), "USTRUCT FRigUnit_AccumulateVectorMul cannot be polymorphic unless super FRigUnit_SimBase is polymorphic");

class UScriptStruct* FRigUnit_AccumulateVectorMul::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_AccumulateVectorMul"), sizeof(FRigUnit_AccumulateVectorMul), Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_AccumulateVectorMul::Execute"), &FRigUnit_AccumulateVectorMul::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_AccumulateVectorMul>()
{
	return FRigUnit_AccumulateVectorMul::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_AccumulateVectorMul(FRigUnit_AccumulateVectorMul::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_AccumulateVectorMul"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AccumulateVectorMul
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AccumulateVectorMul()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_AccumulateVectorMul>(FName(TEXT("RigUnit_AccumulateVectorMul")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AccumulateVectorMul;
	struct Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Multiplier_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Multiplier;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InitialValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InitialValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIntegrateDeltaTime_MetaData[];
#endif
		static void NewProp_bIntegrateDeltaTime_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIntegrateDeltaTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Result;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AccumulatedValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AccumulatedValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Multiplies a vector over time over and over again\n */" },
		{ "DisplayName", "Accumulate Mul (Vector)" },
		{ "Keywords", "Simulate,**" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
		{ "PrototypeName", "AccumulateMul" },
		{ "ToolTip", "Multiplies a vector over time over and over again" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_AccumulateVectorMul>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Statics::NewProp_Multiplier_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Statics::NewProp_Multiplier = { "Multiplier", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateVectorMul, Multiplier), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Statics::NewProp_Multiplier_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Statics::NewProp_Multiplier_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Statics::NewProp_InitialValue_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Statics::NewProp_InitialValue = { "InitialValue", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateVectorMul, InitialValue), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Statics::NewProp_InitialValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Statics::NewProp_InitialValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Statics::NewProp_bIntegrateDeltaTime_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Statics::NewProp_bIntegrateDeltaTime_SetBit(void* Obj)
	{
		((FRigUnit_AccumulateVectorMul*)Obj)->bIntegrateDeltaTime = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Statics::NewProp_bIntegrateDeltaTime = { "bIntegrateDeltaTime", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_AccumulateVectorMul), &Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Statics::NewProp_bIntegrateDeltaTime_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Statics::NewProp_bIntegrateDeltaTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Statics::NewProp_bIntegrateDeltaTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateVectorMul, Result), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Statics::NewProp_Result_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Statics::NewProp_AccumulatedValue_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Statics::NewProp_AccumulatedValue = { "AccumulatedValue", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateVectorMul, AccumulatedValue), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Statics::NewProp_AccumulatedValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Statics::NewProp_AccumulatedValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Statics::NewProp_Multiplier,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Statics::NewProp_InitialValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Statics::NewProp_bIntegrateDeltaTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Statics::NewProp_Result,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Statics::NewProp_AccumulatedValue,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_SimBase,
		&NewStructOps,
		"RigUnit_AccumulateVectorMul",
		sizeof(FRigUnit_AccumulateVectorMul),
		alignof(FRigUnit_AccumulateVectorMul),
		Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_AccumulateVectorMul"), sizeof(FRigUnit_AccumulateVectorMul), Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Hash() { return 3227705580U; }

void FRigUnit_AccumulateVectorMul::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Multiplier,
		InitialValue,
		bIntegrateDeltaTime,
		Result,
		AccumulatedValue,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_AccumulateFloatMul>() == std::is_polymorphic<FRigUnit_SimBase>(), "USTRUCT FRigUnit_AccumulateFloatMul cannot be polymorphic unless super FRigUnit_SimBase is polymorphic");

class UScriptStruct* FRigUnit_AccumulateFloatMul::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_AccumulateFloatMul"), sizeof(FRigUnit_AccumulateFloatMul), Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_AccumulateFloatMul::Execute"), &FRigUnit_AccumulateFloatMul::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_AccumulateFloatMul>()
{
	return FRigUnit_AccumulateFloatMul::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_AccumulateFloatMul(FRigUnit_AccumulateFloatMul::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_AccumulateFloatMul"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AccumulateFloatMul
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AccumulateFloatMul()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_AccumulateFloatMul>(FName(TEXT("RigUnit_AccumulateFloatMul")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AccumulateFloatMul;
	struct Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Multiplier_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Multiplier;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InitialValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InitialValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIntegrateDeltaTime_MetaData[];
#endif
		static void NewProp_bIntegrateDeltaTime_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIntegrateDeltaTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Result;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AccumulatedValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AccumulatedValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Multiplies a value over time over and over again\n */" },
		{ "DisplayName", "Accumulate Mul (Float)" },
		{ "Keywords", "Simulate,**" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
		{ "PrototypeName", "AccumulateMul" },
		{ "ToolTip", "Multiplies a value over time over and over again" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_AccumulateFloatMul>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Statics::NewProp_Multiplier_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Statics::NewProp_Multiplier = { "Multiplier", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateFloatMul, Multiplier), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Statics::NewProp_Multiplier_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Statics::NewProp_Multiplier_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Statics::NewProp_InitialValue_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Statics::NewProp_InitialValue = { "InitialValue", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateFloatMul, InitialValue), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Statics::NewProp_InitialValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Statics::NewProp_InitialValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Statics::NewProp_bIntegrateDeltaTime_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Statics::NewProp_bIntegrateDeltaTime_SetBit(void* Obj)
	{
		((FRigUnit_AccumulateFloatMul*)Obj)->bIntegrateDeltaTime = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Statics::NewProp_bIntegrateDeltaTime = { "bIntegrateDeltaTime", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_AccumulateFloatMul), &Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Statics::NewProp_bIntegrateDeltaTime_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Statics::NewProp_bIntegrateDeltaTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Statics::NewProp_bIntegrateDeltaTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateFloatMul, Result), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Statics::NewProp_Result_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Statics::NewProp_AccumulatedValue_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Statics::NewProp_AccumulatedValue = { "AccumulatedValue", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateFloatMul, AccumulatedValue), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Statics::NewProp_AccumulatedValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Statics::NewProp_AccumulatedValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Statics::NewProp_Multiplier,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Statics::NewProp_InitialValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Statics::NewProp_bIntegrateDeltaTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Statics::NewProp_Result,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Statics::NewProp_AccumulatedValue,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_SimBase,
		&NewStructOps,
		"RigUnit_AccumulateFloatMul",
		sizeof(FRigUnit_AccumulateFloatMul),
		alignof(FRigUnit_AccumulateFloatMul),
		Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_AccumulateFloatMul"), sizeof(FRigUnit_AccumulateFloatMul), Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Hash() { return 1789871505U; }

void FRigUnit_AccumulateFloatMul::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Multiplier,
		InitialValue,
		bIntegrateDeltaTime,
		Result,
		AccumulatedValue,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_AccumulateVectorAdd>() == std::is_polymorphic<FRigUnit_SimBase>(), "USTRUCT FRigUnit_AccumulateVectorAdd cannot be polymorphic unless super FRigUnit_SimBase is polymorphic");

class UScriptStruct* FRigUnit_AccumulateVectorAdd::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_AccumulateVectorAdd"), sizeof(FRigUnit_AccumulateVectorAdd), Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_AccumulateVectorAdd::Execute"), &FRigUnit_AccumulateVectorAdd::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_AccumulateVectorAdd>()
{
	return FRigUnit_AccumulateVectorAdd::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_AccumulateVectorAdd(FRigUnit_AccumulateVectorAdd::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_AccumulateVectorAdd"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AccumulateVectorAdd
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AccumulateVectorAdd()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_AccumulateVectorAdd>(FName(TEXT("RigUnit_AccumulateVectorAdd")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AccumulateVectorAdd;
	struct Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Increment_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Increment;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InitialValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InitialValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIntegrateDeltaTime_MetaData[];
#endif
		static void NewProp_bIntegrateDeltaTime_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIntegrateDeltaTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Result;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AccumulatedValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AccumulatedValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Adds a vector over time over and over again\n */" },
		{ "DisplayName", "Accumulate Add (Vector)" },
		{ "Keywords", "Simulate,++" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
		{ "PrototypeName", "AccumulateAdd" },
		{ "ToolTip", "Adds a vector over time over and over again" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_AccumulateVectorAdd>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Statics::NewProp_Increment_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Statics::NewProp_Increment = { "Increment", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateVectorAdd, Increment), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Statics::NewProp_Increment_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Statics::NewProp_Increment_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Statics::NewProp_InitialValue_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Statics::NewProp_InitialValue = { "InitialValue", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateVectorAdd, InitialValue), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Statics::NewProp_InitialValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Statics::NewProp_InitialValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Statics::NewProp_bIntegrateDeltaTime_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Statics::NewProp_bIntegrateDeltaTime_SetBit(void* Obj)
	{
		((FRigUnit_AccumulateVectorAdd*)Obj)->bIntegrateDeltaTime = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Statics::NewProp_bIntegrateDeltaTime = { "bIntegrateDeltaTime", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_AccumulateVectorAdd), &Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Statics::NewProp_bIntegrateDeltaTime_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Statics::NewProp_bIntegrateDeltaTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Statics::NewProp_bIntegrateDeltaTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateVectorAdd, Result), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Statics::NewProp_Result_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Statics::NewProp_AccumulatedValue_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Statics::NewProp_AccumulatedValue = { "AccumulatedValue", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateVectorAdd, AccumulatedValue), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Statics::NewProp_AccumulatedValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Statics::NewProp_AccumulatedValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Statics::NewProp_Increment,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Statics::NewProp_InitialValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Statics::NewProp_bIntegrateDeltaTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Statics::NewProp_Result,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Statics::NewProp_AccumulatedValue,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_SimBase,
		&NewStructOps,
		"RigUnit_AccumulateVectorAdd",
		sizeof(FRigUnit_AccumulateVectorAdd),
		alignof(FRigUnit_AccumulateVectorAdd),
		Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_AccumulateVectorAdd"), sizeof(FRigUnit_AccumulateVectorAdd), Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Hash() { return 2750354599U; }

void FRigUnit_AccumulateVectorAdd::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Increment,
		InitialValue,
		bIntegrateDeltaTime,
		Result,
		AccumulatedValue,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_AccumulateFloatAdd>() == std::is_polymorphic<FRigUnit_SimBase>(), "USTRUCT FRigUnit_AccumulateFloatAdd cannot be polymorphic unless super FRigUnit_SimBase is polymorphic");

class UScriptStruct* FRigUnit_AccumulateFloatAdd::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_AccumulateFloatAdd"), sizeof(FRigUnit_AccumulateFloatAdd), Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_AccumulateFloatAdd::Execute"), &FRigUnit_AccumulateFloatAdd::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_AccumulateFloatAdd>()
{
	return FRigUnit_AccumulateFloatAdd::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_AccumulateFloatAdd(FRigUnit_AccumulateFloatAdd::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_AccumulateFloatAdd"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AccumulateFloatAdd
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AccumulateFloatAdd()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_AccumulateFloatAdd>(FName(TEXT("RigUnit_AccumulateFloatAdd")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AccumulateFloatAdd;
	struct Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Increment_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Increment;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InitialValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InitialValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIntegrateDeltaTime_MetaData[];
#endif
		static void NewProp_bIntegrateDeltaTime_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIntegrateDeltaTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Result;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AccumulatedValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AccumulatedValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Adds a value over time over and over again\n */" },
		{ "DisplayName", "Accumulate Add (Float)" },
		{ "Keywords", "Simulate,++" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
		{ "PrototypeName", "AccumulateAdd" },
		{ "ToolTip", "Adds a value over time over and over again" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_AccumulateFloatAdd>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Statics::NewProp_Increment_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Statics::NewProp_Increment = { "Increment", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateFloatAdd, Increment), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Statics::NewProp_Increment_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Statics::NewProp_Increment_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Statics::NewProp_InitialValue_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Statics::NewProp_InitialValue = { "InitialValue", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateFloatAdd, InitialValue), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Statics::NewProp_InitialValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Statics::NewProp_InitialValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Statics::NewProp_bIntegrateDeltaTime_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Statics::NewProp_bIntegrateDeltaTime_SetBit(void* Obj)
	{
		((FRigUnit_AccumulateFloatAdd*)Obj)->bIntegrateDeltaTime = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Statics::NewProp_bIntegrateDeltaTime = { "bIntegrateDeltaTime", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_AccumulateFloatAdd), &Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Statics::NewProp_bIntegrateDeltaTime_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Statics::NewProp_bIntegrateDeltaTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Statics::NewProp_bIntegrateDeltaTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateFloatAdd, Result), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Statics::NewProp_Result_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Statics::NewProp_AccumulatedValue_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_Accumulate.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Statics::NewProp_AccumulatedValue = { "AccumulatedValue", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AccumulateFloatAdd, AccumulatedValue), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Statics::NewProp_AccumulatedValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Statics::NewProp_AccumulatedValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Statics::NewProp_Increment,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Statics::NewProp_InitialValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Statics::NewProp_bIntegrateDeltaTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Statics::NewProp_Result,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Statics::NewProp_AccumulatedValue,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_SimBase,
		&NewStructOps,
		"RigUnit_AccumulateFloatAdd",
		sizeof(FRigUnit_AccumulateFloatAdd),
		alignof(FRigUnit_AccumulateFloatAdd),
		Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_AccumulateFloatAdd"), sizeof(FRigUnit_AccumulateFloatAdd), Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Hash() { return 3063725529U; }

void FRigUnit_AccumulateFloatAdd::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Increment,
		InitialValue,
		bIntegrateDeltaTime,
		Result,
		AccumulatedValue,
		Context
	);
}

PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
