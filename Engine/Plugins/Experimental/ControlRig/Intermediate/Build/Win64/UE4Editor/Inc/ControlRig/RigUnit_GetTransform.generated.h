// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigUnit_GetTransform_generated_h
#error "RigUnit_GetTransform.generated.h already included, missing '#pragma once' in RigUnit_GetTransform.h"
#endif
#define CONTROLRIG_RigUnit_GetTransform_generated_h


#define FRigUnit_GetTransform_Execute() \
	void FRigUnit_GetTransform::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKey& Item, \
		const EBoneGetterSetterMode Space, \
		const bool bInitial, \
		FTransform& Transform, \
		FCachedRigElement& CachedIndex, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Hierarchy_RigUnit_GetTransform_h_14_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_GetTransform_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKey& Item, \
		const EBoneGetterSetterMode Space, \
		const bool bInitial, \
		FTransform& Transform, \
		FCachedRigElement& CachedIndex, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FRigElementKey& Item = *(FRigElementKey*)RigVMMemoryHandles[0].GetData(); \
		EBoneGetterSetterMode Space = (EBoneGetterSetterMode)*(uint8*)RigVMMemoryHandles[1].GetData(); \
		const bool bInitial = *(bool*)RigVMMemoryHandles[2].GetData(); \
		FTransform& Transform = *(FTransform*)RigVMMemoryHandles[3].GetData(); \
		FRigVMDynamicArray<FCachedRigElement> CachedIndex_4_Array(*((FRigVMByteArray*)RigVMMemoryHandles[4].GetData(0, false))); \
		CachedIndex_4_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedIndex = CachedIndex_4_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Item, \
			Space, \
			bInitial, \
			Transform, \
			CachedIndex, \
			Context \
		); \
	} \
	typedef FRigUnit Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_GetTransform>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Hierarchy_RigUnit_GetTransform_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
