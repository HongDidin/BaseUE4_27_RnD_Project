// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigUnit_SetRelativeBoneTransform_generated_h
#error "RigUnit_SetRelativeBoneTransform.generated.h already included, missing '#pragma once' in RigUnit_SetRelativeBoneTransform.h"
#endif
#define CONTROLRIG_RigUnit_SetRelativeBoneTransform_generated_h


#define FRigUnit_SetRelativeBoneTransform_Execute() \
	void FRigUnit_SetRelativeBoneTransform::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Bone, \
		const FName& Space, \
		const FTransform& Transform, \
		const float Weight, \
		const bool bPropagateToChildren, \
		FCachedRigElement& CachedBone, \
		FCachedRigElement& CachedSpaceIndex, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Hierarchy_RigUnit_SetRelativeBoneTransform_h_15_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_SetRelativeBoneTransform_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Bone, \
		const FName& Space, \
		const FTransform& Transform, \
		const float Weight, \
		const bool bPropagateToChildren, \
		FCachedRigElement& CachedBone, \
		FCachedRigElement& CachedSpaceIndex, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FName& Bone = *(FName*)RigVMMemoryHandles[0].GetData(); \
		const FName& Space = *(FName*)RigVMMemoryHandles[1].GetData(); \
		const FTransform& Transform = *(FTransform*)RigVMMemoryHandles[2].GetData(); \
		const float Weight = *(float*)RigVMMemoryHandles[3].GetData(); \
		const bool bPropagateToChildren = *(bool*)RigVMMemoryHandles[4].GetData(); \
		FRigVMDynamicArray<FCachedRigElement> CachedBone_5_Array(*((FRigVMByteArray*)RigVMMemoryHandles[5].GetData(0, false))); \
		CachedBone_5_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedBone = CachedBone_5_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<FCachedRigElement> CachedSpaceIndex_6_Array(*((FRigVMByteArray*)RigVMMemoryHandles[6].GetData(0, false))); \
		CachedSpaceIndex_6_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedSpaceIndex = CachedSpaceIndex_6_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[7].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Bone, \
			Space, \
			Transform, \
			Weight, \
			bPropagateToChildren, \
			CachedBone, \
			CachedSpaceIndex, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnitMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_SetRelativeBoneTransform>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Hierarchy_RigUnit_SetRelativeBoneTransform_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
