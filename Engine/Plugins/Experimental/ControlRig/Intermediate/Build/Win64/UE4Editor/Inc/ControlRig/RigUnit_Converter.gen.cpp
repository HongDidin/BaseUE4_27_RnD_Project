// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Deprecated/Math/RigUnit_Converter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_Converter() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ToSwingAndTwist();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FQuat();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternionToVector();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ConvertRotationToVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToQuaternion();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToRotation();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternion();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ConvertVectorRotation();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ConvertRotation();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ConvertEulerTransform();
	ANIMATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FEulerTransform();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ConvertTransform();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_ToSwingAndTwist>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_ToSwingAndTwist cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_ToSwingAndTwist::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ToSwingAndTwist_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_ToSwingAndTwist, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_ToSwingAndTwist"), sizeof(FRigUnit_ToSwingAndTwist), Get_Z_Construct_UScriptStruct_FRigUnit_ToSwingAndTwist_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_ToSwingAndTwist::Execute"), &FRigUnit_ToSwingAndTwist::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_ToSwingAndTwist>()
{
	return FRigUnit_ToSwingAndTwist::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_ToSwingAndTwist(FRigUnit_ToSwingAndTwist::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_ToSwingAndTwist"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ToSwingAndTwist
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ToSwingAndTwist()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_ToSwingAndTwist>(FName(TEXT("RigUnit_ToSwingAndTwist")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ToSwingAndTwist;
	struct Z_Construct_UScriptStruct_FRigUnit_ToSwingAndTwist_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Input_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Input;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TwistAxis_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TwistAxis;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Swing_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Swing;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Twist_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Twist;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ToSwingAndTwist_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Math|Transform" },
		{ "Deprecated", "4.23.0" },
		{ "DisplayName", "ToSwingAndTwist" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Converter.h" },
		{ "NodeColor", "0.1 0.1 0.7" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_ToSwingAndTwist_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_ToSwingAndTwist>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ToSwingAndTwist_Statics::NewProp_Input_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Converter.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ToSwingAndTwist_Statics::NewProp_Input = { "Input", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ToSwingAndTwist, Input), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ToSwingAndTwist_Statics::NewProp_Input_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ToSwingAndTwist_Statics::NewProp_Input_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ToSwingAndTwist_Statics::NewProp_TwistAxis_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Converter.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ToSwingAndTwist_Statics::NewProp_TwistAxis = { "TwistAxis", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ToSwingAndTwist, TwistAxis), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ToSwingAndTwist_Statics::NewProp_TwistAxis_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ToSwingAndTwist_Statics::NewProp_TwistAxis_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ToSwingAndTwist_Statics::NewProp_Swing_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Converter.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ToSwingAndTwist_Statics::NewProp_Swing = { "Swing", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ToSwingAndTwist, Swing), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ToSwingAndTwist_Statics::NewProp_Swing_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ToSwingAndTwist_Statics::NewProp_Swing_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ToSwingAndTwist_Statics::NewProp_Twist_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Converter.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ToSwingAndTwist_Statics::NewProp_Twist = { "Twist", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ToSwingAndTwist, Twist), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ToSwingAndTwist_Statics::NewProp_Twist_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ToSwingAndTwist_Statics::NewProp_Twist_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_ToSwingAndTwist_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ToSwingAndTwist_Statics::NewProp_Input,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ToSwingAndTwist_Statics::NewProp_TwistAxis,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ToSwingAndTwist_Statics::NewProp_Swing,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ToSwingAndTwist_Statics::NewProp_Twist,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_ToSwingAndTwist_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_ToSwingAndTwist",
		sizeof(FRigUnit_ToSwingAndTwist),
		alignof(FRigUnit_ToSwingAndTwist),
		Z_Construct_UScriptStruct_FRigUnit_ToSwingAndTwist_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ToSwingAndTwist_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ToSwingAndTwist_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ToSwingAndTwist_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ToSwingAndTwist()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ToSwingAndTwist_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_ToSwingAndTwist"), sizeof(FRigUnit_ToSwingAndTwist), Get_Z_Construct_UScriptStruct_FRigUnit_ToSwingAndTwist_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_ToSwingAndTwist_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ToSwingAndTwist_Hash() { return 261115987U; }

void FRigUnit_ToSwingAndTwist::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Input,
		TwistAxis,
		Swing,
		Twist,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_ConvertQuaternionToVector>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_ConvertQuaternionToVector cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_ConvertQuaternionToVector::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternionToVector_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternionToVector, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_ConvertQuaternionToVector"), sizeof(FRigUnit_ConvertQuaternionToVector), Get_Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternionToVector_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_ConvertQuaternionToVector::Execute"), &FRigUnit_ConvertQuaternionToVector::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_ConvertQuaternionToVector>()
{
	return FRigUnit_ConvertQuaternionToVector::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_ConvertQuaternionToVector(FRigUnit_ConvertQuaternionToVector::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_ConvertQuaternionToVector"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ConvertQuaternionToVector
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ConvertQuaternionToVector()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_ConvertQuaternionToVector>(FName(TEXT("RigUnit_ConvertQuaternionToVector")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ConvertQuaternionToVector;
	struct Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternionToVector_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Input_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Input;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternionToVector_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Math|Convert" },
		{ "Deprecated", "4.23.0" },
		{ "DisplayName", "ConvertQuaternionToVector" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Converter.h" },
		{ "NodeColor", "0.1 0.1 0.7" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternionToVector_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_ConvertQuaternionToVector>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternionToVector_Statics::NewProp_Input_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Converter.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternionToVector_Statics::NewProp_Input = { "Input", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ConvertQuaternionToVector, Input), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternionToVector_Statics::NewProp_Input_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternionToVector_Statics::NewProp_Input_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternionToVector_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Converter.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternionToVector_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ConvertQuaternionToVector, Result), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternionToVector_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternionToVector_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternionToVector_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternionToVector_Statics::NewProp_Input,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternionToVector_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternionToVector_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_ConvertQuaternionToVector",
		sizeof(FRigUnit_ConvertQuaternionToVector),
		alignof(FRigUnit_ConvertQuaternionToVector),
		Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternionToVector_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternionToVector_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternionToVector_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternionToVector_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternionToVector()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternionToVector_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_ConvertQuaternionToVector"), sizeof(FRigUnit_ConvertQuaternionToVector), Get_Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternionToVector_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternionToVector_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternionToVector_Hash() { return 3148479310U; }

void FRigUnit_ConvertQuaternionToVector::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Input,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_ConvertRotationToVector>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_ConvertRotationToVector cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_ConvertRotationToVector::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ConvertRotationToVector_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_ConvertRotationToVector, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_ConvertRotationToVector"), sizeof(FRigUnit_ConvertRotationToVector), Get_Z_Construct_UScriptStruct_FRigUnit_ConvertRotationToVector_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_ConvertRotationToVector::Execute"), &FRigUnit_ConvertRotationToVector::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_ConvertRotationToVector>()
{
	return FRigUnit_ConvertRotationToVector::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_ConvertRotationToVector(FRigUnit_ConvertRotationToVector::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_ConvertRotationToVector"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ConvertRotationToVector
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ConvertRotationToVector()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_ConvertRotationToVector>(FName(TEXT("RigUnit_ConvertRotationToVector")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ConvertRotationToVector;
	struct Z_Construct_UScriptStruct_FRigUnit_ConvertRotationToVector_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Input_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Input;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ConvertRotationToVector_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Math|Convert" },
		{ "Deprecated", "4.23.0" },
		{ "DisplayName", "ConvertRotationToVector" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Converter.h" },
		{ "NodeColor", "0.1 0.1 0.7" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_ConvertRotationToVector_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_ConvertRotationToVector>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ConvertRotationToVector_Statics::NewProp_Input_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Converter.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ConvertRotationToVector_Statics::NewProp_Input = { "Input", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ConvertRotationToVector, Input), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ConvertRotationToVector_Statics::NewProp_Input_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ConvertRotationToVector_Statics::NewProp_Input_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ConvertRotationToVector_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Converter.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ConvertRotationToVector_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ConvertRotationToVector, Result), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ConvertRotationToVector_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ConvertRotationToVector_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_ConvertRotationToVector_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ConvertRotationToVector_Statics::NewProp_Input,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ConvertRotationToVector_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_ConvertRotationToVector_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_ConvertRotationToVector",
		sizeof(FRigUnit_ConvertRotationToVector),
		alignof(FRigUnit_ConvertRotationToVector),
		Z_Construct_UScriptStruct_FRigUnit_ConvertRotationToVector_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ConvertRotationToVector_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ConvertRotationToVector_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ConvertRotationToVector_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ConvertRotationToVector()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ConvertRotationToVector_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_ConvertRotationToVector"), sizeof(FRigUnit_ConvertRotationToVector), Get_Z_Construct_UScriptStruct_FRigUnit_ConvertRotationToVector_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_ConvertRotationToVector_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ConvertRotationToVector_Hash() { return 1540527232U; }

void FRigUnit_ConvertRotationToVector::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Input,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_ConvertVectorToQuaternion>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_ConvertVectorToQuaternion cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_ConvertVectorToQuaternion::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToQuaternion_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToQuaternion, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_ConvertVectorToQuaternion"), sizeof(FRigUnit_ConvertVectorToQuaternion), Get_Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToQuaternion_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_ConvertVectorToQuaternion::Execute"), &FRigUnit_ConvertVectorToQuaternion::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_ConvertVectorToQuaternion>()
{
	return FRigUnit_ConvertVectorToQuaternion::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_ConvertVectorToQuaternion(FRigUnit_ConvertVectorToQuaternion::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_ConvertVectorToQuaternion"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ConvertVectorToQuaternion
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ConvertVectorToQuaternion()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_ConvertVectorToQuaternion>(FName(TEXT("RigUnit_ConvertVectorToQuaternion")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ConvertVectorToQuaternion;
	struct Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToQuaternion_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Input_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Input;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToQuaternion_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Math|Convert" },
		{ "Deprecated", "4.23.0" },
		{ "DisplayName", "ConvertVectorToQuaternion" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Converter.h" },
		{ "NodeColor", "0.1 0.1 0.7" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToQuaternion_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_ConvertVectorToQuaternion>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToQuaternion_Statics::NewProp_Input_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Converter.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToQuaternion_Statics::NewProp_Input = { "Input", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ConvertVectorToQuaternion, Input), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToQuaternion_Statics::NewProp_Input_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToQuaternion_Statics::NewProp_Input_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToQuaternion_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Converter.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToQuaternion_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ConvertVectorToQuaternion, Result), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToQuaternion_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToQuaternion_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToQuaternion_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToQuaternion_Statics::NewProp_Input,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToQuaternion_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToQuaternion_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_ConvertVectorToQuaternion",
		sizeof(FRigUnit_ConvertVectorToQuaternion),
		alignof(FRigUnit_ConvertVectorToQuaternion),
		Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToQuaternion_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToQuaternion_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToQuaternion_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToQuaternion_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToQuaternion()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToQuaternion_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_ConvertVectorToQuaternion"), sizeof(FRigUnit_ConvertVectorToQuaternion), Get_Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToQuaternion_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToQuaternion_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToQuaternion_Hash() { return 1911740309U; }

void FRigUnit_ConvertVectorToQuaternion::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Input,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_ConvertVectorToRotation>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_ConvertVectorToRotation cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_ConvertVectorToRotation::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToRotation_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToRotation, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_ConvertVectorToRotation"), sizeof(FRigUnit_ConvertVectorToRotation), Get_Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToRotation_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_ConvertVectorToRotation::Execute"), &FRigUnit_ConvertVectorToRotation::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_ConvertVectorToRotation>()
{
	return FRigUnit_ConvertVectorToRotation::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_ConvertVectorToRotation(FRigUnit_ConvertVectorToRotation::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_ConvertVectorToRotation"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ConvertVectorToRotation
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ConvertVectorToRotation()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_ConvertVectorToRotation>(FName(TEXT("RigUnit_ConvertVectorToRotation")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ConvertVectorToRotation;
	struct Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToRotation_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Input_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Input;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToRotation_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Math|Convert" },
		{ "Deprecated", "4.23.0" },
		{ "DisplayName", "ConvertVectorToRotation" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Converter.h" },
		{ "NodeColor", "0.1 0.1 0.7" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToRotation_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_ConvertVectorToRotation>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToRotation_Statics::NewProp_Input_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Converter.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToRotation_Statics::NewProp_Input = { "Input", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ConvertVectorToRotation, Input), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToRotation_Statics::NewProp_Input_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToRotation_Statics::NewProp_Input_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToRotation_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Converter.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToRotation_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ConvertVectorToRotation, Result), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToRotation_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToRotation_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToRotation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToRotation_Statics::NewProp_Input,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToRotation_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToRotation_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_ConvertVectorToRotation",
		sizeof(FRigUnit_ConvertVectorToRotation),
		alignof(FRigUnit_ConvertVectorToRotation),
		Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToRotation_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToRotation_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToRotation_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToRotation_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToRotation()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToRotation_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_ConvertVectorToRotation"), sizeof(FRigUnit_ConvertVectorToRotation), Get_Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToRotation_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToRotation_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ConvertVectorToRotation_Hash() { return 887201585U; }

void FRigUnit_ConvertVectorToRotation::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Input,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_ConvertQuaternion>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_ConvertQuaternion cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_ConvertQuaternion::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternion_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternion, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_ConvertQuaternion"), sizeof(FRigUnit_ConvertQuaternion), Get_Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternion_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_ConvertQuaternion::Execute"), &FRigUnit_ConvertQuaternion::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_ConvertQuaternion>()
{
	return FRigUnit_ConvertQuaternion::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_ConvertQuaternion(FRigUnit_ConvertQuaternion::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_ConvertQuaternion"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ConvertQuaternion
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ConvertQuaternion()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_ConvertQuaternion>(FName(TEXT("RigUnit_ConvertQuaternion")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ConvertQuaternion;
	struct Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternion_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Input_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Input;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternion_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Math|Convert" },
		{ "Deprecated", "4.23.0" },
		{ "DisplayName", "ConvertToRotation" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Converter.h" },
		{ "NodeColor", "0.1 0.1 0.7" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternion_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_ConvertQuaternion>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternion_Statics::NewProp_Input_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Converter.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternion_Statics::NewProp_Input = { "Input", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ConvertQuaternion, Input), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternion_Statics::NewProp_Input_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternion_Statics::NewProp_Input_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternion_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Converter.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternion_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ConvertQuaternion, Result), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternion_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternion_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternion_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternion_Statics::NewProp_Input,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternion_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternion_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_ConvertQuaternion",
		sizeof(FRigUnit_ConvertQuaternion),
		alignof(FRigUnit_ConvertQuaternion),
		Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternion_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternion_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternion_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternion_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternion()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternion_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_ConvertQuaternion"), sizeof(FRigUnit_ConvertQuaternion), Get_Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternion_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternion_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ConvertQuaternion_Hash() { return 3802701461U; }

void FRigUnit_ConvertQuaternion::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Input,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_ConvertVectorRotation>() == std::is_polymorphic<FRigUnit_ConvertRotation>(), "USTRUCT FRigUnit_ConvertVectorRotation cannot be polymorphic unless super FRigUnit_ConvertRotation is polymorphic");

class UScriptStruct* FRigUnit_ConvertVectorRotation::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ConvertVectorRotation_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_ConvertVectorRotation, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_ConvertVectorRotation"), sizeof(FRigUnit_ConvertVectorRotation), Get_Z_Construct_UScriptStruct_FRigUnit_ConvertVectorRotation_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_ConvertVectorRotation>()
{
	return FRigUnit_ConvertVectorRotation::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_ConvertVectorRotation(FRigUnit_ConvertVectorRotation::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_ConvertVectorRotation"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ConvertVectorRotation
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ConvertVectorRotation()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_ConvertVectorRotation>(FName(TEXT("RigUnit_ConvertVectorRotation")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ConvertVectorRotation;
	struct Z_Construct_UScriptStruct_FRigUnit_ConvertVectorRotation_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ConvertVectorRotation_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Math|Convert" },
		{ "Deprecated", "4.23.0" },
		{ "DisplayName", "ConvertToQuaternionDeprecated" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Converter.h" },
		{ "NodeColor", "0.1 0.1 0.7" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_ConvertVectorRotation_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_ConvertVectorRotation>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_ConvertVectorRotation_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_ConvertRotation,
		&NewStructOps,
		"RigUnit_ConvertVectorRotation",
		sizeof(FRigUnit_ConvertVectorRotation),
		alignof(FRigUnit_ConvertVectorRotation),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ConvertVectorRotation_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ConvertVectorRotation_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ConvertVectorRotation()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ConvertVectorRotation_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_ConvertVectorRotation"), sizeof(FRigUnit_ConvertVectorRotation), Get_Z_Construct_UScriptStruct_FRigUnit_ConvertVectorRotation_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_ConvertVectorRotation_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ConvertVectorRotation_Hash() { return 3699139553U; }

static_assert(std::is_polymorphic<FRigUnit_ConvertRotation>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_ConvertRotation cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_ConvertRotation::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ConvertRotation_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_ConvertRotation, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_ConvertRotation"), sizeof(FRigUnit_ConvertRotation), Get_Z_Construct_UScriptStruct_FRigUnit_ConvertRotation_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_ConvertRotation::Execute"), &FRigUnit_ConvertRotation::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_ConvertRotation>()
{
	return FRigUnit_ConvertRotation::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_ConvertRotation(FRigUnit_ConvertRotation::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_ConvertRotation"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ConvertRotation
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ConvertRotation()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_ConvertRotation>(FName(TEXT("RigUnit_ConvertRotation")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ConvertRotation;
	struct Z_Construct_UScriptStruct_FRigUnit_ConvertRotation_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Input_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Input;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ConvertRotation_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Math|Convert" },
		{ "Deprecated", "4.23.0" },
		{ "DisplayName", "ConvertToQuaternion" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Converter.h" },
		{ "NodeColor", "0.1 0.1 0.7" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_ConvertRotation_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_ConvertRotation>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ConvertRotation_Statics::NewProp_Input_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Converter.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ConvertRotation_Statics::NewProp_Input = { "Input", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ConvertRotation, Input), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ConvertRotation_Statics::NewProp_Input_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ConvertRotation_Statics::NewProp_Input_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ConvertRotation_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Converter.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ConvertRotation_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ConvertRotation, Result), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ConvertRotation_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ConvertRotation_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_ConvertRotation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ConvertRotation_Statics::NewProp_Input,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ConvertRotation_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_ConvertRotation_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_ConvertRotation",
		sizeof(FRigUnit_ConvertRotation),
		alignof(FRigUnit_ConvertRotation),
		Z_Construct_UScriptStruct_FRigUnit_ConvertRotation_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ConvertRotation_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ConvertRotation_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ConvertRotation_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ConvertRotation()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ConvertRotation_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_ConvertRotation"), sizeof(FRigUnit_ConvertRotation), Get_Z_Construct_UScriptStruct_FRigUnit_ConvertRotation_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_ConvertRotation_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ConvertRotation_Hash() { return 3137648915U; }

void FRigUnit_ConvertRotation::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Input,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_ConvertEulerTransform>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_ConvertEulerTransform cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_ConvertEulerTransform::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ConvertEulerTransform_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_ConvertEulerTransform, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_ConvertEulerTransform"), sizeof(FRigUnit_ConvertEulerTransform), Get_Z_Construct_UScriptStruct_FRigUnit_ConvertEulerTransform_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_ConvertEulerTransform::Execute"), &FRigUnit_ConvertEulerTransform::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_ConvertEulerTransform>()
{
	return FRigUnit_ConvertEulerTransform::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_ConvertEulerTransform(FRigUnit_ConvertEulerTransform::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_ConvertEulerTransform"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ConvertEulerTransform
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ConvertEulerTransform()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_ConvertEulerTransform>(FName(TEXT("RigUnit_ConvertEulerTransform")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ConvertEulerTransform;
	struct Z_Construct_UScriptStruct_FRigUnit_ConvertEulerTransform_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Input_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Input;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ConvertEulerTransform_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Math|Convert" },
		{ "Deprecated", "4.23.0" },
		{ "DisplayName", "ConvertToTransform" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Converter.h" },
		{ "NodeColor", "0.1 0.1 0.7" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_ConvertEulerTransform_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_ConvertEulerTransform>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ConvertEulerTransform_Statics::NewProp_Input_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Converter.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ConvertEulerTransform_Statics::NewProp_Input = { "Input", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ConvertEulerTransform, Input), Z_Construct_UScriptStruct_FEulerTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ConvertEulerTransform_Statics::NewProp_Input_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ConvertEulerTransform_Statics::NewProp_Input_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ConvertEulerTransform_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Converter.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ConvertEulerTransform_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ConvertEulerTransform, Result), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ConvertEulerTransform_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ConvertEulerTransform_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_ConvertEulerTransform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ConvertEulerTransform_Statics::NewProp_Input,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ConvertEulerTransform_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_ConvertEulerTransform_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_ConvertEulerTransform",
		sizeof(FRigUnit_ConvertEulerTransform),
		alignof(FRigUnit_ConvertEulerTransform),
		Z_Construct_UScriptStruct_FRigUnit_ConvertEulerTransform_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ConvertEulerTransform_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ConvertEulerTransform_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ConvertEulerTransform_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ConvertEulerTransform()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ConvertEulerTransform_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_ConvertEulerTransform"), sizeof(FRigUnit_ConvertEulerTransform), Get_Z_Construct_UScriptStruct_FRigUnit_ConvertEulerTransform_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_ConvertEulerTransform_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ConvertEulerTransform_Hash() { return 369047669U; }

void FRigUnit_ConvertEulerTransform::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Input,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_ConvertTransform>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_ConvertTransform cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_ConvertTransform::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ConvertTransform_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_ConvertTransform, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_ConvertTransform"), sizeof(FRigUnit_ConvertTransform), Get_Z_Construct_UScriptStruct_FRigUnit_ConvertTransform_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_ConvertTransform::Execute"), &FRigUnit_ConvertTransform::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_ConvertTransform>()
{
	return FRigUnit_ConvertTransform::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_ConvertTransform(FRigUnit_ConvertTransform::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_ConvertTransform"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ConvertTransform
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ConvertTransform()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_ConvertTransform>(FName(TEXT("RigUnit_ConvertTransform")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ConvertTransform;
	struct Z_Construct_UScriptStruct_FRigUnit_ConvertTransform_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Input_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Input;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ConvertTransform_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Math|Convert" },
		{ "Deprecated", "4.23.0" },
		{ "DisplayName", "ConvertToEulerTransform" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Converter.h" },
		{ "NodeColor", "0.1 0.1 0.7" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_ConvertTransform_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_ConvertTransform>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ConvertTransform_Statics::NewProp_Input_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Converter.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ConvertTransform_Statics::NewProp_Input = { "Input", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ConvertTransform, Input), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ConvertTransform_Statics::NewProp_Input_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ConvertTransform_Statics::NewProp_Input_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ConvertTransform_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Deprecated/Math/RigUnit_Converter.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ConvertTransform_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ConvertTransform, Result), Z_Construct_UScriptStruct_FEulerTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ConvertTransform_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ConvertTransform_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_ConvertTransform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ConvertTransform_Statics::NewProp_Input,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ConvertTransform_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_ConvertTransform_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_ConvertTransform",
		sizeof(FRigUnit_ConvertTransform),
		alignof(FRigUnit_ConvertTransform),
		Z_Construct_UScriptStruct_FRigUnit_ConvertTransform_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ConvertTransform_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ConvertTransform_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ConvertTransform_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ConvertTransform()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ConvertTransform_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_ConvertTransform"), sizeof(FRigUnit_ConvertTransform), Get_Z_Construct_UScriptStruct_FRigUnit_ConvertTransform_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_ConvertTransform_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ConvertTransform_Hash() { return 969716184U; }

void FRigUnit_ConvertTransform::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Input,
		Result,
		Context
	);
}

PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
