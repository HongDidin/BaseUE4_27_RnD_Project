// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIGEDITOR_BakeToControlRigSettings_generated_h
#error "BakeToControlRigSettings.generated.h already included, missing '#pragma once' in BakeToControlRigSettings.h"
#endif
#define CONTROLRIGEDITOR_BakeToControlRigSettings_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_BakeToControlRigSettings_h_15_SPARSE_DATA
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_BakeToControlRigSettings_h_15_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_BakeToControlRigSettings_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_BakeToControlRigSettings_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUBakeToControlRigSettings(); \
	friend struct Z_Construct_UClass_UBakeToControlRigSettings_Statics; \
public: \
	DECLARE_CLASS(UBakeToControlRigSettings, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRigEditor"), NO_API) \
	DECLARE_SERIALIZER(UBakeToControlRigSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorSettings");} \



#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_BakeToControlRigSettings_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUBakeToControlRigSettings(); \
	friend struct Z_Construct_UClass_UBakeToControlRigSettings_Statics; \
public: \
	DECLARE_CLASS(UBakeToControlRigSettings, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRigEditor"), NO_API) \
	DECLARE_SERIALIZER(UBakeToControlRigSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorSettings");} \



#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_BakeToControlRigSettings_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBakeToControlRigSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBakeToControlRigSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBakeToControlRigSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBakeToControlRigSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBakeToControlRigSettings(UBakeToControlRigSettings&&); \
	NO_API UBakeToControlRigSettings(const UBakeToControlRigSettings&); \
public:


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_BakeToControlRigSettings_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBakeToControlRigSettings(UBakeToControlRigSettings&&); \
	NO_API UBakeToControlRigSettings(const UBakeToControlRigSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBakeToControlRigSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBakeToControlRigSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBakeToControlRigSettings)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_BakeToControlRigSettings_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_BakeToControlRigSettings_h_9_PROLOG
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_BakeToControlRigSettings_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_BakeToControlRigSettings_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_BakeToControlRigSettings_h_15_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_BakeToControlRigSettings_h_15_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_BakeToControlRigSettings_h_15_INCLASS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_BakeToControlRigSettings_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_BakeToControlRigSettings_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_BakeToControlRigSettings_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_BakeToControlRigSettings_h_15_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_BakeToControlRigSettings_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_BakeToControlRigSettings_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_BakeToControlRigSettings_h_15_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONTROLRIGEDITOR_API UClass* StaticClass<class UBakeToControlRigSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_BakeToControlRigSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
