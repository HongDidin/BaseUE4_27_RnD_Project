// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Hierarchy/RigUnit_WorldSpace.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_WorldSpace() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Rotation();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FQuat();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Rotation();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Location();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Location();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Transform();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Transform();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_ToRigSpace_Rotation>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_ToRigSpace_Rotation cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_ToRigSpace_Rotation::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Rotation_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Rotation, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_ToRigSpace_Rotation"), sizeof(FRigUnit_ToRigSpace_Rotation), Get_Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Rotation_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_ToRigSpace_Rotation::Execute"), &FRigUnit_ToRigSpace_Rotation::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_ToRigSpace_Rotation>()
{
	return FRigUnit_ToRigSpace_Rotation::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_ToRigSpace_Rotation(FRigUnit_ToRigSpace_Rotation::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_ToRigSpace_Rotation"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ToRigSpace_Rotation
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ToRigSpace_Rotation()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_ToRigSpace_Rotation>(FName(TEXT("RigUnit_ToRigSpace_Rotation")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ToRigSpace_Rotation;
	struct Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Rotation_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Rotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Rotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Global_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Global;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Rotation_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "/**\n * Converts a rotation from world space to rig (global) space\n */" },
		{ "DisplayName", "From World" },
		{ "DocumentationPolicy", "Strict" },
		{ "Keywords", "Global,Local,World,Actor,ComponentSpace,ToRig" },
		{ "MenuDescSuffix", "(Quat)" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_WorldSpace.h" },
		{ "ToolTip", "Converts a rotation from world space to rig (global) space" },
		{ "Varying", "" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Rotation_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_ToRigSpace_Rotation>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Rotation_Statics::NewProp_Rotation_MetaData[] = {
		{ "Comment", "/**\n\x09 * The input rotation in world\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_WorldSpace.h" },
		{ "ToolTip", "The input rotation in world" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Rotation_Statics::NewProp_Rotation = { "Rotation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ToRigSpace_Rotation, Rotation), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Rotation_Statics::NewProp_Rotation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Rotation_Statics::NewProp_Rotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Rotation_Statics::NewProp_Global_MetaData[] = {
		{ "Comment", "/**\n\x09 * The result rotation in global / rig space\n\x09 */" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_WorldSpace.h" },
		{ "Output", "" },
		{ "ToolTip", "The result rotation in global / rig space" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Rotation_Statics::NewProp_Global = { "Global", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ToRigSpace_Rotation, Global), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Rotation_Statics::NewProp_Global_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Rotation_Statics::NewProp_Global_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Rotation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Rotation_Statics::NewProp_Rotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Rotation_Statics::NewProp_Global,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Rotation_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_ToRigSpace_Rotation",
		sizeof(FRigUnit_ToRigSpace_Rotation),
		alignof(FRigUnit_ToRigSpace_Rotation),
		Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Rotation_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Rotation_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Rotation_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Rotation_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Rotation()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Rotation_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_ToRigSpace_Rotation"), sizeof(FRigUnit_ToRigSpace_Rotation), Get_Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Rotation_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Rotation_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Rotation_Hash() { return 2461241992U; }

void FRigUnit_ToRigSpace_Rotation::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Rotation,
		Global,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_ToWorldSpace_Rotation>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_ToWorldSpace_Rotation cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_ToWorldSpace_Rotation::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Rotation_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Rotation, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_ToWorldSpace_Rotation"), sizeof(FRigUnit_ToWorldSpace_Rotation), Get_Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Rotation_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_ToWorldSpace_Rotation::Execute"), &FRigUnit_ToWorldSpace_Rotation::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_ToWorldSpace_Rotation>()
{
	return FRigUnit_ToWorldSpace_Rotation::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_ToWorldSpace_Rotation(FRigUnit_ToWorldSpace_Rotation::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_ToWorldSpace_Rotation"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ToWorldSpace_Rotation
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ToWorldSpace_Rotation()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_ToWorldSpace_Rotation>(FName(TEXT("RigUnit_ToWorldSpace_Rotation")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ToWorldSpace_Rotation;
	struct Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Rotation_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Rotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Rotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_World_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_World;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Rotation_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "/**\n * Converts a rotation from rig (global) space to world space\n */" },
		{ "DisplayName", "To World" },
		{ "DocumentationPolicy", "Strict" },
		{ "Keywords", "Global,Local,World,Actor,ComponentSpace,FromRig" },
		{ "MenuDescSuffix", "(Quat)" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_WorldSpace.h" },
		{ "ToolTip", "Converts a rotation from rig (global) space to world space" },
		{ "Varying", "" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Rotation_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_ToWorldSpace_Rotation>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Rotation_Statics::NewProp_Rotation_MetaData[] = {
		{ "Comment", "/**\n\x09 * The input rotation in global / rig space\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_WorldSpace.h" },
		{ "ToolTip", "The input rotation in global / rig space" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Rotation_Statics::NewProp_Rotation = { "Rotation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ToWorldSpace_Rotation, Rotation), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Rotation_Statics::NewProp_Rotation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Rotation_Statics::NewProp_Rotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Rotation_Statics::NewProp_World_MetaData[] = {
		{ "Comment", "/**\n\x09 * The result rotation in world space\n\x09 */" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_WorldSpace.h" },
		{ "Output", "" },
		{ "ToolTip", "The result rotation in world space" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Rotation_Statics::NewProp_World = { "World", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ToWorldSpace_Rotation, World), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Rotation_Statics::NewProp_World_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Rotation_Statics::NewProp_World_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Rotation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Rotation_Statics::NewProp_Rotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Rotation_Statics::NewProp_World,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Rotation_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_ToWorldSpace_Rotation",
		sizeof(FRigUnit_ToWorldSpace_Rotation),
		alignof(FRigUnit_ToWorldSpace_Rotation),
		Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Rotation_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Rotation_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Rotation_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Rotation_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Rotation()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Rotation_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_ToWorldSpace_Rotation"), sizeof(FRigUnit_ToWorldSpace_Rotation), Get_Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Rotation_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Rotation_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Rotation_Hash() { return 1986701163U; }

void FRigUnit_ToWorldSpace_Rotation::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Rotation,
		World,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_ToRigSpace_Location>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_ToRigSpace_Location cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_ToRigSpace_Location::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Location_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Location, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_ToRigSpace_Location"), sizeof(FRigUnit_ToRigSpace_Location), Get_Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Location_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_ToRigSpace_Location::Execute"), &FRigUnit_ToRigSpace_Location::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_ToRigSpace_Location>()
{
	return FRigUnit_ToRigSpace_Location::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_ToRigSpace_Location(FRigUnit_ToRigSpace_Location::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_ToRigSpace_Location"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ToRigSpace_Location
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ToRigSpace_Location()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_ToRigSpace_Location>(FName(TEXT("RigUnit_ToRigSpace_Location")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ToRigSpace_Location;
	struct Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Location_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Location_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Location;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Global_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Global;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Location_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "/**\n * Converts a position / location from world space to rig (global) space\n */" },
		{ "DisplayName", "From World" },
		{ "DocumentationPolicy", "Strict" },
		{ "Keywords", "Global,Local,World,Actor,ComponentSpace,ToRig" },
		{ "MenuDescSuffix", "(Vector)" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_WorldSpace.h" },
		{ "ToolTip", "Converts a position / location from world space to rig (global) space" },
		{ "Varying", "" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Location_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_ToRigSpace_Location>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Location_Statics::NewProp_Location_MetaData[] = {
		{ "Comment", "/**\n\x09 * The input position / location in world\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_WorldSpace.h" },
		{ "ToolTip", "The input position / location in world" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Location_Statics::NewProp_Location = { "Location", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ToRigSpace_Location, Location), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Location_Statics::NewProp_Location_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Location_Statics::NewProp_Location_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Location_Statics::NewProp_Global_MetaData[] = {
		{ "Comment", "/**\n\x09 * The result position / location in global / rig space\n\x09 */" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_WorldSpace.h" },
		{ "Output", "" },
		{ "ToolTip", "The result position / location in global / rig space" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Location_Statics::NewProp_Global = { "Global", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ToRigSpace_Location, Global), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Location_Statics::NewProp_Global_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Location_Statics::NewProp_Global_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Location_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Location_Statics::NewProp_Location,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Location_Statics::NewProp_Global,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Location_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_ToRigSpace_Location",
		sizeof(FRigUnit_ToRigSpace_Location),
		alignof(FRigUnit_ToRigSpace_Location),
		Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Location_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Location_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Location_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Location_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Location()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Location_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_ToRigSpace_Location"), sizeof(FRigUnit_ToRigSpace_Location), Get_Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Location_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Location_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Location_Hash() { return 1096455584U; }

void FRigUnit_ToRigSpace_Location::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Location,
		Global,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_ToWorldSpace_Location>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_ToWorldSpace_Location cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_ToWorldSpace_Location::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Location_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Location, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_ToWorldSpace_Location"), sizeof(FRigUnit_ToWorldSpace_Location), Get_Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Location_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_ToWorldSpace_Location::Execute"), &FRigUnit_ToWorldSpace_Location::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_ToWorldSpace_Location>()
{
	return FRigUnit_ToWorldSpace_Location::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_ToWorldSpace_Location(FRigUnit_ToWorldSpace_Location::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_ToWorldSpace_Location"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ToWorldSpace_Location
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ToWorldSpace_Location()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_ToWorldSpace_Location>(FName(TEXT("RigUnit_ToWorldSpace_Location")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ToWorldSpace_Location;
	struct Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Location_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Location_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Location;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_World_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_World;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Location_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "/**\n * Converts a position / location from rig (global) space to world space\n */" },
		{ "DisplayName", "To World" },
		{ "DocumentationPolicy", "Strict" },
		{ "Keywords", "Global,Local,World,Actor,ComponentSpace,FromRig" },
		{ "MenuDescSuffix", "(Vector)" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_WorldSpace.h" },
		{ "ToolTip", "Converts a position / location from rig (global) space to world space" },
		{ "Varying", "" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Location_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_ToWorldSpace_Location>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Location_Statics::NewProp_Location_MetaData[] = {
		{ "Comment", "/**\n\x09 * The input position / location in global / rig space\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_WorldSpace.h" },
		{ "ToolTip", "The input position / location in global / rig space" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Location_Statics::NewProp_Location = { "Location", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ToWorldSpace_Location, Location), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Location_Statics::NewProp_Location_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Location_Statics::NewProp_Location_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Location_Statics::NewProp_World_MetaData[] = {
		{ "Comment", "/**\n\x09 * The result position / location in world space\n\x09 */" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_WorldSpace.h" },
		{ "Output", "" },
		{ "ToolTip", "The result position / location in world space" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Location_Statics::NewProp_World = { "World", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ToWorldSpace_Location, World), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Location_Statics::NewProp_World_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Location_Statics::NewProp_World_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Location_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Location_Statics::NewProp_Location,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Location_Statics::NewProp_World,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Location_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_ToWorldSpace_Location",
		sizeof(FRigUnit_ToWorldSpace_Location),
		alignof(FRigUnit_ToWorldSpace_Location),
		Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Location_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Location_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Location_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Location_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Location()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Location_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_ToWorldSpace_Location"), sizeof(FRigUnit_ToWorldSpace_Location), Get_Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Location_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Location_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Location_Hash() { return 2998951821U; }

void FRigUnit_ToWorldSpace_Location::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Location,
		World,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_ToRigSpace_Transform>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_ToRigSpace_Transform cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_ToRigSpace_Transform::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Transform_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Transform, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_ToRigSpace_Transform"), sizeof(FRigUnit_ToRigSpace_Transform), Get_Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Transform_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_ToRigSpace_Transform::Execute"), &FRigUnit_ToRigSpace_Transform::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_ToRigSpace_Transform>()
{
	return FRigUnit_ToRigSpace_Transform::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_ToRigSpace_Transform(FRigUnit_ToRigSpace_Transform::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_ToRigSpace_Transform"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ToRigSpace_Transform
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ToRigSpace_Transform()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_ToRigSpace_Transform>(FName(TEXT("RigUnit_ToRigSpace_Transform")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ToRigSpace_Transform;
	struct Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Transform_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Transform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Transform;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Global_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Global;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Transform_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "/**\n * Converts a transform from world space to rig (global) space\n */" },
		{ "DisplayName", "From World" },
		{ "DocumentationPolicy", "Strict" },
		{ "Keywords", "Global,Local,World,Actor,ComponentSpace,ToRig" },
		{ "MenuDescSuffix", "(Transform)" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_WorldSpace.h" },
		{ "ToolTip", "Converts a transform from world space to rig (global) space" },
		{ "Varying", "" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Transform_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_ToRigSpace_Transform>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Transform_Statics::NewProp_Transform_MetaData[] = {
		{ "Comment", "/**\n\x09 * The input transform in world\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_WorldSpace.h" },
		{ "ToolTip", "The input transform in world" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Transform_Statics::NewProp_Transform = { "Transform", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ToRigSpace_Transform, Transform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Transform_Statics::NewProp_Transform_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Transform_Statics::NewProp_Transform_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Transform_Statics::NewProp_Global_MetaData[] = {
		{ "Comment", "/**\n\x09 * The result transform in global / rig space\n\x09 */" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_WorldSpace.h" },
		{ "Output", "" },
		{ "ToolTip", "The result transform in global / rig space" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Transform_Statics::NewProp_Global = { "Global", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ToRigSpace_Transform, Global), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Transform_Statics::NewProp_Global_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Transform_Statics::NewProp_Global_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Transform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Transform_Statics::NewProp_Transform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Transform_Statics::NewProp_Global,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Transform_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_ToRigSpace_Transform",
		sizeof(FRigUnit_ToRigSpace_Transform),
		alignof(FRigUnit_ToRigSpace_Transform),
		Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Transform_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Transform_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Transform_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Transform_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Transform()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Transform_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_ToRigSpace_Transform"), sizeof(FRigUnit_ToRigSpace_Transform), Get_Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Transform_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Transform_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ToRigSpace_Transform_Hash() { return 3088983180U; }

void FRigUnit_ToRigSpace_Transform::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Transform,
		Global,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_ToWorldSpace_Transform>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_ToWorldSpace_Transform cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_ToWorldSpace_Transform::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Transform_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Transform, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_ToWorldSpace_Transform"), sizeof(FRigUnit_ToWorldSpace_Transform), Get_Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Transform_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_ToWorldSpace_Transform::Execute"), &FRigUnit_ToWorldSpace_Transform::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_ToWorldSpace_Transform>()
{
	return FRigUnit_ToWorldSpace_Transform::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_ToWorldSpace_Transform(FRigUnit_ToWorldSpace_Transform::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_ToWorldSpace_Transform"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ToWorldSpace_Transform
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ToWorldSpace_Transform()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_ToWorldSpace_Transform>(FName(TEXT("RigUnit_ToWorldSpace_Transform")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ToWorldSpace_Transform;
	struct Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Transform_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Transform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Transform;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_World_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_World;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Transform_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "/**\n * Converts a transform from rig (global) space to world space\n */" },
		{ "DisplayName", "To World" },
		{ "DocumentationPolicy", "Strict" },
		{ "Keywords", "Global,Local,World,Actor,ComponentSpace,FromRig" },
		{ "MenuDescSuffix", "(Transform)" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_WorldSpace.h" },
		{ "ToolTip", "Converts a transform from rig (global) space to world space" },
		{ "Varying", "" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Transform_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_ToWorldSpace_Transform>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Transform_Statics::NewProp_Transform_MetaData[] = {
		{ "Comment", "/**\n\x09 * The input transform in global / rig space\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_WorldSpace.h" },
		{ "ToolTip", "The input transform in global / rig space" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Transform_Statics::NewProp_Transform = { "Transform", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ToWorldSpace_Transform, Transform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Transform_Statics::NewProp_Transform_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Transform_Statics::NewProp_Transform_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Transform_Statics::NewProp_World_MetaData[] = {
		{ "Comment", "/**\n\x09 * The result transform in world space\n\x09 */" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_WorldSpace.h" },
		{ "Output", "" },
		{ "ToolTip", "The result transform in world space" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Transform_Statics::NewProp_World = { "World", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ToWorldSpace_Transform, World), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Transform_Statics::NewProp_World_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Transform_Statics::NewProp_World_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Transform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Transform_Statics::NewProp_Transform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Transform_Statics::NewProp_World,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Transform_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_ToWorldSpace_Transform",
		sizeof(FRigUnit_ToWorldSpace_Transform),
		alignof(FRigUnit_ToWorldSpace_Transform),
		Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Transform_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Transform_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Transform_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Transform_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Transform()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Transform_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_ToWorldSpace_Transform"), sizeof(FRigUnit_ToWorldSpace_Transform), Get_Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Transform_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Transform_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ToWorldSpace_Transform_Hash() { return 3267179383U; }

void FRigUnit_ToWorldSpace_Transform::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Transform,
		World,
		Context
	);
}

PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
