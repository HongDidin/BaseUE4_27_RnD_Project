// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UObject;
class UControlRigBlueprint;
#ifdef CONTROLRIGEDITOR_ControlRigBlueprintFactory_generated_h
#error "ControlRigBlueprintFactory.generated.h already included, missing '#pragma once' in ControlRigBlueprintFactory.h"
#endif
#define CONTROLRIGEDITOR_ControlRigBlueprintFactory_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigBlueprintFactory_h_17_SPARSE_DATA
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigBlueprintFactory_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execCreateControlRigFromSkeletalMeshOrSkeleton); \
	DECLARE_FUNCTION(execCreateNewControlRigAsset);


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigBlueprintFactory_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execCreateControlRigFromSkeletalMeshOrSkeleton); \
	DECLARE_FUNCTION(execCreateNewControlRigAsset);


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigBlueprintFactory_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUControlRigBlueprintFactory(); \
	friend struct Z_Construct_UClass_UControlRigBlueprintFactory_Statics; \
public: \
	DECLARE_CLASS(UControlRigBlueprintFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRigEditor"), CONTROLRIGEDITOR_API) \
	DECLARE_SERIALIZER(UControlRigBlueprintFactory)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigBlueprintFactory_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUControlRigBlueprintFactory(); \
	friend struct Z_Construct_UClass_UControlRigBlueprintFactory_Statics; \
public: \
	DECLARE_CLASS(UControlRigBlueprintFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRigEditor"), CONTROLRIGEDITOR_API) \
	DECLARE_SERIALIZER(UControlRigBlueprintFactory)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigBlueprintFactory_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	CONTROLRIGEDITOR_API UControlRigBlueprintFactory(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigBlueprintFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(CONTROLRIGEDITOR_API, UControlRigBlueprintFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigBlueprintFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	CONTROLRIGEDITOR_API UControlRigBlueprintFactory(UControlRigBlueprintFactory&&); \
	CONTROLRIGEDITOR_API UControlRigBlueprintFactory(const UControlRigBlueprintFactory&); \
public:


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigBlueprintFactory_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	CONTROLRIGEDITOR_API UControlRigBlueprintFactory(UControlRigBlueprintFactory&&); \
	CONTROLRIGEDITOR_API UControlRigBlueprintFactory(const UControlRigBlueprintFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(CONTROLRIGEDITOR_API, UControlRigBlueprintFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigBlueprintFactory); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UControlRigBlueprintFactory)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigBlueprintFactory_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigBlueprintFactory_h_14_PROLOG
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigBlueprintFactory_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigBlueprintFactory_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigBlueprintFactory_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigBlueprintFactory_h_17_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigBlueprintFactory_h_17_INCLASS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigBlueprintFactory_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigBlueprintFactory_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigBlueprintFactory_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigBlueprintFactory_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigBlueprintFactory_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigBlueprintFactory_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigBlueprintFactory_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONTROLRIGEDITOR_API UClass* StaticClass<class UControlRigBlueprintFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_ControlRigBlueprintFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
