// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigUnit_TwoBoneIKSimple_generated_h
#error "RigUnit_TwoBoneIKSimple.generated.h already included, missing '#pragma once' in RigUnit_TwoBoneIKSimple.h"
#endif
#define CONTROLRIG_RigUnit_TwoBoneIKSimple_generated_h


#define FRigUnit_TwoBoneIKSimpleTransforms_Execute() \
	void FRigUnit_TwoBoneIKSimpleTransforms::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FTransform& Root, \
		const FVector& PoleVector, \
		FTransform& Effector, \
		const FVector& PrimaryAxis, \
		const FVector& SecondaryAxis, \
		const float SecondaryAxisWeight, \
		const bool bEnableStretch, \
		const float StretchStartRatio, \
		const float StretchMaximumRatio, \
		const float BoneALength, \
		const float BoneBLength, \
		FTransform& Elbow, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_TwoBoneIKSimple_h_458_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FTransform& Root, \
		const FVector& PoleVector, \
		FTransform& Effector, \
		const FVector& PrimaryAxis, \
		const FVector& SecondaryAxis, \
		const float SecondaryAxisWeight, \
		const bool bEnableStretch, \
		const float StretchStartRatio, \
		const float StretchMaximumRatio, \
		const float BoneALength, \
		const float BoneBLength, \
		FTransform& Elbow, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		FTransform& Root = *(FTransform*)RigVMMemoryHandles[0].GetData(); \
		const FVector& PoleVector = *(FVector*)RigVMMemoryHandles[1].GetData(); \
		FTransform& Effector = *(FTransform*)RigVMMemoryHandles[2].GetData(); \
		const FVector& PrimaryAxis = *(FVector*)RigVMMemoryHandles[3].GetData(); \
		const FVector& SecondaryAxis = *(FVector*)RigVMMemoryHandles[4].GetData(); \
		const float SecondaryAxisWeight = *(float*)RigVMMemoryHandles[5].GetData(); \
		const bool bEnableStretch = *(bool*)RigVMMemoryHandles[6].GetData(); \
		const float StretchStartRatio = *(float*)RigVMMemoryHandles[7].GetData(); \
		const float StretchMaximumRatio = *(float*)RigVMMemoryHandles[8].GetData(); \
		const float BoneALength = *(float*)RigVMMemoryHandles[9].GetData(); \
		const float BoneBLength = *(float*)RigVMMemoryHandles[10].GetData(); \
		FTransform& Elbow = *(FTransform*)RigVMMemoryHandles[11].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Root, \
			PoleVector, \
			Effector, \
			PrimaryAxis, \
			SecondaryAxis, \
			SecondaryAxisWeight, \
			bEnableStretch, \
			StretchStartRatio, \
			StretchMaximumRatio, \
			BoneALength, \
			BoneBLength, \
			Elbow, \
			Context \
		); \
	} \
	typedef FRigUnit_HighlevelBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_TwoBoneIKSimpleTransforms>();


#define FRigUnit_TwoBoneIKSimpleVectors_Execute() \
	void FRigUnit_TwoBoneIKSimpleVectors::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FVector& Root, \
		const FVector& PoleVector, \
		FVector& Effector, \
		const bool bEnableStretch, \
		const float StretchStartRatio, \
		const float StretchMaximumRatio, \
		const float BoneALength, \
		const float BoneBLength, \
		FVector& Elbow, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_TwoBoneIKSimple_h_380_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FVector& Root, \
		const FVector& PoleVector, \
		FVector& Effector, \
		const bool bEnableStretch, \
		const float StretchStartRatio, \
		const float StretchMaximumRatio, \
		const float BoneALength, \
		const float BoneBLength, \
		FVector& Elbow, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FVector& Root = *(FVector*)RigVMMemoryHandles[0].GetData(); \
		const FVector& PoleVector = *(FVector*)RigVMMemoryHandles[1].GetData(); \
		FVector& Effector = *(FVector*)RigVMMemoryHandles[2].GetData(); \
		const bool bEnableStretch = *(bool*)RigVMMemoryHandles[3].GetData(); \
		const float StretchStartRatio = *(float*)RigVMMemoryHandles[4].GetData(); \
		const float StretchMaximumRatio = *(float*)RigVMMemoryHandles[5].GetData(); \
		const float BoneALength = *(float*)RigVMMemoryHandles[6].GetData(); \
		const float BoneBLength = *(float*)RigVMMemoryHandles[7].GetData(); \
		FVector& Elbow = *(FVector*)RigVMMemoryHandles[8].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Root, \
			PoleVector, \
			Effector, \
			bEnableStretch, \
			StretchStartRatio, \
			StretchMaximumRatio, \
			BoneALength, \
			BoneBLength, \
			Elbow, \
			Context \
		); \
	} \
	typedef FRigUnit_HighlevelBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_TwoBoneIKSimpleVectors>();


#define FRigUnit_TwoBoneIKSimplePerItem_Execute() \
	void FRigUnit_TwoBoneIKSimplePerItem::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKey& ItemA, \
		const FRigElementKey& ItemB, \
		const FRigElementKey& EffectorItem, \
		const FTransform& Effector, \
		const FVector& PrimaryAxis, \
		const FVector& SecondaryAxis, \
		const float SecondaryAxisWeight, \
		const FVector& PoleVector, \
		const EControlRigVectorKind PoleVectorKind, \
		const FRigElementKey& PoleVectorSpace, \
		const bool bEnableStretch, \
		const float StretchStartRatio, \
		const float StretchMaximumRatio, \
		const float Weight, \
		const float ItemALength, \
		const float ItemBLength, \
		const bool bPropagateToChildren, \
		const FRigUnit_TwoBoneIKSimple_DebugSettings& DebugSettings, \
		FCachedRigElement& CachedItemAIndex, \
		FCachedRigElement& CachedItemBIndex, \
		FCachedRigElement& CachedEffectorItemIndex, \
		FCachedRigElement& CachedPoleVectorSpaceIndex, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_TwoBoneIKSimple_h_213_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKey& ItemA, \
		const FRigElementKey& ItemB, \
		const FRigElementKey& EffectorItem, \
		const FTransform& Effector, \
		const FVector& PrimaryAxis, \
		const FVector& SecondaryAxis, \
		const float SecondaryAxisWeight, \
		const FVector& PoleVector, \
		const EControlRigVectorKind PoleVectorKind, \
		const FRigElementKey& PoleVectorSpace, \
		const bool bEnableStretch, \
		const float StretchStartRatio, \
		const float StretchMaximumRatio, \
		const float Weight, \
		const float ItemALength, \
		const float ItemBLength, \
		const bool bPropagateToChildren, \
		const FRigUnit_TwoBoneIKSimple_DebugSettings& DebugSettings, \
		FCachedRigElement& CachedItemAIndex, \
		FCachedRigElement& CachedItemBIndex, \
		FCachedRigElement& CachedEffectorItemIndex, \
		FCachedRigElement& CachedPoleVectorSpaceIndex, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FRigElementKey& ItemA = *(FRigElementKey*)RigVMMemoryHandles[0].GetData(); \
		const FRigElementKey& ItemB = *(FRigElementKey*)RigVMMemoryHandles[1].GetData(); \
		const FRigElementKey& EffectorItem = *(FRigElementKey*)RigVMMemoryHandles[2].GetData(); \
		const FTransform& Effector = *(FTransform*)RigVMMemoryHandles[3].GetData(); \
		const FVector& PrimaryAxis = *(FVector*)RigVMMemoryHandles[4].GetData(); \
		const FVector& SecondaryAxis = *(FVector*)RigVMMemoryHandles[5].GetData(); \
		const float SecondaryAxisWeight = *(float*)RigVMMemoryHandles[6].GetData(); \
		const FVector& PoleVector = *(FVector*)RigVMMemoryHandles[7].GetData(); \
		EControlRigVectorKind PoleVectorKind = (EControlRigVectorKind)*(uint8*)RigVMMemoryHandles[8].GetData(); \
		const FRigElementKey& PoleVectorSpace = *(FRigElementKey*)RigVMMemoryHandles[9].GetData(); \
		const bool bEnableStretch = *(bool*)RigVMMemoryHandles[10].GetData(); \
		const float StretchStartRatio = *(float*)RigVMMemoryHandles[11].GetData(); \
		const float StretchMaximumRatio = *(float*)RigVMMemoryHandles[12].GetData(); \
		const float Weight = *(float*)RigVMMemoryHandles[13].GetData(); \
		const float ItemALength = *(float*)RigVMMemoryHandles[14].GetData(); \
		const float ItemBLength = *(float*)RigVMMemoryHandles[15].GetData(); \
		const bool bPropagateToChildren = *(bool*)RigVMMemoryHandles[16].GetData(); \
		const FRigUnit_TwoBoneIKSimple_DebugSettings& DebugSettings = *(FRigUnit_TwoBoneIKSimple_DebugSettings*)RigVMMemoryHandles[17].GetData(); \
		FRigVMDynamicArray<FCachedRigElement> CachedItemAIndex_18_Array(*((FRigVMByteArray*)RigVMMemoryHandles[18].GetData(0, false))); \
		CachedItemAIndex_18_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedItemAIndex = CachedItemAIndex_18_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<FCachedRigElement> CachedItemBIndex_19_Array(*((FRigVMByteArray*)RigVMMemoryHandles[19].GetData(0, false))); \
		CachedItemBIndex_19_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedItemBIndex = CachedItemBIndex_19_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<FCachedRigElement> CachedEffectorItemIndex_20_Array(*((FRigVMByteArray*)RigVMMemoryHandles[20].GetData(0, false))); \
		CachedEffectorItemIndex_20_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedEffectorItemIndex = CachedEffectorItemIndex_20_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<FCachedRigElement> CachedPoleVectorSpaceIndex_21_Array(*((FRigVMByteArray*)RigVMMemoryHandles[21].GetData(0, false))); \
		CachedPoleVectorSpaceIndex_21_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedPoleVectorSpaceIndex = CachedPoleVectorSpaceIndex_21_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[22].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			ItemA, \
			ItemB, \
			EffectorItem, \
			Effector, \
			PrimaryAxis, \
			SecondaryAxis, \
			SecondaryAxisWeight, \
			PoleVector, \
			PoleVectorKind, \
			PoleVectorSpace, \
			bEnableStretch, \
			StretchStartRatio, \
			StretchMaximumRatio, \
			Weight, \
			ItemALength, \
			ItemBLength, \
			bPropagateToChildren, \
			DebugSettings, \
			CachedItemAIndex, \
			CachedItemBIndex, \
			CachedEffectorItemIndex, \
			CachedPoleVectorSpaceIndex, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnit_HighlevelBaseMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_TwoBoneIKSimplePerItem>();


#define FRigUnit_TwoBoneIKSimple_Execute() \
	void FRigUnit_TwoBoneIKSimple::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& BoneA, \
		const FName& BoneB, \
		const FName& EffectorBone, \
		const FTransform& Effector, \
		const FVector& PrimaryAxis, \
		const FVector& SecondaryAxis, \
		const float SecondaryAxisWeight, \
		const FVector& PoleVector, \
		const EControlRigVectorKind PoleVectorKind, \
		const FName& PoleVectorSpace, \
		const bool bEnableStretch, \
		const float StretchStartRatio, \
		const float StretchMaximumRatio, \
		const float Weight, \
		const float BoneALength, \
		const float BoneBLength, \
		const bool bPropagateToChildren, \
		const FRigUnit_TwoBoneIKSimple_DebugSettings& DebugSettings, \
		FCachedRigElement& CachedBoneAIndex, \
		FCachedRigElement& CachedBoneBIndex, \
		FCachedRigElement& CachedEffectorBoneIndex, \
		FCachedRigElement& CachedPoleVectorSpaceIndex, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_TwoBoneIKSimple_h_46_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& BoneA, \
		const FName& BoneB, \
		const FName& EffectorBone, \
		const FTransform& Effector, \
		const FVector& PrimaryAxis, \
		const FVector& SecondaryAxis, \
		const float SecondaryAxisWeight, \
		const FVector& PoleVector, \
		const EControlRigVectorKind PoleVectorKind, \
		const FName& PoleVectorSpace, \
		const bool bEnableStretch, \
		const float StretchStartRatio, \
		const float StretchMaximumRatio, \
		const float Weight, \
		const float BoneALength, \
		const float BoneBLength, \
		const bool bPropagateToChildren, \
		const FRigUnit_TwoBoneIKSimple_DebugSettings& DebugSettings, \
		FCachedRigElement& CachedBoneAIndex, \
		FCachedRigElement& CachedBoneBIndex, \
		FCachedRigElement& CachedEffectorBoneIndex, \
		FCachedRigElement& CachedPoleVectorSpaceIndex, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FName& BoneA = *(FName*)RigVMMemoryHandles[0].GetData(); \
		const FName& BoneB = *(FName*)RigVMMemoryHandles[1].GetData(); \
		const FName& EffectorBone = *(FName*)RigVMMemoryHandles[2].GetData(); \
		const FTransform& Effector = *(FTransform*)RigVMMemoryHandles[3].GetData(); \
		const FVector& PrimaryAxis = *(FVector*)RigVMMemoryHandles[4].GetData(); \
		const FVector& SecondaryAxis = *(FVector*)RigVMMemoryHandles[5].GetData(); \
		const float SecondaryAxisWeight = *(float*)RigVMMemoryHandles[6].GetData(); \
		const FVector& PoleVector = *(FVector*)RigVMMemoryHandles[7].GetData(); \
		EControlRigVectorKind PoleVectorKind = (EControlRigVectorKind)*(uint8*)RigVMMemoryHandles[8].GetData(); \
		const FName& PoleVectorSpace = *(FName*)RigVMMemoryHandles[9].GetData(); \
		const bool bEnableStretch = *(bool*)RigVMMemoryHandles[10].GetData(); \
		const float StretchStartRatio = *(float*)RigVMMemoryHandles[11].GetData(); \
		const float StretchMaximumRatio = *(float*)RigVMMemoryHandles[12].GetData(); \
		const float Weight = *(float*)RigVMMemoryHandles[13].GetData(); \
		const float BoneALength = *(float*)RigVMMemoryHandles[14].GetData(); \
		const float BoneBLength = *(float*)RigVMMemoryHandles[15].GetData(); \
		const bool bPropagateToChildren = *(bool*)RigVMMemoryHandles[16].GetData(); \
		const FRigUnit_TwoBoneIKSimple_DebugSettings& DebugSettings = *(FRigUnit_TwoBoneIKSimple_DebugSettings*)RigVMMemoryHandles[17].GetData(); \
		FRigVMDynamicArray<FCachedRigElement> CachedBoneAIndex_18_Array(*((FRigVMByteArray*)RigVMMemoryHandles[18].GetData(0, false))); \
		CachedBoneAIndex_18_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedBoneAIndex = CachedBoneAIndex_18_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<FCachedRigElement> CachedBoneBIndex_19_Array(*((FRigVMByteArray*)RigVMMemoryHandles[19].GetData(0, false))); \
		CachedBoneBIndex_19_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedBoneBIndex = CachedBoneBIndex_19_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<FCachedRigElement> CachedEffectorBoneIndex_20_Array(*((FRigVMByteArray*)RigVMMemoryHandles[20].GetData(0, false))); \
		CachedEffectorBoneIndex_20_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedEffectorBoneIndex = CachedEffectorBoneIndex_20_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<FCachedRigElement> CachedPoleVectorSpaceIndex_21_Array(*((FRigVMByteArray*)RigVMMemoryHandles[21].GetData(0, false))); \
		CachedPoleVectorSpaceIndex_21_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedPoleVectorSpaceIndex = CachedPoleVectorSpaceIndex_21_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[22].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			BoneA, \
			BoneB, \
			EffectorBone, \
			Effector, \
			PrimaryAxis, \
			SecondaryAxis, \
			SecondaryAxisWeight, \
			PoleVector, \
			PoleVectorKind, \
			PoleVectorSpace, \
			bEnableStretch, \
			StretchStartRatio, \
			StretchMaximumRatio, \
			Weight, \
			BoneALength, \
			BoneBLength, \
			bPropagateToChildren, \
			DebugSettings, \
			CachedBoneAIndex, \
			CachedBoneBIndex, \
			CachedEffectorBoneIndex, \
			CachedPoleVectorSpaceIndex, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnit_HighlevelBaseMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_TwoBoneIKSimple>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_TwoBoneIKSimple_h_11_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_DebugSettings_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_TwoBoneIKSimple_DebugSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_TwoBoneIKSimple_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
