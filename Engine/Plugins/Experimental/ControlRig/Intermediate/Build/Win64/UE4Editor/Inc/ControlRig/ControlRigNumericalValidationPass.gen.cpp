// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Validation/ControlRigNumericalValidationPass.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeControlRigNumericalValidationPass() {}
// Cross Module References
	CONTROLRIG_API UClass* Z_Construct_UClass_UControlRigNumericalValidationPass_NoRegister();
	CONTROLRIG_API UClass* Z_Construct_UClass_UControlRigNumericalValidationPass();
	CONTROLRIG_API UClass* Z_Construct_UClass_UControlRigValidationPass();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigPose();
// End Cross Module References
	void UControlRigNumericalValidationPass::StaticRegisterNativesUControlRigNumericalValidationPass()
	{
	}
	UClass* Z_Construct_UClass_UControlRigNumericalValidationPass_NoRegister()
	{
		return UControlRigNumericalValidationPass::StaticClass();
	}
	struct Z_Construct_UClass_UControlRigNumericalValidationPass_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCheckControls_MetaData[];
#endif
		static void NewProp_bCheckControls_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCheckControls;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCheckBones_MetaData[];
#endif
		static void NewProp_bCheckBones_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCheckBones;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCheckCurves_MetaData[];
#endif
		static void NewProp_bCheckCurves_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCheckCurves;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TranslationPrecision_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TranslationPrecision;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RotationPrecision_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RotationPrecision;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScalePrecision_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScalePrecision;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurvePrecision_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CurvePrecision;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EventNameA_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_EventNameA;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EventNameB_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_EventNameB;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Pose_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Pose;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UControlRigValidationPass,
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Used to perform a numerical comparison of the poses */" },
		{ "DisplayName", "Numerical Comparison" },
		{ "IncludePath", "Validation/ControlRigNumericalValidationPass.h" },
		{ "ModuleRelativePath", "Private/Validation/ControlRigNumericalValidationPass.h" },
		{ "ToolTip", "Used to perform a numerical comparison of the poses" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_bCheckControls_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "// If set to true the pass will validate the poses of all bones\n" },
		{ "ModuleRelativePath", "Private/Validation/ControlRigNumericalValidationPass.h" },
		{ "ToolTip", "If set to true the pass will validate the poses of all bones" },
	};
#endif
	void Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_bCheckControls_SetBit(void* Obj)
	{
		((UControlRigNumericalValidationPass*)Obj)->bCheckControls = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_bCheckControls = { "bCheckControls", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UControlRigNumericalValidationPass), &Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_bCheckControls_SetBit, METADATA_PARAMS(Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_bCheckControls_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_bCheckControls_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_bCheckBones_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "// If set to true the pass will validate the poses of all bones\n" },
		{ "ModuleRelativePath", "Private/Validation/ControlRigNumericalValidationPass.h" },
		{ "ToolTip", "If set to true the pass will validate the poses of all bones" },
	};
#endif
	void Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_bCheckBones_SetBit(void* Obj)
	{
		((UControlRigNumericalValidationPass*)Obj)->bCheckBones = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_bCheckBones = { "bCheckBones", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UControlRigNumericalValidationPass), &Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_bCheckBones_SetBit, METADATA_PARAMS(Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_bCheckBones_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_bCheckBones_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_bCheckCurves_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "// If set to true the pass will validate the values of all curves\n" },
		{ "ModuleRelativePath", "Private/Validation/ControlRigNumericalValidationPass.h" },
		{ "ToolTip", "If set to true the pass will validate the values of all curves" },
	};
#endif
	void Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_bCheckCurves_SetBit(void* Obj)
	{
		((UControlRigNumericalValidationPass*)Obj)->bCheckCurves = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_bCheckCurves = { "bCheckCurves", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UControlRigNumericalValidationPass), &Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_bCheckCurves_SetBit, METADATA_PARAMS(Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_bCheckCurves_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_bCheckCurves_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_TranslationPrecision_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "// The threshold under which we'll ignore a precision issue in the pass\n" },
		{ "ModuleRelativePath", "Private/Validation/ControlRigNumericalValidationPass.h" },
		{ "ToolTip", "The threshold under which we'll ignore a precision issue in the pass" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_TranslationPrecision = { "TranslationPrecision", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigNumericalValidationPass, TranslationPrecision), METADATA_PARAMS(Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_TranslationPrecision_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_TranslationPrecision_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_RotationPrecision_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "// The threshold under which we'll ignore a precision issue in the pass (in degrees)\n" },
		{ "ModuleRelativePath", "Private/Validation/ControlRigNumericalValidationPass.h" },
		{ "ToolTip", "The threshold under which we'll ignore a precision issue in the pass (in degrees)" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_RotationPrecision = { "RotationPrecision", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigNumericalValidationPass, RotationPrecision), METADATA_PARAMS(Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_RotationPrecision_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_RotationPrecision_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_ScalePrecision_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "// The threshold under which we'll ignore a precision issue in the pass\n" },
		{ "ModuleRelativePath", "Private/Validation/ControlRigNumericalValidationPass.h" },
		{ "ToolTip", "The threshold under which we'll ignore a precision issue in the pass" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_ScalePrecision = { "ScalePrecision", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigNumericalValidationPass, ScalePrecision), METADATA_PARAMS(Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_ScalePrecision_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_ScalePrecision_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_CurvePrecision_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "// The threshold under which we'll ignore a precision issue in the pass\n" },
		{ "ModuleRelativePath", "Private/Validation/ControlRigNumericalValidationPass.h" },
		{ "ToolTip", "The threshold under which we'll ignore a precision issue in the pass" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_CurvePrecision = { "CurvePrecision", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigNumericalValidationPass, CurvePrecision), METADATA_PARAMS(Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_CurvePrecision_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_CurvePrecision_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_EventNameA_MetaData[] = {
		{ "ModuleRelativePath", "Private/Validation/ControlRigNumericalValidationPass.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_EventNameA = { "EventNameA", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigNumericalValidationPass, EventNameA), METADATA_PARAMS(Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_EventNameA_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_EventNameA_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_EventNameB_MetaData[] = {
		{ "ModuleRelativePath", "Private/Validation/ControlRigNumericalValidationPass.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_EventNameB = { "EventNameB", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigNumericalValidationPass, EventNameB), METADATA_PARAMS(Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_EventNameB_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_EventNameB_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_Pose_MetaData[] = {
		{ "ModuleRelativePath", "Private/Validation/ControlRigNumericalValidationPass.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_Pose = { "Pose", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigNumericalValidationPass, Pose), Z_Construct_UScriptStruct_FRigPose, METADATA_PARAMS(Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_Pose_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_Pose_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_bCheckControls,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_bCheckBones,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_bCheckCurves,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_TranslationPrecision,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_RotationPrecision,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_ScalePrecision,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_CurvePrecision,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_EventNameA,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_EventNameB,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::NewProp_Pose,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UControlRigNumericalValidationPass>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::ClassParams = {
		&UControlRigNumericalValidationPass::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UControlRigNumericalValidationPass()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UControlRigNumericalValidationPass_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UControlRigNumericalValidationPass, 2040664268);
	template<> CONTROLRIG_API UClass* StaticClass<UControlRigNumericalValidationPass>()
	{
		return UControlRigNumericalValidationPass::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UControlRigNumericalValidationPass(Z_Construct_UClass_UControlRigNumericalValidationPass, &UControlRigNumericalValidationPass::StaticClass, TEXT("/Script/ControlRig"), TEXT("UControlRigNumericalValidationPass"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UControlRigNumericalValidationPass);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
