// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Math/RigUnit_MathFloat.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_MathFloat() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatBase();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatAtan();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatUnaryOp();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatAcos();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatAsin();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatTan();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatCos();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatSin();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatRad();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatDeg();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatSelectBool();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyEqual();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyZero();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatLessEqual();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatGreaterEqual();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatLess();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatGreater();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatNotEquals();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatEquals();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatLerp();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatClamp();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatSign();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatToInt();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatRound();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatCeil();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatFloor();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatAbs();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatNegate();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatSqrt();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatPow();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatBinaryOp();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatMax();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatMin();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatMod();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatDiv();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatMul();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatSub();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatAdd();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatConstTwoPi();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatConstant();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatConstHalfPi();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatConstPi();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathBase();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_MathFloatLawOfCosine>() == std::is_polymorphic<FRigUnit_MathFloatBase>(), "USTRUCT FRigUnit_MathFloatLawOfCosine cannot be polymorphic unless super FRigUnit_MathFloatBase is polymorphic");

class UScriptStruct* FRigUnit_MathFloatLawOfCosine::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatLawOfCosine"), sizeof(FRigUnit_MathFloatLawOfCosine), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathFloatLawOfCosine::Execute"), &FRigUnit_MathFloatLawOfCosine::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatLawOfCosine>()
{
	return FRigUnit_MathFloatLawOfCosine::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatLawOfCosine(FRigUnit_MathFloatLawOfCosine::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatLawOfCosine"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatLawOfCosine
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatLawOfCosine()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatLawOfCosine>(FName(TEXT("RigUnit_MathFloatLawOfCosine")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatLawOfCosine;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_A_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_A;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_B_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_B;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_C_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_C;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AlphaAngle_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AlphaAngle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BetaAngle_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BetaAngle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GammaAngle_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_GammaAngle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bValid_MetaData[];
#endif
		static void NewProp_bValid_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bValid;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Computes the angles alpha, beta and gamma (in radians) between the three sides A, B and C\n */" },
		{ "DisplayName", "Law Of Cosine" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "ToolTip", "Computes the angles alpha, beta and gamma (in radians) between the three sides A, B and C" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatLawOfCosine>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::NewProp_A_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::NewProp_A = { "A", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatLawOfCosine, A), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::NewProp_A_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::NewProp_A_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::NewProp_B_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::NewProp_B = { "B", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatLawOfCosine, B), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::NewProp_B_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::NewProp_B_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::NewProp_C_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::NewProp_C = { "C", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatLawOfCosine, C), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::NewProp_C_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::NewProp_C_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::NewProp_AlphaAngle_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::NewProp_AlphaAngle = { "AlphaAngle", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatLawOfCosine, AlphaAngle), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::NewProp_AlphaAngle_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::NewProp_AlphaAngle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::NewProp_BetaAngle_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::NewProp_BetaAngle = { "BetaAngle", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatLawOfCosine, BetaAngle), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::NewProp_BetaAngle_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::NewProp_BetaAngle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::NewProp_GammaAngle_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::NewProp_GammaAngle = { "GammaAngle", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatLawOfCosine, GammaAngle), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::NewProp_GammaAngle_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::NewProp_GammaAngle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::NewProp_bValid_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "Output", "" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::NewProp_bValid_SetBit(void* Obj)
	{
		((FRigUnit_MathFloatLawOfCosine*)Obj)->bValid = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::NewProp_bValid = { "bValid", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_MathFloatLawOfCosine), &Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::NewProp_bValid_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::NewProp_bValid_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::NewProp_bValid_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::NewProp_A,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::NewProp_B,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::NewProp_C,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::NewProp_AlphaAngle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::NewProp_BetaAngle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::NewProp_GammaAngle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::NewProp_bValid,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatBase,
		&NewStructOps,
		"RigUnit_MathFloatLawOfCosine",
		sizeof(FRigUnit_MathFloatLawOfCosine),
		alignof(FRigUnit_MathFloatLawOfCosine),
		Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatLawOfCosine"), sizeof(FRigUnit_MathFloatLawOfCosine), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatLawOfCosine_Hash() { return 3571501614U; }

void FRigUnit_MathFloatLawOfCosine::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		C,
		AlphaAngle,
		BetaAngle,
		GammaAngle,
		bValid,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathFloatAtan>() == std::is_polymorphic<FRigUnit_MathFloatUnaryOp>(), "USTRUCT FRigUnit_MathFloatAtan cannot be polymorphic unless super FRigUnit_MathFloatUnaryOp is polymorphic");

class UScriptStruct* FRigUnit_MathFloatAtan::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatAtan_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatAtan, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatAtan"), sizeof(FRigUnit_MathFloatAtan), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatAtan_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathFloatAtan::Execute"), &FRigUnit_MathFloatAtan::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatAtan>()
{
	return FRigUnit_MathFloatAtan::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatAtan(FRigUnit_MathFloatAtan::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatAtan"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatAtan
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatAtan()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatAtan>(FName(TEXT("RigUnit_MathFloatAtan")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatAtan;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatAtan_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatAtan_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the inverse tangens value (in radians) of the given value\n */" },
		{ "DisplayName", "Atan" },
		{ "Keywords", "Arctan" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "PrototypeName", "Atan" },
		{ "ToolTip", "Returns the inverse tangens value (in radians) of the given value" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatAtan_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatAtan>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatAtan_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatUnaryOp,
		&NewStructOps,
		"RigUnit_MathFloatAtan",
		sizeof(FRigUnit_MathFloatAtan),
		alignof(FRigUnit_MathFloatAtan),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatAtan_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatAtan_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatAtan()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatAtan_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatAtan"), sizeof(FRigUnit_MathFloatAtan), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatAtan_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatAtan_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatAtan_Hash() { return 562663913U; }

void FRigUnit_MathFloatAtan::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathFloatAcos>() == std::is_polymorphic<FRigUnit_MathFloatUnaryOp>(), "USTRUCT FRigUnit_MathFloatAcos cannot be polymorphic unless super FRigUnit_MathFloatUnaryOp is polymorphic");

class UScriptStruct* FRigUnit_MathFloatAcos::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatAcos_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatAcos, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatAcos"), sizeof(FRigUnit_MathFloatAcos), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatAcos_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathFloatAcos::Execute"), &FRigUnit_MathFloatAcos::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatAcos>()
{
	return FRigUnit_MathFloatAcos::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatAcos(FRigUnit_MathFloatAcos::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatAcos"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatAcos
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatAcos()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatAcos>(FName(TEXT("RigUnit_MathFloatAcos")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatAcos;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatAcos_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatAcos_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the inverse cosinus value (in radians) of the given value\n */" },
		{ "DisplayName", "Acos" },
		{ "Keywords", "Arccos" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "PrototypeName", "Acos" },
		{ "ToolTip", "Returns the inverse cosinus value (in radians) of the given value" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatAcos_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatAcos>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatAcos_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatUnaryOp,
		&NewStructOps,
		"RigUnit_MathFloatAcos",
		sizeof(FRigUnit_MathFloatAcos),
		alignof(FRigUnit_MathFloatAcos),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatAcos_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatAcos_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatAcos()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatAcos_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatAcos"), sizeof(FRigUnit_MathFloatAcos), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatAcos_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatAcos_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatAcos_Hash() { return 2610402924U; }

void FRigUnit_MathFloatAcos::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathFloatAsin>() == std::is_polymorphic<FRigUnit_MathFloatUnaryOp>(), "USTRUCT FRigUnit_MathFloatAsin cannot be polymorphic unless super FRigUnit_MathFloatUnaryOp is polymorphic");

class UScriptStruct* FRigUnit_MathFloatAsin::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatAsin_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatAsin, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatAsin"), sizeof(FRigUnit_MathFloatAsin), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatAsin_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathFloatAsin::Execute"), &FRigUnit_MathFloatAsin::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatAsin>()
{
	return FRigUnit_MathFloatAsin::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatAsin(FRigUnit_MathFloatAsin::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatAsin"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatAsin
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatAsin()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatAsin>(FName(TEXT("RigUnit_MathFloatAsin")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatAsin;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatAsin_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatAsin_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the inverse sinus value (in radians) of the given value\n */" },
		{ "DisplayName", "Asin" },
		{ "Keywords", "Arcsin" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "PrototypeName", "Asin" },
		{ "ToolTip", "Returns the inverse sinus value (in radians) of the given value" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatAsin_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatAsin>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatAsin_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatUnaryOp,
		&NewStructOps,
		"RigUnit_MathFloatAsin",
		sizeof(FRigUnit_MathFloatAsin),
		alignof(FRigUnit_MathFloatAsin),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatAsin_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatAsin_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatAsin()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatAsin_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatAsin"), sizeof(FRigUnit_MathFloatAsin), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatAsin_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatAsin_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatAsin_Hash() { return 3040556630U; }

void FRigUnit_MathFloatAsin::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathFloatTan>() == std::is_polymorphic<FRigUnit_MathFloatUnaryOp>(), "USTRUCT FRigUnit_MathFloatTan cannot be polymorphic unless super FRigUnit_MathFloatUnaryOp is polymorphic");

class UScriptStruct* FRigUnit_MathFloatTan::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatTan_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatTan, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatTan"), sizeof(FRigUnit_MathFloatTan), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatTan_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathFloatTan::Execute"), &FRigUnit_MathFloatTan::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatTan>()
{
	return FRigUnit_MathFloatTan::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatTan(FRigUnit_MathFloatTan::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatTan"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatTan
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatTan()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatTan>(FName(TEXT("RigUnit_MathFloatTan")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatTan;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatTan_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatTan_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the tangens value of the given value (in radians)\n */" },
		{ "DisplayName", "Tan" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "PrototypeName", "Tan" },
		{ "ToolTip", "Returns the tangens value of the given value (in radians)" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatTan_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatTan>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatTan_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatUnaryOp,
		&NewStructOps,
		"RigUnit_MathFloatTan",
		sizeof(FRigUnit_MathFloatTan),
		alignof(FRigUnit_MathFloatTan),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatTan_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatTan_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatTan()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatTan_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatTan"), sizeof(FRigUnit_MathFloatTan), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatTan_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatTan_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatTan_Hash() { return 1313915967U; }

void FRigUnit_MathFloatTan::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathFloatCos>() == std::is_polymorphic<FRigUnit_MathFloatUnaryOp>(), "USTRUCT FRigUnit_MathFloatCos cannot be polymorphic unless super FRigUnit_MathFloatUnaryOp is polymorphic");

class UScriptStruct* FRigUnit_MathFloatCos::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatCos_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatCos, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatCos"), sizeof(FRigUnit_MathFloatCos), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatCos_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathFloatCos::Execute"), &FRigUnit_MathFloatCos::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatCos>()
{
	return FRigUnit_MathFloatCos::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatCos(FRigUnit_MathFloatCos::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatCos"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatCos
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatCos()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatCos>(FName(TEXT("RigUnit_MathFloatCos")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatCos;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatCos_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatCos_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the cosinus value of the given value (in radians)\n */" },
		{ "DisplayName", "Cos" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "PrototypeName", "Cos" },
		{ "ToolTip", "Returns the cosinus value of the given value (in radians)" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatCos_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatCos>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatCos_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatUnaryOp,
		&NewStructOps,
		"RigUnit_MathFloatCos",
		sizeof(FRigUnit_MathFloatCos),
		alignof(FRigUnit_MathFloatCos),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatCos_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatCos_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatCos()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatCos_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatCos"), sizeof(FRigUnit_MathFloatCos), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatCos_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatCos_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatCos_Hash() { return 4273552450U; }

void FRigUnit_MathFloatCos::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathFloatSin>() == std::is_polymorphic<FRigUnit_MathFloatUnaryOp>(), "USTRUCT FRigUnit_MathFloatSin cannot be polymorphic unless super FRigUnit_MathFloatUnaryOp is polymorphic");

class UScriptStruct* FRigUnit_MathFloatSin::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatSin_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatSin, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatSin"), sizeof(FRigUnit_MathFloatSin), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatSin_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathFloatSin::Execute"), &FRigUnit_MathFloatSin::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatSin>()
{
	return FRigUnit_MathFloatSin::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatSin(FRigUnit_MathFloatSin::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatSin"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatSin
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatSin()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatSin>(FName(TEXT("RigUnit_MathFloatSin")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatSin;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatSin_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatSin_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the sinus value of the given value (in radians)\n */" },
		{ "DisplayName", "Sin" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "PrototypeName", "Sin" },
		{ "ToolTip", "Returns the sinus value of the given value (in radians)" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatSin_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatSin>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatSin_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatUnaryOp,
		&NewStructOps,
		"RigUnit_MathFloatSin",
		sizeof(FRigUnit_MathFloatSin),
		alignof(FRigUnit_MathFloatSin),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatSin_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatSin_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatSin()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatSin_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatSin"), sizeof(FRigUnit_MathFloatSin), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatSin_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatSin_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatSin_Hash() { return 3578395733U; }

void FRigUnit_MathFloatSin::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathFloatRad>() == std::is_polymorphic<FRigUnit_MathFloatUnaryOp>(), "USTRUCT FRigUnit_MathFloatRad cannot be polymorphic unless super FRigUnit_MathFloatUnaryOp is polymorphic");

class UScriptStruct* FRigUnit_MathFloatRad::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatRad_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatRad, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatRad"), sizeof(FRigUnit_MathFloatRad), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatRad_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathFloatRad::Execute"), &FRigUnit_MathFloatRad::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatRad>()
{
	return FRigUnit_MathFloatRad::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatRad(FRigUnit_MathFloatRad::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatRad"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatRad
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatRad()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatRad>(FName(TEXT("RigUnit_MathFloatRad")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatRad;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatRad_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatRad_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the radians of a given value in degrees\n */" },
		{ "DisplayName", "Radians" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "PrototypeName", "Radians" },
		{ "ToolTip", "Returns the radians of a given value in degrees" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatRad_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatRad>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatRad_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatUnaryOp,
		&NewStructOps,
		"RigUnit_MathFloatRad",
		sizeof(FRigUnit_MathFloatRad),
		alignof(FRigUnit_MathFloatRad),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatRad_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatRad_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatRad()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatRad_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatRad"), sizeof(FRigUnit_MathFloatRad), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatRad_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatRad_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatRad_Hash() { return 3139582083U; }

void FRigUnit_MathFloatRad::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathFloatDeg>() == std::is_polymorphic<FRigUnit_MathFloatUnaryOp>(), "USTRUCT FRigUnit_MathFloatDeg cannot be polymorphic unless super FRigUnit_MathFloatUnaryOp is polymorphic");

class UScriptStruct* FRigUnit_MathFloatDeg::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatDeg_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatDeg, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatDeg"), sizeof(FRigUnit_MathFloatDeg), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatDeg_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathFloatDeg::Execute"), &FRigUnit_MathFloatDeg::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatDeg>()
{
	return FRigUnit_MathFloatDeg::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatDeg(FRigUnit_MathFloatDeg::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatDeg"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatDeg
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatDeg()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatDeg>(FName(TEXT("RigUnit_MathFloatDeg")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatDeg;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatDeg_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatDeg_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the degrees of a given value in radians\n */" },
		{ "DisplayName", "Degrees" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "PrototypeName", "Degrees" },
		{ "ToolTip", "Returns the degrees of a given value in radians" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatDeg_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatDeg>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatDeg_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatUnaryOp,
		&NewStructOps,
		"RigUnit_MathFloatDeg",
		sizeof(FRigUnit_MathFloatDeg),
		alignof(FRigUnit_MathFloatDeg),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatDeg_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatDeg_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatDeg()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatDeg_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatDeg"), sizeof(FRigUnit_MathFloatDeg), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatDeg_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatDeg_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatDeg_Hash() { return 561466623U; }

void FRigUnit_MathFloatDeg::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathFloatSelectBool>() == std::is_polymorphic<FRigUnit_MathFloatBase>(), "USTRUCT FRigUnit_MathFloatSelectBool cannot be polymorphic unless super FRigUnit_MathFloatBase is polymorphic");

class UScriptStruct* FRigUnit_MathFloatSelectBool::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatSelectBool_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatSelectBool, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatSelectBool"), sizeof(FRigUnit_MathFloatSelectBool), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatSelectBool_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathFloatSelectBool::Execute"), &FRigUnit_MathFloatSelectBool::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatSelectBool>()
{
	return FRigUnit_MathFloatSelectBool::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatSelectBool(FRigUnit_MathFloatSelectBool::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatSelectBool"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatSelectBool
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatSelectBool()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatSelectBool>(FName(TEXT("RigUnit_MathFloatSelectBool")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatSelectBool;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatSelectBool_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Condition_MetaData[];
#endif
		static void NewProp_Condition_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Condition;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IfTrue_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_IfTrue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IfFalse_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_IfFalse;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatSelectBool_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Return one of the two values based on the condition\n */" },
		{ "Deprecated", "4.26.0" },
		{ "DisplayName", "Select" },
		{ "Keywords", "Pick,If" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "PrototypeName", "Select" },
		{ "ToolTip", "Return one of the two values based on the condition" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatSelectBool_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatSelectBool>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatSelectBool_Statics::NewProp_Condition_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_MathFloatSelectBool_Statics::NewProp_Condition_SetBit(void* Obj)
	{
		((FRigUnit_MathFloatSelectBool*)Obj)->Condition = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatSelectBool_Statics::NewProp_Condition = { "Condition", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_MathFloatSelectBool), &Z_Construct_UScriptStruct_FRigUnit_MathFloatSelectBool_Statics::NewProp_Condition_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatSelectBool_Statics::NewProp_Condition_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatSelectBool_Statics::NewProp_Condition_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatSelectBool_Statics::NewProp_IfTrue_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatSelectBool_Statics::NewProp_IfTrue = { "IfTrue", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatSelectBool, IfTrue), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatSelectBool_Statics::NewProp_IfTrue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatSelectBool_Statics::NewProp_IfTrue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatSelectBool_Statics::NewProp_IfFalse_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatSelectBool_Statics::NewProp_IfFalse = { "IfFalse", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatSelectBool, IfFalse), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatSelectBool_Statics::NewProp_IfFalse_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatSelectBool_Statics::NewProp_IfFalse_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatSelectBool_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatSelectBool_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatSelectBool, Result), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatSelectBool_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatSelectBool_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathFloatSelectBool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatSelectBool_Statics::NewProp_Condition,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatSelectBool_Statics::NewProp_IfTrue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatSelectBool_Statics::NewProp_IfFalse,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatSelectBool_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatSelectBool_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatBase,
		&NewStructOps,
		"RigUnit_MathFloatSelectBool",
		sizeof(FRigUnit_MathFloatSelectBool),
		alignof(FRigUnit_MathFloatSelectBool),
		Z_Construct_UScriptStruct_FRigUnit_MathFloatSelectBool_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatSelectBool_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatSelectBool_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatSelectBool_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatSelectBool()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatSelectBool_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatSelectBool"), sizeof(FRigUnit_MathFloatSelectBool), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatSelectBool_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatSelectBool_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatSelectBool_Hash() { return 684273362U; }

void FRigUnit_MathFloatSelectBool::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Condition,
		IfTrue,
		IfFalse,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathFloatIsNearlyEqual>() == std::is_polymorphic<FRigUnit_MathFloatBase>(), "USTRUCT FRigUnit_MathFloatIsNearlyEqual cannot be polymorphic unless super FRigUnit_MathFloatBase is polymorphic");

class UScriptStruct* FRigUnit_MathFloatIsNearlyEqual::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyEqual_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyEqual, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatIsNearlyEqual"), sizeof(FRigUnit_MathFloatIsNearlyEqual), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyEqual_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathFloatIsNearlyEqual::Execute"), &FRigUnit_MathFloatIsNearlyEqual::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatIsNearlyEqual>()
{
	return FRigUnit_MathFloatIsNearlyEqual::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatIsNearlyEqual(FRigUnit_MathFloatIsNearlyEqual::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatIsNearlyEqual"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatIsNearlyEqual
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatIsNearlyEqual()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatIsNearlyEqual>(FName(TEXT("RigUnit_MathFloatIsNearlyEqual")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatIsNearlyEqual;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyEqual_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_A_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_A;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_B_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_B;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Tolerance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Tolerance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static void NewProp_Result_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyEqual_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns true if the value A is almost equal to B\n */" },
		{ "DisplayName", "Is Nearly Equal" },
		{ "Keywords", "AlmostEqual,==" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "PrototypeName", "IsNearlyEqual" },
		{ "ToolTip", "Returns true if the value A is almost equal to B" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyEqual_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatIsNearlyEqual>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyEqual_Statics::NewProp_A_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyEqual_Statics::NewProp_A = { "A", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatIsNearlyEqual, A), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyEqual_Statics::NewProp_A_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyEqual_Statics::NewProp_A_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyEqual_Statics::NewProp_B_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyEqual_Statics::NewProp_B = { "B", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatIsNearlyEqual, B), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyEqual_Statics::NewProp_B_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyEqual_Statics::NewProp_B_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyEqual_Statics::NewProp_Tolerance_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyEqual_Statics::NewProp_Tolerance = { "Tolerance", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatIsNearlyEqual, Tolerance), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyEqual_Statics::NewProp_Tolerance_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyEqual_Statics::NewProp_Tolerance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyEqual_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "Output", "" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyEqual_Statics::NewProp_Result_SetBit(void* Obj)
	{
		((FRigUnit_MathFloatIsNearlyEqual*)Obj)->Result = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyEqual_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_MathFloatIsNearlyEqual), &Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyEqual_Statics::NewProp_Result_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyEqual_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyEqual_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyEqual_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyEqual_Statics::NewProp_A,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyEqual_Statics::NewProp_B,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyEqual_Statics::NewProp_Tolerance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyEqual_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyEqual_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatBase,
		&NewStructOps,
		"RigUnit_MathFloatIsNearlyEqual",
		sizeof(FRigUnit_MathFloatIsNearlyEqual),
		alignof(FRigUnit_MathFloatIsNearlyEqual),
		Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyEqual_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyEqual_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyEqual_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyEqual_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyEqual()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyEqual_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatIsNearlyEqual"), sizeof(FRigUnit_MathFloatIsNearlyEqual), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyEqual_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyEqual_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyEqual_Hash() { return 3479865427U; }

void FRigUnit_MathFloatIsNearlyEqual::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Tolerance,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathFloatIsNearlyZero>() == std::is_polymorphic<FRigUnit_MathFloatBase>(), "USTRUCT FRigUnit_MathFloatIsNearlyZero cannot be polymorphic unless super FRigUnit_MathFloatBase is polymorphic");

class UScriptStruct* FRigUnit_MathFloatIsNearlyZero::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyZero_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyZero, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatIsNearlyZero"), sizeof(FRigUnit_MathFloatIsNearlyZero), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyZero_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathFloatIsNearlyZero::Execute"), &FRigUnit_MathFloatIsNearlyZero::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatIsNearlyZero>()
{
	return FRigUnit_MathFloatIsNearlyZero::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatIsNearlyZero(FRigUnit_MathFloatIsNearlyZero::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatIsNearlyZero"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatIsNearlyZero
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatIsNearlyZero()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatIsNearlyZero>(FName(TEXT("RigUnit_MathFloatIsNearlyZero")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatIsNearlyZero;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyZero_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Tolerance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Tolerance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static void NewProp_Result_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyZero_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns true if the value is nearly zero\n */" },
		{ "DisplayName", "Is Nearly Zero" },
		{ "Keywords", "AlmostZero,0" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "PrototypeName", "IsNearlyZero" },
		{ "ToolTip", "Returns true if the value is nearly zero" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyZero_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatIsNearlyZero>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyZero_Statics::NewProp_Value_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyZero_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatIsNearlyZero, Value), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyZero_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyZero_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyZero_Statics::NewProp_Tolerance_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyZero_Statics::NewProp_Tolerance = { "Tolerance", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatIsNearlyZero, Tolerance), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyZero_Statics::NewProp_Tolerance_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyZero_Statics::NewProp_Tolerance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyZero_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "Output", "" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyZero_Statics::NewProp_Result_SetBit(void* Obj)
	{
		((FRigUnit_MathFloatIsNearlyZero*)Obj)->Result = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyZero_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_MathFloatIsNearlyZero), &Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyZero_Statics::NewProp_Result_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyZero_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyZero_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyZero_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyZero_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyZero_Statics::NewProp_Tolerance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyZero_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyZero_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatBase,
		&NewStructOps,
		"RigUnit_MathFloatIsNearlyZero",
		sizeof(FRigUnit_MathFloatIsNearlyZero),
		alignof(FRigUnit_MathFloatIsNearlyZero),
		Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyZero_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyZero_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyZero_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyZero_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyZero()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyZero_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatIsNearlyZero"), sizeof(FRigUnit_MathFloatIsNearlyZero), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyZero_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyZero_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatIsNearlyZero_Hash() { return 4062111668U; }

void FRigUnit_MathFloatIsNearlyZero::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Tolerance,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathFloatLessEqual>() == std::is_polymorphic<FRigUnit_MathFloatBase>(), "USTRUCT FRigUnit_MathFloatLessEqual cannot be polymorphic unless super FRigUnit_MathFloatBase is polymorphic");

class UScriptStruct* FRigUnit_MathFloatLessEqual::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatLessEqual_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatLessEqual, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatLessEqual"), sizeof(FRigUnit_MathFloatLessEqual), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatLessEqual_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathFloatLessEqual::Execute"), &FRigUnit_MathFloatLessEqual::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatLessEqual>()
{
	return FRigUnit_MathFloatLessEqual::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatLessEqual(FRigUnit_MathFloatLessEqual::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatLessEqual"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatLessEqual
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatLessEqual()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatLessEqual>(FName(TEXT("RigUnit_MathFloatLessEqual")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatLessEqual;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatLessEqual_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_A_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_A;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_B_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_B;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static void NewProp_Result_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatLessEqual_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns true if the value A is less than or equal to B\n */" },
		{ "DisplayName", "Less Equal" },
		{ "Keywords", "Smaller,<=" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "PrototypeName", "LessEqual" },
		{ "ToolTip", "Returns true if the value A is less than or equal to B" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatLessEqual_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatLessEqual>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatLessEqual_Statics::NewProp_A_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatLessEqual_Statics::NewProp_A = { "A", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatLessEqual, A), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatLessEqual_Statics::NewProp_A_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatLessEqual_Statics::NewProp_A_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatLessEqual_Statics::NewProp_B_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatLessEqual_Statics::NewProp_B = { "B", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatLessEqual, B), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatLessEqual_Statics::NewProp_B_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatLessEqual_Statics::NewProp_B_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatLessEqual_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "Output", "" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_MathFloatLessEqual_Statics::NewProp_Result_SetBit(void* Obj)
	{
		((FRigUnit_MathFloatLessEqual*)Obj)->Result = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatLessEqual_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_MathFloatLessEqual), &Z_Construct_UScriptStruct_FRigUnit_MathFloatLessEqual_Statics::NewProp_Result_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatLessEqual_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatLessEqual_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathFloatLessEqual_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatLessEqual_Statics::NewProp_A,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatLessEqual_Statics::NewProp_B,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatLessEqual_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatLessEqual_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatBase,
		&NewStructOps,
		"RigUnit_MathFloatLessEqual",
		sizeof(FRigUnit_MathFloatLessEqual),
		alignof(FRigUnit_MathFloatLessEqual),
		Z_Construct_UScriptStruct_FRigUnit_MathFloatLessEqual_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatLessEqual_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatLessEqual_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatLessEqual_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatLessEqual()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatLessEqual_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatLessEqual"), sizeof(FRigUnit_MathFloatLessEqual), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatLessEqual_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatLessEqual_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatLessEqual_Hash() { return 3027602260U; }

void FRigUnit_MathFloatLessEqual::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathFloatGreaterEqual>() == std::is_polymorphic<FRigUnit_MathFloatBase>(), "USTRUCT FRigUnit_MathFloatGreaterEqual cannot be polymorphic unless super FRigUnit_MathFloatBase is polymorphic");

class UScriptStruct* FRigUnit_MathFloatGreaterEqual::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatGreaterEqual_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatGreaterEqual, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatGreaterEqual"), sizeof(FRigUnit_MathFloatGreaterEqual), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatGreaterEqual_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathFloatGreaterEqual::Execute"), &FRigUnit_MathFloatGreaterEqual::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatGreaterEqual>()
{
	return FRigUnit_MathFloatGreaterEqual::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatGreaterEqual(FRigUnit_MathFloatGreaterEqual::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatGreaterEqual"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatGreaterEqual
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatGreaterEqual()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatGreaterEqual>(FName(TEXT("RigUnit_MathFloatGreaterEqual")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatGreaterEqual;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatGreaterEqual_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_A_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_A;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_B_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_B;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static void NewProp_Result_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatGreaterEqual_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns true if the value A is greater than or equal to B\n */" },
		{ "DisplayName", "Greater Equal" },
		{ "Keywords", "Larger,Bigger,>=" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "PrototypeName", "GreaterEqual" },
		{ "ToolTip", "Returns true if the value A is greater than or equal to B" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatGreaterEqual_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatGreaterEqual>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatGreaterEqual_Statics::NewProp_A_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatGreaterEqual_Statics::NewProp_A = { "A", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatGreaterEqual, A), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatGreaterEqual_Statics::NewProp_A_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatGreaterEqual_Statics::NewProp_A_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatGreaterEqual_Statics::NewProp_B_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatGreaterEqual_Statics::NewProp_B = { "B", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatGreaterEqual, B), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatGreaterEqual_Statics::NewProp_B_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatGreaterEqual_Statics::NewProp_B_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatGreaterEqual_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "Output", "" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_MathFloatGreaterEqual_Statics::NewProp_Result_SetBit(void* Obj)
	{
		((FRigUnit_MathFloatGreaterEqual*)Obj)->Result = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatGreaterEqual_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_MathFloatGreaterEqual), &Z_Construct_UScriptStruct_FRigUnit_MathFloatGreaterEqual_Statics::NewProp_Result_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatGreaterEqual_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatGreaterEqual_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathFloatGreaterEqual_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatGreaterEqual_Statics::NewProp_A,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatGreaterEqual_Statics::NewProp_B,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatGreaterEqual_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatGreaterEqual_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatBase,
		&NewStructOps,
		"RigUnit_MathFloatGreaterEqual",
		sizeof(FRigUnit_MathFloatGreaterEqual),
		alignof(FRigUnit_MathFloatGreaterEqual),
		Z_Construct_UScriptStruct_FRigUnit_MathFloatGreaterEqual_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatGreaterEqual_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatGreaterEqual_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatGreaterEqual_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatGreaterEqual()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatGreaterEqual_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatGreaterEqual"), sizeof(FRigUnit_MathFloatGreaterEqual), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatGreaterEqual_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatGreaterEqual_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatGreaterEqual_Hash() { return 609496844U; }

void FRigUnit_MathFloatGreaterEqual::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathFloatLess>() == std::is_polymorphic<FRigUnit_MathFloatBase>(), "USTRUCT FRigUnit_MathFloatLess cannot be polymorphic unless super FRigUnit_MathFloatBase is polymorphic");

class UScriptStruct* FRigUnit_MathFloatLess::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatLess_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatLess, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatLess"), sizeof(FRigUnit_MathFloatLess), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatLess_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathFloatLess::Execute"), &FRigUnit_MathFloatLess::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatLess>()
{
	return FRigUnit_MathFloatLess::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatLess(FRigUnit_MathFloatLess::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatLess"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatLess
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatLess()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatLess>(FName(TEXT("RigUnit_MathFloatLess")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatLess;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatLess_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_A_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_A;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_B_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_B;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static void NewProp_Result_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatLess_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns true if the value A is less than B\n */" },
		{ "DisplayName", "Less" },
		{ "Keywords", "Smaller,<" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "PrototypeName", "Less" },
		{ "ToolTip", "Returns true if the value A is less than B" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatLess_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatLess>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatLess_Statics::NewProp_A_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatLess_Statics::NewProp_A = { "A", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatLess, A), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatLess_Statics::NewProp_A_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatLess_Statics::NewProp_A_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatLess_Statics::NewProp_B_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatLess_Statics::NewProp_B = { "B", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatLess, B), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatLess_Statics::NewProp_B_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatLess_Statics::NewProp_B_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatLess_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "Output", "" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_MathFloatLess_Statics::NewProp_Result_SetBit(void* Obj)
	{
		((FRigUnit_MathFloatLess*)Obj)->Result = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatLess_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_MathFloatLess), &Z_Construct_UScriptStruct_FRigUnit_MathFloatLess_Statics::NewProp_Result_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatLess_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatLess_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathFloatLess_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatLess_Statics::NewProp_A,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatLess_Statics::NewProp_B,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatLess_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatLess_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatBase,
		&NewStructOps,
		"RigUnit_MathFloatLess",
		sizeof(FRigUnit_MathFloatLess),
		alignof(FRigUnit_MathFloatLess),
		Z_Construct_UScriptStruct_FRigUnit_MathFloatLess_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatLess_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatLess_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatLess_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatLess()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatLess_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatLess"), sizeof(FRigUnit_MathFloatLess), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatLess_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatLess_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatLess_Hash() { return 2671463733U; }

void FRigUnit_MathFloatLess::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathFloatGreater>() == std::is_polymorphic<FRigUnit_MathFloatBase>(), "USTRUCT FRigUnit_MathFloatGreater cannot be polymorphic unless super FRigUnit_MathFloatBase is polymorphic");

class UScriptStruct* FRigUnit_MathFloatGreater::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatGreater_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatGreater, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatGreater"), sizeof(FRigUnit_MathFloatGreater), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatGreater_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathFloatGreater::Execute"), &FRigUnit_MathFloatGreater::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatGreater>()
{
	return FRigUnit_MathFloatGreater::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatGreater(FRigUnit_MathFloatGreater::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatGreater"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatGreater
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatGreater()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatGreater>(FName(TEXT("RigUnit_MathFloatGreater")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatGreater;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatGreater_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_A_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_A;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_B_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_B;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static void NewProp_Result_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatGreater_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns true if the value A is greater than B\n */" },
		{ "DisplayName", "Greater" },
		{ "Keywords", "Larger,Bigger,>" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "PrototypeName", "Greater" },
		{ "ToolTip", "Returns true if the value A is greater than B" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatGreater_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatGreater>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatGreater_Statics::NewProp_A_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatGreater_Statics::NewProp_A = { "A", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatGreater, A), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatGreater_Statics::NewProp_A_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatGreater_Statics::NewProp_A_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatGreater_Statics::NewProp_B_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatGreater_Statics::NewProp_B = { "B", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatGreater, B), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatGreater_Statics::NewProp_B_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatGreater_Statics::NewProp_B_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatGreater_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "Output", "" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_MathFloatGreater_Statics::NewProp_Result_SetBit(void* Obj)
	{
		((FRigUnit_MathFloatGreater*)Obj)->Result = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatGreater_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_MathFloatGreater), &Z_Construct_UScriptStruct_FRigUnit_MathFloatGreater_Statics::NewProp_Result_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatGreater_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatGreater_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathFloatGreater_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatGreater_Statics::NewProp_A,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatGreater_Statics::NewProp_B,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatGreater_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatGreater_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatBase,
		&NewStructOps,
		"RigUnit_MathFloatGreater",
		sizeof(FRigUnit_MathFloatGreater),
		alignof(FRigUnit_MathFloatGreater),
		Z_Construct_UScriptStruct_FRigUnit_MathFloatGreater_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatGreater_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatGreater_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatGreater_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatGreater()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatGreater_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatGreater"), sizeof(FRigUnit_MathFloatGreater), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatGreater_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatGreater_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatGreater_Hash() { return 3235375797U; }

void FRigUnit_MathFloatGreater::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathFloatNotEquals>() == std::is_polymorphic<FRigUnit_MathFloatBase>(), "USTRUCT FRigUnit_MathFloatNotEquals cannot be polymorphic unless super FRigUnit_MathFloatBase is polymorphic");

class UScriptStruct* FRigUnit_MathFloatNotEquals::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatNotEquals_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatNotEquals, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatNotEquals"), sizeof(FRigUnit_MathFloatNotEquals), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatNotEquals_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathFloatNotEquals::Execute"), &FRigUnit_MathFloatNotEquals::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatNotEquals>()
{
	return FRigUnit_MathFloatNotEquals::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatNotEquals(FRigUnit_MathFloatNotEquals::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatNotEquals"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatNotEquals
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatNotEquals()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatNotEquals>(FName(TEXT("RigUnit_MathFloatNotEquals")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatNotEquals;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatNotEquals_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_A_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_A;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_B_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_B;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static void NewProp_Result_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatNotEquals_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns true if the value A does not equal B\n */" },
		{ "DisplayName", "Not Equals" },
		{ "Keywords", "Different,!=" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "PrototypeName", "NotEquals" },
		{ "ToolTip", "Returns true if the value A does not equal B" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatNotEquals_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatNotEquals>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatNotEquals_Statics::NewProp_A_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatNotEquals_Statics::NewProp_A = { "A", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatNotEquals, A), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatNotEquals_Statics::NewProp_A_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatNotEquals_Statics::NewProp_A_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatNotEquals_Statics::NewProp_B_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatNotEquals_Statics::NewProp_B = { "B", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatNotEquals, B), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatNotEquals_Statics::NewProp_B_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatNotEquals_Statics::NewProp_B_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatNotEquals_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "Output", "" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_MathFloatNotEquals_Statics::NewProp_Result_SetBit(void* Obj)
	{
		((FRigUnit_MathFloatNotEquals*)Obj)->Result = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatNotEquals_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_MathFloatNotEquals), &Z_Construct_UScriptStruct_FRigUnit_MathFloatNotEquals_Statics::NewProp_Result_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatNotEquals_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatNotEquals_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathFloatNotEquals_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatNotEquals_Statics::NewProp_A,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatNotEquals_Statics::NewProp_B,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatNotEquals_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatNotEquals_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatBase,
		&NewStructOps,
		"RigUnit_MathFloatNotEquals",
		sizeof(FRigUnit_MathFloatNotEquals),
		alignof(FRigUnit_MathFloatNotEquals),
		Z_Construct_UScriptStruct_FRigUnit_MathFloatNotEquals_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatNotEquals_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatNotEquals_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatNotEquals_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatNotEquals()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatNotEquals_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatNotEquals"), sizeof(FRigUnit_MathFloatNotEquals), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatNotEquals_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatNotEquals_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatNotEquals_Hash() { return 1809155711U; }

void FRigUnit_MathFloatNotEquals::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathFloatEquals>() == std::is_polymorphic<FRigUnit_MathFloatBase>(), "USTRUCT FRigUnit_MathFloatEquals cannot be polymorphic unless super FRigUnit_MathFloatBase is polymorphic");

class UScriptStruct* FRigUnit_MathFloatEquals::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatEquals_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatEquals, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatEquals"), sizeof(FRigUnit_MathFloatEquals), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatEquals_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathFloatEquals::Execute"), &FRigUnit_MathFloatEquals::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatEquals>()
{
	return FRigUnit_MathFloatEquals::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatEquals(FRigUnit_MathFloatEquals::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatEquals"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatEquals
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatEquals()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatEquals>(FName(TEXT("RigUnit_MathFloatEquals")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatEquals;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatEquals_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_A_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_A;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_B_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_B;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static void NewProp_Result_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatEquals_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns true if the value A equals B\n */" },
		{ "DisplayName", "Equals" },
		{ "Keywords", "Same,==" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "PrototypeName", "Equals" },
		{ "ToolTip", "Returns true if the value A equals B" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatEquals_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatEquals>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatEquals_Statics::NewProp_A_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatEquals_Statics::NewProp_A = { "A", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatEquals, A), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatEquals_Statics::NewProp_A_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatEquals_Statics::NewProp_A_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatEquals_Statics::NewProp_B_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatEquals_Statics::NewProp_B = { "B", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatEquals, B), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatEquals_Statics::NewProp_B_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatEquals_Statics::NewProp_B_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatEquals_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "Output", "" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_MathFloatEquals_Statics::NewProp_Result_SetBit(void* Obj)
	{
		((FRigUnit_MathFloatEquals*)Obj)->Result = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatEquals_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_MathFloatEquals), &Z_Construct_UScriptStruct_FRigUnit_MathFloatEquals_Statics::NewProp_Result_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatEquals_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatEquals_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathFloatEquals_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatEquals_Statics::NewProp_A,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatEquals_Statics::NewProp_B,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatEquals_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatEquals_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatBase,
		&NewStructOps,
		"RigUnit_MathFloatEquals",
		sizeof(FRigUnit_MathFloatEquals),
		alignof(FRigUnit_MathFloatEquals),
		Z_Construct_UScriptStruct_FRigUnit_MathFloatEquals_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatEquals_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatEquals_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatEquals_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatEquals()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatEquals_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatEquals"), sizeof(FRigUnit_MathFloatEquals), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatEquals_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatEquals_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatEquals_Hash() { return 2375370416U; }

void FRigUnit_MathFloatEquals::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathFloatRemap>() == std::is_polymorphic<FRigUnit_MathFloatBase>(), "USTRUCT FRigUnit_MathFloatRemap cannot be polymorphic unless super FRigUnit_MathFloatBase is polymorphic");

class UScriptStruct* FRigUnit_MathFloatRemap::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatRemap"), sizeof(FRigUnit_MathFloatRemap), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathFloatRemap::Execute"), &FRigUnit_MathFloatRemap::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatRemap>()
{
	return FRigUnit_MathFloatRemap::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatRemap(FRigUnit_MathFloatRemap::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatRemap"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatRemap
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatRemap()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatRemap>(FName(TEXT("RigUnit_MathFloatRemap")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatRemap;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourceMinimum_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SourceMinimum;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourceMaximum_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SourceMaximum;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetMinimum_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TargetMinimum;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetMaximum_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TargetMaximum;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bClamp_MetaData[];
#endif
		static void NewProp_bClamp_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bClamp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Remaps the given value from a source range to a target range.\n */" },
		{ "DisplayName", "Remap" },
		{ "Keywords", "Rescale,Scale" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "PrototypeName", "Remap" },
		{ "ToolTip", "Remaps the given value from a source range to a target range." },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatRemap>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::NewProp_Value_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatRemap, Value), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::NewProp_SourceMinimum_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::NewProp_SourceMinimum = { "SourceMinimum", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatRemap, SourceMinimum), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::NewProp_SourceMinimum_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::NewProp_SourceMinimum_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::NewProp_SourceMaximum_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::NewProp_SourceMaximum = { "SourceMaximum", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatRemap, SourceMaximum), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::NewProp_SourceMaximum_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::NewProp_SourceMaximum_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::NewProp_TargetMinimum_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::NewProp_TargetMinimum = { "TargetMinimum", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatRemap, TargetMinimum), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::NewProp_TargetMinimum_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::NewProp_TargetMinimum_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::NewProp_TargetMaximum_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::NewProp_TargetMaximum = { "TargetMaximum", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatRemap, TargetMaximum), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::NewProp_TargetMaximum_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::NewProp_TargetMaximum_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::NewProp_bClamp_MetaData[] = {
		{ "Comment", "/** If set to true the result is clamped to the target range */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "ToolTip", "If set to true the result is clamped to the target range" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::NewProp_bClamp_SetBit(void* Obj)
	{
		((FRigUnit_MathFloatRemap*)Obj)->bClamp = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::NewProp_bClamp = { "bClamp", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_MathFloatRemap), &Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::NewProp_bClamp_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::NewProp_bClamp_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::NewProp_bClamp_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatRemap, Result), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::NewProp_SourceMinimum,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::NewProp_SourceMaximum,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::NewProp_TargetMinimum,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::NewProp_TargetMaximum,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::NewProp_bClamp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatBase,
		&NewStructOps,
		"RigUnit_MathFloatRemap",
		sizeof(FRigUnit_MathFloatRemap),
		alignof(FRigUnit_MathFloatRemap),
		Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatRemap"), sizeof(FRigUnit_MathFloatRemap), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatRemap_Hash() { return 4082894993U; }

void FRigUnit_MathFloatRemap::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		SourceMinimum,
		SourceMaximum,
		TargetMinimum,
		TargetMaximum,
		bClamp,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathFloatLerp>() == std::is_polymorphic<FRigUnit_MathFloatBase>(), "USTRUCT FRigUnit_MathFloatLerp cannot be polymorphic unless super FRigUnit_MathFloatBase is polymorphic");

class UScriptStruct* FRigUnit_MathFloatLerp::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatLerp_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatLerp, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatLerp"), sizeof(FRigUnit_MathFloatLerp), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatLerp_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathFloatLerp::Execute"), &FRigUnit_MathFloatLerp::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatLerp>()
{
	return FRigUnit_MathFloatLerp::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatLerp(FRigUnit_MathFloatLerp::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatLerp"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatLerp
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatLerp()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatLerp>(FName(TEXT("RigUnit_MathFloatLerp")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatLerp;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatLerp_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_A_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_A;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_B_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_B;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_T_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_T;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatLerp_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Linearly interpolates between A and B using the ratio T\n */" },
		{ "DisplayName", "Interpolate" },
		{ "Keywords", "Lerp,Mix,Blend" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "PrototypeName", "Interpolate" },
		{ "ToolTip", "Linearly interpolates between A and B using the ratio T" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatLerp_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatLerp>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatLerp_Statics::NewProp_A_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatLerp_Statics::NewProp_A = { "A", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatLerp, A), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatLerp_Statics::NewProp_A_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatLerp_Statics::NewProp_A_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatLerp_Statics::NewProp_B_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatLerp_Statics::NewProp_B = { "B", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatLerp, B), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatLerp_Statics::NewProp_B_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatLerp_Statics::NewProp_B_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatLerp_Statics::NewProp_T_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatLerp_Statics::NewProp_T = { "T", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatLerp, T), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatLerp_Statics::NewProp_T_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatLerp_Statics::NewProp_T_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatLerp_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatLerp_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatLerp, Result), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatLerp_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatLerp_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathFloatLerp_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatLerp_Statics::NewProp_A,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatLerp_Statics::NewProp_B,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatLerp_Statics::NewProp_T,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatLerp_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatLerp_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatBase,
		&NewStructOps,
		"RigUnit_MathFloatLerp",
		sizeof(FRigUnit_MathFloatLerp),
		alignof(FRigUnit_MathFloatLerp),
		Z_Construct_UScriptStruct_FRigUnit_MathFloatLerp_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatLerp_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatLerp_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatLerp_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatLerp()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatLerp_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatLerp"), sizeof(FRigUnit_MathFloatLerp), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatLerp_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatLerp_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatLerp_Hash() { return 512542036U; }

void FRigUnit_MathFloatLerp::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		T,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathFloatClamp>() == std::is_polymorphic<FRigUnit_MathFloatBase>(), "USTRUCT FRigUnit_MathFloatClamp cannot be polymorphic unless super FRigUnit_MathFloatBase is polymorphic");

class UScriptStruct* FRigUnit_MathFloatClamp::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatClamp_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatClamp, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatClamp"), sizeof(FRigUnit_MathFloatClamp), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatClamp_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathFloatClamp::Execute"), &FRigUnit_MathFloatClamp::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatClamp>()
{
	return FRigUnit_MathFloatClamp::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatClamp(FRigUnit_MathFloatClamp::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatClamp"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatClamp
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatClamp()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatClamp>(FName(TEXT("RigUnit_MathFloatClamp")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatClamp;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatClamp_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Minimum_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Minimum;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Maximum_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Maximum;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatClamp_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Clamps the given value within the range provided by minimum and maximum\n */" },
		{ "DisplayName", "Clamp" },
		{ "Keywords", "Range,Remap" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "PrototypeName", "Clamp" },
		{ "ToolTip", "Clamps the given value within the range provided by minimum and maximum" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatClamp_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatClamp>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatClamp_Statics::NewProp_Value_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatClamp_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatClamp, Value), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatClamp_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatClamp_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatClamp_Statics::NewProp_Minimum_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatClamp_Statics::NewProp_Minimum = { "Minimum", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatClamp, Minimum), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatClamp_Statics::NewProp_Minimum_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatClamp_Statics::NewProp_Minimum_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatClamp_Statics::NewProp_Maximum_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatClamp_Statics::NewProp_Maximum = { "Maximum", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatClamp, Maximum), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatClamp_Statics::NewProp_Maximum_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatClamp_Statics::NewProp_Maximum_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatClamp_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatClamp_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatClamp, Result), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatClamp_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatClamp_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathFloatClamp_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatClamp_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatClamp_Statics::NewProp_Minimum,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatClamp_Statics::NewProp_Maximum,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatClamp_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatClamp_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatBase,
		&NewStructOps,
		"RigUnit_MathFloatClamp",
		sizeof(FRigUnit_MathFloatClamp),
		alignof(FRigUnit_MathFloatClamp),
		Z_Construct_UScriptStruct_FRigUnit_MathFloatClamp_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatClamp_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatClamp_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatClamp_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatClamp()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatClamp_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatClamp"), sizeof(FRigUnit_MathFloatClamp), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatClamp_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatClamp_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatClamp_Hash() { return 418312729U; }

void FRigUnit_MathFloatClamp::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Minimum,
		Maximum,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathFloatSign>() == std::is_polymorphic<FRigUnit_MathFloatUnaryOp>(), "USTRUCT FRigUnit_MathFloatSign cannot be polymorphic unless super FRigUnit_MathFloatUnaryOp is polymorphic");

class UScriptStruct* FRigUnit_MathFloatSign::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatSign_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatSign, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatSign"), sizeof(FRigUnit_MathFloatSign), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatSign_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathFloatSign::Execute"), &FRigUnit_MathFloatSign::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatSign>()
{
	return FRigUnit_MathFloatSign::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatSign(FRigUnit_MathFloatSign::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatSign"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatSign
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatSign()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatSign>(FName(TEXT("RigUnit_MathFloatSign")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatSign;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatSign_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatSign_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the sign of the value (+1 for >= 0.f, -1 for < 0.f)\n */" },
		{ "DisplayName", "Sign" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "PrototypeName", "Sign" },
		{ "ToolTip", "Returns the sign of the value (+1 for >= 0.f, -1 for < 0.f)" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatSign_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatSign>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatSign_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatUnaryOp,
		&NewStructOps,
		"RigUnit_MathFloatSign",
		sizeof(FRigUnit_MathFloatSign),
		alignof(FRigUnit_MathFloatSign),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatSign_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatSign_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatSign()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatSign_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatSign"), sizeof(FRigUnit_MathFloatSign), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatSign_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatSign_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatSign_Hash() { return 3215718946U; }

void FRigUnit_MathFloatSign::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathFloatToInt>() == std::is_polymorphic<FRigUnit_MathFloatBase>(), "USTRUCT FRigUnit_MathFloatToInt cannot be polymorphic unless super FRigUnit_MathFloatBase is polymorphic");

class UScriptStruct* FRigUnit_MathFloatToInt::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatToInt_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatToInt, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatToInt"), sizeof(FRigUnit_MathFloatToInt), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatToInt_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathFloatToInt::Execute"), &FRigUnit_MathFloatToInt::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatToInt>()
{
	return FRigUnit_MathFloatToInt::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatToInt(FRigUnit_MathFloatToInt::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatToInt"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatToInt
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatToInt()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatToInt>(FName(TEXT("RigUnit_MathFloatToInt")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatToInt;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatToInt_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatToInt_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the float cast to an int (this uses floor)\n */" },
		{ "DisplayName", "To Int" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "PrototypeName", "Convert" },
		{ "ToolTip", "Returns the float cast to an int (this uses floor)" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatToInt_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatToInt>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatToInt_Statics::NewProp_Value_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatToInt_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatToInt, Value), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatToInt_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatToInt_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatToInt_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatToInt_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatToInt, Result), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatToInt_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatToInt_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathFloatToInt_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatToInt_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatToInt_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatToInt_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatBase,
		&NewStructOps,
		"RigUnit_MathFloatToInt",
		sizeof(FRigUnit_MathFloatToInt),
		alignof(FRigUnit_MathFloatToInt),
		Z_Construct_UScriptStruct_FRigUnit_MathFloatToInt_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatToInt_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatToInt_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatToInt_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatToInt()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatToInt_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatToInt"), sizeof(FRigUnit_MathFloatToInt), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatToInt_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatToInt_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatToInt_Hash() { return 201304998U; }

void FRigUnit_MathFloatToInt::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathFloatRound>() == std::is_polymorphic<FRigUnit_MathFloatBase>(), "USTRUCT FRigUnit_MathFloatRound cannot be polymorphic unless super FRigUnit_MathFloatBase is polymorphic");

class UScriptStruct* FRigUnit_MathFloatRound::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatRound_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatRound, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatRound"), sizeof(FRigUnit_MathFloatRound), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatRound_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathFloatRound::Execute"), &FRigUnit_MathFloatRound::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatRound>()
{
	return FRigUnit_MathFloatRound::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatRound(FRigUnit_MathFloatRound::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatRound"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatRound
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatRound()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatRound>(FName(TEXT("RigUnit_MathFloatRound")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatRound;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatRound_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Result;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Int_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Int;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatRound_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the closest higher full number (integer) of the value\n */" },
		{ "DisplayName", "Round" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "PrototypeName", "Round" },
		{ "ToolTip", "Returns the closest higher full number (integer) of the value" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatRound_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatRound>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatRound_Statics::NewProp_Value_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatRound_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatRound, Value), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatRound_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatRound_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatRound_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatRound_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatRound, Result), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatRound_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatRound_Statics::NewProp_Result_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatRound_Statics::NewProp_Int_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatRound_Statics::NewProp_Int = { "Int", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatRound, Int), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatRound_Statics::NewProp_Int_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatRound_Statics::NewProp_Int_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathFloatRound_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatRound_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatRound_Statics::NewProp_Result,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatRound_Statics::NewProp_Int,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatRound_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatBase,
		&NewStructOps,
		"RigUnit_MathFloatRound",
		sizeof(FRigUnit_MathFloatRound),
		alignof(FRigUnit_MathFloatRound),
		Z_Construct_UScriptStruct_FRigUnit_MathFloatRound_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatRound_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatRound_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatRound_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatRound()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatRound_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatRound"), sizeof(FRigUnit_MathFloatRound), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatRound_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatRound_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatRound_Hash() { return 2399252418U; }

void FRigUnit_MathFloatRound::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Result,
		Int,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathFloatCeil>() == std::is_polymorphic<FRigUnit_MathFloatBase>(), "USTRUCT FRigUnit_MathFloatCeil cannot be polymorphic unless super FRigUnit_MathFloatBase is polymorphic");

class UScriptStruct* FRigUnit_MathFloatCeil::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatCeil_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatCeil, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatCeil"), sizeof(FRigUnit_MathFloatCeil), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatCeil_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathFloatCeil::Execute"), &FRigUnit_MathFloatCeil::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatCeil>()
{
	return FRigUnit_MathFloatCeil::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatCeil(FRigUnit_MathFloatCeil::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatCeil"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatCeil
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatCeil()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatCeil>(FName(TEXT("RigUnit_MathFloatCeil")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatCeil;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatCeil_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Result;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Int_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Int;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatCeil_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the closest higher full number (integer) of the value\n */" },
		{ "DisplayName", "Ceiling" },
		{ "Keywords", "Round" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "PrototypeName", "Ceiling" },
		{ "ToolTip", "Returns the closest higher full number (integer) of the value" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatCeil_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatCeil>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatCeil_Statics::NewProp_Value_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatCeil_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatCeil, Value), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatCeil_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatCeil_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatCeil_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatCeil_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatCeil, Result), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatCeil_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatCeil_Statics::NewProp_Result_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatCeil_Statics::NewProp_Int_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatCeil_Statics::NewProp_Int = { "Int", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatCeil, Int), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatCeil_Statics::NewProp_Int_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatCeil_Statics::NewProp_Int_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathFloatCeil_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatCeil_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatCeil_Statics::NewProp_Result,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatCeil_Statics::NewProp_Int,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatCeil_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatBase,
		&NewStructOps,
		"RigUnit_MathFloatCeil",
		sizeof(FRigUnit_MathFloatCeil),
		alignof(FRigUnit_MathFloatCeil),
		Z_Construct_UScriptStruct_FRigUnit_MathFloatCeil_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatCeil_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatCeil_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatCeil_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatCeil()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatCeil_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatCeil"), sizeof(FRigUnit_MathFloatCeil), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatCeil_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatCeil_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatCeil_Hash() { return 2323165017U; }

void FRigUnit_MathFloatCeil::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Result,
		Int,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathFloatFloor>() == std::is_polymorphic<FRigUnit_MathFloatBase>(), "USTRUCT FRigUnit_MathFloatFloor cannot be polymorphic unless super FRigUnit_MathFloatBase is polymorphic");

class UScriptStruct* FRigUnit_MathFloatFloor::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatFloor_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatFloor, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatFloor"), sizeof(FRigUnit_MathFloatFloor), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatFloor_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathFloatFloor::Execute"), &FRigUnit_MathFloatFloor::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatFloor>()
{
	return FRigUnit_MathFloatFloor::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatFloor(FRigUnit_MathFloatFloor::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatFloor"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatFloor
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatFloor()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatFloor>(FName(TEXT("RigUnit_MathFloatFloor")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatFloor;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatFloor_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Result;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Int_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Int;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatFloor_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the closest lower full number (integer) of the value\n */" },
		{ "DisplayName", "Floor" },
		{ "Keywords", "Round" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "PrototypeName", "Floor" },
		{ "ToolTip", "Returns the closest lower full number (integer) of the value" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatFloor_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatFloor>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatFloor_Statics::NewProp_Value_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatFloor_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatFloor, Value), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatFloor_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatFloor_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatFloor_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatFloor_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatFloor, Result), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatFloor_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatFloor_Statics::NewProp_Result_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatFloor_Statics::NewProp_Int_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatFloor_Statics::NewProp_Int = { "Int", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatFloor, Int), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatFloor_Statics::NewProp_Int_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatFloor_Statics::NewProp_Int_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathFloatFloor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatFloor_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatFloor_Statics::NewProp_Result,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatFloor_Statics::NewProp_Int,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatFloor_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatBase,
		&NewStructOps,
		"RigUnit_MathFloatFloor",
		sizeof(FRigUnit_MathFloatFloor),
		alignof(FRigUnit_MathFloatFloor),
		Z_Construct_UScriptStruct_FRigUnit_MathFloatFloor_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatFloor_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatFloor_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatFloor_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatFloor()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatFloor_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatFloor"), sizeof(FRigUnit_MathFloatFloor), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatFloor_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatFloor_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatFloor_Hash() { return 1353906371U; }

void FRigUnit_MathFloatFloor::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Result,
		Int,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathFloatAbs>() == std::is_polymorphic<FRigUnit_MathFloatUnaryOp>(), "USTRUCT FRigUnit_MathFloatAbs cannot be polymorphic unless super FRigUnit_MathFloatUnaryOp is polymorphic");

class UScriptStruct* FRigUnit_MathFloatAbs::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatAbs_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatAbs, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatAbs"), sizeof(FRigUnit_MathFloatAbs), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatAbs_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathFloatAbs::Execute"), &FRigUnit_MathFloatAbs::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatAbs>()
{
	return FRigUnit_MathFloatAbs::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatAbs(FRigUnit_MathFloatAbs::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatAbs"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatAbs
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatAbs()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatAbs>(FName(TEXT("RigUnit_MathFloatAbs")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatAbs;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatAbs_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatAbs_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the absolute (positive) value\n */" },
		{ "DisplayName", "Absolute" },
		{ "Keywords", "Abs,Neg" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "PrototypeName", "Absolute" },
		{ "ToolTip", "Returns the absolute (positive) value" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatAbs_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatAbs>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatAbs_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatUnaryOp,
		&NewStructOps,
		"RigUnit_MathFloatAbs",
		sizeof(FRigUnit_MathFloatAbs),
		alignof(FRigUnit_MathFloatAbs),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatAbs_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatAbs_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatAbs()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatAbs_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatAbs"), sizeof(FRigUnit_MathFloatAbs), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatAbs_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatAbs_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatAbs_Hash() { return 1529823402U; }

void FRigUnit_MathFloatAbs::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathFloatNegate>() == std::is_polymorphic<FRigUnit_MathFloatUnaryOp>(), "USTRUCT FRigUnit_MathFloatNegate cannot be polymorphic unless super FRigUnit_MathFloatUnaryOp is polymorphic");

class UScriptStruct* FRigUnit_MathFloatNegate::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatNegate_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatNegate, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatNegate"), sizeof(FRigUnit_MathFloatNegate), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatNegate_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathFloatNegate::Execute"), &FRigUnit_MathFloatNegate::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatNegate>()
{
	return FRigUnit_MathFloatNegate::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatNegate(FRigUnit_MathFloatNegate::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatNegate"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatNegate
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatNegate()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatNegate>(FName(TEXT("RigUnit_MathFloatNegate")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatNegate;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatNegate_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatNegate_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the negative value\n */" },
		{ "DisplayName", "Negate" },
		{ "Keywords", "-,Abs" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "PrototypeName", "Negate" },
		{ "ToolTip", "Returns the negative value" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatNegate_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatNegate>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatNegate_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatUnaryOp,
		&NewStructOps,
		"RigUnit_MathFloatNegate",
		sizeof(FRigUnit_MathFloatNegate),
		alignof(FRigUnit_MathFloatNegate),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatNegate_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatNegate_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatNegate()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatNegate_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatNegate"), sizeof(FRigUnit_MathFloatNegate), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatNegate_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatNegate_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatNegate_Hash() { return 2040016048U; }

void FRigUnit_MathFloatNegate::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathFloatSqrt>() == std::is_polymorphic<FRigUnit_MathFloatUnaryOp>(), "USTRUCT FRigUnit_MathFloatSqrt cannot be polymorphic unless super FRigUnit_MathFloatUnaryOp is polymorphic");

class UScriptStruct* FRigUnit_MathFloatSqrt::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatSqrt_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatSqrt, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatSqrt"), sizeof(FRigUnit_MathFloatSqrt), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatSqrt_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathFloatSqrt::Execute"), &FRigUnit_MathFloatSqrt::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatSqrt>()
{
	return FRigUnit_MathFloatSqrt::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatSqrt(FRigUnit_MathFloatSqrt::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatSqrt"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatSqrt
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatSqrt()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatSqrt>(FName(TEXT("RigUnit_MathFloatSqrt")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatSqrt;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatSqrt_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatSqrt_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the square root of the given value\n */" },
		{ "DisplayName", "Sqrt" },
		{ "Keywords", "Root,Square" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "PrototypeName", "Sqrt" },
		{ "ToolTip", "Returns the square root of the given value" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatSqrt_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatSqrt>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatSqrt_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatUnaryOp,
		&NewStructOps,
		"RigUnit_MathFloatSqrt",
		sizeof(FRigUnit_MathFloatSqrt),
		alignof(FRigUnit_MathFloatSqrt),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatSqrt_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatSqrt_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatSqrt()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatSqrt_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatSqrt"), sizeof(FRigUnit_MathFloatSqrt), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatSqrt_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatSqrt_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatSqrt_Hash() { return 3386496039U; }

void FRigUnit_MathFloatSqrt::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathFloatPow>() == std::is_polymorphic<FRigUnit_MathFloatBinaryOp>(), "USTRUCT FRigUnit_MathFloatPow cannot be polymorphic unless super FRigUnit_MathFloatBinaryOp is polymorphic");

class UScriptStruct* FRigUnit_MathFloatPow::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatPow_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatPow, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatPow"), sizeof(FRigUnit_MathFloatPow), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatPow_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathFloatPow::Execute"), &FRigUnit_MathFloatPow::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatPow>()
{
	return FRigUnit_MathFloatPow::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatPow(FRigUnit_MathFloatPow::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatPow"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatPow
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatPow()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatPow>(FName(TEXT("RigUnit_MathFloatPow")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatPow;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatPow_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatPow_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the value of A raised to the power of B.\n */" },
		{ "DisplayName", "Power" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "PrototypeName", "Power" },
		{ "ToolTip", "Returns the value of A raised to the power of B." },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatPow_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatPow>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatPow_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatBinaryOp,
		&NewStructOps,
		"RigUnit_MathFloatPow",
		sizeof(FRigUnit_MathFloatPow),
		alignof(FRigUnit_MathFloatPow),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatPow_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatPow_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatPow()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatPow_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatPow"), sizeof(FRigUnit_MathFloatPow), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatPow_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatPow_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatPow_Hash() { return 645589968U; }

void FRigUnit_MathFloatPow::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathFloatMax>() == std::is_polymorphic<FRigUnit_MathFloatBinaryOp>(), "USTRUCT FRigUnit_MathFloatMax cannot be polymorphic unless super FRigUnit_MathFloatBinaryOp is polymorphic");

class UScriptStruct* FRigUnit_MathFloatMax::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatMax_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatMax, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatMax"), sizeof(FRigUnit_MathFloatMax), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatMax_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathFloatMax::Execute"), &FRigUnit_MathFloatMax::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatMax>()
{
	return FRigUnit_MathFloatMax::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatMax(FRigUnit_MathFloatMax::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatMax"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatMax
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatMax()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatMax>(FName(TEXT("RigUnit_MathFloatMax")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatMax;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatMax_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatMax_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the larger of the two values\n */" },
		{ "DisplayName", "Maximum" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "PrototypeName", "Maximum" },
		{ "ToolTip", "Returns the larger of the two values" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatMax_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatMax>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatMax_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatBinaryOp,
		&NewStructOps,
		"RigUnit_MathFloatMax",
		sizeof(FRigUnit_MathFloatMax),
		alignof(FRigUnit_MathFloatMax),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatMax_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatMax_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatMax()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatMax_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatMax"), sizeof(FRigUnit_MathFloatMax), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatMax_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatMax_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatMax_Hash() { return 4254573963U; }

void FRigUnit_MathFloatMax::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathFloatMin>() == std::is_polymorphic<FRigUnit_MathFloatBinaryOp>(), "USTRUCT FRigUnit_MathFloatMin cannot be polymorphic unless super FRigUnit_MathFloatBinaryOp is polymorphic");

class UScriptStruct* FRigUnit_MathFloatMin::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatMin_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatMin, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatMin"), sizeof(FRigUnit_MathFloatMin), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatMin_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathFloatMin::Execute"), &FRigUnit_MathFloatMin::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatMin>()
{
	return FRigUnit_MathFloatMin::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatMin(FRigUnit_MathFloatMin::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatMin"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatMin
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatMin()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatMin>(FName(TEXT("RigUnit_MathFloatMin")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatMin;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatMin_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatMin_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the smaller of the two values\n */" },
		{ "DisplayName", "Minimum" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "PrototypeName", "Minimum" },
		{ "ToolTip", "Returns the smaller of the two values" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatMin_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatMin>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatMin_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatBinaryOp,
		&NewStructOps,
		"RigUnit_MathFloatMin",
		sizeof(FRigUnit_MathFloatMin),
		alignof(FRigUnit_MathFloatMin),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatMin_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatMin_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatMin()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatMin_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatMin"), sizeof(FRigUnit_MathFloatMin), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatMin_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatMin_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatMin_Hash() { return 2823195424U; }

void FRigUnit_MathFloatMin::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathFloatMod>() == std::is_polymorphic<FRigUnit_MathFloatBinaryOp>(), "USTRUCT FRigUnit_MathFloatMod cannot be polymorphic unless super FRigUnit_MathFloatBinaryOp is polymorphic");

class UScriptStruct* FRigUnit_MathFloatMod::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatMod_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatMod, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatMod"), sizeof(FRigUnit_MathFloatMod), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatMod_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathFloatMod::Execute"), &FRigUnit_MathFloatMod::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatMod>()
{
	return FRigUnit_MathFloatMod::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatMod(FRigUnit_MathFloatMod::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatMod"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatMod
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatMod()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatMod>(FName(TEXT("RigUnit_MathFloatMod")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatMod;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatMod_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatMod_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the modulo of the two values\n */" },
		{ "DisplayName", "Modulo" },
		{ "Keywords", "%,fmod" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "PrototypeName", "Modulo" },
		{ "ToolTip", "Returns the modulo of the two values" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatMod_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatMod>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatMod_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatBinaryOp,
		&NewStructOps,
		"RigUnit_MathFloatMod",
		sizeof(FRigUnit_MathFloatMod),
		alignof(FRigUnit_MathFloatMod),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatMod_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatMod_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatMod()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatMod_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatMod"), sizeof(FRigUnit_MathFloatMod), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatMod_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatMod_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatMod_Hash() { return 2224472401U; }

void FRigUnit_MathFloatMod::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathFloatDiv>() == std::is_polymorphic<FRigUnit_MathFloatBinaryOp>(), "USTRUCT FRigUnit_MathFloatDiv cannot be polymorphic unless super FRigUnit_MathFloatBinaryOp is polymorphic");

class UScriptStruct* FRigUnit_MathFloatDiv::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatDiv_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatDiv, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatDiv"), sizeof(FRigUnit_MathFloatDiv), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatDiv_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathFloatDiv::Execute"), &FRigUnit_MathFloatDiv::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatDiv>()
{
	return FRigUnit_MathFloatDiv::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatDiv(FRigUnit_MathFloatDiv::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatDiv"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatDiv
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatDiv()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatDiv>(FName(TEXT("RigUnit_MathFloatDiv")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatDiv;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatDiv_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatDiv_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the division of the two values\n */" },
		{ "DisplayName", "Divide" },
		{ "Keywords", "Division,Divisor,/" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "PrototypeName", "Divide" },
		{ "ToolTip", "Returns the division of the two values" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatDiv_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatDiv>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatDiv_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatBinaryOp,
		&NewStructOps,
		"RigUnit_MathFloatDiv",
		sizeof(FRigUnit_MathFloatDiv),
		alignof(FRigUnit_MathFloatDiv),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatDiv_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatDiv_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatDiv()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatDiv_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatDiv"), sizeof(FRigUnit_MathFloatDiv), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatDiv_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatDiv_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatDiv_Hash() { return 2924680103U; }

void FRigUnit_MathFloatDiv::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathFloatMul>() == std::is_polymorphic<FRigUnit_MathFloatBinaryOp>(), "USTRUCT FRigUnit_MathFloatMul cannot be polymorphic unless super FRigUnit_MathFloatBinaryOp is polymorphic");

class UScriptStruct* FRigUnit_MathFloatMul::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatMul_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatMul, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatMul"), sizeof(FRigUnit_MathFloatMul), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatMul_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathFloatMul::Execute"), &FRigUnit_MathFloatMul::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatMul>()
{
	return FRigUnit_MathFloatMul::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatMul(FRigUnit_MathFloatMul::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatMul"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatMul
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatMul()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatMul>(FName(TEXT("RigUnit_MathFloatMul")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatMul;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatMul_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatMul_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the product of the two values\n */" },
		{ "DisplayName", "Multiply" },
		{ "Keywords", "Product,*" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "PrototypeName", "Multiply" },
		{ "ToolTip", "Returns the product of the two values" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatMul_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatMul>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatMul_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatBinaryOp,
		&NewStructOps,
		"RigUnit_MathFloatMul",
		sizeof(FRigUnit_MathFloatMul),
		alignof(FRigUnit_MathFloatMul),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatMul_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatMul_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatMul()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatMul_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatMul"), sizeof(FRigUnit_MathFloatMul), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatMul_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatMul_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatMul_Hash() { return 3729778066U; }

void FRigUnit_MathFloatMul::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathFloatSub>() == std::is_polymorphic<FRigUnit_MathFloatBinaryOp>(), "USTRUCT FRigUnit_MathFloatSub cannot be polymorphic unless super FRigUnit_MathFloatBinaryOp is polymorphic");

class UScriptStruct* FRigUnit_MathFloatSub::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatSub_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatSub, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatSub"), sizeof(FRigUnit_MathFloatSub), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatSub_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathFloatSub::Execute"), &FRigUnit_MathFloatSub::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatSub>()
{
	return FRigUnit_MathFloatSub::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatSub(FRigUnit_MathFloatSub::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatSub"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatSub
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatSub()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatSub>(FName(TEXT("RigUnit_MathFloatSub")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatSub;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatSub_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatSub_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the difference of the two values\n */" },
		{ "DisplayName", "Subtract" },
		{ "Keywords", "-" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "PrototypeName", "Subtract" },
		{ "ToolTip", "Returns the difference of the two values" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatSub_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatSub>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatSub_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatBinaryOp,
		&NewStructOps,
		"RigUnit_MathFloatSub",
		sizeof(FRigUnit_MathFloatSub),
		alignof(FRigUnit_MathFloatSub),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatSub_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatSub_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatSub()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatSub_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatSub"), sizeof(FRigUnit_MathFloatSub), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatSub_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatSub_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatSub_Hash() { return 3488747093U; }

void FRigUnit_MathFloatSub::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathFloatAdd>() == std::is_polymorphic<FRigUnit_MathFloatBinaryOp>(), "USTRUCT FRigUnit_MathFloatAdd cannot be polymorphic unless super FRigUnit_MathFloatBinaryOp is polymorphic");

class UScriptStruct* FRigUnit_MathFloatAdd::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatAdd_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatAdd, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatAdd"), sizeof(FRigUnit_MathFloatAdd), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatAdd_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathFloatAdd::Execute"), &FRigUnit_MathFloatAdd::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatAdd>()
{
	return FRigUnit_MathFloatAdd::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatAdd(FRigUnit_MathFloatAdd::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatAdd"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatAdd
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatAdd()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatAdd>(FName(TEXT("RigUnit_MathFloatAdd")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatAdd;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatAdd_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatAdd_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the sum of the two values\n */" },
		{ "DisplayName", "Add" },
		{ "Keywords", "Sum,+" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "PrototypeName", "Add" },
		{ "ToolTip", "Returns the sum of the two values" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatAdd_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatAdd>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatAdd_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatBinaryOp,
		&NewStructOps,
		"RigUnit_MathFloatAdd",
		sizeof(FRigUnit_MathFloatAdd),
		alignof(FRigUnit_MathFloatAdd),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatAdd_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatAdd_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatAdd()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatAdd_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatAdd"), sizeof(FRigUnit_MathFloatAdd), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatAdd_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatAdd_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatAdd_Hash() { return 2000889016U; }

void FRigUnit_MathFloatAdd::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathFloatConstTwoPi>() == std::is_polymorphic<FRigUnit_MathFloatConstant>(), "USTRUCT FRigUnit_MathFloatConstTwoPi cannot be polymorphic unless super FRigUnit_MathFloatConstant is polymorphic");

class UScriptStruct* FRigUnit_MathFloatConstTwoPi::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatConstTwoPi_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatConstTwoPi, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatConstTwoPi"), sizeof(FRigUnit_MathFloatConstTwoPi), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatConstTwoPi_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathFloatConstTwoPi::Execute"), &FRigUnit_MathFloatConstTwoPi::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatConstTwoPi>()
{
	return FRigUnit_MathFloatConstTwoPi::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatConstTwoPi(FRigUnit_MathFloatConstTwoPi::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatConstTwoPi"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatConstTwoPi
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatConstTwoPi()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatConstTwoPi>(FName(TEXT("RigUnit_MathFloatConstTwoPi")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatConstTwoPi;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatConstTwoPi_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatConstTwoPi_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns PI * 2.0\n */" },
		{ "DisplayName", "Two Pi" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "ToolTip", "Returns PI * 2.0" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatConstTwoPi_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatConstTwoPi>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatConstTwoPi_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatConstant,
		&NewStructOps,
		"RigUnit_MathFloatConstTwoPi",
		sizeof(FRigUnit_MathFloatConstTwoPi),
		alignof(FRigUnit_MathFloatConstTwoPi),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatConstTwoPi_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatConstTwoPi_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatConstTwoPi()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatConstTwoPi_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatConstTwoPi"), sizeof(FRigUnit_MathFloatConstTwoPi), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatConstTwoPi_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatConstTwoPi_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatConstTwoPi_Hash() { return 2236545587U; }

void FRigUnit_MathFloatConstTwoPi::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathFloatConstHalfPi>() == std::is_polymorphic<FRigUnit_MathFloatConstant>(), "USTRUCT FRigUnit_MathFloatConstHalfPi cannot be polymorphic unless super FRigUnit_MathFloatConstant is polymorphic");

class UScriptStruct* FRigUnit_MathFloatConstHalfPi::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatConstHalfPi_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatConstHalfPi, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatConstHalfPi"), sizeof(FRigUnit_MathFloatConstHalfPi), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatConstHalfPi_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathFloatConstHalfPi::Execute"), &FRigUnit_MathFloatConstHalfPi::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatConstHalfPi>()
{
	return FRigUnit_MathFloatConstHalfPi::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatConstHalfPi(FRigUnit_MathFloatConstHalfPi::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatConstHalfPi"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatConstHalfPi
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatConstHalfPi()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatConstHalfPi>(FName(TEXT("RigUnit_MathFloatConstHalfPi")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatConstHalfPi;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatConstHalfPi_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatConstHalfPi_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns PI * 0.5\n */" },
		{ "DisplayName", "Half Pi" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "ToolTip", "Returns PI * 0.5" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatConstHalfPi_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatConstHalfPi>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatConstHalfPi_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatConstant,
		&NewStructOps,
		"RigUnit_MathFloatConstHalfPi",
		sizeof(FRigUnit_MathFloatConstHalfPi),
		alignof(FRigUnit_MathFloatConstHalfPi),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatConstHalfPi_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatConstHalfPi_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatConstHalfPi()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatConstHalfPi_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatConstHalfPi"), sizeof(FRigUnit_MathFloatConstHalfPi), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatConstHalfPi_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatConstHalfPi_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatConstHalfPi_Hash() { return 1658892666U; }

void FRigUnit_MathFloatConstHalfPi::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathFloatConstPi>() == std::is_polymorphic<FRigUnit_MathFloatConstant>(), "USTRUCT FRigUnit_MathFloatConstPi cannot be polymorphic unless super FRigUnit_MathFloatConstant is polymorphic");

class UScriptStruct* FRigUnit_MathFloatConstPi::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatConstPi_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatConstPi, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatConstPi"), sizeof(FRigUnit_MathFloatConstPi), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatConstPi_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathFloatConstPi::Execute"), &FRigUnit_MathFloatConstPi::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatConstPi>()
{
	return FRigUnit_MathFloatConstPi::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatConstPi(FRigUnit_MathFloatConstPi::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatConstPi"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatConstPi
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatConstPi()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatConstPi>(FName(TEXT("RigUnit_MathFloatConstPi")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatConstPi;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatConstPi_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatConstPi_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns PI\n */" },
		{ "DisplayName", "Pi" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "ToolTip", "Returns PI" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatConstPi_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatConstPi>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatConstPi_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatConstant,
		&NewStructOps,
		"RigUnit_MathFloatConstPi",
		sizeof(FRigUnit_MathFloatConstPi),
		alignof(FRigUnit_MathFloatConstPi),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatConstPi_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatConstPi_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatConstPi()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatConstPi_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatConstPi"), sizeof(FRigUnit_MathFloatConstPi), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatConstPi_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatConstPi_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatConstPi_Hash() { return 899014031U; }

void FRigUnit_MathFloatConstPi::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathFloatBinaryOp>() == std::is_polymorphic<FRigUnit_MathFloatBase>(), "USTRUCT FRigUnit_MathFloatBinaryOp cannot be polymorphic unless super FRigUnit_MathFloatBase is polymorphic");

class UScriptStruct* FRigUnit_MathFloatBinaryOp::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatBinaryOp_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatBinaryOp, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatBinaryOp"), sizeof(FRigUnit_MathFloatBinaryOp), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatBinaryOp_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatBinaryOp>()
{
	return FRigUnit_MathFloatBinaryOp::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatBinaryOp(FRigUnit_MathFloatBinaryOp::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatBinaryOp"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatBinaryOp
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatBinaryOp()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatBinaryOp>(FName(TEXT("RigUnit_MathFloatBinaryOp")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatBinaryOp;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatBinaryOp_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_A_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_A;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_B_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_B;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatBinaryOp_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatBinaryOp_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatBinaryOp>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatBinaryOp_Statics::NewProp_A_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatBinaryOp_Statics::NewProp_A = { "A", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatBinaryOp, A), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatBinaryOp_Statics::NewProp_A_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatBinaryOp_Statics::NewProp_A_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatBinaryOp_Statics::NewProp_B_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatBinaryOp_Statics::NewProp_B = { "B", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatBinaryOp, B), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatBinaryOp_Statics::NewProp_B_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatBinaryOp_Statics::NewProp_B_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatBinaryOp_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatBinaryOp_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatBinaryOp, Result), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatBinaryOp_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatBinaryOp_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathFloatBinaryOp_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatBinaryOp_Statics::NewProp_A,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatBinaryOp_Statics::NewProp_B,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatBinaryOp_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatBinaryOp_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatBase,
		&NewStructOps,
		"RigUnit_MathFloatBinaryOp",
		sizeof(FRigUnit_MathFloatBinaryOp),
		alignof(FRigUnit_MathFloatBinaryOp),
		Z_Construct_UScriptStruct_FRigUnit_MathFloatBinaryOp_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatBinaryOp_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatBinaryOp_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatBinaryOp_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatBinaryOp()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatBinaryOp_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatBinaryOp"), sizeof(FRigUnit_MathFloatBinaryOp), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatBinaryOp_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatBinaryOp_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatBinaryOp_Hash() { return 4002575740U; }

static_assert(std::is_polymorphic<FRigUnit_MathFloatUnaryOp>() == std::is_polymorphic<FRigUnit_MathFloatBase>(), "USTRUCT FRigUnit_MathFloatUnaryOp cannot be polymorphic unless super FRigUnit_MathFloatBase is polymorphic");

class UScriptStruct* FRigUnit_MathFloatUnaryOp::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatUnaryOp_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatUnaryOp, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatUnaryOp"), sizeof(FRigUnit_MathFloatUnaryOp), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatUnaryOp_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatUnaryOp>()
{
	return FRigUnit_MathFloatUnaryOp::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatUnaryOp(FRigUnit_MathFloatUnaryOp::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatUnaryOp"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatUnaryOp
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatUnaryOp()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatUnaryOp>(FName(TEXT("RigUnit_MathFloatUnaryOp")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatUnaryOp;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatUnaryOp_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatUnaryOp_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatUnaryOp_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatUnaryOp>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatUnaryOp_Statics::NewProp_Value_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatUnaryOp_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatUnaryOp, Value), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatUnaryOp_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatUnaryOp_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatUnaryOp_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatUnaryOp_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatUnaryOp, Result), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatUnaryOp_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatUnaryOp_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathFloatUnaryOp_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatUnaryOp_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatUnaryOp_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatUnaryOp_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatBase,
		&NewStructOps,
		"RigUnit_MathFloatUnaryOp",
		sizeof(FRigUnit_MathFloatUnaryOp),
		alignof(FRigUnit_MathFloatUnaryOp),
		Z_Construct_UScriptStruct_FRigUnit_MathFloatUnaryOp_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatUnaryOp_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatUnaryOp_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatUnaryOp_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatUnaryOp()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatUnaryOp_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatUnaryOp"), sizeof(FRigUnit_MathFloatUnaryOp), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatUnaryOp_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatUnaryOp_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatUnaryOp_Hash() { return 4171454646U; }

static_assert(std::is_polymorphic<FRigUnit_MathFloatConstant>() == std::is_polymorphic<FRigUnit_MathFloatBase>(), "USTRUCT FRigUnit_MathFloatConstant cannot be polymorphic unless super FRigUnit_MathFloatBase is polymorphic");

class UScriptStruct* FRigUnit_MathFloatConstant::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatConstant_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatConstant, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatConstant"), sizeof(FRigUnit_MathFloatConstant), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatConstant_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatConstant>()
{
	return FRigUnit_MathFloatConstant::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatConstant(FRigUnit_MathFloatConstant::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatConstant"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatConstant
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatConstant()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatConstant>(FName(TEXT("RigUnit_MathFloatConstant")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatConstant;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatConstant_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatConstant_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatConstant_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatConstant>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatConstant_Statics::NewProp_Value_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathFloatConstant_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathFloatConstant, Value), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatConstant_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatConstant_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathFloatConstant_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathFloatConstant_Statics::NewProp_Value,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatConstant_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathFloatBase,
		&NewStructOps,
		"RigUnit_MathFloatConstant",
		sizeof(FRigUnit_MathFloatConstant),
		alignof(FRigUnit_MathFloatConstant),
		Z_Construct_UScriptStruct_FRigUnit_MathFloatConstant_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatConstant_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatConstant_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatConstant_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatConstant()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatConstant_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatConstant"), sizeof(FRigUnit_MathFloatConstant), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatConstant_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatConstant_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatConstant_Hash() { return 426360468U; }

static_assert(std::is_polymorphic<FRigUnit_MathFloatBase>() == std::is_polymorphic<FRigUnit_MathBase>(), "USTRUCT FRigUnit_MathFloatBase cannot be polymorphic unless super FRigUnit_MathBase is polymorphic");

class UScriptStruct* FRigUnit_MathFloatBase::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatBase_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathFloatBase, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathFloatBase"), sizeof(FRigUnit_MathFloatBase), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatBase_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathFloatBase>()
{
	return FRigUnit_MathFloatBase::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathFloatBase(FRigUnit_MathFloatBase::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathFloatBase"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatBase
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatBase()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathFloatBase>(FName(TEXT("RigUnit_MathFloatBase")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathFloatBase;
	struct Z_Construct_UScriptStruct_FRigUnit_MathFloatBase_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathFloatBase_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "Category", "Math|Float" },
		{ "MenuDescSuffix", "(Float)" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathFloat.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathFloatBase_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathFloatBase>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathFloatBase_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathBase,
		&NewStructOps,
		"RigUnit_MathFloatBase",
		sizeof(FRigUnit_MathFloatBase),
		alignof(FRigUnit_MathFloatBase),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathFloatBase_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathFloatBase_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathFloatBase()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatBase_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathFloatBase"), sizeof(FRigUnit_MathFloatBase), Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatBase_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathFloatBase_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathFloatBase_Hash() { return 721645148U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
