// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Public/Drawing/ControlRigDrawContainer.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeControlRigDrawContainer() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FControlRigDrawContainer();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FControlRigDrawInstruction();
// End Cross Module References
class UScriptStruct* FControlRigDrawContainer::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FControlRigDrawContainer_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FControlRigDrawContainer, Z_Construct_UPackage__Script_ControlRig(), TEXT("ControlRigDrawContainer"), sizeof(FControlRigDrawContainer), Get_Z_Construct_UScriptStruct_FControlRigDrawContainer_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FControlRigDrawContainer>()
{
	return FControlRigDrawContainer::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FControlRigDrawContainer(FControlRigDrawContainer::StaticStruct, TEXT("/Script/ControlRig"), TEXT("ControlRigDrawContainer"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFControlRigDrawContainer
{
	FScriptStruct_ControlRig_StaticRegisterNativesFControlRigDrawContainer()
	{
		UScriptStruct::DeferCppStructOps<FControlRigDrawContainer>(FName(TEXT("ControlRigDrawContainer")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFControlRigDrawContainer;
	struct Z_Construct_UScriptStruct_FControlRigDrawContainer_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Instructions_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Instructions_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Instructions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FControlRigDrawContainer_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Drawing/ControlRigDrawContainer.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FControlRigDrawContainer_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FControlRigDrawContainer>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FControlRigDrawContainer_Statics::NewProp_Instructions_Inner = { "Instructions", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FControlRigDrawInstruction, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FControlRigDrawContainer_Statics::NewProp_Instructions_MetaData[] = {
		{ "Category", "DrawContainer" },
		{ "ModuleRelativePath", "Public/Drawing/ControlRigDrawContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FControlRigDrawContainer_Statics::NewProp_Instructions = { "Instructions", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FControlRigDrawContainer, Instructions), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FControlRigDrawContainer_Statics::NewProp_Instructions_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FControlRigDrawContainer_Statics::NewProp_Instructions_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FControlRigDrawContainer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FControlRigDrawContainer_Statics::NewProp_Instructions_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FControlRigDrawContainer_Statics::NewProp_Instructions,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FControlRigDrawContainer_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"ControlRigDrawContainer",
		sizeof(FControlRigDrawContainer),
		alignof(FControlRigDrawContainer),
		Z_Construct_UScriptStruct_FControlRigDrawContainer_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FControlRigDrawContainer_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FControlRigDrawContainer_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FControlRigDrawContainer_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FControlRigDrawContainer()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FControlRigDrawContainer_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ControlRigDrawContainer"), sizeof(FControlRigDrawContainer), Get_Z_Construct_UScriptStruct_FControlRigDrawContainer_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FControlRigDrawContainer_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FControlRigDrawContainer_Hash() { return 2394984511U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
