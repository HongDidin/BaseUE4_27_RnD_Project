// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Math/RigUnit_MathQuaternion.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_MathQuaternion() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotationOrder();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathBase();
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_EControlRigRotationOrder();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSwingTwist();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBase();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FQuat();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionGetAxis();
	COREUOBJECT_API UEnum* Z_Construct_UEnum_CoreUObject_EAxis();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotateVector();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnit();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnaryOp();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionDot();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSelectBool();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionNotEquals();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionEquals();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSlerp();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionInverse();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionMul();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBinaryOp();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToRotator();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToEuler();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionScale();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToAxisAndAngle();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromTwoVectors();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromRotator();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromEuler();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromAxisAndAngle();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_MathQuaternionRotationOrder>() == std::is_polymorphic<FRigUnit_MathBase>(), "USTRUCT FRigUnit_MathQuaternionRotationOrder cannot be polymorphic unless super FRigUnit_MathBase is polymorphic");

class UScriptStruct* FRigUnit_MathQuaternionRotationOrder::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotationOrder_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotationOrder, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathQuaternionRotationOrder"), sizeof(FRigUnit_MathQuaternionRotationOrder), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotationOrder_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathQuaternionRotationOrder::Execute"), &FRigUnit_MathQuaternionRotationOrder::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathQuaternionRotationOrder>()
{
	return FRigUnit_MathQuaternionRotationOrder::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathQuaternionRotationOrder(FRigUnit_MathQuaternionRotationOrder::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathQuaternionRotationOrder"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionRotationOrder
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionRotationOrder()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathQuaternionRotationOrder>(FName(TEXT("RigUnit_MathQuaternionRotationOrder")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionRotationOrder;
	struct Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotationOrder_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_RotationOrder_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RotationOrder_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_RotationOrder;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotationOrder_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Math|Rotation" },
		{ "Comment", "/**\n * Enum of possible rotation orders\n */" },
		{ "Constant", "" },
		{ "DisplayName", "Rotation Order" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
		{ "ToolTip", "Enum of possible rotation orders" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotationOrder_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathQuaternionRotationOrder>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotationOrder_Statics::NewProp_RotationOrder_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotationOrder_Statics::NewProp_RotationOrder_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotationOrder_Statics::NewProp_RotationOrder = { "RotationOrder", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionRotationOrder, RotationOrder), Z_Construct_UEnum_ControlRig_EControlRigRotationOrder, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotationOrder_Statics::NewProp_RotationOrder_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotationOrder_Statics::NewProp_RotationOrder_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotationOrder_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotationOrder_Statics::NewProp_RotationOrder_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotationOrder_Statics::NewProp_RotationOrder,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotationOrder_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathBase,
		&NewStructOps,
		"RigUnit_MathQuaternionRotationOrder",
		sizeof(FRigUnit_MathQuaternionRotationOrder),
		alignof(FRigUnit_MathQuaternionRotationOrder),
		Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotationOrder_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotationOrder_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotationOrder_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotationOrder_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotationOrder()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotationOrder_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathQuaternionRotationOrder"), sizeof(FRigUnit_MathQuaternionRotationOrder), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotationOrder_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotationOrder_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotationOrder_Hash() { return 672015119U; }

void FRigUnit_MathQuaternionRotationOrder::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		RotationOrder,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathQuaternionSwingTwist>() == std::is_polymorphic<FRigUnit_MathQuaternionBase>(), "USTRUCT FRigUnit_MathQuaternionSwingTwist cannot be polymorphic unless super FRigUnit_MathQuaternionBase is polymorphic");

class UScriptStruct* FRigUnit_MathQuaternionSwingTwist::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSwingTwist_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSwingTwist, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathQuaternionSwingTwist"), sizeof(FRigUnit_MathQuaternionSwingTwist), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSwingTwist_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathQuaternionSwingTwist::Execute"), &FRigUnit_MathQuaternionSwingTwist::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathQuaternionSwingTwist>()
{
	return FRigUnit_MathQuaternionSwingTwist::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathQuaternionSwingTwist(FRigUnit_MathQuaternionSwingTwist::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathQuaternionSwingTwist"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionSwingTwist
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionSwingTwist()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathQuaternionSwingTwist>(FName(TEXT("RigUnit_MathQuaternionSwingTwist")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionSwingTwist;
	struct Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSwingTwist_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Input_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Input;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TwistAxis_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TwistAxis;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Swing_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Swing;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Twist_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Twist;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSwingTwist_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Computes the swing and twist components of a quaternion\n */" },
		{ "DisplayName", "To Swing & Twist" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
		{ "ToolTip", "Computes the swing and twist components of a quaternion" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSwingTwist_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathQuaternionSwingTwist>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSwingTwist_Statics::NewProp_Input_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSwingTwist_Statics::NewProp_Input = { "Input", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionSwingTwist, Input), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSwingTwist_Statics::NewProp_Input_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSwingTwist_Statics::NewProp_Input_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSwingTwist_Statics::NewProp_TwistAxis_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSwingTwist_Statics::NewProp_TwistAxis = { "TwistAxis", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionSwingTwist, TwistAxis), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSwingTwist_Statics::NewProp_TwistAxis_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSwingTwist_Statics::NewProp_TwistAxis_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSwingTwist_Statics::NewProp_Swing_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSwingTwist_Statics::NewProp_Swing = { "Swing", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionSwingTwist, Swing), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSwingTwist_Statics::NewProp_Swing_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSwingTwist_Statics::NewProp_Swing_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSwingTwist_Statics::NewProp_Twist_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSwingTwist_Statics::NewProp_Twist = { "Twist", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionSwingTwist, Twist), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSwingTwist_Statics::NewProp_Twist_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSwingTwist_Statics::NewProp_Twist_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSwingTwist_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSwingTwist_Statics::NewProp_Input,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSwingTwist_Statics::NewProp_TwistAxis,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSwingTwist_Statics::NewProp_Swing,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSwingTwist_Statics::NewProp_Twist,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSwingTwist_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBase,
		&NewStructOps,
		"RigUnit_MathQuaternionSwingTwist",
		sizeof(FRigUnit_MathQuaternionSwingTwist),
		alignof(FRigUnit_MathQuaternionSwingTwist),
		Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSwingTwist_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSwingTwist_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSwingTwist_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSwingTwist_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSwingTwist()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSwingTwist_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathQuaternionSwingTwist"), sizeof(FRigUnit_MathQuaternionSwingTwist), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSwingTwist_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSwingTwist_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSwingTwist_Hash() { return 1373531157U; }

void FRigUnit_MathQuaternionSwingTwist::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Input,
		TwistAxis,
		Swing,
		Twist,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathQuaternionGetAxis>() == std::is_polymorphic<FRigUnit_MathQuaternionBase>(), "USTRUCT FRigUnit_MathQuaternionGetAxis cannot be polymorphic unless super FRigUnit_MathQuaternionBase is polymorphic");

class UScriptStruct* FRigUnit_MathQuaternionGetAxis::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionGetAxis_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionGetAxis, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathQuaternionGetAxis"), sizeof(FRigUnit_MathQuaternionGetAxis), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionGetAxis_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathQuaternionGetAxis::Execute"), &FRigUnit_MathQuaternionGetAxis::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathQuaternionGetAxis>()
{
	return FRigUnit_MathQuaternionGetAxis::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathQuaternionGetAxis(FRigUnit_MathQuaternionGetAxis::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathQuaternionGetAxis"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionGetAxis
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionGetAxis()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathQuaternionGetAxis>(FName(TEXT("RigUnit_MathQuaternionGetAxis")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionGetAxis;
	struct Z_Construct_UScriptStruct_FRigUnit_MathQuaternionGetAxis_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Quaternion_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Quaternion;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Axis_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Axis;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionGetAxis_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Rotates a given vector by the quaternion\n */" },
		{ "DisplayName", "Axis" },
		{ "Keywords", "GetAxis,xAxis,yAxis,zAxis" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
		{ "PrototypeName", "Axis" },
		{ "ToolTip", "Rotates a given vector by the quaternion" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionGetAxis_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathQuaternionGetAxis>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionGetAxis_Statics::NewProp_Quaternion_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionGetAxis_Statics::NewProp_Quaternion = { "Quaternion", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionGetAxis, Quaternion), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionGetAxis_Statics::NewProp_Quaternion_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionGetAxis_Statics::NewProp_Quaternion_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionGetAxis_Statics::NewProp_Axis_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionGetAxis_Statics::NewProp_Axis = { "Axis", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionGetAxis, Axis), Z_Construct_UEnum_CoreUObject_EAxis, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionGetAxis_Statics::NewProp_Axis_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionGetAxis_Statics::NewProp_Axis_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionGetAxis_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionGetAxis_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionGetAxis, Result), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionGetAxis_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionGetAxis_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathQuaternionGetAxis_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionGetAxis_Statics::NewProp_Quaternion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionGetAxis_Statics::NewProp_Axis,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionGetAxis_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionGetAxis_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBase,
		&NewStructOps,
		"RigUnit_MathQuaternionGetAxis",
		sizeof(FRigUnit_MathQuaternionGetAxis),
		alignof(FRigUnit_MathQuaternionGetAxis),
		Z_Construct_UScriptStruct_FRigUnit_MathQuaternionGetAxis_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionGetAxis_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionGetAxis_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionGetAxis_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionGetAxis()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionGetAxis_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathQuaternionGetAxis"), sizeof(FRigUnit_MathQuaternionGetAxis), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionGetAxis_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathQuaternionGetAxis_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionGetAxis_Hash() { return 875755021U; }

void FRigUnit_MathQuaternionGetAxis::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Quaternion,
		Axis,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathQuaternionRotateVector>() == std::is_polymorphic<FRigUnit_MathQuaternionBase>(), "USTRUCT FRigUnit_MathQuaternionRotateVector cannot be polymorphic unless super FRigUnit_MathQuaternionBase is polymorphic");

class UScriptStruct* FRigUnit_MathQuaternionRotateVector::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotateVector_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotateVector, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathQuaternionRotateVector"), sizeof(FRigUnit_MathQuaternionRotateVector), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotateVector_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathQuaternionRotateVector::Execute"), &FRigUnit_MathQuaternionRotateVector::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathQuaternionRotateVector>()
{
	return FRigUnit_MathQuaternionRotateVector::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathQuaternionRotateVector(FRigUnit_MathQuaternionRotateVector::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathQuaternionRotateVector"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionRotateVector
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionRotateVector()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathQuaternionRotateVector>(FName(TEXT("RigUnit_MathQuaternionRotateVector")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionRotateVector;
	struct Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotateVector_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Quaternion_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Quaternion;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Vector_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Vector;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotateVector_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Rotates a given vector by the quaternion\n */" },
		{ "DisplayName", "Rotate" },
		{ "Keywords", "Transform" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
		{ "PrototypeName", "Multiply" },
		{ "ToolTip", "Rotates a given vector by the quaternion" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotateVector_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathQuaternionRotateVector>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotateVector_Statics::NewProp_Quaternion_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotateVector_Statics::NewProp_Quaternion = { "Quaternion", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionRotateVector, Quaternion), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotateVector_Statics::NewProp_Quaternion_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotateVector_Statics::NewProp_Quaternion_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotateVector_Statics::NewProp_Vector_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotateVector_Statics::NewProp_Vector = { "Vector", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionRotateVector, Vector), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotateVector_Statics::NewProp_Vector_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotateVector_Statics::NewProp_Vector_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotateVector_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotateVector_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionRotateVector, Result), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotateVector_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotateVector_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotateVector_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotateVector_Statics::NewProp_Quaternion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotateVector_Statics::NewProp_Vector,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotateVector_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotateVector_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBase,
		&NewStructOps,
		"RigUnit_MathQuaternionRotateVector",
		sizeof(FRigUnit_MathQuaternionRotateVector),
		alignof(FRigUnit_MathQuaternionRotateVector),
		Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotateVector_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotateVector_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotateVector_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotateVector_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotateVector()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotateVector_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathQuaternionRotateVector"), sizeof(FRigUnit_MathQuaternionRotateVector), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotateVector_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotateVector_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionRotateVector_Hash() { return 747057357U; }

void FRigUnit_MathQuaternionRotateVector::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Quaternion,
		Vector,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathQuaternionUnit>() == std::is_polymorphic<FRigUnit_MathQuaternionUnaryOp>(), "USTRUCT FRigUnit_MathQuaternionUnit cannot be polymorphic unless super FRigUnit_MathQuaternionUnaryOp is polymorphic");

class UScriptStruct* FRigUnit_MathQuaternionUnit::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnit_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnit, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathQuaternionUnit"), sizeof(FRigUnit_MathQuaternionUnit), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnit_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathQuaternionUnit::Execute"), &FRigUnit_MathQuaternionUnit::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathQuaternionUnit>()
{
	return FRigUnit_MathQuaternionUnit::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathQuaternionUnit(FRigUnit_MathQuaternionUnit::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathQuaternionUnit"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionUnit
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionUnit()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathQuaternionUnit>(FName(TEXT("RigUnit_MathQuaternionUnit")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionUnit;
	struct Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnit_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnit_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the normalized quaternion\n */" },
		{ "DisplayName", "Unit" },
		{ "Keywords", "Normalize" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
		{ "PrototypeName", "Unit" },
		{ "ToolTip", "Returns the normalized quaternion" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnit_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathQuaternionUnit>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnit_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnaryOp,
		&NewStructOps,
		"RigUnit_MathQuaternionUnit",
		sizeof(FRigUnit_MathQuaternionUnit),
		alignof(FRigUnit_MathQuaternionUnit),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnit_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnit_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnit()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnit_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathQuaternionUnit"), sizeof(FRigUnit_MathQuaternionUnit), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnit_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnit_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnit_Hash() { return 4113032720U; }

void FRigUnit_MathQuaternionUnit::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathQuaternionDot>() == std::is_polymorphic<FRigUnit_MathQuaternionBase>(), "USTRUCT FRigUnit_MathQuaternionDot cannot be polymorphic unless super FRigUnit_MathQuaternionBase is polymorphic");

class UScriptStruct* FRigUnit_MathQuaternionDot::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionDot_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionDot, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathQuaternionDot"), sizeof(FRigUnit_MathQuaternionDot), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionDot_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathQuaternionDot::Execute"), &FRigUnit_MathQuaternionDot::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathQuaternionDot>()
{
	return FRigUnit_MathQuaternionDot::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathQuaternionDot(FRigUnit_MathQuaternionDot::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathQuaternionDot"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionDot
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionDot()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathQuaternionDot>(FName(TEXT("RigUnit_MathQuaternionDot")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionDot;
	struct Z_Construct_UScriptStruct_FRigUnit_MathQuaternionDot_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_A_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_A;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_B_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_B;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionDot_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the dot product between two quaternions\n */" },
		{ "DisplayName", "Dot" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
		{ "PrototypeName", "Dot,|" },
		{ "ToolTip", "Returns the dot product between two quaternions" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionDot_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathQuaternionDot>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionDot_Statics::NewProp_A_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionDot_Statics::NewProp_A = { "A", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionDot, A), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionDot_Statics::NewProp_A_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionDot_Statics::NewProp_A_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionDot_Statics::NewProp_B_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionDot_Statics::NewProp_B = { "B", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionDot, B), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionDot_Statics::NewProp_B_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionDot_Statics::NewProp_B_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionDot_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionDot_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionDot, Result), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionDot_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionDot_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathQuaternionDot_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionDot_Statics::NewProp_A,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionDot_Statics::NewProp_B,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionDot_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionDot_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBase,
		&NewStructOps,
		"RigUnit_MathQuaternionDot",
		sizeof(FRigUnit_MathQuaternionDot),
		alignof(FRigUnit_MathQuaternionDot),
		Z_Construct_UScriptStruct_FRigUnit_MathQuaternionDot_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionDot_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionDot_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionDot_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionDot()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionDot_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathQuaternionDot"), sizeof(FRigUnit_MathQuaternionDot), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionDot_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathQuaternionDot_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionDot_Hash() { return 2370750646U; }

void FRigUnit_MathQuaternionDot::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathQuaternionSelectBool>() == std::is_polymorphic<FRigUnit_MathQuaternionBase>(), "USTRUCT FRigUnit_MathQuaternionSelectBool cannot be polymorphic unless super FRigUnit_MathQuaternionBase is polymorphic");

class UScriptStruct* FRigUnit_MathQuaternionSelectBool::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSelectBool_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSelectBool, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathQuaternionSelectBool"), sizeof(FRigUnit_MathQuaternionSelectBool), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSelectBool_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathQuaternionSelectBool::Execute"), &FRigUnit_MathQuaternionSelectBool::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathQuaternionSelectBool>()
{
	return FRigUnit_MathQuaternionSelectBool::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathQuaternionSelectBool(FRigUnit_MathQuaternionSelectBool::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathQuaternionSelectBool"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionSelectBool
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionSelectBool()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathQuaternionSelectBool>(FName(TEXT("RigUnit_MathQuaternionSelectBool")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionSelectBool;
	struct Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSelectBool_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Condition_MetaData[];
#endif
		static void NewProp_Condition_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Condition;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IfTrue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_IfTrue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IfFalse_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_IfFalse;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSelectBool_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Return one of the two values based on the condition\n */" },
		{ "Deprecated", "4.26.0" },
		{ "DisplayName", "Select" },
		{ "Keywords", "Pick,If" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
		{ "PrototypeName", "Select" },
		{ "ToolTip", "Return one of the two values based on the condition" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSelectBool_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathQuaternionSelectBool>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSelectBool_Statics::NewProp_Condition_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSelectBool_Statics::NewProp_Condition_SetBit(void* Obj)
	{
		((FRigUnit_MathQuaternionSelectBool*)Obj)->Condition = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSelectBool_Statics::NewProp_Condition = { "Condition", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_MathQuaternionSelectBool), &Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSelectBool_Statics::NewProp_Condition_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSelectBool_Statics::NewProp_Condition_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSelectBool_Statics::NewProp_Condition_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSelectBool_Statics::NewProp_IfTrue_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSelectBool_Statics::NewProp_IfTrue = { "IfTrue", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionSelectBool, IfTrue), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSelectBool_Statics::NewProp_IfTrue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSelectBool_Statics::NewProp_IfTrue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSelectBool_Statics::NewProp_IfFalse_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSelectBool_Statics::NewProp_IfFalse = { "IfFalse", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionSelectBool, IfFalse), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSelectBool_Statics::NewProp_IfFalse_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSelectBool_Statics::NewProp_IfFalse_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSelectBool_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSelectBool_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionSelectBool, Result), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSelectBool_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSelectBool_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSelectBool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSelectBool_Statics::NewProp_Condition,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSelectBool_Statics::NewProp_IfTrue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSelectBool_Statics::NewProp_IfFalse,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSelectBool_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSelectBool_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBase,
		&NewStructOps,
		"RigUnit_MathQuaternionSelectBool",
		sizeof(FRigUnit_MathQuaternionSelectBool),
		alignof(FRigUnit_MathQuaternionSelectBool),
		Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSelectBool_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSelectBool_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSelectBool_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSelectBool_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSelectBool()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSelectBool_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathQuaternionSelectBool"), sizeof(FRigUnit_MathQuaternionSelectBool), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSelectBool_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSelectBool_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSelectBool_Hash() { return 2636598293U; }

void FRigUnit_MathQuaternionSelectBool::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Condition,
		IfTrue,
		IfFalse,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathQuaternionNotEquals>() == std::is_polymorphic<FRigUnit_MathQuaternionBase>(), "USTRUCT FRigUnit_MathQuaternionNotEquals cannot be polymorphic unless super FRigUnit_MathQuaternionBase is polymorphic");

class UScriptStruct* FRigUnit_MathQuaternionNotEquals::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionNotEquals_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionNotEquals, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathQuaternionNotEquals"), sizeof(FRigUnit_MathQuaternionNotEquals), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionNotEquals_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathQuaternionNotEquals::Execute"), &FRigUnit_MathQuaternionNotEquals::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathQuaternionNotEquals>()
{
	return FRigUnit_MathQuaternionNotEquals::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathQuaternionNotEquals(FRigUnit_MathQuaternionNotEquals::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathQuaternionNotEquals"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionNotEquals
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionNotEquals()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathQuaternionNotEquals>(FName(TEXT("RigUnit_MathQuaternionNotEquals")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionNotEquals;
	struct Z_Construct_UScriptStruct_FRigUnit_MathQuaternionNotEquals_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_A_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_A;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_B_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_B;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static void NewProp_Result_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionNotEquals_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns true if the value A does not equal B\n */" },
		{ "DisplayName", "Not Equals" },
		{ "Keywords", "Different,!=" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
		{ "PrototypeName", "NotEquals" },
		{ "ToolTip", "Returns true if the value A does not equal B" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionNotEquals_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathQuaternionNotEquals>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionNotEquals_Statics::NewProp_A_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionNotEquals_Statics::NewProp_A = { "A", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionNotEquals, A), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionNotEquals_Statics::NewProp_A_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionNotEquals_Statics::NewProp_A_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionNotEquals_Statics::NewProp_B_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionNotEquals_Statics::NewProp_B = { "B", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionNotEquals, B), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionNotEquals_Statics::NewProp_B_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionNotEquals_Statics::NewProp_B_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionNotEquals_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
		{ "Output", "" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_MathQuaternionNotEquals_Statics::NewProp_Result_SetBit(void* Obj)
	{
		((FRigUnit_MathQuaternionNotEquals*)Obj)->Result = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionNotEquals_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_MathQuaternionNotEquals), &Z_Construct_UScriptStruct_FRigUnit_MathQuaternionNotEquals_Statics::NewProp_Result_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionNotEquals_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionNotEquals_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathQuaternionNotEquals_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionNotEquals_Statics::NewProp_A,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionNotEquals_Statics::NewProp_B,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionNotEquals_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionNotEquals_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBase,
		&NewStructOps,
		"RigUnit_MathQuaternionNotEquals",
		sizeof(FRigUnit_MathQuaternionNotEquals),
		alignof(FRigUnit_MathQuaternionNotEquals),
		Z_Construct_UScriptStruct_FRigUnit_MathQuaternionNotEquals_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionNotEquals_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionNotEquals_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionNotEquals_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionNotEquals()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionNotEquals_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathQuaternionNotEquals"), sizeof(FRigUnit_MathQuaternionNotEquals), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionNotEquals_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathQuaternionNotEquals_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionNotEquals_Hash() { return 232517248U; }

void FRigUnit_MathQuaternionNotEquals::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathQuaternionEquals>() == std::is_polymorphic<FRigUnit_MathQuaternionBase>(), "USTRUCT FRigUnit_MathQuaternionEquals cannot be polymorphic unless super FRigUnit_MathQuaternionBase is polymorphic");

class UScriptStruct* FRigUnit_MathQuaternionEquals::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionEquals_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionEquals, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathQuaternionEquals"), sizeof(FRigUnit_MathQuaternionEquals), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionEquals_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathQuaternionEquals::Execute"), &FRigUnit_MathQuaternionEquals::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathQuaternionEquals>()
{
	return FRigUnit_MathQuaternionEquals::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathQuaternionEquals(FRigUnit_MathQuaternionEquals::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathQuaternionEquals"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionEquals
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionEquals()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathQuaternionEquals>(FName(TEXT("RigUnit_MathQuaternionEquals")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionEquals;
	struct Z_Construct_UScriptStruct_FRigUnit_MathQuaternionEquals_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_A_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_A;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_B_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_B;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static void NewProp_Result_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionEquals_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns true if the value A equals B\n */" },
		{ "DisplayName", "Equals" },
		{ "Keywords", "Same,==" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
		{ "PrototypeName", "Equals" },
		{ "ToolTip", "Returns true if the value A equals B" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionEquals_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathQuaternionEquals>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionEquals_Statics::NewProp_A_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionEquals_Statics::NewProp_A = { "A", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionEquals, A), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionEquals_Statics::NewProp_A_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionEquals_Statics::NewProp_A_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionEquals_Statics::NewProp_B_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionEquals_Statics::NewProp_B = { "B", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionEquals, B), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionEquals_Statics::NewProp_B_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionEquals_Statics::NewProp_B_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionEquals_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
		{ "Output", "" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_MathQuaternionEquals_Statics::NewProp_Result_SetBit(void* Obj)
	{
		((FRigUnit_MathQuaternionEquals*)Obj)->Result = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionEquals_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_MathQuaternionEquals), &Z_Construct_UScriptStruct_FRigUnit_MathQuaternionEquals_Statics::NewProp_Result_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionEquals_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionEquals_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathQuaternionEquals_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionEquals_Statics::NewProp_A,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionEquals_Statics::NewProp_B,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionEquals_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionEquals_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBase,
		&NewStructOps,
		"RigUnit_MathQuaternionEquals",
		sizeof(FRigUnit_MathQuaternionEquals),
		alignof(FRigUnit_MathQuaternionEquals),
		Z_Construct_UScriptStruct_FRigUnit_MathQuaternionEquals_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionEquals_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionEquals_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionEquals_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionEquals()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionEquals_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathQuaternionEquals"), sizeof(FRigUnit_MathQuaternionEquals), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionEquals_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathQuaternionEquals_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionEquals_Hash() { return 4254789048U; }

void FRigUnit_MathQuaternionEquals::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathQuaternionSlerp>() == std::is_polymorphic<FRigUnit_MathQuaternionBase>(), "USTRUCT FRigUnit_MathQuaternionSlerp cannot be polymorphic unless super FRigUnit_MathQuaternionBase is polymorphic");

class UScriptStruct* FRigUnit_MathQuaternionSlerp::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSlerp_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSlerp, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathQuaternionSlerp"), sizeof(FRigUnit_MathQuaternionSlerp), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSlerp_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathQuaternionSlerp::Execute"), &FRigUnit_MathQuaternionSlerp::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathQuaternionSlerp>()
{
	return FRigUnit_MathQuaternionSlerp::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathQuaternionSlerp(FRigUnit_MathQuaternionSlerp::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathQuaternionSlerp"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionSlerp
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionSlerp()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathQuaternionSlerp>(FName(TEXT("RigUnit_MathQuaternionSlerp")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionSlerp;
	struct Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSlerp_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_A_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_A;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_B_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_B;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_T_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_T;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSlerp_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Linearly interpolates between A and B using the ratio T\n */" },
		{ "DisplayName", "Interpolate" },
		{ "Keywords", "Lerp,Mix,Blend,Slerp,SphericalInterpolate" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
		{ "PrototypeName", "Interpolate" },
		{ "ToolTip", "Linearly interpolates between A and B using the ratio T" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSlerp_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathQuaternionSlerp>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSlerp_Statics::NewProp_A_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSlerp_Statics::NewProp_A = { "A", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionSlerp, A), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSlerp_Statics::NewProp_A_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSlerp_Statics::NewProp_A_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSlerp_Statics::NewProp_B_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSlerp_Statics::NewProp_B = { "B", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionSlerp, B), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSlerp_Statics::NewProp_B_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSlerp_Statics::NewProp_B_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSlerp_Statics::NewProp_T_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSlerp_Statics::NewProp_T = { "T", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionSlerp, T), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSlerp_Statics::NewProp_T_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSlerp_Statics::NewProp_T_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSlerp_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSlerp_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionSlerp, Result), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSlerp_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSlerp_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSlerp_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSlerp_Statics::NewProp_A,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSlerp_Statics::NewProp_B,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSlerp_Statics::NewProp_T,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSlerp_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSlerp_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBase,
		&NewStructOps,
		"RigUnit_MathQuaternionSlerp",
		sizeof(FRigUnit_MathQuaternionSlerp),
		alignof(FRigUnit_MathQuaternionSlerp),
		Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSlerp_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSlerp_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSlerp_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSlerp_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSlerp()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSlerp_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathQuaternionSlerp"), sizeof(FRigUnit_MathQuaternionSlerp), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSlerp_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSlerp_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionSlerp_Hash() { return 3831590862U; }

void FRigUnit_MathQuaternionSlerp::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		T,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathQuaternionInverse>() == std::is_polymorphic<FRigUnit_MathQuaternionUnaryOp>(), "USTRUCT FRigUnit_MathQuaternionInverse cannot be polymorphic unless super FRigUnit_MathQuaternionUnaryOp is polymorphic");

class UScriptStruct* FRigUnit_MathQuaternionInverse::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionInverse_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionInverse, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathQuaternionInverse"), sizeof(FRigUnit_MathQuaternionInverse), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionInverse_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathQuaternionInverse::Execute"), &FRigUnit_MathQuaternionInverse::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathQuaternionInverse>()
{
	return FRigUnit_MathQuaternionInverse::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathQuaternionInverse(FRigUnit_MathQuaternionInverse::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathQuaternionInverse"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionInverse
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionInverse()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathQuaternionInverse>(FName(TEXT("RigUnit_MathQuaternionInverse")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionInverse;
	struct Z_Construct_UScriptStruct_FRigUnit_MathQuaternionInverse_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionInverse_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the negative value\n */" },
		{ "DisplayName", "Inverse" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
		{ "PrototypeName", "Inverse" },
		{ "ToolTip", "Returns the negative value" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionInverse_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathQuaternionInverse>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionInverse_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnaryOp,
		&NewStructOps,
		"RigUnit_MathQuaternionInverse",
		sizeof(FRigUnit_MathQuaternionInverse),
		alignof(FRigUnit_MathQuaternionInverse),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionInverse_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionInverse_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionInverse()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionInverse_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathQuaternionInverse"), sizeof(FRigUnit_MathQuaternionInverse), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionInverse_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathQuaternionInverse_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionInverse_Hash() { return 942089637U; }

void FRigUnit_MathQuaternionInverse::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathQuaternionMul>() == std::is_polymorphic<FRigUnit_MathQuaternionBinaryOp>(), "USTRUCT FRigUnit_MathQuaternionMul cannot be polymorphic unless super FRigUnit_MathQuaternionBinaryOp is polymorphic");

class UScriptStruct* FRigUnit_MathQuaternionMul::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionMul_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionMul, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathQuaternionMul"), sizeof(FRigUnit_MathQuaternionMul), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionMul_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathQuaternionMul::Execute"), &FRigUnit_MathQuaternionMul::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathQuaternionMul>()
{
	return FRigUnit_MathQuaternionMul::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathQuaternionMul(FRigUnit_MathQuaternionMul::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathQuaternionMul"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionMul
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionMul()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathQuaternionMul>(FName(TEXT("RigUnit_MathQuaternionMul")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionMul;
	struct Z_Construct_UScriptStruct_FRigUnit_MathQuaternionMul_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionMul_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns the product of the two values\n */" },
		{ "DisplayName", "Multiply" },
		{ "Keywords", "Product,*" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
		{ "PrototypeName", "Multiply" },
		{ "ToolTip", "Returns the product of the two values" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionMul_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathQuaternionMul>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionMul_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBinaryOp,
		&NewStructOps,
		"RigUnit_MathQuaternionMul",
		sizeof(FRigUnit_MathQuaternionMul),
		alignof(FRigUnit_MathQuaternionMul),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionMul_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionMul_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionMul()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionMul_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathQuaternionMul"), sizeof(FRigUnit_MathQuaternionMul), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionMul_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathQuaternionMul_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionMul_Hash() { return 2505881127U; }

void FRigUnit_MathQuaternionMul::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathQuaternionToRotator>() == std::is_polymorphic<FRigUnit_MathQuaternionBase>(), "USTRUCT FRigUnit_MathQuaternionToRotator cannot be polymorphic unless super FRigUnit_MathQuaternionBase is polymorphic");

class UScriptStruct* FRigUnit_MathQuaternionToRotator::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToRotator_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToRotator, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathQuaternionToRotator"), sizeof(FRigUnit_MathQuaternionToRotator), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToRotator_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathQuaternionToRotator::Execute"), &FRigUnit_MathQuaternionToRotator::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathQuaternionToRotator>()
{
	return FRigUnit_MathQuaternionToRotator::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathQuaternionToRotator(FRigUnit_MathQuaternionToRotator::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathQuaternionToRotator"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionToRotator
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionToRotator()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathQuaternionToRotator>(FName(TEXT("RigUnit_MathQuaternionToRotator")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionToRotator;
	struct Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToRotator_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToRotator_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Retrieves the rotator\n */" },
		{ "DisplayName", "To Rotator" },
		{ "Keywords", "Make,Construct" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
		{ "PrototypeName", "ToRotator" },
		{ "ToolTip", "Retrieves the rotator" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToRotator_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathQuaternionToRotator>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToRotator_Statics::NewProp_Value_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToRotator_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionToRotator, Value), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToRotator_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToRotator_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToRotator_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToRotator_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionToRotator, Result), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToRotator_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToRotator_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToRotator_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToRotator_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToRotator_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToRotator_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBase,
		&NewStructOps,
		"RigUnit_MathQuaternionToRotator",
		sizeof(FRigUnit_MathQuaternionToRotator),
		alignof(FRigUnit_MathQuaternionToRotator),
		Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToRotator_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToRotator_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToRotator_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToRotator_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToRotator()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToRotator_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathQuaternionToRotator"), sizeof(FRigUnit_MathQuaternionToRotator), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToRotator_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToRotator_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToRotator_Hash() { return 3167599707U; }

void FRigUnit_MathQuaternionToRotator::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathQuaternionToEuler>() == std::is_polymorphic<FRigUnit_MathQuaternionBase>(), "USTRUCT FRigUnit_MathQuaternionToEuler cannot be polymorphic unless super FRigUnit_MathQuaternionBase is polymorphic");

class UScriptStruct* FRigUnit_MathQuaternionToEuler::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToEuler_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToEuler, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathQuaternionToEuler"), sizeof(FRigUnit_MathQuaternionToEuler), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToEuler_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathQuaternionToEuler::Execute"), &FRigUnit_MathQuaternionToEuler::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathQuaternionToEuler>()
{
	return FRigUnit_MathQuaternionToEuler::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathQuaternionToEuler(FRigUnit_MathQuaternionToEuler::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathQuaternionToEuler"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionToEuler
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionToEuler()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathQuaternionToEuler>(FName(TEXT("RigUnit_MathQuaternionToEuler")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionToEuler;
	struct Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToEuler_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_RotationOrder_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RotationOrder_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_RotationOrder;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToEuler_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Retrieves the euler angles in degrees\n */" },
		{ "DisplayName", "To Euler" },
		{ "Keywords", "Make,Construct" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
		{ "PrototypeName", "ToEuler" },
		{ "ToolTip", "Retrieves the euler angles in degrees" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToEuler_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathQuaternionToEuler>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToEuler_Statics::NewProp_Value_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToEuler_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionToEuler, Value), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToEuler_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToEuler_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToEuler_Statics::NewProp_RotationOrder_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToEuler_Statics::NewProp_RotationOrder_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToEuler_Statics::NewProp_RotationOrder = { "RotationOrder", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionToEuler, RotationOrder), Z_Construct_UEnum_ControlRig_EControlRigRotationOrder, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToEuler_Statics::NewProp_RotationOrder_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToEuler_Statics::NewProp_RotationOrder_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToEuler_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToEuler_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionToEuler, Result), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToEuler_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToEuler_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToEuler_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToEuler_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToEuler_Statics::NewProp_RotationOrder_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToEuler_Statics::NewProp_RotationOrder,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToEuler_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToEuler_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBase,
		&NewStructOps,
		"RigUnit_MathQuaternionToEuler",
		sizeof(FRigUnit_MathQuaternionToEuler),
		alignof(FRigUnit_MathQuaternionToEuler),
		Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToEuler_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToEuler_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToEuler_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToEuler_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToEuler()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToEuler_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathQuaternionToEuler"), sizeof(FRigUnit_MathQuaternionToEuler), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToEuler_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToEuler_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToEuler_Hash() { return 512312409U; }

void FRigUnit_MathQuaternionToEuler::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		RotationOrder,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathQuaternionScale>() == std::is_polymorphic<FRigUnit_MathQuaternionBase>(), "USTRUCT FRigUnit_MathQuaternionScale cannot be polymorphic unless super FRigUnit_MathQuaternionBase is polymorphic");

class UScriptStruct* FRigUnit_MathQuaternionScale::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionScale_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionScale, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathQuaternionScale"), sizeof(FRigUnit_MathQuaternionScale), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionScale_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathQuaternionScale::Execute"), &FRigUnit_MathQuaternionScale::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathQuaternionScale>()
{
	return FRigUnit_MathQuaternionScale::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathQuaternionScale(FRigUnit_MathQuaternionScale::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathQuaternionScale"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionScale
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionScale()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathQuaternionScale>(FName(TEXT("RigUnit_MathQuaternionScale")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionScale;
	struct Z_Construct_UScriptStruct_FRigUnit_MathQuaternionScale_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Scale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Scale;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionScale_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Scales a quaternion's angle\n */" },
		{ "Constant", "" },
		{ "DisplayName", "Scale" },
		{ "Keywords", "Multiply,Angle,Scale" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
		{ "PrototypeName", "Scale" },
		{ "ToolTip", "Scales a quaternion's angle" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionScale_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathQuaternionScale>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionScale_Statics::NewProp_Value_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionScale_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionScale, Value), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionScale_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionScale_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionScale_Statics::NewProp_Scale_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionScale_Statics::NewProp_Scale = { "Scale", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionScale, Scale), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionScale_Statics::NewProp_Scale_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionScale_Statics::NewProp_Scale_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathQuaternionScale_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionScale_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionScale_Statics::NewProp_Scale,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionScale_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBase,
		&NewStructOps,
		"RigUnit_MathQuaternionScale",
		sizeof(FRigUnit_MathQuaternionScale),
		alignof(FRigUnit_MathQuaternionScale),
		Z_Construct_UScriptStruct_FRigUnit_MathQuaternionScale_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionScale_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionScale_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionScale_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionScale()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionScale_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathQuaternionScale"), sizeof(FRigUnit_MathQuaternionScale), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionScale_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathQuaternionScale_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionScale_Hash() { return 4119940324U; }

void FRigUnit_MathQuaternionScale::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Scale,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathQuaternionToAxisAndAngle>() == std::is_polymorphic<FRigUnit_MathQuaternionBase>(), "USTRUCT FRigUnit_MathQuaternionToAxisAndAngle cannot be polymorphic unless super FRigUnit_MathQuaternionBase is polymorphic");

class UScriptStruct* FRigUnit_MathQuaternionToAxisAndAngle::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToAxisAndAngle_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToAxisAndAngle, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathQuaternionToAxisAndAngle"), sizeof(FRigUnit_MathQuaternionToAxisAndAngle), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToAxisAndAngle_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathQuaternionToAxisAndAngle::Execute"), &FRigUnit_MathQuaternionToAxisAndAngle::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathQuaternionToAxisAndAngle>()
{
	return FRigUnit_MathQuaternionToAxisAndAngle::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathQuaternionToAxisAndAngle(FRigUnit_MathQuaternionToAxisAndAngle::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathQuaternionToAxisAndAngle"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionToAxisAndAngle
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionToAxisAndAngle()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathQuaternionToAxisAndAngle>(FName(TEXT("RigUnit_MathQuaternionToAxisAndAngle")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionToAxisAndAngle;
	struct Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToAxisAndAngle_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Axis_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Axis;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Angle_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Angle;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToAxisAndAngle_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Retrieves the axis and angle of a quaternion in radians\n */" },
		{ "DisplayName", "To Axis And Angle" },
		{ "Keywords", "Make,Construct,GetAxis,GetAngle" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
		{ "PrototypeName", "ToAxisAndAngle" },
		{ "ToolTip", "Retrieves the axis and angle of a quaternion in radians" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToAxisAndAngle_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathQuaternionToAxisAndAngle>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToAxisAndAngle_Statics::NewProp_Value_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToAxisAndAngle_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionToAxisAndAngle, Value), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToAxisAndAngle_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToAxisAndAngle_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToAxisAndAngle_Statics::NewProp_Axis_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToAxisAndAngle_Statics::NewProp_Axis = { "Axis", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionToAxisAndAngle, Axis), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToAxisAndAngle_Statics::NewProp_Axis_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToAxisAndAngle_Statics::NewProp_Axis_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToAxisAndAngle_Statics::NewProp_Angle_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToAxisAndAngle_Statics::NewProp_Angle = { "Angle", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionToAxisAndAngle, Angle), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToAxisAndAngle_Statics::NewProp_Angle_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToAxisAndAngle_Statics::NewProp_Angle_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToAxisAndAngle_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToAxisAndAngle_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToAxisAndAngle_Statics::NewProp_Axis,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToAxisAndAngle_Statics::NewProp_Angle,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToAxisAndAngle_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBase,
		&NewStructOps,
		"RigUnit_MathQuaternionToAxisAndAngle",
		sizeof(FRigUnit_MathQuaternionToAxisAndAngle),
		alignof(FRigUnit_MathQuaternionToAxisAndAngle),
		Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToAxisAndAngle_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToAxisAndAngle_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToAxisAndAngle_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToAxisAndAngle_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToAxisAndAngle()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToAxisAndAngle_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathQuaternionToAxisAndAngle"), sizeof(FRigUnit_MathQuaternionToAxisAndAngle), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToAxisAndAngle_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToAxisAndAngle_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionToAxisAndAngle_Hash() { return 766674243U; }

void FRigUnit_MathQuaternionToAxisAndAngle::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Value,
		Axis,
		Angle,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathQuaternionFromTwoVectors>() == std::is_polymorphic<FRigUnit_MathQuaternionBase>(), "USTRUCT FRigUnit_MathQuaternionFromTwoVectors cannot be polymorphic unless super FRigUnit_MathQuaternionBase is polymorphic");

class UScriptStruct* FRigUnit_MathQuaternionFromTwoVectors::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromTwoVectors_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromTwoVectors, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathQuaternionFromTwoVectors"), sizeof(FRigUnit_MathQuaternionFromTwoVectors), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromTwoVectors_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathQuaternionFromTwoVectors::Execute"), &FRigUnit_MathQuaternionFromTwoVectors::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathQuaternionFromTwoVectors>()
{
	return FRigUnit_MathQuaternionFromTwoVectors::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathQuaternionFromTwoVectors(FRigUnit_MathQuaternionFromTwoVectors::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathQuaternionFromTwoVectors"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionFromTwoVectors
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionFromTwoVectors()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathQuaternionFromTwoVectors>(FName(TEXT("RigUnit_MathQuaternionFromTwoVectors")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionFromTwoVectors;
	struct Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromTwoVectors_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_A_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_A;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_B_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_B;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromTwoVectors_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Makes a quaternion from two vectors, representing the shortest rotation between the two vectors.\n */" },
		{ "DisplayName", "From Two Vectors" },
		{ "Keywords", "Make,Construct" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
		{ "PrototypeName", "FromTwoVectors" },
		{ "ToolTip", "Makes a quaternion from two vectors, representing the shortest rotation between the two vectors." },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromTwoVectors_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathQuaternionFromTwoVectors>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromTwoVectors_Statics::NewProp_A_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromTwoVectors_Statics::NewProp_A = { "A", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionFromTwoVectors, A), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromTwoVectors_Statics::NewProp_A_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromTwoVectors_Statics::NewProp_A_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromTwoVectors_Statics::NewProp_B_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromTwoVectors_Statics::NewProp_B = { "B", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionFromTwoVectors, B), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromTwoVectors_Statics::NewProp_B_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromTwoVectors_Statics::NewProp_B_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromTwoVectors_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromTwoVectors_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionFromTwoVectors, Result), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromTwoVectors_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromTwoVectors_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromTwoVectors_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromTwoVectors_Statics::NewProp_A,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromTwoVectors_Statics::NewProp_B,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromTwoVectors_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromTwoVectors_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBase,
		&NewStructOps,
		"RigUnit_MathQuaternionFromTwoVectors",
		sizeof(FRigUnit_MathQuaternionFromTwoVectors),
		alignof(FRigUnit_MathQuaternionFromTwoVectors),
		Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromTwoVectors_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromTwoVectors_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromTwoVectors_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromTwoVectors_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromTwoVectors()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromTwoVectors_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathQuaternionFromTwoVectors"), sizeof(FRigUnit_MathQuaternionFromTwoVectors), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromTwoVectors_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromTwoVectors_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromTwoVectors_Hash() { return 3601849707U; }

void FRigUnit_MathQuaternionFromTwoVectors::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		A,
		B,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathQuaternionFromRotator>() == std::is_polymorphic<FRigUnit_MathQuaternionBase>(), "USTRUCT FRigUnit_MathQuaternionFromRotator cannot be polymorphic unless super FRigUnit_MathQuaternionBase is polymorphic");

class UScriptStruct* FRigUnit_MathQuaternionFromRotator::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromRotator_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromRotator, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathQuaternionFromRotator"), sizeof(FRigUnit_MathQuaternionFromRotator), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromRotator_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathQuaternionFromRotator::Execute"), &FRigUnit_MathQuaternionFromRotator::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathQuaternionFromRotator>()
{
	return FRigUnit_MathQuaternionFromRotator::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathQuaternionFromRotator(FRigUnit_MathQuaternionFromRotator::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathQuaternionFromRotator"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionFromRotator
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionFromRotator()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathQuaternionFromRotator>(FName(TEXT("RigUnit_MathQuaternionFromRotator")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionFromRotator;
	struct Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromRotator_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Rotator_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Rotator;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromRotator_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Makes a quaternion from a rotator\n */" },
		{ "DisplayName", "From Rotator" },
		{ "Keywords", "Make,Construct" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
		{ "PrototypeName", "FromRotator" },
		{ "ToolTip", "Makes a quaternion from a rotator" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromRotator_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathQuaternionFromRotator>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromRotator_Statics::NewProp_Rotator_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromRotator_Statics::NewProp_Rotator = { "Rotator", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionFromRotator, Rotator), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromRotator_Statics::NewProp_Rotator_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromRotator_Statics::NewProp_Rotator_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromRotator_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromRotator_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionFromRotator, Result), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromRotator_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromRotator_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromRotator_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromRotator_Statics::NewProp_Rotator,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromRotator_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromRotator_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBase,
		&NewStructOps,
		"RigUnit_MathQuaternionFromRotator",
		sizeof(FRigUnit_MathQuaternionFromRotator),
		alignof(FRigUnit_MathQuaternionFromRotator),
		Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromRotator_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromRotator_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromRotator_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromRotator_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromRotator()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromRotator_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathQuaternionFromRotator"), sizeof(FRigUnit_MathQuaternionFromRotator), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromRotator_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromRotator_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromRotator_Hash() { return 3633702639U; }

void FRigUnit_MathQuaternionFromRotator::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Rotator,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathQuaternionFromEuler>() == std::is_polymorphic<FRigUnit_MathQuaternionBase>(), "USTRUCT FRigUnit_MathQuaternionFromEuler cannot be polymorphic unless super FRigUnit_MathQuaternionBase is polymorphic");

class UScriptStruct* FRigUnit_MathQuaternionFromEuler::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromEuler_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromEuler, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathQuaternionFromEuler"), sizeof(FRigUnit_MathQuaternionFromEuler), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromEuler_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathQuaternionFromEuler::Execute"), &FRigUnit_MathQuaternionFromEuler::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathQuaternionFromEuler>()
{
	return FRigUnit_MathQuaternionFromEuler::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathQuaternionFromEuler(FRigUnit_MathQuaternionFromEuler::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathQuaternionFromEuler"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionFromEuler
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionFromEuler()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathQuaternionFromEuler>(FName(TEXT("RigUnit_MathQuaternionFromEuler")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionFromEuler;
	struct Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromEuler_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Euler_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Euler;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_RotationOrder_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RotationOrder_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_RotationOrder;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromEuler_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Makes a quaternion from euler values in degrees\n */" },
		{ "DisplayName", "From Euler" },
		{ "Keywords", "Make,Construct" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
		{ "PrototypeName", "FromEuler" },
		{ "ToolTip", "Makes a quaternion from euler values in degrees" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromEuler_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathQuaternionFromEuler>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromEuler_Statics::NewProp_Euler_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromEuler_Statics::NewProp_Euler = { "Euler", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionFromEuler, Euler), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromEuler_Statics::NewProp_Euler_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromEuler_Statics::NewProp_Euler_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromEuler_Statics::NewProp_RotationOrder_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromEuler_Statics::NewProp_RotationOrder_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromEuler_Statics::NewProp_RotationOrder = { "RotationOrder", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionFromEuler, RotationOrder), Z_Construct_UEnum_ControlRig_EControlRigRotationOrder, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromEuler_Statics::NewProp_RotationOrder_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromEuler_Statics::NewProp_RotationOrder_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromEuler_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromEuler_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionFromEuler, Result), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromEuler_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromEuler_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromEuler_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromEuler_Statics::NewProp_Euler,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromEuler_Statics::NewProp_RotationOrder_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromEuler_Statics::NewProp_RotationOrder,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromEuler_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromEuler_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBase,
		&NewStructOps,
		"RigUnit_MathQuaternionFromEuler",
		sizeof(FRigUnit_MathQuaternionFromEuler),
		alignof(FRigUnit_MathQuaternionFromEuler),
		Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromEuler_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromEuler_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromEuler_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromEuler_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromEuler()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromEuler_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathQuaternionFromEuler"), sizeof(FRigUnit_MathQuaternionFromEuler), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromEuler_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromEuler_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromEuler_Hash() { return 4076244739U; }

void FRigUnit_MathQuaternionFromEuler::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Euler,
		RotationOrder,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathQuaternionFromAxisAndAngle>() == std::is_polymorphic<FRigUnit_MathQuaternionBase>(), "USTRUCT FRigUnit_MathQuaternionFromAxisAndAngle cannot be polymorphic unless super FRigUnit_MathQuaternionBase is polymorphic");

class UScriptStruct* FRigUnit_MathQuaternionFromAxisAndAngle::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromAxisAndAngle_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromAxisAndAngle, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathQuaternionFromAxisAndAngle"), sizeof(FRigUnit_MathQuaternionFromAxisAndAngle), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromAxisAndAngle_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_MathQuaternionFromAxisAndAngle::Execute"), &FRigUnit_MathQuaternionFromAxisAndAngle::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathQuaternionFromAxisAndAngle>()
{
	return FRigUnit_MathQuaternionFromAxisAndAngle::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathQuaternionFromAxisAndAngle(FRigUnit_MathQuaternionFromAxisAndAngle::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathQuaternionFromAxisAndAngle"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionFromAxisAndAngle
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionFromAxisAndAngle()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathQuaternionFromAxisAndAngle>(FName(TEXT("RigUnit_MathQuaternionFromAxisAndAngle")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionFromAxisAndAngle;
	struct Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromAxisAndAngle_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Axis_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Axis;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Angle_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Angle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromAxisAndAngle_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Makes a quaternion from an axis and an angle in radians\n */" },
		{ "DisplayName", "From Axis And Angle" },
		{ "Keywords", "Make,Construct" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
		{ "PrototypeName", "FromAxisAndAngle" },
		{ "ToolTip", "Makes a quaternion from an axis and an angle in radians" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromAxisAndAngle_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathQuaternionFromAxisAndAngle>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromAxisAndAngle_Statics::NewProp_Axis_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromAxisAndAngle_Statics::NewProp_Axis = { "Axis", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionFromAxisAndAngle, Axis), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromAxisAndAngle_Statics::NewProp_Axis_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromAxisAndAngle_Statics::NewProp_Axis_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromAxisAndAngle_Statics::NewProp_Angle_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromAxisAndAngle_Statics::NewProp_Angle = { "Angle", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionFromAxisAndAngle, Angle), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromAxisAndAngle_Statics::NewProp_Angle_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromAxisAndAngle_Statics::NewProp_Angle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromAxisAndAngle_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromAxisAndAngle_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionFromAxisAndAngle, Result), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromAxisAndAngle_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromAxisAndAngle_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromAxisAndAngle_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromAxisAndAngle_Statics::NewProp_Axis,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromAxisAndAngle_Statics::NewProp_Angle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromAxisAndAngle_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromAxisAndAngle_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBase,
		&NewStructOps,
		"RigUnit_MathQuaternionFromAxisAndAngle",
		sizeof(FRigUnit_MathQuaternionFromAxisAndAngle),
		alignof(FRigUnit_MathQuaternionFromAxisAndAngle),
		Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromAxisAndAngle_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromAxisAndAngle_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromAxisAndAngle_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromAxisAndAngle_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromAxisAndAngle()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromAxisAndAngle_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathQuaternionFromAxisAndAngle"), sizeof(FRigUnit_MathQuaternionFromAxisAndAngle), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromAxisAndAngle_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromAxisAndAngle_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionFromAxisAndAngle_Hash() { return 2312015025U; }

void FRigUnit_MathQuaternionFromAxisAndAngle::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Axis,
		Angle,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_MathQuaternionBinaryOp>() == std::is_polymorphic<FRigUnit_MathQuaternionBase>(), "USTRUCT FRigUnit_MathQuaternionBinaryOp cannot be polymorphic unless super FRigUnit_MathQuaternionBase is polymorphic");

class UScriptStruct* FRigUnit_MathQuaternionBinaryOp::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBinaryOp_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBinaryOp, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathQuaternionBinaryOp"), sizeof(FRigUnit_MathQuaternionBinaryOp), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBinaryOp_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathQuaternionBinaryOp>()
{
	return FRigUnit_MathQuaternionBinaryOp::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathQuaternionBinaryOp(FRigUnit_MathQuaternionBinaryOp::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathQuaternionBinaryOp"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionBinaryOp
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionBinaryOp()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathQuaternionBinaryOp>(FName(TEXT("RigUnit_MathQuaternionBinaryOp")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionBinaryOp;
	struct Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBinaryOp_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_A_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_A;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_B_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_B;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBinaryOp_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBinaryOp_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathQuaternionBinaryOp>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBinaryOp_Statics::NewProp_A_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBinaryOp_Statics::NewProp_A = { "A", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionBinaryOp, A), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBinaryOp_Statics::NewProp_A_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBinaryOp_Statics::NewProp_A_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBinaryOp_Statics::NewProp_B_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBinaryOp_Statics::NewProp_B = { "B", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionBinaryOp, B), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBinaryOp_Statics::NewProp_B_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBinaryOp_Statics::NewProp_B_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBinaryOp_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBinaryOp_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionBinaryOp, Result), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBinaryOp_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBinaryOp_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBinaryOp_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBinaryOp_Statics::NewProp_A,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBinaryOp_Statics::NewProp_B,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBinaryOp_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBinaryOp_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBase,
		&NewStructOps,
		"RigUnit_MathQuaternionBinaryOp",
		sizeof(FRigUnit_MathQuaternionBinaryOp),
		alignof(FRigUnit_MathQuaternionBinaryOp),
		Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBinaryOp_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBinaryOp_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBinaryOp_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBinaryOp_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBinaryOp()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBinaryOp_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathQuaternionBinaryOp"), sizeof(FRigUnit_MathQuaternionBinaryOp), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBinaryOp_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBinaryOp_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBinaryOp_Hash() { return 801402584U; }

static_assert(std::is_polymorphic<FRigUnit_MathQuaternionUnaryOp>() == std::is_polymorphic<FRigUnit_MathQuaternionBase>(), "USTRUCT FRigUnit_MathQuaternionUnaryOp cannot be polymorphic unless super FRigUnit_MathQuaternionBase is polymorphic");

class UScriptStruct* FRigUnit_MathQuaternionUnaryOp::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnaryOp_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnaryOp, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathQuaternionUnaryOp"), sizeof(FRigUnit_MathQuaternionUnaryOp), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnaryOp_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathQuaternionUnaryOp>()
{
	return FRigUnit_MathQuaternionUnaryOp::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathQuaternionUnaryOp(FRigUnit_MathQuaternionUnaryOp::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathQuaternionUnaryOp"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionUnaryOp
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionUnaryOp()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathQuaternionUnaryOp>(FName(TEXT("RigUnit_MathQuaternionUnaryOp")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionUnaryOp;
	struct Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnaryOp_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnaryOp_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnaryOp_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathQuaternionUnaryOp>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnaryOp_Statics::NewProp_Value_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnaryOp_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionUnaryOp, Value), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnaryOp_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnaryOp_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnaryOp_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnaryOp_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_MathQuaternionUnaryOp, Result), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnaryOp_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnaryOp_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnaryOp_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnaryOp_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnaryOp_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnaryOp_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBase,
		&NewStructOps,
		"RigUnit_MathQuaternionUnaryOp",
		sizeof(FRigUnit_MathQuaternionUnaryOp),
		alignof(FRigUnit_MathQuaternionUnaryOp),
		Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnaryOp_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnaryOp_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnaryOp_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnaryOp_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnaryOp()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnaryOp_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathQuaternionUnaryOp"), sizeof(FRigUnit_MathQuaternionUnaryOp), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnaryOp_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnaryOp_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionUnaryOp_Hash() { return 1338967979U; }

static_assert(std::is_polymorphic<FRigUnit_MathQuaternionBase>() == std::is_polymorphic<FRigUnit_MathBase>(), "USTRUCT FRigUnit_MathQuaternionBase cannot be polymorphic unless super FRigUnit_MathBase is polymorphic");

class UScriptStruct* FRigUnit_MathQuaternionBase::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBase_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBase, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_MathQuaternionBase"), sizeof(FRigUnit_MathQuaternionBase), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBase_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_MathQuaternionBase>()
{
	return FRigUnit_MathQuaternionBase::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_MathQuaternionBase(FRigUnit_MathQuaternionBase::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_MathQuaternionBase"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionBase
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionBase()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_MathQuaternionBase>(FName(TEXT("RigUnit_MathQuaternionBase")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_MathQuaternionBase;
	struct Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBase_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBase_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "Category", "Math|Quaternion" },
		{ "MenuDescSuffix", "(Quaternion)" },
		{ "ModuleRelativePath", "Private/Units/Math/RigUnit_MathQuaternion.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBase_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_MathQuaternionBase>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBase_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_MathBase,
		&NewStructOps,
		"RigUnit_MathQuaternionBase",
		sizeof(FRigUnit_MathQuaternionBase),
		alignof(FRigUnit_MathQuaternionBase),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBase_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBase_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBase()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBase_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_MathQuaternionBase"), sizeof(FRigUnit_MathQuaternionBase), Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBase_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBase_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_MathQuaternionBase_Hash() { return 1873095646U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
