// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigUnit_Item_generated_h
#error "RigUnit_Item.generated.h already included, missing '#pragma once' in RigUnit_Item.h"
#endif
#define CONTROLRIG_RigUnit_Item_generated_h


#define FRigUnit_ItemReplace_Execute() \
	void FRigUnit_ItemReplace::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKey& Item, \
		const FName& Old, \
		const FName& New, \
		FRigElementKey& Result, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Execution_RigUnit_Item_h_56_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_ItemReplace_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKey& Item, \
		const FName& Old, \
		const FName& New, \
		FRigElementKey& Result, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FRigElementKey& Item = *(FRigElementKey*)RigVMMemoryHandles[0].GetData(); \
		const FName& Old = *(FName*)RigVMMemoryHandles[1].GetData(); \
		const FName& New = *(FName*)RigVMMemoryHandles[2].GetData(); \
		FRigElementKey& Result = *(FRigElementKey*)RigVMMemoryHandles[3].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Item, \
			Old, \
			New, \
			Result, \
			Context \
		); \
	} \
	typedef FRigUnit_ItemBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_ItemReplace>();


#define FRigUnit_ItemExists_Execute() \
	void FRigUnit_ItemExists::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKey& Item, \
		bool& Exists, \
		FCachedRigElement& CachedIndex, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Execution_RigUnit_Item_h_27_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_ItemExists_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKey& Item, \
		bool& Exists, \
		FCachedRigElement& CachedIndex, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FRigElementKey& Item = *(FRigElementKey*)RigVMMemoryHandles[0].GetData(); \
		bool& Exists = *(bool*)RigVMMemoryHandles[1].GetData(); \
		FRigVMDynamicArray<FCachedRigElement> CachedIndex_2_Array(*((FRigVMByteArray*)RigVMMemoryHandles[2].GetData(0, false))); \
		CachedIndex_2_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedIndex = CachedIndex_2_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Item, \
			Exists, \
			CachedIndex, \
			Context \
		); \
	} \
	typedef FRigUnit_ItemBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_ItemExists>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Execution_RigUnit_Item_h_18_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_ItemBaseMutable_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	typedef FRigUnitMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_ItemBaseMutable>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Execution_RigUnit_Item_h_12_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_ItemBase_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	typedef FRigUnit Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_ItemBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Execution_RigUnit_Item_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
