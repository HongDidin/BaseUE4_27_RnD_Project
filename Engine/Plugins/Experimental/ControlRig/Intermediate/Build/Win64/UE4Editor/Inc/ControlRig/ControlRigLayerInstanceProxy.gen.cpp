// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Sequencer/ControlRigLayerInstanceProxy.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeControlRigLayerInstanceProxy() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FControlRigLayerInstanceProxy();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FAnimInstanceProxy();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FAnimNode_ControlRigInputPose();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FAnimNode_Base();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FPoseLink();
// End Cross Module References

static_assert(std::is_polymorphic<FControlRigLayerInstanceProxy>() == std::is_polymorphic<FAnimInstanceProxy>(), "USTRUCT FControlRigLayerInstanceProxy cannot be polymorphic unless super FAnimInstanceProxy is polymorphic");

class UScriptStruct* FControlRigLayerInstanceProxy::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FControlRigLayerInstanceProxy_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FControlRigLayerInstanceProxy, Z_Construct_UPackage__Script_ControlRig(), TEXT("ControlRigLayerInstanceProxy"), sizeof(FControlRigLayerInstanceProxy), Get_Z_Construct_UScriptStruct_FControlRigLayerInstanceProxy_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FControlRigLayerInstanceProxy>()
{
	return FControlRigLayerInstanceProxy::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FControlRigLayerInstanceProxy(FControlRigLayerInstanceProxy::StaticStruct, TEXT("/Script/ControlRig"), TEXT("ControlRigLayerInstanceProxy"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFControlRigLayerInstanceProxy
{
	FScriptStruct_ControlRig_StaticRegisterNativesFControlRigLayerInstanceProxy()
	{
		UScriptStruct::DeferCppStructOps<FControlRigLayerInstanceProxy>(FName(TEXT("ControlRigLayerInstanceProxy")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFControlRigLayerInstanceProxy;
	struct Z_Construct_UScriptStruct_FControlRigLayerInstanceProxy_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FControlRigLayerInstanceProxy_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Proxy override for this UAnimInstance-derived class */" },
		{ "ModuleRelativePath", "Private/Sequencer/ControlRigLayerInstanceProxy.h" },
		{ "ToolTip", "Proxy override for this UAnimInstance-derived class" },
	};
#endif
	void* Z_Construct_UScriptStruct_FControlRigLayerInstanceProxy_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FControlRigLayerInstanceProxy>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FControlRigLayerInstanceProxy_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FAnimInstanceProxy,
		&NewStructOps,
		"ControlRigLayerInstanceProxy",
		sizeof(FControlRigLayerInstanceProxy),
		alignof(FControlRigLayerInstanceProxy),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FControlRigLayerInstanceProxy_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FControlRigLayerInstanceProxy_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FControlRigLayerInstanceProxy()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FControlRigLayerInstanceProxy_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ControlRigLayerInstanceProxy"), sizeof(FControlRigLayerInstanceProxy), Get_Z_Construct_UScriptStruct_FControlRigLayerInstanceProxy_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FControlRigLayerInstanceProxy_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FControlRigLayerInstanceProxy_Hash() { return 2058166944U; }

static_assert(std::is_polymorphic<FAnimNode_ControlRigInputPose>() == std::is_polymorphic<FAnimNode_Base>(), "USTRUCT FAnimNode_ControlRigInputPose cannot be polymorphic unless super FAnimNode_Base is polymorphic");

class UScriptStruct* FAnimNode_ControlRigInputPose::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FAnimNode_ControlRigInputPose_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FAnimNode_ControlRigInputPose, Z_Construct_UPackage__Script_ControlRig(), TEXT("AnimNode_ControlRigInputPose"), sizeof(FAnimNode_ControlRigInputPose), Get_Z_Construct_UScriptStruct_FAnimNode_ControlRigInputPose_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FAnimNode_ControlRigInputPose>()
{
	return FAnimNode_ControlRigInputPose::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FAnimNode_ControlRigInputPose(FAnimNode_ControlRigInputPose::StaticStruct, TEXT("/Script/ControlRig"), TEXT("AnimNode_ControlRigInputPose"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFAnimNode_ControlRigInputPose
{
	FScriptStruct_ControlRig_StaticRegisterNativesFAnimNode_ControlRigInputPose()
	{
		UScriptStruct::DeferCppStructOps<FAnimNode_ControlRigInputPose>(FName(TEXT("AnimNode_ControlRigInputPose")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFAnimNode_ControlRigInputPose;
	struct Z_Construct_UScriptStruct_FAnimNode_ControlRigInputPose_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputPose_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InputPose;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAnimNode_ControlRigInputPose_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Custom internal Input Pose node that handles any AnimInstance */" },
		{ "ModuleRelativePath", "Private/Sequencer/ControlRigLayerInstanceProxy.h" },
		{ "ToolTip", "Custom internal Input Pose node that handles any AnimInstance" },
	};
#endif
	void* Z_Construct_UScriptStruct_FAnimNode_ControlRigInputPose_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FAnimNode_ControlRigInputPose>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAnimNode_ControlRigInputPose_Statics::NewProp_InputPose_MetaData[] = {
		{ "Comment", "/** Input pose, optionally linked dynamically to another graph */" },
		{ "ModuleRelativePath", "Private/Sequencer/ControlRigLayerInstanceProxy.h" },
		{ "ToolTip", "Input pose, optionally linked dynamically to another graph" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FAnimNode_ControlRigInputPose_Statics::NewProp_InputPose = { "InputPose", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAnimNode_ControlRigInputPose, InputPose), Z_Construct_UScriptStruct_FPoseLink, METADATA_PARAMS(Z_Construct_UScriptStruct_FAnimNode_ControlRigInputPose_Statics::NewProp_InputPose_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_ControlRigInputPose_Statics::NewProp_InputPose_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FAnimNode_ControlRigInputPose_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_ControlRigInputPose_Statics::NewProp_InputPose,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FAnimNode_ControlRigInputPose_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FAnimNode_Base,
		&NewStructOps,
		"AnimNode_ControlRigInputPose",
		sizeof(FAnimNode_ControlRigInputPose),
		alignof(FAnimNode_ControlRigInputPose),
		Z_Construct_UScriptStruct_FAnimNode_ControlRigInputPose_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_ControlRigInputPose_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FAnimNode_ControlRigInputPose_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_ControlRigInputPose_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FAnimNode_ControlRigInputPose()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FAnimNode_ControlRigInputPose_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("AnimNode_ControlRigInputPose"), sizeof(FAnimNode_ControlRigInputPose), Get_Z_Construct_UScriptStruct_FAnimNode_ControlRigInputPose_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FAnimNode_ControlRigInputPose_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FAnimNode_ControlRigInputPose_Hash() { return 626633788U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
