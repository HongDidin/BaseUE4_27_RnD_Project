// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRigEditor/Private/EditMode/ControlRigEditModeSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeControlRigEditModeSettings() {}
// Cross Module References
	CONTROLRIGEDITOR_API UClass* Z_Construct_UClass_UControlRigEditModeSettings_NoRegister();
	CONTROLRIGEDITOR_API UClass* Z_Construct_UClass_UControlRigEditModeSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_ControlRigEditor();
// End Cross Module References
	void UControlRigEditModeSettings::StaticRegisterNativesUControlRigEditModeSettings()
	{
	}
	UClass* Z_Construct_UClass_UControlRigEditModeSettings_NoRegister()
	{
		return UControlRigEditModeSettings::StaticClass();
	}
	struct Z_Construct_UClass_UControlRigEditModeSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDisplayHierarchy_MetaData[];
#endif
		static void NewProp_bDisplayHierarchy_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDisplayHierarchy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDisplaySpaces_MetaData[];
#endif
		static void NewProp_bDisplaySpaces_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDisplaySpaces;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bHideManipulators_MetaData[];
#endif
		static void NewProp_bHideManipulators_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bHideManipulators;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDisplayAxesOnSelection_MetaData[];
#endif
		static void NewProp_bDisplayAxesOnSelection_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDisplayAxesOnSelection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AxisScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AxisScale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCoordSystemPerWidgetMode_MetaData[];
#endif
		static void NewProp_bCoordSystemPerWidgetMode_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCoordSystemPerWidgetMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOnlySelectRigControls_MetaData[];
#endif
		static void NewProp_bOnlySelectRigControls_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOnlySelectRigControls;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bLocalTransformsInEachLocalSpace_MetaData[];
#endif
		static void NewProp_bLocalTransformsInEachLocalSpace_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bLocalTransformsInEachLocalSpace;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GizmoScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_GizmoScale;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UControlRigEditModeSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRigEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigEditModeSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Settings object used to show useful information in the details panel */" },
		{ "IncludePath", "EditMode/ControlRigEditModeSettings.h" },
		{ "ModuleRelativePath", "Private/EditMode/ControlRigEditModeSettings.h" },
		{ "ToolTip", "Settings object used to show useful information in the details panel" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bDisplayHierarchy_MetaData[] = {
		{ "Category", "Animation" },
		{ "Comment", "/** Whether to show all bones in the hierarchy */" },
		{ "ModuleRelativePath", "Private/EditMode/ControlRigEditModeSettings.h" },
		{ "ToolTip", "Whether to show all bones in the hierarchy" },
	};
#endif
	void Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bDisplayHierarchy_SetBit(void* Obj)
	{
		((UControlRigEditModeSettings*)Obj)->bDisplayHierarchy = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bDisplayHierarchy = { "bDisplayHierarchy", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UControlRigEditModeSettings), &Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bDisplayHierarchy_SetBit, METADATA_PARAMS(Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bDisplayHierarchy_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bDisplayHierarchy_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bDisplaySpaces_MetaData[] = {
		{ "Category", "Animation" },
		{ "Comment", "/** Whether to show all spaces in the hierarchy */" },
		{ "ModuleRelativePath", "Private/EditMode/ControlRigEditModeSettings.h" },
		{ "ToolTip", "Whether to show all spaces in the hierarchy" },
	};
#endif
	void Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bDisplaySpaces_SetBit(void* Obj)
	{
		((UControlRigEditModeSettings*)Obj)->bDisplaySpaces = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bDisplaySpaces = { "bDisplaySpaces", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UControlRigEditModeSettings), &Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bDisplaySpaces_SetBit, METADATA_PARAMS(Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bDisplaySpaces_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bDisplaySpaces_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bHideManipulators_MetaData[] = {
		{ "Category", "Animation" },
		{ "Comment", "/** Should we always hide manipulators in viewport */" },
		{ "ModuleRelativePath", "Private/EditMode/ControlRigEditModeSettings.h" },
		{ "ToolTip", "Should we always hide manipulators in viewport" },
	};
#endif
	void Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bHideManipulators_SetBit(void* Obj)
	{
		((UControlRigEditModeSettings*)Obj)->bHideManipulators = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bHideManipulators = { "bHideManipulators", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UControlRigEditModeSettings), &Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bHideManipulators_SetBit, METADATA_PARAMS(Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bHideManipulators_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bHideManipulators_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bDisplayAxesOnSelection_MetaData[] = {
		{ "Category", "Animation" },
		{ "Comment", "/** Should we show axes for the selected elements */" },
		{ "ModuleRelativePath", "Private/EditMode/ControlRigEditModeSettings.h" },
		{ "ToolTip", "Should we show axes for the selected elements" },
	};
#endif
	void Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bDisplayAxesOnSelection_SetBit(void* Obj)
	{
		((UControlRigEditModeSettings*)Obj)->bDisplayAxesOnSelection = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bDisplayAxesOnSelection = { "bDisplayAxesOnSelection", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UControlRigEditModeSettings), &Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bDisplayAxesOnSelection_SetBit, METADATA_PARAMS(Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bDisplayAxesOnSelection_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bDisplayAxesOnSelection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_AxisScale_MetaData[] = {
		{ "Category", "Animation" },
		{ "Comment", "/** The scale for axes to draw on the selection */" },
		{ "ModuleRelativePath", "Private/EditMode/ControlRigEditModeSettings.h" },
		{ "ToolTip", "The scale for axes to draw on the selection" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_AxisScale = { "AxisScale", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigEditModeSettings, AxisScale), METADATA_PARAMS(Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_AxisScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_AxisScale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bCoordSystemPerWidgetMode_MetaData[] = {
		{ "Category", "Animation" },
		{ "Comment", "/** If true we restore the coordinate space when changing Widget Modes in the Viewport*/" },
		{ "ModuleRelativePath", "Private/EditMode/ControlRigEditModeSettings.h" },
		{ "ToolTip", "If true we restore the coordinate space when changing Widget Modes in the Viewport" },
	};
#endif
	void Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bCoordSystemPerWidgetMode_SetBit(void* Obj)
	{
		((UControlRigEditModeSettings*)Obj)->bCoordSystemPerWidgetMode = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bCoordSystemPerWidgetMode = { "bCoordSystemPerWidgetMode", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UControlRigEditModeSettings), &Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bCoordSystemPerWidgetMode_SetBit, METADATA_PARAMS(Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bCoordSystemPerWidgetMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bCoordSystemPerWidgetMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bOnlySelectRigControls_MetaData[] = {
		{ "Category", "Animation" },
		{ "Comment", "/** If true we can only select Rig Controls in the scene not other Actors. */" },
		{ "ModuleRelativePath", "Private/EditMode/ControlRigEditModeSettings.h" },
		{ "ToolTip", "If true we can only select Rig Controls in the scene not other Actors." },
	};
#endif
	void Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bOnlySelectRigControls_SetBit(void* Obj)
	{
		((UControlRigEditModeSettings*)Obj)->bOnlySelectRigControls = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bOnlySelectRigControls = { "bOnlySelectRigControls", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UControlRigEditModeSettings), &Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bOnlySelectRigControls_SetBit, METADATA_PARAMS(Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bOnlySelectRigControls_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bOnlySelectRigControls_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bLocalTransformsInEachLocalSpace_MetaData[] = {
		{ "Category", "Animation" },
		{ "Comment", "/** If true when we transform multiple selected objects in the viewport they each transforms along their own local transform space */" },
		{ "ModuleRelativePath", "Private/EditMode/ControlRigEditModeSettings.h" },
		{ "ToolTip", "If true when we transform multiple selected objects in the viewport they each transforms along their own local transform space" },
	};
#endif
	void Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bLocalTransformsInEachLocalSpace_SetBit(void* Obj)
	{
		((UControlRigEditModeSettings*)Obj)->bLocalTransformsInEachLocalSpace = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bLocalTransformsInEachLocalSpace = { "bLocalTransformsInEachLocalSpace", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UControlRigEditModeSettings), &Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bLocalTransformsInEachLocalSpace_SetBit, METADATA_PARAMS(Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bLocalTransformsInEachLocalSpace_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bLocalTransformsInEachLocalSpace_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_GizmoScale_MetaData[] = {
		{ "Category", "Animation" },
		{ "Comment", "/** The scale for Gizmos */" },
		{ "ModuleRelativePath", "Private/EditMode/ControlRigEditModeSettings.h" },
		{ "ToolTip", "The scale for Gizmos" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_GizmoScale = { "GizmoScale", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigEditModeSettings, GizmoScale), METADATA_PARAMS(Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_GizmoScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_GizmoScale_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UControlRigEditModeSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bDisplayHierarchy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bDisplaySpaces,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bHideManipulators,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bDisplayAxesOnSelection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_AxisScale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bCoordSystemPerWidgetMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bOnlySelectRigControls,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_bLocalTransformsInEachLocalSpace,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigEditModeSettings_Statics::NewProp_GizmoScale,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UControlRigEditModeSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UControlRigEditModeSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UControlRigEditModeSettings_Statics::ClassParams = {
		&UControlRigEditModeSettings::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UControlRigEditModeSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigEditModeSettings_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UControlRigEditModeSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigEditModeSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UControlRigEditModeSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UControlRigEditModeSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UControlRigEditModeSettings, 3240020194);
	template<> CONTROLRIGEDITOR_API UClass* StaticClass<UControlRigEditModeSettings>()
	{
		return UControlRigEditModeSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UControlRigEditModeSettings(Z_Construct_UClass_UControlRigEditModeSettings, &UControlRigEditModeSettings::StaticClass, TEXT("/Script/ControlRigEditor"), TEXT("UControlRigEditModeSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UControlRigEditModeSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
