// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigUnit_Accumulate_generated_h
#error "RigUnit_Accumulate.generated.h already included, missing '#pragma once' in RigUnit_Accumulate.h"
#endif
#define CONTROLRIG_RigUnit_Accumulate_generated_h


#define FRigUnit_AccumulateVectorRange_Execute() \
	void FRigUnit_AccumulateVectorRange::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FVector& Value, \
		FVector& Minimum, \
		FVector& Maximum, \
		FVector& AccumulatedMinimum, \
		FVector& AccumulatedMaximum, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Simulation_RigUnit_Accumulate_h_397_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorRange_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FVector& Value, \
		FVector& Minimum, \
		FVector& Maximum, \
		FVector& AccumulatedMinimum, \
		FVector& AccumulatedMaximum, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FVector& Value = *(FVector*)RigVMMemoryHandles[0].GetData(); \
		FVector& Minimum = *(FVector*)RigVMMemoryHandles[1].GetData(); \
		FVector& Maximum = *(FVector*)RigVMMemoryHandles[2].GetData(); \
		FRigVMDynamicArray<FVector> AccumulatedMinimum_3_Array(*((FRigVMByteArray*)RigVMMemoryHandles[3].GetData(0, false))); \
		AccumulatedMinimum_3_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FVector& AccumulatedMinimum = AccumulatedMinimum_3_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<FVector> AccumulatedMaximum_4_Array(*((FRigVMByteArray*)RigVMMemoryHandles[4].GetData(0, false))); \
		AccumulatedMaximum_4_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FVector& AccumulatedMaximum = AccumulatedMaximum_4_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Value, \
			Minimum, \
			Maximum, \
			AccumulatedMinimum, \
			AccumulatedMaximum, \
			Context \
		); \
	} \
	typedef FRigUnit_SimBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_AccumulateVectorRange>();


#define FRigUnit_AccumulateFloatRange_Execute() \
	void FRigUnit_AccumulateFloatRange::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const float Value, \
		float& Minimum, \
		float& Maximum, \
		float& AccumulatedMinimum, \
		float& AccumulatedMaximum, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Simulation_RigUnit_Accumulate_h_365_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatRange_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const float Value, \
		float& Minimum, \
		float& Maximum, \
		float& AccumulatedMinimum, \
		float& AccumulatedMaximum, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const float Value = *(float*)RigVMMemoryHandles[0].GetData(); \
		float& Minimum = *(float*)RigVMMemoryHandles[1].GetData(); \
		float& Maximum = *(float*)RigVMMemoryHandles[2].GetData(); \
		FRigVMDynamicArray<float> AccumulatedMinimum_3_Array(*((FRigVMByteArray*)RigVMMemoryHandles[3].GetData(0, false))); \
		AccumulatedMinimum_3_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		float& AccumulatedMinimum = AccumulatedMinimum_3_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<float> AccumulatedMaximum_4_Array(*((FRigVMByteArray*)RigVMMemoryHandles[4].GetData(0, false))); \
		AccumulatedMaximum_4_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		float& AccumulatedMaximum = AccumulatedMaximum_4_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Value, \
			Minimum, \
			Maximum, \
			AccumulatedMinimum, \
			AccumulatedMaximum, \
			Context \
		); \
	} \
	typedef FRigUnit_SimBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_AccumulateFloatRange>();


#define FRigUnit_AccumulateTransformLerp_Execute() \
	void FRigUnit_AccumulateTransformLerp::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FTransform& TargetValue, \
		const FTransform& InitialValue, \
		const float Blend, \
		const bool bIntegrateDeltaTime, \
		FTransform& Result, \
		FTransform& AccumulatedValue, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Simulation_RigUnit_Accumulate_h_328_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformLerp_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FTransform& TargetValue, \
		const FTransform& InitialValue, \
		const float Blend, \
		const bool bIntegrateDeltaTime, \
		FTransform& Result, \
		FTransform& AccumulatedValue, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FTransform& TargetValue = *(FTransform*)RigVMMemoryHandles[0].GetData(); \
		const FTransform& InitialValue = *(FTransform*)RigVMMemoryHandles[1].GetData(); \
		const float Blend = *(float*)RigVMMemoryHandles[2].GetData(); \
		const bool bIntegrateDeltaTime = *(bool*)RigVMMemoryHandles[3].GetData(); \
		FTransform& Result = *(FTransform*)RigVMMemoryHandles[4].GetData(); \
		FRigVMDynamicArray<FTransform> AccumulatedValue_5_Array(*((FRigVMByteArray*)RigVMMemoryHandles[5].GetData(0, false))); \
		AccumulatedValue_5_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FTransform& AccumulatedValue = AccumulatedValue_5_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			TargetValue, \
			InitialValue, \
			Blend, \
			bIntegrateDeltaTime, \
			Result, \
			AccumulatedValue, \
			Context \
		); \
	} \
	typedef FRigUnit_SimBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_AccumulateTransformLerp>();


#define FRigUnit_AccumulateQuatLerp_Execute() \
	void FRigUnit_AccumulateQuatLerp::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FQuat& TargetValue, \
		const FQuat& InitialValue, \
		const float Blend, \
		const bool bIntegrateDeltaTime, \
		FQuat& Result, \
		FQuat& AccumulatedValue, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Simulation_RigUnit_Accumulate_h_291_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatLerp_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FQuat& TargetValue, \
		const FQuat& InitialValue, \
		const float Blend, \
		const bool bIntegrateDeltaTime, \
		FQuat& Result, \
		FQuat& AccumulatedValue, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FQuat& TargetValue = *(FQuat*)RigVMMemoryHandles[0].GetData(); \
		const FQuat& InitialValue = *(FQuat*)RigVMMemoryHandles[1].GetData(); \
		const float Blend = *(float*)RigVMMemoryHandles[2].GetData(); \
		const bool bIntegrateDeltaTime = *(bool*)RigVMMemoryHandles[3].GetData(); \
		FQuat& Result = *(FQuat*)RigVMMemoryHandles[4].GetData(); \
		FRigVMDynamicArray<FQuat> AccumulatedValue_5_Array(*((FRigVMByteArray*)RigVMMemoryHandles[5].GetData(0, false))); \
		AccumulatedValue_5_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FQuat& AccumulatedValue = AccumulatedValue_5_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			TargetValue, \
			InitialValue, \
			Blend, \
			bIntegrateDeltaTime, \
			Result, \
			AccumulatedValue, \
			Context \
		); \
	} \
	typedef FRigUnit_SimBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_AccumulateQuatLerp>();


#define FRigUnit_AccumulateVectorLerp_Execute() \
	void FRigUnit_AccumulateVectorLerp::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FVector& TargetValue, \
		const FVector& InitialValue, \
		const float Blend, \
		const bool bIntegrateDeltaTime, \
		FVector& Result, \
		FVector& AccumulatedValue, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Simulation_RigUnit_Accumulate_h_254_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorLerp_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FVector& TargetValue, \
		const FVector& InitialValue, \
		const float Blend, \
		const bool bIntegrateDeltaTime, \
		FVector& Result, \
		FVector& AccumulatedValue, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FVector& TargetValue = *(FVector*)RigVMMemoryHandles[0].GetData(); \
		const FVector& InitialValue = *(FVector*)RigVMMemoryHandles[1].GetData(); \
		const float Blend = *(float*)RigVMMemoryHandles[2].GetData(); \
		const bool bIntegrateDeltaTime = *(bool*)RigVMMemoryHandles[3].GetData(); \
		FVector& Result = *(FVector*)RigVMMemoryHandles[4].GetData(); \
		FRigVMDynamicArray<FVector> AccumulatedValue_5_Array(*((FRigVMByteArray*)RigVMMemoryHandles[5].GetData(0, false))); \
		AccumulatedValue_5_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FVector& AccumulatedValue = AccumulatedValue_5_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			TargetValue, \
			InitialValue, \
			Blend, \
			bIntegrateDeltaTime, \
			Result, \
			AccumulatedValue, \
			Context \
		); \
	} \
	typedef FRigUnit_SimBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_AccumulateVectorLerp>();


#define FRigUnit_AccumulateFloatLerp_Execute() \
	void FRigUnit_AccumulateFloatLerp::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const float TargetValue, \
		const float InitialValue, \
		const float Blend, \
		const bool bIntegrateDeltaTime, \
		float& Result, \
		float& AccumulatedValue, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Simulation_RigUnit_Accumulate_h_218_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatLerp_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const float TargetValue, \
		const float InitialValue, \
		const float Blend, \
		const bool bIntegrateDeltaTime, \
		float& Result, \
		float& AccumulatedValue, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const float TargetValue = *(float*)RigVMMemoryHandles[0].GetData(); \
		const float InitialValue = *(float*)RigVMMemoryHandles[1].GetData(); \
		const float Blend = *(float*)RigVMMemoryHandles[2].GetData(); \
		const bool bIntegrateDeltaTime = *(bool*)RigVMMemoryHandles[3].GetData(); \
		float& Result = *(float*)RigVMMemoryHandles[4].GetData(); \
		FRigVMDynamicArray<float> AccumulatedValue_5_Array(*((FRigVMByteArray*)RigVMMemoryHandles[5].GetData(0, false))); \
		AccumulatedValue_5_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		float& AccumulatedValue = AccumulatedValue_5_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			TargetValue, \
			InitialValue, \
			Blend, \
			bIntegrateDeltaTime, \
			Result, \
			AccumulatedValue, \
			Context \
		); \
	} \
	typedef FRigUnit_SimBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_AccumulateFloatLerp>();


#define FRigUnit_AccumulateTransformMul_Execute() \
	void FRigUnit_AccumulateTransformMul::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FTransform& Multiplier, \
		const FTransform& InitialValue, \
		const bool bFlipOrder, \
		const bool bIntegrateDeltaTime, \
		FTransform& Result, \
		FTransform& AccumulatedValue, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Simulation_RigUnit_Accumulate_h_182_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_AccumulateTransformMul_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FTransform& Multiplier, \
		const FTransform& InitialValue, \
		const bool bFlipOrder, \
		const bool bIntegrateDeltaTime, \
		FTransform& Result, \
		FTransform& AccumulatedValue, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FTransform& Multiplier = *(FTransform*)RigVMMemoryHandles[0].GetData(); \
		const FTransform& InitialValue = *(FTransform*)RigVMMemoryHandles[1].GetData(); \
		const bool bFlipOrder = *(bool*)RigVMMemoryHandles[2].GetData(); \
		const bool bIntegrateDeltaTime = *(bool*)RigVMMemoryHandles[3].GetData(); \
		FTransform& Result = *(FTransform*)RigVMMemoryHandles[4].GetData(); \
		FRigVMDynamicArray<FTransform> AccumulatedValue_5_Array(*((FRigVMByteArray*)RigVMMemoryHandles[5].GetData(0, false))); \
		AccumulatedValue_5_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FTransform& AccumulatedValue = AccumulatedValue_5_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Multiplier, \
			InitialValue, \
			bFlipOrder, \
			bIntegrateDeltaTime, \
			Result, \
			AccumulatedValue, \
			Context \
		); \
	} \
	typedef FRigUnit_SimBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_AccumulateTransformMul>();


#define FRigUnit_AccumulateQuatMul_Execute() \
	void FRigUnit_AccumulateQuatMul::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FQuat& Multiplier, \
		const FQuat& InitialValue, \
		const bool bFlipOrder, \
		const bool bIntegrateDeltaTime, \
		FQuat& Result, \
		FQuat& AccumulatedValue, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Simulation_RigUnit_Accumulate_h_146_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_AccumulateQuatMul_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FQuat& Multiplier, \
		const FQuat& InitialValue, \
		const bool bFlipOrder, \
		const bool bIntegrateDeltaTime, \
		FQuat& Result, \
		FQuat& AccumulatedValue, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FQuat& Multiplier = *(FQuat*)RigVMMemoryHandles[0].GetData(); \
		const FQuat& InitialValue = *(FQuat*)RigVMMemoryHandles[1].GetData(); \
		const bool bFlipOrder = *(bool*)RigVMMemoryHandles[2].GetData(); \
		const bool bIntegrateDeltaTime = *(bool*)RigVMMemoryHandles[3].GetData(); \
		FQuat& Result = *(FQuat*)RigVMMemoryHandles[4].GetData(); \
		FRigVMDynamicArray<FQuat> AccumulatedValue_5_Array(*((FRigVMByteArray*)RigVMMemoryHandles[5].GetData(0, false))); \
		AccumulatedValue_5_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FQuat& AccumulatedValue = AccumulatedValue_5_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Multiplier, \
			InitialValue, \
			bFlipOrder, \
			bIntegrateDeltaTime, \
			Result, \
			AccumulatedValue, \
			Context \
		); \
	} \
	typedef FRigUnit_SimBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_AccumulateQuatMul>();


#define FRigUnit_AccumulateVectorMul_Execute() \
	void FRigUnit_AccumulateVectorMul::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FVector& Multiplier, \
		const FVector& InitialValue, \
		const bool bIntegrateDeltaTime, \
		FVector& Result, \
		FVector& AccumulatedValue, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Simulation_RigUnit_Accumulate_h_113_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorMul_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FVector& Multiplier, \
		const FVector& InitialValue, \
		const bool bIntegrateDeltaTime, \
		FVector& Result, \
		FVector& AccumulatedValue, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FVector& Multiplier = *(FVector*)RigVMMemoryHandles[0].GetData(); \
		const FVector& InitialValue = *(FVector*)RigVMMemoryHandles[1].GetData(); \
		const bool bIntegrateDeltaTime = *(bool*)RigVMMemoryHandles[2].GetData(); \
		FVector& Result = *(FVector*)RigVMMemoryHandles[3].GetData(); \
		FRigVMDynamicArray<FVector> AccumulatedValue_4_Array(*((FRigVMByteArray*)RigVMMemoryHandles[4].GetData(0, false))); \
		AccumulatedValue_4_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FVector& AccumulatedValue = AccumulatedValue_4_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Multiplier, \
			InitialValue, \
			bIntegrateDeltaTime, \
			Result, \
			AccumulatedValue, \
			Context \
		); \
	} \
	typedef FRigUnit_SimBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_AccumulateVectorMul>();


#define FRigUnit_AccumulateFloatMul_Execute() \
	void FRigUnit_AccumulateFloatMul::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const float Multiplier, \
		const float InitialValue, \
		const bool bIntegrateDeltaTime, \
		float& Result, \
		float& AccumulatedValue, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Simulation_RigUnit_Accumulate_h_80_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatMul_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const float Multiplier, \
		const float InitialValue, \
		const bool bIntegrateDeltaTime, \
		float& Result, \
		float& AccumulatedValue, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const float Multiplier = *(float*)RigVMMemoryHandles[0].GetData(); \
		const float InitialValue = *(float*)RigVMMemoryHandles[1].GetData(); \
		const bool bIntegrateDeltaTime = *(bool*)RigVMMemoryHandles[2].GetData(); \
		float& Result = *(float*)RigVMMemoryHandles[3].GetData(); \
		FRigVMDynamicArray<float> AccumulatedValue_4_Array(*((FRigVMByteArray*)RigVMMemoryHandles[4].GetData(0, false))); \
		AccumulatedValue_4_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		float& AccumulatedValue = AccumulatedValue_4_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Multiplier, \
			InitialValue, \
			bIntegrateDeltaTime, \
			Result, \
			AccumulatedValue, \
			Context \
		); \
	} \
	typedef FRigUnit_SimBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_AccumulateFloatMul>();


#define FRigUnit_AccumulateVectorAdd_Execute() \
	void FRigUnit_AccumulateVectorAdd::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FVector& Increment, \
		const FVector& InitialValue, \
		const bool bIntegrateDeltaTime, \
		FVector& Result, \
		FVector& AccumulatedValue, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Simulation_RigUnit_Accumulate_h_47_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_AccumulateVectorAdd_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FVector& Increment, \
		const FVector& InitialValue, \
		const bool bIntegrateDeltaTime, \
		FVector& Result, \
		FVector& AccumulatedValue, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FVector& Increment = *(FVector*)RigVMMemoryHandles[0].GetData(); \
		const FVector& InitialValue = *(FVector*)RigVMMemoryHandles[1].GetData(); \
		const bool bIntegrateDeltaTime = *(bool*)RigVMMemoryHandles[2].GetData(); \
		FVector& Result = *(FVector*)RigVMMemoryHandles[3].GetData(); \
		FRigVMDynamicArray<FVector> AccumulatedValue_4_Array(*((FRigVMByteArray*)RigVMMemoryHandles[4].GetData(0, false))); \
		AccumulatedValue_4_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FVector& AccumulatedValue = AccumulatedValue_4_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Increment, \
			InitialValue, \
			bIntegrateDeltaTime, \
			Result, \
			AccumulatedValue, \
			Context \
		); \
	} \
	typedef FRigUnit_SimBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_AccumulateVectorAdd>();


#define FRigUnit_AccumulateFloatAdd_Execute() \
	void FRigUnit_AccumulateFloatAdd::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const float Increment, \
		const float InitialValue, \
		const bool bIntegrateDeltaTime, \
		float& Result, \
		float& AccumulatedValue, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Simulation_RigUnit_Accumulate_h_14_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_AccumulateFloatAdd_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const float Increment, \
		const float InitialValue, \
		const bool bIntegrateDeltaTime, \
		float& Result, \
		float& AccumulatedValue, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const float Increment = *(float*)RigVMMemoryHandles[0].GetData(); \
		const float InitialValue = *(float*)RigVMMemoryHandles[1].GetData(); \
		const bool bIntegrateDeltaTime = *(bool*)RigVMMemoryHandles[2].GetData(); \
		float& Result = *(float*)RigVMMemoryHandles[3].GetData(); \
		FRigVMDynamicArray<float> AccumulatedValue_4_Array(*((FRigVMByteArray*)RigVMMemoryHandles[4].GetData(0, false))); \
		AccumulatedValue_4_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		float& AccumulatedValue = AccumulatedValue_4_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Increment, \
			InitialValue, \
			bIntegrateDeltaTime, \
			Result, \
			AccumulatedValue, \
			Context \
		); \
	} \
	typedef FRigUnit_SimBase Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_AccumulateFloatAdd>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Simulation_RigUnit_Accumulate_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
