// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_MovieSceneControlRigParameterTemplate_generated_h
#error "MovieSceneControlRigParameterTemplate.generated.h already included, missing '#pragma once' in MovieSceneControlRigParameterTemplate.h"
#endif
#define CONTROLRIG_MovieSceneControlRigParameterTemplate_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterTemplate_h_17_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMovieSceneControlRigParameterTemplate_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__Enums() { return STRUCT_OFFSET(FMovieSceneControlRigParameterTemplate, Enums); } \
	FORCEINLINE static uint32 __PPO__Integers() { return STRUCT_OFFSET(FMovieSceneControlRigParameterTemplate, Integers); } \
	typedef FMovieSceneParameterSectionTemplate Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FMovieSceneControlRigParameterTemplate>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Sequencer_MovieSceneControlRigParameterTemplate_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
