// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Public/Math/Simulation/CRSimPoint.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCRSimPoint() {}
// Cross Module References
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_ECRSimPointIntegrateType();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FCRSimPoint();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
// End Cross Module References
	static UEnum* ECRSimPointIntegrateType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ControlRig_ECRSimPointIntegrateType, Z_Construct_UPackage__Script_ControlRig(), TEXT("ECRSimPointIntegrateType"));
		}
		return Singleton;
	}
	template<> CONTROLRIG_API UEnum* StaticEnum<ECRSimPointIntegrateType>()
	{
		return ECRSimPointIntegrateType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ECRSimPointIntegrateType(ECRSimPointIntegrateType_StaticEnum, TEXT("/Script/ControlRig"), TEXT("ECRSimPointIntegrateType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ControlRig_ECRSimPointIntegrateType_Hash() { return 2963321074U; }
	UEnum* Z_Construct_UEnum_ControlRig_ECRSimPointIntegrateType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ECRSimPointIntegrateType"), 0, Get_Z_Construct_UEnum_ControlRig_ECRSimPointIntegrateType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ECRSimPointIntegrateType::Verlet", (int64)ECRSimPointIntegrateType::Verlet },
				{ "ECRSimPointIntegrateType::SemiExplicitEuler", (int64)ECRSimPointIntegrateType::SemiExplicitEuler },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/Math/Simulation/CRSimPoint.h" },
				{ "SemiExplicitEuler.Name", "ECRSimPointIntegrateType::SemiExplicitEuler" },
				{ "Verlet.Name", "ECRSimPointIntegrateType::Verlet" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ControlRig,
				nullptr,
				"ECRSimPointIntegrateType",
				"ECRSimPointIntegrateType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FCRSimPoint::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FCRSimPoint_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCRSimPoint, Z_Construct_UPackage__Script_ControlRig(), TEXT("CRSimPoint"), sizeof(FCRSimPoint), Get_Z_Construct_UScriptStruct_FCRSimPoint_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FCRSimPoint>()
{
	return FCRSimPoint::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCRSimPoint(FCRSimPoint::StaticStruct, TEXT("/Script/ControlRig"), TEXT("CRSimPoint"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFCRSimPoint
{
	FScriptStruct_ControlRig_StaticRegisterNativesFCRSimPoint()
	{
		UScriptStruct::DeferCppStructOps<FCRSimPoint>(FName(TEXT("CRSimPoint")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFCRSimPoint;
	struct Z_Construct_UScriptStruct_FCRSimPoint_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Mass_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Mass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Size_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Size;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LinearDamping_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_LinearDamping;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InheritMotion_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InheritMotion;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Position_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Position;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LinearVelocity_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LinearVelocity;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimPoint_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimPoint.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FCRSimPoint_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FCRSimPoint>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimPoint_Statics::NewProp_Mass_MetaData[] = {
		{ "Comment", "/**\n\x09 * The mass of the point\n\x09 */" },
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimPoint.h" },
		{ "ToolTip", "The mass of the point" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCRSimPoint_Statics::NewProp_Mass = { "Mass", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCRSimPoint, Mass), METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimPoint_Statics::NewProp_Mass_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimPoint_Statics::NewProp_Mass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimPoint_Statics::NewProp_Size_MetaData[] = {
		{ "Comment", "/**\n\x09 * Size of the point - only used for collision\n\x09 */" },
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimPoint.h" },
		{ "ToolTip", "Size of the point - only used for collision" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCRSimPoint_Statics::NewProp_Size = { "Size", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCRSimPoint, Size), METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimPoint_Statics::NewProp_Size_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimPoint_Statics::NewProp_Size_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimPoint_Statics::NewProp_LinearDamping_MetaData[] = {
		{ "Comment", "/**\n\x09 * The linear damping of the point\n\x09 */" },
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimPoint.h" },
		{ "ToolTip", "The linear damping of the point" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCRSimPoint_Statics::NewProp_LinearDamping = { "LinearDamping", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCRSimPoint, LinearDamping), METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimPoint_Statics::NewProp_LinearDamping_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimPoint_Statics::NewProp_LinearDamping_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimPoint_Statics::NewProp_InheritMotion_MetaData[] = {
		{ "Comment", "/**\n\x09 * Defines how much the point will inherit motion from its input.\n\x09 * This does not have an effect on passive (mass == 0.0) points.\n\x09 * Values can be higher than 1 due to timestep - but they are clamped internally.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimPoint.h" },
		{ "ToolTip", "Defines how much the point will inherit motion from its input.\nThis does not have an effect on passive (mass == 0.0) points.\nValues can be higher than 1 due to timestep - but they are clamped internally." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCRSimPoint_Statics::NewProp_InheritMotion = { "InheritMotion", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCRSimPoint, InheritMotion), METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimPoint_Statics::NewProp_InheritMotion_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimPoint_Statics::NewProp_InheritMotion_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimPoint_Statics::NewProp_Position_MetaData[] = {
		{ "Comment", "/**\n\x09 * The position of the point\n\x09 */" },
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimPoint.h" },
		{ "ToolTip", "The position of the point" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCRSimPoint_Statics::NewProp_Position = { "Position", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCRSimPoint, Position), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimPoint_Statics::NewProp_Position_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimPoint_Statics::NewProp_Position_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimPoint_Statics::NewProp_LinearVelocity_MetaData[] = {
		{ "Comment", "/**\n\x09 * The velocity of the point per second\n\x09 */" },
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimPoint.h" },
		{ "ToolTip", "The velocity of the point per second" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCRSimPoint_Statics::NewProp_LinearVelocity = { "LinearVelocity", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCRSimPoint, LinearVelocity), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimPoint_Statics::NewProp_LinearVelocity_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimPoint_Statics::NewProp_LinearVelocity_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FCRSimPoint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimPoint_Statics::NewProp_Mass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimPoint_Statics::NewProp_Size,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimPoint_Statics::NewProp_LinearDamping,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimPoint_Statics::NewProp_InheritMotion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimPoint_Statics::NewProp_Position,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimPoint_Statics::NewProp_LinearVelocity,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FCRSimPoint_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"CRSimPoint",
		sizeof(FCRSimPoint),
		alignof(FCRSimPoint),
		Z_Construct_UScriptStruct_FCRSimPoint_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimPoint_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimPoint_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimPoint_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FCRSimPoint()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FCRSimPoint_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CRSimPoint"), sizeof(FCRSimPoint), Get_Z_Construct_UScriptStruct_FCRSimPoint_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FCRSimPoint_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCRSimPoint_Hash() { return 2870062806U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
