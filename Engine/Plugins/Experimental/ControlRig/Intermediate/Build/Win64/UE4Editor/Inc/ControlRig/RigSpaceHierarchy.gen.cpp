// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Public/Rigs/RigSpaceHierarchy.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigSpaceHierarchy() {}
// Cross Module References
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_ERigSpaceType();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigSpaceHierarchy();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigSpace();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigElement();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
// End Cross Module References
	static UEnum* ERigSpaceType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ControlRig_ERigSpaceType, Z_Construct_UPackage__Script_ControlRig(), TEXT("ERigSpaceType"));
		}
		return Singleton;
	}
	template<> CONTROLRIG_API UEnum* StaticEnum<ERigSpaceType>()
	{
		return ERigSpaceType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ERigSpaceType(ERigSpaceType_StaticEnum, TEXT("/Script/ControlRig"), TEXT("ERigSpaceType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ControlRig_ERigSpaceType_Hash() { return 1008640522U; }
	UEnum* Z_Construct_UEnum_ControlRig_ERigSpaceType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ERigSpaceType"), 0, Get_Z_Construct_UEnum_ControlRig_ERigSpaceType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ERigSpaceType::Global", (int64)ERigSpaceType::Global },
				{ "ERigSpaceType::Bone", (int64)ERigSpaceType::Bone },
				{ "ERigSpaceType::Control", (int64)ERigSpaceType::Control },
				{ "ERigSpaceType::Space", (int64)ERigSpaceType::Space },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Bone.Comment", "/** Attached to a bone */" },
				{ "Bone.Name", "ERigSpaceType::Bone" },
				{ "Bone.ToolTip", "Attached to a bone" },
				{ "Control.Comment", "/** Attached to a control */" },
				{ "Control.Name", "ERigSpaceType::Control" },
				{ "Control.ToolTip", "Attached to a control" },
				{ "Global.Comment", "/** Not attached to anything */" },
				{ "Global.Name", "ERigSpaceType::Global" },
				{ "Global.ToolTip", "Not attached to anything" },
				{ "ModuleRelativePath", "Public/Rigs/RigSpaceHierarchy.h" },
				{ "Space.Comment", "/** Attached to a space*/" },
				{ "Space.Name", "ERigSpaceType::Space" },
				{ "Space.ToolTip", "Attached to a space" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ControlRig,
				nullptr,
				"ERigSpaceType",
				"ERigSpaceType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FRigSpaceHierarchy::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigSpaceHierarchy_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigSpaceHierarchy, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigSpaceHierarchy"), sizeof(FRigSpaceHierarchy), Get_Z_Construct_UScriptStruct_FRigSpaceHierarchy_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigSpaceHierarchy>()
{
	return FRigSpaceHierarchy::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigSpaceHierarchy(FRigSpaceHierarchy::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigSpaceHierarchy"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigSpaceHierarchy
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigSpaceHierarchy()
	{
		UScriptStruct::DeferCppStructOps<FRigSpaceHierarchy>(FName(TEXT("RigSpaceHierarchy")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigSpaceHierarchy;
	struct Z_Construct_UScriptStruct_FRigSpaceHierarchy_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Spaces_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Spaces_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Spaces;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NameToIndexMapping_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_NameToIndexMapping_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NameToIndexMapping_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_NameToIndexMapping;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Selection_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Selection_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Selection;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigSpaceHierarchy_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Rigs/RigSpaceHierarchy.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigSpaceHierarchy_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigSpaceHierarchy>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigSpaceHierarchy_Statics::NewProp_Spaces_Inner = { "Spaces", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRigSpace, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigSpaceHierarchy_Statics::NewProp_Spaces_MetaData[] = {
		{ "Category", "FRigSpaceHierarchy" },
		{ "ModuleRelativePath", "Public/Rigs/RigSpaceHierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigSpaceHierarchy_Statics::NewProp_Spaces = { "Spaces", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigSpaceHierarchy, Spaces), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigSpaceHierarchy_Statics::NewProp_Spaces_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigSpaceHierarchy_Statics::NewProp_Spaces_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigSpaceHierarchy_Statics::NewProp_NameToIndexMapping_ValueProp = { "NameToIndexMapping", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigSpaceHierarchy_Statics::NewProp_NameToIndexMapping_Key_KeyProp = { "NameToIndexMapping_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigSpaceHierarchy_Statics::NewProp_NameToIndexMapping_MetaData[] = {
		{ "Comment", "// can serialize fine? \n" },
		{ "ModuleRelativePath", "Public/Rigs/RigSpaceHierarchy.h" },
		{ "ToolTip", "can serialize fine?" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FRigSpaceHierarchy_Statics::NewProp_NameToIndexMapping = { "NameToIndexMapping", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigSpaceHierarchy, NameToIndexMapping), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigSpaceHierarchy_Statics::NewProp_NameToIndexMapping_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigSpaceHierarchy_Statics::NewProp_NameToIndexMapping_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigSpaceHierarchy_Statics::NewProp_Selection_Inner = { "Selection", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigSpaceHierarchy_Statics::NewProp_Selection_MetaData[] = {
		{ "ModuleRelativePath", "Public/Rigs/RigSpaceHierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigSpaceHierarchy_Statics::NewProp_Selection = { "Selection", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigSpaceHierarchy, Selection), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigSpaceHierarchy_Statics::NewProp_Selection_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigSpaceHierarchy_Statics::NewProp_Selection_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigSpaceHierarchy_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigSpaceHierarchy_Statics::NewProp_Spaces_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigSpaceHierarchy_Statics::NewProp_Spaces,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigSpaceHierarchy_Statics::NewProp_NameToIndexMapping_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigSpaceHierarchy_Statics::NewProp_NameToIndexMapping_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigSpaceHierarchy_Statics::NewProp_NameToIndexMapping,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigSpaceHierarchy_Statics::NewProp_Selection_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigSpaceHierarchy_Statics::NewProp_Selection,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigSpaceHierarchy_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"RigSpaceHierarchy",
		sizeof(FRigSpaceHierarchy),
		alignof(FRigSpaceHierarchy),
		Z_Construct_UScriptStruct_FRigSpaceHierarchy_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigSpaceHierarchy_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigSpaceHierarchy_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigSpaceHierarchy_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigSpaceHierarchy()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigSpaceHierarchy_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigSpaceHierarchy"), sizeof(FRigSpaceHierarchy), Get_Z_Construct_UScriptStruct_FRigSpaceHierarchy_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigSpaceHierarchy_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigSpaceHierarchy_Hash() { return 3433758592U; }

static_assert(std::is_polymorphic<FRigSpace>() == std::is_polymorphic<FRigElement>(), "USTRUCT FRigSpace cannot be polymorphic unless super FRigElement is polymorphic");

class UScriptStruct* FRigSpace::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigSpace_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigSpace, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigSpace"), sizeof(FRigSpace), Get_Z_Construct_UScriptStruct_FRigSpace_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigSpace>()
{
	return FRigSpace::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigSpace(FRigSpace::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigSpace"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigSpace
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigSpace()
	{
		UScriptStruct::DeferCppStructOps<FRigSpace>(FName(TEXT("RigSpace")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigSpace;
	struct Z_Construct_UScriptStruct_FRigSpace_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_SpaceType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpaceType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_SpaceType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParentName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ParentName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParentIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ParentIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InitialTransform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InitialTransform;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LocalTransform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LocalTransform;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigSpace_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Rigs/RigSpaceHierarchy.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigSpace_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigSpace>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRigSpace_Statics::NewProp_SpaceType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigSpace_Statics::NewProp_SpaceType_MetaData[] = {
		{ "Category", "FRigElement" },
		{ "ModuleRelativePath", "Public/Rigs/RigSpaceHierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FRigSpace_Statics::NewProp_SpaceType = { "SpaceType", nullptr, (EPropertyFlags)0x0010000000020005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigSpace, SpaceType), Z_Construct_UEnum_ControlRig_ERigSpaceType, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigSpace_Statics::NewProp_SpaceType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigSpace_Statics::NewProp_SpaceType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigSpace_Statics::NewProp_ParentName_MetaData[] = {
		{ "Category", "FRigElement" },
		{ "ModuleRelativePath", "Public/Rigs/RigSpaceHierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigSpace_Statics::NewProp_ParentName = { "ParentName", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigSpace, ParentName), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigSpace_Statics::NewProp_ParentName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigSpace_Statics::NewProp_ParentName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigSpace_Statics::NewProp_ParentIndex_MetaData[] = {
		{ "Category", "FRigElement" },
		{ "ModuleRelativePath", "Public/Rigs/RigSpaceHierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigSpace_Statics::NewProp_ParentIndex = { "ParentIndex", nullptr, (EPropertyFlags)0x0010000000002014, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigSpace, ParentIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigSpace_Statics::NewProp_ParentIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigSpace_Statics::NewProp_ParentIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigSpace_Statics::NewProp_InitialTransform_MetaData[] = {
		{ "Category", "FRigElement" },
		{ "ModuleRelativePath", "Public/Rigs/RigSpaceHierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigSpace_Statics::NewProp_InitialTransform = { "InitialTransform", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigSpace, InitialTransform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigSpace_Statics::NewProp_InitialTransform_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigSpace_Statics::NewProp_InitialTransform_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigSpace_Statics::NewProp_LocalTransform_MetaData[] = {
		{ "Category", "FRigElement" },
		{ "ModuleRelativePath", "Public/Rigs/RigSpaceHierarchy.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigSpace_Statics::NewProp_LocalTransform = { "LocalTransform", nullptr, (EPropertyFlags)0x0010000000002015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigSpace, LocalTransform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigSpace_Statics::NewProp_LocalTransform_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigSpace_Statics::NewProp_LocalTransform_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigSpace_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigSpace_Statics::NewProp_SpaceType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigSpace_Statics::NewProp_SpaceType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigSpace_Statics::NewProp_ParentName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigSpace_Statics::NewProp_ParentIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigSpace_Statics::NewProp_InitialTransform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigSpace_Statics::NewProp_LocalTransform,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigSpace_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigElement,
		&NewStructOps,
		"RigSpace",
		sizeof(FRigSpace),
		alignof(FRigSpace),
		Z_Construct_UScriptStruct_FRigSpace_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigSpace_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigSpace_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigSpace_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigSpace()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigSpace_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigSpace"), sizeof(FRigSpace), Get_Z_Construct_UScriptStruct_FRigSpace_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigSpace_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigSpace_Hash() { return 882774784U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
