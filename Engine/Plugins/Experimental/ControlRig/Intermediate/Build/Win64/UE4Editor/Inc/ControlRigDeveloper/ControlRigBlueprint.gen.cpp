// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRigDeveloper/Public/ControlRigBlueprint.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeControlRigBlueprint() {}
// Cross Module References
	CONTROLRIGDEVELOPER_API UClass* Z_Construct_UClass_UControlRigBlueprint_NoRegister();
	CONTROLRIGDEVELOPER_API UClass* Z_Construct_UClass_UControlRigBlueprint();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprint();
	UPackage* Z_Construct_UPackage__Script_ControlRigDeveloper();
	COREUOBJECT_API UClass* Z_Construct_UClass_UStruct();
	CONTROLRIGDEVELOPER_API UClass* Z_Construct_UClass_UControlRigHierarchyModifier_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USkeletalMesh_NoRegister();
	RIGVMDEVELOPER_API UScriptStruct* Z_Construct_UScriptStruct_FRigVMCompileSettings();
	RIGVMDEVELOPER_API UClass* Z_Construct_UClass_URigVMGraph_NoRegister();
	RIGVMDEVELOPER_API UClass* Z_Construct_UClass_URigVMController_NoRegister();
	RIGVM_API UScriptStruct* Z_Construct_UScriptStruct_FRigVMOperand();
	CONTROLRIG_API UClass* Z_Construct_UClass_UControlRigGizmoLibrary_NoRegister();
	RIGVM_API UScriptStruct* Z_Construct_UScriptStruct_FRigVMStatistics();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FControlRigDrawContainer();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigInfluenceMapPerEvent();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigHierarchyContainer();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigBoneHierarchy();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigCurveContainer();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	CONTROLRIG_API UClass* Z_Construct_UClass_UControlRigValidator_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UInterface_PreviewMeshProvider_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UControlRigBlueprint::execGetHierarchyModifier)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UControlRigHierarchyModifier**)Z_Param__Result=P_THIS->GetHierarchyModifier();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigBlueprint::execGetAvailableRigUnits)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<UStruct*>*)Z_Param__Result=UControlRigBlueprint::GetAvailableRigUnits();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigBlueprint::execGetCurrentlyOpenRigBlueprints)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<UControlRigBlueprint*>*)Z_Param__Result=UControlRigBlueprint::GetCurrentlyOpenRigBlueprints();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigBlueprint::execRequestControlRigInit)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->RequestControlRigInit();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigBlueprint::execRequestAutoVMRecompilation)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->RequestAutoVMRecompilation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigBlueprint::execRecompileVMIfRequired)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->RecompileVMIfRequired();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigBlueprint::execRecompileVM)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->RecompileVM();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigBlueprint::execGetPreviewMesh)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(USkeletalMesh**)Z_Param__Result=P_THIS->GetPreviewMesh();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UControlRigBlueprint::execSetPreviewMesh)
	{
		P_GET_OBJECT(USkeletalMesh,Z_Param_PreviewMesh);
		P_GET_UBOOL(Z_Param_bMarkAsDirty);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetPreviewMesh(Z_Param_PreviewMesh,Z_Param_bMarkAsDirty);
		P_NATIVE_END;
	}
#if WITH_EDITOR
	DEFINE_FUNCTION(UControlRigBlueprint::execSuspendNotifications)
	{
		P_GET_UBOOL(Z_Param_bSuspendNotifs);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SuspendNotifications(Z_Param_bSuspendNotifs);
		P_NATIVE_END;
	}
#endif //WITH_EDITOR
	void UControlRigBlueprint::StaticRegisterNativesUControlRigBlueprint()
	{
		UClass* Class = UControlRigBlueprint::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetAvailableRigUnits", &UControlRigBlueprint::execGetAvailableRigUnits },
			{ "GetCurrentlyOpenRigBlueprints", &UControlRigBlueprint::execGetCurrentlyOpenRigBlueprints },
			{ "GetHierarchyModifier", &UControlRigBlueprint::execGetHierarchyModifier },
			{ "GetPreviewMesh", &UControlRigBlueprint::execGetPreviewMesh },
			{ "RecompileVM", &UControlRigBlueprint::execRecompileVM },
			{ "RecompileVMIfRequired", &UControlRigBlueprint::execRecompileVMIfRequired },
			{ "RequestAutoVMRecompilation", &UControlRigBlueprint::execRequestAutoVMRecompilation },
			{ "RequestControlRigInit", &UControlRigBlueprint::execRequestControlRigInit },
			{ "SetPreviewMesh", &UControlRigBlueprint::execSetPreviewMesh },
#if WITH_EDITOR
			{ "SuspendNotifications", &UControlRigBlueprint::execSuspendNotifications },
#endif // WITH_EDITOR
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UControlRigBlueprint_GetAvailableRigUnits_Statics
	{
		struct ControlRigBlueprint_eventGetAvailableRigUnits_Parms
		{
			TArray<UStruct*> ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UControlRigBlueprint_GetAvailableRigUnits_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UStruct, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UControlRigBlueprint_GetAvailableRigUnits_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigBlueprint_eventGetAvailableRigUnits_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigBlueprint_GetAvailableRigUnits_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigBlueprint_GetAvailableRigUnits_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigBlueprint_GetAvailableRigUnits_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigBlueprint_GetAvailableRigUnits_Statics::Function_MetaDataParams[] = {
		{ "Category", "VM" },
		{ "ModuleRelativePath", "Public/ControlRigBlueprint.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigBlueprint_GetAvailableRigUnits_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigBlueprint, nullptr, "GetAvailableRigUnits", nullptr, nullptr, sizeof(ControlRigBlueprint_eventGetAvailableRigUnits_Parms), Z_Construct_UFunction_UControlRigBlueprint_GetAvailableRigUnits_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigBlueprint_GetAvailableRigUnits_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigBlueprint_GetAvailableRigUnits_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigBlueprint_GetAvailableRigUnits_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigBlueprint_GetAvailableRigUnits()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigBlueprint_GetAvailableRigUnits_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UControlRigBlueprint_GetCurrentlyOpenRigBlueprints_Statics
	{
		struct ControlRigBlueprint_eventGetCurrentlyOpenRigBlueprints_Parms
		{
			TArray<UControlRigBlueprint*> ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UControlRigBlueprint_GetCurrentlyOpenRigBlueprints_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UControlRigBlueprint_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UControlRigBlueprint_GetCurrentlyOpenRigBlueprints_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigBlueprint_eventGetCurrentlyOpenRigBlueprints_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigBlueprint_GetCurrentlyOpenRigBlueprints_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigBlueprint_GetCurrentlyOpenRigBlueprints_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigBlueprint_GetCurrentlyOpenRigBlueprints_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigBlueprint_GetCurrentlyOpenRigBlueprints_Statics::Function_MetaDataParams[] = {
		{ "Category", "VM" },
		{ "ModuleRelativePath", "Public/ControlRigBlueprint.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigBlueprint_GetCurrentlyOpenRigBlueprints_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigBlueprint, nullptr, "GetCurrentlyOpenRigBlueprints", nullptr, nullptr, sizeof(ControlRigBlueprint_eventGetCurrentlyOpenRigBlueprints_Parms), Z_Construct_UFunction_UControlRigBlueprint_GetCurrentlyOpenRigBlueprints_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigBlueprint_GetCurrentlyOpenRigBlueprints_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigBlueprint_GetCurrentlyOpenRigBlueprints_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigBlueprint_GetCurrentlyOpenRigBlueprints_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigBlueprint_GetCurrentlyOpenRigBlueprints()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigBlueprint_GetCurrentlyOpenRigBlueprints_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UControlRigBlueprint_GetHierarchyModifier_Statics
	{
		struct ControlRigBlueprint_eventGetHierarchyModifier_Parms
		{
			UControlRigHierarchyModifier* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UControlRigBlueprint_GetHierarchyModifier_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigBlueprint_eventGetHierarchyModifier_Parms, ReturnValue), Z_Construct_UClass_UControlRigHierarchyModifier_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigBlueprint_GetHierarchyModifier_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigBlueprint_GetHierarchyModifier_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigBlueprint_GetHierarchyModifier_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "ModuleRelativePath", "Public/ControlRigBlueprint.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigBlueprint_GetHierarchyModifier_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigBlueprint, nullptr, "GetHierarchyModifier", nullptr, nullptr, sizeof(ControlRigBlueprint_eventGetHierarchyModifier_Parms), Z_Construct_UFunction_UControlRigBlueprint_GetHierarchyModifier_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigBlueprint_GetHierarchyModifier_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigBlueprint_GetHierarchyModifier_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigBlueprint_GetHierarchyModifier_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigBlueprint_GetHierarchyModifier()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigBlueprint_GetHierarchyModifier_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UControlRigBlueprint_GetPreviewMesh_Statics
	{
		struct ControlRigBlueprint_eventGetPreviewMesh_Parms
		{
			USkeletalMesh* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UControlRigBlueprint_GetPreviewMesh_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigBlueprint_eventGetPreviewMesh_Parms, ReturnValue), Z_Construct_UClass_USkeletalMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigBlueprint_GetPreviewMesh_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigBlueprint_GetPreviewMesh_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigBlueprint_GetPreviewMesh_Statics::Function_MetaDataParams[] = {
		{ "Category", "Control Rig Blueprint" },
		{ "ModuleRelativePath", "Public/ControlRigBlueprint.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigBlueprint_GetPreviewMesh_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigBlueprint, nullptr, "GetPreviewMesh", nullptr, nullptr, sizeof(ControlRigBlueprint_eventGetPreviewMesh_Parms), Z_Construct_UFunction_UControlRigBlueprint_GetPreviewMesh_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigBlueprint_GetPreviewMesh_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigBlueprint_GetPreviewMesh_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigBlueprint_GetPreviewMesh_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigBlueprint_GetPreviewMesh()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigBlueprint_GetPreviewMesh_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UControlRigBlueprint_RecompileVM_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigBlueprint_RecompileVM_Statics::Function_MetaDataParams[] = {
		{ "Category", "Control Rig Blueprint" },
		{ "ModuleRelativePath", "Public/ControlRigBlueprint.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigBlueprint_RecompileVM_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigBlueprint, nullptr, "RecompileVM", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigBlueprint_RecompileVM_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigBlueprint_RecompileVM_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigBlueprint_RecompileVM()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigBlueprint_RecompileVM_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UControlRigBlueprint_RecompileVMIfRequired_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigBlueprint_RecompileVMIfRequired_Statics::Function_MetaDataParams[] = {
		{ "Category", "Control Rig Blueprint" },
		{ "ModuleRelativePath", "Public/ControlRigBlueprint.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigBlueprint_RecompileVMIfRequired_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigBlueprint, nullptr, "RecompileVMIfRequired", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigBlueprint_RecompileVMIfRequired_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigBlueprint_RecompileVMIfRequired_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigBlueprint_RecompileVMIfRequired()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigBlueprint_RecompileVMIfRequired_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UControlRigBlueprint_RequestAutoVMRecompilation_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigBlueprint_RequestAutoVMRecompilation_Statics::Function_MetaDataParams[] = {
		{ "Category", "Control Rig Blueprint" },
		{ "ModuleRelativePath", "Public/ControlRigBlueprint.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigBlueprint_RequestAutoVMRecompilation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigBlueprint, nullptr, "RequestAutoVMRecompilation", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigBlueprint_RequestAutoVMRecompilation_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigBlueprint_RequestAutoVMRecompilation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigBlueprint_RequestAutoVMRecompilation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigBlueprint_RequestAutoVMRecompilation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UControlRigBlueprint_RequestControlRigInit_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigBlueprint_RequestControlRigInit_Statics::Function_MetaDataParams[] = {
		{ "Category", "Control Rig Blueprint" },
		{ "ModuleRelativePath", "Public/ControlRigBlueprint.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigBlueprint_RequestControlRigInit_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigBlueprint, nullptr, "RequestControlRigInit", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigBlueprint_RequestControlRigInit_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigBlueprint_RequestControlRigInit_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigBlueprint_RequestControlRigInit()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigBlueprint_RequestControlRigInit_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UControlRigBlueprint_SetPreviewMesh_Statics
	{
		struct ControlRigBlueprint_eventSetPreviewMesh_Parms
		{
			USkeletalMesh* PreviewMesh;
			bool bMarkAsDirty;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviewMesh;
		static void NewProp_bMarkAsDirty_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bMarkAsDirty;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UControlRigBlueprint_SetPreviewMesh_Statics::NewProp_PreviewMesh = { "PreviewMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ControlRigBlueprint_eventSetPreviewMesh_Parms, PreviewMesh), Z_Construct_UClass_USkeletalMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UControlRigBlueprint_SetPreviewMesh_Statics::NewProp_bMarkAsDirty_SetBit(void* Obj)
	{
		((ControlRigBlueprint_eventSetPreviewMesh_Parms*)Obj)->bMarkAsDirty = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UControlRigBlueprint_SetPreviewMesh_Statics::NewProp_bMarkAsDirty = { "bMarkAsDirty", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ControlRigBlueprint_eventSetPreviewMesh_Parms), &Z_Construct_UFunction_UControlRigBlueprint_SetPreviewMesh_Statics::NewProp_bMarkAsDirty_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigBlueprint_SetPreviewMesh_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigBlueprint_SetPreviewMesh_Statics::NewProp_PreviewMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigBlueprint_SetPreviewMesh_Statics::NewProp_bMarkAsDirty,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigBlueprint_SetPreviewMesh_Statics::Function_MetaDataParams[] = {
		{ "Category", "Control Rig Blueprint" },
		{ "Comment", "/** IInterface_PreviewMeshProvider interface */" },
		{ "CPP_Default_bMarkAsDirty", "true" },
		{ "ModuleRelativePath", "Public/ControlRigBlueprint.h" },
		{ "ToolTip", "IInterface_PreviewMeshProvider interface" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigBlueprint_SetPreviewMesh_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigBlueprint, nullptr, "SetPreviewMesh", nullptr, nullptr, sizeof(ControlRigBlueprint_eventSetPreviewMesh_Parms), Z_Construct_UFunction_UControlRigBlueprint_SetPreviewMesh_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigBlueprint_SetPreviewMesh_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigBlueprint_SetPreviewMesh_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigBlueprint_SetPreviewMesh_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigBlueprint_SetPreviewMesh()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigBlueprint_SetPreviewMesh_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#if WITH_EDITOR
	struct Z_Construct_UFunction_UControlRigBlueprint_SuspendNotifications_Statics
	{
		struct ControlRigBlueprint_eventSuspendNotifications_Parms
		{
			bool bSuspendNotifs;
		};
		static void NewProp_bSuspendNotifs_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSuspendNotifs;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UControlRigBlueprint_SuspendNotifications_Statics::NewProp_bSuspendNotifs_SetBit(void* Obj)
	{
		((ControlRigBlueprint_eventSuspendNotifications_Parms*)Obj)->bSuspendNotifs = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UControlRigBlueprint_SuspendNotifications_Statics::NewProp_bSuspendNotifs = { "bSuspendNotifs", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ControlRigBlueprint_eventSuspendNotifications_Parms), &Z_Construct_UFunction_UControlRigBlueprint_SuspendNotifications_Statics::NewProp_bSuspendNotifs_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UControlRigBlueprint_SuspendNotifications_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UControlRigBlueprint_SuspendNotifications_Statics::NewProp_bSuspendNotifs,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UControlRigBlueprint_SuspendNotifications_Statics::Function_MetaDataParams[] = {
		{ "Category", "Control Rig Blueprint" },
		{ "ModuleRelativePath", "Public/ControlRigBlueprint.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UControlRigBlueprint_SuspendNotifications_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UControlRigBlueprint, nullptr, "SuspendNotifications", nullptr, nullptr, sizeof(ControlRigBlueprint_eventSuspendNotifications_Parms), Z_Construct_UFunction_UControlRigBlueprint_SuspendNotifications_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigBlueprint_SuspendNotifications_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24040401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UControlRigBlueprint_SuspendNotifications_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UControlRigBlueprint_SuspendNotifications_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UControlRigBlueprint_SuspendNotifications()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UControlRigBlueprint_SuspendNotifications_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
	UClass* Z_Construct_UClass_UControlRigBlueprint_NoRegister()
	{
		return UControlRigBlueprint::StaticClass();
	}
	struct Z_Construct_UClass_UControlRigBlueprint_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VMCompileSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_VMCompileSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Model_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Model;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Controller_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Controller;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PinToOperandMap_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PinToOperandMap_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PinToOperandMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_PinToOperandMap;
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GizmoLibrary_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_GizmoLibrary;
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Statistics_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Statistics;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DrawContainer_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DrawContainer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Influences_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Influences;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HierarchyContainer_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HierarchyContainer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Hierarchy_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Hierarchy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurveContainer_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CurveContainer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSupportsInversion_MetaData[];
#endif
		static void NewProp_bSupportsInversion_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSupportsInversion;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSupportsControls_MetaData[];
#endif
		static void NewProp_bSupportsControls_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSupportsControls;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviewSkeletalMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_PreviewSkeletalMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourceHierarchyImport_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_SourceHierarchyImport;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourceCurveImport_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_SourceCurveImport;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_SupportedEventNames_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SupportedEventNames_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SupportedEventNames;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bExposesAnimatableControls_MetaData[];
#endif
		static void NewProp_bExposesAnimatableControls_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bExposesAnimatableControls;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoRecompileVM_MetaData[];
#endif
		static void NewProp_bAutoRecompileVM_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoRecompileVM;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bVMRecompilationRequired_MetaData[];
#endif
		static void NewProp_bVMRecompilationRequired_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bVMRecompilationRequired;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VMRecompilationBracket_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_VMRecompilationBracket;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HierarchyModifier_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_HierarchyModifier;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Validator_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Validator;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UControlRigBlueprint_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprint,
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRigDeveloper,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UControlRigBlueprint_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UControlRigBlueprint_GetAvailableRigUnits, "GetAvailableRigUnits" }, // 4213264788
		{ &Z_Construct_UFunction_UControlRigBlueprint_GetCurrentlyOpenRigBlueprints, "GetCurrentlyOpenRigBlueprints" }, // 2810507492
		{ &Z_Construct_UFunction_UControlRigBlueprint_GetHierarchyModifier, "GetHierarchyModifier" }, // 966524631
		{ &Z_Construct_UFunction_UControlRigBlueprint_GetPreviewMesh, "GetPreviewMesh" }, // 170159067
		{ &Z_Construct_UFunction_UControlRigBlueprint_RecompileVM, "RecompileVM" }, // 247895489
		{ &Z_Construct_UFunction_UControlRigBlueprint_RecompileVMIfRequired, "RecompileVMIfRequired" }, // 1195052607
		{ &Z_Construct_UFunction_UControlRigBlueprint_RequestAutoVMRecompilation, "RequestAutoVMRecompilation" }, // 1969885069
		{ &Z_Construct_UFunction_UControlRigBlueprint_RequestControlRigInit, "RequestControlRigInit" }, // 1571455934
		{ &Z_Construct_UFunction_UControlRigBlueprint_SetPreviewMesh, "SetPreviewMesh" }, // 153146283
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UControlRigBlueprint_SuspendNotifications, "SuspendNotifications" }, // 1335157187
#endif //WITH_EDITOR
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigBlueprint_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IgnoreClassThumbnail", "" },
		{ "IncludePath", "ControlRigBlueprint.h" },
		{ "ModuleRelativePath", "Public/ControlRigBlueprint.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_VMCompileSettings_MetaData[] = {
		{ "Category", "VM" },
		{ "ModuleRelativePath", "Public/ControlRigBlueprint.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_VMCompileSettings = { "VMCompileSettings", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigBlueprint, VMCompileSettings), Z_Construct_UScriptStruct_FRigVMCompileSettings, METADATA_PARAMS(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_VMCompileSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_VMCompileSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_Model_MetaData[] = {
		{ "Category", "VM" },
		{ "ModuleRelativePath", "Public/ControlRigBlueprint.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_Model = { "Model", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigBlueprint, Model), Z_Construct_UClass_URigVMGraph_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_Model_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_Model_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_Controller_MetaData[] = {
		{ "Category", "VM" },
		{ "ModuleRelativePath", "Public/ControlRigBlueprint.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_Controller = { "Controller", nullptr, (EPropertyFlags)0x0010000000002014, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigBlueprint, Controller), Z_Construct_UClass_URigVMController_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_Controller_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_Controller_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_PinToOperandMap_ValueProp = { "PinToOperandMap", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FRigVMOperand, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_PinToOperandMap_Key_KeyProp = { "PinToOperandMap_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_PinToOperandMap_MetaData[] = {
		{ "ModuleRelativePath", "Public/ControlRigBlueprint.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_PinToOperandMap = { "PinToOperandMap", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigBlueprint, PinToOperandMap), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_PinToOperandMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_PinToOperandMap_MetaData)) };
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_GizmoLibrary_MetaData[] = {
		{ "Category", "DefaultGizmo" },
		{ "ModuleRelativePath", "Public/ControlRigBlueprint.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_GizmoLibrary = { "GizmoLibrary", nullptr, (EPropertyFlags)0x0014000800004001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigBlueprint, GizmoLibrary), Z_Construct_UClass_UControlRigGizmoLibrary_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_GizmoLibrary_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_GizmoLibrary_MetaData)) };
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_Statistics_MetaData[] = {
		{ "Category", "VM" },
		{ "DisplayAfter", "VMCompileSettings" },
		{ "DisplayName", "VM Statistics" },
		{ "ModuleRelativePath", "Public/ControlRigBlueprint.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_Statistics = { "Statistics", nullptr, (EPropertyFlags)0x0010000000022001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigBlueprint, Statistics), Z_Construct_UScriptStruct_FRigVMStatistics, METADATA_PARAMS(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_Statistics_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_Statistics_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_DrawContainer_MetaData[] = {
		{ "Category", "Drawing" },
		{ "ModuleRelativePath", "Public/ControlRigBlueprint.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_DrawContainer = { "DrawContainer", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigBlueprint, DrawContainer), Z_Construct_UScriptStruct_FControlRigDrawContainer, METADATA_PARAMS(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_DrawContainer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_DrawContainer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_Influences_MetaData[] = {
		{ "Category", "Influence Map" },
		{ "ModuleRelativePath", "Public/ControlRigBlueprint.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_Influences = { "Influences", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigBlueprint, Influences), Z_Construct_UScriptStruct_FRigInfluenceMapPerEvent, METADATA_PARAMS(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_Influences_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_Influences_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_HierarchyContainer_MetaData[] = {
		{ "ModuleRelativePath", "Public/ControlRigBlueprint.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_HierarchyContainer = { "HierarchyContainer", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigBlueprint, HierarchyContainer), Z_Construct_UScriptStruct_FRigHierarchyContainer, METADATA_PARAMS(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_HierarchyContainer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_HierarchyContainer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_Hierarchy_MetaData[] = {
		{ "ModuleRelativePath", "Public/ControlRigBlueprint.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_Hierarchy = { "Hierarchy", nullptr, (EPropertyFlags)0x0040000020000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigBlueprint, Hierarchy_DEPRECATED), Z_Construct_UScriptStruct_FRigBoneHierarchy, METADATA_PARAMS(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_Hierarchy_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_Hierarchy_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_CurveContainer_MetaData[] = {
		{ "ModuleRelativePath", "Public/ControlRigBlueprint.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_CurveContainer = { "CurveContainer", nullptr, (EPropertyFlags)0x0040000020000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigBlueprint, CurveContainer_DEPRECATED), Z_Construct_UScriptStruct_FRigCurveContainer, METADATA_PARAMS(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_CurveContainer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_CurveContainer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_bSupportsInversion_MetaData[] = {
		{ "Comment", "/** Whether or not this rig has an Inversion Event */" },
		{ "ModuleRelativePath", "Public/ControlRigBlueprint.h" },
		{ "ToolTip", "Whether or not this rig has an Inversion Event" },
	};
#endif
	void Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_bSupportsInversion_SetBit(void* Obj)
	{
		((UControlRigBlueprint*)Obj)->bSupportsInversion = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_bSupportsInversion = { "bSupportsInversion", nullptr, (EPropertyFlags)0x0040010000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UControlRigBlueprint), &Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_bSupportsInversion_SetBit, METADATA_PARAMS(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_bSupportsInversion_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_bSupportsInversion_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_bSupportsControls_MetaData[] = {
		{ "Comment", "/** Whether or not this rig has Controls on It */" },
		{ "ModuleRelativePath", "Public/ControlRigBlueprint.h" },
		{ "ToolTip", "Whether or not this rig has Controls on It" },
	};
#endif
	void Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_bSupportsControls_SetBit(void* Obj)
	{
		((UControlRigBlueprint*)Obj)->bSupportsControls = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_bSupportsControls = { "bSupportsControls", nullptr, (EPropertyFlags)0x0040010000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UControlRigBlueprint), &Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_bSupportsControls_SetBit, METADATA_PARAMS(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_bSupportsControls_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_bSupportsControls_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_PreviewSkeletalMesh_MetaData[] = {
		{ "Comment", "/** The default skeletal mesh to use when previewing this asset */" },
		{ "ModuleRelativePath", "Public/ControlRigBlueprint.h" },
		{ "ToolTip", "The default skeletal mesh to use when previewing this asset" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_PreviewSkeletalMesh = { "PreviewSkeletalMesh", nullptr, (EPropertyFlags)0x0044010000000000, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigBlueprint, PreviewSkeletalMesh), Z_Construct_UClass_USkeletalMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_PreviewSkeletalMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_PreviewSkeletalMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_SourceHierarchyImport_MetaData[] = {
		{ "Comment", "/** The skeleton from import into a hierarchy */" },
		{ "ModuleRelativePath", "Public/ControlRigBlueprint.h" },
		{ "ToolTip", "The skeleton from import into a hierarchy" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_SourceHierarchyImport = { "SourceHierarchyImport", nullptr, (EPropertyFlags)0x0044010000200000, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigBlueprint, SourceHierarchyImport), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_SourceHierarchyImport_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_SourceHierarchyImport_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_SourceCurveImport_MetaData[] = {
		{ "Comment", "/** The skeleton from import into a curve */" },
		{ "ModuleRelativePath", "Public/ControlRigBlueprint.h" },
		{ "ToolTip", "The skeleton from import into a curve" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_SourceCurveImport = { "SourceCurveImport", nullptr, (EPropertyFlags)0x0044010000200000, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigBlueprint, SourceCurveImport), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_SourceCurveImport_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_SourceCurveImport_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_SupportedEventNames_Inner = { "SupportedEventNames", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_SupportedEventNames_MetaData[] = {
		{ "Comment", "/** The event names this control rig blueprint contains */" },
		{ "ModuleRelativePath", "Public/ControlRigBlueprint.h" },
		{ "ToolTip", "The event names this control rig blueprint contains" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_SupportedEventNames = { "SupportedEventNames", nullptr, (EPropertyFlags)0x0040010000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigBlueprint, SupportedEventNames), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_SupportedEventNames_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_SupportedEventNames_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_bExposesAnimatableControls_MetaData[] = {
		{ "Comment", "/** If set to true, this control rig has animatable controls */" },
		{ "ModuleRelativePath", "Public/ControlRigBlueprint.h" },
		{ "ToolTip", "If set to true, this control rig has animatable controls" },
	};
#endif
	void Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_bExposesAnimatableControls_SetBit(void* Obj)
	{
		((UControlRigBlueprint*)Obj)->bExposesAnimatableControls = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_bExposesAnimatableControls = { "bExposesAnimatableControls", nullptr, (EPropertyFlags)0x0040010000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UControlRigBlueprint), &Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_bExposesAnimatableControls_SetBit, METADATA_PARAMS(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_bExposesAnimatableControls_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_bExposesAnimatableControls_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_bAutoRecompileVM_MetaData[] = {
		{ "ModuleRelativePath", "Public/ControlRigBlueprint.h" },
	};
#endif
	void Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_bAutoRecompileVM_SetBit(void* Obj)
	{
		((UControlRigBlueprint*)Obj)->bAutoRecompileVM = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_bAutoRecompileVM = { "bAutoRecompileVM", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UControlRigBlueprint), &Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_bAutoRecompileVM_SetBit, METADATA_PARAMS(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_bAutoRecompileVM_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_bAutoRecompileVM_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_bVMRecompilationRequired_MetaData[] = {
		{ "ModuleRelativePath", "Public/ControlRigBlueprint.h" },
	};
#endif
	void Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_bVMRecompilationRequired_SetBit(void* Obj)
	{
		((UControlRigBlueprint*)Obj)->bVMRecompilationRequired = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_bVMRecompilationRequired = { "bVMRecompilationRequired", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UControlRigBlueprint), &Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_bVMRecompilationRequired_SetBit, METADATA_PARAMS(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_bVMRecompilationRequired_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_bVMRecompilationRequired_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_VMRecompilationBracket_MetaData[] = {
		{ "ModuleRelativePath", "Public/ControlRigBlueprint.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_VMRecompilationBracket = { "VMRecompilationBracket", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigBlueprint, VMRecompilationBracket), METADATA_PARAMS(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_VMRecompilationBracket_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_VMRecompilationBracket_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_HierarchyModifier_MetaData[] = {
		{ "ModuleRelativePath", "Public/ControlRigBlueprint.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_HierarchyModifier = { "HierarchyModifier", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigBlueprint, HierarchyModifier), Z_Construct_UClass_UControlRigHierarchyModifier_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_HierarchyModifier_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_HierarchyModifier_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_Validator_MetaData[] = {
		{ "ModuleRelativePath", "Public/ControlRigBlueprint.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_Validator = { "Validator", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigBlueprint, Validator), Z_Construct_UClass_UControlRigValidator_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_Validator_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_Validator_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UControlRigBlueprint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_VMCompileSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_Model,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_Controller,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_PinToOperandMap_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_PinToOperandMap_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_PinToOperandMap,
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_GizmoLibrary,
#endif // WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_Statistics,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_DrawContainer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_Influences,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_HierarchyContainer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_Hierarchy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_CurveContainer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_bSupportsInversion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_bSupportsControls,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_PreviewSkeletalMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_SourceHierarchyImport,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_SourceCurveImport,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_SupportedEventNames_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_SupportedEventNames,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_bExposesAnimatableControls,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_bAutoRecompileVM,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_bVMRecompilationRequired,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_VMRecompilationBracket,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_HierarchyModifier,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigBlueprint_Statics::NewProp_Validator,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_UControlRigBlueprint_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInterface_PreviewMeshProvider_NoRegister, (int32)VTABLE_OFFSET(UControlRigBlueprint, IInterface_PreviewMeshProvider), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UControlRigBlueprint_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UControlRigBlueprint>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UControlRigBlueprint_Statics::ClassParams = {
		&UControlRigBlueprint::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UControlRigBlueprint_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigBlueprint_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UControlRigBlueprint_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigBlueprint_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UControlRigBlueprint()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UControlRigBlueprint_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UControlRigBlueprint, 1249507412);
	template<> CONTROLRIGDEVELOPER_API UClass* StaticClass<UControlRigBlueprint>()
	{
		return UControlRigBlueprint::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UControlRigBlueprint(Z_Construct_UClass_UControlRigBlueprint, &UControlRigBlueprint::StaticClass, TEXT("/Script/ControlRigDeveloper"), TEXT("UControlRigBlueprint"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UControlRigBlueprint);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
