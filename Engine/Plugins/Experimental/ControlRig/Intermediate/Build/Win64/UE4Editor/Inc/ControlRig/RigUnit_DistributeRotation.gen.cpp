// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Highlevel/Hierarchy/RigUnit_DistributeRotation.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_DistributeRotation() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_HighlevelBaseMutable();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigElementKeyCollection();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Rotation();
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_EControlRigAnimEasingType();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_DistributeRotation();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FCachedRigElement();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FQuat();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_DistributeRotationForCollection>() == std::is_polymorphic<FRigUnit_HighlevelBaseMutable>(), "USTRUCT FRigUnit_DistributeRotationForCollection cannot be polymorphic unless super FRigUnit_HighlevelBaseMutable is polymorphic");

class UScriptStruct* FRigUnit_DistributeRotationForCollection::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_DistributeRotationForCollection"), sizeof(FRigUnit_DistributeRotationForCollection), Get_Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_DistributeRotationForCollection::Execute"), &FRigUnit_DistributeRotationForCollection::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_DistributeRotationForCollection>()
{
	return FRigUnit_DistributeRotationForCollection::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_DistributeRotationForCollection(FRigUnit_DistributeRotationForCollection::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_DistributeRotationForCollection"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DistributeRotationForCollection
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DistributeRotationForCollection()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_DistributeRotationForCollection>(FName(TEXT("RigUnit_DistributeRotationForCollection")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DistributeRotationForCollection;
	struct Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Items_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Items;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Rotations_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Rotations_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Rotations;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_RotationEaseType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RotationEaseType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_RotationEaseType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Weight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Weight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorkData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WorkData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "/**\n * Distributes rotations provided across a collection of items.\n * Each rotation is expressed by a quaternion and a ratio, where the ratio is between 0.0 and 1.0\n * Note: This node adds rotation in local space of each item!\n */" },
		{ "DisplayName", "Distribute Rotation" },
		{ "Keywords", "TwistBones" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_DistributeRotation.h" },
		{ "ToolTip", "Distributes rotations provided across a collection of items.\nEach rotation is expressed by a quaternion and a ratio, where the ratio is between 0.0 and 1.0\nNote: This node adds rotation in local space of each item!" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_DistributeRotationForCollection>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Statics::NewProp_Items_MetaData[] = {
		{ "Comment", "/** \n\x09 * The items to use to distribute the rotation\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_DistributeRotation.h" },
		{ "ToolTip", "The items to use to distribute the rotation" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Statics::NewProp_Items = { "Items", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DistributeRotationForCollection, Items), Z_Construct_UScriptStruct_FRigElementKeyCollection, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Statics::NewProp_Items_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Statics::NewProp_Items_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Statics::NewProp_Rotations_Inner = { "Rotations", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Rotation, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Statics::NewProp_Rotations_MetaData[] = {
		{ "Comment", "/** \n\x09 * The list of rotations to be applied\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_DistributeRotation.h" },
		{ "ToolTip", "The list of rotations to be applied" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Statics::NewProp_Rotations = { "Rotations", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DistributeRotationForCollection, Rotations), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Statics::NewProp_Rotations_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Statics::NewProp_Rotations_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Statics::NewProp_RotationEaseType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Statics::NewProp_RotationEaseType_MetaData[] = {
		{ "Comment", "/**\n\x09 * The easing to use between to rotations.\n\x09 */" },
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_DistributeRotation.h" },
		{ "ToolTip", "The easing to use between to rotations." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Statics::NewProp_RotationEaseType = { "RotationEaseType", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DistributeRotationForCollection, RotationEaseType), Z_Construct_UEnum_ControlRig_EControlRigAnimEasingType, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Statics::NewProp_RotationEaseType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Statics::NewProp_RotationEaseType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Statics::NewProp_Weight_MetaData[] = {
		{ "Comment", "/**\n\x09 * The weight of the solver - how much the rotation should be applied\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_DistributeRotation.h" },
		{ "ToolTip", "The weight of the solver - how much the rotation should be applied" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Statics::NewProp_Weight = { "Weight", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DistributeRotationForCollection, Weight), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Statics::NewProp_Weight_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Statics::NewProp_Weight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Statics::NewProp_WorkData_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_DistributeRotation.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Statics::NewProp_WorkData = { "WorkData", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DistributeRotationForCollection, WorkData), Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Statics::NewProp_WorkData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Statics::NewProp_WorkData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Statics::NewProp_Items,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Statics::NewProp_Rotations_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Statics::NewProp_Rotations,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Statics::NewProp_RotationEaseType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Statics::NewProp_RotationEaseType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Statics::NewProp_Weight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Statics::NewProp_WorkData,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_HighlevelBaseMutable,
		&NewStructOps,
		"RigUnit_DistributeRotationForCollection",
		sizeof(FRigUnit_DistributeRotationForCollection),
		alignof(FRigUnit_DistributeRotationForCollection),
		Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_DistributeRotationForCollection"), sizeof(FRigUnit_DistributeRotationForCollection), Get_Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DistributeRotationForCollection_Hash() { return 2614892581U; }

void FRigUnit_DistributeRotationForCollection::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
	FRigVMFixedArray<FRigUnit_DistributeRotation_Rotation> Rotations_1_Array(Rotations);
	
    StaticExecute(
		RigVMExecuteContext,
		Items,
		Rotations_1_Array,
		RotationEaseType,
		Weight,
		WorkData,
		ExecuteContext,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_DistributeRotation>() == std::is_polymorphic<FRigUnit_HighlevelBaseMutable>(), "USTRUCT FRigUnit_DistributeRotation cannot be polymorphic unless super FRigUnit_HighlevelBaseMutable is polymorphic");

class UScriptStruct* FRigUnit_DistributeRotation::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_DistributeRotation, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_DistributeRotation"), sizeof(FRigUnit_DistributeRotation), Get_Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_DistributeRotation::Execute"), &FRigUnit_DistributeRotation::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_DistributeRotation>()
{
	return FRigUnit_DistributeRotation::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_DistributeRotation(FRigUnit_DistributeRotation::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_DistributeRotation"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DistributeRotation
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DistributeRotation()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_DistributeRotation>(FName(TEXT("RigUnit_DistributeRotation")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DistributeRotation;
	struct Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StartBone_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_StartBone;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EndBone_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_EndBone;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Rotations_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Rotations_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Rotations;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_RotationEaseType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RotationEaseType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_RotationEaseType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Weight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Weight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPropagateToChildren_MetaData[];
#endif
		static void NewProp_bPropagateToChildren_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPropagateToChildren;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorkData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WorkData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "/**\n * Distributes rotations provided along a chain.\n * Each rotation is expressed by a quaternion and a ratio, where the ratio is between 0.0 and 1.0\n * Note: This node adds rotation in local space of each bone!\n */" },
		{ "Deprecated", "4.25" },
		{ "DisplayName", "Distribute Rotation" },
		{ "Keywords", "TwistBones" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_DistributeRotation.h" },
		{ "ToolTip", "Distributes rotations provided along a chain.\nEach rotation is expressed by a quaternion and a ratio, where the ratio is between 0.0 and 1.0\nNote: This node adds rotation in local space of each bone!" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_DistributeRotation>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewProp_StartBone_MetaData[] = {
		{ "Comment", "/** \n\x09 * The name of the first bone to align\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_DistributeRotation.h" },
		{ "ToolTip", "The name of the first bone to align" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewProp_StartBone = { "StartBone", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DistributeRotation, StartBone), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewProp_StartBone_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewProp_StartBone_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewProp_EndBone_MetaData[] = {
		{ "Comment", "/** \n\x09 * The name of the last bone to align\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_DistributeRotation.h" },
		{ "ToolTip", "The name of the last bone to align" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewProp_EndBone = { "EndBone", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DistributeRotation, EndBone), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewProp_EndBone_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewProp_EndBone_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewProp_Rotations_Inner = { "Rotations", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Rotation, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewProp_Rotations_MetaData[] = {
		{ "Comment", "/** \n\x09 * The list of rotations to be applied\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_DistributeRotation.h" },
		{ "ToolTip", "The list of rotations to be applied" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewProp_Rotations = { "Rotations", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DistributeRotation, Rotations), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewProp_Rotations_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewProp_Rotations_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewProp_RotationEaseType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewProp_RotationEaseType_MetaData[] = {
		{ "Comment", "/**\n\x09 * The easing to use between to rotations.\n\x09 */" },
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_DistributeRotation.h" },
		{ "ToolTip", "The easing to use between to rotations." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewProp_RotationEaseType = { "RotationEaseType", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DistributeRotation, RotationEaseType), Z_Construct_UEnum_ControlRig_EControlRigAnimEasingType, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewProp_RotationEaseType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewProp_RotationEaseType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewProp_Weight_MetaData[] = {
		{ "Comment", "/**\n\x09 * The weight of the solver - how much the rotation should be applied\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_DistributeRotation.h" },
		{ "ToolTip", "The weight of the solver - how much the rotation should be applied" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewProp_Weight = { "Weight", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DistributeRotation, Weight), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewProp_Weight_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewProp_Weight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewProp_bPropagateToChildren_MetaData[] = {
		{ "Comment", "/**\n\x09 * If set to true all of the global transforms of the children\n\x09 * of this bone will be recalculated based on their local transforms.\n\x09 * Note: This is computationally more expensive than turning it off.\n\x09 */" },
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_DistributeRotation.h" },
		{ "ToolTip", "If set to true all of the global transforms of the children\nof this bone will be recalculated based on their local transforms.\nNote: This is computationally more expensive than turning it off." },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewProp_bPropagateToChildren_SetBit(void* Obj)
	{
		((FRigUnit_DistributeRotation*)Obj)->bPropagateToChildren = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewProp_bPropagateToChildren = { "bPropagateToChildren", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_DistributeRotation), &Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewProp_bPropagateToChildren_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewProp_bPropagateToChildren_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewProp_bPropagateToChildren_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewProp_WorkData_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_DistributeRotation.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewProp_WorkData = { "WorkData", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DistributeRotation, WorkData), Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewProp_WorkData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewProp_WorkData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewProp_StartBone,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewProp_EndBone,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewProp_Rotations_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewProp_Rotations,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewProp_RotationEaseType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewProp_RotationEaseType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewProp_Weight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewProp_bPropagateToChildren,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::NewProp_WorkData,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_HighlevelBaseMutable,
		&NewStructOps,
		"RigUnit_DistributeRotation",
		sizeof(FRigUnit_DistributeRotation),
		alignof(FRigUnit_DistributeRotation),
		Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_DistributeRotation()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_DistributeRotation"), sizeof(FRigUnit_DistributeRotation), Get_Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Hash() { return 3208206119U; }

void FRigUnit_DistributeRotation::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
	FRigVMFixedArray<FRigUnit_DistributeRotation_Rotation> Rotations_2_Array(Rotations);
	
    StaticExecute(
		RigVMExecuteContext,
		StartBone,
		EndBone,
		Rotations_2_Array,
		RotationEaseType,
		Weight,
		bPropagateToChildren,
		WorkData,
		ExecuteContext,
		Context
	);
}

class UScriptStruct* FRigUnit_DistributeRotation_WorkData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_DistributeRotation_WorkData"), sizeof(FRigUnit_DistributeRotation_WorkData), Get_Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_DistributeRotation_WorkData>()
{
	return FRigUnit_DistributeRotation_WorkData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_DistributeRotation_WorkData(FRigUnit_DistributeRotation_WorkData::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_DistributeRotation_WorkData"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DistributeRotation_WorkData
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DistributeRotation_WorkData()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_DistributeRotation_WorkData>(FName(TEXT("RigUnit_DistributeRotation_WorkData")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DistributeRotation_WorkData;
	struct Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CachedItems_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedItems_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_CachedItems;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ItemRotationA_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ItemRotationA_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ItemRotationA;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ItemRotationB_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ItemRotationB_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ItemRotationB;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ItemRotationT_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ItemRotationT_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ItemRotationT;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ItemLocalTransforms_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ItemLocalTransforms_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ItemLocalTransforms;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_DistributeRotation.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_DistributeRotation_WorkData>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::NewProp_CachedItems_Inner = { "CachedItems", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FCachedRigElement, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::NewProp_CachedItems_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_DistributeRotation.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::NewProp_CachedItems = { "CachedItems", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DistributeRotation_WorkData, CachedItems), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::NewProp_CachedItems_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::NewProp_CachedItems_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::NewProp_ItemRotationA_Inner = { "ItemRotationA", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::NewProp_ItemRotationA_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_DistributeRotation.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::NewProp_ItemRotationA = { "ItemRotationA", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DistributeRotation_WorkData, ItemRotationA), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::NewProp_ItemRotationA_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::NewProp_ItemRotationA_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::NewProp_ItemRotationB_Inner = { "ItemRotationB", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::NewProp_ItemRotationB_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_DistributeRotation.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::NewProp_ItemRotationB = { "ItemRotationB", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DistributeRotation_WorkData, ItemRotationB), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::NewProp_ItemRotationB_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::NewProp_ItemRotationB_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::NewProp_ItemRotationT_Inner = { "ItemRotationT", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::NewProp_ItemRotationT_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_DistributeRotation.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::NewProp_ItemRotationT = { "ItemRotationT", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DistributeRotation_WorkData, ItemRotationT), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::NewProp_ItemRotationT_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::NewProp_ItemRotationT_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::NewProp_ItemLocalTransforms_Inner = { "ItemLocalTransforms", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::NewProp_ItemLocalTransforms_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_DistributeRotation.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::NewProp_ItemLocalTransforms = { "ItemLocalTransforms", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DistributeRotation_WorkData, ItemLocalTransforms), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::NewProp_ItemLocalTransforms_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::NewProp_ItemLocalTransforms_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::NewProp_CachedItems_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::NewProp_CachedItems,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::NewProp_ItemRotationA_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::NewProp_ItemRotationA,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::NewProp_ItemRotationB_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::NewProp_ItemRotationB,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::NewProp_ItemRotationT_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::NewProp_ItemRotationT,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::NewProp_ItemLocalTransforms_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::NewProp_ItemLocalTransforms,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"RigUnit_DistributeRotation_WorkData",
		sizeof(FRigUnit_DistributeRotation_WorkData),
		alignof(FRigUnit_DistributeRotation_WorkData),
		Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_DistributeRotation_WorkData"), sizeof(FRigUnit_DistributeRotation_WorkData), Get_Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_WorkData_Hash() { return 486005402U; }
class UScriptStruct* FRigUnit_DistributeRotation_Rotation::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Rotation_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Rotation, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_DistributeRotation_Rotation"), sizeof(FRigUnit_DistributeRotation_Rotation), Get_Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Rotation_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_DistributeRotation_Rotation>()
{
	return FRigUnit_DistributeRotation_Rotation::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_DistributeRotation_Rotation(FRigUnit_DistributeRotation_Rotation::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_DistributeRotation_Rotation"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DistributeRotation_Rotation
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DistributeRotation_Rotation()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_DistributeRotation_Rotation>(FName(TEXT("RigUnit_DistributeRotation_Rotation")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_DistributeRotation_Rotation;
	struct Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Rotation_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Rotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Rotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Ratio_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Ratio;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Rotation_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_DistributeRotation.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Rotation_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_DistributeRotation_Rotation>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Rotation_Statics::NewProp_Rotation_MetaData[] = {
		{ "Comment", "/**\n\x09 * The rotation to be applied\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_DistributeRotation.h" },
		{ "ToolTip", "The rotation to be applied" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Rotation_Statics::NewProp_Rotation = { "Rotation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DistributeRotation_Rotation, Rotation), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Rotation_Statics::NewProp_Rotation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Rotation_Statics::NewProp_Rotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Rotation_Statics::NewProp_Ratio_MetaData[] = {
		{ "Comment", "/**\n\x09 * The ratio of where this rotation sits along the chain\n\x09 */" },
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_DistributeRotation.h" },
		{ "ToolTip", "The ratio of where this rotation sits along the chain" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Rotation_Statics::NewProp_Ratio = { "Ratio", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_DistributeRotation_Rotation, Ratio), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Rotation_Statics::NewProp_Ratio_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Rotation_Statics::NewProp_Ratio_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Rotation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Rotation_Statics::NewProp_Rotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Rotation_Statics::NewProp_Ratio,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Rotation_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"RigUnit_DistributeRotation_Rotation",
		sizeof(FRigUnit_DistributeRotation_Rotation),
		alignof(FRigUnit_DistributeRotation_Rotation),
		Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Rotation_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Rotation_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Rotation_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Rotation_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Rotation()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Rotation_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_DistributeRotation_Rotation"), sizeof(FRigUnit_DistributeRotation_Rotation), Get_Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Rotation_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Rotation_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_DistributeRotation_Rotation_Hash() { return 3692605521U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
