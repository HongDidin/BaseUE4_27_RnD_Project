// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Simulation/RigUnit_PointSimulation.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_PointSimulation() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_PointSimulation();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_SimBaseMutable();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FCRSimPoint();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FCRSimLinearSpring();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FCRSimPointForce();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FCRSimSoftCollision();
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_ECRSimPointIntegrateType();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_PointSimulation_BoneTarget();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FCRFourPointBezier();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_PointSimulation_WorkData();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FCRSimPointContainer();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FCachedRigElement();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_PointSimulation>() == std::is_polymorphic<FRigUnit_SimBaseMutable>(), "USTRUCT FRigUnit_PointSimulation cannot be polymorphic unless super FRigUnit_SimBaseMutable is polymorphic");

class UScriptStruct* FRigUnit_PointSimulation::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_PointSimulation, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_PointSimulation"), sizeof(FRigUnit_PointSimulation), Get_Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_PointSimulation::Execute"), &FRigUnit_PointSimulation::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_PointSimulation>()
{
	return FRigUnit_PointSimulation::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_PointSimulation(FRigUnit_PointSimulation::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_PointSimulation"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_PointSimulation
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_PointSimulation()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_PointSimulation>(FName(TEXT("RigUnit_PointSimulation")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_PointSimulation;
	struct Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Points_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Points_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Points;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Links_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Links_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Links;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Forces_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Forces_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Forces;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CollisionVolumes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CollisionVolumes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_CollisionVolumes;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SimulatedStepsPerSecond_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SimulatedStepsPerSecond;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_IntegratorType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IntegratorType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_IntegratorType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VerletBlend_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_VerletBlend;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BoneTargets_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoneTargets_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_BoneTargets;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bLimitLocalPosition_MetaData[];
#endif
		static void NewProp_bLimitLocalPosition_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bLimitLocalPosition;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPropagateToChildren_MetaData[];
#endif
		static void NewProp_bPropagateToChildren_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPropagateToChildren;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PrimaryAimAxis_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PrimaryAimAxis;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SecondaryAimAxis_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SecondaryAimAxis;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DebugSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DebugSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Bezier_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Bezier;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorkData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WorkData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Performs point based simulation\n * Note: Disabled for now.\n */" },
		{ "Deprecated", "4.25" },
		{ "DisplayName", "Point Simulation" },
		{ "Keywords", "Simulate,Verlet,Springs" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_PointSimulation.h" },
		{ "ToolTip", "Performs point based simulation\nNote: Disabled for now." },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_PointSimulation>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_Points_Inner = { "Points", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FCRSimPoint, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_Points_MetaData[] = {
		{ "Comment", "/** The points to simulate */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_PointSimulation.h" },
		{ "ToolTip", "The points to simulate" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_Points = { "Points", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_PointSimulation, Points), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_Points_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_Points_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_Links_Inner = { "Links", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FCRSimLinearSpring, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_Links_MetaData[] = {
		{ "Comment", "/** The links to connect the points with */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_PointSimulation.h" },
		{ "ToolTip", "The links to connect the points with" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_Links = { "Links", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_PointSimulation, Links), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_Links_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_Links_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_Forces_Inner = { "Forces", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FCRSimPointForce, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_Forces_MetaData[] = {
		{ "Comment", "/** The forces to apply */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_PointSimulation.h" },
		{ "ToolTip", "The forces to apply" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_Forces = { "Forces", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_PointSimulation, Forces), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_Forces_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_Forces_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_CollisionVolumes_Inner = { "CollisionVolumes", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FCRSimSoftCollision, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_CollisionVolumes_MetaData[] = {
		{ "Comment", "/** The collision volumes to define */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_PointSimulation.h" },
		{ "ToolTip", "The collision volumes to define" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_CollisionVolumes = { "CollisionVolumes", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_PointSimulation, CollisionVolumes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_CollisionVolumes_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_CollisionVolumes_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_SimulatedStepsPerSecond_MetaData[] = {
		{ "Comment", "/** The frame rate of the simulation */" },
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_PointSimulation.h" },
		{ "ToolTip", "The frame rate of the simulation" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_SimulatedStepsPerSecond = { "SimulatedStepsPerSecond", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_PointSimulation, SimulatedStepsPerSecond), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_SimulatedStepsPerSecond_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_SimulatedStepsPerSecond_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_IntegratorType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_IntegratorType_MetaData[] = {
		{ "Comment", "/** The type of integrator to use */" },
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_PointSimulation.h" },
		{ "ToolTip", "The type of integrator to use" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_IntegratorType = { "IntegratorType", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_PointSimulation, IntegratorType), Z_Construct_UEnum_ControlRig_ECRSimPointIntegrateType, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_IntegratorType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_IntegratorType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_VerletBlend_MetaData[] = {
		{ "Comment", "/** The amount of blending to apply per second ( only for verlet integrations )*/" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_PointSimulation.h" },
		{ "ToolTip", "The amount of blending to apply per second ( only for verlet integrations )" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_VerletBlend = { "VerletBlend", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_PointSimulation, VerletBlend), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_VerletBlend_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_VerletBlend_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_BoneTargets_Inner = { "BoneTargets", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRigUnit_PointSimulation_BoneTarget, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_BoneTargets_MetaData[] = {
		{ "Comment", "/** The bones to map to the simulated points. */" },
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_PointSimulation.h" },
		{ "ToolTip", "The bones to map to the simulated points." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_BoneTargets = { "BoneTargets", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_PointSimulation, BoneTargets), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_BoneTargets_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_BoneTargets_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_bLimitLocalPosition_MetaData[] = {
		{ "Comment", "/**\n\x09 * If set to true bones are placed within the original distance of\n\x09 * the previous local transform. This can be used to avoid stretch.\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_PointSimulation.h" },
		{ "ToolTip", "If set to true bones are placed within the original distance of\nthe previous local transform. This can be used to avoid stretch." },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_bLimitLocalPosition_SetBit(void* Obj)
	{
		((FRigUnit_PointSimulation*)Obj)->bLimitLocalPosition = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_bLimitLocalPosition = { "bLimitLocalPosition", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_PointSimulation), &Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_bLimitLocalPosition_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_bLimitLocalPosition_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_bLimitLocalPosition_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_bPropagateToChildren_MetaData[] = {
		{ "Comment", "/**\n\x09 * If set to true all of the global transforms of the children\n\x09 * of this bone will be recalculated based on their local transforms.\n\x09 * Note: This is computationally more expensive than turning it off.\n\x09 */" },
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_PointSimulation.h" },
		{ "ToolTip", "If set to true all of the global transforms of the children\nof this bone will be recalculated based on their local transforms.\nNote: This is computationally more expensive than turning it off." },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_bPropagateToChildren_SetBit(void* Obj)
	{
		((FRigUnit_PointSimulation*)Obj)->bPropagateToChildren = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_bPropagateToChildren = { "bPropagateToChildren", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_PointSimulation), &Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_bPropagateToChildren_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_bPropagateToChildren_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_bPropagateToChildren_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_PrimaryAimAxis_MetaData[] = {
		{ "Comment", "/** The primary axis to use for the aim */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_PointSimulation.h" },
		{ "ToolTip", "The primary axis to use for the aim" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_PrimaryAimAxis = { "PrimaryAimAxis", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_PointSimulation, PrimaryAimAxis), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_PrimaryAimAxis_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_PrimaryAimAxis_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_SecondaryAimAxis_MetaData[] = {
		{ "Comment", "/** The secondary axis to use for the aim */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_PointSimulation.h" },
		{ "ToolTip", "The secondary axis to use for the aim" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_SecondaryAimAxis = { "SecondaryAimAxis", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_PointSimulation, SecondaryAimAxis), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_SecondaryAimAxis_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_SecondaryAimAxis_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_DebugSettings_MetaData[] = {
		{ "Comment", "/** Debug draw settings for this simulation */" },
		{ "DetailsOnly", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_PointSimulation.h" },
		{ "ToolTip", "Debug draw settings for this simulation" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_DebugSettings = { "DebugSettings", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_PointSimulation, DebugSettings), Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_DebugSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_DebugSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_Bezier_MetaData[] = {
		{ "Comment", "/** If the simulation has at least four points they will be stored in here. */" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_PointSimulation.h" },
		{ "Output", "" },
		{ "ToolTip", "If the simulation has at least four points they will be stored in here." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_Bezier = { "Bezier", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_PointSimulation, Bezier), Z_Construct_UScriptStruct_FCRFourPointBezier, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_Bezier_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_Bezier_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_WorkData_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_PointSimulation.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_WorkData = { "WorkData", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_PointSimulation, WorkData), Z_Construct_UScriptStruct_FRigUnit_PointSimulation_WorkData, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_WorkData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_WorkData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_Points_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_Points,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_Links_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_Links,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_Forces_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_Forces,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_CollisionVolumes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_CollisionVolumes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_SimulatedStepsPerSecond,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_IntegratorType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_IntegratorType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_VerletBlend,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_BoneTargets_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_BoneTargets,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_bLimitLocalPosition,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_bPropagateToChildren,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_PrimaryAimAxis,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_SecondaryAimAxis,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_DebugSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_Bezier,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::NewProp_WorkData,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_SimBaseMutable,
		&NewStructOps,
		"RigUnit_PointSimulation",
		sizeof(FRigUnit_PointSimulation),
		alignof(FRigUnit_PointSimulation),
		Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_PointSimulation()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_PointSimulation"), sizeof(FRigUnit_PointSimulation), Get_Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_PointSimulation_Hash() { return 1089568235U; }

void FRigUnit_PointSimulation::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
	FRigVMFixedArray<FCRSimPoint> Points_0_Array(Points);
	FRigVMFixedArray<FCRSimLinearSpring> Links_1_Array(Links);
	FRigVMFixedArray<FCRSimPointForce> Forces_2_Array(Forces);
	FRigVMFixedArray<FCRSimSoftCollision> CollisionVolumes_3_Array(CollisionVolumes);
	FRigVMFixedArray<FRigUnit_PointSimulation_BoneTarget> BoneTargets_7_Array(BoneTargets);
	
    StaticExecute(
		RigVMExecuteContext,
		Points_0_Array,
		Links_1_Array,
		Forces_2_Array,
		CollisionVolumes_3_Array,
		SimulatedStepsPerSecond,
		IntegratorType,
		VerletBlend,
		BoneTargets_7_Array,
		bLimitLocalPosition,
		bPropagateToChildren,
		PrimaryAimAxis,
		SecondaryAimAxis,
		DebugSettings,
		Bezier,
		WorkData,
		ExecuteContext,
		Context
	);
}

class UScriptStruct* FRigUnit_PointSimulation_WorkData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_PointSimulation_WorkData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_WorkData, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_PointSimulation_WorkData"), sizeof(FRigUnit_PointSimulation_WorkData), Get_Z_Construct_UScriptStruct_FRigUnit_PointSimulation_WorkData_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_PointSimulation_WorkData>()
{
	return FRigUnit_PointSimulation_WorkData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_PointSimulation_WorkData(FRigUnit_PointSimulation_WorkData::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_PointSimulation_WorkData"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_PointSimulation_WorkData
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_PointSimulation_WorkData()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_PointSimulation_WorkData>(FName(TEXT("RigUnit_PointSimulation_WorkData")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_PointSimulation_WorkData;
	struct Z_Construct_UScriptStruct_FRigUnit_PointSimulation_WorkData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Simulation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Simulation;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BoneIndices_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoneIndices_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_BoneIndices;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_PointSimulation_WorkData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_PointSimulation.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_PointSimulation_WorkData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_PointSimulation_WorkData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_PointSimulation_WorkData_Statics::NewProp_Simulation_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_PointSimulation.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_PointSimulation_WorkData_Statics::NewProp_Simulation = { "Simulation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_PointSimulation_WorkData, Simulation), Z_Construct_UScriptStruct_FCRSimPointContainer, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_WorkData_Statics::NewProp_Simulation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_WorkData_Statics::NewProp_Simulation_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_PointSimulation_WorkData_Statics::NewProp_BoneIndices_Inner = { "BoneIndices", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FCachedRigElement, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_PointSimulation_WorkData_Statics::NewProp_BoneIndices_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_PointSimulation.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_PointSimulation_WorkData_Statics::NewProp_BoneIndices = { "BoneIndices", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_PointSimulation_WorkData, BoneIndices), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_WorkData_Statics::NewProp_BoneIndices_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_WorkData_Statics::NewProp_BoneIndices_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_PointSimulation_WorkData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_PointSimulation_WorkData_Statics::NewProp_Simulation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_PointSimulation_WorkData_Statics::NewProp_BoneIndices_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_PointSimulation_WorkData_Statics::NewProp_BoneIndices,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_PointSimulation_WorkData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"RigUnit_PointSimulation_WorkData",
		sizeof(FRigUnit_PointSimulation_WorkData),
		alignof(FRigUnit_PointSimulation_WorkData),
		Z_Construct_UScriptStruct_FRigUnit_PointSimulation_WorkData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_WorkData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_WorkData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_WorkData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_PointSimulation_WorkData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_PointSimulation_WorkData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_PointSimulation_WorkData"), sizeof(FRigUnit_PointSimulation_WorkData), Get_Z_Construct_UScriptStruct_FRigUnit_PointSimulation_WorkData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_PointSimulation_WorkData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_PointSimulation_WorkData_Hash() { return 1223574646U; }
class UScriptStruct* FRigUnit_PointSimulation_BoneTarget::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_PointSimulation_BoneTarget_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_BoneTarget, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_PointSimulation_BoneTarget"), sizeof(FRigUnit_PointSimulation_BoneTarget), Get_Z_Construct_UScriptStruct_FRigUnit_PointSimulation_BoneTarget_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_PointSimulation_BoneTarget>()
{
	return FRigUnit_PointSimulation_BoneTarget::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_PointSimulation_BoneTarget(FRigUnit_PointSimulation_BoneTarget::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_PointSimulation_BoneTarget"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_PointSimulation_BoneTarget
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_PointSimulation_BoneTarget()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_PointSimulation_BoneTarget>(FName(TEXT("RigUnit_PointSimulation_BoneTarget")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_PointSimulation_BoneTarget;
	struct Z_Construct_UScriptStruct_FRigUnit_PointSimulation_BoneTarget_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Bone_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Bone;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TranslationPoint_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_TranslationPoint;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PrimaryAimPoint_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_PrimaryAimPoint;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SecondaryAimPoint_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SecondaryAimPoint;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_PointSimulation_BoneTarget_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_PointSimulation.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_PointSimulation_BoneTarget_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_PointSimulation_BoneTarget>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_PointSimulation_BoneTarget_Statics::NewProp_Bone_MetaData[] = {
		{ "Category", "BoneTarget" },
		{ "Comment", "/**\n\x09 * The name of the bone to map\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_PointSimulation.h" },
		{ "ToolTip", "The name of the bone to map" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_PointSimulation_BoneTarget_Statics::NewProp_Bone = { "Bone", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_PointSimulation_BoneTarget, Bone), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_BoneTarget_Statics::NewProp_Bone_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_BoneTarget_Statics::NewProp_Bone_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_PointSimulation_BoneTarget_Statics::NewProp_TranslationPoint_MetaData[] = {
		{ "Category", "BoneTarget" },
		{ "Comment", "/**\n\x09 * The index of the point to use for translation\n\x09 */" },
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_PointSimulation.h" },
		{ "ToolTip", "The index of the point to use for translation" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_PointSimulation_BoneTarget_Statics::NewProp_TranslationPoint = { "TranslationPoint", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_PointSimulation_BoneTarget, TranslationPoint), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_BoneTarget_Statics::NewProp_TranslationPoint_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_BoneTarget_Statics::NewProp_TranslationPoint_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_PointSimulation_BoneTarget_Statics::NewProp_PrimaryAimPoint_MetaData[] = {
		{ "Category", "BoneTarget" },
		{ "Comment", "/**\n\x09 * The index of the point to use for aiming the primary axis.\n\x09 * Use -1 to indicate that you don't want to aim the bone.\n\x09 */" },
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_PointSimulation.h" },
		{ "ToolTip", "The index of the point to use for aiming the primary axis.\nUse -1 to indicate that you don't want to aim the bone." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_PointSimulation_BoneTarget_Statics::NewProp_PrimaryAimPoint = { "PrimaryAimPoint", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_PointSimulation_BoneTarget, PrimaryAimPoint), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_BoneTarget_Statics::NewProp_PrimaryAimPoint_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_BoneTarget_Statics::NewProp_PrimaryAimPoint_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_PointSimulation_BoneTarget_Statics::NewProp_SecondaryAimPoint_MetaData[] = {
		{ "Category", "BoneTarget" },
		{ "Comment", "/**\n\x09 * The index of the point to use for aiming the secondary axis.\n\x09 * Use -1 to indicate that you don't want to aim the bone.\n\x09 */" },
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_PointSimulation.h" },
		{ "ToolTip", "The index of the point to use for aiming the secondary axis.\nUse -1 to indicate that you don't want to aim the bone." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_PointSimulation_BoneTarget_Statics::NewProp_SecondaryAimPoint = { "SecondaryAimPoint", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_PointSimulation_BoneTarget, SecondaryAimPoint), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_BoneTarget_Statics::NewProp_SecondaryAimPoint_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_BoneTarget_Statics::NewProp_SecondaryAimPoint_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_PointSimulation_BoneTarget_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_PointSimulation_BoneTarget_Statics::NewProp_Bone,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_PointSimulation_BoneTarget_Statics::NewProp_TranslationPoint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_PointSimulation_BoneTarget_Statics::NewProp_PrimaryAimPoint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_PointSimulation_BoneTarget_Statics::NewProp_SecondaryAimPoint,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_PointSimulation_BoneTarget_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"RigUnit_PointSimulation_BoneTarget",
		sizeof(FRigUnit_PointSimulation_BoneTarget),
		alignof(FRigUnit_PointSimulation_BoneTarget),
		Z_Construct_UScriptStruct_FRigUnit_PointSimulation_BoneTarget_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_BoneTarget_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_BoneTarget_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_BoneTarget_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_PointSimulation_BoneTarget()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_PointSimulation_BoneTarget_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_PointSimulation_BoneTarget"), sizeof(FRigUnit_PointSimulation_BoneTarget), Get_Z_Construct_UScriptStruct_FRigUnit_PointSimulation_BoneTarget_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_PointSimulation_BoneTarget_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_PointSimulation_BoneTarget_Hash() { return 2862709204U; }
class UScriptStruct* FRigUnit_PointSimulation_DebugSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_PointSimulation_DebugSettings"), sizeof(FRigUnit_PointSimulation_DebugSettings), Get_Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_PointSimulation_DebugSettings>()
{
	return FRigUnit_PointSimulation_DebugSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings(FRigUnit_PointSimulation_DebugSettings::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_PointSimulation_DebugSettings"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_PointSimulation_DebugSettings
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_PointSimulation_DebugSettings()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_PointSimulation_DebugSettings>(FName(TEXT("RigUnit_PointSimulation_DebugSettings")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_PointSimulation_DebugSettings;
	struct Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnabled_MetaData[];
#endif
		static void NewProp_bEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Scale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Scale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CollisionScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CollisionScale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDrawPointsAsSpheres_MetaData[];
#endif
		static void NewProp_bDrawPointsAsSpheres_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDrawPointsAsSpheres;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Color_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Color;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorldOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WorldOffset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_PointSimulation.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_PointSimulation_DebugSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::NewProp_bEnabled_MetaData[] = {
		{ "Category", "DebugSettings" },
		{ "Comment", "/**\n\x09 * If enabled debug information will be drawn \n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_PointSimulation.h" },
		{ "ToolTip", "If enabled debug information will be drawn" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::NewProp_bEnabled_SetBit(void* Obj)
	{
		((FRigUnit_PointSimulation_DebugSettings*)Obj)->bEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::NewProp_bEnabled = { "bEnabled", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_PointSimulation_DebugSettings), &Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::NewProp_bEnabled_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::NewProp_bEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::NewProp_bEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::NewProp_Scale_MetaData[] = {
		{ "Category", "DebugSettings" },
		{ "Comment", "/**\n\x09 * The size of the debug drawing information\n\x09 */" },
		{ "EditCondition", "bEnabled" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_PointSimulation.h" },
		{ "ToolTip", "The size of the debug drawing information" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::NewProp_Scale = { "Scale", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_PointSimulation_DebugSettings, Scale), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::NewProp_Scale_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::NewProp_Scale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::NewProp_CollisionScale_MetaData[] = {
		{ "Category", "DebugSettings" },
		{ "Comment", "/**\n     * The size of the debug drawing information\n     */" },
		{ "EditCondition", "bEnabled" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_PointSimulation.h" },
		{ "ToolTip", "The size of the debug drawing information" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::NewProp_CollisionScale = { "CollisionScale", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_PointSimulation_DebugSettings, CollisionScale), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::NewProp_CollisionScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::NewProp_CollisionScale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::NewProp_bDrawPointsAsSpheres_MetaData[] = {
		{ "Category", "DebugSettings" },
		{ "Comment", "/**\n\x09 * If set to true points will be drawn as spheres with their sizes reflected\n\x09 */" },
		{ "EditCondition", "bEnabled" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_PointSimulation.h" },
		{ "ToolTip", "If set to true points will be drawn as spheres with their sizes reflected" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::NewProp_bDrawPointsAsSpheres_SetBit(void* Obj)
	{
		((FRigUnit_PointSimulation_DebugSettings*)Obj)->bDrawPointsAsSpheres = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::NewProp_bDrawPointsAsSpheres = { "bDrawPointsAsSpheres", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_PointSimulation_DebugSettings), &Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::NewProp_bDrawPointsAsSpheres_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::NewProp_bDrawPointsAsSpheres_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::NewProp_bDrawPointsAsSpheres_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::NewProp_Color_MetaData[] = {
		{ "Category", "DebugSettings" },
		{ "Comment", "/**\n\x09 * The color to use for debug drawing\n\x09 */" },
		{ "EditCondition", "bEnabled" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_PointSimulation.h" },
		{ "ToolTip", "The color to use for debug drawing" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::NewProp_Color = { "Color", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_PointSimulation_DebugSettings, Color), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::NewProp_Color_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::NewProp_Color_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::NewProp_WorldOffset_MetaData[] = {
		{ "Category", "DebugSettings" },
		{ "Comment", "/**\n\x09 * The offset at which to draw the debug information in the world\n\x09 */" },
		{ "EditCondition", "bEnabled" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Simulation/RigUnit_PointSimulation.h" },
		{ "ToolTip", "The offset at which to draw the debug information in the world" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::NewProp_WorldOffset = { "WorldOffset", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_PointSimulation_DebugSettings, WorldOffset), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::NewProp_WorldOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::NewProp_WorldOffset_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::NewProp_bEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::NewProp_Scale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::NewProp_CollisionScale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::NewProp_bDrawPointsAsSpheres,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::NewProp_Color,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::NewProp_WorldOffset,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"RigUnit_PointSimulation_DebugSettings",
		sizeof(FRigUnit_PointSimulation_DebugSettings),
		alignof(FRigUnit_PointSimulation_DebugSettings),
		Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_PointSimulation_DebugSettings"), sizeof(FRigUnit_PointSimulation_DebugSettings), Get_Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_PointSimulation_DebugSettings_Hash() { return 1951045400U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
