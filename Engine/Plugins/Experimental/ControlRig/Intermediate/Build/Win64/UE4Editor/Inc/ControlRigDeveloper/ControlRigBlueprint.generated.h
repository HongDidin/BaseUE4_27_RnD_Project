// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UControlRigHierarchyModifier;
class UStruct;
class UControlRigBlueprint;
class USkeletalMesh;
#ifdef CONTROLRIGDEVELOPER_ControlRigBlueprint_generated_h
#error "ControlRigBlueprint.generated.h already included, missing '#pragma once' in ControlRigBlueprint.h"
#endif
#define CONTROLRIGDEVELOPER_ControlRigBlueprint_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigBlueprint_h_36_SPARSE_DATA
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigBlueprint_h_36_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetHierarchyModifier); \
	DECLARE_FUNCTION(execGetAvailableRigUnits); \
	DECLARE_FUNCTION(execGetCurrentlyOpenRigBlueprints); \
	DECLARE_FUNCTION(execRequestControlRigInit); \
	DECLARE_FUNCTION(execRequestAutoVMRecompilation); \
	DECLARE_FUNCTION(execRecompileVMIfRequired); \
	DECLARE_FUNCTION(execRecompileVM); \
	DECLARE_FUNCTION(execGetPreviewMesh); \
	DECLARE_FUNCTION(execSetPreviewMesh);


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigBlueprint_h_36_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetHierarchyModifier); \
	DECLARE_FUNCTION(execGetAvailableRigUnits); \
	DECLARE_FUNCTION(execGetCurrentlyOpenRigBlueprints); \
	DECLARE_FUNCTION(execRequestControlRigInit); \
	DECLARE_FUNCTION(execRequestAutoVMRecompilation); \
	DECLARE_FUNCTION(execRecompileVMIfRequired); \
	DECLARE_FUNCTION(execRecompileVM); \
	DECLARE_FUNCTION(execGetPreviewMesh); \
	DECLARE_FUNCTION(execSetPreviewMesh);


#if WITH_EDITOR
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigBlueprint_h_36_EDITOR_ONLY_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSuspendNotifications);


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigBlueprint_h_36_EDITOR_ONLY_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSuspendNotifications);


#else
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigBlueprint_h_36_EDITOR_ONLY_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigBlueprint_h_36_EDITOR_ONLY_RPC_WRAPPERS_NO_PURE_DECLS
#endif //WITH_EDITOR
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigBlueprint_h_36_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUControlRigBlueprint(); \
	friend struct Z_Construct_UClass_UControlRigBlueprint_Statics; \
public: \
	DECLARE_CLASS(UControlRigBlueprint, UBlueprint, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ControlRigDeveloper"), NO_API) \
	DECLARE_SERIALIZER(UControlRigBlueprint) \
	virtual UObject* _getUObject() const override { return const_cast<UControlRigBlueprint*>(this); }


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigBlueprint_h_36_INCLASS \
private: \
	static void StaticRegisterNativesUControlRigBlueprint(); \
	friend struct Z_Construct_UClass_UControlRigBlueprint_Statics; \
public: \
	DECLARE_CLASS(UControlRigBlueprint, UBlueprint, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ControlRigDeveloper"), NO_API) \
	DECLARE_SERIALIZER(UControlRigBlueprint) \
	virtual UObject* _getUObject() const override { return const_cast<UControlRigBlueprint*>(this); }


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigBlueprint_h_36_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UControlRigBlueprint(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigBlueprint) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigBlueprint); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigBlueprint); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigBlueprint(UControlRigBlueprint&&); \
	NO_API UControlRigBlueprint(const UControlRigBlueprint&); \
public:


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigBlueprint_h_36_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigBlueprint(UControlRigBlueprint&&); \
	NO_API UControlRigBlueprint(const UControlRigBlueprint&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigBlueprint); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigBlueprint); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UControlRigBlueprint)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigBlueprint_h_36_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Hierarchy_DEPRECATED() { return STRUCT_OFFSET(UControlRigBlueprint, Hierarchy_DEPRECATED); } \
	FORCEINLINE static uint32 __PPO__CurveContainer_DEPRECATED() { return STRUCT_OFFSET(UControlRigBlueprint, CurveContainer_DEPRECATED); } \
	FORCEINLINE static uint32 __PPO__bSupportsInversion() { return STRUCT_OFFSET(UControlRigBlueprint, bSupportsInversion); } \
	FORCEINLINE static uint32 __PPO__bSupportsControls() { return STRUCT_OFFSET(UControlRigBlueprint, bSupportsControls); } \
	FORCEINLINE static uint32 __PPO__PreviewSkeletalMesh() { return STRUCT_OFFSET(UControlRigBlueprint, PreviewSkeletalMesh); } \
	FORCEINLINE static uint32 __PPO__SourceHierarchyImport() { return STRUCT_OFFSET(UControlRigBlueprint, SourceHierarchyImport); } \
	FORCEINLINE static uint32 __PPO__SourceCurveImport() { return STRUCT_OFFSET(UControlRigBlueprint, SourceCurveImport); } \
	FORCEINLINE static uint32 __PPO__SupportedEventNames() { return STRUCT_OFFSET(UControlRigBlueprint, SupportedEventNames); } \
	FORCEINLINE static uint32 __PPO__bExposesAnimatableControls() { return STRUCT_OFFSET(UControlRigBlueprint, bExposesAnimatableControls); } \
	FORCEINLINE static uint32 __PPO__bAutoRecompileVM() { return STRUCT_OFFSET(UControlRigBlueprint, bAutoRecompileVM); } \
	FORCEINLINE static uint32 __PPO__bVMRecompilationRequired() { return STRUCT_OFFSET(UControlRigBlueprint, bVMRecompilationRequired); } \
	FORCEINLINE static uint32 __PPO__VMRecompilationBracket() { return STRUCT_OFFSET(UControlRigBlueprint, VMRecompilationBracket); } \
	FORCEINLINE static uint32 __PPO__HierarchyModifier() { return STRUCT_OFFSET(UControlRigBlueprint, HierarchyModifier); } \
	FORCEINLINE static uint32 __PPO__Validator() { return STRUCT_OFFSET(UControlRigBlueprint, Validator); }


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigBlueprint_h_33_PROLOG
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigBlueprint_h_36_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigBlueprint_h_36_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigBlueprint_h_36_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigBlueprint_h_36_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigBlueprint_h_36_EDITOR_ONLY_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigBlueprint_h_36_INCLASS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigBlueprint_h_36_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigBlueprint_h_36_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigBlueprint_h_36_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigBlueprint_h_36_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigBlueprint_h_36_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigBlueprint_h_36_EDITOR_ONLY_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigBlueprint_h_36_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigBlueprint_h_36_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ControlRigBlueprint."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONTROLRIGDEVELOPER_API UClass* StaticClass<class UControlRigBlueprint>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_ControlRigBlueprint_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
