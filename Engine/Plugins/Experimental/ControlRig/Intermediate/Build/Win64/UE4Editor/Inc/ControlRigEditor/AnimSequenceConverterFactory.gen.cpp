// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRigEditor/Private/AnimSequenceConverterFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAnimSequenceConverterFactory() {}
// Cross Module References
	CONTROLRIGEDITOR_API UClass* Z_Construct_UClass_UAnimSequenceConverterFactory_NoRegister();
	CONTROLRIGEDITOR_API UClass* Z_Construct_UClass_UAnimSequenceConverterFactory();
	UNREALED_API UClass* Z_Construct_UClass_UAnimSequenceFactory();
	UPackage* Z_Construct_UPackage__Script_ControlRigEditor();
// End Cross Module References
	void UAnimSequenceConverterFactory::StaticRegisterNativesUAnimSequenceConverterFactory()
	{
	}
	UClass* Z_Construct_UClass_UAnimSequenceConverterFactory_NoRegister()
	{
		return UAnimSequenceConverterFactory::StaticClass();
	}
	struct Z_Construct_UClass_UAnimSequenceConverterFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAnimSequenceConverterFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAnimSequenceFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRigEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAnimSequenceConverterFactory_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "AnimSequenceConverterFactory.h" },
		{ "ModuleRelativePath", "Private/AnimSequenceConverterFactory.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAnimSequenceConverterFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAnimSequenceConverterFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAnimSequenceConverterFactory_Statics::ClassParams = {
		&UAnimSequenceConverterFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UAnimSequenceConverterFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAnimSequenceConverterFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAnimSequenceConverterFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAnimSequenceConverterFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAnimSequenceConverterFactory, 1105041675);
	template<> CONTROLRIGEDITOR_API UClass* StaticClass<UAnimSequenceConverterFactory>()
	{
		return UAnimSequenceConverterFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAnimSequenceConverterFactory(Z_Construct_UClass_UAnimSequenceConverterFactory, &UAnimSequenceConverterFactory::StaticClass, TEXT("/Script/ControlRigEditor"), TEXT("UAnimSequenceConverterFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAnimSequenceConverterFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
