// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Animation/RigUnit_AnimRichCurve.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_AnimRichCurve() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_AnimRichCurve();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_AnimBase();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FRuntimeFloatCurve();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_AnimRichCurve>() == std::is_polymorphic<FRigUnit_AnimBase>(), "USTRUCT FRigUnit_AnimRichCurve cannot be polymorphic unless super FRigUnit_AnimBase is polymorphic");

class UScriptStruct* FRigUnit_AnimRichCurve::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AnimRichCurve_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_AnimRichCurve, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_AnimRichCurve"), sizeof(FRigUnit_AnimRichCurve), Get_Z_Construct_UScriptStruct_FRigUnit_AnimRichCurve_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_AnimRichCurve::Execute"), &FRigUnit_AnimRichCurve::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_AnimRichCurve>()
{
	return FRigUnit_AnimRichCurve::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_AnimRichCurve(FRigUnit_AnimRichCurve::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_AnimRichCurve"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AnimRichCurve
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AnimRichCurve()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_AnimRichCurve>(FName(TEXT("RigUnit_AnimRichCurve")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AnimRichCurve;
	struct Z_Construct_UScriptStruct_FRigUnit_AnimRichCurve_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Curve_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Curve;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AnimRichCurve_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Provides a constant curve to be used for multiple curve evaluations\n */" },
		{ "DisplayName", "Curve" },
		{ "Keywords", "Curve,Profile" },
		{ "ModuleRelativePath", "Private/Units/Animation/RigUnit_AnimRichCurve.h" },
		{ "ToolTip", "Provides a constant curve to be used for multiple curve evaluations" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_AnimRichCurve_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_AnimRichCurve>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AnimRichCurve_Statics::NewProp_Curve_MetaData[] = {
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Animation/RigUnit_AnimRichCurve.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AnimRichCurve_Statics::NewProp_Curve = { "Curve", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AnimRichCurve, Curve), Z_Construct_UScriptStruct_FRuntimeFloatCurve, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AnimRichCurve_Statics::NewProp_Curve_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AnimRichCurve_Statics::NewProp_Curve_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_AnimRichCurve_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AnimRichCurve_Statics::NewProp_Curve,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_AnimRichCurve_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_AnimBase,
		&NewStructOps,
		"RigUnit_AnimRichCurve",
		sizeof(FRigUnit_AnimRichCurve),
		alignof(FRigUnit_AnimRichCurve),
		Z_Construct_UScriptStruct_FRigUnit_AnimRichCurve_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AnimRichCurve_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AnimRichCurve_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AnimRichCurve_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_AnimRichCurve()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AnimRichCurve_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_AnimRichCurve"), sizeof(FRigUnit_AnimRichCurve), Get_Z_Construct_UScriptStruct_FRigUnit_AnimRichCurve_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_AnimRichCurve_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AnimRichCurve_Hash() { return 423742423U; }

void FRigUnit_AnimRichCurve::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Curve,
		Context
	);
}

PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
