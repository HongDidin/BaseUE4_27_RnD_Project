// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_AdditiveControlRig_generated_h
#error "AdditiveControlRig.generated.h already included, missing '#pragma once' in AdditiveControlRig.h"
#endif
#define CONTROLRIG_AdditiveControlRig_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_AdditiveControlRig_h_16_SPARSE_DATA
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_AdditiveControlRig_h_16_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_AdditiveControlRig_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_AdditiveControlRig_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAdditiveControlRig(); \
	friend struct Z_Construct_UClass_UAdditiveControlRig_Statics; \
public: \
	DECLARE_CLASS(UAdditiveControlRig, UControlRig, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRig"), NO_API) \
	DECLARE_SERIALIZER(UAdditiveControlRig)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_AdditiveControlRig_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUAdditiveControlRig(); \
	friend struct Z_Construct_UClass_UAdditiveControlRig_Statics; \
public: \
	DECLARE_CLASS(UAdditiveControlRig, UControlRig, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRig"), NO_API) \
	DECLARE_SERIALIZER(UAdditiveControlRig)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_AdditiveControlRig_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAdditiveControlRig(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAdditiveControlRig) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAdditiveControlRig); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAdditiveControlRig); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAdditiveControlRig(UAdditiveControlRig&&); \
	NO_API UAdditiveControlRig(const UAdditiveControlRig&); \
public:


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_AdditiveControlRig_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAdditiveControlRig() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAdditiveControlRig(UAdditiveControlRig&&); \
	NO_API UAdditiveControlRig(const UAdditiveControlRig&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAdditiveControlRig); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAdditiveControlRig); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UAdditiveControlRig)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_AdditiveControlRig_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_AdditiveControlRig_h_13_PROLOG
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_AdditiveControlRig_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_AdditiveControlRig_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_AdditiveControlRig_h_16_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_AdditiveControlRig_h_16_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_AdditiveControlRig_h_16_INCLASS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_AdditiveControlRig_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_AdditiveControlRig_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_AdditiveControlRig_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_AdditiveControlRig_h_16_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_AdditiveControlRig_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_AdditiveControlRig_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_AdditiveControlRig_h_16_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class AdditiveControlRig."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONTROLRIG_API UClass* StaticClass<class UAdditiveControlRig>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_AdditiveControlRig_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
