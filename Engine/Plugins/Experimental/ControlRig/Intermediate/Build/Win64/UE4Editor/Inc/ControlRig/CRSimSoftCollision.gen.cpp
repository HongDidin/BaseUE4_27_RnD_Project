// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Public/Math/Simulation/CRSimSoftCollision.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCRSimSoftCollision() {}
// Cross Module References
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_ECRSimSoftCollisionType();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FCRSimSoftCollision();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_EControlRigAnimEasingType();
// End Cross Module References
	static UEnum* ECRSimSoftCollisionType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ControlRig_ECRSimSoftCollisionType, Z_Construct_UPackage__Script_ControlRig(), TEXT("ECRSimSoftCollisionType"));
		}
		return Singleton;
	}
	template<> CONTROLRIG_API UEnum* StaticEnum<ECRSimSoftCollisionType>()
	{
		return ECRSimSoftCollisionType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ECRSimSoftCollisionType(ECRSimSoftCollisionType_StaticEnum, TEXT("/Script/ControlRig"), TEXT("ECRSimSoftCollisionType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ControlRig_ECRSimSoftCollisionType_Hash() { return 3245024142U; }
	UEnum* Z_Construct_UEnum_ControlRig_ECRSimSoftCollisionType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ECRSimSoftCollisionType"), 0, Get_Z_Construct_UEnum_ControlRig_ECRSimSoftCollisionType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ECRSimSoftCollisionType::Plane", (int64)ECRSimSoftCollisionType::Plane },
				{ "ECRSimSoftCollisionType::Sphere", (int64)ECRSimSoftCollisionType::Sphere },
				{ "ECRSimSoftCollisionType::Cone", (int64)ECRSimSoftCollisionType::Cone },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Cone.Name", "ECRSimSoftCollisionType::Cone" },
				{ "ModuleRelativePath", "Public/Math/Simulation/CRSimSoftCollision.h" },
				{ "Plane.Name", "ECRSimSoftCollisionType::Plane" },
				{ "Sphere.Name", "ECRSimSoftCollisionType::Sphere" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ControlRig,
				nullptr,
				"ECRSimSoftCollisionType",
				"ECRSimSoftCollisionType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FCRSimSoftCollision::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FCRSimSoftCollision_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCRSimSoftCollision, Z_Construct_UPackage__Script_ControlRig(), TEXT("CRSimSoftCollision"), sizeof(FCRSimSoftCollision), Get_Z_Construct_UScriptStruct_FCRSimSoftCollision_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FCRSimSoftCollision>()
{
	return FCRSimSoftCollision::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCRSimSoftCollision(FCRSimSoftCollision::StaticStruct, TEXT("/Script/ControlRig"), TEXT("CRSimSoftCollision"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFCRSimSoftCollision
{
	FScriptStruct_ControlRig_StaticRegisterNativesFCRSimSoftCollision()
	{
		UScriptStruct::DeferCppStructOps<FCRSimSoftCollision>(FName(TEXT("CRSimSoftCollision")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFCRSimSoftCollision;
	struct Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Transform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Transform;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ShapeType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShapeType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ShapeType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MinimumDistance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MinimumDistance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaximumDistance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaximumDistance;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_FalloffType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FalloffType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_FalloffType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Coefficient_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Coefficient;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bInverted_MetaData[];
#endif
		static void NewProp_bInverted_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInverted;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimSoftCollision.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FCRSimSoftCollision>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewProp_Transform_MetaData[] = {
		{ "Comment", "/**\n\x09 * The world / global transform of the collisoin\n\x09 */" },
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimSoftCollision.h" },
		{ "ToolTip", "The world / global transform of the collisoin" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewProp_Transform = { "Transform", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCRSimSoftCollision, Transform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewProp_Transform_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewProp_Transform_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewProp_ShapeType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewProp_ShapeType_MetaData[] = {
		{ "Comment", "/**\n\x09 * The type of collision shape\n\x09 */" },
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimSoftCollision.h" },
		{ "ToolTip", "The type of collision shape" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewProp_ShapeType = { "ShapeType", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCRSimSoftCollision, ShapeType), Z_Construct_UEnum_ControlRig_ECRSimSoftCollisionType, METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewProp_ShapeType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewProp_ShapeType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewProp_MinimumDistance_MetaData[] = {
		{ "Comment", "/**\n\x09 * The minimum distance for the collision.\n\x09 * If this is equal or higher than the maximum there's no falloff.\n\x09 * For a cone shape this represents the minimum angle in degrees.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimSoftCollision.h" },
		{ "ToolTip", "The minimum distance for the collision.\nIf this is equal or higher than the maximum there's no falloff.\nFor a cone shape this represents the minimum angle in degrees." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewProp_MinimumDistance = { "MinimumDistance", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCRSimSoftCollision, MinimumDistance), METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewProp_MinimumDistance_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewProp_MinimumDistance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewProp_MaximumDistance_MetaData[] = {
		{ "Comment", "/**\n\x09 * The maximum distance for the collision.\n\x09 * If this is equal or lower than the minimum there's no falloff.\n\x09 * For a cone shape this represents the maximum angle in degrees.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimSoftCollision.h" },
		{ "ToolTip", "The maximum distance for the collision.\nIf this is equal or lower than the minimum there's no falloff.\nFor a cone shape this represents the maximum angle in degrees." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewProp_MaximumDistance = { "MaximumDistance", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCRSimSoftCollision, MaximumDistance), METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewProp_MaximumDistance_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewProp_MaximumDistance_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewProp_FalloffType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewProp_FalloffType_MetaData[] = {
		{ "Comment", "/**\n\x09 * The type of falloff to use\n\x09 */" },
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimSoftCollision.h" },
		{ "ToolTip", "The type of falloff to use" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewProp_FalloffType = { "FalloffType", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCRSimSoftCollision, FalloffType), Z_Construct_UEnum_ControlRig_EControlRigAnimEasingType, METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewProp_FalloffType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewProp_FalloffType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewProp_Coefficient_MetaData[] = {
		{ "Comment", "/**\n\x09 * The strength of the collision force\n\x09 */" },
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimSoftCollision.h" },
		{ "ToolTip", "The strength of the collision force" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewProp_Coefficient = { "Coefficient", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCRSimSoftCollision, Coefficient), METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewProp_Coefficient_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewProp_Coefficient_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewProp_bInverted_MetaData[] = {
		{ "Comment", "/**\n\x09 * If set to true the collision volume will be inverted\n\x09 */" },
		{ "ModuleRelativePath", "Public/Math/Simulation/CRSimSoftCollision.h" },
		{ "ToolTip", "If set to true the collision volume will be inverted" },
	};
#endif
	void Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewProp_bInverted_SetBit(void* Obj)
	{
		((FCRSimSoftCollision*)Obj)->bInverted = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewProp_bInverted = { "bInverted", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FCRSimSoftCollision), &Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewProp_bInverted_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewProp_bInverted_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewProp_bInverted_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewProp_Transform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewProp_ShapeType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewProp_ShapeType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewProp_MinimumDistance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewProp_MaximumDistance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewProp_FalloffType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewProp_FalloffType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewProp_Coefficient,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::NewProp_bInverted,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"CRSimSoftCollision",
		sizeof(FCRSimSoftCollision),
		alignof(FCRSimSoftCollision),
		Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FCRSimSoftCollision()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FCRSimSoftCollision_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CRSimSoftCollision"), sizeof(FCRSimSoftCollision), Get_Z_Construct_UScriptStruct_FCRSimSoftCollision_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FCRSimSoftCollision_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCRSimSoftCollision_Hash() { return 291090149U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
