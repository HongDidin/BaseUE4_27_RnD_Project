// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigUnit_GetInitialBoneTransform_generated_h
#error "RigUnit_GetInitialBoneTransform.generated.h already included, missing '#pragma once' in RigUnit_GetInitialBoneTransform.h"
#endif
#define CONTROLRIG_RigUnit_GetInitialBoneTransform_generated_h


#define FRigUnit_GetInitialBoneTransform_Execute() \
	void FRigUnit_GetInitialBoneTransform::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Bone, \
		const EBoneGetterSetterMode Space, \
		FTransform& Transform, \
		FCachedRigElement& CachedBone, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Hierarchy_RigUnit_GetInitialBoneTransform_h_14_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_GetInitialBoneTransform_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Bone, \
		const EBoneGetterSetterMode Space, \
		FTransform& Transform, \
		FCachedRigElement& CachedBone, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FName& Bone = *(FName*)RigVMMemoryHandles[0].GetData(); \
		EBoneGetterSetterMode Space = (EBoneGetterSetterMode)*(uint8*)RigVMMemoryHandles[1].GetData(); \
		FTransform& Transform = *(FTransform*)RigVMMemoryHandles[2].GetData(); \
		FRigVMDynamicArray<FCachedRigElement> CachedBone_3_Array(*((FRigVMByteArray*)RigVMMemoryHandles[3].GetData(0, false))); \
		CachedBone_3_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedBone = CachedBone_3_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Bone, \
			Space, \
			Transform, \
			CachedBone, \
			Context \
		); \
	} \
	typedef FRigUnit Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_GetInitialBoneTransform>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Hierarchy_RigUnit_GetInitialBoneTransform_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
