// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_CRSimPointContainer_generated_h
#error "CRSimPointContainer.generated.h already included, missing '#pragma once' in CRSimPointContainer.h"
#endif
#define CONTROLRIG_CRSimPointContainer_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Math_Simulation_CRSimPointContainer_h_17_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCRSimPointContainer_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__PreviousStep() { return STRUCT_OFFSET(FCRSimPointContainer, PreviousStep); } \
	typedef FCRSimContainer Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FCRSimPointContainer>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Math_Simulation_CRSimPointContainer_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
