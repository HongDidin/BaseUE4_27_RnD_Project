// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Execution/RigUnit_Item.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_Item() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ItemReplace();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ItemBase();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigElementKey();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ItemExists();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FCachedRigElement();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ItemBaseMutable();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnitMutable();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_ItemReplace>() == std::is_polymorphic<FRigUnit_ItemBase>(), "USTRUCT FRigUnit_ItemReplace cannot be polymorphic unless super FRigUnit_ItemBase is polymorphic");

class UScriptStruct* FRigUnit_ItemReplace::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ItemReplace_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_ItemReplace, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_ItemReplace"), sizeof(FRigUnit_ItemReplace), Get_Z_Construct_UScriptStruct_FRigUnit_ItemReplace_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_ItemReplace::Execute"), &FRigUnit_ItemReplace::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_ItemReplace>()
{
	return FRigUnit_ItemReplace::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_ItemReplace(FRigUnit_ItemReplace::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_ItemReplace"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ItemReplace
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ItemReplace()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_ItemReplace>(FName(TEXT("RigUnit_ItemReplace")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ItemReplace;
	struct Z_Construct_UScriptStruct_FRigUnit_ItemReplace_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Item_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Item;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Old_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Old;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_New_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_New;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Result;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ItemReplace_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Replaces the text within the name of the item\n */" },
		{ "DisplayName", "Item Replace" },
		{ "Keywords", "Replace,Name" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Item.h" },
		{ "ToolTip", "Replaces the text within the name of the item" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_ItemReplace_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_ItemReplace>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ItemReplace_Statics::NewProp_Item_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Item.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ItemReplace_Statics::NewProp_Item = { "Item", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ItemReplace, Item), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ItemReplace_Statics::NewProp_Item_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ItemReplace_Statics::NewProp_Item_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ItemReplace_Statics::NewProp_Old_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Item.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_ItemReplace_Statics::NewProp_Old = { "Old", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ItemReplace, Old), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ItemReplace_Statics::NewProp_Old_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ItemReplace_Statics::NewProp_Old_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ItemReplace_Statics::NewProp_New_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Item.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_ItemReplace_Statics::NewProp_New = { "New", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ItemReplace, New), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ItemReplace_Statics::NewProp_New_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ItemReplace_Statics::NewProp_New_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ItemReplace_Statics::NewProp_Result_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Item.h" },
		{ "Output", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ItemReplace_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ItemReplace, Result), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ItemReplace_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ItemReplace_Statics::NewProp_Result_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_ItemReplace_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ItemReplace_Statics::NewProp_Item,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ItemReplace_Statics::NewProp_Old,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ItemReplace_Statics::NewProp_New,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ItemReplace_Statics::NewProp_Result,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_ItemReplace_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_ItemBase,
		&NewStructOps,
		"RigUnit_ItemReplace",
		sizeof(FRigUnit_ItemReplace),
		alignof(FRigUnit_ItemReplace),
		Z_Construct_UScriptStruct_FRigUnit_ItemReplace_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ItemReplace_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ItemReplace_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ItemReplace_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ItemReplace()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ItemReplace_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_ItemReplace"), sizeof(FRigUnit_ItemReplace), Get_Z_Construct_UScriptStruct_FRigUnit_ItemReplace_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_ItemReplace_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ItemReplace_Hash() { return 1811659610U; }

void FRigUnit_ItemReplace::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Item,
		Old,
		New,
		Result,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_ItemExists>() == std::is_polymorphic<FRigUnit_ItemBase>(), "USTRUCT FRigUnit_ItemExists cannot be polymorphic unless super FRigUnit_ItemBase is polymorphic");

class UScriptStruct* FRigUnit_ItemExists::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ItemExists_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_ItemExists, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_ItemExists"), sizeof(FRigUnit_ItemExists), Get_Z_Construct_UScriptStruct_FRigUnit_ItemExists_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_ItemExists::Execute"), &FRigUnit_ItemExists::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_ItemExists>()
{
	return FRigUnit_ItemExists::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_ItemExists(FRigUnit_ItemExists::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_ItemExists"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ItemExists
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ItemExists()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_ItemExists>(FName(TEXT("RigUnit_ItemExists")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ItemExists;
	struct Z_Construct_UScriptStruct_FRigUnit_ItemExists_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Item_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Item;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Exists_MetaData[];
#endif
		static void NewProp_Exists_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Exists;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CachedIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ItemExists_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Returns true or false if a given item exists\n */" },
		{ "DisplayName", "Item Exists" },
		{ "Keywords", "" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Item.h" },
		{ "ToolTip", "Returns true or false if a given item exists" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_ItemExists_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_ItemExists>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ItemExists_Statics::NewProp_Item_MetaData[] = {
		{ "ExpandByDefault", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Item.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ItemExists_Statics::NewProp_Item = { "Item", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ItemExists, Item), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ItemExists_Statics::NewProp_Item_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ItemExists_Statics::NewProp_Item_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ItemExists_Statics::NewProp_Exists_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Item.h" },
		{ "Output", "" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_ItemExists_Statics::NewProp_Exists_SetBit(void* Obj)
	{
		((FRigUnit_ItemExists*)Obj)->Exists = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_ItemExists_Statics::NewProp_Exists = { "Exists", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_ItemExists), &Z_Construct_UScriptStruct_FRigUnit_ItemExists_Statics::NewProp_Exists_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ItemExists_Statics::NewProp_Exists_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ItemExists_Statics::NewProp_Exists_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ItemExists_Statics::NewProp_CachedIndex_MetaData[] = {
		{ "Comment", "// Used to cache the internally used index\n" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Item.h" },
		{ "ToolTip", "Used to cache the internally used index" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ItemExists_Statics::NewProp_CachedIndex = { "CachedIndex", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ItemExists, CachedIndex), Z_Construct_UScriptStruct_FCachedRigElement, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ItemExists_Statics::NewProp_CachedIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ItemExists_Statics::NewProp_CachedIndex_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_ItemExists_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ItemExists_Statics::NewProp_Item,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ItemExists_Statics::NewProp_Exists,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ItemExists_Statics::NewProp_CachedIndex,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_ItemExists_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_ItemBase,
		&NewStructOps,
		"RigUnit_ItemExists",
		sizeof(FRigUnit_ItemExists),
		alignof(FRigUnit_ItemExists),
		Z_Construct_UScriptStruct_FRigUnit_ItemExists_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ItemExists_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ItemExists_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ItemExists_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ItemExists()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ItemExists_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_ItemExists"), sizeof(FRigUnit_ItemExists), Get_Z_Construct_UScriptStruct_FRigUnit_ItemExists_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_ItemExists_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ItemExists_Hash() { return 1610420348U; }

void FRigUnit_ItemExists::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Item,
		Exists,
		CachedIndex,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_ItemBaseMutable>() == std::is_polymorphic<FRigUnitMutable>(), "USTRUCT FRigUnit_ItemBaseMutable cannot be polymorphic unless super FRigUnitMutable is polymorphic");

class UScriptStruct* FRigUnit_ItemBaseMutable::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ItemBaseMutable_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_ItemBaseMutable, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_ItemBaseMutable"), sizeof(FRigUnit_ItemBaseMutable), Get_Z_Construct_UScriptStruct_FRigUnit_ItemBaseMutable_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_ItemBaseMutable>()
{
	return FRigUnit_ItemBaseMutable::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_ItemBaseMutable(FRigUnit_ItemBaseMutable::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_ItemBaseMutable"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ItemBaseMutable
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ItemBaseMutable()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_ItemBaseMutable>(FName(TEXT("RigUnit_ItemBaseMutable")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ItemBaseMutable;
	struct Z_Construct_UScriptStruct_FRigUnit_ItemBaseMutable_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ItemBaseMutable_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "Category", "Collections" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Item.h" },
		{ "NodeColor", "0.7 0.05 0.5" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_ItemBaseMutable_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_ItemBaseMutable>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_ItemBaseMutable_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnitMutable,
		&NewStructOps,
		"RigUnit_ItemBaseMutable",
		sizeof(FRigUnit_ItemBaseMutable),
		alignof(FRigUnit_ItemBaseMutable),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ItemBaseMutable_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ItemBaseMutable_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ItemBaseMutable()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ItemBaseMutable_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_ItemBaseMutable"), sizeof(FRigUnit_ItemBaseMutable), Get_Z_Construct_UScriptStruct_FRigUnit_ItemBaseMutable_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_ItemBaseMutable_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ItemBaseMutable_Hash() { return 849713667U; }

static_assert(std::is_polymorphic<FRigUnit_ItemBase>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_ItemBase cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_ItemBase::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ItemBase_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_ItemBase, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_ItemBase"), sizeof(FRigUnit_ItemBase), Get_Z_Construct_UScriptStruct_FRigUnit_ItemBase_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_ItemBase>()
{
	return FRigUnit_ItemBase::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_ItemBase(FRigUnit_ItemBase::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_ItemBase"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ItemBase
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ItemBase()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_ItemBase>(FName(TEXT("RigUnit_ItemBase")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ItemBase;
	struct Z_Construct_UScriptStruct_FRigUnit_ItemBase_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ItemBase_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "Category", "Collections" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_Item.h" },
		{ "NodeColor", "0.7 0.05 0.5" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_ItemBase_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_ItemBase>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_ItemBase_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_ItemBase",
		sizeof(FRigUnit_ItemBase),
		alignof(FRigUnit_ItemBase),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ItemBase_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ItemBase_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ItemBase()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ItemBase_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_ItemBase"), sizeof(FRigUnit_ItemBase), Get_Z_Construct_UScriptStruct_FRigUnit_ItemBase_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_ItemBase_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ItemBase_Hash() { return 2167631491U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
