// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_TwoBoneIKSimple() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_HighlevelBase();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_HighlevelBaseMutable();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigElementKey();
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_EControlRigVectorKind();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_DebugSettings();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FCachedRigElement();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_TwoBoneIKSimpleTransforms>() == std::is_polymorphic<FRigUnit_HighlevelBase>(), "USTRUCT FRigUnit_TwoBoneIKSimpleTransforms cannot be polymorphic unless super FRigUnit_HighlevelBase is polymorphic");

class UScriptStruct* FRigUnit_TwoBoneIKSimpleTransforms::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_TwoBoneIKSimpleTransforms"), sizeof(FRigUnit_TwoBoneIKSimpleTransforms), Get_Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_TwoBoneIKSimpleTransforms::Execute"), &FRigUnit_TwoBoneIKSimpleTransforms::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_TwoBoneIKSimpleTransforms>()
{
	return FRigUnit_TwoBoneIKSimpleTransforms::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms(FRigUnit_TwoBoneIKSimpleTransforms::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_TwoBoneIKSimpleTransforms"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_TwoBoneIKSimpleTransforms
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_TwoBoneIKSimpleTransforms()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_TwoBoneIKSimpleTransforms>(FName(TEXT("RigUnit_TwoBoneIKSimpleTransforms")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_TwoBoneIKSimpleTransforms;
	struct Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Root_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Root;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PoleVector_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PoleVector;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Effector_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Effector;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PrimaryAxis_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PrimaryAxis;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SecondaryAxis_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SecondaryAxis;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SecondaryAxisWeight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SecondaryAxisWeight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableStretch_MetaData[];
#endif
		static void NewProp_bEnableStretch_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableStretch;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StretchStartRatio_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_StretchStartRatio;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StretchMaximumRatio_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_StretchMaximumRatio;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoneALength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BoneALength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoneBLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BoneBLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Elbow_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Elbow;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "/**\n * Solves the two bone IK given transforms\n * Note: This node operates in world space!\n */" },
		{ "DisplayName", "Basic IK Transforms" },
		{ "Keywords", "TwoBone,IK" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "Solves the two bone IK given transforms\nNote: This node operates in world space!" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_TwoBoneIKSimpleTransforms>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_Root_MetaData[] = {
		{ "Comment", "/**\n\x09 * The transform of the root of the triangle\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "Output", "" },
		{ "ToolTip", "The transform of the root of the triangle" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_Root = { "Root", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimpleTransforms, Root), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_Root_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_Root_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_PoleVector_MetaData[] = {
		{ "Comment", "/**\n\x09 * The position of the pole of the triangle\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The position of the pole of the triangle" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_PoleVector = { "PoleVector", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimpleTransforms, PoleVector), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_PoleVector_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_PoleVector_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_Effector_MetaData[] = {
		{ "Comment", "/**\n\x09 * The transform of the effector\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "Output", "" },
		{ "ToolTip", "The transform of the effector" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_Effector = { "Effector", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimpleTransforms, Effector), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_Effector_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_Effector_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_PrimaryAxis_MetaData[] = {
		{ "Comment", "/** \n\x09 * The major axis being aligned - along the bone\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The major axis being aligned - along the bone" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_PrimaryAxis = { "PrimaryAxis", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimpleTransforms, PrimaryAxis), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_PrimaryAxis_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_PrimaryAxis_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_SecondaryAxis_MetaData[] = {
		{ "Comment", "/** \n\x09 * The minor axis being aligned - towards the polevector\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The minor axis being aligned - towards the polevector" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_SecondaryAxis = { "SecondaryAxis", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimpleTransforms, SecondaryAxis), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_SecondaryAxis_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_SecondaryAxis_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_SecondaryAxisWeight_MetaData[] = {
		{ "Comment", "/**\n\x09 * Determines how much the secondary axis roll is being applied\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "Determines how much the secondary axis roll is being applied" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_SecondaryAxisWeight = { "SecondaryAxisWeight", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimpleTransforms, SecondaryAxisWeight), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_SecondaryAxisWeight_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_SecondaryAxisWeight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_bEnableStretch_MetaData[] = {
		{ "Comment", "/**\n\x09 * If set to true the stretch feature of the solver will be enabled\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "If set to true the stretch feature of the solver will be enabled" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_bEnableStretch_SetBit(void* Obj)
	{
		((FRigUnit_TwoBoneIKSimpleTransforms*)Obj)->bEnableStretch = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_bEnableStretch = { "bEnableStretch", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_TwoBoneIKSimpleTransforms), &Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_bEnableStretch_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_bEnableStretch_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_bEnableStretch_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_StretchStartRatio_MetaData[] = {
		{ "Comment", "/**\n\x09 * The ratio where the stretch starts\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The ratio where the stretch starts" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_StretchStartRatio = { "StretchStartRatio", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimpleTransforms, StretchStartRatio), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_StretchStartRatio_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_StretchStartRatio_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_StretchMaximumRatio_MetaData[] = {
		{ "Comment", "/**\n\x09 * The maximum allowed stretch ratio\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The maximum allowed stretch ratio" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_StretchMaximumRatio = { "StretchMaximumRatio", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimpleTransforms, StretchMaximumRatio), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_StretchMaximumRatio_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_StretchMaximumRatio_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_BoneALength_MetaData[] = {
		{ "Comment", "/**\n\x09 * The length of the first bone.\n\x09 * If set to 0.0 it will be determined by the hierarchy\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The length of the first bone.\nIf set to 0.0 it will be determined by the hierarchy" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_BoneALength = { "BoneALength", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimpleTransforms, BoneALength), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_BoneALength_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_BoneALength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_BoneBLength_MetaData[] = {
		{ "Comment", "/**\n\x09 * The length of the second  bone.\n\x09 * If set to 0.0 it will be determined by the hierarchy\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The length of the second  bone.\nIf set to 0.0 it will be determined by the hierarchy" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_BoneBLength = { "BoneBLength", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimpleTransforms, BoneBLength), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_BoneBLength_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_BoneBLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_Elbow_MetaData[] = {
		{ "Comment", "/**\n\x09 * The resulting elbow transform\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "Output", "" },
		{ "ToolTip", "The resulting elbow transform" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_Elbow = { "Elbow", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimpleTransforms, Elbow), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_Elbow_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_Elbow_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_Root,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_PoleVector,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_Effector,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_PrimaryAxis,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_SecondaryAxis,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_SecondaryAxisWeight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_bEnableStretch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_StretchStartRatio,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_StretchMaximumRatio,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_BoneALength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_BoneBLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::NewProp_Elbow,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_HighlevelBase,
		&NewStructOps,
		"RigUnit_TwoBoneIKSimpleTransforms",
		sizeof(FRigUnit_TwoBoneIKSimpleTransforms),
		alignof(FRigUnit_TwoBoneIKSimpleTransforms),
		Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_TwoBoneIKSimpleTransforms"), sizeof(FRigUnit_TwoBoneIKSimpleTransforms), Get_Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleTransforms_Hash() { return 1621580658U; }

void FRigUnit_TwoBoneIKSimpleTransforms::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Root,
		PoleVector,
		Effector,
		PrimaryAxis,
		SecondaryAxis,
		SecondaryAxisWeight,
		bEnableStretch,
		StretchStartRatio,
		StretchMaximumRatio,
		BoneALength,
		BoneBLength,
		Elbow,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_TwoBoneIKSimpleVectors>() == std::is_polymorphic<FRigUnit_HighlevelBase>(), "USTRUCT FRigUnit_TwoBoneIKSimpleVectors cannot be polymorphic unless super FRigUnit_HighlevelBase is polymorphic");

class UScriptStruct* FRigUnit_TwoBoneIKSimpleVectors::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_TwoBoneIKSimpleVectors"), sizeof(FRigUnit_TwoBoneIKSimpleVectors), Get_Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_TwoBoneIKSimpleVectors::Execute"), &FRigUnit_TwoBoneIKSimpleVectors::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_TwoBoneIKSimpleVectors>()
{
	return FRigUnit_TwoBoneIKSimpleVectors::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors(FRigUnit_TwoBoneIKSimpleVectors::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_TwoBoneIKSimpleVectors"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_TwoBoneIKSimpleVectors
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_TwoBoneIKSimpleVectors()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_TwoBoneIKSimpleVectors>(FName(TEXT("RigUnit_TwoBoneIKSimpleVectors")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_TwoBoneIKSimpleVectors;
	struct Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Root_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Root;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PoleVector_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PoleVector;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Effector_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Effector;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableStretch_MetaData[];
#endif
		static void NewProp_bEnableStretch_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableStretch;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StretchStartRatio_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_StretchStartRatio;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StretchMaximumRatio_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_StretchMaximumRatio;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoneALength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BoneALength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoneBLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BoneBLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Elbow_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Elbow;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "/**\n * Solves the two bone IK given positions\n * Note: This node operates in world space!\n */" },
		{ "DisplayName", "Basic IK Positions" },
		{ "Keywords", "TwoBone,IK" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "Solves the two bone IK given positions\nNote: This node operates in world space!" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_TwoBoneIKSimpleVectors>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_Root_MetaData[] = {
		{ "Comment", "/**\n\x09 * The position of the root of the triangle\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The position of the root of the triangle" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_Root = { "Root", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimpleVectors, Root), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_Root_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_Root_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_PoleVector_MetaData[] = {
		{ "Comment", "/**\n\x09 * The position of the pole of the triangle\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The position of the pole of the triangle" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_PoleVector = { "PoleVector", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimpleVectors, PoleVector), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_PoleVector_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_PoleVector_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_Effector_MetaData[] = {
		{ "Comment", "/**\n\x09 * The position of the effector\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "Output", "" },
		{ "ToolTip", "The position of the effector" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_Effector = { "Effector", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimpleVectors, Effector), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_Effector_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_Effector_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_bEnableStretch_MetaData[] = {
		{ "Comment", "/**\n\x09 * If set to true the stretch feature of the solver will be enabled\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "If set to true the stretch feature of the solver will be enabled" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_bEnableStretch_SetBit(void* Obj)
	{
		((FRigUnit_TwoBoneIKSimpleVectors*)Obj)->bEnableStretch = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_bEnableStretch = { "bEnableStretch", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_TwoBoneIKSimpleVectors), &Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_bEnableStretch_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_bEnableStretch_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_bEnableStretch_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_StretchStartRatio_MetaData[] = {
		{ "Comment", "/**\n\x09 * The ratio where the stretch starts\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The ratio where the stretch starts" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_StretchStartRatio = { "StretchStartRatio", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimpleVectors, StretchStartRatio), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_StretchStartRatio_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_StretchStartRatio_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_StretchMaximumRatio_MetaData[] = {
		{ "Comment", "/**\n\x09 * The maximum allowed stretch ratio\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The maximum allowed stretch ratio" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_StretchMaximumRatio = { "StretchMaximumRatio", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimpleVectors, StretchMaximumRatio), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_StretchMaximumRatio_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_StretchMaximumRatio_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_BoneALength_MetaData[] = {
		{ "Comment", "/**\n\x09 * The length of the first bone.\n\x09 * If set to 0.0 it will be determined by the hierarchy\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The length of the first bone.\nIf set to 0.0 it will be determined by the hierarchy" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_BoneALength = { "BoneALength", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimpleVectors, BoneALength), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_BoneALength_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_BoneALength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_BoneBLength_MetaData[] = {
		{ "Comment", "/**\n\x09 * The length of the second  bone.\n\x09 * If set to 0.0 it will be determined by the hierarchy\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The length of the second  bone.\nIf set to 0.0 it will be determined by the hierarchy" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_BoneBLength = { "BoneBLength", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimpleVectors, BoneBLength), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_BoneBLength_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_BoneBLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_Elbow_MetaData[] = {
		{ "Comment", "/**\n\x09 * The resulting elbow position\n\x09 */" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "Output", "" },
		{ "ToolTip", "The resulting elbow position" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_Elbow = { "Elbow", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimpleVectors, Elbow), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_Elbow_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_Elbow_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_Root,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_PoleVector,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_Effector,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_bEnableStretch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_StretchStartRatio,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_StretchMaximumRatio,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_BoneALength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_BoneBLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::NewProp_Elbow,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_HighlevelBase,
		&NewStructOps,
		"RigUnit_TwoBoneIKSimpleVectors",
		sizeof(FRigUnit_TwoBoneIKSimpleVectors),
		alignof(FRigUnit_TwoBoneIKSimpleVectors),
		Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_TwoBoneIKSimpleVectors"), sizeof(FRigUnit_TwoBoneIKSimpleVectors), Get_Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimpleVectors_Hash() { return 3624212899U; }

void FRigUnit_TwoBoneIKSimpleVectors::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Root,
		PoleVector,
		Effector,
		bEnableStretch,
		StretchStartRatio,
		StretchMaximumRatio,
		BoneALength,
		BoneBLength,
		Elbow,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_TwoBoneIKSimplePerItem>() == std::is_polymorphic<FRigUnit_HighlevelBaseMutable>(), "USTRUCT FRigUnit_TwoBoneIKSimplePerItem cannot be polymorphic unless super FRigUnit_HighlevelBaseMutable is polymorphic");

class UScriptStruct* FRigUnit_TwoBoneIKSimplePerItem::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_TwoBoneIKSimplePerItem"), sizeof(FRigUnit_TwoBoneIKSimplePerItem), Get_Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_TwoBoneIKSimplePerItem::Execute"), &FRigUnit_TwoBoneIKSimplePerItem::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_TwoBoneIKSimplePerItem>()
{
	return FRigUnit_TwoBoneIKSimplePerItem::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem(FRigUnit_TwoBoneIKSimplePerItem::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_TwoBoneIKSimplePerItem"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_TwoBoneIKSimplePerItem
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_TwoBoneIKSimplePerItem()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_TwoBoneIKSimplePerItem>(FName(TEXT("RigUnit_TwoBoneIKSimplePerItem")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_TwoBoneIKSimplePerItem;
	struct Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ItemA_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ItemA;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ItemB_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ItemB;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EffectorItem_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EffectorItem;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Effector_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Effector;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PrimaryAxis_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PrimaryAxis;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SecondaryAxis_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SecondaryAxis;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SecondaryAxisWeight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SecondaryAxisWeight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PoleVector_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PoleVector;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_PoleVectorKind_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PoleVectorKind_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_PoleVectorKind;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PoleVectorSpace_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PoleVectorSpace;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableStretch_MetaData[];
#endif
		static void NewProp_bEnableStretch_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableStretch;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StretchStartRatio_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_StretchStartRatio;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StretchMaximumRatio_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_StretchMaximumRatio;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Weight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Weight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ItemALength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ItemALength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ItemBLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ItemBLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPropagateToChildren_MetaData[];
#endif
		static void NewProp_bPropagateToChildren_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPropagateToChildren;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DebugSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DebugSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedItemAIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CachedItemAIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedItemBIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CachedItemBIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedEffectorItemIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CachedEffectorItemIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedPoleVectorSpaceIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CachedPoleVectorSpaceIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "/**\n * Solves the two bone IK given two bones.\n * Note: This node operates in world space!\n */" },
		{ "DisplayName", "Basic IK" },
		{ "Keywords", "TwoBone,IK" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "Solves the two bone IK given two bones.\nNote: This node operates in world space!" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_TwoBoneIKSimplePerItem>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_ItemA_MetaData[] = {
		{ "Comment", "/** \n\x09 * The name of first item\n\x09 */" },
		{ "ExpandByDefault", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The name of first item" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_ItemA = { "ItemA", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimplePerItem, ItemA), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_ItemA_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_ItemA_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_ItemB_MetaData[] = {
		{ "Comment", "/** \n\x09 * The name of second item\n\x09 */" },
		{ "ExpandByDefault", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The name of second item" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_ItemB = { "ItemB", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimplePerItem, ItemB), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_ItemB_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_ItemB_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_EffectorItem_MetaData[] = {
		{ "Comment", "/** \n\x09 * The name of the effector item (if exists)\n\x09 */" },
		{ "ExpandByDefault", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The name of the effector item (if exists)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_EffectorItem = { "EffectorItem", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimplePerItem, EffectorItem), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_EffectorItem_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_EffectorItem_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_Effector_MetaData[] = {
		{ "Comment", "/** \n\x09 * The transform of the effector\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The transform of the effector" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_Effector = { "Effector", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimplePerItem, Effector), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_Effector_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_Effector_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_PrimaryAxis_MetaData[] = {
		{ "Comment", "/** \n\x09 * The major axis being aligned - along the item\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The major axis being aligned - along the item" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_PrimaryAxis = { "PrimaryAxis", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimplePerItem, PrimaryAxis), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_PrimaryAxis_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_PrimaryAxis_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_SecondaryAxis_MetaData[] = {
		{ "Comment", "/** \n\x09 * The minor axis being aligned - towards the polevector\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The minor axis being aligned - towards the polevector" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_SecondaryAxis = { "SecondaryAxis", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimplePerItem, SecondaryAxis), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_SecondaryAxis_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_SecondaryAxis_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_SecondaryAxisWeight_MetaData[] = {
		{ "Comment", "/**\n\x09 * Determines how much the secondary axis roll is being applied\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "Determines how much the secondary axis roll is being applied" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_SecondaryAxisWeight = { "SecondaryAxisWeight", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimplePerItem, SecondaryAxisWeight), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_SecondaryAxisWeight_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_SecondaryAxisWeight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_PoleVector_MetaData[] = {
		{ "Comment", "/** \n\x09 * The polevector to use for the IK solver\n\x09 * This can be a location or direction.\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The polevector to use for the IK solver\nThis can be a location or direction." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_PoleVector = { "PoleVector", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimplePerItem, PoleVector), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_PoleVector_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_PoleVector_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_PoleVectorKind_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_PoleVectorKind_MetaData[] = {
		{ "Comment", "/**\n\x09 * The kind of pole vector this is representing - can be a direction or a location\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The kind of pole vector this is representing - can be a direction or a location" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_PoleVectorKind = { "PoleVectorKind", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimplePerItem, PoleVectorKind), Z_Construct_UEnum_ControlRig_EControlRigVectorKind, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_PoleVectorKind_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_PoleVectorKind_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_PoleVectorSpace_MetaData[] = {
		{ "Comment", "/** \n\x09 * The space in which the pole vector is expressed\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The space in which the pole vector is expressed" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_PoleVectorSpace = { "PoleVectorSpace", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimplePerItem, PoleVectorSpace), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_PoleVectorSpace_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_PoleVectorSpace_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_bEnableStretch_MetaData[] = {
		{ "Comment", "/**\n\x09 * If set to true the stretch feature of the solver will be enabled\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "If set to true the stretch feature of the solver will be enabled" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_bEnableStretch_SetBit(void* Obj)
	{
		((FRigUnit_TwoBoneIKSimplePerItem*)Obj)->bEnableStretch = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_bEnableStretch = { "bEnableStretch", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_TwoBoneIKSimplePerItem), &Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_bEnableStretch_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_bEnableStretch_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_bEnableStretch_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_StretchStartRatio_MetaData[] = {
		{ "Comment", "/**\n\x09 * The ratio where the stretch starts\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The ratio where the stretch starts" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_StretchStartRatio = { "StretchStartRatio", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimplePerItem, StretchStartRatio), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_StretchStartRatio_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_StretchStartRatio_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_StretchMaximumRatio_MetaData[] = {
		{ "Comment", "/**\n     * The maximum allowed stretch ratio\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The maximum allowed stretch ratio" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_StretchMaximumRatio = { "StretchMaximumRatio", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimplePerItem, StretchMaximumRatio), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_StretchMaximumRatio_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_StretchMaximumRatio_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_Weight_MetaData[] = {
		{ "Comment", "/** \n\x09 * The weight of the solver - how much the IK should be applied.\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The weight of the solver - how much the IK should be applied." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_Weight = { "Weight", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimplePerItem, Weight), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_Weight_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_Weight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_ItemALength_MetaData[] = {
		{ "Comment", "/** \n\x09 * The length of the first item.\n\x09 * If set to 0.0 it will be determined by the hierarchy\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The length of the first item.\nIf set to 0.0 it will be determined by the hierarchy" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_ItemALength = { "ItemALength", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimplePerItem, ItemALength), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_ItemALength_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_ItemALength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_ItemBLength_MetaData[] = {
		{ "Comment", "/** \n\x09 * The length of the second item.\n\x09 * If set to 0.0 it will be determined by the hierarchy\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The length of the second item.\nIf set to 0.0 it will be determined by the hierarchy" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_ItemBLength = { "ItemBLength", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimplePerItem, ItemBLength), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_ItemBLength_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_ItemBLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_bPropagateToChildren_MetaData[] = {
		{ "Comment", "/**\n\x09 * If set to true all of the global transforms of the children\n\x09 * of this bone will be recalculated based on their local transforms.\n\x09 * Note: This is computationally more expensive than turning it off.\n\x09 */" },
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "If set to true all of the global transforms of the children\nof this bone will be recalculated based on their local transforms.\nNote: This is computationally more expensive than turning it off." },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_bPropagateToChildren_SetBit(void* Obj)
	{
		((FRigUnit_TwoBoneIKSimplePerItem*)Obj)->bPropagateToChildren = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_bPropagateToChildren = { "bPropagateToChildren", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_TwoBoneIKSimplePerItem), &Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_bPropagateToChildren_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_bPropagateToChildren_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_bPropagateToChildren_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_DebugSettings_MetaData[] = {
		{ "Comment", "/** \n\x09 * The settings for debug drawing\n\x09 */" },
		{ "DetailsOnly", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The settings for debug drawing" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_DebugSettings = { "DebugSettings", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimplePerItem, DebugSettings), Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_DebugSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_DebugSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_DebugSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_CachedItemAIndex_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_CachedItemAIndex = { "CachedItemAIndex", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimplePerItem, CachedItemAIndex), Z_Construct_UScriptStruct_FCachedRigElement, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_CachedItemAIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_CachedItemAIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_CachedItemBIndex_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_CachedItemBIndex = { "CachedItemBIndex", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimplePerItem, CachedItemBIndex), Z_Construct_UScriptStruct_FCachedRigElement, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_CachedItemBIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_CachedItemBIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_CachedEffectorItemIndex_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_CachedEffectorItemIndex = { "CachedEffectorItemIndex", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimplePerItem, CachedEffectorItemIndex), Z_Construct_UScriptStruct_FCachedRigElement, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_CachedEffectorItemIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_CachedEffectorItemIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_CachedPoleVectorSpaceIndex_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_CachedPoleVectorSpaceIndex = { "CachedPoleVectorSpaceIndex", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimplePerItem, CachedPoleVectorSpaceIndex), Z_Construct_UScriptStruct_FCachedRigElement, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_CachedPoleVectorSpaceIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_CachedPoleVectorSpaceIndex_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_ItemA,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_ItemB,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_EffectorItem,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_Effector,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_PrimaryAxis,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_SecondaryAxis,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_SecondaryAxisWeight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_PoleVector,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_PoleVectorKind_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_PoleVectorKind,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_PoleVectorSpace,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_bEnableStretch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_StretchStartRatio,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_StretchMaximumRatio,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_Weight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_ItemALength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_ItemBLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_bPropagateToChildren,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_DebugSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_CachedItemAIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_CachedItemBIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_CachedEffectorItemIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::NewProp_CachedPoleVectorSpaceIndex,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_HighlevelBaseMutable,
		&NewStructOps,
		"RigUnit_TwoBoneIKSimplePerItem",
		sizeof(FRigUnit_TwoBoneIKSimplePerItem),
		alignof(FRigUnit_TwoBoneIKSimplePerItem),
		Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_TwoBoneIKSimplePerItem"), sizeof(FRigUnit_TwoBoneIKSimplePerItem), Get_Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimplePerItem_Hash() { return 2819427735U; }

void FRigUnit_TwoBoneIKSimplePerItem::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		ItemA,
		ItemB,
		EffectorItem,
		Effector,
		PrimaryAxis,
		SecondaryAxis,
		SecondaryAxisWeight,
		PoleVector,
		PoleVectorKind,
		PoleVectorSpace,
		bEnableStretch,
		StretchStartRatio,
		StretchMaximumRatio,
		Weight,
		ItemALength,
		ItemBLength,
		bPropagateToChildren,
		DebugSettings,
		CachedItemAIndex,
		CachedItemBIndex,
		CachedEffectorItemIndex,
		CachedPoleVectorSpaceIndex,
		ExecuteContext,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_TwoBoneIKSimple>() == std::is_polymorphic<FRigUnit_HighlevelBaseMutable>(), "USTRUCT FRigUnit_TwoBoneIKSimple cannot be polymorphic unless super FRigUnit_HighlevelBaseMutable is polymorphic");

class UScriptStruct* FRigUnit_TwoBoneIKSimple::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_TwoBoneIKSimple"), sizeof(FRigUnit_TwoBoneIKSimple), Get_Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_TwoBoneIKSimple::Execute"), &FRigUnit_TwoBoneIKSimple::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_TwoBoneIKSimple>()
{
	return FRigUnit_TwoBoneIKSimple::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_TwoBoneIKSimple(FRigUnit_TwoBoneIKSimple::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_TwoBoneIKSimple"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_TwoBoneIKSimple
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_TwoBoneIKSimple()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_TwoBoneIKSimple>(FName(TEXT("RigUnit_TwoBoneIKSimple")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_TwoBoneIKSimple;
	struct Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoneA_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_BoneA;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoneB_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_BoneB;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EffectorBone_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_EffectorBone;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Effector_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Effector;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PrimaryAxis_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PrimaryAxis;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SecondaryAxis_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SecondaryAxis;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SecondaryAxisWeight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SecondaryAxisWeight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PoleVector_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PoleVector;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_PoleVectorKind_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PoleVectorKind_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_PoleVectorKind;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PoleVectorSpace_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_PoleVectorSpace;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableStretch_MetaData[];
#endif
		static void NewProp_bEnableStretch_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableStretch;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StretchStartRatio_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_StretchStartRatio;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StretchMaximumRatio_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_StretchMaximumRatio;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Weight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Weight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoneALength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BoneALength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoneBLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BoneBLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPropagateToChildren_MetaData[];
#endif
		static void NewProp_bPropagateToChildren_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPropagateToChildren;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DebugSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DebugSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedBoneAIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CachedBoneAIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedBoneBIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CachedBoneBIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedEffectorBoneIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CachedEffectorBoneIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedPoleVectorSpaceIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CachedPoleVectorSpaceIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "/**\n * Solves the two bone IK given two bones.\n * Note: This node operates in world space!\n */" },
		{ "Deprecated", "4.25" },
		{ "DisplayName", "Basic IK" },
		{ "Keywords", "TwoBone,IK" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "Solves the two bone IK given two bones.\nNote: This node operates in world space!" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_TwoBoneIKSimple>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_BoneA_MetaData[] = {
		{ "Comment", "/** \n\x09 * The name of first bone\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The name of first bone" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_BoneA = { "BoneA", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimple, BoneA), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_BoneA_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_BoneA_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_BoneB_MetaData[] = {
		{ "Comment", "/** \n\x09 * The name of second bone\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The name of second bone" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_BoneB = { "BoneB", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimple, BoneB), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_BoneB_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_BoneB_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_EffectorBone_MetaData[] = {
		{ "Comment", "/** \n\x09 * The name of the effector bone (if exists)\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The name of the effector bone (if exists)" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_EffectorBone = { "EffectorBone", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimple, EffectorBone), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_EffectorBone_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_EffectorBone_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_Effector_MetaData[] = {
		{ "Comment", "/** \n\x09 * The transform of the effector\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The transform of the effector" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_Effector = { "Effector", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimple, Effector), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_Effector_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_Effector_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_PrimaryAxis_MetaData[] = {
		{ "Comment", "/** \n\x09 * The major axis being aligned - along the bone\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The major axis being aligned - along the bone" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_PrimaryAxis = { "PrimaryAxis", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimple, PrimaryAxis), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_PrimaryAxis_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_PrimaryAxis_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_SecondaryAxis_MetaData[] = {
		{ "Comment", "/** \n\x09 * The minor axis being aligned - towards the polevector\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The minor axis being aligned - towards the polevector" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_SecondaryAxis = { "SecondaryAxis", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimple, SecondaryAxis), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_SecondaryAxis_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_SecondaryAxis_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_SecondaryAxisWeight_MetaData[] = {
		{ "Comment", "/**\n\x09 * Determines how much the secondary axis roll is being applied\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "Determines how much the secondary axis roll is being applied" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_SecondaryAxisWeight = { "SecondaryAxisWeight", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimple, SecondaryAxisWeight), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_SecondaryAxisWeight_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_SecondaryAxisWeight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_PoleVector_MetaData[] = {
		{ "Comment", "/** \n\x09 * The polevector to use for the IK solver\n\x09 * This can be a location or direction.\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The polevector to use for the IK solver\nThis can be a location or direction." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_PoleVector = { "PoleVector", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimple, PoleVector), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_PoleVector_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_PoleVector_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_PoleVectorKind_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_PoleVectorKind_MetaData[] = {
		{ "Comment", "/**\n\x09 * The kind of pole vector this is representing - can be a direction or a location\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The kind of pole vector this is representing - can be a direction or a location" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_PoleVectorKind = { "PoleVectorKind", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimple, PoleVectorKind), Z_Construct_UEnum_ControlRig_EControlRigVectorKind, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_PoleVectorKind_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_PoleVectorKind_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_PoleVectorSpace_MetaData[] = {
		{ "Comment", "/** \n\x09 * The space in which the pole vector is expressed\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The space in which the pole vector is expressed" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_PoleVectorSpace = { "PoleVectorSpace", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimple, PoleVectorSpace), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_PoleVectorSpace_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_PoleVectorSpace_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_bEnableStretch_MetaData[] = {
		{ "Comment", "/**\n\x09 * If set to true the stretch feature of the solver will be enabled\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "If set to true the stretch feature of the solver will be enabled" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_bEnableStretch_SetBit(void* Obj)
	{
		((FRigUnit_TwoBoneIKSimple*)Obj)->bEnableStretch = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_bEnableStretch = { "bEnableStretch", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_TwoBoneIKSimple), &Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_bEnableStretch_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_bEnableStretch_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_bEnableStretch_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_StretchStartRatio_MetaData[] = {
		{ "Comment", "/**\n\x09 * The ratio where the stretch starts\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The ratio where the stretch starts" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_StretchStartRatio = { "StretchStartRatio", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimple, StretchStartRatio), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_StretchStartRatio_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_StretchStartRatio_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_StretchMaximumRatio_MetaData[] = {
		{ "Comment", "/**\n     * The maximum allowed stretch ratio\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The maximum allowed stretch ratio" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_StretchMaximumRatio = { "StretchMaximumRatio", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimple, StretchMaximumRatio), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_StretchMaximumRatio_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_StretchMaximumRatio_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_Weight_MetaData[] = {
		{ "Comment", "/** \n\x09 * The weight of the solver - how much the IK should be applied.\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The weight of the solver - how much the IK should be applied." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_Weight = { "Weight", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimple, Weight), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_Weight_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_Weight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_BoneALength_MetaData[] = {
		{ "Comment", "/** \n\x09 * The length of the first bone.\n\x09 * If set to 0.0 it will be determined by the hierarchy\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The length of the first bone.\nIf set to 0.0 it will be determined by the hierarchy" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_BoneALength = { "BoneALength", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimple, BoneALength), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_BoneALength_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_BoneALength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_BoneBLength_MetaData[] = {
		{ "Comment", "/** \n\x09 * The length of the second  bone.\n\x09 * If set to 0.0 it will be determined by the hierarchy\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The length of the second  bone.\nIf set to 0.0 it will be determined by the hierarchy" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_BoneBLength = { "BoneBLength", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimple, BoneBLength), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_BoneBLength_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_BoneBLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_bPropagateToChildren_MetaData[] = {
		{ "Comment", "/**\n\x09 * If set to true all of the global transforms of the children\n\x09 * of this bone will be recalculated based on their local transforms.\n\x09 * Note: This is computationally more expensive than turning it off.\n\x09 */" },
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "If set to true all of the global transforms of the children\nof this bone will be recalculated based on their local transforms.\nNote: This is computationally more expensive than turning it off." },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_bPropagateToChildren_SetBit(void* Obj)
	{
		((FRigUnit_TwoBoneIKSimple*)Obj)->bPropagateToChildren = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_bPropagateToChildren = { "bPropagateToChildren", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_TwoBoneIKSimple), &Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_bPropagateToChildren_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_bPropagateToChildren_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_bPropagateToChildren_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_DebugSettings_MetaData[] = {
		{ "Comment", "/** \n\x09 * The settings for debug drawing\n\x09 */" },
		{ "DetailsOnly", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The settings for debug drawing" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_DebugSettings = { "DebugSettings", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimple, DebugSettings), Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_DebugSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_DebugSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_DebugSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_CachedBoneAIndex_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_CachedBoneAIndex = { "CachedBoneAIndex", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimple, CachedBoneAIndex), Z_Construct_UScriptStruct_FCachedRigElement, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_CachedBoneAIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_CachedBoneAIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_CachedBoneBIndex_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_CachedBoneBIndex = { "CachedBoneBIndex", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimple, CachedBoneBIndex), Z_Construct_UScriptStruct_FCachedRigElement, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_CachedBoneBIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_CachedBoneBIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_CachedEffectorBoneIndex_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_CachedEffectorBoneIndex = { "CachedEffectorBoneIndex", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimple, CachedEffectorBoneIndex), Z_Construct_UScriptStruct_FCachedRigElement, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_CachedEffectorBoneIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_CachedEffectorBoneIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_CachedPoleVectorSpaceIndex_MetaData[] = {
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_CachedPoleVectorSpaceIndex = { "CachedPoleVectorSpaceIndex", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimple, CachedPoleVectorSpaceIndex), Z_Construct_UScriptStruct_FCachedRigElement, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_CachedPoleVectorSpaceIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_CachedPoleVectorSpaceIndex_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_BoneA,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_BoneB,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_EffectorBone,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_Effector,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_PrimaryAxis,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_SecondaryAxis,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_SecondaryAxisWeight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_PoleVector,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_PoleVectorKind_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_PoleVectorKind,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_PoleVectorSpace,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_bEnableStretch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_StretchStartRatio,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_StretchMaximumRatio,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_Weight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_BoneALength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_BoneBLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_bPropagateToChildren,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_DebugSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_CachedBoneAIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_CachedBoneBIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_CachedEffectorBoneIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::NewProp_CachedPoleVectorSpaceIndex,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_HighlevelBaseMutable,
		&NewStructOps,
		"RigUnit_TwoBoneIKSimple",
		sizeof(FRigUnit_TwoBoneIKSimple),
		alignof(FRigUnit_TwoBoneIKSimple),
		Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_TwoBoneIKSimple"), sizeof(FRigUnit_TwoBoneIKSimple), Get_Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_Hash() { return 2798513249U; }

void FRigUnit_TwoBoneIKSimple::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		BoneA,
		BoneB,
		EffectorBone,
		Effector,
		PrimaryAxis,
		SecondaryAxis,
		SecondaryAxisWeight,
		PoleVector,
		PoleVectorKind,
		PoleVectorSpace,
		bEnableStretch,
		StretchStartRatio,
		StretchMaximumRatio,
		Weight,
		BoneALength,
		BoneBLength,
		bPropagateToChildren,
		DebugSettings,
		CachedBoneAIndex,
		CachedBoneBIndex,
		CachedEffectorBoneIndex,
		CachedPoleVectorSpaceIndex,
		ExecuteContext,
		Context
	);
}

class UScriptStruct* FRigUnit_TwoBoneIKSimple_DebugSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_DebugSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_DebugSettings, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_TwoBoneIKSimple_DebugSettings"), sizeof(FRigUnit_TwoBoneIKSimple_DebugSettings), Get_Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_DebugSettings_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_TwoBoneIKSimple_DebugSettings>()
{
	return FRigUnit_TwoBoneIKSimple_DebugSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_TwoBoneIKSimple_DebugSettings(FRigUnit_TwoBoneIKSimple_DebugSettings::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_TwoBoneIKSimple_DebugSettings"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_TwoBoneIKSimple_DebugSettings
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_TwoBoneIKSimple_DebugSettings()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_TwoBoneIKSimple_DebugSettings>(FName(TEXT("RigUnit_TwoBoneIKSimple_DebugSettings")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_TwoBoneIKSimple_DebugSettings;
	struct Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_DebugSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnabled_MetaData[];
#endif
		static void NewProp_bEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Scale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Scale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorldOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WorldOffset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_DebugSettings_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_DebugSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_TwoBoneIKSimple_DebugSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_DebugSettings_Statics::NewProp_bEnabled_MetaData[] = {
		{ "Category", "DebugSettings" },
		{ "Comment", "/**\n\x09 * If enabled debug information will be drawn \n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "If enabled debug information will be drawn" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_DebugSettings_Statics::NewProp_bEnabled_SetBit(void* Obj)
	{
		((FRigUnit_TwoBoneIKSimple_DebugSettings*)Obj)->bEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_DebugSettings_Statics::NewProp_bEnabled = { "bEnabled", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_TwoBoneIKSimple_DebugSettings), &Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_DebugSettings_Statics::NewProp_bEnabled_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_DebugSettings_Statics::NewProp_bEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_DebugSettings_Statics::NewProp_bEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_DebugSettings_Statics::NewProp_Scale_MetaData[] = {
		{ "Category", "DebugSettings" },
		{ "Comment", "/**\n\x09 * The size of the debug drawing information\n\x09 */" },
		{ "EditCondition", "bEnabled" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The size of the debug drawing information" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_DebugSettings_Statics::NewProp_Scale = { "Scale", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimple_DebugSettings, Scale), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_DebugSettings_Statics::NewProp_Scale_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_DebugSettings_Statics::NewProp_Scale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_DebugSettings_Statics::NewProp_WorldOffset_MetaData[] = {
		{ "Category", "DebugSettings" },
		{ "Comment", "/**\n\x09 * The offset at which to draw the debug information in the world\n\x09 */" },
		{ "EditCondition", "bEnabled" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_TwoBoneIKSimple.h" },
		{ "ToolTip", "The offset at which to draw the debug information in the world" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_DebugSettings_Statics::NewProp_WorldOffset = { "WorldOffset", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_TwoBoneIKSimple_DebugSettings, WorldOffset), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_DebugSettings_Statics::NewProp_WorldOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_DebugSettings_Statics::NewProp_WorldOffset_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_DebugSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_DebugSettings_Statics::NewProp_bEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_DebugSettings_Statics::NewProp_Scale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_DebugSettings_Statics::NewProp_WorldOffset,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_DebugSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"RigUnit_TwoBoneIKSimple_DebugSettings",
		sizeof(FRigUnit_TwoBoneIKSimple_DebugSettings),
		alignof(FRigUnit_TwoBoneIKSimple_DebugSettings),
		Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_DebugSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_DebugSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_DebugSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_DebugSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_DebugSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_DebugSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_TwoBoneIKSimple_DebugSettings"), sizeof(FRigUnit_TwoBoneIKSimple_DebugSettings), Get_Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_DebugSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_DebugSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_TwoBoneIKSimple_DebugSettings_Hash() { return 3864752535U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
