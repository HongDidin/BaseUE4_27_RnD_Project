// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Highlevel/Hierarchy/RigUnit_ModifyBoneTransforms.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_ModifyBoneTransforms() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_HighlevelBaseMutable();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_PerBone();
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_EControlRigModifyBoneMode();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_WorkData();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ModifyTransforms_WorkData();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_ModifyBoneTransforms>() == std::is_polymorphic<FRigUnit_HighlevelBaseMutable>(), "USTRUCT FRigUnit_ModifyBoneTransforms cannot be polymorphic unless super FRigUnit_HighlevelBaseMutable is polymorphic");

class UScriptStruct* FRigUnit_ModifyBoneTransforms::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_ModifyBoneTransforms"), sizeof(FRigUnit_ModifyBoneTransforms), Get_Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_ModifyBoneTransforms::Execute"), &FRigUnit_ModifyBoneTransforms::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_ModifyBoneTransforms>()
{
	return FRigUnit_ModifyBoneTransforms::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_ModifyBoneTransforms(FRigUnit_ModifyBoneTransforms::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_ModifyBoneTransforms"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ModifyBoneTransforms
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ModifyBoneTransforms()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_ModifyBoneTransforms>(FName(TEXT("RigUnit_ModifyBoneTransforms")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ModifyBoneTransforms;
	struct Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BoneToModify_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoneToModify_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_BoneToModify;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Weight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Weight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WeightMinimum_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_WeightMinimum;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WeightMaximum_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_WeightMaximum;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Mode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Mode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Mode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorkData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WorkData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "/**\n * ModifyBonetransforms is used to perform a change in the hierarchy by setting one or more bones' transforms.\n */" },
		{ "Deprecated", "4.25" },
		{ "DisplayName", "Modify Transforms" },
		{ "DocumentationPolicy", "Strict" },
		{ "Keywords", "ModifyBone" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_ModifyBoneTransforms.h" },
		{ "ToolTip", "ModifyBonetransforms is used to perform a change in the hierarchy by setting one or more bones' transforms." },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_ModifyBoneTransforms>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::NewProp_BoneToModify_Inner = { "BoneToModify", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_PerBone, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::NewProp_BoneToModify_MetaData[] = {
		{ "Comment", "/**\n\x09 * The bones to modify.\n\x09 */" },
		{ "DefaultArraySize", "1" },
		{ "ExpandByDefault", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_ModifyBoneTransforms.h" },
		{ "ToolTip", "The bones to modify." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::NewProp_BoneToModify = { "BoneToModify", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ModifyBoneTransforms, BoneToModify), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::NewProp_BoneToModify_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::NewProp_BoneToModify_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::NewProp_Weight_MetaData[] = {
		{ "ClampMax", "1.000000" },
		{ "ClampMin", "0.000000" },
		{ "Comment", "/**\n\x09 * At 1 this sets the transform, between 0 and 1 the transform is blended with previous results.\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_ModifyBoneTransforms.h" },
		{ "ToolTip", "At 1 this sets the transform, between 0 and 1 the transform is blended with previous results." },
		{ "UIMax", "1.000000" },
		{ "UIMin", "0.000000" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::NewProp_Weight = { "Weight", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ModifyBoneTransforms, Weight), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::NewProp_Weight_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::NewProp_Weight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::NewProp_WeightMinimum_MetaData[] = {
		{ "ClampMax", "1.000000" },
		{ "ClampMin", "0.000000" },
		{ "Comment", "/**\n\x09 * The minimum of the weight - defaults to 0.0\n\x09 */" },
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_ModifyBoneTransforms.h" },
		{ "ToolTip", "The minimum of the weight - defaults to 0.0" },
		{ "UIMax", "1.000000" },
		{ "UIMin", "0.000000" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::NewProp_WeightMinimum = { "WeightMinimum", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ModifyBoneTransforms, WeightMinimum), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::NewProp_WeightMinimum_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::NewProp_WeightMinimum_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::NewProp_WeightMaximum_MetaData[] = {
		{ "ClampMax", "1.000000" },
		{ "ClampMin", "0.000000" },
		{ "Comment", "/**\n\x09 * The maximum of the weight - defaults to 1.0\n\x09 */" },
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_ModifyBoneTransforms.h" },
		{ "ToolTip", "The maximum of the weight - defaults to 1.0" },
		{ "UIMax", "1.000000" },
		{ "UIMin", "0.000000" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::NewProp_WeightMaximum = { "WeightMaximum", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ModifyBoneTransforms, WeightMaximum), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::NewProp_WeightMaximum_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::NewProp_WeightMaximum_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::NewProp_Mode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::NewProp_Mode_MetaData[] = {
		{ "Comment", "/**\n\x09 * Defines if the bone's transform should be set\n\x09 * in local or global space, additive or override.\n\x09 */" },
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_ModifyBoneTransforms.h" },
		{ "ToolTip", "Defines if the bone's transform should be set\nin local or global space, additive or override." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::NewProp_Mode = { "Mode", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ModifyBoneTransforms, Mode), Z_Construct_UEnum_ControlRig_EControlRigModifyBoneMode, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::NewProp_Mode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::NewProp_Mode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::NewProp_WorkData_MetaData[] = {
		{ "Comment", "// Used to cache the internally used bone index\n" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_ModifyBoneTransforms.h" },
		{ "ToolTip", "Used to cache the internally used bone index" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::NewProp_WorkData = { "WorkData", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ModifyBoneTransforms, WorkData), Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_WorkData, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::NewProp_WorkData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::NewProp_WorkData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::NewProp_BoneToModify_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::NewProp_BoneToModify,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::NewProp_Weight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::NewProp_WeightMinimum,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::NewProp_WeightMaximum,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::NewProp_Mode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::NewProp_Mode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::NewProp_WorkData,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_HighlevelBaseMutable,
		&NewStructOps,
		"RigUnit_ModifyBoneTransforms",
		sizeof(FRigUnit_ModifyBoneTransforms),
		alignof(FRigUnit_ModifyBoneTransforms),
		Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_ModifyBoneTransforms"), sizeof(FRigUnit_ModifyBoneTransforms), Get_Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_Hash() { return 3250035603U; }

void FRigUnit_ModifyBoneTransforms::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
	FRigVMFixedArray<FRigUnit_ModifyBoneTransforms_PerBone> BoneToModify_0_Array(BoneToModify);
	
    StaticExecute(
		RigVMExecuteContext,
		BoneToModify_0_Array,
		Weight,
		WeightMinimum,
		WeightMaximum,
		Mode,
		WorkData,
		ExecuteContext,
		Context
	);
}


static_assert(std::is_polymorphic<FRigUnit_ModifyBoneTransforms_WorkData>() == std::is_polymorphic<FRigUnit_ModifyTransforms_WorkData>(), "USTRUCT FRigUnit_ModifyBoneTransforms_WorkData cannot be polymorphic unless super FRigUnit_ModifyTransforms_WorkData is polymorphic");

class UScriptStruct* FRigUnit_ModifyBoneTransforms_WorkData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_WorkData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_WorkData, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_ModifyBoneTransforms_WorkData"), sizeof(FRigUnit_ModifyBoneTransforms_WorkData), Get_Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_WorkData_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_ModifyBoneTransforms_WorkData>()
{
	return FRigUnit_ModifyBoneTransforms_WorkData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_ModifyBoneTransforms_WorkData(FRigUnit_ModifyBoneTransforms_WorkData::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_ModifyBoneTransforms_WorkData"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ModifyBoneTransforms_WorkData
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ModifyBoneTransforms_WorkData()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_ModifyBoneTransforms_WorkData>(FName(TEXT("RigUnit_ModifyBoneTransforms_WorkData")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ModifyBoneTransforms_WorkData;
	struct Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_WorkData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_WorkData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_ModifyBoneTransforms.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_WorkData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_ModifyBoneTransforms_WorkData>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_WorkData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit_ModifyTransforms_WorkData,
		&NewStructOps,
		"RigUnit_ModifyBoneTransforms_WorkData",
		sizeof(FRigUnit_ModifyBoneTransforms_WorkData),
		alignof(FRigUnit_ModifyBoneTransforms_WorkData),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_WorkData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_WorkData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_WorkData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_WorkData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_ModifyBoneTransforms_WorkData"), sizeof(FRigUnit_ModifyBoneTransforms_WorkData), Get_Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_WorkData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_WorkData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_WorkData_Hash() { return 3377392661U; }
class UScriptStruct* FRigUnit_ModifyBoneTransforms_PerBone::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_PerBone_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_PerBone, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_ModifyBoneTransforms_PerBone"), sizeof(FRigUnit_ModifyBoneTransforms_PerBone), Get_Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_PerBone_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_ModifyBoneTransforms_PerBone>()
{
	return FRigUnit_ModifyBoneTransforms_PerBone::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_ModifyBoneTransforms_PerBone(FRigUnit_ModifyBoneTransforms_PerBone::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_ModifyBoneTransforms_PerBone"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ModifyBoneTransforms_PerBone
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ModifyBoneTransforms_PerBone()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_ModifyBoneTransforms_PerBone>(FName(TEXT("RigUnit_ModifyBoneTransforms_PerBone")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_ModifyBoneTransforms_PerBone;
	struct Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_PerBone_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Bone_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Bone;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Transform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Transform;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_PerBone_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_ModifyBoneTransforms.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_PerBone_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_ModifyBoneTransforms_PerBone>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_PerBone_Statics::NewProp_Bone_MetaData[] = {
		{ "Category", "FRigUnit_ModifyBoneTransforms_PerBone" },
		{ "Comment", "/**\n\x09 * The name of the Bone to set the transform for.\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_ModifyBoneTransforms.h" },
		{ "ToolTip", "The name of the Bone to set the transform for." },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_PerBone_Statics::NewProp_Bone = { "Bone", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ModifyBoneTransforms_PerBone, Bone), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_PerBone_Statics::NewProp_Bone_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_PerBone_Statics::NewProp_Bone_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_PerBone_Statics::NewProp_Transform_MetaData[] = {
		{ "Category", "FRigUnit_ModifyBoneTransforms_PerBone" },
		{ "Comment", "/**\n\x09 * The transform value to set for the given Bone.\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Highlevel/Hierarchy/RigUnit_ModifyBoneTransforms.h" },
		{ "ToolTip", "The transform value to set for the given Bone." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_PerBone_Statics::NewProp_Transform = { "Transform", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_ModifyBoneTransforms_PerBone, Transform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_PerBone_Statics::NewProp_Transform_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_PerBone_Statics::NewProp_Transform_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_PerBone_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_PerBone_Statics::NewProp_Bone,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_PerBone_Statics::NewProp_Transform,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_PerBone_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"RigUnit_ModifyBoneTransforms_PerBone",
		sizeof(FRigUnit_ModifyBoneTransforms_PerBone),
		alignof(FRigUnit_ModifyBoneTransforms_PerBone),
		Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_PerBone_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_PerBone_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_PerBone_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_PerBone_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_PerBone()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_PerBone_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_ModifyBoneTransforms_PerBone"), sizeof(FRigUnit_ModifyBoneTransforms_PerBone), Get_Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_PerBone_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_PerBone_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_ModifyBoneTransforms_PerBone_Hash() { return 523613869U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
