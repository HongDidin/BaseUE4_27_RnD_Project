// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigControlHierarchy_generated_h
#error "RigControlHierarchy.generated.h already included, missing '#pragma once' in RigControlHierarchy.h"
#endif
#define CONTROLRIG_RigControlHierarchy_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_RigControlHierarchy_h_460_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigControlHierarchy_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__Controls() { return STRUCT_OFFSET(FRigControlHierarchy, Controls); } \
	FORCEINLINE static uint32 __PPO__NameToIndexMapping() { return STRUCT_OFFSET(FRigControlHierarchy, NameToIndexMapping); } \
	FORCEINLINE static uint32 __PPO__Selection() { return STRUCT_OFFSET(FRigControlHierarchy, Selection); }


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigControlHierarchy>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_RigControlHierarchy_h_204_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigControl_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FRigElement Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigControl>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_RigControlHierarchy_h_113_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigControlValue_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__FloatStorage() { return STRUCT_OFFSET(FRigControlValue, FloatStorage); } \
	FORCEINLINE static uint32 __PPO__Storage_DEPRECATED() { return STRUCT_OFFSET(FRigControlValue, Storage_DEPRECATED); }


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigControlValue>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_RigControlHierarchy_h_51_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigControlValueStorage_Statics; \
	static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigControlValueStorage>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_RigControlHierarchy_h


#define FOREACH_ENUM_ERIGCONTROLAXIS(op) \
	op(ERigControlAxis::X) \
	op(ERigControlAxis::Y) \
	op(ERigControlAxis::Z) 

enum class ERigControlAxis : uint8;
template<> CONTROLRIG_API UEnum* StaticEnum<ERigControlAxis>();

#define FOREACH_ENUM_ERIGCONTROLVALUETYPE(op) \
	op(ERigControlValueType::Initial) \
	op(ERigControlValueType::Current) \
	op(ERigControlValueType::Minimum) \
	op(ERigControlValueType::Maximum) 

enum class ERigControlValueType : uint8;
template<> CONTROLRIG_API UEnum* StaticEnum<ERigControlValueType>();

#define FOREACH_ENUM_ERIGCONTROLTYPE(op) \
	op(ERigControlType::Bool) \
	op(ERigControlType::Float) \
	op(ERigControlType::Integer) \
	op(ERigControlType::Vector2D) \
	op(ERigControlType::Position) \
	op(ERigControlType::Scale) \
	op(ERigControlType::Rotator) \
	op(ERigControlType::Transform) \
	op(ERigControlType::TransformNoScale) \
	op(ERigControlType::EulerTransform) 

enum class ERigControlType : uint8;
template<> CONTROLRIG_API UEnum* StaticEnum<ERigControlType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
