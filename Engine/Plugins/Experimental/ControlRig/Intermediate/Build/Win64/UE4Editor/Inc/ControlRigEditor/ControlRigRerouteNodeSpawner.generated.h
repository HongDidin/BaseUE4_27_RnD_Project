// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIGEDITOR_ControlRigRerouteNodeSpawner_generated_h
#error "ControlRigRerouteNodeSpawner.generated.h already included, missing '#pragma once' in ControlRigRerouteNodeSpawner.h"
#endif
#define CONTROLRIGEDITOR_ControlRigRerouteNodeSpawner_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigRerouteNodeSpawner_h_22_SPARSE_DATA
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigRerouteNodeSpawner_h_22_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigRerouteNodeSpawner_h_22_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigRerouteNodeSpawner_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUControlRigRerouteNodeSpawner(); \
	friend struct Z_Construct_UClass_UControlRigRerouteNodeSpawner_Statics; \
public: \
	DECLARE_CLASS(UControlRigRerouteNodeSpawner, UBlueprintNodeSpawner, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ControlRigEditor"), NO_API) \
	DECLARE_SERIALIZER(UControlRigRerouteNodeSpawner)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigRerouteNodeSpawner_h_22_INCLASS \
private: \
	static void StaticRegisterNativesUControlRigRerouteNodeSpawner(); \
	friend struct Z_Construct_UClass_UControlRigRerouteNodeSpawner_Statics; \
public: \
	DECLARE_CLASS(UControlRigRerouteNodeSpawner, UBlueprintNodeSpawner, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ControlRigEditor"), NO_API) \
	DECLARE_SERIALIZER(UControlRigRerouteNodeSpawner)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigRerouteNodeSpawner_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UControlRigRerouteNodeSpawner(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigRerouteNodeSpawner) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigRerouteNodeSpawner); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigRerouteNodeSpawner); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigRerouteNodeSpawner(UControlRigRerouteNodeSpawner&&); \
	NO_API UControlRigRerouteNodeSpawner(const UControlRigRerouteNodeSpawner&); \
public:


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigRerouteNodeSpawner_h_22_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UControlRigRerouteNodeSpawner(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigRerouteNodeSpawner(UControlRigRerouteNodeSpawner&&); \
	NO_API UControlRigRerouteNodeSpawner(const UControlRigRerouteNodeSpawner&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigRerouteNodeSpawner); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigRerouteNodeSpawner); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigRerouteNodeSpawner)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigRerouteNodeSpawner_h_22_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigRerouteNodeSpawner_h_19_PROLOG
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigRerouteNodeSpawner_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigRerouteNodeSpawner_h_22_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigRerouteNodeSpawner_h_22_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigRerouteNodeSpawner_h_22_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigRerouteNodeSpawner_h_22_INCLASS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigRerouteNodeSpawner_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigRerouteNodeSpawner_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigRerouteNodeSpawner_h_22_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigRerouteNodeSpawner_h_22_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigRerouteNodeSpawner_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigRerouteNodeSpawner_h_22_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigRerouteNodeSpawner_h_22_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONTROLRIGEDITOR_API UClass* StaticClass<class UControlRigRerouteNodeSpawner>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Graph_NodeSpawners_ControlRigRerouteNodeSpawner_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
