// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Public/Sequencer/ControlRigSequenceObjectReference.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeControlRigSequenceObjectReference() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FControlRigSequenceObjectReferenceMap();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FControlRigSequenceObjectReferences();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FControlRigSequenceObjectReference();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	CONTROLRIG_API UClass* Z_Construct_UClass_UControlRig_NoRegister();
// End Cross Module References
class UScriptStruct* FControlRigSequenceObjectReferenceMap::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FControlRigSequenceObjectReferenceMap_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FControlRigSequenceObjectReferenceMap, Z_Construct_UPackage__Script_ControlRig(), TEXT("ControlRigSequenceObjectReferenceMap"), sizeof(FControlRigSequenceObjectReferenceMap), Get_Z_Construct_UScriptStruct_FControlRigSequenceObjectReferenceMap_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FControlRigSequenceObjectReferenceMap>()
{
	return FControlRigSequenceObjectReferenceMap::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FControlRigSequenceObjectReferenceMap(FControlRigSequenceObjectReferenceMap::StaticStruct, TEXT("/Script/ControlRig"), TEXT("ControlRigSequenceObjectReferenceMap"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFControlRigSequenceObjectReferenceMap
{
	FScriptStruct_ControlRig_StaticRegisterNativesFControlRigSequenceObjectReferenceMap()
	{
		UScriptStruct::DeferCppStructOps<FControlRigSequenceObjectReferenceMap>(FName(TEXT("ControlRigSequenceObjectReferenceMap")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFControlRigSequenceObjectReferenceMap;
	struct Z_Construct_UScriptStruct_FControlRigSequenceObjectReferenceMap_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BindingIds_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BindingIds_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_BindingIds;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_References_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_References_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_References;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FControlRigSequenceObjectReferenceMap_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Sequencer/ControlRigSequenceObjectReference.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FControlRigSequenceObjectReferenceMap_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FControlRigSequenceObjectReferenceMap>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FControlRigSequenceObjectReferenceMap_Statics::NewProp_BindingIds_Inner = { "BindingIds", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FControlRigSequenceObjectReferenceMap_Statics::NewProp_BindingIds_MetaData[] = {
		{ "ModuleRelativePath", "Public/Sequencer/ControlRigSequenceObjectReference.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FControlRigSequenceObjectReferenceMap_Statics::NewProp_BindingIds = { "BindingIds", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FControlRigSequenceObjectReferenceMap, BindingIds), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FControlRigSequenceObjectReferenceMap_Statics::NewProp_BindingIds_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FControlRigSequenceObjectReferenceMap_Statics::NewProp_BindingIds_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FControlRigSequenceObjectReferenceMap_Statics::NewProp_References_Inner = { "References", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FControlRigSequenceObjectReferences, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FControlRigSequenceObjectReferenceMap_Statics::NewProp_References_MetaData[] = {
		{ "ModuleRelativePath", "Public/Sequencer/ControlRigSequenceObjectReference.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FControlRigSequenceObjectReferenceMap_Statics::NewProp_References = { "References", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FControlRigSequenceObjectReferenceMap, References), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FControlRigSequenceObjectReferenceMap_Statics::NewProp_References_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FControlRigSequenceObjectReferenceMap_Statics::NewProp_References_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FControlRigSequenceObjectReferenceMap_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FControlRigSequenceObjectReferenceMap_Statics::NewProp_BindingIds_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FControlRigSequenceObjectReferenceMap_Statics::NewProp_BindingIds,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FControlRigSequenceObjectReferenceMap_Statics::NewProp_References_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FControlRigSequenceObjectReferenceMap_Statics::NewProp_References,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FControlRigSequenceObjectReferenceMap_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"ControlRigSequenceObjectReferenceMap",
		sizeof(FControlRigSequenceObjectReferenceMap),
		alignof(FControlRigSequenceObjectReferenceMap),
		Z_Construct_UScriptStruct_FControlRigSequenceObjectReferenceMap_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FControlRigSequenceObjectReferenceMap_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FControlRigSequenceObjectReferenceMap_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FControlRigSequenceObjectReferenceMap_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FControlRigSequenceObjectReferenceMap()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FControlRigSequenceObjectReferenceMap_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ControlRigSequenceObjectReferenceMap"), sizeof(FControlRigSequenceObjectReferenceMap), Get_Z_Construct_UScriptStruct_FControlRigSequenceObjectReferenceMap_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FControlRigSequenceObjectReferenceMap_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FControlRigSequenceObjectReferenceMap_Hash() { return 3130940891U; }
class UScriptStruct* FControlRigSequenceObjectReferences::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FControlRigSequenceObjectReferences_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FControlRigSequenceObjectReferences, Z_Construct_UPackage__Script_ControlRig(), TEXT("ControlRigSequenceObjectReferences"), sizeof(FControlRigSequenceObjectReferences), Get_Z_Construct_UScriptStruct_FControlRigSequenceObjectReferences_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FControlRigSequenceObjectReferences>()
{
	return FControlRigSequenceObjectReferences::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FControlRigSequenceObjectReferences(FControlRigSequenceObjectReferences::StaticStruct, TEXT("/Script/ControlRig"), TEXT("ControlRigSequenceObjectReferences"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFControlRigSequenceObjectReferences
{
	FScriptStruct_ControlRig_StaticRegisterNativesFControlRigSequenceObjectReferences()
	{
		UScriptStruct::DeferCppStructOps<FControlRigSequenceObjectReferences>(FName(TEXT("ControlRigSequenceObjectReferences")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFControlRigSequenceObjectReferences;
	struct Z_Construct_UScriptStruct_FControlRigSequenceObjectReferences_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Array_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Array_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Array;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FControlRigSequenceObjectReferences_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Sequencer/ControlRigSequenceObjectReference.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FControlRigSequenceObjectReferences_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FControlRigSequenceObjectReferences>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FControlRigSequenceObjectReferences_Statics::NewProp_Array_Inner = { "Array", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FControlRigSequenceObjectReference, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FControlRigSequenceObjectReferences_Statics::NewProp_Array_MetaData[] = {
		{ "ModuleRelativePath", "Public/Sequencer/ControlRigSequenceObjectReference.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FControlRigSequenceObjectReferences_Statics::NewProp_Array = { "Array", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FControlRigSequenceObjectReferences, Array), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FControlRigSequenceObjectReferences_Statics::NewProp_Array_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FControlRigSequenceObjectReferences_Statics::NewProp_Array_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FControlRigSequenceObjectReferences_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FControlRigSequenceObjectReferences_Statics::NewProp_Array_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FControlRigSequenceObjectReferences_Statics::NewProp_Array,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FControlRigSequenceObjectReferences_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"ControlRigSequenceObjectReferences",
		sizeof(FControlRigSequenceObjectReferences),
		alignof(FControlRigSequenceObjectReferences),
		Z_Construct_UScriptStruct_FControlRigSequenceObjectReferences_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FControlRigSequenceObjectReferences_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FControlRigSequenceObjectReferences_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FControlRigSequenceObjectReferences_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FControlRigSequenceObjectReferences()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FControlRigSequenceObjectReferences_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ControlRigSequenceObjectReferences"), sizeof(FControlRigSequenceObjectReferences), Get_Z_Construct_UScriptStruct_FControlRigSequenceObjectReferences_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FControlRigSequenceObjectReferences_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FControlRigSequenceObjectReferences_Hash() { return 476793155U; }
class UScriptStruct* FControlRigSequenceObjectReference::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FControlRigSequenceObjectReference_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FControlRigSequenceObjectReference, Z_Construct_UPackage__Script_ControlRig(), TEXT("ControlRigSequenceObjectReference"), sizeof(FControlRigSequenceObjectReference), Get_Z_Construct_UScriptStruct_FControlRigSequenceObjectReference_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FControlRigSequenceObjectReference>()
{
	return FControlRigSequenceObjectReference::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FControlRigSequenceObjectReference(FControlRigSequenceObjectReference::StaticStruct, TEXT("/Script/ControlRig"), TEXT("ControlRigSequenceObjectReference"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFControlRigSequenceObjectReference
{
	FScriptStruct_ControlRig_StaticRegisterNativesFControlRigSequenceObjectReference()
	{
		UScriptStruct::DeferCppStructOps<FControlRigSequenceObjectReference>(FName(TEXT("ControlRigSequenceObjectReference")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFControlRigSequenceObjectReference;
	struct Z_Construct_UScriptStruct_FControlRigSequenceObjectReference_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ControlRigClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ControlRigClass;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FControlRigSequenceObjectReference_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * An external reference to an level sequence object, resolvable through an arbitrary context.\n */" },
		{ "ModuleRelativePath", "Public/Sequencer/ControlRigSequenceObjectReference.h" },
		{ "ToolTip", "An external reference to an level sequence object, resolvable through an arbitrary context." },
	};
#endif
	void* Z_Construct_UScriptStruct_FControlRigSequenceObjectReference_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FControlRigSequenceObjectReference>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FControlRigSequenceObjectReference_Statics::NewProp_ControlRigClass_MetaData[] = {
		{ "Comment", "/** The type of this animation ControlRig */" },
		{ "ModuleRelativePath", "Public/Sequencer/ControlRigSequenceObjectReference.h" },
		{ "ToolTip", "The type of this animation ControlRig" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UScriptStruct_FControlRigSequenceObjectReference_Statics::NewProp_ControlRigClass = { "ControlRigClass", nullptr, (EPropertyFlags)0x0044000000000000, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FControlRigSequenceObjectReference, ControlRigClass), Z_Construct_UClass_UControlRig_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UScriptStruct_FControlRigSequenceObjectReference_Statics::NewProp_ControlRigClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FControlRigSequenceObjectReference_Statics::NewProp_ControlRigClass_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FControlRigSequenceObjectReference_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FControlRigSequenceObjectReference_Statics::NewProp_ControlRigClass,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FControlRigSequenceObjectReference_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"ControlRigSequenceObjectReference",
		sizeof(FControlRigSequenceObjectReference),
		alignof(FControlRigSequenceObjectReference),
		Z_Construct_UScriptStruct_FControlRigSequenceObjectReference_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FControlRigSequenceObjectReference_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FControlRigSequenceObjectReference_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FControlRigSequenceObjectReference_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FControlRigSequenceObjectReference()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FControlRigSequenceObjectReference_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ControlRigSequenceObjectReference"), sizeof(FControlRigSequenceObjectReference), Get_Z_Construct_UScriptStruct_FControlRigSequenceObjectReference_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FControlRigSequenceObjectReference_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FControlRigSequenceObjectReference_Hash() { return 773255340U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
