// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIGDEVELOPER_ControlRigGraph_generated_h
#error "ControlRigGraph.generated.h already included, missing '#pragma once' in ControlRigGraph.h"
#endif
#define CONTROLRIGDEVELOPER_ControlRigGraph_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraph_h_23_SPARSE_DATA
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraph_h_23_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraph_h_23_RPC_WRAPPERS_NO_PURE_DECLS
#if WITH_EDITORONLY_DATA
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraph_h_23_ARCHIVESERIALIZER \
	DECLARE_FSTRUCTUREDARCHIVE_SERIALIZER(UControlRigGraph, NO_API)


#else
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraph_h_23_ARCHIVESERIALIZER
#endif
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraph_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUControlRigGraph(); \
	friend struct Z_Construct_UClass_UControlRigGraph_Statics; \
public: \
	DECLARE_CLASS(UControlRigGraph, UEdGraph, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRigDeveloper"), NO_API) \
	DECLARE_SERIALIZER(UControlRigGraph) \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraph_h_23_ARCHIVESERIALIZER


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraph_h_23_INCLASS \
private: \
	static void StaticRegisterNativesUControlRigGraph(); \
	friend struct Z_Construct_UClass_UControlRigGraph_Statics; \
public: \
	DECLARE_CLASS(UControlRigGraph, UEdGraph, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRigDeveloper"), NO_API) \
	DECLARE_SERIALIZER(UControlRigGraph) \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraph_h_23_ARCHIVESERIALIZER


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraph_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UControlRigGraph(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigGraph) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigGraph); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigGraph); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigGraph(UControlRigGraph&&); \
	NO_API UControlRigGraph(const UControlRigGraph&); \
public:


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraph_h_23_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigGraph(UControlRigGraph&&); \
	NO_API UControlRigGraph(const UControlRigGraph&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigGraph); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigGraph); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UControlRigGraph)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraph_h_23_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraph_h_20_PROLOG
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraph_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraph_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraph_h_23_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraph_h_23_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraph_h_23_INCLASS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraph_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraph_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraph_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraph_h_23_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraph_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraph_h_23_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraph_h_23_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONTROLRIGDEVELOPER_API UClass* StaticClass<class UControlRigGraph>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraph_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
