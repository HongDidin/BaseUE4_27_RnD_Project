// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_FKControlRig_generated_h
#error "FKControlRig.generated.h already included, missing '#pragma once' in FKControlRig.h"
#endif
#define CONTROLRIG_FKControlRig_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_FKControlRig_h_37_SPARSE_DATA
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_FKControlRig_h_37_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_FKControlRig_h_37_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_FKControlRig_h_37_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFKControlRig(); \
	friend struct Z_Construct_UClass_UFKControlRig_Statics; \
public: \
	DECLARE_CLASS(UFKControlRig, UControlRig, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRig"), NO_API) \
	DECLARE_SERIALIZER(UFKControlRig)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_FKControlRig_h_37_INCLASS \
private: \
	static void StaticRegisterNativesUFKControlRig(); \
	friend struct Z_Construct_UClass_UFKControlRig_Statics; \
public: \
	DECLARE_CLASS(UFKControlRig, UControlRig, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRig"), NO_API) \
	DECLARE_SERIALIZER(UFKControlRig)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_FKControlRig_h_37_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFKControlRig(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFKControlRig) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFKControlRig); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFKControlRig); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFKControlRig(UFKControlRig&&); \
	NO_API UFKControlRig(const UFKControlRig&); \
public:


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_FKControlRig_h_37_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFKControlRig() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFKControlRig(UFKControlRig&&); \
	NO_API UFKControlRig(const UFKControlRig&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFKControlRig); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFKControlRig); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UFKControlRig)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_FKControlRig_h_37_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__IsControlActive() { return STRUCT_OFFSET(UFKControlRig, IsControlActive); } \
	FORCEINLINE static uint32 __PPO__ApplyMode() { return STRUCT_OFFSET(UFKControlRig, ApplyMode); }


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_FKControlRig_h_34_PROLOG
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_FKControlRig_h_37_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_FKControlRig_h_37_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_FKControlRig_h_37_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_FKControlRig_h_37_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_FKControlRig_h_37_INCLASS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_FKControlRig_h_37_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_FKControlRig_h_37_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_FKControlRig_h_37_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_FKControlRig_h_37_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_FKControlRig_h_37_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_FKControlRig_h_37_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_FKControlRig_h_37_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class FKControlRig."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONTROLRIG_API UClass* StaticClass<class UFKControlRig>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_FKControlRig_h


#define FOREACH_ENUM_ECONTROLRIGFKRIGEXECUTEMODE(op) \
	op(EControlRigFKRigExecuteMode::Replace) \
	op(EControlRigFKRigExecuteMode::Additive) \
	op(EControlRigFKRigExecuteMode::Max) 

enum class EControlRigFKRigExecuteMode : uint8;
template<> CONTROLRIG_API UEnum* StaticEnum<EControlRigFKRigExecuteMode>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
