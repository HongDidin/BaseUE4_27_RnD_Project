// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIGEDITOR_AnimSequenceConverterFactory_generated_h
#error "AnimSequenceConverterFactory.generated.h already included, missing '#pragma once' in AnimSequenceConverterFactory.h"
#endif
#define CONTROLRIGEDITOR_AnimSequenceConverterFactory_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_AnimSequenceConverterFactory_h_13_SPARSE_DATA
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_AnimSequenceConverterFactory_h_13_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_AnimSequenceConverterFactory_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_AnimSequenceConverterFactory_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAnimSequenceConverterFactory(); \
	friend struct Z_Construct_UClass_UAnimSequenceConverterFactory_Statics; \
public: \
	DECLARE_CLASS(UAnimSequenceConverterFactory, UAnimSequenceFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRigEditor"), CONTROLRIGEDITOR_API) \
	DECLARE_SERIALIZER(UAnimSequenceConverterFactory)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_AnimSequenceConverterFactory_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUAnimSequenceConverterFactory(); \
	friend struct Z_Construct_UClass_UAnimSequenceConverterFactory_Statics; \
public: \
	DECLARE_CLASS(UAnimSequenceConverterFactory, UAnimSequenceFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRigEditor"), CONTROLRIGEDITOR_API) \
	DECLARE_SERIALIZER(UAnimSequenceConverterFactory)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_AnimSequenceConverterFactory_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	CONTROLRIGEDITOR_API UAnimSequenceConverterFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAnimSequenceConverterFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(CONTROLRIGEDITOR_API, UAnimSequenceConverterFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAnimSequenceConverterFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	CONTROLRIGEDITOR_API UAnimSequenceConverterFactory(UAnimSequenceConverterFactory&&); \
	CONTROLRIGEDITOR_API UAnimSequenceConverterFactory(const UAnimSequenceConverterFactory&); \
public:


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_AnimSequenceConverterFactory_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	CONTROLRIGEDITOR_API UAnimSequenceConverterFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	CONTROLRIGEDITOR_API UAnimSequenceConverterFactory(UAnimSequenceConverterFactory&&); \
	CONTROLRIGEDITOR_API UAnimSequenceConverterFactory(const UAnimSequenceConverterFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(CONTROLRIGEDITOR_API, UAnimSequenceConverterFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAnimSequenceConverterFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAnimSequenceConverterFactory)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_AnimSequenceConverterFactory_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_AnimSequenceConverterFactory_h_10_PROLOG
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_AnimSequenceConverterFactory_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_AnimSequenceConverterFactory_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_AnimSequenceConverterFactory_h_13_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_AnimSequenceConverterFactory_h_13_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_AnimSequenceConverterFactory_h_13_INCLASS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_AnimSequenceConverterFactory_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_AnimSequenceConverterFactory_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_AnimSequenceConverterFactory_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_AnimSequenceConverterFactory_h_13_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_AnimSequenceConverterFactory_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_AnimSequenceConverterFactory_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_AnimSequenceConverterFactory_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONTROLRIGEDITOR_API UClass* StaticClass<class UAnimSequenceConverterFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_AnimSequenceConverterFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
