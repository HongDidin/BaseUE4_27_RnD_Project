// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRigDeveloper/Public/Graph/ControlRigGraph.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeControlRigGraph() {}
// Cross Module References
	CONTROLRIGDEVELOPER_API UClass* Z_Construct_UClass_UControlRigGraph_NoRegister();
	CONTROLRIGDEVELOPER_API UClass* Z_Construct_UClass_UControlRigGraph();
	ENGINE_API UClass* Z_Construct_UClass_UEdGraph();
	UPackage* Z_Construct_UPackage__Script_ControlRigDeveloper();
	RIGVMDEVELOPER_API UClass* Z_Construct_UClass_URigVMGraph_NoRegister();
	RIGVMDEVELOPER_API UClass* Z_Construct_UClass_URigVMController_NoRegister();
// End Cross Module References
	void UControlRigGraph::StaticRegisterNativesUControlRigGraph()
	{
	}
	UClass* Z_Construct_UClass_UControlRigGraph_NoRegister()
	{
		return UControlRigGraph::StaticClass();
	}
	struct Z_Construct_UClass_UControlRigGraph_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TemplateModel_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TemplateModel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TemplateController_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TemplateController;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#endif // WITH_EDITORONLY_DATA
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UControlRigGraph_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEdGraph,
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRigDeveloper,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigGraph_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Graph/ControlRigGraph.h" },
		{ "ModuleRelativePath", "Public/Graph/ControlRigGraph.h" },
	};
#endif
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigGraph_Statics::NewProp_TemplateModel_MetaData[] = {
		{ "ModuleRelativePath", "Public/Graph/ControlRigGraph.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UControlRigGraph_Statics::NewProp_TemplateModel = { "TemplateModel", nullptr, (EPropertyFlags)0x0040000800002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigGraph, TemplateModel), Z_Construct_UClass_URigVMGraph_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UControlRigGraph_Statics::NewProp_TemplateModel_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigGraph_Statics::NewProp_TemplateModel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigGraph_Statics::NewProp_TemplateController_MetaData[] = {
		{ "ModuleRelativePath", "Public/Graph/ControlRigGraph.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UControlRigGraph_Statics::NewProp_TemplateController = { "TemplateController", nullptr, (EPropertyFlags)0x0040000800002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigGraph, TemplateController), Z_Construct_UClass_URigVMController_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UControlRigGraph_Statics::NewProp_TemplateController_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigGraph_Statics::NewProp_TemplateController_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UControlRigGraph_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigGraph_Statics::NewProp_TemplateModel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigGraph_Statics::NewProp_TemplateController,
	};
#endif // WITH_EDITORONLY_DATA
	const FCppClassTypeInfoStatic Z_Construct_UClass_UControlRigGraph_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UControlRigGraph>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UControlRigGraph_Statics::ClassParams = {
		&UControlRigGraph::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		IF_WITH_EDITORONLY_DATA(Z_Construct_UClass_UControlRigGraph_Statics::PropPointers, nullptr),
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		IF_WITH_EDITORONLY_DATA(UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigGraph_Statics::PropPointers), 0),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UControlRigGraph_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigGraph_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UControlRigGraph()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UControlRigGraph_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UControlRigGraph, 1445062946);
	template<> CONTROLRIGDEVELOPER_API UClass* StaticClass<UControlRigGraph>()
	{
		return UControlRigGraph::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UControlRigGraph(Z_Construct_UClass_UControlRigGraph, &UControlRigGraph::StaticClass, TEXT("/Script/ControlRigDeveloper"), TEXT("UControlRigGraph"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UControlRigGraph);
#if WITH_EDITORONLY_DATA
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(UControlRigGraph)
#endif
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
