// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigUnit_GetControlTransform_generated_h
#error "RigUnit_GetControlTransform.generated.h already included, missing '#pragma once' in RigUnit_GetControlTransform.h"
#endif
#define CONTROLRIG_RigUnit_GetControlTransform_generated_h


#define FRigUnit_GetControlTransform_Execute() \
	void FRigUnit_GetControlTransform::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Control, \
		const EBoneGetterSetterMode Space, \
		FTransform& Transform, \
		FTransform& Minimum, \
		FTransform& Maximum, \
		FCachedRigElement& CachedControlIndex, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Hierarchy_RigUnit_GetControlTransform_h_267_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_GetControlTransform_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Control, \
		const EBoneGetterSetterMode Space, \
		FTransform& Transform, \
		FTransform& Minimum, \
		FTransform& Maximum, \
		FCachedRigElement& CachedControlIndex, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FName& Control = *(FName*)RigVMMemoryHandles[0].GetData(); \
		EBoneGetterSetterMode Space = (EBoneGetterSetterMode)*(uint8*)RigVMMemoryHandles[1].GetData(); \
		FTransform& Transform = *(FTransform*)RigVMMemoryHandles[2].GetData(); \
		FTransform& Minimum = *(FTransform*)RigVMMemoryHandles[3].GetData(); \
		FTransform& Maximum = *(FTransform*)RigVMMemoryHandles[4].GetData(); \
		FRigVMDynamicArray<FCachedRigElement> CachedControlIndex_5_Array(*((FRigVMByteArray*)RigVMMemoryHandles[5].GetData(0, false))); \
		CachedControlIndex_5_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedControlIndex = CachedControlIndex_5_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Control, \
			Space, \
			Transform, \
			Minimum, \
			Maximum, \
			CachedControlIndex, \
			Context \
		); \
	} \
	typedef FRigUnit Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_GetControlTransform>();


#define FRigUnit_GetControlRotator_Execute() \
	void FRigUnit_GetControlRotator::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Control, \
		const EBoneGetterSetterMode Space, \
		FRotator& Rotator, \
		FRotator& Minimum, \
		FRotator& Maximum, \
		FCachedRigElement& CachedControlIndex, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Hierarchy_RigUnit_GetControlTransform_h_217_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_GetControlRotator_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Control, \
		const EBoneGetterSetterMode Space, \
		FRotator& Rotator, \
		FRotator& Minimum, \
		FRotator& Maximum, \
		FCachedRigElement& CachedControlIndex, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FName& Control = *(FName*)RigVMMemoryHandles[0].GetData(); \
		EBoneGetterSetterMode Space = (EBoneGetterSetterMode)*(uint8*)RigVMMemoryHandles[1].GetData(); \
		FRotator& Rotator = *(FRotator*)RigVMMemoryHandles[2].GetData(); \
		FRotator& Minimum = *(FRotator*)RigVMMemoryHandles[3].GetData(); \
		FRotator& Maximum = *(FRotator*)RigVMMemoryHandles[4].GetData(); \
		FRigVMDynamicArray<FCachedRigElement> CachedControlIndex_5_Array(*((FRigVMByteArray*)RigVMMemoryHandles[5].GetData(0, false))); \
		CachedControlIndex_5_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedControlIndex = CachedControlIndex_5_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Control, \
			Space, \
			Rotator, \
			Minimum, \
			Maximum, \
			CachedControlIndex, \
			Context \
		); \
	} \
	typedef FRigUnit Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_GetControlRotator>();


#define FRigUnit_GetControlVector_Execute() \
	void FRigUnit_GetControlVector::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Control, \
		const EBoneGetterSetterMode Space, \
		FVector& Vector, \
		FVector& Minimum, \
		FVector& Maximum, \
		FCachedRigElement& CachedControlIndex, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Hierarchy_RigUnit_GetControlTransform_h_168_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_GetControlVector_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Control, \
		const EBoneGetterSetterMode Space, \
		FVector& Vector, \
		FVector& Minimum, \
		FVector& Maximum, \
		FCachedRigElement& CachedControlIndex, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FName& Control = *(FName*)RigVMMemoryHandles[0].GetData(); \
		EBoneGetterSetterMode Space = (EBoneGetterSetterMode)*(uint8*)RigVMMemoryHandles[1].GetData(); \
		FVector& Vector = *(FVector*)RigVMMemoryHandles[2].GetData(); \
		FVector& Minimum = *(FVector*)RigVMMemoryHandles[3].GetData(); \
		FVector& Maximum = *(FVector*)RigVMMemoryHandles[4].GetData(); \
		FRigVMDynamicArray<FCachedRigElement> CachedControlIndex_5_Array(*((FRigVMByteArray*)RigVMMemoryHandles[5].GetData(0, false))); \
		CachedControlIndex_5_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedControlIndex = CachedControlIndex_5_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Control, \
			Space, \
			Vector, \
			Minimum, \
			Maximum, \
			CachedControlIndex, \
			Context \
		); \
	} \
	typedef FRigUnit Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_GetControlVector>();


#define FRigUnit_GetControlVector2D_Execute() \
	void FRigUnit_GetControlVector2D::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Control, \
		FVector2D& Vector, \
		FVector2D& Minimum, \
		FVector2D& Maximum, \
		FCachedRigElement& CachedControlIndex, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Hierarchy_RigUnit_GetControlTransform_h_127_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_GetControlVector2D_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Control, \
		FVector2D& Vector, \
		FVector2D& Minimum, \
		FVector2D& Maximum, \
		FCachedRigElement& CachedControlIndex, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FName& Control = *(FName*)RigVMMemoryHandles[0].GetData(); \
		FVector2D& Vector = *(FVector2D*)RigVMMemoryHandles[1].GetData(); \
		FVector2D& Minimum = *(FVector2D*)RigVMMemoryHandles[2].GetData(); \
		FVector2D& Maximum = *(FVector2D*)RigVMMemoryHandles[3].GetData(); \
		FRigVMDynamicArray<FCachedRigElement> CachedControlIndex_4_Array(*((FRigVMByteArray*)RigVMMemoryHandles[4].GetData(0, false))); \
		CachedControlIndex_4_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedControlIndex = CachedControlIndex_4_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Control, \
			Vector, \
			Minimum, \
			Maximum, \
			CachedControlIndex, \
			Context \
		); \
	} \
	typedef FRigUnit Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_GetControlVector2D>();


#define FRigUnit_GetControlInteger_Execute() \
	void FRigUnit_GetControlInteger::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Control, \
		int32& IntegerValue, \
		int32& Minimum, \
		int32& Maximum, \
		FCachedRigElement& CachedControlIndex, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Hierarchy_RigUnit_GetControlTransform_h_86_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_GetControlInteger_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Control, \
		int32& IntegerValue, \
		int32& Minimum, \
		int32& Maximum, \
		FCachedRigElement& CachedControlIndex, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FName& Control = *(FName*)RigVMMemoryHandles[0].GetData(); \
		int32& IntegerValue = *(int32*)RigVMMemoryHandles[1].GetData(); \
		int32& Minimum = *(int32*)RigVMMemoryHandles[2].GetData(); \
		int32& Maximum = *(int32*)RigVMMemoryHandles[3].GetData(); \
		FRigVMDynamicArray<FCachedRigElement> CachedControlIndex_4_Array(*((FRigVMByteArray*)RigVMMemoryHandles[4].GetData(0, false))); \
		CachedControlIndex_4_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedControlIndex = CachedControlIndex_4_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Control, \
			IntegerValue, \
			Minimum, \
			Maximum, \
			CachedControlIndex, \
			Context \
		); \
	} \
	typedef FRigUnit Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_GetControlInteger>();


#define FRigUnit_GetControlFloat_Execute() \
	void FRigUnit_GetControlFloat::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Control, \
		float& FloatValue, \
		float& Minimum, \
		float& Maximum, \
		FCachedRigElement& CachedControlIndex, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Hierarchy_RigUnit_GetControlTransform_h_45_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_GetControlFloat_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Control, \
		float& FloatValue, \
		float& Minimum, \
		float& Maximum, \
		FCachedRigElement& CachedControlIndex, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FName& Control = *(FName*)RigVMMemoryHandles[0].GetData(); \
		float& FloatValue = *(float*)RigVMMemoryHandles[1].GetData(); \
		float& Minimum = *(float*)RigVMMemoryHandles[2].GetData(); \
		float& Maximum = *(float*)RigVMMemoryHandles[3].GetData(); \
		FRigVMDynamicArray<FCachedRigElement> CachedControlIndex_4_Array(*((FRigVMByteArray*)RigVMMemoryHandles[4].GetData(0, false))); \
		CachedControlIndex_4_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedControlIndex = CachedControlIndex_4_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Control, \
			FloatValue, \
			Minimum, \
			Maximum, \
			CachedControlIndex, \
			Context \
		); \
	} \
	typedef FRigUnit Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_GetControlFloat>();


#define FRigUnit_GetControlBool_Execute() \
	void FRigUnit_GetControlBool::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Control, \
		bool& BoolValue, \
		FCachedRigElement& CachedControlIndex, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Hierarchy_RigUnit_GetControlTransform_h_14_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_GetControlBool_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Control, \
		bool& BoolValue, \
		FCachedRigElement& CachedControlIndex, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FName& Control = *(FName*)RigVMMemoryHandles[0].GetData(); \
		bool& BoolValue = *(bool*)RigVMMemoryHandles[1].GetData(); \
		FRigVMDynamicArray<FCachedRigElement> CachedControlIndex_2_Array(*((FRigVMByteArray*)RigVMMemoryHandles[2].GetData(0, false))); \
		CachedControlIndex_2_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedControlIndex = CachedControlIndex_2_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Control, \
			BoolValue, \
			CachedControlIndex, \
			Context \
		); \
	} \
	typedef FRigUnit Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_GetControlBool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Hierarchy_RigUnit_GetControlTransform_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
