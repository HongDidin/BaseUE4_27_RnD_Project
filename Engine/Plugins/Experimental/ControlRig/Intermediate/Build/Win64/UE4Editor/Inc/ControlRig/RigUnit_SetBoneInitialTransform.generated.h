// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigUnit_SetBoneInitialTransform_generated_h
#error "RigUnit_SetBoneInitialTransform.generated.h already included, missing '#pragma once' in RigUnit_SetBoneInitialTransform.h"
#endif
#define CONTROLRIG_RigUnit_SetBoneInitialTransform_generated_h


#define FRigUnit_SetBoneInitialTransform_Execute() \
	void FRigUnit_SetBoneInitialTransform::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Bone, \
		const FTransform& Transform, \
		FTransform& Result, \
		const EBoneGetterSetterMode Space, \
		const bool bPropagateToChildren, \
		FCachedRigElement& CachedBone, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Hierarchy_RigUnit_SetBoneInitialTransform_h_15_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_SetBoneInitialTransform_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Bone, \
		const FTransform& Transform, \
		FTransform& Result, \
		const EBoneGetterSetterMode Space, \
		const bool bPropagateToChildren, \
		FCachedRigElement& CachedBone, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FName& Bone = *(FName*)RigVMMemoryHandles[0].GetData(); \
		const FTransform& Transform = *(FTransform*)RigVMMemoryHandles[1].GetData(); \
		FTransform& Result = *(FTransform*)RigVMMemoryHandles[2].GetData(); \
		EBoneGetterSetterMode Space = (EBoneGetterSetterMode)*(uint8*)RigVMMemoryHandles[3].GetData(); \
		const bool bPropagateToChildren = *(bool*)RigVMMemoryHandles[4].GetData(); \
		FRigVMDynamicArray<FCachedRigElement> CachedBone_5_Array(*((FRigVMByteArray*)RigVMMemoryHandles[5].GetData(0, false))); \
		CachedBone_5_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedBone = CachedBone_5_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[6].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Bone, \
			Transform, \
			Result, \
			Space, \
			bPropagateToChildren, \
			CachedBone, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnitMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_SetBoneInitialTransform>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Hierarchy_RigUnit_SetBoneInitialTransform_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
