// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigInfluenceMap_generated_h
#error "RigInfluenceMap.generated.h already included, missing '#pragma once' in RigInfluenceMap.h"
#endif
#define CONTROLRIG_RigInfluenceMap_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_RigInfluenceMap_h_174_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigInfluenceMapPerEvent_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__Maps() { return STRUCT_OFFSET(FRigInfluenceMapPerEvent, Maps); } \
	FORCEINLINE static uint32 __PPO__EventToIndex() { return STRUCT_OFFSET(FRigInfluenceMapPerEvent, EventToIndex); }


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigInfluenceMapPerEvent>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_RigInfluenceMap_h_103_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigInfluenceMap_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__EventName() { return STRUCT_OFFSET(FRigInfluenceMap, EventName); } \
	FORCEINLINE static uint32 __PPO__Entries() { return STRUCT_OFFSET(FRigInfluenceMap, Entries); } \
	FORCEINLINE static uint32 __PPO__KeyToIndex() { return STRUCT_OFFSET(FRigInfluenceMap, KeyToIndex); }


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigInfluenceMap>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_RigInfluenceMap_h_79_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigInfluenceEntryModifier_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigInfluenceEntryModifier>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_RigInfluenceMap_h_12_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigInfluenceEntry_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__Source() { return STRUCT_OFFSET(FRigInfluenceEntry, Source); } \
	FORCEINLINE static uint32 __PPO__AffectedList() { return STRUCT_OFFSET(FRigInfluenceEntry, AffectedList); }


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigInfluenceEntry>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Rigs_RigInfluenceMap_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
