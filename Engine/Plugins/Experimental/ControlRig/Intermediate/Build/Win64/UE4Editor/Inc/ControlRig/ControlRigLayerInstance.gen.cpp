// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Public/Sequencer/ControlRigLayerInstance.h"
#include "Engine/Classes/Components/SkeletalMeshComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeControlRigLayerInstance() {}
// Cross Module References
	CONTROLRIG_API UClass* Z_Construct_UClass_UControlRigLayerInstance_NoRegister();
	CONTROLRIG_API UClass* Z_Construct_UClass_UControlRigLayerInstance();
	ENGINE_API UClass* Z_Construct_UClass_UAnimInstance();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	ANIMGRAPHRUNTIME_API UClass* Z_Construct_UClass_USequencerAnimationSupport_NoRegister();
// End Cross Module References
	void UControlRigLayerInstance::StaticRegisterNativesUControlRigLayerInstance()
	{
	}
	UClass* Z_Construct_UClass_UControlRigLayerInstance_NoRegister()
	{
		return UControlRigLayerInstance::StaticClass();
	}
	struct Z_Construct_UClass_UControlRigLayerInstance_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UControlRigLayerInstance_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAnimInstance,
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigLayerInstance_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "AnimInstance" },
		{ "IncludePath", "Sequencer/ControlRigLayerInstance.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Public/Sequencer/ControlRigLayerInstance.h" },
	};
#endif
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_UControlRigLayerInstance_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_USequencerAnimationSupport_NoRegister, (int32)VTABLE_OFFSET(UControlRigLayerInstance, ISequencerAnimationSupport), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UControlRigLayerInstance_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UControlRigLayerInstance>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UControlRigLayerInstance_Statics::ClassParams = {
		&UControlRigLayerInstance::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UControlRigLayerInstance_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigLayerInstance_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UControlRigLayerInstance()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UControlRigLayerInstance_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UControlRigLayerInstance, 709686963);
	template<> CONTROLRIG_API UClass* StaticClass<UControlRigLayerInstance>()
	{
		return UControlRigLayerInstance::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UControlRigLayerInstance(Z_Construct_UClass_UControlRigLayerInstance, &UControlRigLayerInstance::StaticClass, TEXT("/Script/ControlRig"), TEXT("UControlRigLayerInstance"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UControlRigLayerInstance);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
