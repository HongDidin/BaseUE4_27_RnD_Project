// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRigEditor/Private/EditMode/ControlRigControlsProxy.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeControlRigControlsProxy() {}
// Cross Module References
	CONTROLRIGEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FControlRigEnumControlProxyValue();
	UPackage* Z_Construct_UPackage__Script_ControlRigEditor();
	COREUOBJECT_API UClass* Z_Construct_UClass_UEnum();
	CONTROLRIGEDITOR_API UClass* Z_Construct_UClass_UControlRigControlsProxy_NoRegister();
	CONTROLRIGEDITOR_API UClass* Z_Construct_UClass_UControlRigControlsProxy();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	CONTROLRIGEDITOR_API UClass* Z_Construct_UClass_UControlRigTransformControlProxy_NoRegister();
	CONTROLRIGEDITOR_API UClass* Z_Construct_UClass_UControlRigTransformControlProxy();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	CONTROLRIGEDITOR_API UClass* Z_Construct_UClass_UControlRigEulerTransformControlProxy_NoRegister();
	CONTROLRIGEDITOR_API UClass* Z_Construct_UClass_UControlRigEulerTransformControlProxy();
	ANIMATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FEulerTransform();
	CONTROLRIGEDITOR_API UClass* Z_Construct_UClass_UControlRigTransformNoScaleControlProxy_NoRegister();
	CONTROLRIGEDITOR_API UClass* Z_Construct_UClass_UControlRigTransformNoScaleControlProxy();
	ANIMATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FTransformNoScale();
	CONTROLRIGEDITOR_API UClass* Z_Construct_UClass_UControlRigFloatControlProxy_NoRegister();
	CONTROLRIGEDITOR_API UClass* Z_Construct_UClass_UControlRigFloatControlProxy();
	CONTROLRIGEDITOR_API UClass* Z_Construct_UClass_UControlRigIntegerControlProxy_NoRegister();
	CONTROLRIGEDITOR_API UClass* Z_Construct_UClass_UControlRigIntegerControlProxy();
	CONTROLRIGEDITOR_API UClass* Z_Construct_UClass_UControlRigEnumControlProxy_NoRegister();
	CONTROLRIGEDITOR_API UClass* Z_Construct_UClass_UControlRigEnumControlProxy();
	CONTROLRIGEDITOR_API UClass* Z_Construct_UClass_UControlRigVectorControlProxy_NoRegister();
	CONTROLRIGEDITOR_API UClass* Z_Construct_UClass_UControlRigVectorControlProxy();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	CONTROLRIGEDITOR_API UClass* Z_Construct_UClass_UControlRigVector2DControlProxy_NoRegister();
	CONTROLRIGEDITOR_API UClass* Z_Construct_UClass_UControlRigVector2DControlProxy();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	CONTROLRIGEDITOR_API UClass* Z_Construct_UClass_UControlRigBoolControlProxy_NoRegister();
	CONTROLRIGEDITOR_API UClass* Z_Construct_UClass_UControlRigBoolControlProxy();
	CONTROLRIGEDITOR_API UClass* Z_Construct_UClass_UControlRigDetailPanelControlProxies_NoRegister();
	CONTROLRIGEDITOR_API UClass* Z_Construct_UClass_UControlRigDetailPanelControlProxies();
// End Cross Module References
class UScriptStruct* FControlRigEnumControlProxyValue::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIGEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FControlRigEnumControlProxyValue_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FControlRigEnumControlProxyValue, Z_Construct_UPackage__Script_ControlRigEditor(), TEXT("ControlRigEnumControlProxyValue"), sizeof(FControlRigEnumControlProxyValue), Get_Z_Construct_UScriptStruct_FControlRigEnumControlProxyValue_Hash());
	}
	return Singleton;
}
template<> CONTROLRIGEDITOR_API UScriptStruct* StaticStruct<FControlRigEnumControlProxyValue>()
{
	return FControlRigEnumControlProxyValue::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FControlRigEnumControlProxyValue(FControlRigEnumControlProxyValue::StaticStruct, TEXT("/Script/ControlRigEditor"), TEXT("ControlRigEnumControlProxyValue"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRigEditor_StaticRegisterNativesFControlRigEnumControlProxyValue
{
	FScriptStruct_ControlRigEditor_StaticRegisterNativesFControlRigEnumControlProxyValue()
	{
		UScriptStruct::DeferCppStructOps<FControlRigEnumControlProxyValue>(FName(TEXT("ControlRigEnumControlProxyValue")));
	}
} ScriptStruct_ControlRigEditor_StaticRegisterNativesFControlRigEnumControlProxyValue;
	struct Z_Construct_UScriptStruct_FControlRigEnumControlProxyValue_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EnumType_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_EnumType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EnumIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_EnumIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FControlRigEnumControlProxyValue_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Private/EditMode/ControlRigControlsProxy.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FControlRigEnumControlProxyValue_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FControlRigEnumControlProxyValue>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FControlRigEnumControlProxyValue_Statics::NewProp_EnumType_MetaData[] = {
		{ "ModuleRelativePath", "Private/EditMode/ControlRigControlsProxy.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FControlRigEnumControlProxyValue_Statics::NewProp_EnumType = { "EnumType", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FControlRigEnumControlProxyValue, EnumType), Z_Construct_UClass_UEnum, METADATA_PARAMS(Z_Construct_UScriptStruct_FControlRigEnumControlProxyValue_Statics::NewProp_EnumType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FControlRigEnumControlProxyValue_Statics::NewProp_EnumType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FControlRigEnumControlProxyValue_Statics::NewProp_EnumIndex_MetaData[] = {
		{ "Category", "Enum" },
		{ "ModuleRelativePath", "Private/EditMode/ControlRigControlsProxy.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FControlRigEnumControlProxyValue_Statics::NewProp_EnumIndex = { "EnumIndex", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FControlRigEnumControlProxyValue, EnumIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FControlRigEnumControlProxyValue_Statics::NewProp_EnumIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FControlRigEnumControlProxyValue_Statics::NewProp_EnumIndex_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FControlRigEnumControlProxyValue_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FControlRigEnumControlProxyValue_Statics::NewProp_EnumType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FControlRigEnumControlProxyValue_Statics::NewProp_EnumIndex,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FControlRigEnumControlProxyValue_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRigEditor,
		nullptr,
		&NewStructOps,
		"ControlRigEnumControlProxyValue",
		sizeof(FControlRigEnumControlProxyValue),
		alignof(FControlRigEnumControlProxyValue),
		Z_Construct_UScriptStruct_FControlRigEnumControlProxyValue_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FControlRigEnumControlProxyValue_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FControlRigEnumControlProxyValue_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FControlRigEnumControlProxyValue_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FControlRigEnumControlProxyValue()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FControlRigEnumControlProxyValue_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRigEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ControlRigEnumControlProxyValue"), sizeof(FControlRigEnumControlProxyValue), Get_Z_Construct_UScriptStruct_FControlRigEnumControlProxyValue_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FControlRigEnumControlProxyValue_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FControlRigEnumControlProxyValue_Hash() { return 3351047565U; }
	void UControlRigControlsProxy::StaticRegisterNativesUControlRigControlsProxy()
	{
	}
	UClass* Z_Construct_UClass_UControlRigControlsProxy_NoRegister()
	{
		return UControlRigControlsProxy::StaticClass();
	}
	struct Z_Construct_UClass_UControlRigControlsProxy_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSelected_MetaData[];
#endif
		static void NewProp_bSelected_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSelected;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ControlName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ControlName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UControlRigControlsProxy_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRigEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigControlsProxy_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "EditMode/ControlRigControlsProxy.h" },
		{ "ModuleRelativePath", "Private/EditMode/ControlRigControlsProxy.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigControlsProxy_Statics::NewProp_bSelected_MetaData[] = {
		{ "ModuleRelativePath", "Private/EditMode/ControlRigControlsProxy.h" },
	};
#endif
	void Z_Construct_UClass_UControlRigControlsProxy_Statics::NewProp_bSelected_SetBit(void* Obj)
	{
		((UControlRigControlsProxy*)Obj)->bSelected = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UControlRigControlsProxy_Statics::NewProp_bSelected = { "bSelected", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UControlRigControlsProxy), &Z_Construct_UClass_UControlRigControlsProxy_Statics::NewProp_bSelected_SetBit, METADATA_PARAMS(Z_Construct_UClass_UControlRigControlsProxy_Statics::NewProp_bSelected_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigControlsProxy_Statics::NewProp_bSelected_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigControlsProxy_Statics::NewProp_ControlName_MetaData[] = {
		{ "Category", "Control" },
		{ "ModuleRelativePath", "Private/EditMode/ControlRigControlsProxy.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UControlRigControlsProxy_Statics::NewProp_ControlName = { "ControlName", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigControlsProxy, ControlName), METADATA_PARAMS(Z_Construct_UClass_UControlRigControlsProxy_Statics::NewProp_ControlName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigControlsProxy_Statics::NewProp_ControlName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UControlRigControlsProxy_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigControlsProxy_Statics::NewProp_bSelected,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigControlsProxy_Statics::NewProp_ControlName,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UControlRigControlsProxy_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UControlRigControlsProxy>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UControlRigControlsProxy_Statics::ClassParams = {
		&UControlRigControlsProxy::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UControlRigControlsProxy_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigControlsProxy_Statics::PropPointers),
		0,
		0x000000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UControlRigControlsProxy_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigControlsProxy_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UControlRigControlsProxy()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UControlRigControlsProxy_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UControlRigControlsProxy, 2939845534);
	template<> CONTROLRIGEDITOR_API UClass* StaticClass<UControlRigControlsProxy>()
	{
		return UControlRigControlsProxy::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UControlRigControlsProxy(Z_Construct_UClass_UControlRigControlsProxy, &UControlRigControlsProxy::StaticClass, TEXT("/Script/ControlRigEditor"), TEXT("UControlRigControlsProxy"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UControlRigControlsProxy);
	void UControlRigTransformControlProxy::StaticRegisterNativesUControlRigTransformControlProxy()
	{
	}
	UClass* Z_Construct_UClass_UControlRigTransformControlProxy_NoRegister()
	{
		return UControlRigTransformControlProxy::StaticClass();
	}
	struct Z_Construct_UClass_UControlRigTransformControlProxy_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Transform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Transform;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UControlRigTransformControlProxy_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UControlRigControlsProxy,
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRigEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigTransformControlProxy_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "EditMode/ControlRigControlsProxy.h" },
		{ "ModuleRelativePath", "Private/EditMode/ControlRigControlsProxy.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigTransformControlProxy_Statics::NewProp_Transform_MetaData[] = {
		{ "Category", "Control" },
		{ "ModuleRelativePath", "Private/EditMode/ControlRigControlsProxy.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UControlRigTransformControlProxy_Statics::NewProp_Transform = { "Transform", nullptr, (EPropertyFlags)0x0010000200000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigTransformControlProxy, Transform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UClass_UControlRigTransformControlProxy_Statics::NewProp_Transform_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigTransformControlProxy_Statics::NewProp_Transform_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UControlRigTransformControlProxy_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigTransformControlProxy_Statics::NewProp_Transform,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UControlRigTransformControlProxy_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UControlRigTransformControlProxy>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UControlRigTransformControlProxy_Statics::ClassParams = {
		&UControlRigTransformControlProxy::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UControlRigTransformControlProxy_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigTransformControlProxy_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UControlRigTransformControlProxy_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigTransformControlProxy_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UControlRigTransformControlProxy()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UControlRigTransformControlProxy_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UControlRigTransformControlProxy, 1607331439);
	template<> CONTROLRIGEDITOR_API UClass* StaticClass<UControlRigTransformControlProxy>()
	{
		return UControlRigTransformControlProxy::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UControlRigTransformControlProxy(Z_Construct_UClass_UControlRigTransformControlProxy, &UControlRigTransformControlProxy::StaticClass, TEXT("/Script/ControlRigEditor"), TEXT("UControlRigTransformControlProxy"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UControlRigTransformControlProxy);
	void UControlRigEulerTransformControlProxy::StaticRegisterNativesUControlRigEulerTransformControlProxy()
	{
	}
	UClass* Z_Construct_UClass_UControlRigEulerTransformControlProxy_NoRegister()
	{
		return UControlRigEulerTransformControlProxy::StaticClass();
	}
	struct Z_Construct_UClass_UControlRigEulerTransformControlProxy_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Transform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Transform;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UControlRigEulerTransformControlProxy_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UControlRigControlsProxy,
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRigEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigEulerTransformControlProxy_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "EditMode/ControlRigControlsProxy.h" },
		{ "ModuleRelativePath", "Private/EditMode/ControlRigControlsProxy.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigEulerTransformControlProxy_Statics::NewProp_Transform_MetaData[] = {
		{ "Category", "Control" },
		{ "ModuleRelativePath", "Private/EditMode/ControlRigControlsProxy.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UControlRigEulerTransformControlProxy_Statics::NewProp_Transform = { "Transform", nullptr, (EPropertyFlags)0x0010000200000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigEulerTransformControlProxy, Transform), Z_Construct_UScriptStruct_FEulerTransform, METADATA_PARAMS(Z_Construct_UClass_UControlRigEulerTransformControlProxy_Statics::NewProp_Transform_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigEulerTransformControlProxy_Statics::NewProp_Transform_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UControlRigEulerTransformControlProxy_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigEulerTransformControlProxy_Statics::NewProp_Transform,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UControlRigEulerTransformControlProxy_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UControlRigEulerTransformControlProxy>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UControlRigEulerTransformControlProxy_Statics::ClassParams = {
		&UControlRigEulerTransformControlProxy::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UControlRigEulerTransformControlProxy_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigEulerTransformControlProxy_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UControlRigEulerTransformControlProxy_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigEulerTransformControlProxy_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UControlRigEulerTransformControlProxy()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UControlRigEulerTransformControlProxy_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UControlRigEulerTransformControlProxy, 4129165926);
	template<> CONTROLRIGEDITOR_API UClass* StaticClass<UControlRigEulerTransformControlProxy>()
	{
		return UControlRigEulerTransformControlProxy::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UControlRigEulerTransformControlProxy(Z_Construct_UClass_UControlRigEulerTransformControlProxy, &UControlRigEulerTransformControlProxy::StaticClass, TEXT("/Script/ControlRigEditor"), TEXT("UControlRigEulerTransformControlProxy"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UControlRigEulerTransformControlProxy);
	void UControlRigTransformNoScaleControlProxy::StaticRegisterNativesUControlRigTransformNoScaleControlProxy()
	{
	}
	UClass* Z_Construct_UClass_UControlRigTransformNoScaleControlProxy_NoRegister()
	{
		return UControlRigTransformNoScaleControlProxy::StaticClass();
	}
	struct Z_Construct_UClass_UControlRigTransformNoScaleControlProxy_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Transform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Transform;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UControlRigTransformNoScaleControlProxy_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UControlRigControlsProxy,
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRigEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigTransformNoScaleControlProxy_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "EditMode/ControlRigControlsProxy.h" },
		{ "ModuleRelativePath", "Private/EditMode/ControlRigControlsProxy.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigTransformNoScaleControlProxy_Statics::NewProp_Transform_MetaData[] = {
		{ "Category", "Control" },
		{ "ModuleRelativePath", "Private/EditMode/ControlRigControlsProxy.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UControlRigTransformNoScaleControlProxy_Statics::NewProp_Transform = { "Transform", nullptr, (EPropertyFlags)0x0010000200000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigTransformNoScaleControlProxy, Transform), Z_Construct_UScriptStruct_FTransformNoScale, METADATA_PARAMS(Z_Construct_UClass_UControlRigTransformNoScaleControlProxy_Statics::NewProp_Transform_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigTransformNoScaleControlProxy_Statics::NewProp_Transform_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UControlRigTransformNoScaleControlProxy_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigTransformNoScaleControlProxy_Statics::NewProp_Transform,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UControlRigTransformNoScaleControlProxy_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UControlRigTransformNoScaleControlProxy>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UControlRigTransformNoScaleControlProxy_Statics::ClassParams = {
		&UControlRigTransformNoScaleControlProxy::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UControlRigTransformNoScaleControlProxy_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigTransformNoScaleControlProxy_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UControlRigTransformNoScaleControlProxy_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigTransformNoScaleControlProxy_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UControlRigTransformNoScaleControlProxy()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UControlRigTransformNoScaleControlProxy_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UControlRigTransformNoScaleControlProxy, 4151273222);
	template<> CONTROLRIGEDITOR_API UClass* StaticClass<UControlRigTransformNoScaleControlProxy>()
	{
		return UControlRigTransformNoScaleControlProxy::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UControlRigTransformNoScaleControlProxy(Z_Construct_UClass_UControlRigTransformNoScaleControlProxy, &UControlRigTransformNoScaleControlProxy::StaticClass, TEXT("/Script/ControlRigEditor"), TEXT("UControlRigTransformNoScaleControlProxy"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UControlRigTransformNoScaleControlProxy);
	void UControlRigFloatControlProxy::StaticRegisterNativesUControlRigFloatControlProxy()
	{
	}
	UClass* Z_Construct_UClass_UControlRigFloatControlProxy_NoRegister()
	{
		return UControlRigFloatControlProxy::StaticClass();
	}
	struct Z_Construct_UClass_UControlRigFloatControlProxy_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Float_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Float;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UControlRigFloatControlProxy_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UControlRigControlsProxy,
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRigEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigFloatControlProxy_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "EditMode/ControlRigControlsProxy.h" },
		{ "ModuleRelativePath", "Private/EditMode/ControlRigControlsProxy.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigFloatControlProxy_Statics::NewProp_Float_MetaData[] = {
		{ "Category", "Control" },
		{ "ModuleRelativePath", "Private/EditMode/ControlRigControlsProxy.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UControlRigFloatControlProxy_Statics::NewProp_Float = { "Float", nullptr, (EPropertyFlags)0x0010000200000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigFloatControlProxy, Float), METADATA_PARAMS(Z_Construct_UClass_UControlRigFloatControlProxy_Statics::NewProp_Float_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigFloatControlProxy_Statics::NewProp_Float_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UControlRigFloatControlProxy_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigFloatControlProxy_Statics::NewProp_Float,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UControlRigFloatControlProxy_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UControlRigFloatControlProxy>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UControlRigFloatControlProxy_Statics::ClassParams = {
		&UControlRigFloatControlProxy::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UControlRigFloatControlProxy_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigFloatControlProxy_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UControlRigFloatControlProxy_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigFloatControlProxy_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UControlRigFloatControlProxy()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UControlRigFloatControlProxy_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UControlRigFloatControlProxy, 1224228428);
	template<> CONTROLRIGEDITOR_API UClass* StaticClass<UControlRigFloatControlProxy>()
	{
		return UControlRigFloatControlProxy::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UControlRigFloatControlProxy(Z_Construct_UClass_UControlRigFloatControlProxy, &UControlRigFloatControlProxy::StaticClass, TEXT("/Script/ControlRigEditor"), TEXT("UControlRigFloatControlProxy"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UControlRigFloatControlProxy);
	void UControlRigIntegerControlProxy::StaticRegisterNativesUControlRigIntegerControlProxy()
	{
	}
	UClass* Z_Construct_UClass_UControlRigIntegerControlProxy_NoRegister()
	{
		return UControlRigIntegerControlProxy::StaticClass();
	}
	struct Z_Construct_UClass_UControlRigIntegerControlProxy_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Integer_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Integer;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UControlRigIntegerControlProxy_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UControlRigControlsProxy,
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRigEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigIntegerControlProxy_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "EditMode/ControlRigControlsProxy.h" },
		{ "ModuleRelativePath", "Private/EditMode/ControlRigControlsProxy.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigIntegerControlProxy_Statics::NewProp_Integer_MetaData[] = {
		{ "Category", "Control" },
		{ "ModuleRelativePath", "Private/EditMode/ControlRigControlsProxy.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UControlRigIntegerControlProxy_Statics::NewProp_Integer = { "Integer", nullptr, (EPropertyFlags)0x0010000200000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigIntegerControlProxy, Integer), METADATA_PARAMS(Z_Construct_UClass_UControlRigIntegerControlProxy_Statics::NewProp_Integer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigIntegerControlProxy_Statics::NewProp_Integer_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UControlRigIntegerControlProxy_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigIntegerControlProxy_Statics::NewProp_Integer,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UControlRigIntegerControlProxy_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UControlRigIntegerControlProxy>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UControlRigIntegerControlProxy_Statics::ClassParams = {
		&UControlRigIntegerControlProxy::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UControlRigIntegerControlProxy_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigIntegerControlProxy_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UControlRigIntegerControlProxy_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigIntegerControlProxy_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UControlRigIntegerControlProxy()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UControlRigIntegerControlProxy_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UControlRigIntegerControlProxy, 4285533055);
	template<> CONTROLRIGEDITOR_API UClass* StaticClass<UControlRigIntegerControlProxy>()
	{
		return UControlRigIntegerControlProxy::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UControlRigIntegerControlProxy(Z_Construct_UClass_UControlRigIntegerControlProxy, &UControlRigIntegerControlProxy::StaticClass, TEXT("/Script/ControlRigEditor"), TEXT("UControlRigIntegerControlProxy"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UControlRigIntegerControlProxy);
	void UControlRigEnumControlProxy::StaticRegisterNativesUControlRigEnumControlProxy()
	{
	}
	UClass* Z_Construct_UClass_UControlRigEnumControlProxy_NoRegister()
	{
		return UControlRigEnumControlProxy::StaticClass();
	}
	struct Z_Construct_UClass_UControlRigEnumControlProxy_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Enum_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Enum;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UControlRigEnumControlProxy_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UControlRigControlsProxy,
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRigEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigEnumControlProxy_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "EditMode/ControlRigControlsProxy.h" },
		{ "ModuleRelativePath", "Private/EditMode/ControlRigControlsProxy.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigEnumControlProxy_Statics::NewProp_Enum_MetaData[] = {
		{ "Category", "Control" },
		{ "ModuleRelativePath", "Private/EditMode/ControlRigControlsProxy.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UControlRigEnumControlProxy_Statics::NewProp_Enum = { "Enum", nullptr, (EPropertyFlags)0x0010000200000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigEnumControlProxy, Enum), Z_Construct_UScriptStruct_FControlRigEnumControlProxyValue, METADATA_PARAMS(Z_Construct_UClass_UControlRigEnumControlProxy_Statics::NewProp_Enum_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigEnumControlProxy_Statics::NewProp_Enum_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UControlRigEnumControlProxy_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigEnumControlProxy_Statics::NewProp_Enum,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UControlRigEnumControlProxy_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UControlRigEnumControlProxy>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UControlRigEnumControlProxy_Statics::ClassParams = {
		&UControlRigEnumControlProxy::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UControlRigEnumControlProxy_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigEnumControlProxy_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UControlRigEnumControlProxy_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigEnumControlProxy_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UControlRigEnumControlProxy()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UControlRigEnumControlProxy_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UControlRigEnumControlProxy, 736818573);
	template<> CONTROLRIGEDITOR_API UClass* StaticClass<UControlRigEnumControlProxy>()
	{
		return UControlRigEnumControlProxy::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UControlRigEnumControlProxy(Z_Construct_UClass_UControlRigEnumControlProxy, &UControlRigEnumControlProxy::StaticClass, TEXT("/Script/ControlRigEditor"), TEXT("UControlRigEnumControlProxy"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UControlRigEnumControlProxy);
	void UControlRigVectorControlProxy::StaticRegisterNativesUControlRigVectorControlProxy()
	{
	}
	UClass* Z_Construct_UClass_UControlRigVectorControlProxy_NoRegister()
	{
		return UControlRigVectorControlProxy::StaticClass();
	}
	struct Z_Construct_UClass_UControlRigVectorControlProxy_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Vector_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Vector;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UControlRigVectorControlProxy_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UControlRigControlsProxy,
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRigEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigVectorControlProxy_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "EditMode/ControlRigControlsProxy.h" },
		{ "ModuleRelativePath", "Private/EditMode/ControlRigControlsProxy.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigVectorControlProxy_Statics::NewProp_Vector_MetaData[] = {
		{ "Category", "Control" },
		{ "ModuleRelativePath", "Private/EditMode/ControlRigControlsProxy.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UControlRigVectorControlProxy_Statics::NewProp_Vector = { "Vector", nullptr, (EPropertyFlags)0x0010000200000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigVectorControlProxy, Vector), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UControlRigVectorControlProxy_Statics::NewProp_Vector_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigVectorControlProxy_Statics::NewProp_Vector_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UControlRigVectorControlProxy_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigVectorControlProxy_Statics::NewProp_Vector,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UControlRigVectorControlProxy_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UControlRigVectorControlProxy>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UControlRigVectorControlProxy_Statics::ClassParams = {
		&UControlRigVectorControlProxy::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UControlRigVectorControlProxy_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigVectorControlProxy_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UControlRigVectorControlProxy_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigVectorControlProxy_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UControlRigVectorControlProxy()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UControlRigVectorControlProxy_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UControlRigVectorControlProxy, 1423432910);
	template<> CONTROLRIGEDITOR_API UClass* StaticClass<UControlRigVectorControlProxy>()
	{
		return UControlRigVectorControlProxy::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UControlRigVectorControlProxy(Z_Construct_UClass_UControlRigVectorControlProxy, &UControlRigVectorControlProxy::StaticClass, TEXT("/Script/ControlRigEditor"), TEXT("UControlRigVectorControlProxy"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UControlRigVectorControlProxy);
	void UControlRigVector2DControlProxy::StaticRegisterNativesUControlRigVector2DControlProxy()
	{
	}
	UClass* Z_Construct_UClass_UControlRigVector2DControlProxy_NoRegister()
	{
		return UControlRigVector2DControlProxy::StaticClass();
	}
	struct Z_Construct_UClass_UControlRigVector2DControlProxy_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Vector2D_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Vector2D;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UControlRigVector2DControlProxy_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UControlRigControlsProxy,
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRigEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigVector2DControlProxy_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "EditMode/ControlRigControlsProxy.h" },
		{ "ModuleRelativePath", "Private/EditMode/ControlRigControlsProxy.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigVector2DControlProxy_Statics::NewProp_Vector2D_MetaData[] = {
		{ "Category", "Control" },
		{ "ModuleRelativePath", "Private/EditMode/ControlRigControlsProxy.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UControlRigVector2DControlProxy_Statics::NewProp_Vector2D = { "Vector2D", nullptr, (EPropertyFlags)0x0010000200000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigVector2DControlProxy, Vector2D), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UClass_UControlRigVector2DControlProxy_Statics::NewProp_Vector2D_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigVector2DControlProxy_Statics::NewProp_Vector2D_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UControlRigVector2DControlProxy_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigVector2DControlProxy_Statics::NewProp_Vector2D,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UControlRigVector2DControlProxy_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UControlRigVector2DControlProxy>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UControlRigVector2DControlProxy_Statics::ClassParams = {
		&UControlRigVector2DControlProxy::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UControlRigVector2DControlProxy_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigVector2DControlProxy_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UControlRigVector2DControlProxy_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigVector2DControlProxy_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UControlRigVector2DControlProxy()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UControlRigVector2DControlProxy_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UControlRigVector2DControlProxy, 3197969045);
	template<> CONTROLRIGEDITOR_API UClass* StaticClass<UControlRigVector2DControlProxy>()
	{
		return UControlRigVector2DControlProxy::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UControlRigVector2DControlProxy(Z_Construct_UClass_UControlRigVector2DControlProxy, &UControlRigVector2DControlProxy::StaticClass, TEXT("/Script/ControlRigEditor"), TEXT("UControlRigVector2DControlProxy"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UControlRigVector2DControlProxy);
	void UControlRigBoolControlProxy::StaticRegisterNativesUControlRigBoolControlProxy()
	{
	}
	UClass* Z_Construct_UClass_UControlRigBoolControlProxy_NoRegister()
	{
		return UControlRigBoolControlProxy::StaticClass();
	}
	struct Z_Construct_UClass_UControlRigBoolControlProxy_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Bool_MetaData[];
#endif
		static void NewProp_Bool_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Bool;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UControlRigBoolControlProxy_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UControlRigControlsProxy,
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRigEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigBoolControlProxy_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "EditMode/ControlRigControlsProxy.h" },
		{ "ModuleRelativePath", "Private/EditMode/ControlRigControlsProxy.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigBoolControlProxy_Statics::NewProp_Bool_MetaData[] = {
		{ "Category", "Control" },
		{ "ModuleRelativePath", "Private/EditMode/ControlRigControlsProxy.h" },
	};
#endif
	void Z_Construct_UClass_UControlRigBoolControlProxy_Statics::NewProp_Bool_SetBit(void* Obj)
	{
		((UControlRigBoolControlProxy*)Obj)->Bool = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UControlRigBoolControlProxy_Statics::NewProp_Bool = { "Bool", nullptr, (EPropertyFlags)0x0010000200000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UControlRigBoolControlProxy), &Z_Construct_UClass_UControlRigBoolControlProxy_Statics::NewProp_Bool_SetBit, METADATA_PARAMS(Z_Construct_UClass_UControlRigBoolControlProxy_Statics::NewProp_Bool_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigBoolControlProxy_Statics::NewProp_Bool_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UControlRigBoolControlProxy_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigBoolControlProxy_Statics::NewProp_Bool,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UControlRigBoolControlProxy_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UControlRigBoolControlProxy>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UControlRigBoolControlProxy_Statics::ClassParams = {
		&UControlRigBoolControlProxy::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UControlRigBoolControlProxy_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigBoolControlProxy_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UControlRigBoolControlProxy_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigBoolControlProxy_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UControlRigBoolControlProxy()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UControlRigBoolControlProxy_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UControlRigBoolControlProxy, 394435387);
	template<> CONTROLRIGEDITOR_API UClass* StaticClass<UControlRigBoolControlProxy>()
	{
		return UControlRigBoolControlProxy::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UControlRigBoolControlProxy(Z_Construct_UClass_UControlRigBoolControlProxy, &UControlRigBoolControlProxy::StaticClass, TEXT("/Script/ControlRigEditor"), TEXT("UControlRigBoolControlProxy"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UControlRigBoolControlProxy);
	void UControlRigDetailPanelControlProxies::StaticRegisterNativesUControlRigDetailPanelControlProxies()
	{
	}
	UClass* Z_Construct_UClass_UControlRigDetailPanelControlProxies_NoRegister()
	{
		return UControlRigDetailPanelControlProxies::StaticClass();
	}
	struct Z_Construct_UClass_UControlRigDetailPanelControlProxies_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AllProxies_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_AllProxies_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AllProxies_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_AllProxies;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SelectedProxies_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SelectedProxies_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SelectedProxies;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UControlRigDetailPanelControlProxies_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRigEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigDetailPanelControlProxies_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Proxy in Details Panel */" },
		{ "IncludePath", "EditMode/ControlRigControlsProxy.h" },
		{ "ModuleRelativePath", "Private/EditMode/ControlRigControlsProxy.h" },
		{ "ToolTip", "Proxy in Details Panel" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UControlRigDetailPanelControlProxies_Statics::NewProp_AllProxies_ValueProp = { "AllProxies", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_UControlRigControlsProxy_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UControlRigDetailPanelControlProxies_Statics::NewProp_AllProxies_Key_KeyProp = { "AllProxies_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigDetailPanelControlProxies_Statics::NewProp_AllProxies_MetaData[] = {
		{ "ModuleRelativePath", "Private/EditMode/ControlRigControlsProxy.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UControlRigDetailPanelControlProxies_Statics::NewProp_AllProxies = { "AllProxies", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigDetailPanelControlProxies, AllProxies), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UControlRigDetailPanelControlProxies_Statics::NewProp_AllProxies_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigDetailPanelControlProxies_Statics::NewProp_AllProxies_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UControlRigDetailPanelControlProxies_Statics::NewProp_SelectedProxies_Inner = { "SelectedProxies", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UControlRigControlsProxy_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigDetailPanelControlProxies_Statics::NewProp_SelectedProxies_MetaData[] = {
		{ "ModuleRelativePath", "Private/EditMode/ControlRigControlsProxy.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UControlRigDetailPanelControlProxies_Statics::NewProp_SelectedProxies = { "SelectedProxies", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigDetailPanelControlProxies, SelectedProxies), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UControlRigDetailPanelControlProxies_Statics::NewProp_SelectedProxies_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigDetailPanelControlProxies_Statics::NewProp_SelectedProxies_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UControlRigDetailPanelControlProxies_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigDetailPanelControlProxies_Statics::NewProp_AllProxies_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigDetailPanelControlProxies_Statics::NewProp_AllProxies_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigDetailPanelControlProxies_Statics::NewProp_AllProxies,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigDetailPanelControlProxies_Statics::NewProp_SelectedProxies_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigDetailPanelControlProxies_Statics::NewProp_SelectedProxies,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UControlRigDetailPanelControlProxies_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UControlRigDetailPanelControlProxies>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UControlRigDetailPanelControlProxies_Statics::ClassParams = {
		&UControlRigDetailPanelControlProxies::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UControlRigDetailPanelControlProxies_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigDetailPanelControlProxies_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UControlRigDetailPanelControlProxies_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigDetailPanelControlProxies_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UControlRigDetailPanelControlProxies()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UControlRigDetailPanelControlProxies_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UControlRigDetailPanelControlProxies, 433391490);
	template<> CONTROLRIGEDITOR_API UClass* StaticClass<UControlRigDetailPanelControlProxies>()
	{
		return UControlRigDetailPanelControlProxies::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UControlRigDetailPanelControlProxies(Z_Construct_UClass_UControlRigDetailPanelControlProxies, &UControlRigDetailPanelControlProxies::StaticClass, TEXT("/Script/ControlRigEditor"), TEXT("UControlRigDetailPanelControlProxies"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UControlRigDetailPanelControlProxies);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
