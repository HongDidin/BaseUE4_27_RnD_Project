// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Public/Sequencer/ControlRigSequencerAnimInstance.h"
#include "Engine/Classes/Components/SkeletalMeshComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeControlRigSequencerAnimInstance() {}
// Cross Module References
	CONTROLRIG_API UClass* Z_Construct_UClass_UControlRigSequencerAnimInstance_NoRegister();
	CONTROLRIG_API UClass* Z_Construct_UClass_UControlRigSequencerAnimInstance();
	ANIMGRAPHRUNTIME_API UClass* Z_Construct_UClass_UAnimSequencerInstance();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
// End Cross Module References
	void UControlRigSequencerAnimInstance::StaticRegisterNativesUControlRigSequencerAnimInstance()
	{
	}
	UClass* Z_Construct_UClass_UControlRigSequencerAnimInstance_NoRegister()
	{
		return UControlRigSequencerAnimInstance::StaticClass();
	}
	struct Z_Construct_UClass_UControlRigSequencerAnimInstance_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UControlRigSequencerAnimInstance_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAnimSequencerInstance,
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigSequencerAnimInstance_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "AnimInstance" },
		{ "IncludePath", "Sequencer/ControlRigSequencerAnimInstance.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Public/Sequencer/ControlRigSequencerAnimInstance.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UControlRigSequencerAnimInstance_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UControlRigSequencerAnimInstance>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UControlRigSequencerAnimInstance_Statics::ClassParams = {
		&UControlRigSequencerAnimInstance::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UControlRigSequencerAnimInstance_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigSequencerAnimInstance_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UControlRigSequencerAnimInstance()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UControlRigSequencerAnimInstance_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UControlRigSequencerAnimInstance, 762235661);
	template<> CONTROLRIG_API UClass* StaticClass<UControlRigSequencerAnimInstance>()
	{
		return UControlRigSequencerAnimInstance::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UControlRigSequencerAnimInstance(Z_Construct_UClass_UControlRigSequencerAnimInstance, &UControlRigSequencerAnimInstance::StaticClass, TEXT("/Script/ControlRig"), TEXT("UControlRigSequencerAnimInstance"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UControlRigSequencerAnimInstance);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
