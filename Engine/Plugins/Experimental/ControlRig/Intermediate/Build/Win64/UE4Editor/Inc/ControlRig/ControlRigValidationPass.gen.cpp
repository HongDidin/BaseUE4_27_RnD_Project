// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Public/ControlRigValidationPass.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeControlRigValidationPass() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FControlRigValidationContext();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UClass* Z_Construct_UClass_UControlRigValidator_NoRegister();
	CONTROLRIG_API UClass* Z_Construct_UClass_UControlRigValidator();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	CONTROLRIG_API UClass* Z_Construct_UClass_UControlRigValidationPass_NoRegister();
	CONTROLRIG_API UClass* Z_Construct_UClass_UControlRigValidationPass();
// End Cross Module References
class UScriptStruct* FControlRigValidationContext::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FControlRigValidationContext_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FControlRigValidationContext, Z_Construct_UPackage__Script_ControlRig(), TEXT("ControlRigValidationContext"), sizeof(FControlRigValidationContext), Get_Z_Construct_UScriptStruct_FControlRigValidationContext_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FControlRigValidationContext>()
{
	return FControlRigValidationContext::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FControlRigValidationContext(FControlRigValidationContext::StaticStruct, TEXT("/Script/ControlRig"), TEXT("ControlRigValidationContext"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFControlRigValidationContext
{
	FScriptStruct_ControlRig_StaticRegisterNativesFControlRigValidationContext()
	{
		UScriptStruct::DeferCppStructOps<FControlRigValidationContext>(FName(TEXT("ControlRigValidationContext")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFControlRigValidationContext;
	struct Z_Construct_UScriptStruct_FControlRigValidationContext_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FControlRigValidationContext_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "// todo DECLARE_DELEGATE_TwoParams(FControlRigValidationControlRigChangedDelegate, UControlRigValidator*, UControlRig*);\n" },
		{ "ModuleRelativePath", "Public/ControlRigValidationPass.h" },
		{ "ToolTip", "todo DECLARE_DELEGATE_TwoParams(FControlRigValidationControlRigChangedDelegate, UControlRigValidator*, UControlRig*);" },
	};
#endif
	void* Z_Construct_UScriptStruct_FControlRigValidationContext_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FControlRigValidationContext>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FControlRigValidationContext_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"ControlRigValidationContext",
		sizeof(FControlRigValidationContext),
		alignof(FControlRigValidationContext),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FControlRigValidationContext_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FControlRigValidationContext_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FControlRigValidationContext()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FControlRigValidationContext_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ControlRigValidationContext"), sizeof(FControlRigValidationContext), Get_Z_Construct_UScriptStruct_FControlRigValidationContext_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FControlRigValidationContext_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FControlRigValidationContext_Hash() { return 2906707553U; }
	void UControlRigValidator::StaticRegisterNativesUControlRigValidator()
	{
	}
	UClass* Z_Construct_UClass_UControlRigValidator_NoRegister()
	{
		return UControlRigValidator::StaticClass();
	}
	struct Z_Construct_UClass_UControlRigValidator_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Passes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Passes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Passes;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UControlRigValidator_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigValidator_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Used to perform validation on a debugged Control Rig */" },
		{ "IncludePath", "ControlRigValidationPass.h" },
		{ "ModuleRelativePath", "Public/ControlRigValidationPass.h" },
		{ "ToolTip", "Used to perform validation on a debugged Control Rig" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UControlRigValidator_Statics::NewProp_Passes_Inner = { "Passes", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UControlRigValidationPass_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigValidator_Statics::NewProp_Passes_MetaData[] = {
		{ "ModuleRelativePath", "Public/ControlRigValidationPass.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UControlRigValidator_Statics::NewProp_Passes = { "Passes", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigValidator, Passes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UControlRigValidator_Statics::NewProp_Passes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigValidator_Statics::NewProp_Passes_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UControlRigValidator_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigValidator_Statics::NewProp_Passes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigValidator_Statics::NewProp_Passes,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UControlRigValidator_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UControlRigValidator>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UControlRigValidator_Statics::ClassParams = {
		&UControlRigValidator::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UControlRigValidator_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigValidator_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UControlRigValidator_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigValidator_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UControlRigValidator()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UControlRigValidator_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UControlRigValidator, 359102225);
	template<> CONTROLRIG_API UClass* StaticClass<UControlRigValidator>()
	{
		return UControlRigValidator::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UControlRigValidator(Z_Construct_UClass_UControlRigValidator, &UControlRigValidator::StaticClass, TEXT("/Script/ControlRig"), TEXT("UControlRigValidator"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UControlRigValidator);
	void UControlRigValidationPass::StaticRegisterNativesUControlRigValidationPass()
	{
	}
	UClass* Z_Construct_UClass_UControlRigValidationPass_NoRegister()
	{
		return UControlRigValidationPass::StaticClass();
	}
	struct Z_Construct_UClass_UControlRigValidationPass_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UControlRigValidationPass_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigValidationPass_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Used to perform validation on a debugged Control Rig */" },
		{ "IncludePath", "ControlRigValidationPass.h" },
		{ "ModuleRelativePath", "Public/ControlRigValidationPass.h" },
		{ "ToolTip", "Used to perform validation on a debugged Control Rig" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UControlRigValidationPass_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UControlRigValidationPass>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UControlRigValidationPass_Statics::ClassParams = {
		&UControlRigValidationPass::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UControlRigValidationPass_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigValidationPass_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UControlRigValidationPass()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UControlRigValidationPass_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UControlRigValidationPass, 4236190222);
	template<> CONTROLRIG_API UClass* StaticClass<UControlRigValidationPass>()
	{
		return UControlRigValidationPass::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UControlRigValidationPass(Z_Construct_UClass_UControlRigValidationPass, &UControlRigValidationPass::StaticClass, TEXT("/Script/ControlRig"), TEXT("UControlRigValidationPass"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UControlRigValidationPass);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
