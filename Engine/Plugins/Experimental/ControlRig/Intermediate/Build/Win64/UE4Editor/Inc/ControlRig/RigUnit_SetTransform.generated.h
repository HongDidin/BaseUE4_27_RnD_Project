// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigUnit_SetTransform_generated_h
#error "RigUnit_SetTransform.generated.h already included, missing '#pragma once' in RigUnit_SetTransform.h"
#endif
#define CONTROLRIG_RigUnit_SetTransform_generated_h


#define FRigUnit_SetScale_Execute() \
	void FRigUnit_SetScale::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKey& Item, \
		const EBoneGetterSetterMode Space, \
		const FVector& Scale, \
		const float Weight, \
		const bool bPropagateToChildren, \
		FCachedRigElement& CachedIndex, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Units_Hierarchy_RigUnit_SetTransform_h_215_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_SetScale_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKey& Item, \
		const EBoneGetterSetterMode Space, \
		const FVector& Scale, \
		const float Weight, \
		const bool bPropagateToChildren, \
		FCachedRigElement& CachedIndex, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FRigElementKey& Item = *(FRigElementKey*)RigVMMemoryHandles[0].GetData(); \
		EBoneGetterSetterMode Space = (EBoneGetterSetterMode)*(uint8*)RigVMMemoryHandles[1].GetData(); \
		const FVector& Scale = *(FVector*)RigVMMemoryHandles[2].GetData(); \
		const float Weight = *(float*)RigVMMemoryHandles[3].GetData(); \
		const bool bPropagateToChildren = *(bool*)RigVMMemoryHandles[4].GetData(); \
		FRigVMDynamicArray<FCachedRigElement> CachedIndex_5_Array(*((FRigVMByteArray*)RigVMMemoryHandles[5].GetData(0, false))); \
		CachedIndex_5_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedIndex = CachedIndex_5_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[6].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Item, \
			Space, \
			Scale, \
			Weight, \
			bPropagateToChildren, \
			CachedIndex, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnitMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_SetScale>();


#define FRigUnit_SetRotation_Execute() \
	void FRigUnit_SetRotation::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKey& Item, \
		const EBoneGetterSetterMode Space, \
		const FQuat& Rotation, \
		const float Weight, \
		const bool bPropagateToChildren, \
		FCachedRigElement& CachedIndex, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Units_Hierarchy_RigUnit_SetTransform_h_151_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_SetRotation_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKey& Item, \
		const EBoneGetterSetterMode Space, \
		const FQuat& Rotation, \
		const float Weight, \
		const bool bPropagateToChildren, \
		FCachedRigElement& CachedIndex, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FRigElementKey& Item = *(FRigElementKey*)RigVMMemoryHandles[0].GetData(); \
		EBoneGetterSetterMode Space = (EBoneGetterSetterMode)*(uint8*)RigVMMemoryHandles[1].GetData(); \
		const FQuat& Rotation = *(FQuat*)RigVMMemoryHandles[2].GetData(); \
		const float Weight = *(float*)RigVMMemoryHandles[3].GetData(); \
		const bool bPropagateToChildren = *(bool*)RigVMMemoryHandles[4].GetData(); \
		FRigVMDynamicArray<FCachedRigElement> CachedIndex_5_Array(*((FRigVMByteArray*)RigVMMemoryHandles[5].GetData(0, false))); \
		CachedIndex_5_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedIndex = CachedIndex_5_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[6].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Item, \
			Space, \
			Rotation, \
			Weight, \
			bPropagateToChildren, \
			CachedIndex, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnitMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_SetRotation>();


#define FRigUnit_SetTranslation_Execute() \
	void FRigUnit_SetTranslation::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKey& Item, \
		const EBoneGetterSetterMode Space, \
		const FVector& Translation, \
		const float Weight, \
		const bool bPropagateToChildren, \
		FCachedRigElement& CachedIndex, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Units_Hierarchy_RigUnit_SetTransform_h_87_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_SetTranslation_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKey& Item, \
		const EBoneGetterSetterMode Space, \
		const FVector& Translation, \
		const float Weight, \
		const bool bPropagateToChildren, \
		FCachedRigElement& CachedIndex, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FRigElementKey& Item = *(FRigElementKey*)RigVMMemoryHandles[0].GetData(); \
		EBoneGetterSetterMode Space = (EBoneGetterSetterMode)*(uint8*)RigVMMemoryHandles[1].GetData(); \
		const FVector& Translation = *(FVector*)RigVMMemoryHandles[2].GetData(); \
		const float Weight = *(float*)RigVMMemoryHandles[3].GetData(); \
		const bool bPropagateToChildren = *(bool*)RigVMMemoryHandles[4].GetData(); \
		FRigVMDynamicArray<FCachedRigElement> CachedIndex_5_Array(*((FRigVMByteArray*)RigVMMemoryHandles[5].GetData(0, false))); \
		CachedIndex_5_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedIndex = CachedIndex_5_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[6].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Item, \
			Space, \
			Translation, \
			Weight, \
			bPropagateToChildren, \
			CachedIndex, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnitMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_SetTranslation>();


#define FRigUnit_SetTransform_Execute() \
	void FRigUnit_SetTransform::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKey& Item, \
		const EBoneGetterSetterMode Space, \
		const bool bInitial, \
		const FTransform& Transform, \
		const float Weight, \
		const bool bPropagateToChildren, \
		FCachedRigElement& CachedIndex, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Units_Hierarchy_RigUnit_SetTransform_h_14_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_SetTransform_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKey& Item, \
		const EBoneGetterSetterMode Space, \
		const bool bInitial, \
		const FTransform& Transform, \
		const float Weight, \
		const bool bPropagateToChildren, \
		FCachedRigElement& CachedIndex, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FRigElementKey& Item = *(FRigElementKey*)RigVMMemoryHandles[0].GetData(); \
		EBoneGetterSetterMode Space = (EBoneGetterSetterMode)*(uint8*)RigVMMemoryHandles[1].GetData(); \
		const bool bInitial = *(bool*)RigVMMemoryHandles[2].GetData(); \
		const FTransform& Transform = *(FTransform*)RigVMMemoryHandles[3].GetData(); \
		const float Weight = *(float*)RigVMMemoryHandles[4].GetData(); \
		const bool bPropagateToChildren = *(bool*)RigVMMemoryHandles[5].GetData(); \
		FRigVMDynamicArray<FCachedRigElement> CachedIndex_6_Array(*((FRigVMByteArray*)RigVMMemoryHandles[6].GetData(0, false))); \
		CachedIndex_6_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedIndex = CachedIndex_6_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[7].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Item, \
			Space, \
			bInitial, \
			Transform, \
			Weight, \
			bPropagateToChildren, \
			CachedIndex, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnitMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_SetTransform>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_Units_Hierarchy_RigUnit_SetTransform_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
