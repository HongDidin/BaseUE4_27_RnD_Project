// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UControlRig;
#ifdef CONTROLRIG_ControlRig_generated_h
#error "ControlRig.generated.h already included, missing '#pragma once' in ControlRig.h"
#endif
#define CONTROLRIG_ControlRig_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRig_h_46_SPARSE_DATA
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRig_h_46_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetInteractionRigClass); \
	DECLARE_FUNCTION(execGetInteractionRigClass); \
	DECLARE_FUNCTION(execSetInteractionRig); \
	DECLARE_FUNCTION(execGetInteractionRig);


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRig_h_46_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetInteractionRigClass); \
	DECLARE_FUNCTION(execGetInteractionRigClass); \
	DECLARE_FUNCTION(execSetInteractionRig); \
	DECLARE_FUNCTION(execGetInteractionRig);


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRig_h_46_ARCHIVESERIALIZER \
	DECLARE_FSTRUCTUREDARCHIVE_SERIALIZER(UControlRig, NO_API)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRig_h_46_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUControlRig(); \
	friend struct Z_Construct_UClass_UControlRig_Statics; \
public: \
	DECLARE_CLASS(UControlRig, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/ControlRig"), NO_API) \
	DECLARE_SERIALIZER(UControlRig) \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRig_h_46_ARCHIVESERIALIZER \
	virtual UObject* _getUObject() const override { return const_cast<UControlRig*>(this); }


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRig_h_46_INCLASS \
private: \
	static void StaticRegisterNativesUControlRig(); \
	friend struct Z_Construct_UClass_UControlRig_Statics; \
public: \
	DECLARE_CLASS(UControlRig, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/ControlRig"), NO_API) \
	DECLARE_SERIALIZER(UControlRig) \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRig_h_46_ARCHIVESERIALIZER \
	virtual UObject* _getUObject() const override { return const_cast<UControlRig*>(this); }


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRig_h_46_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UControlRig(const FObjectInitializer& ObjectInitializer); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRig) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRig); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRig); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRig(UControlRig&&); \
	NO_API UControlRig(const UControlRig&); \
public:


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRig_h_46_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRig(UControlRig&&); \
	NO_API UControlRig(const UControlRig&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRig); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRig); \
	DEFINE_ABSTRACT_DEFAULT_CONSTRUCTOR_CALL(UControlRig)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRig_h_46_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__VM() { return STRUCT_OFFSET(UControlRig, VM); } \
	FORCEINLINE static uint32 __PPO__Hierarchy() { return STRUCT_OFFSET(UControlRig, Hierarchy); } \
	FORCEINLINE static uint32 __PPO__GizmoLibrary() { return STRUCT_OFFSET(UControlRig, GizmoLibrary); } \
	FORCEINLINE static uint32 __PPO__InputProperties_DEPRECATED() { return STRUCT_OFFSET(UControlRig, InputProperties_DEPRECATED); } \
	FORCEINLINE static uint32 __PPO__OutputProperties_DEPRECATED() { return STRUCT_OFFSET(UControlRig, OutputProperties_DEPRECATED); } \
	FORCEINLINE static uint32 __PPO__DrawContainer() { return STRUCT_OFFSET(UControlRig, DrawContainer); } \
	FORCEINLINE static uint32 __PPO__DataSourceRegistry() { return STRUCT_OFFSET(UControlRig, DataSourceRegistry); } \
	FORCEINLINE static uint32 __PPO__EventQueue() { return STRUCT_OFFSET(UControlRig, EventQueue); } \
	FORCEINLINE static uint32 __PPO__Influences() { return STRUCT_OFFSET(UControlRig, Influences); } \
	FORCEINLINE static uint32 __PPO__InteractionRig() { return STRUCT_OFFSET(UControlRig, InteractionRig); } \
	FORCEINLINE static uint32 __PPO__InteractionRigClass() { return STRUCT_OFFSET(UControlRig, InteractionRigClass); } \
	FORCEINLINE static uint32 __PPO__AssetUserData() { return STRUCT_OFFSET(UControlRig, AssetUserData); }


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRig_h_43_PROLOG
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRig_h_46_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRig_h_46_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRig_h_46_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRig_h_46_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRig_h_46_INCLASS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRig_h_46_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRig_h_46_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRig_h_46_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRig_h_46_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRig_h_46_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRig_h_46_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRig_h_46_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ControlRig."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONTROLRIG_API UClass* StaticClass<class UControlRig>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Public_ControlRig_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
