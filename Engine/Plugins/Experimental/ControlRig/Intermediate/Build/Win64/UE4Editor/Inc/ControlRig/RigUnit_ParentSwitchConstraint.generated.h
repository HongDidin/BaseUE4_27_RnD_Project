// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigUnit_ParentSwitchConstraint_generated_h
#error "RigUnit_ParentSwitchConstraint.generated.h already included, missing '#pragma once' in RigUnit_ParentSwitchConstraint.h"
#endif
#define CONTROLRIG_RigUnit_ParentSwitchConstraint_generated_h


#define FRigUnit_ParentSwitchConstraint_Execute() \
	void FRigUnit_ParentSwitchConstraint::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKey& Subject, \
		const int32 ParentIndex, \
		const FRigElementKeyCollection& Parents, \
		const FTransform& InitialGlobalTransform, \
		const float Weight, \
		FTransform& Transform, \
		bool& Switched, \
		FCachedRigElement& CachedSubject, \
		FCachedRigElement& CachedParent, \
		FTransform& RelativeOffset, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Hierarchy_RigUnit_ParentSwitchConstraint_h_16_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_ParentSwitchConstraint_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKey& Subject, \
		const int32 ParentIndex, \
		const FRigElementKeyCollection& Parents, \
		const FTransform& InitialGlobalTransform, \
		const float Weight, \
		FTransform& Transform, \
		bool& Switched, \
		FCachedRigElement& CachedSubject, \
		FCachedRigElement& CachedParent, \
		FTransform& RelativeOffset, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FRigElementKey& Subject = *(FRigElementKey*)RigVMMemoryHandles[0].GetData(); \
		const int32 ParentIndex = *(int32*)RigVMMemoryHandles[1].GetData(); \
		const FRigElementKeyCollection& Parents = *(FRigElementKeyCollection*)RigVMMemoryHandles[2].GetData(); \
		const FTransform& InitialGlobalTransform = *(FTransform*)RigVMMemoryHandles[3].GetData(); \
		const float Weight = *(float*)RigVMMemoryHandles[4].GetData(); \
		FTransform& Transform = *(FTransform*)RigVMMemoryHandles[5].GetData(); \
		bool& Switched = *(bool*)RigVMMemoryHandles[6].GetData(); \
		FRigVMDynamicArray<FCachedRigElement> CachedSubject_7_Array(*((FRigVMByteArray*)RigVMMemoryHandles[7].GetData(0, false))); \
		CachedSubject_7_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedSubject = CachedSubject_7_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<FCachedRigElement> CachedParent_8_Array(*((FRigVMByteArray*)RigVMMemoryHandles[8].GetData(0, false))); \
		CachedParent_8_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedParent = CachedParent_8_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FRigVMDynamicArray<FTransform> RelativeOffset_9_Array(*((FRigVMByteArray*)RigVMMemoryHandles[9].GetData(0, false))); \
		RelativeOffset_9_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FTransform& RelativeOffset = RelativeOffset_9_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[10].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Subject, \
			ParentIndex, \
			Parents, \
			InitialGlobalTransform, \
			Weight, \
			Transform, \
			Switched, \
			CachedSubject, \
			CachedParent, \
			RelativeOffset, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnitMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_ParentSwitchConstraint>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Hierarchy_RigUnit_ParentSwitchConstraint_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
