// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Deprecated/RigUnit_AimConstraint.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_AimConstraint() {}
// Cross Module References
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_EAimMode();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_AimConstraint();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnitMutable();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FAimTarget();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_AimConstraint_WorkData();
	ANIMATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConstraintData();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
// End Cross Module References
	static UEnum* EAimMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ControlRig_EAimMode, Z_Construct_UPackage__Script_ControlRig(), TEXT("EAimMode"));
		}
		return Singleton;
	}
	template<> CONTROLRIG_API UEnum* StaticEnum<EAimMode>()
	{
		return EAimMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EAimMode(EAimMode_StaticEnum, TEXT("/Script/ControlRig"), TEXT("EAimMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ControlRig_EAimMode_Hash() { return 890019980U; }
	UEnum* Z_Construct_UEnum_ControlRig_EAimMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EAimMode"), 0, Get_Z_Construct_UEnum_ControlRig_EAimMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EAimMode::AimAtTarget", (int64)EAimMode::AimAtTarget },
				{ "EAimMode::OrientToTarget", (int64)EAimMode::OrientToTarget },
				{ "EAimMode::MAX", (int64)EAimMode::MAX },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "AimAtTarget.Comment", "/** Aim at Target Transform*/" },
				{ "AimAtTarget.Name", "EAimMode::AimAtTarget" },
				{ "AimAtTarget.ToolTip", "Aim at Target Transform" },
				{ "Comment", "/*\n ENUM: Aim Mode (Default: Aim At Target Transform )  # How to perform an aim\n Aim At Target Transforms\n Orient To Target Transforms\n */" },
				{ "MAX.Comment", "/** MAX - invalid */" },
				{ "MAX.Name", "EAimMode::MAX" },
				{ "MAX.ToolTip", "MAX - invalid" },
				{ "ModuleRelativePath", "Private/Units/Deprecated/RigUnit_AimConstraint.h" },
				{ "OrientToTarget.Comment", "/** Orient to Target Transform */" },
				{ "OrientToTarget.Name", "EAimMode::OrientToTarget" },
				{ "OrientToTarget.ToolTip", "Orient to Target Transform" },
				{ "ToolTip", "ENUM: Aim Mode (Default: Aim At Target Transform )  # How to perform an aim\nAim At Target Transforms\nOrient To Target Transforms" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ControlRig,
				nullptr,
				"EAimMode",
				"EAimMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}

static_assert(std::is_polymorphic<FRigUnit_AimConstraint>() == std::is_polymorphic<FRigUnitMutable>(), "USTRUCT FRigUnit_AimConstraint cannot be polymorphic unless super FRigUnitMutable is polymorphic");

class UScriptStruct* FRigUnit_AimConstraint::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_AimConstraint, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_AimConstraint"), sizeof(FRigUnit_AimConstraint), Get_Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_AimConstraint::Execute"), &FRigUnit_AimConstraint::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_AimConstraint>()
{
	return FRigUnit_AimConstraint::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_AimConstraint(FRigUnit_AimConstraint::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_AimConstraint"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AimConstraint
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AimConstraint()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_AimConstraint>(FName(TEXT("RigUnit_AimConstraint")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AimConstraint;
	struct Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Joint_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Joint;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_AimMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AimMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_AimMode;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_UpMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UpMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_UpMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AimVector_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AimVector;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UpVector_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_UpVector;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AimTargets_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AimTargets_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_AimTargets;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_UpTargets_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UpTargets_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_UpTargets;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorkData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WorkData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Transforms" },
		{ "Deprecated", "4.23.0" },
		{ "DisplayName", "Aim Constraint" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/RigUnit_AimConstraint.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_AimConstraint>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_Joint_MetaData[] = {
		{ "Category", "FRigUnit_AimConstraint" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/RigUnit_AimConstraint.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_Joint = { "Joint", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AimConstraint, Joint), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_Joint_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_Joint_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_AimMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_AimMode_MetaData[] = {
		{ "Category", "FRigUnit_AimConstraint" },
		{ "Comment", "//# How to perform an aim\n" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/RigUnit_AimConstraint.h" },
		{ "ToolTip", "# How to perform an aim" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_AimMode = { "AimMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AimConstraint, AimMode), Z_Construct_UEnum_ControlRig_EAimMode, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_AimMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_AimMode_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_UpMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_UpMode_MetaData[] = {
		{ "Category", "FRigUnit_AimConstraint" },
		{ "Comment", "//# How to perform an upvector stabilization\n" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/RigUnit_AimConstraint.h" },
		{ "ToolTip", "# How to perform an upvector stabilization" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_UpMode = { "UpMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AimConstraint, UpMode), Z_Construct_UEnum_ControlRig_EAimMode, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_UpMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_UpMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_AimVector_MetaData[] = {
		{ "Category", "FRigUnit_AimConstraint" },
		{ "Comment", "// # Vector in the space of Named joint which will be aligned to the aim target\n" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/RigUnit_AimConstraint.h" },
		{ "ToolTip", "# Vector in the space of Named joint which will be aligned to the aim target" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_AimVector = { "AimVector", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AimConstraint, AimVector), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_AimVector_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_AimVector_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_UpVector_MetaData[] = {
		{ "Category", "FRigUnit_AimConstraint" },
		{ "Comment", "//# Vector in the space of Named joint which will be aligned to the up target for stabilization\n" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/RigUnit_AimConstraint.h" },
		{ "ToolTip", "# Vector in the space of Named joint which will be aligned to the up target for stabilization" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_UpVector = { "UpVector", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AimConstraint, UpVector), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_UpVector_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_UpVector_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_AimTargets_Inner = { "AimTargets", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FAimTarget, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_AimTargets_MetaData[] = {
		{ "Category", "FRigUnit_AimConstraint" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/RigUnit_AimConstraint.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_AimTargets = { "AimTargets", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AimConstraint, AimTargets), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_AimTargets_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_AimTargets_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_UpTargets_Inner = { "UpTargets", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FAimTarget, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_UpTargets_MetaData[] = {
		{ "Category", "FRigUnit_AimConstraint" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/RigUnit_AimConstraint.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_UpTargets = { "UpTargets", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AimConstraint, UpTargets), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_UpTargets_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_UpTargets_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_WorkData_MetaData[] = {
		{ "Comment", "// note that Targets.Num () != ConstraintData.Num()\n" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/RigUnit_AimConstraint.h" },
		{ "ToolTip", "note that Targets.Num () != ConstraintData.Num()" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_WorkData = { "WorkData", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AimConstraint, WorkData), Z_Construct_UScriptStruct_FRigUnit_AimConstraint_WorkData, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_WorkData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_WorkData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_Joint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_AimMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_AimMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_UpMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_UpMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_AimVector,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_UpVector,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_AimTargets_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_AimTargets,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_UpTargets_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_UpTargets,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::NewProp_WorkData,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnitMutable,
		&NewStructOps,
		"RigUnit_AimConstraint",
		sizeof(FRigUnit_AimConstraint),
		alignof(FRigUnit_AimConstraint),
		Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_AimConstraint()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_AimConstraint"), sizeof(FRigUnit_AimConstraint), Get_Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AimConstraint_Hash() { return 1057848280U; }

void FRigUnit_AimConstraint::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
	FRigVMFixedArray<FAimTarget> AimTargets_5_Array(AimTargets);
	FRigVMFixedArray<FAimTarget> UpTargets_6_Array(UpTargets);
	
    StaticExecute(
		RigVMExecuteContext,
		Joint,
		AimMode,
		UpMode,
		AimVector,
		UpVector,
		AimTargets_5_Array,
		UpTargets_6_Array,
		WorkData,
		ExecuteContext,
		Context
	);
}

class UScriptStruct* FRigUnit_AimConstraint_WorkData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AimConstraint_WorkData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_AimConstraint_WorkData, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_AimConstraint_WorkData"), sizeof(FRigUnit_AimConstraint_WorkData), Get_Z_Construct_UScriptStruct_FRigUnit_AimConstraint_WorkData_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_AimConstraint_WorkData>()
{
	return FRigUnit_AimConstraint_WorkData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_AimConstraint_WorkData(FRigUnit_AimConstraint_WorkData::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_AimConstraint_WorkData"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AimConstraint_WorkData
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AimConstraint_WorkData()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_AimConstraint_WorkData>(FName(TEXT("RigUnit_AimConstraint_WorkData")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_AimConstraint_WorkData;
	struct Z_Construct_UScriptStruct_FRigUnit_AimConstraint_WorkData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ConstraintData_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConstraintData_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ConstraintData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AimConstraint_WorkData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Units/Deprecated/RigUnit_AimConstraint.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_AimConstraint_WorkData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_AimConstraint_WorkData>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_AimConstraint_WorkData_Statics::NewProp_ConstraintData_Inner = { "ConstraintData", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FConstraintData, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_AimConstraint_WorkData_Statics::NewProp_ConstraintData_MetaData[] = {
		{ "Comment", "// note that Targets.Num () != ConstraintData.Num()\n" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/RigUnit_AimConstraint.h" },
		{ "ToolTip", "note that Targets.Num () != ConstraintData.Num()" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_AimConstraint_WorkData_Statics::NewProp_ConstraintData = { "ConstraintData", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_AimConstraint_WorkData, ConstraintData), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AimConstraint_WorkData_Statics::NewProp_ConstraintData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AimConstraint_WorkData_Statics::NewProp_ConstraintData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_AimConstraint_WorkData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AimConstraint_WorkData_Statics::NewProp_ConstraintData_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_AimConstraint_WorkData_Statics::NewProp_ConstraintData,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_AimConstraint_WorkData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"RigUnit_AimConstraint_WorkData",
		sizeof(FRigUnit_AimConstraint_WorkData),
		alignof(FRigUnit_AimConstraint_WorkData),
		Z_Construct_UScriptStruct_FRigUnit_AimConstraint_WorkData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AimConstraint_WorkData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_AimConstraint_WorkData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_AimConstraint_WorkData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_AimConstraint_WorkData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AimConstraint_WorkData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_AimConstraint_WorkData"), sizeof(FRigUnit_AimConstraint_WorkData), Get_Z_Construct_UScriptStruct_FRigUnit_AimConstraint_WorkData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_AimConstraint_WorkData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_AimConstraint_WorkData_Hash() { return 2331901625U; }
class UScriptStruct* FAimTarget::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FAimTarget_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FAimTarget, Z_Construct_UPackage__Script_ControlRig(), TEXT("AimTarget"), sizeof(FAimTarget), Get_Z_Construct_UScriptStruct_FAimTarget_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FAimTarget>()
{
	return FAimTarget::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FAimTarget(FAimTarget::StaticStruct, TEXT("/Script/ControlRig"), TEXT("AimTarget"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFAimTarget
{
	FScriptStruct_ControlRig_StaticRegisterNativesFAimTarget()
	{
		UScriptStruct::DeferCppStructOps<FAimTarget>(FName(TEXT("AimTarget")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFAimTarget;
	struct Z_Construct_UScriptStruct_FAimTarget_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Weight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Weight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Transform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Transform;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AlignVector_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AlignVector;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAimTarget_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Units/Deprecated/RigUnit_AimConstraint.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FAimTarget_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FAimTarget>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAimTarget_Statics::NewProp_Weight_MetaData[] = {
		{ "Category", "FAimTarget" },
		{ "Comment", "// # Target Weight\n" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/RigUnit_AimConstraint.h" },
		{ "ToolTip", "# Target Weight" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FAimTarget_Statics::NewProp_Weight = { "Weight", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAimTarget, Weight), METADATA_PARAMS(Z_Construct_UScriptStruct_FAimTarget_Statics::NewProp_Weight_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAimTarget_Statics::NewProp_Weight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAimTarget_Statics::NewProp_Transform_MetaData[] = {
		{ "Category", "FAimTarget" },
		{ "Comment", "// # Aim at/Align to this Transform\n" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/RigUnit_AimConstraint.h" },
		{ "ToolTip", "# Aim at/Align to this Transform" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FAimTarget_Statics::NewProp_Transform = { "Transform", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAimTarget, Transform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FAimTarget_Statics::NewProp_Transform_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAimTarget_Statics::NewProp_Transform_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAimTarget_Statics::NewProp_AlignVector_MetaData[] = {
		{ "Category", "FAimTarget" },
		{ "Comment", "//# Orient To Target Transforms mode only : Vector in the space of Target Transform to which the Aim Vector will be aligned\n" },
		{ "ModuleRelativePath", "Private/Units/Deprecated/RigUnit_AimConstraint.h" },
		{ "ToolTip", "# Orient To Target Transforms mode only : Vector in the space of Target Transform to which the Aim Vector will be aligned" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FAimTarget_Statics::NewProp_AlignVector = { "AlignVector", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAimTarget, AlignVector), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FAimTarget_Statics::NewProp_AlignVector_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAimTarget_Statics::NewProp_AlignVector_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FAimTarget_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAimTarget_Statics::NewProp_Weight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAimTarget_Statics::NewProp_Transform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAimTarget_Statics::NewProp_AlignVector,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FAimTarget_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		nullptr,
		&NewStructOps,
		"AimTarget",
		sizeof(FAimTarget),
		alignof(FAimTarget),
		Z_Construct_UScriptStruct_FAimTarget_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAimTarget_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FAimTarget_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAimTarget_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FAimTarget()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FAimTarget_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("AimTarget"), sizeof(FAimTarget), Get_Z_Construct_UScriptStruct_FAimTarget_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FAimTarget_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FAimTarget_Hash() { return 4107024008U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
