// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigUnit_DebugPrimitives_generated_h
#error "RigUnit_DebugPrimitives.generated.h already included, missing '#pragma once' in RigUnit_DebugPrimitives.h"
#endif
#define CONTROLRIG_RigUnit_DebugPrimitives_generated_h


#define FRigUnit_DebugArcItemSpace_Execute() \
	void FRigUnit_DebugArcItemSpace::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FTransform& Transform, \
		const FLinearColor& Color, \
		const float Radius, \
		const float MinimumDegrees, \
		const float MaximumDegrees, \
		const float Thickness, \
		const int32 Detail, \
		const FRigElementKey& Space, \
		const FTransform& WorldOffset, \
		const bool bEnabled, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Debug_RigUnit_DebugPrimitives_h_150_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_DebugArcItemSpace_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FTransform& Transform, \
		const FLinearColor& Color, \
		const float Radius, \
		const float MinimumDegrees, \
		const float MaximumDegrees, \
		const float Thickness, \
		const int32 Detail, \
		const FRigElementKey& Space, \
		const FTransform& WorldOffset, \
		const bool bEnabled, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FTransform& Transform = *(FTransform*)RigVMMemoryHandles[0].GetData(); \
		const FLinearColor& Color = *(FLinearColor*)RigVMMemoryHandles[1].GetData(); \
		const float Radius = *(float*)RigVMMemoryHandles[2].GetData(); \
		const float MinimumDegrees = *(float*)RigVMMemoryHandles[3].GetData(); \
		const float MaximumDegrees = *(float*)RigVMMemoryHandles[4].GetData(); \
		const float Thickness = *(float*)RigVMMemoryHandles[5].GetData(); \
		const int32 Detail = *(int32*)RigVMMemoryHandles[6].GetData(); \
		const FRigElementKey& Space = *(FRigElementKey*)RigVMMemoryHandles[7].GetData(); \
		const FTransform& WorldOffset = *(FTransform*)RigVMMemoryHandles[8].GetData(); \
		const bool bEnabled = *(bool*)RigVMMemoryHandles[9].GetData(); \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[10].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Transform, \
			Color, \
			Radius, \
			MinimumDegrees, \
			MaximumDegrees, \
			Thickness, \
			Detail, \
			Space, \
			WorldOffset, \
			bEnabled, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnit_DebugBaseMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_DebugArcItemSpace>();


#define FRigUnit_DebugArc_Execute() \
	void FRigUnit_DebugArc::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FTransform& Transform, \
		const FLinearColor& Color, \
		const float Radius, \
		const float MinimumDegrees, \
		const float MaximumDegrees, \
		const float Thickness, \
		const int32 Detail, \
		const FName& Space, \
		const FTransform& WorldOffset, \
		const bool bEnabled, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Debug_RigUnit_DebugPrimitives_h_96_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_DebugArc_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FTransform& Transform, \
		const FLinearColor& Color, \
		const float Radius, \
		const float MinimumDegrees, \
		const float MaximumDegrees, \
		const float Thickness, \
		const int32 Detail, \
		const FName& Space, \
		const FTransform& WorldOffset, \
		const bool bEnabled, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FTransform& Transform = *(FTransform*)RigVMMemoryHandles[0].GetData(); \
		const FLinearColor& Color = *(FLinearColor*)RigVMMemoryHandles[1].GetData(); \
		const float Radius = *(float*)RigVMMemoryHandles[2].GetData(); \
		const float MinimumDegrees = *(float*)RigVMMemoryHandles[3].GetData(); \
		const float MaximumDegrees = *(float*)RigVMMemoryHandles[4].GetData(); \
		const float Thickness = *(float*)RigVMMemoryHandles[5].GetData(); \
		const int32 Detail = *(int32*)RigVMMemoryHandles[6].GetData(); \
		const FName& Space = *(FName*)RigVMMemoryHandles[7].GetData(); \
		const FTransform& WorldOffset = *(FTransform*)RigVMMemoryHandles[8].GetData(); \
		const bool bEnabled = *(bool*)RigVMMemoryHandles[9].GetData(); \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[10].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Transform, \
			Color, \
			Radius, \
			MinimumDegrees, \
			MaximumDegrees, \
			Thickness, \
			Detail, \
			Space, \
			WorldOffset, \
			bEnabled, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnit_DebugBaseMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_DebugArc>();


#define FRigUnit_DebugRectangleItemSpace_Execute() \
	void FRigUnit_DebugRectangleItemSpace::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FTransform& Transform, \
		const FLinearColor& Color, \
		const float Scale, \
		const float Thickness, \
		const FRigElementKey& Space, \
		const FTransform& WorldOffset, \
		const bool bEnabled, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Debug_RigUnit_DebugPrimitives_h_54_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_DebugRectangleItemSpace_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FTransform& Transform, \
		const FLinearColor& Color, \
		const float Scale, \
		const float Thickness, \
		const FRigElementKey& Space, \
		const FTransform& WorldOffset, \
		const bool bEnabled, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FTransform& Transform = *(FTransform*)RigVMMemoryHandles[0].GetData(); \
		const FLinearColor& Color = *(FLinearColor*)RigVMMemoryHandles[1].GetData(); \
		const float Scale = *(float*)RigVMMemoryHandles[2].GetData(); \
		const float Thickness = *(float*)RigVMMemoryHandles[3].GetData(); \
		const FRigElementKey& Space = *(FRigElementKey*)RigVMMemoryHandles[4].GetData(); \
		const FTransform& WorldOffset = *(FTransform*)RigVMMemoryHandles[5].GetData(); \
		const bool bEnabled = *(bool*)RigVMMemoryHandles[6].GetData(); \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[7].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Transform, \
			Color, \
			Scale, \
			Thickness, \
			Space, \
			WorldOffset, \
			bEnabled, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnit_DebugBaseMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_DebugRectangleItemSpace>();


#define FRigUnit_DebugRectangle_Execute() \
	void FRigUnit_DebugRectangle::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FTransform& Transform, \
		const FLinearColor& Color, \
		const float Scale, \
		const float Thickness, \
		const FName& Space, \
		const FTransform& WorldOffset, \
		const bool bEnabled, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Debug_RigUnit_DebugPrimitives_h_11_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_DebugRectangle_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FTransform& Transform, \
		const FLinearColor& Color, \
		const float Scale, \
		const float Thickness, \
		const FName& Space, \
		const FTransform& WorldOffset, \
		const bool bEnabled, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FTransform& Transform = *(FTransform*)RigVMMemoryHandles[0].GetData(); \
		const FLinearColor& Color = *(FLinearColor*)RigVMMemoryHandles[1].GetData(); \
		const float Scale = *(float*)RigVMMemoryHandles[2].GetData(); \
		const float Thickness = *(float*)RigVMMemoryHandles[3].GetData(); \
		const FName& Space = *(FName*)RigVMMemoryHandles[4].GetData(); \
		const FTransform& WorldOffset = *(FTransform*)RigVMMemoryHandles[5].GetData(); \
		const bool bEnabled = *(bool*)RigVMMemoryHandles[6].GetData(); \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[7].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Transform, \
			Color, \
			Scale, \
			Thickness, \
			Space, \
			WorldOffset, \
			bEnabled, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnit_DebugBaseMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_DebugRectangle>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Debug_RigUnit_DebugPrimitives_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
