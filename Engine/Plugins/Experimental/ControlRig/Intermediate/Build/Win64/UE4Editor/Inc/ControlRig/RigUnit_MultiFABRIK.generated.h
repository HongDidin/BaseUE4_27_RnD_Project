// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigUnit_MultiFABRIK_generated_h
#error "RigUnit_MultiFABRIK.generated.h already included, missing '#pragma once' in RigUnit_MultiFABRIK.h"
#endif
#define CONTROLRIG_RigUnit_MultiFABRIK_generated_h


#define FRigUnit_MultiFABRIK_Execute() \
	void FRigUnit_MultiFABRIK::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& RootBone, \
		const FRigVMFixedArray<FRigUnit_MultiFABRIK_EndEffector>& Effectors, \
		const float Precision, \
		const bool bPropagateToChildren, \
		const int32 MaxIterations, \
		FRigUnit_MultiFABRIK_WorkData& WorkData, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_MultiFABRIK_h_124_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_MultiFABRIK_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& RootBone, \
		const FRigVMFixedArray<FRigUnit_MultiFABRIK_EndEffector>& Effectors, \
		const float Precision, \
		const bool bPropagateToChildren, \
		const int32 MaxIterations, \
		FRigUnit_MultiFABRIK_WorkData& WorkData, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FName& RootBone = *(FName*)RigVMMemoryHandles[0].GetData(); \
		FRigVMFixedArray<FRigUnit_MultiFABRIK_EndEffector> Effectors((FRigUnit_MultiFABRIK_EndEffector*)RigVMMemoryHandles[1].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[2].GetData())); \
		const float Precision = *(float*)RigVMMemoryHandles[3].GetData(); \
		const bool bPropagateToChildren = *(bool*)RigVMMemoryHandles[4].GetData(); \
		const int32 MaxIterations = *(int32*)RigVMMemoryHandles[5].GetData(); \
		FRigVMDynamicArray<FRigUnit_MultiFABRIK_WorkData> WorkData_6_Array(*((FRigVMByteArray*)RigVMMemoryHandles[6].GetData(0, false))); \
		WorkData_6_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FRigUnit_MultiFABRIK_WorkData& WorkData = WorkData_6_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[7].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			RootBone, \
			Effectors, \
			Precision, \
			bPropagateToChildren, \
			MaxIterations, \
			WorkData, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnit_HighlevelBaseMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_MultiFABRIK>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_MultiFABRIK_h_103_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_MultiFABRIK_EndEffector_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_MultiFABRIK_EndEffector>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_MultiFABRIK_h_84_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_MultiFABRIK_WorkData_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_MultiFABRIK_WorkData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_MultiFABRIK_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
