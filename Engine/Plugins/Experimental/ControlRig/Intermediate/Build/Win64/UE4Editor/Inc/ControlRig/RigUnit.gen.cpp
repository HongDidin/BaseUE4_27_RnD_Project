// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Public/Units/RigUnit.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnitMutable();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FControlRigExecuteContext();
	RIGVM_API UScriptStruct* Z_Construct_UScriptStruct_FRigVMStruct();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnitMutable>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnitMutable cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnitMutable::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnitMutable_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnitMutable, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnitMutable"), sizeof(FRigUnitMutable), Get_Z_Construct_UScriptStruct_FRigUnitMutable_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnitMutable>()
{
	return FRigUnitMutable::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnitMutable(FRigUnitMutable::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnitMutable"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnitMutable
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnitMutable()
	{
		UScriptStruct::DeferCppStructOps<FRigUnitMutable>(FName(TEXT("RigUnitMutable")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnitMutable;
	struct Z_Construct_UScriptStruct_FRigUnitMutable_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExecuteContext_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ExecuteContext;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnitMutable_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "BlueprintType", "true" },
		{ "Comment", "/** Base class for all rig units that can change data */" },
		{ "ModuleRelativePath", "Public/Units/RigUnit.h" },
		{ "ToolTip", "Base class for all rig units that can change data" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnitMutable_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnitMutable>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnitMutable_Statics::NewProp_ExecuteContext_MetaData[] = {
		{ "Comment", "/*\n\x09 * This property is used to chain multiple mutable units together\n\x09 */" },
		{ "DisplayName", "Execute" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Public/Units/RigUnit.h" },
		{ "Output", "" },
		{ "ToolTip", "* This property is used to chain multiple mutable units together" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnitMutable_Statics::NewProp_ExecuteContext = { "ExecuteContext", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnitMutable, ExecuteContext), Z_Construct_UScriptStruct_FControlRigExecuteContext, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnitMutable_Statics::NewProp_ExecuteContext_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnitMutable_Statics::NewProp_ExecuteContext_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnitMutable_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnitMutable_Statics::NewProp_ExecuteContext,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnitMutable_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnitMutable",
		sizeof(FRigUnitMutable),
		alignof(FRigUnitMutable),
		Z_Construct_UScriptStruct_FRigUnitMutable_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnitMutable_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnitMutable_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnitMutable_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnitMutable()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnitMutable_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnitMutable"), sizeof(FRigUnitMutable), Get_Z_Construct_UScriptStruct_FRigUnitMutable_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnitMutable_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnitMutable_Hash() { return 3487765147U; }

static_assert(std::is_polymorphic<FRigUnit>() == std::is_polymorphic<FRigVMStruct>(), "USTRUCT FRigUnit cannot be polymorphic unless super FRigVMStruct is polymorphic");

class UScriptStruct* FRigUnit::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit"), sizeof(FRigUnit), Get_Z_Construct_UScriptStruct_FRigUnit_Hash());
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit>()
{
	return FRigUnit::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit(FRigUnit::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit>(FName(TEXT("RigUnit")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit;
	struct Z_Construct_UScriptStruct_FRigUnit_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_Statics::Struct_MetaDataParams[] = {
		{ "Abstract", "" },
		{ "BlueprintType", "true" },
		{ "Comment", "/** Base class for all rig units */" },
		{ "ModuleRelativePath", "Public/Units/RigUnit.h" },
		{ "NodeColor", "0.1 0.1 0.1" },
		{ "ToolTip", "Base class for all rig units" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigVMStruct,
		&NewStructOps,
		"RigUnit",
		sizeof(FRigUnit),
		alignof(FRigUnit),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit"), sizeof(FRigUnit), Get_Z_Construct_UScriptStruct_FRigUnit_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_Hash() { return 628366730U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
