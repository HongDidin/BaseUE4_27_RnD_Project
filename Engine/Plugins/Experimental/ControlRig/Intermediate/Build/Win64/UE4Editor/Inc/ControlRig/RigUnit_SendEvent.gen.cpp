// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Hierarchy/RigUnit_SendEvent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_SendEvent() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_SendEvent();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnitMutable();
	CONTROLRIG_API UEnum* Z_Construct_UEnum_ControlRig_ERigEvent();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigElementKey();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_SendEvent>() == std::is_polymorphic<FRigUnitMutable>(), "USTRUCT FRigUnit_SendEvent cannot be polymorphic unless super FRigUnitMutable is polymorphic");

class UScriptStruct* FRigUnit_SendEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_SendEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_SendEvent, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_SendEvent"), sizeof(FRigUnit_SendEvent), Get_Z_Construct_UScriptStruct_FRigUnit_SendEvent_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_SendEvent::Execute"), &FRigUnit_SendEvent::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_SendEvent>()
{
	return FRigUnit_SendEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_SendEvent(FRigUnit_SendEvent::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_SendEvent"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_SendEvent
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_SendEvent()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_SendEvent>(FName(TEXT("RigUnit_SendEvent")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_SendEvent;
	struct Z_Construct_UScriptStruct_FRigUnit_SendEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Event_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Event_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Event;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Item_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Item;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OffsetInSeconds_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_OffsetInSeconds;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnable_MetaData[];
#endif
		static void NewProp_bEnable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnable;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOnlyDuringInteraction_MetaData[];
#endif
		static void NewProp_bOnlyDuringInteraction_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOnlyDuringInteraction;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_SendEvent_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "/**\n * SendEvent is used to notify the engine / editor of a change that happend within the Control Rig.\n */" },
		{ "DisplayName", "Send Event" },
		{ "DocumentationPolicy", "Strict" },
		{ "Keywords", "SendEvent" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_SendEvent.h" },
		{ "PrototypeName", "Event,Notify,Notification" },
		{ "ToolTip", "SendEvent is used to notify the engine / editor of a change that happend within the Control Rig." },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_SendEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_SendEvent>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRigUnit_SendEvent_Statics::NewProp_Event_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_SendEvent_Statics::NewProp_Event_MetaData[] = {
		{ "Comment", "/**\n\x09 * The event to send to the engine\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_SendEvent.h" },
		{ "ToolTip", "The event to send to the engine" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FRigUnit_SendEvent_Statics::NewProp_Event = { "Event", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_SendEvent, Event), Z_Construct_UEnum_ControlRig_ERigEvent, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_SendEvent_Statics::NewProp_Event_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_SendEvent_Statics::NewProp_Event_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_SendEvent_Statics::NewProp_Item_MetaData[] = {
		{ "Comment", "/**\n\x09 * The item to send the event for\n\x09 */" },
		{ "ExpandByDefault", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_SendEvent.h" },
		{ "ToolTip", "The item to send the event for" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_SendEvent_Statics::NewProp_Item = { "Item", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_SendEvent, Item), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_SendEvent_Statics::NewProp_Item_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_SendEvent_Statics::NewProp_Item_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_SendEvent_Statics::NewProp_OffsetInSeconds_MetaData[] = {
		{ "Comment", "/**\n\x09 * The time offset to use for the send event\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_SendEvent.h" },
		{ "ToolTip", "The time offset to use for the send event" },
		{ "UIMax", "1000" },
		{ "UIMin", "-1000" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_SendEvent_Statics::NewProp_OffsetInSeconds = { "OffsetInSeconds", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_SendEvent, OffsetInSeconds), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_SendEvent_Statics::NewProp_OffsetInSeconds_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_SendEvent_Statics::NewProp_OffsetInSeconds_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_SendEvent_Statics::NewProp_bEnable_MetaData[] = {
		{ "Comment", "/**\n\x09 * The event will be sent if this is checked\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_SendEvent.h" },
		{ "ToolTip", "The event will be sent if this is checked" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_SendEvent_Statics::NewProp_bEnable_SetBit(void* Obj)
	{
		((FRigUnit_SendEvent*)Obj)->bEnable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_SendEvent_Statics::NewProp_bEnable = { "bEnable", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_SendEvent), &Z_Construct_UScriptStruct_FRigUnit_SendEvent_Statics::NewProp_bEnable_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_SendEvent_Statics::NewProp_bEnable_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_SendEvent_Statics::NewProp_bEnable_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_SendEvent_Statics::NewProp_bOnlyDuringInteraction_MetaData[] = {
		{ "Comment", "/**\n\x09 * The event will be sent if this only during an interaction\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Hierarchy/RigUnit_SendEvent.h" },
		{ "ToolTip", "The event will be sent if this only during an interaction" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_SendEvent_Statics::NewProp_bOnlyDuringInteraction_SetBit(void* Obj)
	{
		((FRigUnit_SendEvent*)Obj)->bOnlyDuringInteraction = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_SendEvent_Statics::NewProp_bOnlyDuringInteraction = { "bOnlyDuringInteraction", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_SendEvent), &Z_Construct_UScriptStruct_FRigUnit_SendEvent_Statics::NewProp_bOnlyDuringInteraction_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_SendEvent_Statics::NewProp_bOnlyDuringInteraction_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_SendEvent_Statics::NewProp_bOnlyDuringInteraction_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_SendEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_SendEvent_Statics::NewProp_Event_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_SendEvent_Statics::NewProp_Event,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_SendEvent_Statics::NewProp_Item,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_SendEvent_Statics::NewProp_OffsetInSeconds,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_SendEvent_Statics::NewProp_bEnable,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_SendEvent_Statics::NewProp_bOnlyDuringInteraction,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_SendEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnitMutable,
		&NewStructOps,
		"RigUnit_SendEvent",
		sizeof(FRigUnit_SendEvent),
		alignof(FRigUnit_SendEvent),
		Z_Construct_UScriptStruct_FRigUnit_SendEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_SendEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_SendEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_SendEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_SendEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_SendEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_SendEvent"), sizeof(FRigUnit_SendEvent), Get_Z_Construct_UScriptStruct_FRigUnit_SendEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_SendEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_SendEvent_Hash() { return 3941858519U; }

void FRigUnit_SendEvent::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Event,
		Item,
		OffsetInSeconds,
		bEnable,
		bOnlyDuringInteraction,
		ExecuteContext,
		Context
	);
}

PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
