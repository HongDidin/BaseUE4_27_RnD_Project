// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIGDEVELOPER_ControlRigGraphSchema_generated_h
#error "ControlRigGraphSchema.generated.h already included, missing '#pragma once' in ControlRigGraphSchema.h"
#endif
#define CONTROLRIGDEVELOPER_ControlRigGraphSchema_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphSchema_h_45_SPARSE_DATA
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphSchema_h_45_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphSchema_h_45_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphSchema_h_45_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUControlRigGraphSchema(); \
	friend struct Z_Construct_UClass_UControlRigGraphSchema_Statics; \
public: \
	DECLARE_CLASS(UControlRigGraphSchema, UEdGraphSchema, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRigDeveloper"), NO_API) \
	DECLARE_SERIALIZER(UControlRigGraphSchema)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphSchema_h_45_INCLASS \
private: \
	static void StaticRegisterNativesUControlRigGraphSchema(); \
	friend struct Z_Construct_UClass_UControlRigGraphSchema_Statics; \
public: \
	DECLARE_CLASS(UControlRigGraphSchema, UEdGraphSchema, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRigDeveloper"), NO_API) \
	DECLARE_SERIALIZER(UControlRigGraphSchema)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphSchema_h_45_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UControlRigGraphSchema(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigGraphSchema) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigGraphSchema); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigGraphSchema); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigGraphSchema(UControlRigGraphSchema&&); \
	NO_API UControlRigGraphSchema(const UControlRigGraphSchema&); \
public:


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphSchema_h_45_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigGraphSchema(UControlRigGraphSchema&&); \
	NO_API UControlRigGraphSchema(const UControlRigGraphSchema&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigGraphSchema); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigGraphSchema); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UControlRigGraphSchema)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphSchema_h_45_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphSchema_h_42_PROLOG
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphSchema_h_45_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphSchema_h_45_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphSchema_h_45_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphSchema_h_45_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphSchema_h_45_INCLASS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphSchema_h_45_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphSchema_h_45_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphSchema_h_45_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphSchema_h_45_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphSchema_h_45_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphSchema_h_45_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphSchema_h_45_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONTROLRIGDEVELOPER_API UClass* StaticClass<class UControlRigGraphSchema>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRigDeveloper_Public_Graph_ControlRigGraphSchema_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
