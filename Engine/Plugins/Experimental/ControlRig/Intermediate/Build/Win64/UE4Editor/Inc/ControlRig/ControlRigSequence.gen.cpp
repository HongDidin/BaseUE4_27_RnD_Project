// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Public/Sequencer/ControlRigSequence.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeControlRigSequence() {}
// Cross Module References
	CONTROLRIG_API UClass* Z_Construct_UClass_UControlRigSequence_NoRegister();
	CONTROLRIG_API UClass* Z_Construct_UClass_UControlRigSequence();
	LEVELSEQUENCE_API UClass* Z_Construct_UClass_ULevelSequence();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	ENGINE_API UClass* Z_Construct_UClass_UAnimSequence_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USkeletalMesh_NoRegister();
// End Cross Module References
	void UControlRigSequence::StaticRegisterNativesUControlRigSequence()
	{
	}
	UClass* Z_Construct_UClass_UControlRigSequence_NoRegister()
	{
		return UControlRigSequence::StaticClass();
	}
	struct Z_Construct_UClass_UControlRigSequence_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastExportedToAnimationSequence_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_LastExportedToAnimationSequence;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastExportedUsingSkeletalMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_LastExportedUsingSkeletalMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastExportedFrameRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_LastExportedFrameRate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UControlRigSequence_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ULevelSequence,
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigSequence_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "DevelopmentStatus", "Experimental" },
		{ "IncludePath", "Sequencer/ControlRigSequence.h" },
		{ "ModuleRelativePath", "Public/Sequencer/ControlRigSequence.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigSequence_Statics::NewProp_LastExportedToAnimationSequence_MetaData[] = {
		{ "Comment", "/** The last animation sequence this control rig sequence was exported to */" },
		{ "ModuleRelativePath", "Public/Sequencer/ControlRigSequence.h" },
		{ "ToolTip", "The last animation sequence this control rig sequence was exported to" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UControlRigSequence_Statics::NewProp_LastExportedToAnimationSequence = { "LastExportedToAnimationSequence", nullptr, (EPropertyFlags)0x0014010000000000, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigSequence, LastExportedToAnimationSequence), Z_Construct_UClass_UAnimSequence_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UControlRigSequence_Statics::NewProp_LastExportedToAnimationSequence_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigSequence_Statics::NewProp_LastExportedToAnimationSequence_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigSequence_Statics::NewProp_LastExportedUsingSkeletalMesh_MetaData[] = {
		{ "Comment", "/** The skeletal mesh that was used the last time we exported this sequence */" },
		{ "ModuleRelativePath", "Public/Sequencer/ControlRigSequence.h" },
		{ "ToolTip", "The skeletal mesh that was used the last time we exported this sequence" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UControlRigSequence_Statics::NewProp_LastExportedUsingSkeletalMesh = { "LastExportedUsingSkeletalMesh", nullptr, (EPropertyFlags)0x0014010000000000, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigSequence, LastExportedUsingSkeletalMesh), Z_Construct_UClass_USkeletalMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UControlRigSequence_Statics::NewProp_LastExportedUsingSkeletalMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigSequence_Statics::NewProp_LastExportedUsingSkeletalMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UControlRigSequence_Statics::NewProp_LastExportedFrameRate_MetaData[] = {
		{ "Comment", "/** The frame rate that was used the last time we exported this sequence */" },
		{ "ModuleRelativePath", "Public/Sequencer/ControlRigSequence.h" },
		{ "ToolTip", "The frame rate that was used the last time we exported this sequence" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UControlRigSequence_Statics::NewProp_LastExportedFrameRate = { "LastExportedFrameRate", nullptr, (EPropertyFlags)0x0010010000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UControlRigSequence, LastExportedFrameRate), METADATA_PARAMS(Z_Construct_UClass_UControlRigSequence_Statics::NewProp_LastExportedFrameRate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigSequence_Statics::NewProp_LastExportedFrameRate_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UControlRigSequence_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigSequence_Statics::NewProp_LastExportedToAnimationSequence,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigSequence_Statics::NewProp_LastExportedUsingSkeletalMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UControlRigSequence_Statics::NewProp_LastExportedFrameRate,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UControlRigSequence_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UControlRigSequence>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UControlRigSequence_Statics::ClassParams = {
		&UControlRigSequence::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UControlRigSequence_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigSequence_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UControlRigSequence_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UControlRigSequence_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UControlRigSequence()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UControlRigSequence_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UControlRigSequence, 1206933014);
	template<> CONTROLRIG_API UClass* StaticClass<UControlRigSequence>()
	{
		return UControlRigSequence::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UControlRigSequence(Z_Construct_UClass_UControlRigSequence, &UControlRigSequence::StaticClass, TEXT("/Script/ControlRig"), TEXT("UControlRigSequence"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UControlRigSequence);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
