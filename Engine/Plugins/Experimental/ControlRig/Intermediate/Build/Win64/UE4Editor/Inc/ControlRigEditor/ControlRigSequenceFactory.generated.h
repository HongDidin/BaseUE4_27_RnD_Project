// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIGEDITOR_ControlRigSequenceFactory_generated_h
#error "ControlRigSequenceFactory.generated.h already included, missing '#pragma once' in ControlRigSequenceFactory.h"
#endif
#define CONTROLRIGEDITOR_ControlRigSequenceFactory_generated_h

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequenceFactory_h_16_SPARSE_DATA
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequenceFactory_h_16_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequenceFactory_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequenceFactory_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUControlRigSequenceFactory(); \
	friend struct Z_Construct_UClass_UControlRigSequenceFactory_Statics; \
public: \
	DECLARE_CLASS(UControlRigSequenceFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRigEditor"), NO_API) \
	DECLARE_SERIALIZER(UControlRigSequenceFactory)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequenceFactory_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUControlRigSequenceFactory(); \
	friend struct Z_Construct_UClass_UControlRigSequenceFactory_Statics; \
public: \
	DECLARE_CLASS(UControlRigSequenceFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ControlRigEditor"), NO_API) \
	DECLARE_SERIALIZER(UControlRigSequenceFactory)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequenceFactory_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UControlRigSequenceFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigSequenceFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigSequenceFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigSequenceFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigSequenceFactory(UControlRigSequenceFactory&&); \
	NO_API UControlRigSequenceFactory(const UControlRigSequenceFactory&); \
public:


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequenceFactory_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UControlRigSequenceFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UControlRigSequenceFactory(UControlRigSequenceFactory&&); \
	NO_API UControlRigSequenceFactory(const UControlRigSequenceFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UControlRigSequenceFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UControlRigSequenceFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UControlRigSequenceFactory)


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequenceFactory_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequenceFactory_h_13_PROLOG
#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequenceFactory_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequenceFactory_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequenceFactory_h_16_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequenceFactory_h_16_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequenceFactory_h_16_INCLASS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequenceFactory_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequenceFactory_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequenceFactory_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequenceFactory_h_16_SPARSE_DATA \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequenceFactory_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequenceFactory_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequenceFactory_h_16_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ControlRigSequenceFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONTROLRIGEDITOR_API UClass* StaticClass<class UControlRigSequenceFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRigEditor_Private_Sequencer_ControlRigSequenceFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
