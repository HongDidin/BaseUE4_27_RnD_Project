// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Collision/RigUnit_WorldCollision.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_WorldCollision() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	ENGINE_API UEnum* Z_Construct_UEnum_Engine_ECollisionChannel();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_SphereTraceWorld>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_SphereTraceWorld cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_SphereTraceWorld::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_SphereTraceWorld"), sizeof(FRigUnit_SphereTraceWorld), Get_Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_SphereTraceWorld::Execute"), &FRigUnit_SphereTraceWorld::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_SphereTraceWorld>()
{
	return FRigUnit_SphereTraceWorld::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_SphereTraceWorld(FRigUnit_SphereTraceWorld::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_SphereTraceWorld"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_SphereTraceWorld
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_SphereTraceWorld()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_SphereTraceWorld>(FName(TEXT("RigUnit_SphereTraceWorld")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_SphereTraceWorld;
	struct Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Start_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Start;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_End_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_End;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Channel_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Channel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Radius_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Radius;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bHit_MetaData[];
#endif
		static void NewProp_bHit_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bHit;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HitLocation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HitLocation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HitNormal_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HitNormal;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Collision" },
		{ "Comment", "/**\n * Sweeps a sphere against the world and return the first blocking hit using a specific channel\n */" },
		{ "DisplayName", "Sphere Trace" },
		{ "DocumentationPolicy", "Strict" },
		{ "Keywords", "Sweep,Raytrace,Collision,Collide,Trace" },
		{ "ModuleRelativePath", "Private/Units/Collision/RigUnit_WorldCollision.h" },
		{ "NodeColor", "0.2 0.4 0.7" },
		{ "ToolTip", "Sweeps a sphere against the world and return the first blocking hit using a specific channel" },
		{ "Varying", "" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_SphereTraceWorld>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::NewProp_Start_MetaData[] = {
		{ "Comment", "/** Start of the trace in rig / global space */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Collision/RigUnit_WorldCollision.h" },
		{ "ToolTip", "Start of the trace in rig / global space" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::NewProp_Start = { "Start", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_SphereTraceWorld, Start), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::NewProp_Start_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::NewProp_Start_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::NewProp_End_MetaData[] = {
		{ "Comment", "/** End of the trace in rig / global space */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Collision/RigUnit_WorldCollision.h" },
		{ "ToolTip", "End of the trace in rig / global space" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::NewProp_End = { "End", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_SphereTraceWorld, End), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::NewProp_End_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::NewProp_End_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::NewProp_Channel_MetaData[] = {
		{ "Comment", "/** The 'channel' that this trace is in, used to determine which components to hit */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Collision/RigUnit_WorldCollision.h" },
		{ "ToolTip", "The 'channel' that this trace is in, used to determine which components to hit" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::NewProp_Channel = { "Channel", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_SphereTraceWorld, Channel), Z_Construct_UEnum_Engine_ECollisionChannel, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::NewProp_Channel_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::NewProp_Channel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::NewProp_Radius_MetaData[] = {
		{ "Comment", "/** Radius of the sphere to use for sweeping / tracing */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/Units/Collision/RigUnit_WorldCollision.h" },
		{ "ToolTip", "Radius of the sphere to use for sweeping / tracing" },
		{ "UIMax", "100.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::NewProp_Radius = { "Radius", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_SphereTraceWorld, Radius), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::NewProp_Radius_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::NewProp_Radius_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::NewProp_bHit_MetaData[] = {
		{ "Comment", "/** Returns true if there was a hit */" },
		{ "ModuleRelativePath", "Private/Units/Collision/RigUnit_WorldCollision.h" },
		{ "Output", "" },
		{ "ToolTip", "Returns true if there was a hit" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::NewProp_bHit_SetBit(void* Obj)
	{
		((FRigUnit_SphereTraceWorld*)Obj)->bHit = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::NewProp_bHit = { "bHit", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_SphereTraceWorld), &Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::NewProp_bHit_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::NewProp_bHit_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::NewProp_bHit_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::NewProp_HitLocation_MetaData[] = {
		{ "Comment", "/** Hit location in rig / global Space */" },
		{ "ModuleRelativePath", "Private/Units/Collision/RigUnit_WorldCollision.h" },
		{ "Output", "" },
		{ "ToolTip", "Hit location in rig / global Space" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::NewProp_HitLocation = { "HitLocation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_SphereTraceWorld, HitLocation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::NewProp_HitLocation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::NewProp_HitLocation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::NewProp_HitNormal_MetaData[] = {
		{ "Comment", "/** Hit normal in rig / global Space */" },
		{ "ModuleRelativePath", "Private/Units/Collision/RigUnit_WorldCollision.h" },
		{ "Output", "" },
		{ "ToolTip", "Hit normal in rig / global Space" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::NewProp_HitNormal = { "HitNormal", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_SphereTraceWorld, HitNormal), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::NewProp_HitNormal_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::NewProp_HitNormal_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::NewProp_Start,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::NewProp_End,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::NewProp_Channel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::NewProp_Radius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::NewProp_bHit,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::NewProp_HitLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::NewProp_HitNormal,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_SphereTraceWorld",
		sizeof(FRigUnit_SphereTraceWorld),
		alignof(FRigUnit_SphereTraceWorld),
		Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_SphereTraceWorld"), sizeof(FRigUnit_SphereTraceWorld), Get_Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_SphereTraceWorld_Hash() { return 2740845886U; }

void FRigUnit_SphereTraceWorld::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Start,
		End,
		Channel,
		Radius,
		bHit,
		HitLocation,
		HitNormal,
		Context
	);
}

PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
