// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Private/Units/Execution/RigUnit_IsInteracting.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_IsInteracting() {}
// Cross Module References
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_IsInteracting();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_IsInteracting>() == std::is_polymorphic<FRigUnit>(), "USTRUCT FRigUnit_IsInteracting cannot be polymorphic unless super FRigUnit is polymorphic");

class UScriptStruct* FRigUnit_IsInteracting::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTROLRIG_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_IsInteracting_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_IsInteracting, Z_Construct_UPackage__Script_ControlRig(), TEXT("RigUnit_IsInteracting"), sizeof(FRigUnit_IsInteracting), Get_Z_Construct_UScriptStruct_FRigUnit_IsInteracting_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_IsInteracting::Execute"), &FRigUnit_IsInteracting::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> CONTROLRIG_API UScriptStruct* StaticStruct<FRigUnit_IsInteracting>()
{
	return FRigUnit_IsInteracting::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_IsInteracting(FRigUnit_IsInteracting::StaticStruct, TEXT("/Script/ControlRig"), TEXT("RigUnit_IsInteracting"), false, nullptr, nullptr);
static struct FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_IsInteracting
{
	FScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_IsInteracting()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_IsInteracting>(FName(TEXT("RigUnit_IsInteracting")));
	}
} ScriptStruct_ControlRig_StaticRegisterNativesFRigUnit_IsInteracting;
	struct Z_Construct_UScriptStruct_FRigUnit_IsInteracting_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsInteracting_MetaData[];
#endif
		static void NewProp_bIsInteracting_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsInteracting;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_IsInteracting_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Execution" },
		{ "Comment", "/**\n * Returns true if the Control Rig is being interacted\n */" },
		{ "DisplayName", "Is Interacting" },
		{ "Keywords", "Gizmo,Manipulation,Interaction" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_IsInteracting.h" },
		{ "NodeColor", "0.1 0.1 0.1" },
		{ "TitleColor", "1 0 0" },
		{ "ToolTip", "Returns true if the Control Rig is being interacted" },
		{ "Varying", "" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_IsInteracting_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_IsInteracting>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_IsInteracting_Statics::NewProp_bIsInteracting_MetaData[] = {
		{ "Category", "Execution" },
		{ "Comment", "// The execution result\n" },
		{ "DisplayName", "Interacting" },
		{ "ModuleRelativePath", "Private/Units/Execution/RigUnit_IsInteracting.h" },
		{ "Output", "" },
		{ "ToolTip", "The execution result" },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_IsInteracting_Statics::NewProp_bIsInteracting_SetBit(void* Obj)
	{
		((FRigUnit_IsInteracting*)Obj)->bIsInteracting = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_IsInteracting_Statics::NewProp_bIsInteracting = { "bIsInteracting", nullptr, (EPropertyFlags)0x0010000000002001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_IsInteracting), &Z_Construct_UScriptStruct_FRigUnit_IsInteracting_Statics::NewProp_bIsInteracting_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_IsInteracting_Statics::NewProp_bIsInteracting_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_IsInteracting_Statics::NewProp_bIsInteracting_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_IsInteracting_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_IsInteracting_Statics::NewProp_bIsInteracting,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_IsInteracting_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
		Z_Construct_UScriptStruct_FRigUnit,
		&NewStructOps,
		"RigUnit_IsInteracting",
		sizeof(FRigUnit_IsInteracting),
		alignof(FRigUnit_IsInteracting),
		Z_Construct_UScriptStruct_FRigUnit_IsInteracting_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_IsInteracting_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_IsInteracting_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_IsInteracting_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_IsInteracting()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_IsInteracting_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ControlRig();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_IsInteracting"), sizeof(FRigUnit_IsInteracting), Get_Z_Construct_UScriptStruct_FRigUnit_IsInteracting_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_IsInteracting_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_IsInteracting_Hash() { return 420543476U; }

void FRigUnit_IsInteracting::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		bIsInteracting,
		Context
	);
}

PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
