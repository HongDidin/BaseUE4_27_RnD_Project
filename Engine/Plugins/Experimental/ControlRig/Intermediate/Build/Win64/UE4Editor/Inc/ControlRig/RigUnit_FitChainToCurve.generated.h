// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigUnit_FitChainToCurve_generated_h
#error "RigUnit_FitChainToCurve.generated.h already included, missing '#pragma once' in RigUnit_FitChainToCurve.h"
#endif
#define CONTROLRIG_RigUnit_FitChainToCurve_generated_h


#define FRigUnit_FitChainToCurvePerItem_Execute() \
	void FRigUnit_FitChainToCurvePerItem::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKeyCollection& Items, \
		const FCRFourPointBezier& Bezier, \
		const EControlRigCurveAlignment Alignment, \
		const float Minimum, \
		const float Maximum, \
		const int32 SamplingPrecision, \
		const FVector& PrimaryAxis, \
		const FVector& SecondaryAxis, \
		const FVector& PoleVectorPosition, \
		const FRigVMFixedArray<FRigUnit_FitChainToCurve_Rotation>& Rotations, \
		const EControlRigAnimEasingType RotationEaseType, \
		const float Weight, \
		const bool bPropagateToChildren, \
		const FRigUnit_FitChainToCurve_DebugSettings& DebugSettings, \
		FRigUnit_FitChainToCurve_WorkData& WorkData, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_FitChainToCurve_h_257_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_FitChainToCurvePerItem_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKeyCollection& Items, \
		const FCRFourPointBezier& Bezier, \
		const EControlRigCurveAlignment Alignment, \
		const float Minimum, \
		const float Maximum, \
		const int32 SamplingPrecision, \
		const FVector& PrimaryAxis, \
		const FVector& SecondaryAxis, \
		const FVector& PoleVectorPosition, \
		const FRigVMFixedArray<FRigUnit_FitChainToCurve_Rotation>& Rotations, \
		const EControlRigAnimEasingType RotationEaseType, \
		const float Weight, \
		const bool bPropagateToChildren, \
		const FRigUnit_FitChainToCurve_DebugSettings& DebugSettings, \
		FRigUnit_FitChainToCurve_WorkData& WorkData, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FRigElementKeyCollection& Items = *(FRigElementKeyCollection*)RigVMMemoryHandles[0].GetData(); \
		const FCRFourPointBezier& Bezier = *(FCRFourPointBezier*)RigVMMemoryHandles[1].GetData(); \
		EControlRigCurveAlignment Alignment = (EControlRigCurveAlignment)*(uint8*)RigVMMemoryHandles[2].GetData(); \
		const float Minimum = *(float*)RigVMMemoryHandles[3].GetData(); \
		const float Maximum = *(float*)RigVMMemoryHandles[4].GetData(); \
		const int32 SamplingPrecision = *(int32*)RigVMMemoryHandles[5].GetData(); \
		const FVector& PrimaryAxis = *(FVector*)RigVMMemoryHandles[6].GetData(); \
		const FVector& SecondaryAxis = *(FVector*)RigVMMemoryHandles[7].GetData(); \
		const FVector& PoleVectorPosition = *(FVector*)RigVMMemoryHandles[8].GetData(); \
		FRigVMFixedArray<FRigUnit_FitChainToCurve_Rotation> Rotations((FRigUnit_FitChainToCurve_Rotation*)RigVMMemoryHandles[9].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[10].GetData())); \
		EControlRigAnimEasingType RotationEaseType = (EControlRigAnimEasingType)*(uint8*)RigVMMemoryHandles[11].GetData(); \
		const float Weight = *(float*)RigVMMemoryHandles[12].GetData(); \
		const bool bPropagateToChildren = *(bool*)RigVMMemoryHandles[13].GetData(); \
		const FRigUnit_FitChainToCurve_DebugSettings& DebugSettings = *(FRigUnit_FitChainToCurve_DebugSettings*)RigVMMemoryHandles[14].GetData(); \
		FRigVMDynamicArray<FRigUnit_FitChainToCurve_WorkData> WorkData_15_Array(*((FRigVMByteArray*)RigVMMemoryHandles[15].GetData(0, false))); \
		WorkData_15_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FRigUnit_FitChainToCurve_WorkData& WorkData = WorkData_15_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[16].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Items, \
			Bezier, \
			Alignment, \
			Minimum, \
			Maximum, \
			SamplingPrecision, \
			PrimaryAxis, \
			SecondaryAxis, \
			PoleVectorPosition, \
			Rotations, \
			RotationEaseType, \
			Weight, \
			bPropagateToChildren, \
			DebugSettings, \
			WorkData, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnit_HighlevelBaseMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_FitChainToCurvePerItem>();


#define FRigUnit_FitChainToCurve_Execute() \
	void FRigUnit_FitChainToCurve::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& StartBone, \
		const FName& EndBone, \
		const FCRFourPointBezier& Bezier, \
		const EControlRigCurveAlignment Alignment, \
		const float Minimum, \
		const float Maximum, \
		const int32 SamplingPrecision, \
		const FVector& PrimaryAxis, \
		const FVector& SecondaryAxis, \
		const FVector& PoleVectorPosition, \
		const FRigVMFixedArray<FRigUnit_FitChainToCurve_Rotation>& Rotations, \
		const EControlRigAnimEasingType RotationEaseType, \
		const float Weight, \
		const bool bPropagateToChildren, \
		const FRigUnit_FitChainToCurve_DebugSettings& DebugSettings, \
		FRigUnit_FitChainToCurve_WorkData& WorkData, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_FitChainToCurve_h_133_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_FitChainToCurve_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& StartBone, \
		const FName& EndBone, \
		const FCRFourPointBezier& Bezier, \
		const EControlRigCurveAlignment Alignment, \
		const float Minimum, \
		const float Maximum, \
		const int32 SamplingPrecision, \
		const FVector& PrimaryAxis, \
		const FVector& SecondaryAxis, \
		const FVector& PoleVectorPosition, \
		const FRigVMFixedArray<FRigUnit_FitChainToCurve_Rotation>& Rotations, \
		const EControlRigAnimEasingType RotationEaseType, \
		const float Weight, \
		const bool bPropagateToChildren, \
		const FRigUnit_FitChainToCurve_DebugSettings& DebugSettings, \
		FRigUnit_FitChainToCurve_WorkData& WorkData, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FName& StartBone = *(FName*)RigVMMemoryHandles[0].GetData(); \
		const FName& EndBone = *(FName*)RigVMMemoryHandles[1].GetData(); \
		const FCRFourPointBezier& Bezier = *(FCRFourPointBezier*)RigVMMemoryHandles[2].GetData(); \
		EControlRigCurveAlignment Alignment = (EControlRigCurveAlignment)*(uint8*)RigVMMemoryHandles[3].GetData(); \
		const float Minimum = *(float*)RigVMMemoryHandles[4].GetData(); \
		const float Maximum = *(float*)RigVMMemoryHandles[5].GetData(); \
		const int32 SamplingPrecision = *(int32*)RigVMMemoryHandles[6].GetData(); \
		const FVector& PrimaryAxis = *(FVector*)RigVMMemoryHandles[7].GetData(); \
		const FVector& SecondaryAxis = *(FVector*)RigVMMemoryHandles[8].GetData(); \
		const FVector& PoleVectorPosition = *(FVector*)RigVMMemoryHandles[9].GetData(); \
		FRigVMFixedArray<FRigUnit_FitChainToCurve_Rotation> Rotations((FRigUnit_FitChainToCurve_Rotation*)RigVMMemoryHandles[10].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[11].GetData())); \
		EControlRigAnimEasingType RotationEaseType = (EControlRigAnimEasingType)*(uint8*)RigVMMemoryHandles[12].GetData(); \
		const float Weight = *(float*)RigVMMemoryHandles[13].GetData(); \
		const bool bPropagateToChildren = *(bool*)RigVMMemoryHandles[14].GetData(); \
		const FRigUnit_FitChainToCurve_DebugSettings& DebugSettings = *(FRigUnit_FitChainToCurve_DebugSettings*)RigVMMemoryHandles[15].GetData(); \
		FRigVMDynamicArray<FRigUnit_FitChainToCurve_WorkData> WorkData_16_Array(*((FRigVMByteArray*)RigVMMemoryHandles[16].GetData(0, false))); \
		WorkData_16_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FRigUnit_FitChainToCurve_WorkData& WorkData = WorkData_16_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[17].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			StartBone, \
			EndBone, \
			Bezier, \
			Alignment, \
			Minimum, \
			Maximum, \
			SamplingPrecision, \
			PrimaryAxis, \
			SecondaryAxis, \
			PoleVectorPosition, \
			Rotations, \
			RotationEaseType, \
			Weight, \
			bPropagateToChildren, \
			DebugSettings, \
			WorkData, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnit_HighlevelBaseMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_FitChainToCurve>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_FitChainToCurve_h_88_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_FitChainToCurve_WorkData_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_FitChainToCurve_WorkData>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_FitChainToCurve_h_43_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_FitChainToCurve_DebugSettings_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_FitChainToCurve_DebugSettings>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_FitChainToCurve_h_19_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_FitChainToCurve_Rotation_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_FitChainToCurve_Rotation>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Highlevel_Hierarchy_RigUnit_FitChainToCurve_h


#define FOREACH_ENUM_ECONTROLRIGCURVEALIGNMENT(op) \
	op(EControlRigCurveAlignment::Front) \
	op(EControlRigCurveAlignment::Stretched) 

enum class EControlRigCurveAlignment : uint8;
template<> CONTROLRIG_API UEnum* StaticEnum<EControlRigCurveAlignment>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
