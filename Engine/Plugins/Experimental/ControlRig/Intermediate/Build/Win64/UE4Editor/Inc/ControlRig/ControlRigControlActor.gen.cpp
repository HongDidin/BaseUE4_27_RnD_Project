// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ControlRig/Public/ControlRigControlActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeControlRigControlActor() {}
// Cross Module References
	CONTROLRIG_API UClass* Z_Construct_UClass_AControlRigControlActor_NoRegister();
	CONTROLRIG_API UClass* Z_Construct_UClass_AControlRigControlActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_ControlRig();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	CONTROLRIG_API UClass* Z_Construct_UClass_UControlRig_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(AControlRigControlActor::execRefresh)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Refresh();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AControlRigControlActor::execClear)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Clear();
		P_NATIVE_END;
	}
	void AControlRigControlActor::StaticRegisterNativesAControlRigControlActor()
	{
		UClass* Class = AControlRigControlActor::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Clear", &AControlRigControlActor::execClear },
			{ "Refresh", &AControlRigControlActor::execRefresh },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AControlRigControlActor_Clear_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AControlRigControlActor_Clear_Statics::Function_MetaDataParams[] = {
		{ "Category", "Control Actor" },
		{ "ModuleRelativePath", "Public/ControlRigControlActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AControlRigControlActor_Clear_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AControlRigControlActor, nullptr, "Clear", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AControlRigControlActor_Clear_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AControlRigControlActor_Clear_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AControlRigControlActor_Clear()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AControlRigControlActor_Clear_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AControlRigControlActor_Refresh_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AControlRigControlActor_Refresh_Statics::Function_MetaDataParams[] = {
		{ "Category", "Control Actor" },
		{ "ModuleRelativePath", "Public/ControlRigControlActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AControlRigControlActor_Refresh_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AControlRigControlActor, nullptr, "Refresh", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AControlRigControlActor_Refresh_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AControlRigControlActor_Refresh_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AControlRigControlActor_Refresh()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AControlRigControlActor_Refresh_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AControlRigControlActor_NoRegister()
	{
		return AControlRigControlActor::StaticClass();
	}
	struct Z_Construct_UClass_AControlRigControlActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActorToTrack_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ActorToTrack;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ControlRigClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ControlRigClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRefreshOnTick_MetaData[];
#endif
		static void NewProp_bRefreshOnTick_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRefreshOnTick;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsSelectable_MetaData[];
#endif
		static void NewProp_bIsSelectable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsSelectable;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaterialOverride_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MaterialOverride;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ColorParameter_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ColorParameter;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCastShadows_MetaData[];
#endif
		static void NewProp_bCastShadows_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCastShadows;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActorRootComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ActorRootComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ControlRig_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ControlRig;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ControlNames_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ControlNames_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ControlNames;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GizmoTransforms_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GizmoTransforms_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_GizmoTransforms;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Components_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Components_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Components;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Materials_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Materials_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Materials;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ColorParameterName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ColorParameterName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AControlRigControlActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_ControlRig,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AControlRigControlActor_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AControlRigControlActor_Clear, "Clear" }, // 3543428814
		{ &Z_Construct_UFunction_AControlRigControlActor_Refresh, "Refresh" }, // 2808148137
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AControlRigControlActor_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "DisplayName", "Control Display Actor" },
		{ "IncludePath", "ControlRigControlActor.h" },
		{ "ModuleRelativePath", "Public/ControlRigControlActor.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_ActorToTrack_MetaData[] = {
		{ "Category", "Control Actor" },
		{ "ModuleRelativePath", "Public/ControlRigControlActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_ActorToTrack = { "ActorToTrack", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AControlRigControlActor, ActorToTrack), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_ActorToTrack_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_ActorToTrack_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_ControlRigClass_MetaData[] = {
		{ "Category", "Control Actor" },
		{ "ModuleRelativePath", "Public/ControlRigControlActor.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_ControlRigClass = { "ControlRigClass", nullptr, (EPropertyFlags)0x0014000000000005, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AControlRigControlActor, ControlRigClass), Z_Construct_UClass_UControlRig_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_ControlRigClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_ControlRigClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_bRefreshOnTick_MetaData[] = {
		{ "Category", "Control Actor" },
		{ "ModuleRelativePath", "Public/ControlRigControlActor.h" },
	};
#endif
	void Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_bRefreshOnTick_SetBit(void* Obj)
	{
		((AControlRigControlActor*)Obj)->bRefreshOnTick = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_bRefreshOnTick = { "bRefreshOnTick", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AControlRigControlActor), &Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_bRefreshOnTick_SetBit, METADATA_PARAMS(Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_bRefreshOnTick_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_bRefreshOnTick_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_bIsSelectable_MetaData[] = {
		{ "Category", "Control Actor" },
		{ "ModuleRelativePath", "Public/ControlRigControlActor.h" },
	};
#endif
	void Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_bIsSelectable_SetBit(void* Obj)
	{
		((AControlRigControlActor*)Obj)->bIsSelectable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_bIsSelectable = { "bIsSelectable", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AControlRigControlActor), &Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_bIsSelectable_SetBit, METADATA_PARAMS(Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_bIsSelectable_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_bIsSelectable_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_MaterialOverride_MetaData[] = {
		{ "Category", "Materials" },
		{ "ModuleRelativePath", "Public/ControlRigControlActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_MaterialOverride = { "MaterialOverride", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AControlRigControlActor, MaterialOverride), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_MaterialOverride_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_MaterialOverride_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_ColorParameter_MetaData[] = {
		{ "Category", "Materials" },
		{ "ModuleRelativePath", "Public/ControlRigControlActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_ColorParameter = { "ColorParameter", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AControlRigControlActor, ColorParameter), METADATA_PARAMS(Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_ColorParameter_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_ColorParameter_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_bCastShadows_MetaData[] = {
		{ "Category", "Materials" },
		{ "ModuleRelativePath", "Public/ControlRigControlActor.h" },
	};
#endif
	void Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_bCastShadows_SetBit(void* Obj)
	{
		((AControlRigControlActor*)Obj)->bCastShadows = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_bCastShadows = { "bCastShadows", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AControlRigControlActor), &Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_bCastShadows_SetBit, METADATA_PARAMS(Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_bCastShadows_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_bCastShadows_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_ActorRootComponent_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/ControlRigControlActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_ActorRootComponent = { "ActorRootComponent", nullptr, (EPropertyFlags)0x0040000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AControlRigControlActor, ActorRootComponent), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_ActorRootComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_ActorRootComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_ControlRig_MetaData[] = {
		{ "ModuleRelativePath", "Public/ControlRigControlActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_ControlRig = { "ControlRig", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AControlRigControlActor, ControlRig), Z_Construct_UClass_UControlRig_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_ControlRig_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_ControlRig_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_ControlNames_Inner = { "ControlNames", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_ControlNames_MetaData[] = {
		{ "ModuleRelativePath", "Public/ControlRigControlActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_ControlNames = { "ControlNames", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AControlRigControlActor, ControlNames), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_ControlNames_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_ControlNames_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_GizmoTransforms_Inner = { "GizmoTransforms", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_GizmoTransforms_MetaData[] = {
		{ "ModuleRelativePath", "Public/ControlRigControlActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_GizmoTransforms = { "GizmoTransforms", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AControlRigControlActor, GizmoTransforms), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_GizmoTransforms_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_GizmoTransforms_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_Components_Inner = { "Components", nullptr, (EPropertyFlags)0x0000000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_Components_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/ControlRigControlActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_Components = { "Components", nullptr, (EPropertyFlags)0x0040008000002008, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AControlRigControlActor, Components), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_Components_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_Components_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_Materials_Inner = { "Materials", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_Materials_MetaData[] = {
		{ "ModuleRelativePath", "Public/ControlRigControlActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_Materials = { "Materials", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AControlRigControlActor, Materials), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_Materials_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_Materials_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_ColorParameterName_MetaData[] = {
		{ "ModuleRelativePath", "Public/ControlRigControlActor.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_ColorParameterName = { "ColorParameterName", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AControlRigControlActor, ColorParameterName), METADATA_PARAMS(Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_ColorParameterName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_ColorParameterName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AControlRigControlActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_ActorToTrack,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_ControlRigClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_bRefreshOnTick,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_bIsSelectable,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_MaterialOverride,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_ColorParameter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_bCastShadows,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_ActorRootComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_ControlRig,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_ControlNames_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_ControlNames,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_GizmoTransforms_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_GizmoTransforms,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_Components_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_Components,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_Materials_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_Materials,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AControlRigControlActor_Statics::NewProp_ColorParameterName,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AControlRigControlActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AControlRigControlActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AControlRigControlActor_Statics::ClassParams = {
		&AControlRigControlActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AControlRigControlActor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AControlRigControlActor_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AControlRigControlActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AControlRigControlActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AControlRigControlActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AControlRigControlActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AControlRigControlActor, 2026178554);
	template<> CONTROLRIG_API UClass* StaticClass<AControlRigControlActor>()
	{
		return AControlRigControlActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AControlRigControlActor(Z_Construct_UClass_AControlRigControlActor, &AControlRigControlActor::StaticClass, TEXT("/Script/ControlRig"), TEXT("AControlRigControlActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AControlRigControlActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
