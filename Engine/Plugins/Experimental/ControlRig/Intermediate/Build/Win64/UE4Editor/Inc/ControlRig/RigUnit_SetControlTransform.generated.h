// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTROLRIG_RigUnit_SetControlTransform_generated_h
#error "RigUnit_SetControlTransform.generated.h already included, missing '#pragma once' in RigUnit_SetControlTransform.h"
#endif
#define CONTROLRIG_RigUnit_SetControlTransform_generated_h


#define FRigUnit_SetControlTransform_Execute() \
	void FRigUnit_SetControlTransform::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Control, \
		const float Weight, \
		FTransform& Transform, \
		const EBoneGetterSetterMode Space, \
		FCachedRigElement& CachedControlIndex, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Hierarchy_RigUnit_SetControlTransform_h_543_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_SetControlTransform_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Control, \
		const float Weight, \
		FTransform& Transform, \
		const EBoneGetterSetterMode Space, \
		FCachedRigElement& CachedControlIndex, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FName& Control = *(FName*)RigVMMemoryHandles[0].GetData(); \
		const float Weight = *(float*)RigVMMemoryHandles[1].GetData(); \
		FTransform& Transform = *(FTransform*)RigVMMemoryHandles[2].GetData(); \
		EBoneGetterSetterMode Space = (EBoneGetterSetterMode)*(uint8*)RigVMMemoryHandles[3].GetData(); \
		FRigVMDynamicArray<FCachedRigElement> CachedControlIndex_4_Array(*((FRigVMByteArray*)RigVMMemoryHandles[4].GetData(0, false))); \
		CachedControlIndex_4_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedControlIndex = CachedControlIndex_4_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[5].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Control, \
			Weight, \
			Transform, \
			Space, \
			CachedControlIndex, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnitMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_SetControlTransform>();


#define FRigUnit_SetMultiControlRotator_Execute() \
	void FRigUnit_SetMultiControlRotator::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FRigUnit_SetMultiControlRotator_Entry>& Entries, \
		const float Weight, \
		FRigVMDynamicArray<FCachedRigElement>& CachedControlIndices, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Hierarchy_RigUnit_SetControlTransform_h_509_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_SetMultiControlRotator_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FRigUnit_SetMultiControlRotator_Entry>& Entries, \
		const float Weight, \
		FRigVMDynamicArray<FCachedRigElement>& CachedControlIndices, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		FRigVMFixedArray<FRigUnit_SetMultiControlRotator_Entry> Entries((FRigUnit_SetMultiControlRotator_Entry*)RigVMMemoryHandles[0].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[1].GetData())); \
		const float Weight = *(float*)RigVMMemoryHandles[2].GetData(); \
		FRigVMNestedByteArray& CachedControlIndices_3_Array = *(FRigVMNestedByteArray*)RigVMMemoryHandles[3].GetData(0, false); \
		CachedControlIndices_3_Array.SetNum(FMath::Max<int32>(RigVMExecuteContext.GetSlice().TotalNum(), CachedControlIndices_3_Array.Num())); \
		FRigVMDynamicArray<FCachedRigElement> CachedControlIndices(CachedControlIndices_3_Array[RigVMExecuteContext.GetSlice().GetIndex()]); \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[4].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Entries, \
			Weight, \
			CachedControlIndices, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnitMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_SetMultiControlRotator>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Hierarchy_RigUnit_SetControlTransform_h_475_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_SetMultiControlRotator_Entry_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_SetMultiControlRotator_Entry>();


#define FRigUnit_SetControlRotator_Execute() \
	void FRigUnit_SetControlRotator::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Control, \
		const float Weight, \
		FRotator& Rotator, \
		const EBoneGetterSetterMode Space, \
		FCachedRigElement& CachedControlIndex, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Hierarchy_RigUnit_SetControlTransform_h_430_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_SetControlRotator_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Control, \
		const float Weight, \
		FRotator& Rotator, \
		const EBoneGetterSetterMode Space, \
		FCachedRigElement& CachedControlIndex, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FName& Control = *(FName*)RigVMMemoryHandles[0].GetData(); \
		const float Weight = *(float*)RigVMMemoryHandles[1].GetData(); \
		FRotator& Rotator = *(FRotator*)RigVMMemoryHandles[2].GetData(); \
		EBoneGetterSetterMode Space = (EBoneGetterSetterMode)*(uint8*)RigVMMemoryHandles[3].GetData(); \
		FRigVMDynamicArray<FCachedRigElement> CachedControlIndex_4_Array(*((FRigVMByteArray*)RigVMMemoryHandles[4].GetData(0, false))); \
		CachedControlIndex_4_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedControlIndex = CachedControlIndex_4_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[5].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Control, \
			Weight, \
			Rotator, \
			Space, \
			CachedControlIndex, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnitMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_SetControlRotator>();


#define FRigUnit_SetControlVector_Execute() \
	void FRigUnit_SetControlVector::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Control, \
		const float Weight, \
		FVector& Vector, \
		const EBoneGetterSetterMode Space, \
		FCachedRigElement& CachedControlIndex, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Hierarchy_RigUnit_SetControlTransform_h_382_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_SetControlVector_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Control, \
		const float Weight, \
		FVector& Vector, \
		const EBoneGetterSetterMode Space, \
		FCachedRigElement& CachedControlIndex, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FName& Control = *(FName*)RigVMMemoryHandles[0].GetData(); \
		const float Weight = *(float*)RigVMMemoryHandles[1].GetData(); \
		FVector& Vector = *(FVector*)RigVMMemoryHandles[2].GetData(); \
		EBoneGetterSetterMode Space = (EBoneGetterSetterMode)*(uint8*)RigVMMemoryHandles[3].GetData(); \
		FRigVMDynamicArray<FCachedRigElement> CachedControlIndex_4_Array(*((FRigVMByteArray*)RigVMMemoryHandles[4].GetData(0, false))); \
		CachedControlIndex_4_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedControlIndex = CachedControlIndex_4_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[5].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Control, \
			Weight, \
			Vector, \
			Space, \
			CachedControlIndex, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnitMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_SetControlVector>();


#define FRigUnit_SetMultiControlVector2D_Execute() \
	void FRigUnit_SetMultiControlVector2D::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FRigUnit_SetMultiControlVector2D_Entry>& Entries, \
		const float Weight, \
		FRigVMDynamicArray<FCachedRigElement>& CachedControlIndices, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Hierarchy_RigUnit_SetControlTransform_h_348_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_SetMultiControlVector2D_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FRigUnit_SetMultiControlVector2D_Entry>& Entries, \
		const float Weight, \
		FRigVMDynamicArray<FCachedRigElement>& CachedControlIndices, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		FRigVMFixedArray<FRigUnit_SetMultiControlVector2D_Entry> Entries((FRigUnit_SetMultiControlVector2D_Entry*)RigVMMemoryHandles[0].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[1].GetData())); \
		const float Weight = *(float*)RigVMMemoryHandles[2].GetData(); \
		FRigVMNestedByteArray& CachedControlIndices_3_Array = *(FRigVMNestedByteArray*)RigVMMemoryHandles[3].GetData(0, false); \
		CachedControlIndices_3_Array.SetNum(FMath::Max<int32>(RigVMExecuteContext.GetSlice().TotalNum(), CachedControlIndices_3_Array.Num())); \
		FRigVMDynamicArray<FCachedRigElement> CachedControlIndices(CachedControlIndices_3_Array[RigVMExecuteContext.GetSlice().GetIndex()]); \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[4].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Entries, \
			Weight, \
			CachedControlIndices, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnitMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_SetMultiControlVector2D>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Hierarchy_RigUnit_SetControlTransform_h_323_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_SetMultiControlVector2D_Entry_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_SetMultiControlVector2D_Entry>();


#define FRigUnit_SetControlVector2D_Execute() \
	void FRigUnit_SetControlVector2D::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Control, \
		const float Weight, \
		FVector2D& Vector, \
		FCachedRigElement& CachedControlIndex, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Hierarchy_RigUnit_SetControlTransform_h_286_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_SetControlVector2D_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Control, \
		const float Weight, \
		FVector2D& Vector, \
		FCachedRigElement& CachedControlIndex, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FName& Control = *(FName*)RigVMMemoryHandles[0].GetData(); \
		const float Weight = *(float*)RigVMMemoryHandles[1].GetData(); \
		FVector2D& Vector = *(FVector2D*)RigVMMemoryHandles[2].GetData(); \
		FRigVMDynamicArray<FCachedRigElement> CachedControlIndex_3_Array(*((FRigVMByteArray*)RigVMMemoryHandles[3].GetData(0, false))); \
		CachedControlIndex_3_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedControlIndex = CachedControlIndex_3_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[4].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Control, \
			Weight, \
			Vector, \
			CachedControlIndex, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnitMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_SetControlVector2D>();


#define FRigUnit_SetMultiControlInteger_Execute() \
	void FRigUnit_SetMultiControlInteger::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FRigUnit_SetMultiControlInteger_Entry>& Entries, \
		const float Weight, \
		FRigVMDynamicArray<FCachedRigElement>& CachedControlIndices, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Hierarchy_RigUnit_SetControlTransform_h_252_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_SetMultiControlInteger_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FRigUnit_SetMultiControlInteger_Entry>& Entries, \
		const float Weight, \
		FRigVMDynamicArray<FCachedRigElement>& CachedControlIndices, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		FRigVMFixedArray<FRigUnit_SetMultiControlInteger_Entry> Entries((FRigUnit_SetMultiControlInteger_Entry*)RigVMMemoryHandles[0].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[1].GetData())); \
		const float Weight = *(float*)RigVMMemoryHandles[2].GetData(); \
		FRigVMNestedByteArray& CachedControlIndices_3_Array = *(FRigVMNestedByteArray*)RigVMMemoryHandles[3].GetData(0, false); \
		CachedControlIndices_3_Array.SetNum(FMath::Max<int32>(RigVMExecuteContext.GetSlice().TotalNum(), CachedControlIndices_3_Array.Num())); \
		FRigVMDynamicArray<FCachedRigElement> CachedControlIndices(CachedControlIndices_3_Array[RigVMExecuteContext.GetSlice().GetIndex()]); \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[4].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Entries, \
			Weight, \
			CachedControlIndices, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnitMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_SetMultiControlInteger>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Hierarchy_RigUnit_SetControlTransform_h_228_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_SetMultiControlInteger_Entry_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_SetMultiControlInteger_Entry>();


#define FRigUnit_SetControlInteger_Execute() \
	void FRigUnit_SetControlInteger::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Control, \
		const int32 Weight, \
		int32& IntegerValue, \
		FCachedRigElement& CachedControlIndex, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Hierarchy_RigUnit_SetControlTransform_h_191_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_SetControlInteger_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Control, \
		const int32 Weight, \
		int32& IntegerValue, \
		FCachedRigElement& CachedControlIndex, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FName& Control = *(FName*)RigVMMemoryHandles[0].GetData(); \
		const int32 Weight = *(int32*)RigVMMemoryHandles[1].GetData(); \
		int32& IntegerValue = *(int32*)RigVMMemoryHandles[2].GetData(); \
		FRigVMDynamicArray<FCachedRigElement> CachedControlIndex_3_Array(*((FRigVMByteArray*)RigVMMemoryHandles[3].GetData(0, false))); \
		CachedControlIndex_3_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedControlIndex = CachedControlIndex_3_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[4].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Control, \
			Weight, \
			IntegerValue, \
			CachedControlIndex, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnitMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_SetControlInteger>();


#define FRigUnit_SetMultiControlFloat_Execute() \
	void FRigUnit_SetMultiControlFloat::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FRigUnit_SetMultiControlFloat_Entry>& Entries, \
		const float Weight, \
		FRigVMDynamicArray<FCachedRigElement>& CachedControlIndices, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Hierarchy_RigUnit_SetControlTransform_h_155_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_SetMultiControlFloat_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FRigUnit_SetMultiControlFloat_Entry>& Entries, \
		const float Weight, \
		FRigVMDynamicArray<FCachedRigElement>& CachedControlIndices, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		FRigVMFixedArray<FRigUnit_SetMultiControlFloat_Entry> Entries((FRigUnit_SetMultiControlFloat_Entry*)RigVMMemoryHandles[0].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[1].GetData())); \
		const float Weight = *(float*)RigVMMemoryHandles[2].GetData(); \
		FRigVMNestedByteArray& CachedControlIndices_3_Array = *(FRigVMNestedByteArray*)RigVMMemoryHandles[3].GetData(0, false); \
		CachedControlIndices_3_Array.SetNum(FMath::Max<int32>(RigVMExecuteContext.GetSlice().TotalNum(), CachedControlIndices_3_Array.Num())); \
		FRigVMDynamicArray<FCachedRigElement> CachedControlIndices(CachedControlIndices_3_Array[RigVMExecuteContext.GetSlice().GetIndex()]); \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[4].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Entries, \
			Weight, \
			CachedControlIndices, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnitMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_SetMultiControlFloat>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Hierarchy_RigUnit_SetControlTransform_h_131_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_SetMultiControlFloat_Entry_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_SetMultiControlFloat_Entry>();


#define FRigUnit_SetControlFloat_Execute() \
	void FRigUnit_SetControlFloat::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Control, \
		const float Weight, \
		float& FloatValue, \
		FCachedRigElement& CachedControlIndex, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Hierarchy_RigUnit_SetControlTransform_h_94_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_SetControlFloat_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Control, \
		const float Weight, \
		float& FloatValue, \
		FCachedRigElement& CachedControlIndex, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FName& Control = *(FName*)RigVMMemoryHandles[0].GetData(); \
		const float Weight = *(float*)RigVMMemoryHandles[1].GetData(); \
		float& FloatValue = *(float*)RigVMMemoryHandles[2].GetData(); \
		FRigVMDynamicArray<FCachedRigElement> CachedControlIndex_3_Array(*((FRigVMByteArray*)RigVMMemoryHandles[3].GetData(0, false))); \
		CachedControlIndex_3_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedControlIndex = CachedControlIndex_3_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[4].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Control, \
			Weight, \
			FloatValue, \
			CachedControlIndex, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnitMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_SetControlFloat>();


#define FRigUnit_SetMultiControlBool_Execute() \
	void FRigUnit_SetMultiControlBool::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FRigUnit_SetMultiControlBool_Entry>& Entries, \
		FRigVMDynamicArray<FCachedRigElement>& CachedControlIndices, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Hierarchy_RigUnit_SetControlTransform_h_67_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_SetMultiControlBool_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigVMFixedArray<FRigUnit_SetMultiControlBool_Entry>& Entries, \
		FRigVMDynamicArray<FCachedRigElement>& CachedControlIndices, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		FRigVMFixedArray<FRigUnit_SetMultiControlBool_Entry> Entries((FRigUnit_SetMultiControlBool_Entry*)RigVMMemoryHandles[0].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[1].GetData())); \
		FRigVMNestedByteArray& CachedControlIndices_2_Array = *(FRigVMNestedByteArray*)RigVMMemoryHandles[2].GetData(0, false); \
		CachedControlIndices_2_Array.SetNum(FMath::Max<int32>(RigVMExecuteContext.GetSlice().TotalNum(), CachedControlIndices_2_Array.Num())); \
		FRigVMDynamicArray<FCachedRigElement> CachedControlIndices(CachedControlIndices_2_Array[RigVMExecuteContext.GetSlice().GetIndex()]); \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[3].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Entries, \
			CachedControlIndices, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnitMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_SetMultiControlBool>();

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Hierarchy_RigUnit_SetControlTransform_h_44_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_SetMultiControlBool_Entry_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct();


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_SetMultiControlBool_Entry>();


#define FRigUnit_SetControlBool_Execute() \
	void FRigUnit_SetControlBool::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Control, \
		bool& BoolValue, \
		FCachedRigElement& CachedControlIndex, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Hierarchy_RigUnit_SetControlTransform_h_14_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_SetControlBool_Statics; \
	CONTROLRIG_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FName& Control, \
		bool& BoolValue, \
		FCachedRigElement& CachedControlIndex, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FName& Control = *(FName*)RigVMMemoryHandles[0].GetData(); \
		bool& BoolValue = *(bool*)RigVMMemoryHandles[1].GetData(); \
		FRigVMDynamicArray<FCachedRigElement> CachedControlIndex_2_Array(*((FRigVMByteArray*)RigVMMemoryHandles[2].GetData(0, false))); \
		CachedControlIndex_2_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FCachedRigElement& CachedControlIndex = CachedControlIndex_2_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[3].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Control, \
			BoolValue, \
			CachedControlIndex, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnitMutable Super;


template<> CONTROLRIG_API UScriptStruct* StaticStruct<struct FRigUnit_SetControlBool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ControlRig_Source_ControlRig_Private_Units_Hierarchy_RigUnit_SetControlTransform_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
