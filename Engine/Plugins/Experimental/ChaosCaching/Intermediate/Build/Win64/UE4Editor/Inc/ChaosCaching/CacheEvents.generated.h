// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CHAOSCACHING_CacheEvents_generated_h
#error "CacheEvents.generated.h already included, missing '#pragma once' in CacheEvents.h"
#endif
#define CHAOSCACHING_CacheEvents_generated_h

#define Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheEvents_h_26_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCacheEventTrack_Statics; \
	CHAOSCACHING_API static class UScriptStruct* StaticStruct();


template<> CHAOSCACHING_API UScriptStruct* StaticStruct<struct FCacheEventTrack>();

#define Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheEvents_h_16_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCacheEventBase_Statics; \
	CHAOSCACHING_API static class UScriptStruct* StaticStruct();


template<> CHAOSCACHING_API UScriptStruct* StaticStruct<struct FCacheEventBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheEvents_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
