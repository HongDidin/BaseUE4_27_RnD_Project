// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
enum class ECacheMode : uint8;
#ifdef CHAOSCACHING_CacheManagerActor_generated_h
#error "CacheManagerActor.generated.h already included, missing '#pragma once' in CacheManagerActor.h"
#endif
#define CHAOSCACHING_CacheManagerActor_generated_h

#define Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheManagerActor_h_45_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FObservedComponent_Statics; \
	static class UScriptStruct* StaticStruct();


template<> CHAOSCACHING_API UScriptStruct* StaticStruct<struct FObservedComponent>();

#define Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheManagerActor_h_120_SPARSE_DATA
#define Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheManagerActor_h_120_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execTriggerAll); \
	DECLARE_FUNCTION(execTriggerComponentByCache); \
	DECLARE_FUNCTION(execTriggerComponent); \
	DECLARE_FUNCTION(execResetSingleTransform); \
	DECLARE_FUNCTION(execResetAllComponentTransforms); \
	DECLARE_FUNCTION(execSetAllMode);


#define Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheManagerActor_h_120_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execTriggerAll); \
	DECLARE_FUNCTION(execTriggerComponentByCache); \
	DECLARE_FUNCTION(execTriggerComponent); \
	DECLARE_FUNCTION(execResetSingleTransform); \
	DECLARE_FUNCTION(execResetAllComponentTransforms); \
	DECLARE_FUNCTION(execSetAllMode);


#define Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheManagerActor_h_120_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAChaosCacheManager(); \
	friend struct Z_Construct_UClass_AChaosCacheManager_Statics; \
public: \
	DECLARE_CLASS(AChaosCacheManager, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ChaosCaching"), NO_API) \
	DECLARE_SERIALIZER(AChaosCacheManager)


#define Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheManagerActor_h_120_INCLASS \
private: \
	static void StaticRegisterNativesAChaosCacheManager(); \
	friend struct Z_Construct_UClass_AChaosCacheManager_Statics; \
public: \
	DECLARE_CLASS(AChaosCacheManager, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ChaosCaching"), NO_API) \
	DECLARE_SERIALIZER(AChaosCacheManager)


#define Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheManagerActor_h_120_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AChaosCacheManager(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AChaosCacheManager) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AChaosCacheManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AChaosCacheManager); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AChaosCacheManager(AChaosCacheManager&&); \
	NO_API AChaosCacheManager(const AChaosCacheManager&); \
public:


#define Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheManagerActor_h_120_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AChaosCacheManager(AChaosCacheManager&&); \
	NO_API AChaosCacheManager(const AChaosCacheManager&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AChaosCacheManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AChaosCacheManager); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AChaosCacheManager)


#define Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheManagerActor_h_120_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ObservedComponents() { return STRUCT_OFFSET(AChaosCacheManager, ObservedComponents); }


#define Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheManagerActor_h_117_PROLOG
#define Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheManagerActor_h_120_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheManagerActor_h_120_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheManagerActor_h_120_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheManagerActor_h_120_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheManagerActor_h_120_INCLASS \
	Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheManagerActor_h_120_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheManagerActor_h_120_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheManagerActor_h_120_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheManagerActor_h_120_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheManagerActor_h_120_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheManagerActor_h_120_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheManagerActor_h_120_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CHAOSCACHING_API UClass* StaticClass<class AChaosCacheManager>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheManagerActor_h


#define FOREACH_ENUM_ESTARTMODE(op) \
	op(EStartMode::Timed) \
	op(EStartMode::Triggered) 

enum class EStartMode : uint8;
template<> CHAOSCACHING_API UEnum* StaticEnum<EStartMode>();

#define FOREACH_ENUM_ECACHEMODE(op) \
	op(ECacheMode::None) \
	op(ECacheMode::Play) \
	op(ECacheMode::Record) 

enum class ECacheMode : uint8;
template<> CHAOSCACHING_API UEnum* StaticEnum<ECacheMode>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
