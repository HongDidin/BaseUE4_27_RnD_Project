// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ChaosCaching/Public/Chaos/CacheCollection.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCacheCollection() {}
// Cross Module References
	CHAOSCACHING_API UClass* Z_Construct_UClass_UChaosCacheCollection_NoRegister();
	CHAOSCACHING_API UClass* Z_Construct_UClass_UChaosCacheCollection();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_ChaosCaching();
	CHAOSCACHING_API UClass* Z_Construct_UClass_UChaosCache_NoRegister();
// End Cross Module References
	void UChaosCacheCollection::StaticRegisterNativesUChaosCacheCollection()
	{
	}
	UClass* Z_Construct_UClass_UChaosCacheCollection_NoRegister()
	{
		return UChaosCacheCollection::StaticClass();
	}
	struct Z_Construct_UClass_UChaosCacheCollection_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Caches_Inner_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Caches_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Caches_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Caches;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UChaosCacheCollection_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_ChaosCaching,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UChaosCacheCollection_Statics::Class_MetaDataParams[] = {
		{ "DevelopmentStatus", "Experimental" },
		{ "IncludePath", "Chaos/CacheCollection.h" },
		{ "ModuleRelativePath", "Public/Chaos/CacheCollection.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UChaosCacheCollection_Statics::NewProp_Caches_Inner_MetaData[] = {
		{ "Category", "Caching" },
		{ "EditFixedOrder", "" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Chaos/CacheCollection.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UChaosCacheCollection_Statics::NewProp_Caches_Inner = { "Caches", nullptr, (EPropertyFlags)0x0002000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UChaosCache_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UChaosCacheCollection_Statics::NewProp_Caches_Inner_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UChaosCacheCollection_Statics::NewProp_Caches_Inner_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UChaosCacheCollection_Statics::NewProp_Caches_MetaData[] = {
		{ "Category", "Caching" },
		{ "EditFixedOrder", "" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Chaos/CacheCollection.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UChaosCacheCollection_Statics::NewProp_Caches = { "Caches", nullptr, (EPropertyFlags)0x0010008000000009, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UChaosCacheCollection, Caches), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UChaosCacheCollection_Statics::NewProp_Caches_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UChaosCacheCollection_Statics::NewProp_Caches_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UChaosCacheCollection_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UChaosCacheCollection_Statics::NewProp_Caches_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UChaosCacheCollection_Statics::NewProp_Caches,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UChaosCacheCollection_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UChaosCacheCollection>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UChaosCacheCollection_Statics::ClassParams = {
		&UChaosCacheCollection::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UChaosCacheCollection_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UChaosCacheCollection_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UChaosCacheCollection_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UChaosCacheCollection_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UChaosCacheCollection()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UChaosCacheCollection_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UChaosCacheCollection, 3542488017);
	template<> CHAOSCACHING_API UClass* StaticClass<UChaosCacheCollection>()
	{
		return UChaosCacheCollection::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UChaosCacheCollection(Z_Construct_UClass_UChaosCacheCollection, &UChaosCacheCollection::StaticClass, TEXT("/Script/ChaosCaching"), TEXT("UChaosCacheCollection"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UChaosCacheCollection);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
