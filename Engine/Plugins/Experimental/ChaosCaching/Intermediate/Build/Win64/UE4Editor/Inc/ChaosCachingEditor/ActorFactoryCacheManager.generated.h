// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CHAOSCACHINGEDITOR_ActorFactoryCacheManager_generated_h
#error "ActorFactoryCacheManager.generated.h already included, missing '#pragma once' in ActorFactoryCacheManager.h"
#endif
#define CHAOSCACHINGEDITOR_ActorFactoryCacheManager_generated_h

#define Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCachingEditor_Public_Chaos_ActorFactoryCacheManager_h_17_SPARSE_DATA
#define Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCachingEditor_Public_Chaos_ActorFactoryCacheManager_h_17_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCachingEditor_Public_Chaos_ActorFactoryCacheManager_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCachingEditor_Public_Chaos_ActorFactoryCacheManager_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUActorFactoryCacheManager(); \
	friend struct Z_Construct_UClass_UActorFactoryCacheManager_Statics; \
public: \
	DECLARE_CLASS(UActorFactoryCacheManager, UActorFactory, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ChaosCachingEditor"), NO_API) \
	DECLARE_SERIALIZER(UActorFactoryCacheManager)


#define Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCachingEditor_Public_Chaos_ActorFactoryCacheManager_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUActorFactoryCacheManager(); \
	friend struct Z_Construct_UClass_UActorFactoryCacheManager_Statics; \
public: \
	DECLARE_CLASS(UActorFactoryCacheManager, UActorFactory, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ChaosCachingEditor"), NO_API) \
	DECLARE_SERIALIZER(UActorFactoryCacheManager)


#define Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCachingEditor_Public_Chaos_ActorFactoryCacheManager_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UActorFactoryCacheManager(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UActorFactoryCacheManager) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UActorFactoryCacheManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UActorFactoryCacheManager); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UActorFactoryCacheManager(UActorFactoryCacheManager&&); \
	NO_API UActorFactoryCacheManager(const UActorFactoryCacheManager&); \
public:


#define Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCachingEditor_Public_Chaos_ActorFactoryCacheManager_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UActorFactoryCacheManager(UActorFactoryCacheManager&&); \
	NO_API UActorFactoryCacheManager(const UActorFactoryCacheManager&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UActorFactoryCacheManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UActorFactoryCacheManager); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UActorFactoryCacheManager)


#define Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCachingEditor_Public_Chaos_ActorFactoryCacheManager_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCachingEditor_Public_Chaos_ActorFactoryCacheManager_h_14_PROLOG
#define Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCachingEditor_Public_Chaos_ActorFactoryCacheManager_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCachingEditor_Public_Chaos_ActorFactoryCacheManager_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCachingEditor_Public_Chaos_ActorFactoryCacheManager_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCachingEditor_Public_Chaos_ActorFactoryCacheManager_h_17_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCachingEditor_Public_Chaos_ActorFactoryCacheManager_h_17_INCLASS \
	Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCachingEditor_Public_Chaos_ActorFactoryCacheManager_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCachingEditor_Public_Chaos_ActorFactoryCacheManager_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCachingEditor_Public_Chaos_ActorFactoryCacheManager_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCachingEditor_Public_Chaos_ActorFactoryCacheManager_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCachingEditor_Public_Chaos_ActorFactoryCacheManager_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCachingEditor_Public_Chaos_ActorFactoryCacheManager_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCachingEditor_Public_Chaos_ActorFactoryCacheManager_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CHAOSCACHINGEDITOR_API UClass* StaticClass<class UActorFactoryCacheManager>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCachingEditor_Public_Chaos_ActorFactoryCacheManager_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
