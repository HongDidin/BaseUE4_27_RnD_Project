// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CHAOSCACHING_GeometryCollectionComponentCacheAdapter_generated_h
#error "GeometryCollectionComponentCacheAdapter.generated.h already included, missing '#pragma once' in GeometryCollectionComponentCacheAdapter.h"
#endif
#define CHAOSCACHING_GeometryCollectionComponentCacheAdapter_generated_h

#define Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_Adapters_GeometryCollectionComponentCacheAdapter_h_12_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FEnableStateEvent_Statics; \
	CHAOSCACHING_API static class UScriptStruct* StaticStruct(); \
	typedef FCacheEventBase Super;


template<> CHAOSCACHING_API UScriptStruct* StaticStruct<struct FEnableStateEvent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_Adapters_GeometryCollectionComponentCacheAdapter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
