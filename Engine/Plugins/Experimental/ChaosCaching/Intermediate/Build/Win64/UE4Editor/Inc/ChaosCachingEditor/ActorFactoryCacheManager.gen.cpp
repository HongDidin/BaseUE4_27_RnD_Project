// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ChaosCachingEditor/Public/Chaos/ActorFactoryCacheManager.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeActorFactoryCacheManager() {}
// Cross Module References
	CHAOSCACHINGEDITOR_API UClass* Z_Construct_UClass_UActorFactoryCacheManager_NoRegister();
	CHAOSCACHINGEDITOR_API UClass* Z_Construct_UClass_UActorFactoryCacheManager();
	UNREALED_API UClass* Z_Construct_UClass_UActorFactory();
	UPackage* Z_Construct_UPackage__Script_ChaosCachingEditor();
// End Cross Module References
	void UActorFactoryCacheManager::StaticRegisterNativesUActorFactoryCacheManager()
	{
	}
	UClass* Z_Construct_UClass_UActorFactoryCacheManager_NoRegister()
	{
		return UActorFactoryCacheManager::StaticClass();
	}
	struct Z_Construct_UClass_UActorFactoryCacheManager_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UActorFactoryCacheManager_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_ChaosCachingEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UActorFactoryCacheManager_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "Chaos/ActorFactoryCacheManager.h" },
		{ "ModuleRelativePath", "Public/Chaos/ActorFactoryCacheManager.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UActorFactoryCacheManager_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UActorFactoryCacheManager>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UActorFactoryCacheManager_Statics::ClassParams = {
		&UActorFactoryCacheManager::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001030ACu,
		METADATA_PARAMS(Z_Construct_UClass_UActorFactoryCacheManager_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UActorFactoryCacheManager_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UActorFactoryCacheManager()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UActorFactoryCacheManager_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UActorFactoryCacheManager, 2089576146);
	template<> CHAOSCACHINGEDITOR_API UClass* StaticClass<UActorFactoryCacheManager>()
	{
		return UActorFactoryCacheManager::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UActorFactoryCacheManager(Z_Construct_UClass_UActorFactoryCacheManager, &UActorFactoryCacheManager::StaticClass, TEXT("/Script/ChaosCachingEditor"), TEXT("UActorFactoryCacheManager"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UActorFactoryCacheManager);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
