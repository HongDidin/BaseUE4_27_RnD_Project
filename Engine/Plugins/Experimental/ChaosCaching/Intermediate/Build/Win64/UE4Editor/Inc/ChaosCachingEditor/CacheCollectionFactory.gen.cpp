// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ChaosCachingEditor/Public/Chaos/CacheCollectionFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCacheCollectionFactory() {}
// Cross Module References
	CHAOSCACHINGEDITOR_API UClass* Z_Construct_UClass_UCacheCollectionFactory_NoRegister();
	CHAOSCACHINGEDITOR_API UClass* Z_Construct_UClass_UCacheCollectionFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_ChaosCachingEditor();
// End Cross Module References
	void UCacheCollectionFactory::StaticRegisterNativesUCacheCollectionFactory()
	{
	}
	UClass* Z_Construct_UClass_UCacheCollectionFactory_NoRegister()
	{
		return UCacheCollectionFactory::StaticClass();
	}
	struct Z_Construct_UClass_UCacheCollectionFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCacheCollectionFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_ChaosCachingEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCacheCollectionFactory_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Chaos/CacheCollectionFactory.h" },
		{ "ModuleRelativePath", "Public/Chaos/CacheCollectionFactory.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCacheCollectionFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCacheCollectionFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCacheCollectionFactory_Statics::ClassParams = {
		&UCacheCollectionFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCacheCollectionFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCacheCollectionFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCacheCollectionFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCacheCollectionFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCacheCollectionFactory, 3337635323);
	template<> CHAOSCACHINGEDITOR_API UClass* StaticClass<UCacheCollectionFactory>()
	{
		return UCacheCollectionFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCacheCollectionFactory(Z_Construct_UClass_UCacheCollectionFactory, &UCacheCollectionFactory::StaticClass, TEXT("/Script/ChaosCachingEditor"), TEXT("UCacheCollectionFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCacheCollectionFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
