// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ChaosCaching/Public/Chaos/ChaosCache.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeChaosCache() {}
// Cross Module References
	CHAOSCACHING_API UScriptStruct* Z_Construct_UScriptStruct_FCacheSpawnableTemplate();
	UPackage* Z_Construct_UPackage__Script_ChaosCaching();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	CHAOSCACHING_API UScriptStruct* Z_Construct_UScriptStruct_FPerParticleCacheData();
	CHAOSCACHING_API UScriptStruct* Z_Construct_UScriptStruct_FParticleTransformTrack();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FRichCurve();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FRawAnimSequenceTrack();
	CHAOSCACHING_API UClass* Z_Construct_UClass_UChaosCache_NoRegister();
	CHAOSCACHING_API UClass* Z_Construct_UClass_UChaosCache();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	CHAOSCACHING_API UScriptStruct* Z_Construct_UScriptStruct_FCacheEventTrack();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
// End Cross Module References
class UScriptStruct* FCacheSpawnableTemplate::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CHAOSCACHING_API uint32 Get_Z_Construct_UScriptStruct_FCacheSpawnableTemplate_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCacheSpawnableTemplate, Z_Construct_UPackage__Script_ChaosCaching(), TEXT("CacheSpawnableTemplate"), sizeof(FCacheSpawnableTemplate), Get_Z_Construct_UScriptStruct_FCacheSpawnableTemplate_Hash());
	}
	return Singleton;
}
template<> CHAOSCACHING_API UScriptStruct* StaticStruct<FCacheSpawnableTemplate>()
{
	return FCacheSpawnableTemplate::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCacheSpawnableTemplate(FCacheSpawnableTemplate::StaticStruct, TEXT("/Script/ChaosCaching"), TEXT("CacheSpawnableTemplate"), false, nullptr, nullptr);
static struct FScriptStruct_ChaosCaching_StaticRegisterNativesFCacheSpawnableTemplate
{
	FScriptStruct_ChaosCaching_StaticRegisterNativesFCacheSpawnableTemplate()
	{
		UScriptStruct::DeferCppStructOps<FCacheSpawnableTemplate>(FName(TEXT("CacheSpawnableTemplate")));
	}
} ScriptStruct_ChaosCaching_StaticRegisterNativesFCacheSpawnableTemplate;
	struct Z_Construct_UScriptStruct_FCacheSpawnableTemplate_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DuplicatedTemplate_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DuplicatedTemplate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InitialTransform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InitialTransform;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCacheSpawnableTemplate_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Chaos/ChaosCache.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FCacheSpawnableTemplate_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FCacheSpawnableTemplate>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCacheSpawnableTemplate_Statics::NewProp_DuplicatedTemplate_MetaData[] = {
		{ "Category", "Caching" },
		{ "ModuleRelativePath", "Public/Chaos/ChaosCache.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FCacheSpawnableTemplate_Statics::NewProp_DuplicatedTemplate = { "DuplicatedTemplate", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCacheSpawnableTemplate, DuplicatedTemplate), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FCacheSpawnableTemplate_Statics::NewProp_DuplicatedTemplate_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCacheSpawnableTemplate_Statics::NewProp_DuplicatedTemplate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCacheSpawnableTemplate_Statics::NewProp_InitialTransform_MetaData[] = {
		{ "Category", "Caching" },
		{ "ModuleRelativePath", "Public/Chaos/ChaosCache.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCacheSpawnableTemplate_Statics::NewProp_InitialTransform = { "InitialTransform", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCacheSpawnableTemplate, InitialTransform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FCacheSpawnableTemplate_Statics::NewProp_InitialTransform_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCacheSpawnableTemplate_Statics::NewProp_InitialTransform_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FCacheSpawnableTemplate_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCacheSpawnableTemplate_Statics::NewProp_DuplicatedTemplate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCacheSpawnableTemplate_Statics::NewProp_InitialTransform,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FCacheSpawnableTemplate_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ChaosCaching,
		nullptr,
		&NewStructOps,
		"CacheSpawnableTemplate",
		sizeof(FCacheSpawnableTemplate),
		alignof(FCacheSpawnableTemplate),
		Z_Construct_UScriptStruct_FCacheSpawnableTemplate_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCacheSpawnableTemplate_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FCacheSpawnableTemplate_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCacheSpawnableTemplate_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FCacheSpawnableTemplate()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FCacheSpawnableTemplate_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ChaosCaching();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CacheSpawnableTemplate"), sizeof(FCacheSpawnableTemplate), Get_Z_Construct_UScriptStruct_FCacheSpawnableTemplate_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FCacheSpawnableTemplate_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCacheSpawnableTemplate_Hash() { return 516108842U; }
class UScriptStruct* FPerParticleCacheData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CHAOSCACHING_API uint32 Get_Z_Construct_UScriptStruct_FPerParticleCacheData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FPerParticleCacheData, Z_Construct_UPackage__Script_ChaosCaching(), TEXT("PerParticleCacheData"), sizeof(FPerParticleCacheData), Get_Z_Construct_UScriptStruct_FPerParticleCacheData_Hash());
	}
	return Singleton;
}
template<> CHAOSCACHING_API UScriptStruct* StaticStruct<FPerParticleCacheData>()
{
	return FPerParticleCacheData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FPerParticleCacheData(FPerParticleCacheData::StaticStruct, TEXT("/Script/ChaosCaching"), TEXT("PerParticleCacheData"), false, nullptr, nullptr);
static struct FScriptStruct_ChaosCaching_StaticRegisterNativesFPerParticleCacheData
{
	FScriptStruct_ChaosCaching_StaticRegisterNativesFPerParticleCacheData()
	{
		UScriptStruct::DeferCppStructOps<FPerParticleCacheData>(FName(TEXT("PerParticleCacheData")));
	}
} ScriptStruct_ChaosCaching_StaticRegisterNativesFPerParticleCacheData;
	struct Z_Construct_UScriptStruct_FPerParticleCacheData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransformData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TransformData;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CurveData_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_CurveData_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurveData_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_CurveData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPerParticleCacheData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Chaos/ChaosCache.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FPerParticleCacheData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FPerParticleCacheData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPerParticleCacheData_Statics::NewProp_TransformData_MetaData[] = {
		{ "ModuleRelativePath", "Public/Chaos/ChaosCache.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FPerParticleCacheData_Statics::NewProp_TransformData = { "TransformData", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPerParticleCacheData, TransformData), Z_Construct_UScriptStruct_FParticleTransformTrack, METADATA_PARAMS(Z_Construct_UScriptStruct_FPerParticleCacheData_Statics::NewProp_TransformData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPerParticleCacheData_Statics::NewProp_TransformData_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FPerParticleCacheData_Statics::NewProp_CurveData_ValueProp = { "CurveData", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FRichCurve, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FPerParticleCacheData_Statics::NewProp_CurveData_Key_KeyProp = { "CurveData_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPerParticleCacheData_Statics::NewProp_CurveData_MetaData[] = {
		{ "Comment", "/**\n\x09 * Named curve data. This can be particle or other continuous curve data pushed by the adapter that created the\n\x09 * cache. Any particle property outside of the transforms will be placed in this container with a suitable name for\n\x09 * the property. Blueprints and adapters can add whatever data they need to this container.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Chaos/ChaosCache.h" },
		{ "ToolTip", "Named curve data. This can be particle or other continuous curve data pushed by the adapter that created the\ncache. Any particle property outside of the transforms will be placed in this container with a suitable name for\nthe property. Blueprints and adapters can add whatever data they need to this container." },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FPerParticleCacheData_Statics::NewProp_CurveData = { "CurveData", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPerParticleCacheData, CurveData), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FPerParticleCacheData_Statics::NewProp_CurveData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPerParticleCacheData_Statics::NewProp_CurveData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FPerParticleCacheData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPerParticleCacheData_Statics::NewProp_TransformData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPerParticleCacheData_Statics::NewProp_CurveData_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPerParticleCacheData_Statics::NewProp_CurveData_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPerParticleCacheData_Statics::NewProp_CurveData,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FPerParticleCacheData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ChaosCaching,
		nullptr,
		&NewStructOps,
		"PerParticleCacheData",
		sizeof(FPerParticleCacheData),
		alignof(FPerParticleCacheData),
		Z_Construct_UScriptStruct_FPerParticleCacheData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPerParticleCacheData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FPerParticleCacheData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPerParticleCacheData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FPerParticleCacheData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FPerParticleCacheData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ChaosCaching();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("PerParticleCacheData"), sizeof(FPerParticleCacheData), Get_Z_Construct_UScriptStruct_FPerParticleCacheData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FPerParticleCacheData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FPerParticleCacheData_Hash() { return 2708156611U; }
class UScriptStruct* FParticleTransformTrack::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CHAOSCACHING_API uint32 Get_Z_Construct_UScriptStruct_FParticleTransformTrack_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FParticleTransformTrack, Z_Construct_UPackage__Script_ChaosCaching(), TEXT("ParticleTransformTrack"), sizeof(FParticleTransformTrack), Get_Z_Construct_UScriptStruct_FParticleTransformTrack_Hash());
	}
	return Singleton;
}
template<> CHAOSCACHING_API UScriptStruct* StaticStruct<FParticleTransformTrack>()
{
	return FParticleTransformTrack::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FParticleTransformTrack(FParticleTransformTrack::StaticStruct, TEXT("/Script/ChaosCaching"), TEXT("ParticleTransformTrack"), false, nullptr, nullptr);
static struct FScriptStruct_ChaosCaching_StaticRegisterNativesFParticleTransformTrack
{
	FScriptStruct_ChaosCaching_StaticRegisterNativesFParticleTransformTrack()
	{
		UScriptStruct::DeferCppStructOps<FParticleTransformTrack>(FName(TEXT("ParticleTransformTrack")));
	}
} ScriptStruct_ChaosCaching_StaticRegisterNativesFParticleTransformTrack;
	struct Z_Construct_UScriptStruct_FParticleTransformTrack_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RawTransformTrack_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RawTransformTrack;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BeginOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BeginOffset;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_KeyTimestamps_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_KeyTimestamps_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_KeyTimestamps;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FParticleTransformTrack_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Chaos/ChaosCache.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FParticleTransformTrack_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FParticleTransformTrack>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FParticleTransformTrack_Statics::NewProp_RawTransformTrack_MetaData[] = {
		{ "Comment", "/**\n\x09 * List of all the transforms this cache cares about, recorded from the simulated transforms of the particles\n\x09 * observed by the adapter that created the cache\n\x09 */" },
		{ "ModuleRelativePath", "Public/Chaos/ChaosCache.h" },
		{ "ToolTip", "List of all the transforms this cache cares about, recorded from the simulated transforms of the particles\nobserved by the adapter that created the cache" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FParticleTransformTrack_Statics::NewProp_RawTransformTrack = { "RawTransformTrack", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FParticleTransformTrack, RawTransformTrack), Z_Construct_UScriptStruct_FRawAnimSequenceTrack, METADATA_PARAMS(Z_Construct_UScriptStruct_FParticleTransformTrack_Statics::NewProp_RawTransformTrack_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FParticleTransformTrack_Statics::NewProp_RawTransformTrack_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FParticleTransformTrack_Statics::NewProp_BeginOffset_MetaData[] = {
		{ "Comment", "/** The offset from the beginning of the cache that holds this track that the track starts */" },
		{ "ModuleRelativePath", "Public/Chaos/ChaosCache.h" },
		{ "ToolTip", "The offset from the beginning of the cache that holds this track that the track starts" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FParticleTransformTrack_Statics::NewProp_BeginOffset = { "BeginOffset", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FParticleTransformTrack, BeginOffset), METADATA_PARAMS(Z_Construct_UScriptStruct_FParticleTransformTrack_Statics::NewProp_BeginOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FParticleTransformTrack_Statics::NewProp_BeginOffset_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FParticleTransformTrack_Statics::NewProp_KeyTimestamps_Inner = { "KeyTimestamps", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FParticleTransformTrack_Statics::NewProp_KeyTimestamps_MetaData[] = {
		{ "Comment", "/**\n\x09 * The above raw track is just the key data and doesn't know at which time those keys are placed, this is\n\x09 * a list of the timestamps for each entry in TransformTrack\n\x09 */" },
		{ "ModuleRelativePath", "Public/Chaos/ChaosCache.h" },
		{ "ToolTip", "The above raw track is just the key data and doesn't know at which time those keys are placed, this is\na list of the timestamps for each entry in TransformTrack" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FParticleTransformTrack_Statics::NewProp_KeyTimestamps = { "KeyTimestamps", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FParticleTransformTrack, KeyTimestamps), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FParticleTransformTrack_Statics::NewProp_KeyTimestamps_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FParticleTransformTrack_Statics::NewProp_KeyTimestamps_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FParticleTransformTrack_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FParticleTransformTrack_Statics::NewProp_RawTransformTrack,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FParticleTransformTrack_Statics::NewProp_BeginOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FParticleTransformTrack_Statics::NewProp_KeyTimestamps_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FParticleTransformTrack_Statics::NewProp_KeyTimestamps,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FParticleTransformTrack_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ChaosCaching,
		nullptr,
		&NewStructOps,
		"ParticleTransformTrack",
		sizeof(FParticleTransformTrack),
		alignof(FParticleTransformTrack),
		Z_Construct_UScriptStruct_FParticleTransformTrack_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FParticleTransformTrack_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FParticleTransformTrack_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FParticleTransformTrack_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FParticleTransformTrack()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FParticleTransformTrack_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ChaosCaching();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ParticleTransformTrack"), sizeof(FParticleTransformTrack), Get_Z_Construct_UScriptStruct_FParticleTransformTrack_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FParticleTransformTrack_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FParticleTransformTrack_Hash() { return 1970330562U; }
	void UChaosCache::StaticRegisterNativesUChaosCache()
	{
	}
	UClass* Z_Construct_UClass_UChaosCache_NoRegister()
	{
		return UChaosCache::StaticClass();
	}
	struct Z_Construct_UClass_UChaosCache_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RecordedDuration_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RecordedDuration;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumRecordedFrames_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_NumRecordedFrames;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_TrackToParticle_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TrackToParticle_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_TrackToParticle;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ParticleTracks_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParticleTracks_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ParticleTracks;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CurveData_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_CurveData_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurveData_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_CurveData;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EventTracks_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_EventTracks_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EventTracks_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_EventTracks;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Spawnable_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Spawnable;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AdapterGuid_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AdapterGuid;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UChaosCache_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_ChaosCaching,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UChaosCache_Statics::Class_MetaDataParams[] = {
		{ "DevelopmentStatus", "Experimental" },
		{ "IncludePath", "Chaos/ChaosCache.h" },
		{ "ModuleRelativePath", "Public/Chaos/ChaosCache.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UChaosCache_Statics::NewProp_RecordedDuration_MetaData[] = {
		{ "Category", "Caching" },
		{ "ModuleRelativePath", "Public/Chaos/ChaosCache.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UChaosCache_Statics::NewProp_RecordedDuration = { "RecordedDuration", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UChaosCache, RecordedDuration), METADATA_PARAMS(Z_Construct_UClass_UChaosCache_Statics::NewProp_RecordedDuration_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UChaosCache_Statics::NewProp_RecordedDuration_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UChaosCache_Statics::NewProp_NumRecordedFrames_MetaData[] = {
		{ "Category", "Caching" },
		{ "ModuleRelativePath", "Public/Chaos/ChaosCache.h" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UClass_UChaosCache_Statics::NewProp_NumRecordedFrames = { "NumRecordedFrames", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UChaosCache, NumRecordedFrames), METADATA_PARAMS(Z_Construct_UClass_UChaosCache_Statics::NewProp_NumRecordedFrames_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UChaosCache_Statics::NewProp_NumRecordedFrames_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UChaosCache_Statics::NewProp_TrackToParticle_Inner = { "TrackToParticle", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UChaosCache_Statics::NewProp_TrackToParticle_MetaData[] = {
		{ "Comment", "/** Maps a track index in the cache to the original particle index specified when recording */" },
		{ "ModuleRelativePath", "Public/Chaos/ChaosCache.h" },
		{ "ToolTip", "Maps a track index in the cache to the original particle index specified when recording" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UChaosCache_Statics::NewProp_TrackToParticle = { "TrackToParticle", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UChaosCache, TrackToParticle), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UChaosCache_Statics::NewProp_TrackToParticle_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UChaosCache_Statics::NewProp_TrackToParticle_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UChaosCache_Statics::NewProp_ParticleTracks_Inner = { "ParticleTracks", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FPerParticleCacheData, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UChaosCache_Statics::NewProp_ParticleTracks_MetaData[] = {
		{ "Comment", "/** Per-particle data, includes transforms, velocities and other per-particle, per-frame data */" },
		{ "ModuleRelativePath", "Public/Chaos/ChaosCache.h" },
		{ "ToolTip", "Per-particle data, includes transforms, velocities and other per-particle, per-frame data" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UChaosCache_Statics::NewProp_ParticleTracks = { "ParticleTracks", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UChaosCache, ParticleTracks), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UChaosCache_Statics::NewProp_ParticleTracks_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UChaosCache_Statics::NewProp_ParticleTracks_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UChaosCache_Statics::NewProp_CurveData_ValueProp = { "CurveData", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FRichCurve, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UChaosCache_Statics::NewProp_CurveData_Key_KeyProp = { "CurveData_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UChaosCache_Statics::NewProp_CurveData_MetaData[] = {
		{ "Comment", "/** Per component/cache curve data, any continuous data that isn't per-particle can be stored here */" },
		{ "ModuleRelativePath", "Public/Chaos/ChaosCache.h" },
		{ "ToolTip", "Per component/cache curve data, any continuous data that isn't per-particle can be stored here" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UChaosCache_Statics::NewProp_CurveData = { "CurveData", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UChaosCache, CurveData), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UChaosCache_Statics::NewProp_CurveData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UChaosCache_Statics::NewProp_CurveData_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UChaosCache_Statics::NewProp_EventTracks_ValueProp = { "EventTracks", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FCacheEventTrack, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UChaosCache_Statics::NewProp_EventTracks_Key_KeyProp = { "EventTracks_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UChaosCache_Statics::NewProp_EventTracks_MetaData[] = {
		{ "Comment", "/** Timestamped generic event tracks */" },
		{ "ModuleRelativePath", "Public/Chaos/ChaosCache.h" },
		{ "ToolTip", "Timestamped generic event tracks" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UChaosCache_Statics::NewProp_EventTracks = { "EventTracks", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UChaosCache, EventTracks), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UChaosCache_Statics::NewProp_EventTracks_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UChaosCache_Statics::NewProp_EventTracks_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UChaosCache_Statics::NewProp_Spawnable_MetaData[] = {
		{ "Category", "Caching" },
		{ "Comment", "/** Spawn template for an actor that can play this cache */" },
		{ "ModuleRelativePath", "Public/Chaos/ChaosCache.h" },
		{ "ToolTip", "Spawn template for an actor that can play this cache" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UChaosCache_Statics::NewProp_Spawnable = { "Spawnable", nullptr, (EPropertyFlags)0x0040000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UChaosCache, Spawnable), Z_Construct_UScriptStruct_FCacheSpawnableTemplate, METADATA_PARAMS(Z_Construct_UClass_UChaosCache_Statics::NewProp_Spawnable_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UChaosCache_Statics::NewProp_Spawnable_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UChaosCache_Statics::NewProp_AdapterGuid_MetaData[] = {
		{ "Comment", "/** GUID identifier for the adapter that spawned this cache */" },
		{ "ModuleRelativePath", "Public/Chaos/ChaosCache.h" },
		{ "ToolTip", "GUID identifier for the adapter that spawned this cache" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UChaosCache_Statics::NewProp_AdapterGuid = { "AdapterGuid", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UChaosCache, AdapterGuid), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UClass_UChaosCache_Statics::NewProp_AdapterGuid_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UChaosCache_Statics::NewProp_AdapterGuid_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UChaosCache_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UChaosCache_Statics::NewProp_RecordedDuration,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UChaosCache_Statics::NewProp_NumRecordedFrames,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UChaosCache_Statics::NewProp_TrackToParticle_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UChaosCache_Statics::NewProp_TrackToParticle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UChaosCache_Statics::NewProp_ParticleTracks_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UChaosCache_Statics::NewProp_ParticleTracks,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UChaosCache_Statics::NewProp_CurveData_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UChaosCache_Statics::NewProp_CurveData_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UChaosCache_Statics::NewProp_CurveData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UChaosCache_Statics::NewProp_EventTracks_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UChaosCache_Statics::NewProp_EventTracks_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UChaosCache_Statics::NewProp_EventTracks,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UChaosCache_Statics::NewProp_Spawnable,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UChaosCache_Statics::NewProp_AdapterGuid,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UChaosCache_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UChaosCache>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UChaosCache_Statics::ClassParams = {
		&UChaosCache::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UChaosCache_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UChaosCache_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UChaosCache_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UChaosCache_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UChaosCache()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UChaosCache_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UChaosCache, 973993341);
	template<> CHAOSCACHING_API UClass* StaticClass<UChaosCache>()
	{
		return UChaosCache::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UChaosCache(Z_Construct_UClass_UChaosCache, &UChaosCache::StaticClass, TEXT("/Script/ChaosCaching"), TEXT("UChaosCache"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UChaosCache);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
