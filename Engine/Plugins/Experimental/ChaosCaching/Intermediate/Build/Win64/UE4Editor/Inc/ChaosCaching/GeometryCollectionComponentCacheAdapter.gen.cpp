// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ChaosCaching/Public/Chaos/Adapters/GeometryCollectionComponentCacheAdapter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGeometryCollectionComponentCacheAdapter() {}
// Cross Module References
	CHAOSCACHING_API UScriptStruct* Z_Construct_UScriptStruct_FEnableStateEvent();
	UPackage* Z_Construct_UPackage__Script_ChaosCaching();
	CHAOSCACHING_API UScriptStruct* Z_Construct_UScriptStruct_FCacheEventBase();
// End Cross Module References

static_assert(std::is_polymorphic<FEnableStateEvent>() == std::is_polymorphic<FCacheEventBase>(), "USTRUCT FEnableStateEvent cannot be polymorphic unless super FCacheEventBase is polymorphic");

class UScriptStruct* FEnableStateEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CHAOSCACHING_API uint32 Get_Z_Construct_UScriptStruct_FEnableStateEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FEnableStateEvent, Z_Construct_UPackage__Script_ChaosCaching(), TEXT("EnableStateEvent"), sizeof(FEnableStateEvent), Get_Z_Construct_UScriptStruct_FEnableStateEvent_Hash());
	}
	return Singleton;
}
template<> CHAOSCACHING_API UScriptStruct* StaticStruct<FEnableStateEvent>()
{
	return FEnableStateEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FEnableStateEvent(FEnableStateEvent::StaticStruct, TEXT("/Script/ChaosCaching"), TEXT("EnableStateEvent"), false, nullptr, nullptr);
static struct FScriptStruct_ChaosCaching_StaticRegisterNativesFEnableStateEvent
{
	FScriptStruct_ChaosCaching_StaticRegisterNativesFEnableStateEvent()
	{
		UScriptStruct::DeferCppStructOps<FEnableStateEvent>(FName(TEXT("EnableStateEvent")));
	}
} ScriptStruct_ChaosCaching_StaticRegisterNativesFEnableStateEvent;
	struct Z_Construct_UScriptStruct_FEnableStateEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnable_MetaData[];
#endif
		static void NewProp_bEnable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnable;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEnableStateEvent_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Chaos/Adapters/GeometryCollectionComponentCacheAdapter.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FEnableStateEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FEnableStateEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEnableStateEvent_Statics::NewProp_Index_MetaData[] = {
		{ "ModuleRelativePath", "Public/Chaos/Adapters/GeometryCollectionComponentCacheAdapter.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FEnableStateEvent_Statics::NewProp_Index = { "Index", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FEnableStateEvent, Index), METADATA_PARAMS(Z_Construct_UScriptStruct_FEnableStateEvent_Statics::NewProp_Index_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEnableStateEvent_Statics::NewProp_Index_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEnableStateEvent_Statics::NewProp_bEnable_MetaData[] = {
		{ "ModuleRelativePath", "Public/Chaos/Adapters/GeometryCollectionComponentCacheAdapter.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FEnableStateEvent_Statics::NewProp_bEnable_SetBit(void* Obj)
	{
		((FEnableStateEvent*)Obj)->bEnable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FEnableStateEvent_Statics::NewProp_bEnable = { "bEnable", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FEnableStateEvent), &Z_Construct_UScriptStruct_FEnableStateEvent_Statics::NewProp_bEnable_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FEnableStateEvent_Statics::NewProp_bEnable_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEnableStateEvent_Statics::NewProp_bEnable_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FEnableStateEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEnableStateEvent_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEnableStateEvent_Statics::NewProp_bEnable,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FEnableStateEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ChaosCaching,
		Z_Construct_UScriptStruct_FCacheEventBase,
		&NewStructOps,
		"EnableStateEvent",
		sizeof(FEnableStateEvent),
		alignof(FEnableStateEvent),
		Z_Construct_UScriptStruct_FEnableStateEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEnableStateEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FEnableStateEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEnableStateEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FEnableStateEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FEnableStateEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ChaosCaching();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("EnableStateEvent"), sizeof(FEnableStateEvent), Get_Z_Construct_UScriptStruct_FEnableStateEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FEnableStateEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FEnableStateEvent_Hash() { return 3071491196U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
