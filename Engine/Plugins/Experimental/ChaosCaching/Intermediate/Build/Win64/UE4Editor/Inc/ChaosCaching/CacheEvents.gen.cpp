// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ChaosCaching/Public/Chaos/CacheEvents.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCacheEvents() {}
// Cross Module References
	CHAOSCACHING_API UScriptStruct* Z_Construct_UScriptStruct_FCacheEventTrack();
	UPackage* Z_Construct_UPackage__Script_ChaosCaching();
	COREUOBJECT_API UClass* Z_Construct_UClass_UScriptStruct();
	CHAOSCACHING_API UScriptStruct* Z_Construct_UScriptStruct_FCacheEventBase();
// End Cross Module References
class UScriptStruct* FCacheEventTrack::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CHAOSCACHING_API uint32 Get_Z_Construct_UScriptStruct_FCacheEventTrack_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCacheEventTrack, Z_Construct_UPackage__Script_ChaosCaching(), TEXT("CacheEventTrack"), sizeof(FCacheEventTrack), Get_Z_Construct_UScriptStruct_FCacheEventTrack_Hash());
	}
	return Singleton;
}
template<> CHAOSCACHING_API UScriptStruct* StaticStruct<FCacheEventTrack>()
{
	return FCacheEventTrack::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCacheEventTrack(FCacheEventTrack::StaticStruct, TEXT("/Script/ChaosCaching"), TEXT("CacheEventTrack"), false, nullptr, nullptr);
static struct FScriptStruct_ChaosCaching_StaticRegisterNativesFCacheEventTrack
{
	FScriptStruct_ChaosCaching_StaticRegisterNativesFCacheEventTrack()
	{
		UScriptStruct::DeferCppStructOps<FCacheEventTrack>(FName(TEXT("CacheEventTrack")));
	}
} ScriptStruct_ChaosCaching_StaticRegisterNativesFCacheEventTrack;
	struct Z_Construct_UScriptStruct_FCacheEventTrack_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Struct_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Struct;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TimeStamps_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimeStamps_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_TimeStamps;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCacheEventTrack_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Chaos/CacheEvents.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FCacheEventTrack_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FCacheEventTrack>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCacheEventTrack_Statics::NewProp_Name_MetaData[] = {
		{ "ModuleRelativePath", "Public/Chaos/CacheEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FCacheEventTrack_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCacheEventTrack, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FCacheEventTrack_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCacheEventTrack_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCacheEventTrack_Statics::NewProp_Struct_MetaData[] = {
		{ "Comment", "/** Type of the event stored in this track. Must inherit FCacheEventBase */" },
		{ "ModuleRelativePath", "Public/Chaos/CacheEvents.h" },
		{ "ToolTip", "Type of the event stored in this track. Must inherit FCacheEventBase" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FCacheEventTrack_Statics::NewProp_Struct = { "Struct", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCacheEventTrack, Struct), Z_Construct_UClass_UScriptStruct, METADATA_PARAMS(Z_Construct_UScriptStruct_FCacheEventTrack_Statics::NewProp_Struct_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCacheEventTrack_Statics::NewProp_Struct_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCacheEventTrack_Statics::NewProp_TimeStamps_Inner = { "TimeStamps", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCacheEventTrack_Statics::NewProp_TimeStamps_MetaData[] = {
		{ "ModuleRelativePath", "Public/Chaos/CacheEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FCacheEventTrack_Statics::NewProp_TimeStamps = { "TimeStamps", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCacheEventTrack, TimeStamps), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FCacheEventTrack_Statics::NewProp_TimeStamps_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCacheEventTrack_Statics::NewProp_TimeStamps_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FCacheEventTrack_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCacheEventTrack_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCacheEventTrack_Statics::NewProp_Struct,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCacheEventTrack_Statics::NewProp_TimeStamps_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCacheEventTrack_Statics::NewProp_TimeStamps,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FCacheEventTrack_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ChaosCaching,
		nullptr,
		&NewStructOps,
		"CacheEventTrack",
		sizeof(FCacheEventTrack),
		alignof(FCacheEventTrack),
		Z_Construct_UScriptStruct_FCacheEventTrack_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCacheEventTrack_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FCacheEventTrack_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCacheEventTrack_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FCacheEventTrack()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FCacheEventTrack_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ChaosCaching();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CacheEventTrack"), sizeof(FCacheEventTrack), Get_Z_Construct_UScriptStruct_FCacheEventTrack_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FCacheEventTrack_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCacheEventTrack_Hash() { return 4092836818U; }
class UScriptStruct* FCacheEventBase::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CHAOSCACHING_API uint32 Get_Z_Construct_UScriptStruct_FCacheEventBase_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCacheEventBase, Z_Construct_UPackage__Script_ChaosCaching(), TEXT("CacheEventBase"), sizeof(FCacheEventBase), Get_Z_Construct_UScriptStruct_FCacheEventBase_Hash());
	}
	return Singleton;
}
template<> CHAOSCACHING_API UScriptStruct* StaticStruct<FCacheEventBase>()
{
	return FCacheEventBase::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCacheEventBase(FCacheEventBase::StaticStruct, TEXT("/Script/ChaosCaching"), TEXT("CacheEventBase"), false, nullptr, nullptr);
static struct FScriptStruct_ChaosCaching_StaticRegisterNativesFCacheEventBase
{
	FScriptStruct_ChaosCaching_StaticRegisterNativesFCacheEventBase()
	{
		UScriptStruct::DeferCppStructOps<FCacheEventBase>(FName(TEXT("CacheEventBase")));
	}
} ScriptStruct_ChaosCaching_StaticRegisterNativesFCacheEventBase;
	struct Z_Construct_UScriptStruct_FCacheEventBase_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCacheEventBase_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Base type for all events, ALL events must derive from this so we have a fallback for serialization\n * when we can't find the actual event struct.\n */" },
		{ "ModuleRelativePath", "Public/Chaos/CacheEvents.h" },
		{ "ToolTip", "Base type for all events, ALL events must derive from this so we have a fallback for serialization\nwhen we can't find the actual event struct." },
	};
#endif
	void* Z_Construct_UScriptStruct_FCacheEventBase_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FCacheEventBase>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FCacheEventBase_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ChaosCaching,
		nullptr,
		&NewStructOps,
		"CacheEventBase",
		sizeof(FCacheEventBase),
		alignof(FCacheEventBase),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FCacheEventBase_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCacheEventBase_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FCacheEventBase()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FCacheEventBase_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ChaosCaching();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CacheEventBase"), sizeof(FCacheEventBase), Get_Z_Construct_UScriptStruct_FCacheEventBase_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FCacheEventBase_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCacheEventBase_Hash() { return 2865620746U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
