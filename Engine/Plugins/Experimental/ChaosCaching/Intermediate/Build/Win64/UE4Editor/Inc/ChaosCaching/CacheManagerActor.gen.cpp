// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ChaosCaching/Public/Chaos/CacheManagerActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCacheManagerActor() {}
// Cross Module References
	CHAOSCACHING_API UEnum* Z_Construct_UEnum_ChaosCaching_EStartMode();
	UPackage* Z_Construct_UPackage__Script_ChaosCaching();
	CHAOSCACHING_API UEnum* Z_Construct_UEnum_ChaosCaching_ECacheMode();
	CHAOSCACHING_API UScriptStruct* Z_Construct_UScriptStruct_FObservedComponent();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FComponentReference();
	CHAOSCACHING_API UClass* Z_Construct_UClass_AChaosCacheManager_NoRegister();
	CHAOSCACHING_API UClass* Z_Construct_UClass_AChaosCacheManager();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	ENGINE_API UClass* Z_Construct_UClass_UPrimitiveComponent_NoRegister();
	CHAOSCACHING_API UClass* Z_Construct_UClass_UChaosCacheCollection_NoRegister();
// End Cross Module References
	static UEnum* EStartMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ChaosCaching_EStartMode, Z_Construct_UPackage__Script_ChaosCaching(), TEXT("EStartMode"));
		}
		return Singleton;
	}
	template<> CHAOSCACHING_API UEnum* StaticEnum<EStartMode>()
	{
		return EStartMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EStartMode(EStartMode_StaticEnum, TEXT("/Script/ChaosCaching"), TEXT("EStartMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ChaosCaching_EStartMode_Hash() { return 157679380U; }
	UEnum* Z_Construct_UEnum_ChaosCaching_EStartMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ChaosCaching();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EStartMode"), 0, Get_Z_Construct_UEnum_ChaosCaching_EStartMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EStartMode::Timed", (int64)EStartMode::Timed },
				{ "EStartMode::Triggered", (int64)EStartMode::Triggered },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/Chaos/CacheManagerActor.h" },
				{ "Timed.Name", "EStartMode::Timed" },
				{ "Triggered.Name", "EStartMode::Triggered" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ChaosCaching,
				nullptr,
				"EStartMode",
				"EStartMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ECacheMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ChaosCaching_ECacheMode, Z_Construct_UPackage__Script_ChaosCaching(), TEXT("ECacheMode"));
		}
		return Singleton;
	}
	template<> CHAOSCACHING_API UEnum* StaticEnum<ECacheMode>()
	{
		return ECacheMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ECacheMode(ECacheMode_StaticEnum, TEXT("/Script/ChaosCaching"), TEXT("ECacheMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ChaosCaching_ECacheMode_Hash() { return 3966844973U; }
	UEnum* Z_Construct_UEnum_ChaosCaching_ECacheMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ChaosCaching();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ECacheMode"), 0, Get_Z_Construct_UEnum_ChaosCaching_ECacheMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ECacheMode::None", (int64)ECacheMode::None },
				{ "ECacheMode::Play", (int64)ECacheMode::Play },
				{ "ECacheMode::Record", (int64)ECacheMode::Record },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/Chaos/CacheManagerActor.h" },
				{ "None.Name", "ECacheMode::None" },
				{ "Play.Name", "ECacheMode::Play" },
				{ "Record.Name", "ECacheMode::Record" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ChaosCaching,
				nullptr,
				"ECacheMode",
				"ECacheMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FObservedComponent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CHAOSCACHING_API uint32 Get_Z_Construct_UScriptStruct_FObservedComponent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FObservedComponent, Z_Construct_UPackage__Script_ChaosCaching(), TEXT("ObservedComponent"), sizeof(FObservedComponent), Get_Z_Construct_UScriptStruct_FObservedComponent_Hash());
	}
	return Singleton;
}
template<> CHAOSCACHING_API UScriptStruct* StaticStruct<FObservedComponent>()
{
	return FObservedComponent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FObservedComponent(FObservedComponent::StaticStruct, TEXT("/Script/ChaosCaching"), TEXT("ObservedComponent"), false, nullptr, nullptr);
static struct FScriptStruct_ChaosCaching_StaticRegisterNativesFObservedComponent
{
	FScriptStruct_ChaosCaching_StaticRegisterNativesFObservedComponent()
	{
		UScriptStruct::DeferCppStructOps<FObservedComponent>(FName(TEXT("ObservedComponent")));
	}
} ScriptStruct_ChaosCaching_StaticRegisterNativesFObservedComponent;
	struct Z_Construct_UScriptStruct_FObservedComponent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CacheName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_CacheName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ComponentRef_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ComponentRef;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_CacheMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CacheMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_CacheMode;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_StartMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StartMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_StartMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimedDuration_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TimedDuration;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FObservedComponent_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Chaos/CacheManagerActor.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FObservedComponent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FObservedComponent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FObservedComponent_Statics::NewProp_CacheName_MetaData[] = {
		{ "Category", "Caching" },
		{ "Comment", "/** Unique name for the cache, used as a key into the cache collection */" },
		{ "ModuleRelativePath", "Public/Chaos/CacheManagerActor.h" },
		{ "ToolTip", "Unique name for the cache, used as a key into the cache collection" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FObservedComponent_Statics::NewProp_CacheName = { "CacheName", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FObservedComponent, CacheName), METADATA_PARAMS(Z_Construct_UScriptStruct_FObservedComponent_Statics::NewProp_CacheName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FObservedComponent_Statics::NewProp_CacheName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FObservedComponent_Statics::NewProp_ComponentRef_MetaData[] = {
		{ "AllowAnyActor", "" },
		{ "Category", "Caching" },
		{ "Comment", "/** The component observed by this object for either playback or recording */" },
		{ "ModuleRelativePath", "Public/Chaos/CacheManagerActor.h" },
		{ "ToolTip", "The component observed by this object for either playback or recording" },
		{ "UseComponentPicker", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FObservedComponent_Statics::NewProp_ComponentRef = { "ComponentRef", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FObservedComponent, ComponentRef), Z_Construct_UScriptStruct_FComponentReference, METADATA_PARAMS(Z_Construct_UScriptStruct_FObservedComponent_Statics::NewProp_ComponentRef_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FObservedComponent_Statics::NewProp_ComponentRef_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FObservedComponent_Statics::NewProp_CacheMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FObservedComponent_Statics::NewProp_CacheMode_MetaData[] = {
		{ "Category", "Caching" },
		{ "Comment", "/** How to use the cache - playback or record */" },
		{ "ModuleRelativePath", "Public/Chaos/CacheManagerActor.h" },
		{ "ToolTip", "How to use the cache - playback or record" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FObservedComponent_Statics::NewProp_CacheMode = { "CacheMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FObservedComponent, CacheMode), Z_Construct_UEnum_ChaosCaching_ECacheMode, METADATA_PARAMS(Z_Construct_UScriptStruct_FObservedComponent_Statics::NewProp_CacheMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FObservedComponent_Statics::NewProp_CacheMode_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FObservedComponent_Statics::NewProp_StartMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FObservedComponent_Statics::NewProp_StartMode_MetaData[] = {
		{ "Category", "Caching" },
		{ "Comment", "/**\n\x09 * How to trigger the cache record or playback, timed will start counting at BeginPlay, Triggered will begin\n\x09 * counting from when the owning cache manager is requested to trigger the cache action\n\x09 * @see AChaosCacheManager::TriggerObservedComponent\n\x09 */" },
		{ "ModuleRelativePath", "Public/Chaos/CacheManagerActor.h" },
		{ "ToolTip", "How to trigger the cache record or playback, timed will start counting at BeginPlay, Triggered will begin\ncounting from when the owning cache manager is requested to trigger the cache action\n@see AChaosCacheManager::TriggerObservedComponent" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FObservedComponent_Statics::NewProp_StartMode = { "StartMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FObservedComponent, StartMode), Z_Construct_UEnum_ChaosCaching_EStartMode, METADATA_PARAMS(Z_Construct_UScriptStruct_FObservedComponent_Statics::NewProp_StartMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FObservedComponent_Statics::NewProp_StartMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FObservedComponent_Statics::NewProp_TimedDuration_MetaData[] = {
		{ "Category", "Caching" },
		{ "Comment", "/**\n\x09 * The time after BeginPlay or a call to AChaosCacheManager::TriggerObservedComponent to wait before beginning\n\x09 * the playback or recording of the component\n\x09 */" },
		{ "ModuleRelativePath", "Public/Chaos/CacheManagerActor.h" },
		{ "ToolTip", "The time after BeginPlay or a call to AChaosCacheManager::TriggerObservedComponent to wait before beginning\nthe playback or recording of the component" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FObservedComponent_Statics::NewProp_TimedDuration = { "TimedDuration", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FObservedComponent, TimedDuration), METADATA_PARAMS(Z_Construct_UScriptStruct_FObservedComponent_Statics::NewProp_TimedDuration_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FObservedComponent_Statics::NewProp_TimedDuration_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FObservedComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FObservedComponent_Statics::NewProp_CacheName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FObservedComponent_Statics::NewProp_ComponentRef,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FObservedComponent_Statics::NewProp_CacheMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FObservedComponent_Statics::NewProp_CacheMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FObservedComponent_Statics::NewProp_StartMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FObservedComponent_Statics::NewProp_StartMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FObservedComponent_Statics::NewProp_TimedDuration,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FObservedComponent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ChaosCaching,
		nullptr,
		&NewStructOps,
		"ObservedComponent",
		sizeof(FObservedComponent),
		alignof(FObservedComponent),
		Z_Construct_UScriptStruct_FObservedComponent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FObservedComponent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FObservedComponent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FObservedComponent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FObservedComponent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FObservedComponent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ChaosCaching();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ObservedComponent"), sizeof(FObservedComponent), Get_Z_Construct_UScriptStruct_FObservedComponent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FObservedComponent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FObservedComponent_Hash() { return 1537053974U; }
	DEFINE_FUNCTION(AChaosCacheManager::execTriggerAll)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->TriggerAll();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AChaosCacheManager::execTriggerComponentByCache)
	{
		P_GET_PROPERTY(FNameProperty,Z_Param_InCacheName);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->TriggerComponentByCache(Z_Param_InCacheName);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AChaosCacheManager::execTriggerComponent)
	{
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_InComponent);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->TriggerComponent(Z_Param_InComponent);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AChaosCacheManager::execResetSingleTransform)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_InIndex);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ResetSingleTransform(Z_Param_InIndex);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AChaosCacheManager::execResetAllComponentTransforms)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ResetAllComponentTransforms();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AChaosCacheManager::execSetAllMode)
	{
		P_GET_ENUM(ECacheMode,Z_Param_InMode);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetAllMode(ECacheMode(Z_Param_InMode));
		P_NATIVE_END;
	}
	void AChaosCacheManager::StaticRegisterNativesAChaosCacheManager()
	{
		UClass* Class = AChaosCacheManager::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ResetAllComponentTransforms", &AChaosCacheManager::execResetAllComponentTransforms },
			{ "ResetSingleTransform", &AChaosCacheManager::execResetSingleTransform },
			{ "SetAllMode", &AChaosCacheManager::execSetAllMode },
			{ "TriggerAll", &AChaosCacheManager::execTriggerAll },
			{ "TriggerComponent", &AChaosCacheManager::execTriggerComponent },
			{ "TriggerComponentByCache", &AChaosCacheManager::execTriggerComponentByCache },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AChaosCacheManager_ResetAllComponentTransforms_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AChaosCacheManager_ResetAllComponentTransforms_Statics::Function_MetaDataParams[] = {
		{ "Category", "Caching" },
		{ "Comment", "/** \n\x09 * Resets all components back to the world space transform they had when the cache for them was originally recorded\n\x09 * if one is available\n\x09 */" },
		{ "ModuleRelativePath", "Public/Chaos/CacheManagerActor.h" },
		{ "ToolTip", "Resets all components back to the world space transform they had when the cache for them was originally recorded\nif one is available" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AChaosCacheManager_ResetAllComponentTransforms_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AChaosCacheManager, nullptr, "ResetAllComponentTransforms", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AChaosCacheManager_ResetAllComponentTransforms_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AChaosCacheManager_ResetAllComponentTransforms_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AChaosCacheManager_ResetAllComponentTransforms()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AChaosCacheManager_ResetAllComponentTransforms_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AChaosCacheManager_ResetSingleTransform_Statics
	{
		struct ChaosCacheManager_eventResetSingleTransform_Parms
		{
			int32 InIndex;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_InIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_AChaosCacheManager_ResetSingleTransform_Statics::NewProp_InIndex = { "InIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ChaosCacheManager_eventResetSingleTransform_Parms, InIndex), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AChaosCacheManager_ResetSingleTransform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AChaosCacheManager_ResetSingleTransform_Statics::NewProp_InIndex,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AChaosCacheManager_ResetSingleTransform_Statics::Function_MetaDataParams[] = {
		{ "Category", "Caching" },
		{ "Comment", "/**\n\x09 * Resets the component at the specified index in the observed list back to the world space transform it had when the \n\x09 * cache for it was originally recorded if one is available\n\x09 * @param InIndex Index of the component to reset\n\x09 */" },
		{ "ModuleRelativePath", "Public/Chaos/CacheManagerActor.h" },
		{ "ToolTip", "Resets the component at the specified index in the observed list back to the world space transform it had when the\ncache for it was originally recorded if one is available\n@param InIndex Index of the component to reset" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AChaosCacheManager_ResetSingleTransform_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AChaosCacheManager, nullptr, "ResetSingleTransform", nullptr, nullptr, sizeof(ChaosCacheManager_eventResetSingleTransform_Parms), Z_Construct_UFunction_AChaosCacheManager_ResetSingleTransform_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AChaosCacheManager_ResetSingleTransform_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AChaosCacheManager_ResetSingleTransform_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AChaosCacheManager_ResetSingleTransform_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AChaosCacheManager_ResetSingleTransform()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AChaosCacheManager_ResetSingleTransform_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AChaosCacheManager_SetAllMode_Statics
	{
		struct ChaosCacheManager_eventSetAllMode_Parms
		{
			ECacheMode InMode;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InMode_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InMode;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_AChaosCacheManager_SetAllMode_Statics::NewProp_InMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_AChaosCacheManager_SetAllMode_Statics::NewProp_InMode = { "InMode", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ChaosCacheManager_eventSetAllMode_Parms, InMode), Z_Construct_UEnum_ChaosCaching_ECacheMode, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AChaosCacheManager_SetAllMode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AChaosCacheManager_SetAllMode_Statics::NewProp_InMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AChaosCacheManager_SetAllMode_Statics::NewProp_InMode,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AChaosCacheManager_SetAllMode_Statics::Function_MetaDataParams[] = {
		{ "Category", "Caching" },
		{ "Comment", "/**\n\x09 * Sets the playback mode of every observed component to the specified mode\n\x09 * @param InMode Mode to set\n\x09 */" },
		{ "ModuleRelativePath", "Public/Chaos/CacheManagerActor.h" },
		{ "ToolTip", "Sets the playback mode of every observed component to the specified mode\n@param InMode Mode to set" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AChaosCacheManager_SetAllMode_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AChaosCacheManager, nullptr, "SetAllMode", nullptr, nullptr, sizeof(ChaosCacheManager_eventSetAllMode_Parms), Z_Construct_UFunction_AChaosCacheManager_SetAllMode_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AChaosCacheManager_SetAllMode_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AChaosCacheManager_SetAllMode_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AChaosCacheManager_SetAllMode_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AChaosCacheManager_SetAllMode()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AChaosCacheManager_SetAllMode_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AChaosCacheManager_TriggerAll_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AChaosCacheManager_TriggerAll_Statics::Function_MetaDataParams[] = {
		{ "Category", "Caching" },
		{ "Comment", "/** Triggers the recording or playback of all observed components */" },
		{ "ModuleRelativePath", "Public/Chaos/CacheManagerActor.h" },
		{ "ToolTip", "Triggers the recording or playback of all observed components" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AChaosCacheManager_TriggerAll_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AChaosCacheManager, nullptr, "TriggerAll", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AChaosCacheManager_TriggerAll_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AChaosCacheManager_TriggerAll_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AChaosCacheManager_TriggerAll()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AChaosCacheManager_TriggerAll_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AChaosCacheManager_TriggerComponent_Statics
	{
		struct ChaosCacheManager_eventTriggerComponent_Parms
		{
			UPrimitiveComponent* InComponent;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AChaosCacheManager_TriggerComponent_Statics::NewProp_InComponent_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AChaosCacheManager_TriggerComponent_Statics::NewProp_InComponent = { "InComponent", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ChaosCacheManager_eventTriggerComponent_Parms, InComponent), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_AChaosCacheManager_TriggerComponent_Statics::NewProp_InComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AChaosCacheManager_TriggerComponent_Statics::NewProp_InComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AChaosCacheManager_TriggerComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AChaosCacheManager_TriggerComponent_Statics::NewProp_InComponent,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AChaosCacheManager_TriggerComponent_Statics::Function_MetaDataParams[] = {
		{ "Category", "Caching" },
		{ "Comment", "/**\n\x09 * Triggers a component to play or record.\n\x09 * If the cache manager has an observed component entry for InComponent and it is a triggered entry\n\x09 * this will begin the playback or record for that component, otherwise no action is taken.\n\x09 * @param InComponent Component to trigger\n\x09 */" },
		{ "ModuleRelativePath", "Public/Chaos/CacheManagerActor.h" },
		{ "ToolTip", "Triggers a component to play or record.\nIf the cache manager has an observed component entry for InComponent and it is a triggered entry\nthis will begin the playback or record for that component, otherwise no action is taken.\n@param InComponent Component to trigger" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AChaosCacheManager_TriggerComponent_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AChaosCacheManager, nullptr, "TriggerComponent", nullptr, nullptr, sizeof(ChaosCacheManager_eventTriggerComponent_Parms), Z_Construct_UFunction_AChaosCacheManager_TriggerComponent_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AChaosCacheManager_TriggerComponent_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AChaosCacheManager_TriggerComponent_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AChaosCacheManager_TriggerComponent_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AChaosCacheManager_TriggerComponent()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AChaosCacheManager_TriggerComponent_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AChaosCacheManager_TriggerComponentByCache_Statics
	{
		struct ChaosCacheManager_eventTriggerComponentByCache_Parms
		{
			FName InCacheName;
		};
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_InCacheName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_AChaosCacheManager_TriggerComponentByCache_Statics::NewProp_InCacheName = { "InCacheName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ChaosCacheManager_eventTriggerComponentByCache_Parms, InCacheName), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AChaosCacheManager_TriggerComponentByCache_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AChaosCacheManager_TriggerComponentByCache_Statics::NewProp_InCacheName,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AChaosCacheManager_TriggerComponentByCache_Statics::Function_MetaDataParams[] = {
		{ "Category", "Caching" },
		{ "Comment", "/**\n\x09 * Triggers a component to play or record.\n\x09 * Searches the observed component list for an entry matching InCacheName and triggers the\n\x09 * playback or recording of the linked observed component\n\x09 * @param InCacheName Cache name to trigger\n\x09 */" },
		{ "ModuleRelativePath", "Public/Chaos/CacheManagerActor.h" },
		{ "ToolTip", "Triggers a component to play or record.\nSearches the observed component list for an entry matching InCacheName and triggers the\nplayback or recording of the linked observed component\n@param InCacheName Cache name to trigger" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AChaosCacheManager_TriggerComponentByCache_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AChaosCacheManager, nullptr, "TriggerComponentByCache", nullptr, nullptr, sizeof(ChaosCacheManager_eventTriggerComponentByCache_Parms), Z_Construct_UFunction_AChaosCacheManager_TriggerComponentByCache_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AChaosCacheManager_TriggerComponentByCache_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AChaosCacheManager_TriggerComponentByCache_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AChaosCacheManager_TriggerComponentByCache_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AChaosCacheManager_TriggerComponentByCache()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AChaosCacheManager_TriggerComponentByCache_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AChaosCacheManager_NoRegister()
	{
		return AChaosCacheManager::StaticClass();
	}
	struct Z_Construct_UClass_AChaosCacheManager_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CacheCollection_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CacheCollection;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ObservedComponents_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObservedComponents_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ObservedComponents;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AChaosCacheManager_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_ChaosCaching,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AChaosCacheManager_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AChaosCacheManager_ResetAllComponentTransforms, "ResetAllComponentTransforms" }, // 2814529713
		{ &Z_Construct_UFunction_AChaosCacheManager_ResetSingleTransform, "ResetSingleTransform" }, // 3212415777
		{ &Z_Construct_UFunction_AChaosCacheManager_SetAllMode, "SetAllMode" }, // 2805384980
		{ &Z_Construct_UFunction_AChaosCacheManager_TriggerAll, "TriggerAll" }, // 1051732432
		{ &Z_Construct_UFunction_AChaosCacheManager_TriggerComponent, "TriggerComponent" }, // 3079140944
		{ &Z_Construct_UFunction_AChaosCacheManager_TriggerComponentByCache, "TriggerComponentByCache" }, // 1141441699
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AChaosCacheManager_Statics::Class_MetaDataParams[] = {
		{ "DevelopmentStatus", "Experimental" },
		{ "IncludePath", "Chaos/CacheManagerActor.h" },
		{ "ModuleRelativePath", "Public/Chaos/CacheManagerActor.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AChaosCacheManager_Statics::NewProp_CacheCollection_MetaData[] = {
		{ "Category", "Caching" },
		{ "Comment", "/**\n\x09 * The Cache Collection asset to use for this observer. This can be used for playback and record simultaneously\n\x09 * across multiple components depending on the settings for that component.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Chaos/CacheManagerActor.h" },
		{ "ToolTip", "The Cache Collection asset to use for this observer. This can be used for playback and record simultaneously\nacross multiple components depending on the settings for that component." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AChaosCacheManager_Statics::NewProp_CacheCollection = { "CacheCollection", nullptr, (EPropertyFlags)0x0010000000000801, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AChaosCacheManager, CacheCollection), Z_Construct_UClass_UChaosCacheCollection_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AChaosCacheManager_Statics::NewProp_CacheCollection_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AChaosCacheManager_Statics::NewProp_CacheCollection_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AChaosCacheManager_Statics::NewProp_ObservedComponents_Inner = { "ObservedComponents", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FObservedComponent, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AChaosCacheManager_Statics::NewProp_ObservedComponents_MetaData[] = {
		{ "Category", "Caching" },
		{ "Comment", "/** List of observed objects and their caches */" },
		{ "ModuleRelativePath", "Public/Chaos/CacheManagerActor.h" },
		{ "ToolTip", "List of observed objects and their caches" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AChaosCacheManager_Statics::NewProp_ObservedComponents = { "ObservedComponents", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AChaosCacheManager, ObservedComponents), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AChaosCacheManager_Statics::NewProp_ObservedComponents_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AChaosCacheManager_Statics::NewProp_ObservedComponents_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AChaosCacheManager_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AChaosCacheManager_Statics::NewProp_CacheCollection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AChaosCacheManager_Statics::NewProp_ObservedComponents_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AChaosCacheManager_Statics::NewProp_ObservedComponents,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AChaosCacheManager_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AChaosCacheManager>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AChaosCacheManager_Statics::ClassParams = {
		&AChaosCacheManager::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AChaosCacheManager_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AChaosCacheManager_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AChaosCacheManager_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AChaosCacheManager_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AChaosCacheManager()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AChaosCacheManager_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AChaosCacheManager, 1856077364);
	template<> CHAOSCACHING_API UClass* StaticClass<AChaosCacheManager>()
	{
		return AChaosCacheManager::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AChaosCacheManager(Z_Construct_UClass_AChaosCacheManager, &AChaosCacheManager::StaticClass, TEXT("/Script/ChaosCaching"), TEXT("AChaosCacheManager"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AChaosCacheManager);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
