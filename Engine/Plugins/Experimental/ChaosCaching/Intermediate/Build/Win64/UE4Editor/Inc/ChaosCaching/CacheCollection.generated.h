// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CHAOSCACHING_CacheCollection_generated_h
#error "CacheCollection.generated.h already included, missing '#pragma once' in CacheCollection.h"
#endif
#define CHAOSCACHING_CacheCollection_generated_h

#define Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheCollection_h_12_SPARSE_DATA
#define Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheCollection_h_12_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheCollection_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheCollection_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUChaosCacheCollection(); \
	friend struct Z_Construct_UClass_UChaosCacheCollection_Statics; \
public: \
	DECLARE_CLASS(UChaosCacheCollection, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ChaosCaching"), NO_API) \
	DECLARE_SERIALIZER(UChaosCacheCollection)


#define Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheCollection_h_12_INCLASS \
private: \
	static void StaticRegisterNativesUChaosCacheCollection(); \
	friend struct Z_Construct_UClass_UChaosCacheCollection_Statics; \
public: \
	DECLARE_CLASS(UChaosCacheCollection, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ChaosCaching"), NO_API) \
	DECLARE_SERIALIZER(UChaosCacheCollection)


#define Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheCollection_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UChaosCacheCollection(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UChaosCacheCollection) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UChaosCacheCollection); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UChaosCacheCollection); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UChaosCacheCollection(UChaosCacheCollection&&); \
	NO_API UChaosCacheCollection(const UChaosCacheCollection&); \
public:


#define Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheCollection_h_12_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UChaosCacheCollection(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UChaosCacheCollection(UChaosCacheCollection&&); \
	NO_API UChaosCacheCollection(const UChaosCacheCollection&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UChaosCacheCollection); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UChaosCacheCollection); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UChaosCacheCollection)


#define Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheCollection_h_12_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheCollection_h_9_PROLOG
#define Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheCollection_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheCollection_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheCollection_h_12_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheCollection_h_12_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheCollection_h_12_INCLASS \
	Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheCollection_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheCollection_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheCollection_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheCollection_h_12_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheCollection_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheCollection_h_12_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheCollection_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CHAOSCACHING_API UClass* StaticClass<class UChaosCacheCollection>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ChaosCaching_Source_ChaosCaching_Public_Chaos_CacheCollection_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
