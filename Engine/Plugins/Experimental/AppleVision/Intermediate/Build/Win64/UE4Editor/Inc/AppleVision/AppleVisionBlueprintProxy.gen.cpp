// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AppleVision/Public/AppleVisionBlueprintProxy.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAppleVisionBlueprintProxy() {}
// Cross Module References
	APPLEVISION_API UFunction* Z_Construct_UDelegateFunction_AppleVision_AppleVisionDetectFacesDelegate__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_AppleVision();
	APPLEVISION_API UScriptStruct* Z_Construct_UScriptStruct_FFaceDetectionResult();
	APPLEVISION_API UClass* Z_Construct_UClass_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_NoRegister();
	APPLEVISION_API UClass* Z_Construct_UClass_UAppleVisionDetectFacesAsyncTaskBlueprintProxy();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	ENGINE_API UClass* Z_Construct_UClass_UTexture_NoRegister();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_AppleVision_AppleVisionDetectFacesDelegate__DelegateSignature_Statics
	{
		struct _Script_AppleVision_eventAppleVisionDetectFacesDelegate_Parms
		{
			FFaceDetectionResult FaceDetectionResult;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FaceDetectionResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FaceDetectionResult;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_AppleVision_AppleVisionDetectFacesDelegate__DelegateSignature_Statics::NewProp_FaceDetectionResult_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_AppleVision_AppleVisionDetectFacesDelegate__DelegateSignature_Statics::NewProp_FaceDetectionResult = { "FaceDetectionResult", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_AppleVision_eventAppleVisionDetectFacesDelegate_Parms, FaceDetectionResult), Z_Construct_UScriptStruct_FFaceDetectionResult, METADATA_PARAMS(Z_Construct_UDelegateFunction_AppleVision_AppleVisionDetectFacesDelegate__DelegateSignature_Statics::NewProp_FaceDetectionResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_AppleVision_AppleVisionDetectFacesDelegate__DelegateSignature_Statics::NewProp_FaceDetectionResult_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_AppleVision_AppleVisionDetectFacesDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_AppleVision_AppleVisionDetectFacesDelegate__DelegateSignature_Statics::NewProp_FaceDetectionResult,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_AppleVision_AppleVisionDetectFacesDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/AppleVisionBlueprintProxy.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_AppleVision_AppleVisionDetectFacesDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_AppleVision, nullptr, "AppleVisionDetectFacesDelegate__DelegateSignature", nullptr, nullptr, sizeof(_Script_AppleVision_eventAppleVisionDetectFacesDelegate_Parms), Z_Construct_UDelegateFunction_AppleVision_AppleVisionDetectFacesDelegate__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_AppleVision_AppleVisionDetectFacesDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_AppleVision_AppleVisionDetectFacesDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_AppleVision_AppleVisionDetectFacesDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_AppleVision_AppleVisionDetectFacesDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_AppleVision_AppleVisionDetectFacesDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(UAppleVisionDetectFacesAsyncTaskBlueprintProxy::execCreateProxyObjectForDetectFaces)
	{
		P_GET_OBJECT(UTexture,Z_Param_SourceImage);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UAppleVisionDetectFacesAsyncTaskBlueprintProxy**)Z_Param__Result=UAppleVisionDetectFacesAsyncTaskBlueprintProxy::CreateProxyObjectForDetectFaces(Z_Param_SourceImage);
		P_NATIVE_END;
	}
	void UAppleVisionDetectFacesAsyncTaskBlueprintProxy::StaticRegisterNativesUAppleVisionDetectFacesAsyncTaskBlueprintProxy()
	{
		UClass* Class = UAppleVisionDetectFacesAsyncTaskBlueprintProxy::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CreateProxyObjectForDetectFaces", &UAppleVisionDetectFacesAsyncTaskBlueprintProxy::execCreateProxyObjectForDetectFaces },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_CreateProxyObjectForDetectFaces_Statics
	{
		struct AppleVisionDetectFacesAsyncTaskBlueprintProxy_eventCreateProxyObjectForDetectFaces_Parms
		{
			UTexture* SourceImage;
			UAppleVisionDetectFacesAsyncTaskBlueprintProxy* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SourceImage;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_CreateProxyObjectForDetectFaces_Statics::NewProp_SourceImage = { "SourceImage", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AppleVisionDetectFacesAsyncTaskBlueprintProxy_eventCreateProxyObjectForDetectFaces_Parms, SourceImage), Z_Construct_UClass_UTexture_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_CreateProxyObjectForDetectFaces_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AppleVisionDetectFacesAsyncTaskBlueprintProxy_eventCreateProxyObjectForDetectFaces_Parms, ReturnValue), Z_Construct_UClass_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_CreateProxyObjectForDetectFaces_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_CreateProxyObjectForDetectFaces_Statics::NewProp_SourceImage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_CreateProxyObjectForDetectFaces_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_CreateProxyObjectForDetectFaces_Statics::Function_MetaDataParams[] = {
		{ "BlueprintInternalUseOnly", "true" },
		{ "Category", "Apple Vision" },
		{ "Comment", "/**\n\x09 * Detects faces within an image\n\x09 *\n\x09 * @param SourceImage the image to detect faces in\n\x09 */" },
		{ "DisplayName", "Detect Faces" },
		{ "ModuleRelativePath", "Public/AppleVisionBlueprintProxy.h" },
		{ "ToolTip", "Detects faces within an image\n\n@param SourceImage the image to detect faces in" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_CreateProxyObjectForDetectFaces_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAppleVisionDetectFacesAsyncTaskBlueprintProxy, nullptr, "CreateProxyObjectForDetectFaces", nullptr, nullptr, sizeof(AppleVisionDetectFacesAsyncTaskBlueprintProxy_eventCreateProxyObjectForDetectFaces_Parms), Z_Construct_UFunction_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_CreateProxyObjectForDetectFaces_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_CreateProxyObjectForDetectFaces_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_CreateProxyObjectForDetectFaces_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_CreateProxyObjectForDetectFaces_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_CreateProxyObjectForDetectFaces()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_CreateProxyObjectForDetectFaces_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_NoRegister()
	{
		return UAppleVisionDetectFacesAsyncTaskBlueprintProxy::StaticClass();
	}
	struct Z_Construct_UClass_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnSuccess_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnSuccess;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnFailure_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnFailure;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FaceDetectionResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FaceDetectionResult;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_AppleVision,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_CreateProxyObjectForDetectFaces, "CreateProxyObjectForDetectFaces" }, // 238392797
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AppleVisionBlueprintProxy.h" },
		{ "ModuleRelativePath", "Public/AppleVisionBlueprintProxy.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_Statics::NewProp_OnSuccess_MetaData[] = {
		{ "ModuleRelativePath", "Public/AppleVisionBlueprintProxy.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_Statics::NewProp_OnSuccess = { "OnSuccess", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAppleVisionDetectFacesAsyncTaskBlueprintProxy, OnSuccess), Z_Construct_UDelegateFunction_AppleVision_AppleVisionDetectFacesDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_Statics::NewProp_OnSuccess_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_Statics::NewProp_OnSuccess_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_Statics::NewProp_OnFailure_MetaData[] = {
		{ "ModuleRelativePath", "Public/AppleVisionBlueprintProxy.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_Statics::NewProp_OnFailure = { "OnFailure", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAppleVisionDetectFacesAsyncTaskBlueprintProxy, OnFailure), Z_Construct_UDelegateFunction_AppleVision_AppleVisionDetectFacesDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_Statics::NewProp_OnFailure_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_Statics::NewProp_OnFailure_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_Statics::NewProp_FaceDetectionResult_MetaData[] = {
		{ "Category", "Apple Vision" },
		{ "ModuleRelativePath", "Public/AppleVisionBlueprintProxy.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_Statics::NewProp_FaceDetectionResult = { "FaceDetectionResult", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAppleVisionDetectFacesAsyncTaskBlueprintProxy, FaceDetectionResult), Z_Construct_UScriptStruct_FFaceDetectionResult, METADATA_PARAMS(Z_Construct_UClass_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_Statics::NewProp_FaceDetectionResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_Statics::NewProp_FaceDetectionResult_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_Statics::NewProp_OnSuccess,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_Statics::NewProp_OnFailure,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_Statics::NewProp_FaceDetectionResult,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAppleVisionDetectFacesAsyncTaskBlueprintProxy>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_Statics::ClassParams = {
		&UAppleVisionDetectFacesAsyncTaskBlueprintProxy::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_Statics::PropPointers),
		0,
		0x008800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAppleVisionDetectFacesAsyncTaskBlueprintProxy()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAppleVisionDetectFacesAsyncTaskBlueprintProxy_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAppleVisionDetectFacesAsyncTaskBlueprintProxy, 43879571);
	template<> APPLEVISION_API UClass* StaticClass<UAppleVisionDetectFacesAsyncTaskBlueprintProxy>()
	{
		return UAppleVisionDetectFacesAsyncTaskBlueprintProxy::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAppleVisionDetectFacesAsyncTaskBlueprintProxy(Z_Construct_UClass_UAppleVisionDetectFacesAsyncTaskBlueprintProxy, &UAppleVisionDetectFacesAsyncTaskBlueprintProxy::StaticClass, TEXT("/Script/AppleVision"), TEXT("UAppleVisionDetectFacesAsyncTaskBlueprintProxy"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAppleVisionDetectFacesAsyncTaskBlueprintProxy);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
