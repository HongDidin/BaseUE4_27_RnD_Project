// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AppleVision/Public/AppleVisionTypes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAppleVisionTypes() {}
// Cross Module References
	APPLEVISION_API UEnum* Z_Construct_UEnum_AppleVision_EDetectedFaceFeatureType();
	UPackage* Z_Construct_UPackage__Script_AppleVision();
	APPLEVISION_API UScriptStruct* Z_Construct_UScriptStruct_FFaceDetectionResult();
	APPLEVISION_API UScriptStruct* Z_Construct_UScriptStruct_FDetectedFace();
	APPLEVISION_API UScriptStruct* Z_Construct_UScriptStruct_FDetectedFeatureRegion();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FBox2D();
	APPLEVISION_API UScriptStruct* Z_Construct_UScriptStruct_FDetectedFaceFeature2D();
	APPLEVISION_API UScriptStruct* Z_Construct_UScriptStruct_FDetectedFaceFeatureRegion();
	APPLEVISION_API UScriptStruct* Z_Construct_UScriptStruct_FDetectedFeature2D();
	APPLEVISION_API UScriptStruct* Z_Construct_UScriptStruct_FDetectedFeature();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
// End Cross Module References
	static UEnum* EDetectedFaceFeatureType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_AppleVision_EDetectedFaceFeatureType, Z_Construct_UPackage__Script_AppleVision(), TEXT("EDetectedFaceFeatureType"));
		}
		return Singleton;
	}
	template<> APPLEVISION_API UEnum* StaticEnum<EDetectedFaceFeatureType>()
	{
		return EDetectedFaceFeatureType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDetectedFaceFeatureType(EDetectedFaceFeatureType_StaticEnum, TEXT("/Script/AppleVision"), TEXT("EDetectedFaceFeatureType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_AppleVision_EDetectedFaceFeatureType_Hash() { return 2691116517U; }
	UEnum* Z_Construct_UEnum_AppleVision_EDetectedFaceFeatureType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_AppleVision();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDetectedFaceFeatureType"), 0, Get_Z_Construct_UEnum_AppleVision_EDetectedFaceFeatureType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDetectedFaceFeatureType::Unkown", (int64)EDetectedFaceFeatureType::Unkown },
				{ "EDetectedFaceFeatureType::FaceContour", (int64)EDetectedFaceFeatureType::FaceContour },
				{ "EDetectedFaceFeatureType::InnerLips", (int64)EDetectedFaceFeatureType::InnerLips },
				{ "EDetectedFaceFeatureType::LeftEye", (int64)EDetectedFaceFeatureType::LeftEye },
				{ "EDetectedFaceFeatureType::LeftEyebrow", (int64)EDetectedFaceFeatureType::LeftEyebrow },
				{ "EDetectedFaceFeatureType::LeftPupil", (int64)EDetectedFaceFeatureType::LeftPupil },
				{ "EDetectedFaceFeatureType::MedianLine", (int64)EDetectedFaceFeatureType::MedianLine },
				{ "EDetectedFaceFeatureType::Nose", (int64)EDetectedFaceFeatureType::Nose },
				{ "EDetectedFaceFeatureType::NoseCrest", (int64)EDetectedFaceFeatureType::NoseCrest },
				{ "EDetectedFaceFeatureType::OuterLips", (int64)EDetectedFaceFeatureType::OuterLips },
				{ "EDetectedFaceFeatureType::RightEye", (int64)EDetectedFaceFeatureType::RightEye },
				{ "EDetectedFaceFeatureType::RightEyebrow", (int64)EDetectedFaceFeatureType::RightEyebrow },
				{ "EDetectedFaceFeatureType::RightPupil", (int64)EDetectedFaceFeatureType::RightPupil },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Category", "Apple Vision" },
				{ "Comment", "/** Features of a face that can be detected */" },
				{ "Experimental", "" },
				{ "FaceContour.Name", "EDetectedFaceFeatureType::FaceContour" },
				{ "InnerLips.Name", "EDetectedFaceFeatureType::InnerLips" },
				{ "LeftEye.Name", "EDetectedFaceFeatureType::LeftEye" },
				{ "LeftEyebrow.Name", "EDetectedFaceFeatureType::LeftEyebrow" },
				{ "LeftPupil.Name", "EDetectedFaceFeatureType::LeftPupil" },
				{ "MedianLine.Name", "EDetectedFaceFeatureType::MedianLine" },
				{ "ModuleRelativePath", "Public/AppleVisionTypes.h" },
				{ "Nose.Name", "EDetectedFaceFeatureType::Nose" },
				{ "NoseCrest.Name", "EDetectedFaceFeatureType::NoseCrest" },
				{ "OuterLips.Name", "EDetectedFaceFeatureType::OuterLips" },
				{ "RightEye.Name", "EDetectedFaceFeatureType::RightEye" },
				{ "RightEyebrow.Name", "EDetectedFaceFeatureType::RightEyebrow" },
				{ "RightPupil.Name", "EDetectedFaceFeatureType::RightPupil" },
				{ "ToolTip", "Features of a face that can be detected" },
				{ "Unkown.Name", "EDetectedFaceFeatureType::Unkown" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_AppleVision,
				nullptr,
				"EDetectedFaceFeatureType",
				"EDetectedFaceFeatureType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FFaceDetectionResult::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern APPLEVISION_API uint32 Get_Z_Construct_UScriptStruct_FFaceDetectionResult_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FFaceDetectionResult, Z_Construct_UPackage__Script_AppleVision(), TEXT("FaceDetectionResult"), sizeof(FFaceDetectionResult), Get_Z_Construct_UScriptStruct_FFaceDetectionResult_Hash());
	}
	return Singleton;
}
template<> APPLEVISION_API UScriptStruct* StaticStruct<FFaceDetectionResult>()
{
	return FFaceDetectionResult::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FFaceDetectionResult(FFaceDetectionResult::StaticStruct, TEXT("/Script/AppleVision"), TEXT("FaceDetectionResult"), false, nullptr, nullptr);
static struct FScriptStruct_AppleVision_StaticRegisterNativesFFaceDetectionResult
{
	FScriptStruct_AppleVision_StaticRegisterNativesFFaceDetectionResult()
	{
		UScriptStruct::DeferCppStructOps<FFaceDetectionResult>(FName(TEXT("FaceDetectionResult")));
	}
} ScriptStruct_AppleVision_StaticRegisterNativesFFaceDetectionResult;
	struct Z_Construct_UScriptStruct_FFaceDetectionResult_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DetectedFaces_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DetectedFaces_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DetectedFaces;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFaceDetectionResult_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** The result of a face detection request with information about the detected faces */" },
		{ "ModuleRelativePath", "Public/AppleVisionTypes.h" },
		{ "ToolTip", "The result of a face detection request with information about the detected faces" },
	};
#endif
	void* Z_Construct_UScriptStruct_FFaceDetectionResult_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FFaceDetectionResult>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FFaceDetectionResult_Statics::NewProp_DetectedFaces_Inner = { "DetectedFaces", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDetectedFace, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFaceDetectionResult_Statics::NewProp_DetectedFaces_MetaData[] = {
		{ "Category", "Apple Vision" },
		{ "Comment", "/** The set of faces that were detected */" },
		{ "ModuleRelativePath", "Public/AppleVisionTypes.h" },
		{ "ToolTip", "The set of faces that were detected" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FFaceDetectionResult_Statics::NewProp_DetectedFaces = { "DetectedFaces", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFaceDetectionResult, DetectedFaces), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FFaceDetectionResult_Statics::NewProp_DetectedFaces_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFaceDetectionResult_Statics::NewProp_DetectedFaces_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FFaceDetectionResult_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFaceDetectionResult_Statics::NewProp_DetectedFaces_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFaceDetectionResult_Statics::NewProp_DetectedFaces,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FFaceDetectionResult_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_AppleVision,
		nullptr,
		&NewStructOps,
		"FaceDetectionResult",
		sizeof(FFaceDetectionResult),
		alignof(FFaceDetectionResult),
		Z_Construct_UScriptStruct_FFaceDetectionResult_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFaceDetectionResult_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FFaceDetectionResult_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFaceDetectionResult_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FFaceDetectionResult()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FFaceDetectionResult_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_AppleVision();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("FaceDetectionResult"), sizeof(FFaceDetectionResult), Get_Z_Construct_UScriptStruct_FFaceDetectionResult_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FFaceDetectionResult_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FFaceDetectionResult_Hash() { return 4002723187U; }

static_assert(std::is_polymorphic<FDetectedFace>() == std::is_polymorphic<FDetectedFeatureRegion>(), "USTRUCT FDetectedFace cannot be polymorphic unless super FDetectedFeatureRegion is polymorphic");

class UScriptStruct* FDetectedFace::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern APPLEVISION_API uint32 Get_Z_Construct_UScriptStruct_FDetectedFace_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDetectedFace, Z_Construct_UPackage__Script_AppleVision(), TEXT("DetectedFace"), sizeof(FDetectedFace), Get_Z_Construct_UScriptStruct_FDetectedFace_Hash());
	}
	return Singleton;
}
template<> APPLEVISION_API UScriptStruct* StaticStruct<FDetectedFace>()
{
	return FDetectedFace::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDetectedFace(FDetectedFace::StaticStruct, TEXT("/Script/AppleVision"), TEXT("DetectedFace"), false, nullptr, nullptr);
static struct FScriptStruct_AppleVision_StaticRegisterNativesFDetectedFace
{
	FScriptStruct_AppleVision_StaticRegisterNativesFDetectedFace()
	{
		UScriptStruct::DeferCppStructOps<FDetectedFace>(FName(TEXT("DetectedFace")));
	}
} ScriptStruct_AppleVision_StaticRegisterNativesFDetectedFace;
	struct Z_Construct_UScriptStruct_FDetectedFace_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoundingBox_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BoundingBox;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Features_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Features_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Features;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FeatureRegions_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FeatureRegions_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FeatureRegions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDetectedFace_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** Area of the image that the computer vision task detected as being a face */" },
		{ "ModuleRelativePath", "Public/AppleVisionTypes.h" },
		{ "ToolTip", "Area of the image that the computer vision task detected as being a face" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDetectedFace_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDetectedFace>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDetectedFace_Statics::NewProp_BoundingBox_MetaData[] = {
		{ "Category", "Apple Vision" },
		{ "Comment", "/** The bounding box of the detected face */" },
		{ "ModuleRelativePath", "Public/AppleVisionTypes.h" },
		{ "ToolTip", "The bounding box of the detected face" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDetectedFace_Statics::NewProp_BoundingBox = { "BoundingBox", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDetectedFace, BoundingBox), Z_Construct_UScriptStruct_FBox2D, METADATA_PARAMS(Z_Construct_UScriptStruct_FDetectedFace_Statics::NewProp_BoundingBox_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDetectedFace_Statics::NewProp_BoundingBox_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDetectedFace_Statics::NewProp_Features_Inner = { "Features", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDetectedFaceFeature2D, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDetectedFace_Statics::NewProp_Features_MetaData[] = {
		{ "Category", "Apple Vision" },
		{ "Comment", "/** The set of 2D features that were detected */" },
		{ "ModuleRelativePath", "Public/AppleVisionTypes.h" },
		{ "ToolTip", "The set of 2D features that were detected" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDetectedFace_Statics::NewProp_Features = { "Features", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDetectedFace, Features), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDetectedFace_Statics::NewProp_Features_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDetectedFace_Statics::NewProp_Features_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDetectedFace_Statics::NewProp_FeatureRegions_Inner = { "FeatureRegions", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDetectedFaceFeatureRegion, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDetectedFace_Statics::NewProp_FeatureRegions_MetaData[] = {
		{ "Category", "Apple Vision" },
		{ "Comment", "/** The set of region features that were detected */" },
		{ "ModuleRelativePath", "Public/AppleVisionTypes.h" },
		{ "ToolTip", "The set of region features that were detected" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDetectedFace_Statics::NewProp_FeatureRegions = { "FeatureRegions", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDetectedFace, FeatureRegions), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDetectedFace_Statics::NewProp_FeatureRegions_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDetectedFace_Statics::NewProp_FeatureRegions_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDetectedFace_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDetectedFace_Statics::NewProp_BoundingBox,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDetectedFace_Statics::NewProp_Features_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDetectedFace_Statics::NewProp_Features,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDetectedFace_Statics::NewProp_FeatureRegions_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDetectedFace_Statics::NewProp_FeatureRegions,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDetectedFace_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_AppleVision,
		Z_Construct_UScriptStruct_FDetectedFeatureRegion,
		&NewStructOps,
		"DetectedFace",
		sizeof(FDetectedFace),
		alignof(FDetectedFace),
		Z_Construct_UScriptStruct_FDetectedFace_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDetectedFace_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDetectedFace_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDetectedFace_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDetectedFace()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDetectedFace_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_AppleVision();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DetectedFace"), sizeof(FDetectedFace), Get_Z_Construct_UScriptStruct_FDetectedFace_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDetectedFace_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDetectedFace_Hash() { return 483884925U; }

static_assert(std::is_polymorphic<FDetectedFaceFeature2D>() == std::is_polymorphic<FDetectedFeature2D>(), "USTRUCT FDetectedFaceFeature2D cannot be polymorphic unless super FDetectedFeature2D is polymorphic");

class UScriptStruct* FDetectedFaceFeature2D::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern APPLEVISION_API uint32 Get_Z_Construct_UScriptStruct_FDetectedFaceFeature2D_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDetectedFaceFeature2D, Z_Construct_UPackage__Script_AppleVision(), TEXT("DetectedFaceFeature2D"), sizeof(FDetectedFaceFeature2D), Get_Z_Construct_UScriptStruct_FDetectedFaceFeature2D_Hash());
	}
	return Singleton;
}
template<> APPLEVISION_API UScriptStruct* StaticStruct<FDetectedFaceFeature2D>()
{
	return FDetectedFaceFeature2D::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDetectedFaceFeature2D(FDetectedFaceFeature2D::StaticStruct, TEXT("/Script/AppleVision"), TEXT("DetectedFaceFeature2D"), false, nullptr, nullptr);
static struct FScriptStruct_AppleVision_StaticRegisterNativesFDetectedFaceFeature2D
{
	FScriptStruct_AppleVision_StaticRegisterNativesFDetectedFaceFeature2D()
	{
		UScriptStruct::DeferCppStructOps<FDetectedFaceFeature2D>(FName(TEXT("DetectedFaceFeature2D")));
	}
} ScriptStruct_AppleVision_StaticRegisterNativesFDetectedFaceFeature2D;
	struct Z_Construct_UScriptStruct_FDetectedFaceFeature2D_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_FeatureType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FeatureType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_FeatureType;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDetectedFaceFeature2D_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** Area of the image that the computer vision task detected as being part of a face */" },
		{ "ModuleRelativePath", "Public/AppleVisionTypes.h" },
		{ "ToolTip", "Area of the image that the computer vision task detected as being part of a face" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDetectedFaceFeature2D_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDetectedFaceFeature2D>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDetectedFaceFeature2D_Statics::NewProp_FeatureType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDetectedFaceFeature2D_Statics::NewProp_FeatureType_MetaData[] = {
		{ "Category", "Apple Vision" },
		{ "Comment", "/** The type of region that was detected */" },
		{ "ModuleRelativePath", "Public/AppleVisionTypes.h" },
		{ "ToolTip", "The type of region that was detected" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FDetectedFaceFeature2D_Statics::NewProp_FeatureType = { "FeatureType", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDetectedFaceFeature2D, FeatureType), Z_Construct_UEnum_AppleVision_EDetectedFaceFeatureType, METADATA_PARAMS(Z_Construct_UScriptStruct_FDetectedFaceFeature2D_Statics::NewProp_FeatureType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDetectedFaceFeature2D_Statics::NewProp_FeatureType_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDetectedFaceFeature2D_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDetectedFaceFeature2D_Statics::NewProp_FeatureType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDetectedFaceFeature2D_Statics::NewProp_FeatureType,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDetectedFaceFeature2D_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_AppleVision,
		Z_Construct_UScriptStruct_FDetectedFeature2D,
		&NewStructOps,
		"DetectedFaceFeature2D",
		sizeof(FDetectedFaceFeature2D),
		alignof(FDetectedFaceFeature2D),
		Z_Construct_UScriptStruct_FDetectedFaceFeature2D_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDetectedFaceFeature2D_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDetectedFaceFeature2D_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDetectedFaceFeature2D_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDetectedFaceFeature2D()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDetectedFaceFeature2D_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_AppleVision();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DetectedFaceFeature2D"), sizeof(FDetectedFaceFeature2D), Get_Z_Construct_UScriptStruct_FDetectedFaceFeature2D_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDetectedFaceFeature2D_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDetectedFaceFeature2D_Hash() { return 245785998U; }

static_assert(std::is_polymorphic<FDetectedFaceFeatureRegion>() == std::is_polymorphic<FDetectedFeatureRegion>(), "USTRUCT FDetectedFaceFeatureRegion cannot be polymorphic unless super FDetectedFeatureRegion is polymorphic");

class UScriptStruct* FDetectedFaceFeatureRegion::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern APPLEVISION_API uint32 Get_Z_Construct_UScriptStruct_FDetectedFaceFeatureRegion_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDetectedFaceFeatureRegion, Z_Construct_UPackage__Script_AppleVision(), TEXT("DetectedFaceFeatureRegion"), sizeof(FDetectedFaceFeatureRegion), Get_Z_Construct_UScriptStruct_FDetectedFaceFeatureRegion_Hash());
	}
	return Singleton;
}
template<> APPLEVISION_API UScriptStruct* StaticStruct<FDetectedFaceFeatureRegion>()
{
	return FDetectedFaceFeatureRegion::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDetectedFaceFeatureRegion(FDetectedFaceFeatureRegion::StaticStruct, TEXT("/Script/AppleVision"), TEXT("DetectedFaceFeatureRegion"), false, nullptr, nullptr);
static struct FScriptStruct_AppleVision_StaticRegisterNativesFDetectedFaceFeatureRegion
{
	FScriptStruct_AppleVision_StaticRegisterNativesFDetectedFaceFeatureRegion()
	{
		UScriptStruct::DeferCppStructOps<FDetectedFaceFeatureRegion>(FName(TEXT("DetectedFaceFeatureRegion")));
	}
} ScriptStruct_AppleVision_StaticRegisterNativesFDetectedFaceFeatureRegion;
	struct Z_Construct_UScriptStruct_FDetectedFaceFeatureRegion_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_FeatureType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FeatureType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_FeatureType;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDetectedFaceFeatureRegion_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** Area of the image that the computer vision task detected as being part of a face */" },
		{ "ModuleRelativePath", "Public/AppleVisionTypes.h" },
		{ "ToolTip", "Area of the image that the computer vision task detected as being part of a face" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDetectedFaceFeatureRegion_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDetectedFaceFeatureRegion>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDetectedFaceFeatureRegion_Statics::NewProp_FeatureType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDetectedFaceFeatureRegion_Statics::NewProp_FeatureType_MetaData[] = {
		{ "Category", "Apple Vision" },
		{ "Comment", "/** The type of region that was detected */" },
		{ "ModuleRelativePath", "Public/AppleVisionTypes.h" },
		{ "ToolTip", "The type of region that was detected" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FDetectedFaceFeatureRegion_Statics::NewProp_FeatureType = { "FeatureType", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDetectedFaceFeatureRegion, FeatureType), Z_Construct_UEnum_AppleVision_EDetectedFaceFeatureType, METADATA_PARAMS(Z_Construct_UScriptStruct_FDetectedFaceFeatureRegion_Statics::NewProp_FeatureType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDetectedFaceFeatureRegion_Statics::NewProp_FeatureType_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDetectedFaceFeatureRegion_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDetectedFaceFeatureRegion_Statics::NewProp_FeatureType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDetectedFaceFeatureRegion_Statics::NewProp_FeatureType,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDetectedFaceFeatureRegion_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_AppleVision,
		Z_Construct_UScriptStruct_FDetectedFeatureRegion,
		&NewStructOps,
		"DetectedFaceFeatureRegion",
		sizeof(FDetectedFaceFeatureRegion),
		alignof(FDetectedFaceFeatureRegion),
		Z_Construct_UScriptStruct_FDetectedFaceFeatureRegion_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDetectedFaceFeatureRegion_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDetectedFaceFeatureRegion_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDetectedFaceFeatureRegion_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDetectedFaceFeatureRegion()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDetectedFaceFeatureRegion_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_AppleVision();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DetectedFaceFeatureRegion"), sizeof(FDetectedFaceFeatureRegion), Get_Z_Construct_UScriptStruct_FDetectedFaceFeatureRegion_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDetectedFaceFeatureRegion_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDetectedFaceFeatureRegion_Hash() { return 3229850331U; }

static_assert(std::is_polymorphic<FDetectedFeatureRegion>() == std::is_polymorphic<FDetectedFeature>(), "USTRUCT FDetectedFeatureRegion cannot be polymorphic unless super FDetectedFeature is polymorphic");

class UScriptStruct* FDetectedFeatureRegion::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern APPLEVISION_API uint32 Get_Z_Construct_UScriptStruct_FDetectedFeatureRegion_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDetectedFeatureRegion, Z_Construct_UPackage__Script_AppleVision(), TEXT("DetectedFeatureRegion"), sizeof(FDetectedFeatureRegion), Get_Z_Construct_UScriptStruct_FDetectedFeatureRegion_Hash());
	}
	return Singleton;
}
template<> APPLEVISION_API UScriptStruct* StaticStruct<FDetectedFeatureRegion>()
{
	return FDetectedFeatureRegion::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDetectedFeatureRegion(FDetectedFeatureRegion::StaticStruct, TEXT("/Script/AppleVision"), TEXT("DetectedFeatureRegion"), false, nullptr, nullptr);
static struct FScriptStruct_AppleVision_StaticRegisterNativesFDetectedFeatureRegion
{
	FScriptStruct_AppleVision_StaticRegisterNativesFDetectedFeatureRegion()
	{
		UScriptStruct::DeferCppStructOps<FDetectedFeatureRegion>(FName(TEXT("DetectedFeatureRegion")));
	}
} ScriptStruct_AppleVision_StaticRegisterNativesFDetectedFeatureRegion;
	struct Z_Construct_UScriptStruct_FDetectedFeatureRegion_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Points_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Points_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Points;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDetectedFeatureRegion_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** Area of the image that the computer vision task detected as being of a particular type */" },
		{ "ModuleRelativePath", "Public/AppleVisionTypes.h" },
		{ "ToolTip", "Area of the image that the computer vision task detected as being of a particular type" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDetectedFeatureRegion_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDetectedFeatureRegion>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDetectedFeatureRegion_Statics::NewProp_Points_Inner = { "Points", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDetectedFeatureRegion_Statics::NewProp_Points_MetaData[] = {
		{ "Category", "Apple Vision" },
		{ "Comment", "/** The set of points that encompass the detected feature area */" },
		{ "ModuleRelativePath", "Public/AppleVisionTypes.h" },
		{ "ToolTip", "The set of points that encompass the detected feature area" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDetectedFeatureRegion_Statics::NewProp_Points = { "Points", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDetectedFeatureRegion, Points), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDetectedFeatureRegion_Statics::NewProp_Points_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDetectedFeatureRegion_Statics::NewProp_Points_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDetectedFeatureRegion_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDetectedFeatureRegion_Statics::NewProp_Points_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDetectedFeatureRegion_Statics::NewProp_Points,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDetectedFeatureRegion_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_AppleVision,
		Z_Construct_UScriptStruct_FDetectedFeature,
		&NewStructOps,
		"DetectedFeatureRegion",
		sizeof(FDetectedFeatureRegion),
		alignof(FDetectedFeatureRegion),
		Z_Construct_UScriptStruct_FDetectedFeatureRegion_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDetectedFeatureRegion_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDetectedFeatureRegion_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDetectedFeatureRegion_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDetectedFeatureRegion()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDetectedFeatureRegion_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_AppleVision();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DetectedFeatureRegion"), sizeof(FDetectedFeatureRegion), Get_Z_Construct_UScriptStruct_FDetectedFeatureRegion_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDetectedFeatureRegion_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDetectedFeatureRegion_Hash() { return 978760221U; }

static_assert(std::is_polymorphic<FDetectedFeature2D>() == std::is_polymorphic<FDetectedFeature>(), "USTRUCT FDetectedFeature2D cannot be polymorphic unless super FDetectedFeature is polymorphic");

class UScriptStruct* FDetectedFeature2D::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern APPLEVISION_API uint32 Get_Z_Construct_UScriptStruct_FDetectedFeature2D_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDetectedFeature2D, Z_Construct_UPackage__Script_AppleVision(), TEXT("DetectedFeature2D"), sizeof(FDetectedFeature2D), Get_Z_Construct_UScriptStruct_FDetectedFeature2D_Hash());
	}
	return Singleton;
}
template<> APPLEVISION_API UScriptStruct* StaticStruct<FDetectedFeature2D>()
{
	return FDetectedFeature2D::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDetectedFeature2D(FDetectedFeature2D::StaticStruct, TEXT("/Script/AppleVision"), TEXT("DetectedFeature2D"), false, nullptr, nullptr);
static struct FScriptStruct_AppleVision_StaticRegisterNativesFDetectedFeature2D
{
	FScriptStruct_AppleVision_StaticRegisterNativesFDetectedFeature2D()
	{
		UScriptStruct::DeferCppStructOps<FDetectedFeature2D>(FName(TEXT("DetectedFeature2D")));
	}
} ScriptStruct_AppleVision_StaticRegisterNativesFDetectedFeature2D;
	struct Z_Construct_UScriptStruct_FDetectedFeature2D_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoundingBox_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BoundingBox;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDetectedFeature2D_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** Area of the image that the computer vision task detected as being of a particular type */" },
		{ "ModuleRelativePath", "Public/AppleVisionTypes.h" },
		{ "ToolTip", "Area of the image that the computer vision task detected as being of a particular type" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDetectedFeature2D_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDetectedFeature2D>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDetectedFeature2D_Statics::NewProp_BoundingBox_MetaData[] = {
		{ "Category", "Apple Vision" },
		{ "Comment", "/** The bounding box of the detected feature in the image */" },
		{ "ModuleRelativePath", "Public/AppleVisionTypes.h" },
		{ "ToolTip", "The bounding box of the detected feature in the image" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDetectedFeature2D_Statics::NewProp_BoundingBox = { "BoundingBox", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDetectedFeature2D, BoundingBox), Z_Construct_UScriptStruct_FBox2D, METADATA_PARAMS(Z_Construct_UScriptStruct_FDetectedFeature2D_Statics::NewProp_BoundingBox_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDetectedFeature2D_Statics::NewProp_BoundingBox_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDetectedFeature2D_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDetectedFeature2D_Statics::NewProp_BoundingBox,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDetectedFeature2D_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_AppleVision,
		Z_Construct_UScriptStruct_FDetectedFeature,
		&NewStructOps,
		"DetectedFeature2D",
		sizeof(FDetectedFeature2D),
		alignof(FDetectedFeature2D),
		Z_Construct_UScriptStruct_FDetectedFeature2D_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDetectedFeature2D_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDetectedFeature2D_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDetectedFeature2D_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDetectedFeature2D()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDetectedFeature2D_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_AppleVision();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DetectedFeature2D"), sizeof(FDetectedFeature2D), Get_Z_Construct_UScriptStruct_FDetectedFeature2D_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDetectedFeature2D_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDetectedFeature2D_Hash() { return 530772000U; }
class UScriptStruct* FDetectedFeature::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern APPLEVISION_API uint32 Get_Z_Construct_UScriptStruct_FDetectedFeature_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDetectedFeature, Z_Construct_UPackage__Script_AppleVision(), TEXT("DetectedFeature"), sizeof(FDetectedFeature), Get_Z_Construct_UScriptStruct_FDetectedFeature_Hash());
	}
	return Singleton;
}
template<> APPLEVISION_API UScriptStruct* StaticStruct<FDetectedFeature>()
{
	return FDetectedFeature::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDetectedFeature(FDetectedFeature::StaticStruct, TEXT("/Script/AppleVision"), TEXT("DetectedFeature"), false, nullptr, nullptr);
static struct FScriptStruct_AppleVision_StaticRegisterNativesFDetectedFeature
{
	FScriptStruct_AppleVision_StaticRegisterNativesFDetectedFeature()
	{
		UScriptStruct::DeferCppStructOps<FDetectedFeature>(FName(TEXT("DetectedFeature")));
	}
} ScriptStruct_AppleVision_StaticRegisterNativesFDetectedFeature;
	struct Z_Construct_UScriptStruct_FDetectedFeature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Confidence_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Confidence;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDetectedFeature_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** Base type for a detected feature */" },
		{ "ModuleRelativePath", "Public/AppleVisionTypes.h" },
		{ "ToolTip", "Base type for a detected feature" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDetectedFeature_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDetectedFeature>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDetectedFeature_Statics::NewProp_Confidence_MetaData[] = {
		{ "Category", "Apple Vision" },
		{ "Comment", "/** How confident the ML was in determining this feature and its type */" },
		{ "ModuleRelativePath", "Public/AppleVisionTypes.h" },
		{ "ToolTip", "How confident the ML was in determining this feature and its type" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDetectedFeature_Statics::NewProp_Confidence = { "Confidence", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDetectedFeature, Confidence), METADATA_PARAMS(Z_Construct_UScriptStruct_FDetectedFeature_Statics::NewProp_Confidence_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDetectedFeature_Statics::NewProp_Confidence_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDetectedFeature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDetectedFeature_Statics::NewProp_Confidence,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDetectedFeature_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_AppleVision,
		nullptr,
		&NewStructOps,
		"DetectedFeature",
		sizeof(FDetectedFeature),
		alignof(FDetectedFeature),
		Z_Construct_UScriptStruct_FDetectedFeature_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDetectedFeature_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDetectedFeature_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDetectedFeature_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDetectedFeature()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDetectedFeature_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_AppleVision();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DetectedFeature"), sizeof(FDetectedFeature), Get_Z_Construct_UScriptStruct_FDetectedFeature_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDetectedFeature_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDetectedFeature_Hash() { return 3691713086U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
