// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AppleVisionBlueprintSupport/Classes/AppleVisionBlueprintSupport.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAppleVisionBlueprintSupport() {}
// Cross Module References
	APPLEVISIONBLUEPRINTSUPPORT_API UClass* Z_Construct_UClass_UK2Node_DetectFaces_NoRegister();
	APPLEVISIONBLUEPRINTSUPPORT_API UClass* Z_Construct_UClass_UK2Node_DetectFaces();
	BLUEPRINTGRAPH_API UClass* Z_Construct_UClass_UK2Node_BaseAsyncTask();
	UPackage* Z_Construct_UPackage__Script_AppleVisionBlueprintSupport();
// End Cross Module References
	void UK2Node_DetectFaces::StaticRegisterNativesUK2Node_DetectFaces()
	{
	}
	UClass* Z_Construct_UClass_UK2Node_DetectFaces_NoRegister()
	{
		return UK2Node_DetectFaces::StaticClass();
	}
	struct Z_Construct_UClass_UK2Node_DetectFaces_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UK2Node_DetectFaces_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UK2Node_BaseAsyncTask,
		(UObject* (*)())Z_Construct_UPackage__Script_AppleVisionBlueprintSupport,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UK2Node_DetectFaces_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AppleVisionBlueprintSupport.h" },
		{ "ModuleRelativePath", "Classes/AppleVisionBlueprintSupport.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UK2Node_DetectFaces_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UK2Node_DetectFaces>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UK2Node_DetectFaces_Statics::ClassParams = {
		&UK2Node_DetectFaces::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UK2Node_DetectFaces_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UK2Node_DetectFaces_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UK2Node_DetectFaces()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UK2Node_DetectFaces_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UK2Node_DetectFaces, 3793950986);
	template<> APPLEVISIONBLUEPRINTSUPPORT_API UClass* StaticClass<UK2Node_DetectFaces>()
	{
		return UK2Node_DetectFaces::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UK2Node_DetectFaces(Z_Construct_UClass_UK2Node_DetectFaces, &UK2Node_DetectFaces::StaticClass, TEXT("/Script/AppleVisionBlueprintSupport"), TEXT("UK2Node_DetectFaces"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UK2Node_DetectFaces);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
