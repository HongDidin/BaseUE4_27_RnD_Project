// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef APPLEVISIONBLUEPRINTSUPPORT_AppleVisionBlueprintSupport_generated_h
#error "AppleVisionBlueprintSupport.generated.h already included, missing '#pragma once' in AppleVisionBlueprintSupport.h"
#endif
#define APPLEVISIONBLUEPRINTSUPPORT_AppleVisionBlueprintSupport_generated_h

#define Engine_Plugins_Experimental_AppleVision_Source_AppleVisionBlueprintSupport_Classes_AppleVisionBlueprintSupport_h_15_SPARSE_DATA
#define Engine_Plugins_Experimental_AppleVision_Source_AppleVisionBlueprintSupport_Classes_AppleVisionBlueprintSupport_h_15_RPC_WRAPPERS
#define Engine_Plugins_Experimental_AppleVision_Source_AppleVisionBlueprintSupport_Classes_AppleVisionBlueprintSupport_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_AppleVision_Source_AppleVisionBlueprintSupport_Classes_AppleVisionBlueprintSupport_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUK2Node_DetectFaces(); \
	friend struct Z_Construct_UClass_UK2Node_DetectFaces_Statics; \
public: \
	DECLARE_CLASS(UK2Node_DetectFaces, UK2Node_BaseAsyncTask, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AppleVisionBlueprintSupport"), NO_API) \
	DECLARE_SERIALIZER(UK2Node_DetectFaces)


#define Engine_Plugins_Experimental_AppleVision_Source_AppleVisionBlueprintSupport_Classes_AppleVisionBlueprintSupport_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUK2Node_DetectFaces(); \
	friend struct Z_Construct_UClass_UK2Node_DetectFaces_Statics; \
public: \
	DECLARE_CLASS(UK2Node_DetectFaces, UK2Node_BaseAsyncTask, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AppleVisionBlueprintSupport"), NO_API) \
	DECLARE_SERIALIZER(UK2Node_DetectFaces)


#define Engine_Plugins_Experimental_AppleVision_Source_AppleVisionBlueprintSupport_Classes_AppleVisionBlueprintSupport_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UK2Node_DetectFaces(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UK2Node_DetectFaces) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UK2Node_DetectFaces); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UK2Node_DetectFaces); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UK2Node_DetectFaces(UK2Node_DetectFaces&&); \
	NO_API UK2Node_DetectFaces(const UK2Node_DetectFaces&); \
public:


#define Engine_Plugins_Experimental_AppleVision_Source_AppleVisionBlueprintSupport_Classes_AppleVisionBlueprintSupport_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UK2Node_DetectFaces(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UK2Node_DetectFaces(UK2Node_DetectFaces&&); \
	NO_API UK2Node_DetectFaces(const UK2Node_DetectFaces&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UK2Node_DetectFaces); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UK2Node_DetectFaces); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UK2Node_DetectFaces)


#define Engine_Plugins_Experimental_AppleVision_Source_AppleVisionBlueprintSupport_Classes_AppleVisionBlueprintSupport_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_AppleVision_Source_AppleVisionBlueprintSupport_Classes_AppleVisionBlueprintSupport_h_11_PROLOG
#define Engine_Plugins_Experimental_AppleVision_Source_AppleVisionBlueprintSupport_Classes_AppleVisionBlueprintSupport_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_AppleVision_Source_AppleVisionBlueprintSupport_Classes_AppleVisionBlueprintSupport_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_AppleVision_Source_AppleVisionBlueprintSupport_Classes_AppleVisionBlueprintSupport_h_15_SPARSE_DATA \
	Engine_Plugins_Experimental_AppleVision_Source_AppleVisionBlueprintSupport_Classes_AppleVisionBlueprintSupport_h_15_RPC_WRAPPERS \
	Engine_Plugins_Experimental_AppleVision_Source_AppleVisionBlueprintSupport_Classes_AppleVisionBlueprintSupport_h_15_INCLASS \
	Engine_Plugins_Experimental_AppleVision_Source_AppleVisionBlueprintSupport_Classes_AppleVisionBlueprintSupport_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_AppleVision_Source_AppleVisionBlueprintSupport_Classes_AppleVisionBlueprintSupport_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_AppleVision_Source_AppleVisionBlueprintSupport_Classes_AppleVisionBlueprintSupport_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_AppleVision_Source_AppleVisionBlueprintSupport_Classes_AppleVisionBlueprintSupport_h_15_SPARSE_DATA \
	Engine_Plugins_Experimental_AppleVision_Source_AppleVisionBlueprintSupport_Classes_AppleVisionBlueprintSupport_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_AppleVision_Source_AppleVisionBlueprintSupport_Classes_AppleVisionBlueprintSupport_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_AppleVision_Source_AppleVisionBlueprintSupport_Classes_AppleVisionBlueprintSupport_h_15_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class K2Node_DetectFaces."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> APPLEVISIONBLUEPRINTSUPPORT_API UClass* StaticClass<class UK2Node_DetectFaces>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_AppleVision_Source_AppleVisionBlueprintSupport_Classes_AppleVisionBlueprintSupport_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
