// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Source/Private/SampleTools/MeasureDistanceSampleTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMeasureDistanceSampleTool() {}
// Cross Module References
	SAMPLETOOLSEDITORMODE_API UClass* Z_Construct_UClass_UMeasureDistanceSampleToolBuilder_NoRegister();
	SAMPLETOOLSEDITORMODE_API UClass* Z_Construct_UClass_UMeasureDistanceSampleToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	UPackage* Z_Construct_UPackage__Script_SampleToolsEditorMode();
	SAMPLETOOLSEDITORMODE_API UClass* Z_Construct_UClass_UMeasureDistanceProperties_NoRegister();
	SAMPLETOOLSEDITORMODE_API UClass* Z_Construct_UClass_UMeasureDistanceProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	SAMPLETOOLSEDITORMODE_API UClass* Z_Construct_UClass_UMeasureDistanceSampleTool_NoRegister();
	SAMPLETOOLSEDITORMODE_API UClass* Z_Construct_UClass_UMeasureDistanceSampleTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveTool();
// End Cross Module References
	void UMeasureDistanceSampleToolBuilder::StaticRegisterNativesUMeasureDistanceSampleToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UMeasureDistanceSampleToolBuilder_NoRegister()
	{
		return UMeasureDistanceSampleToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UMeasureDistanceSampleToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeasureDistanceSampleToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_SampleToolsEditorMode,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeasureDistanceSampleToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Builder for UMeasureDistanceSampleTool\n */" },
		{ "IncludePath", "SampleTools/MeasureDistanceSampleTool.h" },
		{ "ModuleRelativePath", "Private/SampleTools/MeasureDistanceSampleTool.h" },
		{ "ToolTip", "Builder for UMeasureDistanceSampleTool" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeasureDistanceSampleToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeasureDistanceSampleToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeasureDistanceSampleToolBuilder_Statics::ClassParams = {
		&UMeasureDistanceSampleToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeasureDistanceSampleToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeasureDistanceSampleToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeasureDistanceSampleToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeasureDistanceSampleToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeasureDistanceSampleToolBuilder, 1781798474);
	template<> SAMPLETOOLSEDITORMODE_API UClass* StaticClass<UMeasureDistanceSampleToolBuilder>()
	{
		return UMeasureDistanceSampleToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeasureDistanceSampleToolBuilder(Z_Construct_UClass_UMeasureDistanceSampleToolBuilder, &UMeasureDistanceSampleToolBuilder::StaticClass, TEXT("/Script/SampleToolsEditorMode"), TEXT("UMeasureDistanceSampleToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeasureDistanceSampleToolBuilder);
	void UMeasureDistanceProperties::StaticRegisterNativesUMeasureDistanceProperties()
	{
	}
	UClass* Z_Construct_UClass_UMeasureDistanceProperties_NoRegister()
	{
		return UMeasureDistanceProperties::StaticClass();
	}
	struct Z_Construct_UClass_UMeasureDistanceProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StartPoint_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_StartPoint;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EndPoint_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EndPoint;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Distance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Distance;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeasureDistanceProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_SampleToolsEditorMode,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeasureDistanceProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Property set for the UMeasureDistanceSampleTool\n */" },
		{ "IncludePath", "SampleTools/MeasureDistanceSampleTool.h" },
		{ "ModuleRelativePath", "Private/SampleTools/MeasureDistanceSampleTool.h" },
		{ "ToolTip", "Property set for the UMeasureDistanceSampleTool" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeasureDistanceProperties_Statics::NewProp_StartPoint_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** First point of measurement */" },
		{ "ModuleRelativePath", "Private/SampleTools/MeasureDistanceSampleTool.h" },
		{ "ToolTip", "First point of measurement" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMeasureDistanceProperties_Statics::NewProp_StartPoint = { "StartPoint", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeasureDistanceProperties, StartPoint), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UMeasureDistanceProperties_Statics::NewProp_StartPoint_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeasureDistanceProperties_Statics::NewProp_StartPoint_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeasureDistanceProperties_Statics::NewProp_EndPoint_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Second point of measurement */" },
		{ "ModuleRelativePath", "Private/SampleTools/MeasureDistanceSampleTool.h" },
		{ "ToolTip", "Second point of measurement" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMeasureDistanceProperties_Statics::NewProp_EndPoint = { "EndPoint", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeasureDistanceProperties, EndPoint), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UMeasureDistanceProperties_Statics::NewProp_EndPoint_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeasureDistanceProperties_Statics::NewProp_EndPoint_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeasureDistanceProperties_Statics::NewProp_Distance_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Current distance measurement */" },
		{ "ModuleRelativePath", "Private/SampleTools/MeasureDistanceSampleTool.h" },
		{ "ToolTip", "Current distance measurement" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UMeasureDistanceProperties_Statics::NewProp_Distance = { "Distance", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeasureDistanceProperties, Distance), METADATA_PARAMS(Z_Construct_UClass_UMeasureDistanceProperties_Statics::NewProp_Distance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeasureDistanceProperties_Statics::NewProp_Distance_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeasureDistanceProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeasureDistanceProperties_Statics::NewProp_StartPoint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeasureDistanceProperties_Statics::NewProp_EndPoint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeasureDistanceProperties_Statics::NewProp_Distance,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeasureDistanceProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeasureDistanceProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeasureDistanceProperties_Statics::ClassParams = {
		&UMeasureDistanceProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeasureDistanceProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeasureDistanceProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeasureDistanceProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeasureDistanceProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeasureDistanceProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeasureDistanceProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeasureDistanceProperties, 1186708079);
	template<> SAMPLETOOLSEDITORMODE_API UClass* StaticClass<UMeasureDistanceProperties>()
	{
		return UMeasureDistanceProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeasureDistanceProperties(Z_Construct_UClass_UMeasureDistanceProperties, &UMeasureDistanceProperties::StaticClass, TEXT("/Script/SampleToolsEditorMode"), TEXT("UMeasureDistanceProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeasureDistanceProperties);
	void UMeasureDistanceSampleTool::StaticRegisterNativesUMeasureDistanceSampleTool()
	{
	}
	UClass* Z_Construct_UClass_UMeasureDistanceSampleTool_NoRegister()
	{
		return UMeasureDistanceSampleTool::StaticClass();
	}
	struct Z_Construct_UClass_UMeasureDistanceSampleTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Properties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Properties;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeasureDistanceSampleTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveTool,
		(UObject* (*)())Z_Construct_UPackage__Script_SampleToolsEditorMode,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeasureDistanceSampleTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * UMeasureDistanceSampleTool is an example Tool that allows the user to measure the \n * distance between two points. The first point is set by click-dragging the mouse, and\n * the second point is set by shift-click-dragging the mouse.\n */" },
		{ "IncludePath", "SampleTools/MeasureDistanceSampleTool.h" },
		{ "ModuleRelativePath", "Private/SampleTools/MeasureDistanceSampleTool.h" },
		{ "ToolTip", "UMeasureDistanceSampleTool is an example Tool that allows the user to measure the\ndistance between two points. The first point is set by click-dragging the mouse, and\nthe second point is set by shift-click-dragging the mouse." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeasureDistanceSampleTool_Statics::NewProp_Properties_MetaData[] = {
		{ "Comment", "/** Properties of the tool are stored here */" },
		{ "ModuleRelativePath", "Private/SampleTools/MeasureDistanceSampleTool.h" },
		{ "ToolTip", "Properties of the tool are stored here" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeasureDistanceSampleTool_Statics::NewProp_Properties = { "Properties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeasureDistanceSampleTool, Properties), Z_Construct_UClass_UMeasureDistanceProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeasureDistanceSampleTool_Statics::NewProp_Properties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeasureDistanceSampleTool_Statics::NewProp_Properties_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeasureDistanceSampleTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeasureDistanceSampleTool_Statics::NewProp_Properties,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeasureDistanceSampleTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeasureDistanceSampleTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeasureDistanceSampleTool_Statics::ClassParams = {
		&UMeasureDistanceSampleTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeasureDistanceSampleTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeasureDistanceSampleTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeasureDistanceSampleTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeasureDistanceSampleTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeasureDistanceSampleTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeasureDistanceSampleTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeasureDistanceSampleTool, 45171072);
	template<> SAMPLETOOLSEDITORMODE_API UClass* StaticClass<UMeasureDistanceSampleTool>()
	{
		return UMeasureDistanceSampleTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeasureDistanceSampleTool(Z_Construct_UClass_UMeasureDistanceSampleTool, &UMeasureDistanceSampleTool::StaticClass, TEXT("/Script/SampleToolsEditorMode"), TEXT("UMeasureDistanceSampleTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeasureDistanceSampleTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
