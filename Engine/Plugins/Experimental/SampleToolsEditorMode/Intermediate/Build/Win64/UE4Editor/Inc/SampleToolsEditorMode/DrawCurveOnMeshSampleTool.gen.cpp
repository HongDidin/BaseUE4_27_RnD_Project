// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Source/Private/SampleTools/DrawCurveOnMeshSampleTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDrawCurveOnMeshSampleTool() {}
// Cross Module References
	SAMPLETOOLSEDITORMODE_API UClass* Z_Construct_UClass_UDrawCurveOnMeshSampleToolBuilder_NoRegister();
	SAMPLETOOLSEDITORMODE_API UClass* Z_Construct_UClass_UDrawCurveOnMeshSampleToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UMeshSurfacePointToolBuilder();
	UPackage* Z_Construct_UPackage__Script_SampleToolsEditorMode();
	SAMPLETOOLSEDITORMODE_API UClass* Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_NoRegister();
	SAMPLETOOLSEDITORMODE_API UClass* Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
	SAMPLETOOLSEDITORMODE_API UClass* Z_Construct_UClass_UDrawCurveOnMeshSampleTool_NoRegister();
	SAMPLETOOLSEDITORMODE_API UClass* Z_Construct_UClass_UDrawCurveOnMeshSampleTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UMeshSurfacePointTool();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
// End Cross Module References
	void UDrawCurveOnMeshSampleToolBuilder::StaticRegisterNativesUDrawCurveOnMeshSampleToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UDrawCurveOnMeshSampleToolBuilder_NoRegister()
	{
		return UDrawCurveOnMeshSampleToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UDrawCurveOnMeshSampleToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDrawCurveOnMeshSampleToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshSurfacePointToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_SampleToolsEditorMode,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawCurveOnMeshSampleToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * UMeshSurfacePointToolBuilder override for UDrawCurveOnMeshSampleTool\n */" },
		{ "IncludePath", "SampleTools/DrawCurveOnMeshSampleTool.h" },
		{ "ModuleRelativePath", "Private/SampleTools/DrawCurveOnMeshSampleTool.h" },
		{ "ToolTip", "UMeshSurfacePointToolBuilder override for UDrawCurveOnMeshSampleTool" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDrawCurveOnMeshSampleToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDrawCurveOnMeshSampleToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDrawCurveOnMeshSampleToolBuilder_Statics::ClassParams = {
		&UDrawCurveOnMeshSampleToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UDrawCurveOnMeshSampleToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawCurveOnMeshSampleToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDrawCurveOnMeshSampleToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDrawCurveOnMeshSampleToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDrawCurveOnMeshSampleToolBuilder, 3636373858);
	template<> SAMPLETOOLSEDITORMODE_API UClass* StaticClass<UDrawCurveOnMeshSampleToolBuilder>()
	{
		return UDrawCurveOnMeshSampleToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDrawCurveOnMeshSampleToolBuilder(Z_Construct_UClass_UDrawCurveOnMeshSampleToolBuilder, &UDrawCurveOnMeshSampleToolBuilder::StaticClass, TEXT("/Script/SampleToolsEditorMode"), TEXT("UDrawCurveOnMeshSampleToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDrawCurveOnMeshSampleToolBuilder);
	void UDrawCurveOnMeshSampleToolProperties::StaticRegisterNativesUDrawCurveOnMeshSampleToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_NoRegister()
	{
		return UDrawCurveOnMeshSampleToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Color_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Color;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Thickness_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Thickness;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MinSpacing_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MinSpacing;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NormalOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_NormalOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DepthBias_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DepthBias;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bScreenSpace_MetaData[];
#endif
		static void NewProp_bScreenSpace_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bScreenSpace;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_SampleToolsEditorMode,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Settings UObject for UDrawCurveOnMeshSampleTool. This UClass inherits from UInteractiveToolPropertySet,\n * which provides an OnModified delegate that the Tool will listen to for changes in property values.\n */" },
		{ "IncludePath", "SampleTools/DrawCurveOnMeshSampleTool.h" },
		{ "ModuleRelativePath", "Private/SampleTools/DrawCurveOnMeshSampleTool.h" },
		{ "ToolTip", "Settings UObject for UDrawCurveOnMeshSampleTool. This UClass inherits from UInteractiveToolPropertySet,\nwhich provides an OnModified delegate that the Tool will listen to for changes in property values." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::NewProp_Color_MetaData[] = {
		{ "Category", "Options" },
		{ "ModuleRelativePath", "Private/SampleTools/DrawCurveOnMeshSampleTool.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::NewProp_Color = { "Color", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawCurveOnMeshSampleToolProperties, Color), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::NewProp_Color_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::NewProp_Color_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::NewProp_Thickness_MetaData[] = {
		{ "Category", "Options" },
		{ "ClampMax", "1000.0" },
		{ "ClampMin", "0.01" },
		{ "DisplayName", "Thickness" },
		{ "ModuleRelativePath", "Private/SampleTools/DrawCurveOnMeshSampleTool.h" },
		{ "UIMax", "10.0" },
		{ "UIMin", "0.25" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::NewProp_Thickness = { "Thickness", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawCurveOnMeshSampleToolProperties, Thickness), METADATA_PARAMS(Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::NewProp_Thickness_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::NewProp_Thickness_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::NewProp_MinSpacing_MetaData[] = {
		{ "Category", "Options" },
		{ "DisplayName", "Min Spacing" },
		{ "ModuleRelativePath", "Private/SampleTools/DrawCurveOnMeshSampleTool.h" },
		{ "UIMax", "10.0" },
		{ "UIMin", "0.01" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::NewProp_MinSpacing = { "MinSpacing", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawCurveOnMeshSampleToolProperties, MinSpacing), METADATA_PARAMS(Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::NewProp_MinSpacing_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::NewProp_MinSpacing_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::NewProp_NormalOffset_MetaData[] = {
		{ "Category", "Options" },
		{ "ClampMax", "1000.0" },
		{ "ClampMin", "-1000.0" },
		{ "DisplayName", "Offset" },
		{ "ModuleRelativePath", "Private/SampleTools/DrawCurveOnMeshSampleTool.h" },
		{ "UIMax", "10.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::NewProp_NormalOffset = { "NormalOffset", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawCurveOnMeshSampleToolProperties, NormalOffset), METADATA_PARAMS(Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::NewProp_NormalOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::NewProp_NormalOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::NewProp_DepthBias_MetaData[] = {
		{ "Category", "Options" },
		{ "DisplayName", "Depth Bias" },
		{ "ModuleRelativePath", "Private/SampleTools/DrawCurveOnMeshSampleTool.h" },
		{ "UIMax", "10.0" },
		{ "UIMin", "-10.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::NewProp_DepthBias = { "DepthBias", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawCurveOnMeshSampleToolProperties, DepthBias), METADATA_PARAMS(Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::NewProp_DepthBias_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::NewProp_DepthBias_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::NewProp_bScreenSpace_MetaData[] = {
		{ "Category", "Options" },
		{ "ModuleRelativePath", "Private/SampleTools/DrawCurveOnMeshSampleTool.h" },
	};
#endif
	void Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::NewProp_bScreenSpace_SetBit(void* Obj)
	{
		((UDrawCurveOnMeshSampleToolProperties*)Obj)->bScreenSpace = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::NewProp_bScreenSpace = { "bScreenSpace", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDrawCurveOnMeshSampleToolProperties), &Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::NewProp_bScreenSpace_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::NewProp_bScreenSpace_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::NewProp_bScreenSpace_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::NewProp_Color,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::NewProp_Thickness,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::NewProp_MinSpacing,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::NewProp_NormalOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::NewProp_DepthBias,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::NewProp_bScreenSpace,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDrawCurveOnMeshSampleToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::ClassParams = {
		&UDrawCurveOnMeshSampleToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDrawCurveOnMeshSampleToolProperties, 3393904891);
	template<> SAMPLETOOLSEDITORMODE_API UClass* StaticClass<UDrawCurveOnMeshSampleToolProperties>()
	{
		return UDrawCurveOnMeshSampleToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDrawCurveOnMeshSampleToolProperties(Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties, &UDrawCurveOnMeshSampleToolProperties::StaticClass, TEXT("/Script/SampleToolsEditorMode"), TEXT("UDrawCurveOnMeshSampleToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDrawCurveOnMeshSampleToolProperties);
	void UDrawCurveOnMeshSampleTool::StaticRegisterNativesUDrawCurveOnMeshSampleTool()
	{
	}
	UClass* Z_Construct_UClass_UDrawCurveOnMeshSampleTool_NoRegister()
	{
		return UDrawCurveOnMeshSampleTool::StaticClass();
	}
	struct Z_Construct_UClass_UDrawCurveOnMeshSampleTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Settings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Settings;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Positions_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Positions_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Positions;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Normals_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Normals_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Normals;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDrawCurveOnMeshSampleTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshSurfacePointTool,
		(UObject* (*)())Z_Construct_UPackage__Script_SampleToolsEditorMode,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawCurveOnMeshSampleTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * UDrawCurveOnMeshSampleTool is a sample Tool that allows the user to draw curves on the surface of\n * a selected Mesh Component. The various rendering properties of the polycurve are exposed and can be tweaked.\n * Nothing is done with the curve, it is just drawn by ::Render() and discarded when the Tool exits.\n */" },
		{ "IncludePath", "SampleTools/DrawCurveOnMeshSampleTool.h" },
		{ "ModuleRelativePath", "Private/SampleTools/DrawCurveOnMeshSampleTool.h" },
		{ "ToolTip", "UDrawCurveOnMeshSampleTool is a sample Tool that allows the user to draw curves on the surface of\na selected Mesh Component. The various rendering properties of the polycurve are exposed and can be tweaked.\nNothing is done with the curve, it is just drawn by ::Render() and discarded when the Tool exits." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawCurveOnMeshSampleTool_Statics::NewProp_Settings_MetaData[] = {
		{ "ModuleRelativePath", "Private/SampleTools/DrawCurveOnMeshSampleTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDrawCurveOnMeshSampleTool_Statics::NewProp_Settings = { "Settings", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawCurveOnMeshSampleTool, Settings), Z_Construct_UClass_UDrawCurveOnMeshSampleToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDrawCurveOnMeshSampleTool_Statics::NewProp_Settings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawCurveOnMeshSampleTool_Statics::NewProp_Settings_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDrawCurveOnMeshSampleTool_Statics::NewProp_Positions_Inner = { "Positions", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawCurveOnMeshSampleTool_Statics::NewProp_Positions_MetaData[] = {
		{ "ModuleRelativePath", "Private/SampleTools/DrawCurveOnMeshSampleTool.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDrawCurveOnMeshSampleTool_Statics::NewProp_Positions = { "Positions", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawCurveOnMeshSampleTool, Positions), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDrawCurveOnMeshSampleTool_Statics::NewProp_Positions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawCurveOnMeshSampleTool_Statics::NewProp_Positions_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDrawCurveOnMeshSampleTool_Statics::NewProp_Normals_Inner = { "Normals", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDrawCurveOnMeshSampleTool_Statics::NewProp_Normals_MetaData[] = {
		{ "ModuleRelativePath", "Private/SampleTools/DrawCurveOnMeshSampleTool.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDrawCurveOnMeshSampleTool_Statics::NewProp_Normals = { "Normals", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDrawCurveOnMeshSampleTool, Normals), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDrawCurveOnMeshSampleTool_Statics::NewProp_Normals_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawCurveOnMeshSampleTool_Statics::NewProp_Normals_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDrawCurveOnMeshSampleTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawCurveOnMeshSampleTool_Statics::NewProp_Settings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawCurveOnMeshSampleTool_Statics::NewProp_Positions_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawCurveOnMeshSampleTool_Statics::NewProp_Positions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawCurveOnMeshSampleTool_Statics::NewProp_Normals_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDrawCurveOnMeshSampleTool_Statics::NewProp_Normals,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDrawCurveOnMeshSampleTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDrawCurveOnMeshSampleTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDrawCurveOnMeshSampleTool_Statics::ClassParams = {
		&UDrawCurveOnMeshSampleTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDrawCurveOnMeshSampleTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDrawCurveOnMeshSampleTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UDrawCurveOnMeshSampleTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDrawCurveOnMeshSampleTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDrawCurveOnMeshSampleTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDrawCurveOnMeshSampleTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDrawCurveOnMeshSampleTool, 1172869376);
	template<> SAMPLETOOLSEDITORMODE_API UClass* StaticClass<UDrawCurveOnMeshSampleTool>()
	{
		return UDrawCurveOnMeshSampleTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDrawCurveOnMeshSampleTool(Z_Construct_UClass_UDrawCurveOnMeshSampleTool, &UDrawCurveOnMeshSampleTool::StaticClass, TEXT("/Script/SampleToolsEditorMode"), TEXT("UDrawCurveOnMeshSampleTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDrawCurveOnMeshSampleTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
