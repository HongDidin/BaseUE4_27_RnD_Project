// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Source/Private/SampleTools/CreateActorSampleTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCreateActorSampleTool() {}
// Cross Module References
	SAMPLETOOLSEDITORMODE_API UClass* Z_Construct_UClass_UCreateActorSampleToolBuilder_NoRegister();
	SAMPLETOOLSEDITORMODE_API UClass* Z_Construct_UClass_UCreateActorSampleToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	UPackage* Z_Construct_UPackage__Script_SampleToolsEditorMode();
	SAMPLETOOLSEDITORMODE_API UClass* Z_Construct_UClass_UCreateActorSampleToolProperties_NoRegister();
	SAMPLETOOLSEDITORMODE_API UClass* Z_Construct_UClass_UCreateActorSampleToolProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	SAMPLETOOLSEDITORMODE_API UClass* Z_Construct_UClass_UCreateActorSampleTool_NoRegister();
	SAMPLETOOLSEDITORMODE_API UClass* Z_Construct_UClass_UCreateActorSampleTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_USingleClickTool();
// End Cross Module References
	void UCreateActorSampleToolBuilder::StaticRegisterNativesUCreateActorSampleToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UCreateActorSampleToolBuilder_NoRegister()
	{
		return UCreateActorSampleToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UCreateActorSampleToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCreateActorSampleToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_SampleToolsEditorMode,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCreateActorSampleToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Builder for UCreateActorSampleTool\n */" },
		{ "IncludePath", "SampleTools/CreateActorSampleTool.h" },
		{ "ModuleRelativePath", "Private/SampleTools/CreateActorSampleTool.h" },
		{ "ToolTip", "Builder for UCreateActorSampleTool" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCreateActorSampleToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCreateActorSampleToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCreateActorSampleToolBuilder_Statics::ClassParams = {
		&UCreateActorSampleToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UCreateActorSampleToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCreateActorSampleToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCreateActorSampleToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCreateActorSampleToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCreateActorSampleToolBuilder, 2040149021);
	template<> SAMPLETOOLSEDITORMODE_API UClass* StaticClass<UCreateActorSampleToolBuilder>()
	{
		return UCreateActorSampleToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCreateActorSampleToolBuilder(Z_Construct_UClass_UCreateActorSampleToolBuilder, &UCreateActorSampleToolBuilder::StaticClass, TEXT("/Script/SampleToolsEditorMode"), TEXT("UCreateActorSampleToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCreateActorSampleToolBuilder);
	void UCreateActorSampleToolProperties::StaticRegisterNativesUCreateActorSampleToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UCreateActorSampleToolProperties_NoRegister()
	{
		return UCreateActorSampleToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UCreateActorSampleToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlaceOnObjects_MetaData[];
#endif
		static void NewProp_PlaceOnObjects_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_PlaceOnObjects;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GroundHeight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_GroundHeight;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCreateActorSampleToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_SampleToolsEditorMode,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCreateActorSampleToolProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Settings UObject for UCreateActorSampleTool. This UClass inherits from UInteractiveToolPropertySet,\n * which provides an OnModified delegate that the Tool will listen to for changes in property values.\n */" },
		{ "IncludePath", "SampleTools/CreateActorSampleTool.h" },
		{ "ModuleRelativePath", "Private/SampleTools/CreateActorSampleTool.h" },
		{ "ToolTip", "Settings UObject for UCreateActorSampleTool. This UClass inherits from UInteractiveToolPropertySet,\nwhich provides an OnModified delegate that the Tool will listen to for changes in property values." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCreateActorSampleToolProperties_Statics::NewProp_PlaceOnObjects_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Place actors on existing objects */" },
		{ "DisplayName", "Place On Objects" },
		{ "ModuleRelativePath", "Private/SampleTools/CreateActorSampleTool.h" },
		{ "ToolTip", "Place actors on existing objects" },
	};
#endif
	void Z_Construct_UClass_UCreateActorSampleToolProperties_Statics::NewProp_PlaceOnObjects_SetBit(void* Obj)
	{
		((UCreateActorSampleToolProperties*)Obj)->PlaceOnObjects = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCreateActorSampleToolProperties_Statics::NewProp_PlaceOnObjects = { "PlaceOnObjects", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCreateActorSampleToolProperties), &Z_Construct_UClass_UCreateActorSampleToolProperties_Statics::NewProp_PlaceOnObjects_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCreateActorSampleToolProperties_Statics::NewProp_PlaceOnObjects_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCreateActorSampleToolProperties_Statics::NewProp_PlaceOnObjects_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCreateActorSampleToolProperties_Statics::NewProp_GroundHeight_MetaData[] = {
		{ "Category", "Options" },
		{ "ClampMax", "1000000.0" },
		{ "ClampMin", "-1000000" },
		{ "Comment", "/** Height of ground plane */" },
		{ "DisplayName", "Ground Height" },
		{ "ModuleRelativePath", "Private/SampleTools/CreateActorSampleTool.h" },
		{ "ToolTip", "Height of ground plane" },
		{ "UIMax", "1000.0" },
		{ "UIMin", "-1000.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UCreateActorSampleToolProperties_Statics::NewProp_GroundHeight = { "GroundHeight", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCreateActorSampleToolProperties, GroundHeight), METADATA_PARAMS(Z_Construct_UClass_UCreateActorSampleToolProperties_Statics::NewProp_GroundHeight_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCreateActorSampleToolProperties_Statics::NewProp_GroundHeight_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCreateActorSampleToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCreateActorSampleToolProperties_Statics::NewProp_PlaceOnObjects,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCreateActorSampleToolProperties_Statics::NewProp_GroundHeight,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCreateActorSampleToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCreateActorSampleToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCreateActorSampleToolProperties_Statics::ClassParams = {
		&UCreateActorSampleToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UCreateActorSampleToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UCreateActorSampleToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UCreateActorSampleToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCreateActorSampleToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCreateActorSampleToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCreateActorSampleToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCreateActorSampleToolProperties, 4232422509);
	template<> SAMPLETOOLSEDITORMODE_API UClass* StaticClass<UCreateActorSampleToolProperties>()
	{
		return UCreateActorSampleToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCreateActorSampleToolProperties(Z_Construct_UClass_UCreateActorSampleToolProperties, &UCreateActorSampleToolProperties::StaticClass, TEXT("/Script/SampleToolsEditorMode"), TEXT("UCreateActorSampleToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCreateActorSampleToolProperties);
	void UCreateActorSampleTool::StaticRegisterNativesUCreateActorSampleTool()
	{
	}
	UClass* Z_Construct_UClass_UCreateActorSampleTool_NoRegister()
	{
		return UCreateActorSampleTool::StaticClass();
	}
	struct Z_Construct_UClass_UCreateActorSampleTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Properties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Properties;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCreateActorSampleTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USingleClickTool,
		(UObject* (*)())Z_Construct_UPackage__Script_SampleToolsEditorMode,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCreateActorSampleTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * UCreateActorSampleTool is an example Tool that drops an empty Actor at each position the user \n * clicks left mouse button. The Actors are placed at the first ray intersection in the scene,\n * or on a ground plane if no scene objects are hit. All the action is in the ::OnClicked handler.\n */" },
		{ "IncludePath", "SampleTools/CreateActorSampleTool.h" },
		{ "ModuleRelativePath", "Private/SampleTools/CreateActorSampleTool.h" },
		{ "ToolTip", "UCreateActorSampleTool is an example Tool that drops an empty Actor at each position the user\nclicks left mouse button. The Actors are placed at the first ray intersection in the scene,\nor on a ground plane if no scene objects are hit. All the action is in the ::OnClicked handler." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCreateActorSampleTool_Statics::NewProp_Properties_MetaData[] = {
		{ "ModuleRelativePath", "Private/SampleTools/CreateActorSampleTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UCreateActorSampleTool_Statics::NewProp_Properties = { "Properties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCreateActorSampleTool, Properties), Z_Construct_UClass_UCreateActorSampleToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCreateActorSampleTool_Statics::NewProp_Properties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCreateActorSampleTool_Statics::NewProp_Properties_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCreateActorSampleTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCreateActorSampleTool_Statics::NewProp_Properties,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCreateActorSampleTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCreateActorSampleTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCreateActorSampleTool_Statics::ClassParams = {
		&UCreateActorSampleTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UCreateActorSampleTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UCreateActorSampleTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UCreateActorSampleTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCreateActorSampleTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCreateActorSampleTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCreateActorSampleTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCreateActorSampleTool, 2633511110);
	template<> SAMPLETOOLSEDITORMODE_API UClass* StaticClass<UCreateActorSampleTool>()
	{
		return UCreateActorSampleTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCreateActorSampleTool(Z_Construct_UClass_UCreateActorSampleTool, &UCreateActorSampleTool::StaticClass, TEXT("/Script/SampleToolsEditorMode"), TEXT("UCreateActorSampleTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCreateActorSampleTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
