// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UCommonButtonBase;
class UCommonWidgetCarousel;
#ifdef COMMONUI_CommonWidgetCarouselNavBar_generated_h
#error "CommonWidgetCarouselNavBar.generated.h already included, missing '#pragma once' in CommonWidgetCarouselNavBar.h"
#endif
#define COMMONUI_CommonWidgetCarouselNavBar_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarouselNavBar_h_20_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarouselNavBar_h_20_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execHandleButtonClicked); \
	DECLARE_FUNCTION(execHandlePageChanged); \
	DECLARE_FUNCTION(execSetLinkedCarousel);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarouselNavBar_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execHandleButtonClicked); \
	DECLARE_FUNCTION(execHandlePageChanged); \
	DECLARE_FUNCTION(execSetLinkedCarousel);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarouselNavBar_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonWidgetCarouselNavBar(); \
	friend struct Z_Construct_UClass_UCommonWidgetCarouselNavBar_Statics; \
public: \
	DECLARE_CLASS(UCommonWidgetCarouselNavBar, UWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonWidgetCarouselNavBar)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarouselNavBar_h_20_INCLASS \
private: \
	static void StaticRegisterNativesUCommonWidgetCarouselNavBar(); \
	friend struct Z_Construct_UClass_UCommonWidgetCarouselNavBar_Statics; \
public: \
	DECLARE_CLASS(UCommonWidgetCarouselNavBar, UWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonWidgetCarouselNavBar)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarouselNavBar_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonWidgetCarouselNavBar(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonWidgetCarouselNavBar) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonWidgetCarouselNavBar); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonWidgetCarouselNavBar); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonWidgetCarouselNavBar(UCommonWidgetCarouselNavBar&&); \
	NO_API UCommonWidgetCarouselNavBar(const UCommonWidgetCarouselNavBar&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarouselNavBar_h_20_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonWidgetCarouselNavBar(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonWidgetCarouselNavBar(UCommonWidgetCarouselNavBar&&); \
	NO_API UCommonWidgetCarouselNavBar(const UCommonWidgetCarouselNavBar&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonWidgetCarouselNavBar); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonWidgetCarouselNavBar); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonWidgetCarouselNavBar)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarouselNavBar_h_20_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__LinkedCarousel() { return STRUCT_OFFSET(UCommonWidgetCarouselNavBar, LinkedCarousel); } \
	FORCEINLINE static uint32 __PPO__ButtonGroup() { return STRUCT_OFFSET(UCommonWidgetCarouselNavBar, ButtonGroup); } \
	FORCEINLINE static uint32 __PPO__Buttons() { return STRUCT_OFFSET(UCommonWidgetCarouselNavBar, Buttons); }


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarouselNavBar_h_17_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarouselNavBar_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarouselNavBar_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarouselNavBar_h_20_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarouselNavBar_h_20_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarouselNavBar_h_20_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarouselNavBar_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarouselNavBar_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarouselNavBar_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarouselNavBar_h_20_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarouselNavBar_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarouselNavBar_h_20_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarouselNavBar_h_20_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class CommonWidgetCarouselNavBar."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonWidgetCarouselNavBar>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarouselNavBar_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
