// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonUI/Public/CommonActivatableWidgetSwitcher.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCommonActivatableWidgetSwitcher() {}
// Cross Module References
	COMMONUI_API UClass* Z_Construct_UClass_UCommonActivatableWidgetSwitcher_NoRegister();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonActivatableWidgetSwitcher();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonAnimatedSwitcher();
	UPackage* Z_Construct_UPackage__Script_CommonUI();
// End Cross Module References
	void UCommonActivatableWidgetSwitcher::StaticRegisterNativesUCommonActivatableWidgetSwitcher()
	{
	}
	UClass* Z_Construct_UClass_UCommonActivatableWidgetSwitcher_NoRegister()
	{
		return UCommonActivatableWidgetSwitcher::StaticClass();
	}
	struct Z_Construct_UClass_UCommonActivatableWidgetSwitcher_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonActivatableWidgetSwitcher_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCommonAnimatedSwitcher,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonUI,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonActivatableWidgetSwitcher_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * An animated switcher that knows about CommonActivatableWidgets. It can also hold other Widgets.\n */" },
		{ "IncludePath", "CommonActivatableWidgetSwitcher.h" },
		{ "ModuleRelativePath", "Public/CommonActivatableWidgetSwitcher.h" },
		{ "ToolTip", "An animated switcher that knows about CommonActivatableWidgets. It can also hold other Widgets." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonActivatableWidgetSwitcher_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonActivatableWidgetSwitcher>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCommonActivatableWidgetSwitcher_Statics::ClassParams = {
		&UCommonActivatableWidgetSwitcher::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x00B000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonActivatableWidgetSwitcher_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonActivatableWidgetSwitcher_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonActivatableWidgetSwitcher()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCommonActivatableWidgetSwitcher_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCommonActivatableWidgetSwitcher, 2012709609);
	template<> COMMONUI_API UClass* StaticClass<UCommonActivatableWidgetSwitcher>()
	{
		return UCommonActivatableWidgetSwitcher::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCommonActivatableWidgetSwitcher(Z_Construct_UClass_UCommonActivatableWidgetSwitcher, &UCommonActivatableWidgetSwitcher::StaticClass, TEXT("/Script/CommonUI"), TEXT("UCommonActivatableWidgetSwitcher"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonActivatableWidgetSwitcher);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
