// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonUI/Public/Input/CommonInputMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCommonInputMode() {}
// Cross Module References
	COMMONUI_API UEnum* Z_Construct_UEnum_CommonUI_ECommonInputMode();
	UPackage* Z_Construct_UPackage__Script_CommonUI();
// End Cross Module References
	static UEnum* ECommonInputMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_CommonUI_ECommonInputMode, Z_Construct_UPackage__Script_CommonUI(), TEXT("ECommonInputMode"));
		}
		return Singleton;
	}
	template<> COMMONUI_API UEnum* StaticEnum<ECommonInputMode>()
	{
		return ECommonInputMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ECommonInputMode(ECommonInputMode_StaticEnum, TEXT("/Script/CommonUI"), TEXT("ECommonInputMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_CommonUI_ECommonInputMode_Hash() { return 3230080895U; }
	UEnum* Z_Construct_UEnum_CommonUI_ECommonInputMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_CommonUI();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ECommonInputMode"), 0, Get_Z_Construct_UEnum_CommonUI_ECommonInputMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ECommonInputMode::Menu", (int64)ECommonInputMode::Menu },
				{ "ECommonInputMode::Game", (int64)ECommonInputMode::Game },
				{ "ECommonInputMode::All", (int64)ECommonInputMode::All },
				{ "ECommonInputMode::MAX", (int64)ECommonInputMode::MAX },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "All.Name", "ECommonInputMode::All" },
				{ "BlueprintType", "true" },
				{ "Game.Name", "ECommonInputMode::Game" },
				{ "MAX.Name", "ECommonInputMode::MAX" },
				{ "Menu.Name", "ECommonInputMode::Menu" },
				{ "ModuleRelativePath", "Public/Input/CommonInputMode.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_CommonUI,
				nullptr,
				"ECommonInputMode",
				"ECommonInputMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
