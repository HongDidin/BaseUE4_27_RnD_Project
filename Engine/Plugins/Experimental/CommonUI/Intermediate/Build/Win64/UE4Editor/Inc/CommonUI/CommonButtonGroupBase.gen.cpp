// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonUI/Public/Groups/CommonButtonGroupBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCommonButtonGroupBase() {}
// Cross Module References
	COMMONUI_API UFunction* Z_Construct_UDelegateFunction_CommonUI_OnSelectionCleared__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_CommonUI();
	COMMONUI_API UFunction* Z_Construct_UDelegateFunction_CommonUI_SimpleButtonBaseGroupDelegate__DelegateSignature();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonButtonBase_NoRegister();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonButtonGroupBase_NoRegister();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonButtonGroupBase();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonWidgetGroupBase();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_CommonUI_OnSelectionCleared__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_CommonUI_OnSelectionCleared__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Groups/CommonButtonGroupBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_CommonUI_OnSelectionCleared__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_CommonUI, nullptr, "OnSelectionCleared__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_CommonUI_OnSelectionCleared__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_CommonUI_OnSelectionCleared__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_CommonUI_OnSelectionCleared__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_CommonUI_OnSelectionCleared__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_CommonUI_SimpleButtonBaseGroupDelegate__DelegateSignature_Statics
	{
		struct _Script_CommonUI_eventSimpleButtonBaseGroupDelegate_Parms
		{
			UCommonButtonBase* AssociatedButton;
			int32 ButtonIndex;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AssociatedButton_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AssociatedButton;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ButtonIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_CommonUI_SimpleButtonBaseGroupDelegate__DelegateSignature_Statics::NewProp_AssociatedButton_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_CommonUI_SimpleButtonBaseGroupDelegate__DelegateSignature_Statics::NewProp_AssociatedButton = { "AssociatedButton", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_CommonUI_eventSimpleButtonBaseGroupDelegate_Parms, AssociatedButton), Z_Construct_UClass_UCommonButtonBase_NoRegister, METADATA_PARAMS(Z_Construct_UDelegateFunction_CommonUI_SimpleButtonBaseGroupDelegate__DelegateSignature_Statics::NewProp_AssociatedButton_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_CommonUI_SimpleButtonBaseGroupDelegate__DelegateSignature_Statics::NewProp_AssociatedButton_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UDelegateFunction_CommonUI_SimpleButtonBaseGroupDelegate__DelegateSignature_Statics::NewProp_ButtonIndex = { "ButtonIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_CommonUI_eventSimpleButtonBaseGroupDelegate_Parms, ButtonIndex), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_CommonUI_SimpleButtonBaseGroupDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_CommonUI_SimpleButtonBaseGroupDelegate__DelegateSignature_Statics::NewProp_AssociatedButton,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_CommonUI_SimpleButtonBaseGroupDelegate__DelegateSignature_Statics::NewProp_ButtonIndex,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_CommonUI_SimpleButtonBaseGroupDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Groups/CommonButtonGroupBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_CommonUI_SimpleButtonBaseGroupDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_CommonUI, nullptr, "SimpleButtonBaseGroupDelegate__DelegateSignature", nullptr, nullptr, sizeof(_Script_CommonUI_eventSimpleButtonBaseGroupDelegate_Parms), Z_Construct_UDelegateFunction_CommonUI_SimpleButtonBaseGroupDelegate__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_CommonUI_SimpleButtonBaseGroupDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_CommonUI_SimpleButtonBaseGroupDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_CommonUI_SimpleButtonBaseGroupDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_CommonUI_SimpleButtonBaseGroupDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_CommonUI_SimpleButtonBaseGroupDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(UCommonButtonGroupBase::execOnButtonBaseUnhovered)
	{
		P_GET_OBJECT(UCommonButtonBase,Z_Param_BaseButton);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnButtonBaseUnhovered(Z_Param_BaseButton);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonButtonGroupBase::execOnButtonBaseHovered)
	{
		P_GET_OBJECT(UCommonButtonBase,Z_Param_BaseButton);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnButtonBaseHovered(Z_Param_BaseButton);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonButtonGroupBase::execOnHandleButtonBaseDoubleClicked)
	{
		P_GET_OBJECT(UCommonButtonBase,Z_Param_BaseButton);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnHandleButtonBaseDoubleClicked(Z_Param_BaseButton);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonButtonGroupBase::execOnHandleButtonBaseClicked)
	{
		P_GET_OBJECT(UCommonButtonBase,Z_Param_BaseButton);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnHandleButtonBaseClicked(Z_Param_BaseButton);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonButtonGroupBase::execOnSelectionStateChangedBase)
	{
		P_GET_OBJECT(UCommonButtonBase,Z_Param_BaseButton);
		P_GET_UBOOL(Z_Param_bIsSelected);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnSelectionStateChangedBase(Z_Param_BaseButton,Z_Param_bIsSelected);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonButtonGroupBase::execGetButtonCount)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetButtonCount();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonButtonGroupBase::execHasAnyButtons)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->HasAnyButtons();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonButtonGroupBase::execGetSelectedButtonBase)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UCommonButtonBase**)Z_Param__Result=P_THIS->GetSelectedButtonBase();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonButtonGroupBase::execGetButtonBaseAtIndex)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_Index);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UCommonButtonBase**)Z_Param__Result=P_THIS->GetButtonBaseAtIndex(Z_Param_Index);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonButtonGroupBase::execFindButtonIndex)
	{
		P_GET_OBJECT(UCommonButtonBase,Z_Param_ButtonToFind);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->FindButtonIndex(Z_Param_ButtonToFind);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonButtonGroupBase::execGetHoveredButtonIndex)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetHoveredButtonIndex();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonButtonGroupBase::execGetSelectedButtonIndex)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetSelectedButtonIndex();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonButtonGroupBase::execSelectButtonAtIndex)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_ButtonIndex);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SelectButtonAtIndex(Z_Param_ButtonIndex);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonButtonGroupBase::execSelectPreviousButton)
	{
		P_GET_UBOOL(Z_Param_bAllowWrap);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SelectPreviousButton(Z_Param_bAllowWrap);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonButtonGroupBase::execSelectNextButton)
	{
		P_GET_UBOOL(Z_Param_bAllowWrap);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SelectNextButton(Z_Param_bAllowWrap);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonButtonGroupBase::execDeselectAll)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->DeselectAll();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonButtonGroupBase::execSetSelectionRequired)
	{
		P_GET_UBOOL(Z_Param_bRequireSelection);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetSelectionRequired(Z_Param_bRequireSelection);
		P_NATIVE_END;
	}
	void UCommonButtonGroupBase::StaticRegisterNativesUCommonButtonGroupBase()
	{
		UClass* Class = UCommonButtonGroupBase::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "DeselectAll", &UCommonButtonGroupBase::execDeselectAll },
			{ "FindButtonIndex", &UCommonButtonGroupBase::execFindButtonIndex },
			{ "GetButtonBaseAtIndex", &UCommonButtonGroupBase::execGetButtonBaseAtIndex },
			{ "GetButtonCount", &UCommonButtonGroupBase::execGetButtonCount },
			{ "GetHoveredButtonIndex", &UCommonButtonGroupBase::execGetHoveredButtonIndex },
			{ "GetSelectedButtonBase", &UCommonButtonGroupBase::execGetSelectedButtonBase },
			{ "GetSelectedButtonIndex", &UCommonButtonGroupBase::execGetSelectedButtonIndex },
			{ "HasAnyButtons", &UCommonButtonGroupBase::execHasAnyButtons },
			{ "OnButtonBaseHovered", &UCommonButtonGroupBase::execOnButtonBaseHovered },
			{ "OnButtonBaseUnhovered", &UCommonButtonGroupBase::execOnButtonBaseUnhovered },
			{ "OnHandleButtonBaseClicked", &UCommonButtonGroupBase::execOnHandleButtonBaseClicked },
			{ "OnHandleButtonBaseDoubleClicked", &UCommonButtonGroupBase::execOnHandleButtonBaseDoubleClicked },
			{ "OnSelectionStateChangedBase", &UCommonButtonGroupBase::execOnSelectionStateChangedBase },
			{ "SelectButtonAtIndex", &UCommonButtonGroupBase::execSelectButtonAtIndex },
			{ "SelectNextButton", &UCommonButtonGroupBase::execSelectNextButton },
			{ "SelectPreviousButton", &UCommonButtonGroupBase::execSelectPreviousButton },
			{ "SetSelectionRequired", &UCommonButtonGroupBase::execSetSelectionRequired },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCommonButtonGroupBase_DeselectAll_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonButtonGroupBase_DeselectAll_Statics::Function_MetaDataParams[] = {
		{ "Category", "BaseButtonGroup" },
		{ "Comment", "/** Deselects all buttons in the group. */" },
		{ "ModuleRelativePath", "Public/Groups/CommonButtonGroupBase.h" },
		{ "ToolTip", "Deselects all buttons in the group." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonButtonGroupBase_DeselectAll_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonButtonGroupBase, nullptr, "DeselectAll", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonButtonGroupBase_DeselectAll_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonButtonGroupBase_DeselectAll_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonButtonGroupBase_DeselectAll()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonButtonGroupBase_DeselectAll_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonButtonGroupBase_FindButtonIndex_Statics
	{
		struct CommonButtonGroupBase_eventFindButtonIndex_Parms
		{
			const UCommonButtonBase* ButtonToFind;
			int32 ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ButtonToFind_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ButtonToFind;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonButtonGroupBase_FindButtonIndex_Statics::NewProp_ButtonToFind_MetaData[] = {
		{ "EditInline", "true" },
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCommonButtonGroupBase_FindButtonIndex_Statics::NewProp_ButtonToFind = { "ButtonToFind", nullptr, (EPropertyFlags)0x0010000000080082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonButtonGroupBase_eventFindButtonIndex_Parms, ButtonToFind), Z_Construct_UClass_UCommonButtonBase_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UCommonButtonGroupBase_FindButtonIndex_Statics::NewProp_ButtonToFind_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonButtonGroupBase_FindButtonIndex_Statics::NewProp_ButtonToFind_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UCommonButtonGroupBase_FindButtonIndex_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonButtonGroupBase_eventFindButtonIndex_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonButtonGroupBase_FindButtonIndex_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonButtonGroupBase_FindButtonIndex_Statics::NewProp_ButtonToFind,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonButtonGroupBase_FindButtonIndex_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonButtonGroupBase_FindButtonIndex_Statics::Function_MetaDataParams[] = {
		{ "Category", "BaseButtonGroup" },
		{ "Comment", "/**\n\x09 * Find the button index of the specified button, if possible\n\x09 * @param ButtonToFind\x09""Button to find the index of\n\x09 * @return Index of the button in the group. INDEX_NONE if not found\n\x09 */" },
		{ "ModuleRelativePath", "Public/Groups/CommonButtonGroupBase.h" },
		{ "ToolTip", "Find the button index of the specified button, if possible\n@param ButtonToFind  Button to find the index of\n@return Index of the button in the group. INDEX_NONE if not found" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonButtonGroupBase_FindButtonIndex_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonButtonGroupBase, nullptr, "FindButtonIndex", nullptr, nullptr, sizeof(CommonButtonGroupBase_eventFindButtonIndex_Parms), Z_Construct_UFunction_UCommonButtonGroupBase_FindButtonIndex_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonButtonGroupBase_FindButtonIndex_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonButtonGroupBase_FindButtonIndex_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonButtonGroupBase_FindButtonIndex_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonButtonGroupBase_FindButtonIndex()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonButtonGroupBase_FindButtonIndex_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonButtonGroupBase_GetButtonBaseAtIndex_Statics
	{
		struct CommonButtonGroupBase_eventGetButtonBaseAtIndex_Parms
		{
			int32 Index;
			UCommonButtonBase* ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UCommonButtonGroupBase_GetButtonBaseAtIndex_Statics::NewProp_Index = { "Index", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonButtonGroupBase_eventGetButtonBaseAtIndex_Parms, Index), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonButtonGroupBase_GetButtonBaseAtIndex_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCommonButtonGroupBase_GetButtonBaseAtIndex_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonButtonGroupBase_eventGetButtonBaseAtIndex_Parms, ReturnValue), Z_Construct_UClass_UCommonButtonBase_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UCommonButtonGroupBase_GetButtonBaseAtIndex_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonButtonGroupBase_GetButtonBaseAtIndex_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonButtonGroupBase_GetButtonBaseAtIndex_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonButtonGroupBase_GetButtonBaseAtIndex_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonButtonGroupBase_GetButtonBaseAtIndex_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonButtonGroupBase_GetButtonBaseAtIndex_Statics::Function_MetaDataParams[] = {
		{ "Category", "BaseButtonGroup" },
		{ "ModuleRelativePath", "Public/Groups/CommonButtonGroupBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonButtonGroupBase_GetButtonBaseAtIndex_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonButtonGroupBase, nullptr, "GetButtonBaseAtIndex", nullptr, nullptr, sizeof(CommonButtonGroupBase_eventGetButtonBaseAtIndex_Parms), Z_Construct_UFunction_UCommonButtonGroupBase_GetButtonBaseAtIndex_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonButtonGroupBase_GetButtonBaseAtIndex_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonButtonGroupBase_GetButtonBaseAtIndex_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonButtonGroupBase_GetButtonBaseAtIndex_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonButtonGroupBase_GetButtonBaseAtIndex()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonButtonGroupBase_GetButtonBaseAtIndex_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonButtonGroupBase_GetButtonCount_Statics
	{
		struct CommonButtonGroupBase_eventGetButtonCount_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UCommonButtonGroupBase_GetButtonCount_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonButtonGroupBase_eventGetButtonCount_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonButtonGroupBase_GetButtonCount_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonButtonGroupBase_GetButtonCount_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonButtonGroupBase_GetButtonCount_Statics::Function_MetaDataParams[] = {
		{ "Category", "BaseButtonGroup" },
		{ "ModuleRelativePath", "Public/Groups/CommonButtonGroupBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonButtonGroupBase_GetButtonCount_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonButtonGroupBase, nullptr, "GetButtonCount", nullptr, nullptr, sizeof(CommonButtonGroupBase_eventGetButtonCount_Parms), Z_Construct_UFunction_UCommonButtonGroupBase_GetButtonCount_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonButtonGroupBase_GetButtonCount_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonButtonGroupBase_GetButtonCount_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonButtonGroupBase_GetButtonCount_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonButtonGroupBase_GetButtonCount()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonButtonGroupBase_GetButtonCount_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonButtonGroupBase_GetHoveredButtonIndex_Statics
	{
		struct CommonButtonGroupBase_eventGetHoveredButtonIndex_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UCommonButtonGroupBase_GetHoveredButtonIndex_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonButtonGroupBase_eventGetHoveredButtonIndex_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonButtonGroupBase_GetHoveredButtonIndex_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonButtonGroupBase_GetHoveredButtonIndex_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonButtonGroupBase_GetHoveredButtonIndex_Statics::Function_MetaDataParams[] = {
		{ "Category", "BaseButtonGroup" },
		{ "Comment", "/**\n\x09 * Get the index of the currently hovered button, if any.\n\x09 * @param The index of the currently hovered button in the group, or -1 if there is no hovered button.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Groups/CommonButtonGroupBase.h" },
		{ "ToolTip", "Get the index of the currently hovered button, if any.\n@param The index of the currently hovered button in the group, or -1 if there is no hovered button." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonButtonGroupBase_GetHoveredButtonIndex_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonButtonGroupBase, nullptr, "GetHoveredButtonIndex", nullptr, nullptr, sizeof(CommonButtonGroupBase_eventGetHoveredButtonIndex_Parms), Z_Construct_UFunction_UCommonButtonGroupBase_GetHoveredButtonIndex_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonButtonGroupBase_GetHoveredButtonIndex_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonButtonGroupBase_GetHoveredButtonIndex_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonButtonGroupBase_GetHoveredButtonIndex_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonButtonGroupBase_GetHoveredButtonIndex()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonButtonGroupBase_GetHoveredButtonIndex_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonButtonGroupBase_GetSelectedButtonBase_Statics
	{
		struct CommonButtonGroupBase_eventGetSelectedButtonBase_Parms
		{
			UCommonButtonBase* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonButtonGroupBase_GetSelectedButtonBase_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCommonButtonGroupBase_GetSelectedButtonBase_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonButtonGroupBase_eventGetSelectedButtonBase_Parms, ReturnValue), Z_Construct_UClass_UCommonButtonBase_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UCommonButtonGroupBase_GetSelectedButtonBase_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonButtonGroupBase_GetSelectedButtonBase_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonButtonGroupBase_GetSelectedButtonBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonButtonGroupBase_GetSelectedButtonBase_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonButtonGroupBase_GetSelectedButtonBase_Statics::Function_MetaDataParams[] = {
		{ "Category", "BaseButtonGroup" },
		{ "ModuleRelativePath", "Public/Groups/CommonButtonGroupBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonButtonGroupBase_GetSelectedButtonBase_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonButtonGroupBase, nullptr, "GetSelectedButtonBase", nullptr, nullptr, sizeof(CommonButtonGroupBase_eventGetSelectedButtonBase_Parms), Z_Construct_UFunction_UCommonButtonGroupBase_GetSelectedButtonBase_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonButtonGroupBase_GetSelectedButtonBase_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonButtonGroupBase_GetSelectedButtonBase_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonButtonGroupBase_GetSelectedButtonBase_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonButtonGroupBase_GetSelectedButtonBase()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonButtonGroupBase_GetSelectedButtonBase_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonButtonGroupBase_GetSelectedButtonIndex_Statics
	{
		struct CommonButtonGroupBase_eventGetSelectedButtonIndex_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UCommonButtonGroupBase_GetSelectedButtonIndex_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonButtonGroupBase_eventGetSelectedButtonIndex_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonButtonGroupBase_GetSelectedButtonIndex_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonButtonGroupBase_GetSelectedButtonIndex_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonButtonGroupBase_GetSelectedButtonIndex_Statics::Function_MetaDataParams[] = {
		{ "Category", "BaseButtonGroup" },
		{ "Comment", "/**\n\x09 * Get the index of the currently selected button, if any.\n\x09 * @param The index of the currently selected button in the group, or -1 if there is no selected button.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Groups/CommonButtonGroupBase.h" },
		{ "ToolTip", "Get the index of the currently selected button, if any.\n@param The index of the currently selected button in the group, or -1 if there is no selected button." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonButtonGroupBase_GetSelectedButtonIndex_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonButtonGroupBase, nullptr, "GetSelectedButtonIndex", nullptr, nullptr, sizeof(CommonButtonGroupBase_eventGetSelectedButtonIndex_Parms), Z_Construct_UFunction_UCommonButtonGroupBase_GetSelectedButtonIndex_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonButtonGroupBase_GetSelectedButtonIndex_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonButtonGroupBase_GetSelectedButtonIndex_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonButtonGroupBase_GetSelectedButtonIndex_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonButtonGroupBase_GetSelectedButtonIndex()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonButtonGroupBase_GetSelectedButtonIndex_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonButtonGroupBase_HasAnyButtons_Statics
	{
		struct CommonButtonGroupBase_eventHasAnyButtons_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UCommonButtonGroupBase_HasAnyButtons_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((CommonButtonGroupBase_eventHasAnyButtons_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCommonButtonGroupBase_HasAnyButtons_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CommonButtonGroupBase_eventHasAnyButtons_Parms), &Z_Construct_UFunction_UCommonButtonGroupBase_HasAnyButtons_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonButtonGroupBase_HasAnyButtons_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonButtonGroupBase_HasAnyButtons_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonButtonGroupBase_HasAnyButtons_Statics::Function_MetaDataParams[] = {
		{ "Category", "BaseButtonGroup" },
		{ "ModuleRelativePath", "Public/Groups/CommonButtonGroupBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonButtonGroupBase_HasAnyButtons_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonButtonGroupBase, nullptr, "HasAnyButtons", nullptr, nullptr, sizeof(CommonButtonGroupBase_eventHasAnyButtons_Parms), Z_Construct_UFunction_UCommonButtonGroupBase_HasAnyButtons_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonButtonGroupBase_HasAnyButtons_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonButtonGroupBase_HasAnyButtons_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonButtonGroupBase_HasAnyButtons_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonButtonGroupBase_HasAnyButtons()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonButtonGroupBase_HasAnyButtons_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonButtonGroupBase_OnButtonBaseHovered_Statics
	{
		struct CommonButtonGroupBase_eventOnButtonBaseHovered_Parms
		{
			UCommonButtonBase* BaseButton;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseButton_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BaseButton;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonButtonGroupBase_OnButtonBaseHovered_Statics::NewProp_BaseButton_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCommonButtonGroupBase_OnButtonBaseHovered_Statics::NewProp_BaseButton = { "BaseButton", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonButtonGroupBase_eventOnButtonBaseHovered_Parms, BaseButton), Z_Construct_UClass_UCommonButtonBase_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UCommonButtonGroupBase_OnButtonBaseHovered_Statics::NewProp_BaseButton_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonButtonGroupBase_OnButtonBaseHovered_Statics::NewProp_BaseButton_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonButtonGroupBase_OnButtonBaseHovered_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonButtonGroupBase_OnButtonBaseHovered_Statics::NewProp_BaseButton,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonButtonGroupBase_OnButtonBaseHovered_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Groups/CommonButtonGroupBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonButtonGroupBase_OnButtonBaseHovered_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonButtonGroupBase, nullptr, "OnButtonBaseHovered", nullptr, nullptr, sizeof(CommonButtonGroupBase_eventOnButtonBaseHovered_Parms), Z_Construct_UFunction_UCommonButtonGroupBase_OnButtonBaseHovered_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonButtonGroupBase_OnButtonBaseHovered_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonButtonGroupBase_OnButtonBaseHovered_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonButtonGroupBase_OnButtonBaseHovered_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonButtonGroupBase_OnButtonBaseHovered()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonButtonGroupBase_OnButtonBaseHovered_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonButtonGroupBase_OnButtonBaseUnhovered_Statics
	{
		struct CommonButtonGroupBase_eventOnButtonBaseUnhovered_Parms
		{
			UCommonButtonBase* BaseButton;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseButton_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BaseButton;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonButtonGroupBase_OnButtonBaseUnhovered_Statics::NewProp_BaseButton_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCommonButtonGroupBase_OnButtonBaseUnhovered_Statics::NewProp_BaseButton = { "BaseButton", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonButtonGroupBase_eventOnButtonBaseUnhovered_Parms, BaseButton), Z_Construct_UClass_UCommonButtonBase_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UCommonButtonGroupBase_OnButtonBaseUnhovered_Statics::NewProp_BaseButton_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonButtonGroupBase_OnButtonBaseUnhovered_Statics::NewProp_BaseButton_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonButtonGroupBase_OnButtonBaseUnhovered_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonButtonGroupBase_OnButtonBaseUnhovered_Statics::NewProp_BaseButton,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonButtonGroupBase_OnButtonBaseUnhovered_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Groups/CommonButtonGroupBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonButtonGroupBase_OnButtonBaseUnhovered_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonButtonGroupBase, nullptr, "OnButtonBaseUnhovered", nullptr, nullptr, sizeof(CommonButtonGroupBase_eventOnButtonBaseUnhovered_Parms), Z_Construct_UFunction_UCommonButtonGroupBase_OnButtonBaseUnhovered_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonButtonGroupBase_OnButtonBaseUnhovered_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonButtonGroupBase_OnButtonBaseUnhovered_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonButtonGroupBase_OnButtonBaseUnhovered_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonButtonGroupBase_OnButtonBaseUnhovered()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonButtonGroupBase_OnButtonBaseUnhovered_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonButtonGroupBase_OnHandleButtonBaseClicked_Statics
	{
		struct CommonButtonGroupBase_eventOnHandleButtonBaseClicked_Parms
		{
			UCommonButtonBase* BaseButton;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseButton_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BaseButton;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonButtonGroupBase_OnHandleButtonBaseClicked_Statics::NewProp_BaseButton_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCommonButtonGroupBase_OnHandleButtonBaseClicked_Statics::NewProp_BaseButton = { "BaseButton", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonButtonGroupBase_eventOnHandleButtonBaseClicked_Parms, BaseButton), Z_Construct_UClass_UCommonButtonBase_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UCommonButtonGroupBase_OnHandleButtonBaseClicked_Statics::NewProp_BaseButton_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonButtonGroupBase_OnHandleButtonBaseClicked_Statics::NewProp_BaseButton_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonButtonGroupBase_OnHandleButtonBaseClicked_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonButtonGroupBase_OnHandleButtonBaseClicked_Statics::NewProp_BaseButton,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonButtonGroupBase_OnHandleButtonBaseClicked_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Groups/CommonButtonGroupBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonButtonGroupBase_OnHandleButtonBaseClicked_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonButtonGroupBase, nullptr, "OnHandleButtonBaseClicked", nullptr, nullptr, sizeof(CommonButtonGroupBase_eventOnHandleButtonBaseClicked_Parms), Z_Construct_UFunction_UCommonButtonGroupBase_OnHandleButtonBaseClicked_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonButtonGroupBase_OnHandleButtonBaseClicked_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonButtonGroupBase_OnHandleButtonBaseClicked_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonButtonGroupBase_OnHandleButtonBaseClicked_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonButtonGroupBase_OnHandleButtonBaseClicked()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonButtonGroupBase_OnHandleButtonBaseClicked_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonButtonGroupBase_OnHandleButtonBaseDoubleClicked_Statics
	{
		struct CommonButtonGroupBase_eventOnHandleButtonBaseDoubleClicked_Parms
		{
			UCommonButtonBase* BaseButton;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseButton_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BaseButton;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonButtonGroupBase_OnHandleButtonBaseDoubleClicked_Statics::NewProp_BaseButton_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCommonButtonGroupBase_OnHandleButtonBaseDoubleClicked_Statics::NewProp_BaseButton = { "BaseButton", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonButtonGroupBase_eventOnHandleButtonBaseDoubleClicked_Parms, BaseButton), Z_Construct_UClass_UCommonButtonBase_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UCommonButtonGroupBase_OnHandleButtonBaseDoubleClicked_Statics::NewProp_BaseButton_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonButtonGroupBase_OnHandleButtonBaseDoubleClicked_Statics::NewProp_BaseButton_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonButtonGroupBase_OnHandleButtonBaseDoubleClicked_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonButtonGroupBase_OnHandleButtonBaseDoubleClicked_Statics::NewProp_BaseButton,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonButtonGroupBase_OnHandleButtonBaseDoubleClicked_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Groups/CommonButtonGroupBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonButtonGroupBase_OnHandleButtonBaseDoubleClicked_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonButtonGroupBase, nullptr, "OnHandleButtonBaseDoubleClicked", nullptr, nullptr, sizeof(CommonButtonGroupBase_eventOnHandleButtonBaseDoubleClicked_Parms), Z_Construct_UFunction_UCommonButtonGroupBase_OnHandleButtonBaseDoubleClicked_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonButtonGroupBase_OnHandleButtonBaseDoubleClicked_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonButtonGroupBase_OnHandleButtonBaseDoubleClicked_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonButtonGroupBase_OnHandleButtonBaseDoubleClicked_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonButtonGroupBase_OnHandleButtonBaseDoubleClicked()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonButtonGroupBase_OnHandleButtonBaseDoubleClicked_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonButtonGroupBase_OnSelectionStateChangedBase_Statics
	{
		struct CommonButtonGroupBase_eventOnSelectionStateChangedBase_Parms
		{
			UCommonButtonBase* BaseButton;
			bool bIsSelected;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseButton_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BaseButton;
		static void NewProp_bIsSelected_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsSelected;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonButtonGroupBase_OnSelectionStateChangedBase_Statics::NewProp_BaseButton_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCommonButtonGroupBase_OnSelectionStateChangedBase_Statics::NewProp_BaseButton = { "BaseButton", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonButtonGroupBase_eventOnSelectionStateChangedBase_Parms, BaseButton), Z_Construct_UClass_UCommonButtonBase_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UCommonButtonGroupBase_OnSelectionStateChangedBase_Statics::NewProp_BaseButton_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonButtonGroupBase_OnSelectionStateChangedBase_Statics::NewProp_BaseButton_MetaData)) };
	void Z_Construct_UFunction_UCommonButtonGroupBase_OnSelectionStateChangedBase_Statics::NewProp_bIsSelected_SetBit(void* Obj)
	{
		((CommonButtonGroupBase_eventOnSelectionStateChangedBase_Parms*)Obj)->bIsSelected = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCommonButtonGroupBase_OnSelectionStateChangedBase_Statics::NewProp_bIsSelected = { "bIsSelected", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CommonButtonGroupBase_eventOnSelectionStateChangedBase_Parms), &Z_Construct_UFunction_UCommonButtonGroupBase_OnSelectionStateChangedBase_Statics::NewProp_bIsSelected_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonButtonGroupBase_OnSelectionStateChangedBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonButtonGroupBase_OnSelectionStateChangedBase_Statics::NewProp_BaseButton,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonButtonGroupBase_OnSelectionStateChangedBase_Statics::NewProp_bIsSelected,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonButtonGroupBase_OnSelectionStateChangedBase_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Groups/CommonButtonGroupBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonButtonGroupBase_OnSelectionStateChangedBase_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonButtonGroupBase, nullptr, "OnSelectionStateChangedBase", nullptr, nullptr, sizeof(CommonButtonGroupBase_eventOnSelectionStateChangedBase_Parms), Z_Construct_UFunction_UCommonButtonGroupBase_OnSelectionStateChangedBase_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonButtonGroupBase_OnSelectionStateChangedBase_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonButtonGroupBase_OnSelectionStateChangedBase_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonButtonGroupBase_OnSelectionStateChangedBase_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonButtonGroupBase_OnSelectionStateChangedBase()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonButtonGroupBase_OnSelectionStateChangedBase_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonButtonGroupBase_SelectButtonAtIndex_Statics
	{
		struct CommonButtonGroupBase_eventSelectButtonAtIndex_Parms
		{
			int32 ButtonIndex;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ButtonIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UCommonButtonGroupBase_SelectButtonAtIndex_Statics::NewProp_ButtonIndex = { "ButtonIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonButtonGroupBase_eventSelectButtonAtIndex_Parms, ButtonIndex), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonButtonGroupBase_SelectButtonAtIndex_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonButtonGroupBase_SelectButtonAtIndex_Statics::NewProp_ButtonIndex,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonButtonGroupBase_SelectButtonAtIndex_Statics::Function_MetaDataParams[] = {
		{ "Category", "BaseButtonGroup" },
		{ "Comment", "/**\n\x09 * Selects a button at a specific index in the group. Clears all selection if given an invalid index.\n\x09 * @param ButtonIndex The index of the button in the group to select\n\x09 */" },
		{ "ModuleRelativePath", "Public/Groups/CommonButtonGroupBase.h" },
		{ "ToolTip", "Selects a button at a specific index in the group. Clears all selection if given an invalid index.\n@param ButtonIndex The index of the button in the group to select" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonButtonGroupBase_SelectButtonAtIndex_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonButtonGroupBase, nullptr, "SelectButtonAtIndex", nullptr, nullptr, sizeof(CommonButtonGroupBase_eventSelectButtonAtIndex_Parms), Z_Construct_UFunction_UCommonButtonGroupBase_SelectButtonAtIndex_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonButtonGroupBase_SelectButtonAtIndex_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonButtonGroupBase_SelectButtonAtIndex_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonButtonGroupBase_SelectButtonAtIndex_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonButtonGroupBase_SelectButtonAtIndex()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonButtonGroupBase_SelectButtonAtIndex_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonButtonGroupBase_SelectNextButton_Statics
	{
		struct CommonButtonGroupBase_eventSelectNextButton_Parms
		{
			bool bAllowWrap;
		};
		static void NewProp_bAllowWrap_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAllowWrap;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UCommonButtonGroupBase_SelectNextButton_Statics::NewProp_bAllowWrap_SetBit(void* Obj)
	{
		((CommonButtonGroupBase_eventSelectNextButton_Parms*)Obj)->bAllowWrap = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCommonButtonGroupBase_SelectNextButton_Statics::NewProp_bAllowWrap = { "bAllowWrap", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CommonButtonGroupBase_eventSelectNextButton_Parms), &Z_Construct_UFunction_UCommonButtonGroupBase_SelectNextButton_Statics::NewProp_bAllowWrap_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonButtonGroupBase_SelectNextButton_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonButtonGroupBase_SelectNextButton_Statics::NewProp_bAllowWrap,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonButtonGroupBase_SelectNextButton_Statics::Function_MetaDataParams[] = {
		{ "Category", "BaseButtonGroup" },
		{ "Comment", "/** \n\x09 * Selects the next button in the group \n\x09 * @param bAllowWrap Whether to wrap to the first button if the last one is currently selected\n\x09 */" },
		{ "CPP_Default_bAllowWrap", "true" },
		{ "ModuleRelativePath", "Public/Groups/CommonButtonGroupBase.h" },
		{ "ToolTip", "Selects the next button in the group\n@param bAllowWrap Whether to wrap to the first button if the last one is currently selected" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonButtonGroupBase_SelectNextButton_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonButtonGroupBase, nullptr, "SelectNextButton", nullptr, nullptr, sizeof(CommonButtonGroupBase_eventSelectNextButton_Parms), Z_Construct_UFunction_UCommonButtonGroupBase_SelectNextButton_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonButtonGroupBase_SelectNextButton_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonButtonGroupBase_SelectNextButton_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonButtonGroupBase_SelectNextButton_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonButtonGroupBase_SelectNextButton()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonButtonGroupBase_SelectNextButton_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonButtonGroupBase_SelectPreviousButton_Statics
	{
		struct CommonButtonGroupBase_eventSelectPreviousButton_Parms
		{
			bool bAllowWrap;
		};
		static void NewProp_bAllowWrap_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAllowWrap;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UCommonButtonGroupBase_SelectPreviousButton_Statics::NewProp_bAllowWrap_SetBit(void* Obj)
	{
		((CommonButtonGroupBase_eventSelectPreviousButton_Parms*)Obj)->bAllowWrap = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCommonButtonGroupBase_SelectPreviousButton_Statics::NewProp_bAllowWrap = { "bAllowWrap", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CommonButtonGroupBase_eventSelectPreviousButton_Parms), &Z_Construct_UFunction_UCommonButtonGroupBase_SelectPreviousButton_Statics::NewProp_bAllowWrap_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonButtonGroupBase_SelectPreviousButton_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonButtonGroupBase_SelectPreviousButton_Statics::NewProp_bAllowWrap,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonButtonGroupBase_SelectPreviousButton_Statics::Function_MetaDataParams[] = {
		{ "Category", "BaseButtonGroup" },
		{ "Comment", "/** \n\x09 * Selects the previous button in the group \n\x09 * @param bAllowWrap Whether to wrap to the first button if the last one is currently selected\n\x09 */" },
		{ "CPP_Default_bAllowWrap", "true" },
		{ "ModuleRelativePath", "Public/Groups/CommonButtonGroupBase.h" },
		{ "ToolTip", "Selects the previous button in the group\n@param bAllowWrap Whether to wrap to the first button if the last one is currently selected" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonButtonGroupBase_SelectPreviousButton_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonButtonGroupBase, nullptr, "SelectPreviousButton", nullptr, nullptr, sizeof(CommonButtonGroupBase_eventSelectPreviousButton_Parms), Z_Construct_UFunction_UCommonButtonGroupBase_SelectPreviousButton_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonButtonGroupBase_SelectPreviousButton_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonButtonGroupBase_SelectPreviousButton_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonButtonGroupBase_SelectPreviousButton_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonButtonGroupBase_SelectPreviousButton()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonButtonGroupBase_SelectPreviousButton_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonButtonGroupBase_SetSelectionRequired_Statics
	{
		struct CommonButtonGroupBase_eventSetSelectionRequired_Parms
		{
			bool bRequireSelection;
		};
		static void NewProp_bRequireSelection_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRequireSelection;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UCommonButtonGroupBase_SetSelectionRequired_Statics::NewProp_bRequireSelection_SetBit(void* Obj)
	{
		((CommonButtonGroupBase_eventSetSelectionRequired_Parms*)Obj)->bRequireSelection = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCommonButtonGroupBase_SetSelectionRequired_Statics::NewProp_bRequireSelection = { "bRequireSelection", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CommonButtonGroupBase_eventSetSelectionRequired_Parms), &Z_Construct_UFunction_UCommonButtonGroupBase_SetSelectionRequired_Statics::NewProp_bRequireSelection_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonButtonGroupBase_SetSelectionRequired_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonButtonGroupBase_SetSelectionRequired_Statics::NewProp_bRequireSelection,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonButtonGroupBase_SetSelectionRequired_Statics::Function_MetaDataParams[] = {
		{ "Category", "BaseButtonGroup" },
		{ "Comment", "/** \n\x09 * Sets whether the group should always have a button selected.\n\x09 * @param bRequireSelection True to force the group to always have a button selected.\n\x09 * If true and nothing is selected, will select the first entry. If empty, will select the first button added.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Groups/CommonButtonGroupBase.h" },
		{ "ToolTip", "Sets whether the group should always have a button selected.\n@param bRequireSelection True to force the group to always have a button selected.\nIf true and nothing is selected, will select the first entry. If empty, will select the first button added." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonButtonGroupBase_SetSelectionRequired_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonButtonGroupBase, nullptr, "SetSelectionRequired", nullptr, nullptr, sizeof(CommonButtonGroupBase_eventSetSelectionRequired_Parms), Z_Construct_UFunction_UCommonButtonGroupBase_SetSelectionRequired_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonButtonGroupBase_SetSelectionRequired_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonButtonGroupBase_SetSelectionRequired_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonButtonGroupBase_SetSelectionRequired_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonButtonGroupBase_SetSelectionRequired()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonButtonGroupBase_SetSelectionRequired_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UCommonButtonGroupBase_NoRegister()
	{
		return UCommonButtonGroupBase::StaticClass();
	}
	struct Z_Construct_UClass_UCommonButtonGroupBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnSelectedButtonBaseChanged_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnSelectedButtonBaseChanged;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnHoveredButtonBaseChanged_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnHoveredButtonBaseChanged;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnButtonBaseClicked_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnButtonBaseClicked;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnButtonBaseDoubleClicked_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnButtonBaseDoubleClicked;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnSelectionCleared_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnSelectionCleared;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSelectionRequired_MetaData[];
#endif
		static void NewProp_bSelectionRequired_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSelectionRequired;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonButtonGroupBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCommonWidgetGroupBase,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonUI,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCommonButtonGroupBase_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCommonButtonGroupBase_DeselectAll, "DeselectAll" }, // 1809250954
		{ &Z_Construct_UFunction_UCommonButtonGroupBase_FindButtonIndex, "FindButtonIndex" }, // 1703142767
		{ &Z_Construct_UFunction_UCommonButtonGroupBase_GetButtonBaseAtIndex, "GetButtonBaseAtIndex" }, // 2368132845
		{ &Z_Construct_UFunction_UCommonButtonGroupBase_GetButtonCount, "GetButtonCount" }, // 2821813715
		{ &Z_Construct_UFunction_UCommonButtonGroupBase_GetHoveredButtonIndex, "GetHoveredButtonIndex" }, // 2596710984
		{ &Z_Construct_UFunction_UCommonButtonGroupBase_GetSelectedButtonBase, "GetSelectedButtonBase" }, // 2618087659
		{ &Z_Construct_UFunction_UCommonButtonGroupBase_GetSelectedButtonIndex, "GetSelectedButtonIndex" }, // 1187742877
		{ &Z_Construct_UFunction_UCommonButtonGroupBase_HasAnyButtons, "HasAnyButtons" }, // 1325213563
		{ &Z_Construct_UFunction_UCommonButtonGroupBase_OnButtonBaseHovered, "OnButtonBaseHovered" }, // 245476240
		{ &Z_Construct_UFunction_UCommonButtonGroupBase_OnButtonBaseUnhovered, "OnButtonBaseUnhovered" }, // 3703617419
		{ &Z_Construct_UFunction_UCommonButtonGroupBase_OnHandleButtonBaseClicked, "OnHandleButtonBaseClicked" }, // 1339108012
		{ &Z_Construct_UFunction_UCommonButtonGroupBase_OnHandleButtonBaseDoubleClicked, "OnHandleButtonBaseDoubleClicked" }, // 3158520617
		{ &Z_Construct_UFunction_UCommonButtonGroupBase_OnSelectionStateChangedBase, "OnSelectionStateChangedBase" }, // 2746386019
		{ &Z_Construct_UFunction_UCommonButtonGroupBase_SelectButtonAtIndex, "SelectButtonAtIndex" }, // 1298994520
		{ &Z_Construct_UFunction_UCommonButtonGroupBase_SelectNextButton, "SelectNextButton" }, // 1610464800
		{ &Z_Construct_UFunction_UCommonButtonGroupBase_SelectPreviousButton, "SelectPreviousButton" }, // 64153063
		{ &Z_Construct_UFunction_UCommonButtonGroupBase_SetSelectionRequired, "SetSelectionRequired" }, // 1490023077
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonButtonGroupBase_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** \n * Manages an arbitrary collection of CommonButton widgets.\n * Ensures that no more (and optionally, no less) than one button in the group is selected at a time\n */" },
		{ "IncludePath", "Groups/CommonButtonGroupBase.h" },
		{ "ModuleRelativePath", "Public/Groups/CommonButtonGroupBase.h" },
		{ "ToolTip", "Manages an arbitrary collection of CommonButton widgets.\nEnsures that no more (and optionally, no less) than one button in the group is selected at a time" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonButtonGroupBase_Statics::NewProp_OnSelectedButtonBaseChanged_MetaData[] = {
		{ "ModuleRelativePath", "Public/Groups/CommonButtonGroupBase.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UCommonButtonGroupBase_Statics::NewProp_OnSelectedButtonBaseChanged = { "OnSelectedButtonBaseChanged", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonButtonGroupBase, OnSelectedButtonBaseChanged), Z_Construct_UDelegateFunction_CommonUI_SimpleButtonBaseGroupDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UCommonButtonGroupBase_Statics::NewProp_OnSelectedButtonBaseChanged_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonButtonGroupBase_Statics::NewProp_OnSelectedButtonBaseChanged_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonButtonGroupBase_Statics::NewProp_OnHoveredButtonBaseChanged_MetaData[] = {
		{ "ModuleRelativePath", "Public/Groups/CommonButtonGroupBase.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UCommonButtonGroupBase_Statics::NewProp_OnHoveredButtonBaseChanged = { "OnHoveredButtonBaseChanged", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonButtonGroupBase, OnHoveredButtonBaseChanged), Z_Construct_UDelegateFunction_CommonUI_SimpleButtonBaseGroupDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UCommonButtonGroupBase_Statics::NewProp_OnHoveredButtonBaseChanged_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonButtonGroupBase_Statics::NewProp_OnHoveredButtonBaseChanged_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonButtonGroupBase_Statics::NewProp_OnButtonBaseClicked_MetaData[] = {
		{ "ModuleRelativePath", "Public/Groups/CommonButtonGroupBase.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UCommonButtonGroupBase_Statics::NewProp_OnButtonBaseClicked = { "OnButtonBaseClicked", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonButtonGroupBase, OnButtonBaseClicked), Z_Construct_UDelegateFunction_CommonUI_SimpleButtonBaseGroupDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UCommonButtonGroupBase_Statics::NewProp_OnButtonBaseClicked_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonButtonGroupBase_Statics::NewProp_OnButtonBaseClicked_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonButtonGroupBase_Statics::NewProp_OnButtonBaseDoubleClicked_MetaData[] = {
		{ "ModuleRelativePath", "Public/Groups/CommonButtonGroupBase.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UCommonButtonGroupBase_Statics::NewProp_OnButtonBaseDoubleClicked = { "OnButtonBaseDoubleClicked", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonButtonGroupBase, OnButtonBaseDoubleClicked), Z_Construct_UDelegateFunction_CommonUI_SimpleButtonBaseGroupDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UCommonButtonGroupBase_Statics::NewProp_OnButtonBaseDoubleClicked_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonButtonGroupBase_Statics::NewProp_OnButtonBaseDoubleClicked_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonButtonGroupBase_Statics::NewProp_OnSelectionCleared_MetaData[] = {
		{ "ModuleRelativePath", "Public/Groups/CommonButtonGroupBase.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UCommonButtonGroupBase_Statics::NewProp_OnSelectionCleared = { "OnSelectionCleared", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonButtonGroupBase, OnSelectionCleared), Z_Construct_UDelegateFunction_CommonUI_OnSelectionCleared__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UCommonButtonGroupBase_Statics::NewProp_OnSelectionCleared_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonButtonGroupBase_Statics::NewProp_OnSelectionCleared_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonButtonGroupBase_Statics::NewProp_bSelectionRequired_MetaData[] = {
		{ "Category", "BaseButtonGroup" },
		{ "Comment", "/** If true, the group will force that a button be selected at all times */" },
		{ "ExposeOnSpawn", "true" },
		{ "ModuleRelativePath", "Public/Groups/CommonButtonGroupBase.h" },
		{ "ToolTip", "If true, the group will force that a button be selected at all times" },
	};
#endif
	void Z_Construct_UClass_UCommonButtonGroupBase_Statics::NewProp_bSelectionRequired_SetBit(void* Obj)
	{
		((UCommonButtonGroupBase*)Obj)->bSelectionRequired = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCommonButtonGroupBase_Statics::NewProp_bSelectionRequired = { "bSelectionRequired", nullptr, (EPropertyFlags)0x0021080000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCommonButtonGroupBase), &Z_Construct_UClass_UCommonButtonGroupBase_Statics::NewProp_bSelectionRequired_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCommonButtonGroupBase_Statics::NewProp_bSelectionRequired_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonButtonGroupBase_Statics::NewProp_bSelectionRequired_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCommonButtonGroupBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonButtonGroupBase_Statics::NewProp_OnSelectedButtonBaseChanged,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonButtonGroupBase_Statics::NewProp_OnHoveredButtonBaseChanged,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonButtonGroupBase_Statics::NewProp_OnButtonBaseClicked,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonButtonGroupBase_Statics::NewProp_OnButtonBaseDoubleClicked,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonButtonGroupBase_Statics::NewProp_OnSelectionCleared,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonButtonGroupBase_Statics::NewProp_bSelectionRequired,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonButtonGroupBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonButtonGroupBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCommonButtonGroupBase_Statics::ClassParams = {
		&UCommonButtonGroupBase::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UCommonButtonGroupBase_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UCommonButtonGroupBase_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonButtonGroupBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonButtonGroupBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonButtonGroupBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCommonButtonGroupBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCommonButtonGroupBase, 2215491655);
	template<> COMMONUI_API UClass* StaticClass<UCommonButtonGroupBase>()
	{
		return UCommonButtonGroupBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCommonButtonGroupBase(Z_Construct_UClass_UCommonButtonGroupBase, &UCommonButtonGroupBase::StaticClass, TEXT("/Script/CommonUI"), TEXT("UCommonButtonGroupBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonButtonGroupBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
