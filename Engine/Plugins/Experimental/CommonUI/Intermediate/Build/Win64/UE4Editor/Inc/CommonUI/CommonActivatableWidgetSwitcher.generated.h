// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMMONUI_CommonActivatableWidgetSwitcher_generated_h
#error "CommonActivatableWidgetSwitcher.generated.h already included, missing '#pragma once' in CommonActivatableWidgetSwitcher.h"
#endif
#define COMMONUI_CommonActivatableWidgetSwitcher_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidgetSwitcher_h_19_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidgetSwitcher_h_19_RPC_WRAPPERS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidgetSwitcher_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidgetSwitcher_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonActivatableWidgetSwitcher(); \
	friend struct Z_Construct_UClass_UCommonActivatableWidgetSwitcher_Statics; \
public: \
	DECLARE_CLASS(UCommonActivatableWidgetSwitcher, UCommonAnimatedSwitcher, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonActivatableWidgetSwitcher)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidgetSwitcher_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUCommonActivatableWidgetSwitcher(); \
	friend struct Z_Construct_UClass_UCommonActivatableWidgetSwitcher_Statics; \
public: \
	DECLARE_CLASS(UCommonActivatableWidgetSwitcher, UCommonAnimatedSwitcher, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonActivatableWidgetSwitcher)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidgetSwitcher_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonActivatableWidgetSwitcher(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonActivatableWidgetSwitcher) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonActivatableWidgetSwitcher); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonActivatableWidgetSwitcher); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonActivatableWidgetSwitcher(UCommonActivatableWidgetSwitcher&&); \
	NO_API UCommonActivatableWidgetSwitcher(const UCommonActivatableWidgetSwitcher&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidgetSwitcher_h_19_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonActivatableWidgetSwitcher(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonActivatableWidgetSwitcher(UCommonActivatableWidgetSwitcher&&); \
	NO_API UCommonActivatableWidgetSwitcher(const UCommonActivatableWidgetSwitcher&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonActivatableWidgetSwitcher); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonActivatableWidgetSwitcher); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonActivatableWidgetSwitcher)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidgetSwitcher_h_19_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidgetSwitcher_h_16_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidgetSwitcher_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidgetSwitcher_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidgetSwitcher_h_19_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidgetSwitcher_h_19_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidgetSwitcher_h_19_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidgetSwitcher_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidgetSwitcher_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidgetSwitcher_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidgetSwitcher_h_19_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidgetSwitcher_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidgetSwitcher_h_19_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidgetSwitcher_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonActivatableWidgetSwitcher>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidgetSwitcher_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
