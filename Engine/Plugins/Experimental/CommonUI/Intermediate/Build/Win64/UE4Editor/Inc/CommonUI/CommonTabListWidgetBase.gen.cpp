// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonUI/Public/CommonTabListWidgetBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCommonTabListWidgetBase() {}
// Cross Module References
	COMMONUI_API UFunction* Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabButtonRemoval__DelegateSignature();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonTabListWidgetBase();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonButtonBase_NoRegister();
	COMMONUI_API UFunction* Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabButtonCreation__DelegateSignature();
	COMMONUI_API UFunction* Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabSelected__DelegateSignature();
	COMMONUI_API UScriptStruct* Z_Construct_UScriptStruct_FCommonRegisteredTabInfo();
	UPackage* Z_Construct_UPackage__Script_CommonUI();
	UMG_API UClass* Z_Construct_UClass_UWidget_NoRegister();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonTabListWidgetBase_NoRegister();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonUserWidget();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonAnimatedSwitcher_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	UMG_API UEnum* Z_Construct_UEnum_UMG_ESlateVisibility();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FDataTableRowHandle();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonButtonGroupBase_NoRegister();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabButtonRemoval__DelegateSignature_Statics
	{
		struct CommonTabListWidgetBase_eventOnTabButtonRemoval_Parms
		{
			FName TabId;
			UCommonButtonBase* TabButton;
		};
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_TabId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TabButton_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TabButton;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabButtonRemoval__DelegateSignature_Statics::NewProp_TabId = { "TabId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonTabListWidgetBase_eventOnTabButtonRemoval_Parms, TabId), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabButtonRemoval__DelegateSignature_Statics::NewProp_TabButton_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabButtonRemoval__DelegateSignature_Statics::NewProp_TabButton = { "TabButton", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonTabListWidgetBase_eventOnTabButtonRemoval_Parms, TabButton), Z_Construct_UClass_UCommonButtonBase_NoRegister, METADATA_PARAMS(Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabButtonRemoval__DelegateSignature_Statics::NewProp_TabButton_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabButtonRemoval__DelegateSignature_Statics::NewProp_TabButton_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabButtonRemoval__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabButtonRemoval__DelegateSignature_Statics::NewProp_TabId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabButtonRemoval__DelegateSignature_Statics::NewProp_TabButton,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabButtonRemoval__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Delegate broadcast when a tab is being removed. Allows clean ups after destruction. */" },
		{ "ModuleRelativePath", "Public/CommonTabListWidgetBase.h" },
		{ "ToolTip", "Delegate broadcast when a tab is being removed. Allows clean ups after destruction." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabButtonRemoval__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonTabListWidgetBase, nullptr, "OnTabButtonRemoval__DelegateSignature", nullptr, nullptr, sizeof(CommonTabListWidgetBase_eventOnTabButtonRemoval_Parms), Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabButtonRemoval__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabButtonRemoval__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabButtonRemoval__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabButtonRemoval__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabButtonRemoval__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabButtonRemoval__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabButtonCreation__DelegateSignature_Statics
	{
		struct CommonTabListWidgetBase_eventOnTabButtonCreation_Parms
		{
			FName TabId;
			UCommonButtonBase* TabButton;
		};
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_TabId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TabButton_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TabButton;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabButtonCreation__DelegateSignature_Statics::NewProp_TabId = { "TabId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonTabListWidgetBase_eventOnTabButtonCreation_Parms, TabId), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabButtonCreation__DelegateSignature_Statics::NewProp_TabButton_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabButtonCreation__DelegateSignature_Statics::NewProp_TabButton = { "TabButton", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonTabListWidgetBase_eventOnTabButtonCreation_Parms, TabButton), Z_Construct_UClass_UCommonButtonBase_NoRegister, METADATA_PARAMS(Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabButtonCreation__DelegateSignature_Statics::NewProp_TabButton_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabButtonCreation__DelegateSignature_Statics::NewProp_TabButton_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabButtonCreation__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabButtonCreation__DelegateSignature_Statics::NewProp_TabId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabButtonCreation__DelegateSignature_Statics::NewProp_TabButton,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabButtonCreation__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Delegate broadcast when a new tab is created. Allows hook ups after creation. */" },
		{ "ModuleRelativePath", "Public/CommonTabListWidgetBase.h" },
		{ "ToolTip", "Delegate broadcast when a new tab is created. Allows hook ups after creation." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabButtonCreation__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonTabListWidgetBase, nullptr, "OnTabButtonCreation__DelegateSignature", nullptr, nullptr, sizeof(CommonTabListWidgetBase_eventOnTabButtonCreation_Parms), Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabButtonCreation__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabButtonCreation__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabButtonCreation__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabButtonCreation__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabButtonCreation__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabButtonCreation__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabSelected__DelegateSignature_Statics
	{
		struct CommonTabListWidgetBase_eventOnTabSelected_Parms
		{
			FName TabId;
		};
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_TabId;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabSelected__DelegateSignature_Statics::NewProp_TabId = { "TabId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonTabListWidgetBase_eventOnTabSelected_Parms, TabId), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabSelected__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabSelected__DelegateSignature_Statics::NewProp_TabId,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabSelected__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Delegate broadcast when a new tab is selected. */" },
		{ "ModuleRelativePath", "Public/CommonTabListWidgetBase.h" },
		{ "ToolTip", "Delegate broadcast when a new tab is selected." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabSelected__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonTabListWidgetBase, nullptr, "OnTabSelected__DelegateSignature", nullptr, nullptr, sizeof(CommonTabListWidgetBase_eventOnTabSelected_Parms), Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabSelected__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabSelected__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabSelected__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabSelected__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabSelected__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabSelected__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
class UScriptStruct* FCommonRegisteredTabInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern COMMONUI_API uint32 Get_Z_Construct_UScriptStruct_FCommonRegisteredTabInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCommonRegisteredTabInfo, Z_Construct_UPackage__Script_CommonUI(), TEXT("CommonRegisteredTabInfo"), sizeof(FCommonRegisteredTabInfo), Get_Z_Construct_UScriptStruct_FCommonRegisteredTabInfo_Hash());
	}
	return Singleton;
}
template<> COMMONUI_API UScriptStruct* StaticStruct<FCommonRegisteredTabInfo>()
{
	return FCommonRegisteredTabInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCommonRegisteredTabInfo(FCommonRegisteredTabInfo::StaticStruct, TEXT("/Script/CommonUI"), TEXT("CommonRegisteredTabInfo"), false, nullptr, nullptr);
static struct FScriptStruct_CommonUI_StaticRegisterNativesFCommonRegisteredTabInfo
{
	FScriptStruct_CommonUI_StaticRegisterNativesFCommonRegisteredTabInfo()
	{
		UScriptStruct::DeferCppStructOps<FCommonRegisteredTabInfo>(FName(TEXT("CommonRegisteredTabInfo")));
	}
} ScriptStruct_CommonUI_StaticRegisterNativesFCommonRegisteredTabInfo;
	struct Z_Construct_UScriptStruct_FCommonRegisteredTabInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TabIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_TabIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TabButton_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TabButton;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ContentInstance_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ContentInstance;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonRegisteredTabInfo_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Information about a registered tab in the tab list */" },
		{ "ModuleRelativePath", "Public/CommonTabListWidgetBase.h" },
		{ "ToolTip", "Information about a registered tab in the tab list" },
	};
#endif
	void* Z_Construct_UScriptStruct_FCommonRegisteredTabInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FCommonRegisteredTabInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonRegisteredTabInfo_Statics::NewProp_TabIndex_MetaData[] = {
		{ "Comment", "/** The index of the tab in the list */" },
		{ "ModuleRelativePath", "Public/CommonTabListWidgetBase.h" },
		{ "ToolTip", "The index of the tab in the list" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FCommonRegisteredTabInfo_Statics::NewProp_TabIndex = { "TabIndex", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCommonRegisteredTabInfo, TabIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonRegisteredTabInfo_Statics::NewProp_TabIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonRegisteredTabInfo_Statics::NewProp_TabIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonRegisteredTabInfo_Statics::NewProp_TabButton_MetaData[] = {
		{ "Comment", "/** The actual button widget that represents this tab on-screen */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/CommonTabListWidgetBase.h" },
		{ "ToolTip", "The actual button widget that represents this tab on-screen" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FCommonRegisteredTabInfo_Statics::NewProp_TabButton = { "TabButton", nullptr, (EPropertyFlags)0x0010000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCommonRegisteredTabInfo, TabButton), Z_Construct_UClass_UCommonButtonBase_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonRegisteredTabInfo_Statics::NewProp_TabButton_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonRegisteredTabInfo_Statics::NewProp_TabButton_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonRegisteredTabInfo_Statics::NewProp_ContentInstance_MetaData[] = {
		{ "Comment", "/** The actual instance of the content widget to display when this tab is selected. Can be null if a load is required. */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/CommonTabListWidgetBase.h" },
		{ "ToolTip", "The actual instance of the content widget to display when this tab is selected. Can be null if a load is required." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FCommonRegisteredTabInfo_Statics::NewProp_ContentInstance = { "ContentInstance", nullptr, (EPropertyFlags)0x0010000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCommonRegisteredTabInfo, ContentInstance), Z_Construct_UClass_UWidget_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonRegisteredTabInfo_Statics::NewProp_ContentInstance_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonRegisteredTabInfo_Statics::NewProp_ContentInstance_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FCommonRegisteredTabInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonRegisteredTabInfo_Statics::NewProp_TabIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonRegisteredTabInfo_Statics::NewProp_TabButton,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonRegisteredTabInfo_Statics::NewProp_ContentInstance,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FCommonRegisteredTabInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CommonUI,
		nullptr,
		&NewStructOps,
		"CommonRegisteredTabInfo",
		sizeof(FCommonRegisteredTabInfo),
		alignof(FCommonRegisteredTabInfo),
		Z_Construct_UScriptStruct_FCommonRegisteredTabInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonRegisteredTabInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000005),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonRegisteredTabInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonRegisteredTabInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FCommonRegisteredTabInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FCommonRegisteredTabInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CommonUI();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CommonRegisteredTabInfo"), sizeof(FCommonRegisteredTabInfo), Get_Z_Construct_UScriptStruct_FCommonRegisteredTabInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FCommonRegisteredTabInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCommonRegisteredTabInfo_Hash() { return 3576533032U; }
	DEFINE_FUNCTION(UCommonTabListWidgetBase::execHandleNextTabInputAction)
	{
		P_GET_UBOOL_REF(Z_Param_Out_bPassthrough);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->HandleNextTabInputAction(Z_Param_Out_bPassthrough);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonTabListWidgetBase::execHandlePreviousTabInputAction)
	{
		P_GET_UBOOL_REF(Z_Param_Out_bPassthrough);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->HandlePreviousTabInputAction(Z_Param_Out_bPassthrough);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonTabListWidgetBase::execHandleTabButtonSelected)
	{
		P_GET_OBJECT(UCommonButtonBase,Z_Param_SelectedTabButton);
		P_GET_PROPERTY(FIntProperty,Z_Param_ButtonIndex);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->HandleTabButtonSelected(Z_Param_SelectedTabButton,Z_Param_ButtonIndex);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonTabListWidgetBase::execGetTabButtonBaseByID)
	{
		P_GET_PROPERTY(FNameProperty,Z_Param_TabNameID);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UCommonButtonBase**)Z_Param__Result=P_THIS->GetTabButtonBaseByID(Z_Param_TabNameID);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonTabListWidgetBase::execHandleTabRemoval)
	{
		P_GET_PROPERTY(FNameProperty,Z_Param_TabNameID);
		P_GET_OBJECT(UCommonButtonBase,Z_Param_TabButton);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->HandleTabRemoval_Implementation(Z_Param_TabNameID,Z_Param_TabButton);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonTabListWidgetBase::execHandleTabCreation)
	{
		P_GET_PROPERTY(FNameProperty,Z_Param_TabNameID);
		P_GET_OBJECT(UCommonButtonBase,Z_Param_TabButton);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->HandleTabCreation_Implementation(Z_Param_TabNameID,Z_Param_TabButton);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonTabListWidgetBase::execSetListeningForInput)
	{
		P_GET_UBOOL(Z_Param_bShouldListen);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetListeningForInput(Z_Param_bShouldListen);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonTabListWidgetBase::execDisableTabWithReason)
	{
		P_GET_PROPERTY(FNameProperty,Z_Param_TabNameID);
		P_GET_PROPERTY_REF(FTextProperty,Z_Param_Out_Reason);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->DisableTabWithReason(Z_Param_TabNameID,Z_Param_Out_Reason);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonTabListWidgetBase::execSetTabInteractionEnabled)
	{
		P_GET_PROPERTY(FNameProperty,Z_Param_TabNameID);
		P_GET_UBOOL(Z_Param_bEnable);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetTabInteractionEnabled(Z_Param_TabNameID,Z_Param_bEnable);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonTabListWidgetBase::execSetTabEnabled)
	{
		P_GET_PROPERTY(FNameProperty,Z_Param_TabNameID);
		P_GET_UBOOL(Z_Param_bEnable);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetTabEnabled(Z_Param_TabNameID,Z_Param_bEnable);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonTabListWidgetBase::execSetTabVisibility)
	{
		P_GET_PROPERTY(FNameProperty,Z_Param_TabNameID);
		P_GET_ENUM(ESlateVisibility,Z_Param_NewVisibility);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetTabVisibility(Z_Param_TabNameID,ESlateVisibility(Z_Param_NewVisibility));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonTabListWidgetBase::execGetTabIdAtIndex)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_Index);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FName*)Z_Param__Result=P_THIS->GetTabIdAtIndex(Z_Param_Index);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonTabListWidgetBase::execGetSelectedTabId)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FName*)Z_Param__Result=P_THIS->GetSelectedTabId();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonTabListWidgetBase::execSelectTabByID)
	{
		P_GET_PROPERTY(FNameProperty,Z_Param_TabNameID);
		P_GET_UBOOL(Z_Param_bSuppressClickFeedback);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->SelectTabByID(Z_Param_TabNameID,Z_Param_bSuppressClickFeedback);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonTabListWidgetBase::execGetTabCount)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetTabCount();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonTabListWidgetBase::execRemoveAllTabs)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->RemoveAllTabs();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonTabListWidgetBase::execRemoveTab)
	{
		P_GET_PROPERTY(FNameProperty,Z_Param_TabNameID);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->RemoveTab(Z_Param_TabNameID);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonTabListWidgetBase::execRegisterTab)
	{
		P_GET_PROPERTY(FNameProperty,Z_Param_TabNameID);
		P_GET_OBJECT(UClass,Z_Param_ButtonWidgetType);
		P_GET_OBJECT(UWidget,Z_Param_ContentWidget);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->RegisterTab(Z_Param_TabNameID,Z_Param_ButtonWidgetType,Z_Param_ContentWidget);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonTabListWidgetBase::execGetLinkedSwitcher)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UCommonAnimatedSwitcher**)Z_Param__Result=P_THIS->GetLinkedSwitcher();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonTabListWidgetBase::execSetLinkedSwitcher)
	{
		P_GET_OBJECT(UCommonAnimatedSwitcher,Z_Param_CommonSwitcher);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetLinkedSwitcher(Z_Param_CommonSwitcher);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonTabListWidgetBase::execGetActiveTab)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FName*)Z_Param__Result=P_THIS->GetActiveTab();
		P_NATIVE_END;
	}
	static FName NAME_UCommonTabListWidgetBase_HandlePostLinkedSwitcherChanged_BP = FName(TEXT("HandlePostLinkedSwitcherChanged_BP"));
	void UCommonTabListWidgetBase::HandlePostLinkedSwitcherChanged_BP()
	{
		ProcessEvent(FindFunctionChecked(NAME_UCommonTabListWidgetBase_HandlePostLinkedSwitcherChanged_BP),NULL);
	}
	static FName NAME_UCommonTabListWidgetBase_HandlePreLinkedSwitcherChanged_BP = FName(TEXT("HandlePreLinkedSwitcherChanged_BP"));
	void UCommonTabListWidgetBase::HandlePreLinkedSwitcherChanged_BP()
	{
		ProcessEvent(FindFunctionChecked(NAME_UCommonTabListWidgetBase_HandlePreLinkedSwitcherChanged_BP),NULL);
	}
	static FName NAME_UCommonTabListWidgetBase_HandleTabCreation = FName(TEXT("HandleTabCreation"));
	void UCommonTabListWidgetBase::HandleTabCreation(FName TabNameID, UCommonButtonBase* TabButton)
	{
		CommonTabListWidgetBase_eventHandleTabCreation_Parms Parms;
		Parms.TabNameID=TabNameID;
		Parms.TabButton=TabButton;
		ProcessEvent(FindFunctionChecked(NAME_UCommonTabListWidgetBase_HandleTabCreation),&Parms);
	}
	static FName NAME_UCommonTabListWidgetBase_HandleTabRemoval = FName(TEXT("HandleTabRemoval"));
	void UCommonTabListWidgetBase::HandleTabRemoval(FName TabNameID, UCommonButtonBase* TabButton)
	{
		CommonTabListWidgetBase_eventHandleTabRemoval_Parms Parms;
		Parms.TabNameID=TabNameID;
		Parms.TabButton=TabButton;
		ProcessEvent(FindFunctionChecked(NAME_UCommonTabListWidgetBase_HandleTabRemoval),&Parms);
	}
	void UCommonTabListWidgetBase::StaticRegisterNativesUCommonTabListWidgetBase()
	{
		UClass* Class = UCommonTabListWidgetBase::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "DisableTabWithReason", &UCommonTabListWidgetBase::execDisableTabWithReason },
			{ "GetActiveTab", &UCommonTabListWidgetBase::execGetActiveTab },
			{ "GetLinkedSwitcher", &UCommonTabListWidgetBase::execGetLinkedSwitcher },
			{ "GetSelectedTabId", &UCommonTabListWidgetBase::execGetSelectedTabId },
			{ "GetTabButtonBaseByID", &UCommonTabListWidgetBase::execGetTabButtonBaseByID },
			{ "GetTabCount", &UCommonTabListWidgetBase::execGetTabCount },
			{ "GetTabIdAtIndex", &UCommonTabListWidgetBase::execGetTabIdAtIndex },
			{ "HandleNextTabInputAction", &UCommonTabListWidgetBase::execHandleNextTabInputAction },
			{ "HandlePreviousTabInputAction", &UCommonTabListWidgetBase::execHandlePreviousTabInputAction },
			{ "HandleTabButtonSelected", &UCommonTabListWidgetBase::execHandleTabButtonSelected },
			{ "HandleTabCreation", &UCommonTabListWidgetBase::execHandleTabCreation },
			{ "HandleTabRemoval", &UCommonTabListWidgetBase::execHandleTabRemoval },
			{ "RegisterTab", &UCommonTabListWidgetBase::execRegisterTab },
			{ "RemoveAllTabs", &UCommonTabListWidgetBase::execRemoveAllTabs },
			{ "RemoveTab", &UCommonTabListWidgetBase::execRemoveTab },
			{ "SelectTabByID", &UCommonTabListWidgetBase::execSelectTabByID },
			{ "SetLinkedSwitcher", &UCommonTabListWidgetBase::execSetLinkedSwitcher },
			{ "SetListeningForInput", &UCommonTabListWidgetBase::execSetListeningForInput },
			{ "SetTabEnabled", &UCommonTabListWidgetBase::execSetTabEnabled },
			{ "SetTabInteractionEnabled", &UCommonTabListWidgetBase::execSetTabInteractionEnabled },
			{ "SetTabVisibility", &UCommonTabListWidgetBase::execSetTabVisibility },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCommonTabListWidgetBase_DisableTabWithReason_Statics
	{
		struct CommonTabListWidgetBase_eventDisableTabWithReason_Parms
		{
			FName TabNameID;
			FText Reason;
		};
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_TabNameID;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Reason_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_Reason;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UCommonTabListWidgetBase_DisableTabWithReason_Statics::NewProp_TabNameID = { "TabNameID", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonTabListWidgetBase_eventDisableTabWithReason_Parms, TabNameID), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonTabListWidgetBase_DisableTabWithReason_Statics::NewProp_Reason_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UCommonTabListWidgetBase_DisableTabWithReason_Statics::NewProp_Reason = { "Reason", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonTabListWidgetBase_eventDisableTabWithReason_Parms, Reason), METADATA_PARAMS(Z_Construct_UFunction_UCommonTabListWidgetBase_DisableTabWithReason_Statics::NewProp_Reason_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_DisableTabWithReason_Statics::NewProp_Reason_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonTabListWidgetBase_DisableTabWithReason_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonTabListWidgetBase_DisableTabWithReason_Statics::NewProp_TabNameID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonTabListWidgetBase_DisableTabWithReason_Statics::NewProp_Reason,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonTabListWidgetBase_DisableTabWithReason_Statics::Function_MetaDataParams[] = {
		{ "Category", "TabList" },
		{ "Comment", "/** Disables the tab associated with the given ID with a reason */" },
		{ "ModuleRelativePath", "Public/CommonTabListWidgetBase.h" },
		{ "ToolTip", "Disables the tab associated with the given ID with a reason" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonTabListWidgetBase_DisableTabWithReason_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonTabListWidgetBase, nullptr, "DisableTabWithReason", nullptr, nullptr, sizeof(CommonTabListWidgetBase_eventDisableTabWithReason_Parms), Z_Construct_UFunction_UCommonTabListWidgetBase_DisableTabWithReason_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_DisableTabWithReason_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonTabListWidgetBase_DisableTabWithReason_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_DisableTabWithReason_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonTabListWidgetBase_DisableTabWithReason()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonTabListWidgetBase_DisableTabWithReason_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonTabListWidgetBase_GetActiveTab_Statics
	{
		struct CommonTabListWidgetBase_eventGetActiveTab_Parms
		{
			FName ReturnValue;
		};
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UCommonTabListWidgetBase_GetActiveTab_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonTabListWidgetBase_eventGetActiveTab_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonTabListWidgetBase_GetActiveTab_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonTabListWidgetBase_GetActiveTab_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonTabListWidgetBase_GetActiveTab_Statics::Function_MetaDataParams[] = {
		{ "Category", "TabList" },
		{ "Comment", "/** @return The currently active (selected) tab */" },
		{ "ModuleRelativePath", "Public/CommonTabListWidgetBase.h" },
		{ "ToolTip", "@return The currently active (selected) tab" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonTabListWidgetBase_GetActiveTab_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonTabListWidgetBase, nullptr, "GetActiveTab", nullptr, nullptr, sizeof(CommonTabListWidgetBase_eventGetActiveTab_Parms), Z_Construct_UFunction_UCommonTabListWidgetBase_GetActiveTab_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_GetActiveTab_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonTabListWidgetBase_GetActiveTab_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_GetActiveTab_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonTabListWidgetBase_GetActiveTab()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonTabListWidgetBase_GetActiveTab_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonTabListWidgetBase_GetLinkedSwitcher_Statics
	{
		struct CommonTabListWidgetBase_eventGetLinkedSwitcher_Parms
		{
			UCommonAnimatedSwitcher* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonTabListWidgetBase_GetLinkedSwitcher_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCommonTabListWidgetBase_GetLinkedSwitcher_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonTabListWidgetBase_eventGetLinkedSwitcher_Parms, ReturnValue), Z_Construct_UClass_UCommonAnimatedSwitcher_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UCommonTabListWidgetBase_GetLinkedSwitcher_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_GetLinkedSwitcher_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonTabListWidgetBase_GetLinkedSwitcher_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonTabListWidgetBase_GetLinkedSwitcher_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonTabListWidgetBase_GetLinkedSwitcher_Statics::Function_MetaDataParams[] = {
		{ "Category", "TabList" },
		{ "Comment", "/** @return The switcher that this tab list is associated with and manipulates */" },
		{ "ModuleRelativePath", "Public/CommonTabListWidgetBase.h" },
		{ "ToolTip", "@return The switcher that this tab list is associated with and manipulates" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonTabListWidgetBase_GetLinkedSwitcher_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonTabListWidgetBase, nullptr, "GetLinkedSwitcher", nullptr, nullptr, sizeof(CommonTabListWidgetBase_eventGetLinkedSwitcher_Parms), Z_Construct_UFunction_UCommonTabListWidgetBase_GetLinkedSwitcher_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_GetLinkedSwitcher_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonTabListWidgetBase_GetLinkedSwitcher_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_GetLinkedSwitcher_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonTabListWidgetBase_GetLinkedSwitcher()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonTabListWidgetBase_GetLinkedSwitcher_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonTabListWidgetBase_GetSelectedTabId_Statics
	{
		struct CommonTabListWidgetBase_eventGetSelectedTabId_Parms
		{
			FName ReturnValue;
		};
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UCommonTabListWidgetBase_GetSelectedTabId_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonTabListWidgetBase_eventGetSelectedTabId_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonTabListWidgetBase_GetSelectedTabId_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonTabListWidgetBase_GetSelectedTabId_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonTabListWidgetBase_GetSelectedTabId_Statics::Function_MetaDataParams[] = {
		{ "Category", "TabList" },
		{ "ModuleRelativePath", "Public/CommonTabListWidgetBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonTabListWidgetBase_GetSelectedTabId_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonTabListWidgetBase, nullptr, "GetSelectedTabId", nullptr, nullptr, sizeof(CommonTabListWidgetBase_eventGetSelectedTabId_Parms), Z_Construct_UFunction_UCommonTabListWidgetBase_GetSelectedTabId_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_GetSelectedTabId_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonTabListWidgetBase_GetSelectedTabId_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_GetSelectedTabId_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonTabListWidgetBase_GetSelectedTabId()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonTabListWidgetBase_GetSelectedTabId_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabButtonBaseByID_Statics
	{
		struct CommonTabListWidgetBase_eventGetTabButtonBaseByID_Parms
		{
			FName TabNameID;
			UCommonButtonBase* ReturnValue;
		};
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_TabNameID;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabButtonBaseByID_Statics::NewProp_TabNameID = { "TabNameID", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonTabListWidgetBase_eventGetTabButtonBaseByID_Parms, TabNameID), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabButtonBaseByID_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabButtonBaseByID_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonTabListWidgetBase_eventGetTabButtonBaseByID_Parms, ReturnValue), Z_Construct_UClass_UCommonButtonBase_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabButtonBaseByID_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabButtonBaseByID_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabButtonBaseByID_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabButtonBaseByID_Statics::NewProp_TabNameID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabButtonBaseByID_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabButtonBaseByID_Statics::Function_MetaDataParams[] = {
		{ "BlueprintProtected", "true" },
		{ "Category", "TabList" },
		{ "Comment", "/** Returns the tab button matching the ID, if found */" },
		{ "ModuleRelativePath", "Public/CommonTabListWidgetBase.h" },
		{ "ToolTip", "Returns the tab button matching the ID, if found" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabButtonBaseByID_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonTabListWidgetBase, nullptr, "GetTabButtonBaseByID", nullptr, nullptr, sizeof(CommonTabListWidgetBase_eventGetTabButtonBaseByID_Parms), Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabButtonBaseByID_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabButtonBaseByID_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabButtonBaseByID_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabButtonBaseByID_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabButtonBaseByID()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabButtonBaseByID_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabCount_Statics
	{
		struct CommonTabListWidgetBase_eventGetTabCount_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabCount_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonTabListWidgetBase_eventGetTabCount_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabCount_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabCount_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabCount_Statics::Function_MetaDataParams[] = {
		{ "Category", "TabList" },
		{ "ModuleRelativePath", "Public/CommonTabListWidgetBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabCount_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonTabListWidgetBase, nullptr, "GetTabCount", nullptr, nullptr, sizeof(CommonTabListWidgetBase_eventGetTabCount_Parms), Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabCount_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabCount_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabCount_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabCount_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabCount()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabCount_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabIdAtIndex_Statics
	{
		struct CommonTabListWidgetBase_eventGetTabIdAtIndex_Parms
		{
			int32 Index;
			FName ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabIdAtIndex_Statics::NewProp_Index = { "Index", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonTabListWidgetBase_eventGetTabIdAtIndex_Parms, Index), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabIdAtIndex_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonTabListWidgetBase_eventGetTabIdAtIndex_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabIdAtIndex_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabIdAtIndex_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabIdAtIndex_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabIdAtIndex_Statics::Function_MetaDataParams[] = {
		{ "Category", "TabList" },
		{ "ModuleRelativePath", "Public/CommonTabListWidgetBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabIdAtIndex_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonTabListWidgetBase, nullptr, "GetTabIdAtIndex", nullptr, nullptr, sizeof(CommonTabListWidgetBase_eventGetTabIdAtIndex_Parms), Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabIdAtIndex_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabIdAtIndex_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabIdAtIndex_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabIdAtIndex_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabIdAtIndex()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabIdAtIndex_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonTabListWidgetBase_HandleNextTabInputAction_Statics
	{
		struct CommonTabListWidgetBase_eventHandleNextTabInputAction_Parms
		{
			bool bPassthrough;
		};
		static void NewProp_bPassthrough_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPassthrough;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UCommonTabListWidgetBase_HandleNextTabInputAction_Statics::NewProp_bPassthrough_SetBit(void* Obj)
	{
		((CommonTabListWidgetBase_eventHandleNextTabInputAction_Parms*)Obj)->bPassthrough = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCommonTabListWidgetBase_HandleNextTabInputAction_Statics::NewProp_bPassthrough = { "bPassthrough", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CommonTabListWidgetBase_eventHandleNextTabInputAction_Parms), &Z_Construct_UFunction_UCommonTabListWidgetBase_HandleNextTabInputAction_Statics::NewProp_bPassthrough_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonTabListWidgetBase_HandleNextTabInputAction_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonTabListWidgetBase_HandleNextTabInputAction_Statics::NewProp_bPassthrough,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonTabListWidgetBase_HandleNextTabInputAction_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/CommonTabListWidgetBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonTabListWidgetBase_HandleNextTabInputAction_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonTabListWidgetBase, nullptr, "HandleNextTabInputAction", nullptr, nullptr, sizeof(CommonTabListWidgetBase_eventHandleNextTabInputAction_Parms), Z_Construct_UFunction_UCommonTabListWidgetBase_HandleNextTabInputAction_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_HandleNextTabInputAction_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00480401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonTabListWidgetBase_HandleNextTabInputAction_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_HandleNextTabInputAction_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonTabListWidgetBase_HandleNextTabInputAction()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonTabListWidgetBase_HandleNextTabInputAction_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonTabListWidgetBase_HandlePostLinkedSwitcherChanged_BP_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonTabListWidgetBase_HandlePostLinkedSwitcherChanged_BP_Statics::Function_MetaDataParams[] = {
		{ "BlueprintProtected", "true" },
		{ "Category", "TabList" },
		{ "ModuleRelativePath", "Public/CommonTabListWidgetBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonTabListWidgetBase_HandlePostLinkedSwitcherChanged_BP_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonTabListWidgetBase, nullptr, "HandlePostLinkedSwitcherChanged_BP", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08080800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonTabListWidgetBase_HandlePostLinkedSwitcherChanged_BP_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_HandlePostLinkedSwitcherChanged_BP_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonTabListWidgetBase_HandlePostLinkedSwitcherChanged_BP()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonTabListWidgetBase_HandlePostLinkedSwitcherChanged_BP_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonTabListWidgetBase_HandlePreLinkedSwitcherChanged_BP_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonTabListWidgetBase_HandlePreLinkedSwitcherChanged_BP_Statics::Function_MetaDataParams[] = {
		{ "BlueprintProtected", "true" },
		{ "Category", "TabList" },
		{ "ModuleRelativePath", "Public/CommonTabListWidgetBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonTabListWidgetBase_HandlePreLinkedSwitcherChanged_BP_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonTabListWidgetBase, nullptr, "HandlePreLinkedSwitcherChanged_BP", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08080800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonTabListWidgetBase_HandlePreLinkedSwitcherChanged_BP_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_HandlePreLinkedSwitcherChanged_BP_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonTabListWidgetBase_HandlePreLinkedSwitcherChanged_BP()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonTabListWidgetBase_HandlePreLinkedSwitcherChanged_BP_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonTabListWidgetBase_HandlePreviousTabInputAction_Statics
	{
		struct CommonTabListWidgetBase_eventHandlePreviousTabInputAction_Parms
		{
			bool bPassthrough;
		};
		static void NewProp_bPassthrough_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPassthrough;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UCommonTabListWidgetBase_HandlePreviousTabInputAction_Statics::NewProp_bPassthrough_SetBit(void* Obj)
	{
		((CommonTabListWidgetBase_eventHandlePreviousTabInputAction_Parms*)Obj)->bPassthrough = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCommonTabListWidgetBase_HandlePreviousTabInputAction_Statics::NewProp_bPassthrough = { "bPassthrough", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CommonTabListWidgetBase_eventHandlePreviousTabInputAction_Parms), &Z_Construct_UFunction_UCommonTabListWidgetBase_HandlePreviousTabInputAction_Statics::NewProp_bPassthrough_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonTabListWidgetBase_HandlePreviousTabInputAction_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonTabListWidgetBase_HandlePreviousTabInputAction_Statics::NewProp_bPassthrough,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonTabListWidgetBase_HandlePreviousTabInputAction_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/CommonTabListWidgetBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonTabListWidgetBase_HandlePreviousTabInputAction_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonTabListWidgetBase, nullptr, "HandlePreviousTabInputAction", nullptr, nullptr, sizeof(CommonTabListWidgetBase_eventHandlePreviousTabInputAction_Parms), Z_Construct_UFunction_UCommonTabListWidgetBase_HandlePreviousTabInputAction_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_HandlePreviousTabInputAction_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00480401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonTabListWidgetBase_HandlePreviousTabInputAction_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_HandlePreviousTabInputAction_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonTabListWidgetBase_HandlePreviousTabInputAction()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonTabListWidgetBase_HandlePreviousTabInputAction_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabButtonSelected_Statics
	{
		struct CommonTabListWidgetBase_eventHandleTabButtonSelected_Parms
		{
			UCommonButtonBase* SelectedTabButton;
			int32 ButtonIndex;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SelectedTabButton_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SelectedTabButton;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ButtonIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabButtonSelected_Statics::NewProp_SelectedTabButton_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabButtonSelected_Statics::NewProp_SelectedTabButton = { "SelectedTabButton", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonTabListWidgetBase_eventHandleTabButtonSelected_Parms, SelectedTabButton), Z_Construct_UClass_UCommonButtonBase_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabButtonSelected_Statics::NewProp_SelectedTabButton_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabButtonSelected_Statics::NewProp_SelectedTabButton_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabButtonSelected_Statics::NewProp_ButtonIndex = { "ButtonIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonTabListWidgetBase_eventHandleTabButtonSelected_Parms, ButtonIndex), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabButtonSelected_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabButtonSelected_Statics::NewProp_SelectedTabButton,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabButtonSelected_Statics::NewProp_ButtonIndex,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabButtonSelected_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/CommonTabListWidgetBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabButtonSelected_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonTabListWidgetBase, nullptr, "HandleTabButtonSelected", nullptr, nullptr, sizeof(CommonTabListWidgetBase_eventHandleTabButtonSelected_Parms), Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabButtonSelected_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabButtonSelected_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabButtonSelected_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabButtonSelected_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabButtonSelected()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabButtonSelected_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabCreation_Statics
	{
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_TabNameID;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TabButton_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TabButton;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabCreation_Statics::NewProp_TabNameID = { "TabNameID", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonTabListWidgetBase_eventHandleTabCreation_Parms, TabNameID), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabCreation_Statics::NewProp_TabButton_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabCreation_Statics::NewProp_TabButton = { "TabButton", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonTabListWidgetBase_eventHandleTabCreation_Parms, TabButton), Z_Construct_UClass_UCommonButtonBase_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabCreation_Statics::NewProp_TabButton_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabCreation_Statics::NewProp_TabButton_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabCreation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabCreation_Statics::NewProp_TabNameID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabCreation_Statics::NewProp_TabButton,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabCreation_Statics::Function_MetaDataParams[] = {
		{ "BlueprintProtected", "true" },
		{ "Category", "TabList" },
		{ "ModuleRelativePath", "Public/CommonTabListWidgetBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabCreation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonTabListWidgetBase, nullptr, "HandleTabCreation", nullptr, nullptr, sizeof(CommonTabListWidgetBase_eventHandleTabCreation_Parms), Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabCreation_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabCreation_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08080C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabCreation_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabCreation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabCreation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabCreation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabRemoval_Statics
	{
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_TabNameID;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TabButton_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TabButton;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabRemoval_Statics::NewProp_TabNameID = { "TabNameID", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonTabListWidgetBase_eventHandleTabRemoval_Parms, TabNameID), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabRemoval_Statics::NewProp_TabButton_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabRemoval_Statics::NewProp_TabButton = { "TabButton", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonTabListWidgetBase_eventHandleTabRemoval_Parms, TabButton), Z_Construct_UClass_UCommonButtonBase_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabRemoval_Statics::NewProp_TabButton_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabRemoval_Statics::NewProp_TabButton_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabRemoval_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabRemoval_Statics::NewProp_TabNameID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabRemoval_Statics::NewProp_TabButton,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabRemoval_Statics::Function_MetaDataParams[] = {
		{ "BlueprintProtected", "true" },
		{ "Category", "TabList" },
		{ "ModuleRelativePath", "Public/CommonTabListWidgetBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabRemoval_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonTabListWidgetBase, nullptr, "HandleTabRemoval", nullptr, nullptr, sizeof(CommonTabListWidgetBase_eventHandleTabRemoval_Parms), Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabRemoval_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabRemoval_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08080C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabRemoval_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabRemoval_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabRemoval()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabRemoval_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonTabListWidgetBase_RegisterTab_Statics
	{
		struct CommonTabListWidgetBase_eventRegisterTab_Parms
		{
			FName TabNameID;
			TSubclassOf<UCommonButtonBase>  ButtonWidgetType;
			UWidget* ContentWidget;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_TabNameID;
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ButtonWidgetType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ContentWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ContentWidget;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UCommonTabListWidgetBase_RegisterTab_Statics::NewProp_TabNameID = { "TabNameID", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonTabListWidgetBase_eventRegisterTab_Parms, TabNameID), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UCommonTabListWidgetBase_RegisterTab_Statics::NewProp_ButtonWidgetType = { "ButtonWidgetType", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonTabListWidgetBase_eventRegisterTab_Parms, ButtonWidgetType), Z_Construct_UClass_UCommonButtonBase_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonTabListWidgetBase_RegisterTab_Statics::NewProp_ContentWidget_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCommonTabListWidgetBase_RegisterTab_Statics::NewProp_ContentWidget = { "ContentWidget", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonTabListWidgetBase_eventRegisterTab_Parms, ContentWidget), Z_Construct_UClass_UWidget_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UCommonTabListWidgetBase_RegisterTab_Statics::NewProp_ContentWidget_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_RegisterTab_Statics::NewProp_ContentWidget_MetaData)) };
	void Z_Construct_UFunction_UCommonTabListWidgetBase_RegisterTab_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((CommonTabListWidgetBase_eventRegisterTab_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCommonTabListWidgetBase_RegisterTab_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CommonTabListWidgetBase_eventRegisterTab_Parms), &Z_Construct_UFunction_UCommonTabListWidgetBase_RegisterTab_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonTabListWidgetBase_RegisterTab_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonTabListWidgetBase_RegisterTab_Statics::NewProp_TabNameID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonTabListWidgetBase_RegisterTab_Statics::NewProp_ButtonWidgetType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonTabListWidgetBase_RegisterTab_Statics::NewProp_ContentWidget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonTabListWidgetBase_RegisterTab_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonTabListWidgetBase_RegisterTab_Statics::Function_MetaDataParams[] = {
		{ "Category", "TabList" },
		{ "Comment", "/**\n\x09 * Registers and adds a new tab to the list that corresponds to a given widget instance. If not present in the linked switcher, it will be added.\n\x09 * @param TabID The name ID used to keep track of this tab. Attempts to register a tab under a duplicate ID will fail.\n\x09 * @param ButtonWidgetType The widget type to create for this tab\n\x09 * @param ContentWidget The widget to associate with the registered tab\n\x09 * @return True if the new tab registered successfully and there were no name ID conflicts\n\x09 */" },
		{ "ModuleRelativePath", "Public/CommonTabListWidgetBase.h" },
		{ "ToolTip", "Registers and adds a new tab to the list that corresponds to a given widget instance. If not present in the linked switcher, it will be added.\n@param TabID The name ID used to keep track of this tab. Attempts to register a tab under a duplicate ID will fail.\n@param ButtonWidgetType The widget type to create for this tab\n@param ContentWidget The widget to associate with the registered tab\n@return True if the new tab registered successfully and there were no name ID conflicts" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonTabListWidgetBase_RegisterTab_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonTabListWidgetBase, nullptr, "RegisterTab", nullptr, nullptr, sizeof(CommonTabListWidgetBase_eventRegisterTab_Parms), Z_Construct_UFunction_UCommonTabListWidgetBase_RegisterTab_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_RegisterTab_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonTabListWidgetBase_RegisterTab_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_RegisterTab_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonTabListWidgetBase_RegisterTab()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonTabListWidgetBase_RegisterTab_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonTabListWidgetBase_RemoveAllTabs_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonTabListWidgetBase_RemoveAllTabs_Statics::Function_MetaDataParams[] = {
		{ "Category", "TabList" },
		{ "ModuleRelativePath", "Public/CommonTabListWidgetBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonTabListWidgetBase_RemoveAllTabs_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonTabListWidgetBase, nullptr, "RemoveAllTabs", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonTabListWidgetBase_RemoveAllTabs_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_RemoveAllTabs_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonTabListWidgetBase_RemoveAllTabs()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonTabListWidgetBase_RemoveAllTabs_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonTabListWidgetBase_RemoveTab_Statics
	{
		struct CommonTabListWidgetBase_eventRemoveTab_Parms
		{
			FName TabNameID;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_TabNameID;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UCommonTabListWidgetBase_RemoveTab_Statics::NewProp_TabNameID = { "TabNameID", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonTabListWidgetBase_eventRemoveTab_Parms, TabNameID), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UCommonTabListWidgetBase_RemoveTab_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((CommonTabListWidgetBase_eventRemoveTab_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCommonTabListWidgetBase_RemoveTab_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CommonTabListWidgetBase_eventRemoveTab_Parms), &Z_Construct_UFunction_UCommonTabListWidgetBase_RemoveTab_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonTabListWidgetBase_RemoveTab_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonTabListWidgetBase_RemoveTab_Statics::NewProp_TabNameID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonTabListWidgetBase_RemoveTab_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonTabListWidgetBase_RemoveTab_Statics::Function_MetaDataParams[] = {
		{ "Category", "TabList" },
		{ "ModuleRelativePath", "Public/CommonTabListWidgetBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonTabListWidgetBase_RemoveTab_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonTabListWidgetBase, nullptr, "RemoveTab", nullptr, nullptr, sizeof(CommonTabListWidgetBase_eventRemoveTab_Parms), Z_Construct_UFunction_UCommonTabListWidgetBase_RemoveTab_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_RemoveTab_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonTabListWidgetBase_RemoveTab_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_RemoveTab_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonTabListWidgetBase_RemoveTab()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonTabListWidgetBase_RemoveTab_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonTabListWidgetBase_SelectTabByID_Statics
	{
		struct CommonTabListWidgetBase_eventSelectTabByID_Parms
		{
			FName TabNameID;
			bool bSuppressClickFeedback;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_TabNameID;
		static void NewProp_bSuppressClickFeedback_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSuppressClickFeedback;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UCommonTabListWidgetBase_SelectTabByID_Statics::NewProp_TabNameID = { "TabNameID", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonTabListWidgetBase_eventSelectTabByID_Parms, TabNameID), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UCommonTabListWidgetBase_SelectTabByID_Statics::NewProp_bSuppressClickFeedback_SetBit(void* Obj)
	{
		((CommonTabListWidgetBase_eventSelectTabByID_Parms*)Obj)->bSuppressClickFeedback = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCommonTabListWidgetBase_SelectTabByID_Statics::NewProp_bSuppressClickFeedback = { "bSuppressClickFeedback", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CommonTabListWidgetBase_eventSelectTabByID_Parms), &Z_Construct_UFunction_UCommonTabListWidgetBase_SelectTabByID_Statics::NewProp_bSuppressClickFeedback_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UCommonTabListWidgetBase_SelectTabByID_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((CommonTabListWidgetBase_eventSelectTabByID_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCommonTabListWidgetBase_SelectTabByID_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CommonTabListWidgetBase_eventSelectTabByID_Parms), &Z_Construct_UFunction_UCommonTabListWidgetBase_SelectTabByID_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonTabListWidgetBase_SelectTabByID_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonTabListWidgetBase_SelectTabByID_Statics::NewProp_TabNameID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonTabListWidgetBase_SelectTabByID_Statics::NewProp_bSuppressClickFeedback,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonTabListWidgetBase_SelectTabByID_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonTabListWidgetBase_SelectTabByID_Statics::Function_MetaDataParams[] = {
		{ "Category", "TabList" },
		{ "Comment", "/** \n\x09 * Selects the tab registered under the provided name ID\n\x09 * @param TabNameID The name ID for the tab given when registered\n\x09 */" },
		{ "CPP_Default_bSuppressClickFeedback", "false" },
		{ "ModuleRelativePath", "Public/CommonTabListWidgetBase.h" },
		{ "ToolTip", "Selects the tab registered under the provided name ID\n@param TabNameID The name ID for the tab given when registered" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonTabListWidgetBase_SelectTabByID_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonTabListWidgetBase, nullptr, "SelectTabByID", nullptr, nullptr, sizeof(CommonTabListWidgetBase_eventSelectTabByID_Parms), Z_Construct_UFunction_UCommonTabListWidgetBase_SelectTabByID_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_SelectTabByID_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonTabListWidgetBase_SelectTabByID_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_SelectTabByID_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonTabListWidgetBase_SelectTabByID()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonTabListWidgetBase_SelectTabByID_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonTabListWidgetBase_SetLinkedSwitcher_Statics
	{
		struct CommonTabListWidgetBase_eventSetLinkedSwitcher_Parms
		{
			UCommonAnimatedSwitcher* CommonSwitcher;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CommonSwitcher_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CommonSwitcher;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonTabListWidgetBase_SetLinkedSwitcher_Statics::NewProp_CommonSwitcher_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCommonTabListWidgetBase_SetLinkedSwitcher_Statics::NewProp_CommonSwitcher = { "CommonSwitcher", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonTabListWidgetBase_eventSetLinkedSwitcher_Parms, CommonSwitcher), Z_Construct_UClass_UCommonAnimatedSwitcher_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UCommonTabListWidgetBase_SetLinkedSwitcher_Statics::NewProp_CommonSwitcher_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_SetLinkedSwitcher_Statics::NewProp_CommonSwitcher_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonTabListWidgetBase_SetLinkedSwitcher_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonTabListWidgetBase_SetLinkedSwitcher_Statics::NewProp_CommonSwitcher,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonTabListWidgetBase_SetLinkedSwitcher_Statics::Function_MetaDataParams[] = {
		{ "Category", "TabList" },
		{ "Comment", "/**\n\x09 * Establishes the activatable widget switcher instance that this tab list should interact with\n\x09 * @param CommonSwitcher The switcher that this tab list should be associated with and manipulate\n\x09 */" },
		{ "ModuleRelativePath", "Public/CommonTabListWidgetBase.h" },
		{ "ToolTip", "Establishes the activatable widget switcher instance that this tab list should interact with\n@param CommonSwitcher The switcher that this tab list should be associated with and manipulate" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonTabListWidgetBase_SetLinkedSwitcher_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonTabListWidgetBase, nullptr, "SetLinkedSwitcher", nullptr, nullptr, sizeof(CommonTabListWidgetBase_eventSetLinkedSwitcher_Parms), Z_Construct_UFunction_UCommonTabListWidgetBase_SetLinkedSwitcher_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_SetLinkedSwitcher_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonTabListWidgetBase_SetLinkedSwitcher_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_SetLinkedSwitcher_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonTabListWidgetBase_SetLinkedSwitcher()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonTabListWidgetBase_SetLinkedSwitcher_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonTabListWidgetBase_SetListeningForInput_Statics
	{
		struct CommonTabListWidgetBase_eventSetListeningForInput_Parms
		{
			bool bShouldListen;
		};
		static void NewProp_bShouldListen_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShouldListen;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UCommonTabListWidgetBase_SetListeningForInput_Statics::NewProp_bShouldListen_SetBit(void* Obj)
	{
		((CommonTabListWidgetBase_eventSetListeningForInput_Parms*)Obj)->bShouldListen = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCommonTabListWidgetBase_SetListeningForInput_Statics::NewProp_bShouldListen = { "bShouldListen", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CommonTabListWidgetBase_eventSetListeningForInput_Parms), &Z_Construct_UFunction_UCommonTabListWidgetBase_SetListeningForInput_Statics::NewProp_bShouldListen_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonTabListWidgetBase_SetListeningForInput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonTabListWidgetBase_SetListeningForInput_Statics::NewProp_bShouldListen,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonTabListWidgetBase_SetListeningForInput_Statics::Function_MetaDataParams[] = {
		{ "Category", "TabList" },
		{ "ModuleRelativePath", "Public/CommonTabListWidgetBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonTabListWidgetBase_SetListeningForInput_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonTabListWidgetBase, nullptr, "SetListeningForInput", nullptr, nullptr, sizeof(CommonTabListWidgetBase_eventSetListeningForInput_Parms), Z_Construct_UFunction_UCommonTabListWidgetBase_SetListeningForInput_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_SetListeningForInput_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonTabListWidgetBase_SetListeningForInput_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_SetListeningForInput_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonTabListWidgetBase_SetListeningForInput()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonTabListWidgetBase_SetListeningForInput_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabEnabled_Statics
	{
		struct CommonTabListWidgetBase_eventSetTabEnabled_Parms
		{
			FName TabNameID;
			bool bEnable;
		};
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_TabNameID;
		static void NewProp_bEnable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnable;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabEnabled_Statics::NewProp_TabNameID = { "TabNameID", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonTabListWidgetBase_eventSetTabEnabled_Parms, TabNameID), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabEnabled_Statics::NewProp_bEnable_SetBit(void* Obj)
	{
		((CommonTabListWidgetBase_eventSetTabEnabled_Parms*)Obj)->bEnable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabEnabled_Statics::NewProp_bEnable = { "bEnable", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CommonTabListWidgetBase_eventSetTabEnabled_Parms), &Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabEnabled_Statics::NewProp_bEnable_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabEnabled_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabEnabled_Statics::NewProp_TabNameID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabEnabled_Statics::NewProp_bEnable,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabEnabled_Statics::Function_MetaDataParams[] = {
		{ "Category", "TabList" },
		{ "Comment", "/** Sets whether the tab associated with the given ID is enabled/disabled */" },
		{ "ModuleRelativePath", "Public/CommonTabListWidgetBase.h" },
		{ "ToolTip", "Sets whether the tab associated with the given ID is enabled/disabled" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabEnabled_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonTabListWidgetBase, nullptr, "SetTabEnabled", nullptr, nullptr, sizeof(CommonTabListWidgetBase_eventSetTabEnabled_Parms), Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabEnabled_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabEnabled_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabEnabled_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabEnabled_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabEnabled()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabEnabled_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabInteractionEnabled_Statics
	{
		struct CommonTabListWidgetBase_eventSetTabInteractionEnabled_Parms
		{
			FName TabNameID;
			bool bEnable;
		};
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_TabNameID;
		static void NewProp_bEnable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnable;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabInteractionEnabled_Statics::NewProp_TabNameID = { "TabNameID", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonTabListWidgetBase_eventSetTabInteractionEnabled_Parms, TabNameID), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabInteractionEnabled_Statics::NewProp_bEnable_SetBit(void* Obj)
	{
		((CommonTabListWidgetBase_eventSetTabInteractionEnabled_Parms*)Obj)->bEnable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabInteractionEnabled_Statics::NewProp_bEnable = { "bEnable", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CommonTabListWidgetBase_eventSetTabInteractionEnabled_Parms), &Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabInteractionEnabled_Statics::NewProp_bEnable_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabInteractionEnabled_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabInteractionEnabled_Statics::NewProp_TabNameID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabInteractionEnabled_Statics::NewProp_bEnable,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabInteractionEnabled_Statics::Function_MetaDataParams[] = {
		{ "Category", "TabList" },
		{ "Comment", "/** Sets whether the tab associated with the given ID is interactable */" },
		{ "ModuleRelativePath", "Public/CommonTabListWidgetBase.h" },
		{ "ToolTip", "Sets whether the tab associated with the given ID is interactable" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabInteractionEnabled_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonTabListWidgetBase, nullptr, "SetTabInteractionEnabled", nullptr, nullptr, sizeof(CommonTabListWidgetBase_eventSetTabInteractionEnabled_Parms), Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabInteractionEnabled_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabInteractionEnabled_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabInteractionEnabled_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabInteractionEnabled_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabInteractionEnabled()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabInteractionEnabled_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabVisibility_Statics
	{
		struct CommonTabListWidgetBase_eventSetTabVisibility_Parms
		{
			FName TabNameID;
			ESlateVisibility NewVisibility;
		};
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_TabNameID;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_NewVisibility_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_NewVisibility;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabVisibility_Statics::NewProp_TabNameID = { "TabNameID", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonTabListWidgetBase_eventSetTabVisibility_Parms, TabNameID), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabVisibility_Statics::NewProp_NewVisibility_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabVisibility_Statics::NewProp_NewVisibility = { "NewVisibility", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonTabListWidgetBase_eventSetTabVisibility_Parms, NewVisibility), Z_Construct_UEnum_UMG_ESlateVisibility, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabVisibility_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabVisibility_Statics::NewProp_TabNameID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabVisibility_Statics::NewProp_NewVisibility_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabVisibility_Statics::NewProp_NewVisibility,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabVisibility_Statics::Function_MetaDataParams[] = {
		{ "Category", "TabList" },
		{ "Comment", "/** Sets the visibility of the tab associated with the given ID  */" },
		{ "ModuleRelativePath", "Public/CommonTabListWidgetBase.h" },
		{ "ToolTip", "Sets the visibility of the tab associated with the given ID" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabVisibility_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonTabListWidgetBase, nullptr, "SetTabVisibility", nullptr, nullptr, sizeof(CommonTabListWidgetBase_eventSetTabVisibility_Parms), Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabVisibility_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabVisibility_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabVisibility_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabVisibility_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabVisibility()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabVisibility_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UCommonTabListWidgetBase_NoRegister()
	{
		return UCommonTabListWidgetBase::StaticClass();
	}
	struct Z_Construct_UClass_UCommonTabListWidgetBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnTabSelected_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnTabSelected;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnTabButtonCreation_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnTabButtonCreation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnTabButtonRemoval_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnTabButtonRemoval;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NextTabInputActionData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NextTabInputActionData;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviousTabInputActionData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PreviousTabInputActionData;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoListenForInput_MetaData[];
#endif
		static void NewProp_bAutoListenForInput_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoListenForInput;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LinkedSwitcher_MetaData[];
#endif
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_LinkedSwitcher;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TabButtonGroup_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TabButtonGroup;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RegisteredTabsByID_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_RegisteredTabsByID_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RegisteredTabsByID_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_RegisteredTabsByID;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonTabListWidgetBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCommonUserWidget,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonUI,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCommonTabListWidgetBase_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCommonTabListWidgetBase_DisableTabWithReason, "DisableTabWithReason" }, // 1826734453
		{ &Z_Construct_UFunction_UCommonTabListWidgetBase_GetActiveTab, "GetActiveTab" }, // 2874861564
		{ &Z_Construct_UFunction_UCommonTabListWidgetBase_GetLinkedSwitcher, "GetLinkedSwitcher" }, // 1800998754
		{ &Z_Construct_UFunction_UCommonTabListWidgetBase_GetSelectedTabId, "GetSelectedTabId" }, // 2046876524
		{ &Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabButtonBaseByID, "GetTabButtonBaseByID" }, // 2240339745
		{ &Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabCount, "GetTabCount" }, // 2876498700
		{ &Z_Construct_UFunction_UCommonTabListWidgetBase_GetTabIdAtIndex, "GetTabIdAtIndex" }, // 2986357294
		{ &Z_Construct_UFunction_UCommonTabListWidgetBase_HandleNextTabInputAction, "HandleNextTabInputAction" }, // 4037093448
		{ &Z_Construct_UFunction_UCommonTabListWidgetBase_HandlePostLinkedSwitcherChanged_BP, "HandlePostLinkedSwitcherChanged_BP" }, // 3897301522
		{ &Z_Construct_UFunction_UCommonTabListWidgetBase_HandlePreLinkedSwitcherChanged_BP, "HandlePreLinkedSwitcherChanged_BP" }, // 626939103
		{ &Z_Construct_UFunction_UCommonTabListWidgetBase_HandlePreviousTabInputAction, "HandlePreviousTabInputAction" }, // 1975578341
		{ &Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabButtonSelected, "HandleTabButtonSelected" }, // 2796937402
		{ &Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabCreation, "HandleTabCreation" }, // 859184327
		{ &Z_Construct_UFunction_UCommonTabListWidgetBase_HandleTabRemoval, "HandleTabRemoval" }, // 3926609800
		{ &Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabButtonCreation__DelegateSignature, "OnTabButtonCreation__DelegateSignature" }, // 1125521630
		{ &Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabButtonRemoval__DelegateSignature, "OnTabButtonRemoval__DelegateSignature" }, // 1967975715
		{ &Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabSelected__DelegateSignature, "OnTabSelected__DelegateSignature" }, // 3636430807
		{ &Z_Construct_UFunction_UCommonTabListWidgetBase_RegisterTab, "RegisterTab" }, // 3383107757
		{ &Z_Construct_UFunction_UCommonTabListWidgetBase_RemoveAllTabs, "RemoveAllTabs" }, // 1688112967
		{ &Z_Construct_UFunction_UCommonTabListWidgetBase_RemoveTab, "RemoveTab" }, // 2194670816
		{ &Z_Construct_UFunction_UCommonTabListWidgetBase_SelectTabByID, "SelectTabByID" }, // 3690139042
		{ &Z_Construct_UFunction_UCommonTabListWidgetBase_SetLinkedSwitcher, "SetLinkedSwitcher" }, // 497573723
		{ &Z_Construct_UFunction_UCommonTabListWidgetBase_SetListeningForInput, "SetListeningForInput" }, // 1181272705
		{ &Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabEnabled, "SetTabEnabled" }, // 2976283533
		{ &Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabInteractionEnabled, "SetTabInteractionEnabled" }, // 3307836596
		{ &Z_Construct_UFunction_UCommonTabListWidgetBase_SetTabVisibility, "SetTabVisibility" }, // 2988148882
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonTabListWidgetBase_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Category", "Common UI" },
		{ "ClassGroupNames", "UI" },
		{ "Comment", "/** Base class for a list of selectable tabs that correspondingly activate and display an arbitrary widget in a linked switcher */" },
		{ "DisableNativeTick", "" },
		{ "IncludePath", "CommonTabListWidgetBase.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/CommonTabListWidgetBase.h" },
		{ "ToolTip", "Base class for a list of selectable tabs that correspondingly activate and display an arbitrary widget in a linked switcher" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_OnTabSelected_MetaData[] = {
		{ "Category", "TabList" },
		{ "Comment", "/** Broadcasts when a new tab is selected. */" },
		{ "ModuleRelativePath", "Public/CommonTabListWidgetBase.h" },
		{ "ToolTip", "Broadcasts when a new tab is selected." },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_OnTabSelected = { "OnTabSelected", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonTabListWidgetBase, OnTabSelected), Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabSelected__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_OnTabSelected_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_OnTabSelected_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_OnTabButtonCreation_MetaData[] = {
		{ "Category", "TabList" },
		{ "Comment", "/** Broadcasts when a new tab is created. */" },
		{ "ModuleRelativePath", "Public/CommonTabListWidgetBase.h" },
		{ "ToolTip", "Broadcasts when a new tab is created." },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_OnTabButtonCreation = { "OnTabButtonCreation", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonTabListWidgetBase, OnTabButtonCreation), Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabButtonCreation__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_OnTabButtonCreation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_OnTabButtonCreation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_OnTabButtonRemoval_MetaData[] = {
		{ "Category", "TabList" },
		{ "Comment", "/** Broadcasts when a new tab is created. */" },
		{ "ModuleRelativePath", "Public/CommonTabListWidgetBase.h" },
		{ "ToolTip", "Broadcasts when a new tab is created." },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_OnTabButtonRemoval = { "OnTabButtonRemoval", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonTabListWidgetBase, OnTabButtonRemoval), Z_Construct_UDelegateFunction_UCommonTabListWidgetBase_OnTabButtonRemoval__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_OnTabButtonRemoval_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_OnTabButtonRemoval_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_NextTabInputActionData_MetaData[] = {
		{ "Category", "TabList" },
		{ "Comment", "/** The input action to listen for causing the next tab to be selected */" },
		{ "ModuleRelativePath", "Public/CommonTabListWidgetBase.h" },
		{ "RowType", "CommonInputActionDataBase" },
		{ "ToolTip", "The input action to listen for causing the next tab to be selected" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_NextTabInputActionData = { "NextTabInputActionData", nullptr, (EPropertyFlags)0x0020080000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonTabListWidgetBase, NextTabInputActionData), Z_Construct_UScriptStruct_FDataTableRowHandle, METADATA_PARAMS(Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_NextTabInputActionData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_NextTabInputActionData_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_PreviousTabInputActionData_MetaData[] = {
		{ "Category", "TabList" },
		{ "Comment", "/** The input action to listen for causing the previous tab to be selected */" },
		{ "ModuleRelativePath", "Public/CommonTabListWidgetBase.h" },
		{ "RowType", "CommonInputActionDataBase" },
		{ "ToolTip", "The input action to listen for causing the previous tab to be selected" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_PreviousTabInputActionData = { "PreviousTabInputActionData", nullptr, (EPropertyFlags)0x0020080000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonTabListWidgetBase, PreviousTabInputActionData), Z_Construct_UScriptStruct_FDataTableRowHandle, METADATA_PARAMS(Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_PreviousTabInputActionData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_PreviousTabInputActionData_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_bAutoListenForInput_MetaData[] = {
		{ "Category", "TabList" },
		{ "Comment", "/** Whether to register to handle tab list input immediately upon construction */" },
		{ "ExposeOnSpawn", "true" },
		{ "ModuleRelativePath", "Public/CommonTabListWidgetBase.h" },
		{ "ToolTip", "Whether to register to handle tab list input immediately upon construction" },
	};
#endif
	void Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_bAutoListenForInput_SetBit(void* Obj)
	{
		((UCommonTabListWidgetBase*)Obj)->bAutoListenForInput = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_bAutoListenForInput = { "bAutoListenForInput", nullptr, (EPropertyFlags)0x0021080000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCommonTabListWidgetBase), &Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_bAutoListenForInput_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_bAutoListenForInput_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_bAutoListenForInput_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_LinkedSwitcher_MetaData[] = {
		{ "Comment", "/** The activatable widget switcher that this tab list is associated with and manipulates */" },
		{ "ModuleRelativePath", "Public/CommonTabListWidgetBase.h" },
		{ "ToolTip", "The activatable widget switcher that this tab list is associated with and manipulates" },
	};
#endif
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_LinkedSwitcher = { "LinkedSwitcher", nullptr, (EPropertyFlags)0x0024080000082008, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonTabListWidgetBase, LinkedSwitcher), Z_Construct_UClass_UCommonAnimatedSwitcher_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_LinkedSwitcher_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_LinkedSwitcher_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_TabButtonGroup_MetaData[] = {
		{ "Comment", "/** The button group that manages all the created tab buttons */" },
		{ "ModuleRelativePath", "Public/CommonTabListWidgetBase.h" },
		{ "ToolTip", "The button group that manages all the created tab buttons" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_TabButtonGroup = { "TabButtonGroup", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonTabListWidgetBase, TabButtonGroup), Z_Construct_UClass_UCommonButtonGroupBase_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_TabButtonGroup_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_TabButtonGroup_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_RegisteredTabsByID_ValueProp = { "RegisteredTabsByID", nullptr, (EPropertyFlags)0x0000008000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FCommonRegisteredTabInfo, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_RegisteredTabsByID_Key_KeyProp = { "RegisteredTabsByID_Key", nullptr, (EPropertyFlags)0x0000008000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_RegisteredTabsByID_MetaData[] = {
		{ "Comment", "/** Info about each of the currently registered tabs organized by a given registration name ID */" },
		{ "ModuleRelativePath", "Public/CommonTabListWidgetBase.h" },
		{ "ToolTip", "Info about each of the currently registered tabs organized by a given registration name ID" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_RegisteredTabsByID = { "RegisteredTabsByID", nullptr, (EPropertyFlags)0x0040008000002000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonTabListWidgetBase, RegisteredTabsByID), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_RegisteredTabsByID_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_RegisteredTabsByID_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCommonTabListWidgetBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_OnTabSelected,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_OnTabButtonCreation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_OnTabButtonRemoval,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_NextTabInputActionData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_PreviousTabInputActionData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_bAutoListenForInput,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_LinkedSwitcher,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_TabButtonGroup,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_RegisteredTabsByID_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_RegisteredTabsByID_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonTabListWidgetBase_Statics::NewProp_RegisteredTabsByID,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonTabListWidgetBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonTabListWidgetBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCommonTabListWidgetBase_Statics::ClassParams = {
		&UCommonTabListWidgetBase::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UCommonTabListWidgetBase_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UCommonTabListWidgetBase_Statics::PropPointers),
		0,
		0x00B010A1u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonTabListWidgetBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonTabListWidgetBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonTabListWidgetBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCommonTabListWidgetBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCommonTabListWidgetBase, 660191742);
	template<> COMMONUI_API UClass* StaticClass<UCommonTabListWidgetBase>()
	{
		return UCommonTabListWidgetBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCommonTabListWidgetBase(Z_Construct_UClass_UCommonTabListWidgetBase, &UCommonTabListWidgetBase::StaticClass, TEXT("/Script/CommonUI"), TEXT("UCommonTabListWidgetBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonTabListWidgetBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
