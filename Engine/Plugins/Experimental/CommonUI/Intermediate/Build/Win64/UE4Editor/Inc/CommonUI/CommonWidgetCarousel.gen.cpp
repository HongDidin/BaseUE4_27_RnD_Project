// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonUI/Public/CommonWidgetCarousel.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCommonWidgetCarousel() {}
// Cross Module References
	COMMONUI_API UFunction* Z_Construct_UDelegateFunction_CommonUI_OnCurrentPageIndexChanged__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_CommonUI();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonWidgetCarousel_NoRegister();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonWidgetCarousel();
	UMG_API UClass* Z_Construct_UClass_UPanelWidget();
	UMG_API UClass* Z_Construct_UClass_UWidget_NoRegister();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_CommonUI_OnCurrentPageIndexChanged__DelegateSignature_Statics
	{
		struct _Script_CommonUI_eventOnCurrentPageIndexChanged_Parms
		{
			UCommonWidgetCarousel* CarouselWidget;
			int32 CurrentPageIndex;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CarouselWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CarouselWidget;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_CurrentPageIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_CommonUI_OnCurrentPageIndexChanged__DelegateSignature_Statics::NewProp_CarouselWidget_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_CommonUI_OnCurrentPageIndexChanged__DelegateSignature_Statics::NewProp_CarouselWidget = { "CarouselWidget", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_CommonUI_eventOnCurrentPageIndexChanged_Parms, CarouselWidget), Z_Construct_UClass_UCommonWidgetCarousel_NoRegister, METADATA_PARAMS(Z_Construct_UDelegateFunction_CommonUI_OnCurrentPageIndexChanged__DelegateSignature_Statics::NewProp_CarouselWidget_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_CommonUI_OnCurrentPageIndexChanged__DelegateSignature_Statics::NewProp_CarouselWidget_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UDelegateFunction_CommonUI_OnCurrentPageIndexChanged__DelegateSignature_Statics::NewProp_CurrentPageIndex = { "CurrentPageIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_CommonUI_eventOnCurrentPageIndexChanged_Parms, CurrentPageIndex), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_CommonUI_OnCurrentPageIndexChanged__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_CommonUI_OnCurrentPageIndexChanged__DelegateSignature_Statics::NewProp_CarouselWidget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_CommonUI_OnCurrentPageIndexChanged__DelegateSignature_Statics::NewProp_CurrentPageIndex,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_CommonUI_OnCurrentPageIndexChanged__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/CommonWidgetCarousel.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_CommonUI_OnCurrentPageIndexChanged__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_CommonUI, nullptr, "OnCurrentPageIndexChanged__DelegateSignature", nullptr, nullptr, sizeof(_Script_CommonUI_eventOnCurrentPageIndexChanged_Parms), Z_Construct_UDelegateFunction_CommonUI_OnCurrentPageIndexChanged__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_CommonUI_OnCurrentPageIndexChanged__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_CommonUI_OnCurrentPageIndexChanged__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_CommonUI_OnCurrentPageIndexChanged__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_CommonUI_OnCurrentPageIndexChanged__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_CommonUI_OnCurrentPageIndexChanged__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(UCommonWidgetCarousel::execPreviousPage)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->PreviousPage();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonWidgetCarousel::execNextPage)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->NextPage();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonWidgetCarousel::execEndAutoScrolling)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->EndAutoScrolling();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonWidgetCarousel::execBeginAutoScrolling)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_ScrollInterval);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->BeginAutoScrolling(Z_Param_ScrollInterval);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonWidgetCarousel::execGetWidgetAtIndex)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_Index);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UWidget**)Z_Param__Result=P_THIS->GetWidgetAtIndex(Z_Param_Index);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonWidgetCarousel::execSetActiveWidget)
	{
		P_GET_OBJECT(UWidget,Z_Param_Widget);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetActiveWidget(Z_Param_Widget);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonWidgetCarousel::execSetActiveWidgetIndex)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_Index);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetActiveWidgetIndex(Z_Param_Index);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonWidgetCarousel::execGetActiveWidgetIndex)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetActiveWidgetIndex();
		P_NATIVE_END;
	}
	void UCommonWidgetCarousel::StaticRegisterNativesUCommonWidgetCarousel()
	{
		UClass* Class = UCommonWidgetCarousel::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "BeginAutoScrolling", &UCommonWidgetCarousel::execBeginAutoScrolling },
			{ "EndAutoScrolling", &UCommonWidgetCarousel::execEndAutoScrolling },
			{ "GetActiveWidgetIndex", &UCommonWidgetCarousel::execGetActiveWidgetIndex },
			{ "GetWidgetAtIndex", &UCommonWidgetCarousel::execGetWidgetAtIndex },
			{ "NextPage", &UCommonWidgetCarousel::execNextPage },
			{ "PreviousPage", &UCommonWidgetCarousel::execPreviousPage },
			{ "SetActiveWidget", &UCommonWidgetCarousel::execSetActiveWidget },
			{ "SetActiveWidgetIndex", &UCommonWidgetCarousel::execSetActiveWidgetIndex },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCommonWidgetCarousel_BeginAutoScrolling_Statics
	{
		struct CommonWidgetCarousel_eventBeginAutoScrolling_Parms
		{
			float ScrollInterval;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScrollInterval;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UCommonWidgetCarousel_BeginAutoScrolling_Statics::NewProp_ScrollInterval = { "ScrollInterval", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonWidgetCarousel_eventBeginAutoScrolling_Parms, ScrollInterval), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonWidgetCarousel_BeginAutoScrolling_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonWidgetCarousel_BeginAutoScrolling_Statics::NewProp_ScrollInterval,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonWidgetCarousel_BeginAutoScrolling_Statics::Function_MetaDataParams[] = {
		{ "Category", "Carousel" },
		{ "CPP_Default_ScrollInterval", "10.000000" },
		{ "ModuleRelativePath", "Public/CommonWidgetCarousel.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonWidgetCarousel_BeginAutoScrolling_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonWidgetCarousel, nullptr, "BeginAutoScrolling", nullptr, nullptr, sizeof(CommonWidgetCarousel_eventBeginAutoScrolling_Parms), Z_Construct_UFunction_UCommonWidgetCarousel_BeginAutoScrolling_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonWidgetCarousel_BeginAutoScrolling_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonWidgetCarousel_BeginAutoScrolling_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonWidgetCarousel_BeginAutoScrolling_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonWidgetCarousel_BeginAutoScrolling()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonWidgetCarousel_BeginAutoScrolling_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonWidgetCarousel_EndAutoScrolling_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonWidgetCarousel_EndAutoScrolling_Statics::Function_MetaDataParams[] = {
		{ "Category", "Carousel" },
		{ "ModuleRelativePath", "Public/CommonWidgetCarousel.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonWidgetCarousel_EndAutoScrolling_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonWidgetCarousel, nullptr, "EndAutoScrolling", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonWidgetCarousel_EndAutoScrolling_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonWidgetCarousel_EndAutoScrolling_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonWidgetCarousel_EndAutoScrolling()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonWidgetCarousel_EndAutoScrolling_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonWidgetCarousel_GetActiveWidgetIndex_Statics
	{
		struct CommonWidgetCarousel_eventGetActiveWidgetIndex_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UCommonWidgetCarousel_GetActiveWidgetIndex_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonWidgetCarousel_eventGetActiveWidgetIndex_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonWidgetCarousel_GetActiveWidgetIndex_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonWidgetCarousel_GetActiveWidgetIndex_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonWidgetCarousel_GetActiveWidgetIndex_Statics::Function_MetaDataParams[] = {
		{ "Category", "Carousel" },
		{ "Comment", "/** Gets the slot index of the currently active widget */" },
		{ "ModuleRelativePath", "Public/CommonWidgetCarousel.h" },
		{ "ToolTip", "Gets the slot index of the currently active widget" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonWidgetCarousel_GetActiveWidgetIndex_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonWidgetCarousel, nullptr, "GetActiveWidgetIndex", nullptr, nullptr, sizeof(CommonWidgetCarousel_eventGetActiveWidgetIndex_Parms), Z_Construct_UFunction_UCommonWidgetCarousel_GetActiveWidgetIndex_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonWidgetCarousel_GetActiveWidgetIndex_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonWidgetCarousel_GetActiveWidgetIndex_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonWidgetCarousel_GetActiveWidgetIndex_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonWidgetCarousel_GetActiveWidgetIndex()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonWidgetCarousel_GetActiveWidgetIndex_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonWidgetCarousel_GetWidgetAtIndex_Statics
	{
		struct CommonWidgetCarousel_eventGetWidgetAtIndex_Parms
		{
			int32 Index;
			UWidget* ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UCommonWidgetCarousel_GetWidgetAtIndex_Statics::NewProp_Index = { "Index", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonWidgetCarousel_eventGetWidgetAtIndex_Parms, Index), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonWidgetCarousel_GetWidgetAtIndex_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCommonWidgetCarousel_GetWidgetAtIndex_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonWidgetCarousel_eventGetWidgetAtIndex_Parms, ReturnValue), Z_Construct_UClass_UWidget_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UCommonWidgetCarousel_GetWidgetAtIndex_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonWidgetCarousel_GetWidgetAtIndex_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonWidgetCarousel_GetWidgetAtIndex_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonWidgetCarousel_GetWidgetAtIndex_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonWidgetCarousel_GetWidgetAtIndex_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonWidgetCarousel_GetWidgetAtIndex_Statics::Function_MetaDataParams[] = {
		{ "Category", "Carousel" },
		{ "Comment", "/** Get a widget at the provided index */" },
		{ "ModuleRelativePath", "Public/CommonWidgetCarousel.h" },
		{ "ToolTip", "Get a widget at the provided index" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonWidgetCarousel_GetWidgetAtIndex_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonWidgetCarousel, nullptr, "GetWidgetAtIndex", nullptr, nullptr, sizeof(CommonWidgetCarousel_eventGetWidgetAtIndex_Parms), Z_Construct_UFunction_UCommonWidgetCarousel_GetWidgetAtIndex_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonWidgetCarousel_GetWidgetAtIndex_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonWidgetCarousel_GetWidgetAtIndex_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonWidgetCarousel_GetWidgetAtIndex_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonWidgetCarousel_GetWidgetAtIndex()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonWidgetCarousel_GetWidgetAtIndex_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonWidgetCarousel_NextPage_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonWidgetCarousel_NextPage_Statics::Function_MetaDataParams[] = {
		{ "Category", "Carousel" },
		{ "ModuleRelativePath", "Public/CommonWidgetCarousel.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonWidgetCarousel_NextPage_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonWidgetCarousel, nullptr, "NextPage", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonWidgetCarousel_NextPage_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonWidgetCarousel_NextPage_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonWidgetCarousel_NextPage()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonWidgetCarousel_NextPage_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonWidgetCarousel_PreviousPage_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonWidgetCarousel_PreviousPage_Statics::Function_MetaDataParams[] = {
		{ "Category", "Carousel" },
		{ "ModuleRelativePath", "Public/CommonWidgetCarousel.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonWidgetCarousel_PreviousPage_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonWidgetCarousel, nullptr, "PreviousPage", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonWidgetCarousel_PreviousPage_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonWidgetCarousel_PreviousPage_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonWidgetCarousel_PreviousPage()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonWidgetCarousel_PreviousPage_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonWidgetCarousel_SetActiveWidget_Statics
	{
		struct CommonWidgetCarousel_eventSetActiveWidget_Parms
		{
			UWidget* Widget;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Widget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Widget;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonWidgetCarousel_SetActiveWidget_Statics::NewProp_Widget_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCommonWidgetCarousel_SetActiveWidget_Statics::NewProp_Widget = { "Widget", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonWidgetCarousel_eventSetActiveWidget_Parms, Widget), Z_Construct_UClass_UWidget_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UCommonWidgetCarousel_SetActiveWidget_Statics::NewProp_Widget_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonWidgetCarousel_SetActiveWidget_Statics::NewProp_Widget_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonWidgetCarousel_SetActiveWidget_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonWidgetCarousel_SetActiveWidget_Statics::NewProp_Widget,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonWidgetCarousel_SetActiveWidget_Statics::Function_MetaDataParams[] = {
		{ "Category", "Carousel" },
		{ "Comment", "/** Activates the widget and makes it the active index. */" },
		{ "ModuleRelativePath", "Public/CommonWidgetCarousel.h" },
		{ "ToolTip", "Activates the widget and makes it the active index." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonWidgetCarousel_SetActiveWidget_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonWidgetCarousel, nullptr, "SetActiveWidget", nullptr, nullptr, sizeof(CommonWidgetCarousel_eventSetActiveWidget_Parms), Z_Construct_UFunction_UCommonWidgetCarousel_SetActiveWidget_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonWidgetCarousel_SetActiveWidget_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonWidgetCarousel_SetActiveWidget_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonWidgetCarousel_SetActiveWidget_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonWidgetCarousel_SetActiveWidget()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonWidgetCarousel_SetActiveWidget_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonWidgetCarousel_SetActiveWidgetIndex_Statics
	{
		struct CommonWidgetCarousel_eventSetActiveWidgetIndex_Parms
		{
			int32 Index;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UCommonWidgetCarousel_SetActiveWidgetIndex_Statics::NewProp_Index = { "Index", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonWidgetCarousel_eventSetActiveWidgetIndex_Parms, Index), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonWidgetCarousel_SetActiveWidgetIndex_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonWidgetCarousel_SetActiveWidgetIndex_Statics::NewProp_Index,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonWidgetCarousel_SetActiveWidgetIndex_Statics::Function_MetaDataParams[] = {
		{ "Category", "Carousel" },
		{ "Comment", "/** Activates the widget at the specified index. */" },
		{ "ModuleRelativePath", "Public/CommonWidgetCarousel.h" },
		{ "ToolTip", "Activates the widget at the specified index." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonWidgetCarousel_SetActiveWidgetIndex_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonWidgetCarousel, nullptr, "SetActiveWidgetIndex", nullptr, nullptr, sizeof(CommonWidgetCarousel_eventSetActiveWidgetIndex_Parms), Z_Construct_UFunction_UCommonWidgetCarousel_SetActiveWidgetIndex_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonWidgetCarousel_SetActiveWidgetIndex_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonWidgetCarousel_SetActiveWidgetIndex_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonWidgetCarousel_SetActiveWidgetIndex_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonWidgetCarousel_SetActiveWidgetIndex()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonWidgetCarousel_SetActiveWidgetIndex_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UCommonWidgetCarousel_NoRegister()
	{
		return UCommonWidgetCarousel::StaticClass();
	}
	struct Z_Construct_UClass_UCommonWidgetCarousel_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActiveWidgetIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ActiveWidgetIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnCurrentPageIndexChanged_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnCurrentPageIndexChanged;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonWidgetCarousel_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UPanelWidget,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonUI,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCommonWidgetCarousel_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCommonWidgetCarousel_BeginAutoScrolling, "BeginAutoScrolling" }, // 3495516215
		{ &Z_Construct_UFunction_UCommonWidgetCarousel_EndAutoScrolling, "EndAutoScrolling" }, // 2422779349
		{ &Z_Construct_UFunction_UCommonWidgetCarousel_GetActiveWidgetIndex, "GetActiveWidgetIndex" }, // 880301344
		{ &Z_Construct_UFunction_UCommonWidgetCarousel_GetWidgetAtIndex, "GetWidgetAtIndex" }, // 231952798
		{ &Z_Construct_UFunction_UCommonWidgetCarousel_NextPage, "NextPage" }, // 3863481384
		{ &Z_Construct_UFunction_UCommonWidgetCarousel_PreviousPage, "PreviousPage" }, // 2105496646
		{ &Z_Construct_UFunction_UCommonWidgetCarousel_SetActiveWidget, "SetActiveWidget" }, // 2807097728
		{ &Z_Construct_UFunction_UCommonWidgetCarousel_SetActiveWidgetIndex, "SetActiveWidgetIndex" }, // 4230869890
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonWidgetCarousel_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * A widget switcher is like a tab control, but without tabs. At most one widget is visible at time.\n */" },
		{ "IncludePath", "CommonWidgetCarousel.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/CommonWidgetCarousel.h" },
		{ "ToolTip", "A widget switcher is like a tab control, but without tabs. At most one widget is visible at time." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonWidgetCarousel_Statics::NewProp_ActiveWidgetIndex_MetaData[] = {
		{ "Category", "Carousel" },
		{ "ClampMin", "0" },
		{ "Comment", "/** The slot index to display */" },
		{ "ModuleRelativePath", "Public/CommonWidgetCarousel.h" },
		{ "ToolTip", "The slot index to display" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UCommonWidgetCarousel_Statics::NewProp_ActiveWidgetIndex = { "ActiveWidgetIndex", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonWidgetCarousel, ActiveWidgetIndex), METADATA_PARAMS(Z_Construct_UClass_UCommonWidgetCarousel_Statics::NewProp_ActiveWidgetIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonWidgetCarousel_Statics::NewProp_ActiveWidgetIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonWidgetCarousel_Statics::NewProp_OnCurrentPageIndexChanged_MetaData[] = {
		{ "Category", "Carousel" },
		{ "ModuleRelativePath", "Public/CommonWidgetCarousel.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UCommonWidgetCarousel_Statics::NewProp_OnCurrentPageIndexChanged = { "OnCurrentPageIndexChanged", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonWidgetCarousel, OnCurrentPageIndexChanged), Z_Construct_UDelegateFunction_CommonUI_OnCurrentPageIndexChanged__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UCommonWidgetCarousel_Statics::NewProp_OnCurrentPageIndexChanged_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonWidgetCarousel_Statics::NewProp_OnCurrentPageIndexChanged_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCommonWidgetCarousel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonWidgetCarousel_Statics::NewProp_ActiveWidgetIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonWidgetCarousel_Statics::NewProp_OnCurrentPageIndexChanged,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonWidgetCarousel_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonWidgetCarousel>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCommonWidgetCarousel_Statics::ClassParams = {
		&UCommonWidgetCarousel::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UCommonWidgetCarousel_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UCommonWidgetCarousel_Statics::PropPointers),
		0,
		0x00B000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonWidgetCarousel_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonWidgetCarousel_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonWidgetCarousel()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCommonWidgetCarousel_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCommonWidgetCarousel, 326332120);
	template<> COMMONUI_API UClass* StaticClass<UCommonWidgetCarousel>()
	{
		return UCommonWidgetCarousel::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCommonWidgetCarousel(Z_Construct_UClass_UCommonWidgetCarousel, &UCommonWidgetCarousel::StaticClass, TEXT("/Script/CommonUI"), TEXT("UCommonWidgetCarousel"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonWidgetCarousel);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
