// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonUI/Public/CommonUserWidget.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCommonUserWidget() {}
// Cross Module References
	COMMONUI_API UClass* Z_Construct_UClass_UCommonUserWidget_NoRegister();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonUserWidget();
	UMG_API UClass* Z_Construct_UClass_UUserWidget();
	UPackage* Z_Construct_UPackage__Script_CommonUI();
// End Cross Module References
	DEFINE_FUNCTION(UCommonUserWidget::execSetConsumePointerInput)
	{
		P_GET_UBOOL(Z_Param_bInConsumePointerInput);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetConsumePointerInput(Z_Param_bInConsumePointerInput);
		P_NATIVE_END;
	}
	void UCommonUserWidget::StaticRegisterNativesUCommonUserWidget()
	{
		UClass* Class = UCommonUserWidget::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "SetConsumePointerInput", &UCommonUserWidget::execSetConsumePointerInput },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCommonUserWidget_SetConsumePointerInput_Statics
	{
		struct CommonUserWidget_eventSetConsumePointerInput_Parms
		{
			bool bInConsumePointerInput;
		};
		static void NewProp_bInConsumePointerInput_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInConsumePointerInput;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UCommonUserWidget_SetConsumePointerInput_Statics::NewProp_bInConsumePointerInput_SetBit(void* Obj)
	{
		((CommonUserWidget_eventSetConsumePointerInput_Parms*)Obj)->bInConsumePointerInput = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCommonUserWidget_SetConsumePointerInput_Statics::NewProp_bInConsumePointerInput = { "bInConsumePointerInput", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CommonUserWidget_eventSetConsumePointerInput_Parms), &Z_Construct_UFunction_UCommonUserWidget_SetConsumePointerInput_Statics::NewProp_bInConsumePointerInput_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonUserWidget_SetConsumePointerInput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonUserWidget_SetConsumePointerInput_Statics::NewProp_bInConsumePointerInput,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonUserWidget_SetConsumePointerInput_Statics::Function_MetaDataParams[] = {
		{ "Category", "CommonUserWidget" },
		{ "Comment", "/** Sets whether or not this widget will consume ALL pointer input that reaches it */" },
		{ "ModuleRelativePath", "Public/CommonUserWidget.h" },
		{ "ToolTip", "Sets whether or not this widget will consume ALL pointer input that reaches it" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonUserWidget_SetConsumePointerInput_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonUserWidget, nullptr, "SetConsumePointerInput", nullptr, nullptr, sizeof(CommonUserWidget_eventSetConsumePointerInput_Parms), Z_Construct_UFunction_UCommonUserWidget_SetConsumePointerInput_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonUserWidget_SetConsumePointerInput_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonUserWidget_SetConsumePointerInput_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonUserWidget_SetConsumePointerInput_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonUserWidget_SetConsumePointerInput()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonUserWidget_SetConsumePointerInput_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UCommonUserWidget_NoRegister()
	{
		return UCommonUserWidget::StaticClass();
	}
	struct Z_Construct_UClass_UCommonUserWidget_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bConsumePointerInput_MetaData[];
#endif
		static void NewProp_bConsumePointerInput_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bConsumePointerInput;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonUserWidget_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UUserWidget,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonUI,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCommonUserWidget_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCommonUserWidget_SetConsumePointerInput, "SetConsumePointerInput" }, // 1241064741
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonUserWidget_Statics::Class_MetaDataParams[] = {
		{ "Category", "Common UI" },
		{ "ClassGroupNames", "UI" },
		{ "DisableNativeTick", "" },
		{ "IncludePath", "CommonUserWidget.h" },
		{ "ModuleRelativePath", "Public/CommonUserWidget.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonUserWidget_Statics::NewProp_bConsumePointerInput_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Input" },
		{ "Comment", "/** Set this to true if you don't want any pointer (mouse and touch) input to bubble past this widget */" },
		{ "ModuleRelativePath", "Public/CommonUserWidget.h" },
		{ "ToolTip", "Set this to true if you don't want any pointer (mouse and touch) input to bubble past this widget" },
	};
#endif
	void Z_Construct_UClass_UCommonUserWidget_Statics::NewProp_bConsumePointerInput_SetBit(void* Obj)
	{
		((UCommonUserWidget*)Obj)->bConsumePointerInput = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCommonUserWidget_Statics::NewProp_bConsumePointerInput = { "bConsumePointerInput", nullptr, (EPropertyFlags)0x0040000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCommonUserWidget), &Z_Construct_UClass_UCommonUserWidget_Statics::NewProp_bConsumePointerInput_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCommonUserWidget_Statics::NewProp_bConsumePointerInput_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonUserWidget_Statics::NewProp_bConsumePointerInput_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCommonUserWidget_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonUserWidget_Statics::NewProp_bConsumePointerInput,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonUserWidget_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonUserWidget>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCommonUserWidget_Statics::ClassParams = {
		&UCommonUserWidget::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UCommonUserWidget_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UCommonUserWidget_Statics::PropPointers),
		0,
		0x00B010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonUserWidget_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonUserWidget_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonUserWidget()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCommonUserWidget_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCommonUserWidget, 1408715271);
	template<> COMMONUI_API UClass* StaticClass<UCommonUserWidget>()
	{
		return UCommonUserWidget::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCommonUserWidget(Z_Construct_UClass_UCommonUserWidget, &UCommonUserWidget::StaticClass, TEXT("/Script/CommonUI"), TEXT("UCommonUserWidget"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonUserWidget);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
