// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FSlateBrush;
struct FLinearColor;
struct FVector2D;
struct FMargin;
struct FSlateFontInfo;
class UCommonTextStyle;
#ifdef COMMONUI_CommonTextBlock_generated_h
#error "CommonTextBlock.generated.h already included, missing '#pragma once' in CommonTextBlock.h"
#endif
#define COMMONUI_CommonTextBlock_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_17_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetStrikeBrush); \
	DECLARE_FUNCTION(execGetShadowColor); \
	DECLARE_FUNCTION(execGetShadowOffset); \
	DECLARE_FUNCTION(execGetLineHeightPercentage); \
	DECLARE_FUNCTION(execGetMargin); \
	DECLARE_FUNCTION(execGetColor); \
	DECLARE_FUNCTION(execGetFont);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetStrikeBrush); \
	DECLARE_FUNCTION(execGetShadowColor); \
	DECLARE_FUNCTION(execGetShadowOffset); \
	DECLARE_FUNCTION(execGetLineHeightPercentage); \
	DECLARE_FUNCTION(execGetMargin); \
	DECLARE_FUNCTION(execGetColor); \
	DECLARE_FUNCTION(execGetFont);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonTextStyle(); \
	friend struct Z_Construct_UClass_UCommonTextStyle_Statics; \
public: \
	DECLARE_CLASS(UCommonTextStyle, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonTextStyle)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUCommonTextStyle(); \
	friend struct Z_Construct_UClass_UCommonTextStyle_Statics; \
public: \
	DECLARE_CLASS(UCommonTextStyle, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonTextStyle)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonTextStyle(const FObjectInitializer& ObjectInitializer); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonTextStyle) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonTextStyle); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonTextStyle); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonTextStyle(UCommonTextStyle&&); \
	NO_API UCommonTextStyle(const UCommonTextStyle&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonTextStyle(UCommonTextStyle&&); \
	NO_API UCommonTextStyle(const UCommonTextStyle&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonTextStyle); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonTextStyle); \
	DEFINE_ABSTRACT_DEFAULT_CONSTRUCTOR_CALL(UCommonTextStyle)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_14_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_17_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_17_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonTextStyle>();

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_87_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_87_RPC_WRAPPERS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_87_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_87_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonTextScrollStyle(); \
	friend struct Z_Construct_UClass_UCommonTextScrollStyle_Statics; \
public: \
	DECLARE_CLASS(UCommonTextScrollStyle, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonTextScrollStyle)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_87_INCLASS \
private: \
	static void StaticRegisterNativesUCommonTextScrollStyle(); \
	friend struct Z_Construct_UClass_UCommonTextScrollStyle_Statics; \
public: \
	DECLARE_CLASS(UCommonTextScrollStyle, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonTextScrollStyle)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_87_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonTextScrollStyle(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonTextScrollStyle) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonTextScrollStyle); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonTextScrollStyle); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonTextScrollStyle(UCommonTextScrollStyle&&); \
	NO_API UCommonTextScrollStyle(const UCommonTextScrollStyle&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_87_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonTextScrollStyle(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonTextScrollStyle(UCommonTextScrollStyle&&); \
	NO_API UCommonTextScrollStyle(const UCommonTextScrollStyle&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonTextScrollStyle); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonTextScrollStyle); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonTextScrollStyle)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_87_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_84_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_87_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_87_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_87_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_87_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_87_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_87_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_87_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_87_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_87_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_87_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_87_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_87_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonTextScrollStyle>();

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_160_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_160_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execResetScrollState); \
	DECLARE_FUNCTION(execSetStyle); \
	DECLARE_FUNCTION(execSetTextCase); \
	DECLARE_FUNCTION(execSetWrapTextWidth);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_160_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execResetScrollState); \
	DECLARE_FUNCTION(execSetStyle); \
	DECLARE_FUNCTION(execSetTextCase); \
	DECLARE_FUNCTION(execSetWrapTextWidth);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_160_ARCHIVESERIALIZER \
	DECLARE_FSTRUCTUREDARCHIVE_SERIALIZER(UCommonTextBlock, NO_API)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_160_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonTextBlock(); \
	friend struct Z_Construct_UClass_UCommonTextBlock_Statics; \
public: \
	DECLARE_CLASS(UCommonTextBlock, UTextBlock, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonTextBlock) \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_160_ARCHIVESERIALIZER \
	static const TCHAR* StaticConfigName() {return TEXT("CommonUI");} \



#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_160_INCLASS \
private: \
	static void StaticRegisterNativesUCommonTextBlock(); \
	friend struct Z_Construct_UClass_UCommonTextBlock_Statics; \
public: \
	DECLARE_CLASS(UCommonTextBlock, UTextBlock, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonTextBlock) \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_160_ARCHIVESERIALIZER \
	static const TCHAR* StaticConfigName() {return TEXT("CommonUI");} \



#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_160_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonTextBlock(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonTextBlock) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonTextBlock); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonTextBlock); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonTextBlock(UCommonTextBlock&&); \
	NO_API UCommonTextBlock(const UCommonTextBlock&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_160_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonTextBlock(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonTextBlock(UCommonTextBlock&&); \
	NO_API UCommonTextBlock(const UCommonTextBlock&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonTextBlock); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonTextBlock); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonTextBlock)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_160_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Style() { return STRUCT_OFFSET(UCommonTextBlock, Style); } \
	FORCEINLINE static uint32 __PPO__ScrollStyle() { return STRUCT_OFFSET(UCommonTextBlock, ScrollStyle); } \
	FORCEINLINE static uint32 __PPO__bDisplayAllCaps_DEPRECATED() { return STRUCT_OFFSET(UCommonTextBlock, bDisplayAllCaps_DEPRECATED); } \
	FORCEINLINE static uint32 __PPO__bAutoCollapseWithEmptyText() { return STRUCT_OFFSET(UCommonTextBlock, bAutoCollapseWithEmptyText); } \
	FORCEINLINE static uint32 __PPO__MobileFontSizeMultiplier() { return STRUCT_OFFSET(UCommonTextBlock, MobileFontSizeMultiplier); }


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_157_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_160_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_160_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_160_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_160_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_160_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_160_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_160_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_160_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_160_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_160_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_160_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h_160_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class CommonTextBlock."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonTextBlock>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTextBlock_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
