// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMMONUI_CommonTreeView_generated_h
#error "CommonTreeView.generated.h already included, missing '#pragma once' in CommonTreeView.h"
#endif
#define COMMONUI_CommonTreeView_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTreeView_h_87_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTreeView_h_87_RPC_WRAPPERS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTreeView_h_87_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTreeView_h_87_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonTreeView(); \
	friend struct Z_Construct_UClass_UCommonTreeView_Statics; \
public: \
	DECLARE_CLASS(UCommonTreeView, UTreeView, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonTreeView)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTreeView_h_87_INCLASS \
private: \
	static void StaticRegisterNativesUCommonTreeView(); \
	friend struct Z_Construct_UClass_UCommonTreeView_Statics; \
public: \
	DECLARE_CLASS(UCommonTreeView, UTreeView, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonTreeView)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTreeView_h_87_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonTreeView(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonTreeView) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonTreeView); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonTreeView); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonTreeView(UCommonTreeView&&); \
	NO_API UCommonTreeView(const UCommonTreeView&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTreeView_h_87_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonTreeView(UCommonTreeView&&); \
	NO_API UCommonTreeView(const UCommonTreeView&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonTreeView); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonTreeView); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonTreeView)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTreeView_h_87_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTreeView_h_84_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTreeView_h_87_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTreeView_h_87_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTreeView_h_87_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTreeView_h_87_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTreeView_h_87_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTreeView_h_87_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTreeView_h_87_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTreeView_h_87_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTreeView_h_87_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTreeView_h_87_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTreeView_h_87_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTreeView_h_87_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonTreeView>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTreeView_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
