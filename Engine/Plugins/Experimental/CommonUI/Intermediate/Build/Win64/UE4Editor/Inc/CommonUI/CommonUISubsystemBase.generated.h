// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FDataTableRowHandle;
enum class ECommonInputType : uint8;
struct FSlateBrush;
#ifdef COMMONUI_CommonUISubsystemBase_generated_h
#error "CommonUISubsystemBase.generated.h already included, missing '#pragma once' in CommonUISubsystemBase.h"
#endif
#define COMMONUI_CommonUISubsystemBase_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISubsystemBase_h_18_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISubsystemBase_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetInputActionButtonIcon);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISubsystemBase_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetInputActionButtonIcon);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISubsystemBase_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonUISubsystemBase(); \
	friend struct Z_Construct_UClass_UCommonUISubsystemBase_Statics; \
public: \
	DECLARE_CLASS(UCommonUISubsystemBase, UGameInstanceSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonUISubsystemBase)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISubsystemBase_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUCommonUISubsystemBase(); \
	friend struct Z_Construct_UClass_UCommonUISubsystemBase_Statics; \
public: \
	DECLARE_CLASS(UCommonUISubsystemBase, UGameInstanceSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonUISubsystemBase)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISubsystemBase_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonUISubsystemBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonUISubsystemBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonUISubsystemBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonUISubsystemBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonUISubsystemBase(UCommonUISubsystemBase&&); \
	NO_API UCommonUISubsystemBase(const UCommonUISubsystemBase&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISubsystemBase_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonUISubsystemBase() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonUISubsystemBase(UCommonUISubsystemBase&&); \
	NO_API UCommonUISubsystemBase(const UCommonUISubsystemBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonUISubsystemBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonUISubsystemBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UCommonUISubsystemBase)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISubsystemBase_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISubsystemBase_h_15_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISubsystemBase_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISubsystemBase_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISubsystemBase_h_18_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISubsystemBase_h_18_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISubsystemBase_h_18_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISubsystemBase_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISubsystemBase_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISubsystemBase_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISubsystemBase_h_18_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISubsystemBase_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISubsystemBase_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISubsystemBase_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonUISubsystemBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISubsystemBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
