// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMMONUI_CommonUserWidget_generated_h
#error "CommonUserWidget.generated.h already included, missing '#pragma once' in CommonUserWidget.h"
#endif
#define COMMONUI_CommonUserWidget_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUserWidget_h_21_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUserWidget_h_21_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetConsumePointerInput);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUserWidget_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetConsumePointerInput);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUserWidget_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonUserWidget(); \
	friend struct Z_Construct_UClass_UCommonUserWidget_Statics; \
public: \
	DECLARE_CLASS(UCommonUserWidget, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonUserWidget)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUserWidget_h_21_INCLASS \
private: \
	static void StaticRegisterNativesUCommonUserWidget(); \
	friend struct Z_Construct_UClass_UCommonUserWidget_Statics; \
public: \
	DECLARE_CLASS(UCommonUserWidget, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonUserWidget)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUserWidget_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonUserWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonUserWidget) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonUserWidget); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonUserWidget); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonUserWidget(UCommonUserWidget&&); \
	NO_API UCommonUserWidget(const UCommonUserWidget&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUserWidget_h_21_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonUserWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonUserWidget(UCommonUserWidget&&); \
	NO_API UCommonUserWidget(const UCommonUserWidget&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonUserWidget); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonUserWidget); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonUserWidget)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUserWidget_h_21_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bConsumePointerInput() { return STRUCT_OFFSET(UCommonUserWidget, bConsumePointerInput); }


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUserWidget_h_18_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUserWidget_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUserWidget_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUserWidget_h_21_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUserWidget_h_21_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUserWidget_h_21_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUserWidget_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUserWidget_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUserWidget_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUserWidget_h_21_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUserWidget_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUserWidget_h_21_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUserWidget_h_21_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class CommonUserWidget."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonUserWidget>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUserWidget_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
