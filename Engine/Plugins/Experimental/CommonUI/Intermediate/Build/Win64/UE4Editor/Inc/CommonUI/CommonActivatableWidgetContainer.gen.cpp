// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonUI/Public/Widgets/CommonActivatableWidgetContainer.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCommonActivatableWidgetContainer() {}
// Cross Module References
	COMMONUI_API UClass* Z_Construct_UClass_UCommonActivatableWidgetContainerBase_NoRegister();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonActivatableWidgetContainerBase();
	UMG_API UClass* Z_Construct_UClass_UWidget();
	UPackage* Z_Construct_UPackage__Script_CommonUI();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonActivatableWidget_NoRegister();
	COMMONUI_API UEnum* Z_Construct_UEnum_CommonUI_ECommonSwitcherTransition();
	COMMONUI_API UEnum* Z_Construct_UEnum_CommonUI_ETransitionCurve();
	UMG_API UScriptStruct* Z_Construct_UScriptStruct_FUserWidgetPool();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonActivatableWidgetStack_NoRegister();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonActivatableWidgetStack();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonActivatableWidgetQueue_NoRegister();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonActivatableWidgetQueue();
// End Cross Module References
	DEFINE_FUNCTION(UCommonActivatableWidgetContainerBase::execRemoveWidget)
	{
		P_GET_OBJECT(UCommonActivatableWidget,Z_Param_WidgetToRemove);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->RemoveWidget(Z_Param_WidgetToRemove);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonActivatableWidgetContainerBase::execBP_AddWidget)
	{
		P_GET_OBJECT(UClass,Z_Param_ActivatableWidgetClass);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UCommonActivatableWidget**)Z_Param__Result=P_THIS->BP_AddWidget(Z_Param_ActivatableWidgetClass);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonActivatableWidgetContainerBase::execClearWidgets)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ClearWidgets();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonActivatableWidgetContainerBase::execGetActiveWidget)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UCommonActivatableWidget**)Z_Param__Result=P_THIS->GetActiveWidget();
		P_NATIVE_END;
	}
	void UCommonActivatableWidgetContainerBase::StaticRegisterNativesUCommonActivatableWidgetContainerBase()
	{
		UClass* Class = UCommonActivatableWidgetContainerBase::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "BP_AddWidget", &UCommonActivatableWidgetContainerBase::execBP_AddWidget },
			{ "ClearWidgets", &UCommonActivatableWidgetContainerBase::execClearWidgets },
			{ "GetActiveWidget", &UCommonActivatableWidgetContainerBase::execGetActiveWidget },
			{ "RemoveWidget", &UCommonActivatableWidgetContainerBase::execRemoveWidget },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_BP_AddWidget_Statics
	{
		struct CommonActivatableWidgetContainerBase_eventBP_AddWidget_Parms
		{
			TSubclassOf<UCommonActivatableWidget>  ActivatableWidgetClass;
			UCommonActivatableWidget* ReturnValue;
		};
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ActivatableWidgetClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_BP_AddWidget_Statics::NewProp_ActivatableWidgetClass = { "ActivatableWidgetClass", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonActivatableWidgetContainerBase_eventBP_AddWidget_Parms, ActivatableWidgetClass), Z_Construct_UClass_UCommonActivatableWidget_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_BP_AddWidget_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_BP_AddWidget_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonActivatableWidgetContainerBase_eventBP_AddWidget_Parms, ReturnValue), Z_Construct_UClass_UCommonActivatableWidget_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_BP_AddWidget_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_BP_AddWidget_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_BP_AddWidget_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_BP_AddWidget_Statics::NewProp_ActivatableWidgetClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_BP_AddWidget_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_BP_AddWidget_Statics::Function_MetaDataParams[] = {
		{ "Category", "ActivatableWidgetStack" },
		{ "Comment", "/** \n\x09 * Adds a widget of the given class to the container. \n\x09 * Note that all widgets added to the container are pooled, so the caller should not try to cache and re-use the created widget.\n\x09 * \n\x09 * It is possible for multiple instances of the same class to be added to the container at once, so any instance created in the past\n\x09 * is not guaranteed to be the one returned this time.\n\x09 *\n\x09 * So in practice, you should not trust that any prior state has been retained on the returned widget, and establish all appropriate properties every time.\n\x09 */" },
		{ "DeterminesOutputType", "ActivatableWidgetClass" },
		{ "DisplayName", "Push Widget" },
		{ "ModuleRelativePath", "Public/Widgets/CommonActivatableWidgetContainer.h" },
		{ "ToolTip", "Adds a widget of the given class to the container.\nNote that all widgets added to the container are pooled, so the caller should not try to cache and re-use the created widget.\n\nIt is possible for multiple instances of the same class to be added to the container at once, so any instance created in the past\nis not guaranteed to be the one returned this time.\n\nSo in practice, you should not trust that any prior state has been retained on the returned widget, and establish all appropriate properties every time." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_BP_AddWidget_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonActivatableWidgetContainerBase, nullptr, "BP_AddWidget", nullptr, nullptr, sizeof(CommonActivatableWidgetContainerBase_eventBP_AddWidget_Parms), Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_BP_AddWidget_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_BP_AddWidget_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04040401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_BP_AddWidget_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_BP_AddWidget_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_BP_AddWidget()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_BP_AddWidget_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_ClearWidgets_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_ClearWidgets_Statics::Function_MetaDataParams[] = {
		{ "Category", "ActivatableWidgetContainer" },
		{ "ModuleRelativePath", "Public/Widgets/CommonActivatableWidgetContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_ClearWidgets_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonActivatableWidgetContainerBase, nullptr, "ClearWidgets", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_ClearWidgets_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_ClearWidgets_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_ClearWidgets()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_ClearWidgets_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_GetActiveWidget_Statics
	{
		struct CommonActivatableWidgetContainerBase_eventGetActiveWidget_Parms
		{
			UCommonActivatableWidget* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_GetActiveWidget_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_GetActiveWidget_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonActivatableWidgetContainerBase_eventGetActiveWidget_Parms, ReturnValue), Z_Construct_UClass_UCommonActivatableWidget_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_GetActiveWidget_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_GetActiveWidget_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_GetActiveWidget_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_GetActiveWidget_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_GetActiveWidget_Statics::Function_MetaDataParams[] = {
		{ "Category", "ActivatableWidgetStack" },
		{ "ModuleRelativePath", "Public/Widgets/CommonActivatableWidgetContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_GetActiveWidget_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonActivatableWidgetContainerBase, nullptr, "GetActiveWidget", nullptr, nullptr, sizeof(CommonActivatableWidgetContainerBase_eventGetActiveWidget_Parms), Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_GetActiveWidget_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_GetActiveWidget_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_GetActiveWidget_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_GetActiveWidget_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_GetActiveWidget()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_GetActiveWidget_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_RemoveWidget_Statics
	{
		struct CommonActivatableWidgetContainerBase_eventRemoveWidget_Parms
		{
			UCommonActivatableWidget* WidgetToRemove;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WidgetToRemove_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WidgetToRemove;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_RemoveWidget_Statics::NewProp_WidgetToRemove_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_RemoveWidget_Statics::NewProp_WidgetToRemove = { "WidgetToRemove", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonActivatableWidgetContainerBase_eventRemoveWidget_Parms, WidgetToRemove), Z_Construct_UClass_UCommonActivatableWidget_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_RemoveWidget_Statics::NewProp_WidgetToRemove_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_RemoveWidget_Statics::NewProp_WidgetToRemove_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_RemoveWidget_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_RemoveWidget_Statics::NewProp_WidgetToRemove,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_RemoveWidget_Statics::Function_MetaDataParams[] = {
		{ "Category", "ActivatableWidgetContainer" },
		{ "ModuleRelativePath", "Public/Widgets/CommonActivatableWidgetContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_RemoveWidget_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonActivatableWidgetContainerBase, nullptr, "RemoveWidget", nullptr, nullptr, sizeof(CommonActivatableWidgetContainerBase_eventRemoveWidget_Parms), Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_RemoveWidget_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_RemoveWidget_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04040401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_RemoveWidget_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_RemoveWidget_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_RemoveWidget()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_RemoveWidget_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UCommonActivatableWidgetContainerBase_NoRegister()
	{
		return UCommonActivatableWidgetContainerBase::StaticClass();
	}
	struct Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TransitionType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransitionType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TransitionType;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TransitionCurveType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransitionCurveType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TransitionCurveType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransitionDuration_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TransitionDuration;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WidgetList_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WidgetList_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_WidgetList;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisplayedWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DisplayedWidget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GeneratedWidgetsPool_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GeneratedWidgetsPool;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UWidget,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonUI,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_BP_AddWidget, "BP_AddWidget" }, // 2942906053
		{ &Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_ClearWidgets, "ClearWidgets" }, // 4102537750
		{ &Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_GetActiveWidget, "GetActiveWidget" }, // 1679463117
		{ &Z_Construct_UFunction_UCommonActivatableWidgetContainerBase_RemoveWidget, "RemoveWidget" }, // 838357940
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** \n * Base of widgets built to manage N activatable widgets, displaying one at a time.\n * Intentionally meant to be black boxes that do not expose child/slot modification like a normal panel widget.\n */" },
		{ "IncludePath", "Widgets/CommonActivatableWidgetContainer.h" },
		{ "ModuleRelativePath", "Public/Widgets/CommonActivatableWidgetContainer.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Base of widgets built to manage N activatable widgets, displaying one at a time.\nIntentionally meant to be black boxes that do not expose child/slot modification like a normal panel widget." },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::NewProp_TransitionType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::NewProp_TransitionType_MetaData[] = {
		{ "Category", "Transition" },
		{ "Comment", "/** The type of transition to play between widgets */" },
		{ "ModuleRelativePath", "Public/Widgets/CommonActivatableWidgetContainer.h" },
		{ "ToolTip", "The type of transition to play between widgets" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::NewProp_TransitionType = { "TransitionType", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonActivatableWidgetContainerBase, TransitionType), Z_Construct_UEnum_CommonUI_ECommonSwitcherTransition, METADATA_PARAMS(Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::NewProp_TransitionType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::NewProp_TransitionType_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::NewProp_TransitionCurveType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::NewProp_TransitionCurveType_MetaData[] = {
		{ "Category", "Transition" },
		{ "Comment", "/** The curve function type to apply to the transition animation */" },
		{ "ModuleRelativePath", "Public/Widgets/CommonActivatableWidgetContainer.h" },
		{ "ToolTip", "The curve function type to apply to the transition animation" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::NewProp_TransitionCurveType = { "TransitionCurveType", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonActivatableWidgetContainerBase, TransitionCurveType), Z_Construct_UEnum_CommonUI_ETransitionCurve, METADATA_PARAMS(Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::NewProp_TransitionCurveType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::NewProp_TransitionCurveType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::NewProp_TransitionDuration_MetaData[] = {
		{ "Category", "Transition" },
		{ "Comment", "/** The total duration of a single transition between widgets */" },
		{ "ModuleRelativePath", "Public/Widgets/CommonActivatableWidgetContainer.h" },
		{ "ToolTip", "The total duration of a single transition between widgets" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::NewProp_TransitionDuration = { "TransitionDuration", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonActivatableWidgetContainerBase, TransitionDuration), METADATA_PARAMS(Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::NewProp_TransitionDuration_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::NewProp_TransitionDuration_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::NewProp_WidgetList_Inner = { "WidgetList", nullptr, (EPropertyFlags)0x0000000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UCommonActivatableWidget_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::NewProp_WidgetList_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Widgets/CommonActivatableWidgetContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::NewProp_WidgetList = { "WidgetList", nullptr, (EPropertyFlags)0x0020088000002008, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonActivatableWidgetContainerBase, WidgetList), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::NewProp_WidgetList_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::NewProp_WidgetList_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::NewProp_DisplayedWidget_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Widgets/CommonActivatableWidgetContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::NewProp_DisplayedWidget = { "DisplayedWidget", nullptr, (EPropertyFlags)0x0020080000082008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonActivatableWidgetContainerBase, DisplayedWidget), Z_Construct_UClass_UCommonActivatableWidget_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::NewProp_DisplayedWidget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::NewProp_DisplayedWidget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::NewProp_GeneratedWidgetsPool_MetaData[] = {
		{ "ModuleRelativePath", "Public/Widgets/CommonActivatableWidgetContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::NewProp_GeneratedWidgetsPool = { "GeneratedWidgetsPool", nullptr, (EPropertyFlags)0x0020088000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonActivatableWidgetContainerBase, GeneratedWidgetsPool), Z_Construct_UScriptStruct_FUserWidgetPool, METADATA_PARAMS(Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::NewProp_GeneratedWidgetsPool_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::NewProp_GeneratedWidgetsPool_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::NewProp_TransitionType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::NewProp_TransitionType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::NewProp_TransitionCurveType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::NewProp_TransitionCurveType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::NewProp_TransitionDuration,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::NewProp_WidgetList_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::NewProp_WidgetList,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::NewProp_DisplayedWidget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::NewProp_GeneratedWidgetsPool,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonActivatableWidgetContainerBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::ClassParams = {
		&UCommonActivatableWidgetContainerBase::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::PropPointers),
		0,
		0x00B000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonActivatableWidgetContainerBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCommonActivatableWidgetContainerBase, 4092509174);
	template<> COMMONUI_API UClass* StaticClass<UCommonActivatableWidgetContainerBase>()
	{
		return UCommonActivatableWidgetContainerBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCommonActivatableWidgetContainerBase(Z_Construct_UClass_UCommonActivatableWidgetContainerBase, &UCommonActivatableWidgetContainerBase::StaticClass, TEXT("/Script/CommonUI"), TEXT("UCommonActivatableWidgetContainerBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonActivatableWidgetContainerBase);
	void UCommonActivatableWidgetStack::StaticRegisterNativesUCommonActivatableWidgetStack()
	{
	}
	UClass* Z_Construct_UClass_UCommonActivatableWidgetStack_NoRegister()
	{
		return UCommonActivatableWidgetStack::StaticClass();
	}
	struct Z_Construct_UClass_UCommonActivatableWidgetStack_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RootContentWidgetClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_RootContentWidgetClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RootContentWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RootContentWidget;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonActivatableWidgetStack_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCommonActivatableWidgetContainerBase,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonUI,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonActivatableWidgetStack_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** \n * A display stack of ActivatableWidget elements. \n * \n * - Only the widget at the top of the stack is displayed and activated. All others are deactivated.\n * - When that top-most displayed widget deactivates, it's automatically removed and the preceding entry is displayed/activated.\n * - If RootContent is provided, it can never be removed regardless of activation state\n */" },
		{ "IncludePath", "Widgets/CommonActivatableWidgetContainer.h" },
		{ "ModuleRelativePath", "Public/Widgets/CommonActivatableWidgetContainer.h" },
		{ "ToolTip", "A display stack of ActivatableWidget elements.\n\n- Only the widget at the top of the stack is displayed and activated. All others are deactivated.\n- When that top-most displayed widget deactivates, it's automatically removed and the preceding entry is displayed/activated.\n- If RootContent is provided, it can never be removed regardless of activation state" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonActivatableWidgetStack_Statics::NewProp_RootContentWidgetClass_MetaData[] = {
		{ "Category", "Content" },
		{ "Comment", "/** Optional widget to auto-generate as the permanent root element of the stack */" },
		{ "ModuleRelativePath", "Public/Widgets/CommonActivatableWidgetContainer.h" },
		{ "ToolTip", "Optional widget to auto-generate as the permanent root element of the stack" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UCommonActivatableWidgetStack_Statics::NewProp_RootContentWidgetClass = { "RootContentWidgetClass", nullptr, (EPropertyFlags)0x0044000000000001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonActivatableWidgetStack, RootContentWidgetClass), Z_Construct_UClass_UCommonActivatableWidget_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_UCommonActivatableWidgetStack_Statics::NewProp_RootContentWidgetClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonActivatableWidgetStack_Statics::NewProp_RootContentWidgetClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonActivatableWidgetStack_Statics::NewProp_RootContentWidget_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Widgets/CommonActivatableWidgetContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UCommonActivatableWidgetStack_Statics::NewProp_RootContentWidget = { "RootContentWidget", nullptr, (EPropertyFlags)0x0040000000082008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonActivatableWidgetStack, RootContentWidget), Z_Construct_UClass_UCommonActivatableWidget_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCommonActivatableWidgetStack_Statics::NewProp_RootContentWidget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonActivatableWidgetStack_Statics::NewProp_RootContentWidget_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCommonActivatableWidgetStack_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonActivatableWidgetStack_Statics::NewProp_RootContentWidgetClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonActivatableWidgetStack_Statics::NewProp_RootContentWidget,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonActivatableWidgetStack_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonActivatableWidgetStack>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCommonActivatableWidgetStack_Statics::ClassParams = {
		&UCommonActivatableWidgetStack::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UCommonActivatableWidgetStack_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UCommonActivatableWidgetStack_Statics::PropPointers),
		0,
		0x00B000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonActivatableWidgetStack_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonActivatableWidgetStack_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonActivatableWidgetStack()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCommonActivatableWidgetStack_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCommonActivatableWidgetStack, 2852955569);
	template<> COMMONUI_API UClass* StaticClass<UCommonActivatableWidgetStack>()
	{
		return UCommonActivatableWidgetStack::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCommonActivatableWidgetStack(Z_Construct_UClass_UCommonActivatableWidgetStack, &UCommonActivatableWidgetStack::StaticClass, TEXT("/Script/CommonUI"), TEXT("UCommonActivatableWidgetStack"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonActivatableWidgetStack);
	void UCommonActivatableWidgetQueue::StaticRegisterNativesUCommonActivatableWidgetQueue()
	{
	}
	UClass* Z_Construct_UClass_UCommonActivatableWidgetQueue_NoRegister()
	{
		return UCommonActivatableWidgetQueue::StaticClass();
	}
	struct Z_Construct_UClass_UCommonActivatableWidgetQueue_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonActivatableWidgetQueue_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCommonActivatableWidgetContainerBase,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonUI,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonActivatableWidgetQueue_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** \n * A display queue of ActivatableWidget elements. \n *\n * - Only one widget is active/displayed at a time, all others in the queue are deactivated.\n * - When the active widget deactivates, it is automatically removed from the widget, \n *\x09\x09released back to the pool, and the next widget in the queue (if any) is displayed\n */" },
		{ "IncludePath", "Widgets/CommonActivatableWidgetContainer.h" },
		{ "ModuleRelativePath", "Public/Widgets/CommonActivatableWidgetContainer.h" },
		{ "ToolTip", "A display queue of ActivatableWidget elements.\n\n- Only one widget is active/displayed at a time, all others in the queue are deactivated.\n- When the active widget deactivates, it is automatically removed from the widget,\n            released back to the pool, and the next widget in the queue (if any) is displayed" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonActivatableWidgetQueue_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonActivatableWidgetQueue>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCommonActivatableWidgetQueue_Statics::ClassParams = {
		&UCommonActivatableWidgetQueue::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x00B000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonActivatableWidgetQueue_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonActivatableWidgetQueue_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonActivatableWidgetQueue()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCommonActivatableWidgetQueue_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCommonActivatableWidgetQueue, 3556043799);
	template<> COMMONUI_API UClass* StaticClass<UCommonActivatableWidgetQueue>()
	{
		return UCommonActivatableWidgetQueue::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCommonActivatableWidgetQueue(Z_Construct_UClass_UCommonActivatableWidgetQueue, &UCommonActivatableWidgetQueue::StaticClass, TEXT("/Script/CommonUI"), TEXT("UCommonActivatableWidgetQueue"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonActivatableWidgetQueue);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
