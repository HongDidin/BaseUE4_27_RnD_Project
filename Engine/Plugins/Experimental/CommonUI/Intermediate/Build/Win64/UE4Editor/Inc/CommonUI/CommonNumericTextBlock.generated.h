// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UCommonNumericTextBlock;
enum class ECommonNumericType : uint8;
#ifdef COMMONUI_CommonNumericTextBlock_generated_h
#error "CommonNumericTextBlock.generated.h already included, missing '#pragma once' in CommonNumericTextBlock.h"
#endif
#define COMMONUI_CommonNumericTextBlock_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonNumericTextBlock_h_13_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCommonNumberFormattingOptions_Statics; \
	static class UScriptStruct* StaticStruct();


template<> COMMONUI_API UScriptStruct* StaticStruct<struct FCommonNumberFormattingOptions>();

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonNumericTextBlock_h_117_DELEGATE \
struct CommonNumericTextBlock_eventOnInterpolationEnded_Parms \
{ \
	UCommonNumericTextBlock* NumericTextBlock; \
	bool HadCompleted; \
}; \
static inline void FOnInterpolationEnded_DelegateWrapper(const FMulticastScriptDelegate& OnInterpolationEnded, UCommonNumericTextBlock* NumericTextBlock, bool HadCompleted) \
{ \
	CommonNumericTextBlock_eventOnInterpolationEnded_Parms Parms; \
	Parms.NumericTextBlock=NumericTextBlock; \
	Parms.HadCompleted=HadCompleted ? true : false; \
	OnInterpolationEnded.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonNumericTextBlock_h_112_DELEGATE \
struct CommonNumericTextBlock_eventOnOutro_Parms \
{ \
	UCommonNumericTextBlock* NumericTextBlock; \
}; \
static inline void FOnOutro_DelegateWrapper(const FMulticastScriptDelegate& OnOutro, UCommonNumericTextBlock* NumericTextBlock) \
{ \
	CommonNumericTextBlock_eventOnOutro_Parms Parms; \
	Parms.NumericTextBlock=NumericTextBlock; \
	OnOutro.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonNumericTextBlock_h_107_DELEGATE \
struct CommonNumericTextBlock_eventOnInterpolationUpdated_Parms \
{ \
	UCommonNumericTextBlock* NumericTextBlock; \
	float LastValue; \
	float NewValue; \
}; \
static inline void FOnInterpolationUpdated_DelegateWrapper(const FMulticastScriptDelegate& OnInterpolationUpdated, UCommonNumericTextBlock* NumericTextBlock, float LastValue, float NewValue) \
{ \
	CommonNumericTextBlock_eventOnInterpolationUpdated_Parms Parms; \
	Parms.NumericTextBlock=NumericTextBlock; \
	Parms.LastValue=LastValue; \
	Parms.NewValue=NewValue; \
	OnInterpolationUpdated.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonNumericTextBlock_h_102_DELEGATE \
struct CommonNumericTextBlock_eventOnInterpolationStarted_Parms \
{ \
	UCommonNumericTextBlock* NumericTextBlock; \
}; \
static inline void FOnInterpolationStarted_DelegateWrapper(const FMulticastScriptDelegate& OnInterpolationStarted, UCommonNumericTextBlock* NumericTextBlock) \
{ \
	CommonNumericTextBlock_eventOnInterpolationStarted_Parms Parms; \
	Parms.NumericTextBlock=NumericTextBlock; \
	OnInterpolationStarted.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonNumericTextBlock_h_61_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonNumericTextBlock_h_61_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetNumericType); \
	DECLARE_FUNCTION(execIsInterpolatingNumericValue); \
	DECLARE_FUNCTION(execInterpolateToValue); \
	DECLARE_FUNCTION(execSetCurrentValue); \
	DECLARE_FUNCTION(execGetTargetValue);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonNumericTextBlock_h_61_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetNumericType); \
	DECLARE_FUNCTION(execIsInterpolatingNumericValue); \
	DECLARE_FUNCTION(execInterpolateToValue); \
	DECLARE_FUNCTION(execSetCurrentValue); \
	DECLARE_FUNCTION(execGetTargetValue);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonNumericTextBlock_h_61_ARCHIVESERIALIZER \
	DECLARE_FSTRUCTUREDARCHIVE_SERIALIZER(UCommonNumericTextBlock, NO_API)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonNumericTextBlock_h_61_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonNumericTextBlock(); \
	friend struct Z_Construct_UClass_UCommonNumericTextBlock_Statics; \
public: \
	DECLARE_CLASS(UCommonNumericTextBlock, UCommonTextBlock, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonNumericTextBlock) \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonNumericTextBlock_h_61_ARCHIVESERIALIZER


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonNumericTextBlock_h_61_INCLASS \
private: \
	static void StaticRegisterNativesUCommonNumericTextBlock(); \
	friend struct Z_Construct_UClass_UCommonNumericTextBlock_Statics; \
public: \
	DECLARE_CLASS(UCommonNumericTextBlock, UCommonTextBlock, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonNumericTextBlock) \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonNumericTextBlock_h_61_ARCHIVESERIALIZER


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonNumericTextBlock_h_61_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonNumericTextBlock(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonNumericTextBlock) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonNumericTextBlock); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonNumericTextBlock); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonNumericTextBlock(UCommonNumericTextBlock&&); \
	NO_API UCommonNumericTextBlock(const UCommonNumericTextBlock&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonNumericTextBlock_h_61_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonNumericTextBlock(UCommonNumericTextBlock&&); \
	NO_API UCommonNumericTextBlock(const UCommonNumericTextBlock&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonNumericTextBlock); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonNumericTextBlock); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonNumericTextBlock)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonNumericTextBlock_h_61_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__IsPercentage_DEPRECATED() { return STRUCT_OFFSET(UCommonNumericTextBlock, IsPercentage_DEPRECATED); }


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonNumericTextBlock_h_58_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonNumericTextBlock_h_61_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonNumericTextBlock_h_61_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonNumericTextBlock_h_61_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonNumericTextBlock_h_61_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonNumericTextBlock_h_61_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonNumericTextBlock_h_61_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonNumericTextBlock_h_61_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonNumericTextBlock_h_61_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonNumericTextBlock_h_61_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonNumericTextBlock_h_61_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonNumericTextBlock_h_61_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonNumericTextBlock_h_61_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonNumericTextBlock>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonNumericTextBlock_h


#define FOREACH_ENUM_ECOMMONNUMERICTYPE(op) \
	op(ECommonNumericType::Number) \
	op(ECommonNumericType::Percentage) \
	op(ECommonNumericType::Seconds) \
	op(ECommonNumericType::Distance) 

enum class ECommonNumericType : uint8;
template<> COMMONUI_API UEnum* StaticEnum<ECommonNumericType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
