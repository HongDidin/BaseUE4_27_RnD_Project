// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonUI/Public/CommonUISettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCommonUISettings() {}
// Cross Module References
	COMMONUI_API UClass* Z_Construct_UClass_UCommonUISettings_NoRegister();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonUISettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_CommonUI();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonUIRichTextData_NoRegister();
	SLATECORE_API UScriptStruct* Z_Construct_UScriptStruct_FSlateBrush();
// End Cross Module References
	void UCommonUISettings::StaticRegisterNativesUCommonUISettings()
	{
	}
	UClass* Z_Construct_UClass_UCommonUISettings_NoRegister()
	{
		return UCommonUISettings::StaticClass();
	}
	struct Z_Construct_UClass_UCommonUISettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoLoadData_MetaData[];
#endif
		static void NewProp_bAutoLoadData_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoLoadData;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultImageResourceObject_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_DefaultImageResourceObject;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultThrobberMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_DefaultThrobberMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultRichTextDataClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftClassPropertyParams NewProp_DefaultRichTextDataClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultImageResourceObjectInstance_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DefaultImageResourceObjectInstance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultThrobberMaterialInstance_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DefaultThrobberMaterialInstance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultThrobberBrush_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DefaultThrobberBrush;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RichTextDataInstance_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RichTextDataInstance;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonUISettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonUI,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonUISettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "CommonUISettings.h" },
		{ "ModuleRelativePath", "Public/CommonUISettings.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonUISettings_Statics::NewProp_bAutoLoadData_MetaData[] = {
		{ "Category", "Default" },
		{ "Comment", "/** Controls if the data referenced is automatically loaded.\n\x09 *  If False then game code must call LoadData() on it's own.\n\x09 */" },
		{ "ModuleRelativePath", "Public/CommonUISettings.h" },
		{ "ToolTip", "Controls if the data referenced is automatically loaded.\nIf False then game code must call LoadData() on it's own." },
	};
#endif
	void Z_Construct_UClass_UCommonUISettings_Statics::NewProp_bAutoLoadData_SetBit(void* Obj)
	{
		((UCommonUISettings*)Obj)->bAutoLoadData = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCommonUISettings_Statics::NewProp_bAutoLoadData = { "bAutoLoadData", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCommonUISettings), &Z_Construct_UClass_UCommonUISettings_Statics::NewProp_bAutoLoadData_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCommonUISettings_Statics::NewProp_bAutoLoadData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonUISettings_Statics::NewProp_bAutoLoadData_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonUISettings_Statics::NewProp_DefaultImageResourceObject_MetaData[] = {
		{ "AllowedClasses", "Texture2D,MaterialInterface" },
		{ "Category", "Image" },
		{ "Comment", "/** The Default Image Resource, newly created CommonImage Widgets will use this style. */" },
		{ "ModuleRelativePath", "Public/CommonUISettings.h" },
		{ "ToolTip", "The Default Image Resource, newly created CommonImage Widgets will use this style." },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UCommonUISettings_Statics::NewProp_DefaultImageResourceObject = { "DefaultImageResourceObject", nullptr, (EPropertyFlags)0x0044000000004001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonUISettings, DefaultImageResourceObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCommonUISettings_Statics::NewProp_DefaultImageResourceObject_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonUISettings_Statics::NewProp_DefaultImageResourceObject_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonUISettings_Statics::NewProp_DefaultThrobberMaterial_MetaData[] = {
		{ "Category", "Throbber" },
		{ "Comment", "/** The Default Throbber Material, newly created CommonLoadGuard Widget will use this style. */" },
		{ "ModuleRelativePath", "Public/CommonUISettings.h" },
		{ "ToolTip", "The Default Throbber Material, newly created CommonLoadGuard Widget will use this style." },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UCommonUISettings_Statics::NewProp_DefaultThrobberMaterial = { "DefaultThrobberMaterial", nullptr, (EPropertyFlags)0x0044000000004001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonUISettings, DefaultThrobberMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCommonUISettings_Statics::NewProp_DefaultThrobberMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonUISettings_Statics::NewProp_DefaultThrobberMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonUISettings_Statics::NewProp_DefaultRichTextDataClass_MetaData[] = {
		{ "Category", "RichText" },
		{ "Comment", "/** The Default Data for rich text to show inline icon and others. */" },
		{ "ModuleRelativePath", "Public/CommonUISettings.h" },
		{ "ToolTip", "The Default Data for rich text to show inline icon and others." },
	};
#endif
	const UE4CodeGen_Private::FSoftClassPropertyParams Z_Construct_UClass_UCommonUISettings_Statics::NewProp_DefaultRichTextDataClass = { "DefaultRichTextDataClass", nullptr, (EPropertyFlags)0x0044000000004001, UE4CodeGen_Private::EPropertyGenFlags::SoftClass, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonUISettings, DefaultRichTextDataClass), Z_Construct_UClass_UCommonUIRichTextData_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCommonUISettings_Statics::NewProp_DefaultRichTextDataClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonUISettings_Statics::NewProp_DefaultRichTextDataClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonUISettings_Statics::NewProp_DefaultImageResourceObjectInstance_MetaData[] = {
		{ "ModuleRelativePath", "Public/CommonUISettings.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UCommonUISettings_Statics::NewProp_DefaultImageResourceObjectInstance = { "DefaultImageResourceObjectInstance", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonUISettings, DefaultImageResourceObjectInstance), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCommonUISettings_Statics::NewProp_DefaultImageResourceObjectInstance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonUISettings_Statics::NewProp_DefaultImageResourceObjectInstance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonUISettings_Statics::NewProp_DefaultThrobberMaterialInstance_MetaData[] = {
		{ "ModuleRelativePath", "Public/CommonUISettings.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UCommonUISettings_Statics::NewProp_DefaultThrobberMaterialInstance = { "DefaultThrobberMaterialInstance", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonUISettings, DefaultThrobberMaterialInstance), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCommonUISettings_Statics::NewProp_DefaultThrobberMaterialInstance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonUISettings_Statics::NewProp_DefaultThrobberMaterialInstance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonUISettings_Statics::NewProp_DefaultThrobberBrush_MetaData[] = {
		{ "ModuleRelativePath", "Public/CommonUISettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCommonUISettings_Statics::NewProp_DefaultThrobberBrush = { "DefaultThrobberBrush", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonUISettings, DefaultThrobberBrush), Z_Construct_UScriptStruct_FSlateBrush, METADATA_PARAMS(Z_Construct_UClass_UCommonUISettings_Statics::NewProp_DefaultThrobberBrush_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonUISettings_Statics::NewProp_DefaultThrobberBrush_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonUISettings_Statics::NewProp_RichTextDataInstance_MetaData[] = {
		{ "ModuleRelativePath", "Public/CommonUISettings.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UCommonUISettings_Statics::NewProp_RichTextDataInstance = { "RichTextDataInstance", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonUISettings, RichTextDataInstance), Z_Construct_UClass_UCommonUIRichTextData_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCommonUISettings_Statics::NewProp_RichTextDataInstance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonUISettings_Statics::NewProp_RichTextDataInstance_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCommonUISettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonUISettings_Statics::NewProp_bAutoLoadData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonUISettings_Statics::NewProp_DefaultImageResourceObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonUISettings_Statics::NewProp_DefaultThrobberMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonUISettings_Statics::NewProp_DefaultRichTextDataClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonUISettings_Statics::NewProp_DefaultImageResourceObjectInstance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonUISettings_Statics::NewProp_DefaultThrobberMaterialInstance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonUISettings_Statics::NewProp_DefaultThrobberBrush,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonUISettings_Statics::NewProp_RichTextDataInstance,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonUISettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonUISettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCommonUISettings_Statics::ClassParams = {
		&UCommonUISettings::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UCommonUISettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UCommonUISettings_Statics::PropPointers),
		0,
		0x001000A6u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonUISettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonUISettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonUISettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCommonUISettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCommonUISettings, 965244308);
	template<> COMMONUI_API UClass* StaticClass<UCommonUISettings>()
	{
		return UCommonUISettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCommonUISettings(Z_Construct_UClass_UCommonUISettings, &UCommonUISettings::StaticClass, TEXT("/Script/CommonUI"), TEXT("UCommonUISettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonUISettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
