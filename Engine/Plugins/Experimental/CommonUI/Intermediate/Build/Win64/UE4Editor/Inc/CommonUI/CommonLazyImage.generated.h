// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UObject;
class UMaterialInterface;
class UTexture2D;
#ifdef COMMONUI_CommonLazyImage_generated_h
#error "CommonLazyImage.generated.h already included, missing '#pragma once' in CommonLazyImage.h"
#endif
#define COMMONUI_CommonLazyImage_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyImage_h_23_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyImage_h_23_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetMaterialTextureParamName); \
	DECLARE_FUNCTION(execIsLoading); \
	DECLARE_FUNCTION(execSetBrushFromLazyDisplayAsset); \
	DECLARE_FUNCTION(execSetBrushFromLazyMaterial); \
	DECLARE_FUNCTION(execSetBrushFromLazyTexture);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyImage_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetMaterialTextureParamName); \
	DECLARE_FUNCTION(execIsLoading); \
	DECLARE_FUNCTION(execSetBrushFromLazyDisplayAsset); \
	DECLARE_FUNCTION(execSetBrushFromLazyMaterial); \
	DECLARE_FUNCTION(execSetBrushFromLazyTexture);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyImage_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonLazyImage(); \
	friend struct Z_Construct_UClass_UCommonLazyImage_Statics; \
public: \
	DECLARE_CLASS(UCommonLazyImage, UImage, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonLazyImage)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyImage_h_23_INCLASS \
private: \
	static void StaticRegisterNativesUCommonLazyImage(); \
	friend struct Z_Construct_UClass_UCommonLazyImage_Statics; \
public: \
	DECLARE_CLASS(UCommonLazyImage, UImage, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonLazyImage)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyImage_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonLazyImage(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonLazyImage) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonLazyImage); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonLazyImage); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonLazyImage(UCommonLazyImage&&); \
	NO_API UCommonLazyImage(const UCommonLazyImage&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyImage_h_23_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonLazyImage(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonLazyImage(UCommonLazyImage&&); \
	NO_API UCommonLazyImage(const UCommonLazyImage&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonLazyImage); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonLazyImage); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonLazyImage)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyImage_h_23_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__LoadingBackgroundBrush() { return STRUCT_OFFSET(UCommonLazyImage, LoadingBackgroundBrush); } \
	FORCEINLINE static uint32 __PPO__MaterialTextureParamName() { return STRUCT_OFFSET(UCommonLazyImage, MaterialTextureParamName); } \
	FORCEINLINE static uint32 __PPO__BP_OnLoadingStateChanged() { return STRUCT_OFFSET(UCommonLazyImage, BP_OnLoadingStateChanged); }


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyImage_h_20_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyImage_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyImage_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyImage_h_23_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyImage_h_23_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyImage_h_23_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyImage_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyImage_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyImage_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyImage_h_23_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyImage_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyImage_h_23_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyImage_h_23_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class CommonLazyImage."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonLazyImage>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyImage_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
