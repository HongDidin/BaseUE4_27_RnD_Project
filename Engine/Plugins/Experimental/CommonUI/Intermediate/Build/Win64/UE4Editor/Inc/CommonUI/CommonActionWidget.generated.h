// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FSlateBrush;
struct FDataTableRowHandle;
#ifdef COMMONUI_CommonActionWidget_generated_h
#error "CommonActionWidget.generated.h already included, missing '#pragma once' in CommonActionWidget.h"
#endif
#define COMMONUI_CommonActionWidget_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionWidget_h_51_DELEGATE \
struct CommonActionWidget_eventOnInputMethodChanged_Parms \
{ \
	bool bUsingGamepad; \
}; \
static inline void FOnInputMethodChanged_DelegateWrapper(const FMulticastScriptDelegate& OnInputMethodChanged, bool bUsingGamepad) \
{ \
	CommonActionWidget_eventOnInputMethodChanged_Parms Parms; \
	Parms.bUsingGamepad=bUsingGamepad ? true : false; \
	OnInputMethodChanged.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionWidget_h_17_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionWidget_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execIsHeldAction); \
	DECLARE_FUNCTION(execSetIconRimBrush); \
	DECLARE_FUNCTION(execSetInputActions); \
	DECLARE_FUNCTION(execSetInputAction); \
	DECLARE_FUNCTION(execGetDisplayText); \
	DECLARE_FUNCTION(execGetIcon);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionWidget_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execIsHeldAction); \
	DECLARE_FUNCTION(execSetIconRimBrush); \
	DECLARE_FUNCTION(execSetInputActions); \
	DECLARE_FUNCTION(execSetInputAction); \
	DECLARE_FUNCTION(execGetDisplayText); \
	DECLARE_FUNCTION(execGetIcon);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionWidget_h_17_ARCHIVESERIALIZER \
	DECLARE_FSTRUCTUREDARCHIVE_SERIALIZER(UCommonActionWidget, NO_API)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionWidget_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonActionWidget(); \
	friend struct Z_Construct_UClass_UCommonActionWidget_Statics; \
public: \
	DECLARE_CLASS(UCommonActionWidget, UWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonActionWidget) \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionWidget_h_17_ARCHIVESERIALIZER


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionWidget_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUCommonActionWidget(); \
	friend struct Z_Construct_UClass_UCommonActionWidget_Statics; \
public: \
	DECLARE_CLASS(UCommonActionWidget, UWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonActionWidget) \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionWidget_h_17_ARCHIVESERIALIZER


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionWidget_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonActionWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonActionWidget) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonActionWidget); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonActionWidget); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonActionWidget(UCommonActionWidget&&); \
	NO_API UCommonActionWidget(const UCommonActionWidget&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionWidget_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonActionWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonActionWidget(UCommonActionWidget&&); \
	NO_API UCommonActionWidget(const UCommonActionWidget&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonActionWidget); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonActionWidget); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonActionWidget)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionWidget_h_17_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__InputActions() { return STRUCT_OFFSET(UCommonActionWidget, InputActions); } \
	FORCEINLINE static uint32 __PPO__ProgressDynamicMaterial() { return STRUCT_OFFSET(UCommonActionWidget, ProgressDynamicMaterial); }


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionWidget_h_14_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionWidget_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionWidget_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionWidget_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionWidget_h_17_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionWidget_h_17_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionWidget_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionWidget_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionWidget_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionWidget_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionWidget_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionWidget_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionWidget_h_17_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class CommonActionWidget."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonActionWidget>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionWidget_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
